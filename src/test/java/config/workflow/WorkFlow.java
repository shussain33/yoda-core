package config.workflow;


import com.softcell.config.ComponentConfiguration;
import com.softcell.constants.ComponentConfigurationType;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ComponentConfigurationMongoRepository;
import com.softcell.dao.mongodb.repository.ComponentConfigurationRepository;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.ComponentSetting;
import com.softcell.workflow.component.module.ModuleSetting;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.*;

/**
 *
 */
public class WorkFlow {


	public static void main(String[] args) {

			MongoTemplate mongoTemplate=MongoConfig.getMongoTemplate();
		
		ComponentConfigurationRepository componentConfigurationRepository = new ComponentConfigurationMongoRepository(mongoTemplate);

		ComponentSetting componentSetting = new ComponentSetting();
		componentSetting.setInstitutionName("HDBFS");

		Map<Integer, List<Component>> componentMap = new TreeMap<Integer, List<Component>>();
		Component component = new Component();
		component.setComponentId("dedupeProcessor");
		component.setComponentName("Dedupe");

		Map<String, ModuleSetting> moduleSettingMap = new TreeMap<String, ModuleSetting>();
		ModuleSetting moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("NegativeDedupe");
		moduleSetting.setModuleName("Deduped");
		moduleSettingMap.put("NegativeDedupe", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);

		ArrayList<Component> componentsList = new ArrayList<Component>();
		componentsList.add(component);

		component = new Component();
		component.setComponentId("multiBureauProcessor");
		component.setComponentName("MB");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("multiBureauExecutor");
		moduleSetting.setModuleName("MB");
		moduleSettingMap.put("multiBureauExecutor", moduleSetting);

		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		component = new Component();
		component.setComponentId("kycProcessor");
		component.setComponentName("kycProcessor");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("panVarificationExecuter");
		moduleSetting.setModuleName("kycProcessor");
		moduleSettingMap.put("panVarificationExecuter", moduleSetting);

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setModuleId("aadharExecutor");
		moduleSetting.setModuleName("aadharExecutor");
		moduleSettingMap.put("aadharExecutor", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		componentMap.put(1, componentsList);
		// ---

		componentsList = new ArrayList<Component>();
		component = new Component();
		component.setComponentId("ntcProcessor");
		component.setComponentName("NTC-Processor");
		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("ntcExecutor");
		moduleSetting.setModuleName("ntc");
		moduleSettingMap.put("ntcExecutor", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);
		componentMap.put(2, componentsList);

		// ---
		componentsList = new ArrayList<Component>();
		component = new Component();
		component.setComponentId("scoringProcessor");
		component.setComponentName("SCORING");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("scoringExecutor");
		moduleSetting.setModuleName("Scroring");
		moduleSettingMap.put("scoringExecutor", moduleSetting);

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("verificationScoring");
		moduleSetting.setModuleName("verificationScoring");
		moduleSettingMap.put("verificationScoring", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);
		componentMap.put(3, componentsList);

		/*******************************/

		componentSetting.setComponentMap(componentMap);

		ComponentConfiguration componentConfiguration=new ComponentConfiguration();
		componentConfiguration.setComponentConfigType(ComponentConfigurationType.BASIC);
		componentConfiguration.setComponentSettings(componentSetting);
		componentConfiguration.setInstitutionId("4019");
		componentConfiguration.setInsertDate(new Date());
		componentConfiguration.setActive(true);
		componentConfigurationRepository.create(componentConfiguration);
		
		
		/******************* end ***********/

		/************** DMI Start ***************/
		componentSetting = new ComponentSetting();
		componentSetting.setInstitutionName("DMI");

		componentMap = new TreeMap<Integer, List<Component>>();
		component = new Component();
		component.setComponentId("dedupeProcessor");
		component.setComponentName("Dedupe");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setCoApplicant(true);
		moduleSetting.setModuleId("NegativeDedupe");
		moduleSetting.setModuleName("Deduped");
		moduleSettingMap.put("NegativeDedupe", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);

		componentsList = new ArrayList<Component>();
		componentsList.add(component);

		component = new Component();
		component.setComponentId("multiBureauProcessor");
		component.setComponentName("MB");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setCoApplicant(true);
		moduleSetting.setModuleId("multiBureauExecutor");
		moduleSetting.setModuleName("Deduped");
		moduleSettingMap.put("multiBureauExecutor", moduleSetting);

		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		component = new Component();
		component.setComponentId("kycProcessor");
		component.setComponentName("kycProcessor");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setCoApplicant(true);
		moduleSetting.setModuleId("panVarificationExecuter");
		moduleSetting.setModuleName("kycProcessor");
		moduleSettingMap.put("panVarificationExecuter", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		componentMap.put(1, componentsList);

		componentsList = new ArrayList<Component>();
		component = new Component();
		component.setComponentId("scoringProcessor");
		component.setComponentName("SCORING");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setCoApplicant(true);
		moduleSetting.setModuleId("scoringExecutor");
		moduleSetting.setModuleName("Scroring");
		moduleSettingMap.put("scoringExecutor", moduleSetting);

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setCoApplicant(false);
		moduleSetting.setModuleId("verificationScoring");
		moduleSetting.setModuleName("Scroring");
		moduleSettingMap.put("verificationScoring", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);
		componentMap.put(2, componentsList);

		// Addind for sales force integration
		component = new Component();
		component.setComponentId("salesforceProcessor");
		component.setComponentName("salesforce");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("salesforceExecutor");
		moduleSetting.setModuleName("Salesforce");
		moduleSettingMap.put("Salesforce", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);

		componentsList = new ArrayList<Component>();
		componentsList.add(component);

		componentMap.put(3, componentsList);

		// Amazon S3 component
		component = new Component();
		component.setComponentId("amazonS3Processor");
		component.setComponentName("amazonS3");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("amazonS3ServiceExecutor");
		moduleSetting.setModuleName("AmazonS3");
		moduleSettingMap.put("amazonS3ServiceExecutor", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);

		componentsList = new ArrayList<Component>();
		componentsList.add(component);

		componentMap.put(4, componentsList);

		/*******************************/

		componentSetting.setComponentMap(componentMap);

		
		componentConfiguration=new ComponentConfiguration();
		componentConfiguration.setComponentConfigType(ComponentConfigurationType.BASIC);
		componentConfiguration.setComponentSettings(componentSetting);
		componentConfiguration.setInstitutionId("4011");
		componentConfiguration.setInsertDate(new Date());
		componentConfiguration.setActive(true);
		componentConfigurationRepository.create(componentConfiguration);
		/************** DMI END ***********/

		/************** HDFC LIMITED Start ***************/
		componentSetting = new ComponentSetting();
		componentSetting.setInstitutionName("HDFC LIMITED");

		componentMap = new TreeMap<Integer, List<Component>>();
		component = new Component();
		component.setComponentId("dedupeProcessor");
		component.setComponentName("Dedupe");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setModuleId("NegativeDedupe");
		moduleSetting.setModuleName("Deduped");
		moduleSettingMap.put("NegativeDedupe", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);

		componentsList = new ArrayList<Component>();
		componentsList.add(component);

		component = new Component();
		component.setComponentId("multiBureauProcessor");
		component.setComponentName("MB");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("multiBureauExecutor");
		moduleSetting.setModuleName("Deduped");
		moduleSettingMap.put("multiBureauExecutor", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		component = new Component();
		component.setComponentId("kycProcessor");
		component.setComponentName("kycProcessor");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setModuleId("panVarificationExecuter");
		moduleSetting.setModuleName("kycProcessor");
		moduleSettingMap.put("panVarificationExecuter", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		componentMap.put(1, componentsList);

		componentsList = new ArrayList<Component>();
		component = new Component();
		component.setComponentId("scoringProcessor");
		component.setComponentName("SCORING");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("scoringExecutor");
		moduleSetting.setModuleName("Scroring");
		moduleSettingMap.put("scoringExecutor", moduleSetting);

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setModuleId("verificationScoring");
		moduleSetting.setModuleName("Scroring");
		moduleSettingMap.put("verificationScoring", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);
		componentMap.put(2, componentsList);

		componentSetting.setComponentMap(componentMap);

		componentConfiguration=new ComponentConfiguration();
		componentConfiguration.setComponentConfigType(ComponentConfigurationType.BASIC);
		componentConfiguration.setComponentSettings(componentSetting);
		componentConfiguration.setInstitutionId("3994");
		componentConfiguration.setInsertDate(new Date());
		componentConfiguration.setActive(true);
		componentConfigurationRepository.create(componentConfiguration);
		
		/************** HDFC LIMITED END ***********/

		componentSetting = new ComponentSetting();
		componentSetting.setInstitutionName("TATA CAPITAL");

		componentMap = new TreeMap<Integer, List<Component>>();
		component = new Component();
		component.setComponentId("dedupeProcessor");
		component.setComponentName("Dedupe");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setModuleId("NegativeDedupe");
		moduleSetting.setModuleName("Deduped");
		moduleSettingMap.put("NegativeDedupe", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);

		componentsList = new ArrayList<Component>();
		componentsList.add(component);

		component = new Component();
		component.setComponentId("multiBureauProcessor");
		component.setComponentName("MB");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("multiBureauExecutor");
		moduleSetting.setModuleName("MB");
		moduleSettingMap.put("multiBureauExecutor", moduleSetting);

		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		component = new Component();
		component.setComponentId("kycProcessor");
		component.setComponentName("kycProcessor");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("panVarificationExecuter");
		moduleSetting.setModuleName("kycProcessor");
		moduleSettingMap.put("panVarificationExecuter", moduleSetting);

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(false);
		moduleSetting.setModuleId("aadharExecutor");
		moduleSetting.setModuleName("aadharExecutor");
		moduleSettingMap.put("aadharExecutor", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);

		componentMap.put(1, componentsList);
		// ---

		componentsList = new ArrayList<Component>();
		component = new Component();
		component.setComponentId("ntcProcessor");
		component.setComponentName("NTC-Processor");
		moduleSettingMap = new TreeMap<String, ModuleSetting>();
		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("ntcExecutor");
		moduleSetting.setModuleName("ntc");
		moduleSettingMap.put("ntcExecutor", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);
		componentMap.put(2, componentsList);

		// ---
		componentsList = new ArrayList<Component>();
		component = new Component();
		component.setComponentId("scoringProcessor");
		component.setComponentName("SCORING");

		moduleSettingMap = new TreeMap<String, ModuleSetting>();

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("scoringExecutor");
		moduleSetting.setModuleName("Scroring");
		moduleSettingMap.put("scoringExecutor", moduleSetting);

		moduleSetting = new ModuleSetting();
		moduleSetting.setActive(true);
		moduleSetting.setModuleId("verificationScoring");
		moduleSetting.setModuleName("verificationScoring");
		moduleSettingMap.put("verificationScoring", moduleSetting);
		component.setModuleSettingMap(moduleSettingMap);
		componentsList.add(component);
		componentMap.put(3, componentsList);

		componentSetting.setComponentMap(componentMap);

		componentConfiguration = new ComponentConfiguration();
		componentConfiguration
				.setComponentConfigType(ComponentConfigurationType.BASIC);
		componentConfiguration.setComponentSettings(componentSetting);
		componentConfiguration.setInstitutionId("4045");
		componentConfiguration.setInsertDate(new Date());
		componentConfiguration.setActive(true);
		componentConfigurationRepository.create(componentConfiguration);

		componentConfiguration = componentConfigurationRepository
				.getByInstitutionIdAndType(componentConfiguration.getInstitutionId(),
						ComponentConfigurationType.BASIC);

		System.out.println(componentConfiguration);
	}
}
