package com.softcell.utils;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by amit on 22/2/18.
 * Creates Pojo from a csv file.
 * Csv file should follow following standards ::
 *  --> fileName same as className
 *  --> first line of csv file must have package declaration in 1st cell
 *  --> Second line onwards each line defines 1 class member variable and have following cells
 *          cell[1](Must)= variableName (multiple words can be with space seperated)
 *          cell[2](Must)= datatype (datatype for that variable)
 *          cell[3](Optional)= jsonProperty name
 *
 *  In current class
 *      csvFileLocation = folder location of csv files ["/home/amit/Downloads/CSV"]
 *      pojoCreationDirectory = directory location from project where to create class ["/home/amit/Documents/Project/Yoda-core_Sprit2/yoda-core/src/main/java/com/softcell/gonogo/model/core"]
 */
public class POJOBuilder {

    static List<String> fileNames;
    static String csvFileLocation = "/home/amit/Downloads/CSV";
    static String pojoCreationDirectory = "/home/amit/Documents/Project/Yoda-core_Sprit2/yoda-core/src/main/java/com/softcell/gonogo/model/core";

    static int PKG_DECL_ROW = 0;
    static int COMMENT_CELL = 0;
    static int NAME_CELL = 1;
    static int DATA_TYPE_CELL = 2;
    static int JSON_PROP_CELL = 3;
    static{
        fileNames = new ArrayList<>();
        File folder = new File(csvFileLocation);
        if(folder.exists()) {
            for (File file : folder.listFiles()) {
                if(file.getName().contains(".csv"))
                    fileNames.add(file.getName());
            }
        }
    }

    @Test
    public void buildPojo(){

        fileNames.forEach((String fileName) -> {

            List<List<String>> values = readCsvFile(csvFileLocation + File.separator + fileName);
            String pojoName = fileName.replace(".csv", "");

            if(!CollectionUtils.isEmpty(values) && !CollectionUtils.isEmpty(values.get(0)))
            {
                File newPojo = createPojo(pojoName);
                if(newPojo != null) {
                    StringBuilder pojoData = new StringBuilder();
                    createPackageDeclaration(values, pojoData);
                    createImportsForPOJO(pojoData);
                    classDeclarationStart(pojoName, pojoData);
                    createVariableDecleartion(values, pojoData);
                    classDeclarationEnd(pojoName, pojoData);
                    writeDataToFile(newPojo, pojoData);
                }
            }
        });
    }

    private void writeDataToFile(File newPojo, StringBuilder pojoData) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(newPojo));
            writer.write(pojoData.toString());
            writer.close();
        } catch(Exception e){
            System.out.println("Error while writing in file.");
        }
    }

    private void createVariableDecleartion(List<List<String>> values, StringBuilder pojoData) {

        values.forEach(variableData -> {

            if(variableData.get(NAME_CELL) != null){
                StringBuilder temp = new StringBuilder();
                String[] tempArray = variableData.get(NAME_CELL).split("\\s");
                for (int i = 0; i < tempArray.length ; i++) {
                    if(tempArray[i].length() > 0) {
                        if (i == 0) {
                            StringBuilder b = new StringBuilder(tempArray[i]);
                            b.setCharAt(0, Character.toLowerCase(tempArray[i].charAt(0)));
                            temp.append(b.toString());
                        } else {
                            StringBuilder b = new StringBuilder(tempArray[i]);
                            b.setCharAt(0, Character.toUpperCase(tempArray[i].charAt(0)));
                            temp.append(b.toString());
                        }
                    }
                }
                if(!StringUtils.isEmpty(variableData.get(COMMENT_CELL)))
                    pojoData.append("\t/* " + variableData.get(COMMENT_CELL) + " */\n");
                if(variableData.get(JSON_PROP_CELL) != null)
                    pojoData.append("\t@JsonProperty(\"" + variableData.get(JSON_PROP_CELL) + "\")\n");
                pojoData.append("\tprivate " + variableData.get(DATA_TYPE_CELL) + " " + temp.toString()+";\n\n");
            }
        });
    }

    private void classDeclarationStart(String pojoName, StringBuilder pojoData) {
        pojoData.append("/**\n\n * Created by Amit on 26/2/18.\n */\n\n");
        pojoData.append("@Data\n@Builder\n@AllArgsConstructor\n@NoArgsConstructor\n");
        pojoData.append("public class " + pojoName + "{\n\n");
    }

    private void classDeclarationEnd(String pojoName, StringBuilder pojoData) {
        pojoData.append("}\n");
    }

    private void createImportsForPOJO(StringBuilder pojoData) {
        pojoData.append("import java.util.*;\n");
        pojoData.append("import lombok.*;\n");
        pojoData.append("import com.fasterxml.jackson.annotation.JsonProperty;\n");
    }

    private void createPackageDeclaration(List<List<String>> values, StringBuilder pojoData) {
        pojoData.append("package " + values.get(PKG_DECL_ROW).get(0) + ";\n\n");
        values.remove(0);
    }


    public List readCsvFile(String filePath){
        List<String> columns;
        List<List<String>> values = new ArrayList<>();
        Path path = Paths.get(filePath);
        if (Files.exists(path)) {
            try (BufferedReader br = Files.newBufferedReader(path)) {
                values = br.lines()
                        .map(line -> Arrays.asList(line.split(",")))
                        .collect(Collectors.toList());
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        return values;
    }

    private File createPojo(String pojoName) {


        if(!new File(pojoCreationDirectory).exists())
            new File(pojoCreationDirectory).mkdirs();

        File file = new File(pojoCreationDirectory + File.separator + pojoName + ".java");
        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("Error while creating file.");
            return null;
        }
        return file;
    }
}


