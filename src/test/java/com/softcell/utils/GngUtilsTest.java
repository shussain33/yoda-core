package com.softcell.utils;

import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by archana on 20/8/18.
 */
public class GngUtilsTest {

    @Test
    public void testCsvWithComma(){
        List<String> ss = new ArrayList<String>(){{ add("address 1,road"); add("line 2 , area");}};
        List<String> quotedss = addQuotes2List(ss, ",");
        System.out.println(ss);
        System.out.println(quotedss);
    }
    public List<String> addQuotes2List(List<String> list, String delimiter){
        return list.stream()
                .map(element ->  "\"" + element + "\"")
                .collect(Collectors.toList());
    }

    private List<String> addQuotes(String[] entries) {
        return Arrays.stream(entries)
                .map(entry -> String.format("%s", entry))
                .collect(Collectors.toList());
    }

    @Test
    public void testGetRequestStringValueForSingleField(){
        String s ="mamam";
        String s1 ="mnmn,n";


        //System.out.print( s.replaceAll(", $", ""));
        System.out.print( s1.replaceAll(",", "").trim());
    }

    @Test
    public void testGetRequestStringValueForInnerObjectField(){
        ApplicationRequest request = new ApplicationRequest();
        Header header = new Header();
        header.setCroId("cro name");
        request.setHeader(header);
        Assert.assertEquals("cro name", GngUtils.getRequestStringValue(request, "header.croId", ApplicationRequest.class));
    }

    @Test
    public void testGetRequestStringValueForInnerListObjectField(){

        ApplicationRequest request = new ApplicationRequest();
        Header header = new Header();
        header.setCroId("cro name");
        request.setHeader(header);

        CamDetails camDetails = new CamDetails();
        List<RTRDetail> list = new ArrayList<RTRDetail>();
        RTRDetail r1 = new RTRDetail();
        r1.setTotalOutstandingAmount(2345.9d);
        RTRDetail r2 = new RTRDetail();
        r2.setTotalOutstandingAmount(5555.9d);
        list.add(r1);
        list.add(r2);
        camDetails.setRtrDetailList(list);

        Assert.assertEquals("2345.9,5555.9", GngUtils.getRequestStringValue(camDetails, "rtrDetailList.rtrTransactions.loanAmount", CamDetails.class));
    }
}
