package com.softcell.utils;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class DataCleanerTest {

	@Test
	  public void clenaDate(){
		  assert StringUtils.equalsIgnoreCase("Kishor '", DataCleaner.cleanInvisible("Kishor \n'"));
	  }
}
