package com.softcell.workflow;

import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.workflow.component.manager.ComponentManager;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.Test;

/**
 * Created by archana on 3/10/17.
 */
public class ComponentsExecutionTest {
    private final String institutionId = "TEST";
    private final String executorUsage = "TEST_EXECUTOR";
    private final String completedStage = "COMPLETED_STAGE_2";

    /*@Test
    public void testNewWorking(){
        Cache cache = new Cache();
        System.out.println("Executing for "+ institutionId);
        ComponentManager componentManager = new ComponentManager(institutionId);
        componentManager.execute();
    }*/

    /*@Test
    public void testThreadExecutorService(){
        Cache cache = new Cache();
        System.out.println("Executing for "+ executorUsage);
        ComponentManager componentManager = new ComponentManager(executorUsage);
        componentManager.execute();
    }*/

    /*@Test
    public void testMultiBreExecution(){
        Cache cache = new Cache();
        System.out.println( String.format( "Executing for %s stage %s ", executorUsage, completedStage) );
        //ComponentManager componentManager = new ComponentManager(executorUsage, completedStage);
        GoNoGoCustomerApplication app = new GoNoGoCustomerApplication();
        app.setApplicationRequest(new ApplicationRequest());
        app.getApplicationRequest().setHeader(new Header());
        app.getApplicationRequest().getHeader().setInstitutionId("4075");
        ComponentManager componentManager = new ComponentManager(app, "fresh-application");
        componentManager.execute();
    }*/

    @Test
    public void test(){
        Map<Integer, String> HOSTING = new HashMap<>();
        HOSTING.put(1, "linode.com");
        HOSTING.put(2, "heroku.com");
        HOSTING.put(3, "digitalocean.com");
        HOSTING.put(4, "aws.amazon.com");

        //Map -> Stream -> Filter -> MAP
        Map<Integer, String> collect = HOSTING.entrySet().stream()
                .filter(map -> map.getKey() == 2)
                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
    }
}
