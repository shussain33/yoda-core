package com.softcell.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.config.MultiProductConfiguration;
import com.softcell.dao.mongodb.repository.multiproduct.MultiProductRepository;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.multiproduct.MultiProductConfigRequest;
import com.softcell.rest.controllers.ConfigurationController;
import com.softcell.service.impl.ConfigurationManagerImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;

/**
 * Test Cases to be visited before making this class {@see ConfigurationController}
 * productionalized
 * <p>
 * Created by prateek on 25/1/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:mongo-data.xml"})
@WebAppConfiguration
public class ConfigurationControllerTest {


    @Mock
    ConfigurationManagerImpl configurationManager;


    @Mock
    MultiProductRepository multiProductRepository;


    @InjectMocks
    private ConfigurationController configurationController;

    private MockMvc mockMvc;


    /**
     * this {@see ConfigurationControllerTest#setup} setup method contains all the resources required by test cases
     * this method will execute after ConfigurationControllerTest constructor i.e after instantiation
     * of this class
     */
    @Before
    public void setup() {

        // setup resources to be used in with test cases

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(configurationController).build();

    }

    @Test
    public void testDeleteMultiproductConfiguration() throws Exception {

    }

    @Test
    public void testUpdateMultiproductConfiguration() throws Exception {

    }

    @Test
    public void testFindAllMultiproductConfiguration() throws Exception {

    }

    @Test
    public void testSetVersion() throws Exception {

    }

    @Test
    public void testRemoveVersion() throws Exception {

    }

    @Test
    public void testShowVersion() throws Exception {

    }

    @Test
    public void testIsValidVersion() throws Exception {

    }

    @Test
    public void testSetInstitutionProductConfiguration() throws Exception {

    }

    @Test
    public void testRemoveInstitutionProductConfiguration() throws Exception {

    }

    @Test
    public void testUpdateInstitutionProductConfiguration() throws Exception {

    }

    @Test
    public void testGetInstitutionProductConfiguration() throws Exception {

    }

    @Test
    public void testAddTemplateConfiguration() throws Exception {

    }

    @Test
    public void testUpdateTemplatePath() throws Exception {

    }

    @Test
    public void testUpdateTemplateLogo() throws Exception {

    }

    @Test
    public void testEnableTemplate() throws Exception {

    }

    @Test
    public void testDisableTemplate() throws Exception {

    }

    @Test
    public void testGetTemplateConfiguration() throws Exception {

    }

    @Test
    public void testAddActionConfiguration() throws Exception {

    }

    @Test
    public void testUpdateActionConfiguration() throws Exception {

    }

    @Test
    public void testDeleteActionConfiguration() throws Exception {

    }

    @Test
    public void testAddSmsConfiguration() throws Exception {

    }

    @Test
    public void testAddEmailConfiguration() throws Exception {

    }

    @Test
    public void testUpdateEmailConfiguration() throws Exception {

    }

    @Test
    public void testDisableEmailConfiguration() throws Exception {

    }

    @Test
    public void testEnableEmailConfiguration() throws Exception {

    }

    @Test
    public void testGetAllEmailConfiguration() throws Exception {

    }

    @Test
    public void testUpdateSmsConfiguration() throws Exception {

    }

    @Test
    public void testDeleteSmsConfiguration() throws Exception {

    }

    private MultiProductConfigRequest buildDummyObject() throws JsonProcessingException {

        Header header = new Header();
        header.setInstitutionId("4019");
        header.setDateTime(new Date());
        MultiProductConfiguration multiProductConfiguration = new MultiProductConfiguration();
        multiProductConfiguration.setInstitutionID("4019");
        return MultiProductConfigRequest.getBuilder(header)
                .multiProductConfiguration(multiProductConfiguration)
                .build();

    }
}