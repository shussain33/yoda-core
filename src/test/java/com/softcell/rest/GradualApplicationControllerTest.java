package com.softcell.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.WebConfigTest;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class GradualApplicationControllerTest {

    private ObjectMapper objectMapper;

    private ApplicationRequest applicationRequest;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup() throws Exception{

        objectMapper = new ObjectMapper();

        File file = new File("src/test/resources/RequestJson/ApplicationRequest.json");
        applicationRequest = objectMapper.readValue(file, ApplicationRequest.class);

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    }


    @Test
    public void saveApplicationStep1() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.SUBMIT_APPLICATION_STEPID, "step1").
                        content(TestUtils.convertObjectToJsonBytes(applicationRequest)).
                        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.oBody.payLoad.sStat", is(Status.SUCCESS.name())));

    }

    @Test
    public void saveApplicationStep2() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.SUBMIT_APPLICATION_STEPID, "step2").
                        content(TestUtils.convertObjectToJsonBytes(applicationRequest)).
                        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.oBody.payLoad.sStat", is(Status.SUCCESS.name())));

    }

    @Test
    public void saveApplicationStep3() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.SUBMIT_APPLICATION_STEPID, "step3").
                        content(TestUtils.convertObjectToJsonBytes(applicationRequest)).
                        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.oBody.payLoad.sStat", is(Status.SUCCESS.name())));

    }

    @Test
    public void saveApplicationStep4() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.SUBMIT_APPLICATION_STEPID, "step4").
                        content(TestUtils.convertObjectToJsonBytes(applicationRequest)).
                        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.oBody.payLoad.sStat", is(Status.SUCCESS.name())));

    }


    @Test
    public void validateApplicationRequest() throws Exception {

    }

}