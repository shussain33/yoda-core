package com.softcell.rest;

import com.softcell.app.config.web.WebConfig;
import com.softcell.constants.RestClientType;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.security.ChangePasswordRequest;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.gonogo.model.security.LogoutRequest;
import com.softcell.rest.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


/**
 * Test class for home controller
 *
 * @author prateek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfig.class})
@WebAppConfiguration
public class HomeControllerTest {


    private MockMvc mockMvc;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getLogin() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/login-web")
                        .content(TestUtils.convertObjectToJsonBytes(buildRequest()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void logout() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/logout")
                        .content(TestUtils.convertObjectToJsonBytes(builderRequest()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void changePassword() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/change-password-gng")
                        .content(TestUtils.convertObjectToJsonBytes(getChangePasswordRequest()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void resetPassword() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/reset-password-gng")
                        .content(TestUtils.convertObjectToJsonBytes(getChangePasswordRequest()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());

    }


    private LoginRequest buildRequest() {


        LoginRequest loginRequest = new LoginRequest();

        loginRequest.setPassword("5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8");
        loginRequest.setHeader(getHeader());
        loginRequest.setUserName("HDBFS_CRO1@softcell.com");
        loginRequest.setRefId("123");
        loginRequest.setInstId("4019");

        return loginRequest;

    }

    private Header getHeader() {
        Header header = new Header();

        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("json");
        header.setSourceId("GONOGO_HDBFS");
        header.setInstitutionId("4019");
        return header;
    }

    private LogoutRequest builderRequest() {

        LogoutRequest logoutRequest = new LogoutRequest();
        logoutRequest.setHeader(getHeader());
        logoutRequest.setInstitutionId("4019");
        logoutRequest.setUserId("HDBFS_CRO1@softcell.com");

        return logoutRequest;

    }

    private ChangePasswordRequest getChangePasswordRequest() {
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setHeader(getHeader());
        changePasswordRequest.setNewPassword("password");
        changePasswordRequest.setUserName("HDBFS_CRO1@softcell.com");
        changePasswordRequest.setOldpassword("5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8");

        return changePasswordRequest;
    }


}
