package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.Product;
import com.softcell.constants.RestClientType;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import com.softcell.utils.GngDateUtil;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class AnalyticsControllerTest {


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Test
    public void getOldStack() {
    }

    @Test
    public void getStack() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.LOGIN_BY_STATUS_GRAPH)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        //TODO: Add more assertions here
    }

    @Test
    public void getLoginStatustable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.LOGIN_BY_STATUS_TABLE)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        //TODO: Add more assertions here
    }

    @Test
    public void getLoginStatustablePL() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.LOGIN_BY_STATUS_TABLE_DTC)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        ;
        //TODO: Add more assertions here
    }

    @Test
    public void getLoginByStagesReport() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/"+EndPointReferrer.LOGIN_BY_STAGE)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }

    @Test
    public void getAssetManufactureReport() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/"+EndPointReferrer.ANALYTICS_ASSET_MANUFACTURE_GRAPH)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }

    @Test
    public void getStackScore() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/"+EndPointReferrer.SCORE_TREE_GRAPH)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        ;
        //TODO: Add More Assertions.
    }

    @Test
    public void getCSVLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/"+EndPointReferrer.DSA_REPORTING_DATA)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }

    @Test
    public void getUniqueCitiesBasedOnHierarchy() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/"+EndPointReferrer.UNIQUE_CITIES)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }

    @Test
    public void getRoleBasedOnDistinctDealers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.UNIQUE_DEALERS)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }

    @Test
    public void getRoleBasedOnDistinctDSA() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/"+EndPointReferrer.UNIQUE_DSAS)
                        .content(TestUtils.convertObjectToJsonBytes(buildStackRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }

    private StackRequest buildStackRequest(){
        StackRequest stackRequest = new StackRequest();

        stackRequest.setFromDate(GngDateUtil.getyyyyMMdd(new DateTime().minus(4)));
        stackRequest.setToDate(GngDateUtil.getyyyyMMdd(new DateTime()));
        stackRequest.setLimit(100);
        stackRequest.setHeader(getHeader());
        stackRequest.setCriteria(getRequestCriteria());
        stackRequest.setStatus("Approved");
        stackRequest.setReferenceID("27611000011");
        return stackRequest;
    }


    private Header getHeader() {
        Header header = new Header();

        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("json");
        header.setSourceId("GONOGO_HDBFS");
        header.setInstitutionId("4019");
        header.setDsaId("HDBFS_DSA1@softcell.com");

        return header;
    }

    private RequestCriteria getRequestCriteria() {
        RequestCriteria requestCriteria = new RequestCriteria();

        requestCriteria.setBranches(Collections.singletonList("Madurai"));
        requestCriteria.setProducts(Collections.singletonList(Product.CDL));
        requestCriteria.setCities(Collections.singletonList("Chennai"));
        requestCriteria.setDsaIds(Collections.singletonList("dsaId"));
        requestCriteria.setStages(Collections.singletonList("APRV"));
        requestCriteria.setDealerIds(Collections.singletonList("dealerId"));
        requestCriteria.setStatuses(Collections.singletonList("Approved"));

        return requestCriteria;
    }
}