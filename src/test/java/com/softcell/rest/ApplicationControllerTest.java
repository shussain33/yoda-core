package com.softcell.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.WebConfigTest;
import com.softcell.constants.RestClientType;
import com.softcell.dao.mongodb.repository.master.MasterDataViewMongoRepository;
import com.softcell.dao.mongodb.repository.master.MasterDataViewRepository;
import com.softcell.gonogo.model.core.FileHandoverDetails;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.dashboard.DashboardRequest;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class ApplicationControllerTest {

    private MockMvc mockMvc;

    private ApplicationRequest applicationRequest;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void setApp() throws Exception {
        File file = new File("src/test/resources/RequestJson/ApplicationRequest.json");
        applicationRequest = new ObjectMapper().readValue(file, ApplicationRequest.class);

        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.SUBMIT_APPLICATION)
                        .content(TestUtils.convertObjectToJsonBytes(applicationRequest))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.STATUS)
                        .content(TestUtils.convertObjectToJsonBytes(buildCheckApplicationStatusRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void setpostIpaPdf() throws Exception {

    }

    @Test
    public void getpostIpaPdf() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.GET_POST_IPA)
                        .content(TestUtils.convertObjectToJsonBytes(buildPostIPARequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getDashBoardData() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.DASHBOARD_DETAIL)
                        .content(TestUtils.convertObjectToJsonBytes(buildDashboardRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void setPostIpaStage() throws Exception {

    }

    @Test
    public void getPartialSaveRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.GET_PARTIAL_SAVE_REQUEST)
                        .content(TestUtils.convertObjectToJsonBytes(buildDashboardRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getFileHandOverDetails() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.GET_FILE_HANDOVER_DETAILS)
                        .content(TestUtils.convertObjectToJsonBytes(buildFileHandoverDetailsRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void setFileHadoverDetails() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.SET_FILE_HANDOVER_DETAILS)
                        .content(TestUtils.convertObjectToJsonBytes(buildFileHandoverDetailsRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getNegativeAreaFundingDetails() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/get-negative-area-funding-details")
                        .content(TestUtils.convertObjectToJsonBytes(buildDummyObject()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());

    }

    private NegativeAreaFundingRequest buildDummyObject() throws JsonProcessingException {

        Header header = new Header();
        header.setInstitutionId("4019");
        header.setDateTime(new Date());
        header.setApplicationId("27611000011");
        header.setApplicationSource("Android");
        header.setRequestType("asdf");
        header.setCroId("HDBFS_CRO1@softcell.com");
        header.setDealerId("12345");
        header.setDsaId("HDBFS_DSA1@softcell.com");
        header.setSourceId("Source");

        NegativeAreaFundingRequest negativeAreaFundingRequest = new NegativeAreaFundingRequest();
        negativeAreaFundingRequest.setHeader(header);
        negativeAreaFundingRequest.setPinCode("636003");

        return negativeAreaFundingRequest;
    }

    private CheckApplicationStatus buildCheckApplicationStatusRequest() {

        CheckApplicationStatus checkApplicationStatus = new CheckApplicationStatus();
        checkApplicationStatus.setApplicantId("");
        checkApplicationStatus.setGonogoRefId("27611000011");
        checkApplicationStatus.setHeader(getHeader());

        return checkApplicationStatus;
    }

    private FileHeader getHeader() {
        FileHeader header = new FileHeader();

        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("TestUtils.convertObjectToJsonBytes(applicationRequest)");
        header.setSourceId("GONOGO_HDBFS");
        header.setInstitutionId("4019");
        header.setDealerId("27611");
        header.setDsaId("DSA1");

        return header;
    }

    private PostIpaRequest buildPostIPARequest() {

        PostIpaRequest postIpaRequest = new PostIpaRequest();

        postIpaRequest.setHeader(getHeader());
        postIpaRequest.setRefID("27611000011");
        postIpaRequest.setDate(new Date());
        postIpaRequest.setPostIPA(getPostIPA());

        return postIpaRequest;
    }

    private PostIPA getPostIPA() {
        PostIPA postIPA = new PostIPA();
        return postIPA;
    }

    private DashboardRequest buildDashboardRequest() {
        DashboardRequest dashboardRequest = new DashboardRequest();

        dashboardRequest.setDsaId("HDBFS_DSA1@softcell.com");

        dashboardRequest.setHeader(getHeader());
        dashboardRequest.setFromDate(new DateTime().minus(5).toDate());
        dashboardRequest.setLimit(1000);
        dashboardRequest.setSkip(0);
        dashboardRequest.setToDate(new Date());
        dashboardRequest.setDsaId("DSA1");

        return dashboardRequest;
    }

    private FileHandoverDetailsRequest buildFileHandoverDetailsRequest() {
        FileHandoverDetailsRequest request = new FileHandoverDetailsRequest();
        request.setHeader(getHeader());
        request.setRefId("27611000011");
        return request;
    }

    private FileHandoverDetails buildFileHandoverDetails() {
        FileHandoverDetails fileHandoverDetails = new FileHandoverDetails();
        fileHandoverDetails.setHeader(getHeader());
        fileHandoverDetails.setRefId("27611000011");
        fileHandoverDetails.setHandoverDate(new Date());
        fileHandoverDetails.setDisbursementDate(new Date());
        fileHandoverDetails.setFileStatus("Disbursed");
        return fileHandoverDetails;
    }
}