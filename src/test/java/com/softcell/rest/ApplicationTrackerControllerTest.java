package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.RestClientType;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import com.softcell.utils.GngDateUtil;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class ApplicationTrackerControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getApplicationTrackingLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.APPLICATION)
                        .content(TestUtils.convertObjectToJsonBytes(buildCheckApplicationStatusRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void generateReport() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.DOWNLOAD_CASEHISTORY)
                        .content(TestUtils.convertObjectToJsonBytes(buildReportingModuleConfiguration()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }


    private CheckApplicationStatus buildCheckApplicationStatusRequest() {
        CheckApplicationStatus checkApplicationStatus = new CheckApplicationStatus();
        checkApplicationStatus.setHeader(getHeader());
        checkApplicationStatus.setGonogoRefId("27611000011");
        return checkApplicationStatus;
    }

    private Header getHeader() {
        Header header = new Header();

        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("json");
        header.setSourceId("GONOGO_HDBFS");
        header.setInstitutionId("4019");
        header.setDealerId("27611");
        header.setDsaId("DSA1");

        return header;
    }

    private ReportingModuleConfiguration buildReportingModuleConfiguration() {

        ReportingModuleConfiguration repModConf = new ReportingModuleConfiguration();
        repModConf.setActive(true);
        repModConf.setProductType("Consumer Durable");
        repModConf.setFormat("csv");
        repModConf.setInstitutionID("4019");
        repModConf.setInsertDate(new Date());

        repModConf.setStartDate(GngDateUtil.getyyyyMMdd(new DateTime().minus(1)));

        repModConf.setEndDate(GngDateUtil.getyyyyMMdd(new DateTime()));
        return repModConf;
    }
}