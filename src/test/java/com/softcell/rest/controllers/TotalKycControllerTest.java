package com.softcell.rest.controllers;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.Product;
import com.softcell.constants.RestClientType;
import com.softcell.gonogo.model.kyc.request.TotalKycLpgDetailRequest;
import com.softcell.gonogo.model.kyc.request.TotalKycVoterDetailRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

/**
 * Created by Softcell on 18/07/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class TotalKycControllerTest
{
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private String ENDPOINT_BASE =  "/" + EndPointReferrer.TOTAL_KYC + "/";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void voterDetail()throws Exception
    {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.TOTAL_KYC_VOTER)
                .content(TestUtils.convertObjectToJsonBytes(buildTotalKycVoterDetailRequest()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void lpgDetail()throws Exception
    {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.TOTAL_KYC_LPG)
                .content(TestUtils.convertObjectToJsonBytes(buildTotalKycLpgDetailRequest()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }



    private Header getHeader() {
        Header header = new Header();
        header.setInstitutionId("4059");
        header.setDateTime(new Date());
        header.setApplicationId("");
        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("application/json");
        header.setCroId("defau");
        header.setDealerId("25213");
        header.setDsaId("HDBFS_DSA1@softcell.com");
        header.setSourceId("Source");
        header.setProduct(Product.CDL);
        return header;
    }

    private TotalKycVoterDetailRequest buildTotalKycVoterDetailRequest()
    {
        TotalKycVoterDetailRequest totalKycVoterDetailRequest = new TotalKycVoterDetailRequest();
        totalKycVoterDetailRequest.setHeader(getHeader());
        totalKycVoterDetailRequest.setEpicNo("ZZT4773586");
        return totalKycVoterDetailRequest;
    }
    private TotalKycLpgDetailRequest buildTotalKycLpgDetailRequest()
    {
        TotalKycLpgDetailRequest totalKycLpgDetailRequest = new TotalKycLpgDetailRequest();
        totalKycLpgDetailRequest.setHeader(getHeader());
        totalKycLpgDetailRequest.setMobileNumber("9028190756");
        return totalKycLpgDetailRequest;
    }



}
