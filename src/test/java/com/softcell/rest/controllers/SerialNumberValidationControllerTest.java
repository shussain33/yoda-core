package com.softcell.rest.controllers;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.Product;
import com.softcell.constants.RestClientType;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberApplicableCheckRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.UpdatedSRNumberDetailsRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

/**
 * Created by vinodk on 14/3/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class SerialNumberValidationControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private String ENDPOINT_BASE = "/";

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void serialNumberValidation() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.API_MANUFACTURER_SERIAL_NUMBER_VALIDATION)
                        .content(TestUtils.convertObjectToJsonBytes(buildTestSerialNumberValidation()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void videoconSerialNumberValidation() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.API_MANUFACTURER_SERIAL_NUMBER_VALIDATION)
                .content(TestUtils.convertObjectToJsonBytes(buildTestVideoconSerialNumberValidation()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void changePassword() throws Exception {

    }

    @Test
    public void serialSaleConfirmation() throws Exception {

    }

    @Test
    public void isSerialNumberConfirmationApplicableOld() throws Exception {

    }

    @Test
    public void isSerialNumberConfirmationApplicable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.API_MANUFACTURER_IS_SERIAL_NO_APPLICABLE)
                        .content(TestUtils.convertObjectToJsonBytes(buildSerialNumberApplicationCheckRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void blockSerialResult() throws Exception {

    }

    @Test
    public void updatedSRNumberDetailsResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.GET_UPDATED_SERIAL_NO_DETAILS)
                        .content(TestUtils.convertObjectToJsonBytes(buildUpdatedSRNumberDetailsRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }



    @Test
    public void panasonicSerialNumberValidation() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.API_MANUFACTURER_SERIAL_NUMBER_VALIDATION)
                .content(TestUtils.convertObjectToJsonBytes(buildTestPanasonicSerialNumberValidation()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private SerialSaleConfirmationRequest buildTestVideoconSerialNumberValidation() {

        SerialSaleConfirmationRequest serialSaleConfirmationRequest = new SerialSaleConfirmationRequest()
                .builder()
                .header(getHeader())
                .saleDate(new Date().toString())
                .vendor("VIDEOCON")
                .referenceID("25397000449")
                .serialNumber("980414261391700784")
                .model("REF KCP325TCRT-FDB")
                .build();
        return serialSaleConfirmationRequest;
    }

    private SerialSaleConfirmationRequest buildTestSerialNumberValidation() {


        SerialSaleConfirmationRequest serialSaleConfirmationRequest = new SerialSaleConfirmationRequest()
                .builder()
                .header(getHeader())
                .saleDate(new Date().toString())
                .vendor("SAMSUNG").skuCode("UA32FH4003RLXL")
                .referenceID("25397000449")
                .serialNumber("0a8w3paha15914")
                .build();

        return serialSaleConfirmationRequest;
    }

    public SerialSaleConfirmationRequest buildTestPanasonicSerialNumberValidation() {

        SerialSaleConfirmationRequest serialSaleConfirmationRequest = new SerialSaleConfirmationRequest()
                .builder()
                .header(getHeader())
                .saleDate(new Date().toString())
                .vendor("PANASONIC")
                .referenceID("25397000449")
                .serialNumber("5VN101152")
                .model("NR-BW465VNX4")
                .build();


        return serialSaleConfirmationRequest;
    }

    private Header getHeader() {
        Header header = new Header();
        header.setApplicationId("160530DTJKRL");
        header.setInstitutionId("4019");
        header.setSourceId("Source");
        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("asdf");
        header.setDsaId("HDBFS_DSA1@softcell.com");
        header.setDealerId("d123");
        header.setProduct(Product.CDL);
        header.setCroId("CRO1");
        header.setDateTime(new Date());
        return header;
    }

    private SerialNumberApplicableCheckRequest buildSerialNumberApplicationCheckRequest() {
        SerialNumberApplicableCheckRequest serialNumberApplicableCheckRequest = new SerialNumberApplicableCheckRequest();
        serialNumberApplicableCheckRequest.setHeader(getHeader());
        serialNumberApplicableCheckRequest.setRefId("25397000449");
        return serialNumberApplicableCheckRequest;
    }

    private UpdatedSRNumberDetailsRequest buildUpdatedSRNumberDetailsRequest() {
        UpdatedSRNumberDetailsRequest updatedSRNumberDetailsRequest = new UpdatedSRNumberDetailsRequest();
        updatedSRNumberDetailsRequest.setHeader(getHeader());
        updatedSRNumberDetailsRequest.setReferenceID("25397000449");
        return updatedSRNumberDetailsRequest;
    }
}