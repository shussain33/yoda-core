package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.request.ApplicationMetadataRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class MasterControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getManufacturers() throws Exception {

    }

    @Test
    public void getModelsWithOtherDetails() throws Exception {
        ApplicationMetadataRequest metadataRequest = new ApplicationMetadataRequest();
        Header header = new Header();
        header.setInstitutionId("4019");
        header.setProduct(Product.CDL);
        metadataRequest.setManufacturer("LG");
        metadataRequest.setModelNo("VALL");
        metadataRequest.setHeader(header);

        mockMvc.perform(MockMvcRequestBuilders.post("/" + EndPointReferrer.MASTER_BASE_ENDPOINT + EndPointReferrer.ASSET_MODEL_MASTER_GET_MODEL_NUMBER_WITH_OTHER_DETAILS)
                .content(TestUtils.convertObjectToJsonBytes(metadataRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getCategories() throws Exception {

    }

    @Test
    public void getMakes() throws Exception {

    }

    @Test
    public void getModelNos() throws Exception {

    }

    @Test
    public void getAssetModelMaster() throws Exception {

    }

    @Test
    public void getAssetModelMasterByTextSearch() throws Exception {

    }

    @Test
    public void getDealerRanking() throws Exception {

    }

}