package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.Product;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class WorkFlowControllerTest {


    private static final String END_PONT = "/" + EndPointReferrer.WORKFLOW_BASE_ENPOINT + "/";

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private static WFJobCommDomain build() {

        return WFJobCommDomain.builder().
                institutionId("4019").
                isEnabled(true).
                baseUrl("http://202.154.165.85:90/SoftcellEKYC/").
                endpoint("kyc/kycV2AadharRequest.action").
                aggregratorId("553").
                memberId("hdbs_cpu_gonogo@hdbs.com").
                noOfRetry(4).
                password("UCojQlpaNmw=").
                productName(Product.CDL.name()).
                insertDt(new Date()).
                updatedDt(new Date()).
                type(WfJobTypeConst.AADHAR_EXECUTOR.getValue()).
                serviceId("test").
                build();

    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    }

    @Test
    public void reprocessApplicationByRefId() throws Exception {

    }

    @Test
    public void reprocessUpdatedApplication() throws Exception {

    }

    @Test
    public void reprocessUpdatedApplication1() throws Exception {

    }

    @Test
    public void reappraiseApplication() throws Exception {

    }

    @Test
    public void getReappraisedStatus() throws Exception {

    }

    @Test
    public void getCommunicationDomain() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get(END_PONT + EndPointReferrer.WF_COMM_CONNECTION + "/4019").
                        contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        ).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8));
    }

    @Test
    public void saveWfCommConnection() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.put(END_PONT + EndPointReferrer.WF_COMM_CONNECTION).
                        contentType(TestUtils.APPLICATION_JSON_UTF8).
                        content(TestUtils.convertObjectToJsonBytes(build()))
        ).andDo(MockMvcResultHandlers.print()).
                andExpect(MockMvcResultMatchers.status().isCreated());

    }

    @Ignore
    public void removeWfCommConnection() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.delete(END_PONT + EndPointReferrer.WF_COMM_CONNECTION).
                        contentType(TestUtils.APPLICATION_JSON_UTF8).
                        content(TestUtils.convertObjectToJsonBytes(build()))
        ).andDo(MockMvcResultHandlers.print()).
                andExpect(MockMvcResultMatchers.status().isNoContent());
    }

}