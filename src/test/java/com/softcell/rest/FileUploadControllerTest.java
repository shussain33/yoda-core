package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.core.error.FileUploadStatus;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import com.softcell.service.FileUploadManager;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.FileInputStream;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class FileUploadControllerTest {

    @Mock
    private FileUploadManager fileUploadManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private String FILE_CONTROLLER_BASE_EP = "/" + EndPointReferrer.MASTERS_UPLOAD_BASE_ENDPOINT + "/";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void handleFileUploadSchememaster() throws Exception {

        FileUploadStatus fileUploadStatus = new FileUploadStatus();

        File file = new File("src/test/resources/master/scheme_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        //when(fileUploadManager.uploadSchemesMaster(file, "4019")).thenReturn(fileUploadStatus);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.SCHEME_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

        //verify(fileUploadManager.uploadSchemesMaster(any(File.class),anyString()), times(1));
        verifyZeroInteractions(fileUploadManager);
        verifyNoMoreInteractions(fileUploadManager);

    }

    @Test
    public void handleFileUploadAssetModelMaster() throws Exception {

        File file = new File("src/test/resources/master/model_spool.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.ASSET_MODEL_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));


    }

    @Test
    public void handleFileUploadPinCodeMaster() throws Exception {

        File file = new File("src/test/resources/master/city_zip_code_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.PINCODE_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadEmployerMaster() throws Exception {

        File file = new File("src/test/resources/master/employer_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.EMPLOYER_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadDealerEmailMaster() throws Exception {

        File file = new File("src/test/resources/master/dealer_email_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.DEALER_EMAIL_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadGNGDealerEmailMaster() throws Exception {

        File file = new File("src/test/resources/master/gng_dealer_email_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.GNG_CC_DEALEREMAIL_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadSchemeModelDealerCityMappingMaster() throws Exception {
        File file = new File("src/test/resources/master/scheme_model_dealer_city_mapping_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.SCHEME_MODEL_DEALER_CITY_MAPPING_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleFileUploadSchemeDateMappingMaster() throws Exception {

        File file = new File("src/test/resources/master/scheme_date_mapping_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.SCHEME_DATE_MAPPING_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadCarSurrogateMaster() throws Exception {

        File file = new File("src/test/resources/master/car_surrogate_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.CAR_SURROGATE_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadPlPincodeEmailMaster() throws Exception {

        File file = new File("src/test/resources/master/pl_pincode_email_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.PL_PINCODE_EMAIL_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadBankDetailsMaster() throws Exception {

        File file = new File("src/test/resources/master/bank_details_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.BANK_DETAILS_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadHierarchyMaster() throws Exception {

        File file = new File("src/test/resources/master/hierarchy_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.HIERARCHY_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleFileUploadCDLHierarchyMaster() throws Exception {

        File file = new File("src/test/resources/master/cdl_hiearchy_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.CDL_HIERARCHY_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }


    @Ignore
    public void handleFileUploadCityStateMaster() throws Exception {

        File file = new File("src/test/resources/master/.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.CITY_STATE_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleFileUploadVersion() throws Exception {
        File file = new File("src/test/resources/master/version_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.VERSION)
                .file(mockMultipartFile))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleModelVariantMaster() throws Exception {
        File file = new File("src/test/resources/master/variant_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.MODEL_VARIANT_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleAssetCategoryMaster() throws Exception {

        File file = new File("src/test/resources/master/asset_category_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.ASSET_CATEGORY_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleStateMaster() throws Exception {

        File file = new File("src/test/resources/master/state_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.STATE_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleCityMaster() throws Exception {

        File file = new File("src/test/resources/master/city_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.CITY_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));

    }

    @Test
    public void handleCreditPromotionMaster() throws Exception {

        File file = new File("src/test/resources/master/credit_promotion_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.CREDIT_PROMOTION_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleDsaBranchMaster() throws Exception {

        File file = new File("src/test/resources/master/dsa_branch_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.DSA_BRANCH_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleDsaProductMaster() throws Exception {

        File file = new File("src/test/resources/master/dsa_product_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.DSA_PRODUCT_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleDealerBranchMaster() throws Exception {
        File file = new File("src/test/resources/master/dealer_branch_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.DEALER_BRANCH_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.status", is(Status.SUCCESS.name())));
    }

    @Test
    public void handleNegativeAreaFundingMaster() throws Exception {
        File file = new File("src/test/resources/master/neg_area_funding_master.csv");

        MockMultipartFile mockMultipartFile = new MockMultipartFile("file", file.getName(), MediaType.MULTIPART_FORM_DATA_VALUE, new FileInputStream(file));

        mockMvc.perform(MockMvcRequestBuilders.fileUpload(FILE_CONTROLLER_BASE_EP + EndPointReferrer.NEGATIVE_AREA_GEO_LIMIT_MASTER)
                .file(mockMultipartFile).param("institutionId", "4019"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(TestUtils.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.oBody.payLoad.errorDesc", is("Database operation success")));
    }
}
