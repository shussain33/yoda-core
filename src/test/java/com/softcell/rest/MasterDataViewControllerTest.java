package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.RestClientType;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.master.AssetModelRequest;
import com.softcell.gonogo.model.request.master.DealerEmailMasterRequest;
import com.softcell.gonogo.model.request.master.EmployerMasterRequest;
import com.softcell.gonogo.model.request.master.PinCodeMasterRequest;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostDetails;
import com.softcell.gonogo.model.request.master.assetvalidate.AssetCostValidationRequest;
import com.softcell.gonogo.model.request.master.schememaster.GetSchemeZipReportRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.GetAllApplicableSchemeRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.GetConditionalSchemeRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.SchemeMasterRequest;
import com.softcell.gonogo.model.request.master.schememaster.schemedealermapping.*;
import com.softcell.rest.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class MasterDataViewControllerTest {

    private MockMvc mockMvc;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();


    }

    @Test
    public void getAssetModelMasterDetails() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/asset-model-master")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAssetModelMasterDetails_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad.data[0].sMdlID").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad.data[0].sInstId").isNotEmpty());

    }

    @Test
    public void getAllSchemeIds() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-all-schemeIds")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAllSchemeIds_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.oBody.payLoad[0].sSchID").isNotEmpty())
                .andExpect(jsonPath("$.oBody.payLoad[0].sSchDes").isNotEmpty());

    }

    @Test
    public void validateAssetCost_SuccessCase() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/validate-asset-cost")
                        .content(TestUtils.convertObjectToJsonBytes(buildValidateAssetCost_SuccessCase()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).andExpect(jsonPath("$.oBody.payLoad.sStatus", is(Status.SUCCESS.name())));

    }

    @Test
    public void validateAssetCost_FailureCase() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/validate-asset-cost")
                        .content(TestUtils.convertObjectToJsonBytes(buildValidateAssetCost_FailureCase()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).andExpect(jsonPath("$.oBody.payLoad.sStatus", is(Status.FAILED.name())));

    }

    @Test
    public void getpinCodeMasterDetails() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/pincode-master")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetPinCodeMasterDetails_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad.data[0].sZipCode").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad.data[0].sZipCode").isNotEmpty());

    }

    @Test
    public void getEmployerMasterDetails() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/employer-master")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetEmployerMasterDetails_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad.data[0].sEmpID").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad.data[0].sEmpName").isNotEmpty());

    }

    @Test
    public void getDealerEmailMasterDetails() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/dealer-email-master")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetDealerEmailMasterDetails_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).andExpect(jsonPath("$.oBody.payLoad.data[0].supplierID").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad.data[0].dealerID").isNotEmpty());
    }


    @Test
    public void getManufacturerAgainstCcid() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-manfc-against-ccid")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetManufacturerAgainstCcid_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());

    }

    @Test
    public void getAllStates() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-all-states")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAllStates_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }


    @Test
    public void getAllCitiesAgainstState() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-all-city-against-state")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAllCitiesAgainstState_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());

    }

    @Test
    public void getAssetCtgAgainstManfc() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-assetCtg-against-manfcr")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAllAssetCtgAgainstManf_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]", is("AIR CONDITIONER")));
    }

    @Test
    public void getModelNoAgainstAsstManf() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-modelno-against-ctg-mnfc-make")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetModelNoAgainstAsstManfc_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).andExpect(jsonPath("$.oBody.payLoad[0].sModelNo").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad[0].sModelId").isNotEmpty());
    }

    @Test
    public void getDealerAgainstCity() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-all-dealer-against-city")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAllDealerAgainstCity_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0].sDealerID").isNotEmpty());
    }

    @Test
    public void getMakeAgainstMnfcCtg() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-all-make-against-mnfc-ctg")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetMakeAgainstMnfcCtg_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0]").isNotEmpty());
    }

    @Test
    public void getAllManufacturer() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-all-manufacturer")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAllManufacturer_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0].sManfcDesc").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad[0].sManfcId").isNotEmpty());
    }


    @Test
    public void getConditionalScheme() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-schemes-against-condition")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetSchemeAgainstCondition_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0].sSchID").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad[0].sSchDes").isNotEmpty());

    }

    @Test
    public void getAllApplicableScheme() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/get-all-applicable-scheme")
                        .content(TestUtils.convertObjectToJsonBytes(buildGetAllApplicableScheme_Success_Case()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
         .andExpect(jsonPath("$.oBody.payLoad[0].sSchemeID").isNotEmpty())
         .andExpect(jsonPath("$.oBody.payLoad[0].sSchDes").isNotEmpty());
    }


    @Test
    public void csvZipReport() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/master/view/download-scheme-report")
                        .content(TestUtils.convertObjectToJsonBytes(buildCsvZipReport_SuccessCase()))
                        .contentType(TestUtils.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk());
    }

    private GetAllApplicableSchemeRequest buildGetAllApplicableScheme_Success_Case() {
        GetAllApplicableSchemeRequest getAllApplicableSchemeRequest = new GetAllApplicableSchemeRequest();
        getAllApplicableSchemeRequest.setHeader(getHeader());
        getAllApplicableSchemeRequest.setTypeOfScheme(Status.APPROVED.name());
        return getAllApplicableSchemeRequest;
    }

    private GetConditionalSchemeRequest buildGetSchemeAgainstCondition_Success_Case() {
        GetConditionalSchemeRequest getConditionalSchemeRequest = new GetConditionalSchemeRequest();
        getConditionalSchemeRequest.setHeader(getHeader());
        getConditionalSchemeRequest.setTypeOfScheme(Status.ALL.name());
        return getConditionalSchemeRequest;
    }

    private GetAllManufacturerRequest buildGetAllManufacturer_Success_Case() {
        GetAllManufacturerRequest getAllManufacturerRequest = new GetAllManufacturerRequest();
        getAllManufacturerRequest.setHeader(getHeader());
        return getAllManufacturerRequest;
    }

    private GetAllAssetMakeAgainstMnfCtgRequest buildGetMakeAgainstMnfcCtg_Success_Case() {
        GetAllAssetMakeAgainstMnfCtgRequest getAllAssetMakeAgainstMnfCtgRequest = new GetAllAssetMakeAgainstMnfCtgRequest();
        getAllAssetMakeAgainstMnfCtgRequest.setHeader(getHeader());
        List<IncludedAssetCtgWithManfc> includedAssetCtgWithManfcs = new ArrayList<>();
        IncludedAssetCtgWithManfc withManfc = new IncludedAssetCtgWithManfc();
        withManfc.setIncludedACtgr("WASHING MACHINE");
        withManfc.setIncludedManfc("BOSCH");
        includedAssetCtgWithManfcs.add(withManfc);
        getAllAssetMakeAgainstMnfCtgRequest.setIncludedAssetCtgWithManfcs(includedAssetCtgWithManfcs);
        return getAllAssetMakeAgainstMnfCtgRequest;
    }

    private GetAllDealerAgainstCityRequest buildGetAllDealerAgainstCity_Success_Case() {
        GetAllDealerAgainstCityRequest getAllDealerAgainstCityRequest = new GetAllDealerAgainstCityRequest();
        getAllDealerAgainstCityRequest.setHeader(getHeader());
        List<InclCityWithStateName> inclCityWithStateNames =new ArrayList<>();
        InclCityWithStateName inclCityWithStateName =new InclCityWithStateName();
        inclCityWithStateName.setStateName("SALEM");
        inclCityWithStateName.setCityName("TAMIL NADU");
        inclCityWithStateNames.add(inclCityWithStateName);
        getAllDealerAgainstCityRequest.setInclCityWithStateNames(inclCityWithStateNames);
        return getAllDealerAgainstCityRequest;
    }

    private GetAllModelAgainstAsstCtgRequest buildGetModelNoAgainstAsstManfc_Success_Case() {
        GetAllModelAgainstAsstCtgRequest getAllModelAgainstAsstCtgRequest = new GetAllModelAgainstAsstCtgRequest();
        getAllModelAgainstAsstCtgRequest.setHeader(getHeader());
        IncludedMakeWithCtgMnfcr includedMakeWithCtgMnfcr = new IncludedMakeWithCtgMnfcr();
        List<IncludedMakeWithCtgMnfcr> includedMakeWithCtgMnfcrs = new ArrayList<>();
        includedMakeWithCtgMnfcr.setIncludeManfcr("BLUESTAR");
        includedMakeWithCtgMnfcr.setIncludedAsstCtg("SPLIT AC");
        includedMakeWithCtgMnfcr.setIncludeAsstMake("SAC");
        includedMakeWithCtgMnfcrs.add(includedMakeWithCtgMnfcr);
        getAllModelAgainstAsstCtgRequest.setIncludedMakeWithCtgMnfcrs(includedMakeWithCtgMnfcrs);

        return getAllModelAgainstAsstCtgRequest;
    }

    private GetAllAssetCtgrAgainstMnfcRequest buildGetAllAssetCtgAgainstManf_Success_Case() {
        GetAllAssetCtgrAgainstMnfcRequest getAllAssetCtgrAgainstMnfcRequest = new GetAllAssetCtgrAgainstMnfcRequest();
        getAllAssetCtgrAgainstMnfcRequest.setHeader(getHeader());
        List<String> manList =new ArrayList<>();
        manList.add("BLUESTAR");
        getAllAssetCtgrAgainstMnfcRequest .setManufacturer(manList);
        return getAllAssetCtgrAgainstMnfcRequest;

    }

    private GetAllCityAgainstStateRequest buildGetAllCitiesAgainstState_Success_Case() {
        GetAllCityAgainstStateRequest getAllCityAgainstStateRequest = new GetAllCityAgainstStateRequest();
        getAllCityAgainstStateRequest.setHeader(getHeader());
        List<String> stateList= new ArrayList<>();
        stateList.add("WEST BENGAL");
        getAllCityAgainstStateRequest.setStateDesc(stateList);
        return getAllCityAgainstStateRequest;
    }

    private GetAllStateRequest buildGetAllStates_Success_Case() {
        GetAllStateRequest getAllStateRequest = new GetAllStateRequest();
        getAllStateRequest.setHeader(getHeader());
        return getAllStateRequest;

    }

    private GetSchemeInformationRequest buildGetManufacturerAgainstCcid_Success_Case() {
        GetSchemeInformationRequest getSchemeInformationRequest = new GetSchemeInformationRequest();
        getSchemeInformationRequest.setHeader(getHeader());
        getSchemeInformationRequest.setCcid("84");
        return getSchemeInformationRequest;
    }

    private DealerEmailMasterRequest buildGetDealerEmailMasterDetails_Success_Case() {
        DealerEmailMasterRequest dealerEmailMasterRequest = new DealerEmailMasterRequest();
        dealerEmailMasterRequest.setHeader(getHeader());
        dealerEmailMasterRequest.setLimit(10);
        return dealerEmailMasterRequest;
    }

    private EmployerMasterRequest buildGetEmployerMasterDetails_Success_Case() {
        EmployerMasterRequest employerMasterRequest = new EmployerMasterRequest();
        employerMasterRequest.setHeader(getHeader());
        return employerMasterRequest;
    }

    private PinCodeMasterRequest buildGetPinCodeMasterDetails_Success_Case() {
        PinCodeMasterRequest pinCodeMasterRequest = new PinCodeMasterRequest();
        pinCodeMasterRequest.setHeader(getHeader());
        return pinCodeMasterRequest;
    }

    private GetSchemeZipReportRequest buildCsvZipReport_SuccessCase() {
        GetSchemeZipReportRequest getSchemeZipReportRequest = new GetSchemeZipReportRequest();
        getSchemeZipReportRequest.setHeader(getHeader());
        return getSchemeZipReportRequest;
    }


    private SchemeMasterRequest buildGetAllSchemeIds_Success_Case() {
        SchemeMasterRequest schemeMasterRequest = new SchemeMasterRequest();
        schemeMasterRequest.setHeader(getHeader());
        return schemeMasterRequest;

    }


    private AssetCostValidationRequest buildValidateAssetCost_SuccessCase() {
        AssetCostValidationRequest assetCostValidationRequest = new AssetCostValidationRequest();

        assetCostValidationRequest.setHeader(getHeader());

        AssetCostDetails assetCostDetails = new AssetCostDetails();

        assetCostDetails.setAssetManufacture("SONY");
        assetCostDetails.setAssetCtg("TELEVISION");
        assetCostDetails.setAssetModelMake("BRAVIA");
        assetCostDetails.setModelNo("KD-65X9000C IN5");
        assetCostDetails.setPrice(10000);//actual asset price 31700
        assetCostValidationRequest.setAssetCostDetails(assetCostDetails);

        return assetCostValidationRequest;

    }

    private AssetCostValidationRequest buildValidateAssetCost_FailureCase() {

        AssetCostValidationRequest assetCostValidationRequest = new AssetCostValidationRequest();
        assetCostValidationRequest.setHeader(getHeader());

        AssetCostDetails assetCostDetails = new AssetCostDetails();
        assetCostDetails.setAssetManufacture("BLUESTAR");
        assetCostDetails.setAssetCtg("SPLIT AC");
        assetCostDetails.setAssetModelMake("SAC");
        assetCostDetails.setModelNo("3HW12JB1");
        assetCostDetails.setPrice(317100);//actual asset price 31700
        assetCostValidationRequest.setAssetCostDetails(assetCostDetails);

        return assetCostValidationRequest;

    }

    private AssetModelRequest buildGetAssetModelMasterDetails_Success_Case() {
        AssetModelRequest assetModelRequest = new AssetModelRequest();
        assetModelRequest.setHeader(getHeader());
        assetModelRequest.setLimit(10);
        return assetModelRequest;
    }

    private Header getHeader() {
        Header header = new Header();
        header.setInstitutionId("4019");
        header.setDateTime(new Date());
        header.setApplicationId("160530DTJKRL");
        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("asdf");
        header.setCroId("CRO1");
        header.setDealerId("12345");
        header.setDsaId("HDBFS_DSA1@softcell.com");
        header.setSourceId("Source");
        return header;
    }

}