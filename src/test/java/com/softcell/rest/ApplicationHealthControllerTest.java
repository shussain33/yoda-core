package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.RestClientType;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class ApplicationHealthControllerTest {


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private final String ENDPOINT_BASE = "/";

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getDBLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_BASE + EndPointReferrer.DB_STATS)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getDBCollections() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_BASE + EndPointReferrer.DB_COLLECTIONS)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getDBServerStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_BASE + EndPointReferrer.DB_SERVERSTATUS)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getDBhostInfo() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_BASE + EndPointReferrer.DB_HOSTINFO)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getServiceStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.EXTERNAL_SERVICES_STATUS)
                        .content(TestUtils.convertObjectToJsonBytes(getHeader()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getSetServerInPassiveMode() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.SET_APPLICATION_PASSIVE_MODE)
                        .content(TestUtils.convertObjectToJsonBytes(buildLoginRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getSetServerInActiveMode() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.SET_APPLICATION_ACTIVE_MODE)
                        .content(TestUtils.convertObjectToJsonBytes(buildLoginRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andDo(MockMvcResultHandlers.print())
         .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private Header getHeader() {
        Header header = new Header();

        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("json");
        header.setSourceId("GONOGO_HDBFS");
        header.setInstitutionId("4019");

        return header;
    }

    private LoginRequest buildLoginRequest() {


        LoginRequest loginRequest = new LoginRequest();

        loginRequest.setPassword("admintext");
        loginRequest.setHeader(getHeader());
        loginRequest.setUserName("admintext");
        loginRequest.setRefId("123");
        loginRequest.setInstId("4019");

        return loginRequest;

    }
}