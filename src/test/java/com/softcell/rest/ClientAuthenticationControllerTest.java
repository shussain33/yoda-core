package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.security.GetOtpRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class ClientAuthenticationControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getOtp() throws Exception {

    }

    @Test
    public void getOtpByAadhar() throws Exception {

    }

    @Test
    public void verifyOtpByAadhar() throws Exception {

    }

    @Test
    public void getEkycByBIO() throws Exception {

    }

    @Test
    public void getEkycByIRIS() throws Exception {

    }

    @Test
    public void sendSms() throws Exception {

    }


    private GetOtpRequest buildGetOtpRequest(){
        GetOtpRequest getOtpRequest = new GetOtpRequest();
        getOtpRequest.setHeader(getHeader());
        getOtpRequest.setInstitutionId("4019");
        //getOtpRequest.setOtpDetails();
        return buildGetOtpRequest();
    }

    private Header getHeader() {
        Header header = new Header();

        header.setApplicationSource("WEB 2.01.01");
        header.setRequestType("json");
        header.setSourceId("GONOGO_HDBFS");
        header.setInstitutionId("4019");
        header.setDealerId("27611");
        header.setDsaId("DSA1");

        return header;
    }
}