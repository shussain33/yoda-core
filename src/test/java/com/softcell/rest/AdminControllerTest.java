package com.softcell.rest;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.RestClientType;
import com.softcell.gonogo.model.logger.OtpLog;
import com.softcell.gonogo.model.request.AdminLogRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.security.LoginRequest;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.rest.utils.TestUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by prateek on 6/2/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class AdminControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private final String ENDPOINT_BASE = "/";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void setCredentials() {

    }

    @Test
    public void getPanLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.PANLOG)
                        .content(TestUtils.convertObjectToJsonBytes(buildAdminLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAadharLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.AADHARLOG)
                        .content(TestUtils.convertObjectToJsonBytes(buildAdminLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getMbLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.MBLOG)
                        .content(TestUtils.convertObjectToJsonBytes(buildAdminLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getMbLogWithCoapplicant() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.MBLOG_WITH_COAPPLICANT)
                        .content(TestUtils.convertObjectToJsonBytes(buildAdminLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getScoringLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.SCORINGLOG)
                        .content(TestUtils.convertObjectToJsonBytes(buildAdminLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getMainLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.MAINLOG)
                        .content(TestUtils.convertObjectToJsonBytes(buildAdminLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getSerialNumberLogReportByRefID() throws Exception {

    }

    @Test
    public void getSerialNumberLogReport() throws Exception {

    }

    @Test
    public void getSerialNumberRequestLogReport() throws Exception {

    }

    @Test
    public void getSerialNumberRequestLogReport1() throws Exception {

    }

    @Test
    public void getMailReportLogByID() throws Exception {

    }

    @Test
    public void getMailLogReport() throws Exception {

    }

    @Test
    public void getMainAllLog() throws Exception {

    }

    @Test
    public void getApplicationResponseLog() throws Exception {

    }

    @Test
    public void getSingleApplicationResponseLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.SINGLE_BUCKET_LOG)
                        .content(TestUtils.convertObjectToJsonBytes(buildAdminLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.GET_LOG_REPORT)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getCSVLog() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.GET_JSON_TO_CSV_REPORT)
                        .content(TestUtils.convertObjectToJsonBytes(buildCSVLogRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void get24HoursLogReport() throws Exception {

    }

    @Test
    public void refresh() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.CACHE_REFRESH)
                        .content(TestUtils.convertObjectToJsonBytes(buildLoginRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateStatusAndStage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(ENDPOINT_BASE + EndPointReferrer.UPDATE_STATUS_AND_STAGE)
                        .content(TestUtils.convertObjectToJsonBytes(buildLoginRequest()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    private AdminLogRequest buildAdminLogRequest() {
        AdminLogRequest adminLogRequest = new AdminLogRequest();
        adminLogRequest.setRefID("27611000011");
        adminLogRequest.setUserID("userID");
        adminLogRequest.setPassword("password");
        adminLogRequest.setVendor("Vendor");
        return adminLogRequest;
    }

    private List<OtpLog> buildCSVLogRequest() {

        Date endTime = new Date();
        DateTime dateTime = new DateTime().minusDays(5);

        OtpLog otpLog = new OtpLog().builder()
                .dsaID("HDBFS_DSA1@softcell.com")
                .emailID("HDBFS_DSA1@softcell.com")
                .endTime(endTime)
                .endTimeToString(endTime.toString())
                .institutionID("4019")
                .mobileNumber("9898989898")
                .startTime(dateTime.toDate())
                .startTimeToString(dateTime.toDate().toString())
                .refID("27611000011")
                .build();

        return Collections.singletonList(otpLog);
    }

    private LoginRequest buildLoginRequest() {

        LoginRequest loginRequest = new LoginRequest();

        loginRequest.setPassword("1234554321");
        loginRequest.setHeader(getHeader());
        loginRequest.setUserName("yogesh");
        loginRequest.setInstId("4019");

        return loginRequest;
    }

    private Header getHeader() {
        Header header = new Header();

        header.setApplicationSource(RestClientType.WEB.getValue());
        header.setRequestType("json");
        header.setSourceId("GONOGO_HDBFS");
        header.setInstitutionId("4019");
        return header;
    }
}