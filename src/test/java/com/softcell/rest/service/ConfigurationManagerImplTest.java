package com.softcell.rest.service;

import com.softcell.config.MultiProductConfiguration;
import com.softcell.dao.mongodb.repository.multiproduct.MultiProductRepository;
import com.softcell.dao.mongodb.repository.security.SecurityMongoRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.service.impl.ConfigurationManagerImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by prateek on 26/1/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfigurationManagerImplTest {

    ArgumentCaptor<MultiProductConfiguration>  argumentCaptor = ArgumentCaptor.forClass(MultiProductConfiguration.class);

    @Mock
    MultiProductRepository multiProductRepository;

    @Mock
    SecurityMongoRepository securityMongoRepository;


    @Mock
    ConfigurationRepository configurationRepository;

    @InjectMocks
    ConfigurationManagerImpl configurationManager;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testDeleteMultiProductConfiguration() throws Exception {
    	
    }

    @Test
    public void testUpdateMultiProductConfiguration() throws Exception {

    }

    @Test
    public void testFindAllMultiProductConfiguration() throws Exception {

    }

    @Test
    public void testSetVersion() throws Exception {

    }

    @Test
    public void testRemoveVersion() throws Exception {

    }

    @Test
    public void testGetVersionConguration() throws Exception {

    }

    @Test
    public void testIsValidVersion() throws Exception {

    }

    @Test
    public void testAddInstitutionProductConfig() throws Exception {

    }

    @Test
    public void testRemoveInstitutionProductConfig() throws Exception {

    }

    @Test
    public void testUpdateInstitutionProductConfig() throws Exception {

    }

    @Test
    public void testGetInstitutionProductConfig() throws Exception {

    }

    @Test
    public void testAddTemplateConfiguration() throws Exception {

    	
    }

    @Test
    public void testGetTemplateConfiguration() throws Exception {

    }

    @Test
    public void testGetTemplateConfiguration1() throws Exception {

    }

    @Test
    public void testUpdateTemplatePath() throws Exception {

    }

    @Test
    public void testUpdateTemplateLogo() throws Exception {

    }

    @Test
    public void testEnableTemplate() throws Exception {

    }

    @Test
    public void testDisableTemplate() throws Exception {

    }

    @Test
    public void testAddEmailConfiguration() throws Exception {

    }

    @Test
    public void testUpdateEmailConfiguration() throws Exception {

    }

    @Test
    public void testDisableEmailConfiguration() throws Exception {

    }

    @Test
    public void testEnableEmailConfiguration() throws Exception {

    }

    @Test
    public void testGetAllEmailConfiguration() throws Exception {

    }
}