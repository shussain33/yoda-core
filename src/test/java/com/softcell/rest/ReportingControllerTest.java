package com.softcell.rest;

import org.junit.Test;

/**
 * Created by prateek on 6/2/17.
 */
public class ReportingControllerTest {
    @Test
    public void customReport() throws Exception {

    }

    @Test
    public void getAssetManufactureReport() throws Exception {

    }

    @Test
    public void generateReport() throws Exception {

    }

    @Test
    public void generateTVRReport() throws Exception {

    }

    @Test
    public void saveCustomReport() throws Exception {

    }

    @Test
    public void previewCustomReport() throws Exception {

    }

    @Test
    public void csvZipReport() throws Exception {

    }

    @Test
    public void getGoNoGoSystemFields() throws Exception {

    }

    @Test
    public void searchByReportName() throws Exception {

    }

    @Test
    public void deliverReportConfig() throws Exception {

    }

    @Test
    public void saveDefaultReport() throws Exception {

    }

    @Test
    public void getChannelWiseReport() throws Exception {

    }

    @Test
    public void getDeviceReport() throws Exception {

    }

    @Test
    public void getSalesIncentiveReport() throws Exception {

    }

    @Test
    public void getZippedSalesReport() throws Exception {

    }

    @Test
    public void getSalesReport() throws Exception {

    }

    @Test
    public void getCreditReport() throws Exception {

    }

    @Test
    public void getFirstLastLoginReport() throws Exception {

    }

    @Test
    public void getZippedFirstLastLoginReport() throws Exception {

    }

    @Test
    public void getOtpReport() throws Exception {

    }

    @Test
    public void getZippedOtpReport() throws Exception {

    }

    @Test
    public void getSerialNumberLogs() throws Exception {

    }

    @Test
    public void getSerialNumberLogReportByRefID() throws Exception {

    }

    @Test
    public void getAssetAnalyticalReport() throws Exception {

    }

}