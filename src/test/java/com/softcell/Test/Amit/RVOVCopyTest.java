package com.softcell.Test.Amit;

import com.softcell.constants.LosTvsAddressTypeEnum;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.core.verification.PropertyVerificationInput;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.rest.utils.EndPointReferrer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by archana on 4/3/18.
 */
public class RVOVCopyTest {

    private List<CustomerAddress> addressList;
    private List<PropertyVerification> residenceVerification;
    private List<PropertyVerification> officeVerification;
    private Applicant applicant;

    //private DataEntryManagerImpl mgr = new DataEntryManagerImpl();
    private UnderTest mgr = new UnderTest();


    @Before
    public void init(){
        applicant = new Applicant();
        applicant.setApplicantId("0");
        applicant.setApplicantName(new Name());
        applicant.getApplicantName().setFirstName("firstName");

        // Fill in residence list
        residenceVerification = new ArrayList<>();
        PropertyVerification residence = new PropertyVerification();
        residence.setApplicantId("0");
        residence.setApplicantName(applicant.getApplicantName());
        residence.setStatus(ThirdPartyVerification.Status.PENDING.name());
        residence.setInput(new PropertyVerificationInput());
        residence.getInput().setAddress(new CustomerAddress());
        residenceVerification.add(residence);

        // Fill in office list
        officeVerification = new ArrayList<>();
        PropertyVerification office = new PropertyVerification();
        office.setApplicantId("0");
        office.setApplicantName(applicant.getApplicantName());
        office.setStatus(ThirdPartyVerification.Status.PENDING.name());
        office.setInput(new PropertyVerificationInput());
        office.getInput().setAddress(new CustomerAddress());
        officeVerification.add(office);

        // fill in address list
        CustomerAddress address;
        addressList = new ArrayList<>();
        for( int i = 1; i <= 2; i++){
            address = new CustomerAddress();
            addressList.add(address);

            if( i % 2 == 0) address.setAddressType(LosTvsAddressTypeEnum.OFFICE.name());
            else address.setAddressType(LosTvsAddressTypeEnum.RESIDENCE.name());

            address.setLatitude(i * 200);
        }


    }

    @Test
    public void testVerifiedResiAddresses(){
        residenceVerification.get(0).setStatus(ThirdPartyVerification.Status.VERIFIED.name());
        // Method under test
        mgr.copyAddresses(applicant, addressList, "", residenceVerification, officeVerification);

        Assert.assertTrue(residenceVerification.get(0).getInput().getAddress() != addressList.get(0));
        Assert.assertTrue(officeVerification.get(0).getInput().getAddress() == addressList.get(1));

    }

    @Test
    public void testVerifiedResiOfficeAddresses(){
        residenceVerification.get(0).setStatus(ThirdPartyVerification.Status.VERIFIED.name());
        officeVerification.get(0).setStatus(ThirdPartyVerification.Status.VERIFIED.name());
        // Method under test
        mgr.copyAddresses(applicant, addressList, "", residenceVerification, officeVerification);

        Assert.assertTrue(residenceVerification.get(0).getInput().getAddress() != addressList.get(0));
        Assert.assertTrue(officeVerification.get(0).getInput().getAddress() != addressList.get(1));

    }

    @Test
    public void testVerifiedOfficeAddresses(){
        officeVerification.get(0).setStatus(ThirdPartyVerification.Status.VERIFIED.name());
        // Method under test
        mgr.copyAddresses(applicant, addressList, "", residenceVerification, officeVerification);

        Assert.assertTrue(residenceVerification.get(0).getInput().getAddress() == addressList.get(0));
        Assert.assertTrue(officeVerification.get(0).getInput().getAddress() != addressList.get(1));

    }


    @Test
    public void testNoResiAddresses(){
        residenceVerification.get(0).getInput().setAddress(null);
        // Method under test
        mgr.copyAddresses(applicant, addressList, "", residenceVerification, officeVerification);

        Assert.assertTrue(residenceVerification.get(0).getInput().getAddress() == addressList.get(0));
        Assert.assertTrue(officeVerification.get(0).getInput().getAddress() == addressList.get(1));

    }

    @Test
    public void testNoOfficeAddresses(){
        officeVerification.get(0).getInput().setAddress(null);
        // Method under test
        mgr.copyAddresses(applicant, addressList, "", residenceVerification, officeVerification);

        Assert.assertTrue(residenceVerification.get(0).getInput().getAddress() == addressList.get(0));
        Assert.assertTrue(officeVerification.get(0).getInput().getAddress() == addressList.get(1));

    }

    @Test
    public void testResiOfficeAddressesPresent(){

        // Method under test
        mgr.copyAddresses(applicant, addressList, "", residenceVerification, officeVerification);

        Assert.assertTrue(residenceVerification.get(0).getInput().getAddress() == addressList.get(0));
        Assert.assertTrue(officeVerification.get(0).getInput().getAddress() == addressList.get(1));
    }

}

class UnderTest
{
    public void copyAddresses(Applicant applicant, List<CustomerAddress> addressList, String productName,
                              List<PropertyVerification> residenceVerification,
                              List<PropertyVerification> officeVerification) {
        /* Copy the address which are newly filled in; But do not copy for VERIFIED addresses
                     Fetch all the address office and RV from db
                     Exclude VERIFIED addresses and add/overwrite other addresses, based on applicant name
            */
        boolean emptyOfficeList = CollectionUtils.isEmpty(officeVerification);
        boolean emptyResiList = CollectionUtils.isEmpty(residenceVerification);
        List<PropertyVerification> thisApplcntNonVerifiedResi = new ArrayList<>();
        List<PropertyVerification> thisApplcntNonVerifiedOffices = new ArrayList<>();
        if (!emptyResiList) {
            thisApplcntNonVerifiedResi = residenceVerification.stream()
                    .filter(property -> StringUtils.equalsIgnoreCase(property.getApplicantName().getFirstName(),
                            applicant.getApplicantName().getFirstName())
                            && !StringUtils.equalsIgnoreCase(property.getStatus(),
                            ThirdPartyVerification.Status.VERIFIED.name())
                    ).collect(Collectors.toList());
        }

        if (!emptyOfficeList) {
            thisApplcntNonVerifiedOffices = officeVerification.stream()
                    .filter(property -> StringUtils.equalsIgnoreCase(property.getApplicantName().getFirstName(),
                            applicant.getApplicantName().getFirstName())
                            && !StringUtils.equalsIgnoreCase(property.getStatus(),
                            ThirdPartyVerification.Status.VERIFIED.name())
                    ).collect(Collectors.toList());
        }

        for (CustomerAddress customerAddress : addressList) {
            if (customerAddress.getAddressType().equals(EndPointReferrer.ADDRESS_TYPE_RESIDENCE)) {
                if (emptyResiList) {
                    residenceVerification.add(copyAddresses(applicant, customerAddress, productName));
                } else {
                    if (CollectionUtils.isEmpty(thisApplcntNonVerifiedResi)) {
                        // all are verified do nothing
                    } else {
                        //replace
                        thisApplcntNonVerifiedResi.get(0).getInput().setAddress(customerAddress);
                    }
                }
            } else if (customerAddress.getAddressType().equals(EndPointReferrer.ADDRESS_TYPE_OFFICE)) {
                if (emptyOfficeList) {
                    officeVerification.add(copyAddresses(applicant, customerAddress, productName));
                } else {
                    if (CollectionUtils.isEmpty(thisApplcntNonVerifiedOffices)) {
                        // all are verified do nothing
                    } else {
                        //replace
                        thisApplcntNonVerifiedOffices.get(0).getInput().setAddress(customerAddress);
                    }
                }
            }
        }
    }
    private PropertyVerification copyAddresses(Applicant applicant, CustomerAddress address, String productName) {
        PropertyVerification property = new PropertyVerification();
        PropertyVerificationInput input = new PropertyVerificationInput();

        property.setApplicantId(applicant.getApplicantId());
        property.setApplicantName(applicant.getApplicantName());

        input.setAddress(address);
        input.setPhone(applicant.getPhone());
        input.setProduct(productName);
        property.setInput(input);
        property.setStatus(ThirdPartyVerification.Status.PENDING.name());
        return property;
    }

}
