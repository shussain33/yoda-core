package com.softcell.Test.Amit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.scoring.response.Result;
import com.softcell.gonogo.model.core.scoring.response.ScoringApplicantResponse;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.response.ComponentResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;
import org.junit.Test;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by amit on 7/2/18.
 */
public class GeneralTest {

    @Test
    public void testBeanComparison() {
        Employee e1 = new Employee(1, "Amit", 1200d);
        Employee e2 = new Employee(2, "Sumit", 1200d);
        System.out.println(e1);
        System.out.println(e2);
        Javers j = JaversBuilder.javers().build();
        Diff diff = j.compare(e1, e2);
        System.out.println(diff);
    }

    @Test
    public void testDateToLong(){
        Date received = new Date();//1510815917542
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(1510815917542L);
        System.out.println(cal.getTime());

        cal.set(2018,01,01, 0,0,0);
        System.out.println("date : " + cal.getTime() + " milliseconds =" +cal.getTimeInMillis());

        cal.set(2018,01,20,0,0,0);
        System.out.println("date : " + cal.getTime() + " milliseconds =" +cal.getTimeInMillis());
    }

    @Test
    public void listcopy(){
        ArrayList<String> dbObjList = new ArrayList<String>() {{
            add("A");
            add("B");
            add("C");
        }};

        ArrayList<String> ui = new ArrayList<String>() {{
            add("a");
            add("Y");
            add("Z");
        }};
        System.out.println(" dbObjList : " + dbObjList);
        System.out.println(" ui : " + ui);
        System.out.println(" ---------  ");

        for (int i = 0; i < dbObjList.size(); i++) {
            if (dbObjList.get(i).equalsIgnoreCase("C")) {
                ui.remove(i);
                ui.add(i, dbObjList.get(i));
            }
        }
        System.out.println(" dbObjList : " + dbObjList);
        System.out.println(" ui : " + ui);
    }

class Employee{
    int id;
    String name;
    double d;

    public Employee(int i, String amit, double v) {
        id = i;
        name = amit;
        d = v;
    }
}
  @Test
  public void testScoring()
 {
    String response ="{\"SUMMARY\":{\"APPLICATION-DECISION\":\"Queue\",\"APPLICATION-APPROVED-AMOUNT\":\"0.0\"},\"APPLICANT-RESULT\":[{\"APPLICANT-ID\":\"1\",\"STATUS\":\"SUCCESS\",\"DECISION\":\"Queue\",\"RULES\":[],\"ELIGIBILITY-AMOUNT\":\"\",\"ELIGIBILITY-DECISION\":\"Queue\",\"SCORE_DATA\":null,\"ELIGIBILITY_RESPONSE\":{\"ElgbltyID\":1.0,\"DECISION\":\"Queue\",\"REMARK\":\"No eligibility criteria matched\",\"APPROVED_AMOUNT\":0.0},\"DERIVED_FIELDS\":{\"CUSTOM_FIELDS$NUM_OF_ACC\":0.0,\"CUSTOM_FIELDS$TOTAL_SEC_ENQ\":0.0,\"CUSTOM_FIELDS$SEC_ENQ_L30D\":0.0,\"CUSTOM_FIELDS$UNSEC_ENQ_L30D\":0.0,\"CUSTOM_FIELDS$SEC_ENQ_L12M\":0.0,\"CUSTOM_FIELDS$CURR_BAL_SUM\":0.0,\"CUSTOM_FIELDS$SEC_ENQ_L6M\":0.0,\"CUSTOM_FIELDS$OVERDUE_AMT_SUM\":0.0,\"CUSTOM_FIELDS$UNSEC_ENQ_L6M\":0.0,\"CUSTOM_FIELDS$HIGH_SAN_AMT_SUM\":0.0,\"CUSTOM_FIELDS$UNSEC_ENQ_L12M\":0.0},\"POLICY_ID\":1.0,\"POLICY_NAME\":\"Test\"},{\"APPLICANT-ID\":\"2\",\"STATUS\":\"SUCCESS\",\"DECISION\":\"Queue\",\"RULES\":[],\"ELIGIBILITY-AMOUNT\":\"\",\"ELIGIBILITY-DECISION\":\"Queue\",\"SCORE_DATA\":null,\"ELIGIBILITY_RESPONSE\":{\"ElgbltyID\":1.0,\"DECISION\":\"Queue\",\"REMARK\":\"No eligibility criteria matched\",\"APPROVED_AMOUNT\":0.0},\"DERIVED_FIELDS\":{\"CUSTOM_FIELDS$NUM_OF_ACC\":0.0,\"CUSTOM_FIELDS$TOTAL_SEC_ENQ\":0.0,\"CUSTOM_FIELDS$SEC_ENQ_L30D\":0.0,\"CUSTOM_FIELDS$UNSEC_ENQ_L30D\":0.0,\"CUSTOM_FIELDS$SEC_ENQ_L12M\":0.0,\"CUSTOM_FIELDS$CURR_BAL_SUM\":0.0,\"CUSTOM_FIELDS$SEC_ENQ_L6M\":0.0,\"CUSTOM_FIELDS$OVERDUE_AMT_SUM\":0.0,\"CUSTOM_FIELDS$UNSEC_ENQ_L6M\":0.0,\"CUSTOM_FIELDS$HIGH_SAN_AMT_SUM\":0.0,\"CUSTOM_FIELDS$UNSEC_ENQ_L12M\":0.0},\"POLICY_ID\":1.0,\"POLICY_NAME\":\"Test\"}]}";
    response = response.replaceAll("customerId", "sApplID");
    ObjectMapper request = new ObjectMapper();
    ScoringApplicantResponse scoringApplicantResponse = null;
    try {
     ApplicationRepository applicationRepository = new ApplicationMongoRepository();
     scoringApplicantResponse = request.readValue(response, ScoringApplicantResponse.class);
     }catch(Exception e){
        e.printStackTrace();
    }
    segregrateApplicantsResponse(scoringApplicantResponse);
 }
    private void segregrateApplicantsResponse(ScoringApplicantResponse scoringApplicantResponse) {
        List<Result> applicantResultList = new ArrayList<>();
        List<Result> coApplicantResultList = new ArrayList<>();
        ScoringApplicantResponse applicantScoringResponses = new ScoringApplicantResponse();
        applicantScoringResponses.setSummary(scoringApplicantResponse.getSummary());
        ScoringApplicantResponse coApplicantScoringResponses = new ScoringApplicantResponse();
        coApplicantScoringResponses.setSummary(scoringApplicantResponse.getSummary());
        GoNoGoCustomerApplication goNoGoCustomerApplication = null;
        try {
            ApplicationRepository applicationRepository = new ApplicationMongoRepository();
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId("4075060000000028");
        }catch(Exception e){
            e.printStackTrace();
        }

        String applicantId = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId();
        List<Result> applicantsResult = scoringApplicantResponse.getApplicantResult();
        for(Result result : applicantsResult) {
            if(applicantId.equalsIgnoreCase(result.getApplicantId())) {
                applicantResultList.add(result);
            } else {
                coApplicantResultList.add(result);
            }
        }
        applicantScoringResponses.setApplicantResult(applicantResultList);
        coApplicantScoringResponses.setApplicantResult(coApplicantResultList);

        if (goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse() == null) {
            ScoringResponse scoringResp = new ScoringResponse();
            goNoGoCustomerApplication.getApplicantComponentResponse().setScoringServiceResponse(scoringResp);
        }
        goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse().setApplicantResponse(applicantScoringResponses);
        goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse().setCoApplicantResponse(coApplicantScoringResponses);
    }



    @Test
    public void testFilteredArray(){
        List<ComponentResponse> responseList = new ArrayList<>();
        ComponentResponse comp = new ComponentResponse(); comp.setApplicantId("1"); responseList.add(comp);
        comp = new ComponentResponse(); comp.setApplicantId("2");comp.setApplicationId("app"); responseList.add(comp);
        comp = new ComponentResponse(); comp.setApplicantId("3"); responseList.add(comp);

        System.out.println(responseList);
        System.out.println("---------");
        ComponentResponse componentResponse = new ComponentResponse(); componentResponse.setApplicantId("2");
        componentResponse.setApplicationId("xyz");
        /*List<ComponentResponse> filtered = responseList.stream()
                                        .filter(compResponse -> StringUtils.endsWithIgnoreCase(compResponse.getApplicantId(),
                                                componentResponse.getApplicantId() ))
                                        .collect(Collectors.toList());
        if( CollectionUtils.isEmpty(filtered) ) {
            responseList.add(componentResponse);
        } else {
            // replace
            filtered.remove(0);
            filtered.add(componentResponse);
        }*/

        if( CollectionUtils.isEmpty(responseList) ) {
            responseList.add(componentResponse);
        } else {
            //iterate to replace
            int i = 0;
            for( ComponentResponse response : responseList){
                if(StringUtils.endsWithIgnoreCase(response.getApplicantId(), componentResponse.getApplicantId() )){
                    responseList.remove(i);
                    responseList.add(i,componentResponse);
                    break;
                }
                i++;
            }
        }
        System.out.println(responseList);
    }

    @Test
    public void tester(){
        double d = 0.0;
        DecimalFormat df = new DecimalFormat(".##");
//if(d<0)
//        df.setRoundingMode(RoundingMode.FLOOR);
df.setRoundingMode(RoundingMode.CEILING);
        double result = new Double(df.format(d));
        System.out.println(String.valueOf(result));
    }
}
