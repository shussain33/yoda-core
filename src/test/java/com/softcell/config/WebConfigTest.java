package com.softcell.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.app.config.AsyncConfiguration;
import com.softcell.app.config.apidocs.SwaggerConfiguration;
import com.softcell.app.config.logging.LoggingConfiguration;
import com.softcell.app.config.web.ProfileConsts;
import com.softcell.app.config.web.PropertiesSourcesConfiguration;
import com.softcell.app.config.web.ServiceConfig;
import com.softcell.rest.exhandler.RestHandlerExceptionResolver;
import com.softcell.rest.exhandler.supports.HttpMessageConverterUtils;
import com.softcell.rest.interceptors.TokenAuthInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.resource.GzipResourceResolver;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * Created by yogeshb on 20/2/17.
 */
@ComponentScan(basePackages = {"com.softcell.rest"})
@Configuration
@Import({
        DBConfigurationTest.class,
        ServiceConfig.class,
        PropertiesSourcesConfiguration.class,
        AsyncConfiguration.class,
        SwaggerConfiguration.class,
        LoggingConfiguration.class

})
@EnableWebMvc
public class WebConfigTest extends WebMvcConfigurerAdapter {

    private static final Logger logger = LoggerFactory.getLogger(WebConfigTest.class);

    private static final int CACHE_PERIOD = 3600; // one year

    @Inject
    private Environment env;

    public WebConfigTest() {
    }



    @Override
    @Profile(ProfileConsts.GNG_SWAGGER)
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {

        if (Arrays.asList(env.getActiveProfiles()).contains("dev")) {
            configurer.enable();
        }

    }


    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        ObjectMapper objectMapper = null;
        for (HttpMessageConverter converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                MappingJackson2HttpMessageConverter jacksonConverter =
                        ((MappingJackson2HttpMessageConverter) converter);

                if (objectMapper == null) {
                    objectMapper = jacksonConverter.getObjectMapper();
                } else {
                    jacksonConverter.setObjectMapper(objectMapper);
                }
            }
        }
    }


    @Profile(ProfileConsts.GNG_DEV)
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        logger.debug("Registering swagger ui path begun");

        registry
                .addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html")
                .setCachePeriod(CACHE_PERIOD)
                .resourceChain(true)
                .addResolver(new GzipResourceResolver())
                .addResolver(new PathResourceResolver());

        registry
                .addResourceHandler("/webjars/** ")
                .addResourceLocations("classpath:/META-INF/resources/webjars/")
                .setCachePeriod(CACHE_PERIOD)
                .resourceChain(true)
                .addResolver(new GzipResourceResolver())
                .addResolver(new PathResourceResolver());

        logger.debug("Swagger ui path registered successfully !! ");

    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        super.configureMessageConverters(converters);
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {

        logger.debug("Registering content negotiation configurer started ...");
        configurer.favorPathExtension(true)
                .favorParameter(false)
                .ignoreAcceptHeader(true)
                .useJaf(false)
                .defaultContentType(MediaType.APPLICATION_JSON_UTF8)
                .mediaType("xml", MediaType.APPLICATION_XML)
                .mediaType("json", MediaType.APPLICATION_JSON_UTF8)
                .mediaType("html", MediaType.TEXT_HTML);


        logger.debug("Registering content negotiation configurer completed");
    }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        logger.debug("Registering view resolver started ...");

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/jsp/");
        viewResolver.setSuffix(".jsp");

        logger.debug("Registering view resolver completed successfully  ...");

        return viewResolver;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        logger.debug("Registering multipart resolver started ...");

        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setMaxInMemorySize(1048576);
        commonsMultipartResolver.setMaxUploadSize(52428800);

        logger.debug("Registering multipart resolver completed successfully !!");

        return commonsMultipartResolver;
    }

    @Override
    public org.springframework.validation.Validator getValidator() {
        return new LocalValidatorFactoryBean();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        logger.debug("Registering interceptors started ...");

        registry.addInterceptor(new LocaleChangeInterceptor());
        registry.addInterceptor(interceptor()).addPathPatterns("/**");

        logger.debug("Registering interceptors completed successfully !!");

    }

    @Bean
    public TokenAuthInterceptor interceptor() {
        return new TokenAuthInterceptor();
    }


    @Bean
    public CorsFilter corsFilter(){
        logger.debug("Registration of cors started ... ");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(false);

        config.addAllowedOrigin("*");

        config.addAllowedHeader("*");

        config.addAllowedMethod("GET");

        config.addAllowedMethod("PUT");

        source.registerCorsConfiguration("/**", config);

        logger.debug("Registration of cors completed successfully !! ");

        return new CorsFilter(source);
    }

    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        exceptionResolvers.add(exceptionHandlerExceptionResolver());
        exceptionResolvers.add(restExceptionResolver());
    }

    @Bean
    public ExceptionHandlerExceptionResolver exceptionHandlerExceptionResolver() {

        logger.info("Registering of rest exception handling started ...");

        ExceptionHandlerExceptionResolver resolver = new ExceptionHandlerExceptionResolver();

        resolver.setMessageConverters(HttpMessageConverterUtils.getDefaultHttpMessageConverters());

        logger.debug("Registration of rest exception handling completed successfully !! ");

        return resolver;
    }

    @Bean
    public RestHandlerExceptionResolver restExceptionResolver() {
        return RestHandlerExceptionResolver
                .builder()
                .messageSource(httpErrorMessageSource())
                .defaultContentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .withDefaultHandlers(true)
                .addErrorMessageHandler(EmptyResultDataAccessException.class, org.springframework.http.HttpStatus.NOT_FOUND)
                .build();
    }

    @Bean
    public MessageSource httpErrorMessageSource() {
        ReloadableResourceBundleMessageSource m = new ReloadableResourceBundleMessageSource();
        m.setBasename("classpath:/org/example/messages");
        m.setDefaultEncoding("UTF-8");
        return m;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {

        logger.debug("Registration of formatter started ...");

        logger.info("Setting date time formatter ");

        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();

        registrar.setUseIsoFormat(true);

        registrar.registerFormatters(registry);

        logger.debug("Registration of formatter completed successfully ...");
    }


}
