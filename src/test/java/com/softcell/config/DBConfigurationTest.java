package com.softcell.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.softcell.rest.utils.JSR310DateConverters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.WriteResultChecking;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yogeshb on 20/2/17.
 */

@Configuration
@EnableMongoRepositories(basePackages = {"com.softcell.dao.mongodb"})
@ComponentScan(basePackages = {"com.softcell.dao.mongodb"})
public class DBConfigurationTest extends AbstractMongoConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(DBConfigurationTest.class);

    @Value("${gonogo.db.name}")
    private String dbName;

    @Value("${gonogo.db.host}")
    private String dbHost;

    @Value("${gonogo.db.port}")
    private int dbPort;

    @Value("${gonogo.db.totalConnection}")
    private int connectionPerHost;

    @Override
    protected String getDatabaseName() {
        return dbName;
    }

    @Override
    public Mongo mongo() throws Exception {

        logger.debug("Initializing mongo db with host [{}] port [{}] ", dbHost, dbPort);

        ServerAddress serverAddress = new ServerAddress(dbHost, dbPort);

        MongoClientOptions options = MongoClientOptions.builder().
                connectionsPerHost(connectionPerHost).
                socketKeepAlive(true).
                build();

        logger.debug("Initialization of mongo db client completed successfully !!");

        return new MongoClient(serverAddress, options);
    }

    @Bean(name = "MongoTemplate")
    public MongoTemplate mongoTemplate() throws Exception {

        logger.debug("Initializing mongo template with dbName [{}]", dbName);

        MongoTemplate mongoTemplate = new MongoTemplate(mongo(), dbName);

        mongoTemplate.setWriteResultChecking(WriteResultChecking.EXCEPTION);

        logger.debug("Initialization of mongo template completed successfully ");

        return mongoTemplate;
    }

    @Bean
    public GridFsTemplate gridFsTemplate() throws Exception {
        return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
    }

    @Bean
    public ValidatingMongoEventListener validatingMongoEventListener() {
        return new ValidatingMongoEventListener(validator());
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public CustomConversions customConversions() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(JSR310DateConverters.DateToZonedDateTimeConverter.INSTANCE);
        converters.add(JSR310DateConverters.ZonedDateTimeToDateConverter.INSTANCE);
        return new CustomConversions(converters);
    }

}
