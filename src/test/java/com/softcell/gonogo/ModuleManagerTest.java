package com.softcell.gonogo;

import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.workflow.component.manager.ComponentManager;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by archana on 29/1/18.
 */
public class ModuleManagerTest {
    @Test
    public void testExecuteBre() throws Exception {

        Cache cache = new Cache();
        GoNoGoCustomerApplication app = new GoNoGoCustomerApplication();
        app.setApplicationRequest(new ApplicationRequest());
        app.getApplicationRequest().setHeader(new Header());
        app.getApplicationRequest().getHeader().setInstitutionId("4075");

        ModuleManager moduleManager = new ModuleManager();
        // Method under test
        moduleManager.executeBre(app, "fresh-application", "role", new Header(), "type");
        moduleManager.executeBre(app, "fresh-application", "role", new Header(), "type");
        moduleManager.executeBre(app, "demographic-details", "role", new Header(), "type");
        moduleManager.executeBre(app, "demographic-details", "role", new Header(), "type");
        System.out.println("----------- ModuleManagerTest ended ");

    }

}