package com.softcell.gonogo.service.factory.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.scoring.response.ScoringApplicantResponse;
import com.softcell.gonogo.service.factory.ScoringJsonBuilder;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by archana on 10/3/18.
 */
public class ScoringJsonBuilderImplTest {
    private ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();
    @Test
    public void buildApplicantScoringJSon() throws Exception {

       // ScoringApplicantRequest scoringApplicantRequest = builder.buildApplicantScoringJSon(goNoGoCustomerApplication);
        String response =
                "{\"SUMMARY\":{\"APPLICATION-DECISION\":\"Queue\",\"APPLICATION-APPROVED-AMOUNT\":\"445\"},\"APPLICANT-RESULT\":[{\"APPLICANT-ID\":\"\",\"STATUS\":\"SUCCESS\",\"DECISION\":\"Queue\",\"SCORE\":\"0\",\"RULES\":[],\"ELIGIBILITY-AMOUNT\":\"\",\"ELIGIBILITY-DECISION\":\"Queue\"}]}";

        ObjectMapper objectMapper = new ObjectMapper();
        ScoringApplicantResponse scoringApplicantResponse = objectMapper.readValue(response, ScoringApplicantResponse.class);
        System.out.println(scoringApplicantResponse);
    }

}