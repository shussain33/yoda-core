package com.softcell.gonogo.service.lookup;

import com.softcell.config.WebConfigTest;
import com.softcell.dao.mongodb.lookup.LookupDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by yogeshb on 12/5/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfigTest.class)
public class LookupServiceTest {

    @Autowired
    private LookupDao lookupDao;

    @Autowired
    private LookupService lookupService;

    @Test
    public void checkActionsAccess() throws Exception {

    }

    @Test
    public void getTemplate() throws Exception {

    }

    @Test
    public void getEmailConfiguration() throws Exception {

    }

    @Test
    public void getApplicableVendors() throws Exception {

    }

    @Test
    public void getAliasNameByInstitutionIdAndProductName() throws Exception {

    }

}