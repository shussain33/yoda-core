package com.softcell.gonogo.service.lookup;

import com.softcell.config.WebConfigTest;
import com.softcell.constants.ActionName;
import com.softcell.dao.mongodb.lookup.LookupDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by yogeshb on 10/5/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfigTest.class)
public class LookupServiceHandlerTest {


    @Autowired
    private LookupDao lookupDao;
    @Autowired
    private LookupService lookupService;

    @Before
    public void setup() {

       /* Mockito.when(lookupDao.actionConfigurationReferrer("4019", "01", ActionName.DIGITIZATION))
                .thenReturn(new HashMap<String, ActionConfiguration>() {{
                    put("401901DIGITIZATION", new ActionConfiguration());
                }});
*/
    }

    @Test
    public void checkActionsAccess() throws Exception {

        boolean b = lookupService.checkActionsAccess("4019", "01", ActionName.DIGITIZATION);

        Assert.assertTrue(b);


    }

    @Test
    public void getTemplate() throws Exception {

    }

    @Configuration
    public class LookupServiceHandlerTestConfiguration {

        @Bean
        public LookupDao lookupDao() {
            return Mockito.mock(LookupDao.class);
        }

        @Bean
        public LookupService lookupService() {
            return new LookupServiceHandler();
        }

    }

}