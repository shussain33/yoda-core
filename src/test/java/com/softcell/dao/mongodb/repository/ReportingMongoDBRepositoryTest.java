package com.softcell.dao.mongodb.repository;

import com.softcell.config.WebConfigTest;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarLogRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.rest.utils.TestUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by yogeshb on 14/4/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebConfigTest.class})
@WebAppConfiguration
public class ReportingMongoDBRepositoryTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void findAadhaarLogs() throws Exception {

        AadhaarLogRequest aadhaarLogRequest = new AadhaarLogRequest();

        Header header = new Header();
        header.setInstitutionId("4019");

        aadhaarLogRequest.setHeader(header);
        aadhaarLogRequest.setDateStartFrom(new DateTime("2016-01-01").toDate());
        aadhaarLogRequest.setEndDate(new DateTime("2017-04-01").toDate());

        mockMvc.perform(MockMvcRequestBuilders.post("/report/aadhaar-log")
                .content(TestUtils.convertObjectToJsonBytes(aadhaarLogRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(MockMvcResultMatchers.status().isOk());


    }

}