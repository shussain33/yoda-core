package service.impl;

import com.softcell.service.AdminManager;
import com.softcell.service.impl.AdminManagerImpl;
import org.junit.Test;

import java.io.File;

/**
 * Created by archana on 25/10/18.
 */
public class AdminManagerTest {


    private AdminManager adminManager = new AdminManagerImpl();
    @Test
    public void testDeclineCases() throws Exception {
        final String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);
        File file = new File("src/test/resources/25-10-2018.txt");
        adminManager.bulkDeclineCases(file, "inst");
    }
}
