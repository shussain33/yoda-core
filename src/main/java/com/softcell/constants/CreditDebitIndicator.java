package com.softcell.constants;

/**
 * Created by Softcell on 29/09/17.
 */
public enum CreditDebitIndicator {

    H("H"),
    S("S");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    CreditDebitIndicator(String value) {
        this.value = value;
    }
}
