package com.softcell.constants;

public enum LosTvsStartPointService {

    tvscdlos,
    tvscdlosByDateAndStage,
    intiateTvsService,
    tvscdlosCreateGroupOne,
    tvscdlosCreateGroupTwo,
    tvscdlosCreateGroupThree,
    tvscdlosCreateGroupFour,
    tvscdlosCreateGroupFive
}
