package com.softcell.constants;

public enum LosTvsPersonalDetailsEnum {

    male("M"),
    female("F"),
    other("O");

    private  String value;

    LosTvsPersonalDetailsEnum(String value){
        this.value = value;
    }

    public String  toValue(){
        return this.value;
    }
}
