package com.softcell.constants;

import java.util.HashMap;
import java.util.Map;

public interface UIDAIERROR_METADATA {

	Map<String,String> aadharError = new HashMap<String,String>(){
		private static final long serialVersionUID = 1L;
		{
			put("100","basic attributes of demographic data did not match");
			put("200","address attributes of demographic data did not match");
			put("300","Biometric data did not match");
			put("310","Duplicate fingers used");
			put("311","Duplicate Irises used.");
			put("312","FMR and FIR cannt be used in same transactin");	
			put("313","Single FIR recrd cntains mre than one finger");	
			put("314","Number of FMR/FIR shuld nt exceed 10");
			put("315","Number of IIR shuld nt exceed 2");
			put("316","Number of FID should not exceed 1");
			put("330","Biometrics locked by Aadhaar holder");
			put("400","Invalid OTP value");
			put("401","Invalid TKN value");
			put("402","txn value did not match with “txn” value used in Request OTP API");
			put("500","Invalid encryption of Skey");
			put("501","Invalid certificate identifier in attribute of Skey");
			put("502","Invalid encryption of Pid");
			put("503","Invalid encryption of Hmac");
			put("504","Session key re-initiation required due to expiry or key out of sync");
			put("505","Synchronized Key usage not allowed for the AUA");
			put("510","Invalid Auth XML format");
			put("511","Invalid PID XML format");
			put("512","Invalid resident consent in rc attribute of Auth");
			put("520","Invalid device");
			
			put("521","Invalid FDC code under Meta tag");
			put("522","Invalid IDC code under Meta tag");
			put("523","Invalid CDC code under Meta tag");
			put("524","Invalid fpmi code under Meta tag");
			put("525","Invalid fpmc code under Meta tag");
			put("526","Invalid irmi code under Meta tag");
			put("527","Invalid irmc code under Meta tag");
			put("528","Invalid fdmi code under Meta tag");
			put("529","Invalid fdmc code under Meta tag");
			
			put("530","Invalid authenticator code");
			put("540","Invalid Auth XML version");
			put("541","Invalid PID XML version");
			put("542","AUA not authorized");
			put("543","Sub-AUA not associated with AUA");
			put("550","Invalid Uses element attributes");
			
			put("551","Invalid tid value");
			put("553","Registered devices currently not supported. This feature is being implemented in a phased manner.");
			
			put("561","Request expired");
			put("562","Timestamp value is future time");
			put("563","Duplicate request (this error occurs when exactly same authentication request was re-sent by AUA)");
			put("564","HMAC Validation failed");
			put("565","AUA license key has expired or is invalid");
			put("566","ASA license key has expired or is invalid");
			put("567","Invalid input (this error occurs when some unsupported characters were found in Indian language values, lname or lav)");
			put("568","Unsupported Language");
			put("569","Digital signature cam failed (means that authentication request XML was modified after it was signed)");
			put("570","Invalid key info in digital signature");
			put("571","PIN Requires reset (this error will be returned if resident is using the default PIN which needs to be reset before usage)");
			put("572","Invalid biometric position");
			put("573","Pi usage not allowed as per license");
			put("574","Pa usage not allowed as per license");
			put("575","Pfa usage not allowed as per license");
			put("576","FMR usage not allowed as per license");
			put("577","FIR usage not allowed as per license");
			put("578","IIR usage not allowed as per license");
			put("579","OTP usage not allowed as per license");
			put("580","PIN usage not allowed as per license");
			put("581","Fuzzy matching usage not allowed as per license");
			put("582","Local language usage not allowed as per license");
			
			put("586","FID usage not allowed as per license. This feature is being implemented in a phased manner");
			put("587","Name space not allowed");
			put("588","Registered device not allowed as per license. This feature is being implemented in a phased manner");
			put("590","Public device not allowed as per license");
			
			put("710","Missing Pi data as specified in Uses");
			put("720","Missing Pa data as specified in Uses");
			put("721","Missing Pfa data as specified in Uses");
			put("730","Missing PIN data as specified in Uses");
			put("740","Missing OTP data as specified in Uses");
			put("800","Invalid biometric data");
			put("810","Missing biometric data as specified in Uses");
			put("811","Missing biometric data in CIDR for the given Aadhaar number");
			put("812","Resident has not done Best Finger Detection");
			put("820","Missing or empty value for bt attribute in Uses element");
			put("821","Invalid value in the bt attribute of Uses element");
			
			put("822","Invalid value in the bs attribute of Bio element within Pid");
			
			put("901","No authentication data found in the request");
			put("902","Invalid dob value in the Pi element (this corresponds to a scenarios wherein dob attribute is not of the format YYYY or YYYY-MM-DD, or the age of resident is not in valid range)");
			put("910","Invalid mv value in the Pi element");
			put("911","Invalid mv value in the Pfa element");
			put("912","Invalid ms value");
			put("913","Both Pa and Pfa are present in the authentication request (Pa and Pfa are mutually exclusive)");
			put("930","Technical error that are internal to authentication server");
			put("931","Technical error that are internal to authentication server");
			put("932","Technical error that are internal to authentication server");
			put("933","Technical error that are internal to authentication server");
			put("934","Technical error that are internal to authentication server");
			put("935","Technical error that are internal to authentication server");
			put("936","Technical error that are internal to authentication server");
			put("937","Technical error that are internal to authentication server");
			put("938","Technical error that are internal to authentication server");
			put("939","Technical error that are internal to authentication server");
			put("940","Unauthorized ASA channel");
			put("941","Unspecified ASA channel");
			
			put("950","OTP store related technical error");
			put("951","Biometric lock related technical error");
			
			put("980","Unsupported option");
			
			put("995","Aadhaar suspended by competent authority");
			put("996","Aadhaar cancelled (Aadhaar is no in authenticable status)");
			
			put("997","Your Aadhaar number status is not active. Kindly contact UIDAI Helpline.");
			put("998","Invalid Aadhaar Number");
			put("999","Unknown error");
		}
	};
	
	Map<String,String> aadharRespMap = new HashMap<String, String>(){
		{
			put("y","AadharNumber is valid");
			put("n","AadharNumber is Invalid");
		}
	};
	
	Map<String,String> otpErrorMap = new HashMap<String, String>(){
		{
			put("110","Aadhaar number does not have verified mobile/email");
			put("111","Aadhaar number does not have verified mobile");
			put("112","Aadhaar number does not have both email and mobile.");
			put("510","Invalid Otp XML format");
			put("520","Invalid device");
			put("530","Invalid AUA code");
			put("540","Invalid OTP XML version");
			put("542","AUA not authorized for ASA. This error will be returned if AUA and ASA do not have linking in the portal");
			put("543","Sub-AUA not associated with AUA. This error will be returned if Sub-AUA specified in sa attribute is not added as Sub-AUA in portal");
			put("565","AUA License key has expired or is invalid");
			put("566","ASA license key has expired or is invalid");
			put("569","Digital signature cam failed");
			put("570","Invalid key info in digital signature (this means that certificate used for signing the OTP request is not valid. it is either expired, or does not belong to the AUA or is not created by a CA)");
			put("940","Unauthorized ASA channel");
			put("941","Unspecified ASA channel");
			put("950","Could not generate and/or send OTP");
			put("997","Your Aadhaar number status is not active. Kindly contact UIDAI Helpline.");
			put("998","Invalid Aadhaar Number or Non Availability of Aadhaar data");
			put("999","Unknown error");
		}
	};
	
	Map<String,String> kycErrorMap = new HashMap<String, String>(){
		{
			put("K-100","Resident authentication failed");
			put("K-200","Resident data currently not available");
			put("K-540","Invalid KYC XML");
			put("K-541","Invalid KYC API version");
			put("K-542","Invalid resident consent (rc attribute in Kyc element)");
			put("K-543","Invalid timestamp (ts attribute in Kyc element)");
			put("K-544","Invalid resident auth type (ra attribute in Kyc element does not match what is in PID block)");
			put("K-545","Resident has opted-out of this service");
			put("K-550","Invalid Uses Attribute");
			put("K-551","Invalid Txn namespace");
			put("K-552","Invalid License key");
			put("K-569","Digital signature cam failed for KYC XML (means that authentication request XML was modified after it was signed)");
			put("K-570","Invalid key info in digital signature for KYC XML (it is either expired, or does not belong to the AUA or is not created by a well-known Certification Authority)");
			put("K-600","AUA is invalid or not an authorized KUA");
			put("K-601","ASA is invalid or not an authorized KSA");
			put("K-602","KUA encryption key not available");
			put("K-603","KSA encryption key not available");
			put("K-604","KSA Signature not allowed");
			put("K-605","Neither KUA key nor KSA encryption key are available");
			put("K-955","Technical Failure");
			put("997","Your Aadhaar number status is not active. Kindly contact UIDAI Helpline.");
			put("998","Invalid Aadhaar Number");
			put("K-999","Unknown error");
			
		}
	};
	
	Map<String,String> otpRspMap = new HashMap<String, String>(){
		{
			put("y","OTP generated successfully");
			put("n","Failed to generate OTP");
		}
	};
	
	Map<String,String> bfdErrorMap = new HashMap<String, String>(){
		{
			put("300","Biometric data did not match");
			put("314","Number of fingers should not exceed 10");
			put("500","Invalid encryption of Skey");
			put("501","Invalid certificate identifier in ci attribute of Skey");
			put("502","Invalid encryption of Rbd");
			put("503","Invalid encryption of Hmac");
			put("504","Session key re-initiation required due to expiry or key out of sync");
			put("510","Invalid Bfd XML format");
			put("511","Invalid Rbd XML format");
			put("520","Invalid device");
			put("530","Invalid AUA code");
			put("540","Invalid BFD XML version");
			put("541","Invalid RBD XML version");
			put("542","AUA not authorized for ASA. This error will be returned if AUA and ASA do not have linking in the portal");
			put("543","Sub-AUA not associated with AUA. This error will be returned if Sub-AUA specified in sa attribute is not added as Sub-AUA in portal");
			put("561","Request expired (Rbd->ts value is older than N hours where N is a configured threshold in BFD server)");
			put("562","Timestamp value is future time (value specified Rbd->ts is ahead of BFD server time beyond acceptable threshold)");
			put("563","Duplicate request (this error occurs when exactly same BFD request was re-sent by AUA)");
			put("564","HMAC Validation failed");
			put("565","AUA license key has expired or is invalid");
			put("566","ASA license key has expired or is invalid");
			put("569","Digital signature cam failed");
			put("570","Invalid key info in digital signature (this means that certificate used for signing the BFD request is not valid.it is either expired, or does not belong to the AUA or is not created by a CA)");
			put("572","Invalid biometric position");
			put("583","Best finger detection not allowed as per license");
			put("800","Invalid biometric data");
			put("811","Missing biometric data in CIDR for the given Aadhaar number");
			put("930","Technical error that are internal to authentication server");
			put("931","Technical error that are internal to authentication server");
			put("932","Technical error that are internal to authentication server");
			put("933","Technical error that are internal to authentication server");
			put("934","Technical error that are internal to authentication server");
			put("935","Technical error that are internal to authentication server");
			put("936","Technical error that are internal to authentication server");
			put("937","Technical error that are internal to authentication server");
			put("938","Technical error that are internal to authentication server");
			put("939","Technical error that are internal to authentication server");
			put("940","Unauthorized ASA channel");
			put("941","Unspecified ASA channel");
			put("996","Aadhaar Cancelled");
			put("997","Your Aadhaar number status is not active. Kindly contact UIDAI Helpline.");
			put("998","Invalid Aadhaar Number or Non Availability of Aadhaar data");
			put("999","Unknown error");
		}
	};
	
	Map<String,String> bfdActionMap = new HashMap<String, String>(){
		{
			put("00","No action necessary");
			put("01","Please check if your AADHAAR number was entered correctly. If it was correct, you may have to update your enrollment");
			put("02","Reserved for future use");
			put("03","Please try to use BFD again. Your biometrics have not been captured properly");
			
			put("A401","Aadhaar cancelled being a duplicate. Resident shall use valid Aadhaar.");
			put("A402","Aadhaar cancelled due to disputed enrolment. Shall re-enrol if resident doesn't have a valid Aadhaar.");
			put("A403","Aadhaar cancelled due to technical Issues. Resident shall contact UIDAI contact centre.");
			put("A404","Aadhaar cancelled due to disputed enrolment (BE). Shall re-enrol if resident doesn't have a valid Aadhaar.");
			put("A405","Aadhaar cancelled due to errorneous enrolment (Iris). Shall re-enrol if resident doesn't have a valid Aadhaar.");
			put("A301","Aadhaar suspended due to inactivity. Resident shall follow the Reactivation process.");
			put("A302","Aadhaar suspended due to Biometrics integrity issue. Resident shall visit Permanent Enrolment Centre with document proofs for update.");
			put("A303","Aadhaar suspended due to Demographic integrity issue. Resident shall visit Permanent Enrolment Centre / UIDAI website with document proofs for update.");
			put("A304","Aadhaar suspended due to Address dispute. Resident shall visit Permanent Enrolment Centre with document proofs for update.");
			put("A305","Aadhaar suspended due to Photo dispute. Resident shall visit Permanent Enrolment Centre with document proofs for update.");
			put("A306","Aadhaar suspended due to Age dispute. Resident shall update the age.");
			put("A307","Aadhaar suspended since child has not updated biometrics after age of 5. Resident shall update the biometrics.");
			put("A308","Aadhaar suspended since resident has not updated biometrics after age of 15. Resident shall update the biometrics.");
			put("A309","Aadhaar is locked by resident. Resident shall follow the Un-locking process.");
			put("A201","Aadhaar number is incorrect. Resident shall use correct Aadhaar.");
			put("A202","Authentication temporarily not available, resident shall retry after sometime.");
			put("997","Your Aadhaar number status is not active. Kindly contact UIDAI Helpline.");
			put("99","Please review BFD result and determine suitable action");
		}
	};
		
	Map<String,String> mouError = new HashMap<String,String>(){
		private static final long serialVersionUID = 1L;
		{
			put("M-100","Resident authentication failed, see “Rar” element to see details of resident authentication error.");
			put("M-110","Operator authentication failed, see “Oar” element to see details of operator authentication error.");
			put("M-120","Authentication code that was sent under “Oad” is invalid. Operator must be re-authenticated.");
			put("M-121","Aadhaar number of the operator does not match with the earlier authentication request for which response code was provided.");
			put("M-200","Update service currently not available.");
			put("M-540","Invalid Mobile Update XML.");
			put("M-541","Invalid Mobile Update API version.");
			put("M-542","Invalid resident consent (“rc” attribute in “Mou” element).");
			put("M-543","Invalid timestamp (“ts” attribute in “Mou” element)");
			put("M-544","Invalid resident auth type (“ra” attribute in “Mou” element does not match what is in PID block within “Rad”).");
			put("M-545","Resident has opted-out of this service.");
			put("M-546","Invalid mobile cam code.");
			put("M-547","Invalid email address (“nem” value has invalid format).");
			put("M-546","test");
			put("M-551","Invalid “Txn” namespace either for “UMN:R” or for “UMN:O");
			put("M-569","Digital signature cam failed for Mobile Update XML");
			put("M-570","Invalid key info in digital signature for Mobile Update XML (it is either expired, or does not belong to the AUA)");
			put("M-600","AUA is invalid or not an authorized to call this API");
			put("M-999","Unknown error");
		}
	};
	
	
}
