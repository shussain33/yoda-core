package com.softcell.constants;

/**
    This enum used to find out transactional master details.
    Transactional masters are mainly used to update existing documents based on a certain condition.
    Ex. postIpa, gonogoCustomerApplication collection updated by losUrMaster,netDisbursedMaster.
 **/
public enum TransactionalMasterEnum {
    UPDATE_LOS_UTR_DETAILS_MASTER,
    NET_DISBURSAL_DETAILS_MASTER,
    UTR_MASTER,
    UTR_BULK_MASTER;
}
