package com.softcell.constants;

/**
 * @author kishorp
 */
public final class FieldSeparator {

    public static final char PIPE = '|';

    public static final char COMMA = ',';

    public static final String COMMA_STR = ",";

    public static final String COMMA_SPACE = ", ";

    public static final char SPACE = ' ';

    public static final String  SPACE_STR = " ";

    public static final char UNDER_SQURE = '_';

    public static final char DOLLER = '$';

    public static final String FORWARD_SLASH = "/";

    public static final String DOT = ".";

    public static final String HYPHEN = "-";

    public static final String BLANK = "";

    public static final String COLON = ":";

    public static final String AT_THE_RATE = "@";

    public static final String CROSS = "x";

    public static final String QUESTION_MARK = "?";

    public static final String UNDER_SCORE_STR = "_";

    public static final String OPENING_SQUARE_BRACKET = "[";

    public static final String CLOSING_SQUARE_BRACKET = "]";

    public static final String BACKWARD_SLASH_DOUBLE_QUOTE = "\"";

    public static final String HASH = "#";

    public static final String ASTERISK = "*";
}
