package com.softcell.constants;

/**
 * Created by priyanka on 6/9/17.
 */
public enum SurrogateType {



    BANK_SURROGATE("BANK_SURROGATE","CD_BANKING SURROGATE","Bank Surrogate"),
    CAR_SURROGATE("CAR_SURROGATE", "CD_OWNED CAR","Car Surrogate"),
    HOUSE_SURROGATE("HOUSE_SURROGATE", "CD_OWNED HOUSE","Owned House Surrogate"),
    SALARIED_SURROGATE("SALARIED_SURROGATE", "CD_SALARIED","Salaried Surrogate"),
    BUSINESS_SURROGATE("BUSINESS_SURROGATE", "CD_TRADER SURROGATE","Trader Surrogate"),
    CREDIT_SURROGATE("CREDIT_SURROGATE", "CD_CREDIT CARD","Credit Card Surrogate"),
    DEBIT_CARD_SURROGATE("DEBIT_CARD_SURROGATE", "CD_DEBIT CARD","Debit Card Surrogate"),
    OLD_MOBILE_PHONE_SURROGATE("OLD_MOBILE_PHONE_SURROGATE", "OLD_MOBILE_PHONE_SURROGATE","Old Mobile Phone Surrogate"),
    POST_PAID_BILL_SURROGATE("POST_PAID_BILL_SURROGATE", "POST_PAID_BILL_SURROGATE","Post Paid Bill Surrogate"),
    SALARIED_NET_TAKE_HOME("SALARIED_NET_TAKE_HOME", "SALARIED_NET_TAKE_HOME","Salary Home Surrogate");


    private final String gngSurrogate;
    private final String losSurrogate;
    private final String tvrSurrogate;


    SurrogateType(final String s1, final String s2,final String s3) {
        gngSurrogate = s1;
        losSurrogate = s2;
        tvrSurrogate = s3;
    }

    public String getGngSurrogate() {
        return gngSurrogate;
    }

    public String getLosSurrogate() {
        return losSurrogate;
    }

    public String getTvrSurrogate() {
        return tvrSurrogate;
    }
}
