package com.softcell.constants.master;

import com.softcell.gonogo.model.masters.*;

/**
 * Created by bhuvneshk on 27/9/17.
 */
public enum MasterDetails {

    PINCODE_MASTER("pincodeMaster", PinCodeMaster.class, PincodeStateMasterFields.class),
    BRANCH_MASTER("branchMaster",BranchMaster.class, BranchMasterFields.class),
    DEALER_EMAIL_MASTER("DealerEmailMaster",DealerEmailMaster.class, DealerEmailFields.class),
    INSURANCE_PREMIUM_MASTER("insurancePremiumMaster",InsurancePremiumMaster.class, InsurancePremiumFields.class),
    STATE_MASTER("stateMaster",StateMaster.class, StateMasterfields.class),
    CITY_STATE_MASTER("cityStateMappingMaster",CityStateMappingMaster.class, CityStateMasterFields.class),
    SCHEME_MASTER("schemeMaster",SchemeMasterData.class, SchemeMasterFields.class),
    ASSET_MASTER("assetModelMaster",AssetModelMaster.class, AssetModelMasterfields.class),
    API_ROLE_AUTHORISATION_MASTER("apiRoleAuthorisationMaster",ApiRoleAuthorisationMaster.class,ApiRoleAuthorisationFields.class);

    String masterName;
    Class masterClass;
    Class masterFiledsEnum;

    MasterDetails(String masterNm, Class masterClss, Class fieldEnum) {

        masterName = masterNm;
        masterClass = masterClss;
        masterFiledsEnum = fieldEnum;
    }

    public String getMasterName() {
        return masterName;
    }

    public Class getMasterClass() {
        return masterClass;
    }

    public Class getMasterFiledsEnum() {
        return masterFiledsEnum;
    }
}
