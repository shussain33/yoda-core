package com.softcell.constants.master;

/**
 * Created by anupamad on 15/9/17.
 */
public enum StateBranchMasterFields {

    STATE_CODE("stateCode"),
    SAP_STATE_CODE("sapStateCode"),
    BRANCH_CODE("branchCode");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    StateBranchMasterFields(String value) {
        this.value = value;
    }

}
