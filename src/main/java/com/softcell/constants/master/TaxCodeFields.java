package com.softcell.constants.master;

/**
 * Created by abhishek on 8/9/17.
 */
public enum TaxCodeFields {

    //Only fields from the CSV are mapped.
    STATE_CODE("stateCode"),
    OLD_TAX_CODE("oldTaxCode"),
    NEW_TAX_CODE("newTaxCode"),
    SAC_CODE("sacCode"),
    SGST_VALUE("sgstValue"),
    GST_VALUE("gstValue"),
    EFFECTIVE_FROM("effectiveFrom"),
    EFFECTIVE_TO("effectiveTo"),
    CUSTOMER_NAME("customerName"),
    STATE("state");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private TaxCodeFields(String value) {
        this.value = value;
    }

    public static String getValueByName(String name) {
        for (int i = 0; i < TaxCodeFields.values().length; i++) {
            if (name.equals(TaxCodeFields.values()[i]))
                return TaxCodeFields.values()[i].value;
        }
        return null;
    }
}
