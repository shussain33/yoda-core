package com.softcell.constants.master;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by anupamad on 11/10/17.
 */
public enum ManufacturerMasterFields {
    MFR_CODE("mfrCode"),
    MFR_DESC("mfrDesc"),
    MFR_NAME("mfrName"),
    ADDRESS("address"),
    GST_NO("gstNo"),
    SAP_CODE("sapCode");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    ManufacturerMasterFields(String value) {
        this.value = value;
    }

}
