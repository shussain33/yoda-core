package com.softcell.constants.master;

/**
 * Created by abhishek on 8/9/17.
 */
public enum AccountsHeadFields {

    TRAN_ACCOUNT_HEAD("tranAccountHead"),
    TRAN_ACC_DESC("tranAccDesc"),
    TVSM_ACCOUNT_CODE("tvsmAccountCode"),
    GL_TYPE("glType"),
    POSTING_LINK("postingLink"),
    POSTING_LINK_CODE("postingLinkCode"),
    SPECIAL_GL_INDICATOR("specialGlIndicator"),
    POSTING_KEY_DB("postingKeyDb"),
    POSTING_KEY_CR("postingKeyCr"),
    TAX_FLAG("taxFlag"),
    TAX_ACC_CODE("taxAccCode"),
    TAX_TYPE("taxType"),
    PANDL_FLAG("pandlFlag"),
    BUSINESS_AREA("businessArea"),
    COST_CENTER("costCenter"),
    CUSTOMER("customer"),
    PRODUCT("product"),
    BRANCH_CODE("branchCode"),
    PAYMENT_MODE("paymentMode");
    // ,CACTIVE("cactive")

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    AccountsHeadFields(String value) {
        this.value = value;
    }
}
