package com.softcell.constants.master;

/**
 * Created by bhuvneshk on 31/10/17.
 */
public enum SupplierLocationFields {

    SUPPLIER_ID("supplierId"),
    BRANCH_ID("branchId"),
    SUPPLIERLOCATION("supplierLocationCode"),
    CITY("city"),
    ADDRESS4("address"),
    INACTIVE("inactive");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    SupplierLocationFields(String value) {
        this.value = value;
    }
}
