package com.softcell.constants.master;

/**
 * Created by Softcell on 26/09/17.
 */
public enum TCLosGngMappingFields {
    SR_NO("srNo"),
    GNG_FIELD("gngField"),
    ACCOUNT_ENTRY_TYPE("accEntrytype"),
    ACCOUNT_TYPE("accType"),
    ACC_ENTRY_NUMBER("accEntryNumber"),
    ACC_FIELD_TYPE("accFieldType"),
    SAP_FIELD("sapField"),
    FIELD_TYPE("fieldType");


    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    TCLosGngMappingFields(String value) {
        this.value = value;
    }
}
