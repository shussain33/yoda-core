package com.softcell.constants;

/**
 * @author vinodk
 */
public enum AuditType {

    RE_APPRAISE_RE_INITIATE,
    RE_INITIATE,
    EXPRESS_QDE,
    SOLICITED_QDE,
    EXISTING_QDE;
}
