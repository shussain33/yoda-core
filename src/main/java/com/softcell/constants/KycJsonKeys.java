package com.softcell.constants;

/**
 * @author yogeshb
 *         Josn Constant for Kyc class
 */

public class KycJsonKeys {

    public static String ID_NO = "Identification No";

    //Applicant Photo reason for applicant photo.
    public static String APPLICANT_PHOTO = "Applicant Photo";

    public static class IdNoValues {

        public static final String VALID_PASSPORT = "Valid Passport";
        public static final String PAN_CARD = "Pan Card";
        public static final String DRIVING_LICENSE = "Driving Licence";
        public static final String VOTERS_CARD = "Voter’s Identity Card";
        public static final String AADHAR_CARD = "Aadhar UID card";
        public static final String BANK_PASSBOOK_PHOTO = "Bank Passbook with photo";
    }
}