package com.softcell.constants;

/**
 * Created by amit on 23/7/18.
 */
public class ConfigurationConstants {

    public enum ConditionExpression{
        equals, nEquals, OR, AND
    }

    public enum ConditionType{
        COND_BOOLEAN, COND_MULTIPLE
    }

    public static final String APPLICATION_FLOW_TYPE_NORMAL = "NORMAL";
    public static final String APPLICATION_FLOW_TYPE_COORIGINATION = "CO-ORIGINATION";

    public static final String TYPE_COND = "TYPE_CONDITION";
    public static final String TYPE_EXP = "TYPE_EXP";
    public static final String COND_TYPE_ROLE = "ROLE";
    public static final String COND_TYPE_STAGE = "STAGE";
    public static final String COND_TYPE_STATUS = "STATUS";
    public static final String COND_TYPE_FLOW_TYPE = "FlowType";

    public static final String WF_TYPE_MAIN = "MAIN";
    public static final String WF_START_NODE = "FIRST_WORKFLOW_NODE";
    public static final String WF_NODE_TYPE_STAGE = "STAGE";
    public static final String WF_NODE_TYPE_SCREEN = "SCREEN";
    public static final String WF_NODE_TYPE_SUBFLOW = "SUBFLOW";
    public static final String WF_LAST_NODE = "WF_LAST_NODE";
    public static final String WF_EVENT_SUBMIT = "SUBMIT";
    public static final String WF_EVENT_SAVE = "SAVE";
    public static final String HIERARCHY_TYPE_ROLE = "ROLE";
    public static final String EXTERNAL_SERVICE_MIFIN = "MIFIN";
    public static final String EXTERNAL_SERVICE_AMBIT_MIFIN_APPLICANTS ="AMBIT_MIFIN_APPLICANTS";
    public static final String EXTERNAL_SERVICE_AMBIT_MIFIN_SAVE_DATA ="AMBIT_MIFIN_SAVE_DATA";
    public static final String MIFIN_DEDUPE_CALL = "MIFIN_DEDUPE";
    public static final String CREDIT = "CREDIT";
    public static final String LEGAL = "LEGAL";
    public static final String OPERATIONS = "OPERATIONS";
    public static final String BOTTOM = "BOTTOM";
    public static final String TOP = "TOP";

    public static final String NODE_THIRD_PARTY_SAVE = "third-party-save";

    public static final String EXTERNAL_SERVIC_EMI_CAL = "MIFIN-EMI-CAL";

    public static final String EXTERNAL_SERVICE_PREMIUM_CALCULATOR = "EXTERNAL-SERVICE-PREMIUM-CALCULATOR";

    public static final String EXTERNAL_SERVICE_LMS = "LMS";

    public static final String EXTERNAL_SERVICE_ICICI_CORPORATE_API = "ICICI_CORPORATE_API";
    public static final String QUICK_CHECK_API = "QUICK_CHECK_API";

    public static final String EXTERNAL_SERVICE_SBFC_MIFIN_IMD ="SBFC_MIFIN_IMD";
}