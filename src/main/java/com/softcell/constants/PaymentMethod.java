package com.softcell.constants;

/**
 * Created by Softcell on 31/08/17.
 */
public enum PaymentMethod {

    DOWN_PAYMENT,
    EMI;
}
