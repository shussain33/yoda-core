package com.softcell.constants;

/**
 * Created by mahesh on 12/12/17.
 */
public enum LoyaltyCardType {

    HDBFS("HDBFS"),
    CROMA("CROMA");

    private final String cardType;

    LoyaltyCardType(final String cardType) {
        this.cardType = cardType;
    }

    public String toFaceValue() {
        return cardType;
    }
}
