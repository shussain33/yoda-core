package com.softcell.constants;

public enum LosTvsEndPointServiceEnums {

    invokeTvsGroupThreeCreateService,
    invokeTvsCreateServiceGroupOne,
    invokeTvsGroupFourCreateService,
    invokeTvsCreateServiceGroupFive,
    invokeTvsCreateServiceGroupTwo,
    invokeTVSIntiateService

}
