package com.softcell.constants;

/**
 * Created by amit on 11/7/18.
 */
public class IntimationConstants {

    public static final String GROUP_TYPE_GENERAL = "GENERAL";
    public static final String GROUP_TYPE_SPECIFIC = "SPECIFIC";
    public static final String GROUP_TYPE_PAN_INDIA = "PAN_INDIA";
    public static final String GROUP_TYPE_ROLE_WISE = "ROLE_WISE";
    public static final String GROUP_TYPE_BRANCH_WISE = "BRANCH_WISE";
    public static final String GROUP_TYPE_BRANCH_ROLE_WISE = "BRANCH_ROLE_WISE";
    public static final String GROUP_TYPE_ZONE_WISE = "ZONE_WISE";
    public static final String GROUP_TYPE_ZONE_ROLE_WISE = "ZONE_ROLE_WISE";
    public static final String GROUP_TYPE_REGION_ROLE_WISE = "REGION_ROLE_WISE";
    public static final String GROUP_TYPE_LOCATION_ROLE_WISE = "LOCATION_ROLE_WISE";
    public static final String REGION = "REGION";
    public static final String LOCATION = "LOCATION";
    public static final String INTIMATION_TYPE_EMAIL = "EMAIL";
    public static final String INTIMATION_TYPE_SMS = "SMS";
    public static final String GROUP_TYPE_VALUE_ROLE = "ROLE";
    public static final String ZONE = "ZONE";
    public static final String VERIFICATION_STATUS_TRIGGERED = "TRIGGERED";
    public static final String VERIFICATION_STATUS_VERIFIED = "VERIFIED";
    public static final String GROUP_TYPE_CUSTOMER = "CUSTOMER";
    public static final String GROUP_TYPE_APPLICANT = "APPLICANT";
    public static final String GROUP_TYPE_CO_APPLICANTS = "CO-APPLICANTS";

    // Product actions
    public static final String REGISTRATION = "Registration";
    public static final String DEMOGRAPHIC_DETAILS = "Demographic Save";
    public static final String MCP_DETAILS = "MCP Save";
    public static final String IMD_DETAILS = "IMD Save";
    public static final String COLLATERALS_DETAILS = "Collaterals Save";
    public static final String PERSONAL_DETAILS = "PD Save";
    public static final String SALES_SUBMIT = "Sales Submit";
    public static final String CPA_SUBMIT = "CPA Submit";
    public static final String CREDIT_SUBMIT = "Credit Submit";
    public static final String BOPS_SUBMIT = "BOPS Submit";
    public static final String HOPS_SUBMIT = "HOPS Submit";
    public static final String CREDIT_DECISION = "Credit Decision";
    public static final String VERIFICATION_TRIGGER = "Verification Trigger";
    public static final String VERIFICATION_DONE = "Verifier Verified";
    public static final String CASE_RECOMMENDATION = "Case Recommend";
    public static final String PROPERTY_VALUATION = "Property Valuation";
    public static final String BRANCH = "BRANCH";

    public static final String VER_TYPE = "VER_TYPE";
    public static final String VER_TYPE_VALUATION = "Valuation";
    public static final String VER_TYPE_LEGAL = "LEGAL";
    public static final String RCU_SUBMIT = "RCU Submit";
    public static final String RCU_INITIATED = "RCU initiated";
}
