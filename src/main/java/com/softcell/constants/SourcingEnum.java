package com.softcell.constants;

/**
 * Created by mahesh on 17/5/17.
 * This class is used for sourcing related constant.
 */
public enum SourcingEnum {
    SOURCEID,
    SOURCE,
    VALIDSRC ,
    Y,
    LEA,
    ACCTYPE
}
