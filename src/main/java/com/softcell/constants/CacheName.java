package com.softcell.constants;

/**
 * Created by yogeshb on 11/5/17.
 */
public enum CacheName {
    ACTION, EMAIL, TEMPLATE, APPLICABLE_VENDOR, ALIAS_NAME,LOYALTY_CARD,DMS_FOLDER_CONFIG,SMS_TEMPLATE,
    MASTER_MAPPING ,CASE_CANCEL_CONFIG, IMPS, API_ROLE_AUTH,
}
