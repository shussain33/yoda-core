package com.softcell.constants;


/**
 * @author bhuvneshk
 *         * Will used to  validate access control and maintain data consistency.
 *         All component will defined here, which will be use for access check purpose.
 */

public enum ModuleKeyName {
    NEGATIVE_DEDUPE_EXECUTOR("NegativeDedupe"),
    MB_EXECUTOR("multiBureauExecutor"),
    AADHAR_EXECUTOR("aadharExecutor"),
    PAN_EXECUTOR("panVarificationExecuter"),
    NTC_EXECUTOR("ntcExecutor"),
    SCORING_EXECUTOR("scoringExecutor"),
    VERIFICATION_EXECUTOR("verificationScoring");

    private final String moduleKey;

    private ModuleKeyName(final String moduleKey) {
        this.moduleKey = moduleKey;
    }

    public String toModuleKey() {
        return moduleKey;
    }
}
