package com.softcell.constants;

/**
 * @author vinodk
 */
public class UploadFileTypes {

    public static final String PERMANENT_ADDRESS = "Permanent Address Proof";
    public static final String PRESENT_ADDRESS = "Present Residence Address";

    public static final String DATE_OF_BIRTH = "Date of Birth";
    public static final String ID_NO = "Identification No.";
    public static final String SIGNATURE_PROOF = "Signature Proof";
    public static final String GOVT_ISSUED_DOCUMENT = "Govt. issued document";
    public static final String BANK_SIGN_VERIFICATION = "Bank Sign Verification";

    public static final String PAN = "PAN";
    public static final String APPLICANT_PHOTO = "Applicant Photo";
    public static final String AADHAR = "Aadhar";
    public static final String PASSPORT = "Passport";
    public static final String DRIVING_LICENSE = "Driving License";

    public static final String OTHER1 = "Other1";
    public static final String OTHER2 = "Other2";
    public static final String OTHER3 = "Other3";
    public static final String OTHER4 = "Other4";
    public static final String OTHER5 = "Other5";

    public static final String BUSINESS_PHOTO_1 = "Business Photo 1";
    public static final String BUSINESS_PHOTO_2 = "Business Photo 2";
    public static final String BUSINESS_PHOTO_3 = "Business Photo 3";


    public static final String PROPERTY_IMAGE_1 = "Property Image 1";
    public static final String PROPERTY_IMAGE_2 = "Property Image 2";
    public static final String PROPERTY_IMAGE_3 = "Property Image 3";

    // File names
    public static final String MARK_SHEET ="Marks Sheet" ;
    public static final String LANDLINE_BILL = "Fixed Line Telephone Bill";
    public static final String POST_PAID_BILL = "Post Paid Bill";
    public static final String ELECTRIC_BILL  = "Electric Bill";
    public static final String WATER_BILL  = "Water Bill";
    public static final String GAS_BILL  = "Gas Bill";
    public static final String TAX_BILL  = "Tax Bill";
    public static final String UTILITY_BILL = "Utility bill";

    public static final String INCOME_PROOF1 = "Income Proof1";
    public static final String INCOME_PROOF2 = "Income Proof2";
    public static final String BANK_STATEMENT = "Bank Statement";
    public static final String DEBIT_CARD = "Debit Card";
    public static final String COMPANY_ID_CARD = "Company Id Card";
    public static final String SALARY_SLIP = "Salary Slip";

    public static final String POWER_OF_ATTORNEY = "Power of Attorney";
    public static final String POSSESSION_LETTER  = "Possession Letter";
    public static final String RENT_AGREEMENT = "Rent Agreement";
    public static final String INDEX_COPY = "Index copy";
    public static final String SALE_DEED = "Sale Deed";
    public static final String ALLOTMENT_LETTER = "Allotment Letter";

    // file Type
    public static final String SECTION_ID_PROOF = "Identity Proof";
    public static final String SECTION_AGE_PROOF = "Age Proof";
    public static final String SECTION_SIGN_VERIFY_PROOF = "Signature Verification";
    public static final String SECTION_ADDRESS_PROOF = "Residence/Address Proof";
    public static final String SECTION_PROPERTY_PROOF = "Property Ownership Proof";
    public static final String SECTION_INCOME_PROOF = "Income Proof";
}
