package com.softcell.constants;

/**
 * Created by yogeshb on 11/5/17.
 */
public enum Vendor {
    SAMSUNG("SAMSUNG"),
    LG("LG"),
    SONY("SONY"),
    INTEX("INTEX"),
    APPLE("APPLE"),
    HAIER("HAIER"),
    HITACHI("HITACHI"),
    GIONEE("GIONEE"),
    PANASONIC("PANASONIC"),
    VIDEOCON("VIDEOCON"),
    ONIDA("ONIDA"),
    GODREJ("GODREJ"),
    DAIKIN("DAIKIN"),
    KENTSTAR("KENTSTAR"),
    KELVINATOR("KELVINATOR"),
    SANSUI("SANSUI"),
    KENT("KENT"),
    OPPO("OPPO"),
    GOOGLE("GOOGLE"),
    YUHO("YUHO"),
    COMIO("COMIO"),
    NOKIA("NOKIA"),
    VOLTAS("VOLTAS"),
    PAYNIMO("PAYNIMO"),
    LAVA("LAVA"),
    CARRIER("CARRIER");




    private final String stringValue;

    Vendor(final String s) {
        stringValue = s;
    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toFaceValue() {
        return stringValue;
    }
}
