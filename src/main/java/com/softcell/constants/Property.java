package com.softcell.constants;

/**
 * @author suhasinis
 *
 */

public enum Property {

	USAGE_RESIDENTIAL("Residential"), 
	USAGE_COMMERCIAL("Commercial"),

	STAUS_COMPLETE("Complete"), 
	STATUS_UC("UC"),

	TYPE_FLAT("Flat"), 
	TYPE_BUNGLOW("Bunglow"), 
	TYPE_OFFICE("Office"), 
	TYPE_ROWHOUSE("Rowhouse"), 
	TYPE_SHOP("Shop"), 
	TYPE_MIXED_USE("Mixed Use"),

	OCCUPATION_SELF_OCCUPIED("Self-occupied"), 
	OCCUPATION_RENTED("Rented"), 
	OCCUPATION_VACANT("Vacant"), 
	OCCUPATION_MIXED_USE("Mixed Use");

	private final String property;

	private Property(String property) {
		this.property = property;
	}

	public String value() {
		return property;
	}

	public static Property get(String value) {

		if (value.equals(Property.USAGE_COMMERCIAL.value())) {
			return Property.USAGE_COMMERCIAL;
		}
		if (value.equals(Property.USAGE_RESIDENTIAL.value())) {
			return Property.USAGE_RESIDENTIAL;
		}
		if (value.equals(Property.STATUS_UC.value())) {
			return Property.STATUS_UC;
		}
		if (value.equals(Property.STAUS_COMPLETE.value())) {
			return Property.STAUS_COMPLETE;
		}
		if (value.equals(Property.TYPE_BUNGLOW.value())) {
			return Property.TYPE_BUNGLOW;
		}
		if (value.equals(Property.TYPE_FLAT.value())) {
			return Property.TYPE_FLAT;
		}
		if (value.equals(Property.TYPE_MIXED_USE.value())) {
			return Property.TYPE_MIXED_USE;
		}
		if (value.equals(Property.TYPE_OFFICE.value())) {
			return Property.TYPE_OFFICE;
		}
		if (value.equals(Property.TYPE_ROWHOUSE.value())) {
			return Property.TYPE_ROWHOUSE;
		}
		if (value.equals(Property.TYPE_SHOP.value())) {
			return Property.TYPE_SHOP;
		}
		if (value.equals(Property.OCCUPATION_MIXED_USE.value())) {
			return Property.OCCUPATION_MIXED_USE;
		}
		if (value.equals(Property.OCCUPATION_RENTED.value())) {
			return Property.OCCUPATION_RENTED;
		}
		if (value.equals(Property.OCCUPATION_SELF_OCCUPIED.value())) {
			return Property.OCCUPATION_SELF_OCCUPIED;
		}
		return Property.OCCUPATION_VACANT;
	}

}
