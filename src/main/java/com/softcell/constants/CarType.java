package com.softcell.constants;

/**
 * Created by mahesh on 24/9/17.
 */
public enum CarType {

    MUV,SUV,PREMIUM,COMPACT,MINI
}
