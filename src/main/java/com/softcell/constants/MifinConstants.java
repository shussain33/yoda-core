package com.softcell.constants;

public class MifinConstants {
    public static final String mifin ="mifin";
    public static final String newLoan ="newLoan";
    public static final String saveApplicantDetails = "saveApplicantDetails";
    public static final String saveApplicantAddress = "saveApplicantAddress";
    public static final String saveFinancialInfo = "saveFinancialInfo";
    public static final String panCardSearch = "panCardSearch";
    public static final String deleteApplicant ="deleteApplicant";
    public static final String saveCRDecision="saveCRDecision";
    public static final String processDedupe="processDedupe";
    public static final String updateDedupeStatus="updateDedupeStatus";
    public static final String saveVerification = "saveVerification";
    public static final String saveDisbursalMaker = "saveDisbursalMaker";
    public static final String searchExistingApplicant="searchExistingApplicant";
    public static final String bodyStr = "bodyStr";
    public static final String updateLoanDetails = "updateLoanDetails";
    public static final String body = "body";
    public static final String RECEIVE ="RECEIVE";
    public static final String STATUS="STATUS";
    public static final String STATUS_S ="S";
    public static final String STATUS_F ="F";
    public static final String MESSAGE="MESSAGE";
    public static final String PROSPECTID="PROSPECTID";
    public static final String PROSPECTCODE ="PROSPECTCODE";
    public static final String PROSPECT_CODE ="PROSPECT_CODE";
    public static final String APPLICANTID ="APPLICANTID";
    public static final String APPLICANTCODE ="APPLICANTCODE";
    public static final String PAN="PAN";
    public static final String RESIADDRESS_ID="RESIADDRESS_ID";
    public static final String PERMAADDRESS_ID ="PERMAADDRESS_ID";
    public static final String OFFADDRESS_ID ="OFFADDRESS_ID";
    public static final String EMPLOYMENTDETAILS_ID="EMPLOYMENTDETAILS_ID";
    public static final String EMPLOYMENTDETAILS = "EMPLOYMENTDETAILS";
    public static final String APPLICANT_DETAILS ="APPLICANT_DETAILS";
    public static final String BANKING_ID = "BANKING_ID";
    public static final String ACC_DTL_ID = "ACC_DTL_ID";
    public static final String BANKDETAILSID = "BANKDETAILSID";
    public static final String OTHER_LOANS_DETL_ID = "OTHER_LOANS_DETL_ID";
    public static final String OTHERLOANDETAILSID = "OTHERLOANDETAILSID";
    public static final String FINANCIAL_ID = "FINANCIAL_ID";
    public static final String FINANCIAL_TAB_ID = "FINANCIAL_TAB_ID";
    public static final String ITR_ID = "ITR_ID";
    public static final String APPLICANT_ADDRESS = "APPLICANT_ADDRESS" ;
    public static final String CUSTOMERDETAIL ="CUSTOMERDETAIL";
    public static final String CREDITDECISIONID="CREDITDECISIONID";
    public static final String CUSTOMER_MATCHED_INFO="CUSTOMER_MATCHED_INFO";
    public static final String APPLICANT_MATCHED_DETAILS="APPLICANT_MATCHED_DETAILS";
    public static final String LOAN_DETAILS = "LOAN_DETAILS";
    public static final String VERIFICATION_DETAILS = "VERIFICATION_DETAILS";
    public static final String VERIFICATION_ID = "VERIFICATIONID";
    public static final String VERIFIED = "VERIFIED";
    public static final String COMPLETED= "COMPLETED";
    public static final String RESIDENCE = "RESIDENCE";
    public static final String OFFICE = "OFFICE";
    public static final String RCU_CHECK = "RCU check";
    public static final String PERSONAL_VERIFICATION = "Personal Verification";
    public static final String APPLICANT="APPLICANT";
    public static final String CO_APPLICANT="CO-APPLICANT";
    public static final String INVALID_DATE="Invalid date";
    public static final String AMBIT_UDYAM="AMBIT Udyam-Income Based";
    public static final String AMBIT_VYAPAR="AMBIT VYAPAR-INCOME BASED";
    public static final String savePersonalInsurance = "savePersonalInsurance";
    public static final String DISBURAL_DETAILS = "DISBURAL_DETAILS";
    public static final String NEW="NEW";
    public static final String CHANNEL="DSA";
    public static final String DSA = "Direct Sales Agent";
    public static final String DELHI_BRANCH = "NEW DELHI";
    public static final String DSA_DELHI = "DSADELHI";
    public static final String ELECTRICITY_BILL="Electricity Bill Authentication";
    public static final String LPG_BILL="LPG ID Authentication";
    public static final String PNG_BILL="PNG Authentication";



}
