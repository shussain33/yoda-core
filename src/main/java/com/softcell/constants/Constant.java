package com.softcell.constants;

/**
 * @author kishor
 */
public interface Constant {
     String SUCCESS = "SUCCESS";
     String ERROR = "ERROR";
     String FAILED = "FAILED";
     String DO = "DO";
     String DM = "DM";
     String RB = "RB";
     String BLOCKSTATUS = "Y";
     String ROLLBACKSTATUS = "N";
     String BLANK = "";
     String BRANCH = "Branch";
     String NA = "N/A";
     String ZERO = "0";
     String SEVENTY = "70";
     String ONE = "1";
     String YN_Yes = "Yes";
     String YN_No = "No";
     String NULL = "NULL";
     String CO_ORIGINATION = "-C";
     String UNDERSCORE = "_";
     String CURRENT = "Current";
     String SAVING = "Saving";
     String PENDING = "Pending";
     String SPACE = " ";
     String DEFAULT = "default";
    String COMPANYNAME = "TATA AIG";

     String DEFAULT_INSTID = "9999";

     String VERIFIED = "Verified";
     String EXPIRED = "Expired";

     //OSOL constants
     String ROLENAME = "role";
     String USER_ID = "userId";
     String AUTH_TOKEN = "authToken";
     String WARNING = "WARNING";
     String CRITICAL = "CRITICAL";
     String SOURCE_ID = "sourceId";
     String INST_ID = "institutionID";
     String AUTHORIZATION = "Authorization";
     String USER_DETAILS = "User-Details";
     String NON_TRANSACTIONAL = "NON-TRANSACTIONAL";
     String TRANSACTIONAL = "TRANSACTIONAL";
     String USER_EMAIL = "helpdesk@lentra.ai";
     String USER_NAME = "lentra";
     String USER_ROLE = "admin";
     String MASTER_UPLOAD_CONFIGURATION = "masterUploadConfiguration";
     String GRID_FS = "GRID_FS";
     String MASTER_NAME = "masterName";
     String MASTER_CATEGORY = "masterCategory";
     String LAST_UPDATE_DATE = "updateDate";
     String STATUS_s = "status";
     String ABORT = "ABORT";
     String ACTIVE = "ACTIVE";
     String DESTINATION_MASTER = "destinationMasterName";
     String ENABLE = "enable";
     String INSTITUTION_ID = "institutionId";
     String HIERARCHY = "Hierarchy";
     String DEALERS = "Dealers";
     String PRODUCTS = "Products";
     String ROLES = "Roles";
     String BRANCHES = "Branches";
     String SME = "SME";
     String NORMAL = "Normal";

     String LEADSOURCE ="Leadsource" ;
     String STATUS = "status";
     String ALL_MATCH = "*";
     String CASE_CANCELLATION_JOB_CONFIG = "caseCancellationJobConfig";
     String CACHE_CONFIGURATION_COLLECTION = "cacheConfiguration";
     String WARNING_THRESHOLD = "warningThreshold";
     String CRITICAL_THRESHOLD = "criticalThreshold";
     String IS_CACHE_ENABLED = "isCacheEnabled";
     String STANDALONE_SERVER="standalone";
     String CLUSTER_SERVER="cluster";

     //Miscellaneous
     String NUMERIC = "NUMERIC";
     String ALPHA_NUMERIC = "ALPHA_NUMERIC";
}
