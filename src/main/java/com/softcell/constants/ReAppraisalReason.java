package com.softcell.constants;

public enum ReAppraisalReason {
    HIGHER_LOAN_AMOUNT,
    ADDITIONAL_SURROGATE,
    RELOOK
}
