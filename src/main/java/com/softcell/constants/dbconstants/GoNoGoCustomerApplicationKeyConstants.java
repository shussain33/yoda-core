package com.softcell.constants.dbconstants;

/**
 * @author yogeshb Write here key constants for database fields to access
 *         directly using that key. It increase productivity,so follow it .
 */
public interface GoNoGoCustomerApplicationKeyConstants {


    String REF_ID = "gngRefId";
    String APPLICATION_SUBMIT_DATE_TIME = "dateTime";

    /**
     * Application Request
     */

    String DSA_ID = "applicationRequest.header.dsaId";
    String APPLICATION_SOURCE = "applicationRequest.header.applicationSource";
    String LOAN_TYPE = "applicationRequest.request.application.loanType";
    String INSTITUTION_ID = "applicationRequest.header.institutionId";
    String DEVICE_INFO = "applicationRequest.header.deviceInfo";
    /**
     * MultiBureau
     */
    String SCORE_VALUE = "applicantComponentResponse.scoringServiceResponse.scoreData.scoreValue";
    String BUREAU = "applicantComponentResponse.multiBureauJsonRespose.finishedList.bureau";
    String PDF_REPORT = "applicantComponentResponse.multiBureauJsonRespose.finishedList.pdfReport";
    String STATUS = "applicantComponentResponse.multiBureauJsonRespose.finishedList.status";
    String SCORE = "applicantComponentResponse.multiBureauJsonRespose.finishedList.responseJsonObject.scoreList.score";
    String SCORE_NAME = "applicantComponentResponse.multiBureauJsonRespose.finishedList.responseJsonObject.scoreList.scoreName";
    String RRP_LIVE_TRADES_COUNT = "applicantComponentResponse.multiBureauJsonRespose.finishedList.bureauRrpObject.rrpLiveTradesCount";
    String RRP_TOTAL_OUTSTANDING = "applicantComponentResponse.multiBureauJsonRespose.finishedList.bureauRrpObject.rrpTotalOutstanding";

    String IDENTIFIER = "identifier";
    String LASTUPDATEDDATE = "lastUpdatedDate";
    String ACTUALDISBDATE = "actualDisbDate";
}
