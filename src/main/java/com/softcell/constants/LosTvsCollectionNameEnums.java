package com.softcell.constants;

public enum LosTvsCollectionNameEnums {

    GoNoGoCustomerApplication_query,
    PostIpaRequest_query,
    InsurancePremiumDetails_query,
    SchemeMasterData_query,
    SerialNumberInfo_query,
    ESignedLog_query,
    ExtendedWarrantyDetails_query,
    DocumentListForGroupFive_query;

}
