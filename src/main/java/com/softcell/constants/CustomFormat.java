/**
 * yogeshb1:38:13 pm  Copyright Softcell Technolgy
 **/
package com.softcell.constants;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @author yogeshb
 *
 */
public class CustomFormat {
    public static final NumberFormat NUMBER_0_00 = new DecimalFormat("#0.00");
    public static final NumberFormat NUMBER_0 = new DecimalFormat("#0");
    public static final DecimalFormat DECIMAL_FORMATE_0 = new DecimalFormat("#");
    public static final DecimalFormat DF = new DecimalFormat("#.##");
}
