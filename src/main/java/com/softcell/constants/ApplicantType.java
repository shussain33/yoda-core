package com.softcell.constants;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by archana on 4/8/17.
 */
public enum ApplicantType {
    EXISTING("EXISTING"),
    SOLICITED("SOLICITED"),
    EXPRESS("EXPRESS"),
    NORMAL("NORMAL"),

    /* mapped with applicant.applicantType */
    // UI sends camel-case value; we need to set manually sometimes when UI fails to send.
    INDIVIDUAL("Individual"),

    PROPRIETORSHIP("Proprietorship"),
    PARTNERSHIP("Partnership"),
    PVT_LTD("Pvt. Ltd."),
    LTD("Ltd."),

    // This is derived value - not set on applicant but used to determine the applicant type based on value INDIVIDUAL
    NON_INDIVIDUAL("NON-INDIVIDUAL"),
    /*  --- end applicant.applicantType */

    //mifin type
    CORPORATE("CORPORATE");

    private final String applicantType;

    private ApplicantType(String applicantType){
        this.applicantType = applicantType;
    }

    public String value(){
        return applicantType;
    }

    public static ApplicantType get(String value){

        if( value.equals(ApplicantType.EXPRESS.value() )) {
            return ApplicantType.EXPRESS;
        }
        if( value.equals(ApplicantType.EXISTING.value() )) {
            return ApplicantType.EXISTING;
        }
        if( value.equals(ApplicantType.SOLICITED.value() )) {
            return ApplicantType.SOLICITED;
        }
        if( value.equals(ApplicantType.INDIVIDUAL.value() )) {
            return ApplicantType.INDIVIDUAL;
        }
        if( value.equals(ApplicantType.NON_INDIVIDUAL.value() )) {
            return ApplicantType.NON_INDIVIDUAL;
        }
        return ApplicantType.NORMAL;
    }

    public static boolean isIndividual(String applicantType) {
        return StringUtils.equalsIgnoreCase(INDIVIDUAL.toString(),applicantType);
    }
}
