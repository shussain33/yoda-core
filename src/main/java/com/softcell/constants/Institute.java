package com.softcell.constants;

import org.apache.commons.lang3.StringUtils;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by ssg408 on 19/9/18.
 */
public enum Institute {

    DEFAULT("00", "00"),
    SBFC("4075", "4021"),
    JVFS("4084", "4025"),
    FVSG("4080", "4033"),
    AMBIT("4160","4074"),
    TRIC("4170","4071");

    private final String lowerEnvrment;
    private final String production;

    static final private Map<String,String > ID_MAP = new HashMap<>();

    static {


        for(Institute institute :  Institute.values()){
            ID_MAP.put(institute.name(),institute.toProductionId());
        }
    }

    Institute(final String lowerEnvrment, final String production) {
        this.lowerEnvrment = lowerEnvrment;
        this.production = production;

    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toLowerEnvId() {return lowerEnvrment;}

    public String toProductionId() {
        return production;
    }

    public static boolean isInstitute(String institutionId, Institute institute){
        if(StringUtils.equalsIgnoreCase(institutionId, institute.lowerEnvrment)
                || StringUtils.equalsIgnoreCase(institutionId, institute.production)){
            return true;
        }
        return false;
    }

    public static Institute getInstitute(String institutionId){
        for(Institute institute :  Institute.values()) {
            if (StringUtils.equalsIgnoreCase(institutionId, institute.lowerEnvrment)
                    || StringUtils.equalsIgnoreCase(institutionId, institute.production)) {
                return institute;
            }
        }
        return null;
    }
}
