package com.softcell.constants;

/***
 * @author kishorp
 */
public enum CacheConstant {
    /**
     * Error Map From Cache
     */
    ERROR_MAP, BEAN_FACTORY, GENDER, ADDRESS_TYPE, CONSTITUTION, EMPLOY_TYPE, KYC_DOCUMENTS, STATUS_INCLUDE_FIELD_TAB, WEB
}
