package com.softcell.constants;

/**
 * Created by anupamad on 8/8/17.
 */
public enum FieldConstants {
    /**
     * Welcome Screen Flags
     */
    MOBILE_NUMBER("sMobileNumber"),
    DOB("sDob"),
    HOUSE("sResAddrType"),
    YRS_AT_CURRENT_RESI("iMonthAtAddr"),
    EMPLOYMENT_TYPE("sEmplType"),
    COMPANY_NAME("sEmplName"),
    EMPLOYMENT_ADDRESS("sEmplAddress"),
    DESIGNATION("sDesig"),
    INCOME_PM("dGrossMonthlyAmt"),
    PRODUCT_SELECTED("aAssetDetail"),
    SERIAL_NUMBER("sSerialNumber"),
    ASSET_DEL_STATUS("sAssetDelStatus"),
    CUSTOMER_NAME("sCustomerName"),
    CUSTOMER_ADDRESS("sCustomerAddress"),
    ENDUSAGE("sEndUsage");

    private final String stringValue;

    FieldConstants(final String s) {
        stringValue = s;
    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toFaceValue() {
        return stringValue;
    }

    public static String getEnumNameForValue(String value){
        FieldConstants[] values = FieldConstants.values();
        String enumValue = null;

        for(FieldConstants eachValue : values) {
            enumValue =eachValue.toFaceValue();
            if (enumValue.equals(value)) {
                return eachValue.name();
            }
        }
        return enumValue;
    }
}
