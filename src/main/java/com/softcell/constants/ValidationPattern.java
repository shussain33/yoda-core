package com.softcell.constants;

/**
 * @author yogeshb
 */

/**
 * @author mahesh
 */
public class ValidationPattern {

    /**
     * Regex Patterns
     */

    public static final String PAN = "[A-Z]{5}[0-9]{4}[A-Z]{1}";// correct

    public static final String UID = "[0-9]{12}";// correct

    public static final String DATE_MILLISECOND_FORMAT = "[0-9]{13}"; //13 digit format

    public static final String VOTER_ID = "[A-Z]{3}[0-9]{7}";// correct

    public static final String DRIVING_LICENSE = "[A-Z]{1,4}[-]{0,1}[0-9]{13,14}";// correct

    public static final String PASSPORT = "[A-Z]{1}[0-9]{7}";// correct

    public static final String AADHAAR_CARD = "^$|[0-9]{12}";

    public static final String RATION_CARD = "[A-Z]{12}";// not sure

    public static final String PIN_CODE = "[0-9]{6}";// correct

    public static final String TIME_AT_ADDRESS = "[0-9]{1,2}";// correct

    public static final String PHONE_NUMBER = "[0-9]{10,20}";// correct

    public static final String EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";// correct

    public static final String FIRST_NAME = "^[a-zA-Z\\s]*$";// correct

    public static final String LAST_NAME = "^[a-zA-Z\\s]*$";// correct

    public static final String MIDDLE_NAME = "^[a-zA-Z\\s]*$";// correct

    public static final String DOB = "(0?[1-9]|[12][0-9]|3[01])(0?[1-9]|1[012])((19|20)\\d\\d)";// correct

    public static final String STATE_CITY_COUNTRY = "([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)";

    public static final String SPECIAL_CHARACTER = "[^&%$#@!~]+";

    public static final String SAMSUNG_SALE_DATE = "^\\d{4}\\d{2}\\d{2}$";

    public static final String GST_NUMBER = "^([0-9]{2})([A-Z]{5})([0-9]{4})([A-Z]{1})([A-Za-z0-9]{3})$";
}
