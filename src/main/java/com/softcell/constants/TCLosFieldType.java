package com.softcell.constants;

/**
 * Created by anupamad on 30/9/17.
 */
public enum TCLosFieldType {
    INCOME,
    EXPENSE;
}
