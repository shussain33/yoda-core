package com.softcell.constants;

/**
 * Created by bhuvneshk on 7/3/17.
 */
public enum CustomHttpStatus{


    CONFIGURATION_NOT_FOUND(5000, "CONFIGURATION_NOT_FOUND"),
    INVALID_REQUEST_CODE(5001,"INVALID_REQUEST_CODE"),
    THIRD_PARTY_INTERNAL_SERVER_ERROR(5002,"THIRD_PARTY_INTERNAL_SERVER_ERROR"),
    UNABLE_TO_CHANGE_APPLICATION_STATUS(5003,"UNABLE_TO_CHANGE_APPLICATION_STATUS"),
    THIRD_PARTY_ERROR(5004,"THIRD_PARTY_ERROR"),
    THIRD_PARTY_NO_CONTENT(5005,"THIRD_PARTY_NO_CONTENT"),
    IMPS_CONFIGURATION_NOT_FOUND(5006, "IMPS_CONFIGURATION_NOT_FOUND"),
    INTERNAL_SERVER_ERROR(500,"INTERNAL_SERVER_ERROR"),
    UNAUTHORIZED_FAILED(499,"Unauthorized service");


    private final int value;
    private final String reasonPhrase;

    CustomHttpStatus(int value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    public int value() {
        return this.value;
    }

    public String getReasonPhrase() {
        return this.reasonPhrase;
    }


    }
