package com.softcell.constants.error;

/**
 * @author mahesh
 */
public class ErrorCode {
    /**
     * HEADER EROR CODE
     */
    public static final String HEADER_BLANK = "H100";
    public static final String HEADER_APPLICATION_SOURCE_BLANK = "H101";
    public static final String HEADER_INSTITUTION_ID_BLANK = "H102";
    public static final String HEADER_REQUEST_TYPE_BLANK = "H103";
    public static final String HEADER_SOURCE_ID_BLANK = "H104";
    public static final String HEADER_APPLICATION_SOURCE_INVALID = "H105";
    public static final String HEADER_INSTITUTION_ID_INVALID = "H106";
    public static final String HEADER_REQUEST_TYPE_INVALID = "H107";
    public static final String HEADER_SOURCE_ID_INVALID = "H108";
    public static final String HEADER_DATE_TIME_BLANK = "H109";
    public static final String HEADER_DATE_TIME_INVALID = "H110";
    public static final String HEADER_CRO_ID_BLANK = "H111";
    public static final String HEADER_DEALER_ID_BLANK = "H112";
    public static final String HEADER_DSA_ID_BLANK = "H113";
    public static final String HEADER_CRO_ID_INVALID = "H114";
    public static final String HEADER_DEALER_ID_INVALID = "H115";
    public static final String HEADER_DSA_ID_INVALID = "H116";

    /**
     * REQUEST ERROR CODE
     */

    public static final String REQUEST_HEADER_BLANK = "H117";
    public static final String REQUEST_SUSPICIOUS_ACTIVITY_BLANK = "H118";

    /**
     * APPLICANT ERROR CODE HERE
     */
    public static final String APPLICANT_ID_BLANK = "H119";
    public static final String APPLICANT_ID_INVALID = "H120";
    public static final String APPLICANT_NAME_BLANK = "H121";
    public static final String APPLICANT_FIRST_NAME_BLANK = "H122";
    public static final String APPLICANT_FIRST_NAME_INVALID = "H123";
    public static final String APPLICANT_LAST_NAME_BLANK = "H124";
    public static final String APPLICANT_LAST_NAME_INVALID = "H125";
    public static final String APPLICANT_HEADER_BLANK = "H126";
    public static final String APPLICANT_GENDER_INVALID = "H127";
    public static final String APPLICANT_GENDER_BLANK = "H128";
    public static final String APPLICANT_NAME_INVALID = "H129";
    public static final String APPLICANT_DATE_OF_BIRTH_INVALID = "H130";
    public static final String APPLICANT_DATE_OF_BIRTH_BLANK = "H131";
    public static final String APPLICANT_KYC_BLANK = "H132";
    public static final String APPLICANT_KYC_NAME_INVALID = "H133";
    public static final String APPLICANT_KYC_NAME_BLANK = "H134";
    public static final String APPLICANT_KYC_NUMBER_BLANK = "H135";
    public static final String APPLICANT_PAN_NUMBER_INVALID = "H136";
    public static final String APPLICANT_UID_INVALID = "H137";
    public static final String APPLICANT_VOTER_ID_INVALID = "H138";
    public static final String APPLICANT_DRIVING_LICENSE_NUMBER_INVALID = "H139";
    public static final String APPLICANT_PASSPORT_INVALID = "H140";
    public static final String APPLICANT_RATION_CARD_INVALID = "H141";
    public static final String APPLICANT_KYC_EXPIRY_DATE_BLANK = "H142";
    public static final String APPLICANT_KYC_ISSUE_DATE_BLANK = "H143";
    public static final String APPLICANT_KYC_EXPIRY_DATE_INVALID = "H144";
    public static final String APPLICANT_KYC_ISSUE_DATE_INVALID = "H145";
    public static final String APPLICANT_KYC_STATUS_BLANK = "H146";
    public static final String APPLICANT_ADDRESS_BLANK = "H147";
    public static final String APPLICANT_ADDRESS_LINE1_BLANK = "H148";
    public static final String APPLICANT_ADDRESS_LINE1_INVALID = "H149";
    public static final String APPLICANT_ADDRESS_LINE2_BLANK = "H150";
    public static final String APPLICANT_ADDRESS_LINE2_INVALID = "H151";
    public static final String APPLICANT_ADDRESS_TYPE_BLANK = "H152";
    public static final String APPLICANT_ADDRESS_TYPE_INVALID = "H153";
    public static final String APPLICANT_ADDRESS_CITY_BLANK = "H154";
    public static final String APPLICANT_ADDRESS_CITY_INVALID = "H155";
    public static final String APPLICANT_ADDRESS_COUNTRY_BLANK = "H156";
    public static final String APPLICANT_ADDRESS_COUNTRY_INVALID = "H157";
    public static final String APPLICANT_ADDRESS_PIN_NUMBER_INVALID = "H158";
    public static final String APPLICANT_ADDRESS_PIN_BLANK = "H159";
    public static final String APPLICANT_ADDRESS_STATE_BLANK = "H160";
    public static final String APPLICANT_ADDRESS_STATE_INVALID = "H161";
    public static final String APPLICANT_MONTH_AT_ADDRESS_BLANK = "H162";
    public static final String APPLICANT_ADDRESS_TIME_AT_ADDRESS_INVALID = "H163";
    public static final String APPLICANT_PHONE_BLANK = "H164";
    public static final String APPLICANT_PHONE_TYPE_BLANK = "H165";
    public static final String APPLICANT_PHONE_TYPE_INVALID = "H166";
    public static final String APPLICANT_PHONE_NUMBER_BLANK = "H167";
    public static final String APPLICANT_PHONE_NUMBER_INVALID = "H168";
    public static final String APPLICANT_PHONE_AREA_CODE_BLANK = "H169";
    public static final String APPLICANT_PHONE_COUNTRY_CODE_BLANK = "H170";
    public static final String APPLICANT_EMAIL_TYPE_INVALID = "H171";
    public static final String APPLICANT_EMAIL_TYPE_BLANK = "H172";
    public static final String APPLICANT_EMAIL_ADDRESS_BLANK = "H173";
    public static final String APPLICANT_EMAIL_ADDRESS_INVALID = "H174";

    public static final String APPLICANT_PROPERTY_LOCATION_BLANK = "H175";
    public static final String APPLICANT_PROPERTY_NAME_BLANK = "H176";
    public static final String APPLICANT_PROPERTY_TYPE_BLANK = "H177";
    public static final String APPLICANT_PROPERTY_STATUS_BLANK = "H178";
    public static final String APPLICANT_PROPERTY_BLANK = "H179";
    public static final String APPLICANT_PROPERTY_LOCATION_INVALID = "H180";
    public static final String APPLICANT_PROPERTY_NAME_INVALID = "H181";
    public static final String APPLICANT_PROPERTY_TYPE_INVALID = "H182";
    public static final String APPLICANT_PROPERTY_STATUS_INVALID = "H183";
    public static final String APPLICANT_PROPERTY_ID_BLANK = "H184";
    public static final String APPLICANT_PROPERTY_ID_INVALID = "H185";
    public static final String APPLICANT_PROPERTY_VALUE_INVALID = "H186";
    public static final String APPLICANT_PROPERTY_VALUE_BLANK = "H187";
    public static final String APPLICANT_EMPLOYMENT_BLANK = "H188";
    public static final String APPLICANT_EMPLOYMENT_NAME_BLANK = "H189";
    public static final String APPLICANT_EMPLOYMENT_TYPE_BLANK = "H190";
    public static final String APPLICANT_EMPLOYMENT_MONTHLY_SALARY_BLANK = "H191";
    public static final String APPLICANT_EMPLOYMENT_GROSS_SALARY_BLANK = "H192";
    public static final String APPLICANT_EMPLOYMENT_TIME_WITH_EMPLOYER_BLANK = "H193";
    public static final String APPLICANT_EMPLOYMENT_DATE_OF_JOINING_BLANK = "H194";
    public static final String APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_BLANK = "H195";
    public static final String APPLICANT_EMPLOYMENT_DATE_OF_LEAVING_INVALID = "H196";
    public static final String APPLICANT_EMPLOYMENT_DATE_OF_JOINING_INVALID = "H197";

    public static final String APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_LIST_BLANK = "H198";
    public static final String APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_BLANK = "H199";
    public static final String APPLICANT_EMPLOYMENT_LAST_MONTH_NAME_INVALID = "H200";
    public static final String APPLICANT_EMPLOYMENT_LAST_MONTH_INCOME_BLANK = "H201";
    public static final String APPLICANT_APPLICATION_BLANK = "H202";
    public static final String APPLICANT_APPLICATION_LOAN_TYPE_INVALID = "H203";
    public static final String APPLICANT_APPLICATION_LOAN_TYPE_BLANK = "H204";
    public static final String APPLICANT_APPLICATION_LOAN_TYPE_BLANK_DESC = "Loan type should not be blank.";
    public static final String APPLICANT_APPLICATION_PRODUCT_ID_INVALID = "H203";
    public static final String APPLICANT_APPLICATION_PRODUCT_ID_BLANK = "H229";
    public static final String APPLICANT_APPLICATION_PRODUCT_ID_BLANK_DESC = "ProductID should not be blank.";
    public static final String APPLICANT_APPLICATION_LOAN_APPLIED_FOR_BLANK = "H205";
    public static final String APPLICANT_APPLICATION_LOAN_APPLIED_FOR_INVALID = "H206";
    public static final String APPLICANT_APPLICATION_LOAN_AMOUNT_BLANK = "H207";
    public static final String HEADER_APPLICATION_ID_BLANK = "H208";
    public static final String HEADER_APPLICATION_ID_INVALID = "H209";
    public static final String APPLICANT_APPLICATION_LOAN_TENOR_BLANK = "H210";
    public static final String APPLICANT_APPLICATION_PROPERTY_BLANK = "H211";
    public static final String REQUEST_SUSPICIOUS_ACTIVITY_INVALID = "H212";
    public static final String APPLICANT_MARITAL_STATUS_BLANK = "H213";
    public static final String APPLICANT_MARITAL_STATUS_INVALID = "H214";
    public static final String APPLICANT_EDUCATION_BLANK = "H215";
    public static final String APPLICANT_EDUCATION_INVALID = "H216";

    public static final String APPLICANT_MONTH_AT_CITY_BLANK = "H217";
    public static final String APPLICANT_RESIDENCE_ADDRESS_TYPE_BLANK = "H218";
    public static final String APPLICANT_RESIDENCE_ADDRESS_TYPE_INVALID = "H219";
    public static final String APPLICANT_EMPLOYMENT_TYPE_INVALID = "H220";
    public static final String APPLICANT_EMPLOYMENT_CONSTITUTION_BLANK = "H221";
    public static final String APPLICANT_EMPLOYMENT_CONSTITUTION_INVALID = "H222";
    public static final String APPLICANT_DRIVINGLICENSE_BLANK = "H223";
    public static final String APPLICANT_RATIONCARD_BLANK = "H224";
    public static final String APPLICANT_VOTERID_BLANK = "H225";
    public static final String APPLICANT_AADHAAR_BLANK = "H226";
    public static final String APPLICANT_PASSPORT_BLANK = "H227";
    public static final String APPLICANT_PAN_NUMBER_BLANK = "H228";


    public static final String INVALID_VERSION_CODE = "DVE01";
    public static final String INVALID_VERSION_DSCR = "You are using an unsupported version of the app, " +
            "please contact your administrator and upgrade to the latest version.";
    public static final String INVALID_PRODUCT_CODE = "DVE01";
    public static final String INVALID_PRODUCT_DSCR = "Product not configured in system.";
    public static final String INVALID_REQUEST_CODE = "DVE01";
    public static final String INVALID_REQUEST_DSCR = "Invalid Request";
    public static final String INVALID_REQUEST_PARAMS = "Invalid Request - institute, product, loantype not provided";

    public static final String DEALER_NOT_FOUND = "Dealer not found against the provided request, " +
            "please contact your administrator and update to the dealer details in system.";
    public static final String PROCESSING_FAILED = "Failed to process the request, please contact the product support team.";
    public static final String NO_DATA_FOUND = "No data found against provided request.";
    public static final String NO_DATA = "No data found against ";
    public static final String NO_OF_HITS_EXCEEDED = "No of hits for the api has exceeded than required";

    public static final String UNREGISTERED_PRODUCT = " product is not registered in application system, available  products are [%s]";

    public static final String PRODUCT_ALLOWED_FAILED = "You can not add new Product because," +
            " Limit exceeded to add new Product.";
    public static final String NON_AUTHORITATIVE_INFORMATION = "Not authorized to access this service.";
    public static final String CONFIGURATION_NOT_FOUND = "Service is not available for this institution.";
    public static final String EXTERNAL_SERVICE_FAILURE = "Third Party service failure.";

    public static final String EXP_AADHAAR_XML = "Exception while creating aadhaar XML.";
    public static final String TC_LOS_CONFIGURATION_NT_FOUND="TC LOS SAP configuration not found";
    public final static String INVALID_VENDOR = "Please Enter Valid Vendor.";

    public static final String ASSET_CATEGORY_FAILED = "Same Asset Category not allowed with other manufacturer.";

    public static final String SERIAL_NUMBER_DEDUP = "Serial/IMEI Number Already Exist in Database.";
    public static final String INVALID_STORE_CODE = "Store code should not be blank.";
    public static final String INVALID_SALE_DATE = "Sale date should not be blank.";
    public static final String DATE_FORMAT_NOT_SUPPORTED = "Please enter valid sale date.";
    public static final String INVALID_SALE_TYPE = "Sale type should not be empty and must not contain special character.";
    public static final String INVALID_PARTNER_CODE = "Partner code should not be empty and must not contain special character.";
    public static final String INVALID_PRODUCT_TYPE = "Product type should not be blank and must not contain special character.";
    public static final String INVALID_MATERIAL_NAME = "Material name should not be blank.";
    public static final String INVALID_IMEI_NUMBER = "IMEI number should not be blank.";
    public static final String INVALID_SERIAL_NUMBER_PRODUCT_CODE = "Product code should not be blank and must be 8 digits.";
    public static final String INVALID_SERIAL_NUMBER = "Serial number should not be blank and must be %s digits.";
    public static final String LENGHT_SERIAL_INBETWEEN = "Serial number should be min %s and max %s digits.";
    public static final String INVALID_CHANNEL_CODE = "Please enter valid channel code";
    public static final String INVALID_IMEI_LENGTH = "IMEI number must be %s digits.";
    public static final String BLANK_RDS_CODE = "RDS code should not be blank.";
    public static final String IMEI_NOT_GREATER = "IMEI number should not greater than %s digits.";
    public static final String BLANK_RETAILER_CODE = "Retailer code should not be blank.";
    public static final String BLANK_SERVICE_REQUEST_NUMBER = "Service Request Number should not be blank.";
    public static final String SERIAL_NOT_GREATER = "Serial number should not be greater than %s digits.";

    public static final String APPLICATION_NOT_FOUND = "Application Information Not Found Against ReferenceId.";
    public static final String LOS_ID_NOT_MATCH = "Rejected - LOS id mismatch, id present in the system does not match with the one provided in the upload request.";
    public static final String NOT_VALID_APPLICATION_STAGE_FOUND = "Rejected - Not a valid application stage or status.";
    public static final String NET_DISBURSAL_NOT_MATCH = "Rejected - Net disbursal amount does not match.";
    public static final String POST_IPA_DETAILS_NOT_FOUND ="post-ipa details not found";
    public static final String NET_DISBURSAL_AMT_ALREADY_UPDATED = "net desbursal amount already updated";

    public static final String SUCCESSFULLY_RECORD_INSERTED = "Record successfully inserted.";
    public static final String PROBLEM_TO_INSERT_RECORD = "Problem occur during record insert.";
    public static final String PROBLEM_TO_UPDATE_RECORD = "Problem occur during record update.";

    public static final String BLANK_SERIAL_NUMBER = "Serial number should not be blank.";
    public static final String INVALID_SKUCODE = "Skucode should not be blank.";

    public static final String INKVOKE_SERVICE_ERROR = "Service invoked from unexpected stage. ";

    public static final String SERIAL_NUMBER_BLANK_AND_NOT_VALID_FOR_INTEX = "Serial number should not be blank and not more than 30 characters.";
    public static final String LOSID_UTRNO_ALREADY_EXIST = "Rejected - LOS id and UTR number already exists against the reference id.";
    public static final String BLANK_BANK_NAME = "Bank name should not be blank.";
    public static final String BLANK_MPN = "MPN should not be blank.";

    public static final String BLANK_STORE_APPLE_ID = "Apple store id should not be blank.";
    public static final String BLANK_TENURE = "Tenure should not be blank.";
    public static final String INVALID_IMEI_NUMBER_OR_SERIAL_NUMBER = "Provide either Imei Number or serial Number.";
    public static final String INVALID_PRODUCT = "Invalid Product.";
    public static final String IMEI_NUMBER_EMPTY_OR_INVALID = "IMEI number should not be blank and must be valid.";
    public static final String SERIAL_NUMBER_EMPTY_OR_INVALID = "Serial number should not be blank and must be valid.";
    public static final String UNABLE_TO_CHANGE_APPLICATION_STATUS = "Unable to change application status";
    public static final String BLANK_SCHEME = "Scheme should not be blank.";
    public static final String INVALID_DEALER_ID = "Dealer Name should not be blank.";
    public static final String DEALER_MISMATCH = "Dealer Name mismatch, Case punch by dealer id [%s]" +
            " and provided request has different dealer id [%s],Please try again ...";
    public static final String DATA_SAVE_MESSAGE = "Data Successfully Saved";
    public static final String DATA_UPDATE_MESSAGE = "Data Successfully Updated";

    public static final String DATA_DELETE_MESSAGE = "Data Successfully Deleted";
    public static final String FAILED_MESSAGE = "Problem Occured During Data Save";
    public static final String BLANK_UTR_NUMBER = "UTR Number should not be blank.";
    public static final String BLANK_UTR_NUMBER_OR_DEALER_REFERENCE = "UTR Number/Dealer Reference Number,at least one field is compulsory.";
    public static final String INVALID_DEALER_TIEUP_FLAG = "Invalid DealerTieUpFlag ,possible values are (YES,NO)";
    public static final String OPS_ERROR_MESSAGE = "Application does not exist against provided refId and losId.";
    public static final String POST_IPA_NOT_EXIST = "Invalid reference id or Declined application.";
    public static final String LOYALTY_CARD_DEDUPE_MSG = "Loyalty Card Number Already Found in Database";
    public static final String GST_NOT_NECESSARY = "Don't send Gst details ,if you are sending gstCapture flag NO.";

    public static final String GST_IS_MANDATORY = "Gst details object should not be empty.";
    public static final String BLANK_OR_INVALID_GST_EMAIL = "Email Id should not be blank or invalid.";
    public static final String BLANK_GST_NUMBER = "GST Number should not be blank or invalid.";
    public static final String NULL_GST_ADDRESS = "GST Address should not be empty.";
    public static final String BLANK_ADDRESS_LINE1 = "Address line 1 should not be blank.";
    public static final String BLANK_ADDRESS_LINE2 = "Address line 2 should not be blank.";
    public static final String BLANK_ADDRESS_CITY = "City should not be blank.";
    public static final String BLANK_ADDRESS_STATE = "State should not be blank.";
    public static final String BLANK_ADDRESS_COUNTRY = "Country should not be blank.";
    public static final String BLANK_TO_DATE_OR_FROM_DATE = "From Date or To Date should not be blank.";
    public static final String WRONG_RANGE_BETWEEN_TO_DATE_AND_FROM_DATE = "Please enter the To Date greater than the From Date.";
    public static final String INVALID_TO_DATE = "To Date Should be greater than GST Start Date (01-07-2017).";
    public static final String INVALID_FROM_DATE = "From Date Should be greater than GST Start Date (01-07-2017).";
    public static final String EXTERNAL_SERVICE_NOT_REACHABLE = "Remote Service not reachable.";
    public static final String INVALID_MODEL_CODE = "Model Code must be valid length and should not be blank.";
    public static final String BLANK_MODEL_CODE = "Model code should not be blank";
    public static final String IMAGE_NOT_FOUND="Image not found in the database/Base64 code missing.";
    public static final String EMPTY_RESPONSE_FROM_DMS ="Empty response from DMS.";
    public static final String FOLDER_NOT_FOUND = "Folder id not found against folder name.";
    public static final String ADDITIONAL_DOC_CONFIG_NOT_FOUNT ="Additional doc information configuration not found or gonogo customer application not found";
    public static final String SOME_USER_ALREADY_LOGIN = "unable to create cabinet connection with dms because of some user already login";
    public static final String PROBLEM_WITH_CABINET_CONNECTION = "Problem occur at the time creating cabinet connection with DMS";
    public static final String LOS_INFO_NOT_FOUND = "Unable to push documents to dms,los id not generated for this application";
    public static final String DMS_EXCEPTION_MESSAGE = "Exception occur at the time of pushing dms data with probable cause :";
    public static final String DOCUMENTS_NOT_FOUND = "Documents not found for this application to push DMS";
    public static final String DOCUMENTS_NOT_FOUND_FOR_ESIGN = "Documents not found for this application to esign with e-emudra";
    public static final String DETAILS_NOT_FOUND_REF_ID = "Details not found for this ref id";

    public static final String BLANK_BRANCH_CODE = "Branch code should not be blank.";
    public static final String NO_STATE_DETAILS_FOUND = "No State Details/StateCode found for Branch %s for Ref Id: %s .";

    public static final String BLANK_RESPONSE_FROM_DMZ = "Blank response from DmzConnector";
    public static final String CIBIL_REPORT_NOT_FOUND = "Cibil Image Not Found in database because of Cibil error..";

    public static final String LENGTH_IMEI_NUMBER = "Imei Number should be %s digit.";
    public static final String INVALID_SERIAL_LENGTH = "Serial number length should be %s";
    public static final String FILE_UPLOAD_EXCEPTION = "Exception in fileUpload.";
    public static final String DMS_PUSH_DOC_ACTION_NOT_FOUND = "Dms push document action not found";
    public static final String SCHEMEMASTERDATA_NOT_FOUND = "SchemeMasterData not found.";
    public static final String APPLICATION_ALREADY_DISBURSED = "Application is already disbursed.";

    public static final String NO_DATA_FOUND_CODE = "NO_CONTENT";
    public static final String INVALID_DO_NUMBER = "DO Number should not be blank.";
    public static final String INVALID_NAME = "Customer name should not be blank.";
    public static final String INVALID_MAKE = "Product brand should not be blank.";
    public static final String INVALID_PRODUCT_COST = "Product cost should not be blank.";
    public static final String INVALID_NET_LOAN_AMOUNT = "Net loan amount should not be blank.";
    public static final String INVALID_DISBURSEMENT_AMOUNT = "Disbursement amount should not be blank.";
    public static final String INVALID_PROCESSING_FEES = "Processing fees should not be blank.";
    public static final String INVALID_DEALER_SUBVENTION = "Dealer subvention should not be blank.";
    public static final String INVALID_INTERFACE = "Invalid dealer.";
    public static final String DISB_UPDATE_DATA_FAILED = "Disbursement data updation failed for RefId %s and Institute %s.";
    public static final String GENERATE_REF_NO_ERROR= "Error in generating reference number.";
    public static final String DEALER_ID_NOT_MATCH = "Submitted coupon code not applicable for selected dealer";

    public static final String PERSONAL_MOBILE_UNAVAILABLE = "Personal mobile details unavailable. SMS not sent.";
    public static final String ACTION_NOT_ENABLED = "Action %s not enabled.";
    public static final String DO_CANCELLATION_FAIL ="Problem occur at the time of cancelling DO";
    public static final String DO_CANCELLATION_SUCCESS ="DO has been successfully cancelled";
    public static final String DO_ALREADY_SUBMITTED_TO_DEALER ="DO already submitted to dealer";
    public static final String DO_ALREADY_CANCELLED ="DO already Cancelled";
    public static final String DO_NOT_CREATED_FOR_THIS_APP="Unable to cancel DO, DO is not created for this application";
    public static final String DO_RESTORATION_FAIL="Problem occur at the time of restoring DO";
    public static final String DO_RESTORATION_SUCCESS ="DO has been successfully restored";
    public static final String CAN_NOT_RESTORE_DO ="Cannot restore DO, Delivery order status is %s ,previous status should be cancelled.";
    public static final String DO_ALREADY_RESTORED ="DO already restored";
    public static final String DO_STATUS_FOUND = "DO status has been successfully found";
    public static final String ERROR_UPLOADING_TAXCODES_MASTER = "Error in uploading tax code master csv.";
    public static final String ERROR_UPLOADING_ACCHEAD_MASTER = "Error in uploading Accounts Header master csv.";
    public static final String ERROR_UPLOADING_STATEBRANCH_MASTER = "Error in uploading State Branch Header master csv.";
    public static final String ERROR_UPLOADING_TCLOSGNGMAPPING = "Error in uploading TCLosGngMappings csv.";


    public static final String ERROR_UPLOADING_STATE_MASTER = "Error in uploading State master csv.";

    public static final String ERROR_SENDING_SMS = "Error in sending SMS. One of the required field is Null.";
    public static final String ERROR_SENDING_SMS_FIELD_VALUES = "Field Values: SMS Message: [%s] , Model No: [%s], EMI Start Date: [%s], EMI End Date: [%s]";
    public static final String ERROR_SENDING_SMS_TEMPLATE_NT_FOUND = "Error in sending SMS. smsTemplateConfiguration Not Found.";
    public static final String ERROR_SENDING_SMS_POSTIPA_DTLS_NTFND = "Error in sending SMS. PostIPA details Not Found.";
    public static final String ERROR_SENDING_SMS_APP_NTFND = "Error in sending SMS. Application not found for refId [%s]";
    public static final String ERROR_DATA_RETRIEVAL = "Error in retrieving data for refId [%s]. The probable cause is : [%s]";
    public static final String ERR_MSG_NT_CONFIGURED = "Error in sending SMS. Message template not configured.";
    public static final String ERROR_LTV_VALUE_NTFND = "LTV value not set by SOBRE/CRO for Application.";
    public static final String ERROR_EW_DETAILS_NTFND = "Extended Waarranty Details not found for refId [%s]";


    public static final String ERROR_SAP_FILE_NOTGENERATED = "SAP file not generated.";

    public static final String ONLY_IMEI_NUMBER_REQUIRED = "Only IMEI Number is applicable for this product.";
    public static final String INVALID_RETAILER_CODE = "Retailer code should not be blank.";
    public static final String LOS_CONNECT_ERROR = "Unable to get response from LOS service";
    public static final String AUTO_DEDUPE_CANCEL_DESCRIPTION = "Auto cancelled because found dedupe of application with refId %s, and DO was not generated.";
    public static final String BATCH_CANCEL_AFTER_X_DAYS_DESCRIPTION = "Batch Cancelled because application was approved or OnHold or Queued before  %s number of days and DO was not generated";

    public static final String ERROR_SCHEME_NTFND = "Scheme data not found with maxLTV < [%s]";
    public static final String ERR_INIT_POST_IPA = "Error in initializing the post IPA screen.";
    public static final String ERR_APP_NTFND_SCORES_NTFND = "Application not found/Scoring LTV/CRO LTV not found for this application.";

    public static final String INVALID_FILE_NAME ="Invalid file name provided ,it should be APPLICATION_FORM or AGREEMENT_FORM";

    public static final String LENGTH_MODEL_INBETWEEN = "Model number should be min %s and max %s digits.";

    public static final String DEALER_CODE_EMPTY = "Dealer Code should not be blank.";

    public static final String CHANNEL_CODE_EMPTY = "Channel Code should not be blank.";

    public static final String MODEL_NOT_GREATER = "Model Code should not greater than %s digits.";

    //SAP File generation errors.
    public static final String MASTER_RETRIEVE_ERROR = "Error in retrieving pre-requisite data for RefId %s . Probable Cause: %s .";
    public static final String NO_RECORDS_ERR = "No records found. CSV not written for refID : %s ";
    public static final String CSV_FILE_CREATION_ERR = "Error while creating CSV. CSV not written for refID : %s . Probable Cause: %s ";

    public static final String APP_NT_FND = "Application not found for refId %s .";
    public static final String TAXCODE_MASTER_NFND = "TaxCode Master entry not found for State: %s .";
    public static final String STATE_BRANCH_MASTER_NFND = "StateBranchMaster entry not found for branch with branch code %s & Institute %s.";
    public static final String ACCHEADER_NFND = "ACCOUNTS_HEAD_MASTER entry not found for field %s & Institute %s. ";
    public static final String MAPPING_MASTER_EMPTY = "TCLOS_GNG_MAPPING_MASTER is empty.";
    public static final String MFR_MASTER_NFND = "MANUFACTURER_MASTER entry not found for manufacturer %s & institution %s .";
    public static final String ASSET_DTL_NFND = "Asset details not found.";
    public static final String POSTIPA_NFND = "postIPA is empty for RefId : %s";
    public static final String DEALERMASTER_NFND = "DealerEmailMaster not found for institution %s and dealer %s .";
    public static final String EXCEPTION_CALLING_API = "Exception occurred while fetching TC LOS SAP response from connector with probable cause [{%s}]";

    //Vendor errors
    public static final String UNABLE_TO_GET_RESPONSE = "Not getting response from %s.";

    public static final String INVALID_APPLICANT_TYPE="Invalid applicant type";
    public static final String INVALID_IMEI="Invalid IMEI number";
    public static final String VALID_IMEI="Valid IMEI number";

    public static final String BLANK_MAKE_CODE = "Make should not be blank";
    public static final String MAKE_NOT_GREATER = "Make should not be greater than %s digits.";



    //
    public static final String SMSSERV_REQRESLOG_ERROR="Error occurred while fetching Request Response Log with probable cause %s .";
    public static final String TC_LOS_FAILED="Failed to get TC LOS SAP response.";
    public static final String KYC_SUCCESS_RESPONSE="eKYC response retrieved successfully";
    public static final String UIDAI_SUCCESS_RESPONSE="UIDAI response retrieved successfully";
    public static final String GNG_TRANSACTION_FAILURE="GNG transaction failure";
    public static final String KYC_TRANSACTION_FAILURE="eKYC transaction failure";
    public static final String UIDAI_TRANSACTION_FAILURE="UIDAI transaction failure";
    public static final String GNG_SUCCESS_RESPONSE="GNG Success";
    public static final String GNG_ERROR_RESPONSE="GNG Error";
    public static final String NOT_AVAILABLE="Not available";
    public static final String KYC_RESPONSE="eKYC response not available.";
    public static final String UIDAI_RESPONSE="UIDAI response not available.";
    public static final String EMPTY_POSTDATA="Data %s in post request URL %s is null.";
    public static final String EXCEPTION_ERROR="Exception caught while sending post request to url %s with data %s with probable cause %s";
    public static final String STATUS_CODE="Received HttpStatusCode %s after sending request with post data %s to url [%s]";




    public static final String NO_KYC_RESPONSE="No response from kyc.";

    //geo limit related error code

    public static final String GEOLMT_NT_APPICBL_APLCNT_TPE="GeoLimit is not application for applicantType %s";
    public static final String GEOLMT_NT_APPICBL_ADDR_TPE="GeoLimit is not application for addressType %s";
    public static final String PIN_NT_FND_IN_GEO_LMT="Please enter the pin code within Geo Limit";
    public static final String PIN_FND_IN_GEO_LMT ="The pin code successfully found within Geo Limit";
    public static final String SOMETHING_WENT_WRG="Something went wrong .Please try again later...";
    public static final String BANK_REPAYMENT_TYPE_NOT_ELIGIBLE="Either the BANK or REPAYMENT TYPE selected is NOT eligible for EMANDATE , please fill the physical ACH form";

    //IMPS ERROR CODES
    public static final String BLANK_ACC_NO = "Account number should not be blank";
    public static final String BLANK_IFSC = "IFSC code should not be blank";
    public static final String BLANK_HOLDER_NAME = "Account holder name should not be blank";
    public static final String IMPS_CONFIGURATION_NOT_FOUND = "IMPS configuration is not available for this institution";
    public final static String ATTEMPT_OVER = "Your account number validation attempts are over";
    public final static String ACCOUNT_NO_DEDUPE = "This account details already exist";
    public final static String INVALID_ACCOUNT_DETAILS = "Please enter other bank account details";
    public final static String INVALID_BANK_DETAILS = "Bank name not in the list";
    public final static String ACC_VALID_NAME_INVALID = "Account number valid but name mismatch. ";
    public final static String ACC_NO_APPLICABLE = "Account number validation is applicable for this institute";
    public final static String ACC_NO_NOT_APPLICABLE = "Account number validation is not applicable for this institute";


    //Service Code Registration Error Code
    public static final String VALID_SERVICE_REQUEST_NUM="Valid service request number";
    public static final String INVALID_SERVICE_REQUEST_NUM="Invalid service request number";


    //service code related to the modify application status and stage.

    public static final String PROBLEM_OCCUR_DURING_STAGE_UPDATE = "Problem occur during update application stage and status";
    public static final String APP_STAGE_SUCCESSFULLY_UPDATE = "Application stage and status successfully updated";
    public static final String ONLY_PDDE_STAGE_ALLOWED_TO_RESET_DO = "Only PD_DE stage application are allowed to reset delivery order";
    public static final String APPLICATION_STAGE_AND_STATUS = "The application status is %s and stage is %s";

    //los tvs related
    public static final String FAILED_LOSTVS_PULLANDTRANSFORM = "los push failed.";

    //SBFC-LMS
    public static final String BLANK_RESPONSE = "Blank response from LMS %s.";

    //Mifin related
    public static final String NO_RESPONSE_FROM_MIFIN = "Blank response from Mifin";
    public static final String NO_PAN_DATA_FOUND = "Customer Data not found against PAN CARD in MIFIN";

    /**
     * Admin module related Error Codes
     */
    public static final String CALCULATION_CONFIG_NOT_FOUND = "Calculation configuration not found for this institution or product";
    public static final String DROPDOWN_MASTER_NOT_FOUND = "Dropdown master not found";

    public static final String MASTER_UPLOAD_RESTRICTION="%s Master uploading is not allowed between %s to %s";
    public static final String MASTER_UPLOAD_RESTRICTION_BYPASS="%s Master uploading is allowed between %s and %s";
    public static final String MASTER_CONFIGURATION_NOT_FOUND = "master restriction configuration not found";
    public static final String MASTER_CONFIG_NOT_FOUND="Master Configuration not found against provided institutionId:%s";
    public static final String MASTER_CONFIG_RESTRICTION = "Validation failed kindly check institutionId:%s or MasterName:%s or FromAndToTime validation";

    public static final String MASTER_CONFIG_NOT_UPDATED = "Bypass master configuration is not updated in database";
    public static final String EXTERNAL_CALL_FAILURE = "Exception Occurs While Third Party Call.";

    //email and sms otp service related constants
    public static final String EMAIL_OTP_LIMIT_EXCEEDED = "OTP generation limit for the specified Email-Id has been exceeded.";
    public static final String LIMIT_EXCEEDED = "OTP generation limit for the specified mobile number has been exceeded.";
    public static final String OTP_VERIFICATION_LIMIT_EXCEEDED = "You have already exceeded your maximum OTP verification limit.";

    //OSOL constants
    public static final String ERROR_UPLOADING_MASTER = "Error occured while uploading %s master";
    public static final String CSV_FILE_VALID_ERROR_MSG="Kindly upload CSV format file";
    public static final String WRONG_FILE_CONTENT = "%s File content is not valid : %s column not found";
    public static final String MST_CONFIG_NOT_FOUND = "Master mapping not found for %s master. Please contact development team";
    public static final String REQ_VALIDATION_FAILED = "Request Validation Failed";
    public static final String  DB_OPERATION_FAILED="Database operation failure";
    public static final String MISSING_MANDATORY_HEADERS = "InstitutionId or sourceId is missing from request headers.";

    public static final String INVALID_OTP = "invalid OTP";

}

