package com.softcell.constants;

import com.softcell.gonogo.exceptions.SystemException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * @author kishorp
 */
public enum Product {

    DEFAULT("DEFAULT", "00", "DEFAULT"),
    CDL("Consumer Durables", "01", "CONSUMER DURABLES"),
    PL("PERSONAL LOAN", "02","PL"),
    CCBT("CCBT", "03", "CCBT"),
    AL("AUTO_LOAN", "04","AUTO LOAN"),
    BL("BL", "05","BUSINESS LOAN"),
    LAP("LAP", "06" , "LOAN AGAINST PROPERTY"),
    LAS("LOAN_AGAINST_SHARES", "07","LOAN AGAINST SHARES"),
    GL("GOLD_LOAN", "08", "GOLD LOAN"),
    GOLD_LOAN("GOLD_LOAN", "08", "GOLD LOAN"),
    CEL("CONSTRUCTION_EQUIPMENT_LOAN", "09","CONSTRUCTION EQUIPMENT LOAN"),
    HL("HOUSING LOAN", "10", "HOUSING LOAN"),
    DPL("DIGITAL_PRODUCT_LOAN", "11", "DIGITAL PRODUCT"),
    CV("COMMERCIAL_VEHICLES","12","COMMERCIAL VEHICLES"),
    DIGI_PL("DIGI_PL","99","DIGI_PL");


    private final String productName;
    private final String productId;
    private final String displayName;


    static final private Map<String,String > ID_MAP = new HashMap<>();

    static {


        for(Product product :  Product.values()){
            ID_MAP.put(product.name(),product.toProductId());
        }
    }

    Product(final String productName, final String productId,final String displayName) {
        this.productName = productName;
        this.productId = productId;
        this.displayName = displayName;
    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toProductName() {return productName;}

    public String toProductId() {
        return productId;
    }

    public String toDisplayName(){ return displayName;}

    public static String getProductIdFromName(String productName){

        if(ID_MAP.containsKey(productName)){
            return ID_MAP.get(productName);
        }else{
            throw new SystemException(" productName not available in system ").set("AVAILABLE_PRODUCT",Product.values());
        }

    }


    public static String getProductNameFromEnumName(String enumName){

        return Stream.of(values())
                .filter(product -> StringUtils.equalsIgnoreCase(product.name(), enumName))
                .map(product -> product.toProductName()).findFirst().orElse("");

    }

    public static Product getProductFromName(String productName){

        List<Product> products = Stream.of(Product.values())
                .filter(product -> StringUtils.equalsIgnoreCase(product.name(), productName)).collect(toList());
        // Check the size
        if(CollectionUtils.isNotEmpty(products)){
            return products.get(0);
        }

        return null;

    }
}
