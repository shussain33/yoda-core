package com.softcell.constants;

/**
 * @author kishorp
 */
public enum LogoType {
    OTP("OTP");

    private String type;

    LogoType(String type){
        this.type=type;
    }
}
