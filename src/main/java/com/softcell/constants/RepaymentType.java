package com.softcell.constants;

/**
 * Created by abhishek on 12/12/17.
 */
public enum RepaymentType {
    ACH,ECS,ADM
}
