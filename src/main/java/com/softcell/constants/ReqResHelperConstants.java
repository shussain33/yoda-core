package com.softcell.constants;

/**
 * Created by yogeshb on 17/2/17.
 */
public enum ReqResHelperConstants {

    /**
     * KYC-PAN
     */
    REQUEST, ISSUE,

    /**
     * Mb
     */
    INDV,
    GONOGO,
    GoNoGoCore,
    LOW_PRIORITY,

    /**
     * Aadhaar
     */
    request,
    NA,
    NC,
    P,
    y,
    n,
    N,
    Y,

    auth,
    kyc,
    Male,
    Female,
    M,
    F,
    IIR,
    FMR,

    /**
     * Auth
     */
    W,
    GNG,

    /**
     * SalesForce
     */
    Yes,
    No,

    /**
     * sony
     */
    Approved,

    /**
     * Posidex Actions and other constants
     */
    doenquiry,
    getenquirystatus,
    I,
    U,
    BORROWER,

    /**
    EMudra Status
     */
    Success,
    failure
}
