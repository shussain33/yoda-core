package com.softcell.constants;

/**
 * Created by mahesh on 29/7/17.
 */
public enum  DmsEnum {
    Doc_Related,Stages,Product,Stages_Sourcing,Stages_Underwriting,Stages_Disbursal,LOS_ID,
    Application_No ,Cust_ID,Vehicle_Registration_Number,UnderWriting_Disbursal,Cheque_Number_From,
    Cheque_Number_To,DO_Number,Source_Under_Disbursal,Pan_Card_Number,Aadhar_Card_Number,Driving_Licence_Number,
    S,L,APPLICANT,CD;
}
