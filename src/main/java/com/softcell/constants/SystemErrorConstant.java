package com.softcell.constants;

/**
 * Created by bhuvneshk on 28/2/17.
 *
 * This will be used to store system error constant
 */
public class SystemErrorConstant {

    public static final String INTERNAl_SERVER_ERROR = "Internal Server Error";
    public static final String WRONG_CSV = "Wrong csv file uploaded, please upload valid csv file";
    public static final String RUNTIME_EXCEPTION = "Run time Exception ";
    public static final String FORMAT_NOT_SUPPORTED = "File Format not supported";
    public static final String CURRUPT_RECORD = "Corrupt or blank record at index";
}
