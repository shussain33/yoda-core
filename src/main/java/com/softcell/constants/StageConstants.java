package com.softcell.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * It contains gonogo stage constant definition
 *
 * @author bhuvneshk
 */
public class StageConstants {

    public static Map<String, String> mapConstant = new HashMap<String, String>();

    static {
        mapConstant.put("APRV", "Approved");
        mapConstant.put("PD_DE", "Post disbursement data entry");
        mapConstant.put("DCLN", "Declined");
        mapConstant.put("CR_Q", "CRO QUEUE");
        mapConstant.put("BRE", "Rule Engine");
        mapConstant.put("DE", "Application Entry");
        mapConstant.put("CR_H", "CRO HOLD");
        mapConstant.put("DO", "Disbursed");
        mapConstant.put("LOS_DCLN", "LOS Declined");
        mapConstant.put("LOS_DISB", "LOS Disbursed");
        mapConstant.put("LOS_APRV", "LOS Approved");
        mapConstant.put("LOS_QDE", "LOS Quick Data Entry");
        mapConstant.put("NegativeDedupe", " Dedupde Verification");
        mapConstant.put("multiBureauExecutor", "Credit Bureau Verification");
        mapConstant.put("panVarificationExecuter", "PAN Verification");
        mapConstant.put("scoringExecutor", "Score Executor");
        mapConstant.put("verificationScoring", "Score Verification");
        mapConstant.put("aadharExecutor", "Aadhar Verification");
        mapConstant.put("ntcExecutor", "NTC Verification");
        mapConstant.put("salesforceExecutor", "SalesForce Verification");
        mapConstant.put("amazonS3ServiceExecutor", "Amazon Service Verification");
    }
}
