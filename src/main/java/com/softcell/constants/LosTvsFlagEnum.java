package com.softcell.constants;

public enum LosTvsFlagEnum {

    TRUE,
    FALSE,
    Y,
    N,
    T,
    F,
    SUCCESS
}
