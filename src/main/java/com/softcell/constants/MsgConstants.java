package com.softcell.constants;

/**
 * Created by yogesh on 11/10/17.
 */
public class MsgConstants {

    public static final String EW_SMS_SUCCESS_MSG = "Extended Warranty Sms sent successfully";
    public static final String BUSINESS_ERROR = "Business error";

}
