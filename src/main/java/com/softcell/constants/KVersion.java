package com.softcell.constants;

/**
 * Created by abhishek on 9/1/18.
 */
public enum KVersion {

    KVERSION2("KVERSION2");

    private String version;

    KVersion(String version){
        this.version = version;
    }

    public String  toValue(){
        return this.version;
    }
}
