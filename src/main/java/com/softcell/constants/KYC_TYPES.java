package com.softcell.constants;

import org.apache.commons.lang3.StringUtils;

public enum KYC_TYPES {
    PAN {
        @Override
        public String toString() {
            return "PAN";
        }
    },
    AADHAR {
        @Override
        public String toString() {
            return "AADHAR";
        }
    },
    VOTER_ID {
        @Override
        public String toString() {
            return "VOTER-ID";
        }
    },
    DRIVING_LICENSE {
        @Override
        public String toString() {
            return "DRIVING-LICENSE";
        }
    },
    PASSPORT {
        @Override
        public String toString() {
            return "PASSPORT";
        }
    };

    public static boolean isKYCType(String documentName){
        boolean kycType = false;
        for(KYC_TYPES kyc : KYC_TYPES.values() ){
            if(StringUtils.equalsIgnoreCase(kyc.toString(), documentName) ||
                    StringUtils.contains( documentName, kyc.toString())  ){
                kycType = true;
                break;
            }
        }
        return kycType;
    }
}
