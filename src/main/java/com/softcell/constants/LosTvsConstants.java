package com.softcell.constants;

public class LosTvsConstants {
    public static final String AADHAR = "aadhaar";
    public static final String ddMMyyyy = "ddMMyyyy";
    public static final String AADHAR_ADDRESS = "aadhar_address";
    public static final String REQUEST_TYPE = "requestType";
    public static final String GONOGO_REPAYMENT_TYPE_ECS = "REPAYMENT_TYPE_ECS";
    public static final String TVS_REPAYMENT_TYPE_ECS = "ECS";

    public static final String GONOGO_REPAYMENT_TYPE_ADM = "REPAYMENT_TYPE_ADM";
    public static final String TVS_REPAYMENT_TYPE_ADM = "ADM";

    public static final String GONOGO_REPAYMENT_TYPE_ACH = "REPAYMENT_TYPE_ACH";
    public static final String TVS_REPAYMENT_TYPE_ACH = "ACH";
}
