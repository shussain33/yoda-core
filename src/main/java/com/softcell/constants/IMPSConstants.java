package com.softcell.constants;

/**
 * Created by sampat on 24/11/17.
 */
public class IMPSConstants {

    public static final String PNY_SUCCESS_CODE = "PNYERR000";
    public static final String PNY_DEDUP_CODE = "PNYERR013";
    public static final String PNY_TECHNICAL_ERROR = "PNYERR999";
}
