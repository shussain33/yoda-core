package com.softcell.constants;

/**
 * Created by yogesh on 27/4/18.
 */
public class EmploymentType {
    public static final String SEP = "Self-Employed Professional";
    public static final String SENP = "Self-Employed Non-Professional";
    public static final String SALARIED = "Salaried";

}
