package com.softcell.constants;

/**
 * Created by Softcell on 29/09/17.
 */
public enum TokenCode {

    DP("101"),
    DIS("102"),
    AGGR("103");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    TokenCode(String value) {
        this.value = value;
    }
}
