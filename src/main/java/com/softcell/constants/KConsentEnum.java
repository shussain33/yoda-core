package com.softcell.constants;

/**
 * Created by abhishek on 12/1/18.
 */
public enum KConsentEnum {
    y,
    n,
    PAN,
    GST,
    GSTAUTH,
    TAN,
    TANAUTH
}
