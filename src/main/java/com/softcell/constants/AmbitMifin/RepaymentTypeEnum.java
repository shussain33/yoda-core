package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

/**
 * Created by ssguser on 29/12/20.
 */
public enum RepaymentTypeEnum {

    CASH("Cash","CASH"),
    CHEQUE("Cheque","CHEQUE"),
    DD("DD","DEMAND DRAFT"),
    FUNDTRANSFER("Fund Transfer","RTGS"),
    NEFT("NEFT","NEFT"),
    RTGS("RTGS","RTGS"),
    IMPS("IMPS","CASH"),
    NACH("NACH","ACH");

    private String gonogoRepaymentType;
    private String mifinRepayemntType;

    public String getMifinRepayemntType() {
        return mifinRepayemntType;
    }
    public String getGonogoRepaymentType() {
        return gonogoRepaymentType;
    }

    RepaymentTypeEnum(String gonogoRepaymentType, String mifinRepayemntType) {
        this.gonogoRepaymentType = gonogoRepaymentType;
        this.mifinRepayemntType= mifinRepayemntType;
    }

    public static String getMifinRepayementTypeFromRepaymentType(String repaymentType){
        return Stream.of(values())
                .filter(repayment -> StringUtils.equalsIgnoreCase(repayment.getGonogoRepaymentType(), repaymentType))
                .map(repayment -> repayment.getMifinRepayemntType()).findFirst().orElse("CASH");

    }

}
