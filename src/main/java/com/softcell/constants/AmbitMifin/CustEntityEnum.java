package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

public enum CustEntityEnum {

    _1000000001("Individual"),
    _1000000002("NON-INDIVIDUAL");

    private String entityType;

    CustEntityEnum(String entityType){
        this.entityType=entityType;
    }

    public String getEntityType() {
        return entityType;
    }

    public static String getGonogoCustEntity(String entityType){

        return Stream.of(values())
                .filter(type -> StringUtils.equalsIgnoreCase(type.name().substring(1), entityType))
                .map(type -> type.getEntityType()).findFirst().orElse("");


    }

}
