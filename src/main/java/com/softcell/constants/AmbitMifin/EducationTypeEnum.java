package com.softcell.constants.AmbitMifin;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

/**
 * Created by ssguser on 21/1/21.
 */
public enum EducationTypeEnum {

    UNDERGRADUATE("UnderGraduate", "High School", "1000000011"),
    GRADUATE("Graduate", "Graduate", "1000000008"),
    POSTGRADUATE("PostGraduate", "Post-Graduate", "1000000006");


    private String GonogoEducationType;
    private String MifinEducationType;
    private String EducationCode;


    private String getGonogoEducationType() {
        return GonogoEducationType;
    }

    private String getMifinEducationType() {
        return MifinEducationType;
    }

    private String getEducationCode() { return EducationCode; }

    EducationTypeEnum(String gonogoEducationType, String MifinEducationType, String EducationCode) {
        this.GonogoEducationType = gonogoEducationType;
        this.MifinEducationType = MifinEducationType;
        this.EducationCode = EducationCode;
    }

    public static String getMifinEducationTypeFromGonogoType(String Enum) {

        return Stream.of(values())
                .filter(educationType -> StringUtils.equalsIgnoreCase(Enum, educationType.getGonogoEducationType()))
                .map(value -> value.getMifinEducationType()).findFirst().orElse("Graduate");
    }
    public static String getGonogoTypefromMifinCOde(String code){
        return Stream.of(values())
                .filter(educationType -> StringUtils.equalsIgnoreCase(code, educationType.getEducationCode()))
                .map(value -> value.getGonogoEducationType()).findFirst().orElse("");
    }

}
