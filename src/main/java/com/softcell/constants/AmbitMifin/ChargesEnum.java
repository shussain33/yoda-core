package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;
import java.util.stream.Stream;

public enum ChargesEnum {

    ADVEMI("Advance EMI","Advance EMI Charges","1000010119"),
    ADMIN("Admin charges","Admin Charges", "1000010171"),
    DIFFCHARGE("Differencial Interest receivables", "Differencial Interest Receivable", "1000010133"),
    LATEFEE("Late fee charge", "Late Payment Interest","1000000114"),
    LOGINCHARGE("Login Fees","Login Fees","1000002100"),
    PREEMI("PRE-EMI Intrest Amount","Advance EMI / Interest / Pre-EMI", "1000010132"),
    PROCESSINGFEE("Processing fees Amount","Processing Fee Pre-D","1000000111"),
    OTHERCHARGE("Other charges PRE-D","Other Charges-PRE D","1000010169"),
    ROC("ROC charges","ROC Charges","1000010122"),
    STAMPING("Stamping charges","Stamping charges","1000010121"),
    INSURANCEPREMIUM("Insurance Amount","Insurance Premium","1000000200"),
    DSRA("DSRA (RECEIVABLE)","DSRA (RECEIVABLE)","1000010106"),
    SLM("SUBVENTION CHARGE INTEREST SLM","SUBVENTION CHARGE INTEREST SLM","1000100027"),
    WDV("SUBVENTION CHARGE INTEREST WDV","SUBVENTION CHARGE INTEREST WDV","1000100031"),
    SLMGST("SUBVENTION CHARGE INTEREST SLM GST","SUBVENTION CHARGE INTEREST SLM GST","1000100035"),
    WDVGST("SUBVENTION CHARGE INTEREST WDV GST","SUBVENTION CHARGE INTEREST WDV GST","1000100039"),
    ADVEMIINTEREST("ADVANCE EMI / INTEREST / PRE-EMI", "ADVANCE EMI / INTEREST / PRE-EMI"," 1000010132");

    private String gonogoCharge;
    private String mifinCharge;
    private String chargeCode;

    public String getGonogoCharge() { return gonogoCharge; }

    public String getMifinCharge() { return mifinCharge;}

    public String getChargeCode(){ return chargeCode; }

    ChargesEnum(String gonogoCharge, String mifinCharge, String chargeCode) {
        this.gonogoCharge = gonogoCharge;
        this.mifinCharge = mifinCharge;
        this.chargeCode = chargeCode;;
    }

    public static String getMifinChargeFromGonogoCharge(String Enum){
        return Stream.of(values())
                .filter(chargeName -> StringUtils.equalsIgnoreCase(Enum, chargeName.getGonogoCharge()))
                .map(chargeName -> chargeName.getMifinCharge()).findFirst().orElse("");
    }
}
