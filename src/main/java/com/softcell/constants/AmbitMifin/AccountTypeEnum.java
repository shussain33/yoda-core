package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;
import java.util.stream.Stream;

public enum AccountTypeEnum {

    Cash("Cash Credit", "CASH CREDIT"),
    OverDraft("OverDraft", "OVER DRAFT"),
    Current("Current", "CURRENT ACCOUNT"),
    Saving("Saving", "SAVING ACCOUNT"),
    Loan("Loan Account", "LOAN ACCOUNT");

    private String repaymentAccountType;
    private String mifinAccountType;

    public String getMifinAccountType() {
        return mifinAccountType;
    }

    public String getRepaymentAccountType() {
        return repaymentAccountType;
    }

    AccountTypeEnum(String repaymentAccountType, String mifinAccountType) {
        this.repaymentAccountType = repaymentAccountType;
        this.mifinAccountType = mifinAccountType;
    }

    public static String getMifinAccountTypeFromRepaymentType(String repaymentAccountType) {

        return Stream.of(values())
                .filter(accountType -> StringUtils.equalsIgnoreCase(accountType.getRepaymentAccountType(), repaymentAccountType))
                .map(accountType -> accountType.getMifinAccountType()).findFirst().orElse("");

    }
}