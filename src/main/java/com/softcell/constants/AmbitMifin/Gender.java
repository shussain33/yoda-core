package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

public enum Gender {
    _1000000001("Male"),
    _1000000002("Female");

    private String gender;
    Gender(String gender)
    {
        this.gender=gender;
    }

    private String getGender(){return gender;}

    public static String getGenderfromCode(String genderCode){

        return Stream.of(values())
                .filter(type -> StringUtils.equalsIgnoreCase(type.name().substring(1), genderCode))
                .map(type -> type.getGender()).findFirst().orElse("MALE");


    }
}
