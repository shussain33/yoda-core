package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

public enum SocialCategoryEnum {
    _1000000001("SC"),
    _1000000002("ST"),
    _1000000003("OBC"),
    _1000000004("GENERAL"),
    _1000000005("OTHERS"),
    _1000000007("TeSTGN");

    private String socialCegory;
    SocialCategoryEnum(String socialCegory)
    {
        this.socialCegory=socialCegory;
    }

    private String getsocialCategory(){return socialCegory;}

    public static String getCatgoryfromCode(String code){

        return Stream.of(values())
                .filter(type -> StringUtils.equalsIgnoreCase(type.name().substring(1), code))
                .map(type -> type.getsocialCategory()).findFirst().orElse("");


    }

}
