package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

/**
 * Created by ssguser on 24/12/20.
 */

public enum AddressTypeEnum {

    RESIDENCE("RESIDENCE","RESIDENCE ADDRESS","1000000001"),
    OFFICE("OFFICE","OFFICE ADDRESS","1000000002"),
    GODOWN("GODOWN","COMMUNICATION ADDRESS","1000000009"),
    PERMANENT("PERMANENT", "PERMANENT ADDRESS","1000000003"),
    PARENTAL("FACTORY","REGISTERED OFFICE","1000000006");

    private String gonogoAddressType;
    private String mifinAddressType;
    private String addressCode;

    public String getGonogoAddressType() {
        return gonogoAddressType;
    }

    public String getMifinAddressType() {
        return mifinAddressType;
    }

    public String getAddressCode(){
        return addressCode;
    }

    AddressTypeEnum(String gonogoAddressType, String mifinAddressType,String addressCode) {
        this.gonogoAddressType = gonogoAddressType;
        this.mifinAddressType = mifinAddressType;
        this.addressCode=addressCode;
    }

    public static String getMifinAddressTypeFromGonogoAddressType(String Enum){

        return Stream.of(values())
                .filter(addressType -> StringUtils.equalsIgnoreCase(Enum, addressType.getGonogoAddressType()))
                .map(addressType -> addressType.getMifinAddressType()).findFirst().orElse("RESIDENCE");

    }

    public static String getAddressfromCode(String addressCode){

        return Stream.of(values())
                .filter(type -> StringUtils.equalsIgnoreCase(type.getAddressCode(), addressCode))
                .map(type -> type.getGonogoAddressType()).findFirst().orElse("");

    }
}
