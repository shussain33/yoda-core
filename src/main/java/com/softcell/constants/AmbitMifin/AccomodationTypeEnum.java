package com.softcell.constants.AmbitMifin;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import java.util.stream.Stream;

public enum AccomodationTypeEnum {

    OWNED("Owned","SELFOWNED","1000000001"),
    COPROVIDED("CoProvided","COMPANY LEASE","1000000004"),
    OTHER("Other","SELFOWNED","1000000001"),
    RENTED("Rented", "RENTED","1000000005"),
    PARENTAL("Parental","PARENTAL","1000000006");

    private String GonogoAccomodationType;
    private String MifinAccomodationType;
    private String AccomodationCode;

    public String getGonogoAccomodationType(){
        return GonogoAccomodationType;
    }
    public String getMifinAccomodationType(){
        return MifinAccomodationType;
    }
    public String getAccomodationCode(){return AccomodationCode;}

    AccomodationTypeEnum(String gonogoAccomodationType,String MifinAccomodationType,String AccomodationCode){
        this.GonogoAccomodationType=gonogoAccomodationType;
        this.MifinAccomodationType=MifinAccomodationType;
        this.AccomodationCode=AccomodationCode;
    }
    public static String getMifinAccomodationTypeFromGonogoType(String Enum){
        return Stream.of(values())
                .filter(accomodationType->StringUtils.equalsIgnoreCase(Enum,accomodationType.getGonogoAccomodationType()))
                .map(value ->value.getMifinAccomodationType()).findFirst().orElse("SELFOWNED");
    }

    public static String getAccomodationCodeFromGonogoType(String Enum){
        return Stream.of(values())
                .filter(accomodationType->StringUtils.equalsIgnoreCase(Enum,accomodationType.getAccomodationCode()))
                .map(value ->value.getGonogoAccomodationType()).findFirst().orElse("Owned");
    }
}
