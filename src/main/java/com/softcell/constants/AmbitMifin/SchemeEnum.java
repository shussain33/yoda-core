package com.softcell.constants.AmbitMifin;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import java.util.stream.Stream;

public enum SchemeEnum {

    INCOMEBASED("AMBIT Udyam-Income Based","1000000111"),
    DAYS100VARIANT("AMBIT UDYAM - 100 DAYS VARIANT","2000000065"),
    GSTBASED("AMBIT UDYAM-GST Udyam","2000000068"),
    COVIDLINETOCREDIT("Ambit Udyam-COVID Line of Credit","2000000098"),
    LIP("AMBIT UDYAM-LIP Assessment","2000000090"),
    ABB("AMBIT UDYAM-Average Bank Balance (ABB)","2000000076");

    private String gonogoScheme;
    private String mifinSchemeCode;

    public String getGonogoScheme(){
        return gonogoScheme;
    }

    public String getMifinSchemeCode(){
        return mifinSchemeCode;
    }

    SchemeEnum(String gonogoScheme,String mifinSchemeCode){
        this.gonogoScheme=gonogoScheme;
        this.mifinSchemeCode=mifinSchemeCode;
    }

    public static String getMifinSchemeCodeFromGonogoScheme(String Enum){
        return Stream.of(values())
                .filter(scheme -> StringUtils.equalsIgnoreCase(Enum,scheme.getGonogoScheme()))
                .map(value ->value.getMifinSchemeCode()).findFirst().orElse("");
    }

    public static String getGoNoGoSchemeFromMifinSchemeCode(String Enum){
        return Stream.of(values())
                .filter(schemeCode->StringUtils.equalsIgnoreCase(Enum, schemeCode.getMifinSchemeCode()))
                .map(value ->value.getGonogoScheme()).findFirst().orElse("");
    }

}
