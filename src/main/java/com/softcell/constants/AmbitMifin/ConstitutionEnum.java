package com.softcell.constants.AmbitMifin;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

public enum ConstitutionEnum
{
    SALARIED("Salaried","SALARIED","1000000020"),
    SEP("Self-Employed Professional","SEP","1000000018"),
    SENP("Self-Employed Non-Professional","SENP","1000000019"),
    NONWORKING("Not Working","NON WORKING","1000000021"),
    PROPRIETORSHIP("Proprietorship","PROPRIETARY","1000000013"),
    PARTNERSHIP("Partnership","PARTNERSHIP FIRM","1000000014"),
    PRIVATE("Pvt. Ltd.","PRIVATE LIMITED","1000000008"),
    PUBLIC("Public Ltd.","PUBLIC LIMITED","1000000009"),
    TRUST("Trust","TRUST / SOCIETY","1000000023");


    private String GonogoCustEntityType;
    private String MifinCustEntityType;
    private String MifinCode;


    private  String getGonogoCustEntityType(){
        return GonogoCustEntityType;
    }
    private String getMifinCustEntityType(){
        return MifinCustEntityType;
    }
    private String getMifinCode(){
        return MifinCode;
    }


    ConstitutionEnum(String GonogoCustEntityType, String MifinCustEntityType,String MifinCode){
        this.MifinCustEntityType=MifinCustEntityType;
        this.GonogoCustEntityType=GonogoCustEntityType;
        this.MifinCode=MifinCode;
    }
    public static String getMifinConstitutionFromGonogoType(String Enum){

        return Stream.of(values())
                .filter(accomodationType-> StringUtils.equalsIgnoreCase(Enum,accomodationType.getGonogoCustEntityType()))
                .map(value ->":"+value.getMifinCustEntityType()).findFirst().orElse("");

    }

    public static String getGonogoTypeFromMifincode(String Enum){

        return Stream.of(values())
                .filter(constitutionEnum-> StringUtils.equalsIgnoreCase(Enum,constitutionEnum.getMifinCode()))
                .map(value ->value.getGonogoCustEntityType()).findFirst().orElse("");

    }

}

