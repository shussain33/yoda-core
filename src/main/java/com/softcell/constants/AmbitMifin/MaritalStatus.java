package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

public enum MaritalStatus {

    _1000000001("Married"),
    _1000000002("Single"),
    _1000000008("Divorced"),
    _1000000009("Widow");
    private String maritalType;

    MaritalStatus(String maritalType ){
        this.maritalType=maritalType;
    }

    public String getMaritalType() {
        return maritalType;
    }

    public static String getGonogoMaritalType(String maritalTypetype){

        return Stream.of(values())
                .filter(type -> StringUtils.equalsIgnoreCase(type.name().substring(1), maritalTypetype))
                .map(type -> type.getMaritalType()).findFirst().orElse("");


    }
}
