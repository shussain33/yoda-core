package com.softcell.constants.AmbitMifin;

import org.apache.commons.lang.StringUtils;

import java.util.stream.Stream;

public enum ProductEnum {
    BL("AMBIT-Udyam"),
    LAP("AMBIT_VYAPAR");

    private String mifinProductName;

    public String getMifinProductName() {
        return mifinProductName;
    }

    ProductEnum(String mifinProductName) {
        this.mifinProductName = mifinProductName;
    }

    public static String getMifinProductNameFromGonogoName(String enumName){

        return Stream.of(values())
                .filter(product -> StringUtils.equalsIgnoreCase(product.name(), enumName))
                .map(product -> product.getMifinProductName()).findFirst().orElse("");


    }

}
