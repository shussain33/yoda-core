package com.softcell.constants;

/**
 * @author kishorp
 */
public enum ReportConstant {
    MTD, WTD, YTD
}
