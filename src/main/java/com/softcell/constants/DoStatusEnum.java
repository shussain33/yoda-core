package com.softcell.constants;

/**
 * Created by mahesh on 30/8/17.
 */
public enum DoStatusEnum {

    CANCELLED,CREATED,MAILED,RESTORED,NOT_CREATED ,MAIL_FAILED ,MAIL_IN_PROGRESS
}
