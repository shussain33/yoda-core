package com.softcell.constants.templates;

/**
 * @author yogeshb
 */
public interface EmailTemplateConstant {
    String PL_APPROVED_TEMPLATE = "approveCaseTemplate.vm";
    String PL_DECLINED_TEMPLATE = "declinedCaseTemplate.vm";
    String PL_PENDING_TEMPLATE = "pendingCaseTemplate.vm";

    String CCBT_APPROVED_TEMPLATE = "approveCaseTemplate.vm";
    String CCBT_DECLINED_TEMPLATE = "declinedCaseTemplate.vm";
    String CCBT_PENDING_TEMPLATE = "pendingCaseTemplate.vm";

    String PL_APPROVED_SUBJECT_LINE = "Thank You for Applying for HDBFS ZipLoan. Loan Application Number is ";
    String PL_DECLINED_SUBJECT_LINE = "Thank You for Applying for HDBFS ZipLoan. ";
    String PL_PENDING_SUBJECT_LINE = "Thank You for Applying for HDBFS ZipLoan. Loan Application Number is ";

    String CCBT_APPROVED_SUBJECT_LINE = "Thank You for Applying for HDBFS CCBT. Loan Application Number is ";
    String CCBT_DECLINED_SUBJECT_LINE = "Thank You for Applying for HDBFS CCBT. ";
    String CCBT_PENDING_SUBJECT_LINE = "Thank You for Applying for HDBFS CCBT. Loan Application Number is ";
}
