package com.softcell.constants;

/**
 * Created by sampat on 6/1/18.
 */
public enum IMPSStatus {

    VALID,INVALID,NOT_SUPPORTED,IMPS_ERROR,ATTEMPT_OVER,DEDUPE
}
