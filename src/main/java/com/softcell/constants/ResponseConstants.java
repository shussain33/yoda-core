/**
 * kishorp9:14:26 PM  Copyright Softcell Technolgy
 **/
package com.softcell.constants;

/**
 * @author kishorp
 */
public class ResponseConstants {
    public final static String RESIDENTIAL_ADDRESS = "101";
    public final static String OFFICE_ADDRESS = "102";
    public final static String MATCH = "Match";
    public final static String NOT_MATCH = "Not Match";
    public final static String INVALID_RESPONSE_CODE = "Invalid response code";

    /**
     * For intex serial number response code.
     */
    public final static String INTEX_CODE_ZERO = "Invalid LoginId";
    public final static String INTEX_CODE_ONE = "Serial Number already Sold";
    public final static String INTEX_CODE_TWO = "Serial Number is Not Valid";
    public final static String INTEX_CODE_THREE = "Serial Number Approved Successfully";

    /**
     * Is serial number applicable messages
     */
    public final static String APPLICABLE_VANDOR = "Serial Number is Applicable";
    public final static String NOT_APPLICABLE_VANDOR = "Serial Number is not Applicable";

    public static final String SERIAL_BLOCK_SUCCESS = "Serial Block Successfully";
    public static final String SERIAL_ALREADY__BLOCK = "Serial Already Block";
    public static final String SERIAL_NUMBER_EXIST = "Serial Number Exists";
    public static final String SERIAL_NUMBER_NOT_EXIST = "Serial Number does not Exists";
    public static final String NO_RESPONSE = "No Response";

    /**
     * For samsung imei number response code.
     */
    public static final String SAMSUNG_CODE_ZERO = "Correct IMEI for EMI Process";

    public static final String SAMSUNG_CODE_MINUS_ONE = "Invalid User Name or Password";

    public static final String SAMSUNG_CODE_ONE = "Invalid Product Code/SKU code, that is not configured in the master";

    public static final String SAMSUNG_CODE_TWO = "Already Sold that is EMI already done";

    public static final String SAMSUNG_CODE_THREE = "Invalid IMEI, that is not a Samsung India valid IMEI Or Demo IMEI";

    public static final String SAMSUNG_CODE_FOUR = "EMI can be offer but rejected due to Invalid Channel";

    public static final String SAMSUNG_CODE_FIVE = "If user is blocked";

    public static final String SAMSUNG_CODE_SIX = "Invalid date time or invalid status code";

    /**
     * For videocon serial number response code.
     */
    public final static String VIDEOCON_CODE_ZERO = "Model and serial number not found";
    public final static String VIDEOCON_CODE_ONE = "Model and serial number found";
    public final static String VIDEOCON_CODE_TWO = "Finance has already been processed for this model and serial number and not eligible for finance";
    public final static String VIDEOCON_CODE_THREE = "Finance has already been processed for this model and serial number by other finance company and not eligible for finance";
    public final static String VIDEOCON_CODE_FOUR = "Model and serial number locked as disbursement already processed";
    public final static String VIDEOCON_CODE_FIVE = "Delivery order has been reverted for this model and serial number";
    public final static String VIDEOCON_CODE_SIX = "Delivery order should be generated before a rollback";

    /**
     * For TKIL data push response code.
     */
    public final static String TKIL_TRUE = "Delivery order data pushed successfully";
    public final static String TKIL_FALSE = "Delivery order data could not pushed";

    /**
     * For lg serial number response code.
     */
    public static final String LG_VALID_SERIAL  = "Valid Serial Number";
    public static final String LG_INVALID_SERIAL  = "Invalid Serial Number";
    public static final String LG_SERIAL_ALREADY_BLOCK = "Serial number already blocked";

    /**
     * For lg imei number response code.
     */
    public static final String LG_VALID_IMEI  = "Valid IMEI Number";
    public static final String LG_INVALID_IMEI  = "Invalid IMEI Number";
    public static final String LG_CODE_TWO = "IMEI number block successfully";
    public static final String LG_CODE_THREE = "IMEI number unblock successfully";
    public static final String LG_IMEI_ALREADY_BLOCK = "IMEI number already blocked";
    public static final String LG_IMEI_NUMBER_NOT_EXIST = "IMEI Number does not Exists";

    /**
     * For Google IMEI number response code.
     */
    public final static String GOOGLE_CODE_ZERO = "EMI can be offered.";
    public final static String GOOGLE_CODE_ONE = "EMI can’t be offered as product is already sold.";
    public final static String GOOGLE_CODE_TWO = "EMI can’t be offered as invalid OEM IMEI code.";

    /**
     * For Google Rollback IMEI number response code.
     */
    public final static String GOOGLE_ROLLBACK_CODE_ZERO = "Successfully rolled back IMEI number.";
    public final static String GOOGLE_ROLLBACK_CODE_ONE = "Failed to rollback IMEI number.";
    public final static String GOOGLE_ROLLBACK_CODE_TWO = "IMEI number already rolled back.";

    /**
     * For Oppo IMEI number response code.
     */
    public final static String OPPO_CODE_ZERO = "Valid IMEI Number.";

    /**
     * For COMIO IMEI number response code.
     */
    public final static String COMIO_CODE_ZERO = "Valid IMEI Number";
    public final static String COMIO_CODE_ONE = "IMEI is already validated.";
    public final static String COMIO_CODE_TWO = "IMEI is invalid.";
    public final static String COMIO_CODE_THREE = "Invalid retailer code.";
    public final static String COMIO_CODE_FOUR = "IMEI does not exist with given retailer.";

    /**
     * For COMIO IMEI Rollback response code.
     */
    public final static String COMIO_ROLLBACK_CODE_ZERO = "Successfully rollback IMEI number.";
    public final static String COMIO_ROLLBACK_CODE_ONE = "IMEI is invalid.";
    public final static String COMIO_ROLLBACK_CODE_TWO = "IMEI is already validated";
    public final static String COMIO_ROLLBACK_CODE_THREE = "Invalid Model code.";
    public final static String COMIO_ROLLBACK_CODE_FOUR = "IMEI does not belong to given model.";
    public final static String COMIO_ROLLBACK_CODE_FIVE = "Invalid retailer code.";
    public final static String COMIO_ROLLBACK_CODE_SIX = "IMEI does not exists with given retailer.";
    public final static String COMIO_ROLLBACK_CODE_SEVEN = "Invalid Request type.";
    public final static String COMIO_ROLLBACK_CODE_EIGHT = "IMEI number already rollback.";

    /**
     * For Nokia IMEI number response code.
     */
    public final static String NOKIA_IMEI_CODE_ZERO = "EMI can be offered.";
    public final static String NOKIA_IMEI_CODE_ONE = "EMI can’t be offered as the IMEI is already sold.";
    public final static String NOKIA_IMEI_CODE_TWO = "IMEI is not a valid Nokia IMEI number.";
    public final static String NOKIA_IMEI_CODE_THREE = "Model is different or incorrect.";

    /**
     * For Nokia IMEI number Rollback response code.
     */
    public final static String NOKIA_ROLLBACK_CODE_ZERO = "Successfully rolled back IMEI number.";
    public final static String NOKIA_ROLLBACK_CODE_ONE = "IMEI number already rolled back.";
    public final static String NOKIA_ROLLBACK_CODE_TWO = "IMEI number is invalid.";
    public final static String NOKIA_ROLLBACK_CODE_THREE = "Model number is invalid.";

    /**
     * For Onida Serial number response code.
     */
    public final static String ONIDA_CODE_ZERO = "Invalid Serial Number.";
    public final static String ONIDA_CODE_ONE = "Valid Serial Number.";


    /**
     * For IMPS response code.
     */
    public final static String IMPS_SUCCESS = "Account number is valid";
    public final static String IMPS_FAILED = "Account number is invalid";

    /**
     * IMEI Response Constants
     */
    public final static String VALID_IMEI = "Valid IMEI number.";
    public final static String INVALID_IMEI = "Invalid IMEI number.";

    /**
     * For Lava IMEI number response code.
     */
    public final static String LAVA_IMEI_CODE_ONEZEROONE = "The tertiary has happened already.";
    public final static String LAVA_IMEI_CODE_ONEZEROTWO = "The phone doesn’t belong in Lava database.";
    public final static String LAVA_IMEI_CODE_ONEZEROTHREE = "Entered price is more than MRP.";
    public final static String LAVA_IMEI_CODE_ONEZEROFOUR = "Entered price is less than MOP.";
    public final static String LAVA_IMEI_CODE_ONEZEROFIVE = "Model name provided doesn’t match with Lava DB.";
    public final static String LAVA_IMEI_CODE_ONEZEROSIX = "The phone is not associated with any distributor.";
    public final static String LAVA_IMEI_CODE_ONEZEROEIGHT = "Handset is not covered under financing.Try again with a different handset.";



    /**
     * For Carrier Serial number response code.
     */
    public final static String CARRIER_CODE_MINUS_ONE = "Sorry! You are not authorized to access this Service";
    public final static String CARRIER_CODE_ZERO = "Product and Serial No. is Valid";
    public final static String CARRIER_CODE_ONE = "Product is Invalid.";
    public final static String CARRIER_CODE_TWO = "Serial No is Invalid.";
    public final static String CARRIER_CODE_THREE = "Serial number is already validated.";

    /**
     * For Carrier Serial number Rollback response code.
     */
    public final static String CARRIER_ROLLBACK_CODE_MINUS_ONE = "Sorry! You are not authorized to access this Service";
    public final static String CARRIER_ROLLBACK_CODE_ZERO = "Successfully rollback.";
    public final static String CARRIER_ROLLBACK_CODE_ONE = "Product is Invalid.";
    public final static String CARRIER_ROLLBACK_CODE_TWO = "Serial No is Invalid.";


}
