package com.softcell.constants;

/**
 * @author vinodk
 */
public enum ComponentConfigurationType {
    BASIC,
    RE_APPRAISE_RE_INITIATE,
    EXPRESS_QUICK_DATA_ENTRY,
    SOLICITED_QUICK_DATA_ENTRY,
    EXISTING_QUICK_DATA_ENTRY;
}
