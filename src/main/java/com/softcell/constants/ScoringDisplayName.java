/**
 * kishorp9:40:26 PM  Copyright Softcell Technolgy
 **/
package com.softcell.constants;

/**
 * @author kishorp
 *This Class deal with  display names at  UI side, Please don't make any change or modify  without permission.Thanks. 
 */
public final class ScoringDisplayName {
    /**
     * This will  used to display.
     */
    public final static String RESIDENTIAL_ADDRESS = "Residential Address Score";
    public final static String OFFICE_ADDRESS = "Office Address Score";
    public final static String CIBIL_SCORE = "CIBIL Score";
    public final static String PAN_RESULT = "PAN Verification";
    public final static String AADHAR_RESULT = "AADHAR Verification";
    public final static String APPLICATION_SCORE = "Application Score";
    public final static String EXPERIAN_SCORE = "Experian Score";
    public final static String HIGHMARK_SCORE = "Highmark Score";
    public final static String SAATHI_CASE_STATUS = "Saathi Case Status";
    public final static String CREDIT_VIDYA_STATUS = "CreditVidya Status";
    public static final String POSIDEX_DEDUPE_STATUS = "Posidex Dedupe Status";
    public static final String INTERNAL_DEDUPE_STATUS = "Internal Dedupe Status";

    /**
     * This order is used to display.
     */
    public final static int RESIDENTIAL_ADDRESS_ORDER = 1;
    public final static int OFFICE_ADDRESS_ORDER = 2;
    public final static int CIBIL_SCORE_ORDER = 3;
    public final static int PAN_RESULT_ORDER = 4;
    public final static int APPLICATION_SCORE_ORDERE = 5;
    public final static int EXPERIAN_SCORE_ORDER = 6;
    public final static int AADHAR_RESULT_ORDER = 7;
    public final static int SAATHI_RESULT_ORDER = 8;
    public final static int CREDIT_VIDYA_RESULT_ORDER = 9;
    public final static int INTERNAL_DEDUPE_ORDER = 10;
    public final static int POSIDEX_RESULT_ORDER = 11;
    public final static int HIGHMARK_SCORE_ORDER = 12;


}
