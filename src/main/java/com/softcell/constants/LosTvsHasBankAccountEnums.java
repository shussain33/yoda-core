package com.softcell.constants;

public enum LosTvsHasBankAccountEnums {

    yes("Y"),
    no("N");

    private  String value;

    LosTvsHasBankAccountEnums(String value){
        this.value = value;
    }

    public String  toValue(){
        return this.value;
    }

}
