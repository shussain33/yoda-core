package com.softcell.constants;

/**
 * Created by yogeshb on 2/2/17.
 */
public class DateUnit {

    public enum UNIT {
        ms, s, m, h, d, y, milliseconds, seconds, minutes, hours, days, months, years, mins, secs, hrs, msecs
    }
}
