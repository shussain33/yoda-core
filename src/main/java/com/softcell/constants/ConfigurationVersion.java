package com.softcell.constants;

/**
 * This class will basically hold versions related to services like
 * Aadhar, Mb or any services whose version can vary with time.
 * Created by bhuvneshk on 7/9/17.
 */
public enum ConfigurationVersion {

    /**
     * Aadhar verison
     */
    v2_1,v2_2;
}
