package com.softcell.constants;

public class PhoneJsonKeys {

    //Mobile type for phone type
    public static final String PERSONAL_MOBILE_TYPE = "PERSONAL_MOBILE";

    //Mobile type for phone type
    public static final String PERSONAL_PHONE_TYPE = "PERSONAL_PHONE";

    //Office type for phone type
    public static final String OFFICE_TYPE = "OFFICE_PHONE";

    //Mobile type for phone type
    public static final String OFFICE_MOBILE = "OFFICE_MOBILE";


    //Office type for phone type
    public static final String RESIDENCE_MOBILE = "RESIDENCE_MOBILE";

    //Mobile type for phone type
    public static final String RESIDENCE_PHONE = "RESIDENCE_PHONE";
}
