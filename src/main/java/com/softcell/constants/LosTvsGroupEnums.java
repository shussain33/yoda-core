package com.softcell.constants;

public enum LosTvsGroupEnums {

    GroupOne,
    GroupTwo,
    GroupThree,
    GroupFour,
    GroupFive,
    Initiate;
}
