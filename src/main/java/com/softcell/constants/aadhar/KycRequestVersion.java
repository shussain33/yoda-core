package com.softcell.constants.aadhar;

/**
 * Created by mahesh on 30/11/17.
 */
public enum KycRequestVersion {

    HSM, NON_HSM
}
