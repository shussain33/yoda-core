package com.softcell.constants.aadhar;

/**
 * Created by mahesh on 30/11/17.
 */
public enum KycRequestType {
    GET_OTP, VERIFY_OTP, VERIFY_IRIS, VERIFY_BIO
}
