package com.softcell.constants;

import com.softcell.service.impl.KarzaHelper;

import java.util.Arrays;

public enum KycRquestTypesEnum {

    BLANK("", ""),
    PAN ("PAN", KarzaHelper.PAN_REQUEST_TYPE),
    AADHAR("AADHAR", KarzaHelper.AADHAR_REQUEST_TYPE),
    VOTER_ID ("VOTER-ID", KarzaHelper.VOTER_REQUEST_TYPE),
    DRIVING_LICENSE ("DRIVING-LICENSE", KarzaHelper.DL_REQUEST_TYPE),
    GST ("GST", KarzaHelper.GST_REQUEST_TYPE),
    PASSPORT ("PASSPORT", KarzaHelper.PASSPORT_REQUEST_TYPE),
    GST_DETAILED("gstdetailed", KarzaHelper.GST_DETAILED_REQUEST_TYPE),
    TAN_AUTH("tan", KarzaHelper.TAN_AUTH_REQUEST_TYPE),
    TAN_DETAIL("tandetailed", KarzaHelper.TAN_DETAIL_REQUEST_TYPE),
    COMPANY_SEARCH("compsearch", KarzaHelper.COMPANY_SEARCH_REQUEST_TYPE),
    COMPANY_AND_LLP_CIN("cinlookup", KarzaHelper.COMPANY_AND_LLP_CIN_REQUEST_TYPE),
    COMPANY_AND_LLP_CIN_ID("mca", KarzaHelper.COMPANY_AND_LLP_CIN_ID_REQUEST_TYPE),
    UAN("uam", KarzaHelper.UAN_REQUEST_TYPE),
    NREGA("mnrega", KarzaHelper.NREGA_REQUEST_TYPE),
    IEC("iec", KarzaHelper.IEC_REQUEST_TYPE),
    IEC_DETAILED("iecdetailed", KarzaHelper.IEC_DETAILED_REQUEST_TYPE),
    MCA_SIGNATORIES("mca-signatories", KarzaHelper.MCA_SIGNATORIES_REQUEST_TYPE),
    GST_SEARCH("gst-search", KarzaHelper.GST_SEARCH_REQUEST_TYPE),
    GSP_GSTIN_AUTH("gst-verification", KarzaHelper.GSP_GSTIN_AUTH_REQUEST_TYPE),
    GSP_GST_RETURN_FILLING("gst-return-status", KarzaHelper.GSP_GST_RETURN_FILLING_REQUEST_TYPE),
    SHOP_ESTABLISHMENT("shop", KarzaHelper.SHOP_ESTABLISHMENT_REQUEST_TYPE),
    FSSAI_LICENSE_AUTH("fssai", KarzaHelper.FSSAI_LICENSE_AUTH_REQUEST_TYPE),
    FDA_LICENSE_AUTH("fda", KarzaHelper.FDA_LICENSE_AUTH_REQUEST_TYPE),
    CA_MEMBERSHIP_AUTH("icai", KarzaHelper.CA_MEMBERSHIP_AUTH_REQUEST_TYPE),
    ICSI_MEMBERSHIP("icsi", KarzaHelper.ICSI_MEMBERSHIP_REQUEST_TYPE),
    ICWAI_MEMBERSHIP("icwaim", KarzaHelper.ICWAI_MEMBERSHIP_REQUEST_TYPE),
    ICWAI_FIRM("icwaif", KarzaHelper.ICWAI_FIRM_REQUEST_TYPE),
    MCI_MEMBERSHIP("mci", KarzaHelper.MCI_MEMBERSHIP_REQUEST_TYPE),
    PNG_AUTH("png", KarzaHelper.PNG_AUTH_REQUEST_TYPE),
    ELECTRICITY_BILL_AUTH("elec", KarzaHelper.ELECTRICITY_BILL_AUTH_REQUEST_TYPE),
    TELEPHONE_BILL_AUTH("tele", KarzaHelper.TELEPHONE_BILL_AUTH_REQUEST_TYPE),
    MOBILE_OTP("mobile-otp", KarzaHelper.MOBILE_OTP_REQUEST_TYPE),
    MOBILE_DETAILS("mobile-verify", KarzaHelper.MOBILE_DETAILS_REQUEST_TYPE),
    LPG_ID("lpg", KarzaHelper.LPG_ID_REQUEST_TYPE),
    EPF_OTP("epf-get-otp", KarzaHelper.EPF_OTP_REQUEST_TYPE),
    EPF_PASSBOOK("epf-get-passbook", KarzaHelper.EPF_PASSBOOK_REQUEST_TYPE),
    EPF_UAN_LOOKUP("uan-lookup", KarzaHelper.EPF_UAN_LOOKUP_REQUEST_TYPE),
    EMPLOYER_LOOKUP("membership-lookup", KarzaHelper.EMPLOYER_LOOKUP_REQUEST_TYPE),
    ESIC_ID("esic", KarzaHelper.ESIC_ID_REQUEST_TYPE),
    FORM_16("tds", KarzaHelper.FORM_16_REQUEST_TYPE),
    FORM_16_QUATERLY("tdsq", KarzaHelper.FORM_16_QUATERLY_REQUEST_TYPE),
    ITR("itr", KarzaHelper.ITR_REQUEST_TYPE),
    ADDRESS_MATCHING("address", KarzaHelper.ADDRESS_MATCHING_REQUEST_TYPE),
    EMAIL("email", KarzaHelper.EMAIL_REQUEST_TYPE),
    NAME_SIMILARITY("name", KarzaHelper.NAME_SIMILARITY_REQUEST_TYPE),
    IFSC_CODE("ifsc", KarzaHelper.IFSC_CODE_REQUEST_TYPE),
    BANK_ACCOUNT_VERIFICATION("bankacc", KarzaHelper.BANK_ACCOUNT_VERIFICATION_REQUEST_TYPE),
    HSN_CODE("dgft", KarzaHelper.HSN_CODE_REQUEST_TYPE),
    WEBSITE_DOMAIN("whois", KarzaHelper.WEBSITE_DOMAIN_REQUEST_TYPE),
    VEHICLE_RC_AUTH("rc", KarzaHelper.VEHICLE_RC_AUTH_REQUEST_TYPE),
    VEHICLE_RC_SEARCH("rcsearch", KarzaHelper.VEHICLE_RC_SEARCH_REQUEST_TYPE),
    GST_SEARCH_BY_PAN("gst-search-by-pan", KarzaHelper.GST_SEARCH_BY_PAN_REQUEST_TYPE),
    GST_TRANS_API("gst-trrn", KarzaHelper.GST_TRANS_API_REQUEST_TYPE),
    GST_TRRN_OTP("gst-trrn-otp", KarzaHelper.GST_TRRN_OTP_REQUEST_TYPE),
    GST_TRRN_VERIFY_OTP("gst-trrn-otp-verify", KarzaHelper.GST_TRRN_VERIFY_OTP_REQUEST_TYPE),
    GST_CREDENTIAL_LINK_GENERATION("gst-trrn-credlink", KarzaHelper.GST_CREDENTIAL_LINK_GENERATION_REQUEST_TYPE),
    GST_CREDENTIAL_LINK_SEND("gst-trrn-credlink-send", KarzaHelper.GST_CREDENTIAL_LINK_SEND_REQUEST_TYPE),
    ENTITY_SEARCH("corp-search", KarzaHelper.ENTITY_SEARCH_REQUEST_TYPE),
    DETAILED_PROFILE("corp-profile", KarzaHelper.DETAILED_PROFILE_REQUEST_TYPE),
    GST_FILING_STATUS("filingstatus", KarzaHelper.GST_FILING_STATUS_REQUEST_TYPE),
    GST_PROFILE("gst-profile", KarzaHelper.GST_PROFILE_REQUEST_TYPE),
    IBBI("ibbi", KarzaHelper.IBBI_REQUEST_TYPE),
    NCLT("nclt", KarzaHelper.NCLT_REQUEST_TYPE),
    BIFR_CHECK("bifr", KarzaHelper.BIFR_CHECK_REQUEST_TYPE),
    DRT_CHECK("drt", KarzaHelper.DRT_CHECK_REQUEST_TYPE),
    SUIT_FILED_SEARCH("suits", KarzaHelper.SUIT_FILED_SEARCH_REQUEST_TYPE),
    LITIGATIONS_SEARCH("litigations-search", KarzaHelper.LITIGATIONS_SEARCH_REQUEST_TYPE),
    LITIGATIONS_DETAILS_DISTRICT_COURTS("distdetail", KarzaHelper.LITIGATIONS_DETAILS_DISTRICT_COURTS_REQUEST_TYPE);


    private final String key;
    private final String value;

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    KycRquestTypesEnum(final String key, final String value) {
        this.value = value;
        this.key = key;
    }

    public static String getRequestType(final String key){
        return (Arrays.stream(KycRquestTypesEnum.values())
                .filter(kycRquestTypesEnum -> kycRquestTypesEnum.getKey().equals(key))
                .findFirst()
                .orElse(BLANK)).getValue();
    }
}
