package com.softcell.constants;

public enum LosTvsPhoneTypeEnum {
    PERSONAL_MOBILE,
    PERSONAL_PHONE,
    RESIDENCE_MOBILE,
    RESIDENCE_PHONE,
    OFFICE_PHONE,
    OFFICE_MOBILE
}
