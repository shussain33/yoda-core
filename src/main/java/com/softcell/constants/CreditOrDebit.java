package com.softcell.constants;

/**
 * Created by Softcell on 29/09/17.
 */
public enum CreditOrDebit {
    CREDIT,
    DEBIT;
}
