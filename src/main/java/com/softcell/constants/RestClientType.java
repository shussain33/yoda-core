package com.softcell.constants;

/**
 * Created by vinod on 17/3/17.
 */
public enum RestClientType {

    WEB("WEB"),
    ANDROID("Android");

    RestClientType(String value){
        this.value = value;
    }

    private String value;

    public String getValue(){
        return value;
    }
}
