package com.softcell.constants;

/**
 * Created by mahesh on 10/8/17.
 */
public enum  DigitizationConstant {

    ZERO("0.00");

    private final String stringValue;

    DigitizationConstant(final String s) {
        stringValue = s;
    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toFaceValue() {
        return stringValue;
    }

}
