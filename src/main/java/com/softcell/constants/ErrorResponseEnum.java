package com.softcell.constants;

/**
 * Created by mahesh on 2/3/17.
 */
public enum ErrorResponseEnum {

    SYSTEM("SYSTEM", 1000),

    BUSINESS("BUSINESS", 2000),

    DATABASE("DATABASE", 3000);

    private int code;
    private String value;

    private ErrorResponseEnum(final String value, final int code) {
        this.value = value;
        this.code = code;
    }

    public String toValue() {
        return value;
    }

    public int toCode() {
        return code;
    }

}
