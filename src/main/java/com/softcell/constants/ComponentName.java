package com.softcell.constants;

/**
 * @author bhuvneshk
 *         * Will used to  validate access control and maintain data consistency.
 *         All component will defined here, which will be use for access check purpose.
 */

public enum ComponentName {
    DEDUPE("Dedupe"),
    MB("MB"),
    KYC("kycProcessor"),
    NTC("NTC-Processor"),
    SCORING("SCORING"),
    SALESFORCE("salesforce"),
    AMAZON("amazonS3");

    private final String componentName;

    private ComponentName(final String componentName) {
        this.componentName = componentName;
    }

    public String toComponentName() {
        return componentName;
    }
}
