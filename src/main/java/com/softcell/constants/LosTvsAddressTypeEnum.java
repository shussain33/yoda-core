package com.softcell.constants;

public enum LosTvsAddressTypeEnum {

    AADHAR_ADDRESS,
    RESIDENCE,
    OFFICE,
    PERMANENT,
    RESIDENTIAL,
    COMMERCIAL
}
