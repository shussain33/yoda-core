package com.softcell.constants;

/**
 * Created by mahesh on 16/10/17.
 */
public enum DebitCardType {

    PREPAID("PREPAID"),
    CLASSIC("CLASSIC"),
    GOLD("GOLD"),
    PLATINUM("PLATINUM"),
    SIGNATURE("SIGNATURE"),
    WORLD("WORLD");

    private final String debitCardType;

    DebitCardType(final String s) {
        debitCardType = s;
    }

    /**
     * method to return enum constants, face values
     *
     * @return
     */
    public String toFaceValue() {
        return debitCardType;
    }
}
