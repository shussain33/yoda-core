package com.softcell.gonogo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssg0302 on 14/8/19.
 */
public class Buckets {

    public enum Bucket{
        B1, BUCKET1, B2, BUCKET2, B3, BUCKET3, B4,BUCKET4, B5,BUCKET5, B6,BUCKET6
    }
    private static ArrayList<String> fosBuckets = new ArrayList<String>(){
        {
            add(Bucket.B1.name()); add(Bucket.BUCKET1.name());
            add(Bucket.B5.name()); add(Bucket.BUCKET5.name());
            add(Bucket.B6.name()); add(Bucket.BUCKET6.name());
        }
    };

    private static ArrayList<String> creditBuckets = new ArrayList<String>(){
        {
            add(Bucket.B2.name()); add(Bucket.BUCKET2.name());
            add(Bucket.B3.name()); add(Bucket.BUCKET3.name());
            add(Bucket.B4.name()); add(Bucket.BUCKET4.name());
        }
    };

    private static ArrayList<String> verificationBuckets = new ArrayList<String>(){
        {
            add(Bucket.B4.name()); add(Bucket.BUCKET4.name());
            add(Bucket.B5.name()); add(Bucket.BUCKET5.name());
            add(Bucket.B6.name()); add(Bucket.BUCKET6.name());
        }
    };

    private static ArrayList<String> stpBucket = new ArrayList<String>(){
        {
            add(Bucket.B1.name()); add(Bucket.BUCKET1.name());
        }
    };

    private static ArrayList<String> declinedBucket = new ArrayList<String>(){
        {
            add(Bucket.B2.name()); add(Bucket.BUCKET2.name());
        }
    };

    public static List<String> getFosBuckets() { return (List) fosBuckets.clone(); }

    public static List<String> getCreditBuckets() {
        return (List) creditBuckets.clone();
    }

    public static boolean isFosBucket(String stage){        return fosBuckets.contains(stage);    }

    public static boolean isCreditBucket(String stage){        return creditBuckets.contains(stage);    }

    public static boolean isVerificationBucket(String bucket){        return verificationBuckets.contains(bucket);    }

    public static boolean isSTPBucket(String bucket){        return stpBucket.contains(bucket);    }

    public static boolean isDeclinedBucket(String bucket){        return declinedBucket.contains(bucket);    }
}
