/**
 * kishorp1:54:52 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.gridengine;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.gonogo.grid.EligibilityGrid;
import com.softcell.gonogo.grid.GridExpresionCondition;
import com.softcell.gonogo.grid.GridExpressionRules;
import com.softcell.gonogo.grid.GridExpressionSuccessorRule;

import java.io.IOException;

/**
 * @author kishorp
 *
 */
public class GridBuilder {

    /**
     * @param eligibilityGrid
     */
    private static void getExp(EligibilityGrid eligibilityGrid) {
        for (GridExpressionRules rules : eligibilityGrid.getRULES()) {

            for (GridExpresionCondition codition : rules.getCondition()) {
                StringBuffer buffer = new StringBuffer();
                buffer.append('(');
                buffer.append(codition.getFieldname()).append(codition.getExp2()).append(codition.getVal2());
                if (codition.getOperator() == null || codition.getOperator().isEmpty()) {
                    buffer.append(')');
                } else {
                    buffer.append(')').append(codition.getOperator()).append('(');
                    for (GridExpressionSuccessorRule succesor : codition.getRef()) {
                        buffer.append(succesor.getFieldname()).append(succesor.getExp2()).append(succesor.getVal2());
                        if (succesor.getOperator() == null || succesor.getOperator().isEmpty()) {
                            buffer.append(')');
                        } else {
                            buffer.append(')').append(succesor.getOperator()).append('(');
                        }
                    }
                }

            }
        }

    }
}
