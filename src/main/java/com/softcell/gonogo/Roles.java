package com.softcell.gonogo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Has information about the roles which are supposed to
 * work on queued cases. Exposes methods to access those roles.
 * Created by archana on 31/8/17.
 */
public class Roles {

    public enum Role {
        DSA, CRO1, CRO2, CRO3, CRO4,
        CRO, OPS, Sales, QUEUE_ADMIN,
        FOS, CPA, CREDIT, ACM, RCM, ZCM, NCM, BOPS, HOPS, VIEWALL,EXTERNAL_OPS, EXTERNAL_CREDIT,
        RESI_VERIFIER, OFC_VERIFIER, LEGAL_VERIFIER, PROPERTY_VERIFIER, ITR_VERIFIER, BANK_VERIFIER, VIEWALL_EMAIL, SAMPLER, RCU_AGENCY, RCU_VERIFIER;
    };

    private static ArrayList<String> croRoles = new ArrayList<String>()
    {{  add(Role.CRO.name()); add(Role.OPS.name());  add(Role.Sales.name());
        add(Role.CRO1.name()); add(Role.CRO2.name());  add(Role.CRO3.name()); add(Role.CRO4.name());
    }};

    private static ArrayList<String> cpaRoles = new ArrayList<String>()
    {{  add(Roles.Role.CPA.name());
    }};

    private static ArrayList<String> creditRoles = new ArrayList<String>()
    {{  add(Roles.Role.ACM.name()); add(Roles.Role.RCM.name()); add(Roles.Role.ZCM.name());
        add(Roles.Role.NCM.name()); add(Role.CRO.name());
    }};

    private static ArrayList<String> opsRoles = new ArrayList<String>()
    {{  add(Role.BOPS.name());  add(Roles.Role.HOPS.name());
    }};

    private static  ArrayList<String> coOrigination= new ArrayList<String>()
    {{
        add(Role.EXTERNAL_CREDIT.name()); add(Role.EXTERNAL_OPS.name());
    }};

    private static  ArrayList<String> docVerifierRoles = new ArrayList<String>()
    {{
        add(Role.SAMPLER.name());
    }};

    private static  ArrayList<String> docSamplerRoles = new ArrayList<String>()
    {{
        add(Role.RCU_AGENCY.name());
        add(Role.RCU_VERIFIER.name());
    }};

    public static List<String> getCroRoles() {
        return (List)croRoles.clone();
    }

    public static List<String> getCpaRoles() {
        return (List)cpaRoles.clone();
    }

    public static List<String> getCreditRoles() {
        return (List)creditRoles.clone();
    }

    public static List<String> getOpsRoles() {
        return (List)opsRoles.clone();
    }

    public static boolean isCroRole(String role){
        return croRoles.contains(role);
    }

    public static boolean isCpaRole(String role){
        return cpaRoles.contains(role);
    }

    public static boolean isCreditRole(String role){
        return creditRoles.contains(role);
    }

    public static boolean isCoOriginationRole(String role){ return coOrigination.contains(role);   }

    public static boolean isOpsRole(String role){        return opsRoles.contains(role);    }

    public static boolean hasCroRole(Set<String> roles){
        // Returns true if the two specified collections have no elements in common.
        return !Collections.disjoint(roles, croRoles);
    }

    public static void retainCroRoles(Set<String> roles){
        roles.retainAll(croRoles);
    }

    public static boolean isDocVerRole(String role){ return docVerifierRoles.contains(role);   }

    public static boolean isDocSamplerRole(String role){ return docSamplerRoles.contains(role);   }

}
