package com.softcell.gonogo.servicecoderegistration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * created by prasenjit wadmare on 21/12/2017
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceCodeRegistrationRequest {

    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.InstWithProductGrp.class
            }
    )
    @Valid
    private Header header;

    /**
     * Following are the possible values for vendor
     */
    @JsonProperty("sVendor")
    @NotEmpty(
            groups = {
                    ServiceCodeRegistrationRequest.FetchGrp.class
            }
    )
    private String vendor;


    public interface FetchGrp {
    }

}
