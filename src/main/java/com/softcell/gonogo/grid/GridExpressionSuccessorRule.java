package com.softcell.gonogo.grid;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "val1",
        "exp1",
        "fieldname",
        "displayname",
        "ExpType",
        "FType",
        "DType",
        "AFSpec",
        "exp2",
        "val2",
        "operator",
        "Difference",
        "DiffOpr"
})
public class GridExpressionSuccessorRule {

    @JsonProperty("val1")
    private String val1;
    @JsonProperty("exp1")
    private String exp1;
    @JsonProperty("fieldname")
    private String fieldname;
    @JsonProperty("displayname")
    private String displayname;
    @JsonProperty("ExpType")
    private String ExpType;
    @JsonProperty("FType")
    private String FType;
    @JsonProperty("DType")
    private String DType;
    @JsonProperty("AFSpec")
    private String AFSpec;
    @JsonProperty("exp2")
    private String exp2;
    @JsonProperty("val2")
    private String val2;
    @JsonProperty("operator")
    private String operator;
    @JsonProperty("Difference")
    private String Difference;
    @JsonProperty("DiffOpr")
    private String DiffOpr;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The val1
     */
    @JsonProperty("val1")
    public String getVal1() {
        return val1;
    }

    /**
     * @param val1 The val1
     */
    @JsonProperty("val1")
    public void setVal1(String val1) {
        this.val1 = val1;
    }

    /**
     * @return The exp1
     */
    @JsonProperty("exp1")
    public String getExp1() {
        return exp1;
    }

    /**
     * @param exp1 The exp1
     */
    @JsonProperty("exp1")
    public void setExp1(String exp1) {
        this.exp1 = exp1;
    }

    /**
     * @return The fieldname
     */
    @JsonProperty("fieldname")
    public String getFieldname() {
        return fieldname;
    }

    /**
     * @param fieldname The fieldname
     */
    @JsonProperty("fieldname")
    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    /**
     * @return The displayname
     */
    @JsonProperty("displayname")
    public String getDisplayname() {
        return displayname;
    }

    /**
     * @param displayname The displayname
     */
    @JsonProperty("displayname")
    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    /**
     * @return The ExpType
     */
    @JsonProperty("ExpType")
    public String getExpType() {
        return ExpType;
    }

    /**
     * @param ExpType The ExpType
     */
    @JsonProperty("ExpType")
    public void setExpType(String ExpType) {
        this.ExpType = ExpType;
    }

    /**
     * @return The FType
     */
    @JsonProperty("FType")
    public String getFType() {
        return FType;
    }

    /**
     * @param FType The FType
     */
    @JsonProperty("FType")
    public void setFType(String FType) {
        this.FType = FType;
    }

    /**
     * @return The DType
     */
    @JsonProperty("DType")
    public String getDType() {
        return DType;
    }

    /**
     * @param DType The DType
     */
    @JsonProperty("DType")
    public void setDType(String DType) {
        this.DType = DType;
    }

    /**
     * @return The AFSpec
     */
    @JsonProperty("AFSpec")
    public String getAFSpec() {
        return AFSpec;
    }

    /**
     * @param AFSpec The AFSpec
     */
    @JsonProperty("AFSpec")
    public void setAFSpec(String AFSpec) {
        this.AFSpec = AFSpec;
    }

    /**
     * @return The exp2
     */
    @JsonProperty("exp2")
    public String getExp2() {
        return exp2;
    }

    /**
     * @param exp2 The exp2
     */
    @JsonProperty("exp2")
    public void setExp2(String exp2) {
        this.exp2 = exp2;
    }

    /**
     * @return The val2
     */
    @JsonProperty("val2")
    public String getVal2() {
        return val2;
    }

    /**
     * @param val2 The val2
     */
    @JsonProperty("val2")
    public void setVal2(String val2) {
        this.val2 = val2;
    }

    /**
     * @return The operator
     */
    @JsonProperty("operator")
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator The operator
     */
    @JsonProperty("operator")
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return The Difference
     */
    @JsonProperty("Difference")
    public String getDifference() {
        return Difference;
    }

    /**
     * @param Difference The Difference
     */
    @JsonProperty("Difference")
    public void setDifference(String Difference) {
        this.Difference = Difference;
    }

    /**
     * @return The DiffOpr
     */
    @JsonProperty("DiffOpr")
    public String getDiffOpr() {
        return DiffOpr;
    }

    /**
     * @param DiffOpr The DiffOpr
     */
    @JsonProperty("DiffOpr")
    public void setDiffOpr(String DiffOpr) {
        this.DiffOpr = DiffOpr;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
