/**
 */
package com.softcell.gonogo.grid;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ELGBLTY",
        "LOAN_AMOUNT",
        "DP",
        "MAX_TENOR",
        "DEC_PRRTY"
})
/**
 *
 * @author kishorp
 *
 */
public class AGRTDVALUES {

    @JsonProperty("ELGBLTY")
    private com.softcell.gonogo.grid.ELGBLTY ELGBLTY;
    @JsonProperty("LOAN_AMOUNT")
    private com.softcell.gonogo.grid.LOANAMOUNT LOANAMOUNT;
    @JsonProperty("DP")
    private com.softcell.gonogo.grid.DP DP;
    @JsonProperty("MAX_TENOR")
    private com.softcell.gonogo.grid.MAXTENOR MAXTENOR;
    @JsonProperty("DEC_PRRTY")
    private List<String> DECPRRTY = new ArrayList<String>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The ELGBLTY
     */
    @JsonProperty("ELGBLTY")
    public com.softcell.gonogo.grid.ELGBLTY getELGBLTY() {
        return ELGBLTY;
    }

    /**
     * @param ELGBLTY The ELGBLTY
     */
    @JsonProperty("ELGBLTY")
    public void setELGBLTY(com.softcell.gonogo.grid.ELGBLTY ELGBLTY) {
        this.ELGBLTY = ELGBLTY;
    }

    /**
     * @return The LOANAMOUNT
     */
    @JsonProperty("LOAN_AMOUNT")
    public com.softcell.gonogo.grid.LOANAMOUNT getLOANAMOUNT() {
        return LOANAMOUNT;
    }

    /**
     * @param LOANAMOUNT The LOAN_AMOUNT
     */
    @JsonProperty("LOAN_AMOUNT")
    public void setLOANAMOUNT(com.softcell.gonogo.grid.LOANAMOUNT LOANAMOUNT) {
        this.LOANAMOUNT = LOANAMOUNT;
    }

    /**
     * @return The DP
     */
    @JsonProperty("DP")
    public com.softcell.gonogo.grid.DP getDP() {
        return DP;
    }

    /**
     * @param DP The DP
     */
    @JsonProperty("DP")
    public void setDP(com.softcell.gonogo.grid.DP DP) {
        this.DP = DP;
    }

    /**
     * @return The MAXTENOR
     */
    @JsonProperty("MAX_TENOR")
    public com.softcell.gonogo.grid.MAXTENOR getMAXTENOR() {
        return MAXTENOR;
    }

    /**
     * @param MAXTENOR The MAX_TENOR
     */
    @JsonProperty("MAX_TENOR")
    public void setMAXTENOR(com.softcell.gonogo.grid.MAXTENOR MAXTENOR) {
        this.MAXTENOR = MAXTENOR;
    }

    /**
     * @return The DECPRRTY
     */
    @JsonProperty("DEC_PRRTY")
    public List<String> getDECPRRTY() {
        return DECPRRTY;
    }

    /**
     * @param DECPRRTY The DEC_PRRTY
     */
    @JsonProperty("DEC_PRRTY")
    public void setDECPRRTY(List<String> DECPRRTY) {
        this.DECPRRTY = DECPRRTY;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "AGRTDVALUES [ELGBLTY=" + ELGBLTY + ", LOANAMOUNT=" + LOANAMOUNT
                + ", DP=" + DP + ", MAXTENOR=" + MAXTENOR + ", DECPRRTY="
                + DECPRRTY + ", additionalProperties=" + additionalProperties
                + "]";
    }

}
