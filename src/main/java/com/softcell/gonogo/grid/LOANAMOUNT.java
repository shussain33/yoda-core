package com.softcell.gonogo.grid;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "AGGRTN",
        "MAX",
        "MIN"
})
public class LOANAMOUNT {

    @JsonProperty("AGGRTN")
    private String AGGRTN;
    @JsonProperty("MAX")
    private String MAX;
    @JsonProperty("MIN")
    private String MIN;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The AGGRTN
     */
    @JsonProperty("AGGRTN")
    public String getAGGRTN() {
        return AGGRTN;
    }

    /**
     * @param AGGRTN The AGGRTN
     */
    @JsonProperty("AGGRTN")
    public void setAGGRTN(String AGGRTN) {
        this.AGGRTN = AGGRTN;
    }

    /**
     * @return The MAX
     */
    @JsonProperty("MAX")
    public String getMAX() {
        return MAX;
    }

    /**
     * @param MAX The MAX
     */
    @JsonProperty("MAX")
    public void setMAX(String MAX) {
        this.MAX = MAX;
    }

    /**
     * @return The MIN
     */
    @JsonProperty("MIN")
    public String getMIN() {
        return MIN;
    }

    /**
     * @param MIN The MIN
     */
    @JsonProperty("MIN")
    public void setMIN(String MIN) {
        this.MIN = MIN;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
