package com.softcell.gonogo.grid;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Condition",
        "Outcome"
})
public class GridExpressionRules {

    @JsonProperty("Condition")
    private List<com.softcell.gonogo.grid.GridExpresionCondition> Condition = new ArrayList<com.softcell.gonogo.grid.GridExpresionCondition>();
    @JsonProperty("Outcome")
    private com.softcell.gonogo.grid.Outcome Outcome;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The Condition
     */
    @JsonProperty("Condition")
    public List<com.softcell.gonogo.grid.GridExpresionCondition> getCondition() {
        return Condition;
    }

    /**
     * @param Condition The Condition
     */
    @JsonProperty("Condition")
    public void setCondition(List<com.softcell.gonogo.grid.GridExpresionCondition> Condition) {
        this.Condition = Condition;
    }

    /**
     * @return The Outcome
     */
    @JsonProperty("Outcome")
    public com.softcell.gonogo.grid.Outcome getOutcome() {
        return Outcome;
    }

    /**
     * @param Outcome The Outcome
     */
    @JsonProperty("Outcome")
    public void setOutcome(com.softcell.gonogo.grid.Outcome Outcome) {
        this.Outcome = Outcome;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "GridExpressionRules [Condition=" + Condition + ", Outcome="
                + Outcome + ", additionalProperties=" + additionalProperties
                + "]";
    }

}
