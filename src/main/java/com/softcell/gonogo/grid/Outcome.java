package com.softcell.gonogo.grid;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "DECISION",
        "MAX_AMOUNT",
        "MIN_AMOUNT",
        "DP",
        "MAX_TENOR",
        "REMARK"
})
public class Outcome {

    @JsonProperty("DECISION")
    private String DECISION;
    @JsonProperty("MAX_AMOUNT")
    private String MAXAMOUNT;
    @JsonProperty("MIN_AMOUNT")
    private String MINAMOUNT;
    @JsonProperty("DP")
    private String DP;
    @JsonProperty("MAX_TENOR")
    private String MAXTENOR;
    @JsonProperty("REMARK")
    private String REMARK;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The DECISION
     */
    @JsonProperty("DECISION")
    public String getDECISION() {
        return DECISION;
    }

    /**
     * @param DECISION The DECISION
     */
    @JsonProperty("DECISION")
    public void setDECISION(String DECISION) {
        this.DECISION = DECISION;
    }

    /**
     * @return The MAXAMOUNT
     */
    @JsonProperty("MAX_AMOUNT")
    public String getMAXAMOUNT() {
        return MAXAMOUNT;
    }

    /**
     * @param MAXAMOUNT The MAX_AMOUNT
     */
    @JsonProperty("MAX_AMOUNT")
    public void setMAXAMOUNT(String MAXAMOUNT) {
        this.MAXAMOUNT = MAXAMOUNT;
    }

    /**
     * @return The MINAMOUNT
     */
    @JsonProperty("MIN_AMOUNT")
    public String getMINAMOUNT() {
        return MINAMOUNT;
    }

    /**
     * @param MINAMOUNT The MIN_AMOUNT
     */
    @JsonProperty("MIN_AMOUNT")
    public void setMINAMOUNT(String MINAMOUNT) {
        this.MINAMOUNT = MINAMOUNT;
    }

    /**
     * @return The DP
     */
    @JsonProperty("DP")
    public String getDP() {
        return DP;
    }

    /**
     * @param DP The DP
     */
    @JsonProperty("DP")
    public void setDP(String DP) {
        this.DP = DP;
    }

    /**
     * @return The MAXTENOR
     */
    @JsonProperty("MAX_TENOR")
    public String getMAXTENOR() {
        return MAXTENOR;
    }

    /**
     * @param MAXTENOR The MAX_TENOR
     */
    @JsonProperty("MAX_TENOR")
    public void setMAXTENOR(String MAXTENOR) {
        this.MAXTENOR = MAXTENOR;
    }

    /**
     * @return The REMARK
     */
    @JsonProperty("REMARK")
    public String getREMARK() {
        return REMARK;
    }

    /**
     * @param REMARK The REMARK
     */
    @JsonProperty("REMARK")
    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Outcome [DECISION=" + DECISION + ", MAXAMOUNT=" + MAXAMOUNT
                + ", MINAMOUNT=" + MINAMOUNT + ", DP=" + DP + ", MAXTENOR="
                + MAXTENOR + ", REMARK=" + REMARK + ", additionalProperties="
                + additionalProperties + "]";
    }

}
