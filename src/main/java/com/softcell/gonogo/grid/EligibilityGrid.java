package com.softcell.gonogo.grid;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ElgbltyID",
        "RULES",
        "AGRTD_VALUES"
})
public class EligibilityGrid {

    @JsonProperty("ElgbltyID")
    private Integer ElgbltyID;
    @JsonProperty("RULES")
    private List<GridExpressionRules> RULES = new ArrayList<GridExpressionRules>();
    @JsonProperty("AGRTD_VALUES")
    private com.softcell.gonogo.grid.AGRTDVALUES AGRTDVALUES;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The ElgbltyID
     */
    @JsonProperty("ElgbltyID")
    public Integer getElgbltyID() {
        return ElgbltyID;
    }

    /**
     * @param ElgbltyID The ElgbltyID
     */
    @JsonProperty("ElgbltyID")
    public void setElgbltyID(Integer ElgbltyID) {
        this.ElgbltyID = ElgbltyID;
    }

    /**
     * @return The RULES
     */
    @JsonProperty("RULES")
    public List<GridExpressionRules> getRULES() {
        return RULES;
    }

    /**
     * @param RULES The RULES
     */
    @JsonProperty("RULES")
    public void setRULES(List<GridExpressionRules> RULES) {
        this.RULES = RULES;
    }

    /**
     * @return The AGRTDVALUES
     */
    @JsonProperty("AGRTD_VALUES")
    public com.softcell.gonogo.grid.AGRTDVALUES getAGRTDVALUES() {
        return AGRTDVALUES;
    }

    /**
     * @param AGRTDVALUES The AGRTD_VALUES
     */
    @JsonProperty("AGRTD_VALUES")
    public void setAGRTDVALUES(com.softcell.gonogo.grid.AGRTDVALUES AGRTDVALUES) {
        this.AGRTDVALUES = AGRTDVALUES;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "EligibilityGrid [ElgbltyID=" + ElgbltyID + ", RULES=" + RULES
                + ", AGRTDVALUES=" + AGRTDVALUES + ", additionalProperties="
                + additionalProperties + "]";
    }

}
