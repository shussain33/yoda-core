package com.softcell.gonogo.model.sms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 22/11/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsServReqResLogRequest {

    @NotNull
    @JsonProperty("sMobNo")
    private String mobileNumber;


}

