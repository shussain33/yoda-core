package com.softcell.gonogo.model.sms;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class SmsServiceBaseResponse {

    @JsonProperty("bSent")
    private boolean sent;

    @JsonProperty("bDelivered")
    private boolean delivered;

    @JsonProperty("aErrors")
    private Error[] errors;

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    public Error[] getErrors() {
        return errors;
    }

    public void setErrors(Error[] errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsServiceBaseResponse [sent=");
        builder.append(sent);
        builder.append(", delivered=");
        builder.append(delivered);
        builder.append(", errors=");
        builder.append(Arrays.toString(errors));
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (delivered ? 1231 : 1237);
        result = prime * result + Arrays.hashCode(errors);
        result = prime * result + (sent ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SmsServiceBaseResponse other = (SmsServiceBaseResponse) obj;
        if (delivered != other.delivered)
            return false;
        if (!Arrays.equals(errors, other.errors))
            return false;
        if (sent != other.sent)
            return false;
        return true;
    }

}
