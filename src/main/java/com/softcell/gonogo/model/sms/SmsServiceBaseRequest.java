package com.softcell.gonogo.model.sms;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class SmsServiceBaseRequest {

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("aMobNo")
    private String[] mobileNumber;

    @JsonProperty("sInstId")
    private String institutionId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String[] mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsServiceBaseRequest [message=");
        builder.append(message);
        builder.append(", mobileNumber=");
        builder.append(Arrays.toString(mobileNumber));
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + Arrays.hashCode(mobileNumber);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SmsServiceBaseRequest other = (SmsServiceBaseRequest) obj;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (message == null) {
            if (other.message != null)
                return false;
        } else if (!message.equals(other.message))
            return false;
        if (!Arrays.equals(mobileNumber, other.mobileNumber))
            return false;
        return true;
    }

}
