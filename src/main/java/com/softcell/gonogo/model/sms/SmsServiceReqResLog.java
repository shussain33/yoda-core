package com.softcell.gonogo.model.sms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.gonogo.model.request.SmsRequest;
import com.softcell.gonogo.model.response.SmsResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by yogesh on 21/11/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="smsServiceReqResLog")
public class SmsServiceReqResLog {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sAction")
    private String action;

    @JsonProperty("sMobNo")
    private String mobileNumber;

    @JsonProperty("sEmailIds")
    private String emailIds;

    @JsonProperty("sOrgReq")
    private String orgReq;

    @JsonProperty("sOrgRes")
    private String orgRes;

    @JsonProperty("dtDateTime")
    private Date dateTime = new Date();

    @JsonProperty("oOrgReqObj")
    private SmsRequest orgReqObj;

    @JsonProperty("sSmsType")
    private GNGWorkflowConstant smsType;

    @JsonProperty("sSmsContent")
    private String smsContent;

    @JsonProperty("oOrgResObj")
    private SmsResponse orgResObj;

    @JsonProperty("aMobNo")
    private String[] mobileNumbers;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("sIdentifier")
    private String identifier;
}
