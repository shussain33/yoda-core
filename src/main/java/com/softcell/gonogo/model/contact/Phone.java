package com.softcell.gonogo.model.contact;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


/**
 * @author prateek
 */
public class Phone implements Serializable {
    @JsonProperty("sPhoneType")
    private String phoneType;

    @JsonProperty("sAreaCode")
    private String areaCode;

    @JsonProperty("sCountryCode")
    private String countryCode;

    @JsonProperty("sPhoneNumber")
    private String phoneNumber;

    /**
     * Newly Added attributes for hdfs
     */
    @JsonProperty("sExt")
    private String extension;

    /**
     * Newly Added attributes for sbfc
     */
    @JsonProperty("bVerified")
    private boolean verified;


    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Phone [phoneType=");
        builder.append(phoneType);
        builder.append(", areaCode=");
        builder.append(areaCode);
        builder.append(", countryCode=");
        builder.append(countryCode);
        builder.append(", phoneNumber=");
        builder.append(phoneNumber);
        builder.append(", extension=");
        builder.append(extension);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((areaCode == null) ? 0 : areaCode.hashCode());
        result = prime * result
                + ((countryCode == null) ? 0 : countryCode.hashCode());
        result = prime * result
                + ((extension == null) ? 0 : extension.hashCode());
        result = prime * result
                + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
        result = prime * result
                + ((phoneType == null) ? 0 : phoneType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Phone other = (Phone) obj;
        if (areaCode == null) {
            if (other.areaCode != null)
                return false;
        } else if (!areaCode.equals(other.areaCode))
            return false;
        if (countryCode == null) {
            if (other.countryCode != null)
                return false;
        } else if (!countryCode.equals(other.countryCode))
            return false;
        if (extension == null) {
            if (other.extension != null)
                return false;
        } else if (!extension.equals(other.extension))
            return false;
        if (phoneNumber == null) {
            if (other.phoneNumber != null)
                return false;
        } else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        if (phoneType == null) {
            if (other.phoneType != null)
                return false;
        } else if (!phoneType.equals(other.phoneType))
            return false;
        return true;
    }

}
