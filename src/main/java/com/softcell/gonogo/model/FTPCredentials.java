package com.softcell.gonogo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by shyam on 27/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FTPCredentials {
    private String host;
    private String user;
    private String pwd;
    private int port;
    //FTP destination path for esign NSDL
    private String tempFilePath;
    //Temporary local application server path
    private String localTempFilePath;

}
