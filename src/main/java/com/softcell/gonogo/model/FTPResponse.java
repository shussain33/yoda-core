package com.softcell.gonogo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 26/11/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FTPResponse {
    private String status;
    private Error error;

    private String fileContent;
    private byte[] bytes;
}