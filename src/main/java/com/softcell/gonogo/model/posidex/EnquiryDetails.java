package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 9/8/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EnquiryDetails {
    @JsonProperty("completeddate")
    private String completedDate;

    @JsonProperty("middlename")
    private String middleName;

    @JsonProperty("dlno")
    private String dlNo;

    @JsonProperty("registrationno")
    private String registrationNo;

    @JsonProperty("applicanttype")
    private String applicantType;

    @JsonProperty("enquirynumber")
    private String enquiryNumber;

    @JsonProperty("passportno")
    private String passportNo;

    @JsonProperty("pancardno")
    private String panCardNo;

    @JsonProperty("lastname")
    private String lastName;

    @JsonProperty("rationcardno")
    private String rationCardNo;

    @JsonProperty("firstname")
    private String firstName;

    @JsonProperty("product")
    private String product;

    @JsonProperty("aadharno")
    private String aadharNo;

    @JsonProperty("fathername")
    private String fatherName;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("applicantid")
    private String applicantId;

    @JsonProperty("voteridno")
    private String voterIdNo;
}
