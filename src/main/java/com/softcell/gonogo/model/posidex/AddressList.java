package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Softcell on 08/08/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressList {
    @JsonProperty("address")
    private String address;

    @JsonProperty("state")
    private String state;

    @JsonProperty("country")
    private String country;

    @JsonProperty("pincode")
    private String pincode;

    @JsonProperty("addresstype")
    private String addressType;
}
