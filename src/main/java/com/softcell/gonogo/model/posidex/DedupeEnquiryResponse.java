package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Softcell on 07/08/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DedupeEnquiryResponse implements Serializable{

    @JsonProperty("statusCode")
    private String statusCode;

    @JsonProperty("statusMessage")
    private String statusMessage;

    @JsonProperty("matchdetails")
    private List<MatchDetail> matchDetails;

    @JsonProperty("enquirydetails")
    private EnquiryDetails enquiryDetails;

    @JsonProperty("oError")
    private ThirdPartyException error;
}
