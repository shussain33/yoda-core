package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Softcell on 08/08/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContactList {
    @JsonProperty("contacttype")
    private String contactType;

    @JsonProperty("contactno")
    private String contactNumber;
}
