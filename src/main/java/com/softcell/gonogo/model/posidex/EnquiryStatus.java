package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 9/8/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EnquiryStatus {

    @JsonProperty("applicanttype")
    private String applicantType;

    @JsonProperty("enquirynumber")
    private String enquiryNumber;

    @JsonProperty("applicantid")
    private String applicantId;

}
