package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 9/8/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetEnquiryRequest extends PosidexRequest {

    @JsonProperty("data")
    private EnquiryStatus enquiryStatus;
}
