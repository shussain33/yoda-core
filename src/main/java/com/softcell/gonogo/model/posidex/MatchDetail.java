package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 9/8/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchDetail {
    @JsonProperty("customerId")
    private String customerId;

    @JsonProperty("prospectno")
    private String prospectNo;

    @JsonProperty("applicanttype")
    private String applicantType;

    @JsonProperty("customername")
    private String customerName;

    @JsonProperty("dlno")
    private String dlNo;

    @JsonProperty("mobile2")
    private String mobile2;

    @JsonProperty("passportno")
    private String passportNo;

    @JsonProperty("pid")
    private String pid;

    @JsonProperty("rationcardno")
    private String rationCardNo;

    @JsonProperty("address")
    private String address;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("fathername")
    private String fatherName;

    @JsonProperty("pancardno")
    private String panCardNo;

    @JsonProperty("aadharno")
    private String aadharNo;

    @JsonProperty("mobile1")
    private String mobile1;

    @JsonProperty("voteridno")
    private String voterIdNo;

}
