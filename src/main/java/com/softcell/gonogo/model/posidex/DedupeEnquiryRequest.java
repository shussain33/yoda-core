package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Softcell on 07/08/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DedupeEnquiryRequest extends PosidexRequest {

    @JsonProperty("data")
    private EnquiryData enquiryData;

}
