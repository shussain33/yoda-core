package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Softcell on 07/08/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnquiryData {

    @JsonProperty("enquirynumber")
    private String enquiryNumber;

    @JsonProperty("product")
    private String product;

    @JsonProperty("applicanttype")
    private String applicantType;

    @JsonProperty("applicantid")
    private String applicantId;

    @JsonProperty("firstname")
    private String firstName;

    @JsonProperty("middlename")
    private String middleName;

    @JsonProperty("lastname")
    private String lastName;

    @JsonProperty("fathername")
    private String fatherName;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("pancardno")
    private String panCardNumber;

    @JsonProperty("passportno")
    private String passportNumber;

    @JsonProperty("dlno")
    private String drivingLicenceNumber;

    @JsonProperty("aadharno")
    private String aadhaarNumber;

    @JsonProperty("registrationno")
    private String registrationNumber;

    @JsonProperty("rationcardno")
    private String rationCardNumber;

    @JsonProperty("addresslist")
    private List<AddressList> addressList;

    @JsonProperty("contactlist")
    private List<ContactList> contactList;

}
