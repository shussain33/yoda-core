package com.softcell.gonogo.model.posidex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 9/8/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PosidexRequest {

    @JsonProperty("name")
    private String name;

    @JsonProperty("key")
    private String key;

    @JsonProperty("action")
    private String action;
}
