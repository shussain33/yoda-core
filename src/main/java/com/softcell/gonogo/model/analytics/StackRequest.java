package com.softcell.gonogo.model.analytics;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author bhuvneshk
 */
public class StackRequest {

    /**r
     * POJO: Stack Graph JSON POJO
     */
    private static final long serialVersionUID = 1L;

    @JsonProperty("oHeader")
    @NotNull(groups = {
            Header.FetchGrp.class
    })
    @Valid
    private Header header;

    @JsonProperty("dtFrmDate")
    private String fromDate;

    @JsonProperty("dtToDate")
    private String toDate;

    @JsonProperty("sStat")
    private String status;

    @JsonProperty("sRefID")
    @NotEmpty(
            groups = {
                    StackRequest.ScoreTreeGrp.class
            }
    )
    private String referenceID;

    @JsonProperty("iSkip")
    private int skip;

    @JsonProperty("iLimit")
    private int limit;

    /**
     * Request Criteria is for getting analytic data based on different criteria
     * like products or branches
     */
    @JsonProperty("oCriteria")
    @Valid
    private RequestCriteria criteria;

    public StackRequest() {
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }


    /**
     * @return the fromDate
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public String getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the skip
     */
    public int getSkip() {
        return skip;
    }

    /**
     * @param skip the skip to set
     */
    public void setSkip(int skip) {
        this.skip = skip;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * @return the criteria
     */
    public RequestCriteria getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(RequestCriteria criteria) {
        this.criteria = criteria;
    }

    /**
     * @return
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StackRequest{");
        sb.append("header=").append(header);
        sb.append(", fromDate='").append(fromDate).append('\'');
        sb.append(", toDate='").append(toDate).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", referenceID='").append(referenceID).append('\'');
        sb.append(", skip=").append(skip);
        sb.append(", limit=").append(limit);
        sb.append(", criteria=").append(criteria);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StackRequest)) return false;
        StackRequest that = (StackRequest) o;
        return Objects.equal(getSkip(), that.getSkip()) &&
                Objects.equal(getLimit(), that.getLimit()) &&
                Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getFromDate(), that.getFromDate()) &&
                Objects.equal(getToDate(), that.getToDate()) &&
                Objects.equal(getStatus(), that.getStatus()) &&
                Objects.equal(getReferenceID(), that.getReferenceID()) &&
                Objects.equal(getCriteria(), that.getCriteria());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHeader(), getFromDate(), getToDate(), getStatus(), getReferenceID(), getSkip(), getLimit(), getCriteria());
    }

    public interface StackGrp {
    }

    public interface UpgradedStackGrp {
    }

    public interface UnrestrictedGrp {
    }

    public interface StatusTblGrp {
    }

    public interface StatusTblLgnGrp {
    }

    public interface ScoreTreeGrp {
    }
}
