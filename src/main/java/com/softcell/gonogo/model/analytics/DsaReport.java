/**
 * yogeshb11:58:03 am  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.analytics;

import com.google.common.base.Objects;

import java.util.Date;

/**
 * @author yogeshb
 */
public class DsaReport {
    private Date date;
    private String dsaID;
    private String applicationStatus;
    private double appliedAmount;
    private double approvedAmount;
    private double eligibleAmount;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDsaID() {
        return dsaID;
    }

    public void setDsaID(String dsaID) {
        this.dsaID = dsaID;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public double getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(double appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public double getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(double eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DsaReport{");
        sb.append("date=").append(date);
        sb.append(", dsaID='").append(dsaID).append('\'');
        sb.append(", applicationStatus='").append(applicationStatus).append('\'');
        sb.append(", appliedAmount=").append(appliedAmount);
        sb.append(", approvedAmount=").append(approvedAmount);
        sb.append(", eligibleAmount=").append(eligibleAmount);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DsaReport)) return false;
        DsaReport dsaReport = (DsaReport) o;
        return Objects.equal(getAppliedAmount(), dsaReport.getAppliedAmount()) &&
                Objects.equal(getApprovedAmount(), dsaReport.getApprovedAmount()) &&
                Objects.equal(getEligibleAmount(), dsaReport.getEligibleAmount()) &&
                Objects.equal(getDate(), dsaReport.getDate()) &&
                Objects.equal(getDsaID(), dsaReport.getDsaID()) &&
                Objects.equal(getApplicationStatus(), dsaReport.getApplicationStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getDate(), getDsaID(), getApplicationStatus(), getAppliedAmount(), getApprovedAmount(), getEligibleAmount());
    }
}
