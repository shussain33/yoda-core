package com.softcell.gonogo.model.analytics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

/**
 * @author bhuvneshk
 */

public class StackGraph {

    @JsonIgnore
    private String date;

    @JsonIgnore
    private String status;

    @JsonIgnore
    private int count;

    @JsonIgnore
    private int day;

    @JsonIgnore
    private int month;

    @JsonIgnore
    private int year;

    @JsonProperty("x")
    private Collection<String> x;

    @JsonProperty("y")
    private Collection<Integer> y;

    @JsonProperty("name")
    private String name;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Collection<String> getX() {
        return x;
    }

    public void setX(Collection<String> x) {
        this.x = x;
    }

    public Collection<Integer> getY() {
        return y;
    }

    public void setY(Collection<Integer> y) {
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StackGraph [date=");
        builder.append(date);
        builder.append(", status=");
        builder.append(status);
        builder.append(", count=");
        builder.append(count);
        builder.append(", day=");
        builder.append(day);
        builder.append(", month=");
        builder.append(month);
        builder.append(", year=");
        builder.append(year);
        builder.append(", x=");
        builder.append(x);
        builder.append(", y=");
        builder.append(y);
        builder.append(", name=");
        builder.append(name);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + count;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + day;
        result = prime * result + month;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((x == null) ? 0 : x.hashCode());
        result = prime * result + ((y == null) ? 0 : y.hashCode());
        result = prime * result + year;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StackGraph other = (StackGraph) obj;
        if (count != other.count)
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (day != other.day)
            return false;
        if (month != other.month)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (x == null) {
            if (other.x != null)
                return false;
        } else if (!x.equals(other.x))
            return false;
        if (y == null) {
            if (other.y != null)
                return false;
        } else if (!y.equals(other.y))
            return false;
        if (year != other.year)
            return false;
        return true;
    }

}
