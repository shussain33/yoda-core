package com.softcell.gonogo.model.analytics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.CroJustification;
import org.apache.lucene.document.StringField;

import java.util.Date;
import java.util.List;

/**
 * @author bhuvneshk
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class StackTable {

    @JsonProperty("applicationId")
    private String applicationId;

    @JsonProperty("branch")
    private String branch;

    @JsonProperty("loanAmount")
    private double loanAmount;

    @JsonProperty("approvedAmount")
    private double approvedAmount;

    @JsonProperty("income")
    private double income;

    @JsonProperty("bureauScore")
    private String bureauScore;

    @JsonProperty("applicationScore")
    private String applicationScore;

    @JsonProperty("applicationStatus")
    private String applicationStatus;

    @JsonProperty("applicantName")
    private String applicantName;

    @JsonProperty("companyName")
    private String companyName;

    @JsonProperty("customerId")
    private String customerId;

    @JsonProperty("date")
    private Date date;

    @JsonProperty("remark")
    private String remark;

    @JsonProperty("dsaId")
    private String dsaId;

    @JsonProperty("dealerId")
    private String dealerId;

    @JsonProperty("stageID")
    private String stageID;

    @JsonProperty("croJustificationList")
    private List<CroJustification> croJustificationList;

    /**
     * @return the applicationId
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * @param applicationId the applicationId to set
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * @return the loanAmount
     */
    public double getLoanAmount() {
        return loanAmount;
    }

    /**
     * @param loanAmount the loanAmount to set
     */
    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    /**
     * @return the income
     */
    public double getIncome() {
        return income;
    }

    /**
     * @param income the income to set
     */
    public void setIncome(double income) {
        this.income = income;
    }

    /**
     * @return the bureauScore
     */
    public String getBureauScore() {
        return bureauScore;
    }

    /**
     * @param bureauScore the bureauScore to set
     */
    public void setBureauScore(String bureauScore) {
        this.bureauScore = bureauScore;
    }

    /**
     * @return the applicationScore
     */
    public String getApplicationScore() {
        return applicationScore;
    }

    /**
     * @param applicationScore the applicationScore to set
     */
    public void setApplicationScore(String applicationScore) {
        this.applicationScore = applicationScore;
    }

    /**
     * @return the applicationStatus
     */
    public String getApplicationStatus() {
        return applicationStatus;
    }

    /**
     * @param applicationStatus the applicationStatus to set
     */
    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    /**
     * @return the applicantName
     */
    public String getApplicantName() {
        return applicantName;
    }

    /**
     * @param applicantName the applicantName to set
     */
    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName the companyName to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the dsaId
     */
    public String getDsaId() {
        return dsaId;
    }

    /**
     * @param dsaId the dsaId to set
     */
    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }

    /**
     * @return the dealerId
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * @param dealerId the dealerId to set
     */
    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getStageID() {
        return stageID;
    }

    public void setStageID(String stageID) {
        this.stageID = stageID;
    }

    public List<CroJustification> getCroJustificationList() {
        return croJustificationList;
    }

    public void setCroJustificationList(
            List<CroJustification> croJustificationList) {
        this.croJustificationList = croJustificationList;
    }

    public double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StackTable [applicationId=");
        builder.append(applicationId);
        builder.append(", branch=");
        builder.append(branch);
        builder.append(", loanAmount=");
        builder.append(loanAmount);
        builder.append(", approvedAmount=");
        builder.append(approvedAmount);
        builder.append(", income=");
        builder.append(income);
        builder.append(", bureauScore=");
        builder.append(bureauScore);
        builder.append(", applicationScore=");
        builder.append(applicationScore);
        builder.append(", applicationStatus=");
        builder.append(applicationStatus);
        builder.append(", applicantName=");
        builder.append(applicantName);
        builder.append(", companyName=");
        builder.append(companyName);
        builder.append(", customerId=");
        builder.append(customerId);
        builder.append(", date=");
        builder.append(date);
        builder.append(", remark=");
        builder.append(remark);
        builder.append(", dsaId=");
        builder.append(dsaId);
        builder.append(", dealerId=");
        builder.append(dealerId);
        builder.append(", stageID=");
        builder.append(stageID);
        builder.append(", croJustificationList=");
        builder.append(croJustificationList);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((applicantName == null) ? 0 : applicantName.hashCode());
        result = prime * result
                + ((applicationId == null) ? 0 : applicationId.hashCode());
        result = prime
                * result
                + ((applicationScore == null) ? 0 : applicationScore.hashCode());
        result = prime
                * result
                + ((applicationStatus == null) ? 0 : applicationStatus
                .hashCode());
        long temp;
        temp = Double.doubleToLongBits(approvedAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((branch == null) ? 0 : branch.hashCode());
        result = prime * result
                + ((bureauScore == null) ? 0 : bureauScore.hashCode());
        result = prime * result
                + ((companyName == null) ? 0 : companyName.hashCode());
        result = prime
                * result
                + ((croJustificationList == null) ? 0 : croJustificationList
                .hashCode());
        result = prime * result
                + ((customerId == null) ? 0 : customerId.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result
                + ((dealerId == null) ? 0 : dealerId.hashCode());
        result = prime * result + ((dsaId == null) ? 0 : dsaId.hashCode());
        temp = Double.doubleToLongBits(income);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(loanAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((remark == null) ? 0 : remark.hashCode());
        result = prime * result + ((stageID == null) ? 0 : stageID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StackTable other = (StackTable) obj;
        if (applicantName == null) {
            if (other.applicantName != null)
                return false;
        } else if (!applicantName.equals(other.applicantName))
            return false;
        if (applicationId == null) {
            if (other.applicationId != null)
                return false;
        } else if (!applicationId.equals(other.applicationId))
            return false;
        if (applicationScore == null) {
            if (other.applicationScore != null)
                return false;
        } else if (!applicationScore.equals(other.applicationScore))
            return false;
        if (applicationStatus == null) {
            if (other.applicationStatus != null)
                return false;
        } else if (!applicationStatus.equals(other.applicationStatus))
            return false;
        if (Double.doubleToLongBits(approvedAmount) != Double
                .doubleToLongBits(other.approvedAmount))
            return false;
        if (branch == null) {
            if (other.branch != null)
                return false;
        } else if (!branch.equals(other.branch))
            return false;
        if (bureauScore == null) {
            if (other.bureauScore != null)
                return false;
        } else if (!bureauScore.equals(other.bureauScore))
            return false;
        if (companyName == null) {
            if (other.companyName != null)
                return false;
        } else if (!companyName.equals(other.companyName))
            return false;
        if (croJustificationList == null) {
            if (other.croJustificationList != null)
                return false;
        } else if (!croJustificationList.equals(other.croJustificationList))
            return false;
        if (customerId == null) {
            if (other.customerId != null)
                return false;
        } else if (!customerId.equals(other.customerId))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (dealerId == null) {
            if (other.dealerId != null)
                return false;
        } else if (!dealerId.equals(other.dealerId))
            return false;
        if (dsaId == null) {
            if (other.dsaId != null)
                return false;
        } else if (!dsaId.equals(other.dsaId))
            return false;
        if (Double.doubleToLongBits(income) != Double
                .doubleToLongBits(other.income))
            return false;
        if (Double.doubleToLongBits(loanAmount) != Double
                .doubleToLongBits(other.loanAmount))
            return false;
        if (remark == null) {
            if (other.remark != null)
                return false;
        } else if (!remark.equals(other.remark))
            return false;
        if (stageID == null) {
            if (other.stageID != null)
                return false;
        } else if (!stageID.equals(other.stageID))
            return false;
        return true;
    }

    public static Builder  builder(){
        return new Builder();
    }

    public static class Builder{

        private StackTable stackTable = new StackTable();

        public StackTable build(){
            return this.stackTable;
        }

        public Builder applicationId(String applicationId){
            this.stackTable.setApplicationId(applicationId);
            return this;
        }

        public Builder branch(String branch){
            this.stackTable.setBranch(branch);
            return this;
        }
        public Builder loanAmount(double loanAmount){
            this.stackTable.setLoanAmount(loanAmount);
            return this;
        }

        public Builder approvedAmount(double approvedAmount){
            this.stackTable.setApprovedAmount(approvedAmount);
            return this;
        }
        public Builder income(double income){
            this.stackTable.setIncome(income);
            return this;
        }

        public Builder bureauScore(String bureauScore){
            this.stackTable.setBureauScore(bureauScore);
            return this;
        }

        public Builder applicationScore(String applicationScore){
            this.stackTable.setApplicationScore(applicationScore);
            return this;
        }

        public Builder applicationStatus(String applicationStatus){
            this.stackTable.setApplicationStatus(applicationStatus);
            return this;
        }

        public Builder applicantName(String applicantName){
            this.stackTable.setApplicantName(applicantName);
            return this;
        }


        public Builder companyName(String companyName){
               this.stackTable.setCompanyName(companyName);
               return this;
        }

        public Builder customerId(String customerId){
               this.stackTable.setCustomerId(customerId);
               return this;
        }

        public Builder date(Date date){
            this.stackTable.setDate(date);
            return this;
        }

        public Builder remark(String remark){
            this.stackTable.setRemark(remark);
            return this;
        }

        public Builder dsaId(String dsaId){
            this.stackTable.setDsaId(dsaId);
            return this;
        }

        public Builder dealerId(String dealerId){
            this.stackTable.setDealerId(dealerId);
            return this;
        }


        public Builder stageID(String stageID){
            this.stackTable.setStageID(stageID);
            return this;
        }

        public Builder croJustificationList(List<CroJustification> croJustificationList){
            this.stackTable.setCroJustificationList(croJustificationList);
            return this;
        }


    }
}
