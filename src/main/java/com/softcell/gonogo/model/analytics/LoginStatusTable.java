package com.softcell.gonogo.model.analytics;

import java.util.List;


/**
 * class serve as a wrapper over StackTable
 *
 * @author prateek
 */
public class LoginStatusTable implements Comparable<LoginStatusTable> {

    private long totalCount;
    private List<StackTable> stackTable;


    public long getTotalCount() {
        return totalCount;
    }


    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }


    public List<StackTable> getStackTable() {
        return stackTable;
    }


    public void setStackTable(List<StackTable> stackTable) {
        this.stackTable = stackTable;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((stackTable == null) ? 0 : stackTable.hashCode());
        result = prime * result + (int) (totalCount ^ (totalCount >>> 32));
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LoginStatusTable other = (LoginStatusTable) obj;
        if (stackTable == null) {
            if (other.stackTable != null)
                return false;
        } else if (!stackTable.equals(other.stackTable))
            return false;
        if (totalCount != other.totalCount)
            return false;
        return true;
    }


    @Override
    public int compareTo(LoginStatusTable loginGraph) {
        return 0;
    }

}
