package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kumar on 10/8/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApproveCoreRequest {

    private String approvedOnDate;

    private String approvedLoanAmount;

    private String expectedDisbursementDate;

    private List<String> disbursementData;

    private String locale;

    private String dateFormat;

    private String loanId;
}