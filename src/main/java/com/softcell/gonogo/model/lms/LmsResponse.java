package com.softcell.gonogo.model.lms;


import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg408 on 27/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LmsResponse {

    @JsonProperty("officeId")
    private String officeId;

    @JsonProperty("clientId")
    private String clientId;

    @JsonProperty("loanId")
    private String loanId;

    @JsonProperty("resourceId")
    private String resourceId;

    @JsonProperty("developerMessage")
    private String developerMessage;

    @JsonProperty("httpStatusCode")
    private String httpStatusCode;

    @JsonProperty("defaultUserMessage")
    private String defaultUserMessage;

    @JsonProperty("userMessageGlobalisationCode")
    private String userMessageGlobalisationCode;

    @JsonProperty("errors")
    private List<Errors> errors;

    @JsonProperty("error")
    private String error;

    @JsonProperty("error_description")
    private String error_description;

    @JsonProperty("changes")
    private Changes changes;

}
