package com.softcell.gonogo.model.lms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kumar on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SBFCLMSIntegrationRequest {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("oHeader")
    private Header oHeader;

    private RetrieveClientRequest retrieveClientRequest;

    private ClientCreationRequest clientCreationRequest;

    private CreateLoanRequest createLoanRequest;

    private ApproveCoreRequest approveCoreRequest;

    private DisburseCoreRequest disburseCoreRequest;
}