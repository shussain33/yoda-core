package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kumar on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Timeline {

    private List<Integer> submittedOnDate;

    private String submittedByUsername;

    private String submittedByFirstname;

    private String submittedByLastname;

    private List<Integer> activatedOnDate;

    private String activatedByUsername;

    private String activatedByFirstname;

    private String activatedByLastname;
}