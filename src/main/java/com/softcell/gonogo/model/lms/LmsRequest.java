package com.softcell.gonogo.model.lms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg408 on 27/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LmsRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @Id
    @JsonProperty("sRefID")
    private String refID;
}
