package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kumar on 10/8/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApproveResponse {

    private String officeId;

    private String clientId;

    private String loanId;

    private String resourceId;

    private Changes changes;

    private String developerMessage;

    private String httpStatusCode;

    private String defaultUserMessage;

    private String userMessageGlobalisationCode;

    private List<Errors> errors;

    private String error;

    private String error_description;
}