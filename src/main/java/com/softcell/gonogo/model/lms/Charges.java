package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kumar on 24/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Charges {

    private String chargeId;

    private String amount;
}