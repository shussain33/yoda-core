package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kumar on 10/8/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DisburseCoreRequest {

    private String paymentTypeId;

    private String transactionAmount;

    private String actualDisbursementDate;

    private String accountNumber;

    private String checkNumber;

    private String routingCode;

    private String receiptNumber;

    private String bankNumber;

    private String note;

    private String locale;

    private String dateFormat;

    private String loanId;
}