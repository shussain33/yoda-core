package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by kumar on 30/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "SBFCLMSIntegrationLog")
public class SBFCLMSIntegrationLog {

    private String refId;

    private String instId;

    private Date dateTime;

    private String clientId;

    private String loanId;

    private String responseMsg;

    private String status;
}
