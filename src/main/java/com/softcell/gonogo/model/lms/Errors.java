package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kumar on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Errors {

    private String developerMessage;

    private String defaultUserMessage;

    private String userMessageGlobalisationCode;

    private String parameterName;

    private String value;

    private List<Args> args;
}