package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kumar on 10/8/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Changes {

    private Status status;

    private String locale;

    private String dateFormat;

    private String approvedOnDate;

    private Object expectedDisbursementDate;

    private String approvedLoanAmount;

    private String principal;

    private String accountNumber;

    private String checkNumber;

    private String routingCode;

    private String receiptNumber;

    private String bankNumber;

    private String actualDisbursementDate;
}