package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kumar on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Status {

    private String id;

    private String code;

    private String value;

    //For Approve
    private String pendingApproval;

    private String waitingForDisbursal;

    private String active;

    private String closedObligationsMet;

    private String closedWrittenOff;

    private String closedRescheduled;

    private String closed;

    private String overpaid;

    private String awaitingCollateralClose;

    private String awaitingCollateralCloseOverpaid;

    private String disputed;
}