package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kumar on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientDetails {

    private String id;

    private String accountNo;

    private Status status;

    private SubStatus subStatus;

    private String active;

    private String firstname;

    private String lastname;

    private String displayName;

    private String emailAddress;

    private List<Integer> dateOfBirth;

    private Gender gender;

    private ClientType clientType;

    private ClientClassification clientClassification;

    private String isStaff;

    private String officeId;

    private String officeName;

    private Timeline timeline;

    private LegalForm legalForm;

    private List<String> groups;

    private ClientNonPersonDetails clientNonPersonDetails;

    private List<Integer> activationDate;

    private String mobileNo;

    private String staffId;

    private String staffName;

    private List<Integer> transactionDate;

    private String isLimit;

    private String maxLimit;

}