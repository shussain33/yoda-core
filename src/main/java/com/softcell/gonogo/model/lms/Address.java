package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by kumar on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    private String addressTypeId;

    private String street;

    private String city;

    private String countryId;

    private String stateProvinceId;

    private String postalCode;

    private String isActive;
}