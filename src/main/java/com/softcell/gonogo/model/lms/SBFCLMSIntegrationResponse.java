package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by kumar on 26/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "SBFCLMSIntegrationInfo")
public class SBFCLMSIntegrationResponse {

    private SBFCLMSIntegrationRequest sbfclmsIntegrationRequest;

    private RetrieveClientResponse retrieveClientResponse;

    private ClientCreationResponse clientCreationResponse;

    private CreateLoanResponse createLoanResponse;

    private ApproveResponse approveResponse;

    private DisburseResponse disburseResponse;

    private Date date = new Date();
}