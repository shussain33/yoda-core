package com.softcell.gonogo.model.lms;

public class LOSLMSIntegrationConstants {

    public static final String OFFICE_ID = "Office Id";
    public static final String STAFF_ID = "Staff Id";
    public static final String LEGAL_FORM_ID_PERSON = "Legal Form Id Person";
    public static final String LEGAL_FORM_ID_ENTITY = "Legal Form Id Entity";
    public static final String MALE = "MALE";
    public static final String CLIENT_TYPE_ID = "Client Type Id";
    public static final String CLIENT_CLASSIFICATION_ID = "Client Classification Id";
    public static final String CLIENTACTIVE = "Client Active";
    public static final String LOCALE = "locale";
    public static final String DATE_FORMAT = "dateFormat";
    public static final String INDIA = "India";
    public static final String IS_ACTIVE_PARAMETER = "true";
    public static final String STATUS = "Active";
    public static final String MAHARASHTRA = "Maharashtra";
    public static final String MONTHLY_REPAYMENT_TYPE = "Monthly Repayment Type";
    public static final String MONTHLY_INTEREST_REPAYMENT_TYPE = "Monthly Interest Repayment Type";
    public static final String MONTHLY_PRINCIPAL_REPAYMENT_TYPE = "Monthly Principal Repayment Type";
    public static final String FUND_ID = "Fund Id";
    public static final String IS_COBORROWER = "isCoborrower";
    public static final String LOAN_TERM_FREQ_TYPE = "Loan Term Frequency Type";
    public static final String REPAYMENT_EVERY = "Repayment Every";
    public static final String REPAYMENT_FREQ_TYPE = "Repayment Frequency Type";
    public static final String NUMBER_OF_INTEREST_REPAYMENTS = "Number Of Interest Repayments";
    public static final String INTEREST_REPAYMENT_EVERY = "Interest Repayment Every";
    public static final String INTEREST_REPAYMENT_FREQ_TYPE = "Interest Repayment Frequency Type";
    public static final String NUMBER_OF_PRINCIPAL_REPAYMENTS = "Number Of Principal Repayments";
    public static final String PRINCIPAL_REPAYMENT_EVERY = "Principal Repayment Every";
    public static final String PRINCIPAL_REPAYMENT_FREQ_TYPE = "Principal Repayment Frequency Type";
    public static final String AMORTIZATION_TYPE = "Amortization Type";
    public static final String IS_EQUAL_AMORTIZATION = "isEqual Amortization";
    public static final String INTEREST_TYPE = "Interest Type";
    public static final String INTEREST_CALCULATION_PERIOD_TYPE = "Interest Calculation Period Type";
    public static final String PARTIAL_INTEREST_CALCULATION = "Allow Partial Period Interest Calcualtion";
    public static final String REPAYMENT_STRATEGY = "Transaction Processing Strategy Id";
    public static final String LOAN_OFFICER_ID = "Loan Officer Id";
    public static final String LOAN_TYPE = "Loan Type";
    public static final String IS_TOPUP = "isTopup";
    public static final String CHEQUE = "Cheque";
    //SELFIN
    public static final String PF_INTRA = "PF(Intra)";
    public static final String PF_INTER = "PF(Inter)";
    public static final String BL_INTRA = "BL(Intra)";
    public static final String BL_INTER = "BL(Inter)";
    //TRICOLOR
    public static final String PROCESSING_FEE = "PF";
    public static final String Pre_EMI = "Pre-EMI";
    public static final String Document_Charges = "Doc_Charges";
    public static final String PDC_Amount = "PDC";

}
