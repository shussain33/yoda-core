package com.softcell.gonogo.model.lms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;


import java.util.Date;

/**
 * Created by ssg408 on 27/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "LmsCallLog")
public class LmsCallLog {

    @JsonProperty("api")
    private String api;

    @JsonProperty("refId")
    private String refId;

    @JsonProperty("callDate")
    private Date callDate = new Date();

    @JsonProperty("request")
    private LmsRequest lmsRequest;

    @JsonProperty("response")
    private LmsResponse lmsResponse;
}
