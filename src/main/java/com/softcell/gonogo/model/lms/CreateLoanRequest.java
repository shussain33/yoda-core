package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kumar on 24/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateLoanRequest {

    private String monthlyRepaymentType;

    private String monthlyInterestRepaymentType;

    private String monthlyPrincipalRepaymentType;

    private String clientId;

    private String productId;

    private List<String> disbursementData;

    private String fundId;

    private String isCoborrower;

    private String principal;

    private String loanTermFrequency;

    private String loanTermFrequencyType;

    private String numberOfRepayments;

    private String repaymentEvery;

    private String repaymentFrequencyType;

    private String numberOfInterestRepayments;

    private String interestRepaymentEvery;

    private String interestRepaymentFrequencyType;

    private String numberOfPrincipalRepayments;

    private String principalRepaymentEvery;

    private String principalRepaymentFrequencyType;

    private String interestRatePerPeriod;

    private String amortizationType;

    private String isEqualAmortization;

    private String interestType;

    private String interestCalculationPeriodType;

    private String allowPartialPeriodInterestCalcualtion;

    private String transactionProcessingStrategyId;

    private String loanOfficerId;

    private String externalId;

    private List<Charges> charges;

    private String locale;

    private String dateFormat;

    private String loanType;

    private String expectedDisbursementDate;

    private String submittedOnDate;

    private String isTopup;

    private String repaymentsStartingFromDate;
}