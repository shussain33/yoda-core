package com.softcell.gonogo.model.lms;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by kumar on 30/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SBFCLMSIntegrationLogRequest {

    @JsonProperty("sInstId")
    private String instId;

    @JsonProperty("sStartDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    SBFCLMSIntegrationLogRequest.class
            }
    )
    private Date startDate;

    @JsonProperty("sEndDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    SBFCLMSIntegrationLogRequest.class
            }
    )
    private Date endDate;
}
