package com.softcell.gonogo.model.lms;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Service
public class RequestsWithInstitutionId {

    private String institutionId;

    private RetrieveClientRequest retrieveClientRequest;

    private ClientCreationRequest clientCreationRequest;

    private CreateLoanRequest createLoanRequest;

    private ApproveCoreRequest approveCoreRequest;

    private DisburseCoreRequest disburseCoreRequest;
}
