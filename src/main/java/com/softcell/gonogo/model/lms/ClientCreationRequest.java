package com.softcell.gonogo.model.lms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by kumar on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientCreationRequest {

    private List<ClientIdentifiers> clientIdentifiers;

    private List<Address> address;

    private List<String> familyMembers;

    private String officeId;

    private String legalFormId;

    private String staffId;

    private String firstname;

    private String lastname;

    private String genderId;

    private String clientTypeId;

    private String clientClassificationId;

    private String active;

    private String mobileNo;

    private String locale;

    private String dateFormat;

    private String activationDate;

    private String submittedOnDate;

    private String dateOfBirth;

    private String savingsProductId;

    private String commAddressTypeId;

    private String fullname;
}