package com.softcell.gonogo.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.DateTime;
import org.springframework.data.annotation.*;

import java.util.Objects;

/**
 * Created by prateek on 26/2/17.
 */
public abstract class AuditEntity {


    @CreatedDate
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private DateTime createdDate;

    @CreatedBy
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String createdBy;

    @LastModifiedDate
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private DateTime updatedDate;

    @LastModifiedBy
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String updateBy;

    @Version
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long version;


    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public DateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(DateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuditEntity)) return false;
        AuditEntity that = (AuditEntity) o;
        return Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(createdBy, that.createdBy) &&
                Objects.equals(updatedDate, that.updatedDate) &&
                Objects.equals(updateBy, that.updateBy) &&
                Objects.equals(version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(createdDate, createdBy, updatedDate, updateBy, version);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("createdDate", createdDate)
                .append("createdBy", createdBy)
                .append("updatedDate", updatedDate)
                .append("updateBy", updateBy)
                .append("version", version)
                .toString();
    }
}
