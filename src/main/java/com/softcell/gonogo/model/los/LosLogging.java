package com.softcell.gonogo.model.los;

import com.softcell.gonogo.model.AuditEntity;
import com.softcell.soap.mb.nucleus.ws.los.types.LosCustomerResponse;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Document(collection = "LosGNGLogging")
public class LosLogging extends AuditEntity {


    private String institutionId;
    private String refId;
    private Date insertDate;
    private String request;
    private String response;
    private String losStage;
    private LosCustomerResponse losCustomerResponse;
    private LosException exception;
    private String rawResponse;



    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }


    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }


    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }


    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }


    /**
     * @return the request
     */
    public String getRequest() {
        return request;
    }


    /**
     * @param request the request to set
     */
    public void setRequest(String request) {
        this.request = request;
    }


    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }


    /**
     * @param response the response to set
     */
    public void setResponse(String response) {
        this.response = response;
    }


    /**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }


    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }


    /**
     * @return the exception
     */
    public LosException getException() {
        return exception;
    }


    /**
     * @param exception the exception to set
     */
    public void setException(LosException exception) {
        this.exception = exception;
    }


    public LosCustomerResponse getLosCustomerResponse() {
        return losCustomerResponse;
    }


    public void setLosCustomerResponse(LosCustomerResponse losCustomerResponse) {
        this.losCustomerResponse = losCustomerResponse;
    }


    public String getLosStage() {
        return losStage;
    }


    public void setLosStage(String losStage) {
        this.losStage = losStage;
    }

    public String getRawResponse() {
        return rawResponse;
    }

    public void setRawResponse(String rawResponse) {
        this.rawResponse = rawResponse;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LosLogging [institutionId=");
        builder.append(institutionId);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append(", request=");
        builder.append(request);
        builder.append(", response=");
        builder.append(response);
        builder.append(", losCustomerResponse=");
        builder.append(losCustomerResponse);
        builder.append(", exception=");
        builder.append(exception);
        builder.append("]");
        return builder.toString();
    }

    public class LosException {
        private String devMessage;
        private String cause;

        /**
         * @return the devMessage
         */
        public String getDevMessage() {
            return devMessage;
        }

        /**
         * @param devMessage the devMessage to set
         */
        public void setDevMessage(String devMessage) {
            this.devMessage = devMessage;
        }

        /**
         * @return the cause
         */
        public String getCause() {
            return cause;
        }

        /**
         * @param cause the cause to set
         */
        public void setCause(String cause) {
            this.cause = cause;
        }


    }


}
