package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Professionaldetails {

	private int mnthswithcurrrentemployer;

    private int grossmnthlyincome;

    private int netmonthlyincome;

    private String email;

    private int additionalmonthlyincome;

    private String employmenttype;

    private String saathidowloaded;

    private String employername;

    private int yrswithcurrentemployer;

    private int totalfamilyincome;


}
