package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KarzaDrivingLicense {

                private String father_husband_name;

                private String status;

                private String s_kycnumber;

                private String address;

                private String validity;

                private String issuedate;

                private String name;

                private String dl_number;

                private String s_addrtype;

                private String dateofbirth;

                private String iscallmode;

                private String covdetails;
}
