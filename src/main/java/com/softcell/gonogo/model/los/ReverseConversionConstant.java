package com.softcell.gonogo.model.los;

public class ReverseConversionConstant {

	public interface ConversionConstant {
		public static final String GENDER 				      = "GENDER";                          
		public static final String RELATION_TYPE 		      = "RELATION_TYPE";                  
		public static final String KEY_PERSON_RELATION 	      = "KEY_PERSON_RELATION";             
		public static final String NOMINEE_RALATION_TYPE      = "NOMINEE_RALATION_TYPE";           
		public static final String PHONE_TYPE	 		      = "PHONE_TYPE";                    
		public static final String ID_TYPE 				      = "ID_TYPE";
		public static final String ADDRESS_TYPE 		      = "ADDRESS_TYPE";                  
		public static final String ADDRESS_STATE 		      = "ADDRESS_STATE";                 
		public static final String REQUEST_TYPE 		      = "REQUEST_TYPE";                    
		public static final String INQUIRY_STAGE 		      = "INQUIRY_STAGE";                   
		public static final String LOAN_TYPE 			      = "LOAN_TYPE";                       
		public static final String ADDRESS_RESIDENCE_CODE     = "ADDRESS_RESIDENCE_CODE";
		public static final String FREQUENCY_OF_PAYMENT       = "FREQUENCY_OF_PAYMENT";      
		public static final String FINANCIAL_PURPOSE 	      = "FINANCIAL_PURPOSE";         
		public static final String ENQUIRY_TYPE 		      = "ENQUIRY_TYPE";              
		public static final String ADDITIONAL_NAME_TYPE       = "ADDITIONAL_NAME_TYPE";      
		public static final String MARITAL_STATUS 		      = "MARITAL_STATUS";            
		public static final String CREDIT_TYPE 			      = "CREDIT_TYPE";               
		public static final String LEGAL_CONSTITUTION 	      = "LEGAL_CONSTITUTION";        
		public static final String ASSET_CLASSIFICATION       = "ASSET_CLASSIFICATION";      
		public static final String RELATED_TYPE 		      = "RELATED_TYPE";              
		public static final String RELATIONSHIP_TYPE 	      = "RELATIONSHIP_TYPE";         
		public static final String SUIT_FILED_STATUS 	      = "SUIT_FILED_STATUS";         
		public static final String ACCOUNT_TYPE 		      = "LOAN_TYPE";              
		public static final String OWNERSHIP_INDICATOR 	      = "OWNERSHIP_INDICATOR";       
		public static final String COLLATERAL_TYPE 		      = "COLLATERAL_TYPE";           
		public static final String OCCUPATION_CODE 		      = "OCCUPATION_CODE";           
		public static final String INCOME 				      = "INCOME";                    
		public static final String GROSS_INCOME 		      = "GROSS_INCOME";              
		public static final String MONTHLY_ANNUALLY_INDICATOR = "MONTHLY_ANNUALLY_INDICATOR";
		public static final String SCORE_NAME 			      = "SCORE_NAME";
		public static final String SCORE_VERSION 		      = "SCORE_VERSION";
		public static final String SCORE_CARD_NAME			  =  "SCORE_CARD_NAME";	
		public static final String REASON_AND_EXCLUSION_CODE  =  "REASON_AND_EXCLUSION_CODE";
		public static final Object WRITTEN_OFF_AND_SETTLED_STATUS = "WRITTEN_OFF_AND_SETTLED_STATUS";
		
		public static final String EXCLUSION_CODE1 		      = "EXCLUSION_CODE1";
		public static final String EXCLUSION_CODE2 		      = "EXCLUSION_CODE2";
		public static final String EXCLUSION_CODE3 		      = "EXCLUSION_CODE3";
		public static final String EXCLUSION_CODE4 		      = "EXCLUSION_CODE4";
		public static final String EXCLUSION_CODE5 		      = "EXCLUSION_CODE5";
		public static final String EXCLUSION_CODE6 		      = "EXCLUSION_CODE6";
		public static final String EXCLUSION_CODE7 		      = "EXCLUSION_CODE7";
		public static final String EXCLUSION_CODE8 		      = "EXCLUSION_CODE8";
		public static final String EXCLUSION_CODE9 		      = "EXCLUSION_CODE9";
		public static final String EXCLUSION_CODE10 	      = "EXCLUSION_CODE10";
		public static final String REASON_CODE1 		      = "REASON_CODE1";
		public static final String REASON_CODE2 		      = "REASON_CODE2";
		public static final String REASON_CODE3 		      = "REASON_CODE3";
		public static final String REASON_CODE4 		      = "REASON_CODE4";
		public static final String REASON_CODE5 		      = "REASON_CODE5";
		public static final String WILLFUL_DEFAULT_STATUS     = "WILLFUL_DEFAULT_STATUS";
		public static final String ACCOUNT_STATUS 		      = "ACCOUNT_STATUS";
		public static final String ACCOUNT_HOLDER_TYPE 	      = "ACCOUNT_HOLDER_TYPE";
		public static final String ENQUIRY_REASON 		      = "ENQUIRY_REASON";
		public static final String EMPLOYMENT_STATUS 	      = "EMPLOYMENT_STATUS" ;
		
		
		//New codes for Detect product
		public static final String 	SUBJECT_TYPE                  =    	"SUBJECT_TYPE" ;                
		public static final String 	INCIDENT_TYPE                 =    	"INCIDENT_TYPE" ;               
		public static final String 	INCIDENT_RELATIONSHIP         =    	"INCIDENT_RELATIONSHIP" ;      
		public static final String 	BANK_RELATIONSHIP             =    	"BANK_RELATIONSHIP" ;       
		public static final String 	AREA_OF_OPERATION_TYPE        =    	"AREA_OF_OPERATION_TYPE" ;      
		public static final String 	AREA_OF_OPERATION_SUB_TYPE    =    	"AREA_OF_OPERATION_SUB_TYPE" ;  
		public static final String 	INCIDENT_STATUS               =    	"INCIDENT_STATUS" ;             
		public static final String 	MODUS_OPERANDI                =    	"MODUS_OPERANDI" ;              
		public static final String 	ASSET_TYPE                    =    	"ASSET_TYPE" ;                  
		public static final String 	COMPLAINT_TO_LAW_ENFORCEMENT  =    	"COMPLAINT_TO_LAW_ENFORCEMENT" ;
		public static final String 	ENQUIRY_PURPOSE               =    	"ENQUIRY_PURPOSE" ;             
	}
}
