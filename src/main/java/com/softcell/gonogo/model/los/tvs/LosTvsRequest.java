package com.softcell.gonogo.model.los.tvs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LosTvsRequest {

    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefID")
    private String refID;

    private String stage;

    private String referenceno;

    private String vendorid;

}
