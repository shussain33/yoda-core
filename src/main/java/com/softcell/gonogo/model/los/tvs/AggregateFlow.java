package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AggregateFlow {

	private String totalDebitMonth1;

    private String totalCreditMonth0;

    private String totalDebitMonth0;

    private String totalCreditMonth2;

    private String totalDebitMonth2;

    private String totalCreditMonth1;


}
