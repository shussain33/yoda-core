package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Decision {

		private String crouserid;

	    private String crostatus;

	    private int croapprovedamount;

	    private int crotenor;

	    private int croappliedamount;

}
