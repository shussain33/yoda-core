package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class MultibureauCIBIL {
	private AddressList[] addressList;

    private EnquiryList[] enquiryList;

    private AccountList[] accountList;

    private PhoneList[] phoneList;

    private Name name;

    private SecondaryMatches[] secondaryMatches;

    private IdList[] idList;

    private EmploymentList[] employmentList;

    private Header header;

    private com.softcell.gonogo.model.los.tvs.LosDto.ScoreList[] scoreList;


}

	

