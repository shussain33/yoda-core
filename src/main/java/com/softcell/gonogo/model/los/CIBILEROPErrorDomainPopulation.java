package com.softcell.gonogo.model.los;

import com.softcell.gonogo.model.core.request.scoring.CibilErrorResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class CIBILEROPErrorDomainPopulation {
	private static Logger logger_= LoggerFactory.getLogger(CIBILEROPErrorDomainPopulation.class);
	
	public List<CIBILEROPErrorDomain>  cibilEROPErrorDomainGenerator(CibilErrorResponse cibilErrorResponse,String srNo,
																	 String soaSourceName, String memberReferenceNumber, Long institutionId, Date enquiryDate){
		return cibilEROPErrorDomainGenerator_(cibilErrorResponse,srNo,soaSourceName,memberReferenceNumber,institutionId,enquiryDate);
	}
	
	private List<CIBILEROPErrorDomain> cibilEROPErrorDomainGenerator_(CibilErrorResponse cibilErrorResponse,String srNo,
																	  String soaSourceName, String memberReferenceNo, Long institutionId,Date enquiryDate){

		Long manualUploadBatchId = Long.MIN_VALUE;
		Long requestId = Long.MIN_VALUE;

		boolean tempLosErrorCodeFlag = false;
		String fileName = memberReferenceNo;

		List<CIBILEROPErrorDomain> listOfDomain = new ArrayList<CIBILEROPErrorDomain>();
		
		
		CIBILEROPErrorDomain cibileropErrorDomain= new CIBILEROPErrorDomain(srNo, soaSourceName, fileName, manualUploadBatchId, requestId, fileName);
		listOfDomain.add(cibileropErrorDomain);
		
		if(cibilErrorResponse !=null){
			cibileropErrorDomain.setEnquiryDate_(DateUtils.getFormattedDate(enquiryDate, "ddMMyyyy"));
			if(StringUtils.isBlank(cibileropErrorDomain.getEnquiryDate_())){
				cibileropErrorDomain.setEnquiryDate_(DateUtils.getFormattedDate(new Date(), "ddMMyyyy"));
			}
			cibileropErrorDomain.setDateProcessed_(cibilErrorResponse.getDateProcessed());
			cibileropErrorDomain.setTimeProcessed_(cibilErrorResponse.getTimeProcessed());
			cibileropErrorDomain.setInvalidFieldLength_(cibilErrorResponse.getInvFieldLength());
			cibileropErrorDomain.setInvalidTotalLength_(cibilErrorResponse.getInvTotalLength());
			cibileropErrorDomain.setInvalidVersion_(cibilErrorResponse.getInvVersion());
			cibileropErrorDomain.setInvalidEnquiryPurpose_(cibilErrorResponse.getInvEnquiryPurpose());
			cibileropErrorDomain.setInvalidEnquiryAmount_(cibilErrorResponse.getInvEnquiryAmount());
			cibileropErrorDomain.setInvalidEnquiryMemberUserId_(cibilErrorResponse.getInvUserPwd());
			cibileropErrorDomain.setRequiredEnquirySegmentMissing_(cibilErrorResponse.getReqSegMissing());
			cibileropErrorDomain.setCibilSystemError_(cibilErrorResponse.getCibilSystemError());
			cibileropErrorDomain.setInvalidSegmentTag_(cibilErrorResponse.getInvSegTag());
			cibileropErrorDomain.setInvalidSegmentOrder_(cibilErrorResponse.getInvSegOrder());
			cibileropErrorDomain.setInvalidFieldTagOrder_(cibilErrorResponse.getInvFieldTagOrder());
			cibileropErrorDomain.setRequestedRespSizeExceeded_(cibilErrorResponse.getReqResSizeExceeded());
			cibileropErrorDomain.setInvalidInputOutputMedia_(cibilErrorResponse.getInvIpOpMedia());
			cibileropErrorDomain.setOutputWriteFlag_("0");
			cibileropErrorDomain.setOutputWriteTime_(DateUtils.getFormattedDate(new Date(), "ddMMyyyy HH:mm:ss"));
			
		}

		int counter = 0;
		if(cibilErrorResponse!=null && cibilErrorResponse.getInvEnqDataList()!=null && cibilErrorResponse.getInvEnqDataList().size() > 0){
			for(String tempString : cibilErrorResponse.getInvEnqDataList()){
				if(counter == 0){
					listOfDomain.get(0).setInvalidEnquiryData_(tempString);
					if (tempLosErrorCodeFlag) {
						if (StringUtils.equalsIgnoreCase(soaSourceName, "LOS")) {
							listOfDomain.get(0).setErrorTypeCode_("00");
						} else {
							listOfDomain.get(0).setErrorTypeCode_(cibilErrorResponse.getErrorDomainList().get(0).getErrorCode());
						}
					}
					else{
						listOfDomain.get(0).setErrorTypeCode_(cibilErrorResponse.getErrorDomainList().get(0).getErrorCode());
					}
					listOfDomain.get(0).setErrorType_(cibilErrorResponse.getErrorDomainList().get(0).getErrorDescription());
				} else {
					CIBILEROPErrorDomain localCibileropErrorDomain= new CIBILEROPErrorDomain(srNo, soaSourceName, memberReferenceNo, manualUploadBatchId, requestId, fileName);
					localCibileropErrorDomain.setInvalidEnquiryData_(tempString);
					if (tempLosErrorCodeFlag) {
						if (StringUtils.equalsIgnoreCase(soaSourceName, "LOS")) {
							localCibileropErrorDomain.setErrorTypeCode_("00");
						} else {
							localCibileropErrorDomain.setErrorTypeCode_(cibilErrorResponse.getErrorDomainList().get(counter).getErrorCode());
						}
					}else{
						localCibileropErrorDomain.setErrorTypeCode_(cibilErrorResponse.getErrorDomainList().get(counter).getErrorCode());	
					}
					localCibileropErrorDomain.setErrorType_(cibilErrorResponse.getErrorDomainList().get(counter).getErrorDescription());
					listOfDomain.add(localCibileropErrorDomain);
				}
				counter++;
			}
		}
		
		if(cibilErrorResponse!=null && cibilErrorResponse.getReqFieldMissingList()!=null && cibilErrorResponse.getReqFieldMissingList().size() > 0){
			for (int index = 0; index < cibilErrorResponse.getReqFieldMissingList().size(); index++) {
				if(counter==0){
					listOfDomain.get(0).setMissingRequiredField_(cibilErrorResponse.getReqFieldMissingList().get(index));
					counter++;
				}else{
					if(index < counter){
						listOfDomain.get(index).setMissingRequiredField_(cibilErrorResponse.getReqFieldMissingList().get(index));
					}else{
						CIBILEROPErrorDomain localCibileropErrorDomain= new CIBILEROPErrorDomain(srNo, soaSourceName, memberReferenceNo, manualUploadBatchId, requestId, fileName);
						localCibileropErrorDomain.setMissingRequiredField_(cibilErrorResponse.getReqFieldMissingList().get(index));
						if(tempLosErrorCodeFlag){
							if(StringUtils.equalsIgnoreCase(soaSourceName, "LOS")){
								localCibileropErrorDomain.setErrorTypeCode_("00");	
							}
							else{
							localCibileropErrorDomain.setErrorTypeCode_(cibilErrorResponse.getErrorDomainList().get(index).getErrorCode());
							}
						}
						else
						{
							localCibileropErrorDomain.setErrorTypeCode_(cibilErrorResponse.getErrorDomainList().get(index).getErrorCode());
						}
						localCibileropErrorDomain.setErrorType_(cibilErrorResponse.getErrorDomainList().get(index).getErrorDescription());
						listOfDomain.add(localCibileropErrorDomain);
					}
				}
			}
		}
		
		
		logger_.warn(">>    cibilEROPErrorDomainGenerator_()  :  [  return :  { cibileropErrorDomain }]");
		return listOfDomain;
	}
}
