package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CIBILEROPErrorDomain implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Expose
    @SerializedName("SRNO")
    private String srNo_;
    @Expose
    @SerializedName("SOA_SOURCE_NAME")
    private String soaSourceName_;
    @Expose
    @SerializedName("MEMBER_REFERENCE_NUMBER")
    private String memberReferenceNo_;
    private Long batchId_;
    private Long requestId_;
    private String fileName_;
    @Expose
    @SerializedName("ENQUIRY_DATE")
    private String enquiryDate_;
    @Expose
    @SerializedName("SEGMENT_TAG")
    private String segmentTag_;
    @Expose
    @SerializedName("TYPE_OF_ERROR")
    private String typeOfError_;
    @Expose
    @SerializedName("DATE_PROCESSED")
    private String dateProcessed_;
    @Expose
    @SerializedName("TIME_PROCESSED")
    private String timeProcessed_;
    @Expose
    @SerializedName("ENQUIRY_MEMBER_CODE_ID")
    private String enquiryMemberCodeId_;
    @Expose
    @SerializedName("ERROR_TYPE_CODE")
    private String errorTypeCode_;
    @Expose
    @SerializedName("ERROR_TYPE")
    private String errorType_;
    @Expose
    @SerializedName("DATA")
    private String data_;
    @Expose
    @SerializedName("SEGMENT")
    private String segment_;
    @Expose
    @SerializedName("SPECIFIED")
    private String specified_;
    @Expose
    @SerializedName("EXPECTED")
    private String expected_;
    @Expose
    @SerializedName("INVALID_VERSION")
    private String invalidVersion_;
    @Expose
    @SerializedName("INVALID_FIELD_LENGTH")
    private String invalidFieldLength_;
    @Expose
    @SerializedName("INVALID_TOTAL_LENGTH")
    private String invalidTotalLength_;
    @Expose
    @SerializedName("INVALID_ENQUIRY_PURPOSE")
    private String invalidEnquiryPurpose_;
    @Expose
    @SerializedName("INVALID_ENQUIRY_AMOUNT")
    private String invalidEnquiryAmount_;
    @Expose
    @SerializedName("INVALID_ENQUIRY_MEMBER_USER_ID")
    private String invalidEnquiryMemberUserId_;
    @Expose
    @SerializedName("REQUIRED_ENQ_SEG_MISSING")
    private String requiredEnquirySegmentMissing_;
    @Expose
    @SerializedName("INVALID_ENQUIRY_DATA")
    private String invalidEnquiryData_;
    @Expose
    @SerializedName("CIBIL_SYSTEM_ERROR")
    private String cibilSystemError_;
    @Expose
    @SerializedName("INVALID_SEGMENT_TAG")
    private String invalidSegmentTag_;
    @Expose
    @SerializedName("INVALID_SEGMENT_ORDER")
    private String invalidSegmentOrder_;
    @Expose
    @SerializedName("INVALID_FIELD_TAG_ORDER")
    private String invalidFieldTagOrder_;
    @Expose
    @SerializedName("MISSING_REQUIRED_FIELD")
    private String missingRequiredField_;
    @Expose
    @SerializedName("REQUESTED_RESP_SIZE_EXCEEDED")
    private String requestedRespSizeExceeded_;
    @Expose
    @SerializedName("INVALID_INPUT_OUTPUT_MEDIA")
    private String invalidInputOutputMedia_;
    @Expose
    @SerializedName("OUTPUT_WRITE_FLAG")
    private String outputWriteFlag_;
    @Expose
    @SerializedName("OUTPUT_WRITE_TIME")
    private String outputWriteTime_;
    @Expose
    @SerializedName("OUTPUT_READ_TIME")
    private String outputReadTime_;


    public CIBILEROPErrorDomain(String srNo_, String soaSourceName_, String memberReferenceNo_, Long batchId_, Long requestId_, String fileName_) {
        this.srNo_ = srNo_;
        this.soaSourceName_ = soaSourceName_;
        this.memberReferenceNo_ = memberReferenceNo_;
        this.batchId_ = batchId_;
        this.requestId_ = requestId_;
        this.fileName_ = fileName_;
    }

    public String getSrNo_() {
        return srNo_;
    }

    public void setSrNo_(String srNo_) {
        this.srNo_ = srNo_;
    }

    public String getSoaSourceName_() {
        return soaSourceName_;
    }

    public void setSoaSourceName_(String soaSourceName_) {
        this.soaSourceName_ = soaSourceName_;
    }

    public String getMemberReferenceNo_() {
        return memberReferenceNo_;
    }

    public void setMemberReferenceNo_(String memberReferenceNo_) {
        this.memberReferenceNo_ = memberReferenceNo_;
    }

    public Long getBatchId_() {
        return batchId_;
    }

    public void setBatchId_(Long batchId_) {
        this.batchId_ = batchId_;
    }

    public Long getRequestId_() {
        return requestId_;
    }

    public void setRequestId_(Long requestId_) {
        this.requestId_ = requestId_;
    }

    public String getEnquiryDate_() {
        return enquiryDate_;
    }

    public void setEnquiryDate_(String enquiryDate_) {
        this.enquiryDate_ = enquiryDate_;
    }

    public String getSegmentTag_() {
        return segmentTag_;
    }

    public void setSegmentTag_(String segmentTag_) {
        this.segmentTag_ = segmentTag_;
    }

    public String getTypeOfError_() {
        return typeOfError_;
    }

    public void setTypeOfError_(String typeOfError_) {
        this.typeOfError_ = typeOfError_;
    }

    public String getDateProcessed_() {
        return dateProcessed_;
    }

    public void setDateProcessed_(String dateProcessed_) {
        this.dateProcessed_ = dateProcessed_;
    }

    public String getTimeProcessed_() {
        return timeProcessed_;
    }

    public void setTimeProcessed_(String timeProcessed_) {
        this.timeProcessed_ = timeProcessed_;
    }

    public String getEnquiryMemberCodeId_() {
        return enquiryMemberCodeId_;
    }

    public void setEnquiryMemberCodeId_(String enquiryMemberCodeId_) {
        this.enquiryMemberCodeId_ = enquiryMemberCodeId_;
    }

    public String getErrorTypeCode_() {
        return errorTypeCode_;
    }

    public void setErrorTypeCode_(String errorTypeCode_) {
        this.errorTypeCode_ = errorTypeCode_;
    }

    public String getErrorType_() {
        return errorType_;
    }

    public void setErrorType_(String errorType_) {
        this.errorType_ = errorType_;
    }

    public String getData_() {
        return data_;
    }

    public void setData_(String data_) {
        this.data_ = data_;
    }

    public String getSegment_() {
        return segment_;
    }

    public void setSegment_(String segment_) {
        this.segment_ = segment_;
    }

    public String getSpecified_() {
        return specified_;
    }

    public void setSpecified_(String specified_) {
        this.specified_ = specified_;
    }

    public String getExpected_() {
        return expected_;
    }

    public void setExpected_(String expected_) {
        this.expected_ = expected_;
    }

    public String getInvalidVersion_() {
        return invalidVersion_;
    }

    public void setInvalidVersion_(String invalidVersion_) {
        this.invalidVersion_ = invalidVersion_;
    }

    public String getInvalidFieldLength_() {
        return invalidFieldLength_;
    }

    public void setInvalidFieldLength_(String invalidFieldLength_) {
        this.invalidFieldLength_ = invalidFieldLength_;
    }

    public String getInvalidTotalLength_() {
        return invalidTotalLength_;
    }

    public void setInvalidTotalLength_(String invalidTotalLength_) {
        this.invalidTotalLength_ = invalidTotalLength_;
    }

    public String getInvalidEnquiryPurpose_() {
        return invalidEnquiryPurpose_;
    }

    public void setInvalidEnquiryPurpose_(String invalidEnquiryPurpose_) {
        this.invalidEnquiryPurpose_ = invalidEnquiryPurpose_;
    }

    public String getInvalidEnquiryAmount_() {
        return invalidEnquiryAmount_;
    }

    public void setInvalidEnquiryAmount_(String invalidEnquiryAmount_) {
        this.invalidEnquiryAmount_ = invalidEnquiryAmount_;
    }

    public String getInvalidEnquiryMemberUserId_() {
        return invalidEnquiryMemberUserId_;
    }

    public void setInvalidEnquiryMemberUserId_(String invalidEnquiryMemberUserId_) {
        this.invalidEnquiryMemberUserId_ = invalidEnquiryMemberUserId_;
    }

    public String getRequiredEnquirySegmentMissing_() {
        return requiredEnquirySegmentMissing_;
    }

    public void setRequiredEnquirySegmentMissing_(
            String requiredEnquirySegmentMissing_) {
        this.requiredEnquirySegmentMissing_ = requiredEnquirySegmentMissing_;
    }

    public String getInvalidEnquiryData_() {
        return invalidEnquiryData_;
    }

    public void setInvalidEnquiryData_(String invalidEnquiryData_) {
        this.invalidEnquiryData_ = invalidEnquiryData_;
    }

    public String getCibilSystemError_() {
        return cibilSystemError_;
    }

    public void setCibilSystemError_(String cibilSystemError_) {
        this.cibilSystemError_ = cibilSystemError_;
    }

    public String getInvalidSegmentTag_() {
        return invalidSegmentTag_;
    }

    public void setInvalidSegmentTag_(String invalidSegmentTag_) {
        this.invalidSegmentTag_ = invalidSegmentTag_;
    }

    public String getInvalidSegmentOrder_() {
        return invalidSegmentOrder_;
    }

    public void setInvalidSegmentOrder_(String invalidSegmentOrder_) {
        this.invalidSegmentOrder_ = invalidSegmentOrder_;
    }

    public String getInvalidFieldTagOrder_() {
        return invalidFieldTagOrder_;
    }

    public void setInvalidFieldTagOrder_(String invalidFieldTagOrder_) {
        this.invalidFieldTagOrder_ = invalidFieldTagOrder_;
    }

    public String getMissingRequiredField_() {
        return missingRequiredField_;
    }

    public void setMissingRequiredField_(String missingRequiredField_) {
        this.missingRequiredField_ = missingRequiredField_;
    }

    public String getRequestedRespSizeExceeded_() {
        return requestedRespSizeExceeded_;
    }

    public void setRequestedRespSizeExceeded_(String requestedRespSizeExceeded_) {
        this.requestedRespSizeExceeded_ = requestedRespSizeExceeded_;
    }

    public String getInvalidInputOutputMedia_() {
        return invalidInputOutputMedia_;
    }

    public void setInvalidInputOutputMedia_(String invalidInputOutputMedia_) {
        this.invalidInputOutputMedia_ = invalidInputOutputMedia_;
    }

    public String getOutputWriteFlag_() {
        return outputWriteFlag_;
    }

    public void setOutputWriteFlag_(String outputWriteFlag_) {
        this.outputWriteFlag_ = outputWriteFlag_;
    }

    public String getOutputWriteTime_() {
        return outputWriteTime_;
    }

    public void setOutputWriteTime_(String outputWriteTime_) {
        this.outputWriteTime_ = outputWriteTime_;
    }

    public String getOutputReadTime_() {
        return outputReadTime_;
    }

    public void setOutputReadTime_(String outputReadTime_) {
        this.outputReadTime_ = outputReadTime_;
    }


    @Override
    public String toString() {
        return "CIBILEROPErrorDomain [srNo_=" + srNo_ + ", soaSourceName_="
                + soaSourceName_ + ", memberReferenceNo_=" + memberReferenceNo_
                + ", batchId_=" + batchId_ + ", requestId_=" + requestId_
                + ", enquiryDate_=" + enquiryDate_ + ", segmentTag_="
                + segmentTag_ + ", typeOfError_=" + typeOfError_
                + ", dateProcessed_=" + dateProcessed_ + ", timeProcessed_="
                + timeProcessed_ + ", enquiryMemberCodeId_="
                + enquiryMemberCodeId_ + ", errorTypeCode_=" + errorTypeCode_
                + ", errorType_=" + errorType_ + ", data_=" + data_
                + ", segment_=" + segment_ + ", specified_=" + specified_
                + ", expected_=" + expected_ + ", invalidVersion_="
                + invalidVersion_ + ", invalidFieldLength_="
                + invalidFieldLength_ + ", invalidTotalLength_="
                + invalidTotalLength_ + ", invalidEnquiryPurpose_="
                + invalidEnquiryPurpose_ + ", invalidEnquiryAmount_="
                + invalidEnquiryAmount_ + ", invalidEnquiryMemberUserId_="
                + invalidEnquiryMemberUserId_
                + ", requiredEnquirySegmentMissing_="
                + requiredEnquirySegmentMissing_ + ", invalidEnquiryData_="
                + invalidEnquiryData_ + ", cibilSystemError_="
                + cibilSystemError_ + ", invalidSegmentTag_="
                + invalidSegmentTag_ + ", invalidSegmentOrder_="
                + invalidSegmentOrder_ + ", invalidFieldTagOrder_="
                + invalidFieldTagOrder_ + ", missingRequiredField_="
                + missingRequiredField_ + ", requestedRespSizeExceeded_="
                + requestedRespSizeExceeded_ + ", invalidInputOutputMedia_="
                + invalidInputOutputMedia_ + ", outputWriteFlag_="
                + outputWriteFlag_ + ", outputWriteTime_=" + outputWriteTime_
                + ", outputReadTime_=" + outputReadTime_ + "]";
    }

    public String getFileName_() {
        return fileName_;
    }

    public void setFileName_(String fileName_) {
        this.fileName_ = fileName_;
    }
}                    