package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class IndvResponses {

	    private Summary summary;

	    private SecSummary secSummary;

	    private PrimarySummary primarySummary;


}
