package com.softcell.gonogo.model.los.tvs;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KarzaVoterID {

        private String rlnname;

        private String dealerid;

        private String houseno;

        private String state;

        private String crold;

        private String s_action;

        private String institutionid;

        private String id;

        private String last_update;

        private String age;

        private String name;

        private String gender;

        private String applicationsource;

        private String psname;

        private String ps_lng;

        private String district;

        private String slnoinpart;

        private String rlntype;

        private String requesttype;

        private String partname;

        private String pcname;

        private String status;

        private String sectionno;

        private String s_kycid;

        private String epicno;

        private String dsald;

        private String ps_lat;

        private String applicationid;

        private String product;

        private String acname;

        private String acno;

        private String stcode;

        private String dob;

        private String sourceid;

        private String iscallmade;

        private String voteridno;
}
