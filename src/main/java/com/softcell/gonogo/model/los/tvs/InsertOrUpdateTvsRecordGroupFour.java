package com.softcell.gonogo.model.los.tvs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsertOrUpdateTvsRecordGroupFour {
	private BankDetails bankDetails;

    private InsuranceDetails insuranceDetails;

    private Enach enach;

    private SerialNumber serialNumber;

    private ExtendedWarranty extendedWarranty;

    @JsonProperty("CDLos")
    private CDLos CDLos;

    @JsonProperty("EMudra")
    private EMudra EMudra;

}

