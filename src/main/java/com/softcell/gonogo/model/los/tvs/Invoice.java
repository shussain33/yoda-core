package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

	private String invoiceserialnumber;

    private String invoicedate;

    private String invoicenumber;

    private String invoicetype;


}
