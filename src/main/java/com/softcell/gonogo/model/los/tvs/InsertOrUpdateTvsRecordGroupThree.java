package com.softcell.gonogo.model.los.tvs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsertOrUpdateTvsRecordGroupThree
{
    private PostIPA postIPA;

    private ProductInformation productInformation;

    @JsonProperty("CDLos")
    private CDLos CDLos;

}
