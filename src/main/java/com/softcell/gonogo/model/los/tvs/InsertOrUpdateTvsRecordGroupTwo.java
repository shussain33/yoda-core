package com.softcell.gonogo.model.los.tvs;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InsertOrUpdateTvsRecordGroupTwo {

    private Application application;

    private CreditVidya creditVidya;

    private Cro cro;

    private Posidex posidex;

    private WelcomeScreen welcomeScreen;

    private DoStages doStages;

    private KarzaElectricityBill karzaElectricityBill;

    private KarzaVoterID karzaVoterID;

    private Scoring scoring;

    private Surrogate surrogate;

    private KarzaLPG karzaLPG;

    private Saathi saathi;

    private KarzaDrivingLicense karzaDrivingLicense;

    @JsonProperty("CDLos")
    private com.softcell.gonogo.model.los.tvs.CDLos CDLos;

    private MultibureauCRIF multibureauCRIF;

    private MultibureauCIBIL multibureauCIBIL;

    public MultibureauCRIF getMultibureauCRIF() {
        return multibureauCRIF;
    }

    public void setMultibureauCRIF(MultibureauCRIF multibureauCRIF) {
        this.multibureauCRIF = multibureauCRIF;
    }

    public MultibureauCIBIL getMultibureauCIBIL() {
        return multibureauCIBIL;
    }

    public void setMultibureauCIBIL(MultibureauCIBIL multibureauCIBIL) {
        this.multibureauCIBIL = multibureauCIBIL;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public CreditVidya getCreditVidya() {
        return creditVidya;
    }

    public void setCreditVidya(CreditVidya creditVidya) {
        this.creditVidya = creditVidya;
    }

    public Cro getCro() {
        return cro;
    }

    public void setCro(Cro cro) {
        this.cro = cro;
    }

    public Posidex getPosidex() {
        return posidex;
    }

    public void setPosidex(Posidex posidex) {
        this.posidex = posidex;
    }

    public WelcomeScreen getWelcomeScreen() {
        return welcomeScreen;
    }

    public void setWelcomeScreen(WelcomeScreen welcomeScreen) {
        this.welcomeScreen = welcomeScreen;
    }

    public DoStages getDoStages() {
        return doStages;
    }

    public void setDoStages(DoStages doStages) {
        this.doStages = doStages;
    }

    public KarzaElectricityBill getKarzaElectricityBill() {
        return karzaElectricityBill;
    }

    public void setKarzaElectricityBill(KarzaElectricityBill karzaElectricityBill) {
        this.karzaElectricityBill = karzaElectricityBill;
    }

    public KarzaVoterID getKarzaVoterID() {
        return karzaVoterID;
    }

    public void setKarzaVoterID(KarzaVoterID karzaVoterID) {
        this.karzaVoterID = karzaVoterID;
    }

    public Scoring getScoring() {
        return scoring;
    }

    public void setScoring(Scoring scoring) {
        this.scoring = scoring;
    }

    public Surrogate getSurrogate() {
        return surrogate;
    }

    public void setSurrogate(Surrogate surrogate) {
        this.surrogate = surrogate;
    }

    public KarzaLPG getKarzaLPG() {
        return karzaLPG;
    }

    public void setKarzaLPG(KarzaLPG karzaLPG) {
        this.karzaLPG = karzaLPG;
    }

    public Saathi getSaathi() {
        return saathi;
    }

    public void setSaathi(Saathi saathi) {
        this.saathi = saathi;
    }

    public KarzaDrivingLicense getKarzaDrivingLicense() {
        return karzaDrivingLicense;
    }

    public void setKarzaDrivingLicense(KarzaDrivingLicense karzaDrivingLicense) {
        this.karzaDrivingLicense = karzaDrivingLicense;
    }

    public com.softcell.gonogo.model.los.tvs.CDLos getCDLos() {
        return CDLos;
    }

    public void setCDLos(com.softcell.gonogo.model.los.tvs.CDLos CDLos) {
        this.CDLos = CDLos;
    }
}
