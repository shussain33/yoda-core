package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class AccountSummary {

	private DrivedAttributes drivedAttributes;

    private SecondaryAccountSummary secondaryAccountSummary;

    private PrimaryAccountsSummary primaryAccountsSummary;


}
