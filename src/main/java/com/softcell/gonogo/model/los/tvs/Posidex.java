package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Posidex {


        private String posidexid;

        private String fldval;

        private String posidexdedupesta;

        private String tenorpaid;

        private String dedupeuserid;

        private String tenoravailable;

        private String addrstblty;

        private String ordernumber;

        private String msg;
}
