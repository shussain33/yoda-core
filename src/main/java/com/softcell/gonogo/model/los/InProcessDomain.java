package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InProcessDomain implements Serializable {

    /**
     * @author AshwiniP
     */
    private static final long serialVersionUID = 1L;

    @Expose
    @SerializedName("TRACKING-ID")
    private Long trackingId;
    @Expose
    @SerializedName("BUREAU")
    private String bureau;
    @Expose
    @SerializedName("PRODUCT")
    private String product;
    @Expose
    @SerializedName("STATUS")
    private String status;

    public Long getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(Long trackingId) {
        this.trackingId = trackingId;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "InProcessDomain [trackingId=" + trackingId + ", bureau="
                + bureau + ", product=" + product + ", status=" + status + "]";
    }


}
