package com.softcell.gonogo.model.los.tvs;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppographicProfile{

    private String countOfFinancialApps;
    private String countOfShoppingApps;
    private String countOfFitnessApps;
    private String countOfWalletApps;
    private String countOfSocialApps;
    private String countOfDistinctAppCategories;
    private String countOfapps;
}
