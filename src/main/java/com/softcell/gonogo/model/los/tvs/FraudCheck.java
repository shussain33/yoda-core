package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FraudCheck {

		private String gpsLocationMasked;

	    private String highSimChanger;

	    private String imeiMasked;

	    private String rootedDeviceAdvanced;



}
