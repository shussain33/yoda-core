package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExtendedWarranty {
	  private String extendedwarrantytenor;

	    private String extendedwarrantycategory;

	    private String premium;

	    private String extendedwarrantypremiumtype;

	    private String extendedwarrantydownpayment;

	    private String extendedwarrantyemi;

	    private String isextendedwarranty;

	    private String manufacturerwarranty;


	}

