package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Saathi {

        private String calldate;

        private String status;

        private String branch;

        private String updateddate;

        private String dealername;

        private String mobileno;

        private String customername;
}
