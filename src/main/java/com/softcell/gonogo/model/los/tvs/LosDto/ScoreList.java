package com.softcell.gonogo.model.los.tvs.LosDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScoreList
{
	private String sno;

	private String score;

	private String reasonCode2;

	private String reasonCode1;

	private String scoreCardVersion;

	private String scoreDate;

	private String scoreName;

	private String scoreCardName;

}
