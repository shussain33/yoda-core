package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressOffice {

	 private String addressline2;

	    private String landmark;

	    private String addressline3;

	    private String state;

	    private String addressline1;

	    private int pin;

	    private String city;

}
