package com.softcell.gonogo.model.los.tvr;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 27/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TvrSuccessOrErrorMessage {
    @JsonProperty("iAppId")
    protected int appId;
    @JsonProperty("iResCode")
    protected int responseCode;
    @JsonProperty("sDescription")
    protected String description;
}
