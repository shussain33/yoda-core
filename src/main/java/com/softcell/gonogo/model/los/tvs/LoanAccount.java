package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoanAccount {


    private com.softcell.gonogo.model.los.tvs.Loans[] loans;

    private String countLoanAccounts;

    private String countSecuredLoanAccounts;

    private String monthsLoanDataAvailable;

    private String loanDataSufficiencyFlag;

    private String countUnsecuredLoanAccounts;

}
