package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class LocationProfile {

	  private String daysLocationMonitored;

	    private String accuracyProminentPlace1;

	    private String countLocationReadingCollected;

	    private String prominentPlace1;

	    private String prominentPlace2;

	    private String prominentPlace3;

	    private String pointOfInstallationAddress;

	    private String accuracyProminentPlace2;

	    private String accuracyProminentPlace3;

	    private String pointOfInstallationAddressAccuracy;
}
