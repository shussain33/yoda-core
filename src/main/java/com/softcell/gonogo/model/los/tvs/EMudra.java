package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EMudra {

	private String status;

    private String consenttoesigned;

    private String aadharnumber;

    private String updateddate;

    private String numberofdocsigned;

    private String institutionid;

    private String version;

    private String updateby;

    private String consenttosigned;

    private String authmode;

    private String transactionid;

    private String emudradate;

    private String numberofdocsend;

    private String filetype;

}
