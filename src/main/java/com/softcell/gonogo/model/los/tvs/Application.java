package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Application {

    private String aggrementNumber;
    private String appId;
    private String applicationSource;
    private String branchCode;
    private String croId;
    private String dealerId;
    private String dsald;
    private String dtSubmit;
    private String institutionId;
    private String product;
    private String sourceId;


}
