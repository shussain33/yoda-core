package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@NoArgsConstructor
@Data
public class UtilityAccount {

	    private RiskEvents riskEvents;

	    private String utilityBillDataTelcoSufficiencyFlag;

	    private String monthsUtilityBillDataAvailableTelecom;

	    private String utilityBillDataNonTelcoSufficiencyFlag;

	    private String countUtilityAccount;

	    private AggregatePayment aggregatePayment;

	    private String monthsUtilityBillDataAvailableElectricity;



}
