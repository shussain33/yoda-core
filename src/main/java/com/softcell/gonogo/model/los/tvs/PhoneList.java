package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class PhoneList {

	    private String sno;

	    private String telephoneNumber;

	    private String telephoneType;

	    private String enrichEnquiryForPhone;


}
