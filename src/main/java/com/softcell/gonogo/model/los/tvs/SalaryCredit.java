package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class SalaryCredit {

	    private String salaryCreditAccount;

	    private String salaryWithKeywordMonth0;

	    private String salaryCreditDate;

	    private String salaryWithKeywordMonth2;

	    private String salaryWithKeywordMonth1;


}
