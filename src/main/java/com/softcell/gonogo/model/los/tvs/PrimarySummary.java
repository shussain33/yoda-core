package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class PrimarySummary {

	private String noOfOtherMfis;

    private String noOfDefaultAccounts;

    private String noOfActiveAccounts;

    private String noOfOwnMfi;

    private String noOfClosedAccounts;

    private String totalResponses;


}
