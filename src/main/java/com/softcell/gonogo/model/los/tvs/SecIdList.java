package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class SecIdList {

	private String idType;

	private String enrichedThroughtEnquiry;

	private String sno;

	private String idValue;

}
