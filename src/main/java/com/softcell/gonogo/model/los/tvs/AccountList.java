package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class AccountList {
	    private String sno;

	    private String highCreditOrSanctionedAmount;

	    private String dateReportedAndCertified;

	    private String ownershipIndicator;

	    private String currentBalance;

	    private String dateOfLastPayment;

	    private String dateClosed;

	    private String reportingMemberShortName;

	    private String paymentHistoryEndDate;

	    private String accountType;

	    private String paymentHistory2;

	    private String paymentHistoryStartDate;

	    private String paymentHistory1;

	    private String dateOpenedOrDisbursed;


}
