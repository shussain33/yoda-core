package com.softcell.gonogo.model.los;

import java.util.HashMap;
import java.util.Map;


public interface CibilMetadata {

	Map<String, String> reasonCodeAndExclusioncode = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("0101CIBILTUSC2"  	,"ONE OR MORE TRADES WITH SUIT FILED STATUS IN THE PAST 24 MONTHS.");
			put("0101CIBILTUSCR"  	,"ONE OR MORE TRADES WITH SUIT FILED STATUS IN THE PAST 24 MONTHS.");
			put("0101PLSCORE"  		,"ONE OR MORE TRADES WITH SUIT FILED STATUS IN THE PAST 24 MONTHS.");
			put("01CIBILTUSC2"  	,"NOT ENOUGH CREDIT CARD DEBT EXPERIENCE");
			put("01CIBILTUSCR"  	,"TOO MANY TRADELINES 91+ DAYS DELINQUENT IN THE PAST 6 MONTHS");
			put("0201CIBILTUSC2"  	,"ONE OR MORE TRADES WITH WILFUL DEFAULT STATUS IN THE PAST 24 MONTHS.");
			put("0201CIBILTUSCR"  	,"ONE OR MORE TRADES WITH WILFUL DEFAULT STATUS IN THE PAST 24 MONTHS.");
			put("0201PLSCORE"  		,"ONE OR MORE TRADES WITH WILFUL DEFAULT STATUS IN THE PAST 24 MONTHS.");
			put("02CIBILTUSC2"  	,"LENGTH OF TIME SINCE MOST RECENT ACCOUNT DELINQUENCY IS TOO SHORT");
			put("02CIBILTUSCR"  	,"PRESENCE OF A TRADELINE 91+ DAYS DELINQUENT IN THE PAST 6 MONTHS ");
			put("0301CIBILTUSC2"  	,"ONE OR MORE TRADES WITH SUIT FILED (WILFUL DEFAULT) STATUS IN THE PAST 24 MONTHS.");
			put("0301CIBILTUSCR"  	,"ONE OR MORE TRADES WITH SUIT FILED (WILFUL DEFAULT) STATUS IN THE PAST 24 MONTHS.");
			put("0301PLSCORE"  		,"ONE OR MORE TRADES WITH SUIT FILED (WILFUL DEFAULT) STATUS IN THE PAST 24 MONTHS.");
			put("03CIBILTUSC2"  	,"TOO MANY TWO-WHEELER ACCOUNTS");
			put("03CIBILTUSCR"  	,"CREDIT CARD BALANCES ARE TOO HIGH IN PROPORTION TO HIGH CREDIT AMOUNT");
			put("0401CIBILTUSC2"  	,"ONE OR MORE TRADES WRITTEN OFF IN THE PAST 24 MONTHS.");
			put("0401CIBILTUSCR"  	,"ONE OR MORE TRADES WRITTEN OFF IN THE PAST 24 MONTHS.");
			put("0401PLSCORE"  		,"ONE OR MORE TRADES WRITTEN OFF IN THE PAST 24 MONTHS.");
			put("04CIBILTUSC2"  	,"TOO MANY BUSINESS LOANS");
			put("04CIBILTUSCR"  	,"TOO MANY TRADELINES WITH WORST STATUS IN THE PAST 6 MONTHS");
			put("0501CIBILTUSC2"  	,"ONE OR MORE TRADES WITH SUIT FILED AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("0501CIBILTUSCR"  	,"ONE OR MORE TRADES WITH SUIT FILED AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("0501PLSCORE"  		,"ONE OR MORE TRADES WITH SUIT FILED AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("05CIBILTUSC2"  	,"CREDIT CARD ACCOUNT BALANCES TOO HIGH IN PROPORTION TO HIGH CREDIT AMOUNT");
			put("05CIBILTUSCR"  	,"PRESENCE OF SEVERE DELINQUENCY IN THE PAST 6 MONTHS");
			put("0601CIBILTUSC2"  	,"ONE OR MORE TRADES WITH WILFUL DEFAULT AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("0601CIBILTUSCR"  	,"ONE OR MORE TRADES WITH WILFUL DEFAULT AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("0601PLSCORE"  		,"ONE OR MORE TRADES WITH WILFUL DEFAULT AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("06CIBILTUSC2"  	,"MAXIMUM AMOUNT ON MORTGAGE LOAN IS LOW");
			put("06CIBILTUSCR"  	,"PRESENCE OF A MINOR DELINQUENCY IN THE PAST 6 MONTHS");
			put("0701CIBILTUSC2"  	,"ONE OR MORE TRADES WITH SUIT FILED (WILFUL DEFAULT) AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("0701CIBILTUSCR" 	,"ONE OR MORE TRADES WITH SUIT FILED (WILFUL DEFAULT) AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("0701PLSCORE"  		,"ONE OR MORE TRADES WITH SUIT FILED (WILFUL DEFAULT) AND WRITTEN OFF STATUS IN THE PAST 24 MONTHS.");
			put("07CIBILTUSC2"  	,"TOTAL AMOUNT PAST DUE IS TOO HIGH");
			put("07CIBILTUSCR"  	,"PRESENCE OF A TRADELINE WITH WORST STATUS IN THE PAST 6 MONTHS");
			put("0801CIBILTUSC2"  	,"NO ELIGIBLE TRADE FOR SCORING.");
			put("0801CIBILTUSCR"  	,"NO ELIGIBLE TRADE FOR SCORING.");
			put("0801PLSCORE"  		,"NO ELIGIBLE TRADE FOR SCORING.");
			put("08CIBILTUSC2"  	,"NOT ENOUGH MORTGAGE DEBT EXPERIENCE");
			put("08CIBILTUSCR"  	,"CREDIT CARD BALANCES ARE HIGH IN PROPORTION TO HIGH CREDIT AMOUNT");
			put("08PLSCORE"  		,"CREDIT CARD BALANCES ARE HIGH IN PROPORTION TO HIGH CREDIT AMOUNT");
			put("0901CIBILTUSC2" 	,"ONE OR MORE TRADES WITH RESTRUCTURED DEBT IN THE PAST 24 MONTHS.");
			put("0901CIBILTUSCR"  	,"ONE OR MORE TRADES WITH RESTRUCTURED DEBT IN THE PAST 24 MONTHS.");
			put("0901PLSCORE"  		,"ONE OR MORE TRADES WITH RESTRUCTURED DEBT IN THE PAST 24 MONTHS.");
			put("09CIBILTUSC2"  	,"TOO MUCH CHANGE OF INDEBTEDNESS ON NON-MORTGAGE TRADES OVER THE PAST 24 MONTHS");
			put("09CIBILTUSCR"  	,"HIGH NUMBER OF TRADES WITH LOW PROPORTION OF SATISFACTORY TRADES");
			put("1001CIBILTUSC2"  	,"ONE OR MORE TRADES WITH SETTLED DEBT IN THE PAST 24 MONTHS.");
			put("1001CIBILTUSCR"  	,"ONE OR MORE TRADES WITH SETTLED DEBT IN THE PAST 24 MONTHS.");
			put("1001PLSCORE"  		,"ONE OR MORE TRADES WITH SETTLED DEBT IN THE PAST 24 MONTHS.");
			put("10CIBILTUSC2"  	,"INSUFFICIENT IMPROVEMENT IN DELINQUENCY STATUS");
			put("10CIBILTUSCR"  	,"LOW PROPORTION OF SATISFACTORY TRADES ");
			put("10PLSCORE"  		,"LOW PROPORTION OF SATISFACTORY TRADES ");
			put("11CIBILTUSC2"  	,"TOO MUCH INCREASE OF INDEBTEDNESS ON NON-MORTGAGE TRADES OVER THE PAST 12 MONTHS");
			put("11CIBILTUSCR"  	,"NO PRESENCE OF A REVOLVING TRADELINE");
			put("12CIBILTUSC2"  	,"TOO MANY ENQUIRIES");
			put("12CIBILTUSCR"  	,"PRESENCE OF A TRADELINE 91+ DAYS DELINQUENT 7 TO 12 MONTHS AGO");
			put("13CIBILTUSC2"  	,"TOO MANY ACCOUNTS WITH A BALANCE");
			put("13CIBILTUSCR"  	,"LOW AVERAGE TRADE AGE");
			put("13PLSCORE"		  	,"LOW AVERAGE TRADE AGE");
			put("14CIBILTUSC2"  	,"LENGTH OF TIME ACCOUNTS HAVE BEEN ESTABLISHED IS TOO SHORT ");
			put("14CIBILTUSCR"  	,"PRESENCE OF A TRADELINE 91+ DAYS DELINQUENT 13 OR MORE MONTHS AGO");
			put("15CIBILTUSC2"  	,"NOT ENOUGH DEBT EXPERIENCE");
			put("15CIBILTUSCR"  	,"PRESENCE OF A MINOR DELINQUENCY 7 TO 12 MONTHS AGO");
			put("16CIBILTUSC2"  	,"TOO MANY CREDIT CARD ACCOUNTS");
			put("16CIBILTUSCR"  	,"PRESENCE OF A SEVERE DELINQUENCY 7 TO 12 MONTHS AGO");
			put("17CIBILTUSC2"  	,"PRESENCE OF A HIGH NUMBER OF ENQUIRIES");
			put("17CIBILTUSCR"  	,"PRESENCE OF A HIGH NUMBER OF ENQUIRIES");
			put("17PLSCORE"  		,"PRESENCE OF A HIGH NUMBER OF ENQUIRIES");
			put("18CIBILTUSC2"  	,"NUMBER OF ACTIVE TRADES WITH A BALANCE TOO HIGH IN PROPORTION TO TOTAL TRADES");
			put("18PLSCORE"  		,"OVER DUE AMOUNT TOO HIGH");
			put("19CIBILTUSC2"  	,"TOO MUCH CHANGE OF INDEBTEDNESS ON CREDIT CARDS OVER THE PAST 24 MONTHS");
			put("19PLSCORE"  		,"NOT ENOUGH AVAILABLE CREDIT");
			put("20CIBILTUSC2"  	,"CREDIT CARD BALANCE TOO HIGH");
			put("20PLSCORE"  		,"TOO FEW SATISFACTORY BANKCARD ACCOUNTS");
			put("21CIBILTUSC2"  	,"PROPORTION OF DELINQUENT TRADES TOO HIGH");
			put("21PLSCORE"  		,"TOTAL BALANCE OF DELINQUENCIES IS TOO HIGH");
			put("22PLSCORE"  		,"PRESENCE OF DELINQUENCY");
			put("23PLSCORE"  		,"TOTAL HIGH CREDIT OF DELINQUENCIES IS TOO HIGH");
			put("24PLSCORE"  		,"PRESENCE OF A MINOR DELINQUENCY ON PERSONAL LOAN");
		}
	};
	
	
	Map<String, String> loanType = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {




			put("52","BUSINESS LOAN - PRIORITY SECTOR - SMALL BUSINESS");
            put("53","BUSINESS LOAN - PRIORITY SECTOR - AGRICULTURE");
			put("54","BUSINESS LOAN - PRIORITY SECTOR - OTHERS");
            put("55","BUSINESS NON-FUNDED CREDIT FACILITY - GENERAL");
			put("56","BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - SMALL  BUSINESS");
            put("57","BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - AGRICULTURE");
			put("58","BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR-OTHERS");
			put("59","BUSINESS LOAN AGAINST BANK DEPOSITS");
			put("80","MICROFINANCE DETAILED REPORT");
			put("81","SUMMARY REPORT");
			put("89","VB OLM RETRIEVAL SERVICE");
			put("90","ACCOUNT REVIEW");
			put("91","RETRO ENQUIRY");
			put("92","LOCATE PLUS");
			put("93","FOR INDIVIDUAL");
			put("94","INDICATIVE REPORT");
			put("95","CONSUMER DISCLOSURE REPORT");
			put("96","BANK OLM RETRIEVAL SERVICE");
			put("97","ADVISER LIABILITY");
			put("15","LOAN AGAINST BANK DEPOSITS");
			put("16","FLEET CARD");
			put("17","COMMERCIAL VEHICLE LOAN");
			put("18","TELCO - WIRELESS");
			put("19","TELCO - BROADBAND");
			put("20","TELCO - LANDLINE");
			put("40","MICROFINANCE - BUSINESS LOAN");
			put("41","MICROFINANCE - PERSONAL LOAN");
			put("42","MICROFINANCE - HOUSING LOAN");
			put("43","MICROFINANCE - OTHER");
			put("51","BUSINESS LOAN - GENERAL");
			put("98","SECURED");
			put("99","UNSECURED");
			put("31","SECURED CREDIT CARD");
			put("32","USED CAR LOAN");
			put("33","CONSTRUCTION EQUIPMENT LOAN");
			put("34","TRACTOR LOAN");
			put("60","CURRENT BUSINESS LOAN-DIRECTOR SEARCH");
			put("88","LOCATE PLUS FOR INSURANCE");
			put("00","OTHER");
			put("01","AUTO LOAN (PERSONAL)");
			put("02","HOUSING LOAN");
			put("03","PROPERTY LOAN");
			put("04","LOAN AGAINST SHARES/SECURITIES");
			put("05","PERSONAL LOAN");
			put("06","CONSUMER LOAN");
			put("07","GOLD LOAN");
			put("08","EDUCATION LOAN");
			put("09","LOAN TO PROFESSIONAL");
			put("10","CREDIT CARD");
			put("11","LEASING");
			put("12","OVERDRAFT");
			put("13","TWO-WHEELER LOAN");
			put("14","NON-FUNDED CREDIT FACILITY");
			put("00","Tractor Finance");
		}
	};
	
	Map<String, String> genderMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("1", "FEMALE");
			put("2", "MALE");
			put("3", "TRANSGENDER");
		}
	};
	
	
	Map<String, String> subjectReturnCode = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("1", "FOUND");
			put("0", "NOT FOUND");
		}
	};
	
	// Appendix G � Error Codes
	
	Map<String, String> pnErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("001", "Consumer Name in Dispute");
			put("007", "Date of Birth in Dispute");
			put("008", "Gender in Dispute");
			put("201", "Consumer Name - Dispute accepted - Pending corrections by the Member");
			put("207", "Date of Birth - Dispute accepted - Pending corrections by the Member");
			put("208", "Gender - Dispute accepted - Pending corrections by the Member");
			put("998", "Multiple Disputes in Name (PN) Segment - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes in Name (PN) Segment");
		}
	};
	
	Map<String, String> idErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("001", "ID Type in Dispute");
			put("002", "ID Number in Dispute");
			put("003", "Issue Date in Dispute");
			put("004", "Expiration Date in Dispute");
			put("201", "ID Type - Dispute accepted - Pending corrections by the Member");
			put("202", "ID Number - Dispute accepted - Pending corrections by the Member");
			put("203", "Issue Date - Dispute accepted - Pending corrections by the Member");
			put("204", "Expiration Date - Dispute accepted - Pending corrections by the Member");
			put("998", "Multiple Disputes in Identification (ID) Segment - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes in Identification (ID) Segment");
		}
	};
	
	Map<String, String> ptErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("001", "Telephone Number in Dispute");
			put("002", "Telephone Extension in Dispute");
			put("003", "Telephone Type in Dispute");
			put("201", "Telephone Number - Dispute accepted - Pending corrections by the Member");
			put("202", "Telephone Extension - Dispute accepted - Pending corrections by the Member");
			put("203", "Telephone Type - Dispute accepted - Pending corrections by the Member");
			put("998", "Multiple Disputes in Telephone (PT) Segment - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes in Telephone (PT) Segment");
		}
	};
	
	Map<String, String> ecErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("001", "E-Mail ID in Dispute");
			put("201", "E-Mail ID - Dispute accepted - Pending corrections by the Member");
			put("998", "Multiple Disputes in Email Contact (EC) Segment - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes in Email Contact (EC) Segment");
		}
	};
	
	Map<String, String> paErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("001", "Address Line (except State Code and PIN Code) in Dispute");
			put("006", "State in Dispute");
			put("007", "PIN Code in Dispute");
			put("008", "Address Category in Dispute");
			put("009", "Residence Code in Dispute");
			put("201", "Address Line - Dispute accepted - Pending corrections by the Member");
			put("206", "State - Dispute accepted - Pending corrections by the Member");
			put("207", "PIN Code - Dispute accepted - Pending corrections by the Member");
			put("206", "Address Category - Dispute accepted - Pending corrections by the Member");
			put("207", "Residence Code - Dispute accepted - Pending corrections by the Member");
			put("998", "Multiple Disputes in Address (PA) Segment - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes in Address (PA) Segment");
		}
	};
	
	Map<String, String> iqErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("001", "Enquiry Purpose in Dispute");
			put("006", "Enquiry Amount in Dispute");
			put("201", "Enquiry Purpose - Dispute accepted - Pending corrections by the Member");
			put("206", "Enquiry Amount - Dispute accepted - Pending corrections by the Member");
			put("887", "Enquiry Ownership Error - Dispute accepted - Pending corrections by the Member");
			put("888", "Enquiry Ownership Error");
			put("998", "Multiple Disputes in Enquiry (IQ) Segment - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes in Enquiry (IQ) Segment");
		}
	};
	
	Map<String, String> zzErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("00", "No Dispute");
			put("998", "Multiple Disputes in multiple segments - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes in multiple segments");
		}
	};
	
	Map<String, String> emErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("000", "No Dispute");
			put("003", "Occupation Code in Dispute");
			put("004", "Income in Dispute");
			put("005", "Net/Gross Income Indicator in Dispute");
			put("006", "Monthly/Annual Income Indicator in Dispute");
			put("203", "Occupation Code - Dispute accepted - Pending corrections by the Member");
			put("204", "Income - Dispute accepted - Pending corrections by the Member");
			put("205", "Net/Gross Income Indicator - Dispute accepted - Pending corrections by the Member");
			put("206", "Monthly/Annual Income Indicator - Dispute accepted - Pending corrections by the Member");
			put("998", "Multiple Disputes - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes");
		}
	};
	
	Map<String, String> tlErrorMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("000", "No Disputes");
			put("003", "Account Number in Dispute"); 
			put("004", "Account Type in Dispute"); 
			put("005", "Ownership Indicator in Dispute"); 
			put("008", "Date Opened/Disbursed in Dispute"); 
			put("009", "Date of Last Payment in Dispute"); 
			put("010", "Date Closed in Dispute"); 
			put("011", "Date Reported and Certified in Dispute"); 
			put("012", "High Credit/Sanctioned Amount in Dispute"); 
			put("013", "Current Balance in Dispute"); 
			put("014", "Amount Overdue in Dispute"); 
			put("030", "Payment History Start Date in Dispute"); 
			put("031", "Payment History End Date in Dispute"); 
			put("032", "Suit Filed / Wilful Default in Dispute"); 
			put("033", "Written-off and Settled Status in Dispute"); 
			put("034", "Value of Collateral in Dispute"); 
			put("035", "Type of Collateral in Dispute"); 
			put("036", "Credit Limit in Dispute"); 
			put("037", "Cash Limit in Dispute");
			put("038", "Rate Of Interest in Dispute"); 
			put("039", "Repayment Tenure in Dispute"); 
			put("040", "EMI Amount in Dispute");
			put("041", "Written-off Amount (Total) in Dispute");
			put("042", "Written-off Amount (Principal) in Dispute");
			put("043", "Settlement Amount in Dispute");
			put("044", "Payment Frequency in Dispute");
			put("045", "Actual Payment Amount in Dispute");
			put("101", "Payment History 1 in Dispute");
			put("102", "Payment History 2 in Dispute");
			put("103", "Payment History 3 in Dispute");
			put("104", "Payment History 4 in Dispute");
			put("105", "Payment History 5 in Dispute");
			put("106", "Payment History 6 in Dispute");
			put("107", "Payment History 7 in Dispute");
			put("108", "Payment History 8 in Dispute");
			put("109", "Payment History 9 in Dispute");
			put("110", "Payment History 10 in Dispute");
			put("111", "Payment History 11 in Dispute");
			put("112", "Payment History 12 in Dispute");
			put("113", "Payment History 13 in Dispute");
			put("114", "Payment History 14 in Dispute");
			put("115", "Payment History 15 in Dispute");
			put("116", "Payment History 16 in Dispute");
			put("117", "Payment History 17 in Dispute");
			put("118", "Payment History 18 in Dispute");
			put("119", "Payment History 19 in Dispute");
			put("120", "Payment History 20 in Dispute");
			put("121", "Payment History 21 in Dispute");
			put("122", "Payment History 22 in Dispute");
			put("123", "Payment History 23 in Dispute");
			put("124", "Payment History 24 in Dispute");
			put("125", "Payment History 25 in Dispute");
			put("126", "Payment History 26 in Dispute");
			put("127", "Payment History 27 in Dispute");
			put("128", "Payment History 28 in Dispute");
			put("129", "Payment History 29 in Dispute");
			put("130", "Payment History 30 in Dispute");
			put("131", "Payment History 31 in Dispute");
			put("132", "Payment History 32 in Dispute");
			put("133", "Payment History 33 in Dispute");
			put("134", "Payment History 34 in Dispute");
			put("135", "Payment History 35 in Dispute");
			put("136", "Payment History 36 in Dispute");
			put("203", "Account Number - Dispute accepted - Pending corrections by the Member");
			put("204", "Account Type - Dispute accepted - Pending corrections by the Member");
			put("205", "Ownership Indicator - Dispute accepted - Pending corrections by the Member");
			put("208", "Date Opened/Disbursed - Dispute accepted - Pending corrections by the Member");
			put("209", "Date of Last Payment - Dispute accepted - Pending corrections by the Member");
			put("210", "Date Closed - Dispute accepted - Pending corrections by the Member");
			put("211", "Date Reported and Certified - Dispute accepted - Pending corrections by the Member");
			put("212", "High Credit/Sanctioned Amount - Dispute accepted - Pending corrections by the Member");
			put("213", "Current Balance - Dispute accepted - Pending corrections by the Member");
			put("214", "Amount Overdue - Dispute accepted - Pending corrections by the Member");
			put("230", "Payment History Start Date - Dispute accepted - Pending corrections by the Member");
			put("231", "Payment History End Date - Dispute accepted - Pending corrections by the Member");
			put("232", "Suit Filed / Wilful Default - Dispute accepted - Pending corrections by the Member");
			put("233", "Written-off and Settled Status - Dispute accepted - Pending corrections by the Member");
			put("234", "Value of Collateral - Dispute accepted - Pending corrections by the Member");
			put("235", "Type of Collateral - Dispute accepted - Pending corrections by the Member");
			put("236", "Credit Limit - Dispute accepted - Pending corrections by the Member");
			put("237", "Cash Limit - Dispute accepted - Pending corrections by the Member");
			put("238", "Rate Of Interest - Dispute accepted - Pending corrections by the Member");
			put("239", "Repayment Tenure - Dispute accepted - Pending corrections by the Member");
			put("240", "EMI Amount - Dispute accepted - Pending corrections by the Member");
			put("241", "Written-off Amount (Total) - Dispute accepted - Pending corrections by the Member");
			put("242", "Written-off Amount (Principal) - Dispute accepted - Pending corrections by the Member");
			put("243", "Settlement Amount - Dispute accepted - Pending corrections by the Member");
			put("244", "Payment Frequency - Dispute accepted - Pending corrections by the Member");
			put("245", "Actual Payment Amount - Dispute accepted - Pending corrections by the Member");
			put("301", "Payment History 1 - Dispute accepted - Pending corrections by the Member");
			put("302", "Payment History 2 - Dispute accepted - Pending corrections by the Member");
			put("303", "Payment History 3 - Dispute accepted - Pending corrections by the Member");
			put("304", "Payment History 4 - Dispute accepted - Pending corrections by the Member");
			put("305", "Payment History 5 - Dispute accepted - Pending corrections by the Member");
			put("306", "Payment History 6 - Dispute accepted - Pending corrections by the Member");
			put("307", "Payment History 7 - Dispute accepted - Pending corrections by the Member");
			put("308", "Payment History 8 - Dispute accepted - Pending corrections by the Member");
			put("309", "Payment History 9 - Dispute accepted - Pending corrections by the Member");
			put("310", "Payment History 10 - Dispute accepted - Pending corrections by the Member");
			put("311", "Payment History 11 - Dispute accepted - Pending corrections by the Member");
			put("312", "Payment History 12 - Dispute accepted - Pending corrections by the Member");
			put("313", "Payment History 13 - Dispute accepted - Pending corrections by the Member");
			put("314", "Payment History 14 - Dispute accepted - Pending corrections by the Member");
			put("315", "Payment History 15 - Dispute accepted - Pending corrections by the Member");
			put("316", "Payment History 16 - Dispute accepted - Pending corrections by the Member");
			put("317", "Payment History 17 - Dispute accepted - Pending corrections by the Member");
			put("318", "Payment History 18 - Dispute accepted - Pending corrections by the Member");
			put("319", "Payment History 19 - Dispute accepted - Pending corrections by the Member");
			put("320", "Payment History 20 - Dispute accepted - Pending corrections by the Member");
			put("321", "Payment History 21 - Dispute accepted - Pending corrections by the Member");
			put("322", "Payment History 22 - Dispute accepted - Pending corrections by the Member");
			put("323", "Payment History 23 - Dispute accepted - Pending corrections by the Member");
			put("324", "Payment History 24 - Dispute accepted - Pending corrections by the Member");
			put("325", "Payment History 25 - Dispute accepted - Pending corrections by the Member");
			put("326", "Payment History 26 - Dispute accepted - Pending corrections by the Member");
			put("327", "Payment History 27 - Dispute accepted - Pending corrections by the Member");
			put("328", "Payment History 28 - Dispute accepted - Pending corrections by the Member");
			put("329", "Payment History 29 - Dispute accepted - Pending corrections by the Member");
			put("330", "Payment History 30 - Dispute accepted - Pending corrections by the Member");
			put("331", "Payment History 31 - Dispute accepted - Pending corrections by the Member");
			put("332", "Payment History 32 - Dispute accepted - Pending corrections by the Member");
			put("333", "Payment History 33 - Dispute accepted - Pending corrections by the Member");
			put("334", "Payment History 34 - Dispute accepted - Pending corrections by the Member");
			put("335", "Payment History 35 - Dispute accepted - Pending corrections by the Member");
			put("336", "Payment History 36 - Dispute accepted - Pending corrections by the Member");
			put("885", "Duplicate Account - Dispute accepted - Pending corrections by the Member");
			put("886", "Duplicate Account");
			put("887", "Account Ownership Error - Dispute accepted - Pending corrections by the Member");
			put("888", "Account Ownership Error");
			put("998", "Multiple Disputes - Dispute accepted - Pending corrections by the Member");
			put("999", "Multiple Disputes");
		}
	};
	
	// Appendix H � Error/Dispute Remarks Codes
	Map<String, String> errorDisputeRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("000001", "Disputed accepted � under investigation");
		}
	};
	
	// Appendix I � CIBIL Remarks Codes
	Map<String, String> pnRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("PN0001", "Certain information under Personal / Contract / Enquiry information section has been disputed by the consumer.");
			put("PN1001", "Consumer Name in Dispute");
			put("PN1007", "Date of Birth in Dispute");
			put("PN1008", "Gender in Dispute");
			put("PN1999", "Multiple Disputes in Name (PN) Segment");
		}
	};
	
	Map<String, String> idRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("ID1001", "ID Type in Dispute");
			put("ID1002", "ID Number in Dispute");
			put("ID1003", "Issue Date in Dispute");
			put("ID1004", "Expiration Date in Dispute");
			put("ID1999", "Multiple Disputes in Identification (ID) Segment");
		}
	};
	
	Map<String, String> ptRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("PT1001", "Telephone Number in Dispute");
			put("PT1002", "Telephone Extension in Dispute");
			put("PT1003", "Telephone Type in Dispute");
			put("PT1999", "Multiple Disputes in Telephone (PT) Segment");
		}
	};
	
	Map<String, String> ecRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("EC1001", "E-Mail ID in Dispute");
			put("EC1999", "Multiple Disputes in Email Contact (EC) Segment");
		}
	};
	
	Map<String, String> paRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("PA1001", "Address Line (except State Code and PIN Code) in Dispute");
			put("PA1006", "State in Dispute");
			put("PA1007", "PIN Code in Dispute");
			put("PA1008", "Address Category in Dispute");
			put("PA1009", "Residence Code in Dispute");
			put("PA1999", "Multiple Disputes in Address (PA) Segment");
		}
	};
	
	Map<String, String> iqRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("IQ1001", "Enquiry Purpose in Dispute");
			put("IQ1006", "Enquiry Amount in Dispute");
			put("IQ1888", "Enquiry Ownership Error");
			put("IQ1999", "Multiple Disputes in Enquiry (IQ) Segment");
		}
	};
	
	Map<String, String> zzRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("000002", "Dispute accepted � pending correction by the Member");
			put("000001", "One or more Members have not responded to your Dispute");
			put("ZZ0999", "Multiple Disputes in multiple segments");
		}
	};
	
	Map<String, String> emRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("EM0001", "Certain information under Employment information section has been disputed by the consumer.");
			put("EM1003", "Occupation Code in Dispute");
			put("EM1004", "Income in Dispute");
			put("EM1005", "Net/Gross Income Indicator in Dispute");
			put("EM1006", "Monthly/Annual Income Indicator in Dispute");
			put("EM1999", "Multiple Disputes in Employment (EM) Segment");
		}
	};
	
	Map<String, String> tlRemarksMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("TL0001", "Certain information for this account has been disputed by the consumer.");
			put("TL1003", "Account Number in Dispute"); 
			put("TL1004", "Account Type in Dispute"); 
			put("TL1005", "Ownership Indicator in Dispute"); 
			put("TL1008", "Date Opened/Disbursed in Dispute"); 
			put("TL1009", "Date of Last Payment in Dispute"); 
			put("TL1010", "Date Closed in Dispute"); 
			put("TL1011", "Date Reported and Certified in Dispute"); 
			put("TL1012", "High Credit/Sanctioned Amount in Dispute"); 
			put("TL1013", "Current Balance in Dispute"); 
			put("TL1014", "Amount Overdue in Dispute"); 
			put("TL1030", "Payment History Start Date in Dispute"); 
			put("TL1031", "Payment History End Date in Dispute"); 
			put("TL1032", "Suit Filed / Wilful Default in Dispute"); 
			put("TL1033", "Written-off and Settled Status in Dispute"); 
			put("TL1034", "Value of Collateral in Dispute"); 
			put("TL1035", "Type of Collateral in Dispute"); 
			put("TL1036", "Credit Limit in Dispute"); 
			put("TL1037", "Cash Limit in Dispute");
			put("TL1038", "Rate Of Interest in Dispute"); 
			put("TL1039", "Repayment Tenure in Dispute"); 
			put("TL1040", "EMI Amount in Dispute");
			put("TL1041", "Written-off Amount (Total) in Dispute");
			put("TL1042", "Written-off Amount (Principal) in Dispute");
			put("TL1043", "Settlement Amount in Dispute");
			put("TL1044", "Payment Frequency in Dispute");
			put("TL1045", "Actual Payment Amount in Dispute");
			put("TL1101", "Payment History 1 in Dispute");
			put("TL1102", "Payment History 2 in Dispute");
			put("TL1103", "Payment History 3 in Dispute");
			put("TL1104", "Payment History 4 in Dispute");
			put("TL1105", "Payment History 5 in Dispute");
			put("TL1106", "Payment History 6 in Dispute");
			put("TL1107", "Payment History 7 in Dispute");
			put("TL1108", "Payment History 8 in Dispute");
			put("TL1109", "Payment History 9 in Dispute");
			put("TL1110", "Payment History 10 in Dispute");
			put("TL1111", "Payment History 11 in Dispute");
			put("TL1112", "Payment History 12 in Dispute");
			put("TL1113", "Payment History 13 in Dispute");
			put("TL1114", "Payment History 14 in Dispute");
			put("TL1115", "Payment History 15 in Dispute");
			put("TL1116", "Payment History 16 in Dispute");
			put("TL1117", "Payment History 17 in Dispute");
			put("TL1118", "Payment History 18 in Dispute");
			put("TL1119", "Payment History 19 in Dispute");
			put("TL1120", "Payment History 20 in Dispute");
			put("TL1121", "Payment History 21 in Dispute");
			put("TL1122", "Payment History 22 in Dispute");
			put("TL1123", "Payment History 23 in Dispute");
			put("TL1124", "Payment History 24 in Dispute");
			put("TL1125", "Payment History 25 in Dispute");
			put("TL1126", "Payment History 26 in Dispute");
			put("TL1127", "Payment History 27 in Dispute");
			put("TL1128", "Payment History 28 in Dispute");
			put("TL1129", "Payment History 29 in Dispute");
			put("TL1130", "Payment History 30 in Dispute");
			put("TL1131", "Payment History 31 in Dispute");
			put("TL1132", "Payment History 32 in Dispute");
			put("TL1133", "Payment History 33 in Dispute");
			put("TL1134", "Payment History 34 in Dispute");
			put("TL1135", "Payment History 35 in Dispute");
			put("TL1136", "Payment History 36 in Dispute");
			put("TL1886", "Duplicate Account");
			put("TL1888", "Account Ownership Error");
			put("TL1999", "Multiple Disputes in Account (TL) Segment");
		}
	};
	
	
	Map<String, String> accountTypeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			put("01", "AUTO LOAN (PERSONAL)");
			put("02", "HOUSING LOAN");
			put("03", "PROPERTY LOAN");
			put("04", "LOAN AGAINST SHARES/SECURITIES");
			put("05", "PERSONAL LOAN");
			put("06", "CONSUMER LOAN");
			put("07", "GOLD LOAN");
			put("08", "EDUCATION LOAN");
			put("09", "LOAN TO PROFESSIONAL");
			put("10", "CREDIT CARD");
			put("11", "LEASING");
			put("12", "OVERDRAFT");
			put("13", "TWO-WHEELER LOAN");
			put("14", "NON-FUNDED CREDIT FACILITY");
			put("15", "LOAN AGAINST BANK DEPOSITS");
			put("16", "FLEET CARD");
			put("17", "COMMERCIAL VEHICLE LOAN");
			put("18", "TELCO - WIRELESS");
			put("19", "TELCO - BROADBAND");
			put("20", "TELCO - LANDLINE");
			put("40", "MICROFINANCE - BUSINESS LOAN");
			put("41", "MICROFINANCE - PERSONAL LOAN");
			put("42", "MICROFINANCE - HOUSING LOAN");
			put("43", "MICROFINANCE - OTHER");
			put("51", "BUSINESS LOAN - GENERAL");
			put("52", "BUSINESS LOAN - PRIORITY SECTOR - SMALL BUSINESS");
			put("53", "BUSINESS LOAN - PRIORITY SECTOR - AGRICULTURE");
			put("54", "BUSINESS LOAN - PRIORITY SECTOR - OTHERS");
			put("55", "BUSINESS NON-FUNDED CREDIT FACILITY - GENERAL");
			put("56", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - SMALL BUSINESS");
			put("57", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - AGRICULTURE");
			put("58", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR-OTHERS");
			put("59", "BUSINESS LOAN AGAINST BANK DEPOSITS");
			put("80", "MICROFINANCE DETAILED REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("81", "SUMMARY REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("89", "VB OLM RETRIEVAL SERVICE (APPLICABLE TO ENQUIRY PURPOSE FOR VB OLM REQUEST ONLY)");
			put("90", "ACCOUNT REVIEW (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("91", "RETRO ENQUIRY (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("92", "LOCATE PLUS (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("93", "FOR INDIVIDUAL (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("94", "INDICATIVE REPORT (APPLICABLE TO ENQUIRY PURPOSE FOR CRS REQUEST ONLY)");
			put("95", "CONSUMER DISCLOSURE REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("96", "BANK OLM RETRIEVAL SERVICE (APPLICABLE TO ENQUIRY PURPOSE FOR CRS REQUEST ONLY)");
			put("97", "ADVISER LIABILITY (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("00", "OTHER");
			put("98", "SECURED (ACCOUNT GROUPFOR PORTFOLIO REVIEW RESPONSE)");
			put("99", "UNSECURED (ACCOUNT GROUP FOR PORTFOLIO REVIEW RESPONSE)");
		}      
	};
	
	  
	  Map<String, String> occupationMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("01", "SALARIED");
			  put("02", "SELF EMPLOYED PROFESSIONAL");
			  put("03", "SELF EMPLOYED");
			  put("04", "OTHERS");
		}
	  };
	  
	  Map<String, String> grossIncomeMap  = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
		  put("G", "GROSS INCOME");
		  put("N", "NET INCOME");
		}
	 };
	 
	 Map<String, String> AnnualIncome = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			 put("M", "MONTHLY");
			 put("A", "ANNUAL");
		 }
	 };
	  
	  Map<String, String> remarkCodeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("000001", "ONE OR MORE MEMBERS HAVE NOT RESPONDED TO YOUR DISPUTE");
			  put("000002", "DISPUTE ACCEPTED - PENDING CORRECTION BY THE MEMBER");
		  }
	  };
	  
	  Map<String, String> stateMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("01", "JAMMU & KASHMIR");
			  put("02", "HIMACHAL PRADESH");
			  put("03", "PUNJAB");
			  put("04", "CHANDIGARH");
			  put("05", "UTTARANCHAL");
			  put("06", "HARYANA");
			  put("07", "DELHI");
			  put("08", "RAJASTHAN");
			  put("09", "UTTAR PRADESH");
			  put("10", "BIHAR");
			  put("11", "SIKKIM");
			  put("12", "ARUNACHAL PRADESH");
			  put("13", "NAGALAND");
			  put("14", "MANIPUR");
			  put("15", "MIZORAM");
			  put("16", "TRIPURA");
			  put("17", "MEGHALAYA");
			  put("18", "ASSAM");
			  put("19", "WEST BENGAL");
			  put("20", "JHARKHAND");
			  put("21", "ORISSA");
			  put("22", "CHHATTISGARH");
			  put("23", "MADHYA PRADESH");
			  put("24", "GUJARAT");
			  put("25", "DAMAN & DIU");
			  put("26", "DADRA & NAGAR HAVELI");
			  put("27", "MAHARASHTRA");
			  put("28", "ANDHRA PRADESH");
			  put("29", "KARNATAKA");
			  put("30", "GOA");
			  put("31", "LAKSHADWEEP");
			  put("32", "KERALA");
			  put("33", "TAMIL NADU");
			  put("34", "PONDICHERRY");
			  put("35", "ANDAMAN & NICOBAR ISLANDS");
			  put("36", "TELANGANA");
			  put("99", "APO ADDRESS");
		  }
	  };
	  
	  
	  
	  Map<String, String> addressCategoryMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("01", "PERMANENT ADDRESS");
			  put("02", "RESIDENCE ADDRESS");
			  put("03", "OFFICE ADDRESS");
			  put("04", "NOT CATEGORIZED");
		  }  
	  };
	   
	   
	   Map<String, String> residenceCodeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			   put("01", "OWNED");
			   put("02", "RENTED");
		   }  
	   };
	   
	   Map<String, String> suitFiledMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			//CIBIL/Experian
			   put("00", "NO SUIT FILED");
			   put("01", "SUIT FILED");
			   put("02", "WILFUL DEFAULT");
			   put("03", "SUIT FILED (WILFUL DEFAULT)");
			
			   
			   
		   }
	   };
	   
		Map<String, String> collateralMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;

			{
				put("00", "NO COLLATERAL");
				put("01", "PROPERTY");
				put("02", "GOLD");
				put("03", "SHARES");
				put("04", "SAVING ACCOUNT AND FIXED DEPOSIT");
			}
		};
		
		Map<String, String> phoneMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;
			{
				
				put("00", "NOT CLASSIFIED");
				put("01", "MOBILE");
				put("02", "HOME");
				put("03", "OFFICE");
				
			}
		};
		
	   Map<String, String> ownershipMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			   put("1", "INDIVIDUAL");
			   put("2", "AUTHORISED");
			   put("3", "GUARANTOR");
			   put("4", "JOIN");
		   }
	   };
	   Map<String, String> expAccountHolderTypeMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;
			{
				   put("1", "INDIVIDUAL");
				   put("2", "JOINT");
				   put("3", "AUTHORISED USER");
				   put("7", "GUARANTOR");
			   }
		   };
	   
	   Map<String, String>  idTypeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;
		{
			put("01","PAN");
			put("02","PASSPORT");
			put("03","VOTER ID");
			put("04","DRIVING LICENSE");
			put("05","RATION CARD");
			put("06","UID");
			put("07","OTHER ID1");
			put("08","OTHER ID2");

		}
	   };
	   
	   
	  Map<String, String> writtenOffSettledStatus = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;

			{
				put("00", "RESTRUCTURED LOAN");
				put("01", "RESTRUCTURED LOAN (GOVT. MANDATED)");
				put("02", "WRITTEN-OFF");
				put("03", "SETTLED");
				put("04", "POST (WO) SETTLED");
				put("05", "ACCOUNT SOLD");
				put("06", "WRITTEN OFF AND ACCOUNT SOLD");
				put("07", "ACCOUNT PURCHASED");
				put("08", "ACCOUNT PURCHASED AND WRITTEN OFF");
				put("09", "ACCOUNT PURCHASED AND SETTLED");
				put("10", "ACCOUNT PURCHASED AND RESTRUCTURED");
			}
	  };
	  
	  Map<String, String> scoreCardName = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L; {
				put("01", "CIBILTUSCR");
				put("02", "PLSCORE");
				put("04", "CIBILTUSC2");
			}
		};
		
		 Map<String, String> scoreErrorMap = new HashMap<String, String>(){
				private static final long serialVersionUID = 1L; {
					
					put("00001", "FID NOT FOUND");
					put("00002", "DUPLICATE FID");
					put("00003", "ERROR RELATED TO CONSUMER DATA ISSUE");
					put("00004", "ERROR RELATED TO DATABASE ISSUE");
					put("00005", "ERROR DURING SCORE CALCULATION");
				}
			};
			
			Map<String, String> paymentFreq = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;

			{
				  put("01", "WEEKLY");
				  put("02", "FORTNIGHTLY");
				  put("03", "MONTHLY");
				  put("04", "QUARTERLY");
			}
		  };
		
}
