package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsuranceDetails {

	 	private String isinsurancedetails;

	    private String insurancedownpayment;

	    private String insurancepremiumemi;

	    private String insurancepremiumtype;

	    private String insuranceprovider;

}
