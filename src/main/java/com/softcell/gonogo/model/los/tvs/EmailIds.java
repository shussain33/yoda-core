package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class EmailIds {

    private String sno;

	private String emailID;

    private String corporateFlag;

    private String companyName;

    private String companyCategory;


}
