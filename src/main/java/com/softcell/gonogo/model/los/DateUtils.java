package com.softcell.gonogo.model.los;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class DateUtils {
	private static Logger logger_ = LoggerFactory.getLogger(DateUtils.class);
	public static String DATE_FORMAT1 = "yyyy-MM-dd";
	public static String DATE_FORMAT2 = "dd/MM/yyyy";
	public static String DATE_FORMAT3 = "dd:MM:yyyy";
	public static String DATE_FORMAT4 = "ddMMyyyy";
	public static String DATE_FORMAT5 = "d MMMM, yyyy";
	public static String DATE_FORMAT6 = "E MMM dd yyyy HH:mm:ss z";
	public static String TIMESTAMP_FORMAT1="dd/MM/yyyy hh:mm:ss.SSS";
	public static String sample="yyyy-mm-dd";
	
//	private static SimpleDateFormat sdf =null;
	
	public static Date parseDate(String dateString){
		logger_.warn("<<  parseDate():  [  dateString  :  {}]",dateString);
		
		SimpleDateFormat sdf = null;
		
		if(StringUtils.isNotBlank(dateString)){
			try{
				if(StringUtils.contains(dateString, "-")){
					sdf = new SimpleDateFormat(DATE_FORMAT1);
					Date parse = sdf.parse(dateString);
					logger_.warn(">>  parseDate():  [  return  :  {}]",parse);
					return parse ;
				}else if(StringUtils.contains(dateString, "/")){
					sdf = new SimpleDateFormat(DATE_FORMAT2);
					Date parse = sdf.parse(dateString);
					logger_.warn(">>  parseDate():  [  return  :  {}]",parse);
					return parse;
				}else if(StringUtils.contains(dateString, ":")){
					sdf = new SimpleDateFormat(DATE_FORMAT3);
					Date parse = sdf.parse(dateString);
					logger_.warn(">>  parseDate():  [  return  :  {}]",parse);
					return parse;
				}else if(StringUtils.contains(dateString, ",")){
					return new SimpleDateFormat(DATE_FORMAT5, Locale.ENGLISH).parse(dateString);
				}else if(!StringUtils.contains(dateString, "-") && !StringUtils.contains(dateString, "/") && !StringUtils.contains(dateString, ":") && StringUtils.length(dateString) == 8){
					sdf = new SimpleDateFormat(DATE_FORMAT4);
					Date parse = sdf.parse(dateString);
					logger_.warn(">>  parseDate():  [  return  :  {}]",parse);
					return parse;			
				}
			}catch(Exception e){
				System.out.println("CAN NOT PARSE DATE STRING TO DATE OBJECT "+dateString);
				return null;
			}
		}
		return null;
	}
	
	public static String getCurrentDate(){
		logger_.warn("<<  getCurrentDate() ");
		SimpleDateFormat dateFormat=new  SimpleDateFormat("EEEE  dd MMMM yyyy , hh:mm:ss");
		logger_.warn(">>  getCurrentDate() ");
		return dateFormat.format(new Date());

	}
	
	public static String getDate(){
		SimpleDateFormat dateFormat=new  SimpleDateFormat("ddMMMM,yyyy");
		return dateFormat.format(new Date());
	}
	
	public static String getDateyyyyMMdd(){
		SimpleDateFormat dateFormat=new  SimpleDateFormat("yyyyMMdd");
		return dateFormat.format(new Date());
	}
	
	public static String getFormattedDate(Date date,String format){
		
		logger_.warn("<<  getFormattedDate() : [ date : {},  format : {} ]",date,format);
		
		SimpleDateFormat sdf = null;
		
		try {
			if(StringUtils.isNotBlank(format) && date != null){
				sdf= new SimpleDateFormat(format);
		
				String format2 = sdf.format(date);
				logger_.warn(">> getFormattedDate() : [ return : {}]",format2);
				return format2 ;
				
			}
			
			return null;
		} catch (Exception e) {
			System.out.println("CAN NOT FORMAT DATE STRING "); 
			return null;
		}
	}

	public static Date getDateFromString(String inputDtString,String format){

		logger_.warn("<<  getDateFromString() : [ inputDtString : {},  format : {} ]",inputDtString,format);
		
		SimpleDateFormat sdf = null;
		
		Date outputDt = null;
		//inputDtString = 20150430
		//format = yyyy-MM-dd HH:mm:ss
		try {
			sdf= new SimpleDateFormat(format);
			String formatedDate = sdf.format(sdf.parse(inputDtString));
			
			if(StringUtils.equalsIgnoreCase(inputDtString, formatedDate)){
				outputDt = sdf.parse(inputDtString);
			}
		} catch (NullPointerException e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		} catch (IllegalArgumentException e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		} catch (ParseException e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		}

		logger_.warn(">>  getDateFromString() : [ return : {} ]",outputDt);
		return outputDt;
	}

	public static String getFormatedTimestamp(String date){
	
		logger_.warn("<<  getFormatedTimestamp() :  [ date : {} ]",date);
		
		SimpleDateFormat sdf = null;
		
		sdf=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date parse2=null;
		try {
			if (StringUtils.isNotBlank(date)) {
				parse2 = sdf.parse(date);
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
				date = dateFormat.format(parse2);
			}
		logger_.warn(">>  getFormatedTimestamp() :  [ return : {} ]",date);
		return	date;
		} catch (ParseException e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		} 
	}	
	
	
	public static Object getSQLFormattedDateAsString(Date date){
	
		logger_.warn("<<   getSQLFormattedDateAsString() : [  date : {}]",date);
		
		SimpleDateFormat sdf = null;
		
		try{
			if(date!=null){
				sdf = new SimpleDateFormat("dd-MMM-yyyy");
				String format = sdf.format(date);
				logger_.warn(">>   getSQLFormattedDateAsString() : [  return : {}]",format);
				return format ;
			}
			
		}catch(Exception e){
			logger_.error("** "+e.getMessage(),e);
			return null;
		}
		return null;
	}
	
	
	public static java.sql.Date getJavaDateToSqlDate(Date date){
	
		logger_.warn("<< getJavaDateToSqlDate() : [ date  :  {}]",date);
		
		try{
			if(date!=null){

				java.sql.Date sqlDate = new java.sql.Date(date.getTime());

				logger_.warn(">>  getJavaDateToSqlDate() :  [  return : {}]",sqlDate);
				return  sqlDate;
				
			}
				
		}catch(Exception e){
			logger_.error("** "+e.getMessage(),e);
			return null;
		}
		return null;
	}
	
	public static String  getConvertedString(String str){
		
		logger_.warn("<<  getConvertedString() : [   str  :  {}]",str);
		
		try {
			if(StringUtils.isNotBlank(str)){
				String replace = str.trim().replace("-", "").replace("/", "");
				String substring = StringUtils.substring(replace, 0,9);
				
				logger_.warn(">> getConvertedString() : [  return  : {} ]",substring);
				return substring;
			}
		} catch (Exception e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		}
		
		return null;
	}
	
	public static boolean formatChecker(String date,String format){
			
		logger_.warn("<<  formatChecker() : [  date  :  {},  format  :  {}]",date,format);
		String formatedDate;
		SimpleDateFormat sdf = null;
		
		try {
			sdf= new SimpleDateFormat(format);
			formatedDate = sdf.format(sdf.parse(date));
			if(StringUtils.equalsIgnoreCase(date, formatedDate)){
				return true;
			}
			
		} catch (ParseException e) {
			logger_.error("{}",e.getStackTrace());
			logger_.error("** "+e.getMessage(),e);
			return false;
		}
		
		
		logger_.warn(">>  formatChecker() : [  return  :  {}]",false);
		return false;
	}
	
	public static String getDateTimeAsStringAndFormatIt(String requestDateTime_,String currentDateTimeFormat, String newDateTimeFormat) {

			if (StringUtils.isNotBlank(requestDateTime_)) {
				String newDateString = requestDateTime_.replace("-", "");
				logger_.warn(">>  getDateTimeAsStringAndFormatIt()  : [ return : {}]",newDateString);
				return newDateString;
			}
			return null;
	}
	public static String getDateAsStringAndFormatIt(String ageAsOn_,String currentDateFormat, String newDateFormat) {

		if (StringUtils.isNotBlank(ageAsOn_)) {
			String newDateString = ageAsOn_.replace("-", "");
			logger_.warn(">>  getDateAsStringAndFormatIt()  : [ return : {}]",newDateString);
			return newDateString;
		}
		return null;
	}

	public static String changeFormat (String dateString, String currentFormat, String newFormat) {
		SimpleDateFormat sdf;
		try {
			sdf = new SimpleDateFormat(currentFormat);
		} catch (Exception e) {
			logger_.warn(">>  changeFormat()  : [ return : {}]", "null");
			return null;
		}
		
		Date convertedDate = null;
		
		try {
			convertedDate = sdf.parse(dateString);
		} catch (Exception e) {
			logger_.warn(">>  changeFormat()  : [ return : {}]", "null");
			return null;
		}
		
		String tempString = sdf.format(convertedDate);
		
		if (!StringUtils.equals(tempString, dateString)) {
			logger_.warn(">>  changeFormat()  : [ return : {}]", "null");
			return null;
		}
		
		SimpleDateFormat newSdf = null;
		try {
			newSdf = new SimpleDateFormat(newFormat);
		} catch (Exception e) {
		}
		
		String finalResultString = newSdf.format(convertedDate);
		
		logger_.warn(">>  changeFormat()  : [ return : {}]", finalResultString);
		return finalResultString;
	}

	public static String convert(String requestDateTime_, String format1_, String format2_) {
		
		SimpleDateFormat sdf = new SimpleDateFormat(format1_);
		String returnVal = null;
		
		if(StringUtils.isNotBlank(requestDateTime_)){
			Date date = null;
			try {
				date = sdf.parse(requestDateTime_);
			} catch (ParseException e) {
				System.out.println("INVALID DATE FORMAT PROVIDED FOR PARSING DATE OBJECT {DATE : "+requestDateTime_+" FORMAT1 : "+format1_+" FORMAT2 : "+format2_+"}");
				return null;
			}
			try {
				sdf = new SimpleDateFormat(format2_);
				returnVal = sdf.format(date);
			} catch (Exception e) {
				System.out.println("INVALID DATE FORMAT PROVIDED FOR PARSING DATE OBJECT {DATE : "+requestDateTime_+" FORMAT1 : "+format1_+" FORMAT2 : "+format2_+"}");
				return null;
			}
			
			return returnVal;
		}
		
		return null;
	}
	
	public static int monthsBetween(Date minuend, Date subtrahend)
	{
		Calendar cal = Calendar.getInstance();
		// default will be Gregorian in US Locales
		cal.setTime(minuend);
		int minuendMonth =  cal.get(Calendar.MONTH);
		int minuendYear = cal.get(Calendar.YEAR);
		cal.setTime(subtrahend);
		int subtrahendMonth =  cal.get(Calendar.MONTH);
		int subtrahendYear = cal.get(Calendar.YEAR);
		 
		// the following will work okay for Gregorian but will not
		// work correctly in a Calendar where the number of months 
		// in a year is not constant
		return ((minuendYear - subtrahendYear) * cal.getMaximum(Calendar.MONTH)) +  
		(minuendMonth - subtrahendMonth) + 1;
	}
	
	public static int daysBetween(Date minuend, Date subtrahend)
	{
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		// default will be Gregorian in US Locales
		cal1.setTime(minuend);
		cal2.setTime(subtrahend);
		 
		 // Get the represented date in milliseconds
        long milis1 = cal1.getTimeInMillis();
        long milis2 = cal2.getTimeInMillis();
        
        // Calculate difference in milliseconds
        long diff = milis1 - milis2;
       // Calculate difference in days
       long diffDays = diff / (24 * 60 * 60 * 1000);
       return (int) diffDays;
       
	}
	
	public static Date getLatestDate(Date dateObj1, Date dateObj2)
	{
		if(dateObj1.compareTo(dateObj2)>0){
    		System.out.println("Date1 is after Date2");
    		return dateObj1;
    	}else if(dateObj1.compareTo(dateObj2)<0){
    		System.out.println("Date1 is before Date2");
    		return dateObj2;
    	}else if(dateObj1.compareTo(dateObj2)==0){
    		return dateObj1;
    	} else {
    		return dateObj1;
    	}
		
	}
	
	

	public static boolean isBeforeMonth(Date reportedDate,  Date accountOpenDate ) throws Exception {
		
		DateTime first = new DateTime(reportedDate);
		DateTime second =new DateTime(accountOpenDate);

		LocalDate firstDate = first.toLocalDate();
		LocalDate secondDate = second.toLocalDate();

		return (secondDate.compareTo(firstDate) <= 0);
	}
	
	
	public static Date getPreviousDate(Date reportedDate, int month) throws Exception {
		Calendar calendar = Calendar.getInstance();
	    calendar.setTime(reportedDate);
	    calendar.add(Calendar.MONTH, month);
	    return calendar.getTime();
	}
	
	
	public static Date getDateFromMonthYear(String month, String year) throws Exception {
		Calendar calendar = Calendar.getInstance();
		 calendar.set(Calendar.YEAR, Integer.parseInt(year));
	    calendar.set(Calendar.MONTH, getMonth(month));
	    calendar.set(Calendar.DATE,     calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	    return calendar.getTime();
	}
	
	private static int getMonth (String month) {
		
		switch (month) {
		
		case "Jan" : return 0;
		case "Feb" : return 1;
		case "Mar" : return 2;
		case "Apr" : return 3;
		case "May" : return 4;
		case "Jun" : return 5;
		case "Jul" : return 6;
		case "Aug" : return 7;
		case "Sep" : return 8;
		case "Oct" : return 9;
		case "Nov" : return 10;
		case "Dec" : return 11;
		default : return 0;
		}
	}

	public static Date changeFormat(String inputDtString, String format) {
		logger_.info("<<  changeFormat() : [ inputDtString : {},  format : {} ]",inputDtString,format);
		
	        Date date = null;
			try {
				SimpleDateFormat originalFormat = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat targetFormat = new SimpleDateFormat(format);
				date = originalFormat.parse(inputDtString);
				String formatedDate = targetFormat.format(date); 
//		        System.out.println(formatedDate);
				return date;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				logger_.error("{}",e.getStackTrace());
				return null;
			}
	}
	
	public static String expChangeFormate(String inputDateString, String changeFormat){
		logger_.info("<< expChangeFormate() : [ inputDateString : {}, changeFormat : {} ]", inputDateString,changeFormat);
		Date date = null;
		try{
			SimpleDateFormat expDtFormat = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat targetFormat = new SimpleDateFormat(changeFormat);
			date = expDtFormat.parse(inputDateString);
			String formatedDate = targetFormat.format(date); 
//	        System.out.println(formatedDate);
	        return formatedDate;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger_.error("{}",e.getStackTrace());
			return null;
		}
	}
	
	public static int monthsBetweenJoda(Date minuend, Date subtrahend)
	{
		
		DateTime dt1 = new DateTime(subtrahend);
		DateTime dt2 = new DateTime(minuend);
		
		int months = 0;
		months = Months.monthsBetween(dt1, dt2).getMonths();
		
		return months;
		//return ;
		
		//return 0;
	}
	
	public static Date getINDVDateFromString(String inputDtString,String format){

		logger_.warn("<<  getDateFromString() : [ inputDtString : {},  format : {} ]",inputDtString,format);
		
		SimpleDateFormat sdf = null;
		
		Date outputDt = null;
		//inputDtString = 20150430
		//format = yyyy-MM-dd HH:mm:ss
		try {
			sdf= new SimpleDateFormat(format);
			String formatedDate = sdf.format(sdf.parse(inputDtString));
			
			//if(StringUtils.equalsIgnoreCase(inputDtString, formatedDate)){
				outputDt = sdf.parse(inputDtString);
			//}
		} catch (NullPointerException e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		} catch (IllegalArgumentException e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		} catch (ParseException e) {
			logger_.error("** "+e.getMessage(),e);
			return null;
		}

		logger_.warn(">>  getDateFromString() : [ return : {} ]",outputDt);
		return outputDt;
	}


	public static Date getEndOfDay(Date date) {
		return org.apache.commons.lang3.time.DateUtils.addMilliseconds(org.apache.commons.lang3.time.DateUtils.ceiling(date, Calendar.DATE), -1);
	}

	public static Date getStartOfDay(Date date) {
		return org.apache.commons.lang3.time.DateUtils.truncate(date, Calendar.DATE);
	}

}
