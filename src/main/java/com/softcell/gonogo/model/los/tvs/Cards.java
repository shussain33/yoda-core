package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cards {

	private String maxCreditUtilization;

	private String totalCCPaymentMonthYear0;

	private String totalCCPaymentMonthYear1;

	private String totalCCPaymentMonthYear2;

	private String sno;

	private String latestCCDueDate;

	private String countOverdueLast3Months;

	private String totalCCTraxnAmountMonthYear2;

	private String totalCCTraxnAmountMonthYear1;

	private String totalCCTraxnAmountMonthYear0;

	private String creditLimit;

	private String issuerName;

	private String countOverdueEver;


}
