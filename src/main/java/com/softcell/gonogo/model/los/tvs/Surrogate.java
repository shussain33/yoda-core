package com.softcell.gonogo.model.los.tvs;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Surrogate {

        private CreditCard creditCard;

        private DebitCard debitCard;

        private OwnedHouse ownedHouse;

        private OwnedCar ownedCar;

        private SalaryHome salaryHome;

        private Business business;
}
