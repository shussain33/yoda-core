package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Request
{
private String ageAsOn;

private String loanAmount;

private String aka;

private String creditRequestType;

private String mfiscore;

private Ids ids;

private String creditInquiryStage;

private String creditInquiryPurposeType;

private String email1;

private Phones phones;

private String mfiInd;

private String cnsscore;

private String creditInquiryPurposeTypeDescription;

private String mfigroup;

private String name;

private String dob;

private String gender;

private String branch;

private String creditReportTransectionDatetime;

private String memberId;

private Addresses addresses;

private String losApplicationId;

private String cnsind;

}
