package com.softcell.gonogo.model.los.tvr;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.Date;

/**
 * Created by yogeshb on 4/7/17.
 */
@Builder
public class TvrRequestDTO {
    @JsonProperty("sApplicationId")
    private String applicationId;

    @JsonProperty("dtReqDt")
    private Date requestDate;

    @JsonProperty("sDsaId")
    private String dsaId;

    @JsonProperty("sDealerId")
    private int dealerId;

    @JsonProperty("dtLogin")
    private Date loginDate;

    @JsonProperty("sTimeStamp")
    private String timeStamp;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sQueueId")
    private String queueId;

    @JsonProperty("sFirstName")
    private String firstName;

    @JsonProperty("sMiddleName")
    private String middleName;

    @JsonProperty("sLastName")
    private String lastName;

    @JsonProperty("sMaritalStatus")
    private String maritalStatus;

    @JsonProperty("sGender")
    private String gender;

    @JsonProperty("iDob")
    private int dob;

    @JsonProperty("sEducation")
    private String education;

    @JsonProperty("sResidenceType")
    private String residenceType;

    @JsonProperty("sAddressLine1")
    private String addressLine1;

    @JsonProperty("sAddressLine2")
    private String addressLine2;

    @JsonProperty("sAddressLine3")
    private String addressLine3;

    @JsonProperty("iZipCode")
    private int zipCode;

    @JsonProperty("sCity")
    private String city;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sOfficeResidenceType")
    private String oResidenceType;

    @JsonProperty("sOfficeAddressLine1")
    private String oAddressLine1;

    @JsonProperty("sOfficeAddressLine2")
    private String oAddressLine2;

    @JsonProperty("sOfficeAddressLine3")
    private String oAddressLine3;

    @JsonProperty("sOfficeZipCode")
    private int oZipCode;

    @JsonProperty("sOfficeCity")
    private String oCity;

    @JsonProperty("sOfficeState")
    private String oState;

    @JsonProperty("sPPresidenceType")
    private String pPresidenceType;

    @JsonProperty("sPAddressLine2")
    private String pAddressLine2;

    @JsonProperty("sPAddressLine1")
    private String pAddressLine1;

    @JsonProperty("sPAddressLine3")
    private String pAddressLine3;

    @JsonProperty("iPZipCode")
    private int pZipCode;

    @JsonProperty("sPCity")
    private String pCity;

    @JsonProperty("sPState")
    private String pState;

    @JsonProperty("sPPhoneType")
    private String pPhoneType;

    @JsonProperty("lPPhoneAreaCode")
    private long pPhoneAreaCode;

    @JsonProperty("lPPhoneNumber")
    private long pPhoneNumber;

    @JsonProperty("sP2PhoneType")
    private String p2PhoneType;

    @JsonProperty("iP2PhoneAreaCode")
    private int p2PhoneAreaCode;

    @JsonProperty("lP2PhoneNumber")
    private long p2PhoneNumber;

    @JsonProperty("sResHomeTyrE")
    private String rHomeTyrE;

    @JsonProperty("sResHomeExt")
    private String rHomeExt;

    @JsonProperty("sResHomeAreaCode")
    private int rHomeAreaCode;

    @JsonProperty("sResHomeNumber")
    private long rHomeNumber;

    @JsonProperty("sOfficePhoneType")
    private String oPhoneType;

    @JsonProperty("sOfficePhoneExt")
    private String oPhoneExt;

    @JsonProperty("iOfficePhoneAreaCode")
    private int oPhoneAreaCode;

    @JsonProperty("lOfficePhoneNumber")
    private long oPhoneNumber;

    @JsonProperty("sResEmailType")
    private String rEmailType;

    @JsonProperty("sResEmailAddress")
    private String rEmailAddress;

    @JsonProperty("sOfficeEmailType")
    private String oEmailType;

    @JsonProperty("sOfficeEmailAddress")
    private String oEmailAddress;

    @JsonProperty("sEmployConstitution")
    private String employConstitution;

    @JsonProperty("sEmployerName")
    private String employerName;

    @JsonProperty("sKyc1Pan")
    private String kyc1Pan;

    @JsonProperty("sKyc2VoterId")
    private String kyc2VoterId;

    @JsonProperty("sKyc3Passport")
    private String kyc3Passport;

    @JsonProperty("sKyc4DrivingLicense")
    private String kyc4DrivingLicense;

    @JsonProperty("lKyc5AadhaarNumber")
    private long kyc5AadhaarNumber;

    @JsonProperty("sKyc6Other")
    private String kyc6Other;

    @JsonProperty("sSurrogate")
    private String surrogate;

    @JsonProperty("iNetTakeHomeSalary")
    private int netTakeHomeSalary;

    @JsonProperty("sCarModelName")
    private String carModelName;

    @JsonProperty("sCarCategory")
    private String carCategory;

    @JsonProperty("dtCarManufacturer")
    private Date carManufacturerYear;

    @JsonProperty("sCarRegistrationNumber")
    private String carRegistrationNumber;

    @JsonProperty("sOwnHouseType")
    private String ownHouseType;

    @JsonProperty("sHouseSurrogateDocumentType")
    private String houseSurrogateDocumentType;

    @JsonProperty("shTraderYearInBusiness")
    private short traderYearInBusiness;

    @JsonProperty("sTraderBusinessProof")
    private String traderBusinessProof;

    @JsonProperty("lCreditCardNumber")
    private long creditCardNumber;

    @JsonProperty("sCreditCardsCategory")
    private String creditCardsCategory;

    @JsonProperty("sDedupeStatus")
    private String dedupeStatus;

    @JsonProperty("iApprovedAmountByCro")
    private int approvedAmountByCro;

    @JsonProperty("sApprovedDate")
    private String approvedDate;

    @JsonProperty("lEligibleAmount")
    private long eligibleAmount;

    @JsonProperty("sPanName")
    private String panName;

    @JsonProperty("sPanStatus")
    private String panStatus;

    @JsonProperty("sCibilScore")
    private String cibilScore;

    @JsonProperty("iApplicationScore")
    private int applicationScore;

    @JsonProperty("sNegativePinFlag")
    private String negativePinFlag;

    @JsonProperty("iAppliedAmount")
    private int appliedAmount;

    @JsonProperty("sPrimaryAssetCtg")
    private String primaryAssetCtg;

    @JsonProperty("sPrimaryAssetMake")
    private String primaryAssetMake;

    @JsonProperty("sPrimaryAssetModelNo")
    private String primaryAssetModelNo;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getDsaId() {
        return dsaId;
    }

    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }

    public int getDealerId() {
        return dealerId;
    }

    public void setDealerId(int dealerId) {
        this.dealerId = dealerId;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQueueId() {
        return queueId;
    }

    public void setQueueId(String queueId) {
        this.queueId = queueId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getDob() {
        return dob;
    }

    public void setDob(int dob) {
        this.dob = dob;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getResidenceType() {
        return residenceType;
    }

    public void setResidenceType(String residenceType) {
        this.residenceType = residenceType;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getoResidenceType() {
        return oResidenceType;
    }

    public void setoResidenceType(String oResidenceType) {
        this.oResidenceType = oResidenceType;
    }

    public String getoAddressLine1() {
        return oAddressLine1;
    }

    public void setoAddressLine1(String oAddressLine1) {
        this.oAddressLine1 = oAddressLine1;
    }

    public String getoAddressLine2() {
        return oAddressLine2;
    }

    public void setoAddressLine2(String oAddressLine2) {
        this.oAddressLine2 = oAddressLine2;
    }

    public String getoAddressLine3() {
        return oAddressLine3;
    }

    public void setoAddressLine3(String oAddressLine3) {
        this.oAddressLine3 = oAddressLine3;
    }

    public int getoZipCode() {
        return oZipCode;
    }

    public void setoZipCode(int oZipCode) {
        this.oZipCode = oZipCode;
    }

    public String getoCity() {
        return oCity;
    }

    public void setoCity(String oCity) {
        this.oCity = oCity;
    }

    public String getoState() {
        return oState;
    }

    public void setoState(String oState) {
        this.oState = oState;
    }

    public String getpPresidenceType() {
        return pPresidenceType;
    }

    public void setpPresidenceType(String pPresidenceType) {
        this.pPresidenceType = pPresidenceType;
    }

    public String getpAddressLine2() {
        return pAddressLine2;
    }

    public void setpAddressLine2(String pAddressLine2) {
        this.pAddressLine2 = pAddressLine2;
    }

    public String getpAddressLine1() {
        return pAddressLine1;
    }

    public void setpAddressLine1(String pAddressLine1) {
        this.pAddressLine1 = pAddressLine1;
    }

    public String getpAddressLine3() {
        return pAddressLine3;
    }

    public void setpAddressLine3(String pAddressLine3) {
        this.pAddressLine3 = pAddressLine3;
    }

    public int getpZipCode() {
        return pZipCode;
    }

    public void setpZipCode(int pZipCode) {
        this.pZipCode = pZipCode;
    }

    public String getpCity() {
        return pCity;
    }

    public void setpCity(String pCity) {
        this.pCity = pCity;
    }

    public String getpState() {
        return pState;
    }

    public void setpState(String pState) {
        this.pState = pState;
    }

    public String getpPhoneType() {
        return pPhoneType;
    }

    public void setpPhoneType(String pPhoneType) {
        this.pPhoneType = pPhoneType;
    }

    public long getpPhoneAreaCode() {
        return pPhoneAreaCode;
    }

    public void setpPhoneAreaCode(long pPhoneAreaCode) {
        this.pPhoneAreaCode = pPhoneAreaCode;
    }

    public long getpPhoneNumber() {
        return pPhoneNumber;
    }

    public void setpPhoneNumber(long pPhoneNumber) {
        this.pPhoneNumber = pPhoneNumber;
    }

    public String getP2PhoneType() {
        return p2PhoneType;
    }

    public void setP2PhoneType(String p2PhoneType) {
        this.p2PhoneType = p2PhoneType;
    }

    public int getP2PhoneAreaCode() {
        return p2PhoneAreaCode;
    }

    public void setP2PhoneAreaCode(int p2PhoneAreaCode) {
        this.p2PhoneAreaCode = p2PhoneAreaCode;
    }

    public long getP2PhoneNumber() {
        return p2PhoneNumber;
    }

    public void setP2PhoneNumber(long p2PhoneNumber) {
        this.p2PhoneNumber = p2PhoneNumber;
    }

    public String getrHomeTyrE() {
        return rHomeTyrE;
    }

    public void setrHomeTyrE(String rHomeTyrE) {
        this.rHomeTyrE = rHomeTyrE;
    }

    public String getrHomeExt() {
        return rHomeExt;
    }

    public void setrHomeExt(String rHomeExt) {
        this.rHomeExt = rHomeExt;
    }

    public int getrHomeAreaCode() {
        return rHomeAreaCode;
    }

    public void setrHomeAreaCode(int rHomeAreaCode) {
        this.rHomeAreaCode = rHomeAreaCode;
    }

    public long getrHomeNumber() {
        return rHomeNumber;
    }

    public void setrHomeNumber(long rHomeNumber) {
        this.rHomeNumber = rHomeNumber;
    }

    public String getoPhoneType() {
        return oPhoneType;
    }

    public void setoPhoneType(String oPhoneType) {
        this.oPhoneType = oPhoneType;
    }

    public String getoPhoneExt() {
        return oPhoneExt;
    }

    public void setoPhoneExt(String oPhoneExt) {
        this.oPhoneExt = oPhoneExt;
    }

    public int getoPhoneAreaCode() {
        return oPhoneAreaCode;
    }

    public void setoPhoneAreaCode(int oPhoneAreaCode) {
        this.oPhoneAreaCode = oPhoneAreaCode;
    }

    public long getoPhoneNumber() {
        return oPhoneNumber;
    }

    public void setoPhoneNumber(long oPhoneNumber) {
        this.oPhoneNumber = oPhoneNumber;
    }

    public String getrEmailType() {
        return rEmailType;
    }

    public void setrEmailType(String rEmailType) {
        this.rEmailType = rEmailType;
    }

    public String getrEmailAddress() {
        return rEmailAddress;
    }

    public void setrEmailAddress(String rEmailAddress) {
        this.rEmailAddress = rEmailAddress;
    }

    public String getoEmailType() {
        return oEmailType;
    }

    public void setoEmailType(String oEmailType) {
        this.oEmailType = oEmailType;
    }

    public String getoEmailAddress() {
        return oEmailAddress;
    }

    public void setoEmailAddress(String oEmailAddress) {
        this.oEmailAddress = oEmailAddress;
    }

    public String getEmployConstitution() {
        return employConstitution;
    }

    public void setEmployConstitution(String employConstitution) {
        this.employConstitution = employConstitution;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getKyc1Pan() {
        return kyc1Pan;
    }

    public void setKyc1Pan(String kyc1Pan) {
        this.kyc1Pan = kyc1Pan;
    }

    public String getKyc2VoterId() {
        return kyc2VoterId;
    }

    public void setKyc2VoterId(String kyc2VoterId) {
        this.kyc2VoterId = kyc2VoterId;
    }

    public String getKyc3Passport() {
        return kyc3Passport;
    }

    public void setKyc3Passport(String kyc3Passport) {
        this.kyc3Passport = kyc3Passport;
    }

    public String getKyc4DrivingLicense() {
        return kyc4DrivingLicense;
    }

    public void setKyc4DrivingLicense(String kyc4DrivingLicense) {
        this.kyc4DrivingLicense = kyc4DrivingLicense;
    }

    public long getKyc5AadhaarNumber() {
        return kyc5AadhaarNumber;
    }

    public void setKyc5AadhaarNumber(long kyc5AadhaarNumber) {
        this.kyc5AadhaarNumber = kyc5AadhaarNumber;
    }

    public String getKyc6Other() {
        return kyc6Other;
    }

    public void setKyc6Other(String kyc6Other) {
        this.kyc6Other = kyc6Other;
    }

    public String getSurrogate() {
        return surrogate;
    }

    public void setSurrogate(String surrogate) {
        this.surrogate = surrogate;
    }

    public int getNetTakeHomeSalary() {
        return netTakeHomeSalary;
    }

    public void setNetTakeHomeSalary(int netTakeHomeSalary) {
        this.netTakeHomeSalary = netTakeHomeSalary;
    }

    public String getCarModelName() {
        return carModelName;
    }

    public void setCarModelName(String carModelName) {
        this.carModelName = carModelName;
    }

    public String getCarCategory() {
        return carCategory;
    }

    public void setCarCategory(String carCategory) {
        this.carCategory = carCategory;
    }

    public Date getCarManufacturerYear() {
        return carManufacturerYear;
    }

    public void setCarManufacturerYear(Date carManufacturerYear) {
        this.carManufacturerYear = carManufacturerYear;
    }

    public String getCarRegistrationNumber() {
        return carRegistrationNumber;
    }

    public void setCarRegistrationNumber(String carRegistrationNumber) {
        this.carRegistrationNumber = carRegistrationNumber;
    }

    public String getOwnHouseType() {
        return ownHouseType;
    }

    public void setOwnHouseType(String ownHouseType) {
        this.ownHouseType = ownHouseType;
    }

    public String getHouseSurrogateDocumentType() {
        return houseSurrogateDocumentType;
    }

    public void setHouseSurrogateDocumentType(String houseSurrogateDocumentType) {
        this.houseSurrogateDocumentType = houseSurrogateDocumentType;
    }

    public short getTraderYearInBusiness() {
        return traderYearInBusiness;
    }

    public void setTraderYearInBusiness(short traderYearInBusiness) {
        this.traderYearInBusiness = traderYearInBusiness;
    }

    public String getTraderBusinessProof() {
        return traderBusinessProof;
    }

    public void setTraderBusinessProof(String traderBusinessProof) {
        this.traderBusinessProof = traderBusinessProof;
    }

    public long getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(long creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardsCategory() {
        return creditCardsCategory;
    }

    public void setCreditCardsCategory(String creditCardsCategory) {
        this.creditCardsCategory = creditCardsCategory;
    }

    public String getDedupeStatus() {
        return dedupeStatus;
    }

    public void setDedupeStatus(String dedupeStatus) {
        this.dedupeStatus = dedupeStatus;
    }

    public int getApprovedAmountByCro() {
        return approvedAmountByCro;
    }

    public void setApprovedAmountByCro(int approvedAmountByCro) {
        this.approvedAmountByCro = approvedAmountByCro;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public long getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(long eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public String getPanName() {
        return panName;
    }

    public void setPanName(String panName) {
        this.panName = panName;
    }

    public String getPanStatus() {
        return panStatus;
    }

    public void setPanStatus(String panStatus) {
        this.panStatus = panStatus;
    }

    public String getCibilScore() {
        return cibilScore;
    }

    public void setCibilScore(String cibilScore) {
        this.cibilScore = cibilScore;
    }

    public int getApplicationScore() {
        return applicationScore;
    }

    public void setApplicationScore(int applicationScore) {
        this.applicationScore = applicationScore;
    }

    public String getNegativePinFlag() {
        return negativePinFlag;
    }

    public void setNegativePinFlag(String negativePinFlag) {
        this.negativePinFlag = negativePinFlag;
    }

    public int getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(int appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public String getPrimaryAssetCtg() {
        return primaryAssetCtg;
    }

    public void setPrimaryAssetCtg(String primaryAssetCtg) {
        this.primaryAssetCtg = primaryAssetCtg;
    }

    public String getPrimaryAssetMake() {
        return primaryAssetMake;
    }

    public void setPrimaryAssetMake(String primaryAssetMake) {
        this.primaryAssetMake = primaryAssetMake;
    }

    public String getPrimaryAssetModelNo() {
        return primaryAssetModelNo;
    }

    public void setPrimaryAssetModelNo(String primaryAssetModelNo) {
        this.primaryAssetModelNo = primaryAssetModelNo;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TvrRequestDTO{");
        sb.append("applicationId='").append(applicationId).append('\'');
        sb.append(", requestDate=").append(requestDate);
        sb.append(", dsaId='").append(dsaId).append('\'');
        sb.append(", dealerId=").append(dealerId);
        sb.append(", loginDate=").append(loginDate);
        sb.append(", timeStamp='").append(timeStamp).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", queueId='").append(queueId).append('\'');
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", middleName='").append(middleName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", maritalStatus='").append(maritalStatus).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", dob=").append(dob);
        sb.append(", education='").append(education).append('\'');
        sb.append(", residenceType='").append(residenceType).append('\'');
        sb.append(", addressLine1='").append(addressLine1).append('\'');
        sb.append(", addressLine2='").append(addressLine2).append('\'');
        sb.append(", addressLine3='").append(addressLine3).append('\'');
        sb.append(", zipCode=").append(zipCode);
        sb.append(", city='").append(city).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", oResidenceType='").append(oResidenceType).append('\'');
        sb.append(", oAddressLine1='").append(oAddressLine1).append('\'');
        sb.append(", oAddressLine2='").append(oAddressLine2).append('\'');
        sb.append(", oAddressLine3='").append(oAddressLine3).append('\'');
        sb.append(", oZipCode=").append(oZipCode);
        sb.append(", oCity='").append(oCity).append('\'');
        sb.append(", oState='").append(oState).append('\'');
        sb.append(", pPresidenceType='").append(pPresidenceType).append('\'');
        sb.append(", pAddressLine2='").append(pAddressLine2).append('\'');
        sb.append(", pAddressLine1='").append(pAddressLine1).append('\'');
        sb.append(", pAddressLine3='").append(pAddressLine3).append('\'');
        sb.append(", pZipCode=").append(pZipCode);
        sb.append(", pCity='").append(pCity).append('\'');
        sb.append(", pState='").append(pState).append('\'');
        sb.append(", pPhoneType='").append(pPhoneType).append('\'');
        sb.append(", pPhoneAreaCode=").append(pPhoneAreaCode);
        sb.append(", pPhoneNumber=").append(pPhoneNumber);
        sb.append(", p2PhoneType='").append(p2PhoneType).append('\'');
        sb.append(", p2PhoneAreaCode=").append(p2PhoneAreaCode);
        sb.append(", p2PhoneNumber=").append(p2PhoneNumber);
        sb.append(", rHomeTyrE='").append(rHomeTyrE).append('\'');
        sb.append(", rHomeExt='").append(rHomeExt).append('\'');
        sb.append(", rHomeAreaCode=").append(rHomeAreaCode);
        sb.append(", rHomeNumber=").append(rHomeNumber);
        sb.append(", oPhoneType='").append(oPhoneType).append('\'');
        sb.append(", oPhoneExt='").append(oPhoneExt).append('\'');
        sb.append(", oPhoneAreaCode=").append(oPhoneAreaCode);
        sb.append(", oPhoneNumber=").append(oPhoneNumber);
        sb.append(", rEmailType='").append(rEmailType).append('\'');
        sb.append(", rEmailAddress='").append(rEmailAddress).append('\'');
        sb.append(", oEmailType='").append(oEmailType).append('\'');
        sb.append(", oEmailAddress='").append(oEmailAddress).append('\'');
        sb.append(", employConstitution='").append(employConstitution).append('\'');
        sb.append(", employerName='").append(employerName).append('\'');
        sb.append(", kyc1Pan='").append(kyc1Pan).append('\'');
        sb.append(", kyc2VoterId='").append(kyc2VoterId).append('\'');
        sb.append(", kyc3Passport='").append(kyc3Passport).append('\'');
        sb.append(", kyc4DrivingLicense='").append(kyc4DrivingLicense).append('\'');
        sb.append(", kyc5AadhaarNumber=").append(kyc5AadhaarNumber);
        sb.append(", kyc6Other='").append(kyc6Other).append('\'');
        sb.append(", surrogate='").append(surrogate).append('\'');
        sb.append(", netTakeHomeSalary=").append(netTakeHomeSalary);
        sb.append(", carModelName='").append(carModelName).append('\'');
        sb.append(", carCategory='").append(carCategory).append('\'');
        sb.append(", carManufacturerYear=").append(carManufacturerYear);
        sb.append(", carRegistrationNumber='").append(carRegistrationNumber).append('\'');
        sb.append(", ownHouseType='").append(ownHouseType).append('\'');
        sb.append(", houseSurrogateDocumentType='").append(houseSurrogateDocumentType).append('\'');
        sb.append(", traderYearInBusiness=").append(traderYearInBusiness);
        sb.append(", traderBusinessProof='").append(traderBusinessProof).append('\'');
        sb.append(", creditCardNumber=").append(creditCardNumber);
        sb.append(", creditCardsCategory='").append(creditCardsCategory).append('\'');
        sb.append(", dedupeStatus='").append(dedupeStatus).append('\'');
        sb.append(", approvedAmountByCro=").append(approvedAmountByCro);
        sb.append(", approvedDate='").append(approvedDate).append('\'');
        sb.append(", eligibleAmount=").append(eligibleAmount);
        sb.append(", panName='").append(panName).append('\'');
        sb.append(", panStatus='").append(panStatus).append('\'');
        sb.append(", cibilScore='").append(cibilScore).append('\'');
        sb.append(", applicationScore=").append(applicationScore);
        sb.append(", negativePinFlag='").append(negativePinFlag).append('\'');
        sb.append(", appliedAmount=").append(appliedAmount);
        sb.append(", primaryAssetCtg='").append(primaryAssetCtg).append('\'');
        sb.append(", primaryAssetMake='").append(primaryAssetMake).append('\'');
        sb.append(", primaryAssetModelNo='").append(primaryAssetModelNo).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TvrRequestDTO that = (TvrRequestDTO) o;

        if (dealerId != that.dealerId) return false;
        if (dob != that.dob) return false;
        if (zipCode != that.zipCode) return false;
        if (oZipCode != that.oZipCode) return false;
        if (pZipCode != that.pZipCode) return false;
        if (pPhoneAreaCode != that.pPhoneAreaCode) return false;
        if (pPhoneNumber != that.pPhoneNumber) return false;
        if (p2PhoneAreaCode != that.p2PhoneAreaCode) return false;
        if (p2PhoneNumber != that.p2PhoneNumber) return false;
        if (rHomeAreaCode != that.rHomeAreaCode) return false;
        if (rHomeNumber != that.rHomeNumber) return false;
        if (oPhoneAreaCode != that.oPhoneAreaCode) return false;
        if (oPhoneNumber != that.oPhoneNumber) return false;
        if (kyc5AadhaarNumber != that.kyc5AadhaarNumber) return false;
        if (netTakeHomeSalary != that.netTakeHomeSalary) return false;
        if (traderYearInBusiness != that.traderYearInBusiness) return false;
        if (creditCardNumber != that.creditCardNumber) return false;
        if (approvedAmountByCro != that.approvedAmountByCro) return false;
        if (eligibleAmount != that.eligibleAmount) return false;
        if (applicationScore != that.applicationScore) return false;
        if (appliedAmount != that.appliedAmount) return false;
        if (applicationId != null ? !applicationId.equals(that.applicationId) : that.applicationId != null)
            return false;
        if (requestDate != null ? !requestDate.equals(that.requestDate) : that.requestDate != null) return false;
        if (dsaId != null ? !dsaId.equals(that.dsaId) : that.dsaId != null) return false;
        if (loginDate != null ? !loginDate.equals(that.loginDate) : that.loginDate != null) return false;
        if (timeStamp != null ? !timeStamp.equals(that.timeStamp) : that.timeStamp != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (queueId != null ? !queueId.equals(that.queueId) : that.queueId != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (maritalStatus != null ? !maritalStatus.equals(that.maritalStatus) : that.maritalStatus != null)
            return false;
        if (gender != null ? !gender.equals(that.gender) : that.gender != null) return false;
        if (education != null ? !education.equals(that.education) : that.education != null) return false;
        if (residenceType != null ? !residenceType.equals(that.residenceType) : that.residenceType != null)
            return false;
        if (addressLine1 != null ? !addressLine1.equals(that.addressLine1) : that.addressLine1 != null) return false;
        if (addressLine2 != null ? !addressLine2.equals(that.addressLine2) : that.addressLine2 != null) return false;
        if (addressLine3 != null ? !addressLine3.equals(that.addressLine3) : that.addressLine3 != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        if (oResidenceType != null ? !oResidenceType.equals(that.oResidenceType) : that.oResidenceType != null)
            return false;
        if (oAddressLine1 != null ? !oAddressLine1.equals(that.oAddressLine1) : that.oAddressLine1 != null)
            return false;
        if (oAddressLine2 != null ? !oAddressLine2.equals(that.oAddressLine2) : that.oAddressLine2 != null)
            return false;
        if (oAddressLine3 != null ? !oAddressLine3.equals(that.oAddressLine3) : that.oAddressLine3 != null)
            return false;
        if (oCity != null ? !oCity.equals(that.oCity) : that.oCity != null) return false;
        if (oState != null ? !oState.equals(that.oState) : that.oState != null) return false;
        if (pPresidenceType != null ? !pPresidenceType.equals(that.pPresidenceType) : that.pPresidenceType != null)
            return false;
        if (pAddressLine2 != null ? !pAddressLine2.equals(that.pAddressLine2) : that.pAddressLine2 != null)
            return false;
        if (pAddressLine1 != null ? !pAddressLine1.equals(that.pAddressLine1) : that.pAddressLine1 != null)
            return false;
        if (pAddressLine3 != null ? !pAddressLine3.equals(that.pAddressLine3) : that.pAddressLine3 != null)
            return false;
        if (pCity != null ? !pCity.equals(that.pCity) : that.pCity != null) return false;
        if (pState != null ? !pState.equals(that.pState) : that.pState != null) return false;
        if (pPhoneType != null ? !pPhoneType.equals(that.pPhoneType) : that.pPhoneType != null) return false;
        if (p2PhoneType != null ? !p2PhoneType.equals(that.p2PhoneType) : that.p2PhoneType != null) return false;
        if (rHomeTyrE != null ? !rHomeTyrE.equals(that.rHomeTyrE) : that.rHomeTyrE != null) return false;
        if (rHomeExt != null ? !rHomeExt.equals(that.rHomeExt) : that.rHomeExt != null) return false;
        if (oPhoneType != null ? !oPhoneType.equals(that.oPhoneType) : that.oPhoneType != null) return false;
        if (oPhoneExt != null ? !oPhoneExt.equals(that.oPhoneExt) : that.oPhoneExt != null) return false;
        if (rEmailType != null ? !rEmailType.equals(that.rEmailType) : that.rEmailType != null) return false;
        if (rEmailAddress != null ? !rEmailAddress.equals(that.rEmailAddress) : that.rEmailAddress != null)
            return false;
        if (oEmailType != null ? !oEmailType.equals(that.oEmailType) : that.oEmailType != null) return false;
        if (oEmailAddress != null ? !oEmailAddress.equals(that.oEmailAddress) : that.oEmailAddress != null)
            return false;
        if (employConstitution != null ? !employConstitution.equals(that.employConstitution) : that.employConstitution != null)
            return false;
        if (employerName != null ? !employerName.equals(that.employerName) : that.employerName != null) return false;
        if (kyc1Pan != null ? !kyc1Pan.equals(that.kyc1Pan) : that.kyc1Pan != null) return false;
        if (kyc2VoterId != null ? !kyc2VoterId.equals(that.kyc2VoterId) : that.kyc2VoterId != null) return false;
        if (kyc3Passport != null ? !kyc3Passport.equals(that.kyc3Passport) : that.kyc3Passport != null) return false;
        if (kyc4DrivingLicense != null ? !kyc4DrivingLicense.equals(that.kyc4DrivingLicense) : that.kyc4DrivingLicense != null)
            return false;
        if (kyc6Other != null ? !kyc6Other.equals(that.kyc6Other) : that.kyc6Other != null) return false;
        if (surrogate != null ? !surrogate.equals(that.surrogate) : that.surrogate != null) return false;
        if (carModelName != null ? !carModelName.equals(that.carModelName) : that.carModelName != null) return false;
        if (carCategory != null ? !carCategory.equals(that.carCategory) : that.carCategory != null) return false;
        if (carManufacturerYear != null ? !carManufacturerYear.equals(that.carManufacturerYear) : that.carManufacturerYear != null)
            return false;
        if (carRegistrationNumber != null ? !carRegistrationNumber.equals(that.carRegistrationNumber) : that.carRegistrationNumber != null)
            return false;
        if (ownHouseType != null ? !ownHouseType.equals(that.ownHouseType) : that.ownHouseType != null) return false;
        if (houseSurrogateDocumentType != null ? !houseSurrogateDocumentType.equals(that.houseSurrogateDocumentType) : that.houseSurrogateDocumentType != null)
            return false;
        if (traderBusinessProof != null ? !traderBusinessProof.equals(that.traderBusinessProof) : that.traderBusinessProof != null)
            return false;
        if (creditCardsCategory != null ? !creditCardsCategory.equals(that.creditCardsCategory) : that.creditCardsCategory != null)
            return false;
        if (dedupeStatus != null ? !dedupeStatus.equals(that.dedupeStatus) : that.dedupeStatus != null) return false;
        if (approvedDate != null ? !approvedDate.equals(that.approvedDate) : that.approvedDate != null) return false;
        if (panName != null ? !panName.equals(that.panName) : that.panName != null) return false;
        if (panStatus != null ? !panStatus.equals(that.panStatus) : that.panStatus != null) return false;
        if (cibilScore != null ? !cibilScore.equals(that.cibilScore) : that.cibilScore != null) return false;
        if (negativePinFlag != null ? !negativePinFlag.equals(that.negativePinFlag) : that.negativePinFlag != null)
            return false;
        if (primaryAssetCtg != null ? !primaryAssetCtg.equals(that.primaryAssetCtg) : that.primaryAssetCtg != null)
            return false;
        if (primaryAssetMake != null ? !primaryAssetMake.equals(that.primaryAssetMake) : that.primaryAssetMake != null)
            return false;
        return primaryAssetModelNo != null ? primaryAssetModelNo.equals(that.primaryAssetModelNo) : that.primaryAssetModelNo == null;
    }

    @Override
    public int hashCode() {
        int result = applicationId != null ? applicationId.hashCode() : 0;
        result = 31 * result + (requestDate != null ? requestDate.hashCode() : 0);
        result = 31 * result + (dsaId != null ? dsaId.hashCode() : 0);
        result = 31 * result + dealerId;
        result = 31 * result + (loginDate != null ? loginDate.hashCode() : 0);
        result = 31 * result + (timeStamp != null ? timeStamp.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (queueId != null ? queueId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (maritalStatus != null ? maritalStatus.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + dob;
        result = 31 * result + (education != null ? education.hashCode() : 0);
        result = 31 * result + (residenceType != null ? residenceType.hashCode() : 0);
        result = 31 * result + (addressLine1 != null ? addressLine1.hashCode() : 0);
        result = 31 * result + (addressLine2 != null ? addressLine2.hashCode() : 0);
        result = 31 * result + (addressLine3 != null ? addressLine3.hashCode() : 0);
        result = 31 * result + zipCode;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (oResidenceType != null ? oResidenceType.hashCode() : 0);
        result = 31 * result + (oAddressLine1 != null ? oAddressLine1.hashCode() : 0);
        result = 31 * result + (oAddressLine2 != null ? oAddressLine2.hashCode() : 0);
        result = 31 * result + (oAddressLine3 != null ? oAddressLine3.hashCode() : 0);
        result = 31 * result + oZipCode;
        result = 31 * result + (oCity != null ? oCity.hashCode() : 0);
        result = 31 * result + (oState != null ? oState.hashCode() : 0);
        result = 31 * result + (pPresidenceType != null ? pPresidenceType.hashCode() : 0);
        result = 31 * result + (pAddressLine2 != null ? pAddressLine2.hashCode() : 0);
        result = 31 * result + (pAddressLine1 != null ? pAddressLine1.hashCode() : 0);
        result = 31 * result + (pAddressLine3 != null ? pAddressLine3.hashCode() : 0);
        result = 31 * result + pZipCode;
        result = 31 * result + (pCity != null ? pCity.hashCode() : 0);
        result = 31 * result + (pState != null ? pState.hashCode() : 0);
        result = 31 * result + (pPhoneType != null ? pPhoneType.hashCode() : 0);
        result = 31 * result + (int) (pPhoneAreaCode ^ (pPhoneAreaCode >>> 32));
        result = 31 * result + (int) (pPhoneNumber ^ (pPhoneNumber >>> 32));
        result = 31 * result + (p2PhoneType != null ? p2PhoneType.hashCode() : 0);
        result = 31 * result + p2PhoneAreaCode;
        result = 31 * result + (int) (p2PhoneNumber ^ (p2PhoneNumber >>> 32));
        result = 31 * result + (rHomeTyrE != null ? rHomeTyrE.hashCode() : 0);
        result = 31 * result + (rHomeExt != null ? rHomeExt.hashCode() : 0);
        result = 31 * result + rHomeAreaCode;
        result = 31 * result + (int) (rHomeNumber ^ (rHomeNumber >>> 32));
        result = 31 * result + (oPhoneType != null ? oPhoneType.hashCode() : 0);
        result = 31 * result + (oPhoneExt != null ? oPhoneExt.hashCode() : 0);
        result = 31 * result + oPhoneAreaCode;
        result = 31 * result + (int) (oPhoneNumber ^ (oPhoneNumber >>> 32));
        result = 31 * result + (rEmailType != null ? rEmailType.hashCode() : 0);
        result = 31 * result + (rEmailAddress != null ? rEmailAddress.hashCode() : 0);
        result = 31 * result + (oEmailType != null ? oEmailType.hashCode() : 0);
        result = 31 * result + (oEmailAddress != null ? oEmailAddress.hashCode() : 0);
        result = 31 * result + (employConstitution != null ? employConstitution.hashCode() : 0);
        result = 31 * result + (employerName != null ? employerName.hashCode() : 0);
        result = 31 * result + (kyc1Pan != null ? kyc1Pan.hashCode() : 0);
        result = 31 * result + (kyc2VoterId != null ? kyc2VoterId.hashCode() : 0);
        result = 31 * result + (kyc3Passport != null ? kyc3Passport.hashCode() : 0);
        result = 31 * result + (kyc4DrivingLicense != null ? kyc4DrivingLicense.hashCode() : 0);
        result = 31 * result + (int) (kyc5AadhaarNumber ^ (kyc5AadhaarNumber >>> 32));
        result = 31 * result + (kyc6Other != null ? kyc6Other.hashCode() : 0);
        result = 31 * result + (surrogate != null ? surrogate.hashCode() : 0);
        result = 31 * result + netTakeHomeSalary;
        result = 31 * result + (carModelName != null ? carModelName.hashCode() : 0);
        result = 31 * result + (carCategory != null ? carCategory.hashCode() : 0);
        result = 31 * result + (carManufacturerYear != null ? carManufacturerYear.hashCode() : 0);
        result = 31 * result + (carRegistrationNumber != null ? carRegistrationNumber.hashCode() : 0);
        result = 31 * result + (ownHouseType != null ? ownHouseType.hashCode() : 0);
        result = 31 * result + (houseSurrogateDocumentType != null ? houseSurrogateDocumentType.hashCode() : 0);
        result = 31 * result + (int) traderYearInBusiness;
        result = 31 * result + (traderBusinessProof != null ? traderBusinessProof.hashCode() : 0);
        result = 31 * result + (int) (creditCardNumber ^ (creditCardNumber >>> 32));
        result = 31 * result + (creditCardsCategory != null ? creditCardsCategory.hashCode() : 0);
        result = 31 * result + (dedupeStatus != null ? dedupeStatus.hashCode() : 0);
        result = 31 * result + approvedAmountByCro;
        result = 31 * result + (approvedDate != null ? approvedDate.hashCode() : 0);
        result = 31 * result + (int) (eligibleAmount ^ (eligibleAmount >>> 32));
        result = 31 * result + (panName != null ? panName.hashCode() : 0);
        result = 31 * result + (panStatus != null ? panStatus.hashCode() : 0);
        result = 31 * result + (cibilScore != null ? cibilScore.hashCode() : 0);
        result = 31 * result + applicationScore;
        result = 31 * result + (negativePinFlag != null ? negativePinFlag.hashCode() : 0);
        result = 31 * result + appliedAmount;
        result = 31 * result + (primaryAssetCtg != null ? primaryAssetCtg.hashCode() : 0);
        result = 31 * result + (primaryAssetMake != null ? primaryAssetMake.hashCode() : 0);
        result = 31 * result + (primaryAssetModelNo != null ? primaryAssetModelNo.hashCode() : 0);
        return result;
    }
}
