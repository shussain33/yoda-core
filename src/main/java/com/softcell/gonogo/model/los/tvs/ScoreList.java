package com.softcell.gonogo.model.los.tvs;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScoreList {

    private String addtype;

    private String gngrefno;

    private String cibiladdsubdatetodatedays;

    private String score;

    private String add2;

    private String add1;
}
