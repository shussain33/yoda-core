package com.softcell.gonogo.model.los.tvs.LosDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RiskEvents
{
private String countInsufficientBal;

private String countChequeBounceOutward;

private String countECSBounce;

private String countChequeBounceInward;

}
