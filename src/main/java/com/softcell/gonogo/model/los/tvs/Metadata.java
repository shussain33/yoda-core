package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Metadata {

        private DataThickness dataThickness;

        private PermissionGiven permissionGiven;

        private String requestTime;

        private String sourceApp;

        private String installTime;

        private String lastPingTime;

        private String uniqueId;
}
