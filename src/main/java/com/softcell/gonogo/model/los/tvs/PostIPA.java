package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostIPA
{
    private int netdisbursalamount;

    private int revisedadvanceemi;

    private int totalreceivabledownpay;

    private int emi;

    private int totalreceivable;

    private int downpaymentreceiptno;

    private int processingfee;

    private int amountfinanced;

	private String ltvmax;

	private String scheme;

    private int revisedloanamount;

    private int tenor;

    private String dlrsubventionmasteramt;

    private int assetcost;

    private int amountA;

    private int amountB;

    private int downpaymentamount;

    private int amountC;

    private int advemiamount;

    private String emistartdate;

    private String dlrsubventiontype;

    private int revisedfinanceamount;

    private String disbursementuserid;

    private String schemecode;

    private int creditshieldupfront;

    private int mfrsubventioncalculatedamt;

    private int othercharges;

    private int umfc;

    private String mfrsubventiontype;

    private int creditshieldemi;

    private int marginmoney;

    private String modelnumber;

    private String mfrsubventionmasteramt;

    private int stockonhire;

    private String downpaymentdate;

    private int roi;

    private int advemitenor;

    private int initialhire;

    private int revisedemi;

    private int dlrsubventioncalculatedamt;

    private String emienddate;

}
