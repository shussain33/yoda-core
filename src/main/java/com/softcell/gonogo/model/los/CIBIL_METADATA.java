package com.softcell.gonogo.model.los;

import java.util.HashMap;
import java.util.Map;


public interface CIBIL_METADATA {

	
	Map<String, String> genderMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("1", "FEMALE");
			put("2", "MALE");
			put("3","TRANSGENDER");
		}
	};
	
	Map<String, String> subjectReturnCode = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L; {
			put("1", "FOUND");
			put("0", "NOT FOUND");
		}
	};
	
	
	Map<String, String> accountTypeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			put("01", "AUTO LOAN (PERSONAL)");
			put("02", "HOUSING LOAN");
			put("03", "PROPERTY LOAN");
			put("04", "LOAN AGAINST SHARES/SECURITIES");
			put("05", "PERSONAL LOAN");
			put("06", "CONSUMER LOAN");
			put("07", "GOLD LOAN");
			put("08", "EDUCATION LOAN");
			put("09", "LOAN TO PROFESSIONAL");
			put("10", "CREDIT CARD");
			put("11", "LEASING");
			put("12", "OVERDRAFT");
			put("13", "TWO-WHEELER LOAN");
			put("14", "NON-FUNDED CREDIT FACILITY");
			put("15", "LOAN AGAINST BANK DEPOSITS");
			put("16", "FLEET CARD");
			put("17", "COMMERCIAL VEHICLE LOAN");
			put("18", "TELCO - WIRELESS");
			put("19", "TELCO - BROADBAND");
			put("20", "TELCO - LANDLINE");
			
			put("31", "SECURED CREDIT CARD");
			put("32", "USED CAR LOAN");
			put("33", "CONSTRUCTION EQUIPMENT LOAN");
			put("34", "TRACTOR LOAN");
			put("35", "CORPORATE CREDIT CARD");
			put("36", "KISAN CREDIT CARD");
			put("37", "LOAN ON CREDIT CARD");
			
			put("40", "MICROFINANCE - BUSINESS LOAN");
			put("41", "MICROFINANCE - PERSONAL LOAN");
			put("42", "MICROFINANCE - HOUSING LOAN");
			put("43", "MICROFINANCE - OTHER");
			put("51", "BUSINESS LOAN - GENERAL");
			put("52", "BUSINESS LOAN - PRIORITY SECTOR - SMALL BUSINESS");
			put("53", "BUSINESS LOAN - PRIORITY SECTOR - AGRICULTURE");
			put("54", "BUSINESS LOAN - PRIORITY SECTOR - OTHERS");
			put("55", "BUSINESS NON-FUNDED CREDIT FACILITY - GENERAL");
			put("56", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - SMALL BUSINESS");
			put("57", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - AGRICULTURE");
			put("58", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR-OTHERS");
			put("59", "BUSINESS LOAN AGAINST BANK DEPOSITS");
			put("60", "BUSINESS LOAN - DIRECTOR SEARCH");
			//put("60", "BUSINESS LOAN � DIRECTOR SEARCH");
			put("61", "BUSINESS LOAN - UNSECURED");
			
			put("80", "MICROFINANCE DETAILED REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("81", "SUMMARY REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("88", "LOCATE PLUS FOR INSURANCE (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			
			put("89", "VB OLM RETRIEVAL SERVICE (APPLICABLE TO ENQUIRY PURPOSE FOR VB OLM REQUEST ONLY)");
			put("90", "ACCOUNT REVIEW (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("91", "RETRO ENQUIRY (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("92", "LOCATE PLUS (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("93", "FOR INDIVIDUAL (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("94", "INDICATIVE REPORT (APPLICABLE TO ENQUIRY PURPOSE FOR CRS REQUEST ONLY)");
			put("95", "CONSUMER DISCLOSURE REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("96", "BANK OLM RETRIEVAL SERVICE (APPLICABLE TO ENQUIRY PURPOSE FOR CRS REQUEST ONLY)");
			put("97", "ADVISER LIABILITY (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
			put("00", "OTHER");
			put("98", "SECURED (ACCOUNT GROUPFOR PORTFOLIO REVIEW RESPONSE)");
			put("99", "UNSECURED (ACCOUNT GROUP FOR PORTFOLIO REVIEW RESPONSE)");
		}      
	};
	
	  
	  Map<String, String> occupationMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("01", "SALARIED");
			  put("02", "SELF EMPLOYED PROFESSIONAL");
			  put("03", "SELF EMPLOYED");
			  put("04", "OTHERS");
		  }
	  };
	  
	  Map<String, String> grossIncomeMap  = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
		  put("G", "GROSS INCOME");
		  put("N", "NET INCOME");
		}
	 };
	 
	 Map<String, String> AnnualIncome = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			 put("M", "MONTHLY");
			 put("A", "ANNUAL");
		 }
	 };
	  
	  Map<String, String> remarkCodeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("000001", "ONE OR MORE MEMBERS HAVE NOT RESPONDED TO YOUR DISPUTE");
			  put("000002", "DISPUTE ACCEPTED - PENDING CORRECTION BY THE MEMBER");
		  }
	  };
	  
	  Map<String, String> stateMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("01", "JAMMU & KASHMIR");
			  put("02", "HIMACHAL PRADESH");
			  put("03", "PUNJAB");
			  put("04", "CHANDIGARH");
			  put("05", "UTTARANCHAL");
			  put("06", "HARYANA");
			  put("07", "DELHI");
			  put("08", "RAJASTHAN");
			  put("09", "UTTAR PRADESH");
			  put("10", "BIHAR");
			  put("11", "SIKKIM");
			  put("12", "ARUNACHAL PRADESH");
			  put("13", "NAGALAND");
			  put("14", "MANIPUR");
			  put("15", "MIZORAM");
			  put("16", "TRIPURA");
			  put("17", "MEGHALAYA");
			  put("18", "ASSAM");
			  put("19", "WEST BENGAL");
			  put("20", "JHARKHAND");
			  put("21", "ORISSA");
			  put("22", "CHHATTISGARH");
			  put("23", "MADHYA PRADESH");
			  put("24", "GUJARAT");
			  put("25", "DAMAN & DIU");
			  put("26", "DADRA & NAGAR HAVELI");
			  put("27", "MAHARASHTRA");
			  put("28", "ANDHRA PRADESH");
			  put("29", "KARNATAKA");
			  put("30", "GOA");
			  put("31", "LAKSHADWEEP");
			  put("32", "KERALA");
			  put("33", "TAMIL NADU");
			  put("34", "PONDICHERRY");
			  put("35", "ANDAMAN & NICOBAR ISLANDS");
			  put("36", "TELANGANA");
			  put("99", "APO ADDRESS");
		  }
	  };
	  
	  
	  
	  Map<String, String> addressCategoryMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			  put("01", "PERMANENT ADDRESS");
			  put("02", "RESIDENCE ADDRESS");
			  put("03", "OFFICE ADDRESS");
			  put("04", "NOT CATEGORIZED");
		  }  
	  };
	   
	   
	   Map<String, String> residenceCodeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			   put("01", "OWNED");
			   put("02", "RENTED");
		   }  
	   };
	   
	   Map<String, String> suitFiledMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			   put("00", "NO SUIT FILED");
			   put("01", "SUIT FILED");
			   put("02", "WILFUL DEFAULT");
			   put("03", "SUIT FILED (WILFUL DEFAULT)");
		   }
	   };
	   
		Map<String, String> collateralMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;

			{
				put("00", "NO COLLATERAL");
				put("01", "PROPERTY");
				put("02", "GOLD");
				put("03", "SHARES");
				put("04", "SAVING ACCOUNT AND FIXED DEPOSIT");
			}
		};
		
		Map<String, String> phoneMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;
			{
				
				put("00", "NOT CLASSIFIED");
				put("01", "MOBILE PHONE");
				put("02", "HOME PHONE");
				put("03", "OFFICE PHONE");
				
			}
		};
		
	   Map<String, String> ownershipMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			   put("1", "INDIVIDUAL");
			   put("2", "AUTHORISED USER (REFERS TO SUPPLEMENTARY CREDIT CARD HOLDER)");
			   put("3", "GUARANTOR");
			   put("4", "JOINT");
			   put("5", "DECEASED");
		   }
	   };
	   
	   Map<String, String>  idTypeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;

		{
			   put("01", "INCOME TAX ID NUMBER");
			   put("02", "PASSPORT NUMBER");
			   put("03", "VOTER ID NUMBER");
			   put("04", "DRIVING LICENSE NO");
			   put("05", "RATION CARD NUMBER");
			   put("06", "UNIVERSAL ID NUMBER");
			   put("07", "ADDITIONAL ID 1");
			   put("08", "ADDITIONAL ID 2");
		   }
	   };
	   
	   Map<String, String> scoreCardMap  = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;

			{
			  put("G", "GROSS INCOME");
			  put("N", "NET INCOME");
			}
		 };
		 
		 Map<String, String> enquiryTypeMap  = new HashMap<String, String>(){
				private static final long serialVersionUID = 1L;

				{
					put("01", "AUTO LOAN (PERSONAL)");
					put("02", "HOUSING LOAN");
					put("03", "PROPERTY LOAN");
					put("04", "LOAN AGAINST SHARES/SECURITIES");
					put("05", "PERSONAL LOAN");
					put("06", "CONSUMER LOAN");
					put("07", "GOLD LOAN");
					put("08", "EDUCATION LOAN");
					put("09", "LOAN TO PROFESSIONAL");
					put("10", "CREDIT CARD");
					put("11", "LEASING");
					put("12", "OVERDRAFT");
					put("13", "TWO-WHEELER LOAN");
					put("14", "NON-FUNDED CREDIT FACILITY");
					put("15", "LOAN AGAINST BANK DEPOSITS");
					put("16", "FLEET CARD");
					put("17", "COMMERCIAL VEHICLE LOAN");
					put("18", "TELCO - WIRELESS");
					put("19", "TELCO - BROADBAND");
					put("20", "TELCO - LANDLINE");
					
					put("31", "SECURED CREDIT CARD");
					put("32", "USED CAR LOAN");
					put("33", "CONSTRUCTION EQUIPMENT LOAN");
					put("34", "TRACTOR LOAN");
					
					put("40", "MICROFINANCE - BUSINESS LOAN");
					put("41", "MICROFINANCE - PERSONAL LOAN");
					put("42", "MICROFINANCE - HOUSING LOAN");
					put("43", "MICROFINANCE - OTHER");
					put("51", "BUSINESS LOAN - GENERAL");
					put("52", "BUSINESS LOAN - PRIORITY SECTOR - SMALL BUSINESS");
					put("53", "BUSINESS LOAN - PRIORITY SECTOR - AGRICULTURE");
					put("54", "BUSINESS LOAN - PRIORITY SECTOR - OTHERS");
					put("55", "BUSINESS NON-FUNDED CREDIT FACILITY - GENERAL");
					put("56", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - SMALL BUSINESS");
					put("57", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - AGRICULTURE");
					put("58", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR-OTHERS");
					put("59", "BUSINESS LOAN AGAINST BANK DEPOSITS");
					put("60", "BUSINESS LOAN - DIRECTOR SEARCH");
					
					put("80", "MICROFINANCE DETAILED REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("81", "SUMMARY REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("88", "LOCATE PLUS FOR INSURANCE (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					
					put("89", "VB OLM RETRIEVAL SERVICE (APPLICABLE TO ENQUIRY PURPOSE FOR VB OLM REQUEST ONLY)");
					put("90", "ACCOUNT REVIEW (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("91", "RETRO ENQUIRY (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("92", "LOCATE PLUS (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("93", "FOR INDIVIDUAL (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("94", "INDICATIVE REPORT (APPLICABLE TO ENQUIRY PURPOSE FOR CRS REQUEST ONLY)");
					put("95", "CONSUMER DISCLOSURE REPORT (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("96", "BANK OLM RETRIEVAL SERVICE (APPLICABLE TO ENQUIRY PURPOSE FOR CRS REQUEST ONLY)");
					put("97", "ADVISER LIABILITY (APPLICABLE TO ENQUIRY PURPOSE ONLY)");
					put("00", "OTHER");
					put("98", "SECURED (ACCOUNT GROUPFOR PORTFOLIO REVIEW RESPONSE)");
					put("99", "UNSECURED (ACCOUNT GROUP FOR PORTFOLIO REVIEW RESPONSE)");
				}
			 };
			
			 
			 Map<String, String> frequencyMap = new HashMap<String, String>(){
					private static final long serialVersionUID = 1L;

					{
						put("01", "WEEKLY");
						put("02", "FORTNIGHTLY");
						put("03", "MONTHLY");
						put("04", "QUARTERLY");
					}
				};
}
