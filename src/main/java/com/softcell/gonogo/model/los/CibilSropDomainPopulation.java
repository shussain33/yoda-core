package com.softcell.gonogo.model.los;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.softcell.gonogo.model.core.request.scoring.*;
import org.apache.commons.lang.StringUtils;

public class CibilSropDomainPopulation {
	
	private CibilUtils cibilUtils ;
	public static String SAAS_DATE_TIME_FORMAT = "ddMMyyyy HH:mm:ss";
	public CibilSropDomainPopulation() {
		this.cibilUtils = new CibilUtils();
	}
	
	public List<HibCIBILSropDomain> cibilSropDomainGenerator(CibilResponse cibilResponse,  String srNo,
			String soaSourceName, String memberReferenceNumber, Long institutionId){
		
		List<HibCIBILSropDomain> cibilSropDomainsList = new ArrayList<HibCIBILSropDomain>();
		
		Integer srNumber = Integer.parseInt(srNo);
		
		HibCIBILSropDomain cibilSropDomain = new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
		
		if(null != cibilResponse){
			CibilRespHeader header = cibilResponse.getHeader();
			
			if (null != header) {
				cibilSropDomain.setDateProcessed(CibilSropDomainPopulation.getDate(header.getDateProceed()));
				cibilSropDomain.setSubjectReturnCode(cibilUtils.getSubjectReturnCode(header.getSubjectReturnCode()));
				cibilSropDomain.setEnquiryControlNumber(header.getEnquiryControlNumber());
			}
			
			CibilRespName name = cibilResponse.getName();
			if(null != name){
					cibilSropDomain.setConsumerNameField1(name.getName1());
					cibilSropDomain.setConsumerNameField2(name.getName2());
					cibilSropDomain.setConsumerNameField3(name.getName3());
					cibilSropDomain.setConsumerNameField4(name.getName4());
					cibilSropDomain.setConsumerNameField5(name.getName5());
					cibilSropDomain.setDateOfBirth(CibilSropDomainPopulation.getDate(name.getDob()));
					cibilSropDomain.setGenderValue(CIBILUtils.getValue(name.getGender(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.GENDER));
					cibilSropDomain.setDateErrorCodePN(CibilSropDomainPopulation.getDate(name.getDateOfEntryForErrorCode()));
					cibilSropDomain.setErrorSegTagPN(name.getErrorSegmentTag());
					cibilSropDomain.setErrorCodePN(name.getErrorCode());
					cibilSropDomain.setErrorPN(cibilUtils.getRemarkCode(name.getErrorCode())); 
					cibilSropDomain.setDateEntryCIBILRemarkCodePN(CibilSropDomainPopulation.getDate(name.getDateOfEntryForCibilRemarksCode()));
					cibilSropDomain.setCibilRemarkCodePN(name.getCibilRemarkCode());
					cibilSropDomain.setDateEntryErrorDisputeRemarkCodePN(CibilSropDomainPopulation.getDate(name.getDateOfEntryForErrorOrDisputeRemarksCode()));
					cibilSropDomain.setErrorDisputeRemarkCode1PN(name.getErrorOrDisputeRemarksCode1());
					cibilSropDomain.setErrorDisputeRemarkCode2PN(name.getErrorOrDisputeRemarksCode2());
					cibilSropDomain.setOutputWriteFlag("0");
					cibilSropDomain.setOutputWriteTime(new SimpleDateFormat(SAAS_DATE_TIME_FORMAT).format(new Date()));
			}
			
			CibilRespDisputeRemarks disputeRemarks = cibilResponse.getDisputeRemarks();
			if(null != disputeRemarks){
				cibilSropDomain.setDateCIBILErrorCodeEM(CibilSropDomainPopulation.getDate(disputeRemarks.getDateOfEntry()));
				cibilSropDomain.setDisputeRemarkLine1(disputeRemarks.getDisputeRemarksLine1());
				cibilSropDomain.setDisputeRemarkLine2(disputeRemarks.getDisputeRemarksLine2());
				cibilSropDomain.setDisputeRemarkLine3(disputeRemarks.getDisputeRemarksLine3());
				cibilSropDomain.setDisputeRemarkLine4(disputeRemarks.getDisputeRemarksLine4());
				cibilSropDomain.setDisputeRemarkLine5(disputeRemarks.getDisputeRemarksLine5());
				cibilSropDomain.setDisputeRemarkLine6(disputeRemarks.getDisputeRemarksLine6());
				
			}
			
			cibilSropDomainsList.add(cibilSropDomain);
			
			Integer counter=0;
			
			List<CibilRespID> idList = cibilResponse.getIdList();
			if(null != idList && idList.size() > 0){
				for (Integer i = 0; i < idList.size(); i ++) {
					
					if (i > counter) {
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					
//					String idType = cibilUtils.getIdType(idList.get(i).getIdType());
					String idType = CIBILUtils.getValue(idList.get(i).getIdType(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.ID_TYPE);
					
					if(StringUtils.isNotBlank(idType) && StringUtils.equalsIgnoreCase(idType, "EMAIL")){
						cibilSropDomainsList.get(i).setEmailId(cibilResponse.getIdList().get(i).getIdValue());
						
					}else{
						cibilSropDomainsList.get(i).setIdType(idType); 
						cibilSropDomainsList.get(i).setIdNumber(idList.get(i).getIdValue());
						cibilSropDomainsList.get(i).setIssueDate(CibilSropDomainPopulation.getDate(idList.get(i).getIssueDate()));
						cibilSropDomainsList.get(i).setExpirationDate(CibilSropDomainPopulation.getDate(idList.get(i).getExpiryDate()));
						cibilSropDomainsList.get(i).setEnrichedThroughEnquiryId(idList.get(i).getEnrichedThroughtEnquiry());
					}
					
				}
			}
			
			List<CibilRespPhone> phoneList = cibilResponse.getPhoneList();
			if(null != phoneList && phoneList.size() > 0){
				for(Integer i = 0; i < phoneList.size(); i ++){
					if(i > counter){
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					cibilSropDomainsList.get(i).setTelephoneTypeValue(CIBILUtils.getValue(phoneList.get(i).getTelephoneType(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.PHONE_TYPE));
					cibilSropDomainsList.get(i).setTelephoneNumber(phoneList.get(i).getTelephoneNumber());
					cibilSropDomainsList.get(i).setTelephoneExtension(phoneList.get(i).getTelephoneExtention());
					cibilSropDomainsList.get(i).setEnrichedThroughEnquiryPT(phoneList.get(i).getEnrichEnquiryForPhone());
					
				}
			}
			
			List<CibilRespEmployment> employmentList = cibilResponse.getEmploymentList();
			if(null != employmentList && employmentList.size() > 0){
				for(Integer i = 0; i < employmentList.size(); i ++){
					if(i > counter){
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					
					cibilSropDomainsList.get(i).setAccountTypeValue(CIBILUtils.getValue(employmentList.get(i).getAccountType(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.ACCOUNT_TYPE));
					cibilSropDomainsList.get(i).setDateReportedAndCertifiedEM(CibilSropDomainPopulation.getDate(employmentList.get(i).getDateReported()));
					cibilSropDomainsList.get(i).setOccupationValueEM(CIBILUtils.getValue(employmentList.get(i).getOccupationCode(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.OCCUPATION_CODE));
				    cibilSropDomainsList.get(i).setIncomeValueEM(employmentList.get(i).getIncome());
					cibilSropDomainsList.get(i).setNetGrossIndicatorValueEM(cibilUtils.getGrossIncome(employmentList.get(i).getNetGrossIndicator()));
				    cibilSropDomainsList.get(i).setMonthlyAnnualIndicatorValueEM(CIBILUtils.getValue(employmentList.get(i).getMonthlyAnnuallyIndicator(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.MONTHLY_ANNUALLY_INDICATOR));
					cibilSropDomainsList.get(i).setDateEntryErrorCodeEM(CibilSropDomainPopulation.getDate(employmentList.get(i).getDateOfEntryForErrorCode()));
					cibilSropDomainsList.get(i).setErrorCodeEM(employmentList.get(i).getErrorCode());
				    cibilSropDomainsList.get(i).setDateCIBILErrorCodeEM(CibilSropDomainPopulation.getDate(employmentList.get(i).getDateOfEntryForErrorCode())); 
					cibilSropDomainsList.get(i).setCibilRemarkCodeEM(employmentList.get(i).getCibilRemarkCode());
					cibilSropDomainsList.get(i).setDateDisputeRemarkCodeEM(CibilSropDomainPopulation.getDate(employmentList.get(i).getDateOfEntryForErrorOrDisputeRemarksCode()));
					cibilSropDomainsList.get(i).setErrorDisputeRemarkCode1EM(employmentList.get(i).getErrorOrDisputeRemarksCode1());
					cibilSropDomainsList.get(i).setErrorDisputeRemarkCode2EM(employmentList.get(i).getErrorOrDisputeRemarksCode2());
					
				}
			}
			
			List<CibilRespEnqActNo> enqActNoList = cibilResponse.getEnqActNoList();
			if(null != enqActNoList && enqActNoList.size() > 0){
				for(Integer i = 0 ;i< enqActNoList.size();i++){
					if(i > counter){
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					cibilSropDomainsList.get(i).setAccountNumberPI(enqActNoList.get(i).getAccountNumber());
				}
				
			}
			
			List<CibilRespScore> scoreList = cibilResponse.getScoreList();
			if(null != scoreList && scoreList.size() > 0){
				for(Integer i=0 ;i < scoreList.size();i++){
					if(i > counter){
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					cibilSropDomainsList.get(i).setScoreName(scoreList.get(i).getScoreName());
					cibilSropDomainsList.get(i).setScoreCardName(CIBILUtils.getValue(scoreList.get(i).getScoreCardName(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.SCORE_CARD_NAME));
					cibilSropDomainsList.get(i).setScoreCardVersion(scoreList.get(i).getScoreCardVersion());
					cibilSropDomainsList.get(i).setScoreDate(CibilSropDomainPopulation.getDate(scoreList.get(i).getScoreDate()));
					
					cibilSropDomainsList.get(i).setScore(getFormattedScore(scoreList.get(i).getScore()));
					
					String exclusionCode1 = scoreList.get(i).getExclusionCode1();
					String exclusionCode2 =scoreList.get(i).getExclusionCode2();
					String exclusionCode3 =scoreList.get(i).getExclusionCode3();
					String exclusionCode4 = scoreList.get(i).getExclusionCode4();
					String exclusionCode5 = scoreList.get(i).getExclusionCode5();
					String exclusionCodes1to5 = formattedExclusionCodes(exclusionCode1, exclusionCode2, exclusionCode3, exclusionCode4, exclusionCode5);
					cibilSropDomainsList.get(i).setExclusionCodes1_to_5(exclusionCodes1to5);
					
					
					String exclusionCode6 = scoreList.get(i).getExclusionCode6();
					String exclusionCode7 = scoreList.get(i).getExclusionCode7();
					String exclusionCode8 = scoreList.get(i).getExclusionCode8();
					String exclusionCode9 = scoreList.get(i).getExclusionCode9();
					String exclusionCode10 = scoreList.get(i).getExclusionCode10();
					String exclusionCode6to10 = formattedExclusionCodes(exclusionCode6, exclusionCode7, exclusionCode8, exclusionCode9, exclusionCode10);
					cibilSropDomainsList.get(i).setExclusionCodes6_TO_10(exclusionCode6to10);
					
					String reasonCode1 = scoreList.get(i).getReasonCode1();
					String reasonCode2 = scoreList.get(i).getReasonCode2();
					String reasonCode3 = scoreList.get(i).getReasonCode3();
					String reasonCode4 = scoreList.get(i).getReasonCode4();
					String reasonCode5 = scoreList.get(i).getReasonCode5();
					String reasonCode1to5 = formattedReasonCodes(reasonCode1, reasonCode2, reasonCode3, reasonCode4, reasonCode5);
					cibilSropDomainsList.get(i).setReasonCodes1_TO_5(reasonCode1to5);
					
					/*String demonCode1 = ((CibilRespScore)scoreList.get(i.intValue())).getCreditVisionDemonetisationAlgorithm1();
			          String demonCode2 = ((CibilRespScore)scoreList.get(i.intValue())).getCreditVisionDemonetisationAlgorithm2();
			          String demonCode3 = ((CibilRespScore)scoreList.get(i.intValue())).getCreditVisionDemonetisationAlgorithm3();
			          String demonCode4 = ((CibilRespScore)scoreList.get(i.intValue())).getCreditVisionDemonetisationAlgorithm4();
			          String demonCode2to4 = formattedDemonCodes2to4(demonCode2, demonCode3, demonCode4);
			          ((HibCIBILSropDomain)cibilSropDomainsList.get(i.intValue())).setReasonCodes26_TO_30(formattedDemonCodes(demonCode1));
			          ((HibCIBILSropDomain)cibilSropDomainsList.get(i.intValue())).setReasonCodes31_TO_35(demonCode2to4);*/
					
					cibilSropDomainsList.get(i).setErrorCodeSC(scoreList.get(i).getScoreErrorCode());
				}
			}
		
			List<CibilRespAddress> addressList = cibilResponse.getAddressList();
			if(null != addressList && addressList.size() > 0){
				for(Integer i=0; i< addressList.size(); i++){
					if(i > counter){
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					cibilSropDomainsList.get(i).setAddressLine1(addressList.get(i).getAddressLine1());
					cibilSropDomainsList.get(i).setAddressLine2(addressList.get(i).getAddressLine2());
					cibilSropDomainsList.get(i).setAddressLine3(addressList.get(i).getAddressLine3());
					cibilSropDomainsList.get(i).setAddressLine4(addressList.get(i).getAddressLine4());
					cibilSropDomainsList.get(i).setAddressLine5(addressList.get(i).getAddressLine5());
					if(StringUtils.isNotBlank(addressList.get(i).getStateCode()) && StringUtils.isNumeric(addressList.get(i).getStateCode())){
						cibilSropDomainsList.get(i).setStateCode(Integer.parseInt(addressList.get(i).getStateCode()));
					}
					cibilSropDomainsList.get(i).setState(CIBILUtils.getValue(addressList.get(i).getStateCode(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.ADDRESS_STATE)); 
					cibilSropDomainsList.get(i).setPincode(addressList.get(i).getPinCode());
					cibilSropDomainsList.get(i).setAddressCatergoryValue(cibilUtils.getAddressCategory(addressList.get(i).getAddressCategory()));
					cibilSropDomainsList.get(i).setResidenceValue(CIBILUtils.getValue(addressList.get(i).getResidenceCode(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.ADDRESS_RESIDENCE_CODE));
					cibilSropDomainsList.get(i).setDateReportedPA(addressList.get(i).getDateReported());
					cibilSropDomainsList.get(i).setEnrichedThroughEnquiryPA(addressList.get(i).getEnrichedThroughtEnquiry());
				}
			}
			
			List<CibilRespAccount> accountList = cibilResponse.getAccountList();
			if(null != accountList && accountList.size() > 0){
				for(Integer i=0 ; i < accountList.size(); i++){
					if(i > counter){
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					cibilSropDomainsList.get(i).setReportMemberShortNameTL(accountList.get(i).getReportingMemberShortName());
					cibilSropDomainsList.get(i).setAccountTypeValueTL(CIBILUtils.getValue(accountList.get(i).getAccountType(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.ACCOUNT_TYPE));
							//cibilUtils.getAccountType(accountList.get(i).getAccountType()));
					cibilSropDomainsList.get(i).setAccountNumberTL(accountList.get(i).getAccountNumber());
					cibilSropDomainsList.get(i).setOwnershipIndicatorValueTL(CIBILUtils.getValue(accountList.get(i).getOwnershipIndicator(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.OWNERSHIP_INDICATOR));
					cibilSropDomainsList.get(i).setDateOpenedDisbursedTL(CibilSropDomainPopulation.getDate(accountList.get(i).getDateOpenedOrDisbursed()));
					cibilSropDomainsList.get(i).setDateOfLastPaymentTL(CibilSropDomainPopulation.getDate(accountList.get(i).getDateOfLastPayment()));
					cibilSropDomainsList.get(i).setDateClosedTL(CibilSropDomainPopulation.getDate(accountList.get(i).getDateClosed()));
					cibilSropDomainsList.get(i).setDateReportedTL(CibilSropDomainPopulation.getDate(accountList.get(i).getDateReportedAndCertified()));
					cibilSropDomainsList.get(i).setHighCreditSanctionedAmount(accountList.get(i).getHighCreditOrSanctionedAmount());
					cibilSropDomainsList.get(i).setCurrentBalanceTL(accountList.get(i).getCurrentBalance());
					cibilSropDomainsList.get(i).setAmountOverdueTL(accountList.get(i).getOverdueAmount());
					cibilSropDomainsList.get(i).setPaymentHistory1(accountList.get(i).getPaymentHistory1());
					cibilSropDomainsList.get(i).setPaymentHistory2(accountList.get(i).getPaymentHistory2());
					cibilSropDomainsList.get(i).setPaymentHistoryStartDate(CibilSropDomainPopulation.getDate(accountList.get(i).getPaymentHistoryStartDate()));
					cibilSropDomainsList.get(i).setPaymentHistoryEndDate(CibilSropDomainPopulation.getDate(accountList.get(i).getPaymentHistoryEndDate()));
					cibilSropDomainsList.get(i).setSuitFiledStatusValueTL(CIBILUtils.getValue(accountList.get(i).getSuitFiledOrWilfulDefault(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.SUIT_FILED_STATUS));  
					cibilSropDomainsList.get(i).setWofSettledStatusTL(CIBILUtils.getValue(accountList.get(i).getWrittenOffAndSettledStatus(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.WRITTEN_OFF_AND_SETTLED_STATUS.toString()));
					cibilSropDomainsList.get(i).setWofTotalAmountTL(accountList.get(i).getWrittenOffAmountTotal());
					cibilSropDomainsList.get(i).setCollateralValueTL(accountList.get(i).getValueOfCollateral());
					cibilSropDomainsList.get(i).setTypeOfCollateralTL(CIBILUtils.getValue(accountList.get(i).getTypeOfCollateral(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.COLLATERAL_TYPE));
					cibilSropDomainsList.get(i).setCreditLimitTL(accountList.get(i).getCreditLimit());
					cibilSropDomainsList.get(i).setCashLimitTL(accountList.get(i).getCashLimit());
					cibilSropDomainsList.get(i).setRateOfIntrestTL(accountList.get(i).getRateOfInterest());
					cibilSropDomainsList.get(i).setRepayTenure(accountList.get(i).getRepaymentTenure());
					cibilSropDomainsList.get(i).setEmiAmountTL(accountList.get(i).getEmiAmount());
					cibilSropDomainsList.get(i).setWofPrincipalTL(accountList.get(i).getWrittenOffAmountPrincipal());
					cibilSropDomainsList.get(i).setSettlementAmountTL(accountList.get(i).getSettlementAmount());
					cibilSropDomainsList.get(i).setPaymentFrequencyTL(CIBILUtils.getValue(accountList.get(i).getPaymentFrequence(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.FREQUENCY_OF_PAYMENT));
					cibilSropDomainsList.get(i).setActualPaymentAmountTL(accountList.get(i).getActualPaymentAmount());
					cibilSropDomainsList.get(i).setDateEntErrorCodeTL(CibilSropDomainPopulation.getDate(accountList.get(i).getDateOfEntryForErrorCode()));
					cibilSropDomainsList.get(i).setErrorCodeTL(accountList.get(i).getErrorCode());
					cibilSropDomainsList.get(i).setDateEntCIBILRemarkCodeTL(CibilSropDomainPopulation.getDate(accountList.get(i).getDateOfEntryForCibilRemarksCode()));
					cibilSropDomainsList.get(i).setCibilRemarksCodeTL(accountList.get(i).getCibilRemarkCode());
					cibilSropDomainsList.get(i).setDateDisputeCodeTL(CibilSropDomainPopulation.getDate(accountList.get(i).getDateOfEntryForErrorOrDisputeRemarksCode()));
					cibilSropDomainsList.get(i).setErrorDisputeRemarkCode1TL(accountList.get(i).getErrorOrDisputeRemarksCode1());
					cibilSropDomainsList.get(i).setErrorDisputeRemarkCode2TL(accountList.get(i).getErrorOrDisputeRemarksCode2());
					
				}
			}
			
			List<CibilRespEnquiry> enquiryList = cibilResponse.getEnquiryList();
			if(null != enquiryList && enquiryList.size() > 0){
				for(Integer i=0; i < enquiryList.size(); i++){
					if(i > counter){
						HibCIBILSropDomain sropDomain= new HibCIBILSropDomain(srNumber, soaSourceName, memberReferenceNumber);
						cibilSropDomainsList.add(sropDomain);
						counter ++;
					}
					cibilSropDomainsList.get(i).setDateOfEnquiryIQ(CibilSropDomainPopulation.getDate(enquiryList.get(i).getDateReported()));
					cibilSropDomainsList.get(i).setEnquiryMemberShortNameIQ(enquiryList.get(i).getReportingMemberShortName());
					cibilSropDomainsList.get(i).setEnquiryPurposeIQ(CIBILUtils.getValue(enquiryList.get(i).getEnquiryPurpose(), soaSourceName, institutionId, ReverseConversionConstant.ConversionConstant.ENQUIRY_PURPOSE));
					cibilSropDomainsList.get(i).setEnqiuryAmountIQ(enquiryList.get(i).getEnquiryAmount());
				}
			}
		}
		return cibilSropDomainsList;
	}
	
	private String formattedDemonCodes(String str1) {
		String finalString = "";
    try
    {
      if ((StringUtils.isNotBlank(str1)) && 
        (StringUtils.isBlank(finalString))) {
        finalString = "CV01=" + str1;
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return finalString;}

	private String formattedDemonCodes2to4(String str2, String str3,
			String str4) {  String finalString = "";
		    try
		    {
		      if (StringUtils.isNotBlank(str2)) {
		        finalString = "CV02=" + str2;
		      }
		      
		      if (StringUtils.isNotBlank(str3)) {
		        if (StringUtils.isBlank(finalString)) {
		          finalString = finalString + "CV03=" + str3;
		        } else {
		          finalString = finalString + ",CV03=" + str3;
		        }
		      }
		      
		      if (StringUtils.isNotBlank(str4)) {
		        if (StringUtils.isBlank(finalString)) {
		          finalString = finalString + "CV04=" + str4;
		        } else {
		          finalString = finalString + ",CV04=" + str4;
		        }
		      }
		    }
		    catch (Exception e) {
		      e.printStackTrace();
		    }
		    return finalString;
		  }

	private String formattedExclusionCodes(String str1  , String str2, String str3, String str4, String str5){
		String str = "" ;
		
		if(StringUtils.isNotBlank(str1)){
			str += StringUtils.trim(str1);
		}else {
			str += "XX";
		}
		
		if(StringUtils.isNotBlank(str2)){
			str += StringUtils.trim(str2);
		}else {
			str += "XX";
		}
		
		if(StringUtils.isNotBlank(str3)){
			str += StringUtils.trim(str3);
		}else {
			str += "XX";
		}
		
		if(StringUtils.isNotBlank(str4)){
			str += StringUtils.trim(str4);
		}else {
			str += "XX";
		}
		
		if(StringUtils.isNotBlank(str5)){
			str += StringUtils.trim(str5);
		}else {
			str += "XX";
		}
		return str;
	}
	
	private String formattedReasonCodes(String str1  , String str2, String str3, String str4, String str5){
		String finalString = "";
		try{
			
			if(StringUtils.isNotBlank(str1)){
				if(StringUtils.isBlank(finalString)){
					finalString = "RC01="+str1;
				}
			}
			
			if(StringUtils.isNotBlank(str2)){
				if(StringUtils.isBlank(finalString)){
					finalString += "RC02="+str2;
				}else{
					finalString += ","+"RC02="+str2;
				}
			}
			
			if(StringUtils.isNotBlank(str3)){
				if(StringUtils.isBlank(finalString)){
					finalString += "RC03="+str3;
				}else{
					finalString += ","+"RC03="+str3;
				}
			}
			
			if(StringUtils.isNotBlank(str4)){
				if(StringUtils.isBlank(finalString)){
					finalString += "RC04="+str4;
				}else{
					finalString += ","+"RC04="+str4;
				}
			}
			
			if(StringUtils.isNotBlank(str5)){
				if(StringUtils.isBlank(finalString)){
					finalString += "RC05="+str5;
				}else{
					finalString += ","+"RC05="+str5;
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return finalString;
	}
	
	public static Date getDate(String date){
		Date dateObject = null; 
		try{
			
			if(StringUtils.isNotBlank(date)){
				SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
				dateObject = dateFormat.parse(date);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return dateObject;
	}
	
	private  String getFormattedScore(String score){
		try {
			if(null != score  && score.length() == 5){
				if(score.charAt(3) == '-'){
					score = "-1";
				}else if(Character.getNumericValue(score.charAt(2))  > 0){
					score =  score.substring(2, score.length());
				}else{
					if(StringUtils.equalsIgnoreCase(score, "00000")){
						score =  "0";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return score;
	}


/*public static void main(String[] args) {
	
	try {
		String response ="TUEF12488775                     0000BP03081030                    100100048286121062017102118PN03N010122RAJNEESH KUMAR GARG SO0213INDERJIT GARG07082405196108012ID03I010102010210ACMPG5578P9001YID03I020102020208H9566568ID03I030102030210VA232782729001YID03I040102060212868716460115PT03T0101129193222510070302039001YPT03T02011091402234030302039001YPT03T03011094553178030302019001YPT03T04011094553178030302039001YEC03C010122MD@GARGELECTRONICS.COMEC03C020120NOORANGARG@YAHOO.COMEC03C030113MD@ICL.ORG.INEC03C040116PCB@GLIDE.NET.INEM03E01010207020831032015030204SC10CIBILTUSCR0102010202100308210620170405000-1300103102-23202-23302-2PA03A010139NO 85  NAYANDAHALLI BANGALORE BANGALORE06022907065600390802010902011008100120179001YPA03A020135GARG ELECTRONICS PLOT NO 24 BHATOLI0240KALAN INDUSTRIAL AREA BADDI DISTT SO SOL0333SOLAN GARG ELECTRONICS PLOT NO 240435BHATOLI KALAN INDUSTRIAL AREA BADDI0538DISTT SO SOL    SOLAN GARG ELECTRONICS06022707064000120802021008291220169001YPA03A030135GARG ELECTRONICS PLOT NO 24 BHATOLI0240KALAN INDUSTRIAL AREA BADDI DISTT SO SOL0311SOLAN SOLAN06020207061732120802031008310320159001YPA03A040137PLOT NO 196, INDUSTRIAL AREA PHASE II0221PANCHKULA   PANCHKULA0602060706134109080203100815012015TL04T0010213NOT DISCLOSED04020205014080803062004090830042008100830042008110831052009120710000001301028540300000000000000000000000000000000000000000000000000002954000000000000000000000000000000000000000000000000000000300801042008310801052005TL04T0020213NOT DISCLOSED0402030501108083103201511083103201512082500000013082527528214062752822803001300801032015310801032015TL04T0030209HDFC BANK030832234898040207050110808300320151108310320151207106800013071068000280300030080103201531080103201544020345049853TL04T0040213NOT DISCLOSED040210050110808110320151108310320151301028030003008010320153108010320153606200000440203TL04T0050213NOT DISCLOSED040203050110808230220150908100320151108310320151208275000001308272359542803000300801032015310801032015TL04T0060213NOT DISCLOSED04021205011080821112014090827032015110831032015120670000013066975942815STDSTDSTDSTDSTD300801032015310801112014TL04T0070209HDFC BANK03190004854980600379525040210050110808051120140908030320151108310320151205493871304-5002815000000000000000300801032015310801112014TL04T0080213NOT DISCLOSED0402120501108080805201409082703201511083103201512076072000130755493302833STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTD300801032015310801052014380511.25TL04T0090209HDFC BANK030827740207040207050110808280320140908300320151008300320151108310320151207106800013010283900000000000000000000000000000000000000030080103201531080104201244020345049790TL04T0100213NOT DISCLOSED040205050130808080320140908080320151108310320151205400001305216232839STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTD300801032015310801032014380514.2039022440041924TL04T0110209HDFC BANK0319000485498060005740204021005011080812122013110831032015130102848000000000000000000000000000000000000000000000000300801032015310801122013TL04T0120209HDFC BANK0316303LN371307000110402070501108080311201310083105201411083105201412071390000130102821000STDSTDSTDSTDSTDSTD300801052014310801112013TL04T0130213NOT DISCLOSED0402020501408082009201309081802201410082802201411083103201412073000000130102803000300801022014310801022014TL04T0140213NOT DISCLOSED0402030501408082009201309082503201511083103201512073970000130733122172854000000000000000000XXX00000000000000000000000000000000029030003008010320153108010920133903180400549970440203TL04T0150213NOT DISCLOSED0402030501408082009201309082503201511083103201512072030000130714385422854000000000000000000XXX00000000000000000000000000000000029030003008010320153108010920133903180400525219440203TL04T0160213NOT DISCLOSED0402050501308080407201309080403201511083103201512061500001305985522854STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTD2909STDSTDSTD300801032015310801072013380514.2039024840044114TL04T0170209HDFC BANK0316303LN371308100020402070501108082203201310082803201411083103201412071102000130102836000STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTD300801032014310801042013TL04T0180213NOT DISCLOSED0402040501108080510201209082005201410082005201411083105201412075422000130102854STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDXXXSTDSTDSTDSTD2906STDSTD300801052014310801062011380510.9539025540078061979TL04T0190209HDFC BANK0316303LN371226400010402070501108082009201210082203201311083103201312071102000130102818000STDSTDSTDSTDSTD300801032013310801102012TL04T0200209HDFC BANK0316303LN371225400970402070501108081009201210081103201311083103201312071390000130102818000STDSTDSTDSTDSTD300801032013310801102012TL04T0210209HDFC BANK0316303LN371208300120402070501108082303201210082009201211083009201212071102000130102818000STDSTDSTDSTDSTD300801092012310801042012TL04T0220209HDFC BANK0316303LN371207400440402070501108081403201210081009201211083009201212071390000130102818000STDSTDSTDSTDSTD300801092012310801042012TL04T0230213NOT DISCLOSED040212050110808301220110908181120141008181120141108301120141206390000130102854STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTD2954STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTD300801112014310801122011TL04T0240209HDFC BANK0316303LN371126900070402070501108082609201110082303201211083103201212071102000130102815000STD000000000300801032012310801112011TL04T0250209HDFC BANK0316303LN371125800340402070501108081509201110081403201211083103201212071390000130102815000STD000000000300801032012310801112011TL04T0260213NOT DISCLOSED040200050110808260720111008250220121108290220121206610000130102815STDSTDXXXSTDSTD300801022012310801102011380511.25TL04T0270213NOT DISCLOSED040210050110808091120100908260720131108230420151205658051305-1275285400000000000000000000000000000000000000000000000000000029540000000000000000000000000000000000000000000000000000003008010420153108010520123606600000380530.00440203450510254TL04T0280213NOT DISCLOSED040203050110808300720100908250320151108310320151207100000013067581872854000000000000000000XXX00000000000000000000000000000000029540000000000000000000000000000000000000000000000000000003008010320153108010420123903120400513354440203TL04T0290213NOT DISCLOSED040203050110808270220100908260220151008260220151108280220151208321980001301028540000000000000000000000000000000000000000000000000000002954000000000000000000000000000000000000000000000000000000300801022015310801032012TL04T0300213NOT DISCLOSED0402100501108081712200810082904201011083112201312041243130102803000300801042010310801042010360512000440203TL04T0310213NOT DISCLOSED040210050110808201120080908251120141108280220151205888681305-12742854000XXX000000015000000000000000000000000000000000000000295400000000000000000000000000000000000000000000000000000030080102201531080103201236051000037041000TL04T0320213NOT DISCLOSED040212050110808140220080908301220111008301220111108311220111206292000130102854STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTDXXXSTDSTDXXXXXXXXXSTD2954STDXXXSTDSTDSTDSTDSTDSTDSTDSTDSTDXXXSTDSTDSTDSTDXXXSTD300801122011310801012009TL04T0330213NOT DISCLOSED04020205014080831122006090826022010100828022010110831032010120779500001301028540000000000000000000000000000000000000000000000000000002954000000000000000000000030000000000000000000030030000000300801022010310801032007TL04T0340213NOT DISCLOSED04020205014080831072006090826022010100828022010110831032010120799500001301028540000000000000000000000000000000000000000000000000000002954000000000000000000000030000000000000000000030030000000300801022010310801032007TL04T0350213NOT DISCLOSED04021005011080824042006100824042006110830092006130102803000300801042006310801042006TL04T0360213NOT DISCLOSED04021005012080814012006100827122006110831052007130102836000000000000XXXXXXXXX000000000000000300801122006310801012006TL04T0370213NOT DISCLOSED04021005011080811082005090826102006100813102010110831032015120524908130102854000000000000000000000000000000000000000000000000000XXX2954XXX00000000000000000000000000000000000000000000000000030080110201031080111200736045000440203TL04T0380209HDFC BANK031900040502820005121320402100501108082306200509083012201110082907201311083107201312061550561301028540000000000000000000000000000000000000000000000000000002954000000000000000000000000000000000000000000000000000000300801072013310801082010TL04T0390213NOT DISCLOSED0402010501108080110200409081409200910082005201011083101201512071310000130102854000000000000000000000000000000XXX000000XXX000000XXX0292912028029000000300801052010310801082008TL04T0400213NOT DISCLOSED0402020501408080609200409080605200910083004200911083103201012069000001301028540000000000000000000000000000000000000300000000000000002954000000000000000000000000000000000000000000000000000000300801042009310801052006TL04T0410213NOT DISCLOSED0402020501408080306200409083105200710083105200711083105200912063000001301028540000000000000000000000000000000000000000000000000000002954000000000000XXX000000000000000000000000000000000000000300801052007310801062004TL04T0420213NOT DISCLOSED040202050140808211120020908310320041008310320041108310820121206305000130102806000000300801032004310801022004TL04T0430213NOT DISCLOSED04020205014080821112002090831122007100831122007110831052009120711350001301028540000000000000000000000000000000000000000000000000000002954000000024000000000000000000000000000000000000000000000300801122007310801012005TL04T0440213NOT DISCLOSED04021005011080818012001090817072008100830122008110831122013120515032130102803000300801122008310801122008360540000440203TL04T0450213NOT DISCLOSED040212050110808180420040908060220081008060220081108110120101206441000130102854XXXXXXSTDXXXXXXXXXXXXSTDXXXSTDXXXSTDXXXSTDSTDSTDSTDSTD2933STDSTDSTDSTDSTDSTDSTDSTDSTDSTDSTD300801022008310801102005TL04T0460213NOT DISCLOSED040202050140808211120020908310720031008310720031108310820121206320000130102803XXX300801072003310801072003IQ04I0010108200620170409HDFC BANK0502000606250000IQ04I0020108200620170409HDFC BANK0502000606250000IQ04I0030108010620170413NOT DISCLOSED0502000606100000IQ04I0040108180120170413NOT DISCLOSED05025106071150000IQ04I0050108100120170413NOT DISCLOSED05025106071150000IQ04I0060108291220160413NOT DISCLOSED0502050606250000IQ04I0070108071020160413NOT DISCLOSED050213060550000IQ04I0080108071020160413NOT DISCLOSED050213060550000IQ04I0090108180120160413NOT DISCLOSED0502000606100084IQ04I0100108021120150413NOT DISCLOSED0502000606100084IQ04I0110108080920150413NOT DISCLOSED0502000606100084IQ04I0120108240820150413NOT DISCLOSED0502000606100084IQ04I0130108110820150413NOT DISCLOSED0502000606100084IQ04I0140108270320150409HDFC BANK05020706071068000IQ04I0150108080320150413NOT DISCLOSED050210060550000IQ04I0160108150120150413NOT DISCLOSED050202060812500000IQ04I0170108150120150413NOT DISCLOSED050202060852500000IQ04I0180108060120150413NOT DISCLOSED050251060816100000IQ04I0190108081220140413NOT DISCLOSED050251060810000000IQ04I0200108281120140411CANARA BANK05025106071500000IQ04I0210108181020140409HDFC BANK050210060210IQ04I0220108210820140411CANARA BANK0502540609100000000IQ04I0230108050520140413NOT DISCLOSED05025106077500000IQ04I0240108270320140409HDFC BANK05020706071068000IQ04I0250108200220140413NOT DISCLOSED050251060810000000IQ04I0260108140220140413NOT DISCLOSED05020306072030000IQ04I0270108130220140411CANARA BANK0502000609115000000IQ04I0280108101220130409HDFC BANK05021006041000IQ04I0290108141020130411CANARA BANK0502100606100000IQ04I0300108070920130413NOT DISCLOSED05020306072030000IQ04I0310108050920130413NOT DISCLOSED05020906073970000IQ04I0320108160820130413NOT DISCLOSED050203060850000000IQ04I0330108160820130413BAJAJ FIN LTD05020306011IQ04I0340108130820130413NOT DISCLOSED0502020606750000IQ04I0350108130820130413NOT DISCLOSED05020206071000000IQ04I0360108200520130411CANARA BANK0502000609115000000IQ04I0370108230420130413NOT DISCLOSED05025106011IQ04I0380108170120130411CANARA BANK05020006071000000IQ04I0390108271220120413NOT DISCLOSED05025106011IQ04I0400108300820120412CENTRAL BANK0502510609170000000IQ04I0410108120720120413NOT DISCLOSED05025106011IQ04I0420108120720120413NOT DISCLOSED05025106011IQ04I0430108150520120413NOT DISCLOSED05025106011IQ04I0440108121220110413NOT DISCLOSED050251060820000000IQ04I0450108121220110413NOT DISCLOSED050251060820000000IQ04I0460108101220110413NOT DISCLOSED050251060820000000IQ04I0470108101220110413NOT DISCLOSED050251060820000000IQ04I0480108211020110411CANARA BANK05025106071000000IQ04I0490108120920110409HDFC BANK05020706076000000IQ04I0500108100820110413INDUSIND BANK050203060850000000IQ04I0510108260720110413NOT DISCLOSED050202060812500000IQ04I0520108160720110413NOT DISCLOSED050203060810000000IQ04I0530108060620110413NOT DISCLOSED05025106011IQ04I0540108060120110413NOT DISCLOSED050251060850000000IQ04I0550108201220100413NOT DISCLOSED050251060850000000IQ04I0560108061120100413NOT DISCLOSED0502100606100000IQ04I0570108041020100413NOT DISCLOSED0502020606750000IQ04I0580108041020100413NOT DISCLOSED0502020606750000IQ04I0590108270720100413NOT DISCLOSED05020206071000000IQ04I0600108240720100413NOT DISCLOSED050209060820000000IQ04I0610108070520100413NOT DISCLOSED050202060850000000IQ04I0620108080420100413NOT DISCLOSED050202060850000000IQ04I0630108050220100413NOT DISCLOSED050202060850000000IQ04I0640108020220100413NOT DISCLOSED050209060820000000IQ04I0650108010920090413NOT DISCLOSED05025106077000000IQ04I0660108151220060413NOT DISCLOSED05020306078000000IQ04I0670108240120060409HDFC BANK05020306071500000IQ04I0680108291220050413NOT DISCLOSED050209060810000000ES0700132550102**";

		CIBILGenericResponseParser cibilGenericResponseParser = new CIBILGenericResponseParser();
		CibilResponse cibilResponse = new CibilResponse();
			if(StringUtils.startsWithIgnoreCase(response, "TUEF") && StringUtils.endsWithIgnoreCase(response, "**") ){
				if(StringUtils.countMatches(response, "TUEF") == 1 && StringUtils.countMatches(response, "**TUEF") == 0){
					cibilResponse = cibilGenericResponseParser.parseCibilResponse(1L, 2L, response);
				} else {
					
					String[] mulitpleResponses = StringUtils.splitByWholeSeparatorPreserveAllTokens(response, "**TUEF");
					Boolean mainResponseFlag = true;
					List<SecondaryMatches> secondaryMatches = new ArrayList<SecondaryMatches>();
					for(String temp : mulitpleResponses){
						if(mainResponseFlag){
							cibilResponse = cibilGenericResponseParser.parseCibilResponse(1L, 2L, temp);
							System.out.println(temp);
							mainResponseFlag = false;
						} else {
							temp = "TUEF"+temp;
							System.out.println(temp);
							CibilResponse secondResponse = new CibilResponse();
							secondResponse = cibilGenericResponseParser.parseCibilResponse(1L, 2L, temp);
							if(secondResponse!=null){
								SecondaryMatches matches = new SecondaryMatches();
								matches.setSecName(secondResponse.getName());
								matches.setSecAddressList(secondResponse.getAddressList());
								matches.setSecPhoneList(secondResponse.getPhoneList());
								matches.setSecIdList(secondResponse.getIdList());
								secondaryMatches.add(matches);
								
							}
						}
					}
					if(secondaryMatches!=null && secondaryMatches.size() > 0){
						cibilResponse.setSecondaryMatches(secondaryMatches);
					}
				
				}
			}
			System.out.println(new XMLUtils().jaxbObjectToXML(cibilResponse));
			RequestDomain requestDomain= new RequestDomain();
			requestDomain.setUnqRefNo("999");
			requestDomain.setSoaFileNameC("SOA FILE NAME");
			requestDomain.setBatchId(44L);
			requestDomain.setRequestId(55L);
			requestDomain.setFirstName("prateek");
			requestDomain.setLastName("chandra");
			requestDomain.setMiddleName("goel");
			requestDomain.setBirthDt("02011989");
			requestDomain.setGender("Male");
			requestDomain.setPhoneNumber1("1234567890");
			requestDomain.setPhoneNumber2("1234567890");
			requestDomain.setPhoneNumber3("1234567890");
			requestDomain.setPhoneNumber4("1234567890");
			requestDomain.setPassportId("passport");
			requestDomain.setVoterId("voter");
			requestDomain.setUidNo("UID");
			requestDomain.setPanId("panId");
			requestDomain.setDrivingLicenseNo("drivingLicenseNo");
			requestDomain.setEmailId1("emailId1");
			requestDomain.setEmailId2("emailId2");
			
			
			//CustomReport customReport = new CreateCustomReport("test", 3977L).createCustomReport(cibilResponse, requestDomain);
			CibilSropDomainPopulation opt=new CibilSropDomainPopulation();
			List<HibCIBILSropDomain> domain = null;
			try {
				domain = opt.cibilSropDomainGenerator(cibilResponse,  "1234","LOS", "1234", 4010L);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//CustomReport customReport = new CreateCustomReport("test", 3977L).createCustomReport(cibilResponse, requestDomain);
			System.out.println("--> domain >> "+domain.toString());
			System.out.println("Domain :"+domain);
	} catch (Exception e) {
		
		e.printStackTrace();
	}
}*/
}