package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WelcomeScreen {

        private String welcomecalluserid;

        private String tvrdecision;

        private String tvrremarks;

        private String tvrstatus;
}
