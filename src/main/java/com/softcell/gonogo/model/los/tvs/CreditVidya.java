package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditVidya {

/*

    Object bankAccount;
    CreditCardAccount creditCardAccount;
*/

        private String errorMessage;

        private CreditCardAccount creditCardAccount;

        private Metadata metadata;

        private BankAccount bankAccount;

}
