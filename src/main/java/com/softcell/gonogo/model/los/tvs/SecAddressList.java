package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class SecAddressList {
	private String pinCode;

    private String enrichedThroughtEnquiry;

    private String sno;

    private String dateReported;

    private String stateCode;

    private String addressLine3;

    private String addressCategory;

    private String addressLine2;

    private String addressLine1;


}
