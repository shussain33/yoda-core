package com.softcell.gonogo.model.los;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

/**
 * Created by vinod on 7/11/17.
 */
@Document(collection = "losMbData")
public class LosMbData extends AuditEntity {

    @Id
    private String refId;

    private String losMbFileName;

    private String losMbInterfaceFlag;

    private String losId;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getLosMbFileName() {
        return losMbFileName;
    }

    public void setLosMbFileName(String losMbFileName) {
        this.losMbFileName = losMbFileName;
    }

    public String getLosMbInterfaceFlag() {
        return losMbInterfaceFlag;
    }

    public void setLosMbInterfaceFlag(String losMbInterfaceFlag) {
        this.losMbInterfaceFlag = losMbInterfaceFlag;
    }

    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LosMbData losMbData = (LosMbData) o;
        return Objects.equals(refId, losMbData.refId) &&
                Objects.equals(losMbFileName, losMbData.losMbFileName) &&
                Objects.equals(losMbInterfaceFlag, losMbData.losMbInterfaceFlag) &&
                Objects.equals(losId, losMbData.losId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), refId, losMbFileName, losMbInterfaceFlag, losId);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LosMbData{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", losMbFileName='").append(losMbFileName).append('\'');
        sb.append(", losMbInterfaceFlag='").append(losMbInterfaceFlag).append('\'');
        sb.append(", losId='").append(losId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{

        private LosMbData losMbData  =  new LosMbData();

        public LosMbData build(){
            return this.losMbData;
        }

        public Builder refId(String refId){
            this.losMbData.setRefId(refId);
            return this;
        }

        public Builder losMbFileName(String fileName){
            this.losMbData.setLosMbFileName(fileName);
            return this;
        }

        public Builder losMbInterfaceFlag(String mbInterfaceFlag){
            this.losMbData.setLosMbInterfaceFlag(mbInterfaceFlag);
            return this;
        }

        public Builder losId(String losId){
            this.losMbData.setLosId(losId);
            return this;
        }
    }
}
