package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Accounts {


	private String ambMonth0;

	private String account_cnt;

	private String ambMonth1;

	private String volatilityDebit;

	private String sno;

	private String volatilityCredit;

	private String volatilityBalance;

	private String latestBalance;

	private String debitMonth2;

	private String bankName;

	private String debitMonth1;

	private String debitMonth0;

	private String ambMonth2;

	private String creditMonth0;

	private String creditMonth1;

	private String creditMonth2;


}
