package com.softcell.gonogo.model.los;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

/**
 * Created by vinod on 30/11/17.
 */
@Document(collection = "losChargeConfig")
public class LosChargeConfig {

    public enum LosChargeType{
        LOYALTY_CARD,
        EXTENDED_WARRANTY
    }

    @NotEmpty(groups = InsertGrp.class)
    @JsonProperty("sChrgTyp")
    private LosChargeType chargeType;

    @NotEmpty(groups = InsertGrp.class)
    @JsonProperty("sProductId")
    private String productId;

    @NotEmpty(groups = InsertGrp.class)
    @JsonProperty("sChrgCode")
    private String chargeCode;

    @NotEmpty(groups = InsertGrp.class)
    @JsonProperty("sInstId")
    private String institutionId;

    public LosChargeType getChargeType() {
        return chargeType;
    }

    public void setChargeType(LosChargeType chargeType) {
        this.chargeType = chargeType;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LosChargeConfig that = (LosChargeConfig) o;
        return Objects.equals(chargeType, that.chargeType) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(chargeCode, that.chargeCode) &&
                Objects.equals(institutionId, that.institutionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chargeType, productId, chargeCode, institutionId);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LosChargeConfig{");
        sb.append("chargeType=").append(chargeType);
        sb.append(", productId='").append(productId).append('\'');
        sb.append(", chargeCode='").append(chargeCode).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public interface InsertGrp{}
}
