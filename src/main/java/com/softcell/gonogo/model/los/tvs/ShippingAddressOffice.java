package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ShippingAddressOffice {

	private String addressline2;

    private String landmark;

    private String addressline1;

    private int pin;

    private String state;

    private int monthataddress;

    private String city;


}
