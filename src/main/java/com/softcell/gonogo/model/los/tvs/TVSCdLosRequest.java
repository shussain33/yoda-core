package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by bhuvneshk on 5/1/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TVSCdLosRequest {

    String name;
    String data;
    String key;
}
