package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SerialNumber {
	private String scheme;

    private String dealerid;

    private String snoValidationStatus;

    private String saleDate;

    private String assetvendor;

    private String serialnumber;

    private String producttype;

    private String imeinumber;

    private String skucode;


}
