package com.softcell.gonogo.model.los.tvs;

import java.util.List;

public class DocumentListForGroupFive {


	private List<DMSDocuments> dmsDocList;
    private	List<Pdfs> pdfsDocList;
	public List<Pdfs> getPdfsDocList() {
		return pdfsDocList;
	}
	public void setPdfsDocList(List<Pdfs> pdfsDocList) {
		this.pdfsDocList = pdfsDocList;
	}
	public List<DMSDocuments> getDmsDocList() {
		return dmsDocList;
	}
	public void setDmsDocList(List<DMSDocuments> dmsDocList) {
		this.dmsDocList = dmsDocList;
	}

}
