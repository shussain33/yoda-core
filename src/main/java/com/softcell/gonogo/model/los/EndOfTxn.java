package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EndOfTxn implements Serializable {

    /**
     * @author AshwiniP
     */
    private static final long serialVersionUID = 1L;

    @Expose
    @SerializedName("HEADER")
    private ResponseHeader headerObj;

    @Expose
    @SerializedName("STATUS")
    private String status;

    @Expose
    @SerializedName("SENT-TO-CIBIL")
    private String sentToCibil;

    @Expose
    @SerializedName("SENT-TO-EQUIFAX")
    private String sentToEquifax;

    @Expose
    @SerializedName("SENT-TO-EXPERIAN")
    private String sentToExperian;

    @Expose
    @SerializedName("SENT-TO-CHM")
    private String sentToChm;

    @Expose
    @SerializedName("ACKNOWLEDGEMENT-ID")
    private String ackId;

    @Expose
    @SerializedName("SENT-SCORE")
    private String sentScore;

    @Expose
    @SerializedName("SENT-MERGED-REPORT")
    private String sentMergedReport;

    @Expose
    @SerializedName("FILLER1")
    private java.lang.String filler1;

    @Expose
    @SerializedName("FILLER2")
    private java.lang.String filler2;

    @Expose
    @SerializedName("FILLER3")
    private java.lang.String filler3;

    @Expose
    @SerializedName("FILLER4")
    private java.lang.String filler4;

    @Expose
    @SerializedName("FILLER5")
    private java.lang.String filler5;

    public String getAckId() {
        return ackId;
    }

    public void setAckId(String ackId) {
        this.ackId = ackId;
    }

    public ResponseHeader getHeaderObj() {
        return headerObj;
    }

    public void setHeaderObj(ResponseHeader headerObj) {
        this.headerObj = headerObj;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSentToCibil() {
        return sentToCibil;
    }

    public void setSentToCibil(String sentToCibil) {
        this.sentToCibil = sentToCibil;
    }

    public String getSentToEquifax() {
        return sentToEquifax;
    }

    public void setSentToEquifax(String sentToEquifax) {
        this.sentToEquifax = sentToEquifax;
    }

    public String getSentToExperian() {
        return sentToExperian;
    }

    public void setSentToExperian(String sentToExperian) {
        this.sentToExperian = sentToExperian;
    }

    public String getSentToChm() {
        return sentToChm;
    }

    public void setSentToChm(String sentToChm) {
        this.sentToChm = sentToChm;
    }

    public String getSentScore() {
        return sentScore;
    }

    public void setSentScore(String sentScore) {
        this.sentScore = sentScore;
    }

    public String getSentMergedReport() {
        return sentMergedReport;
    }

    public void setSentMergedReport(String sentMergedReport) {
        this.sentMergedReport = sentMergedReport;
    }


    public java.lang.String getFiller1() {
        return filler1;
    }

    public void setFiller1(java.lang.String filler1) {
        this.filler1 = filler1;
    }

    public java.lang.String getFiller2() {
        return filler2;
    }

    public void setFiller2(java.lang.String filler2) {
        this.filler2 = filler2;
    }

    public java.lang.String getFiller3() {
        return filler3;
    }

    public void setFiller3(java.lang.String filler3) {
        this.filler3 = filler3;
    }

    public java.lang.String getFiller4() {
        return filler4;
    }

    public void setFiller4(java.lang.String filler4) {
        this.filler4 = filler4;
    }

    public java.lang.String getFiller5() {
        return filler5;
    }

    public void setFiller5(java.lang.String filler5) {
        this.filler5 = filler5;
    }

    @Override
    public String toString() {
        return "EndOfTxn [headerObj=" + headerObj + ", status=" + status
                + ", sentToCibil=" + sentToCibil + ", sentToEquifax="
                + sentToEquifax + ", sentToExperian=" + sentToExperian
                + ", sentToChm=" + sentToChm + ", ackId=" + ackId
                + ", sentScore=" + sentScore + ", sentMergedReport="
                + sentMergedReport + ", filler1=" + filler1 + ", filler2="
                + filler2 + ", filler3=" + filler3 + ", filler4=" + filler4
                + ", filler5=" + filler5 + "]";
    }


}
