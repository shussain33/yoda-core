package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Enach {

	 private String aadhaarnumber;

	    private String enachdate;

	    private String status;

	    private String consenttoesigned;

	    private String transactionid;

	    private String authmode;

	    private String filetype;

	    private String refid;

}
