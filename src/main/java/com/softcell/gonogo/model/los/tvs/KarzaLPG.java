package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KarzaLPG {


        private String totalrefillconsumed;

        private String status;

        private String bankname;

        private String consumerno;

        private String lpgstatus;

        private String lng;

        private String iscallmode;

        private String distributorname;

        private String city;

        private String pincode;

        private String addressline2;

        private String aadhaarnumber;

        private String lpg_id;

        private String consumeremail;

        private String addressline1;

        private String bankaccountno;

        private String consumeraddress;

        private String consumername;

        private String distributorcode;

        private String subsidyavailed;

        private String refillconsumed;

        private String ifsccode;

        private String lat;

        private String givenupsubsidy;

        private String consumercontact;

        private String distributoraddress;

}
