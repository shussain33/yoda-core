package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BankDetails {
    private String bankbranch;

    private String repaymenttype;

    private String accountingtype;

    private String bankname;

    private String lastname;

    private String accountnumber;

    private String firstname;

    private String micrcode;

    private String ifsccode;

}



