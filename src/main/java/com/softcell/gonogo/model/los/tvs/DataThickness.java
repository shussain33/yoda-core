package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class DataThickness {

	  private String imageCount;

	    private String contactCount;

	    private String smsCount;

	    private String accountCount;

	    private String locationCount;

	    private String appCount;

	    private String callCount;



}
