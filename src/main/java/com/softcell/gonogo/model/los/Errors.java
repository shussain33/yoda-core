package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Errors implements Serializable {

    /**
     * @author AshwiniP
     */
    private static final long serialVersionUID = 1L;

    @Expose
    @SerializedName("CODE")
    private String code;
    @Expose
    @SerializedName("DESCRIPTION")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Errors [code=" + code + ", description=" + description + "]";
    }


}
