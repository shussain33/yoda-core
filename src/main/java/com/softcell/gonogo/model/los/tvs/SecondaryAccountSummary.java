package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class SecondaryAccountSummary {

	    private String secondaryUntaggedNumberOfAccounts;

	    private String secondaryNumberOfAccounts;

	    private String secondarySanctionedAmount;

	    private String secondarySecuredNumberOfAccounts;

	    private String secondaryActiveNumberOfAccounts;

	    private String secondaryUnsecuredNumberOfAccounts;

	    private String secondaryCurrentBalance;

	    private String secondaryOverdueNumberOfAccounts;

	    private String secondaryDisbursedAmount;


}
