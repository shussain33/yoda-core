package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PermissionGiven {

	private String phone;

    private String location;

    private String account;

    private String sms;

    private String storage;

    private String calendar;


}
