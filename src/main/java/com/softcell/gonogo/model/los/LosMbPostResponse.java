package com.softcell.gonogo.model.los;

import com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauEoTAck;
import com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauResponseAck;


public class LosMbPostResponse {

    MultiBureauResponseAck mbResponseAck;
    MultiBureauEoTAck mbEoTAck;

    /**
     * @return the mbResponseAck
     */
    public MultiBureauResponseAck getMbResponseAck() {
        return mbResponseAck;
    }

    /**
     * @param mbResponseAck the mbResponseAck to set
     */
    public void setMbResponseAck(MultiBureauResponseAck mbResponseAck) {
        this.mbResponseAck = mbResponseAck;
    }

    /**
     * @return the mbEoTAck
     */
    public MultiBureauEoTAck getMbEoTAck() {
        return mbEoTAck;
    }

    /**
     * @param mbEoTAck the mbEoTAck to set
     */
    public void setMbEoTAck(MultiBureauEoTAck mbEoTAck) {
        this.mbEoTAck = mbEoTAck;
    }


}
