package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Scoring {

        private String officeaddrscore;

        private String residenceaddrscore;

        private String crifscore;

        private String cibilscore;

        private String applicationscore;

        private ScoreList[] scoreList;

    }
