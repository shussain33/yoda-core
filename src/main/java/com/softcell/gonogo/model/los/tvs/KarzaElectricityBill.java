package com.softcell.gonogo.model.los.tvs;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KarzaElectricityBill {

        private String status;

        private String serviceprovider;

        private String billissuedate;

        private String iscallmode;

        private String billamount;

        private String address;

        private String consumerid;

        private String consumername;

        private String email;

        private String mobilenumber;

        private String amountpayable;

        private String billno;

        private String billduedate;

        private String totalamount;
}
