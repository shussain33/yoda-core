package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class EnquiryList {

    private String sno;

    private String dateReported;

    private String enquiryPurpose;

    private String enquiryAmount;

    private String reportingMemberShortName;


}
