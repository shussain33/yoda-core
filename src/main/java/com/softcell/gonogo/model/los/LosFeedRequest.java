package com.softcell.gonogo.model.los;

/**
 * It will be use to send request to los for gng storage
 *
 * @author bhuvneshk
 */
public class LosFeedRequest {

    private String institutionId;
    private String request;

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the request
     */
    public String getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(String request) {
        this.request = request;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LosFeedRequest [institutionId=");
        builder.append(institutionId);
        builder.append(", request=");
        builder.append(request);
        builder.append("]");
        return builder.toString();
    }


}
