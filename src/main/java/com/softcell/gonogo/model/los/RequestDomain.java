package com.softcell.gonogo.model.los;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class RequestDomain implements Cloneable, Serializable {


    private static final long serialVersionUID = 1L;


    static Logger logger_ = LoggerFactory.getLogger(RequestDomain.class);


    private String fax1;
    private String fax2;
    private String fax3;
    private String cin;
    private String requestStatus;
    private Integer lastRunRuleTreeNo;
    private Long decisionByNodeId;
    private Long institutionId;
    private Long requestId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String name4;
    private String name5;
    private String gender;
    private String maritalStatus;
    private String fatherName;
    private String spouseName;
    private String motherName;
    private String relationType1;
    private String relationType1Value;
    private String relationType2;
    private String relationType2Value;
    private String keyPersonName;
    private String keyPersonRelation;
    private String nomineeName;
    private String nomineeRelationType;
    private String age;
    private String ageAsOnDt;
    private String birthDt;
    private String address1Type;
    private String address1ResidenceCode;
    private String address1;
    private String address1City;
    private String address1Pin;
    private String address1Country;
    private String address1District;
    private String address1State;
    private String address2Type;
    private String address2ResidenceCode;
    private String address2;
    private String address2City;
    private String address2Pin;
    private String address2State;
    private String address3Type;
    private String address3ResidenceCode;
    private String address3;
    private String address3City;
    private String address3Pin;
    private String address3State;
    private String panId;
    private String passportId;
    private String voterId;
    private String drivingLicenseNo;
    private String uidNo;
    private String rationCard;
    private String idType1;
    private String idType1Value;
    private String idType2;
    private String idType2Value;
    private String phoneType1;
    private String phoneNumber1;
    private String phoneExtn1;
    private String phoneType2;
    private String phoneNumber2;
    private String phoneExtn2;
    private String phoneType3;
    private String phoneNumber3;
    private String phoneExtn3;
    private String phoneType4;
    private String phoneNumber4;
    private String phoneExtn4;
    private String emailId1;
    private String emailId2;
    private String alias;
    private String loanType;
    private String loanAmount;
    private String consumerId;
    private String requestType;
    private String applicationId;
    private String hmStatus;
    private String cibilStatus;
    private String equifaxStatus;
    private String experianStatus;
    private String inquirySubmittedBy;
    private String sourceSystemName;
    private String sourceSystemVersion;
    private String sourceSystemVender;
    private String sourceSystemInstanceId;
    private String bureauRegion;
    private String loanPurposeDesc;
    private String branchIfsccode;
    private String kendra;
    private Long batchId;
    private String inquiryStage;
    private String actionType;
    private String authrizationFlag;
    private String authroizedBy;
    private String responseFormat;
    private String mbrRfnceOverrideFlag;
    private String embedResponseFlag;
    private String testFlag;
    private String tuFlag;
    private String unqRefNo;
    private String credtRptTrnId;
    private String credtInqPurpsTypDesc;
    private String credtRptTrnDtTm;
    private String actOpeningDt;
    private Date insertDt;
    private Date ipStagingInsertDt;
    private String accountNumber1;
    private String accountNumber2;
    private String accountNumber3;
    private String accountNumber4;
    private String fullName;
    private String sourceSystemGroupCode;

    private String soaFileNameC;
    private String tenure;
    private String internalExternalFlag;
    private String soaInstaLoanFlagC;
    private String firstSource;
    private String soaSourceBrokerIdN;
    private String policySegment;
    private String soaPromotionC;
    private String regionCode;
    private String lsiLosNo;
    private String lsiFinnoneLanNo;
    private String tokenNumber1;
    private String tokenNumber2;
    private String tokenNumber3;
    private String loanAccountNumber1;
    private String loanAccountNumber2;
    private String loanAccountNumber3;
    private String panIssueDate;
    private String panExpirationDate;
    private String passportIssueDate;
    private String passportExpirationDate;
    private String voterIdIssueDate;
    private String voterIdExpirationDate;
    private String driverLicenseIssueDate;
    private String driverLicenseExpirationDate;
    private String rationCardIssueDate;
    private String rationCardExpirationDate;
    private String universalIdIssueDate;
    private String universalIdExpirationDate;
    private String customerSegment;
    private String borrowerFlag;
    private String individualCorporateFlag;
    private String constitution;
    private String groupId;
    private String numberCreditCards;
    private String creditCardNo;
    private String monthlyIncome;
    private String soaEmployerNameC;
    private String timeWithEmploy;
    private String companyCategory;
    private String natureOfBusiness;
    private String assetCost;
    private String logo;
    private String collateral1;
    private String collateral1Valuation1;
    private String collateral1Valuation2;
    private String collateral2;
    private String collateral2Valuation1;
    private String collateral2Valuation2;
    private String applicationScore;
    private String ddeScore;
    private String debitScore;
    private String behaviourScore;
    private Long ipStgSrNo;
    private Date requestReadDateTime;
    private String parameter1;
    private String parameter2;
    private String parameter3;
    private String parameter4;
    private String parameter5;
    private String parameter6;
    private String parameter7;
    private String parameter8;
    private String parameter9;
    private String parameter10;
    private String parameter11;
    private String parameter12;
    private String parameter13;
    private String parameter14;
    private String parameter15;
    private String parameter16;
    private String parameter17;
    private String parameter18;
    private String parameter19;
    private String parameter20;
    private String parameter21;
    private String parameter22;
    private String parameter23;
    private String parameter24;
    private String parameter25;
    private String parameter26;
    private String parameter27;
    private String parameter28;
    private String parameter29;
    private String parameter30;
    private String parameter31;
    private String parameter32;
    private String parameter33;
    private String parameter34;
    private String parameter35;
    private String parameter36;
    private String parameter37;
    private String parameter38;
    private String parameter39;
    private String parameter40;
    private String parameter41;
    private String parameter42;
    private String parameter43;
    private String parameter44;
    private String parameter45;
    private String parameter46;
    private String parameter47;
    private String parameter48;
    private String parameter49;
    private String parameter50;
    private String serviceTaxNo;

    // job priority

    //private JobPriorityType jobPriorityType;

    // tag for equifax request
    private String noOfDependents;

    // Fields specific to commercial inquiry
    private String duns;
    private String tin;
    private String regNo;
    private String commercialIndr;
    private String legalConstitution;
    private String tan;

    private boolean address1ValidSwitch = true;
    private boolean address2ValidSwitch = true;
    private boolean address3ValidSwitch = true;
    private boolean phone1ValidSwitch = true;
    private boolean phone2ValidSwitch = true;
    private boolean phone3ValidSwitch = true;
    private boolean phone4ValidSwitch = true;
    private boolean phone1ExtnValidSwitch = true;
    private boolean phone2ExtnValidSwitch = true;
    private boolean phone3ExtnValidSwitch = true;
    private boolean phone4ExtnValidSwitch = true;
    private boolean id1ValidSwitch = true;
    private boolean id2ValidSwitch = true;
    private boolean relation1ValidSwitch = true;
    private boolean relation2ValidSwitch = true;
    private boolean nomineeValidSwitch = true;
    private boolean keyPersonValidSwitch = true;
    private boolean dobValidSwitch = true;
    private boolean ageValidSwitch = true;
    private boolean panValidSwitch = true;
    private boolean passportValidSwitch = true;
    private boolean voterValidSwitch = true;
    private boolean uidValidSwitch = true;
    private boolean rationValidSwitch = true;
    private boolean drivingLicenseValidSwitch = true;
    private boolean email1ValidSwitch = true;
    private boolean email2ValidSwitch = true;
    private boolean fatherNameValidSwitch = true;
    private boolean motherNameValidSwitch = true;
    private boolean spouseNameValidSwitch = true;

    private String bureauProduct;


    ///Commercial Ace

    //private IndividualEntitiesSegment individualEntitiesSegment;
    //private OrganisationEntitiesSegment organisationEntitiesSegment;
    private String classOfActivity1;
    private String classOfActivity2;
    private String classOfActivity3;


    // Fields added for Rule engine override
    private String defaultConfig;
    private String cibProduct;
    private String chmProduct;
    private String eqfProduct;
    private String expProduct;

    private Long commRefId;

    private String tan2;
    private String tan3;
    private String duns2;
    private String duns3;
    private String requestTime;
    private boolean syncMode;
    //private SerializedJSONArray issueFormat;

    private boolean scoringFlag;
    private String rawRequest;

    public String getTan2() {
        return tan2;
    }

    public void setTan2(String tan2) {
        this.tan2 = tan2;
    }

    public String getTan3() {
        return tan3;
    }

    public void setTan3(String tan3) {
        this.tan3 = tan3;
    }

    public String getDuns2() {
        return duns2;
    }

    public void setDuns2(String duns2) {
        this.duns2 = duns2;
    }

    public String getDuns3() {
        return duns3;
    }

    public void setDuns3(String duns3) {
        this.duns3 = duns3;
    }

    public String getFax1() {
        return fax1;
    }

    public void setFax1(String fax1) {
        this.fax1 = fax1;
    }

    public String getTan() {
        return tan;
    }

    public void setTan(String tan) {
        this.tan = tan;
    }

    public String getFax2() {
        return fax2;
    }

    public void setFax2(String fax2) {
        this.fax2 = fax2;
    }

    public String getFax3() {
        return fax3;
    }

    public void setFax3(String fax3) {
        this.fax3 = fax3;
    }

    public String getClassOfActivity1() {
        return classOfActivity1;
    }

    public void setClassOfActivity1(String classOfActivity1) {
        this.classOfActivity1 = classOfActivity1;
    }

    public String getClassOfActivity2() {
        return classOfActivity2;
    }

    public void setClassOfActivity2(String classOfActivity2) {
        this.classOfActivity2 = classOfActivity2;
    }

    public String getClassOfActivity3() {
        return classOfActivity3;
    }

    public void setClassOfActivity3(String classOfActivity3) {
        this.classOfActivity3 = classOfActivity3;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getServiceTaxNo() {
        return serviceTaxNo;
    }

    public void setServiceTaxNo(String serviceTaxNo) {
        this.serviceTaxNo = serviceTaxNo;
    }

    public String getLegalConstitution() {
        return legalConstitution;
    }

    public void setLegalConstitution(String legalConstitution) {
        this.legalConstitution = legalConstitution;
    }

/*	public IndividualEntitiesSegment getIndividualEntitiesSegment() {
        return individualEntitiesSegment;
	}

	public void setIndividualEntitiesSegment(IndividualEntitiesSegment individualEntitiesSegment) {
		this.individualEntitiesSegment = individualEntitiesSegment;
	}

	public OrganisationEntitiesSegment getOrganisationEntitiesSegment() {
		return organisationEntitiesSegment;
	}

	public void setOrganisationEntitiesSegment(
			OrganisationEntitiesSegment organisationEntitiesSegment) {
		this.organisationEntitiesSegment = organisationEntitiesSegment;
	}*/

    public RequestDomain clone() {
        try {
            return (RequestDomain) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getParameter2() {
        return parameter2;
    }

    public void setParameter2(String parameter2) {
        this.parameter2 = parameter2;
    }

    public String getParameter3() {
        return parameter3;
    }

    public void setParameter3(String parameter3) {
        this.parameter3 = parameter3;
    }

    public String getParameter4() {
        return parameter4;
    }

    public void setParameter4(String parameter4) {
        this.parameter4 = parameter4;
    }

    public String getParameter5() {
        return parameter5;
    }

    public void setParameter5(String parameter5) {
        this.parameter5 = parameter5;
    }

    public String getParameter6() {
        return parameter6;
    }

    public void setParameter6(String parameter6) {
        this.parameter6 = parameter6;
    }

    public String getParameter7() {
        return parameter7;
    }

    public void setParameter7(String parameter7) {
        this.parameter7 = parameter7;
    }

    public String getParameter8() {
        return parameter8;
    }

    public void setParameter8(String parameter8) {
        this.parameter8 = parameter8;
    }

    public String getParameter9() {
        return parameter9;
    }

    public void setParameter9(String parameter9) {
        this.parameter9 = parameter9;
    }

    public Long getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(Long institutionId) {
        this.institutionId = institutionId;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public String getName5() {
        return name5;
    }

    public void setName5(String name5) {
        this.name5 = name5;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getRelationType1() {
        return relationType1;
    }

    public void setRelationType1(String relationType1) {
        this.relationType1 = relationType1;
    }

    public String getRelationType1Value() {
        return relationType1Value;
    }

    public void setRelationType1Value(String relationType1Value) {
        this.relationType1Value = relationType1Value;
    }

    public String getRelationType2() {
        return relationType2;
    }

    public void setRelationType2(String relationType2) {
        this.relationType2 = relationType2;
    }

    public String getRelationType2Value() {
        return relationType2Value;
    }

    public void setRelationType2Value(String relationType2Value) {
        this.relationType2Value = relationType2Value;
    }

    public String getKeyPersonName() {
        return keyPersonName;
    }

    public void setKeyPersonName(String keyPersonName) {
        this.keyPersonName = keyPersonName;
    }

    public String getKeyPersonRelation() {
        return keyPersonRelation;
    }

    public void setKeyPersonRelation(String keyPersonRelation) {
        this.keyPersonRelation = keyPersonRelation;
    }

    public String getNomineeName() {
        return nomineeName;
    }

    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    public String getNomineeRelationType() {
        return nomineeRelationType;
    }

    public void setNomineeRelationType(String nomineeRelationType) {
        this.nomineeRelationType = nomineeRelationType;
    }

    public String getAddress1Type() {
        return address1Type;
    }

    public void setAddress1Type(String address1Type) {
        this.address1Type = address1Type;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1City() {
        return address1City;
    }

    public void setAddress1City(String address1City) {
        this.address1City = address1City;
    }

    public String getAddress1Pin() {
        return address1Pin;
    }

    public void setAddress1Pin(String address1Pin) {
        this.address1Pin = address1Pin;
    }

    public String getAddress1State() {
        return address1State;
    }

    public void setAddress1State(String address1State) {
        this.address1State = address1State;
    }

    public String getAddress2Type() {
        return address2Type;
    }

    public void setAddress2Type(String address2Type) {
        this.address2Type = address2Type;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress2City() {
        return address2City;
    }

    public void setAddress2City(String address2City) {
        this.address2City = address2City;
    }

    public String getAddress2Pin() {
        return address2Pin;
    }

    public void setAddress2Pin(String address2Pin) {
        this.address2Pin = address2Pin;
    }

    public String getAddress2State() {
        return address2State;
    }

    public void setAddress2State(String address2State) {
        this.address2State = address2State;
    }

    public String getAddress3Type() {
        return address3Type;
    }

    public void setAddress3Type(String address3Type) {
        this.address3Type = address3Type;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress3City() {
        return address3City;
    }

    public void setAddress3City(String address3City) {
        this.address3City = address3City;
    }

    public String getAddress3Pin() {
        return address3Pin;
    }

    public void setAddress3Pin(String address3Pin) {
        this.address3Pin = address3Pin;
    }

    public String getAddress3State() {
        return address3State;
    }

    public void setAddress3State(String address3State) {
        this.address3State = address3State;
    }

    public String getPanId() {
        return panId;
    }

    public void setPanId(String panId) {
        this.panId = panId;
    }

    public String getPassportId() {
        return passportId;
    }

    public void setPassportId(String passportId) {
        this.passportId = passportId;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getDrivingLicenseNo() {
        return drivingLicenseNo;
    }

    public void setDrivingLicenseNo(String drivingLicenseNo) {
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public String getUidNo() {
        return uidNo;
    }

    public void setUidNo(String uidNo) {
        this.uidNo = uidNo;
    }

    public String getIdType1() {
        return idType1;
    }

    public void setIdType1(String idType1) {
        this.idType1 = idType1;
    }

    public String getIdType1Value() {
        return idType1Value;
    }

    public void setIdType1Value(String idType1Value) {
        this.idType1Value = idType1Value;
    }

    public String getIdType2() {
        return idType2;
    }

    public void setIdType2(String idType2) {
        this.idType2 = idType2;
    }

    public String getIdType2Value() {
        return idType2Value;
    }

    public void setIdType2Value(String idType2Value) {
        this.idType2Value = idType2Value;
    }

    public String getPhoneType1() {
        return phoneType1;
    }

    public void setPhoneType1(String phoneType1) {
        this.phoneType1 = phoneType1;
    }

    public String getPhoneNumber1() {
        return phoneNumber1;
    }

    public void setPhoneNumber1(String phoneNumber1) {
        this.phoneNumber1 = phoneNumber1;
    }

    public String getPhoneExtn1() {
        return phoneExtn1;
    }

    public void setPhoneExtn1(String phoneExtn1) {
        this.phoneExtn1 = phoneExtn1;
    }

    public String getPhoneType2() {
        return phoneType2;
    }

    public void setPhoneType2(String phoneType2) {
        this.phoneType2 = phoneType2;
    }

    public String getPhoneNumber2() {
        return phoneNumber2;
    }

    public void setPhoneNumber2(String phoneNumber2) {
        this.phoneNumber2 = phoneNumber2;
    }

    public String getPhoneExtn2() {
        return phoneExtn2;
    }

    public void setPhoneExtn2(String phoneExtn2) {
        this.phoneExtn2 = phoneExtn2;
    }

    public String getPhoneType3() {
        return phoneType3;
    }

    public void setPhoneType3(String phoneType3) {
        this.phoneType3 = phoneType3;
    }

    public String getPhoneNumber3() {
        return phoneNumber3;
    }

    public void setPhoneNumber3(String phoneNumber3) {
        this.phoneNumber3 = phoneNumber3;
    }

    public String getPhoneExtn3() {
        return phoneExtn3;
    }

    public void setPhoneExtn3(String phoneExtn3) {
        this.phoneExtn3 = phoneExtn3;
    }

    public String getPhoneType4() {
        return phoneType4;
    }

    public void setPhoneType4(String phoneType4) {
        this.phoneType4 = phoneType4;
    }

    public String getPhoneNumber4() {
        return phoneNumber4;
    }

    public void setPhoneNumber4(String phoneNumber4) {
        this.phoneNumber4 = phoneNumber4;
    }

    public String getPhoneExtn4() {
        return phoneExtn4;
    }

    public void setPhoneExtn4(String phoneExtn4) {
        this.phoneExtn4 = phoneExtn4;
    }

    public String getEmailId1() {
        return emailId1;
    }

    public void setEmailId1(String emailId1) {
        this.emailId1 = emailId1;
    }

    public String getEmailId2() {
        return emailId2;
    }

    public void setEmailId2(String emailId) {
        this.emailId2 = emailId;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getHmStatus() {
        return hmStatus;
    }

    public void setHmStatus(String hmStatus) {
        this.hmStatus = hmStatus;
    }

    public String getCibilStatus() {
        return cibilStatus;
    }

    public void setCibilStatus(String cibilStatus) {
        this.cibilStatus = cibilStatus;
    }

    public String getEquifaxStatus() {
        return equifaxStatus;
    }

    public void setEquifaxStatus(String equifaxStatus) {
        this.equifaxStatus = equifaxStatus;
    }

    public String getExperianStatus() {
        return experianStatus;
    }

    public void setExperianStatus(String experianStatus) {
        this.experianStatus = experianStatus;
    }

    public String getInquirySubmittedBy() {
        return inquirySubmittedBy;
    }

    public void setInquirySubmittedBy(String inquirySubmittedBy) {
        this.inquirySubmittedBy = inquirySubmittedBy;
    }

    public String getSourceSystemName() {
        return sourceSystemName;
    }

    public void setSourceSystemName(String sourceSystemName) {
        this.sourceSystemName = sourceSystemName;
    }

    public String getSourceSystemVersion() {
        return sourceSystemVersion;
    }

    public void setSourceSystemVersion(String sourceSystemVersion) {
        this.sourceSystemVersion = sourceSystemVersion;
    }

    public String getSourceSystemVender() {
        return sourceSystemVender;
    }

    public void setSourceSystemVender(String sourceSystemVender) {
        this.sourceSystemVender = sourceSystemVender;
    }

    public String getSourceSystemInstanceId() {
        return sourceSystemInstanceId;
    }

    public void setSourceSystemInstanceId(String sourceSystemInstanceId) {
        this.sourceSystemInstanceId = sourceSystemInstanceId;
    }

    public String getLoanPurposeDesc() {
        return loanPurposeDesc;
    }

    public void setLoanPurposeDesc(String loanPurposeDesc) {
        this.loanPurposeDesc = loanPurposeDesc;
    }

    public String getKendra() {
        return kendra;
    }

    public void setKendra(String kendra) {
        this.kendra = kendra;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public String getRationCard() {
        return rationCard;
    }

    public void setRationCard(String rationCard) {
        this.rationCard = rationCard;
    }

    public String getLosInstanceId() {
        return sourceSystemInstanceId;
    }

    public void setLosInstanceId(String losInstanceId) {
        this.sourceSystemInstanceId = losInstanceId;
    }

    public String getBureauRegion() {
        return bureauRegion;
    }

    public void setBureauRegion(String bureauRegion) {
        this.bureauRegion = bureauRegion;
    }

    public String getBranchIfsccode() {
        return branchIfsccode;
    }

    public void setBranchIfsccode(String branchIfsccode) {
        this.branchIfsccode = branchIfsccode;
    }

    public String getInquiryStage() {
        return inquiryStage;
    }

    public void setInquiryStage(String inquiryStage) {
        this.inquiryStage = inquiryStage;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getAuthrizationFlag() {
        return authrizationFlag;
    }

    public void setAuthrizationFlag(String authrizationFlag) {
        this.authrizationFlag = authrizationFlag;
    }

    public String getAuthroizedBy() {
        return authroizedBy;
    }

    public void setAuthroizedBy(String authroizedBy) {
        this.authroizedBy = authroizedBy;
    }

    public String getResponseFormat() {
        return responseFormat;
    }

    public void setResponseFormat(String responseFormat) {
        this.responseFormat = responseFormat;
    }

    public String getMbrRfnceOverrideFlag() {
        return mbrRfnceOverrideFlag;
    }

    public void setMbrRfnceOverrideFlag(String mbrRfnceOverrideFlag) {
        this.mbrRfnceOverrideFlag = mbrRfnceOverrideFlag;
    }

    public String getEmbedResponseFlag() {
        return embedResponseFlag;
    }

    public void setEmbedResponseFlag(String embedResponseFlag) {
        this.embedResponseFlag = embedResponseFlag;
    }

    public String getTestFlag() {
        return testFlag;
    }

    public void setTestFlag(String testFlag) {
        this.testFlag = testFlag;
    }

    public String getTuFlag() {
        return tuFlag;
    }

    public void setTuFlag(String tuFlag) {
        this.tuFlag = tuFlag;
    }

    public String getUnqRefNo() {
        return unqRefNo;
    }

    public void setUnqRefNo(String unqRefNo) {
        this.unqRefNo = unqRefNo;
    }

    public String getCredtRptTrnId() {
        return credtRptTrnId;
    }

    public void setCredtRptTrnId(String credtRptTrnId) {
        this.credtRptTrnId = credtRptTrnId;
    }

    public String getCredtInqPurpsTypDesc() {
        return credtInqPurpsTypDesc;
    }

    public void setCredtInqPurpsTypDesc(String credtInqPurpsTypDesc) {
        this.credtInqPurpsTypDesc = credtInqPurpsTypDesc;
    }

    public String getActOpeningDt() {
        return actOpeningDt;
    }

    public void setActOpeningDt(String actOpeningDt) {
        this.actOpeningDt = actOpeningDt;
    }

    public Date getInsertDt() {
        return insertDt;
    }

    public void setInsertDt(Date insertDt) {
        this.insertDt = insertDt;
    }

    public Date getIpStagingInsertDt() {
        return ipStagingInsertDt;
    }

    public void setIpStagingInsertDt(Date ipStagingInsertDt) {
        this.ipStagingInsertDt = ipStagingInsertDt;
    }

    public String getAddress1ResidenceCode() {
        return address1ResidenceCode;
    }

    public void setAddress1ResidenceCode(String address1ResidenceCode) {
        this.address1ResidenceCode = address1ResidenceCode;
    }

    public String getAddress2ResidenceCode() {
        return address2ResidenceCode;
    }

    public void setAddress2ResidenceCode(String address2ResidenceCode) {
        this.address2ResidenceCode = address2ResidenceCode;
    }

    public String getAddress3ResidenceCode() {
        return address3ResidenceCode;
    }

    public void setAddress3ResidenceCode(String address3ResidenceCode) {
        this.address3ResidenceCode = address3ResidenceCode;
    }

    public String getAccountNumber1() {
        return accountNumber1;
    }

    public void setAccountNumber1(String accountNumber1) {
        this.accountNumber1 = accountNumber1;
    }

    public String getAccountNumber2() {
        return accountNumber2;
    }

    public void setAccountNumber2(String accountNumber2) {
        this.accountNumber2 = accountNumber2;
    }

    public String getAccountNumber3() {
        return accountNumber3;
    }

    public void setAccountNumber3(String accountNumber3) {
        this.accountNumber3 = accountNumber3;
    }

    public String getAccountNumber4() {
        return accountNumber4;
    }

    public void setAccountNumber4(String accountNumber4) {
        this.accountNumber4 = accountNumber4;
    }

    public String getFullName() {

        String fullName = "";

        if (StringUtils.isNotBlank(firstName))
            fullName += firstName;

        if (StringUtils.isNotBlank(middleName))
            fullName += " " + middleName;

        if (StringUtils.isNotBlank(lastName))
            fullName += " " + lastName;

        if (StringUtils.isNotBlank(name4))
            fullName += " " + name4;

        if (StringUtils.isNotBlank(name5))
            fullName += " " + name5;

        this.fullName = fullName.trim();

        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSourceSystemGroupCode() {
        return sourceSystemGroupCode;
    }

    public void setSourceSystemGroupCode(String sourceSystemGroupCode) {
        this.sourceSystemGroupCode = sourceSystemGroupCode;
    }

    public String getSoaFileNameC() {
        return soaFileNameC;
    }

    public void setSoaFileNameC(String soaFileNameC) {
        this.soaFileNameC = soaFileNameC;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public String getInternalExternalFlag() {
        return internalExternalFlag;
    }

    public void setInternalExternalFlag(String internalExternalFlag) {
        this.internalExternalFlag = internalExternalFlag;
    }

    public String getSoaInstaLoanFlagC() {
        return soaInstaLoanFlagC;
    }

    public void setSoaInstaLoanFlagC(String soaInstaLoanFlagC) {
        this.soaInstaLoanFlagC = soaInstaLoanFlagC;
    }

    public String getFirstSource() {
        return firstSource;
    }

    public void setFirstSource(String firstSource) {
        this.firstSource = firstSource;
    }

    public String getSoaSourceBrokerIdN() {
        return soaSourceBrokerIdN;
    }

    public void setSoaSourceBrokerIdN(String soaSourceBrokerIdN) {
        this.soaSourceBrokerIdN = soaSourceBrokerIdN;
    }

    public String getPolicySegment() {
        return policySegment;
    }

    public void setPolicySegment(String policySegment) {
        this.policySegment = policySegment;
    }

    public String getSoaPromotionC() {
        return soaPromotionC;
    }

    public void setSoaPromotionC(String soaPromotionC) {
        this.soaPromotionC = soaPromotionC;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getLsiLosNo() {
        return lsiLosNo;
    }

    public void setLsiLosNo(String lsiLosNo) {
        this.lsiLosNo = lsiLosNo;
    }

    public String getLsiFinnoneLanNo() {
        return lsiFinnoneLanNo;
    }

    public void setLsiFinnoneLanNo(String lsiFinnoneLanNo) {
        this.lsiFinnoneLanNo = lsiFinnoneLanNo;
    }

    public String getTokenNumber1() {
        return tokenNumber1;
    }

    public void setTokenNumber1(String tokenNumber1) {
        this.tokenNumber1 = tokenNumber1;
    }

    public String getTokenNumber2() {
        return tokenNumber2;
    }

    public void setTokenNumber2(String tokenNumber2) {
        this.tokenNumber2 = tokenNumber2;
    }

    public String getTokenNumber3() {
        return tokenNumber3;
    }

    public void setTokenNumber3(String tokenNumber3) {
        this.tokenNumber3 = tokenNumber3;
    }

    public String getLoanAccountNumber1() {
        return loanAccountNumber1;
    }

    public void setLoanAccountNumber1(String loanAccountNumber1) {
        this.loanAccountNumber1 = loanAccountNumber1;
    }

    public String getLoanAccountNumber2() {
        return loanAccountNumber2;
    }

    public void setLoanAccountNumber2(String loanAccountNumber2) {
        this.loanAccountNumber2 = loanAccountNumber2;
    }

    public String getLoanAccountNumber3() {
        return loanAccountNumber3;
    }

    public void setLoanAccountNumber3(String loanAccountNumber3) {
        this.loanAccountNumber3 = loanAccountNumber3;
    }

    public String getPanIssueDate() {
        return panIssueDate;
    }

    public void setPanIssueDate(String panIssueDate) {
        this.panIssueDate = panIssueDate;
    }

    public String getPanExpirationDate() {
        return panExpirationDate;
    }

    public void setPanExpirationDate(String panExpirationDate) {
        this.panExpirationDate = panExpirationDate;
    }

    public String getPassportIssueDate() {
        return passportIssueDate;
    }

    public void setPassportIssueDate(String passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }

    public String getPassportExpirationDate() {
        return passportExpirationDate;
    }

    public void setPassportExpirationDate(String passportExpirationDate) {
        this.passportExpirationDate = passportExpirationDate;
    }

    public String getVoterIdIssueDate() {
        return voterIdIssueDate;
    }

    public void setVoterIdIssueDate(String voterIdIssueDate) {
        this.voterIdIssueDate = voterIdIssueDate;
    }

    public String getVoterIdExpirationDate() {
        return voterIdExpirationDate;
    }

    public void setVoterIdExpirationDate(String voterIdExpirationDate) {
        this.voterIdExpirationDate = voterIdExpirationDate;
    }

    public String getDriverLicenseIssueDate() {
        return driverLicenseIssueDate;
    }

    public void setDriverLicenseIssueDate(String driverLicenseIssueDate) {
        this.driverLicenseIssueDate = driverLicenseIssueDate;
    }

    public String getDriverLicenseExpirationDate() {
        return driverLicenseExpirationDate;
    }

    public void setDriverLicenseExpirationDate(
            String driverLicenseExpirationDate) {
        this.driverLicenseExpirationDate = driverLicenseExpirationDate;
    }

    public String getRationCardIssueDate() {
        return rationCardIssueDate;
    }

    public void setRationCardIssueDate(String rationCardIssueDate) {
        this.rationCardIssueDate = rationCardIssueDate;
    }

    public String getRationCardExpirationDate() {
        return rationCardExpirationDate;
    }

    public void setRationCardExpirationDate(String rationCardExpirationDate) {
        this.rationCardExpirationDate = rationCardExpirationDate;
    }

    public String getUniversalIdIssueDate() {
        return universalIdIssueDate;
    }

    public void setUniversalIdIssueDate(String universalIdIssueDate) {
        this.universalIdIssueDate = universalIdIssueDate;
    }

    public String getUniversalIdExpirationDate() {
        return universalIdExpirationDate;
    }

    public void setUniversalIdExpirationDate(String universalIdExpirationDate) {
        this.universalIdExpirationDate = universalIdExpirationDate;
    }

    public String getCustomerSegment() {
        return customerSegment;
    }

    public void setCustomerSegment(String customerSegment) {
        this.customerSegment = customerSegment;
    }

    public String getBorrowerFlag() {
        return borrowerFlag;
    }

    public void setBorrowerFlag(String borrowerFlag) {
        this.borrowerFlag = borrowerFlag;
    }

    public String getIndividualCorporateFlag() {
        return individualCorporateFlag;
    }

    public void setIndividualCorporateFlag(String individualCorporateFlag) {
        this.individualCorporateFlag = individualCorporateFlag;
    }

    public String getConstitution() {
        return constitution;
    }

    public void setConstitution(String constitution) {
        this.constitution = constitution;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getNumberCreditCards() {
        return numberCreditCards;
    }

    public void setNumberCreditCards(String numberCreditCards) {
        this.numberCreditCards = numberCreditCards;
    }

    public String getCreditCardNo() {
        return creditCardNo;
    }

    public void setCreditCardNo(String creditCardNo) {
        this.creditCardNo = creditCardNo;
    }

    public String getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(String monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getSoaEmployerNameC() {
        return soaEmployerNameC;
    }

    public void setSoaEmployerNameC(String soaEmployerNameC) {
        this.soaEmployerNameC = soaEmployerNameC;
    }

    public String getTimeWithEmploy() {
        return timeWithEmploy;
    }

    public void setTimeWithEmploy(String timeWithEmploy) {
        this.timeWithEmploy = timeWithEmploy;
    }

    public String getCompanyCategory() {
        return companyCategory;
    }

    public void setCompanyCategory(String companyCategory) {
        this.companyCategory = companyCategory;
    }

    public String getNatureOfBusiness() {
        return natureOfBusiness;
    }

    public void setNatureOfBusiness(String natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    public String getAssetCost() {
        return assetCost;
    }

    public void setAssetCost(String assetCost) {
        this.assetCost = assetCost;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCollateral1() {
        return collateral1;
    }

    public void setCollateral1(String collateral1) {
        this.collateral1 = collateral1;
    }

    public String getCollateral1Valuation1() {
        return collateral1Valuation1;
    }

    public void setCollateral1Valuation1(String collateral1Valuation1) {
        this.collateral1Valuation1 = collateral1Valuation1;
    }

    public String getCollateral1Valuation2() {
        return collateral1Valuation2;
    }

    public void setCollateral1Valuation2(String collateral1Valuation2) {
        this.collateral1Valuation2 = collateral1Valuation2;
    }

    public String getCollateral2() {
        return collateral2;
    }

    public void setCollateral2(String collateral2) {
        this.collateral2 = collateral2;
    }

    public String getCollateral2Valuation1() {
        return collateral2Valuation1;
    }

    public void setCollateral2Valuation1(String collateral2Valuation1) {
        this.collateral2Valuation1 = collateral2Valuation1;
    }

    public String getCollateral2Valuation2() {
        return collateral2Valuation2;
    }

    public void setCollateral2Valuation2(String collateral2Valuation2) {
        this.collateral2Valuation2 = collateral2Valuation2;
    }

    public String getApplicationScore() {
        return applicationScore;
    }

    public void setApplicationScore(String applicationScore) {
        this.applicationScore = applicationScore;
    }

    public String getDdeScore() {
        return ddeScore;
    }

    public void setDdeScore(String ddeScore) {
        this.ddeScore = ddeScore;
    }

    public String getDebitScore() {
        return debitScore;
    }

    public void setDebitScore(String debitScore) {
        this.debitScore = debitScore;
    }

    public String getBehaviourScore() {
        return behaviourScore;
    }

    public void setBehaviourScore(String behaviourScore) {
        this.behaviourScore = behaviourScore;
    }

    public boolean isAddress1ValidSwitch() {
        return address1ValidSwitch;
    }

    public void setAddress1ValidSwitch(boolean address1ValidSwitch) {
        this.address1ValidSwitch = address1ValidSwitch;
    }

    public boolean isAddress2ValidSwitch() {
        return address2ValidSwitch;
    }

    public void setAddress2ValidSwitch(boolean address2ValidSwitch) {
        this.address2ValidSwitch = address2ValidSwitch;
    }

    public boolean isAddress3ValidSwitch() {
        return address3ValidSwitch;
    }

    public void setAddress3ValidSwitch(boolean address3ValidSwitch) {
        this.address3ValidSwitch = address3ValidSwitch;
    }

    public boolean isPhone1ValidSwitch() {
        return phone1ValidSwitch;
    }

    public void setPhone1ValidSwitch(boolean phone1ValidSwitch) {
        this.phone1ValidSwitch = phone1ValidSwitch;
    }

    public boolean isPhone2ValidSwitch() {
        return phone2ValidSwitch;
    }

    public void setPhone2ValidSwitch(boolean phone2ValidSwitch) {
        this.phone2ValidSwitch = phone2ValidSwitch;
    }

    public boolean isPhone3ValidSwitch() {
        return phone3ValidSwitch;
    }

    public void setPhone3ValidSwitch(boolean phone3ValidSwitch) {
        this.phone3ValidSwitch = phone3ValidSwitch;
    }

    public boolean isPhone4ValidSwitch() {
        return phone4ValidSwitch;
    }

    public void setPhone4ValidSwitch(boolean phone4ValidSwitch) {
        this.phone4ValidSwitch = phone4ValidSwitch;
    }

    public boolean isPhone1ExtnValidSwitch() {
        return phone1ExtnValidSwitch;
    }

    public void setPhone1ExtnValidSwitch(boolean phone1ExtnValidSwitch) {
        this.phone1ExtnValidSwitch = phone1ExtnValidSwitch;
    }

    public boolean isPhone2ExtnValidSwitch() {
        return phone2ExtnValidSwitch;
    }

    public void setPhone2ExtnValidSwitch(boolean phone2ExtnValidSwitch) {
        this.phone2ExtnValidSwitch = phone2ExtnValidSwitch;
    }

    public boolean isPhone3ExtnValidSwitch() {
        return phone3ExtnValidSwitch;
    }

    public void setPhone3ExtnValidSwitch(boolean phone3ExtnValidSwitch) {
        this.phone3ExtnValidSwitch = phone3ExtnValidSwitch;
    }

    public boolean isPhone4ExtnValidSwitch() {
        return phone4ExtnValidSwitch;
    }

    public void setPhone4ExtnValidSwitch(boolean phone4ExtnValidSwitch) {
        this.phone4ExtnValidSwitch = phone4ExtnValidSwitch;
    }

    public boolean isId1ValidSwitch() {
        return id1ValidSwitch;
    }

    public void setId1ValidSwitch(boolean id1ValidSwitch) {
        this.id1ValidSwitch = id1ValidSwitch;
    }

    public boolean isId2ValidSwitch() {
        return id2ValidSwitch;
    }

    public void setId2ValidSwitch(boolean id2ValidSwitch) {
        this.id2ValidSwitch = id2ValidSwitch;
    }

    public boolean isRelation1ValidSwitch() {
        return relation1ValidSwitch;
    }

    public void setRelation1ValidSwitch(boolean relation1ValidSwitch) {
        this.relation1ValidSwitch = relation1ValidSwitch;
    }

    public boolean isRelation2ValidSwitch() {
        return relation2ValidSwitch;
    }

    public void setRelation2ValidSwitch(boolean relation2ValidSwitch) {
        this.relation2ValidSwitch = relation2ValidSwitch;
    }

    public boolean isNomineeValidSwitch() {
        return nomineeValidSwitch;
    }

    public void setNomineeValidSwitch(boolean nomineeValidSwitch) {
        this.nomineeValidSwitch = nomineeValidSwitch;
    }

    public boolean isKeyPersonValidSwitch() {
        return keyPersonValidSwitch;
    }

    public void setKeyPersonValidSwitch(boolean keyPersonValidSwitch) {
        this.keyPersonValidSwitch = keyPersonValidSwitch;
    }

    public void setDobSwitch(boolean dobSwitch) {
        this.dobValidSwitch = dobSwitch;
    }

    public boolean isAgeValidSwitch() {
        return ageValidSwitch;
    }

    public void setAgeValidSwitch(boolean ageSwitch) {
        this.ageValidSwitch = ageSwitch;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAgeAsOnDt() {
        return ageAsOnDt;
    }

    public void setAgeAsOnDt(String ageAsOnDt) {
        this.ageAsOnDt = ageAsOnDt;
    }

    public String getBirthDt() {
        return birthDt;
    }

    public void setBirthDt(String birthDt) {
        this.birthDt = birthDt;
    }

    public String getCredtRptTrnDtTm() {
        return credtRptTrnDtTm;
    }

    public void setCredtRptTrnDtTm(String credtRptTrnDtTm) {
        this.credtRptTrnDtTm = credtRptTrnDtTm;
    }

    public boolean isDobValidSwitch() {
        return dobValidSwitch;
    }

    public void setDobValidSwitch(boolean dobValidSwitch) {
        this.dobValidSwitch = dobValidSwitch;
    }

    public boolean isPanValidSwitch() {
        return panValidSwitch;
    }

    public void setPanValidSwitch(boolean panValidSwitch) {
        this.panValidSwitch = panValidSwitch;
    }

    public boolean isPassportValidSwitch() {
        return passportValidSwitch;
    }

    public void setPassportValidSwitch(boolean passportValidSwitch) {
        this.passportValidSwitch = passportValidSwitch;
    }

    public boolean isVoterValidSwitch() {
        return voterValidSwitch;
    }

    public void setVoterValidSwitch(boolean voterValidSwitch) {
        this.voterValidSwitch = voterValidSwitch;
    }

    public boolean isUidValidSwitch() {
        return uidValidSwitch;
    }

    public void setUidValidSwitch(boolean uidValidSwitch) {
        this.uidValidSwitch = uidValidSwitch;
    }

    public boolean isRationValidSwitch() {
        return rationValidSwitch;
    }

    public void setRationValidSwitch(boolean rationValidSwitch) {
        this.rationValidSwitch = rationValidSwitch;
    }

    public boolean isDrivingLicenseValidSwitch() {
        return drivingLicenseValidSwitch;
    }

    public void setDrivingLicenseValidSwitch(boolean drivingLicenseValidSwitch) {
        this.drivingLicenseValidSwitch = drivingLicenseValidSwitch;
    }

    public boolean isEmail1ValidSwitch() {
        return email1ValidSwitch;
    }

    public void setEmail1ValidSwitch(boolean email1ValidSwitch) {
        this.email1ValidSwitch = email1ValidSwitch;
    }

    public boolean isEmail2ValidSwitch() {
        return email2ValidSwitch;
    }

    public void setEmail2ValidSwitch(boolean email2ValidSwitch) {
        this.email2ValidSwitch = email2ValidSwitch;
    }

    public boolean isFatherNameValidSwitch() {
        return fatherNameValidSwitch;
    }

    public void setFatherNameValidSwitch(boolean fatherNameValdiSwitch) {
        this.fatherNameValidSwitch = fatherNameValdiSwitch;
    }

    public boolean isMotherNameValidSwitch() {
        return motherNameValidSwitch;
    }

    public void setMotherNameValidSwitch(boolean motherNameValdiSwitch) {
        this.motherNameValidSwitch = motherNameValdiSwitch;
    }

    public boolean isSpouseNameValidSwitch() {
        return spouseNameValidSwitch;
    }

    public void setSpouseNameValidSwitch(boolean spouseNameValdiSwitch) {
        this.spouseNameValidSwitch = spouseNameValdiSwitch;
    }

	/*public JobPriorityType getJobPriorityType() {
        if (jobPriorityType == null)
			return JobPriorityType.MEDIUM_PRIORITY;
		return jobPriorityType;
	}
	public void setJobPriorityType(JobPriorityType jobPriorityType_) {
		this.jobPriorityType = jobPriorityType_;
	}*/

    public String getNoOfDependents() {
        return noOfDependents;
    }

    public void setNoOfDependents(String noOfDependents) {
        this.noOfDependents = noOfDependents;
    }

    public Integer getLastRunRuleTreeNo() {
        return lastRunRuleTreeNo;
    }

    public void setLastRunRuleTreeNo(Integer lastRunRuleTreeNo) {
        this.lastRunRuleTreeNo = lastRunRuleTreeNo;
    }

    public Long getDecisionByNodeId() {
        return decisionByNodeId;
    }

    public void setDecisionByNodeId(Long decisionByNodeId) {
        this.decisionByNodeId = decisionByNodeId;
    }

    public Long getIpStgSrNo() {
        return ipStgSrNo;
    }

    public void setIpStgSrNo(Long ipStgSrNo) {
        this.ipStgSrNo = ipStgSrNo;
    }

    public Date getRequestReadDateTime() {
        return requestReadDateTime;
    }

    public void setRequestReadDateTime(Date requestReadDateTime) {
        this.requestReadDateTime = requestReadDateTime;
    }

    public String getDuns() {
        return duns;
    }

    public void setDuns(String duns) {
        this.duns = duns;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getCommercialIndr() {
        return commercialIndr;
    }

    public void setCommercialIndr(String commercialIndr) {
        this.commercialIndr = commercialIndr;
    }

    public String getBureauProduct() {
        return bureauProduct;
    }

    public void setBureauProduct(String bureauProduct) {
        this.bureauProduct = bureauProduct;
    }

    public String getAddress1Country() {
        return address1Country;
    }

    public void setAddress1Country(String address1Country) {
        this.address1Country = address1Country;
    }

    public String getAddress1District() {
        return address1District;
    }

    public void setAddress1District(String address1District) {
        this.address1District = address1District;
    }


    public Map<String, Object> getDomainAsMap() {

        Map<String, Object> value = new ConcurrentHashMap<String, Object>();

        value.put("INSTITUTION_ID", this.institutionId != null ? this.institutionId + "" : "");
        value.put("REQUEST_ID", this.requestId != null ? this.requestId + "" : "");
        value.put("FIRST_NAME", this.firstName != null ? this.firstName : "");
        value.put("MIDDLE_NAME", this.middleName != null ? this.middleName : "");
        value.put("LAST_NAME", this.lastName != null ? this.lastName : "");
        value.put("NAME4", this.name4 != null ? this.name4 : "");
        value.put("NAME5", this.name5 != null ? this.name5 : "");
        value.put("GENDER", this.gender != null ? this.gender : "");
        value.put("MARITAL_STATUS", this.maritalStatus != null ? this.maritalStatus : "");
        value.put("NO_OF_DEPENDENTS", this.noOfDependents != null ? this.noOfDependents : "");
        value.put("FATHER_NAME", this.fatherName != null ? this.fatherName : "");
        value.put("SPOUSE_NAME", this.spouseName != null ? this.spouseName : "");
        value.put("MOTHER_NAME", this.motherName != null ? this.motherName : "");
        value.put("RELATION_TYPE1", this.relationType1 != null ? this.relationType1 : "");
        value.put("RELATION_TYPE1_VALUE", this.relationType1Value != null ? this.relationType1Value : "");
        value.put("RELATION_TYPE2", this.relationType2 != null ? this.relationType2 : "");
        value.put("RELATION_TYPE2_VALUE", this.relationType2Value != null ? this.relationType2Value : "");
        value.put("KEY_PERSON_NAME", this.keyPersonName != null ? this.keyPersonName : "");
        value.put("KEY_PERSON_RELATION", this.keyPersonRelation != null ? this.keyPersonRelation : "");
        value.put("NOMINEE_NAME", this.nomineeName != null ? this.nomineeName : "");
        value.put("NOMINEE_RALATION_TYPE", this.nomineeRelationType != null ? this.nomineeRelationType : "");
        value.put("ADDRESS_1_TYPE", this.address1Type != null ? this.address1Type : "");
        value.put("ADDRESS_1", this.address1 != null ? this.address1 : "");
        value.put("ADDRESS_1_CITY", this.address1City != null ? this.address1City : "");
        value.put("ADDRESS_1_PIN", this.address1Pin != null ? this.address1Pin : "");
        value.put("ADDRESS_1_STATE", this.address1State != null ? this.address1State : "");
        value.put("ADDRESS_1_DISTRICT", this.address1District != null ? this.address1District + "" : "");
        value.put("ADDRESS_1_COUNTRY", this.address1Country != null ? this.address1Country + "" : "");
        value.put("ADDRESS1_RESIDENCE_CODE", this.address1ResidenceCode != null ? this.address1ResidenceCode : "");
        value.put("ADDRESS_2_TYPE", this.address2Type != null ? this.address2Type : "");
        value.put("ADDRESS_2", this.address2 != null ? this.address2 : "");
        value.put("ADDRESS_2_CITY", this.address2City != null ? this.address2City : "");
        value.put("ADDRESS_2_PIN", this.address2Pin != null ? this.address2Pin : "");
        value.put("ADDRESS_2_STATE", this.address2State != null ? this.address2State : "");
        value.put("ADDRESS2_RESIDENCE_CODE", this.address2ResidenceCode != null ? this.address2ResidenceCode : "");
        value.put("ADDRESS_3_TYPE", this.address3Type != null ? this.address3Type : "");
        value.put("ADDRESS_3", this.address3 != null ? this.address3 : "");
        value.put("ADDRESS_3_CITY", this.address3City != null ? this.address3City : "");
        value.put("ADDRESS_3_PIN", this.address3Pin != null ? this.address3Pin : "");
        value.put("ADDRESS_3_STATE", this.address3State != null ? this.address3State : "");
        value.put("ADDRESS3_RESIDENCE_CODE", this.address3ResidenceCode != null ? this.address3ResidenceCode : "");
        value.put("PAN_ID", this.panId != null ? this.panId : "");
        value.put("PASSPORT_ID", this.passportId != null ? this.passportId : "");
        value.put("VOTER_ID", this.voterId != null ? this.voterId : "");
        value.put("DRIVING_LICENSE_NO", this.drivingLicenseNo != null ? this.drivingLicenseNo : "");
        value.put("UID_NUMBER", this.uidNo != null ? this.uidNo : "");
        value.put("RATION_CARD", this.rationCard != null ? this.rationCard : "");
        value.put("ID_TYPE_1", this.idType1 != null ? this.idType1 : "");
        value.put("ID_TYPE_1_VALUE", this.idType1Value != null ? this.idType1Value : "");
        value.put("ID_TYPE_2", this.idType2 != null ? this.idType2 : "");
        value.put("ID_TYPE_2_VALUE", this.idType2Value != null ? this.idType2Value : "");
        value.put("PHONE_TYPE_1", this.phoneType1 != null ? this.phoneType1 : "");
        value.put("PHONE_TYPE_1_VALUE", this.phoneNumber1 != null ? this.phoneNumber1 : "");

        value.put("PHONE_EXTN_1", this.phoneExtn1 != null ? this.phoneExtn1 : "");
        value.put("PHONE_TYPE_2", this.phoneType2 != null ? this.phoneType2 : "");
        value.put("PHONE_TYPE_2_VALUE", this.phoneNumber2 != null ? this.phoneNumber2 : "");
        value.put("PHONE_EXTN_2", this.phoneExtn2 != null ? this.phoneExtn2 : "");
        value.put("PHONE_TYPE_3", this.phoneType3 != null ? this.phoneType3 : "");
        value.put("PHONE_TYPE_3_VALUE", this.phoneNumber3 != null ? this.phoneNumber3 : "");
        value.put("PHONE_EXTN_3", this.phoneExtn3 != null ? this.phoneExtn3 : "");
        value.put("PHONE_TYPE_4", this.phoneType4 != null ? this.phoneType4 : "");
        value.put("PHONE_TYPE_4_VALUE", this.phoneNumber4 != null ? this.phoneNumber4 : "");
        value.put("PHONE_EXTN_4", this.phoneExtn4 != null ? this.phoneExtn4 : "");
        value.put("EMAIL_ID_1", this.emailId1 != null ? this.emailId1 : "");
        value.put("EMAIL_ID_2", this.emailId2 != null ? this.emailId2 : "");
        value.put("AKA", this.alias != null ? this.alias : "");
        value.put("LOAN_TYPE", this.loanType != null ? this.loanType : "");
        value.put("LOAN_AMOUNT", this.loanAmount != null ? this.loanAmount : "");
        value.put("CUSTOMER_ID", this.consumerId != null ? this.consumerId : "");
        value.put("REQUEST_TYPE", this.requestType != null ? this.requestType : "");
        value.put("APPLICATION_ID", this.applicationId != null ? this.applicationId : "");
        value.put("USER", this.inquirySubmittedBy != null ? this.inquirySubmittedBy : "");
        value.put("SOURCE_SYSTEM_NAME", this.sourceSystemName != null ? this.sourceSystemName : "");
        value.put("SOURCE_SYSTEM_VERSION", this.sourceSystemVersion != null ? this.sourceSystemVersion : "");
        value.put("SOURCE_SYSTEM_VENDER", this.sourceSystemVender != null ? this.sourceSystemVender : "");
        value.put("FINANCIAL_PURPOSE", this.loanPurposeDesc != null ? this.loanPurposeDesc : "");
        value.put("BRANCH_IFSC_CODE", this.branchIfsccode != null ? this.branchIfsccode : "");
        value.put("KENDRA", this.kendra != null ? this.kendra : "");
        value.put("BATCH_ID", this.batchId != null ? this.batchId + "" : "");
        value.put("INSERT_DATE", this.insertDt != null ? this.insertDt + "" : "");
        value.put("INQUIRY_STAGE", this.inquiryStage != null ? this.inquiryStage : "");
        value.put("ACTION_TYPE", this.actionType != null ? this.actionType : "");
        value.put("AUTHRIZATION_FLAG", this.authrizationFlag != null ? this.authrizationFlag : "");
        value.put("AUTHROIZED_BY", this.authroizedBy != null ? this.authroizedBy : "");
        value.put("RESPONSE_FORMAT", this.responseFormat != null ? this.responseFormat : "");
        value.put("MBR_RFNCE_OVERRIDE_FLAG", this.mbrRfnceOverrideFlag != null ? this.mbrRfnceOverrideFlag : "");
        value.put("EMBED_RESPONSE_FLAG", this.embedResponseFlag != null ? this.embedResponseFlag : "");
        value.put("TEST_FLAG", this.testFlag != null ? this.testFlag : "");
        value.put("TU_FLAG", this.tuFlag != null ? this.tuFlag : "");
        value.put("HM_REF_NO", this.unqRefNo != null ? this.unqRefNo : "");
        value.put("CREDT_RPT_TRN_ID", this.credtRptTrnId != null ? this.credtRptTrnId : "");
        value.put("CREDT_INQ_PURPS_TYP_DESC", this.credtInqPurpsTypDesc != null ? this.credtInqPurpsTypDesc : "");
        value.put("HM_STATUS", this.hmStatus != null ? this.hmStatus : "");
        value.put("CIBIL_STATUS", this.cibilStatus != null ? this.cibilStatus : "");
        value.put("EQUIFAX_STATUS", this.equifaxStatus != null ? this.equifaxStatus : "");
        value.put("EXPERIAN_STATUS", this.experianStatus != null ? this.experianStatus : "");
        value.put("SOURCE_SYSTEM_INSTANCE_ID", this.sourceSystemInstanceId != null ? this.sourceSystemInstanceId : "");
        value.put("BUREAU_REGION", this.bureauRegion != null ? this.bureauRegion : "");
        value.put("INQUIRY_SUBMITTED_BY", this.inquirySubmittedBy != null ? this.inquirySubmittedBy : "");
        value.put("ACCOUNT_NUMBER1", this.accountNumber1 != null ? this.accountNumber1 : "");
        value.put("ACCOUNT_NUMBER2", this.accountNumber2 != null ? this.accountNumber2 : "");
        value.put("ACCOUNT_NUMBER3", this.accountNumber3 != null ? this.accountNumber3 : "");
        value.put("ACCOUNT_NUMBER4", this.accountNumber4 != null ? this.accountNumber4 : "");
        value.put("IP_STAGING_INSERT_DT", this.ipStagingInsertDt != null ? this.ipStagingInsertDt + "" : "");
        value.put("SOURCE_SYSTEM_GRP_CODE", this.sourceSystemGroupCode != null ? this.sourceSystemGroupCode : "");
        value.put("SOA_FILE_NAME_C", this.soaFileNameC != null ? this.soaFileNameC : "");
        value.put("TENURE", this.tenure != null ? this.tenure : "");
        value.put("INTERNAL_EXTERNAL_FLAG", this.internalExternalFlag != null ? this.internalExternalFlag : "");
        value.put("SOA_INSTA_LOAN_FLAG_C", this.soaInstaLoanFlagC != null ? this.soaInstaLoanFlagC : "");
        value.put("FIRST_SOURCE", this.firstSource != null ? this.firstSource : "");
        value.put("SOA_SOURCE_BROKERID_N", this.soaSourceBrokerIdN != null ? this.soaSourceBrokerIdN : "");
        value.put("POLICY_SEGMENT", this.policySegment != null ? this.policySegment : "");
        value.put("SOA_PROMOTION_C", this.soaPromotionC != null ? this.soaPromotionC : "");
        value.put("REGION_CODE", this.regionCode != null ? this.regionCode : "");
        value.put("LSI_LOS_NO", this.lsiLosNo != null ? this.lsiLosNo : "");
        value.put("LSI_FINNONE_LAN_NO", this.lsiFinnoneLanNo != null ? this.lsiFinnoneLanNo : "");
        value.put("TOKEN_NUMBER_1", this.tokenNumber1 != null ? this.tokenNumber1 : "");
        value.put("TOKEN_NUMBER_2", this.tokenNumber2 != null ? this.tokenNumber2 : "");
        value.put("TOKEN_NUMBER_3", this.tokenNumber3 != null ? this.tokenNumber3 : "");
        value.put("LOAN_ACCOUNT_NUMBER_1", this.loanAccountNumber1 != null ? this.loanAccountNumber1 : "");
        value.put("LOAN_ACCOUNT_NUMBER_2", this.loanAccountNumber2 != null ? this.loanAccountNumber2 : "");
        value.put("LOAN_ACCOUNT_NUMBER_3", this.loanAccountNumber3 != null ? this.loanAccountNumber3 : "");
        value.put("AGE", this.age != null ? this.age : "");
        value.put("CUSTOMER_SEGMENT", this.customerSegment != null ? this.customerSegment : "");
        value.put("BORROWER_FLAG", this.borrowerFlag != null ? this.borrowerFlag : "");
        value.put("INDIVIDUAL_CORPORATE_FLAG", this.individualCorporateFlag != null ? this.individualCorporateFlag : "");
        value.put("CONSTITUTION", this.constitution != null ? this.constitution : "");
        value.put("GROUP_ID", this.groupId != null ? this.groupId : "");
        value.put("NUMBER_CREDIT_CARDS", this.numberCreditCards != null ? this.numberCreditCards : "");
        value.put("CREDIT_CARD_NO", this.creditCardNo != null ? this.creditCardNo : "");
        value.put("MONTHLY_INCOME", this.monthlyIncome != null ? this.monthlyIncome : "");
        value.put("SOA_EMPLOYER_NAME_C", this.soaEmployerNameC != null ? this.soaEmployerNameC : "");
        value.put("TIMEWITHEMPLOY", this.timeWithEmploy != null ? this.timeWithEmploy : "");
        value.put("COMPANY_CATEGORY", this.companyCategory != null ? this.companyCategory : "");
        value.put("NATURE_OF_BUSINESS", this.natureOfBusiness != null ? this.natureOfBusiness : "");
        value.put("ASSET_COST", this.assetCost != null ? this.assetCost : "");
        value.put("LOGO", this.logo != null ? this.logo : "");
        value.put("COLLATERAL1", this.collateral1 != null ? this.collateral1 : "");
        value.put("COLLATERAL1_VALUATION_1", this.collateral1Valuation1 != null ? this.collateral1Valuation1 : "");
        value.put("COLLATERAL1_VALUATION_2", this.collateral1Valuation2 != null ? this.collateral1Valuation2 : "");
        value.put("COLLATERAL2", this.collateral2 != null ? this.collateral2 : "");
        value.put("COLLATERAL2_VALUATION_1", this.collateral2Valuation1 != null ? this.collateral2Valuation1 : "");
        value.put("COLLATERAL2_VALUATION_2", this.collateral2Valuation2 != null ? this.collateral2Valuation2 : "");
        value.put("APPLICATION_SCORE", this.applicationScore != null ? this.applicationScore : "");
        value.put("DDE_SCORE", this.ddeScore != null ? this.ddeScore : "");
        value.put("DEBIT_SCORE", this.debitScore != null ? this.debitScore : "");
        value.put("BEHAVIOR_SCORE", this.behaviourScore != null ? this.behaviourScore : "");
        //value.put("JOB_PRIORITY_TYPE", this.jobPriorityType!=null? this.jobPriorityType+"":"");
        value.put("FULL_NAME", getFullName() != null ? getFullName() : "");
        value.put("TENURE", this.tenure != null ? this.tenure : "");
        value.put("NO-OF-DEPENDENTS", this.noOfDependents != null ? this.noOfDependents : "");

        value.put("FILLER01", this.parameter1 != null ? this.parameter1 + "" : "");
        value.put("FILLER02", this.parameter2 != null ? this.parameter2 + "" : "");
        value.put("FILLER03", this.parameter3 != null ? this.parameter3 + "" : "");
        value.put("FILLER04", this.parameter4 != null ? this.parameter4 + "" : "");
        value.put("FILLER05", this.parameter5 != null ? this.parameter5 + "" : "");
        value.put("FILLER06", this.parameter6 != null ? this.parameter6 + "" : "");
        value.put("FILLER07", this.parameter7 != null ? this.parameter7 + "" : "");
        value.put("FILLER08", this.parameter8 != null ? this.parameter8 + "" : "");
        value.put("FILLER09", this.parameter9 != null ? this.parameter9 + "" : "");
        value.put("FILLER10", this.parameter10 != null ? this.parameter10 + "" : "");
        value.put("FILLER11", this.parameter11 != null ? this.parameter11 + "" : "");
        value.put("FILLER12", this.parameter12 != null ? this.parameter12 + "" : "");
        value.put("FILLER13", this.parameter13 != null ? this.parameter13 + "" : "");
        value.put("FILLER14", this.parameter14 != null ? this.parameter14 + "" : "");
        value.put("FILLER15", this.parameter15 != null ? this.parameter15 + "" : "");
        value.put("FILLER16", this.parameter16 != null ? this.parameter16 + "" : "");
        value.put("FILLER17", this.parameter17 != null ? this.parameter17 + "" : "");
        value.put("FILLER18", this.parameter18 != null ? this.parameter18 + "" : "");
        value.put("FILLER19", this.parameter19 != null ? this.parameter19 + "" : "");
        value.put("FILLER20", this.parameter20 != null ? this.parameter20 + "" : "");
        value.put("FILLER21", this.parameter21 != null ? this.parameter21 + "" : "");
        value.put("FILLER22", this.parameter22 != null ? this.parameter22 + "" : "");
        value.put("FILLER23", this.parameter23 != null ? this.parameter23 + "" : "");
        value.put("FILLER24", this.parameter24 != null ? this.parameter24 + "" : "");
        value.put("FILLER25", this.parameter25 != null ? this.parameter25 + "" : "");
        value.put("FILLER26", this.parameter26 != null ? this.parameter26 + "" : "");
        value.put("FILLER27", this.parameter27 != null ? this.parameter27 + "" : "");
        value.put("FILLER28", this.parameter28 != null ? this.parameter28 + "" : "");
        value.put("FILLER29", this.parameter29 != null ? this.parameter29 + "" : "");
        value.put("FILLER30", this.parameter30 != null ? this.parameter30 + "" : "");
        value.put("FILLER31", this.parameter31 != null ? this.parameter31 + "" : "");
        value.put("FILLER32", this.parameter32 != null ? this.parameter32 + "" : "");
        value.put("FILLER33", this.parameter33 != null ? this.parameter33 + "" : "");
        value.put("FILLER34", this.parameter34 != null ? this.parameter34 + "" : "");
        value.put("FILLER35", this.parameter35 != null ? this.parameter35 + "" : "");
        value.put("FILLER36", this.parameter36 != null ? this.parameter36 + "" : "");
        value.put("FILLER37", this.parameter37 != null ? this.parameter37 + "" : "");
        value.put("FILLER38", this.parameter38 != null ? this.parameter38 + "" : "");
        value.put("FILLER39", this.parameter39 != null ? this.parameter39 + "" : "");
        value.put("FILLER40", this.parameter40 != null ? this.parameter40 + "" : "");
        value.put("FILLER41", this.parameter41 != null ? this.parameter41 + "" : "");
        value.put("FILLER42", this.parameter42 != null ? this.parameter42 + "" : "");
        value.put("FILLER43", this.parameter43 != null ? this.parameter43 + "" : "");
        value.put("FILLER44", this.parameter44 != null ? this.parameter44 + "" : "");
        value.put("FILLER45", this.parameter45 != null ? this.parameter45 + "" : "");
        value.put("FILLER46", this.parameter46 != null ? this.parameter46 + "" : "");
        value.put("FILLER47", this.parameter47 != null ? this.parameter47 + "" : "");
        value.put("FILLER48", this.parameter48 != null ? this.parameter48 + "" : "");
        value.put("FILLER49", this.parameter49 != null ? this.parameter49 + "" : "");
        value.put("FILLER50", this.parameter50 != null ? this.parameter50 + "" : "");


        // Date Fields
        value.put("AGE-AS-ON-DT", this.ageAsOnDt != null ? this.ageAsOnDt + "" : "");
        value.put("ACT-OPENING-DT", this.actOpeningDt != null ? this.actOpeningDt + "" : "");
        value.put("PAN-ISSUE-DATE", this.panIssueDate != null ? this.panIssueDate + "" : "");
        value.put("PAN-EXPIRATION-DATE", this.panExpirationDate != null ? this.panExpirationDate + "" : "");
        value.put("PASSPORT-ISSUE-DATE", this.passportIssueDate != null ? this.passportIssueDate + "" : "");
        value.put("PASSPORT-EXPIRATION-DATE", this.passportExpirationDate != null ? this.passportExpirationDate + "" : "");
        value.put("VOTER-ID-ISSUE-DATE", this.voterIdIssueDate != null ? this.voterIdIssueDate + "" : "");
        value.put("VOTER-ID-EXPIRATION-DATE", this.voterIdExpirationDate != null ? this.voterIdExpirationDate + "" : "");
        value.put("DRIVER-LICENSE-ISSUE-DATE", this.driverLicenseIssueDate != null ? this.driverLicenseIssueDate + "" : "");
        value.put("DRIVER-LICENSE-EXPIRATION-DATE", this.driverLicenseExpirationDate != null ? this.driverLicenseExpirationDate + "" : "");
        value.put("UNIVERSAL-ID-ISSUE-DATE", this.universalIdIssueDate != null ? this.universalIdIssueDate + "" : "");
        value.put("UNIVERSAL-ID-EXPIRATION-DATE", this.universalIdExpirationDate != null ? this.universalIdExpirationDate + "" : "");
        value.put("RATION-CARD-ISSUE-DATE", this.rationCardIssueDate != null ? this.rationCardIssueDate + "" : "");
        value.put("RATION-CARD-EXPIRATION-DATE", this.rationCardExpirationDate != null ? this.rationCardExpirationDate + "" : "");

        //value.put("INDIVIDUAL_ENTITIES_SEGMENT", this.individualEntitiesSegment!=null? this.individualEntitiesSegment+"":"");
        //value.put("ORGANISATION_ENTITIES_SEGMENT", this.organisationEntitiesSegment!=null? this.organisationEntitiesSegment+"":"");

        value.put("BIRTH_DATE", this.birthDt == null ? "" : this.birthDt);
        value.put("SOURCE_SYSTEM_INST_ID", this.sourceSystemInstanceId != null ? this.sourceSystemInstanceId : "");
        value.put("REQUEST_TIME", this.requestTime != null ? this.requestTime : "");
/*		String instDtFormat = null;

		try {
			instDtFormat = LookUpService.getConfiguration().get(this.institutionId).get(CacheConstants.ScreenConfiguration.INSTITUTION_DATE_FORMAT);
		} catch (DataException e) {
			e.printStackTrace();
			logger_.error("** "+e.getMessage(), e);
		} catch (SystemException e) {
			e.printStackTrace();
			logger_.error("** "+e.getMessage(), e);
		} catch (ApplicationResourceNotFound e) {
			e.printStackTrace();
			logger_.error("** "+e.getMessage(), e);
		}*/
		
		/*if (instDtFormat != null) {
			value.put("PAN_ISSUE_DATE", DateUtils.changeFormat(this.panIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.panIssueDate==null?"":this.panIssueDate):DateUtils.changeFormat(this.panIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("PAN_EXPIRATION_DATE", DateUtils.changeFormat(this.panExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.panExpirationDate==null?"":this.panExpirationDate):DateUtils.changeFormat(this.panExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("PASSPORT_ISSUE_DATE", DateUtils.changeFormat(this.passportIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.passportIssueDate==null?"":this.passportIssueDate):DateUtils.changeFormat(this.passportIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("PASSPORT_EXPIRATION_DATE", DateUtils.changeFormat(this.passportExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.passportExpirationDate==null?"":this.passportExpirationDate):DateUtils.changeFormat(this.passportExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("VOTER_ID_ISSUE_DATE", DateUtils.changeFormat(this.voterIdIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.voterIdIssueDate==null?"":this.voterIdIssueDate):DateUtils.changeFormat(this.voterIdIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("VOTER_ID_EXPIRATION_DATE", DateUtils.changeFormat(this.voterIdExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.voterIdExpirationDate==null?"":this.voterIdExpirationDate):DateUtils.changeFormat(this.voterIdExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("DRIVER_LICENSE_ISSUE_DATE", DateUtils.changeFormat(this.driverLicenseIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.driverLicenseIssueDate==null?"":this.driverLicenseIssueDate):DateUtils.changeFormat(this.driverLicenseIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("DRIVER_LICENSE_EXPIRATION_DATE", DateUtils.changeFormat(this.driverLicenseExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.driverLicenseExpirationDate==null?"":this.driverLicenseExpirationDate):DateUtils.changeFormat(this.driverLicenseExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("RATION_CARD_ISSUE_DATE", DateUtils.changeFormat(this.rationCardIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.rationCardIssueDate==null?"":this.rationCardIssueDate):DateUtils.changeFormat(this.rationCardIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("RATION_CARD_EXPIRATION_DATE", DateUtils.changeFormat(this.rationCardExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.rationCardExpirationDate==null?"":this.rationCardExpirationDate):DateUtils.changeFormat(this.rationCardExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("UNIVERSAL_ID_ISSUE_DATE", DateUtils.changeFormat(this.universalIdIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.universalIdIssueDate==null?"":this.universalIdIssueDate):DateUtils.changeFormat(this.universalIdIssueDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("UNIVERSAL_ID_EXPIRATION_DATE", DateUtils.changeFormat(this.universalIdExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.universalIdExpirationDate==null?"":this.universalIdExpirationDate):DateUtils.changeFormat(this.universalIdExpirationDate, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("AGE_AS_ON", DateUtils.changeFormat(this.ageAsOnDt, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.ageAsOnDt==null?"":this.ageAsOnDt):DateUtils.changeFormat(this.ageAsOnDt, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
			value.put("BIRTH_DATE", DateUtils.changeFormat(this.birthDt, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT)==null?(this.birthDt==null?"":this.birthDt):DateUtils.changeFormat(this.birthDt, instDtFormat, GenConsts.CALC_FIELDS.MASTER_RANGE_DT_FORMAT));
		}*/

        value.put("IP_STG_SRNO", getIpStgSrNo() != null ? getIpStgSrNo() + "" : "");
        value.put("DUNS", getDuns() != null ? getDuns() : "");
        value.put("TIN", getTin() != null ? getTin() : "");
        value.put("REG_NO", getRegNo() != null ? getRegNo() : "");
        value.put("BUREAU_PRODUCT", getBureauProduct() != null ? getBureauProduct() : "");

        value.put("FILLER01", this.parameter1 != null ? this.parameter1 + "" : "");
        value.put("FILLER02", this.parameter2 != null ? this.parameter2 + "" : "");
        value.put("FILLER03", this.parameter3 != null ? this.parameter3 + "" : "");
        value.put("FILLER04", this.parameter4 != null ? this.parameter4 + "" : "");
        value.put("FILLER05", this.parameter5 != null ? this.parameter5 + "" : "");
        value.put("FILLER06", this.parameter6 != null ? this.parameter6 + "" : "");
        value.put("FILLER07", this.parameter7 != null ? this.parameter7 + "" : "");
        value.put("FILLER08", this.parameter8 != null ? this.parameter8 + "" : "");
        value.put("FILLER09", this.parameter9 != null ? this.parameter9 + "" : "");
        value.put("FILLER10", this.parameter10 != null ? this.parameter10 + "" : "");
        value.put("FILLER11", this.parameter11 != null ? this.parameter11 + "" : "");
        value.put("FILLER12", this.parameter12 != null ? this.parameter12 + "" : "");
        value.put("FILLER13", this.parameter13 != null ? this.parameter13 + "" : "");
        value.put("FILLER14", this.parameter14 != null ? this.parameter14 + "" : "");
        value.put("FILLER15", this.parameter15 != null ? this.parameter15 + "" : "");
        value.put("FILLER16", this.parameter16 != null ? this.parameter16 + "" : "");
        value.put("FILLER17", this.parameter17 != null ? this.parameter17 + "" : "");
        value.put("FILLER18", this.parameter18 != null ? this.parameter18 + "" : "");
        value.put("FILLER19", this.parameter19 != null ? this.parameter19 + "" : "");
        value.put("FILLER20", this.parameter20 != null ? this.parameter20 + "" : "");
        value.put("FILLER21", this.parameter21 != null ? this.parameter21 + "" : "");
        value.put("FILLER22", this.parameter22 != null ? this.parameter22 + "" : "");
        value.put("FILLER23", this.parameter23 != null ? this.parameter23 + "" : "");
        value.put("FILLER24", this.parameter24 != null ? this.parameter24 + "" : "");
        value.put("FILLER25", this.parameter25 != null ? this.parameter25 + "" : "");
        value.put("FILLER26", this.parameter26 != null ? this.parameter26 + "" : "");
        value.put("FILLER27", this.parameter27 != null ? this.parameter27 + "" : "");
        value.put("FILLER28", this.parameter28 != null ? this.parameter28 + "" : "");
        value.put("FILLER29", this.parameter29 != null ? this.parameter29 + "" : "");
        value.put("FILLER30", this.parameter30 != null ? this.parameter30 + "" : "");
        value.put("FILLER31", this.parameter31 != null ? this.parameter31 + "" : "");
        value.put("FILLER32", this.parameter32 != null ? this.parameter32 + "" : "");
        value.put("FILLER33", this.parameter33 != null ? this.parameter33 + "" : "");
        value.put("FILLER34", this.parameter34 != null ? this.parameter34 + "" : "");
        value.put("FILLER35", this.parameter35 != null ? this.parameter35 + "" : "");
        value.put("FILLER36", this.parameter36 != null ? this.parameter36 + "" : "");
        value.put("FILLER37", this.parameter37 != null ? this.parameter37 + "" : "");
        value.put("FILLER38", this.parameter38 != null ? this.parameter38 + "" : "");
        value.put("FILLER39", this.parameter39 != null ? this.parameter39 + "" : "");
        value.put("FILLER40", this.parameter40 != null ? this.parameter40 + "" : "");
        value.put("FILLER41", this.parameter41 != null ? this.parameter41 + "" : "");
        value.put("FILLER42", this.parameter42 != null ? this.parameter42 + "" : "");
        value.put("FILLER43", this.parameter43 != null ? this.parameter43 + "" : "");
        value.put("FILLER44", this.parameter44 != null ? this.parameter44 + "" : "");
        value.put("FILLER45", this.parameter45 != null ? this.parameter45 + "" : "");
        value.put("FILLER46", this.parameter46 != null ? this.parameter46 + "" : "");
        value.put("FILLER47", this.parameter47 != null ? this.parameter47 + "" : "");
        value.put("FILLER48", this.parameter48 != null ? this.parameter48 + "" : "");
        value.put("FILLER49", this.parameter49 != null ? this.parameter49 + "" : "");
        value.put("FILLER50", this.parameter50 != null ? this.parameter50 + "" : "");
        value.put("RAW_REQUEST", this.rawRequest != null ? this.rawRequest + "" : "");
        return value;
    }

    public String getParameter10() {
        return parameter10;
    }

    public void setParameter10(String parameter10) {
        this.parameter10 = parameter10;
    }

    public String getParameter11() {
        return parameter11;
    }

    public void setParameter11(String parameter11) {
        this.parameter11 = parameter11;
    }

    public String getParameter12() {
        return parameter12;
    }

    public void setParameter12(String parameter12) {
        this.parameter12 = parameter12;
    }

    public String getParameter13() {
        return parameter13;
    }

    public void setParameter13(String parameter13) {
        this.parameter13 = parameter13;
    }

    public String getParameter14() {
        return parameter14;
    }

    public void setParameter14(String parameter14) {
        this.parameter14 = parameter14;
    }

    public String getParameter15() {
        return parameter15;
    }

    public void setParameter15(String parameter15) {
        this.parameter15 = parameter15;
    }

    public String getParameter16() {
        return parameter16;
    }

    public void setParameter16(String parameter16) {
        this.parameter16 = parameter16;
    }

    public String getParameter17() {
        return parameter17;
    }

    public void setParameter17(String parameter17) {
        this.parameter17 = parameter17;
    }

    public String getParameter18() {
        return parameter18;
    }

    public void setParameter18(String parameter18) {
        this.parameter18 = parameter18;
    }

    public String getParameter19() {
        return parameter19;
    }

    public void setParameter19(String parameter19) {
        this.parameter19 = parameter19;
    }

    public String getParameter20() {
        return parameter20;
    }

    public void setParameter20(String parameter20) {
        this.parameter20 = parameter20;
    }

    public String getParameter21() {
        return parameter21;
    }

    public void setParameter21(String parameter21) {
        this.parameter21 = parameter21;
    }

    public String getParameter22() {
        return parameter22;
    }

    public void setParameter22(String parameter22) {
        this.parameter22 = parameter22;
    }

    public String getParameter23() {
        return parameter23;
    }

    public void setParameter23(String parameter23) {
        this.parameter23 = parameter23;
    }

    public String getParameter24() {
        return parameter24;
    }

    public void setParameter24(String parameter24) {
        this.parameter24 = parameter24;
    }

    public String getParameter25() {
        return parameter25;
    }

    public void setParameter25(String parameter25) {
        this.parameter25 = parameter25;
    }

    public String getParameter26() {
        return parameter26;
    }

    public void setParameter26(String parameter26) {
        this.parameter26 = parameter26;
    }

    public String getParameter27() {
        return parameter27;
    }

    public void setParameter27(String parameter27) {
        this.parameter27 = parameter27;
    }

    public String getParameter28() {
        return parameter28;
    }

    public void setParameter28(String parameter28) {
        this.parameter28 = parameter28;
    }

    public String getParameter29() {
        return parameter29;
    }

    public void setParameter29(String parameter29) {
        this.parameter29 = parameter29;
    }

    public String getParameter30() {
        return parameter30;
    }

    public void setParameter30(String parameter30) {
        this.parameter30 = parameter30;
    }

    public String getParameter31() {
        return parameter31;
    }

    public void setParameter31(String parameter31) {
        this.parameter31 = parameter31;
    }

    public String getParameter32() {
        return parameter32;
    }

    public void setParameter32(String parameter32) {
        this.parameter32 = parameter32;
    }

    public String getParameter33() {
        return parameter33;
    }

    public void setParameter33(String parameter33) {
        this.parameter33 = parameter33;
    }

    public String getParameter34() {
        return parameter34;
    }

    public void setParameter34(String parameter34) {
        this.parameter34 = parameter34;
    }

    public String getParameter35() {
        return parameter35;
    }

    public void setParameter35(String parameter35) {
        this.parameter35 = parameter35;
    }

    public String getParameter36() {
        return parameter36;
    }

    public void setParameter36(String parameter36) {
        this.parameter36 = parameter36;
    }

    public String getParameter37() {
        return parameter37;
    }

    public void setParameter37(String parameter37) {
        this.parameter37 = parameter37;
    }

    public String getParameter38() {
        return parameter38;
    }

    public void setParameter38(String parameter38) {
        this.parameter38 = parameter38;
    }

    public String getParameter39() {
        return parameter39;
    }

    public void setParameter39(String parameter39) {
        this.parameter39 = parameter39;
    }

    public String getParameter40() {
        return parameter40;
    }

    public void setParameter40(String parameter40) {
        this.parameter40 = parameter40;
    }

    public String getParameter41() {
        return parameter41;
    }

    public void setParameter41(String parameter41) {
        this.parameter41 = parameter41;
    }

    public String getParameter42() {
        return parameter42;
    }

    public void setParameter42(String parameter42) {
        this.parameter42 = parameter42;
    }

    public String getParameter43() {
        return parameter43;
    }

    public void setParameter43(String parameter43) {
        this.parameter43 = parameter43;
    }

    public String getParameter44() {
        return parameter44;
    }

    public void setParameter44(String parameter44) {
        this.parameter44 = parameter44;
    }

    public String getParameter45() {
        return parameter45;
    }

    public void setParameter45(String parameter45) {
        this.parameter45 = parameter45;
    }

    public String getParameter46() {
        return parameter46;
    }

    public void setParameter46(String parameter46) {
        this.parameter46 = parameter46;
    }

    public String getParameter47() {
        return parameter47;
    }

    public void setParameter47(String parameter47) {
        this.parameter47 = parameter47;
    }

    public String getParameter48() {
        return parameter48;
    }

    public void setParameter48(String parameter48) {
        this.parameter48 = parameter48;
    }

    public String getParameter49() {
        return parameter49;
    }

    public void setParameter49(String parameter49) {
        this.parameter49 = parameter49;
    }

    public String getParameter50() {
        return parameter50;
    }

    public void setParameter50(String parameter50) {
        this.parameter50 = parameter50;
    }


    public String getDefaultConfig() {
        return defaultConfig;
    }

    public void setDefaultConfig(String defaultConfig) {
        this.defaultConfig = defaultConfig;
    }

    public String getCibProduct() {
        return cibProduct;
    }

    public void setCibProduct(String cibProduct) {
        this.cibProduct = cibProduct;
    }

    public String getChmProduct() {
        return chmProduct;
    }

    public void setChmProduct(String chmProduct) {
        this.chmProduct = chmProduct;
    }

    public String getEqfProduct() {
        return eqfProduct;
    }

    public void setEqfProduct(String eqfProduct) {
        this.eqfProduct = eqfProduct;
    }

    public String getExpProduct() {
        return expProduct;
    }

    public void setExpProduct(String expProduct) {
        this.expProduct = expProduct;
    }

    public Long getCommRefId() {
        return commRefId;
    }

    public void setCommRefId(Long commRefId) {
        this.commRefId = commRefId;
    }


    public boolean isSyncMode() {
        return syncMode;
    }

    public void setSyncMode(boolean syncMode) {
        this.syncMode = syncMode;
    }

	/*public SerializedJSONArray getIssueFormat() {
		
		if (issueFormat == null || issueFormat.length() == 0) {
			
			if (StringUtils.isNotBlank(responseFormat)) {
			
				try {
					issueFormat =  new SerializedJSONArray(responseFormat);
					
					return issueFormat;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return issueFormat;
	}

	public void setIssueFormat(SerializedJSONArray issueFormat) {
		this.issueFormat = issueFormat;
	}*/

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public boolean isScoringFlag() {
        return scoringFlag;
    }

    public void setScoringFlag(boolean scoringFlag) {
        this.scoringFlag = scoringFlag;
    }

    public String getRawRequest() {
        return rawRequest;
    }

    public void setRawRequest(String rawRequest) {
        this.rawRequest = rawRequest;
    }

}
