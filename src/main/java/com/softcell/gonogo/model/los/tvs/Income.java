package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Income {

	 private String predictedIncomeConfidenceLevel;

	    private String incomePredictiondataSufficiencyFlag;

	    private String predictedIncome;

	    private SalaryCredit salaryCredit;


}
