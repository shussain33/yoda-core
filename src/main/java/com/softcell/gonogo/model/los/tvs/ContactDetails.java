package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContactDetails {

		private String landline;

	    private String mobilenumber;

	    private int stdcode;

	    private String officenumber;

	    private String lpgmobilenumber;


}
