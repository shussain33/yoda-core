package com.softcell.gonogo.model.los.tvs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TVSGroupThreeRequest {

    @JsonProperty("name")
    private String name;

    @JsonProperty("key")
    private String key;

    @JsonProperty("data")
    InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordForGroupThree;
}
