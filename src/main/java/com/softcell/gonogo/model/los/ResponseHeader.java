package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseHeader implements Serializable {

    /**
     * @author AshwiniP
     */
    private static final long serialVersionUID = 1L;

    @Expose
    @SerializedName("APPLICATION-ID")
    private String applicationId;
    @Expose
    @SerializedName("CUST-ID")
    private String custId;
    @Expose
    @SerializedName("RESPONSE-TYPE")
    private String responseType;
    @Expose
    @SerializedName("REQUEST-RECEIVED-TIME")
    private String reqRcvdTime;
    @Expose
    @SerializedName("SOURCE-SYSTEM")
    private String sourceSystem;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getReqRcvdTime() {
        return reqRcvdTime;
    }

    public void setReqRcvdTime(String reqRcvdTime) {
        this.reqRcvdTime = reqRcvdTime;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @Override
    public String toString() {
        return "ResponseHeader [applicationId=" + applicationId + ", custId="
                + custId + ", responseType=" + responseType + ", reqRcvdTime="
                + reqRcvdTime + ", sourceSystem=" + sourceSystem + "]";
    }

}
