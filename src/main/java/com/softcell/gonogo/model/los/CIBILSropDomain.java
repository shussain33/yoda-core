package com.softcell.gonogo.model.los;

public class CIBILSropDomain {

    private String srNo_;
    private String soaSourceName_;
    private String dateProcessed_;
    private String memberReferenceNo_;
    private String subjectReturnCode_;
    private String enquiryControlNumber_;
    private String consumerNameField1_;
    private String consumerNameField2_;
    private String consumerNameField3_;
    private String consumerNameField4_;
    private String consumerNameField5_;
    private String dateOfBirth_;
    private String genderCode_;
    private String genderValue_;
    private String dateErrorCodePN_;
    private String errorSegTagPN_;
    private String errorCodePN_;
    private String errorPN_;
    private String dateEntryCIBILRemarkCodePN_;
    private String cibilRemarkCodePN_;
    private String dateEntryErrorDisputeRemarkCodePN_;
    private String errorDisputeRemarkCode1PN_;
    private String errorDisputeRemarkCode2PN_;
    private String idType_;
    private String idNumber_;
    private String issueDate_;
    private String expirationDate_;
    private String enrichedThroughEnquiryId_;
    private String telephoneTypeCode_;
    private String telephoneNumber_;
    private String telephoneExtension_;
    private String enrichedThroughEnquiryPT_;
    private String emailId_;
    private String accountTypeCode_;
    private String accountTypeValue_;
    private String dateReportedAndCertifiedEM_;
    private String occupationCodeEM_;
    private String incomeCodeEM_;
    private String incomeValueEM_;
    private String netGrossIndicatorCodeEM_;
    private String netGrossIndicatorValueEM_;
    private String monthlyAnnualIndicatorCodeEM_;
    private String monthlyAnnualIndicatorValueEM_;
    private String dateEntryErrorCodeEM_;
    private String errorCodeEM_;
    private String dateCIBILErrorCodeEM_;
    private String cibilRemarkCodeEM_;
    private String dateDisputeRemarkCodeEM_;
    private String errorDisputeRemarkCode1EM_;
    private String errorDisputeRemarkCode2EM_;
    private String accountNumberPI_;
    private String scoreName_;
    private String scoreCardName_;
    private String scoreCardVersion_;
    private String scoreDate_;
    private String score_;
    private String exclusionCodes1_to_5_;
    private String exclusionCodes6_TO_10_;
    private String exclusionCodes11_TO_15_;
    private String exclusionCodes16_TO_20_;
    private String reasonCodes1_TO_5_;
    private String reasonCodes6_TO_10_;
    private String reasonCodes11_TO_15_;
    private String reasonCodes16_TO_20_;
    private String reasonCodes21_TO_25_;
    private String reasonCodes26_TO_30_;
    private String reasonCodes31_TO_35_;
    private String reasonCodes36_TO_40_;
    private String reasonCodes41_TO_45_;
    private String reasonCodes46_TO_50_;
    private String errorCodeSC_;
    private String addressLine1_;
    private String addressLine2_;
    private String addressLine3_;
    private String addressLine4_;
    private String addressLine5_;
    private String stateCode_;
    private String state_;
    private String pincode_;
    private String addressCatergoryCode_;
    private String addressCatergoryValue_;
    private String residenceCode_;
    private String dateReportedPA_;
    private String reportMemberShortNameTL_;
    private String accountNumberTL_;
    private String accountTypeCodeTL_;
    private String accountTypeValueTL_;
    private String ownershipIndicatorCodeTL_;
    private String ownershipIndicatorValueTL_;
    private String dateOpenedDisbursedTL_;
    private String dateOfLastPaymentTL_;
    private String dateClosedTL_;
    private String dateReportedTL_;
    private String highCreditSanctionedAmount_;
    private String currentBalanceTL_;
    private String amountOverdueTL_;
    private String paymentHistory1_;
    private String paymentHistory2_;
    private String paymentHistoryStartDate_;
    private String paymentHistoryEndDate_;
    private String suitFiledStatusCodeTL_;
    private String suitFiledStatusValueTL_;
    private String wofSettledStatusTL_;
    private String collateralCodeTL_;
    private String collateralValueTL_;
    private String typeOfCollateralTL_;
    private String creditLimitTL_;
    private String cashLimitTL_;
    private String rateOfIntrestTL_;
    private String repayTenure_;
    private String emiAmountTL_;
    private String wofTotalAmountTL_;
    private String wofPrincipalTL_;
    private String settlementAmountTL_;
    private String paymentFrequencyTL_;
    private String actualPaymentAmountTL_;
    private String dateEntErrorCodeTL_;
    private String errorCodeTL_;
    private String dateEntCIBILRemarkCodeTL_;
    private String cibilRemarksCodeTL_;
    private String dateDisputeCodeTL_;
    private String errorDisputeRemarkCode1TL_;
    private String errorDisputeRemarkCode2TL_;
    private String dateOfEnquiryIQ_;
    private String enquiryMemberShortNameIQ_;
    private String enquiryPurposeIQ_;
    private String enqiuryAmountIQ_;
    private String dateOfEntryDR_;
    private String disputeRemarkLine1_;
    private String disputeRemarkLine2_;
    private String disputeRemarkLine3_;
    private String disputeRemarkLine4_;
    private String disputeRemarkLine5_;
    private String disputeRemarkLine6_;
    private Long manualUploadBatchId_;
    private Long requestId_;
    private String applicationId_;
    private String customerId_;
    private String fileName_;
    private String memberShortName_;
    private String enrichedInd_;
    private String resonCode1_to_5Value_;
    private String resonCode6_to_10Value_;
    private String exclusionCodes_1_TO_5_Value_;
    private String exclusionCodes_6_TO_10_Value_;
    private String telephoneTypeValue_;
    private String residenceValue_;
    private String occupationValueEM_;

    public CIBILSropDomain(String srNo_, String soaSourceName_,
                           String memberReferenceNo_, Long manualUploadBatchId_,
                           Long requestId_, String fileName_, String appId, String custId) {
        this.srNo_ = srNo_;
        this.soaSourceName_ = soaSourceName_;
        this.memberReferenceNo_ = memberReferenceNo_;
        this.manualUploadBatchId_ = manualUploadBatchId_;
        this.requestId_ = requestId_;
        this.fileName_ = fileName_;
        this.applicationId_ = appId;
        this.customerId_ = custId;
    }

    public String getSrNo() {
        return srNo_;
    }

    public void setSrNo(String srNo) {
        srNo_ = srNo;
    }

    public String getSoaSourceName() {
        return soaSourceName_;
    }

    public void setSoaSourceName(String soaSourceName) {
        soaSourceName_ = soaSourceName;
    }

    public String getDateProcessed() {
        return dateProcessed_;
    }

    public void setDateProcessed(String dateProcessed) {
        dateProcessed_ = dateProcessed;
    }

    public String getMemberReferenceNo() {
        return memberReferenceNo_;
    }

    public void setMemberReferenceNo(String memberReferenceNo) {
        memberReferenceNo_ = memberReferenceNo;
    }

    public String getSubjectReturnCode() {
        return subjectReturnCode_;
    }

    public void setSubjectReturnCode(String subjectReturnCode) {
        subjectReturnCode_ = subjectReturnCode;
    }

    public String getEnquiryControlNumber() {
        return enquiryControlNumber_;
    }

    public void setEnquiryControlNumber(String enquiryControlNumber) {
        enquiryControlNumber_ = enquiryControlNumber;
    }

    public String getConsumerNameField1() {
        return consumerNameField1_;
    }

    public void setConsumerNameField1(String consumerNameField1) {
        consumerNameField1_ = consumerNameField1;
    }

    public String getConsumerNameField2() {
        return consumerNameField2_;
    }

    public void setConsumerNameField2(String consumerNameField2) {
        consumerNameField2_ = consumerNameField2;
    }

    public String getConsumerNameField3() {
        return consumerNameField3_;
    }

    public void setConsumerNameField3(String consumerNameField3) {
        consumerNameField3_ = consumerNameField3;
    }

    public String getConsumerNameField4() {
        return consumerNameField4_;
    }

    public void setConsumerNameField4(String consumerNameField4) {
        consumerNameField4_ = consumerNameField4;
    }

    public String getConsumerNameField5() {
        return consumerNameField5_;
    }

    public void setConsumerNameField5(String consumerNameField5) {
        consumerNameField5_ = consumerNameField5;
    }

    public String getDateOfBirth() {
        return dateOfBirth_;
    }

    public void setDateOfBirth(String dateOfBirth) {
        dateOfBirth_ = dateOfBirth;
    }

    public String getGenderCode() {
        return genderCode_;
    }

    public void setGenderCode(String genderCode) {
        genderCode_ = genderCode;
    }

    public String getGenderValue() {
        return genderValue_;
    }

    public void setGenderValue(String genderValue) {
        genderValue_ = genderValue;
    }

    public String getDateErrorCodePN() {
        return dateErrorCodePN_;
    }

    public void setDateErrorCodePN(String dateErrorCodePN) {
        dateErrorCodePN_ = dateErrorCodePN;
    }

    public String getErrorSegTagPN() {
        return errorSegTagPN_;
    }

    public void setErrorSegTagPN(String errorSegTagPN) {
        errorSegTagPN_ = errorSegTagPN;
    }

    public String getErrorCodePN() {
        return errorCodePN_;
    }

    public void setErrorCodePN(String errorCodePN) {
        errorCodePN_ = errorCodePN;
    }

    public String getErrorPN() {
        return errorPN_;
    }

    public void setErrorPN(String errorPN) {
        errorPN_ = errorPN;
    }

    public String getDateEntryCIBILRemarkCodePN() {
        return dateEntryCIBILRemarkCodePN_;
    }

    public void setDateEntryCIBILRemarkCodePN(String dateEntryCIBILRemarkCodePN) {
        dateEntryCIBILRemarkCodePN_ = dateEntryCIBILRemarkCodePN;
    }

    public String getCibilRemarkCodePN() {
        return cibilRemarkCodePN_;
    }

    public void setCibilRemarkCodePN(String cibilRemarkCodePN) {
        cibilRemarkCodePN_ = cibilRemarkCodePN;
    }

    public String getDateEntryErrorDisputeRemarkCodePN() {
        return dateEntryErrorDisputeRemarkCodePN_;
    }

    public void setDateEntryErrorDisputeRemarkCodePN(
            String dateEntryErrorDisputeRemarkCodePN) {
        dateEntryErrorDisputeRemarkCodePN_ = dateEntryErrorDisputeRemarkCodePN;
    }

    public String getErrorDisputeRemarkCode1PN() {
        return errorDisputeRemarkCode1PN_;
    }

    public void setErrorDisputeRemarkCode1PN(String errorDisputeRemarkCode1PN) {
        errorDisputeRemarkCode1PN_ = errorDisputeRemarkCode1PN;
    }

    public String getErrorDisputeRemarkCode2PN() {
        return errorDisputeRemarkCode2PN_;
    }

    public void setErrorDisputeRemarkCode2PN(String errorDisputeRemarkCode2PN) {
        errorDisputeRemarkCode2PN_ = errorDisputeRemarkCode2PN;
    }

    public String getIdType() {
        return idType_;
    }

    public void setIdType(String idType) {
        idType_ = idType;
    }

    public String getIdNumber() {
        return idNumber_;
    }

    public void setIdNumber(String idNumber) {
        idNumber_ = idNumber;
    }

    public String getIssueDate() {
        return issueDate_;
    }

    public void setIssueDate(String issueDate) {
        issueDate_ = issueDate;
    }

    public String getExpirationDate() {
        return expirationDate_;
    }

    public void setExpirationDate(String expirationDate) {
        expirationDate_ = expirationDate;
    }

    public String getEnrichedThroughEnquiryId() {
        return enrichedThroughEnquiryId_;
    }

    public void setEnrichedThroughEnquiryId(String enrichedThroughEnquiryId) {
        enrichedThroughEnquiryId_ = enrichedThroughEnquiryId;
    }

    public String getTelephoneTypeCode() {
        return telephoneTypeCode_;
    }

    public void setTelephoneTypeCode(String telephoneTypeCode) {
        telephoneTypeCode_ = telephoneTypeCode;
    }

    public String getTelephoneNumber() {
        return telephoneNumber_;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        telephoneNumber_ = telephoneNumber;
    }

    public String getTelephoneExtension() {
        return telephoneExtension_;
    }

    public void setTelephoneExtension(String telephoneExtension) {
        telephoneExtension_ = telephoneExtension;
    }

    public String getEnrichedThroughEnquiryPT() {
        return enrichedThroughEnquiryPT_;
    }

    public void setEnrichedThroughEnquiryPT(String enrichedThroughEnquiryPT) {
        enrichedThroughEnquiryPT_ = enrichedThroughEnquiryPT;
    }

    public String getEmailId() {
        return emailId_;
    }

    public void setEmailId(String emailId) {
        emailId_ = emailId;
    }

    public String getAccountTypeCode() {
        return accountTypeCode_;
    }

    public void setAccountTypeCode(String accountTypeCode) {
        accountTypeCode_ = accountTypeCode;
    }

    public String getAccountTypeValue() {
        return accountTypeValue_;
    }

    public void setAccountTypeValue(String accountTypeValue) {
        accountTypeValue_ = accountTypeValue;
    }

    public String getDateReportedAndCertifiedEM() {
        return dateReportedAndCertifiedEM_;
    }

    public void setDateReportedAndCertifiedEM(String dateReportedAndCertifiedEM) {
        dateReportedAndCertifiedEM_ = dateReportedAndCertifiedEM;
    }

    public String getOccupationCodeEM() {
        return occupationCodeEM_;
    }

    public void setOccupationCodeEM(String occupationCodeEM) {
        occupationCodeEM_ = occupationCodeEM;
    }

    public String getIncomeCodeEM() {
        return incomeCodeEM_;
    }

    public void setIncomeCodeEM(String incomeCodeEM) {
        incomeCodeEM_ = incomeCodeEM;
    }

    public String getIncomeValueEM() {
        return incomeValueEM_;
    }

    public void setIncomeValueEM(String incomeValueEM) {
        incomeValueEM_ = incomeValueEM;
    }

    public String getNetGrossIndicatorCodeEM() {
        return netGrossIndicatorCodeEM_;
    }

    public void setNetGrossIndicatorCodeEM(String netGrossIndicatorCodeEM) {
        netGrossIndicatorCodeEM_ = netGrossIndicatorCodeEM;
    }

    public String getNetGrossIndicatorValueEM() {
        return netGrossIndicatorValueEM_;
    }

    public void setNetGrossIndicatorValueEM(String netGrossIndicatorValueEM) {
        netGrossIndicatorValueEM_ = netGrossIndicatorValueEM;
    }

    public String getMonthlyAnnualIndicatorCodeEM() {
        return monthlyAnnualIndicatorCodeEM_;
    }

    public void setMonthlyAnnualIndicatorCodeEM(
            String monthlyAnnualIndicatorCodeEM) {
        monthlyAnnualIndicatorCodeEM_ = monthlyAnnualIndicatorCodeEM;
    }

    public String getMonthlyAnnualIndicatorValueEM() {
        return monthlyAnnualIndicatorValueEM_;
    }

    public void setMonthlyAnnualIndicatorValueEM(
            String monthlyAnnualIndicatorValueEM) {
        monthlyAnnualIndicatorValueEM_ = monthlyAnnualIndicatorValueEM;
    }

    public String getDateEntryErrorCodeEM() {
        return dateEntryErrorCodeEM_;
    }

    public void setDateEntryErrorCodeEM(String dateEntryErrorCodeEM) {
        dateEntryErrorCodeEM_ = dateEntryErrorCodeEM;
    }

    public String getErrorCodeEM() {
        return errorCodeEM_;
    }

    public void setErrorCodeEM(String errorCodeEM) {
        errorCodeEM_ = errorCodeEM;
    }

    public String getDateCIBILErrorCodeEM() {
        return dateCIBILErrorCodeEM_;
    }

    public void setDateCIBILErrorCodeEM(String dateCIBILErrorCodeEM) {
        dateCIBILErrorCodeEM_ = dateCIBILErrorCodeEM;
    }

    public String getCibilRemarkCodeEM() {
        return cibilRemarkCodeEM_;
    }

    public void setCibilRemarkCodeEM(String cibilRemarkCodeEM) {
        cibilRemarkCodeEM_ = cibilRemarkCodeEM;
    }

    public String getDateDisputeRemarkCodeEM() {
        return dateDisputeRemarkCodeEM_;
    }

    public void setDateDisputeRemarkCodeEM(String dateDisputeRemarkCodeEM) {
        dateDisputeRemarkCodeEM_ = dateDisputeRemarkCodeEM;
    }

    public String getErrorDisputeRemarkCode1EM() {
        return errorDisputeRemarkCode1EM_;
    }

    public void setErrorDisputeRemarkCode1EM(String errorDisputeRemarkCode1EM) {
        errorDisputeRemarkCode1EM_ = errorDisputeRemarkCode1EM;
    }

    public String getErrorDisputeRemarkCode2EM() {
        return errorDisputeRemarkCode2EM_;
    }

    public void setErrorDisputeRemarkCode2EM(String errorDisputeRemarkCode2EM) {
        errorDisputeRemarkCode2EM_ = errorDisputeRemarkCode2EM;
    }

    public String getAccountNumberPI() {
        return accountNumberPI_;
    }

    public void setAccountNumberPI(String accountNumberPI) {
        accountNumberPI_ = accountNumberPI;
    }

    public String getScoreName() {
        return scoreName_;
    }

    public void setScoreName(String scoreName) {
        scoreName_ = scoreName;
    }

    public String getScoreCardName() {
        return scoreCardName_;
    }

    public void setScoreCardName(String scoreCardName) {
        scoreCardName_ = scoreCardName;
    }

    public String getScoreCardVersion() {
        return scoreCardVersion_;
    }

    public void setScoreCardVersion(String scoreCardVersion) {
        scoreCardVersion_ = scoreCardVersion;
    }

    public String getScoreDate() {
        return scoreDate_;
    }

    public void setScoreDate(String scoreDate) {
        scoreDate_ = scoreDate;
    }

    public String getScore() {
        return score_;
    }

    public void setScore(String score) {
        score_ = score;
    }

    public String getExclusionCodes1_to_5() {
        return exclusionCodes1_to_5_;
    }

    public void setExclusionCodes1_to_5(String exclusionCodes1_to_5) {
        exclusionCodes1_to_5_ = exclusionCodes1_to_5;
    }

    public String getExclusionCodes6_TO_10() {
        return exclusionCodes6_TO_10_;
    }

    public void setExclusionCodes6_TO_10(String exclusionCodes6_TO_10) {
        exclusionCodes6_TO_10_ = exclusionCodes6_TO_10;
    }

    public String getExclusionCodes11_TO_15() {
        return exclusionCodes11_TO_15_;
    }

    public void setExclusionCodes11_TO_15(String exclusionCodes11_TO_15) {
        exclusionCodes11_TO_15_ = exclusionCodes11_TO_15;
    }

    public String getExclusionCodes16_TO_20() {
        return exclusionCodes16_TO_20_;
    }

    public void setExclusionCodes16_TO_20(String exclusionCodes16_TO_20) {
        exclusionCodes16_TO_20_ = exclusionCodes16_TO_20;
    }

    public String getReasonCodes1_TO_5() {
        return reasonCodes1_TO_5_;
    }

    public void setReasonCodes1_TO_5(String reasonCodes1_TO_5) {
        reasonCodes1_TO_5_ = reasonCodes1_TO_5;
    }

    public String getReasonCodes6_TO_10() {
        return reasonCodes6_TO_10_;
    }

    public void setReasonCodes6_TO_10(String reasonCodes6_TO_10) {
        reasonCodes6_TO_10_ = reasonCodes6_TO_10;
    }

    public String getReasonCodes11_TO_15() {
        return reasonCodes11_TO_15_;
    }

    public void setReasonCodes11_TO_15(String reasonCodes11_TO_15) {
        reasonCodes11_TO_15_ = reasonCodes11_TO_15;
    }

    public String getReasonCodes16_TO_20() {
        return reasonCodes16_TO_20_;
    }

    public void setReasonCodes16_TO_20(String reasonCodes16_TO_20) {
        reasonCodes16_TO_20_ = reasonCodes16_TO_20;
    }

    public String getReasonCodes21_TO_25() {
        return reasonCodes21_TO_25_;
    }

    public void setReasonCodes21_TO_25(String reasonCodes21_TO_25) {
        reasonCodes21_TO_25_ = reasonCodes21_TO_25;
    }

    public String getReasonCodes26_TO_30() {
        return reasonCodes26_TO_30_;
    }

    public void setReasonCodes26_TO_30(String reasonCodes26_TO_30) {
        reasonCodes26_TO_30_ = reasonCodes26_TO_30;
    }

    public String getReasonCodes31_TO_35() {
        return reasonCodes31_TO_35_;
    }

    public void setReasonCodes31_TO_35(String reasonCodes31_TO_35) {
        reasonCodes31_TO_35_ = reasonCodes31_TO_35;
    }

    public String getReasonCodes36_TO_40() {
        return reasonCodes36_TO_40_;
    }

    public void setReasonCodes36_TO_40(String reasonCodes36_TO_40) {
        reasonCodes36_TO_40_ = reasonCodes36_TO_40;
    }

    public String getReasonCodes41_TO_45() {
        return reasonCodes41_TO_45_;
    }

    public void setReasonCodes41_TO_45(String reasonCodes41_TO_45) {
        reasonCodes41_TO_45_ = reasonCodes41_TO_45;
    }

    public String getReasonCodes46_TO_50() {
        return reasonCodes46_TO_50_;
    }

    public void setReasonCodes46_TO_50(String reasonCodes46_TO_50) {
        reasonCodes46_TO_50_ = reasonCodes46_TO_50;
    }

    public String getErrorCodeSC() {
        return errorCodeSC_;
    }

    public void setErrorCodeSC(String errorCodeSC) {
        errorCodeSC_ = errorCodeSC;
    }

    public String getAddressLine1() {
        return addressLine1_;
    }

    public void setAddressLine1(String addressLine1) {
        addressLine1_ = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2_;
    }

    public void setAddressLine2(String addressLine2) {
        addressLine2_ = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3_;
    }

    public void setAddressLine3(String addressLine3) {
        addressLine3_ = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4_;
    }

    public void setAddressLine4(String addressLine4) {
        addressLine4_ = addressLine4;
    }

    public String getAddressLine5() {
        return addressLine5_;
    }

    public void setAddressLine5(String addressLine5) {
        addressLine5_ = addressLine5;
    }

    public String getStateCode() {
        return stateCode_;
    }

    public void setStateCode(String stateCode) {
        stateCode_ = stateCode;
    }

    public String getState() {
        return state_;
    }

    public void setState(String state) {
        state_ = state;
    }

    public String getPincode() {
        return pincode_;
    }

    public void setPincode(String pincode) {
        pincode_ = pincode;
    }

    public String getResidenceCode() {
        return residenceCode_;
    }

    public void setResidenceCode(String residenceCode) {
        residenceCode_ = residenceCode;
    }

    public String getDateReportedPA() {
        return dateReportedPA_;
    }

    public void setDateReportedPA(String dateReportedPA) {
        dateReportedPA_ = dateReportedPA;
    }

    public String getReportMemberShortNameTL() {
        return reportMemberShortNameTL_;
    }

    public void setReportMemberShortNameTL(String reportMemberShortNameTL) {
        reportMemberShortNameTL_ = reportMemberShortNameTL;
    }

    public String getAccountNumberTL() {
        return accountNumberTL_;
    }

    public void setAccountNumberTL(String accountNumberTL) {
        accountNumberTL_ = accountNumberTL;
    }

    public String getAccountTypeCodeTL() {
        return accountTypeCodeTL_;
    }

    public void setAccountTypeCodeTL(String accountTypeCodeTL) {
        accountTypeCodeTL_ = accountTypeCodeTL;
    }

    public String getAccountTypeValueTL() {
        return accountTypeValueTL_;
    }

    public void setAccountTypeValueTL(String accountTypeValueTL) {
        accountTypeValueTL_ = accountTypeValueTL;
    }

    public String getOwnershipIndicatorCodeTL() {
        return ownershipIndicatorCodeTL_;
    }

    public void setOwnershipIndicatorCodeTL(String ownershipIndicatorCodeTL) {
        ownershipIndicatorCodeTL_ = ownershipIndicatorCodeTL;
    }

    public String getOwnershipIndicatorValueTL() {
        return ownershipIndicatorValueTL_;
    }

    public void setOwnershipIndicatorValueTL(String ownershipIndicatorValueTL) {
        ownershipIndicatorValueTL_ = ownershipIndicatorValueTL;
    }

    public String getDateOpenedDisbursedTL() {
        return dateOpenedDisbursedTL_;
    }

    public void setDateOpenedDisbursedTL(String dateOpenedDisbursedTL) {
        dateOpenedDisbursedTL_ = dateOpenedDisbursedTL;
    }

    public String getDateOfLastPaymentTL() {
        return dateOfLastPaymentTL_;
    }

    public void setDateOfLastPaymentTL(String dateOfLastPaymentTL) {
        dateOfLastPaymentTL_ = dateOfLastPaymentTL;
    }

    public String getDateClosedTL() {
        return dateClosedTL_;
    }

    public void setDateClosedTL(String dateClosedTL) {
        dateClosedTL_ = dateClosedTL;
    }

    public String getDateReportedTL() {
        return dateReportedTL_;
    }

    public void setDateReportedTL(String dateReportedTL) {
        dateReportedTL_ = dateReportedTL;
    }

    public String getHighCreditSanctionedAmount() {
        return highCreditSanctionedAmount_;
    }

    public void setHighCreditSanctionedAmount(String highCreditSanctionedAmount) {
        highCreditSanctionedAmount_ = highCreditSanctionedAmount;
    }

    public String getCurrentBalanceTL() {
        return currentBalanceTL_;
    }

    public void setCurrentBalanceTL(String currentBalanceTL) {
        currentBalanceTL_ = currentBalanceTL;
    }

    public String getAmountOverdueTL() {
        return amountOverdueTL_;
    }

    public void setAmountOverdueTL(String amountOverdueTL) {
        amountOverdueTL_ = amountOverdueTL;
    }

    public String getPaymentHistory1() {
        return paymentHistory1_;
    }

    public void setPaymentHistory1(String paymentHistory1) {
        paymentHistory1_ = paymentHistory1;
    }

    public String getPaymentHistory2() {
        return paymentHistory2_;
    }

    public void setPaymentHistory2(String paymentHistory2) {
        paymentHistory2_ = paymentHistory2;
    }

    public String getPaymentHistoryStartDate() {
        return paymentHistoryStartDate_;
    }

    public void setPaymentHistoryStartDate(String paymentHistoryStartDate) {
        paymentHistoryStartDate_ = paymentHistoryStartDate;
    }

    public String getPaymentHistoryEndDate() {
        return paymentHistoryEndDate_;
    }

    public void setPaymentHistoryEndDate(String paymentHistoryEndDate) {
        paymentHistoryEndDate_ = paymentHistoryEndDate;
    }

    public String getSuitFiledStatusCodeTL() {
        return suitFiledStatusCodeTL_;
    }

    public void setSuitFiledStatusCodeTL(String suitFiledStatusCodeTL) {
        suitFiledStatusCodeTL_ = suitFiledStatusCodeTL;
    }

    public String getSuitFiledStatusValueTL() {
        return suitFiledStatusValueTL_;
    }

    public void setSuitFiledStatusValueTL(String suitFiledStatusValueTL) {
        suitFiledStatusValueTL_ = suitFiledStatusValueTL;
    }

    public String getWofSettledStatusTL() {
        return wofSettledStatusTL_;
    }

    public void setWofSettledStatusTL(String wofSettledStatusTL) {
        wofSettledStatusTL_ = wofSettledStatusTL;
    }

    public String getCollateralCodeTL() {
        return collateralCodeTL_;
    }

    public void setCollateralCodeTL(String collateralCodeTL) {
        collateralCodeTL_ = collateralCodeTL;
    }

    public String getCollateralValueTL() {
        return collateralValueTL_;
    }

    public void setCollateralValueTL(String collateralValueTL) {
        collateralValueTL_ = collateralValueTL;
    }

    public String getTypeOfCollateralTL() {
        return typeOfCollateralTL_;
    }

    public void setTypeOfCollateralTL(String typeOfCollateralTL) {
        typeOfCollateralTL_ = typeOfCollateralTL;
    }

    public String getCreditLimitTL() {
        return creditLimitTL_;
    }

    public void setCreditLimitTL(String creditLimitTL) {
        creditLimitTL_ = creditLimitTL;
    }

    public String getCashLimitTL() {
        return cashLimitTL_;
    }

    public void setCashLimitTL(String cashLimitTL) {
        cashLimitTL_ = cashLimitTL;
    }

    public String getRateOfIntrestTL() {
        return rateOfIntrestTL_;
    }

    public void setRateOfIntrestTL(String rateOfIntrestTL) {
        rateOfIntrestTL_ = rateOfIntrestTL;
    }

    public String getRepayTenure() {
        return repayTenure_;
    }

    public void setRepayTenure(String repayTenure) {
        repayTenure_ = repayTenure;
    }

    public String getEmiAmountTL() {
        return emiAmountTL_;
    }

    public void setEmiAmountTL(String emiAmountTL) {
        emiAmountTL_ = emiAmountTL;
    }

    public String getWofTotalAmountTL() {
        return wofTotalAmountTL_;
    }

    public void setWofTotalAmountTL(String wofTotalAmountTL) {
        wofTotalAmountTL_ = wofTotalAmountTL;
    }

    public String getWofPrincipalTL() {
        return wofPrincipalTL_;
    }

    public void setWofPrincipalTL(String wofPrincipalTL) {
        wofPrincipalTL_ = wofPrincipalTL;
    }

    public String getSettlementAmountTL() {
        return settlementAmountTL_;
    }

    public void setSettlementAmountTL(String settlementAmountTL) {
        settlementAmountTL_ = settlementAmountTL;
    }

    public String getPaymentFrequencyTL() {
        return paymentFrequencyTL_;
    }

    public void setPaymentFrequencyTL(String paymentFrequencyTL) {
        paymentFrequencyTL_ = paymentFrequencyTL;
    }

    public String getActualPaymentAmountTL() {
        return actualPaymentAmountTL_;
    }

    public void setActualPaymentAmountTL(String actualPaymentAmountTL) {
        actualPaymentAmountTL_ = actualPaymentAmountTL;
    }

    public String getDateEntErrorCodeTL() {
        return dateEntErrorCodeTL_;
    }

    public void setDateEntErrorCodeTL(String dateEntErrorCodeTL) {
        dateEntErrorCodeTL_ = dateEntErrorCodeTL;
    }

    public String getErrorCodeTL() {
        return errorCodeTL_;
    }

    public void setErrorCodeTL(String errorCodeTL) {
        errorCodeTL_ = errorCodeTL;
    }

    public String getDateEntCIBILRemarkCodeTL() {
        return dateEntCIBILRemarkCodeTL_;
    }

    public void setDateEntCIBILRemarkCodeTL(String dateEntCIBILRemarkCodeTL) {
        dateEntCIBILRemarkCodeTL_ = dateEntCIBILRemarkCodeTL;
    }

    public String getCibilRemarksCodeTL() {
        return cibilRemarksCodeTL_;
    }

    public void setCibilRemarksCodeTL(String cibilRemarksCodeTL) {
        cibilRemarksCodeTL_ = cibilRemarksCodeTL;
    }

    public String getDateDisputeCodeTL() {
        return dateDisputeCodeTL_;
    }

    public void setDateDisputeCodeTL(String dateDisputeCodeTL) {
        dateDisputeCodeTL_ = dateDisputeCodeTL;
    }

    public String getErrorDisputeRemarkCode1TL() {
        return errorDisputeRemarkCode1TL_;
    }

    public void setErrorDisputeRemarkCode1TL(String errorDisputeRemarkCode1TL) {
        errorDisputeRemarkCode1TL_ = errorDisputeRemarkCode1TL;
    }

    public String getErrorDisputeRemarkCode2TL() {
        return errorDisputeRemarkCode2TL_;
    }

    public void setErrorDisputeRemarkCode2TL(String errorDisputeRemarkCode2TL) {
        errorDisputeRemarkCode2TL_ = errorDisputeRemarkCode2TL;
    }

    public String getDateOfEnquiryIQ() {
        return dateOfEnquiryIQ_;
    }

    public void setDateOfEnquiryIQ(String dateOfEnquiryIQ) {
        dateOfEnquiryIQ_ = dateOfEnquiryIQ;
    }

    public String getEnquiryMemberShortNameIQ() {
        return enquiryMemberShortNameIQ_;
    }

    public void setEnquiryMemberShortNameIQ(String enquiryMemberShortNameIQ) {
        enquiryMemberShortNameIQ_ = enquiryMemberShortNameIQ;
    }

    public String getEnquiryPurposeIQ() {
        return enquiryPurposeIQ_;
    }

    public void setEnquiryPurposeIQ(String enquiryPurposeIQ) {
        enquiryPurposeIQ_ = enquiryPurposeIQ;
    }

    public String getEnqiuryAmountIQ() {
        return enqiuryAmountIQ_;
    }

    public void setEnqiuryAmountIQ(String enqiuryAmountIQ) {
        enqiuryAmountIQ_ = enqiuryAmountIQ;
    }

    public String getDateOfEntryDR() {
        return dateOfEntryDR_;
    }

    public void setDateOfEntryDR(String dateOfEntryDR) {
        dateOfEntryDR_ = dateOfEntryDR;
    }

    public String getDisputeRemarkLine1() {
        return disputeRemarkLine1_;
    }

    public void setDisputeRemarkLine1(String disputeRemarkLine1) {
        disputeRemarkLine1_ = disputeRemarkLine1;
    }

    public String getDisputeRemarkLine2() {
        return disputeRemarkLine2_;
    }

    public void setDisputeRemarkLine2(String disputeRemarkLine2) {
        disputeRemarkLine2_ = disputeRemarkLine2;
    }

    public String getDisputeRemarkLine3() {
        return disputeRemarkLine3_;
    }

    public void setDisputeRemarkLine3(String disputeRemarkLine3) {
        disputeRemarkLine3_ = disputeRemarkLine3;
    }

    public String getDisputeRemarkLine4() {
        return disputeRemarkLine4_;
    }

    public void setDisputeRemarkLine4(String disputeRemarkLine4) {
        disputeRemarkLine4_ = disputeRemarkLine4;
    }

    public String getDisputeRemarkLine5() {
        return disputeRemarkLine5_;
    }

    public void setDisputeRemarkLine5(String disputeRemarkLine5) {
        disputeRemarkLine5_ = disputeRemarkLine5;
    }

    public String getDisputeRemarkLine6() {
        return disputeRemarkLine6_;
    }

    public void setDisputeRemarkLine6(String disputeRemarkLine6) {
        disputeRemarkLine6_ = disputeRemarkLine6;
    }

    public Long getManualUploadBatchId() {
        return manualUploadBatchId_;
    }

    public void setManualUploadBatchId(Long manualUploadBatchId) {
        manualUploadBatchId_ = manualUploadBatchId;
    }

    public Long getRequestId() {
        return requestId_;
    }

    public void setRequestId(Long requestId) {
        requestId_ = requestId;
    }

    public String getApplicationId() {
        return applicationId_;
    }

    public void setApplicationId(String applicationId) {
        applicationId_ = applicationId;
    }

    public String getCustomerId() {
        return customerId_;
    }

    public void setCustomerId(String customerId) {
        customerId_ = customerId;
    }

    public String getFileName() {
        return fileName_;
    }

    public void setFileName(String fileName) {
        fileName_ = fileName;
    }

    public String getMemberShortName() {
        return memberShortName_;
    }

    public void setMemberShortName(String memberShortName) {
        memberShortName_ = memberShortName;
    }

    public String getEnrichedInd() {
        return enrichedInd_;
    }

    public void setEnrichedInd(String enrichedInd) {
        enrichedInd_ = enrichedInd;
    }

    public String getResonCode1_to_5Value() {
        return resonCode1_to_5Value_;
    }

    public void setResonCode1_to_5Value(String resonCode1_to_5Value) {
        resonCode1_to_5Value_ = resonCode1_to_5Value;
    }

    public String getResonCode6_to_10Value() {
        return resonCode6_to_10Value_;
    }

    public void setResonCode6_to_10Value(String resonCode6_to_10Value) {
        resonCode6_to_10Value_ = resonCode6_to_10Value;
    }

    public String getExclusionCodes_1_TO_5_Value() {
        return exclusionCodes_1_TO_5_Value_;
    }

    public void setExclusionCodes_1_TO_5_Value(
            String exclusionCodes_1_TO_5_Value) {
        exclusionCodes_1_TO_5_Value_ = exclusionCodes_1_TO_5_Value;
    }

    public String getExclusionCodes_6_TO_10_Value() {
        return exclusionCodes_6_TO_10_Value_;
    }

    public void setExclusionCodes_6_TO_10_Value(
            String exclusionCodes_6_TO_10_Value) {
        exclusionCodes_6_TO_10_Value_ = exclusionCodes_6_TO_10_Value;
    }

    public String getTelephoneTypeValue() {
        return telephoneTypeValue_;
    }

    public void setTelephoneTypeValue(String telephoneTypeValue) {
        telephoneTypeValue_ = telephoneTypeValue;
    }

    public String getResidenceValue() {
        return residenceValue_;
    }

    public void setResidenceValue(String residenceValue) {
        residenceValue_ = residenceValue;
    }

    public String getOccupationValueEM() {
        return occupationValueEM_;
    }

    public void setOccupationValueEM(String occupationValueEM) {
        occupationValueEM_ = occupationValueEM;
    }

    public String getAddressCatergoryCode() {
        return addressCatergoryCode_;
    }

    public void setAddressCatergoryCode(String addressCatergoryCode) {
        addressCatergoryCode_ = addressCatergoryCode;
    }

    public String getAddressCatergoryValue() {
        return addressCatergoryValue_;
    }

    public void setAddressCatergoryValue(String addressCatergoryValue) {
        addressCatergoryValue_ = addressCatergoryValue;
    }


}