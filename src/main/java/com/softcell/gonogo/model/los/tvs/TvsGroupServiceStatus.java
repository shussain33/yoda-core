package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection="TvsGroupService_Status")
public class TvsGroupServiceStatus {

	@Id
	private String id;
	private String caseId;
	private String TVSGroup1Status;
	private String TVSGroup2Status;
	private String TVSGroup3Status;
	private String TVSGroup4Status;
	private String TVSGroup5Status;
	private Date updateDateTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCaseId() {
		return caseId;
	}

}
