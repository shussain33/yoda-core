package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class SecondaryMatches {

	    private String sno;

	    private SecIdList[] secIdList;

	    private SecName secName;

	    private SecAddressList[] secAddressList;

	    private SecPhoneList[] secPhoneList;


}
