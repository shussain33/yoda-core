package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CDLos
{
    private String vendorid;

    private String action;

    private String remarks;

    private String stage;

    private String prospectid;

}
