package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Loans {


	private String countOverdueEmiLast3Months;

	private String loanType;

	private String loanNo;

	private String lenderName;

	private String emiAmountMonth0;

	private String countOverdueEmiEver;

	private String emiAmountMonth2;

	private String emiDueDate;

	private String emiAmountMonth1;

}
