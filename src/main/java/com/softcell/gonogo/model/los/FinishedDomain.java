package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;

public class FinishedDomain implements Serializable {

    /**
     * @author AshwiniP
     */
    private static final long serialVersionUID = 1L;

    @Expose
    @SerializedName("TRACKING-ID")
    private Long trackingId;
    @Expose
    @SerializedName("BUREAU")
    private String bureau;
    @Expose
    @SerializedName("PRODUCT")
    private String product;
    @Expose
    @SerializedName("STATUS")
    private String status;
    @Expose
    @SerializedName("JSON-RESPONSE-OBJECT")
    private Object responseJsonObject;
    @Expose
    @SerializedName("BUREAU-STRING")
    private String bureauString;
    @Expose
    @SerializedName("HTML-REPORT")
    private String htmlReport;
    @Expose
    @SerializedName("PDF-REPORT")
    private byte[] pdfReport;
    @Expose
    @SerializedName("ENCODED-PDF")
    private byte[] encPdfReport;
    @Expose
    @SerializedName("ENCODED-STRING")
    private String encodedString;
    @Expose
    @SerializedName("DERIVED-ATTRIBUTES")
    private String derivedAttributes;
    @Expose
    @SerializedName("XML-RESPONSE-OBJECT")
    private String xmlResponseObject;

    public Long getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(Long trackingId) {
        this.trackingId = trackingId;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getResponseJsonObject() {
        return responseJsonObject;
    }

    public void setResponseJsonObject(Object responseJsonObject) {
        this.responseJsonObject = responseJsonObject;
    }

    public String getBureauString() {
        return bureauString;
    }

    public void setBureauString(String bureauString) {
        this.bureauString = bureauString;
    }

    public String getHtmlReport() {
        return htmlReport;
    }

    public void setHtmlReport(String htmlReport) {
        this.htmlReport = htmlReport;
    }

    public byte[] getPdfReport() {
        return pdfReport;
    }

    public void setPdfReport(byte[] pdfReport) {
        this.pdfReport = pdfReport;
    }

    public byte[] getEncPdfReport() {
        return encPdfReport;
    }

    public void setEncPdfReport(byte[] encPdfReport) {
        this.encPdfReport = encPdfReport;
    }

    public String getEncodedString() {
        return encodedString;
    }

    public void setEncodedString(String encodedString) {
        this.encodedString = encodedString;
    }

    public String getDerivedAttributes() {
        return derivedAttributes;
    }

    public void setDerivedAttributes(String derivedAttributes) {
        this.derivedAttributes = derivedAttributes;
    }

    public String getXmlResponseObject() {
        return xmlResponseObject;
    }

    public void setXmlResponseObject(String xmlResponseObject) {
        this.xmlResponseObject = xmlResponseObject;
    }

    @Override
    public String toString() {
        return "FinishedDomain [trackingId=" + trackingId + ", bureau="
                + bureau + ", product=" + product + ", status=" + status
                + ", responseJsonObject=" + responseJsonObject
                + ", bureauString=" + bureauString + ", htmlReport="
                + htmlReport + ", pdfReport=" + Arrays.toString(pdfReport)
                + ", encPdfReport=" + Arrays.toString(encPdfReport)
                + ", encodedString=" + encodedString + ", derivedAttributes="
                + derivedAttributes + ", xmlResponseObject="
                + xmlResponseObject + "]";
    }


}
