package com.softcell.gonogo.model.los;

/**
 * This domain will be used to post request in MB
 *
 * @author bhuvneshk
 */
public class LosMbPostRequest {

    private String institutionId;
    private byte[] requestData;

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the requestData
     */
    public byte[] getRequestData() {
        return requestData;
    }

    /**
     * @param requestData the requestData to set
     */
    public void setRequestData(byte[] requestData) {
        this.requestData = requestData;
    }

}
