package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Name {
	private String name5;

    private String name3;

    private String name4;

    private String dob;

    private String gender;

    private String name1;

    private String name2;


}
