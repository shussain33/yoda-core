package com.softcell.gonogo.model.los.tvs;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DoStages {

        private String disbursmentuserid;

        private String disbursmentdate;

        private String agreementno;

        private String isemailsent;

        private String isapplicationgenerated;

        private String isagreementgenerated;

        private String isdogenerated;
}
