package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AlternateContact {

	  private String regularlyCalledContact1;

	    private String freqContact2;

	    private String freqContact3;

	    private String regularlyCalledContact2;

	    private String regularlyCalledContact3;

	    private String freqContact1;

	    private String freqFamilyContact;


}
