/**
 *
 */
package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Dipak
 */
public class HibCIBILSropDomain implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer id;
    @Expose
    @SerializedName("SRNO")
    private Integer srNo;
    @Expose
    @SerializedName("SOA_SOURCE_NAME")
    private String soaSourceName;
    @Expose
    @SerializedName("DATE_PROCESSED")
    private Date dateProcessed;
    @Expose
    @SerializedName("MEMBER_REFERENCE_NUMBER")
    private String memberReferenceNo;
    @Expose
    @SerializedName("SUBJECT_RETURN_CODE")
    private String subjectReturnCode;
    @Expose
    @SerializedName("ENQUIRY_CONTROL_NUMBER")
    private String enquiryControlNumber;
    @Expose
    @SerializedName("CONSUMER_NAME_FIELD1")
    private String consumerNameField1;
    @Expose
    @SerializedName("CONSUME_NAME_FIELD2")
    private String consumerNameField2;
    @Expose
    @SerializedName("CONSUMER_NAME_FIELD3")
    private String consumerNameField3;
    @Expose
    @SerializedName("CONSUME_NAME_FIELD4")
    private String consumerNameField4;
    @Expose
    @SerializedName("CONSUMER_NAME_FIELD5")
    private String consumerNameField5;
    @Expose
    @SerializedName("DATE_OF_BIRTH")
    private Date dateOfBirth;
    @Expose
    @SerializedName("GENDER")
    private String genderValue;
    @Expose
    @SerializedName("DT_ERRCODE_PN")
    private Date dateErrorCodePN;
    @Expose
    @SerializedName("ERR_SEG_TAG_PN")
    private String errorSegTagPN;
    @Expose
    @SerializedName("ERR_CODE_PN")
    private String errorCodePN;
    @Expose
    @SerializedName("ERROR_PN")
    private String errorPN;
    @Expose
    @SerializedName("DT_ENTCIBILRECODE_PN")
    private Date dateEntryCIBILRemarkCodePN;
    @Expose
    @SerializedName("CIBIL_REMARK_CODE_PN")
    private String cibilRemarkCodePN;
    @Expose
    @SerializedName("DATE_DISP_REMARK_CODE_PN")
    private Date dateEntryErrorDisputeRemarkCodePN;
    @Expose
    @SerializedName("ERR_DISP_REM_CODE1_PN")
    private String errorDisputeRemarkCode1PN;
    @Expose
    @SerializedName("ERR_DISP_REM_CODE2_PN")
    private String errorDisputeRemarkCode2PN;
    @Expose
    @SerializedName("ID_TYPE")
    private String idType;
    @Expose
    @SerializedName("ID_NUMBER")
    private String idNumber;
    @Expose
    @SerializedName("ISSUE_DATE")
    private Date issueDate;
    @Expose
    @SerializedName("EXPIRATION_DATE")
    private Date expirationDate;
    @Expose
    @SerializedName("ENRICHED_THROUGH_ENQUIRY_ID")
    private String enrichedThroughEnquiryId;
    @Expose
    @SerializedName("TELEPHONE_NUMBER")
    private String telephoneNumber;
    @Expose
    @SerializedName("TELEPHONE_EXTENSION")
    private String telephoneExtension;
    @Expose
    @SerializedName("TELEPHONE_TYPE")
    private String telephoneTypeValue;
    @Expose
    @SerializedName("ENRICHED_THROUGH_ENQUIRY_PT")
    private String enrichedThroughEnquiryPT;
    @Expose
    @SerializedName("EMAIL_ID")
    private String emailId;
    @Expose
    @SerializedName("ACCOUNT_TYPE")
    private String accountTypeValue;
    @Expose
    @SerializedName("DATE_REPORTED_AND_CERTIFIED_EM")
    private Date dateReportedAndCertifiedEM;
    @Expose
    @SerializedName("OCCUPATION_CODE_EM")
    private String occupationValueEM;
    @Expose
    @SerializedName("INCOME_EM")
    private String incomeValueEM;
    @Expose
    @SerializedName("NET_GROSS_INDICATOR_EM")
    private String netGrossIndicatorValueEM;
    @Expose
    @SerializedName("MNTHLY_ANNUAL_INDICATOR_EM")
    private String monthlyAnnualIndicatorValueEM;
    @Expose
    @SerializedName("DATE_ENTRY_ERR_CODE_EM")
    private Date dateEntryErrorCodeEM;
    @Expose
    @SerializedName("ERR_CODE_EM")
    private String errorCodeEM;
    @Expose
    @SerializedName("DATE_CIBIL_ERR_CODE_EM")
    private Date dateCIBILErrorCodeEM;
    @Expose
    @SerializedName("CIBIL_REMARK_CODE_EM")
    private String cibilRemarkCodeEM;
    @Expose
    @SerializedName("DT_DIS_REMARK_CODE_EM")
    private Date dateDisputeRemarkCodeEM;
    @Expose
    @SerializedName("ERR_DISP_REMCODE1_EM")
    private String errorDisputeRemarkCode1EM;
    @Expose
    @SerializedName("ERR_DISP_REMCODE2_EM")
    private String errorDisputeRemarkCode2EM;
    @Expose
    @SerializedName("ACCOUNT_NUMBER_PI")
    private String accountNumberPI;
    @Expose
    @SerializedName("SCORE_NAME")
    private String scoreName;
    @Expose
    @SerializedName("SCORE_CARD_NAME")
    private String scoreCardName;
    @Expose
    @SerializedName("SCORE_CARD_VERSION")
    private String scoreCardVersion;
    @Expose
    @SerializedName("SCORE_DATE")
    private Date scoreDate;
    @Expose
    @SerializedName("SCORE")
    private String score;
    @Expose
    @SerializedName("EXCLUSION_CODES_1_TO_5")
    private String exclusionCodes1_to_5;
    @Expose
    @SerializedName("EXCLUSION_CODES_6_TO_10")
    private String exclusionCodes6_TO_10;
    @Expose
    @SerializedName("EXCLUSION_CODES_11_TO_15")
    private String exclusionCodes11_TO_15;
    @Expose
    @SerializedName("EXCLUSION_CODES_16_TO_20")
    private String exclusionCodes16_TO_20;
    @Expose
    @SerializedName("REASON_CODES_1_TO_5")
    private String reasonCodes1_TO_5;
    @Expose
    @SerializedName("REASON_CODES_6_TO_10")
    private String reasonCodes6_TO_10;
    @Expose
    @SerializedName("REASON_CODES_11_TO_15")
    private String reasonCodes11_TO_15;
    @Expose
    @SerializedName("REASON_CODES_16_TO_20")
    private String reasonCodes16_TO_20;
    @Expose
    @SerializedName("REASON_CODES_21_TO_25")
    private String reasonCodes21_TO_25;
    @Expose
    @SerializedName("REASON_CODES_26_TO_30")
    private String reasonCodes26_TO_30;
    @Expose
    @SerializedName("REASON_CODES_31_TO_35")
    private String reasonCodes31_TO_35;
    @Expose
    @SerializedName("REASON_CODES_36_TO_40")
    private String reasonCodes36_TO_40;
    @Expose
    @SerializedName("REASON_CODES_41_TO_45")
    private String reasonCodes41_TO_45;
    @Expose
    @SerializedName("REASON_CODES_46_TO_50")
    private String reasonCodes46_TO_50;
    @Expose
    @SerializedName("ERROR_CODE_SC")
    private String errorCodeSC;
    @Expose
    @SerializedName("ADDRESS_LINE_1")
    private String addressLine1;
    @Expose
    @SerializedName("ADDRESS_LINE_2")
    private String addressLine2;
    @Expose
    @SerializedName("ADDRESS_LINE_3")
    private String addressLine3;
    @Expose
    @SerializedName("ADDRESS_LINE_4")
    private String addressLine4;
    @Expose
    @SerializedName("ADDRESS_LINE_5")
    private String addressLine5;
    @Expose
    @SerializedName("STATE_CODE")
    private Integer stateCode;
    @Expose
    @SerializedName("STATE")
    private String state;
    @Expose
    @SerializedName("PINCODE")
    private String pincode;
    @Expose
    @SerializedName("ADDRESS_CATERGORY")
    private String addressCatergoryValue;
    @Expose
    @SerializedName("RESIDENCE_CODE")
    private String residenceValue;
    @Expose
    @SerializedName("DATE_REPORTED_PA")
    private String dateReportedPA;
    @Expose
    @SerializedName("ENRICHED_THROUGH_ENQUIRY_PA")
    private String enrichedThroughEnquiryPA;
    @Expose
    @SerializedName("REPO_MEMSHORTNAME_TL")
    private String reportMemberShortNameTL;
    @Expose
    @SerializedName("ACCOUNT_NUMBER_TL")
    private String accountNumberTL;
    @Expose
    @SerializedName("ACCOUNT_TYPE_TL")
    private String accountTypeValueTL;
    @Expose
    @SerializedName("OWNERSHIP_INDICATOR_TL")
    private String ownershipIndicatorValueTL;
    @Expose
    @SerializedName("DATE_OPENED_DISBURSED_TL")
    private Date dateOpenedDisbursedTL;
    @Expose
    @SerializedName("DATE_OF_LAST_PAYMENT_TL")
    private Date dateOfLastPaymentTL;
    @Expose
    @SerializedName("DATE_CLOSED_TL")
    private Date dateClosedTL;
    @Expose
    @SerializedName("TL_DATE_REPORTED")
    private Date dateReportedTL;
    @Expose
    @SerializedName("HIGH_CREDIT_SANCTIONED_AMOUNT")
    private String highCreditSanctionedAmount;
    @Expose
    @SerializedName("CURRENT_BALANCE_TL")
    private String currentBalanceTL;
    @Expose
    @SerializedName("AMOUNT_OVERDUE_TL")
    private String amountOverdueTL;
    @Expose
    @SerializedName("PAYMENT_HISTORY_1")
    private String paymentHistory1;
    @Expose
    @SerializedName("PAYMENT_HISTORY_2")
    private String paymentHistory2;
    @Expose
    @SerializedName("PAYMENT_HISTORY_START_DATE")
    private Date paymentHistoryStartDate;
    @Expose
    @SerializedName("PAYMENT_HISTORY_END_DATE")
    private Date paymentHistoryEndDate;
    @Expose
    @SerializedName("SUIT_FILED_STATUS_TL")
    private String suitFiledStatusValueTL;
    @Expose
    @SerializedName("WOF_SETTLED_STATUS_TL")
    private String wofSettledStatusTL;
    @Expose
    @SerializedName("VALOFCOLLATERAL_TL")
    private String collateralValueTL;
    @Expose
    @SerializedName("TYPEOFCOLLATERAL_TL")
    private String typeOfCollateralTL;
    @Expose
    @SerializedName("CREDITLIMIT_TL")
    private String creditLimitTL;
    @Expose
    @SerializedName("CASHLIMIT_TL")
    private String cashLimitTL;
    @Expose
    @SerializedName("RATEOFINTREST_TL")
    private String rateOfIntrestTL;
    @Expose
    @SerializedName("REPAY_TENURE")
    private String repayTenure;
    @Expose
    @SerializedName("EMI_AMOUNT_TL")
    private String emiAmountTL;
    @Expose
    @SerializedName("WOF_TOT_AMOUNT_TL")
    private String wofTotalAmountTL;
    @Expose
    @SerializedName("WOF_PRINCIPAL_TL")
    private String wofPrincipalTL;
    @Expose
    @SerializedName("SETTLEMENT_AMOUNT_TL")
    private String settlementAmountTL;
    @Expose
    @SerializedName("PAYMENT_FREQUENCY_TL")
    private String paymentFrequencyTL;
    @Expose
    @SerializedName("ACTUAL_PAYMENT_AMOUNT_TL")
    private String actualPaymentAmountTL;
    @Expose
    @SerializedName("DT_ENTERRCODE_TL")
    private Date dateEntErrorCodeTL;
    @Expose
    @SerializedName("ERRCODE_TL")
    private String errorCodeTL;
    @Expose
    @SerializedName("DT_ENTCIBIL_REMARK_CODE_TL")
    private Date dateEntCIBILRemarkCodeTL;
    @Expose
    @SerializedName("CIBIL_REMARKS_CODE_TL")
    private String cibilRemarksCodeTL;
    @Expose
    @SerializedName("DT_DISPUTE_CODE_TL")
    private Date dateDisputeCodeTL;
    @Expose
    @SerializedName("ERR_DISP_REMCODE1_TL")
    private String errorDisputeRemarkCode1TL;
    @Expose
    @SerializedName("ERR_DISP_REMCODE2_TL")
    private String errorDisputeRemarkCode2TL;
    @Expose
    @SerializedName("DATE_OF_ENQUIRY_IQ")
    private Date dateOfEnquiryIQ;
    @Expose
    @SerializedName("ENQ_MEMBER_SHORT_NAME_IQ")
    private String enquiryMemberShortNameIQ;
    @Expose
    @SerializedName("ENQUIRY_PURPOSE_IQ")
    private String enquiryPurposeIQ;
    @Expose
    @SerializedName("ENQIURY_AMOUNT_IQ")
    private String enqiuryAmountIQ;
    @Expose
    @SerializedName("DATE_OF_ENTRY_DR")
    private Date dateOfEntryDR;
    @Expose
    @SerializedName("DISP_REM_LINE1")
    private String disputeRemarkLine1;
    @Expose
    @SerializedName("DISP_REM_LINE2")
    private String disputeRemarkLine2;
    @Expose
    @SerializedName("DISP_REM_LINE3")
    private String disputeRemarkLine3;
    @Expose
    @SerializedName("DISP_REM_LINE4")
    private String disputeRemarkLine4;
    @Expose
    @SerializedName("DISP_REM_LINE5")
    private String disputeRemarkLine5;
    @Expose
    @SerializedName("DISP_REM_LINE6")
    private String disputeRemarkLine6;
    @Expose
    @SerializedName("OUTPUT_WRITE_FLAG")
    private String outputWriteFlag;
    @Expose
    @SerializedName("OUTPUT_WRITE_TIME")
    private String outputWriteTime;
    @Expose
    @SerializedName("OUTPUT_READ_TIME")
    private String outputReadTime;


    public HibCIBILSropDomain(Integer srNo, String soaSourceName,
                              String memberReferenceNumber) {
        this.srNo = srNo;
        this.soaSourceName = soaSourceName;
        this.memberReferenceNo = memberReferenceNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSrNo() {
        return srNo;
    }

    public void setSrNo(Integer srNo) {
        this.srNo = srNo;
    }

    public String getSoaSourceName() {
        return soaSourceName;
    }

    public void setSoaSourceName(String soaSourceName) {
        this.soaSourceName = soaSourceName;
    }

    public Date getDateProcessed() {
        return dateProcessed;
    }

    public void setDateProcessed(Date dateProcessed) {
        this.dateProcessed = dateProcessed;
    }

    public String getMemberReferenceNo() {
        return memberReferenceNo;
    }

    public void setMemberReferenceNo(String memberReferenceNo) {
        this.memberReferenceNo = memberReferenceNo;
    }

    public String getSubjectReturnCode() {
        return subjectReturnCode;
    }

    public void setSubjectReturnCode(String subjectReturnCode) {
        this.subjectReturnCode = subjectReturnCode;
    }

    public String getEnquiryControlNumber() {
        return enquiryControlNumber;
    }

    public void setEnquiryControlNumber(String enquiryControlNumber) {
        this.enquiryControlNumber = enquiryControlNumber;
    }

    public String getConsumerNameField1() {
        return consumerNameField1;
    }

    public void setConsumerNameField1(String consumerNameField1) {
        this.consumerNameField1 = consumerNameField1;
    }

    public String getConsumerNameField2() {
        return consumerNameField2;
    }

    public void setConsumerNameField2(String consumerNameField2) {
        this.consumerNameField2 = consumerNameField2;
    }

    public String getConsumerNameField3() {
        return consumerNameField3;
    }

    public void setConsumerNameField3(String consumerNameField3) {
        this.consumerNameField3 = consumerNameField3;
    }

    public String getConsumerNameField4() {
        return consumerNameField4;
    }

    public void setConsumerNameField4(String consumerNameField4) {
        this.consumerNameField4 = consumerNameField4;
    }

    public String getConsumerNameField5() {
        return consumerNameField5;
    }

    public void setConsumerNameField5(String consumerNameField5) {
        this.consumerNameField5 = consumerNameField5;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGenderValue() {
        return genderValue;
    }

    public void setGenderValue(String genderValue) {
        this.genderValue = genderValue;
    }

    public Date getDateErrorCodePN() {
        return dateErrorCodePN;
    }

    public void setDateErrorCodePN(Date dateErrorCodePN) {
        this.dateErrorCodePN = dateErrorCodePN;
    }

    public String getErrorSegTagPN() {
        return errorSegTagPN;
    }

    public void setErrorSegTagPN(String errorSegTagPN) {
        this.errorSegTagPN = errorSegTagPN;
    }

    public String getErrorCodePN() {
        return errorCodePN;
    }

    public void setErrorCodePN(String errorCodePN) {
        this.errorCodePN = errorCodePN;
    }

    public String getErrorPN() {
        return errorPN;
    }

    public void setErrorPN(String errorPN) {
        this.errorPN = errorPN;
    }

    public Date getDateEntryCIBILRemarkCodePN() {
        return dateEntryCIBILRemarkCodePN;
    }

    public void setDateEntryCIBILRemarkCodePN(Date dateEntryCIBILRemarkCodePN) {
        this.dateEntryCIBILRemarkCodePN = dateEntryCIBILRemarkCodePN;
    }

    public String getCibilRemarkCodePN() {
        return cibilRemarkCodePN;
    }

    public void setCibilRemarkCodePN(String cibilRemarkCodePN) {
        this.cibilRemarkCodePN = cibilRemarkCodePN;
    }

    public Date getDateEntryErrorDisputeRemarkCodePN() {
        return dateEntryErrorDisputeRemarkCodePN;
    }

    public void setDateEntryErrorDisputeRemarkCodePN(
            Date dateEntryErrorDisputeRemarkCodePN) {
        this.dateEntryErrorDisputeRemarkCodePN = dateEntryErrorDisputeRemarkCodePN;
    }

    public String getErrorDisputeRemarkCode1PN() {
        return errorDisputeRemarkCode1PN;
    }

    public void setErrorDisputeRemarkCode1PN(String errorDisputeRemarkCode1PN) {
        this.errorDisputeRemarkCode1PN = errorDisputeRemarkCode1PN;
    }

    public String getErrorDisputeRemarkCode2PN() {
        return errorDisputeRemarkCode2PN;
    }

    public void setErrorDisputeRemarkCode2PN(String errorDisputeRemarkCode2PN) {
        this.errorDisputeRemarkCode2PN = errorDisputeRemarkCode2PN;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getEnrichedThroughEnquiryId() {
        return enrichedThroughEnquiryId;
    }

    public void setEnrichedThroughEnquiryId(String enrichedThroughEnquiryId) {
        this.enrichedThroughEnquiryId = enrichedThroughEnquiryId;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneExtension() {
        return telephoneExtension;
    }

    public void setTelephoneExtension(String telephoneExtension) {
        this.telephoneExtension = telephoneExtension;
    }

    public String getTelephoneTypeValue() {
        return telephoneTypeValue;
    }

    public void setTelephoneTypeValue(String telephoneTypeValue) {
        this.telephoneTypeValue = telephoneTypeValue;
    }

    public String getEnrichedThroughEnquiryPT() {
        return enrichedThroughEnquiryPT;
    }

    public void setEnrichedThroughEnquiryPT(String enrichedThroughEnquiryPT) {
        this.enrichedThroughEnquiryPT = enrichedThroughEnquiryPT;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAccountTypeValue() {
        return accountTypeValue;
    }

    public void setAccountTypeValue(String accountTypeValue) {
        this.accountTypeValue = accountTypeValue;
    }

    public Date getDateReportedAndCertifiedEM() {
        return dateReportedAndCertifiedEM;
    }

    public void setDateReportedAndCertifiedEM(Date dateReportedAndCertifiedEM) {
        this.dateReportedAndCertifiedEM = dateReportedAndCertifiedEM;
    }

    public String getOccupationValueEM() {
        return occupationValueEM;
    }

    public void setOccupationValueEM(String occupationValueEM) {
        this.occupationValueEM = occupationValueEM;
    }

    public String getIncomeValueEM() {
        return incomeValueEM;
    }

    public void setIncomeValueEM(String incomeValueEM) {
        this.incomeValueEM = incomeValueEM;
    }

    public String getNetGrossIndicatorValueEM() {
        return netGrossIndicatorValueEM;
    }

    public void setNetGrossIndicatorValueEM(String netGrossIndicatorValueEM) {
        this.netGrossIndicatorValueEM = netGrossIndicatorValueEM;
    }

    public String getMonthlyAnnualIndicatorValueEM() {
        return monthlyAnnualIndicatorValueEM;
    }

    public void setMonthlyAnnualIndicatorValueEM(
            String monthlyAnnualIndicatorValueEM) {
        this.monthlyAnnualIndicatorValueEM = monthlyAnnualIndicatorValueEM;
    }

    public Date getDateEntryErrorCodeEM() {
        return dateEntryErrorCodeEM;
    }

    public void setDateEntryErrorCodeEM(Date dateEntryErrorCodeEM) {
        this.dateEntryErrorCodeEM = dateEntryErrorCodeEM;
    }

    public String getErrorCodeEM() {
        return errorCodeEM;
    }

    public void setErrorCodeEM(String errorCodeEM) {
        this.errorCodeEM = errorCodeEM;
    }

    public Date getDateCIBILErrorCodeEM() {
        return dateCIBILErrorCodeEM;
    }

    public void setDateCIBILErrorCodeEM(Date dateCIBILErrorCodeEM) {
        this.dateCIBILErrorCodeEM = dateCIBILErrorCodeEM;
    }

    public String getCibilRemarkCodeEM() {
        return cibilRemarkCodeEM;
    }

    public void setCibilRemarkCodeEM(String cibilRemarkCodeEM) {
        this.cibilRemarkCodeEM = cibilRemarkCodeEM;
    }

    public Date getDateDisputeRemarkCodeEM() {
        return dateDisputeRemarkCodeEM;
    }

    public void setDateDisputeRemarkCodeEM(Date dateDisputeRemarkCodeEM) {
        this.dateDisputeRemarkCodeEM = dateDisputeRemarkCodeEM;
    }

    public String getErrorDisputeRemarkCode1EM() {
        return errorDisputeRemarkCode1EM;
    }

    public void setErrorDisputeRemarkCode1EM(String errorDisputeRemarkCode1EM) {
        this.errorDisputeRemarkCode1EM = errorDisputeRemarkCode1EM;
    }

    public String getErrorDisputeRemarkCode2EM() {
        return errorDisputeRemarkCode2EM;
    }

    public void setErrorDisputeRemarkCode2EM(String errorDisputeRemarkCode2EM) {
        this.errorDisputeRemarkCode2EM = errorDisputeRemarkCode2EM;
    }

    public String getAccountNumberPI() {
        return accountNumberPI;
    }

    public void setAccountNumberPI(String accountNumberPI) {
        this.accountNumberPI = accountNumberPI;
    }

    public String getScoreName() {
        return scoreName;
    }

    public void setScoreName(String scoreName) {
        this.scoreName = scoreName;
    }

    public String getScoreCardName() {
        return scoreCardName;
    }

    public void setScoreCardName(String scoreCardName) {
        this.scoreCardName = scoreCardName;
    }

    public String getScoreCardVersion() {
        return scoreCardVersion;
    }

    public void setScoreCardVersion(String scoreCardVersion) {
        this.scoreCardVersion = scoreCardVersion;
    }

    public Date getScoreDate() {
        return scoreDate;
    }

    public void setScoreDate(Date scoreDate) {
        this.scoreDate = scoreDate;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getExclusionCodes1_to_5() {
        return exclusionCodes1_to_5;
    }

    public void setExclusionCodes1_to_5(String exclusionCodes1_to_5) {
        this.exclusionCodes1_to_5 = exclusionCodes1_to_5;
    }

    public String getExclusionCodes6_TO_10() {
        return exclusionCodes6_TO_10;
    }

    public void setExclusionCodes6_TO_10(String exclusionCodes6_TO_10) {
        this.exclusionCodes6_TO_10 = exclusionCodes6_TO_10;
    }

    public String getExclusionCodes11_TO_15() {
        return exclusionCodes11_TO_15;
    }

    public void setExclusionCodes11_TO_15(String exclusionCodes11_TO_15) {
        this.exclusionCodes11_TO_15 = exclusionCodes11_TO_15;
    }

    public String getExclusionCodes16_TO_20() {
        return exclusionCodes16_TO_20;
    }

    public void setExclusionCodes16_TO_20(String exclusionCodes16_TO_20) {
        this.exclusionCodes16_TO_20 = exclusionCodes16_TO_20;
    }

    public String getReasonCodes1_TO_5() {
        return reasonCodes1_TO_5;
    }

    public void setReasonCodes1_TO_5(String reasonCodes1_TO_5) {
        this.reasonCodes1_TO_5 = reasonCodes1_TO_5;
    }

    public String getReasonCodes6_TO_10() {
        return reasonCodes6_TO_10;
    }

    public void setReasonCodes6_TO_10(String reasonCodes6_TO_10) {
        this.reasonCodes6_TO_10 = reasonCodes6_TO_10;
    }

    public String getReasonCodes11_TO_15() {
        return reasonCodes11_TO_15;
    }

    public void setReasonCodes11_TO_15(String reasonCodes11_TO_15) {
        this.reasonCodes11_TO_15 = reasonCodes11_TO_15;
    }

    public String getReasonCodes16_TO_20() {
        return reasonCodes16_TO_20;
    }

    public void setReasonCodes16_TO_20(String reasonCodes16_TO_20) {
        this.reasonCodes16_TO_20 = reasonCodes16_TO_20;
    }

    public String getReasonCodes21_TO_25() {
        return reasonCodes21_TO_25;
    }

    public void setReasonCodes21_TO_25(String reasonCodes21_TO_25) {
        this.reasonCodes21_TO_25 = reasonCodes21_TO_25;
    }

    public String getReasonCodes26_TO_30() {
        return reasonCodes26_TO_30;
    }

    public void setReasonCodes26_TO_30(String reasonCodes26_TO_30) {
        this.reasonCodes26_TO_30 = reasonCodes26_TO_30;
    }

    public String getReasonCodes31_TO_35() {
        return reasonCodes31_TO_35;
    }

    public void setReasonCodes31_TO_35(String reasonCodes31_TO_35) {
        this.reasonCodes31_TO_35 = reasonCodes31_TO_35;
    }

    public String getReasonCodes36_TO_40() {
        return reasonCodes36_TO_40;
    }

    public void setReasonCodes36_TO_40(String reasonCodes36_TO_40) {
        this.reasonCodes36_TO_40 = reasonCodes36_TO_40;
    }

    public String getReasonCodes41_TO_45() {
        return reasonCodes41_TO_45;
    }

    public void setReasonCodes41_TO_45(String reasonCodes41_TO_45) {
        this.reasonCodes41_TO_45 = reasonCodes41_TO_45;
    }

    public String getReasonCodes46_TO_50() {
        return reasonCodes46_TO_50;
    }

    public void setReasonCodes46_TO_50(String reasonCodes46_TO_50) {
        this.reasonCodes46_TO_50 = reasonCodes46_TO_50;
    }

    public String getErrorCodeSC() {
        return errorCodeSC;
    }

    public void setErrorCodeSC(String errorCodeSC) {
        this.errorCodeSC = errorCodeSC;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getAddressLine5() {
        return addressLine5;
    }

    public void setAddressLine5(String addressLine5) {
        this.addressLine5 = addressLine5;
    }

    public Integer getStateCode() {
        return stateCode;
    }

    public void setStateCode(Integer stateCode) {
        this.stateCode = stateCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddressCatergoryValue() {
        return addressCatergoryValue;
    }

    public void setAddressCatergoryValue(String addressCatergoryValue) {
        this.addressCatergoryValue = addressCatergoryValue;
    }

    public String getResidenceValue() {
        return residenceValue;
    }

    public void setResidenceValue(String residenceValue) {
        this.residenceValue = residenceValue;
    }

    public String getDateReportedPA() {
        return dateReportedPA;
    }

    public void setDateReportedPA(String dateReportedPA) {
        this.dateReportedPA = dateReportedPA;
    }

    public String getReportMemberShortNameTL() {
        return reportMemberShortNameTL;
    }

    public void setReportMemberShortNameTL(String reportMemberShortNameTL) {
        this.reportMemberShortNameTL = reportMemberShortNameTL;
    }

    public String getAccountNumberTL() {
        return accountNumberTL;
    }

    public void setAccountNumberTL(String accountNumberTL) {
        this.accountNumberTL = accountNumberTL;
    }

    public String getAccountTypeValueTL() {
        return accountTypeValueTL;
    }

    public void setAccountTypeValueTL(String accountTypeValueTL) {
        this.accountTypeValueTL = accountTypeValueTL;
    }

    public String getOwnershipIndicatorValueTL() {
        return ownershipIndicatorValueTL;
    }

    public void setOwnershipIndicatorValueTL(String ownershipIndicatorValueTL) {
        this.ownershipIndicatorValueTL = ownershipIndicatorValueTL;
    }

    public Date getDateOpenedDisbursedTL() {
        return dateOpenedDisbursedTL;
    }

    public void setDateOpenedDisbursedTL(Date dateOpenedDisbursedTL) {
        this.dateOpenedDisbursedTL = dateOpenedDisbursedTL;
    }

    public Date getDateOfLastPaymentTL() {
        return dateOfLastPaymentTL;
    }

    public void setDateOfLastPaymentTL(Date dateOfLastPaymentTL) {
        this.dateOfLastPaymentTL = dateOfLastPaymentTL;
    }

    public Date getDateClosedTL() {
        return dateClosedTL;
    }

    public void setDateClosedTL(Date dateClosedTL) {
        this.dateClosedTL = dateClosedTL;
    }

    public Date getDateReportedTL() {
        return dateReportedTL;
    }

    public void setDateReportedTL(Date dateReportedTL) {
        this.dateReportedTL = dateReportedTL;
    }

    public String getHighCreditSanctionedAmount() {
        return highCreditSanctionedAmount;
    }

    public void setHighCreditSanctionedAmount(String highCreditSanctionedAmount) {
        this.highCreditSanctionedAmount = highCreditSanctionedAmount;
    }

    public String getCurrentBalanceTL() {
        return currentBalanceTL;
    }

    public void setCurrentBalanceTL(String currentBalanceTL) {
        this.currentBalanceTL = currentBalanceTL;
    }

    public String getAmountOverdueTL() {
        return amountOverdueTL;
    }

    public void setAmountOverdueTL(String amountOverdueTL) {
        this.amountOverdueTL = amountOverdueTL;
    }

    public String getPaymentHistory1() {
        return paymentHistory1;
    }

    public void setPaymentHistory1(String paymentHistory1) {
        this.paymentHistory1 = paymentHistory1;
    }

    public String getPaymentHistory2() {
        return paymentHistory2;
    }

    public void setPaymentHistory2(String paymentHistory2) {
        this.paymentHistory2 = paymentHistory2;
    }

    public Date getPaymentHistoryStartDate() {
        return paymentHistoryStartDate;
    }

    public void setPaymentHistoryStartDate(Date paymentHistoryStartDate) {
        this.paymentHistoryStartDate = paymentHistoryStartDate;
    }

    public Date getPaymentHistoryEndDate() {
        return paymentHistoryEndDate;
    }

    public void setPaymentHistoryEndDate(Date paymentHistoryEndDate) {
        this.paymentHistoryEndDate = paymentHistoryEndDate;
    }

    public String getSuitFiledStatusValueTL() {
        return suitFiledStatusValueTL;
    }

    public void setSuitFiledStatusValueTL(String suitFiledStatusValueTL) {
        this.suitFiledStatusValueTL = suitFiledStatusValueTL;
    }

    public String getWofSettledStatusTL() {
        return wofSettledStatusTL;
    }

    public void setWofSettledStatusTL(String wofSettledStatusTL) {
        this.wofSettledStatusTL = wofSettledStatusTL;
    }

    public String getCollateralValueTL() {
        return collateralValueTL;
    }

    public void setCollateralValueTL(String collateralValueTL) {
        this.collateralValueTL = collateralValueTL;
    }

    public String getTypeOfCollateralTL() {
        return typeOfCollateralTL;
    }

    public void setTypeOfCollateralTL(String typeOfCollateralTL) {
        this.typeOfCollateralTL = typeOfCollateralTL;
    }

    public String getCreditLimitTL() {
        return creditLimitTL;
    }

    public void setCreditLimitTL(String creditLimitTL) {
        this.creditLimitTL = creditLimitTL;
    }

    public String getCashLimitTL() {
        return cashLimitTL;
    }

    public void setCashLimitTL(String cashLimitTL) {
        this.cashLimitTL = cashLimitTL;
    }

    public String getRateOfIntrestTL() {
        return rateOfIntrestTL;
    }

    public void setRateOfIntrestTL(String rateOfIntrestTL) {
        this.rateOfIntrestTL = rateOfIntrestTL;
    }

    public String getRepayTenure() {
        return repayTenure;
    }

    public void setRepayTenure(String repayTenure) {
        this.repayTenure = repayTenure;
    }

    public String getEmiAmountTL() {
        return emiAmountTL;
    }

    public void setEmiAmountTL(String emiAmountTL) {
        this.emiAmountTL = emiAmountTL;
    }

    public String getWofTotalAmountTL() {
        return wofTotalAmountTL;
    }

    public void setWofTotalAmountTL(String wofTotalAmountTL) {
        this.wofTotalAmountTL = wofTotalAmountTL;
    }

    public String getWofPrincipalTL() {
        return wofPrincipalTL;
    }

    public void setWofPrincipalTL(String wofPrincipalTL) {
        this.wofPrincipalTL = wofPrincipalTL;
    }

    public String getSettlementAmountTL() {
        return settlementAmountTL;
    }

    public void setSettlementAmountTL(String settlementAmountTL) {
        this.settlementAmountTL = settlementAmountTL;
    }

    public String getPaymentFrequencyTL() {
        return paymentFrequencyTL;
    }

    public void setPaymentFrequencyTL(String paymentFrequencyTL) {
        this.paymentFrequencyTL = paymentFrequencyTL;
    }

    public String getActualPaymentAmountTL() {
        return actualPaymentAmountTL;
    }

    public void setActualPaymentAmountTL(String actualPaymentAmountTL) {
        this.actualPaymentAmountTL = actualPaymentAmountTL;
    }

    public Date getDateEntErrorCodeTL() {
        return dateEntErrorCodeTL;
    }

    public void setDateEntErrorCodeTL(Date dateEntErrorCodeTL) {
        this.dateEntErrorCodeTL = dateEntErrorCodeTL;
    }

    public String getErrorCodeTL() {
        return errorCodeTL;
    }

    public void setErrorCodeTL(String errorCodeTL) {
        this.errorCodeTL = errorCodeTL;
    }

    public Date getDateEntCIBILRemarkCodeTL() {
        return dateEntCIBILRemarkCodeTL;
    }

    public void setDateEntCIBILRemarkCodeTL(Date dateEntCIBILRemarkCodeTL) {
        this.dateEntCIBILRemarkCodeTL = dateEntCIBILRemarkCodeTL;
    }

    public String getCibilRemarksCodeTL() {
        return cibilRemarksCodeTL;
    }

    public void setCibilRemarksCodeTL(String cibilRemarksCodeTL) {
        this.cibilRemarksCodeTL = cibilRemarksCodeTL;
    }

    public Date getDateDisputeCodeTL() {
        return dateDisputeCodeTL;
    }

    public void setDateDisputeCodeTL(Date dateDisputeCodeTL) {
        this.dateDisputeCodeTL = dateDisputeCodeTL;
    }

    public String getErrorDisputeRemarkCode1TL() {
        return errorDisputeRemarkCode1TL;
    }

    public void setErrorDisputeRemarkCode1TL(String errorDisputeRemarkCode1TL) {
        this.errorDisputeRemarkCode1TL = errorDisputeRemarkCode1TL;
    }

    public String getErrorDisputeRemarkCode2TL() {
        return errorDisputeRemarkCode2TL;
    }

    public void setErrorDisputeRemarkCode2TL(String errorDisputeRemarkCode2TL) {
        this.errorDisputeRemarkCode2TL = errorDisputeRemarkCode2TL;
    }

    public Date getDateOfEnquiryIQ() {
        return dateOfEnquiryIQ;
    }

    public void setDateOfEnquiryIQ(Date dateOfEnquiryIQ) {
        this.dateOfEnquiryIQ = dateOfEnquiryIQ;
    }

    public String getEnquiryMemberShortNameIQ() {
        return enquiryMemberShortNameIQ;
    }

    public void setEnquiryMemberShortNameIQ(String enquiryMemberShortNameIQ) {
        this.enquiryMemberShortNameIQ = enquiryMemberShortNameIQ;
    }

    public String getEnquiryPurposeIQ() {
        return enquiryPurposeIQ;
    }

    public void setEnquiryPurposeIQ(String enquiryPurposeIQ) {
        this.enquiryPurposeIQ = enquiryPurposeIQ;
    }

    public String getEnqiuryAmountIQ() {
        return enqiuryAmountIQ;
    }

    public void setEnqiuryAmountIQ(String enqiuryAmountIQ) {
        this.enqiuryAmountIQ = enqiuryAmountIQ;
    }

    public Date getDateOfEntryDR() {
        return dateOfEntryDR;
    }

    public void setDateOfEntryDR(Date dateOfEntryDR) {
        this.dateOfEntryDR = dateOfEntryDR;
    }

    public String getDisputeRemarkLine1() {
        return disputeRemarkLine1;
    }

    public void setDisputeRemarkLine1(String disputeRemarkLine1) {
        this.disputeRemarkLine1 = disputeRemarkLine1;
    }

    public String getDisputeRemarkLine2() {
        return disputeRemarkLine2;
    }

    public void setDisputeRemarkLine2(String disputeRemarkLine2) {
        this.disputeRemarkLine2 = disputeRemarkLine2;
    }

    public String getDisputeRemarkLine3() {
        return disputeRemarkLine3;
    }

    public void setDisputeRemarkLine3(String disputeRemarkLine3) {
        this.disputeRemarkLine3 = disputeRemarkLine3;
    }

    public String getDisputeRemarkLine4() {
        return disputeRemarkLine4;
    }

    public void setDisputeRemarkLine4(String disputeRemarkLine4) {
        this.disputeRemarkLine4 = disputeRemarkLine4;
    }

    public String getDisputeRemarkLine5() {
        return disputeRemarkLine5;
    }

    public void setDisputeRemarkLine5(String disputeRemarkLine5) {
        this.disputeRemarkLine5 = disputeRemarkLine5;
    }

    public String getDisputeRemarkLine6() {
        return disputeRemarkLine6;
    }

    public void setDisputeRemarkLine6(String disputeRemarkLine6) {
        this.disputeRemarkLine6 = disputeRemarkLine6;
    }

    public String getOutputWriteFlag() {
        return outputWriteFlag;
    }

    public void setOutputWriteFlag(String outputWriteFlag) {
        this.outputWriteFlag = outputWriteFlag;
    }

    public String getOutputWriteTime() {
        return outputWriteTime;
    }

    public void setOutputWriteTime(String outputWriteTime) {
        this.outputWriteTime = outputWriteTime;
    }

    public String getOutputReadTime() {
        return outputReadTime;
    }

    public void setOutputReadTime(String outputReadTime) {
        this.outputReadTime = outputReadTime;
    }

    public String getEnrichedThroughEnquiryPA() {
        return enrichedThroughEnquiryPA;
    }

    public void setEnrichedThroughEnquiryPA(String enrichedThroughEnquiryPA) {
        this.enrichedThroughEnquiryPA = enrichedThroughEnquiryPA;
    }

    @Override
    public String toString() {
        return "CibilSropDomain [id=" + id + ", srNo=" + srNo
                + ", soaSourceName=" + soaSourceName + ", dateProcessed="
                + dateProcessed + ", memberReferenceNo=" + memberReferenceNo
                + ", subjectReturnCode=" + subjectReturnCode
                + ", enquiryControlNumber=" + enquiryControlNumber
                + ", consumerNameField1=" + consumerNameField1
                + ", consumerNameField2=" + consumerNameField2
                + ", consumerNameField3=" + consumerNameField3
                + ", consumerNameField4=" + consumerNameField4
                + ", consumerNameField5=" + consumerNameField5
                + ", dateOfBirth=" + dateOfBirth + ", genderValue="
                + genderValue + ", dateErrorCodePN=" + dateErrorCodePN
                + ", errorSegTagPN=" + errorSegTagPN + ", errorCodePN="
                + errorCodePN + ", errorPN=" + errorPN
                + ", dateEntryCIBILRemarkCodePN=" + dateEntryCIBILRemarkCodePN
                + ", cibilRemarkCodePN=" + cibilRemarkCodePN
                + ", dateEntryErrorDisputeRemarkCodePN="
                + dateEntryErrorDisputeRemarkCodePN
                + ", errorDisputeRemarkCode1PN=" + errorDisputeRemarkCode1PN
                + ", errorDisputeRemarkCode2PN=" + errorDisputeRemarkCode2PN
                + ", idType=" + idType + ", idNumber=" + idNumber
                + ", issueDate=" + issueDate + ", expirationDate="
                + expirationDate + ", enrichedThroughEnquiryId="
                + enrichedThroughEnquiryId + ", telephoneNumber="
                + telephoneNumber + ", telephoneExtension="
                + telephoneExtension + ", telephoneTypeValue="
                + telephoneTypeValue + ", enrichedThroughEnquiryPT="
                + enrichedThroughEnquiryPT + ", emailId=" + emailId
                + ", accountTypeValue=" + accountTypeValue
                + ", dateReportedAndCertifiedEM=" + dateReportedAndCertifiedEM
                + ", occupationValueEM=" + occupationValueEM
                + ", incomeValueEM=" + incomeValueEM
                + ", netGrossIndicatorValueEM=" + netGrossIndicatorValueEM
                + ", monthlyAnnualIndicatorValueEM="
                + monthlyAnnualIndicatorValueEM + ", dateEntryErrorCodeEM="
                + dateEntryErrorCodeEM + ", errorCodeEM=" + errorCodeEM
                + ", dateCIBILErrorCodeEM=" + dateCIBILErrorCodeEM
                + ", cibilRemarkCodeEM=" + cibilRemarkCodeEM
                + ", dateDisputeRemarkCodeEM=" + dateDisputeRemarkCodeEM
                + ", errorDisputeRemarkCode1EM=" + errorDisputeRemarkCode1EM
                + ", errorDisputeRemarkCode2EM=" + errorDisputeRemarkCode2EM
                + ", accountNumberPI=" + accountNumberPI + ", scoreName="
                + scoreName + ", scoreCardName=" + scoreCardName
                + ", scoreCardVersion=" + scoreCardVersion + ", scoreDate="
                + scoreDate + ", score=" + score + ", exclusionCodes1_to_5="
                + exclusionCodes1_to_5 + ", exclusionCodes6_TO_10="
                + exclusionCodes6_TO_10 + ", exclusionCodes11_TO_15="
                + exclusionCodes11_TO_15 + ", exclusionCodes16_TO_20="
                + exclusionCodes16_TO_20 + ", reasonCodes1_TO_5="
                + reasonCodes1_TO_5 + ", reasonCodes6_TO_10="
                + reasonCodes6_TO_10 + ", reasonCodes11_TO_15="
                + reasonCodes11_TO_15 + ", reasonCodes16_TO_20="
                + reasonCodes16_TO_20 + ", reasonCodes21_TO_25="
                + reasonCodes21_TO_25 + ", reasonCodes26_TO_30="
                + reasonCodes26_TO_30 + ", reasonCodes31_TO_35="
                + reasonCodes31_TO_35 + ", reasonCodes36_TO_40="
                + reasonCodes36_TO_40 + ", reasonCodes41_TO_45="
                + reasonCodes41_TO_45 + ", reasonCodes46_TO_50="
                + reasonCodes46_TO_50 + ", errorCodeSC=" + errorCodeSC
                + ", addressLine1=" + addressLine1 + ", addressLine2="
                + addressLine2 + ", addressLine3=" + addressLine3
                + ", addressLine4=" + addressLine4 + ", addressLine5="
                + addressLine5 + ", stateCode=" + stateCode + ", state="
                + state + ", pincode=" + pincode + ", addressCatergoryValue="
                + addressCatergoryValue + ", residenceValue=" + residenceValue
                + ", dateReportedPA=" + dateReportedPA
                + ", enrichedThroughEnquiryPA=" + enrichedThroughEnquiryPA
                + ", reportMemberShortNameTL=" + reportMemberShortNameTL
                + ", accountNumberTL=" + accountNumberTL
                + ", accountTypeValueTL=" + accountTypeValueTL
                + ", ownershipIndicatorValueTL=" + ownershipIndicatorValueTL
                + ", dateOpenedDisbursedTL=" + dateOpenedDisbursedTL
                + ", dateOfLastPaymentTL=" + dateOfLastPaymentTL
                + ", dateClosedTL=" + dateClosedTL + ", dateReportedTL="
                + dateReportedTL + ", highCreditSanctionedAmount="
                + highCreditSanctionedAmount + ", currentBalanceTL="
                + currentBalanceTL + ", amountOverdueTL=" + amountOverdueTL
                + ", paymentHistory1=" + paymentHistory1 + ", paymentHistory2="
                + paymentHistory2 + ", paymentHistoryStartDate="
                + paymentHistoryStartDate + ", paymentHistoryEndDate="
                + paymentHistoryEndDate + ", suitFiledStatusValueTL="
                + suitFiledStatusValueTL + ", wofSettledStatusTL="
                + wofSettledStatusTL + ", collateralValueTL="
                + collateralValueTL + ", typeOfCollateralTL="
                + typeOfCollateralTL + ", creditLimitTL=" + creditLimitTL
                + ", cashLimitTL=" + cashLimitTL + ", rateOfIntrestTL="
                + rateOfIntrestTL + ", repayTenure=" + repayTenure
                + ", emiAmountTL=" + emiAmountTL + ", wofTotalAmountTL="
                + wofTotalAmountTL + ", wofPrincipalTL=" + wofPrincipalTL
                + ", settlementAmountTL=" + settlementAmountTL
                + ", paymentFrequencyTL=" + paymentFrequencyTL
                + ", actualPaymentAmountTL=" + actualPaymentAmountTL
                + ", dateEntErrorCodeTL=" + dateEntErrorCodeTL
                + ", errorCodeTL=" + errorCodeTL
                + ", dateEntCIBILRemarkCodeTL=" + dateEntCIBILRemarkCodeTL
                + ", cibilRemarksCodeTL=" + cibilRemarksCodeTL
                + ", dateDisputeCodeTL=" + dateDisputeCodeTL
                + ", errorDisputeRemarkCode1TL=" + errorDisputeRemarkCode1TL
                + ", errorDisputeRemarkCode2TL=" + errorDisputeRemarkCode2TL
                + ", dateOfEnquiryIQ=" + dateOfEnquiryIQ
                + ", enquiryMemberShortNameIQ=" + enquiryMemberShortNameIQ
                + ", enquiryPurposeIQ=" + enquiryPurposeIQ
                + ", enqiuryAmountIQ=" + enqiuryAmountIQ + ", dateOfEntryDR="
                + dateOfEntryDR + ", disputeRemarkLine1=" + disputeRemarkLine1
                + ", disputeRemarkLine2=" + disputeRemarkLine2
                + ", disputeRemarkLine3=" + disputeRemarkLine3
                + ", disputeRemarkLine4=" + disputeRemarkLine4
                + ", disputeRemarkLine5=" + disputeRemarkLine5
                + ", disputeRemarkLine6=" + disputeRemarkLine6
                + ", outputWriteFlag=" + outputWriteFlag + ", outputWriteTime="
                + outputWriteTime + ", outputReadTime=" + outputReadTime + "]";
    }


}
