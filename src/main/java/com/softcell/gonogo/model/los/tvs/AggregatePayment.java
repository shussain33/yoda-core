package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AggregatePayment {

	 private String prepaidPostpaidFlag;

	    private String averageElectricityBillAmountLast3Mon;

	    private String avgGrossUtilitySpendLast3Mon;

	    private String averageMobileRechargeAmountLast3Mon;

	    private String averageInternetBillAmountLast3Mon;

	    private String averageLandlineBillAmountLast3Mon;

	    private String averageMobileBillAmountLast3Mon;


}
