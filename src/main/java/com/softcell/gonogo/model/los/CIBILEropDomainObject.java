/**
 *
 */
package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 */
public class CIBILEropDomainObject implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Expose
    @SerializedName("CIBIL_EROP_DOMAIN_LIST")
    List<CIBILEROPErrorDomain> cibilEropDomainList;

    public List<CIBILEROPErrorDomain> getCibilEropDomainList() {
        return cibilEropDomainList;
    }

    public void setCibilEropDomainList(
            List<CIBILEROPErrorDomain> cibilEropDomainList) {
        this.cibilEropDomainList = cibilEropDomainList;
    }

    @Override
    public String toString() {
        return "CIBILEropDomainObject [cibilEropDomainList="
                + cibilEropDomainList + "]";
    }


}
