package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContactInfo {

	 private AlternateContact alternateContact;

	    private MobileNos[] mobileNos;

	    private EmailIds[] emailIds;


}
