package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class DrivedAttributes {

	private String lengthOfCreditHistoryYear;

    private String inquiriesInLastSixMonth;

    private String newAccountInLastSixMonths;

    private String averageAccountAgeYear;

    private String averageAccountAgeMonth;

    private String newDlinqAccountInLastSixMonths;

    private String lengthOfCreditHistoryMonth;


}
