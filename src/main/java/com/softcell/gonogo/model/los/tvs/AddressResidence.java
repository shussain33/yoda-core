package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressResidence {

		 private int monthataddress;

	     private String addressline2;

	     private String addressline1;

	     private int pin;

	     private String state;

	     private String residenceaddresstype;

	     private String city;

}
