package com.softcell.gonogo.model.los.tvs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsertOrUpdateTvsRecordGroupOne {

    private PersonalDetails personalDetails;

    private AddressResidence addressResidence;

    private AddressOffice addressOffice;

    private AadharDetails aadharDetails;

    private Professionaldetails professionaldetails;

    private ContactDetails contactDetails;

    @JsonProperty("CDLos")
    private CDLos CDLos;

    private PanDetails panDetails;

    private Decision decision;

    private CreditLoanDetails[] creditLoanDetails;

    private AddressPermanent addressPermanent;


}