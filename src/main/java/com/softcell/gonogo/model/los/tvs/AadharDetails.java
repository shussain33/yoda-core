package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AadharDetails {

	private String landmark;

    private String mobilenumber;

    private String name;

    private String aadharnumber;

    private String locality;

    private String houseidentifier;

    private String dateofbirth;

    private String district;

    private String fatherspousename;

    private String subdistrict;

    private String villagecity;

    private String streetname;

    
}
