package com.softcell.gonogo.model.los;

public class LosFeedResponse {

    private String responseXml;

    /**
     * @return the responseXml
     */
    public String getResponseXml() {
        return responseXml;
    }

    /**
     * @param responseXml the responseXml to set
     */
    public void setResponseXml(String responseXml) {
        this.responseXml = responseXml;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LosFeedResponse [responseXml=");
        builder.append(responseXml);
        builder.append("]");
        return builder.toString();
    }


}
