package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PanDetails {

	private String paninfo;

    private String lastupdatedate;

    private String pantitle;

    private String panstatus;

    private String lastname;

    private String firstname;

    private String pannumber;


}
