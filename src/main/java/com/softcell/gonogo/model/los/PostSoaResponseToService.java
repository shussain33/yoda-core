package com.softcell.gonogo.model.los;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ReqResHelperConstants;
import com.softcell.gonogo.model.core.request.scoring.CibilErrorResponse;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.mbdatapush.*;
import com.softcell.gonogo.model.mbdatapush.chm.AckReportFile;
import com.softcell.gonogo.model.mbdatapush.chm.BaseReportFile;
import com.softcell.gonogo.model.multibureau.experian.InProfileResponse;
import com.softcell.soap.mb.hdfcbank.xsd.multibureau.*;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

public class PostSoaResponseToService {
    private static Logger logger = LoggerFactory.getLogger(PostSoaResponseToService.class);
    private Object response;
    private Object ackId;
    private String trackId;
    private String bureau;
    private int lastAtmpt;
    private boolean dlQueue;


    public boolean isDlQueue() {
        return dlQueue;
    }

    public void setDlQueue(boolean dlQueue) {
        this.dlQueue = dlQueue;
    }

    public int getLastAtmpt() {
        return lastAtmpt;
    }

    public void setLastAtmpt(int lastAtmpt) {
        this.lastAtmpt = lastAtmpt;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public Object getAckId() {
        return ackId;
    }

    public void setAckId(Object ackId) {
        this.ackId = ackId;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "PostSoaResponseToService [response=" + response + ", ackId="
                + ackId + ", trackId=" + trackId + ", bureau=" + bureau
                + ", lastAtmpt=" + lastAtmpt + ", dlQueue=" + dlQueue + "]";
    }

    public Object convert(Object response,LosMbData losMbData) throws Exception {
        return convert_(response,losMbData);

    }

    private Object convert_(Object response,LosMbData losMbData) throws Exception {
//		String ackid = null;
        try {
            if (response instanceof IssueResponseMB) {
                try {
                    IssueResponseMB issueresponse = (IssueResponseMB) response;

                    String json = new Gson().toJson(issueresponse);
                    if (issueresponse != null) {
                        ResponseType responseType = new ResponseType();
                        ResponseHeader headerObj = issueresponse.getHeaderObj();
                        if (headerObj != null) {
                            AckHeaderType ackHeaderType = new AckHeaderType();
                            if (StringUtils.isNotBlank(headerObj.getApplicationId()))
                                ackHeaderType.setAPPLICATIONID(headerObj.getApplicationId() == null ? "" : headerObj.getApplicationId());
                            if (StringUtils.isNotBlank(headerObj.getCustId()))
                                ackHeaderType.setCUSTID(headerObj.getCustId() == null ? "" : headerObj.getCustId());
                            if (StringUtils.isNotBlank(headerObj.getReqRcvdTime()))
                                ackHeaderType.setREQUESTRECEIVEDTIME(headerObj.getReqRcvdTime() == null ? "" : headerObj.getReqRcvdTime());
                            if (StringUtils.isNotBlank(headerObj.getResponseType()))
                                ackHeaderType.setRESPONSETYPE(headerObj.getResponseType() == null ? "" : headerObj.getResponseType());
                            if (StringUtils.isNotBlank(headerObj.getSourceSystem()))
                                ackHeaderType.setSOURCESYSTEM(headerObj.getSourceSystem() == null ? "" : headerObj.getSourceSystem());
                            responseType.setHEADER(ackHeaderType);
                        }

                        if (issueresponse.getAckId() != null)
                            responseType.setACKNOWLEDGEMENTID(issueresponse.getAckId() == null ? "" : issueresponse.getAckId().toString());
//					ackid=  issueresponse.getAckId().toString();
                        setAckId(issueresponse.getAckId().toString());
                        if (StringUtils.isNotBlank(issueresponse.getStatus()))
                            responseType.setSTATUS(issueresponse.getStatus() == null ? "" : issueresponse.getStatus());
                        List<FinishedDomain> finishedObjectList = issueresponse.getFinishedObject();
                        if (finishedObjectList != null && finishedObjectList.size() > 0) {
                            List<FinishedType> finishedTypeList = new ArrayList<FinishedType>();

                            for (FinishedDomain finishedDomain : finishedObjectList) {
                                FinishedType finishedType = new FinishedType();
                                if (StringUtils.isNotBlank(finishedDomain.getBureau()))
                                    finishedType.setBUREAU(finishedDomain.getBureau() == null ? "" : finishedDomain.getBureau());
                                if (StringUtils.isNotBlank(finishedDomain.getBureauString()))
                                    finishedType.setBUREAUSTRING(finishedDomain.getBureauString() == null ? "" : finishedDomain.getBureauString());
                                if (StringUtils.isNotBlank(finishedDomain.getHtmlReport()))
                                    finishedType.setHTMLREPORT(finishedDomain.getHtmlReport() == null ? "" : finishedDomain.getHtmlReport());
                                if (finishedDomain.getTrackingId() != null)
                                    finishedType.setTRACKINGID(finishedDomain.getTrackingId() == null ? "" : finishedDomain.getTrackingId().toString());
                                if (StringUtils.isNotBlank(finishedDomain.getProduct()))
                                    finishedType.setPRODUCT(finishedDomain.getProduct() == null ? "" : finishedDomain.getProduct());
                                if (StringUtils.isNotBlank(finishedDomain.getStatus())) {

                                    if (StringUtils.equals(finishedDomain.getStatus(), GNGWorkflowConstant.BUREAU_SUCCESS.toFaceValue()))
                                    {
                                        finishedType.setSTATUS(GNGWorkflowConstant.SUCCESS_SOA.toFaceValue());
                                    }else{
                                        finishedType.setSTATUS(finishedDomain.getStatus() == null ? "" : finishedDomain.getStatus());
                                    }
                                }
                                if (finishedDomain.getPdfReport() != null && finishedDomain.getPdfReport().length > 0)
                                    finishedType.setPDFREPORT(finishedDomain.getPdfReport() == null ? "" : Base64.getEncoder().encodeToString(finishedDomain.getPdfReport()));

                                setTrackId(finishedDomain.getTrackingId().toString());
                                //Object responseJsonObject = finishedDomain.getResponseJsonObject();
                                Object sropObject = getSropEropObject(finishedDomain, issueresponse,losMbData);
                                if (sropObject != null) {
                                    try {
                                        checkResponseJsonObject(sropObject, finishedType);
                                    } catch (Exception e) {
                                        logger.error(e.getMessage(), e);
                                        e.printStackTrace();
                                    }

                                }

                                finishedTypeList.add(finishedType);
                            }
                            responseType.setFINISHED(finishedTypeList.toArray(new FinishedType[finishedTypeList.size()]));
                        }

                        List<RejectDomain> rejectObjectList = issueresponse.getRejectObject();
                        if (rejectObjectList != null && rejectObjectList.size() > 0) {
                            List<RejectType> rejectTypeList = new ArrayList<RejectType>();
                            for (RejectDomain rejectDomain : rejectObjectList) {
                                RejectType rejectType = new RejectType();
                                List<Errors> errorObject = rejectDomain.getErrorObject();
                                if (errorObject != null && errorObject.size() > 0) {
                                    List<ErrorsType> errorsTypeList = new ArrayList<ErrorsType>();
                                    for (Errors errors : errorObject) {
                                        ErrorsType errorsType = new ErrorsType();
                                        if (StringUtils.isNotBlank(errors.getCode()))
                                            errorsType.setCODE(errors.getCode() == null ? "" : errors.getCode());
                                        if (StringUtils.isNotBlank(errors.getDescription()))
                                            errorsType.setDESCRIPTION(errors.getDescription() == null ? "" : errors.getDescription());
                                        errorsTypeList.add(errorsType);
                                    }
                                    rejectType.setERRORS(errorsTypeList.toArray(new ErrorsType[errorsTypeList.size()]));
                                }
                                if (StringUtils.isNotBlank(rejectDomain.getProduct()))
                                    rejectType.setPRODUCT(rejectDomain.getProduct() == null ? "" : rejectDomain.getProduct());
                                if (StringUtils.isNotBlank(rejectDomain.getStatus()))
                                    rejectType.setSTATUS(rejectDomain.getStatus() == null ? "" : rejectDomain.getStatus());
                                if (StringUtils.isNotBlank(rejectDomain.getBureau()))
                                    rejectType.setBUREAU(rejectDomain.getBureau() == null ? "" : rejectDomain.getBureau());
                                if (rejectDomain.getTrackingId() != null)
                                    rejectType.setTRACKINGID(rejectDomain.getTrackingId() == null ? "" : rejectDomain.getTrackingId().toString());
                                rejectTypeList.add(rejectType);

                                setTrackId(rejectDomain.getTrackingId().toString());

                            }
                            responseType.setREJECT(rejectTypeList.toArray(new RejectType[rejectTypeList.size()]));
                        }
                        MBIssueResponseType mbIssueResponseType = new MBIssueResponseType();
                        mbIssueResponseType.setRESPONSE(responseType);
                        return mbIssueResponseType;
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    e.printStackTrace();
                }

            } else if (response instanceof EndOfTxn) {


                try {
                    EndOfTxn endOfTxn = (EndOfTxn) response;

                    String json = new Gson().toJson(endOfTxn);

                    if (endOfTxn != null) {
                        ResponseHeader headerObj = endOfTxn.getHeaderObj();
                        MBEoTRequestType mbEoTRequestType = new MBEoTRequestType();
                        if (headerObj != null) {
                            EotHeaderType eotHeaderType = new EotHeaderType();
                            if (StringUtils.isNotBlank(headerObj.getApplicationId()))
                                eotHeaderType.setAPPLICATIONID(headerObj.getApplicationId() == null ? "" : headerObj.getApplicationId());
                            if (StringUtils.isNotBlank(headerObj.getCustId()))
                                eotHeaderType.setCUSTID(headerObj.getCustId() == null ? "" : headerObj.getCustId());
                            if (StringUtils.isNotBlank(headerObj.getReqRcvdTime()))
                                eotHeaderType.setREQUESTRECEIVEDTIME(headerObj.getReqRcvdTime() == null ? "" : headerObj.getReqRcvdTime());
                            if (StringUtils.isNotBlank(headerObj.getResponseType()))
                                eotHeaderType.setRESPONSETYPE(headerObj.getResponseType() == null ? "" : headerObj.getResponseType());
                            if (StringUtils.isNotBlank(headerObj.getSourceSystem()))
                                eotHeaderType.setSOURCESYSTEM(headerObj.getSourceSystem() == null ? "" : headerObj.getSourceSystem());
                            mbEoTRequestType.setHEADER(eotHeaderType);
                            if (StringUtils.isNotBlank(endOfTxn.getAckId()))
                                mbEoTRequestType.setACKNOWLEDGEMENTID(endOfTxn.getAckId() == null ? "" : endOfTxn.getAckId());
                            setAckId(endOfTxn.getAckId().toString());
                            mbEoTRequestType.setSTATUS(endOfTxn.getStatus());
                            //mbEoTRequestType.setSENTTOCHM(endOfTxn.getSentToChm()==null?"N":endOfTxn.getSentToChm());
                            mbEoTRequestType.setSENTTOCIBIL(endOfTxn.getSentToCibil() == null ? "N" : endOfTxn.getSentToCibil());
                            //mbEoTRequestType.setSENTTOEQUIFAX(endOfTxn.getSentToEquifax()==null?"N":endOfTxn.getSentToEquifax());
                            //mbEoTRequestType.setSENTTOEXPERIAN(endOfTxn.getSentToExperian()==null?"N":endOfTxn.getSentToExperian());
                            //mbEoTRequestType.setSENTMERGEDREPORT(endOfTxn.getSentMergedReport()==null?"N":endOfTxn.getSentMergedReport());
                            //mbEoTRequestType.setSENTSCORE(endOfTxn.getSentScore()==null?"N":endOfTxn.getSentScore());
                        }
                        return mbEoTRequestType;
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    e.printStackTrace();
                }

            } else {
                logger.error("response not of type IssueResponseMB and EOT");
            }
            return "";
        } catch (Exception e) {
            throw new Exception("Exception in converting response to soa Response");
        }

    }

    private Object getSropEropObject(FinishedDomain finishedDomain, IssueResponseMB issueresponse, LosMbData losMbData) throws Exception {

        String memberRefNum = losMbData != null ? losMbData.getLosMbFileName() : issueresponse.getHeaderObj().getApplicationId();

        switch (GNGWorkflowConstant.valueOf(finishedDomain.getBureau())) {
            case CIBIL:
                return getCibilSropEropObject(finishedDomain, issueresponse, memberRefNum);
            case EXPERIAN:
                return getExperianSropEropObject(finishedDomain, issueresponse, memberRefNum);
            case HIGHMARK:
                return getHighmarkBaseSropEropObject(finishedDomain, issueresponse, memberRefNum);
            default:
                return null;
        }
    }

    private FinishedType checkResponseJsonObject(Object responseJsonObject, FinishedType finishedType) {
        logger.debug("** PostSoaResponseToService >> checkResponseJsonObject");
        ResponseJSONType responseJSONType = new ResponseJSONType();
        com.softcell.gonogo.model.mbdatapush.SoaSropEropMapper soaSropEropMapper = new com.softcell.gonogo.model.mbdatapush.SoaSropEropMapper();

        if (responseJsonObject instanceof CIBILSropDomainObject) {
            soaSropEropMapper.cibilSropMapping(responseJsonObject, responseJSONType);
        } else if (responseJsonObject instanceof CIBILEropDomainObject) {
            soaSropEropMapper.cibilEropMapping(responseJsonObject, responseJSONType);
        } else if (responseJsonObject instanceof ExperianEropDomainObject) {
            soaSropEropMapper.experianEropMapping(responseJsonObject, responseJSONType);
        } else if (responseJsonObject instanceof ExperianSropDomainObject) {
            soaSropEropMapper.experianSropMapping(responseJsonObject, responseJSONType);
        } else if (responseJsonObject instanceof CHMBaseSropDomainObject) {
            soaSropEropMapper.chmBaseSropMapping(responseJsonObject, responseJSONType);
        } else if (responseJsonObject instanceof CHMBaseEropDomainObject) {
            soaSropEropMapper.chmBaseEropMapping(responseJsonObject, responseJSONType);
        }

        finishedType.setJSONRESPONSEOBJECT(responseJSONType);

        return finishedType;
    }

    private Object getCibilSropEropObject(FinishedDomain finishedDomain, IssueResponseMB issueresponse, String memberRefNum) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String cibilObj = GngUtils.getBureauResponse(finishedDomain.getResponseJsonObject());

        if (StringUtils.equals(finishedDomain.getStatus(), GNGWorkflowConstant.BUREAU_SUCCESS.toFaceValue())) {

            CIBILSropDomainObject cibilSropDomainObject = new CIBILSropDomainObject();
            CibilSropDomainPopulation cibilSropDomainPopulation = new CibilSropDomainPopulation();

            CibilResponse cibilResponse =objectMapper.readValue(cibilObj, CibilResponse.class);

            List<HibCIBILSropDomain> hibCIBILSropDomains =
                    cibilSropDomainPopulation.cibilSropDomainGenerator(cibilResponse, issueresponse.getAckId().toString(),
                            ReqResHelperConstants.GONOGO.name(), memberRefNum,
                            Long.MIN_VALUE);

            cibilSropDomainObject.setCibilSropDomainList(hibCIBILSropDomains);

            return cibilSropDomainObject;

        } else if (StringUtils.equals(finishedDomain.getStatus(), GNGWorkflowConstant.BUREAU_ERROR.toFaceValue())) {

            CIBILEropDomainObject cibilEropDomainObject = new CIBILEropDomainObject();
            CIBILEROPErrorDomainPopulation cibileropErrorDomainPopulation = new CIBILEROPErrorDomainPopulation();

            CibilErrorResponse cibilErrorResponse = objectMapper.readValue(cibilObj, CibilErrorResponse.class);

            List<CIBILEROPErrorDomain> cibileropErrorDomains = cibileropErrorDomainPopulation.cibilEROPErrorDomainGenerator(cibilErrorResponse, issueresponse.getAckId().toString(),
                    ReqResHelperConstants.GONOGO.name(), memberRefNum,
                    Long.MIN_VALUE, new Date());

            cibilEropDomainObject.setCibilEropDomainList(cibileropErrorDomains);
            return cibilEropDomainObject;
        }
        return null;
    }


    private Object getExperianSropEropObject(FinishedDomain finishedDomain, IssueResponseMB issueresponse, String memberRefNum) throws Exception {


        String experianRespString = GngUtils.getBureauResponse(finishedDomain.getResponseJsonObject());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        InProfileResponse inProfileResponse = objectMapper.readValue(experianRespString, InProfileResponse.class);

        if (StringUtils.equals(finishedDomain.getStatus(), GNGWorkflowConstant.BUREAU_SUCCESS.toFaceValue())) {
            ExperianSropDomainObject experianSropDomainObject = new ExperianSropDomainObject();
            ExperianSropDomainPopulation experianSropDomainPopulation = new ExperianSropDomainPopulation();

            List<HibExperianSropDomain> hibExperianSropDomains = experianSropDomainPopulation.generateSropDomain(inProfileResponse, issueresponse.getAckId().toString(),
                    ReqResHelperConstants.GONOGO.name(), memberRefNum, new Date());
            experianSropDomainObject.setExperianSropDomainList(hibExperianSropDomains);
            return experianSropDomainObject;

        } else if (StringUtils.equals(finishedDomain.getStatus(), GNGWorkflowConstant.BUREAU_ERROR.toFaceValue())) {
            ExperianEropDomainObject experianEropDomainObject = new ExperianEropDomainObject();
            ExperianEROPDomainPopulation experianEROPDomainPopulation = new ExperianEROPDomainPopulation();

            List<ExperianEROPDomain> experianEROPDomains = experianEROPDomainPopulation.generateExperianEropDomain(inProfileResponse, issueresponse.getAckId().toString(),
                    ReqResHelperConstants.GONOGO.name(), memberRefNum,
                    Long.MIN_VALUE, new Date());

            experianEropDomainObject.setExperianEropDomainList(experianEROPDomains);
            return experianEropDomainObject;
        }
        return null;
    }

    private Object getHighmarkBaseSropEropObject(FinishedDomain finishedDomain, IssueResponseMB issueresponse, String memberRefNum) throws IOException {

        HighmarkBaseDomainPopulation highmarkBaseDomainPopulation = new HighmarkBaseDomainPopulation();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String chmRespString = GngUtils.getBureauResponse(finishedDomain.getResponseJsonObject());

        if (StringUtils.equals(finishedDomain.getStatus(), GNGWorkflowConstant.BUREAU_SUCCESS.toFaceValue())) {
            CHMBaseSropDomainObject chmBaseSropDomainObject = new CHMBaseSropDomainObject();

            BaseReportFile baseReportFile = objectMapper.readValue(chmRespString, BaseReportFile.class);
            List<HibHighmarkBaseSropDomain> hibHighmarkBaseSropDomains = highmarkBaseDomainPopulation.highmarkBaseSropDomainGenerator(baseReportFile, issueresponse.getAckId().toString(),
                    ReqResHelperConstants.GONOGO.name(), memberRefNum, memberRefNum);
            chmBaseSropDomainObject.setHmBaseSropDomainList(hibHighmarkBaseSropDomains);

            return chmBaseSropDomainObject;
        } else if (StringUtils.equals(finishedDomain.getStatus(), GNGWorkflowConstant.BUREAU_ERROR.toFaceValue())) {

            CHMBaseEropDomainObject chmBaseEropDomainObject = new CHMBaseEropDomainObject();
            AckReportFile ackReportFile = objectMapper.readValue(chmRespString, AckReportFile.class);
            List<HibHighmarkBaseEropDomain> hibHighmarkBaseEropDomains = highmarkBaseDomainPopulation.highmarkBaseEropDomainGenerator(ackReportFile, issueresponse.getAckId().toString(),
                    ReqResHelperConstants.GONOGO.name(), memberRefNum, memberRefNum);
            chmBaseEropDomainObject.setHmBaseEropDomainList(hibHighmarkBaseEropDomains);
            return chmBaseEropDomainObject;
        }
        return null;
    }
}
