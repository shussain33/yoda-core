package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class IssueResponseMB implements Serializable {

    /**
     * @author AshwiniP
     */
    private static final long serialVersionUID = 1L;

    @Expose
    @SerializedName("HEADER")
    private ResponseHeader headerObj;
    @Expose
    @SerializedName("ACKNOWLEDGEMENT-ID")
    private Long ackId;
    @Expose
    @SerializedName("STATUS")
    private String status;
    @Expose
    @SerializedName("FINISHED")
    private List<FinishedDomain> finishedObject;
    @Expose
    @SerializedName("IN-PROCESS")
    private List<InProcessDomain> inProcessObject;
    @Expose
    @SerializedName("REJECT")
    private List<RejectDomain> rejectObject;

    public ResponseHeader getHeaderObj() {
        return headerObj;
    }

    public void setHeaderObj(ResponseHeader headerObj) {
        this.headerObj = headerObj;
    }

    public Long getAckId() {
        return ackId;
    }

    public void setAckId(Long ackId) {
        this.ackId = ackId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FinishedDomain> getFinishedObject() {
        return finishedObject;
    }

    public void setFinishedObject(List<FinishedDomain> finishedObject) {
        this.finishedObject = finishedObject;
    }

    public List<InProcessDomain> getInProcessObject() {
        return inProcessObject;
    }

    public void setInProcessObject(List<InProcessDomain> inProcessObject) {
        this.inProcessObject = inProcessObject;
    }

    public List<RejectDomain> getRejectObject() {
        return rejectObject;
    }

    public void setRejectObject(List<RejectDomain> rejectObject) {
        this.rejectObject = rejectObject;
    }

    @Override
    public String toString() {
        return "IssueResponse [headerObj=" + headerObj + ", ackId=" + ackId
                + ", status=" + status + ", finishedObject=" + finishedObject
                + ", inProcessObject=" + inProcessObject + ", rejectObject="
                + rejectObject + "]";
    }


}
