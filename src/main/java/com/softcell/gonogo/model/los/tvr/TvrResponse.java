package com.softcell.gonogo.model.los.tvr;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by yogeshb on 4/7/17.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TvrResponse {

    @JsonProperty("dtRespDt")
    private Date responseDate;

    @JsonProperty("oTvrResponse")
    private TvrSuccessOrErrorMessage tvrSuccessOrErrorMessage;

    @JsonProperty("oError")
    private Error error;
}
