/**
 *
 */
package com.softcell.gonogo.model.los;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 */
public class CIBILSropDomainObject implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Expose
    @SerializedName("CIBIL_SROP_DOMAIN_LIST")
    List<HibCIBILSropDomain> cibilSropDomainList;

    public List<HibCIBILSropDomain> getCibilSropDomainList() {
        return cibilSropDomainList;
    }

    public void setCibilSropDomainList(List<HibCIBILSropDomain> cibilSropDomainList) {
        this.cibilSropDomainList = cibilSropDomainList;
    }

    @Override
    public String toString() {
        return "CIBILSropDomainObject [cibilSropDomainList="
                + cibilSropDomainList + "]";
    }


}
