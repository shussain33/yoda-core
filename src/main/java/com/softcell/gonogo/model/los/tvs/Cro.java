package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Cro {

        private String mtappr;

        private String eligibleamt;

        private String itrrt;

        private String tenor;

        private String emi;

        private String decisionupdatedate;

        private String dpay;

        private String unutilizedamt;

        private String ltv;

        private String utilizedamt;
}
