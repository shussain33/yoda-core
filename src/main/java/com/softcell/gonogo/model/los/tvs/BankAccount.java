package com.softcell.gonogo.model.los.tvs;

import com.softcell.gonogo.model.los.tvs.LosDto.RiskEvents;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BankAccount {

        private RiskEvents riskEvents;

        private String countAccounts;

        private String countActiveAccounts;

        private Accounts accounts;

        private String primaryAccount;

        private String monthsBankDataAvailable;

        private AggregateFlow aggregateFlow;

        private String primaryAccountCashWithdwl;

        private String bankDataSufficiencyFlag;
}
