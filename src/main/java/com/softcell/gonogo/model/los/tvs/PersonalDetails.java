package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonalDetails {

	private int totalfamilymember;

    private String designation;

    private String lastname;

    private String dateofbirth;

    private String aadharauthentication;

    private String education;

    private String lng;

    private String stage;

    private String religion;

    private String maritalstatus;

    private int tenor;

    private int age;

    private String fathername;

    private String gender;

    private int bankaccountsince;

    private String lat;

    private String profile;

    private String iscurrentaddress;

    private String isbankaccount;

    private String middlename;

    private String status;

    private int noofdependant;

    private String firstname;

    private int totalearning;

    private String email;

    private String spousename;

    private int appliedamount;

}
