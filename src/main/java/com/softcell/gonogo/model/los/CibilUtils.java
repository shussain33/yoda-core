package com.softcell.gonogo.model.los;

import org.apache.commons.lang.StringUtils;

public class CibilUtils {
	
	public String getGender(String key){
		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.genderMap.containsKey(key)){
				return CIBIL_METADATA.genderMap.get(key);
			}
		}
		return key; 
	}
	
	public String getSubjectReturnCode(String key){
		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.subjectReturnCode.containsKey(key)){
				return CIBIL_METADATA.subjectReturnCode.get(key);
			}
		}
		return key; 
	}
	
	public String getAccountType(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.accountTypeMap.containsKey(key)){
				return CIBIL_METADATA.accountTypeMap.get(key);
			}
		}
		return key; 
	}
	
	public String getOccupation(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.occupationMap.containsKey(key)){
				return CIBIL_METADATA.occupationMap.get(key);
			}
		}
		return key; 
	}
	
	public String getGrossIncome(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.grossIncomeMap.containsKey(key)){
				return CIBIL_METADATA.grossIncomeMap.get(key);
			}
		}
		return key; 
	}
	
	public String getAnnualIncome(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.AnnualIncome.containsKey(key)){
				return CIBIL_METADATA.AnnualIncome.get(key);
			}
		}
		return key; 
	}
	
	public String getPhoneTypeValue(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.phoneMap.containsKey(key)){
				return CIBIL_METADATA.phoneMap.get(key);
			}
		}
		return key; 
	}
	
	public String getState(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.stateMap.containsKey(key)){
				return CIBIL_METADATA.stateMap.get(key);
			}
		}
		return key; 
	}
	
	public String getAddressCategory(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.addressCategoryMap.containsKey(key)){
				return CIBIL_METADATA.addressCategoryMap.get(key);
			}
		}
		return key; 
	}
	
	public String getResidenceValue(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.residenceCodeMap.containsKey(key)){
				return CIBIL_METADATA.residenceCodeMap.get(key);
			}
		}
		return key; 
	}
	
	public String getOwnership(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.ownershipMap.containsKey(key)){
				return CIBIL_METADATA.ownershipMap.get(key);
			}
		}
		return key; 
	}
	
	public String getSuitFiledStatus(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.suitFiledMap.containsKey(key)){
				return CIBIL_METADATA.suitFiledMap.get(key);
			}
		}
		return key; 
	}
	
	public String getCollateral(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.collateralMap.containsKey(key)){
				return CIBIL_METADATA.collateralMap.get(key);
			}
		}
		return key; 
	}
	
	public String getRemarkCode(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.remarkCodeMap.containsKey(key)){
				return CIBIL_METADATA.remarkCodeMap.get(key);
			}
		}
		return key; 
	}
	
	public String getIdType(String key){
		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.idTypeMap.containsKey(key)){
				return CIBIL_METADATA.idTypeMap.get(key);
			}
		}
		return key; 
	}
	public String getScoreCardName(String key){
		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.scoreCardMap.containsKey(key)){
				return CIBIL_METADATA.scoreCardMap.get(key);
			}
		}
		return key; 
	}
	
	public String getEnquiryType(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.enquiryTypeMap.containsKey(key)){
				return CIBIL_METADATA.enquiryTypeMap.get(key);
			}
		}
		return key; 
	}
	
	public String getRepaymentFrequency(String key){

		if(StringUtils.isNotBlank(key)){
			if(CIBIL_METADATA.frequencyMap.containsKey(key)){
				return CIBIL_METADATA.frequencyMap.get(key);
			}
		}
		return key; 
	}
	
}
