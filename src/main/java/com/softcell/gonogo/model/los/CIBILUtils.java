package com.softcell.gonogo.model.los;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class CIBILUtils {

    private static Map<String,Map<String,String>> cibilMetadata = new HashMap<String,Map<String,String>>(){{

        put(ReverseConversionConstant.ConversionConstant.GENDER,CibilMetadata.genderMap);
        put(ReverseConversionConstant.ConversionConstant.ACCOUNT_TYPE,CibilMetadata.accountTypeMap);
        put(ReverseConversionConstant.ConversionConstant.ADDRESS_TYPE,CibilMetadata.addressCategoryMap);
        put(ReverseConversionConstant.ConversionConstant.MONTHLY_ANNUALLY_INDICATOR,CibilMetadata.AnnualIncome);
        put(ReverseConversionConstant.ConversionConstant.ID_TYPE,CibilMetadata.idTypeMap);
        put(ReverseConversionConstant.ConversionConstant.OWNERSHIP_INDICATOR,CibilMetadata.ownershipMap);
        put(ReverseConversionConstant.ConversionConstant.OCCUPATION_CODE,CibilMetadata.occupationMap);
        put(ReverseConversionConstant.ConversionConstant.PHONE_TYPE,CibilMetadata.phoneMap);
        put(ReverseConversionConstant.ConversionConstant.ADDRESS_RESIDENCE_CODE,CibilMetadata.residenceCodeMap);
        put(ReverseConversionConstant.ConversionConstant.GROSS_INCOME,CibilMetadata.grossIncomeMap);
        put(ReverseConversionConstant.ConversionConstant.FREQUENCY_OF_PAYMENT,CibilMetadata.paymentFreq);
        put(ReverseConversionConstant.ConversionConstant.SCORE_CARD_NAME,CibilMetadata.scoreCardName);
        put(ReverseConversionConstant.ConversionConstant.SUIT_FILED_STATUS,CibilMetadata.suitFiledMap);
        put(ReverseConversionConstant.ConversionConstant.WRITTEN_OFF_AND_SETTLED_STATUS.toString(),CibilMetadata.writtenOffSettledStatus);
        put(ReverseConversionConstant.ConversionConstant.COLLATERAL_TYPE,CibilMetadata.collateralMap);
        put(ReverseConversionConstant.ConversionConstant.ENQUIRY_PURPOSE,CibilMetadata.loanType);
        put(ReverseConversionConstant.ConversionConstant.LOAN_TYPE,CibilMetadata.loanType);
        put(ReverseConversionConstant.ConversionConstant.ADDRESS_STATE, CibilMetadata.stateMap);

    }};


    private static Map<String, String> getCodeMap(Long institutionId, String sourceSystem, String mapForCode) {
        Map<String, String> map = null;
        try {
            if (null != cibilMetadata) {
                map = cibilMetadata.get(mapForCode);
            }
        } catch (Exception e) {
            return null;
        }
        return map;
    }


    public static String getValue(String code, String sourceSystem, Long institutionId, String mapForCode) {
        Map<String, String> codeMap = getCodeMap(institutionId, sourceSystem, mapForCode);
        if (null != code) {
            if (null != codeMap) {
                if (codeMap.containsKey(code)) {
                    return codeMap.get(code);
                }
            }
        }
        return code;
    }

    public static List<Map<String, String>> getDpd(String refDate, List<String> pmtHist) {
        List<Map<String, String>> pmtHistLst = new ArrayList<Map<String, String>>();
        Date time = DateUtils.getDateFromString(refDate, "ddMMyyyy");
        Calendar c = Calendar.getInstance();
        DateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        if (null != pmtHist && pmtHist.size() > 0) {
            for (String st : pmtHist) {
                if (StringUtils.isNotBlank(st)) {
                    int length = st.length();
                    int size = 3;
                    for (int start = 0; start < length; start += size) {
                        Map<String, String> payMap = new HashMap<String, String>();

                        String substring = st.substring(start, Math.min(length, start + size));
                        payMap.put("dpd", substring);

                        String monthVal = "";
                        if (StringUtils.isNotBlank(refDate)) {
                            String format = sdf.format(time);
                            String[] split = StringUtils.split(format, "-");
                            monthVal = split[1] + "-" + split[2];

                            c.setTime(time);
                            c.add(Calendar.MONTH, -1);
                            time = c.getTime();
                        }
                        payMap.put("month", monthVal);
                        pmtHistLst.add(payMap);

                    }
                }
            }
        }

        if (pmtHistLst.size() < 18) {
            while (pmtHistLst.size() != 18) {
                Map<String, String> payMap = new HashMap<String, String>();
                pmtHistLst.add(payMap);
            }
            return pmtHistLst;
        }

        if (pmtHistLst.size() < 36) {
            while (pmtHistLst.size() != 36) {
                Map<String, String> payMap = new HashMap<String, String>();
                pmtHistLst.add(payMap);
            }
            return pmtHistLst;
        }
        return pmtHistLst;
    }

    public static Long DateDiff(String startdate, String enddate) {
        int sday = 0;
        int smonth = 0;
        int syear = 0;
        int eday = 0;
        int emonth = 0;
        int eyear = 0;
        sday = Integer.parseInt(startdate.split("-")[0]);
        smonth = Integer.parseInt(startdate.split("-")[1]);
        syear = Integer.parseInt(startdate.split("-")[2]);

        eday = Integer.parseInt(enddate.split("-")[0]);
        emonth = Integer.parseInt(enddate.split("-")[1]);
        eyear = Integer.parseInt(enddate.split("-")[2]);

        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.set(syear, smonth, sday);
        calendar2.set(eyear, emonth, eday);

        long milsecs1 = calendar1.getTimeInMillis();
        long milsecs2 = calendar2.getTimeInMillis();
        long diff = milsecs2 - milsecs1;
        long ddays = diff / (24 * 60 * 60 * 1000);
        return ddays;
    }

    private static int getNumOfTimesDPDMonthsCIBIL(String paymentHistory1,
                                                   String paymentHistory2) {

        int counter = 0;
        int monthCnt = 0;
        if (StringUtils.isNotBlank(paymentHistory1)) {
            Integer ph1Len = Integer.valueOf(StringUtils.length(paymentHistory1));
            for (Integer i = Integer.valueOf(0); i.intValue() < ph1Len.intValue(); i = Integer.valueOf(i.intValue() + 3)) {

                if (monthCnt >= 10) {
                    break;
                }
                monthCnt++;
                String dpdStr = StringUtils.substring(paymentHistory1, i.intValue(), i.intValue() + 3);
                String assetClass = "";
                Integer dpd = Integer.valueOf(0);
                try {
                    dpd = Integer.valueOf(Integer.parseInt(dpdStr));
                } catch (NumberFormatException e) {
                    assetClass = dpdStr;
                }
                if (StringUtils.isNotBlank(assetClass)) {
                    if ((StringUtils.equalsIgnoreCase(assetClass, "SMA")) ||
                            (StringUtils.equalsIgnoreCase(assetClass, "SUB")) ||
                            (StringUtils.equalsIgnoreCase(assetClass, "DBT")) ||
                            (StringUtils.equalsIgnoreCase(assetClass, "LSS"))) {
                        counter++;
                    }
                } else if (dpd.intValue() >= 30) {
                    counter++;
                }

            }
        }
        if (StringUtils.isNotBlank(paymentHistory2)) {
            Integer ph1Len = Integer.valueOf(StringUtils.length(paymentHistory2));
            for (Integer i = Integer.valueOf(0); i.intValue() < ph1Len.intValue(); i = Integer.valueOf(i.intValue() + 3)) {

                if (monthCnt >= 10) {
                    break;
                }
                monthCnt++;
                String dpdStr = StringUtils.substring(paymentHistory2, i.intValue(), i.intValue() + 3);
                String assetClass = "";
                Integer dpd = Integer.valueOf(0);
                try {
                    dpd = Integer.valueOf(Integer.parseInt(dpdStr));
                } catch (NumberFormatException e) {
                    assetClass = dpdStr;
                }
                if (StringUtils.isNotBlank(assetClass)) {
                    if ((StringUtils.equalsIgnoreCase(assetClass, "SMA")) ||
                            (StringUtils.equalsIgnoreCase(assetClass, "SUB")) ||
                            (StringUtils.equalsIgnoreCase(assetClass, "DBT")) ||
                            (StringUtils.equalsIgnoreCase(assetClass, "LSS"))) {
                        counter++;
                    }
                } else if (dpd.intValue() >= 30) {
                    counter++;
                }

            }
        }
        //  System.out.println("--> counter --> "+counter);
        return counter;
    }


    public static String format(String value) {
        if (StringUtils.isBlank(value)) {
            return "";
        }
        if (StringUtils.isNumeric(value)) {
            double parseDouble = Double.parseDouble(value);
            if (parseDouble < 1000) {
                return format("###", parseDouble);
            } else {
                double hundreds = parseDouble % 1000;
                int other = (int) (parseDouble / 1000);
                return format(",##", other) + ',' + format("000", hundreds);
            }
        } else {
            return value;
        }
    }

    private static String format(String pattern, Object value) {
        return new DecimalFormat(pattern).format(value);
    }
}
