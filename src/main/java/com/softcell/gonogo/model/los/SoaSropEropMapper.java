package com.softcell.gonogo.model.los;


import com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilERespType;
import com.softcell.soap.mb.hdfcbank.xsd.multibureau.CibilSRespType;
import com.softcell.soap.mb.hdfcbank.xsd.multibureau.ResponseJSONType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SoaSropEropMapper {
    static SimpleDateFormat sdf = null;
    static SimpleDateFormat sdf1 = null;
    static SimpleDateFormat sdf2 = null;
    static SimpleDateFormat sdf3 = null;
    private static Logger logger_ = LoggerFactory.getLogger(SoaSropEropMapper.class);

    static {
        sdf = new SimpleDateFormat("ddMMyyyy");
        sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        sdf2 = new SimpleDateFormat("yyyyMMdd");
        sdf3 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    }


    public ResponseJSONType cibilEropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
        logger_.warn("** SoaSropEropMapper >> cibilEropMapping ");
        try {
            CIBILEropDomainObject cibilEropDomainObject = (CIBILEropDomainObject) responseJsonObject;

            if (cibilEropDomainObject != null) {
                List<CIBILEROPErrorDomain> cibilEropDomainList = cibilEropDomainObject.getCibilEropDomainList();

                if (cibilEropDomainList != null && cibilEropDomainList.size() > 0) {
                    List<CibilERespType> cibilERespTypeList = new ArrayList<CibilERespType>();
                    for (CIBILEROPErrorDomain cibilEropDomain : cibilEropDomainList) {

                        CibilERespType cibilERespType = new CibilERespType();
                        cibilERespType.setSRNO(cibilEropDomain.getSrNo_() == null ? "" : cibilEropDomain.getSrNo_());
                        cibilERespType.setSOA_SOURCE_NAME(cibilEropDomain.getSoaSourceName_() == null ? "" : cibilEropDomain.getSoaSourceName_());
                        cibilERespType.setMEMBER_REFERENCE_NUMBER(cibilEropDomain.getMemberReferenceNo_() == null ? "" : cibilEropDomain.getMemberReferenceNo_());
                        cibilERespType.setENQUIRY_DATE(cibilEropDomain.getEnquiryDate_() == null ? "" : cibilEropDomain.getEnquiryDate_());
                        cibilERespType.setSEGMENT_TAG(cibilEropDomain.getSegmentTag_() == null ? "" : cibilEropDomain.getSegmentTag_());
                        cibilERespType.setTYPE_OF_ERROR(cibilEropDomain.getTypeOfError_() == null ? "" : cibilEropDomain.getTypeOfError_());
                        cibilERespType.setDATE_PROCESSED(cibilEropDomain.getDateProcessed_() == null ? "" : cibilEropDomain.getDateProcessed_());
                        cibilERespType.setTIME_PROCESSED(cibilEropDomain.getTimeProcessed_() == null ? "" : cibilEropDomain.getTimeProcessed_());
                        cibilERespType.setENQUIRY_MEMBER_CODE_ID(cibilEropDomain.getEnquiryMemberCodeId_() == null ? "" : cibilEropDomain.getEnquiryMemberCodeId_());
                        cibilERespType.setERROR_TYPE_CODE(cibilEropDomain.getErrorTypeCode_() == null ? "" : cibilEropDomain.getErrorTypeCode_());
                        cibilERespType.setERROR_TYPE(cibilEropDomain.getErrorType_() == null ? "" : cibilEropDomain.getErrorType_());
                        cibilERespType.setDATA(cibilEropDomain.getData_() == null ? "" : cibilEropDomain.getData_());
                        cibilERespType.setSEGMENT(cibilEropDomain.getSegment_() == null ? "" : cibilEropDomain.getSegment_());
                        cibilERespType.setSPECIFIED(cibilEropDomain.getSpecified_() == null ? "" : cibilEropDomain.getSpecified_());
                        cibilERespType.setEXPECTED(cibilEropDomain.getExpected_() == null ? "" : cibilEropDomain.getExpected_());
                        cibilERespType.setINVALID_VERSION(cibilEropDomain.getInvalidVersion_() == null ? "" : cibilEropDomain.getInvalidVersion_());
                        cibilERespType.setINVALID_FIELD_LENGTH(cibilEropDomain.getInvalidFieldLength_() == null ? "" : cibilEropDomain.getInvalidFieldLength_());
                        cibilERespType.setINVALID_TOTAL_LENGTH(cibilEropDomain.getInvalidTotalLength_() == null ? "" : cibilEropDomain.getInvalidTotalLength_());
                        cibilERespType.setINVALID_ENQUIRY_PURPOSE(cibilEropDomain.getInvalidEnquiryPurpose_() == null ? "" : cibilEropDomain.getInvalidEnquiryPurpose_());
                        cibilERespType.setINVALID_ENQUIRY_AMOUNT(cibilEropDomain.getInvalidEnquiryAmount_() == null ? "" : cibilEropDomain.getInvalidEnquiryAmount_());
                        cibilERespType.setINVALID_ENQUIRY_MEMBER_USER_ID(cibilEropDomain.getInvalidEnquiryMemberUserId_() == null ? "" : cibilEropDomain.getInvalidEnquiryMemberUserId_());
                        cibilERespType.setREQUIRED_ENQ_SEG_MISSING(cibilEropDomain.getRequiredEnquirySegmentMissing_() == null ? "" : cibilEropDomain.getRequiredEnquirySegmentMissing_());
                        cibilERespType.setINVALID_ENQUIRY_DATA(cibilEropDomain.getInvalidEnquiryData_() == null ? "" : cibilEropDomain.getInvalidEnquiryData_());
                        cibilERespType.setCIBIL_SYSTEM_ERROR(cibilEropDomain.getCibilSystemError_() == null ? "" : cibilEropDomain.getCibilSystemError_());
                        cibilERespType.setINVALID_SEGMENT_TAG(cibilEropDomain.getInvalidSegmentTag_() == null ? "" : cibilEropDomain.getInvalidSegmentTag_());
                        cibilERespType.setINVALID_SEGMENT_ORDER(cibilEropDomain.getInvalidSegmentOrder_() == null ? "" : cibilEropDomain.getInvalidSegmentOrder_());
                        cibilERespType.setINVALID_FIELD_TAG_ORDER(cibilEropDomain.getInvalidFieldTagOrder_() == null ? "" : cibilEropDomain.getInvalidFieldTagOrder_());
                        cibilERespType.setMISSING_REQUIRED_FIELD(cibilEropDomain.getMissingRequiredField_() == null ? "" : cibilEropDomain.getMissingRequiredField_());
                        cibilERespType.setREQUESTED_RESP_SIZE_EXCEEDED(cibilEropDomain.getRequestedRespSizeExceeded_() == null ? "" : cibilEropDomain.getRequestedRespSizeExceeded_());
                        cibilERespType.setINVALID_INPUT_OUTPUT_MEDIA(cibilEropDomain.getInvalidInputOutputMedia_() == null ? "" : cibilEropDomain.getInvalidInputOutputMedia_());
                        cibilERespType.setOUTPUT_WRITE_FLAG(cibilEropDomain.getOutputWriteFlag_() == null ? "" : cibilEropDomain.getOutputWriteFlag_());
                        cibilERespType.setOUTPUT_WRITE_TIME(cibilEropDomain.getOutputWriteTime_() == null ? "" : cibilEropDomain.getOutputWriteTime_());
                        cibilERespType.setOUTPUT_READ_TIME(cibilEropDomain.getOutputReadTime_() == null ? "" : cibilEropDomain.getOutputReadTime_());


                        cibilERespTypeList.add(cibilERespType);

                    }
                    responseJSONType.setCIBIL_EROP_DOMAIN_LIST(cibilERespTypeList.toArray(new CibilERespType[cibilERespTypeList.size()]));
                }
            }

        } catch (Exception e) {
            logger_.error(e.getMessage(), e);
            e.printStackTrace();
        }
        logger_.warn("** SoaSropEropMapper ** cibilEropMapping >>");
        return responseJSONType;
    }

    public ResponseJSONType cibilSropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
        logger_.warn("** SoaSropEropMapper >> cibilSropMapping ");
        try {
            CIBILSropDomainObject cibilSropDomainObject = (CIBILSropDomainObject) responseJsonObject;
            if (cibilSropDomainObject != null) {
                List<HibCIBILSropDomain> hibCibilSropDomainList = cibilSropDomainObject.getCibilSropDomainList();

                if (hibCibilSropDomainList != null && hibCibilSropDomainList.size() > 0) {
                    List<CibilSRespType> cibilSRespTypeList = new ArrayList<CibilSRespType>();
                    for (HibCIBILSropDomain hibCibilSropDomain : hibCibilSropDomainList) {
                        CibilSRespType cibilSRespType = new CibilSRespType();

                        cibilSRespType.setSRNO(hibCibilSropDomain.getSrNo() == null ? "" : Integer.toString(hibCibilSropDomain.getSrNo()));
                        cibilSRespType.setSOA_SOURCE_NAME(hibCibilSropDomain.getSoaSourceName() == null ? "" : hibCibilSropDomain.getSoaSourceName());
                        cibilSRespType.setDATE_PROCESSED(hibCibilSropDomain.getDateProcessed() == null ? "" : sdf.format(hibCibilSropDomain.getDateProcessed()));
                        cibilSRespType.setMEMBER_REFERENCE_NUMBER(hibCibilSropDomain.getMemberReferenceNo() == null ? "" : hibCibilSropDomain.getMemberReferenceNo());
                        cibilSRespType.setSUBJECT_RETURN_CODE(hibCibilSropDomain.getSubjectReturnCode() == null ? "" : hibCibilSropDomain.getSubjectReturnCode());
                        cibilSRespType.setENQUIRY_CONTROL_NUMBER(hibCibilSropDomain.getEnquiryControlNumber() == null ? "" : hibCibilSropDomain.getEnquiryControlNumber());
                        cibilSRespType.setCONSUMER_NAME_FIELD1(hibCibilSropDomain.getConsumerNameField1() == null ? "" : hibCibilSropDomain.getConsumerNameField1());
                        cibilSRespType.setCONSUME_NAME_FIELD2(hibCibilSropDomain.getConsumerNameField2() == null ? "" : hibCibilSropDomain.getConsumerNameField2());
                        cibilSRespType.setCONSUMER_NAME_FIELD3(hibCibilSropDomain.getConsumerNameField3() == null ? "" : hibCibilSropDomain.getConsumerNameField3());
                        cibilSRespType.setCONSUMER_NAME_FIELD4(hibCibilSropDomain.getConsumerNameField4() == null ? "" : hibCibilSropDomain.getConsumerNameField4());
                        cibilSRespType.setCONSUMER_NAME_FIELD5(hibCibilSropDomain.getConsumerNameField5() == null ? "" : hibCibilSropDomain.getConsumerNameField5());
                        cibilSRespType.setDATE_OF_BIRTH(hibCibilSropDomain.getDateOfBirth() == null ? "" : sdf.format(hibCibilSropDomain.getDateOfBirth()));
                        cibilSRespType.setGENDER(hibCibilSropDomain.getGenderValue() == null ? "" : hibCibilSropDomain.getGenderValue());
                        cibilSRespType.setDT_ERRCODE_PN(hibCibilSropDomain.getDateErrorCodePN() == null ? "" : sdf.format(hibCibilSropDomain.getDateErrorCodePN()));
                        cibilSRespType.setERR_SEG_TAG_PN(hibCibilSropDomain.getErrorSegTagPN() == null ? "" : hibCibilSropDomain.getErrorSegTagPN());
                        cibilSRespType.setERR_CODE_PN(hibCibilSropDomain.getErrorCodePN() == null ? "" : hibCibilSropDomain.getErrorCodePN());
                        cibilSRespType.setERROR_PN(hibCibilSropDomain.getErrorPN() == null ? "" : hibCibilSropDomain.getErrorPN());
                        cibilSRespType.setDT_ENTCIBILRECODE_PN(hibCibilSropDomain.getDateEntryCIBILRemarkCodePN() == null ? "" : sdf.format(hibCibilSropDomain.getDateEntryCIBILRemarkCodePN()));
                        cibilSRespType.setCIBIL_REMARK_CODE_PN(hibCibilSropDomain.getCibilRemarkCodePN() == null ? "" : hibCibilSropDomain.getCibilRemarkCodePN());
                        cibilSRespType.setDATE_DISP_REMARK_CODE_PN(hibCibilSropDomain.getDateEntryErrorDisputeRemarkCodePN() == null ? "" : sdf.format(hibCibilSropDomain.getDateEntryErrorDisputeRemarkCodePN()));
                        cibilSRespType.setERR_DISP_REM_CODE1_PN(hibCibilSropDomain.getErrorDisputeRemarkCode1PN() == null ? "" : hibCibilSropDomain.getErrorDisputeRemarkCode1PN());
                        cibilSRespType.setERR_DISP_REM_CODE2_PN(hibCibilSropDomain.getErrorDisputeRemarkCode2PN() == null ? "" : hibCibilSropDomain.getErrorDisputeRemarkCode2PN());
                        cibilSRespType.setID_TYPE(hibCibilSropDomain.getIdType() == null ? "" : hibCibilSropDomain.getIdType());
                        cibilSRespType.setID_NUMBER(hibCibilSropDomain.getIdNumber() == null ? "" : hibCibilSropDomain.getIdNumber());
                        cibilSRespType.setISSUE_DATE(hibCibilSropDomain.getIssueDate() == null ? "" : sdf.format(hibCibilSropDomain.getIssueDate()));
                        cibilSRespType.setEXPIRATION_DATE(hibCibilSropDomain.getExpirationDate() == null ? "" : sdf.format(hibCibilSropDomain.getExpirationDate()));
                        cibilSRespType.setENRICHED_THROUGH_ENQUIRY_ID(hibCibilSropDomain.getEnrichedThroughEnquiryId() == null ? "" : hibCibilSropDomain.getEnrichedThroughEnquiryId());
                        cibilSRespType.setTELEPHONE_TYPE(hibCibilSropDomain.getTelephoneTypeValue() == null ? "" : hibCibilSropDomain.getTelephoneTypeValue());
                        cibilSRespType.setTELEPHONE_EXTENSION(hibCibilSropDomain.getTelephoneExtension() == null ? "" : hibCibilSropDomain.getTelephoneExtension());
                        cibilSRespType.setTELEPHONE_NUMBER(hibCibilSropDomain.getTelephoneNumber() == null ? "" : hibCibilSropDomain.getTelephoneNumber());
                        cibilSRespType.setENRICHED_THROUGH_ENQUIRY_PT(hibCibilSropDomain.getEnrichedThroughEnquiryPT() == null ? "" : hibCibilSropDomain.getEnrichedThroughEnquiryPT());
                        cibilSRespType.setEMAIL_ID(hibCibilSropDomain.getEmailId() == null ? "" : hibCibilSropDomain.getEmailId());
                        cibilSRespType.setACCOUNT_TYPE(hibCibilSropDomain.getAccountTypeValue() == null ? "" : hibCibilSropDomain.getAccountTypeValue());
                        cibilSRespType.setDATE_REPORTED_AND_CERTIFIED_EM(hibCibilSropDomain.getDateReportedAndCertifiedEM() == null ? "" : sdf.format(hibCibilSropDomain.getDateReportedAndCertifiedEM()));
                        cibilSRespType.setOCCUPATION_CODE_EM(hibCibilSropDomain.getOccupationValueEM() == null ? "" : hibCibilSropDomain.getOccupationValueEM());
                        cibilSRespType.setINCOME_EM(hibCibilSropDomain.getIncomeValueEM() == null ? "" : hibCibilSropDomain.getIncomeValueEM());
                        cibilSRespType.setNET_GROSS_INDICATOR_EM(hibCibilSropDomain.getNetGrossIndicatorValueEM() == null ? "" : hibCibilSropDomain.getNetGrossIndicatorValueEM());
                        cibilSRespType.setMNTHLY_ANNUAL_INDICATOR_EM(hibCibilSropDomain.getMonthlyAnnualIndicatorValueEM() == null ? "" : hibCibilSropDomain.getMonthlyAnnualIndicatorValueEM());
                        cibilSRespType.setDATE_ENTRY_ERR_CODE_EM(hibCibilSropDomain.getDateEntryErrorCodeEM() == null ? "" : sdf.format(hibCibilSropDomain.getDateEntryErrorCodeEM()));
                        cibilSRespType.setERR_CODE_EM(hibCibilSropDomain.getErrorCodeEM() == null ? "" : hibCibilSropDomain.getErrorCodeEM());
                        cibilSRespType.setDATE_CIBIL_ERR_CODE_EM(hibCibilSropDomain.getDateCIBILErrorCodeEM() == null ? "" : sdf.format(hibCibilSropDomain.getDateCIBILErrorCodeEM()));
                        cibilSRespType.setCIBIL_REMARK_CODE_EM(hibCibilSropDomain.getCibilRemarkCodeEM() == null ? "" : hibCibilSropDomain.getCibilRemarkCodeEM());
                        cibilSRespType.setDT_DIS_REMARK_CODE_EM(hibCibilSropDomain.getDateDisputeRemarkCodeEM() == null ? "" : sdf.format(hibCibilSropDomain.getDateDisputeRemarkCodeEM()));
                        cibilSRespType.setERR_DISP_REMCODE1_EM(hibCibilSropDomain.getErrorDisputeRemarkCode1EM() == null ? "" : hibCibilSropDomain.getErrorDisputeRemarkCode1EM());
                        cibilSRespType.setERR_DISP_REMCODE2_EM(hibCibilSropDomain.getErrorDisputeRemarkCode2EM() == null ? "" : hibCibilSropDomain.getErrorDisputeRemarkCode2EM());
                        cibilSRespType.setACCOUNT_NUMBER_PI(hibCibilSropDomain.getAccountNumberPI() == null ? "" : hibCibilSropDomain.getAccountNumberPI());
                        cibilSRespType.setSCORE_NAME(hibCibilSropDomain.getScoreName() == null ? "" : hibCibilSropDomain.getScoreName());
                        cibilSRespType.setSCORE_CARD_NAME(hibCibilSropDomain.getScoreCardName() == null ? "" : hibCibilSropDomain.getScoreCardName());
                        cibilSRespType.setSCORE_CARD_VERSION(hibCibilSropDomain.getScoreCardVersion() == null ? "" : hibCibilSropDomain.getScoreCardVersion());
                        cibilSRespType.setSCORE_DATE(hibCibilSropDomain.getScoreDate() == null ? "" : sdf.format(hibCibilSropDomain.getScoreDate()));
                        cibilSRespType.setSCORE(hibCibilSropDomain.getScore() == null ? "" : hibCibilSropDomain.getScore());
                        cibilSRespType.setEXCLUSION_CODES_1_TO_5(hibCibilSropDomain.getExclusionCodes1_to_5() == null ? "" : hibCibilSropDomain.getExclusionCodes1_to_5());
                        cibilSRespType.setEXCLUSION_CODES_6_TO_10(hibCibilSropDomain.getExclusionCodes6_TO_10() == null ? "" : hibCibilSropDomain.getExclusionCodes6_TO_10());
                        cibilSRespType.setEXCLUSION_CODES_11_TO_15(hibCibilSropDomain.getExclusionCodes11_TO_15() == null ? "" : hibCibilSropDomain.getExclusionCodes11_TO_15());
                        cibilSRespType.setEXCLUSION_CODES_16_TO_20(hibCibilSropDomain.getExclusionCodes16_TO_20() == null ? "" : hibCibilSropDomain.getExclusionCodes16_TO_20());
                        cibilSRespType.setREASON_CODES_1_TO_5(hibCibilSropDomain.getReasonCodes1_TO_5() == null ? "" : hibCibilSropDomain.getReasonCodes1_TO_5());
                        cibilSRespType.setREASON_CODES_6_TO_10(hibCibilSropDomain.getReasonCodes6_TO_10() == null ? "" : hibCibilSropDomain.getReasonCodes6_TO_10());
                        cibilSRespType.setREASON_CODES_11_TO_15(hibCibilSropDomain.getReasonCodes11_TO_15() == null ? "" : hibCibilSropDomain.getReasonCodes11_TO_15());
                        cibilSRespType.setREASON_CODES_16_TO_20(hibCibilSropDomain.getReasonCodes16_TO_20() == null ? "" : hibCibilSropDomain.getReasonCodes16_TO_20());
                        cibilSRespType.setREASON_CODES_21_TO_25(hibCibilSropDomain.getReasonCodes21_TO_25() == null ? "" : hibCibilSropDomain.getReasonCodes21_TO_25());
                        cibilSRespType.setREASON_CODES_26_TO_30(hibCibilSropDomain.getReasonCodes26_TO_30() == null ? "" : hibCibilSropDomain.getReasonCodes26_TO_30());
                        cibilSRespType.setREASON_CODES_31_TO_35(hibCibilSropDomain.getReasonCodes31_TO_35() == null ? "" : hibCibilSropDomain.getReasonCodes31_TO_35());
                        cibilSRespType.setREASON_CODES_36_TO_40(hibCibilSropDomain.getReasonCodes36_TO_40() == null ? "" : hibCibilSropDomain.getReasonCodes36_TO_40());
                        cibilSRespType.setREASON_CODES_41_TO_45(hibCibilSropDomain.getReasonCodes41_TO_45() == null ? "" : hibCibilSropDomain.getReasonCodes41_TO_45());
                        cibilSRespType.setREASON_CODES_46_TO_50(hibCibilSropDomain.getReasonCodes46_TO_50() == null ? "" : hibCibilSropDomain.getReasonCodes46_TO_50());
                        cibilSRespType.setERROR_CODE_SC(hibCibilSropDomain.getErrorCodeSC() == null ? "" : hibCibilSropDomain.getErrorCodeSC());
                        cibilSRespType.setADDRESS_LINE_1(hibCibilSropDomain.getAddressLine1() == null ? "" : hibCibilSropDomain.getAddressLine1());
                        cibilSRespType.setADDRESS_LINE_2(hibCibilSropDomain.getAddressLine2() == null ? "" : hibCibilSropDomain.getAddressLine2());
                        cibilSRespType.setADDRESS_LINE_3(hibCibilSropDomain.getAddressLine3() == null ? "" : hibCibilSropDomain.getAddressLine3());
                        cibilSRespType.setADDRESS_LINE_4(hibCibilSropDomain.getAddressLine4() == null ? "" : hibCibilSropDomain.getAddressLine4());
                        cibilSRespType.setADDRESS_LINE_5(hibCibilSropDomain.getAddressLine5() == null ? "" : hibCibilSropDomain.getAddressLine5());
                        cibilSRespType.setSTATE_CODE(hibCibilSropDomain.getStateCode() == null ? "" : Integer.toString(hibCibilSropDomain.getStateCode()));
                        cibilSRespType.setSTATE(hibCibilSropDomain.getState() == null ? "" : hibCibilSropDomain.getState());
                        cibilSRespType.setPINCODE(hibCibilSropDomain.getPincode() == null ? "" : hibCibilSropDomain.getPincode());
                        cibilSRespType.setADDRESS_CATERGORY(hibCibilSropDomain.getAddressCatergoryValue() == null ? "" : hibCibilSropDomain.getAddressCatergoryValue());
                        cibilSRespType.setRESIDENCE_CODE(hibCibilSropDomain.getResidenceValue() == null ? "" : hibCibilSropDomain.getResidenceValue());
                        cibilSRespType.setDATE_REPORTED_PA(hibCibilSropDomain.getDateReportedPA() == null ? "" : hibCibilSropDomain.getDateReportedPA());
                        cibilSRespType.setENRICHED_THROUGH_ENQUIRY_PA(hibCibilSropDomain.getEnrichedThroughEnquiryPA() == null ? "" : hibCibilSropDomain.getEnrichedThroughEnquiryPA());
                        cibilSRespType.setREPO_MEMSHORTNAME_TL(hibCibilSropDomain.getReportMemberShortNameTL() == null ? "" : hibCibilSropDomain.getReportMemberShortNameTL());
                        cibilSRespType.setACCOUNT_NUMBER_TL(hibCibilSropDomain.getAccountNumberTL() == null ? "" : hibCibilSropDomain.getAccountNumberTL());
                        cibilSRespType.setACCOUNT_TYPE_TL(hibCibilSropDomain.getAccountTypeValueTL() == null ? "" : hibCibilSropDomain.getAccountTypeValueTL());
                        cibilSRespType.setOWNERSHIP_INDICATOR_TL(hibCibilSropDomain.getOwnershipIndicatorValueTL() == null ? "" : hibCibilSropDomain.getOwnershipIndicatorValueTL());
                        cibilSRespType.setDATE_OPENED_DISBURSED_TL(hibCibilSropDomain.getDateOpenedDisbursedTL() == null ? "" : sdf.format(hibCibilSropDomain.getDateOpenedDisbursedTL()));
                        cibilSRespType.setDATE_OF_LAST_PAYMENT_TL(hibCibilSropDomain.getDateOfLastPaymentTL() == null ? "" : sdf.format(hibCibilSropDomain.getDateOfLastPaymentTL()));
                        cibilSRespType.setDATE_CLOSED_TL(hibCibilSropDomain.getDateClosedTL() == null ? "" : sdf.format(hibCibilSropDomain.getDateClosedTL()));
                        cibilSRespType.setTL_DATE_REPORTED(hibCibilSropDomain.getDateReportedTL() == null ? "" : sdf.format(hibCibilSropDomain.getDateReportedTL()));
                        cibilSRespType.setHIGH_CREDIT_SANCTIONED_AMOUNT(hibCibilSropDomain.getHighCreditSanctionedAmount() == null ? "" : hibCibilSropDomain.getHighCreditSanctionedAmount());
                        cibilSRespType.setCURRENT_BALANCE_TL(hibCibilSropDomain.getCurrentBalanceTL() == null ? "" : hibCibilSropDomain.getCurrentBalanceTL());
                        cibilSRespType.setAMOUNT_OVERDUE_TL(hibCibilSropDomain.getAmountOverdueTL() == null ? "" : hibCibilSropDomain.getAmountOverdueTL());
                        cibilSRespType.setPAYMENT_HISTORY_1(hibCibilSropDomain.getPaymentHistory1() == null ? "" : hibCibilSropDomain.getPaymentHistory1());
                        cibilSRespType.setPAYMENT_HISTORY_2(hibCibilSropDomain.getPaymentHistory2() == null ? "" : hibCibilSropDomain.getPaymentHistory2());
                        cibilSRespType.setPAYMENT_HISTORY_START_DATE(hibCibilSropDomain.getPaymentHistoryStartDate() == null ? "" : sdf.format(hibCibilSropDomain.getPaymentHistoryStartDate()));
                        cibilSRespType.setPAYMENT_HISTORY_END_DATE(hibCibilSropDomain.getPaymentHistoryEndDate() == null ? "" : sdf.format(hibCibilSropDomain.getPaymentHistoryEndDate()));
                        cibilSRespType.setSUIT_FILED_STATUS_TL(hibCibilSropDomain.getSuitFiledStatusValueTL() == null ? "" : hibCibilSropDomain.getSuitFiledStatusValueTL());
                        cibilSRespType.setWOF_SETTLED_STATUS_TL(hibCibilSropDomain.getWofSettledStatusTL() == null ? "" : hibCibilSropDomain.getWofSettledStatusTL());
                        cibilSRespType.setVALOFCOLLATERAL_TL(hibCibilSropDomain.getCollateralValueTL() == null ? "" : hibCibilSropDomain.getCollateralValueTL());
                        cibilSRespType.setTYPEOFCOLLATERAL_TL(hibCibilSropDomain.getTypeOfCollateralTL() == null ? "" : hibCibilSropDomain.getTypeOfCollateralTL());
                        cibilSRespType.setCREDITLIMIT_TL(hibCibilSropDomain.getCreditLimitTL() == null ? "" : hibCibilSropDomain.getCreditLimitTL());
                        cibilSRespType.setCASHLIMIT_TL(hibCibilSropDomain.getCashLimitTL() == null ? "" : hibCibilSropDomain.getCashLimitTL());
                        cibilSRespType.setRATEOFINTREST_TL(hibCibilSropDomain.getRateOfIntrestTL() == null ? "" : hibCibilSropDomain.getRateOfIntrestTL());
                        cibilSRespType.setREPAY_TENURE(hibCibilSropDomain.getRepayTenure() == null ? "" : hibCibilSropDomain.getRepayTenure());
                        cibilSRespType.setEMI_AMOUNT_TL(hibCibilSropDomain.getEmiAmountTL() == null ? "" : hibCibilSropDomain.getEmiAmountTL());
                        cibilSRespType.setWOF_TOT_AMOUNT_TL(hibCibilSropDomain.getWofTotalAmountTL() == null ? "" : hibCibilSropDomain.getWofTotalAmountTL());
                        cibilSRespType.setWOF_PRINCIPAL_TL(hibCibilSropDomain.getWofPrincipalTL() == null ? "" : hibCibilSropDomain.getWofPrincipalTL());
                        cibilSRespType.setSETTLEMENT_AMOUNT_TL(hibCibilSropDomain.getSettlementAmountTL() == null ? "" : hibCibilSropDomain.getSettlementAmountTL());
                        cibilSRespType.setPAYMENT_FREQUENCY_TL(hibCibilSropDomain.getPaymentFrequencyTL() == null ? "" : hibCibilSropDomain.getPaymentFrequencyTL());
                        cibilSRespType.setACTUAL_PAYMENT_AMOUNT_TL(hibCibilSropDomain.getActualPaymentAmountTL() == null ? "" : hibCibilSropDomain.getActualPaymentAmountTL());
                        cibilSRespType.setDT_ENTERRCODE_TL(hibCibilSropDomain.getDateEntErrorCodeTL() == null ? "" : sdf.format(hibCibilSropDomain.getDateEntErrorCodeTL()));
                        cibilSRespType.setERRCODE_TL(hibCibilSropDomain.getErrorCodeTL() == null ? "" : hibCibilSropDomain.getErrorCodeTL());
                        cibilSRespType.setDT_ENTCIBIL_REMARK_CODE_TL(hibCibilSropDomain.getDateEntCIBILRemarkCodeTL() == null ? "" : sdf.format(hibCibilSropDomain.getDateEntCIBILRemarkCodeTL()));
                        cibilSRespType.setCIBIL_REMARKS_CODE_TL(hibCibilSropDomain.getCibilRemarksCodeTL() == null ? "" : hibCibilSropDomain.getCibilRemarksCodeTL());
                        cibilSRespType.setDT_DISPUTE_CODE_TL(hibCibilSropDomain.getDateDisputeCodeTL() == null ? "" : sdf.format(hibCibilSropDomain.getDateDisputeCodeTL()));
                        cibilSRespType.setERR_DISP_REMCODE1_TL(hibCibilSropDomain.getErrorDisputeRemarkCode1TL() == null ? "" : hibCibilSropDomain.getErrorDisputeRemarkCode1TL());
                        cibilSRespType.setERR_DISP_REMCODE2_TL(hibCibilSropDomain.getErrorDisputeRemarkCode2TL() == null ? "" : hibCibilSropDomain.getErrorDisputeRemarkCode2TL());
                        cibilSRespType.setDATE_OF_ENQUIRY_IQ(hibCibilSropDomain.getDateOfEnquiryIQ() == null ? "" : sdf.format(hibCibilSropDomain.getDateOfEnquiryIQ()));
                        cibilSRespType.setENQ_MEMBER_SHORT_NAME_IQ(hibCibilSropDomain.getEnquiryMemberShortNameIQ() == null ? "" : hibCibilSropDomain.getEnquiryMemberShortNameIQ());
                        cibilSRespType.setENQUIRY_PURPOSE_IQ(hibCibilSropDomain.getEnquiryPurposeIQ() == null ? "" : hibCibilSropDomain.getEnquiryPurposeIQ());
                        cibilSRespType.setENQIURY_AMOUNT_IQ(hibCibilSropDomain.getEnqiuryAmountIQ() == null ? "" : hibCibilSropDomain.getEnqiuryAmountIQ());
                        cibilSRespType.setDATE_OF_ENTRY_DR(hibCibilSropDomain.getDateOfEntryDR() == null ? "" : sdf.format(hibCibilSropDomain.getDateOfEntryDR()));
                        cibilSRespType.setDISP_REM_LINE1(hibCibilSropDomain.getDisputeRemarkLine1() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine1());
                        cibilSRespType.setDISP_REM_LINE2(hibCibilSropDomain.getDisputeRemarkLine2() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine2());
                        cibilSRespType.setDISP_REM_LINE3(hibCibilSropDomain.getDisputeRemarkLine3() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine3());
                        cibilSRespType.setDISP_REM_LINE4(hibCibilSropDomain.getDisputeRemarkLine4() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine4());
                        cibilSRespType.setDISP_REM_LINE5(hibCibilSropDomain.getDisputeRemarkLine5() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine5());
                        cibilSRespType.setDISP_REM_LINE6(hibCibilSropDomain.getDisputeRemarkLine6() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine6());
                        cibilSRespType.setOUTPUT_WRITE_FLAG(hibCibilSropDomain.getOutputWriteFlag() == null ? "" : hibCibilSropDomain.getOutputWriteFlag());
                        cibilSRespType.setOUTPUT_WRITE_TIME(hibCibilSropDomain.getOutputWriteTime() == null ? "" : hibCibilSropDomain.getOutputWriteTime());
                        cibilSRespType.setOUTPUT_READ_TIME(hibCibilSropDomain.getOutputReadTime() == null ? "" : hibCibilSropDomain.getOutputReadTime());

                        cibilSRespTypeList.add(cibilSRespType);

                    }
                    responseJSONType.setCIBIL_SROP_DOMAIN_LIST(cibilSRespTypeList.toArray(new CibilSRespType[cibilSRespTypeList.size()]));
                }
            }
        } catch (Exception e) {
            logger_.error(e.getMessage(), e);
            e.printStackTrace();
        }
        logger_.warn("** SoaSropEropMapper ** cibilSropMapping >>");
        return responseJSONType;
    }
}