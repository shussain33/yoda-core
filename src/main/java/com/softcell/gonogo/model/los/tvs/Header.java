package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Header {

	    private String memberReferenceNumber;

	    private String enquiryControlNumber;

	    private String dateOfRequest;

	    private String batchId;

	    private String enquiryMemberUserID;

	    private String reportId;

	    private String timeProceed;

	    private String version;

	    private String preparedFor;

	    private String dateProceed;

	    private String subjectReturnCode;

	    private String dateOfIssue;

	    private String preparedForId;


}
