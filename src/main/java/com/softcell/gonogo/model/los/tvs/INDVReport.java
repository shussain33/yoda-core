package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class INDVReport {

	    private IndvResponses indvResponses;

	    private Alerts alerts;

	    private Request request;

	    private PrintableReport printableReport;

	    private PersonalInfoVariation personalInfoVariation;

	    private StatusDetails statusDetails;

	    private Header header;

	    private AccountSummary accountSummary;


}
