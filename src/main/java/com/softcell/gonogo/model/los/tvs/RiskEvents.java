package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class RiskEvents {


    private String countOfPenaltiesInUtilityBills;

    private String connectionSuspendedPostpaid;

    private String billOverdueBroadband;

    private String connectionSuspendedBroadband;

    private String connectionSuspendedLandline;

    private String billOverdueElectricity;

    private String billOverduePostpaid;

    private String billOverdueLandline;

}
