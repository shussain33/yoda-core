package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressPermanent {

		private String addressline2;

	    private String addressline1;

	    private String state;

	    private int monthataddress;

	    private int pin;

	    private String city;

}
