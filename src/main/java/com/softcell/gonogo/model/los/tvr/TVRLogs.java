package com.softcell.gonogo.model.los.tvr;

import com.softcell.gonogo.model.AuditEntity;
import lombok.Builder;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by yogeshb on 28/7/17.
 */
@Document(collection = "TVRLogs")
@Builder
public class TVRLogs extends AuditEntity {
    private String refId;
    private TvrRequestDTO tvrRequest;
    private TvrResponse tvrResponse;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public TvrRequestDTO getTvrRequest() {
        return tvrRequest;
    }

    public void setTvrRequest(TvrRequestDTO tvrRequest) {
        this.tvrRequest = tvrRequest;
    }

    public TvrResponse getTvrResponse() {
        return tvrResponse;
    }

    public void setTvrResponse(TvrResponse tvrResponse) {
        this.tvrResponse = tvrResponse;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TVRLogs{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", tvrRequest=").append(tvrRequest);
        sb.append(", tvrResponse=").append(tvrResponse);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TVRLogs tvrLogs = (TVRLogs) o;

        if (refId != null ? !refId.equals(tvrLogs.refId) : tvrLogs.refId != null) return false;
        if (tvrRequest != null ? !tvrRequest.equals(tvrLogs.tvrRequest) : tvrLogs.tvrRequest != null) return false;
        return tvrResponse != null ? tvrResponse.equals(tvrLogs.tvrResponse) : tvrLogs.tvrResponse == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (refId != null ? refId.hashCode() : 0);
        result = 31 * result + (tvrRequest != null ? tvrRequest.hashCode() : 0);
        result = 31 * result + (tvrResponse != null ? tvrResponse.hashCode() : 0);
        return result;
    }
}
