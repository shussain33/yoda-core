package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CreditCardAccount {

        private CreditScore creditScore;

        private AuxiliaryInfo auxiliaryInfo;

        private LocationProfile locationProfile;

        private LoanAccount loanAccount;

        private String avgCCTraxnAmountLast3Mon;

        private UtilityAccount utilityAccount;

        private ContactInfo contactInfo;

        private String countTotalCreditCard;

        private String countActiveCreditCards;

        private Income income;

        private AppographicProfile appographicProfile;

        private String ccDataSufficiencyFlag;

        private String monthsCCDataAvailable;

        private FraudCheck fraudCheck;

        private Cards[] cards;

}

