package com.softcell.gonogo.model.los.tvs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsertOrUpdateTvsRecordGroupFive {

	 	private Pdfs[] pdfs;

	    private ShippingAddressResidence shippingAddressResidence;

	    private ShippingAddressPermanent shippingAddressPermanent;

	    private ShippingAddressOffice shippingAddressOffice;

	    private Invoice invoice;

	    private ShippingAddressOther shippingAddressOther;

		@JsonProperty("CDLos")
		private CDLos CDLos;

		@JsonProperty("DMSDocuments")
	    private DMSDocuments[] DMSDocuments;


	}

