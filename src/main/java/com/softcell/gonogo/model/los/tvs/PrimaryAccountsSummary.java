package com.softcell.gonogo.model.los.tvs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class PrimaryAccountsSummary {

	private String primaryUnsecuredNumberOfAccounts;

    private String primaryOverdueNumberOfAccounts;

    private String primaryCurrentBalance;

    private String primaryDisbursedAmount;

    private String primarySanctionedAmount;

    private String primaryActiveNumberOfAccounts;

    private String primaryUntaggedNumberOfAccounts;

    private String primaryNumberOfAccounts;

    private String primarySecuredNumberOfAccounts;


}
