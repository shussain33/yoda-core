package com.softcell.gonogo.model.reports.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoNoGoReportRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("dtValueDateFrom")
    private Date valueDateFrom;

    @JsonProperty("dtValueDateTo")
    private Date valueDateTo;

    @JsonProperty("sReportName")
    private String reportName;
}
