package com.softcell.gonogo.model.reports;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;


/**
 * @author yogeshb
 */

@JsonPropertyOrder({"REFERENCE_ID", "CHANNEL", "APPLICATION_DATE",
        "APPLICATION_TIME", "DSA_ID"})
public class ChannelRptDomain {

    @JsonProperty("REFERENCE_ID")
    private String refID;

    @JsonProperty("APPLICATION_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "dd-MM-yyyy")
    private Date date;

    @JsonProperty("APPLICATION_TIME")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "hh:mm a")
    private Date time;

    @JsonProperty("DSA_ID")
    private String dsaID;

    @JsonProperty("CHANNEL")
    private String channel;

    @JsonProperty("REFERENCE_ID")
    public String getRefID() {
        return refID;
    }

    @JsonProperty("REFERENCE_ID")
    public void setRefID(String refID) {
        this.refID = refID;
    }

    @JsonProperty("APPLICATION_DATE")
    public Date getDate() {
        return date;
    }

    @JsonProperty("APPLICATION_DATE")
    public void setDate(Date date) {
        this.date = date;
    }

    @JsonProperty("APPLICATION_TIME")
    public Date getTime() {
        return time;
    }

    @JsonProperty("APPLICATION_TIME")
    public void setTime(Date time) {
        this.time = time;
    }

    @JsonProperty("DSA_ID")
    public String getDsaID() {
        return dsaID;
    }

    @JsonProperty("DSA_ID")
    public void setDsaID(String dsaID) {
        this.dsaID = dsaID;
    }

    @JsonProperty("CHANNEL")
    public String getChannel() {
        return channel;
    }

    @JsonProperty("CHANNEL")
    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ChannelRptDomain [refID=");
        builder.append(refID);
        builder.append(", date=");
        builder.append(date);
        builder.append(", time=");
        builder.append(time);
        builder.append(", dsaID=");
        builder.append(dsaID);
        builder.append(", channel=");
        builder.append(channel);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((channel == null) ? 0 : channel.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((dsaID == null) ? 0 : dsaID.hashCode());
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        result = prime * result + ((time == null) ? 0 : time.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ChannelRptDomain))
            return false;
        ChannelRptDomain other = (ChannelRptDomain) obj;
        if (channel == null) {
            if (other.channel != null)
                return false;
        } else if (!channel.equals(other.channel))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (dsaID == null) {
            if (other.dsaID != null)
                return false;
        } else if (!dsaID.equals(other.dsaID))
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        if (time == null) {
            if (other.time != null)
                return false;
        } else if (!time.equals(other.time))
            return false;
        return true;
    }
}
