/**
 *
 */
package com.softcell.gonogo.model.reports;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * @author yogeshb
 */
public class DeviceInfoRptDomain {

    @JsonProperty("INSTITUTION")
    private String institutionID;

    @JsonProperty("DSA_ID")
    private String dsaID;

    @JsonProperty("REFERENCE_ID")
    private String referenceID;

    @JsonProperty("APPLICATION_DATE")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "dd-MM-yyyy")
    private Date date;

    @JsonProperty("APPLICATION_TIME")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "hh:mm a")
    private Date time;

    @JsonProperty("CHANNEL")
    private String appSource;

    @JsonProperty("LATTITUDE")
    private String lattitude;

    @JsonProperty("LONGITUDE")
    private String longitude;

    @JsonProperty("IMEI_NUMBER")
    private String imeiNumber;

    @JsonProperty("IP_ADDRESS")
    private String ipAddress;

    @JsonProperty("DEVICE_ID")
    private String deviceID;

    @JsonProperty("DEVICE_MANUFACTURER")
    private String deviceManufacturer;

    @JsonProperty("DEVICE_MODEL")
    private String deviceModel;

    @JsonProperty("BROWSER_NAME")
    private String browserName;

    @JsonProperty("BROWSER_VERSION")
    private String browserVersion;

    @JsonProperty("MAC_ADDRESS")
    private String macAddress;

    @JsonProperty("OS_NAME")
    private String osName;

    /**
     * @return the institutionID
     */
    public String getInstitutionID() {
        return institutionID;
    }

    /**
     * @param institutionID the institutionID to set
     */
    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    /**
     * @return the dsaID
     */
    public String getDsaID() {
        return dsaID;
    }

    /**
     * @param dsaID the dsaID to set
     */
    public void setDsaID(String dsaID) {
        this.dsaID = dsaID;
    }

    /**
     * @return the referenceID
     */
    public String getReferenceID() {
        return referenceID;
    }

    /**
     * @param referenceID the referenceID to set
     */
    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * @return the appSource
     */
    public String getAppSource() {
        return appSource;
    }

    /**
     * @param appSource the appSource to set
     */
    public void setAppSource(String appSource) {
        this.appSource = appSource;
    }

    /**
     * @return the lattitude
     */
    public String getLattitude() {
        return lattitude;
    }

    /**
     * @param lattitude the lattitude to set
     */
    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the imeiNumber
     */
    public String getImeiNumber() {
        return imeiNumber;
    }

    /**
     * @param imeiNumber the imeiNumber to set
     */
    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress the ipAddress to set
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the deviceID
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * @param deviceID the deviceID to set
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * @return the browserName
     */
    public String getBrowserName() {
        return browserName;
    }

    /**
     * @param browserName the browserName to set
     */
    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    /**
     * @return the browserVersion
     */
    public String getBrowserVersion() {
        return browserVersion;
    }

    /**
     * @param browserVersion the browserVersion to set
     */
    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    /**
     * @return the macAddress
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * @param macAddress the macAddress to set
     */
    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    /**
     * @return the osName
     */
    public String getOsName() {
        return osName;
    }

    /**
     * @param osName the osName to set
     */
    public void setOsName(String osName) {
        this.osName = osName;
    }

    /**
     * @return the deviceManufacturer
     */
    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    /**
     * @param deviceManufacturer the deviceManufacturer to set
     */
    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    /**
     * @return the deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * @param deviceModel the deviceModel to set
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DeviceInfoRptDomain [institutionID=");
        builder.append(institutionID);
        builder.append(", dsaID=");
        builder.append(dsaID);
        builder.append(", referenceID=");
        builder.append(referenceID);
        builder.append(", date=");
        builder.append(date);
        builder.append(", time=");
        builder.append(time);
        builder.append(", appSource=");
        builder.append(appSource);
        builder.append(", lattitude=");
        builder.append(lattitude);
        builder.append(", longitude=");
        builder.append(longitude);
        builder.append(", imeiNumber=");
        builder.append(imeiNumber);
        builder.append(", ipAddress=");
        builder.append(ipAddress);
        builder.append(", deviceID=");
        builder.append(deviceID);
        builder.append(", deviceManufacturer=");
        builder.append(deviceManufacturer);
        builder.append(", deviceModel=");
        builder.append(deviceModel);
        builder.append(", browserName=");
        builder.append(browserName);
        builder.append(", browserVersion=");
        builder.append(browserVersion);
        builder.append(", macAddress=");
        builder.append(macAddress);
        builder.append(", osName=");
        builder.append(osName);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((appSource == null) ? 0 : appSource.hashCode());
        result = prime * result
                + ((browserName == null) ? 0 : browserName.hashCode());
        result = prime * result
                + ((browserVersion == null) ? 0 : browserVersion.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result
                + ((deviceID == null) ? 0 : deviceID.hashCode());
        result = prime
                * result
                + ((deviceManufacturer == null) ? 0 : deviceManufacturer
                .hashCode());
        result = prime * result
                + ((deviceModel == null) ? 0 : deviceModel.hashCode());
        result = prime * result + ((dsaID == null) ? 0 : dsaID.hashCode());
        result = prime * result
                + ((imeiNumber == null) ? 0 : imeiNumber.hashCode());
        result = prime * result
                + ((institutionID == null) ? 0 : institutionID.hashCode());
        result = prime * result
                + ((ipAddress == null) ? 0 : ipAddress.hashCode());
        result = prime * result
                + ((lattitude == null) ? 0 : lattitude.hashCode());
        result = prime * result
                + ((longitude == null) ? 0 : longitude.hashCode());
        result = prime * result
                + ((macAddress == null) ? 0 : macAddress.hashCode());
        result = prime * result + ((osName == null) ? 0 : osName.hashCode());
        result = prime * result
                + ((referenceID == null) ? 0 : referenceID.hashCode());
        result = prime * result + ((time == null) ? 0 : time.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeviceInfoRptDomain other = (DeviceInfoRptDomain) obj;
        if (appSource == null) {
            if (other.appSource != null)
                return false;
        } else if (!appSource.equals(other.appSource))
            return false;
        if (browserName == null) {
            if (other.browserName != null)
                return false;
        } else if (!browserName.equals(other.browserName))
            return false;
        if (browserVersion == null) {
            if (other.browserVersion != null)
                return false;
        } else if (!browserVersion.equals(other.browserVersion))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (deviceID == null) {
            if (other.deviceID != null)
                return false;
        } else if (!deviceID.equals(other.deviceID))
            return false;
        if (deviceManufacturer == null) {
            if (other.deviceManufacturer != null)
                return false;
        } else if (!deviceManufacturer.equals(other.deviceManufacturer))
            return false;
        if (deviceModel == null) {
            if (other.deviceModel != null)
                return false;
        } else if (!deviceModel.equals(other.deviceModel))
            return false;
        if (dsaID == null) {
            if (other.dsaID != null)
                return false;
        } else if (!dsaID.equals(other.dsaID))
            return false;
        if (imeiNumber == null) {
            if (other.imeiNumber != null)
                return false;
        } else if (!imeiNumber.equals(other.imeiNumber))
            return false;
        if (institutionID == null) {
            if (other.institutionID != null)
                return false;
        } else if (!institutionID.equals(other.institutionID))
            return false;
        if (ipAddress == null) {
            if (other.ipAddress != null)
                return false;
        } else if (!ipAddress.equals(other.ipAddress))
            return false;
        if (lattitude == null) {
            if (other.lattitude != null)
                return false;
        } else if (!lattitude.equals(other.lattitude))
            return false;
        if (longitude == null) {
            if (other.longitude != null)
                return false;
        } else if (!longitude.equals(other.longitude))
            return false;
        if (macAddress == null) {
            if (other.macAddress != null)
                return false;
        } else if (!macAddress.equals(other.macAddress))
            return false;
        if (osName == null) {
            if (other.osName != null)
                return false;
        } else if (!osName.equals(other.osName))
            return false;
        if (referenceID == null) {
            if (other.referenceID != null)
                return false;
        } else if (!referenceID.equals(other.referenceID))
            return false;
        if (time == null) {
            if (other.time != null)
                return false;
        } else if (!time.equals(other.time))
            return false;
        return true;
    }


}
