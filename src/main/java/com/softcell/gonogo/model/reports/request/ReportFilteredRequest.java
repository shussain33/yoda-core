package com.softcell.gonogo.model.reports.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;


/**
 * @author yogeshb
 */

public class ReportFilteredRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("sProduct")
    @NotEmpty(
            groups = {
                    ReportFilteredRequest.FetchGrp.class
            }
    )
    private String product;

    @JsonProperty("dtStartFrom")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    ReportFilteredRequest.FetchGrp.class
            }
    )
    private Date dateStartFrom;

    @JsonProperty("dtEnd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    ReportFilteredRequest.FetchGrp.class
            }
    )
    private Date endDate;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Date getDateStartFrom() {
        return dateStartFrom;
    }

    public void setDateStartFrom(Date dateStartFrom) {
        this.dateStartFrom = dateStartFrom;
    }

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ReportFilteredRequest [header=");
        builder.append(header);
        builder.append(", product=");
        builder.append(product);
        builder.append(", dateStartFrom=");
        builder.append(dateStartFrom);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((dateStartFrom == null) ? 0 : dateStartFrom.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReportFilteredRequest other = (ReportFilteredRequest) obj;
        if (dateStartFrom == null) {
            if (other.dateStartFrom != null)
                return false;
        } else if (!dateStartFrom.equals(other.dateStartFrom))
            return false;
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;
        return true;
    }

    public interface FetchGrp {
    }
}
