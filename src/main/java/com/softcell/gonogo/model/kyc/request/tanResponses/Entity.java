package com.softcell.gonogo.model.kyc.request.tanResponses;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 20/7/18.
 */
public class Entity {

    @JsonProperty("categoryOfDeductor")
    public String categoryOfDeductor;
    @JsonProperty("name")
    public String name;
    @JsonProperty("emailId1")
    public String emailId1;
    @JsonProperty("emailId2")
    public String emailId2;
    @JsonProperty("address")
    public String address;
    @JsonProperty("statusOfTan")
    public String statusOfTan;
    @JsonProperty("tan")
    public String tan;
    @JsonProperty("pan")
    public String pan;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
