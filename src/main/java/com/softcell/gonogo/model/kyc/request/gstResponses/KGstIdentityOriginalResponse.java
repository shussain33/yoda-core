package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 25/6/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KGstIdentityOriginalResponse {

    @JsonProperty("request_id")
    private String requestId;

    @JsonProperty("status-code")
    private String statusCode;

    @JsonProperty("result")
    private List<KGstIdentityResult> result;
}
