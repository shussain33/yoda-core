package com.softcell.gonogo.model.kyc.karza.drivlic.request;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by archana on 21/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KDrivLicResult {
    @JsonProperty("issue_date")
    public String issueDate;
    @JsonProperty("father/husband")
    public String fatherHusband;
    @JsonProperty("name")
    public String name;
    @JsonProperty("img")
    public String img;
    @JsonProperty("blood_group")
    public String bloodGroup;
    @JsonProperty("dob")
    public String dob;
    @JsonProperty("validity")
    public Validity validity;
    @JsonProperty("cov_details")
    public List<CovDetail> covDetails = null;
    @JsonProperty("address")
    public String address;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
