package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by Softcell on 17/07/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TotalKycVoterDetailRequest
{
    @JsonProperty("oHeader")
    @NotNull( groups = {Header.InstWithProductGrp.class})
    @Valid
    private Header header;


    @JsonProperty("epic_no")
    @NotNull( groups = {TotalKycVoterDetailRequest.FetchGrp.class})
    @Valid
    private String epicNo;

    public interface FetchGrp {
    }

}