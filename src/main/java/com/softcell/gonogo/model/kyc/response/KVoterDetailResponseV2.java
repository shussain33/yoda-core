package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek on 10/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KVoterDetailResponseV2 {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("sOrgRes")
    private String orgRes;

    //Response details
    @JsonProperty("sRlnNameV1")
    private String rlnNameV1;

    @JsonProperty("sPartNo")
    private String partNo;

    @JsonProperty("sRlnType")
    private String rlnType;

     @JsonProperty("sId")
     private String id;

    @JsonProperty("sNameV1")
    private String nameV1;

    @JsonProperty("sRlnName")
    private String rlnName;

    @JsonProperty("sDistrict")
    private String district;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sPsName")
    private String psName;

    @JsonProperty("sPcName")
    private String pcName;

    @JsonProperty("sSlNoInpart")
    private String slNoInpart;

    @JsonProperty("sName")
    private String name;

    @JsonProperty("sPartName")
    private String partName;

    @JsonProperty("sDob")
    private String dob;

    @JsonProperty("sGender")
    private String gender;

    @JsonProperty("sAge")
    private String age;

    @JsonProperty("sAcName")
    private String acName;

    @JsonProperty("sEpicNo")
    private String epicNo;

    @JsonProperty("sPsLatLong")
    private String psLatLong;

    @JsonProperty("sRlnNameV2")
    private String rlnNameV2;

    @JsonProperty("sRlnNameV3")
    private String rlnNameV3;

    @JsonProperty("sSelectionNo")
    private String selectionNo;

    @JsonProperty("sLastUpdate")
    private String lastUpdate;

    @JsonProperty("sAcNo")
    private String acNo;

    @JsonProperty("sHouseNo")
    private String houseNo;

    @JsonProperty("sStCode")
    private String stCode;

    @JsonProperty("sNameV2")
    private String nameV2;

    @JsonProperty("sNameV3")
    private String nameV3;

    @JsonProperty("sRequestId")
    private String request_id;

    @JsonProperty("sStatusCode")
    private String status_code;
}
