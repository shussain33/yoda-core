package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yogesh on 22/6/18.
 */
public class KGstAuthResult {

    @JsonProperty("mbr")
    public List<String> mbr = null;
    @JsonProperty("canFlag")
    public String canFlag;
    @JsonProperty("pradr")
    public PradrDetails pradr;
    @JsonProperty("tradeNam")
    public String tradeNam;
    @JsonProperty("contacted")
    public String contacted;
    @JsonProperty("gstin")
    public String gstin;
    @JsonProperty("nba")
    public List<String> nba = null;
    @JsonProperty("stjCd")
    public String stjCd;
    @JsonProperty("stj")
    public String stj;
    @JsonProperty("ppr")
    public String ppr;
    @JsonProperty("dty")
    public String dty;
    @JsonProperty("cmpRt")
    public String cmpRt;
    @JsonProperty("lstupdt")
    public String lstupdt;
    @JsonProperty("ctb")
    public String ctb;
    @JsonProperty("sts")
    public String sts;
    @JsonProperty("cxdt")
    public String cxdt;
    @JsonProperty("adadr")
    public List<Object> adadr = null;
    @JsonProperty("lgnm")
    public String lgnm;
    @JsonProperty("ctjCd")
    public String ctjCd;
    @JsonProperty("ctj")
    public String ctj;
    @JsonProperty("rgdt")
    public String rgdt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
