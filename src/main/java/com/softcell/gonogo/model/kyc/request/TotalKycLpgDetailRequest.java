package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by anupamad on 11/7/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TotalKycLpgDetailRequest {

    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    @NotNull( groups = {Header.InstWithProductGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sMobileNumber")
    @NotNull( groups = {TotalKycLpgDetailRequest.FetchGrp.class})
    @Valid
    private String mobileNumber;

    public interface FetchGrp {
    }

}
