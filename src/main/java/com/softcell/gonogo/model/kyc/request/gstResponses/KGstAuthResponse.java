package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.*;


/**
 * Created by yogesh on 27/12/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KGstAuthResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private Error error;

    @JsonProperty("sOrgRes")
    private KGstAuthOriginalResponse orgRes;

}
