package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.kyc.response.KPanAuthResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 22/6/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KGstAuthOriginalResponse {

    @JsonProperty("request_id")
    private String requestId;

    @JsonProperty("status-code")
    private String statusCode;

    @JsonProperty("result")
    private KGstAuthResult result;

}
