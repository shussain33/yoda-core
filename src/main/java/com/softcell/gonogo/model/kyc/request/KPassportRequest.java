package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 21/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KPassportRequest {

    @JsonProperty("consent")
    public String consent;
    @JsonProperty("name")
    public String name;
    @JsonProperty("last_name")
    public String lastName;
    @JsonProperty("dob")
    public String dob;
    @JsonProperty("doe")
    public String doe;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("passport_no")
    public String passportNo;
    @JsonProperty("type")
    public String type;
    @JsonProperty("country")
    public String country;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
