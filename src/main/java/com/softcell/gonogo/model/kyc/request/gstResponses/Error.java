package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 26/6/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Error {

    @JsonProperty("sType")
    private String type;

    @JsonProperty("sMsg")
    private String message;
}
