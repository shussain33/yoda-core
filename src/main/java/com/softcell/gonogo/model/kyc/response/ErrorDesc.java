package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 15/3/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDesc {
    @JsonProperty("sType")
    private String type;

    @JsonProperty("sMsg")
    private String message;

}
