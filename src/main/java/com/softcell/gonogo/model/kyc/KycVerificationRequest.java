package com.softcell.gonogo.model.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by archana on 25/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KycVerificationRequest {
    @JsonProperty("oHeader")
    @NotNull( groups = {Header.InstWithProductGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotNull
    public String refId;

    // PAN / GST/ GSTAUTH / TAN / TANAUTH / PASSPORT / VOTER-ID / DRIVING-LICENSE
    @JsonProperty("sKycType")
    @NotNull
    public String kycType;

    @JsonProperty("sKycNumber")
    @NotNull
    public String kycNumber;

    // Following fields are required as per kycType.
    @JsonProperty("sState")
    private String state;

    @JsonProperty("sExpiryDate")
    private String sExpiryDate;

    public interface InsertGrp{}
}
