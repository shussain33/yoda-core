package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 22/6/18.
 */
public class PradrDetails {

    @JsonProperty("em")
    public String em;
    @JsonProperty("adr")
    public String adr;
    @JsonProperty("addr")
    public String addr;
    @JsonProperty("mb")
    public String mb;
    @JsonProperty("ntr")
    public String ntr;
    @JsonProperty("lastUpdatedDate")
    public String lastUpdatedDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
