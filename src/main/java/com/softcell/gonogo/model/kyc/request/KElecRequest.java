package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by anupamad on 12/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class KElecRequest {

    @JsonProperty("consumer_id")
    private String consumerId;

    @JsonProperty("service_provider")
    private String serviceProvider;

    @JsonProperty("key")
    private String key;
}
