package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by anupamad on 13/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KDrivLicRequest {
    @JsonProperty("dl_no")
    private String dlNo;

    @JsonProperty("dob")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private String dob;

    @JsonProperty("key")
    private String key;
}
