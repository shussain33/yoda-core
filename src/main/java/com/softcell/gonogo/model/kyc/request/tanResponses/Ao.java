package com.softcell.gonogo.model.kyc.request.tanResponses;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 20/7/18.
 */
public class Ao {

    @JsonProperty("emailId")
    public String emailId;
    @JsonProperty("aoType")
    public String aoType;
    @JsonProperty("buildingName")
    public String buildingName;
    @JsonProperty("areaCode")
    public String areaCode;
    @JsonProperty("rangeCode")
    public String rangeCode;
    @JsonProperty("aoDescription")
    public String aoDescription;
    @JsonProperty("aoNumber")
    public String aoNumber;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
