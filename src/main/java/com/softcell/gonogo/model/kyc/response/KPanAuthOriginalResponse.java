package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg408 on 5/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KPanAuthOriginalResponse {

    @JsonProperty("request_id")
    private String requestId;

    @JsonProperty("status-code")
    private String statusCode;

    @JsonProperty("result")
    private KPanAuthResult result;
}
