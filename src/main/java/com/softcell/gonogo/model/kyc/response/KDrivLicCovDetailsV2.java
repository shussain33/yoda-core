package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Created by abhishek on 22/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KDrivLicCovDetailsV2 {
    @JsonProperty("issue_date")
    private String issueDate;

    @JsonProperty("cov")
    private String cov;
}
