package com.softcell.gonogo.model.kyc.request.VoterIdResponses;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KVoterIdResult {

    @JsonProperty("ps_lat_long")
    public String psLatLong;
    @JsonProperty("rln_name_v1")
    public String rlnNameV1;
    @JsonProperty("rln_name_v2")
    public String rlnNameV2;
    @JsonProperty("rln_name_v3")
    public String rlnNameV3;
    @JsonProperty("part_no")
    public String partNo;
    @JsonProperty("rln_type")
    public String rlnType;
    @JsonProperty("section_no")
    public String sectionNo;
    @JsonProperty("id")
    public String id;
    @JsonProperty("name_v1")
    public String nameV1;
    @JsonProperty("rln_name")
    public String rlnName;
    @JsonProperty("district")
    public String district;
    @JsonProperty("last_update")
    public String lastUpdate;
    @JsonProperty("state")
    public String state;
    @JsonProperty("ac_no")
    public String acNo;
    @JsonProperty("house_no")
    public String houseNo;
    @JsonProperty("ps_name")
    public String psName;
    @JsonProperty("pc_name")
    public String pcName;
    @JsonProperty("slno_inpart")
    public String slnoInpart;
    @JsonProperty("name")
    public String name;
    @JsonProperty("part_name")
    public String partName;
    @JsonProperty("st_code")
    public String stCode;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("age")
    public Integer age;
    @JsonProperty("ac_name")
    public String acName;
    @JsonProperty("epic_no")
    public String epicNo;
    @JsonProperty("name_v3")
    public String nameV3;
    @JsonProperty("name_v2")
    public String nameV2;
    @JsonProperty("dob")
    public String dob;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
