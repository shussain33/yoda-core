package com.softcell.gonogo.model.kyc.request.gstRequests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import com.softcell.gonogo.model.kyc.request.TotalKycVoterDetailRequestV2;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 26/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TotalKycGstAuthDetailRequest {
    @JsonProperty("oHeader")
    @NotNull( groups = {Header.InstWithProductGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sGstin")
    @NotNull( groups = {TotalKycGstAuthDetailRequest.InsertGrp.class})
    @Valid
    private String gstin;

    @JsonProperty("sConsent")
    @NotNull( groups = {TotalKycGstAuthDetailRequest.InsertGrp.class})
    @Valid
    private KConsentEnum consent;

    public interface InsertGrp{}
}
