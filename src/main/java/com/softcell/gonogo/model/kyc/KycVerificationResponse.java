package com.softcell.gonogo.model.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 25/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KycVerificationResponse {

    @JsonProperty("sRefID")
    public String refId;

    @JsonProperty("sKycType")
    public String kycType;

    @JsonProperty("sKycNumber")
    public String kycNumber;

    @JsonProperty("bVerified")
    public boolean verified;
}
