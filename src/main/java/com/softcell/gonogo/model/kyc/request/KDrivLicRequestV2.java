package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek on 6/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KDrivLicRequestV2 {

    @JsonProperty("dl_no")
    private String dlNo;

    @JsonProperty("dob")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private String dob;

    @JsonProperty("consent")
    private KConsentEnum consent;}
