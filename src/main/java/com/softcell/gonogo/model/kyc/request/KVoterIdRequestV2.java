package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek on 7/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KVoterIdRequestV2 {
    @JsonProperty("epic_no")
    private String epicNo;

    @JsonProperty("consent")
    private KConsentEnum consent;}
