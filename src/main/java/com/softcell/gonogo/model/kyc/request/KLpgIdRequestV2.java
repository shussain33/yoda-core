package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek on 6/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KLpgIdRequestV2 {

    @JsonProperty(value = "input", required = true)
    private String mobileNumber;

    @JsonProperty("consent")
    private KConsentEnum consent;}
