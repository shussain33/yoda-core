package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 19/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KarzaResponse {
    @JsonProperty("request_id")
    private String requestId;

    @JsonProperty("status-code")
    private String statusCode;

    @JsonProperty("result")
    private Object result;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        if( this.additionalProperties == null ) {
            additionalProperties = new HashMap<>();
        }
        this.additionalProperties.put(name, value);
    }
}
