package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import com.softcell.gonogo.model.kyc.request.gstRequests.KGstRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by ssg408 on 5/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KPanRequest {
    @NotNull(groups = {KGstRequest.InputGrp.class})
    @JsonProperty("pan")
    private String pan;

    @JsonProperty("consent")
    private KConsentEnum consent;

    public interface InputGrp{

    }
}
