package com.softcell.gonogo.model.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.kyc.response.KDrivLicCovDetailsV2;
import com.softcell.gonogo.model.kyc.response.KDrivLicValidity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by anupamad on 14/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "totalKycReqResLog")
public class TotalKycReqResLog extends AuditEntity {
    //Common Feilds For All
    @JsonProperty("sKycId")
    private String kycId;

    @JsonProperty("sAction")
    private String action;

    @JsonProperty("sOrgReq")
    private String orgReq;

    @JsonProperty("sOrgRes")
    private String orgRes;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("sRequestId")
    private String requestId;

    @JsonProperty("sKycVersion")
    private String kycVersion;

}
