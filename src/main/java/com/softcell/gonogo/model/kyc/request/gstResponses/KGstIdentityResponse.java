package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 25/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KGstIdentityResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private Error error;

    @JsonProperty("sOrgRes")
    private KGstIdentityOriginalResponse orgRes;
}
