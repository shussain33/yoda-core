package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Created by abhishek on 22/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KDrivLicValidity {
    @JsonProperty("non-transport")
    private String nonTransport;

    @JsonProperty("transport")
    private String transport;
}
