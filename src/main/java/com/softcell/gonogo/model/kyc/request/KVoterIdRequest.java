package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Created by Softcell on 17/07/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KVoterIdRequest {
    @JsonProperty("epic_no")
    private String epicNo;

    @JsonProperty("key")
    private String key;
}
