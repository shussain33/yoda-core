package com.softcell.gonogo.model.kyc.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 18/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KarzaRequest {

    String kycType;

    String kycRequest;

    String karzaKey;

}
