package com.softcell.gonogo.model.kyc.request.tanRequests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 19/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KTanRequest {

    @NotNull(groups = {KTanRequest.InputGrp.class})
    @JsonProperty("tan")
    private String tan;

    @JsonProperty("consent")
    private KConsentEnum consent;

    public interface InputGrp{

    }
}
