package com.softcell.gonogo.model.kyc.request.gstResponses;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 25/6/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KGstIdentityResult {

    @JsonProperty("emailId")
    public String emailId;
    @JsonProperty("applicationStatus")
    public String applicationStatus;
    @JsonProperty("mobNum")
    public String mobNum;
    @JsonProperty("pan")
    public String pan;
    @JsonProperty("gstinRefId")
    public String gstinRefId;
    @JsonProperty("regType")
    public String regType;
    @JsonProperty("gstinId")
    public String gstinId;
    @JsonProperty("registrationName")
    public String registrationName;
    @JsonProperty("tinNumber")
    public String tinNumber;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
