package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 19/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RawBaseResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ErrorDesc error;

    @JsonProperty("oKarzaResponse")
    private KarzaResponse karzaResponse;
}
