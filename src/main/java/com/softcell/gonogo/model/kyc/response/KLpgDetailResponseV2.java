package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek on 10/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KLpgDetailResponseV2 {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("sOrgRes")
    private String orgRes;

    //Response details
    @JsonProperty("sPin")
    private String pin;

    @JsonProperty("sCityTown")
    private String cityTown;

    @JsonProperty("sConsumerNo")
    private String consumerNo;

    @JsonProperty("sConsumerName")
    private String consumerName;

    @JsonProperty("sConsumerEmail")
    private String consumerEmail;

    @JsonProperty("sConsumerAddress")
    private String consumerAddress;

    @JsonProperty("sConsumerContact")
    private String consumerContact;

    @JsonProperty("sDistributorCode")
    private String distributorCode;

    @JsonProperty("sDistributorName")
    private String distributorName;

    @JsonProperty("sSubsidizedRefilConsumed")
    private String subsidizedRefilConsumed;

    @JsonProperty("sGivenUpSubsidy")
    private String givenUpSubsidy;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sIFSCCode")
    private String IFSCCode;

    @JsonProperty("sAadharNumber")
    private String aadharNumber;

    @JsonProperty("sApproximateSubsidyAvailed")
    private String approximateSubsidyAvailed;

    @JsonProperty("sDistributorAddress")
    private String distributorAddress;

    @JsonProperty("sBankAccountNo")
    private String bankAccountNo;

    @JsonProperty("sLastBookingDate")
    private String lastBookingDate;

    @JsonProperty("sTotalRefillConsumed")
    private String totalRefillConsumed;

    @JsonProperty("sRequestId")
    private String request_id;

    @JsonProperty("sStatusCode")
    private String status_code;
}
