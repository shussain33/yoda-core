package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek on 10/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KDrivLicResponseV2 {
    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("sOrgRes")
    private String orgRes;

    @JsonProperty("sFatherHusband")
    private String fatherHusbandName;

    @JsonProperty("sAddress")
    private String address;

    @JsonProperty("sDlNumber")
    private String dlNumber;

    @JsonProperty("sDob")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private String dob;

    @JsonProperty("sName")
    private String name;

    @JsonProperty("sIssueDate")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private String issueDate;

    @JsonProperty("sExpDate")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private String expDate;

    @JsonProperty("sImage")
    private String image;

    @JsonProperty("sBloodGroup")
    private String bloodGroup;

    @JsonProperty("validity")
    private KDrivLicValidity validity;

    @JsonProperty("cov_details")
    private KDrivLicCovDetailsV2[] kDrivLicCovDetailsV2;

    @JsonProperty("sRequestId")
    private String request_id;

    @JsonProperty("sStatusCode")
    private String status_code;
}
