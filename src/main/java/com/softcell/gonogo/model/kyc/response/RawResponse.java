package com.softcell.gonogo.model.kyc.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Created by yogesh on 18/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RawResponse {

    Map<Integer,String> response;
}
