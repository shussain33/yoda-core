package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.kyc.request.gstResponses.Error;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg408 on 5/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KPanAuthResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private Error error;

    @JsonProperty("sOrgRes")
    private KPanAuthOriginalResponse orgRes;
}
