package com.softcell.gonogo.model.kyc.request.gstRequests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 25/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KGstIdentityRequest {

    @JsonProperty("sGstin")
    private String gstin;

    @JsonProperty("sKycNumber")
    private String kycNumber;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("eConsent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("bGstinAvailable")
    private boolean gstinAvailable;
}
