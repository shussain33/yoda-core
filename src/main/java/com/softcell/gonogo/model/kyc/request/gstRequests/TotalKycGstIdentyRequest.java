package com.softcell.gonogo.model.kyc.request.gstRequests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 27/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TotalKycGstIdentyRequest {

    @JsonProperty("oHeader")
    @NotNull( groups = {Header.InstWithProductGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sGstin")
    @Valid
    private String gstin;

    @JsonProperty("sKycNumber")
    private String kycNumber;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sConsent")
    @NotNull
    private KConsentEnum consent;

    @JsonProperty("bGstinAvailable")
    private boolean gstinAvailable;

    public interface InsertGrp{}
}
