package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by anupamad on 11/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KLpgIdRequest {
    @JsonProperty(value = "input", required = true)
    private String mobileNumber;

    @JsonProperty("key")
    private String key;
}
