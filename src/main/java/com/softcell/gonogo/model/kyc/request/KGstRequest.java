package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 22/6/18.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KGstRequest {
    @NotNull(groups = {InputGrp.class})
    @JsonProperty("gstin")
    private String gstin;

    @JsonProperty("consent")
    private KConsentEnum consent;

    public interface InputGrp{

    }
}
