package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek on 10/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KElectrBillResponseV2 {
    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("sOrgRes")
    private String orgRes;

    @JsonProperty("sMobileNumber")
    private String mobileNumber;

    @JsonProperty("sAddress")
    private String address;

    @JsonProperty("sConsumerName")
    private String consumerName;

    @JsonProperty("sConsumerNumber")
    private String consumerNumber;

    @JsonProperty("sEmailAddress")
    private String emailAddress;

    @JsonProperty("sBillNo")
    private String billNo;

    @JsonProperty("sBillDueDate")
    private String billDueDate;

    @JsonProperty("sBillAmount")
    private String billAmount;

    @JsonProperty("sBillIssueDate")
    private String billIssueDate;

    @JsonProperty("sAmountPayable")
    private String amountPayable;

    @JsonProperty("sTotalAmount")
    private String totalAmount;

    @JsonProperty("sBillDate")
    private String billDate;

    @JsonProperty("sRequestId")
    private String request_id;

    @JsonProperty("sStatusCode")
    private String status_code;
}
