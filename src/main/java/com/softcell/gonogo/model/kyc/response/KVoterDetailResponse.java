package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Softcell on 17/07/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KVoterDetailResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("sOrgRes")
    private String orgRes;

    //Response details
    @JsonProperty("sRlnNameV1")
    private String rlnNameV1;

    @JsonProperty("sPartNo")
    private String partNo;

    @JsonProperty("sRlnType")
    private String rlnType;

    @JsonProperty("sId")
    private String id;

    @JsonProperty("sNameV1")
    private String nameV1;

    @JsonProperty("sRlnName")
    private String rlnName;

    @JsonProperty("sDistrict")
    private String district;


    @JsonProperty("sState")
    private String state;

    @JsonProperty("sPsName")
    private String psName;

    @JsonProperty("sPcName")
    private String pcName;

    @JsonProperty("sSlNoInpart")
    private String slNoInpart;

    @JsonProperty("sName")
    private String name;

    @JsonProperty("sPartName")
    private String partName;

    @JsonProperty("sGender")
    private String gender;

    @JsonProperty("sAge")
    private String age;

    @JsonProperty("sAcName")
    private String acName;

    @JsonProperty("sEpicNo")
    private String epicNo;
}
