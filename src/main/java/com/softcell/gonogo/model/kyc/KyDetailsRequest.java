package com.softcell.gonogo.model.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.ApplicationRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 20/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KyDetailsRequest {

    @JsonProperty("oAppRequest")
    public ApplicationRequest applicationRequest;

    @JsonProperty("sKycType")
    public String kycType;
}
