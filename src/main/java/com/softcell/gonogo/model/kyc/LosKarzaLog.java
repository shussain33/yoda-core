package com.softcell.gonogo.model.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.logger.KarzaLog;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "losKarzaLog")
public class LosKarzaLog {

    @JsonProperty("sRefId")
    private String refId;

    private KarzaLog karzaLog;
}
