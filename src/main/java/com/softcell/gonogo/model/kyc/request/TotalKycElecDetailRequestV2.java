package com.softcell.gonogo.model.kyc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by abhishek on 7/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TotalKycElecDetailRequestV2 {
    @JsonProperty("oHeader")
    @NotNull( groups = {Header.InstWithProductGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sConsumerId")
    @NotNull( groups = {TotalKycElecDetailRequestV2.FetchGrp.class})
    @Valid
    private String consumerId;

    @JsonProperty("sServiceProvider")
    @NotNull( groups = {TotalKycElecDetailRequestV2.FetchGrp.class})
    @Valid
    private String serviceProvider;

    @JsonProperty("sConsent")
    @NotNull( groups = {TotalKycElecDetailRequestV2.FetchGrp.class})
    @Valid
    private KConsentEnum consent;

    public interface FetchGrp
    {
    }
}
