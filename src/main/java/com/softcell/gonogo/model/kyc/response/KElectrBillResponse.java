package com.softcell.gonogo.model.kyc.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by anupamad on 12/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)

public class KElectrBillResponse {
    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("sOrgRes")
    private String orgRes;

    @JsonProperty("sMobileNumber")
    private String mobileNumber;

    @JsonProperty("sAddress")
    private String address;

    @JsonProperty("sConsumerName")
    private String consumerName;

    @JsonProperty("sConsumerNumber")
    private String consumerNumber;

    @JsonProperty("sEmailAddress")
    private String emailAddress;
}
