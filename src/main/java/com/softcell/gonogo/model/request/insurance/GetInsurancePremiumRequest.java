package com.softcell.gonogo.model.request.insurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.InsuranceType;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 29/5/17.
 */
public class GetInsurancePremiumRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {GetInsurancePremiumRequest.FetchGrp.class, GetInsurancePremiumRequest.GetInsurancePremium.class, GetInsurancePremiumRequest.FetchInsurancePremium.class} )
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {GetInsurancePremiumRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("sGngAssetCat")
    @NotEmpty(groups = {GetInsurancePremiumRequest.FetchGrp.class})
    private String gngAssetCategory;

    @JsonProperty("dAssetPrice")
    private double assetPrice;

    @JsonProperty("eInsuranceType")
    @NotNull(groups = {GetInsurancePremiumRequest.FetchInsurancePremium.class})
    private InsuranceType insuranceType;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getGngAssetCategory() {
        return gngAssetCategory;
    }

    public void setGngAssetCategory(String gngAssetCategory) {
        this.gngAssetCategory = gngAssetCategory;
    }

    public double getAssetPrice() {
        return assetPrice;
    }

    public void setAssetPrice(double assetPrice) {
        this.assetPrice = assetPrice;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(InsuranceType insuranceType) {
        this.insuranceType = insuranceType;
    }

    public interface FetchGrp{

    }

    public interface  GetInsurancePremium{

    }

    public interface  FetchInsurancePremium{

    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GetInsurancePremiumRequest{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", gngAssetCategory='").append(gngAssetCategory).append('\'');
        sb.append(", assetPrice=").append(assetPrice);
        sb.append('}');
        return sb.toString();
    }

}

