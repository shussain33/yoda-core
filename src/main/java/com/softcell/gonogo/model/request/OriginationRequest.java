package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.CoOrigination;
import com.softcell.gonogo.model.core.CoOriginationDetails;
import com.softcell.gonogo.model.request.core.Header;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg222 on 27/12/18.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
public class OriginationRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @Id
    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("oCoOrigination")
    private CoOrigination coOrigination;

}
