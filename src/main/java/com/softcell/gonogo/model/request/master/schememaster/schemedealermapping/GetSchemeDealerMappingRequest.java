package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class GetSchemeDealerMappingRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sCid")
    private String ccid;

    @JsonProperty("sSchID")
    @NotNull(groups = {GetSchemeDealerMappingRequest.FetchGrp.class})
    private String schemeID;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getCcid() {
        return ccid;
    }

    public void setCcid(String ccid) {
        this.ccid = ccid;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public interface FetchGrp{

    }

}
