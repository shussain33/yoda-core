package com.softcell.gonogo.model.request.dmz;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.HeaderKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0268 on 9/11/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DMZRequest {

    @JsonProperty("sRequestType")
    private String requestType;

    @JsonProperty("sRequestUrl")
    private String requestURL;

    @JsonProperty("sRequestString")
    private String requestString;


    @JsonProperty("headerKeys")
    private List<HeaderKey> headerKeyList;
}
