package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by amit on 23/11/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeleteCoApplicantRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("aCoApplIds")
    private List<String> coApplicantIds;
}
