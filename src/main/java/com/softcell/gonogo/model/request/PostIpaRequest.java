/**
 * yogeshb2:54:50 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.core.FileHeader;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author yogeshb
 *
 */
@Document(collection = "postIpaDoc")
public class PostIpaRequest extends AuditEntity {

    @JsonProperty("dtDateTime")
    Date date = new Date();
    @Id
    @JsonProperty("sRefID")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class, PostIpaRequest.FetchGrp.class})
    private String RefID;
    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {PostIpaRequest.InsertGrp.class, FileHeader.FetchGrp.class}, message = "oHeader Object must not be null")
    private FileHeader header;

    @JsonProperty("opostIPA")
    @Valid
    @NotNull(groups = {PostIpaRequest.InsertGrp.class}, message = "postIPA Object must not be null")
    private PostIPA postIPA;

    public String getRefID() {
        return RefID;
    }

    public void setRefID(String refID) {
        RefID = refID;
    }

    public FileHeader getHeader() {
        return header;
    }

    public void setHeader(FileHeader header) {
        this.header = header;
    }

    public PostIPA getPostIPA() {
        return postIPA;
    }

    public void setPostIPA(PostIPA postIPA) {
        this.postIPA = postIPA;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PostIpaRequest{");
        sb.append("RefID='").append(RefID).append('\'');
        sb.append(", date=").append(date);
        sb.append(", header=").append(header);
        sb.append(", postIPA=").append(postIPA);
        sb.append('}');
        return sb.toString();
    }


    public interface InsertGrp {
    }

    public interface FetchGrp {
    }

}
