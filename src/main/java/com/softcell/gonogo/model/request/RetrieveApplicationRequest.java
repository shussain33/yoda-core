/**
 * vinodk5:47:45 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.request.core.Header;

/**
 * @author vinodk
 *
 */
public class RetrieveApplicationRequest {

    @JsonProperty("oPhone")
    private Phone phone;

    @JsonProperty("sDob")
    private String dateOfBirth;
    @JsonProperty("oHeader")
    private Header header;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the phone
     */
    public Phone getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth
     *            the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RetrieveApplicationRequest [phone=" + phone + ", dateOfBirth="
                + dateOfBirth + "]";
    }
}
