package com.softcell.gonogo.model.request.imps;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by sampat on 3/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IMPSRequest {

    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class, Header.InstWithProductGrp.class})
    @Valid
    private Header header;

    /**
     * Mandatory field
     */
    @JsonProperty("sReferenceID")
    @NotEmpty(groups = {IMPSRequest.FetchGrp.class})
    private String referenceID;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sBankBranch")
    private String bankBranch;

    @JsonProperty("sAccountNumber")
    private String accountNumber;

    @JsonProperty("sAccountType")
    private String accountType;

    @JsonProperty("sAccountHolderFName")
    private String accountHolderFName;

    @JsonProperty("sAccountHolderMName")
    private String accountHolderMName;

    @JsonProperty("sAccountHolderLName")
    private String accountHolderLName;

    @JsonProperty("sIFSCCode")
    private String IFSCCode;

    @JsonProperty("sYearsHeld")
    private int yearsHeld;

    @JsonProperty("sBankingType")
    private String bankingType;

    public interface FetchGrp {
    }


}
