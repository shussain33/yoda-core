package com.softcell.gonogo.model.request.digitalcore;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 2/7/19.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationData {

    @JsonProperty("sFirstName")
    private String firstName;

    @JsonProperty("sMiddleName")
    private String middleName;

    @JsonProperty("sLastName")
    private String lastName;

    @JsonProperty("sPhoneNumber")
    private String phoneNumber;

    @JsonProperty("bPhoneVerified")
    private boolean phoneVerified; /// To check mobile number is phoneVerified or not

    @JsonProperty("sEmailAddr")
    private String emailAddress;

    @JsonProperty("sCity")
    private String city;

    @JsonProperty("bTermsAndConditions")
    private boolean termsAndConditions;

    @JsonProperty("sLnPurp")
    private String loanPurpose;

    @JsonProperty("sLoanPurposeOther")
    private String loanPurposeOther;

    @JsonProperty("sChannel")
    private String channel;

}
