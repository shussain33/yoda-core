package com.softcell.gonogo.model.request.dooperation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 2/9/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeliveryOrderOperationRequest {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;


    public interface FetchGrp {
    }

}
