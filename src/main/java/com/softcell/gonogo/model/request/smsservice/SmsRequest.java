/**
 * yogeshb12:43:33 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request.smsservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 *
 * @Deprecated
 */
@Deprecated
public class SmsRequest {

    @JsonProperty("MOBILE-NUMBER")
    private String mobileNumber;

    @JsonProperty("MESSAGE")
    private String message;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SmsRequest{");
        sb.append("mobileNumber='").append(mobileNumber).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SmsRequest)) return false;
        SmsRequest that = (SmsRequest) o;
        return Objects.equal(getMobileNumber(), that.getMobileNumber()) &&
                Objects.equal(getMessage(), that.getMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getMobileNumber(), getMessage());
    }

    public static Builder builder(){
        return new Builder();
    }
    public static class Builder{
        private SmsRequest smsRequest = new SmsRequest();

        public SmsRequest build(){
            return smsRequest;
        }

        public Builder mobileNumber(String mobileNumber){
            this.smsRequest.mobileNumber=mobileNumber;
            return this;
        }

        public Builder message(String message){
            this.smsRequest.message= message;
            return this;
        }
    }
}
