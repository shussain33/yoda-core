/**
 * bhuvneshk12:05:17 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author bhuvneshk
 *
 */

@Document(collection = "DealerSequence")
public class DealerSequenceRequest extends AuditEntity {

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("sProductID")
    private String productID;

    @JsonProperty("sBranchCode")
    private String branchCode;

    @JsonProperty("sSeq")
    private String sequence;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    public String getSequenceType() {
        return sequenceType;
    }

    public void setSequenceType(String sequenceType) {
        this.sequenceType = sequenceType;
    }

    @JsonProperty("sSequenceType")
    private String sequenceType;


    /**
     * @return the productID
     */
    public String getProductID() {
        return productID;
    }

    /**
     * @param productID the productID to set
     */
    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the dealerId
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * @param dealerId the dealerId to set
     */
    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    /**
     * @return the sequence
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DealerSequenceRequest{");
        sb.append("dealerId='").append(dealerId).append('\'');
        sb.append(", productID='").append(productID).append('\'');
        sb.append(", branchCode='").append(branchCode).append('\'');
        sb.append(", sequence='").append(sequence).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", sequenceType='").append(sequenceType).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DealerSequenceRequest that = (DealerSequenceRequest) o;

        if (dealerId != null ? !dealerId.equals(that.dealerId) : that.dealerId != null) return false;
        if (productID != null ? !productID.equals(that.productID) : that.productID != null) return false;
        if (branchCode != null ? !branchCode.equals(that.branchCode) : that.branchCode != null) return false;
        if (sequence != null ? !sequence.equals(that.sequence) : that.sequence != null) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        return sequenceType != null ? sequenceType.equals(that.sequenceType) : that.sequenceType == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (dealerId != null ? dealerId.hashCode() : 0);
        result = 31 * result + (productID != null ? productID.hashCode() : 0);
        result = 31 * result + (branchCode != null ? branchCode.hashCode() : 0);
        result = 31 * result + (sequence != null ? sequence.hashCode() : 0);
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (sequenceType != null ? sequenceType.hashCode() : 0);
        return result;
    }
}
