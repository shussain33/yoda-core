package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantAddress;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssguser on 27/11/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveApplicantAddressRequest {

    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authentication;

    @JsonProperty("BASICINFO")
    private SaveAddressBasicInfo saveAddressBasicInfo;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;

    @JsonProperty("P_FORM60")
    private String p_form;
}
