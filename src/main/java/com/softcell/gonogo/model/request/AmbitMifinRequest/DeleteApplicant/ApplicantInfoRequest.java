package com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApplicantInfoRequest {

    @JsonProperty("APPLICANT_CODE")
    private String applicantCode;

    @JsonProperty("APPLICANT_TYPE")
    private String applicantType;

    @JsonProperty("FLAG")
    private String flag;
}
