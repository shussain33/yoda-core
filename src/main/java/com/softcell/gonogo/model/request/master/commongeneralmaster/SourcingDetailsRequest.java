package com.softcell.gonogo.model.request.master.commongeneralmaster;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 17/5/17.
 */
public class SourcingDetailsRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {SourcingDetailsRequest.FetchGrp.class})
    @Valid
    private Header header;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SourcingDetailsRequest{");
        sb.append("header=").append(header);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp{

    }
}
