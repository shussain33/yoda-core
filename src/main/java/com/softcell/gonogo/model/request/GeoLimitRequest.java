package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.ApplicantType;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 9/12/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeoLimitRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sAddressType")
    @NotEmpty(groups = {GeoLimitRequest.FetchGrp.class})
    private String addressType;

    @JsonProperty("sApplicantType")
    private ApplicantType applicantType;

    @JsonProperty("sPinCode")
    @NotEmpty(groups = {GeoLimitRequest.FetchGrp.class})
    private String pinCode;

   public interface FetchGrp{

    }

}
