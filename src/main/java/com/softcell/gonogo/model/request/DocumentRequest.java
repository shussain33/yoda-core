package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 25/4/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocumentRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sFileName")
    @NotEmpty(groups = {DocumentRequest.InsertGrp.class,DocumentRequest.FetchGrp.class})
    public String fileName;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {DocumentRequest.InsertGrp.class,DocumentRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }

}
