package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * This class contains the Dedupe Application details which will send to the Scoring for rule creation
 * and will display on cro screen as well.
 * Created by mahesh on 18/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DedupeApplicationDetails implements Comparable<DedupeApplicationDetails>{

    @JsonProperty("sRefID")
    private String refID;

    /**
     * ddMMyyyy
     */
    @JsonProperty("sLoginDate")
    private String loginDate;

    @JsonProperty("sCurrentStage")
    private String currentStage;

    @JsonProperty("sApplicationStatus")
    private String applicationStatus;

    @JsonProperty("sCibilScore")
    private String cibilScore;

    @JsonProperty("sDealerName")
    private String dealerName;

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("sDORaisedStatus")
    private String doRaisedStatus;

    @JsonProperty("sDOMailStatus")
    private String doMailStatus;

    @JsonIgnore
    private Date submitDate;

    @Override
    public int compareTo(DedupeApplicationDetails o) {
        if (null != this.submitDate && null != o && null != o.submitDate)
            return o.submitDate.compareTo(this.submitDate);
        else
            return 0;
    }
}
