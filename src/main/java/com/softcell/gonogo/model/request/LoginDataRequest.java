package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.topup.MatchApplicantDetails;
import com.softcell.gonogo.model.request.core.AppMetaData;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.digitalcore.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg0302 on 2/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginDataRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("oAppMetaData")
    private AppMetaData appMetaData;

    @JsonProperty("sRefID")
    public String refId;

    @JsonProperty("sStepName")
    private String stepName;///These are the different step name while case login

    @JsonProperty("oRegistrationData")
    private RegistrationData registrationData;

    @JsonProperty("oEligibilityData")
    private EligibilityData eligibilityData;

    @JsonProperty("oPersonalDetails")
    private PersonalDetails personalDetails;

    @JsonProperty("oDemographicData")
    private DemographicData demographicData;

    @JsonProperty("oEmploymentData")
    private EmploymentData employmentData;

    @JsonProperty("bWithDedupe")
    private boolean withDedupe;///if dedupe response is there and we proceed with one of dedupe refId

    @JsonProperty("bSkipDedupe")
    private boolean skipDedupe;///if dedupe response is there and we skip all the dedupe refId

    @JsonProperty("sDedupeRefID")
    public String dedupeRefId;///when we proceed with one of dedupe application set this refId.

    @JsonProperty("sPospectCode")
    public String prospectCode;

    @JsonProperty("oMatchApplicantDetails")
    MatchApplicantDetails matchApplicantDetails;

    @JsonProperty("bExistingAppln")
    private boolean existingAppln; // when editing existing application.

    @JsonProperty("sApplnStatus")
    private String applnStatus; // for setting application status explicitly

    @JsonProperty("sApplnBucket")
    private String applnBucket; // for setting application Bucket explicitly

    @JsonProperty("OAggregatorData")
    private AggregatorData aggregatorData;

}
