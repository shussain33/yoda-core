package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by mahesh on 10/10/17.
 */
public class GngMasterFields {

    @JsonProperty("ASST_CAT_MAST_ASST_CAT_ID")
    private String assetCatId;

    @JsonProperty("SUPP_MAST_DEALER_STATE_ID")
    private String dealerStateId;

    @JsonProperty("SUPP_MAST_DEALER_CITY_ID")
    private String dealerCityId;

    @JsonProperty("ASST_MODEL_MASTER_MANUFACTURER_ID")
    private String assetManctrId;

    public String getAssetManctrId() {
        return assetManctrId;
    }

    public void setAssetManctrId(String assetManctrId) {
        this.assetManctrId = assetManctrId;
    }

    public String getDealerCityId() {
        return dealerCityId;
    }

    public void setDealerCityId(String dealerCityId) {
        this.dealerCityId = dealerCityId;
    }

    public String getAssetCatId() {
        return assetCatId;
    }

    public void setAssetCatId(String assetCatId) {
        this.assetCatId = assetCatId;
    }

    public String getDealerStateId() {
        return dealerStateId;
    }

    public void setDealerStateId(String dealerStateId) {
        this.dealerStateId = dealerStateId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GngMasterFields{");
        sb.append("assetCatId='").append(assetCatId).append('\'');
        sb.append(", dealerStateId='").append(dealerStateId).append('\'');
        sb.append(", dealerCityId='").append(dealerCityId).append('\'');
        sb.append(", assetManctrId='").append(assetManctrId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
