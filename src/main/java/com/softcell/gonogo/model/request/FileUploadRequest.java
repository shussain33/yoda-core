package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.core.UploadFileDetails;
import com.softcell.gonogo.model.request.core.FileHeader;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

public class FileUploadRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {FileHeader.InsertGrp.class, FileHeader.FetchGrp.class})
    @Valid
    private FileHeader fileHeader;

    @JsonProperty("sImageID")
    private String imageID;

    @JsonProperty("sRefID")
    @NotBlank(groups = {FileUploadRequest.InsertGrp.class, FileUploadRequest.FetchGrp.class, FileUploadRequest.UpdateOrDelete.class,
            FileUploadRequest.ApproveOrReject.class})
    private String gonogoReferanceId;

    @JsonProperty("sParentGNGRefId")
    private String parentGNGRefId;

    @JsonProperty("oUpldDtl")
    @NotNull(groups = {FileUploadRequest.InsertGrp.class, FileUploadRequest.FetchGrp.class, FileUploadRequest.UpdateOrDelete.class,
            FileUploadRequest.ApproveOrReject.class, FileUploadRequest.DocumentInsertGrp.class})
    @Valid
    private UploadFileDetails uploadFileDetails;

    @JsonProperty("aApplicationImgLinkage")
    private Set<String> appImageLinkage;

    public FileHeader getFileHeader() {
        return fileHeader;
    }

    public void setFileHeader(FileHeader fileHeader) {
        this.fileHeader = fileHeader;
    }

    public String getGonogoReferanceId() {
        return gonogoReferanceId;
    }

    public void setGonogoReferanceId(String gonogoReferanceId) {
        this.gonogoReferanceId = gonogoReferanceId;
    }

    public UploadFileDetails getUploadFileDetails() {
        return uploadFileDetails;
    }

    public void setUploadFileDetails(UploadFileDetails uploadFileDetails) {
        this.uploadFileDetails = uploadFileDetails;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    /**
     * @return the appImageLinkage
     */
    public Set<String> getAppImageLinkage() {
        return appImageLinkage;
    }

    /**
     * @param appImageLinkage the appImageLinkage to set
     */
    public void setAppImageLinkage(Set<String> appImageLinkage) {
        this.appImageLinkage = appImageLinkage;
    }

    /**
     * @return the parentGNGRefId
     */
    public String getParentGNGRefId() {
        return parentGNGRefId;
    }

    /**
     * @param parentGNGRefId the parentGNGRefId to set
     */
    public void setParentGNGRefId(String parentGNGRefId) {
        this.parentGNGRefId = parentGNGRefId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FileUploadRequest{");
        sb.append("fileHeader=").append(fileHeader);
        sb.append(", imageID='").append(imageID).append('\'');
        sb.append(", gonogoReferanceId='").append(gonogoReferanceId).append('\'');
        sb.append(", parentGNGRefId='").append(parentGNGRefId).append('\'');
        sb.append(", uploadFileDetails=").append(uploadFileDetails);
        sb.append(", appImageLinkage=").append(appImageLinkage);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileUploadRequest)) return false;
        FileUploadRequest that = (FileUploadRequest) o;
        return Objects.equal(getFileHeader(), that.getFileHeader()) &&
                Objects.equal(getImageID(), that.getImageID()) &&
                Objects.equal(getGonogoReferanceId(), that.getGonogoReferanceId()) &&
                Objects.equal(getParentGNGRefId(), that.getParentGNGRefId()) &&
                Objects.equal(getUploadFileDetails(), that.getUploadFileDetails()) &&
                Objects.equal(getAppImageLinkage(), that.getAppImageLinkage());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getFileHeader(), getImageID(), getGonogoReferanceId(), getParentGNGRefId(), getUploadFileDetails(), getAppImageLinkage());
    }

    public interface InsertGrp {
    }

    public interface FetchGrp {
    }

    public interface UpdateOrDelete {
    }

    public interface ApproveOrReject {
    }

    public interface DocumentInsertGrp {
    }

    public interface DocumentFetchGrp {
    }
}
