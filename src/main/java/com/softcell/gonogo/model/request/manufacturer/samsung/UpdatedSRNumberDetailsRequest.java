package com.softcell.gonogo.model.request.manufacturer.samsung;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class UpdatedSRNumberDetailsRequest {


    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(
            groups = {
                    UpdatedSRNumberDetailsRequest.FetchGrp.class
            }
    )
    private String referenceID;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdatedSRNumberDetailsRequest{");
        sb.append("header=").append(header);
        sb.append(", referenceID='").append(referenceID).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdatedSRNumberDetailsRequest)) return false;
        UpdatedSRNumberDetailsRequest that = (UpdatedSRNumberDetailsRequest) o;
        return Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getReferenceID(), that.getReferenceID());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHeader(), getReferenceID());
    }

    public interface FetchGrp {
    }
}
