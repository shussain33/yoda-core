package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class InclDealerIdWithStateCity {

    @JsonProperty("sDealerID")
    private String dealerID;

    @JsonProperty("sSupplierDesc")
    private String supplierDesc;

    @JsonProperty("sCityName")
    private String cityName;

    @JsonProperty("sStateName")
    private String stateName;

    public String getDealerID() {
        return dealerID;
    }

    public void setDealerID(String dealerID) {
        this.dealerID = dealerID;
    }

    public String getSupplierDesc() {
        return supplierDesc;
    }

    public void setSupplierDesc(String supplierDesc) {
        this.supplierDesc = supplierDesc;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InclDealerIdWithStateCity [dealerID=");
        builder.append(dealerID);
        builder.append(", supplierDesc=");
        builder.append(supplierDesc);
        builder.append(", cityName=");
        builder.append(cityName);
        builder.append(", stateName=");
        builder.append(stateName);
        builder.append("]");
        return builder.toString();
    }

}
