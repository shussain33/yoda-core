package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg0268 on 8/11/19.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsuranceRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class, Header.InsertGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {ListOfDocsRequest.InsertGrp.class,ListOfDocsRequest.FetchGrp.class})
    public String refId;

    @JsonProperty("oInsuranceData")
    public  InsuranceData insuranceData;
}
