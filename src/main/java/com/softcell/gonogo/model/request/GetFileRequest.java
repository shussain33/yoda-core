/**
 * yogeshb10:37:47 am  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */

@Document(collection = "emailRequestLog")
public class GetFileRequest extends AuditEntity {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class, Header.InstWithProductGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sImgID")
    @NotBlank(groups = {GetFileRequest.ImgFetchGrp.class,GetFileRequest.FetchGrp.class})
    private String imageFileID;

    @JsonProperty("sRefID")
    @NotBlank(groups = {GetFileRequest.FetchGrp.class,GetFileRequest.FetchRefIdGrp.class})
    private String refID;

    @JsonProperty("bIsCoApp")
    private String isCoApplicant;

    @JsonIgnore
    private String emailProcessStatus;

    @JsonProperty("sDocumentType")
    @NotBlank(groups = {GetFileRequest.FetchDocumentGrp.class, GetFileRequest.UploadDocumentGrp.class})
    private String documentType;

    @JsonProperty("sDocumentSubType")
    @NotBlank(groups = {GetFileRequest.FetchDocumentGrp.class, GetFileRequest.UploadDocumentGrp.class})
    private String documentSubType;


    public String getEmailProcessStatus() {
        return emailProcessStatus;
    }

    public void setEmailProcessStatus(String emailProcessStatus) {
        this.emailProcessStatus = emailProcessStatus;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getImageFileID() {
        return imageFileID;
    }

    public void setImageFileID(String imageFileID) {
        this.imageFileID = imageFileID;
    }

    public String getIsCoApplicant() {
        return isCoApplicant;
    }

    public void setIsCoApplicant(String isCoApplicant) {
        this.isCoApplicant = isCoApplicant;
    }

    public String getDocumentType() { return documentType; }

    public void setDocumentType(String documentType) { this.documentType = documentType; }

    public String getDocumentSubType() { return documentSubType; }

    public void setDocumentSubType(String documentSubType) { this.documentSubType = documentSubType; }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GetFileRequest{");
        sb.append("header=").append(header);
        sb.append(", imageFileID='").append(imageFileID).append('\'');
        sb.append(", refID='").append(refID).append('\'');
        sb.append(", isCoApplicant='").append(isCoApplicant).append('\'');
        sb.append(", emailProcessStatus='").append(emailProcessStatus).append('\'');
        sb.append(", documentType='").append(documentType).append('\'');
        sb.append(", documentSubType='").append(documentSubType).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetFileRequest)) return false;
        GetFileRequest that = (GetFileRequest) o;
        return Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getImageFileID(), that.getImageFileID()) &&
                Objects.equal(getRefID(), that.getRefID()) &&
                Objects.equal(getIsCoApplicant(), that.getIsCoApplicant()) &&
                Objects.equal(getEmailProcessStatus(), that.getEmailProcessStatus()) &&
                Objects.equal(getDocumentType(), that.getDocumentType()) &&
                Objects.equal(getDocumentSubType(), that.getDocumentSubType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHeader(), getImageFileID(), getRefID(), getIsCoApplicant(), getEmailProcessStatus()
                , getDocumentType()
                , getDocumentSubType());
    }

    public interface FetchGrp {
    }

    public interface ImgFetchGrp {
    }

    public interface FetchRefIdGrp {
    }

    public interface FetchDocumentGrp {}

    public interface UploadDocumentGrp {}
}
