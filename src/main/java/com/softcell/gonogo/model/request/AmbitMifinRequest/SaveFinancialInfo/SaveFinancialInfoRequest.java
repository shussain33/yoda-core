package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SaveFinancialInfoRequest{

    @JsonProperty("AUTHENTICATION")
    AuthenticationDetails authenticationDetails;

    @JsonProperty("BANKING")
    List<SaveFinancialInfoBankingDetails> saveFinancialInfoBankingDetailsList;

    @JsonProperty("BASICINFO")
    SaveFinancialInfoBasicInfoDetails saveFinancialInfoBasicInfoDetails;

    @JsonProperty("FINANCIAL")
    List<Financial> financialList;

    @JsonProperty("ITR")
    List<Itr> itrList;

    @JsonProperty("OTHER_LOANS_DET")
    List<OtherLoansDet> otherLoansDetList;

    @JsonProperty("VIABILITY")
    Viability viability;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;
}
