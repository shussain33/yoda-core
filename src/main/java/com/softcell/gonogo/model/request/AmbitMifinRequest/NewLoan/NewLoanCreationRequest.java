package com.softcell.gonogo.model.request.AmbitMifinRequest.NewLoan;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NewLoanCreationRequest {

    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authentication;

    @JsonProperty("BASICINFO")
    private NewLoanBasicInfo newLoanBasicInfo;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;

    @JsonProperty("P_FORM60")
    private String p_form;
}