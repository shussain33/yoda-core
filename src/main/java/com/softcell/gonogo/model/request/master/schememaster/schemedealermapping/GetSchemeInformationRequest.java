package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class GetSchemeInformationRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sSchID")
    private String schemeID;

    @JsonProperty("sSchDes")
    private String schemeDesc;

    @JsonProperty("sCid")
    private String ccid;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public String getCcid() {
        return ccid;
    }

    public void setCcid(String ccid) {
        this.ccid = ccid;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AssetDetailsForDlrMappingRequest [header=");
        builder.append(header);
        builder.append(", schemeID=");
        builder.append(schemeID);
        builder.append(", schemeDesc=");
        builder.append(schemeDesc);
        builder.append(", ccid=");
        builder.append(ccid);
        builder.append("]");
        return builder.toString();
    }


}
