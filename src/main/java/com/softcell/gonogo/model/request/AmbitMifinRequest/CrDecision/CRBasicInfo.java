package com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CRBasicInfo {

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

}
