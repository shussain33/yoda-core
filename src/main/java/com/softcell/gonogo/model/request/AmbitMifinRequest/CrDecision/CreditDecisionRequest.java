package com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditDecisionRequest {

    @JsonProperty("ACTION")
    private String action;

    @JsonProperty("REJECT_TYPE")
    private String rejectType;

    @JsonProperty("REJECT_CODE")
    private String rejectCode;

    @JsonProperty("DECISION_DATE")
    private String decisionDate;

    @JsonProperty("RECOMMENDED_AMOUNT")
    private String recommadedAmt;

    @JsonProperty("RECOMMENDED_TENOR")
    private String recommendedTenor;

    @JsonProperty("RECOMMENDED_ROI")
    private String recommecdedRoi;

    @JsonProperty("REMARKS")
    private String remarks;

    @JsonProperty("RISK_CATEGORY")
    private String riskCategory;

    @JsonProperty("CR_LEVEL")
    private String crLevel;

    @JsonProperty("CREDIT_OFFICER")
    private String creditOfficer;

}
