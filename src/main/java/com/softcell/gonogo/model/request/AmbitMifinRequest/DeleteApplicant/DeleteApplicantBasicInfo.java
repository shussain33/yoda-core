package com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeleteApplicantBasicInfo {

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

}
