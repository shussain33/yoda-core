package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 7/5/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateGNGRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {UpdateGNGRequest.InsertGrp.class,UpdateGNGRequest.FetchGrp.class})
    private String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
