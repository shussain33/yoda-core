package com.softcell.gonogo.model.request.AmbitMifinRequest.PanSearch;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MifinPanSearchRequest {

    @NotEmpty
    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sInstID")
    @NotEmpty
    private String institutionId;

    @JsonProperty("sProduct")
    @NotEmpty
    private String product;

    @JsonProperty("sPanNo")
    @NotEmpty
    private String panNo;

    @JsonProperty("sCustEntityType")
    @NotEmpty
    private String custEntityType;
}
