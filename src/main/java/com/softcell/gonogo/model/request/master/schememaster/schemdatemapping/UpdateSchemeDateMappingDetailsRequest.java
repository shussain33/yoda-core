package com.softcell.gonogo.model.request.master.schememaster.schemdatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.SchemeDetailsDateMapping;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class UpdateSchemeDateMappingDetailsRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oSchemeDetailsDateMapping")
    @NotNull(groups = {SchemeDetailsDateMapping.FetchGrp.class})
    @Valid
    private SchemeDetailsDateMapping schemeDetailsDateMapping;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public SchemeDetailsDateMapping getSchemeDetailsDateMapping() {
        return schemeDetailsDateMapping;
    }

    public void setSchemeDetailsDateMapping(
            SchemeDetailsDateMapping schemeDetailsDateMapping) {
        this.schemeDetailsDateMapping = schemeDetailsDateMapping;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UpdateSchemeDateMappingDetailsRequest [header=");
        builder.append(header);
        builder.append(", schemeDetailsDateMapping=");
        builder.append(schemeDetailsDateMapping);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp{

    }


}
