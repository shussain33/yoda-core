package com.softcell.gonogo.model.request.smsservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.SmsRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;

/**
 * @author vinodk
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsServiceBaseRequest {

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("aMobNo")
    private String[] mobileNumber;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("bOtp")
    private Boolean otpMsg;

    @JsonProperty("aByteStringMsg")
    private byte[] byteStringMsg;

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the mobileNumber
     */
    public String[] getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String[] mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Boolean getOtpMsg() {
        return otpMsg;
    }

    public void setOtpMsg(Boolean otpMsg) {
        this.otpMsg = otpMsg;
    }

    public byte[] getByteStringMsg() {
        return byteStringMsg;
    }

    public void setByteStringMsg(byte[] byteStringMsg) {
        this.byteStringMsg = byteStringMsg;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsServiceBaseRequest [message=");
        builder.append(message);
        builder.append(", mobileNumber=");
        builder.append(Arrays.toString(mobileNumber));
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", otpMsg=");
        builder.append(otpMsg);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + Arrays.hashCode(mobileNumber);
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SmsServiceBaseRequest other = (SmsServiceBaseRequest) obj;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (message == null) {
            if (other.message != null)
                return false;
        } else if (!message.equals(other.message))
            return false;
        if (!Arrays.equals(mobileNumber, other.mobileNumber))
            return false;
        return true;
    }

    public void map(SmsRequest smsRequest) {
        this.setInstitutionId(smsRequest.getHeader().getInstitutionId());
        this.setMessage(smsRequest.getMessageContent());
        this.setMobileNumber(smsRequest.getMobileNumber());
    }
}
