package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.response.master.commongeneralmaster.SourcingDetails;
import com.softcell.gonogo.model.security.v2.Branch;

import java.io.Serializable;

/**
 * Created by vinod on 3/5/17.
 */
public class AppMetaData implements Serializable{

    @JsonProperty("sEmpId")
    private String empId;

    /**
     * Deprecated as new domain for branch domain added due to Oracle to Mongo
     * migration of uam
     */
    @Deprecated
    @JsonProperty("oBranch")
    private com.softcell.gonogo.model.security.Branch branch;

    @JsonProperty("oNBranch")
    private Branch branchV2;

    @JsonProperty("oDsaName")
    private Name dsaName;

    @JsonProperty("oDsaEmailId")
    private Email dsaEmailId;

    @JsonProperty("oPhone")
    private Phone phone;

    @JsonProperty("oSourcingDetails")
    private SourcingDetails sourcingDetails;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    @Deprecated
    public com.softcell.gonogo.model.security.Branch getBranch() {
        return branch;
    }

    @Deprecated
    public void setBranch(com.softcell.gonogo.model.security.Branch branch) {
        this.branch = branch;
    }

    public Name getDsaName() {
        return dsaName;
    }

    public void setDsaName(Name dsaName) {
        this.dsaName = dsaName;
    }

    public Email getDsaEmailId() {
        return dsaEmailId;
    }

    public void setDsaEmailId(Email dsaEmailId) {
        this.dsaEmailId = dsaEmailId;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public SourcingDetails getSourcingDetails() {
        return sourcingDetails;
    }

    public void setSourcingDetails(SourcingDetails sourcingDetails) {
        this.sourcingDetails = sourcingDetails;
    }

    public Branch getBranchV2() {
        return branchV2;
    }

    public void setBranchV2(Branch branchV2) {
        this.branchV2 = branchV2;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AppMetaData{");
        sb.append("empId=").append(empId);
        sb.append("branch=").append(branchV2);
        sb.append(", dsaName=").append(dsaName);
        sb.append(", dsaEmailId=").append(dsaEmailId);
        sb.append(", phone=").append(phone);
        sb.append(", sourcingDetails=").append(sourcingDetails);
        sb.append('}');
        return sb.toString();
    }
}
