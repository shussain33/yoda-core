package com.softcell.gonogo.model.request.digitalcore;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.eligibility.EligibleOffers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssg0302 on 2/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityData {

    @JsonProperty("dLoanAmt")
    private Double appliedLoan;

    @JsonProperty("dIncome")
    private Double income;

    @JsonProperty("dFixedOblgation")
    private Double fixedOblgation;

    @JsonProperty("dCreditCardOutStanding")
    private Double creditCardOutStanding;

    @JsonProperty("sTenorRequested")
    private String appliedTenor;

    @JsonProperty("bIsExistingLoan")
    private boolean isExistingLoan;

    @JsonProperty("aEligibleOffers")
    private List<EligibleOffers> eligibleOffersList = new ArrayList<>();

    @JsonProperty("dRoi")
    private Double roi;
}
