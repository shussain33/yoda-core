package com.softcell.gonogo.model.request.manufacturer.samsung;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */

@Document(collection = "serialNumberRequest")
public class SerialSaleConfirmationRequest extends AuditEntity {
    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.InstWithProductGrp.class
            }
    )
    @Valid
    private Header header;

    /**
     * Mandatory field
     */
    @JsonProperty("sSerialNumber")
    private String serialNumber;

    /**
     * Optional field
     */
    @JsonProperty("sSaleDate")
    private String saleDate;

    /**
     * Mandatory field
     */
    @JsonProperty("sSkuCode")
    private String skuCode;

    /**
     * Following are the possible values for vendor
     */
    @JsonProperty("sVendor")
    @NotEmpty(
            groups = {
                    SerialSaleConfirmationRequest.FetchGrp.class
            }
    )
    private String vendor;
    /**
     * Optional field
     * Following are the possible values for saleType
     */
    @JsonProperty("sSaleType")
    private String saleType;

    /**
     * Optional field
     */
    @JsonProperty("sPartnerCode")
    private String partnerCode;

    @JsonProperty("sChannelCode")
    private String channelCode;

    /**
     * Mandatory field
     */
    @JsonProperty("sReferenceID")
    @NotEmpty(
            groups = {
                    SerialSaleConfirmationRequest.FetchGrp.class
            }
    )
    private String referenceID;


    /**
     * Optional field
     */
    @JsonProperty("sProductType")
    private String productType;

    /**
     * Optional field for a while
     */
    @JsonProperty("iFlag")
    private int flag;

    /**
     * Sony request parameter
     */


    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sMaterialName")
    private String materialName;

    @JsonProperty("sModel")
    private String model;

    @JsonProperty("sProductCode")
    private String productCode;

    @JsonProperty("iRequestID")
    private int requestID;//Optional

    @JsonProperty("iCategoryID")
    private int categoryID;//Optional

    @JsonProperty("sStoreCode")
    private String storeCode;//Optional

    @JsonProperty("bConfirmationStatus")
    private boolean confirmationStatus;//Optional

    /**
     * Apple fields
     */

    @JsonProperty("sMpn")
    private String mpn;

    @JsonProperty("lStoreAppleId")
    private long storeAppleId;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("iTenure")
    private int tenure;

    @JsonProperty("sScheme")
    private String scheme;

    /**
     * dealerId is used in Oppo
     */
    @JsonProperty("sDealerId")
    private String dealerId;

    /**
     * Mandatory field is only used for Google
     */
    @JsonProperty("sRDSCode")
    private String rdsCode;

    @JsonProperty(value = "sSRNumber")
    private String serviceRequestNumber;

    @JsonProperty("sMaterialCode")
    private String materialCode;

    @JsonProperty("sMake")
    private String make;

    public static Builder builder() {
        return new Builder();
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }


    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public boolean isConfirmationStatus() {
        return confirmationStatus;
    }

    public void setConfirmationStatus(boolean confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }


    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public long getStoreAppleId() {
        return storeAppleId;
    }

    public void setStoreAppleId(long storeAppleId) {
        this.storeAppleId = storeAppleId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getTenure() {
        return tenure;
    }

    public void setTenure(int tenure) {
        this.tenure = tenure;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getRdsCode() {
        return rdsCode;
    }

    public void setRdsCode(String rdsCode) {
        this.rdsCode = rdsCode;
    }

    public String getServiceRequestNumber() {
        return serviceRequestNumber;
    }

    public void setServiceRequestNumber(String serviceRequestNumber) { this.serviceRequestNumber = serviceRequestNumber; }

    public String getMaterialCode() { return materialCode; }

    public void setMaterialCode(String materialCode) { this.materialCode = materialCode; }

    public String getMake() { return make; }

    public void setMake(String make) { this.make = make; }

    public interface FetchGrp {
    }


    public static final class Builder {

        private SerialSaleConfirmationRequest serialSaleConfirmationRequest = new SerialSaleConfirmationRequest();

        private Builder() {
        }

        public SerialSaleConfirmationRequest build() {
            return this.serialSaleConfirmationRequest;
        }

        public Builder header(Header header) {
            this.serialSaleConfirmationRequest.header = header;
            return this;
        }

        public Builder serialNumber(String serialNumber) {
            this.serialSaleConfirmationRequest.serialNumber = serialNumber;
            return this;
        }

        public Builder saleDate(String saleDate) {
            this.serialSaleConfirmationRequest.saleDate = saleDate;
            return this;
        }

        public Builder skuCode(String skuCode) {
            this.serialSaleConfirmationRequest.skuCode = skuCode;
            return this;
        }

        public Builder vendor(String vendor) {
            this.serialSaleConfirmationRequest.vendor = vendor;
            return this;
        }

        public Builder saleType(String saleType) {
            this.serialSaleConfirmationRequest.saleType = saleType;
            return this;
        }

        public Builder partnerCode(String partnerCode) {
            this.serialSaleConfirmationRequest.partnerCode = partnerCode;
            return this;
        }

        public Builder referenceID(String referenceID) {
            this.serialSaleConfirmationRequest.referenceID = referenceID;
            return this;
        }

        public Builder productType(String productType) {
            this.serialSaleConfirmationRequest.productType = productType;
            return this;
        }

        public Builder flag(int flag) {
            this.serialSaleConfirmationRequest.flag = flag;
            return this;
        }

        public Builder imeiNumber(String imeiNumber) {
            this.serialSaleConfirmationRequest.imeiNumber = imeiNumber;
            return this;
        }

        public Builder materialName(String materialName) {
            this.serialSaleConfirmationRequest.materialName = materialName;
            return this;
        }

        public Builder model(String model) {
            this.serialSaleConfirmationRequest.model = model;
            return this;
        }

        public Builder productCode(String productCode) {
            this.serialSaleConfirmationRequest.productCode = productCode;
            return this;
        }

        public Builder requestID(int requestID) {
            this.serialSaleConfirmationRequest.requestID = requestID;
            return this;
        }

        public Builder categoryID(int categoryID) {
            this.serialSaleConfirmationRequest.categoryID = categoryID;
            return this;
        }

        public Builder storeCode(String storeCode) {
            this.serialSaleConfirmationRequest.storeCode = storeCode;
            return this;
        }

        public Builder confirmationStatus(boolean confirmationStatus) {
            this.serialSaleConfirmationRequest.confirmationStatus = confirmationStatus;
            return this;
        }

        public Builder scheme(String scheme) {
            this.serialSaleConfirmationRequest.scheme = scheme;
            return this;
        }

        public Builder channelCode(String channelCode){
            this.serialSaleConfirmationRequest.channelCode = channelCode;
            return this;
        }

        public Builder dealerId(String dealerId) {
            this.serialSaleConfirmationRequest.dealerId = dealerId;
            return this;
        }

        public Builder rdsCode(String rdsCode) {
            this.serialSaleConfirmationRequest.rdsCode = rdsCode;
            return this;
        }

        public Builder serviceRequestNumber(String serviceRequestNumber) {
            this.serialSaleConfirmationRequest.serviceRequestNumber = serviceRequestNumber;
            return this;
        }

        public Builder materialCode(String materialCode) {
            this.serialSaleConfirmationRequest.materialCode = materialCode;
            return this;
        }

        public Builder make(String make) {
            this.serialSaleConfirmationRequest.make = make;
            return this;
        }
    }
}
