package com.softcell.gonogo.model.request.master.assetvalidate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class AssetCostValidationRequest {

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oAssetCostDetails")
    @NotNull(groups = {AssetCostDetails.FetchGrp.class})
    @Valid
    private AssetCostDetails assetCostDetails;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public AssetCostDetails getAssetCostDetails() {
        return assetCostDetails;
    }

    public void setAssetCostDetails(AssetCostDetails assetCostDetails) {
        this.assetCostDetails = assetCostDetails;
    }

    public interface FetchGrp{

    }


}
