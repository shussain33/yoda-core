package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Financial  {

    @JsonProperty("FINANCIAL_ID")
    String financialId;
    @JsonProperty("FINANCIAL_YEAR")
    String financialYear;

    @JsonProperty("AUDITED_FLAG")
    String auditedFlag;

    @JsonProperty("CONSIDER_IN_FIN_ASMNT_FLG")
    String considerInFinAsmntFlg;

    @JsonProperty("SHARE_CAPITAL")
    String shareCapital;

    @JsonProperty("RESERVES_AND_SURPLUS")
    String reservesAndSurplus;

    @JsonProperty("SHORT_TERM_BANK_BORROWING")
    String shortTermBankBorrowing;

    @JsonProperty("LONG_TERM_DEBT")
    String longTermDebt;

    @JsonProperty("UNSECURED_LOANS")
    String unsecuredLoans;

    @JsonProperty("CURRENT_LIABILITIES")
    String currentLiabilities;

    @JsonProperty("PROVISION")
    String provision;

    @JsonProperty("DEFFERED_TAX_LIABILITY")
    String defferedTaxLiability;

    @JsonProperty("CONTINGENT_LIABILITIES")
    String contingentLiabilities;

    @JsonProperty("NET_FIXED_ASSETS")
    String netFixedAssets;

    @JsonProperty("INVESTMENTS")
    String investments;

    @JsonProperty("LOANS_AND_ADVANCES")
    String loansAndAdvances;

    @JsonProperty("BOOK_DEBT_LESSTHAN_SIXMONTHS")
    String bookDebtLessthanSixmonths;

    @JsonProperty("BOOK_DEBT_GREATER_OR_EQUALTHAN_SIXMONTHS")
    String bookDebtGreaterOrEqualthanSixmonths;

    @JsonProperty("INVENTORY")
    String inventory;

    @JsonProperty("CASH_AND_BANKBALANCES")
    String cashAndBankBalances;

    @JsonProperty("OTHER_CURRENT_ASSETS")
    String otherCurrentAssets;

    @JsonProperty("DEFFERED_TAX_ASSETS")
    String defferedTaxAssets;

    @JsonProperty("NET_SALES")
    String netSales;

    @JsonProperty("OTHER_INCOME")
    String otherIncome;

    @JsonProperty("TOTAL_EXPENSE")
    String totalExpense;

    @JsonProperty("TOTAL_INTEREST")
    String totalInterest;

    @JsonProperty("DEPRICIATION")
    String depriciation;

    @JsonProperty("TAX_PAID")
    String taxPaid;

    @JsonProperty("PROFIT_AFTER_TAX_PAID")
    String profitAfterTaxPaid;

    @JsonProperty("INCOME_FROM_OTHER_SOURCES")
    String incomeFromOtherSources;

    @JsonProperty("INTEREST_PAID_TO_PARTNERS")
    String interestPaidToPartners;

    @JsonProperty("PARTNERS_SALARY")
    String partnersSalary;

    @JsonProperty("MISC_OUTFLOW_AMOUNT")
    String miscOutflowAmount;

}
