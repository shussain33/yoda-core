package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantAddress;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssguser on 27/11/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveAddressBasicInfo {

    @JsonProperty("PROSPECT_CODE")
    String prospectCode;

    @JsonProperty("APPLICANT_CODE")
    String applicantCode;

    @JsonProperty("APPLICANT_ADDRESS_ID")
    String applicantAddressId;

    @JsonProperty("ADDRESSTYPE")
    String addressType;

    @JsonProperty("ADDRESS1")
    String address1;

    @JsonProperty("ADDRESS2")
    String address2;

    @JsonProperty("ADDRESS3")
    String address3;

    @JsonProperty("LANDMARK")
    String landmark;

    @JsonProperty("DISTRICT")
    String district;

    @JsonProperty("STATE")
    String state;

    @JsonProperty("CITY")
    String city;

    @JsonProperty("MOBILE1")
    String mobile1;

    @JsonProperty("MOBILE2")
    String mobile2;

    @JsonProperty("PINCODE")
    String pincode;

    @JsonProperty("PHONE1")
    String phone1;

    @JsonProperty("PHONE2")
    String phone2;

    @JsonProperty("EMAIL")
    String email;

    @JsonProperty("FAX")
    String fax;

    @JsonProperty("MAILING_ADDRESS")
    String mailingAddress;

    @JsonProperty("DESTINATION_ADDRESS")
    String destinationAddress;

    @JsonProperty("OCCUPANCYSTATUS")
    String occupanyStatus;

    @JsonProperty("YEAR_OF_STAY_CURRENT_AREA")
    String yearOfStayCurrentArea;

    @JsonProperty("MONTHS_OF_STAY_CURRENT_AREA")
    String monthsOfStayArea;

    @JsonProperty("GST_APPLICABLE")
    String gstApplicable;

    @JsonProperty("GSTIN_NO")
    String gstNo;

    @JsonProperty("CREATED_BY")
    String createdBy;

    @JsonProperty("UPDATED_BY")
    String updatedBy;

    @JsonProperty("COMPANY_NAME")
    String companyName;
}
