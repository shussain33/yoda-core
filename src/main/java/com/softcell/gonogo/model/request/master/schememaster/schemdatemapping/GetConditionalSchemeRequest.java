package com.softcell.gonogo.model.request.master.schememaster.schemdatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class GetConditionalSchemeRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sTypeOfScheme")
    @NotEmpty(groups = {GetConditionalSchemeRequest.FetchGrp.class})
    private String typeOfScheme;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getTypeOfScheme() {
        return typeOfScheme;
    }

    public void setTypeOfScheme(String typeOfScheme) {
        this.typeOfScheme = typeOfScheme;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GetFilteredSchemeRequest [header=");
        builder.append(header);
        builder.append(", typeOfScheme=");
        builder.append(typeOfScheme);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp{

    }

}
