package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;


/**
 * Created by ssg408 on 13/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "templateDetails")
public class TemplateDetails {

    @Id
    @JsonProperty("sTempName")
    public String templateName;

    @JsonProperty("sInstitutionId")
    public String institutionId;

    @JsonProperty("sTempType")
    public String type;

    @JsonProperty("sSubject")
    public String mailSubject;

    @JsonProperty("sProName")
    public String productName;

    @JsonProperty("sTempContent")
    public String templateContent;

    @JsonProperty("sApiType")
    public String apiType = "Default";

    @JsonProperty("sTemplateDisplayName")
    private String templateDisplayName;

    @JsonProperty("sTemplateKey")
    private String templateKey;

    @JsonProperty("aConfigDetail")
    public Map<String,String> configDetailsList;

    @JsonProperty("bIsOtpTemplate")
    private boolean otpTemplate;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
