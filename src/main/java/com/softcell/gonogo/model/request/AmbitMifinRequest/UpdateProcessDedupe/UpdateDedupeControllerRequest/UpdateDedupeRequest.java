package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateDedupeRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("RefId")
    private String refId;

    @JsonProperty("InstitutionId")
    private String instId;

    @JsonProperty("Product")
    private Product product;

    @JsonProperty("Applicant")
    private ApplicantdedupeRequest applicantdedupeRequest;

    @JsonProperty("CoApplicant")
    private List<CoapplicantDedupeRequest> coapplicantDedupeRequest;

    @JsonProperty("sMifinDedupeDecision")
    private String mifinDedupeDecision;

    @JsonProperty("sMifinDedupeReason")
    private String mifinReason;
}
