package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.SchemeModelDealerCityMapping;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class SchemeModelDealerCityMappingRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oSchemeModelDealerCityMapping")
    private SchemeModelDealerCityMapping schemeModelDealerCityMapping;

    @JsonProperty("iLimit")
    private int limit;

    @JsonProperty("iSkip")
    private int skip;



    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public SchemeModelDealerCityMapping getSchemeModelDealerCityMapping() {
        return schemeModelDealerCityMapping;
    }

    public void setSchemeModelDealerCityMapping(
            SchemeModelDealerCityMapping schemeModelDealerCityMapping) {
        this.schemeModelDealerCityMapping = schemeModelDealerCityMapping;
    }


}
