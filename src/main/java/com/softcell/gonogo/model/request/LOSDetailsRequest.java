/**
 * yogeshb2:44:38 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
public class LOSDetailsRequest {

    @JsonProperty("sRefID")
    @NotBlank(groups = {LOSDetailsRequest.UpdateOrDeleteGrp.class,LOSDetailsRequest.LosGrp.class})
    private String refID;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oLosDtls")
    @NotNull(groups = {LOSDetailsRequest.UpdateOrDeleteGrp.class,LOSDetailsRequest.LosGrp.class})
    @Valid
    private LOSDetails lOSDetails;

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public LOSDetails getlOSDetails() {
        return lOSDetails;
    }

    public void setlOSDetails(LOSDetails lOSDetails) {
        this.lOSDetails = lOSDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LOSDetailsRequest{");
        sb.append("refID='").append(refID).append('\'');
        sb.append(", header=").append(header);
        sb.append(", lOSDetails=").append(lOSDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LOSDetailsRequest)) return false;
        LOSDetailsRequest that = (LOSDetailsRequest) o;
        return Objects.equal(getRefID(), that.getRefID()) &&
                Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getlOSDetails(), that.getlOSDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getRefID(), getHeader(), getlOSDetails());
    }

    public interface FetchGrp {
    }

    public interface InsertGrp {
    }

    public interface UpdateOrDeleteGrp {
    }

    public interface LosGrp{}


}
