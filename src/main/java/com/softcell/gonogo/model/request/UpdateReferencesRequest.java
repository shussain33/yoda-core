package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.ApplicantReference;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by yogeshb on 24/4/17.
 */
public class UpdateReferencesRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {UpdateReferencesRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("aReferences")
    @NotNull(groups = {UpdateReferencesRequest.FetchGrp.class})
    @Size(min = 1, groups = {UpdateReferencesRequest.FetchGrp.class})
    @Valid
    private List<ApplicantReference> applicantReferences;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public List<ApplicantReference> getApplicantReferences() {
        return applicantReferences;
    }

    public void setApplicantReferences(List<ApplicantReference> applicantReferences) {
        this.applicantReferences = applicantReferences;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdateReferencesRequest{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", applicantReferences=").append(applicantReferences);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {
    }
}
