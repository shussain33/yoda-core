package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 21/12/20.
 */
@Data
@Builder
public class VerificationQuesAns{

    @JsonProperty("QUESTION_ID")
    private String questionId;

    @JsonProperty("ANSWER")
    private String answer;
}