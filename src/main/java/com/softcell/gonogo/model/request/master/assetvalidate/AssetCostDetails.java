package com.softcell.gonogo.model.request.master.assetvalidate;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author mahesh
 */
public class AssetCostDetails {

    @JsonProperty("sAssetCtg")
    @NotEmpty(groups = {AssetCostDetails.FetchGrp.class})
    private String assetCtg;

    @JsonProperty("sDlrName")
    private String dlrName;

    @JsonProperty("sAssetMnfctr")
    @NotEmpty(groups = {AssetCostDetails.FetchGrp.class})
    private String assetManufacture;

    @JsonProperty("sAssetModelMake")
    @NotEmpty(groups = {AssetCostDetails.FetchGrp.class})
    private String assetModelMake;

    @JsonProperty("sModelNo")
    @NotEmpty(groups = {AssetCostDetails.FetchGrp.class})
    private String modelNo;

    @JsonProperty("dPrice")
    private double price;

    public String getAssetCtg() {
        return assetCtg;
    }

    public void setAssetCtg(String assetCtg) {
        this.assetCtg = assetCtg;
    }

    public String getDlrName() {
        return dlrName;
    }

    public void setDlrName(String dlrName) {
        this.dlrName = dlrName;
    }

    public String getAssetManufacture() {
        return assetManufacture;
    }

    public void setAssetManufacture(String assetManufacture) {
        this.assetManufacture = assetManufacture;
    }

    public String getAssetModelMake() {
        return assetModelMake;
    }

    public void setAssetModelMake(String assetModelMake) {
        this.assetModelMake = assetModelMake;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AssetCostDetails [assetCtg=");
        builder.append(assetCtg);
        builder.append(", dlrName=");
        builder.append(dlrName);
        builder.append(", assetManufacture=");
        builder.append(assetManufacture);
        builder.append(", assetModelMake=");
        builder.append(assetModelMake);
        builder.append(", modelNo=");
        builder.append(modelNo);
        builder.append(", price=");
        builder.append(price);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp{

    }

}
