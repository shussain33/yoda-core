package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankDetails {

    @JsonProperty("ACC_DTL_ID")
    String accDtlId;
    @JsonProperty("AVG_BANK_BAL_CAL_MONTH")
    String month;
    @JsonProperty("AVG_BANK_BAL_CAL_YEAR")
    String year;
    @JsonProperty("BALANCE_AS_ON_2ND")
    String balanceAsOn2nd;
    @JsonProperty("BALANCE_AS_ON_8TH")
    String balanceAsOn8th;
    @JsonProperty("BALANCE_AS_ON_15TH")
    String balanceAsOn15th;
    @JsonProperty("BALANCE_AS_ON_22ND")
    String balanceAsOn22nd;
    @JsonProperty("BALANCE_AS_ON_27TH")
    String balanceAsOn27th;
    @JsonProperty("CASH_CREDITS")
    String cashCredits;
    @JsonProperty("CHEQUE_CREDITS")
    String chequeCredits;
    @JsonProperty("NO_OF_CASH_CREDITS")
    String noOfCashCredits;
    @JsonProperty("NO_OF_CHEQUE_CREDITS")
    String noOfChequeCredits;
    @JsonProperty("NO_OF_CHEQUE_ISSUED")
    String noOfChequeIssued;
    @JsonProperty("NO_OF_INWARD_CHEQUE_RETURN")
    String noOfInwardChequeReturn;
    @JsonProperty("NO_OF_OUTWARD_CHEQUE_RETURN")
    String noOfOutwardChequeReturn;
    @JsonProperty("MOM_SALES")
    String momSales;
    @JsonProperty("DELETE_FLAG")
    String deleteFlag;

}
