package com.softcell.gonogo.model.request;

public class ComponentConfig {
    private String stepID;
    private String component;
    private String parallelExecutionComponent;
    private String componentID;
    private String module;
    private String parallelExecutionModule;
    private String active;
    private String moduleID;
    private String institutionId;

    public String getStepID() {
        return stepID;
    }

    public void setStepID(String stepID) {
        this.stepID = stepID;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getParallelExecutionComponent() {
        return parallelExecutionComponent;
    }

    public void setParallelExecutionComponent(String parallelExecutionComponent) {
        this.parallelExecutionComponent = parallelExecutionComponent;
    }

    public String getComponentID() {
        return componentID;
    }

    public void setComponentID(String componentID) {
        this.componentID = componentID;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getParallelExecutionModule() {
        return parallelExecutionModule;
    }

    public void setParallelExecutionModule(String parallelExecutionModule) {
        this.parallelExecutionModule = parallelExecutionModule;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getModuleID() {
        return moduleID;
    }

    public void setModuleID(String moduleID) {
        this.moduleID = moduleID;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    @Override
    public String toString() {
        return "ComponentConfig [stepID=" + stepID + ", component=" + component
                + ", parallelExecutionComponent=" + parallelExecutionComponent
                + ", componentID=" + componentID + ", module=" + module
                + ", parallelExecutionModule=" + parallelExecutionModule
                + ", active=" + active + ", moduleID=" + moduleID
                + ", institutionId=" + institutionId + "]";
    }
}
