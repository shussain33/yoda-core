package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.PinCodeMaster;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class PinCodeMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oPinCodeMaster")
    private PinCodeMaster pinCodeMaster;

    @JsonProperty("iLimit")
    private int limit;

    @JsonProperty("iSkip")
    private int skip;



    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public PinCodeMaster getPinCodeMaster() {
        return pinCodeMaster;
    }

    public void setPinCodeMaster(PinCodeMaster pinCodeMaster) {
        this.pinCodeMaster = pinCodeMaster;
    }

    @Override
    public String toString() {
        return "PinCodeMasterRequest{" +
                "header=" + header +
                ", pinCodeMaster=" + pinCodeMaster +
                ", limit=" + limit +
                ", skip=" + skip +
                '}';
    }
}
