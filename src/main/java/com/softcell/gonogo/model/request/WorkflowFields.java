/**
 * kishorp5:47:19 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author kishorp
 */
public class WorkflowFields {

    @JsonProperty("AADHAAR_VERIFY_STATUS")
    private String aadhaarVerifyStatus;
    @JsonProperty("AADHAAR_GONOGO_VERIFY_STATUS")
    private String aadhaarGoNoGoVerifyStatus;
    @JsonProperty("PAN_NAME_VERIFY_SCORE")
    private String panNameVerifyScore;
    @JsonProperty("PAN_VERIFY_STATUS")
    private String panVerifyStatus;
    @JsonProperty("PAN_GONOGO_VERIFY_STATUS")
    private String panGoNoGoVerifyStatus;
    @JsonProperty("CALCULATED_SCORE")
    private String calculatedScore;
    @JsonProperty("DEDUPE_CUSTOMER_FLAG")
    private String dedupeCustomerFlag;
    @JsonProperty("DEDUPE_CUSTOMER_STATUS")
    private String dedupeCustomerStatus;
    @JsonProperty("POSIDEX_DEDUPE_STATUS")
    private String posidexDedupeStatus;
    @JsonProperty("DEDUPE_CUSTOMER_ELG_LOAN_AMT")
    private int dedupeCusteligibleLoanAmount;
    @JsonProperty("ELIGIBLE_DECISION")
    private String eligibleDecision;
    //Commenting out below fields because SOBRE gets confused between calculated vs workflow fields.
    /*@JsonProperty("ELIGIBLE_LOAN_AMOUNT")
    private int eligibleLoanAmount;
    @JsonProperty("ELIGIBLE_LOAN_DP")
    private int eligibleLoanDp;
    @JsonProperty("ELIGIBLE_LOAN_EMI")
    private int eligibleLoanEmi;
    @JsonProperty("ELIGIBLE_LOAN_TENOR")
    private int eligibleLoanTenor*/;
    @JsonProperty("NEG_CUSTOMER_FLAG")
    private String negCustomerFlag;
    @JsonProperty("NEG_PINCODE_CHECK")
    private String negPincodeCheck;
    @JsonProperty("RESI_VERIFY_SCORE")
    private String resiVerifyScore;
    @JsonProperty("RESI_VERIFY_SCORE_STATUS")
    private String resiVerifyScoreStatus;
    @JsonProperty("ADDR_VERIFY_SCORE")
    private String addrVerifyScore;
    @JsonProperty("ADDR_VERIFY_SCORE_STATUS")
    private String addrVerifyScoreStatus;
    @JsonProperty("RESI_VERIFY_SCORE_STABILITY")
    private int resiVerifyScoreStability;
    @JsonProperty("ADDR_VERIFY_SCORE_STABILITY")
    private int addrVerifyScoreStability;
    @JsonProperty("OTP_STATUS")
    private String otpStatus;
    @JsonProperty("NTC_SCORE")
    private String ntcScore;
    // GoNoGo Application submit date in ddMMyyy format for scoring purpose.
    @JsonProperty("SUBMIT_DATE")
    private String submitDate;

    /**
     * @return the aadhaarVerifyStatus
     */
    public String getAadhaarVerifyStatus() {
        return aadhaarVerifyStatus;
    }

    /**
     * @param aadhaarVerifyStatus the aadhaarVerifyStatus to set
     */
    public void setAadhaarVerifyStatus(String aadhaarVerifyStatus) {
        this.aadhaarVerifyStatus = aadhaarVerifyStatus;
    }

    /**
     * @return the aadhaarGoNoGoVerifyStatus
     */
    public String getAadhaarGoNoGoVerifyStatus() {
        return aadhaarGoNoGoVerifyStatus;
    }

    /**
     * @param aadhaarGoNoGoVerifyStatus the aadhaarGoNoGoVerifyStatus to set
     */
    public void setAadhaarGoNoGoVerifyStatus(String aadhaarGoNoGoVerifyStatus) {
        this.aadhaarGoNoGoVerifyStatus = aadhaarGoNoGoVerifyStatus;
    }

    /**
     * @return the panNameVerifyScore
     */
    public String getPanNameVerifyScore() {
        return panNameVerifyScore;
    }

    /**
     * @param panNameVerifyScore the panNameVerifyScore to set
     */
    public void setPanNameVerifyScore(String panNameVerifyScore) {
        this.panNameVerifyScore = panNameVerifyScore;
    }

    /**
     * @return the panVerifyStatus
     */
    public String getPanVerifyStatus() {
        return panVerifyStatus;
    }

    /**
     * @param panVerifyStatus the panVerifyStatus to set
     */
    public void setPanVerifyStatus(String panVerifyStatus) {
        this.panVerifyStatus = panVerifyStatus;
    }

    /**
     * @return the panGoNoGoVerifyStatus
     */
    public String getPanGoNoGoVerifyStatus() {
        return panGoNoGoVerifyStatus;
    }

    /**
     * @param panGoNoGoVerifyStatus the panGoNoGoVerifyStatus to set
     */
    public void setPanGoNoGoVerifyStatus(String panGoNoGoVerifyStatus) {
        this.panGoNoGoVerifyStatus = panGoNoGoVerifyStatus;
    }

    /**
     * @return the calculatedScore
     */
    public String getCalculatedScore() {
        return calculatedScore;
    }

    /**
     * @param calculatedScore the calculatedScore to set
     */
    public void setCalculatedScore(String calculatedScore) {
        this.calculatedScore = calculatedScore;
    }

    /**
     * @return the dedupeCustomerFlag
     */
    public String getDedupeCustomerFlag() {
        return dedupeCustomerFlag;
    }

    /**
     * @param dedupeCustomerFlag the dedupeCustomerFlag to set
     */
    public void setDedupeCustomerFlag(String dedupeCustomerFlag) {
        this.dedupeCustomerFlag = dedupeCustomerFlag;
    }

    /**
     * @return the dedupeCustomerStatus
     */
    public String getDedupeCustomerStatus() {
        return dedupeCustomerStatus;
    }

    /**
     * @param dedupeCustomerStatus the dedupeCustomerStatus to set
     */
    public void setDedupeCustomerStatus(String dedupeCustomerStatus) {
        this.dedupeCustomerStatus = dedupeCustomerStatus;
    }

    /**
     * @return the dedupeCusteligibleLoanAmount
     */
    public int getDedupeCusteligibleLoanAmount() {
        return dedupeCusteligibleLoanAmount;
    }

    /**
     * @param dedupeCusteligibleLoanAmount the dedupeCusteligibleLoanAmount to set
     */
    public void setDedupeCusteligibleLoanAmount(int dedupeCusteligibleLoanAmount) {
        this.dedupeCusteligibleLoanAmount = dedupeCusteligibleLoanAmount;
    }

    /**
     * @return the eligibleDecision
     */
    public String getEligibleDecision() {
        return eligibleDecision;
    }

    /**
     * @param eligibleDecision the eligibleDecision to set
     */
    public void setEligibleDecision(String eligibleDecision) {
        this.eligibleDecision = eligibleDecision;
    }

    /**
     * @return the eligibleLoanAmount
     */
    /*public int getEligibleLoanAmount() {
        return eligibleLoanAmount;
    }

    *//**
     * @param eligibleLoanAmount the eligibleLoanAmount to set
     *//*
    public void setEligibleLoanAmount(int eligibleLoanAmount) {
        this.eligibleLoanAmount = eligibleLoanAmount;
    }

    *//**
     * @return the eligibleLoanDp
     *//*
    public int getEligibleLoanDp() {
        return eligibleLoanDp;
    }

    *//**
     * @param eligibleLoanDp the eligibleLoanDp to set
     *//*
    public void setEligibleLoanDp(int eligibleLoanDp) {
        this.eligibleLoanDp = eligibleLoanDp;
    }

    *//**
     * @return the eligibleLoanEmi
     *//*
    public int getEligibleLoanEmi() {
        return eligibleLoanEmi;
    }

    *//**
     * @param eligibleLoanEmi the eligibleLoanEmi to set
     *//*
    public void setEligibleLoanEmi(int eligibleLoanEmi) {
        this.eligibleLoanEmi = eligibleLoanEmi;
    }

    *//**
     * @return the eligibleLoanTenor
     *//*
    public int getEligibleLoanTenor() {
        return eligibleLoanTenor;
    }

    *//**
     * @param eligibleLoanTenor the eligibleLoanTenor to set
     *//*
    public void setEligibleLoanTenor(int eligibleLoanTenor) {
        this.eligibleLoanTenor = eligibleLoanTenor;
    }*/

    /**
     * @return the negCustomerFlag
     */
    public String getNegCustomerFlag() {
        return negCustomerFlag;
    }

    /**
     * @param negCustomerFlag the negCustomerFlag to set
     */
    public void setNegCustomerFlag(String negCustomerFlag) {
        this.negCustomerFlag = negCustomerFlag;
    }

    /**
     * @return the negPincodeCheck
     */
    public String getNegPincodeCheck() {
        return negPincodeCheck;
    }

    /**
     * @param negPincodeCheck the negPincodeCheck to set
     */
    public void setNegPincodeCheck(String negPincodeCheck) {
        this.negPincodeCheck = negPincodeCheck;
    }

    /**
     * @return the resiVerifyScore
     */
    public String getResiVerifyScore() {
        return resiVerifyScore;
    }

    /**
     * @param resiVerifyScore the resiVerifyScore to set
     */
    public void setResiVerifyScore(String resiVerifyScore) {
        this.resiVerifyScore = resiVerifyScore;
    }

    /**
     * @return the resiVerifyScoreStatus
     */
    public String getResiVerifyScoreStatus() {
        return resiVerifyScoreStatus;
    }

    /**
     * @param resiVerifyScoreStatus the resiVerifyScoreStatus to set
     */
    public void setResiVerifyScoreStatus(String resiVerifyScoreStatus) {
        this.resiVerifyScoreStatus = resiVerifyScoreStatus;
    }

    /**
     * @return the addrVerifyScore
     */
    public String getAddrVerifyScore() {
        return addrVerifyScore;
    }

    /**
     * @param addrVerifyScore the addrVerifyScore to set
     */
    public void setAddrVerifyScore(String addrVerifyScore) {
        this.addrVerifyScore = addrVerifyScore;
    }

    /**
     * @return the addrVerifyScoreStatus
     */
    public String getAddrVerifyScoreStatus() {
        return addrVerifyScoreStatus;
    }

    /**
     * @param addrVerifyScoreStatus the addrVerifyScoreStatus to set
     */
    public void setAddrVerifyScoreStatus(String addrVerifyScoreStatus) {
        this.addrVerifyScoreStatus = addrVerifyScoreStatus;
    }

    /**
     * @return the resiVerifyScoreStability
     */
    public int getResiVerifyScoreStability() {
        return resiVerifyScoreStability;
    }

    /**
     * @param resiVerifyScoreStability the resiVerifyScoreStability to set
     */
    public void setResiVerifyScoreStability(int resiVerifyScoreStability) {
        this.resiVerifyScoreStability = resiVerifyScoreStability;
    }

    /**
     * @return the addrVerifyScoreStability
     */
    public int getAddrVerifyScoreStability() {
        return addrVerifyScoreStability;
    }

    /**
     * @param addrVerifyScoreStability the addrVerifyScoreStability to set
     */
    public void setAddrVerifyScoreStability(int addrVerifyScoreStability) {
        this.addrVerifyScoreStability = addrVerifyScoreStability;
    }

    /**
     * @return the otpStatus
     */
    public String getOtpStatus() {
        return otpStatus;
    }

    /**
     * @param otpStatus the otpStatus to set
     */
    public void setOtpStatus(String otpStatus) {
        this.otpStatus = otpStatus;
    }

    /**
     * @return the ntcScore
     */
    public String getNtcScore() {
        return ntcScore;
    }

    /**
     * @param ntcScore the ntcScore to set
     */
    public void setNtcScore(String ntcScore) {
        this.ntcScore = ntcScore;
    }

    /**
     * @return the submitDate
     */
    public String getSubmitDate() {
        return submitDate;
    }

    /**
     * @param submitDate the submitDate to set
     */
    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getPosidexDedupeStatus() {
        return posidexDedupeStatus;
    }

    public void setPosidexDedupeStatus(String posidexDedupeStatus) {
        this.posidexDedupeStatus = posidexDedupeStatus;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("WorkflowFields{");
        sb.append("aadhaarVerifyStatus='").append(aadhaarVerifyStatus).append('\'');
        sb.append(", aadhaarGoNoGoVerifyStatus='").append(aadhaarGoNoGoVerifyStatus).append('\'');
        sb.append(", panNameVerifyScore='").append(panNameVerifyScore).append('\'');
        sb.append(", panVerifyStatus='").append(panVerifyStatus).append('\'');
        sb.append(", panGoNoGoVerifyStatus='").append(panGoNoGoVerifyStatus).append('\'');
        sb.append(", calculatedScore='").append(calculatedScore).append('\'');
        sb.append(", dedupeCustomerFlag='").append(dedupeCustomerFlag).append('\'');
        sb.append(", dedupeCustomerStatus='").append(dedupeCustomerStatus).append('\'');
        sb.append(", posidexDedupeStatus='").append(posidexDedupeStatus).append('\'');
        sb.append(", dedupeCusteligibleLoanAmount=").append(dedupeCusteligibleLoanAmount);
        sb.append(", eligibleDecision='").append(eligibleDecision).append('\'');
       /* sb.append(", eligibleLoanAmount=").append(eligibleLoanAmount);
        sb.append(", eligibleLoanDp=").append(eligibleLoanDp);
        sb.append(", eligibleLoanEmi=").append(eligibleLoanEmi);
        sb.append(", eligibleLoanTenor=").append(eligibleLoanTenor);*/
        sb.append(", negCustomerFlag='").append(negCustomerFlag).append('\'');
        sb.append(", negPincodeCheck='").append(negPincodeCheck).append('\'');
        sb.append(", resiVerifyScore='").append(resiVerifyScore).append('\'');
        sb.append(", resiVerifyScoreStatus='").append(resiVerifyScoreStatus).append('\'');
        sb.append(", addrVerifyScore='").append(addrVerifyScore).append('\'');
        sb.append(", addrVerifyScoreStatus='").append(addrVerifyScoreStatus).append('\'');
        sb.append(", resiVerifyScoreStability=").append(resiVerifyScoreStability);
        sb.append(", addrVerifyScoreStability=").append(addrVerifyScoreStability);
        sb.append(", otpStatus='").append(otpStatus).append('\'');
        sb.append(", ntcScore='").append(ntcScore).append('\'');
        sb.append(", submitDate='").append(submitDate).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WorkflowFields that = (WorkflowFields) o;

        if (dedupeCusteligibleLoanAmount != that.dedupeCusteligibleLoanAmount) return false;
        /*if (eligibleLoanAmount != that.eligibleLoanAmount) return false;
        if (eligibleLoanDp != that.eligibleLoanDp) return false;
        if (eligibleLoanEmi != that.eligibleLoanEmi) return false;
        if (eligibleLoanTenor != that.eligibleLoanTenor) return false;*/
        if (resiVerifyScoreStability != that.resiVerifyScoreStability) return false;
        if (addrVerifyScoreStability != that.addrVerifyScoreStability) return false;
        if (aadhaarVerifyStatus != null ? !aadhaarVerifyStatus.equals(that.aadhaarVerifyStatus) : that.aadhaarVerifyStatus != null)
            return false;
        if (aadhaarGoNoGoVerifyStatus != null ? !aadhaarGoNoGoVerifyStatus.equals(that.aadhaarGoNoGoVerifyStatus) : that.aadhaarGoNoGoVerifyStatus != null)
            return false;
        if (panNameVerifyScore != null ? !panNameVerifyScore.equals(that.panNameVerifyScore) : that.panNameVerifyScore != null)
            return false;
        if (panVerifyStatus != null ? !panVerifyStatus.equals(that.panVerifyStatus) : that.panVerifyStatus != null)
            return false;
        if (panGoNoGoVerifyStatus != null ? !panGoNoGoVerifyStatus.equals(that.panGoNoGoVerifyStatus) : that.panGoNoGoVerifyStatus != null)
            return false;
        if (calculatedScore != null ? !calculatedScore.equals(that.calculatedScore) : that.calculatedScore != null)
            return false;
        if (dedupeCustomerFlag != null ? !dedupeCustomerFlag.equals(that.dedupeCustomerFlag) : that.dedupeCustomerFlag != null)
            return false;
        if (dedupeCustomerStatus != null ? !dedupeCustomerStatus.equals(that.dedupeCustomerStatus) : that.dedupeCustomerStatus != null)
            return false;
        if (posidexDedupeStatus != null ? !posidexDedupeStatus.equals(that.posidexDedupeStatus) : that.posidexDedupeStatus != null)
            return false;
        if (eligibleDecision != null ? !eligibleDecision.equals(that.eligibleDecision) : that.eligibleDecision != null)
            return false;
        if (negCustomerFlag != null ? !negCustomerFlag.equals(that.negCustomerFlag) : that.negCustomerFlag != null)
            return false;
        if (negPincodeCheck != null ? !negPincodeCheck.equals(that.negPincodeCheck) : that.negPincodeCheck != null)
            return false;
        if (resiVerifyScore != null ? !resiVerifyScore.equals(that.resiVerifyScore) : that.resiVerifyScore != null)
            return false;
        if (resiVerifyScoreStatus != null ? !resiVerifyScoreStatus.equals(that.resiVerifyScoreStatus) : that.resiVerifyScoreStatus != null)
            return false;
        if (addrVerifyScore != null ? !addrVerifyScore.equals(that.addrVerifyScore) : that.addrVerifyScore != null)
            return false;
        if (addrVerifyScoreStatus != null ? !addrVerifyScoreStatus.equals(that.addrVerifyScoreStatus) : that.addrVerifyScoreStatus != null)
            return false;
        if (otpStatus != null ? !otpStatus.equals(that.otpStatus) : that.otpStatus != null) return false;
        if (ntcScore != null ? !ntcScore.equals(that.ntcScore) : that.ntcScore != null) return false;
        return !(submitDate != null ? !submitDate.equals(that.submitDate) : that.submitDate != null);

    }

    @Override
    public int hashCode() {
        int result = aadhaarVerifyStatus != null ? aadhaarVerifyStatus.hashCode() : 0;
        result = 31 * result + (aadhaarGoNoGoVerifyStatus != null ? aadhaarGoNoGoVerifyStatus.hashCode() : 0);
        result = 31 * result + (panNameVerifyScore != null ? panNameVerifyScore.hashCode() : 0);
        result = 31 * result + (panVerifyStatus != null ? panVerifyStatus.hashCode() : 0);
        result = 31 * result + (panGoNoGoVerifyStatus != null ? panGoNoGoVerifyStatus.hashCode() : 0);
        result = 31 * result + (calculatedScore != null ? calculatedScore.hashCode() : 0);
        result = 31 * result + (dedupeCustomerFlag != null ? dedupeCustomerFlag.hashCode() : 0);
        result = 31 * result + (dedupeCustomerStatus != null ? dedupeCustomerStatus.hashCode() : 0);
        result = 31 * result + (posidexDedupeStatus != null ? posidexDedupeStatus.hashCode() : 0);
        result = 31 * result + dedupeCusteligibleLoanAmount;
        result = 31 * result + (eligibleDecision != null ? eligibleDecision.hashCode() : 0);
      /*  result = 31 * result + eligibleLoanAmount;
        result = 31 * result + eligibleLoanDp;
        result = 31 * result + eligibleLoanEmi;
        result = 31 * result + eligibleLoanTenor;*/
        result = 31 * result + (negCustomerFlag != null ? negCustomerFlag.hashCode() : 0);
        result = 31 * result + (negPincodeCheck != null ? negPincodeCheck.hashCode() : 0);
        result = 31 * result + (resiVerifyScore != null ? resiVerifyScore.hashCode() : 0);
        result = 31 * result + (resiVerifyScoreStatus != null ? resiVerifyScoreStatus.hashCode() : 0);
        result = 31 * result + (addrVerifyScore != null ? addrVerifyScore.hashCode() : 0);
        result = 31 * result + (addrVerifyScoreStatus != null ? addrVerifyScoreStatus.hashCode() : 0);
        result = 31 * result + resiVerifyScoreStability;
        result = 31 * result + addrVerifyScoreStability;
        result = 31 * result + (otpStatus != null ? otpStatus.hashCode() : 0);
        result = 31 * result + (ntcScore != null ? ntcScore.hashCode() : 0);
        result = 31 * result + (submitDate != null ? submitDate.hashCode() : 0);
        return result;
    }
}
