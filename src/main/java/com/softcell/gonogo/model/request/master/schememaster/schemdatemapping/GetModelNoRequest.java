package com.softcell.gonogo.model.request.master.schememaster.schemdatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * @author mahesh
 */
public class GetModelNoRequest {

    @JsonProperty("sManDesc")
    private String manDesc;

    @JsonProperty("asCatDesc")
    private Set<String> catDesc;

    @JsonProperty("asMake")
    private Set<String> make;

    @JsonProperty("sMakeModelFlag")
    private String makeModelFlag;

    public String getManDesc() {
        return manDesc;
    }

    public void setManDesc(String manDesc) {
        this.manDesc = manDesc;
    }

    public Set<String> getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(Set<String> catDesc) {
        this.catDesc = catDesc;
    }

    public Set<String> getMake() {
        return make;
    }

    public void setMake(Set<String> make) {
        this.make = make;
    }

    public String getMakeModelFlag() {
        return makeModelFlag;
    }

    public void setMakeModelFlag(String makeModelFlag) {
        this.makeModelFlag = makeModelFlag;
    }

}
