package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckApplicationStatus {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {CheckApplicationStatus.FetchGrp.class})
    private String gonogoRefId;

    @JsonProperty("sApplicantID")
    private String applicantId;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getGonogoRefId() {
        return gonogoRefId;
    }

    public void setGonogoRefId(String gonogoRefId) {
        this.gonogoRefId = gonogoRefId;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CheckApplicationStatus [header=");
        builder.append(header);
        builder.append(", gonogoRefId=");
        builder.append(gonogoRefId);
        builder.append(", applicantId=");
        builder.append(applicantId);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((applicantId == null) ? 0 : applicantId.hashCode());
        result = prime * result
                + ((gonogoRefId == null) ? 0 : gonogoRefId.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CheckApplicationStatus))
            return false;
        CheckApplicationStatus other = (CheckApplicationStatus) obj;
        if (applicantId == null) {
            if (other.applicantId != null)
                return false;
        } else if (!applicantId.equals(other.applicantId))
            return false;
        if (gonogoRefId == null) {
            if (other.gonogoRefId != null)
                return false;
        } else if (!gonogoRefId.equals(other.gonogoRefId))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        return true;
    }

    public interface FetchGrp {
    }
}
