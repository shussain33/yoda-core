package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by amit on 11/3/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MigrationRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("aRefIds")
    private List<String> refIds;

    @JsonProperty("oDsaIdData")
    private List<DsaIdData> dsaIdDataList;

    @JsonProperty("bHit")
    private boolean hit;

}
