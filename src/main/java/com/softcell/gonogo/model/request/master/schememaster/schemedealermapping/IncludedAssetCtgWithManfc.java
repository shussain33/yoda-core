package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author mahesh
 */
public class IncludedAssetCtgWithManfc implements Comparable<IncludedAssetCtgWithManfc> {

    @JsonProperty("sIncldACtgr")
    @NotBlank(groups = {IncludedAssetCtgWithManfc.FetchGrp.class})
    private String includedACtgr;

    @JsonProperty("sIncldMnfcr")
    @NotBlank(groups = {IncludedAssetCtgWithManfc.FetchGrp.class})
    private String includedManfc;

    public String getIncludedACtgr() {
        return includedACtgr;
    }

    public void setIncludedACtgr(String includedACtgr) {
        this.includedACtgr = includedACtgr;
    }

    public String getIncludedManfc() {
        return includedManfc;
    }

    public void setIncludedManfc(String includedManfc) {
        this.includedManfc = includedManfc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncludedAssetCtgWithManfc that = (IncludedAssetCtgWithManfc) o;

        if (includedACtgr != null ? !includedACtgr.equals(that.includedACtgr) : that.includedACtgr != null)
            return false;
        return !(includedManfc != null ? !includedManfc.equals(that.includedManfc) : that.includedManfc != null);

    }

    @Override
    public int hashCode() {
        int result = includedACtgr != null ? includedACtgr.hashCode() : 0;
        result = 31 * result + (includedManfc != null ? includedManfc.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("IncludedAssetCtgWithManfc [includedACtgr=");
        builder.append(includedACtgr);
        builder.append(", includedManfc=");
        builder.append(includedManfc);
        builder.append("]");
        return builder.toString();
    }

    public int compareTo(IncludedAssetCtgWithManfc includedAssetCtgWithManfc) {
        int manufacturer = this.includedManfc.compareTo(includedAssetCtgWithManfc.getIncludedManfc());
        return manufacturer != 0 ? manufacturer
                : this.includedACtgr.compareTo(includedAssetCtgWithManfc.getIncludedACtgr());
    }

    public interface FetchGrp {

    }

}
