package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ApplicantdedupeRequest
{
    @JsonProperty("AppId")
    private String appId;

    @JsonProperty("ApplicantCode")
    private String applicantCode;

    @JsonProperty("ProspectCode")
    private String prospectCode;

    @JsonProperty("sCustomerDedupeStatus")
    private String cuomerDedupeStatus;

    @JsonProperty("sRemark")
    private String remarks;

    @JsonProperty("DedupeRequest")
    private UpdateDedupeBaseRequest updateDedupeBaseRequest;
}
