package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.topup.ApplicantDetails;
import com.softcell.gonogo.model.mifin.topup.MatchApplicantDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;



@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MifinDedupeLoanInfo
{
    @JsonProperty("APPLICANTDETAILS")
    private ApplicantDetails applicantDetailsList;


    @JsonProperty("MATCHEDAPPLICANTS")
    private List <MatchApplicantDetails> matchApplicantList;

    @JsonProperty("sTopUpType")
    private String topUpType;
}
