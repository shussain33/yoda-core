/**
 * bhuvneshk4:02:08 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

/**
 * @author bhuvneshk
 *
 */
public class PartialSaveRequest {

    @JsonProperty("oHeader")
    private Header header;
    @JsonProperty("sDob")
    private String dob;
    @JsonProperty("sDlrId")
    private String dealerId;
    @JsonProperty("sMobNo")
    private String mobileNo;

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the dealerId
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * @param dealerId the dealerId to set
     */
    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }
}
