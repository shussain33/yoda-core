package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 28/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChangeAllocationInfoRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {ChangeAllocationInfoRequest.InsertGrp.class})
    private String refId;

    @JsonProperty("bSetNull")
    private boolean setToNull;

    @JsonProperty("sStageName")
    private String stageName;

    @JsonProperty("bManualLock")
    private boolean manualLock;

    @JsonProperty("sAssignedUser")
    private String userName;

    @JsonProperty("sAssignedRole")
    private String role;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
