package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateStatusDedupeResquest {

    @JsonProperty("AUTHENTICATION")
    AuthenticationDetails authenticationDetails;

    @JsonProperty("BASICINFO")
    UpdateStatusDedupeBasicInfo updateStatusDedupeBasicInfo;

    @JsonProperty("DEDUPEUPDATE")
    private DedupeUpdate dedupeUpdate;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;
}
