package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.custom.SequenceProvider.ModelVariantSequenceProvider;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.group.GroupSequenceProvider;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author vinodk
 */
@GroupSequenceProvider(ModelVariantSequenceProvider.class)
public class ModelVariantMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sManufacturer")
    @NotEmpty(groups = ModelVariantMasterRequest.ModFetchGrp.class)
    private String manufacturer;

    @JsonProperty("sModel")
    @NotEmpty(groups = ModelVariantMasterRequest.VarFetchGrp.class)
    private String model;

    @JsonProperty("sReqTyp")
    @NotEmpty(groups = {ModelVariantMasterRequest.ModFetchGrp.class, ModelVariantMasterRequest.VarFetchGrp.class})
    private String requestType;

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * @param requestType the requestType to set
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ModelVariantMasterRequest [header=");
        builder.append(header);
        builder.append(", manufacturer=");
        builder.append(manufacturer);
        builder.append(", model=");
        builder.append(model);
        builder.append(", requestType=");
        builder.append(requestType);
        builder.append("]");
        return builder.toString();
    }

    public enum RequestType {
        MANUFACTURER, MODEL, VARIANT
    }

    public interface ModFetchGrp{}

    public interface VarFetchGrp{}
}
