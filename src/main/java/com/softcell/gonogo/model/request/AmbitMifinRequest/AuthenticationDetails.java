package com.softcell.gonogo.model.request.AmbitMifinRequest;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Constant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationDetails {

    @JsonProperty(value = "APP_NAME", defaultValue = Constant.BLANK)
    private String appName;

    @JsonProperty(value = "APP_PASS", defaultValue = Constant.BLANK)
    private String appPass;

    @JsonProperty(value = "IPADDRESS", defaultValue = "123.0.0.1")
    private String ipAddress;

    @JsonProperty(value = "DEVICE_ID", defaultValue = "GONOGO")
    private String deviceId;

    @JsonProperty(value = "LONGITUDE", defaultValue = "41.0")
    private String longitude;

    @JsonProperty(value = "LATITUDE", defaultValue = "23.0")
    private String latitude;

    @JsonProperty(value = "USER_ID", defaultValue = "1000000023")
    private String userId;

    @JsonProperty(value = "APP_VERSION", defaultValue = Constant.BLANK)
    private String appVersion;

    @JsonProperty(value = "UNIQUE_REQUEST_ID", defaultValue = Constant.BLANK)
    private String uniqueRequestId;
}