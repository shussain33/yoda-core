package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class DealerRankingRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {Header.FetchWithDealerCriteriaGrp.class}
    )
    @Valid
    private Header header;



    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DealerRankingRequest [header=");
        builder.append(header);
        builder.append("]");
        return builder.toString();
    }

}
