package com.softcell.gonogo.model.request.emudra;

import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by yogeshb on 20/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@org.springframework.data.mongodb.core.mapping.Document(collection = "sftpLog")
public class SFTPLog extends AuditEntity {
    private Date date;
    private String institutionId;
    private String refId;
    private String status;
    private String file;
    private String fileName;
}
