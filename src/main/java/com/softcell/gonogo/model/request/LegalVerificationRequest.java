package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.LegalVerification;
import com.softcell.gonogo.model.core.verification.PropertyVisit;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 24/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LegalVerificationRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.InsertGrp.class, Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oLegalVerification")
    @NotNull(groups = {LegalVerification.InsertGrp.class})
    private LegalVerification legalVerification;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {LegalVerificationRequest.InsertGrp.class,LegalVerificationRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
