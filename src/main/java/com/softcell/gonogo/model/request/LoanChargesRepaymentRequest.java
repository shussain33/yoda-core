package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg0302 on 26/3/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanChargesRepaymentRequest {
    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("oLoanCharges")
    @NotNull(groups = {LoanChargesRepaymentRequest.InsertGrp.class})
    private LoanCharges loanCharges;

    @JsonProperty("oRepayment")
    @NotNull(groups = {LoanChargesRepaymentRequest.InsertGrp.class})
    private Repayment repayment;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {LoanChargesRepaymentRequest.InsertGrp.class,LoanChargesRepaymentRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
