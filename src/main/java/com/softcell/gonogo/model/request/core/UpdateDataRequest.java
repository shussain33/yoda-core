package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by ssg0268 on 2/9/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateDataRequest {
    @JsonProperty("aRefId")
    private String refId;

    @JsonProperty("oDataToUpdate")
    private  DataToUpdate dataToUpdate;

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("sType")
    private String type ;
}
