package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg408 on 13/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TemplateRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.InsertGrp.class, Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oTemplateDetails")
    @NotNull(groups = {TemplateDetails.InsertGrp.class, TemplateDetails.FetchGrp.class})
    private TemplateDetails templateDetails;

    @JsonProperty("sRefId")
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }


}
