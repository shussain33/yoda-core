package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Itr {

    @JsonProperty("ITR_ID")
    String itrId;

    @JsonProperty("MEMBER_ID")
    String memberId;

    @JsonProperty("ASSESSMENT_YEAR")
    String assessmentYear;

    @JsonProperty("INC")
    String inc;

    @JsonProperty("TAX")
    String tax;

    @JsonProperty("DELETE_FLAG")
    String deleteFlag;
}
