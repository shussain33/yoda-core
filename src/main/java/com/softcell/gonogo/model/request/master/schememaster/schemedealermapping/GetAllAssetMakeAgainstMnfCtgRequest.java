package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author mahesh
 */
public class GetAllAssetMakeAgainstMnfCtgRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("aIncludedCtgWithManfcr")
    @NotNull(groups = {GetAllAssetMakeAgainstMnfCtgRequest.FetchGrp.class})
    private List<IncludedAssetCtgWithManfc> includedAssetCtgWithManfcs;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public List<IncludedAssetCtgWithManfc> getIncludedAssetCtgWithManfcs() {
        return includedAssetCtgWithManfcs;
    }

    public void setIncludedAssetCtgWithManfcs(List<IncludedAssetCtgWithManfc> includedAssetCtgWithManfcs) {
        this.includedAssetCtgWithManfcs = includedAssetCtgWithManfcs;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GetAllAssetMakeAgainstMnfCtgRequest{");
        sb.append("header=").append(header);
        sb.append(", includedAssetCtgWithManfcs=").append(includedAssetCtgWithManfcs);
        sb.append('}');
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GetAllAssetMakeAgainstMnfCtgRequest that = (GetAllAssetMakeAgainstMnfCtgRequest) o;

        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        return !(includedAssetCtgWithManfcs != null ? !includedAssetCtgWithManfcs.equals(that.includedAssetCtgWithManfcs) : that.includedAssetCtgWithManfcs != null);

    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + (includedAssetCtgWithManfcs != null ? includedAssetCtgWithManfcs.hashCode() : 0);
        return result;
    }

    public interface FetchGrp{

    }

}
