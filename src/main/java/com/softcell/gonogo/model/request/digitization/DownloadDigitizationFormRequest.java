package com.softcell.gonogo.model.request.digitization;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.gonogo.model.request.core.Header;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 27/9/17.
 */
@Data
public class DownloadDigitizationFormRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {DigitizeApplicationFormRequest.FetchGrp.class})
    private String refID;

    @JsonProperty("sFileName")
    @NotEmpty(groups = {DigitizeApplicationFormRequest.FetchGrp.class})
    private GNGWorkflowConstant fileName;

    public interface FetchGrp {

    }
}
