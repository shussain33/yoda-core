package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0268 on 13/8/19.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DsaIdData {
    @JsonProperty("sPrimaryId")
    private String primaryId;

    @JsonProperty("aSecondaryId")
    private List<String> secondaryIdList;
}
