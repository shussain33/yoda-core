package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.ReAppraisalDetails;
import com.softcell.gonogo.model.request.core.AppMetaData;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author yogeshb
 */
@Document(collection = "ApplicationDocument")
@Validated
public class ApplicationRequest extends AuditEntity implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sUserId")
    private String userId;
    @JsonProperty("sPassword")
    private String password;

    @Id
    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("oReq")
    private Request request;

    @JsonProperty("oReApprsDtls")
    @Valid
    @NotNull(groups = {ReAppraisalDetails.FetchGrp.class})
    private ReAppraisalDetails reAppraiseDetails;

    @JsonProperty("sRespFormat")
    private String responseFormat;

    @JsonProperty("sCurrentStageId")
    private String currentStageId = "DE";

    @JsonProperty("iDealerRank")
    private double dealerRank;

    @JsonProperty("sAppFormNumber")
    private String applicationFormNumber;

    @JsonProperty("oAppMetaData")
    private AppMetaData appMetaData;

    @JsonProperty("sApplicantType")
    private String applicantType;

    @JsonProperty("sCouponCode")
    private String couponCode;

    @JsonProperty("bQdeDecision")
    private boolean qdeDecision;

    @JsonProperty("oExternalServiceCalls")
    private ExternalServiceCalls externalServiceCalls;

    @JsonProperty("sMiFinProspectCode")
    private String miFinProspectCode;

    @JsonProperty("sMiFinCustomerCode")
    private String miFinCustomerCode;

    @JsonProperty("sPerfiosAckId")
    private String perfiosAckId;


    @JsonProperty("OAggregatorData")
    private AggregatorData aggregatorData;

    @JsonProperty("bStProcessed")
    private boolean stProcessed;

    @JsonProperty("bFinalTranchCancelled")
    private boolean finalTranchCancelled;

    @JsonProperty("sFinalTranchCancelledRemarks")
    private String finalTranchCancelledRemarks;

    @JsonProperty("sMifinImd2Message")
    private String mifinImd2Message;

    @JsonProperty("sMifinImd2Status")
    private String mifinImd2Status;

    public String getMifinImd2Status() {
        return mifinImd2Status;
    }

    public void setMifinImd2Status(String mifinImd2Status) {
        this.mifinImd2Status = mifinImd2Status;
    }

    public String getMifinImd2Message() {
        return mifinImd2Message;
    }

    public void setMifinImd2Message(String mifinImd2Message) {
        this.mifinImd2Message = mifinImd2Message;
    }

    public boolean isFinalTranchCancelled() { return finalTranchCancelled; }

    public void setFinalTranchCancelled(boolean finalTranchCancelled) { this.finalTranchCancelled = finalTranchCancelled; }

    public String getFinalTranchCancelledRemarks() { return finalTranchCancelledRemarks; }

    public void setFinalTranchCancelledRemarks(String finalTranchCancelledRemarks) { this.finalTranchCancelledRemarks = finalTranchCancelledRemarks; }


    public boolean isStProcessed() {  return stProcessed;   }

    public void setStProcessed(boolean stProcessed) {   this.stProcessed = stProcessed; }

    public ApplicationRequest() {
    }

    public AggregatorData getAggregatorData() {
        return aggregatorData;
    }

    public void setAggregatorData(AggregatorData aggregatorData) {
        this.aggregatorData = aggregatorData;
    }

    public double getDealerRank() {
        return dealerRank;
    }

    public void setDealerRank(double dealerRank) {
        this.dealerRank = dealerRank;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getUserId() {
        return userId;
    }

    public String getApplicantType() {        return applicantType;    }

    public void setApplicantType(String customerType) {        this.applicantType = customerType;    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getResponseFormat() {
        return responseFormat;
    }

    public void setResponseFormat(String responseFormat) {
        this.responseFormat = responseFormat;
    }

    public String getCurrentStageId() {
        return currentStageId;
    }

    public void setCurrentStageId(String currentStageId) {
        this.currentStageId = currentStageId;
    }

    /**
     * @return the reAppraiseDetails
     */
    public ReAppraisalDetails getReAppraiseDetails() {
        return reAppraiseDetails;
    }

    /**
     * @param reAppraiseDetails the reAppraiseDetails to set
     */
    public void setReAppraiseDetails(ReAppraisalDetails reAppraiseDetails) {
        this.reAppraiseDetails = reAppraiseDetails;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    /**
     * @return the applicationFormNumber
     */
    public String getApplicationFormNumber() {
        return applicationFormNumber;
    }

    /**
     * @param applicationFormNumber the applicationFormNumber to set
     */
    public void setApplicationFormNumber(String applicationFormNumber) {
        this.applicationFormNumber = applicationFormNumber;
    }

    public AppMetaData getAppMetaData() {
        return appMetaData;
    }

    public void setAppMetaData(AppMetaData appMetaData) {
        this.appMetaData = appMetaData;
    }

    public boolean isQdeDecision() {
        return qdeDecision;
    }

    public void setQdeDecision(boolean qdeDecision) {
        this.qdeDecision = qdeDecision;
    }

    public ExternalServiceCalls getExternalServiceCalls() {
        return externalServiceCalls;
    }

    public void setExternalServiceCalls(ExternalServiceCalls externalServiceCalls) {
        this.externalServiceCalls = externalServiceCalls;
    }

    public String getMiFinProspectCode() {
        return miFinProspectCode;
    }

    public void setMiFinProspectCode(String miFinProspectCode) {
        this.miFinProspectCode = miFinProspectCode;
    }

    public String getMiFinCustomerCode() {
        return miFinCustomerCode;
    }

    public void setMiFinCustomerCode(String miFinCustomerCode) {
        this.miFinCustomerCode = miFinCustomerCode;
    }

    public String getPerfiosAckId() {
        return perfiosAckId;
    }

    public void setPerfiosAckId(String perfiosAckId) {
        this.perfiosAckId = perfiosAckId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApplicationRequest{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", refID='").append(refID).append('\'');
        sb.append(", customerType='").append(applicantType).append('\'');
        sb.append(", couponCode='").append(couponCode).append('\'');
        sb.append(", header=").append(header);
        sb.append(", request=").append(request);
        sb.append(", reAppraiseDetails=").append(reAppraiseDetails);
        sb.append(", responseFormat='").append(responseFormat).append('\'');
        sb.append(", currentStageId='").append(currentStageId).append('\'');
        sb.append(", dealerRank=").append(dealerRank);
        sb.append(", applicationFormNumber='").append(applicationFormNumber).append('\'');
        sb.append(", appMetaData=").append(appMetaData);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ApplicationRequest that = (ApplicationRequest) o;
        return Objects.equals(dealerRank, that.dealerRank) &&
                Objects.equals(institutionId, that.institutionId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(password, that.password) &&
                Objects.equals(refID, that.refID) &&
                Objects.equals(header, that.header) &&
                Objects.equals(request, that.request) &&
                Objects.equals(reAppraiseDetails, that.reAppraiseDetails) &&
                Objects.equals(responseFormat, that.responseFormat) &&
                Objects.equals(currentStageId, that.currentStageId) &&
                Objects.equals(applicationFormNumber, that.applicationFormNumber) &&
                Objects.equals(appMetaData, that.appMetaData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), institutionId, userId, password, refID, header, request, reAppraiseDetails, responseFormat, currentStageId, dealerRank, applicationFormNumber, appMetaData);
    }

    public interface FetchGrp {
    }

}
