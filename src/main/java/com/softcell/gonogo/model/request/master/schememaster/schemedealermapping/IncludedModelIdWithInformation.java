package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class IncludedModelIdWithInformation {

    @JsonProperty("sIncldMnfcr")
    private String includeManfcr;

    @JsonProperty("sIncldAsstCtg")
    private String includedAsstCtg;

    @JsonProperty("sIncldAsstMk")
    private String includeAsstMake;

    @JsonProperty("sIncldMdl")
    private String includedModel;

    @JsonProperty("sModelId")
    private String modelId;

    public String getIncludeManfcr() {
        return includeManfcr;
    }

    public void setIncludeManfcr(String includeManfcr) {
        this.includeManfcr = includeManfcr;
    }

    public String getIncludedAsstCtg() {
        return includedAsstCtg;
    }

    public void setIncludedAsstCtg(String includedAsstCtg) {
        this.includedAsstCtg = includedAsstCtg;
    }

    public String getIncludeAsstMake() {
        return includeAsstMake;
    }

    public void setIncludeAsstMake(String includeAsstMake) {
        this.includeAsstMake = includeAsstMake;
    }

    public String getIncludedModel() {
        return includedModel;
    }

    public void setIncludedModel(String includedModel) {
        this.includedModel = includedModel;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("IncludedModelIdWithInformation [includeManfcr=");
        builder.append(includeManfcr);
        builder.append(", includedAsstCtg=");
        builder.append(includedAsstCtg);
        builder.append(", includeAsstMake=");
        builder.append(includeAsstMake);
        builder.append(", includedModel=");
        builder.append(includedModel);
        builder.append(", modelId=");
        builder.append(modelId);
        builder.append("]");
        return builder.toString();
    }

}
