package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by piyushg on 14/2/17.
 */
public class DealerBranchMasterRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oDealerBranchMaster")
    private DealerBranchMaster dealerBranchMaster;

    @JsonProperty("iLimit")
    private int limit;

    @JsonProperty("iSkip")
    private int skip;

    @Override
    public String toString() {
        return "DealerBranchMasterRequest{" +
                "header=" + header +
                ", dealerBranchMaster=" + dealerBranchMaster +
                ", limit=" + limit +
                ", skip=" + skip +
                '}';
    }

    public interface FetchGrp{

    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public DealerBranchMaster getDealerBranchMaster() {
        return dealerBranchMaster;
    }

    public void setDealerBranchMaster(DealerBranchMaster dealerBranchMaster) {
        this.dealerBranchMaster = dealerBranchMaster;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }
}
