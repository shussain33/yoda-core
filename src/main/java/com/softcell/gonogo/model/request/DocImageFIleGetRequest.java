package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.FileHeader;

public class DocImageFIleGetRequest {
    @JsonProperty("01")
    private FileHeader fileHeader;

    @JsonProperty("02")
    private ImageRetrieveFilter imageRetrieveFilter;

    public FileHeader getFileHeader() {
        return fileHeader;
    }

    public void setFileHeader(FileHeader fileHeader) {
        this.fileHeader = fileHeader;
    }

    public ImageRetrieveFilter getImageRetrieveFilter() {
        return imageRetrieveFilter;
    }

    public void setImageRetrieveFilter(ImageRetrieveFilter imageRetrieveFilter) {
        this.imageRetrieveFilter = imageRetrieveFilter;
    }

    @Override
    public String toString() {
        return "FIleGetRequest [fileHeader=" + fileHeader
                + ", imageRetrieveFilter=" + imageRetrieveFilter + "]";
    }
}
