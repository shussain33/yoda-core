package com.softcell.gonogo.model.request.gst;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.GstDetails;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogeshb on 19/6/17.
 */
public class GstDetailsRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {GstDetailsRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("oGstDetails")
    private GstDetails gstDetails;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public GstDetails getGstDetails() {
        return gstDetails;
    }

    public void setGstDetails(GstDetails gstDetails) {
        this.gstDetails = gstDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GstDetailsRequest{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", gstDetails=").append(gstDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GstDetailsRequest that = (GstDetailsRequest) o;

        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (refId != null ? !refId.equals(that.refId) : that.refId != null) return false;
        return gstDetails != null ? gstDetails.equals(that.gstDetails) : that.gstDetails == null;
    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + (refId != null ? refId.hashCode() : 0);
        result = 31 * result + (gstDetails != null ? gstDetails.hashCode() : 0);
        return result;
    }

    public interface FetchGrp {
    }
}
