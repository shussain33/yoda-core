package com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DisbursalDecisionInfo {

    @JsonProperty("APPLICANT_ID")
    private String applicantId;

    @JsonProperty("ENTITY_TYPE")
    private String entityType;

    @JsonProperty("ENTITY_NAME")
    private String entityName;

    @JsonProperty("DISBURSAL_AMOUNT")
    private String disbAmount;

    @JsonProperty("INSTRUMENT_TYPE")
    private String instrumentType;

    @JsonProperty("BANK_ID")
    private String bankId;

    @JsonProperty("ACCOUNT_TYPE")
    private String accountType;

    @JsonProperty("BENEFICIARY_NAME")
    private String beneficiaryName;

    @JsonProperty("ACCOUNT_NO")
    private String accountNo;

    @JsonProperty("IFSC_CODE")
    private String ifscCode;

}
