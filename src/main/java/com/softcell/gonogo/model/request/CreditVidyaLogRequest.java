package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by archana on 28/9/17.
 */
public class CreditVidyaLogRequest {

    @JsonProperty("sMobileNo")
    @NotEmpty
    private String mobileNo;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
