package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
public class ApplicationMetadataRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sQuery")
    @NotEmpty(
            groups = {
                    ApplicationMetadataRequest.FetchGrp.class
            })
    private String query;

    @JsonProperty("sQuery2")
    private String query2;

    /**
     * Newly added filters for data selection.
     *
     * @return
     */

    @JsonProperty("sDealerID")
    private String dealerID;

    @JsonProperty("sModelID")
    private String modelID;

    @JsonProperty("sModelNo")
    @NotEmpty(
            groups = {
                    ApplicationMetadataRequest.ModelNumberGrp.class,
                    ApplicationMetadataRequest.FetchWithMnfModelGrp.class
            }
    )
    private String modelNo;

    @JsonProperty("sMnfr")
    @NotEmpty(
            groups = {
                    ApplicationMetadataRequest.FetchWithManufacturerGrp.class,
                    ApplicationMetadataRequest.FetchWithMnfCatGrp.class,
                    ApplicationMetadataRequest.FetchAllForModelGrp.class,
                    ApplicationMetadataRequest.FetchWithMnfModelGrp.class
            }
    )
    private String manufacturer;

    @JsonProperty("sCtg")
    @NotEmpty(
            groups = {
                    ApplicationMetadataRequest.FetchWithCategoryGrp.class,
                    ApplicationMetadataRequest.FetchWithMnfCatGrp.class,
                    ApplicationMetadataRequest.FetchAllForModelGrp.class
            }
    )
    private String category;

    @JsonProperty("sMake")
    @NotEmpty(
            groups = {
                    ApplicationMetadataRequest.FetchAllForModelGrp.class,

            }
    )
    private String make;


    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getQuery2() {
        return query2;
    }

    public void setQuery2(String query2) {
        this.query2 = query2;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    public String getDealerID() {
        return dealerID;
    }

    public void setDealerID(String dealerID) {
        this.dealerID = dealerID;
    }

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    public interface FetchGrp {
    }

    public interface FetchWithCategoryGrp {
    }

    public interface FetchWithManufacturerGrp {
    }

    public interface FetchWithMnfCatGrp {
    }

    public interface FetchAllForModelGrp {
    }

    public interface FetchWithMnfModelGrp {
    }

    public interface ModelNumberGrp {
    }

    public interface FetchWithEmployerMasterWebGrp {

    }
}
