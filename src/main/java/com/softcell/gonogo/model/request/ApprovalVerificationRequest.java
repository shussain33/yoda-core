package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.ws.rs.HEAD;

/**
 * Created by yogesh on 13/5/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApprovalVerificationRequest {

    @JsonProperty("oHeader")
    @NotEmpty(groups = {InsertGrp.class})
    public Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {InsertGrp.class})
    private String refId;

    @JsonProperty("dUserMaxAmount")
    @NotEmpty(groups = {InsertGrp.class})
    private double userMaxAmount;

    public interface InsertGrp {
    }

}


