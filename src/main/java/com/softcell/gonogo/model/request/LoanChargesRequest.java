package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 27/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanChargesRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oLoanCharges")
    @NotNull(groups = {LoanCharges.InsertGrp.class})
    private LoanCharges loanCharges;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {LoanChargesRequest.InsertGrp.class,LoanChargesRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
