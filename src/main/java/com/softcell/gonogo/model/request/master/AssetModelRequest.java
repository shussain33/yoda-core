package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.AssetModelMaster;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class AssetModelRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oAssetModelMaster")
    private AssetModelMaster assetModelMaster;

    @JsonProperty("iLimit")
    private int limit;

    @JsonProperty("iSkip")
    private int skip;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public AssetModelMaster getAssetModelMaster() {
        return assetModelMaster;
    }

    public void setAssetModelMaster(AssetModelMaster assetModelMaster) {
        this.assetModelMaster = assetModelMaster;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AssetModelRequest [header=");
        builder.append(header);
        builder.append(", assetModelMaster=");
        builder.append(assetModelMaster);
        builder.append(", limit=");
        builder.append(limit);
        builder.append(", skip=");
        builder.append(skip);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp{

    }

}
