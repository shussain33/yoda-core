package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 26/3/18.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BreExecRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @Id
    @JsonProperty("sRefID")
    @NotNull
    private String refID;

    @JsonProperty("sApplID")
    private String applicantId;

}
