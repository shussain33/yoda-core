package com.softcell.gonogo.model.request.emudra;

import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by yogeshb on 20/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SFTPLogOutput extends AuditEntity {
    private SFTPLog sftpLog;
    private String dateTime;
}
