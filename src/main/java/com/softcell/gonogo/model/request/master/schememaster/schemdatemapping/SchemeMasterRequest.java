package com.softcell.gonogo.model.request.master.schememaster.schemdatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class SchemeMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oSchemeMasterData")
    private SchemeMasterData schemeMasterData;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public SchemeMasterData getSchemeMasterData() {
        return schemeMasterData;
    }

    public void setSchemeMasterData(SchemeMasterData schemeMasterData) {
        this.schemeMasterData = schemeMasterData;
    }

    public interface FetchGrp{

    }

}
