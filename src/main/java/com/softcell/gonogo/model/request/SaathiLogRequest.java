package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by archana on 28/9/17.
 */
public class SaathiLogRequest {
    @JsonProperty("sMobileNo")
    @NotEmpty
    private String mobileNo;

    @JsonProperty("sRefID")
    @NotEmpty
    private String refId;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

}
