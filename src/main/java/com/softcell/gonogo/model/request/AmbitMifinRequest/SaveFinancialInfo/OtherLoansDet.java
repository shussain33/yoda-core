package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OtherLoansDet  {

    @JsonProperty("OTHER_LOANS_DETL_ID")
    String otherLoansDetlId;

    @JsonProperty("INSTITUTION_FROM")
    String institutionFrom;

    @JsonProperty("ACCOUNT_NUMBER")
    String accountNumber;

    @JsonProperty("FACILITY_TYPE")
    String facilityType;

    @JsonProperty("EMI_START_DATE")
    String emiStartDate;

    @JsonProperty("TENOR_OF_LOAN")
    String tenorOfLoan;

    @JsonProperty("ORIGINAL_LOAN_AMOUNT")
    String originalLoanAmount;

    @JsonProperty("TOTAL_OUTSTANDING")
    String totalOutstanding;

    @JsonProperty("OTHER_LOAN_STATUS")
    String otherLoanStatus;

    @JsonProperty("EMI_AMT")
    String emiAmt;

    @JsonProperty("NUM_OF_EMI_PAID")
    String numOfEmiPaid;

    @JsonProperty("NUM_OF_BOUNCED_EMI")
    String numOfBouncedEmi;

    @JsonProperty("REMARKS")
    String remarks;

    @JsonProperty("CONSIDER_FOR_OBL_FLG")
    String considerForOblFlg;

    @JsonProperty("LAST_EMI_PAID_DATE")
    String lastEmiPaidDate;

    @JsonProperty("EMI_BOUNCED_TIMES_OVERDRAWN")
    String emiBouncedTimesOverDrawn;

    @JsonProperty("MOB")
    String mob;

    @JsonProperty("AVERAGE_DELAY")
    String averageDelay;

    @JsonProperty("PEAK_DELAY")
    String peakDelay;

    @JsonProperty("RTR_STATUS")
    String  rtrStatus;

    @JsonProperty("ANALYSIS_BASED_ON")
    String analysisBasedOn;

    @JsonProperty("BT_AMT")
    String btAmt;

    @JsonProperty("REG_OWNER_SOA_NAME")
    String regOwnerSoaName;

    @JsonProperty("REGN_NUM")
    String regnNum;
}
