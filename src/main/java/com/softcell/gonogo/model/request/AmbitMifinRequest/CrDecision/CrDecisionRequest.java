package com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CrDecisionRequest {
    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authentication;

    @JsonProperty("BASICINFO")
    private CRBasicInfo crBasicInfo;

    @JsonProperty("SANCTION_TERMS")
    private sanctionTermsrequest sanctionTermsrequest;

    @JsonProperty("CREDIT_DECISION")
    private List<CreditDecisionRequest> creditDecisionRequest;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;

}
