package com.softcell.gonogo.model.request.dmz;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by bhuvneshk on 17/2/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DMZResponse {

    @JsonProperty("sResponse")
    private String responseString;

    @JsonProperty("sErrorCode")
    private String errorCode;

    @JsonProperty("sErrorMsg")
    private String errorMsg;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DMZResponse{");
        sb.append("errorCode='").append(errorCode).append('\'');
        sb.append(", errorMsg='").append(errorMsg).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
