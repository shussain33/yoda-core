package com.softcell.gonogo.model.request.insurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.InsuranceType;
import com.softcell.constants.PaymentMethod;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 29/5/17.
 */
@Document(collection = "insurancePremiumDetails")
public class InsurancePremiumDetails {

    @JsonProperty("oHeader")
    @NotNull(groups = {InsurancePremiumDetails.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {InsurancePremiumDetails.FetchGrp.class,InsurancePremiumDetails.UpdateGrp.class})
    private String refId;

    @JsonProperty("sGngAssetCat")
    @NotEmpty(groups = {InsurancePremiumDetails.FetchGrp.class})
    private String gngAssetCategory;

    @JsonProperty("sInsAssetCat")
    @NotEmpty(groups = {InsurancePremiumDetails.FetchGrp.class})
    private String insAssetCategory;


    @JsonProperty("sAssetSrNumber")
    @NotEmpty(groups = {InsurancePremiumDetails.FetchGrp.class})
    private String assetSerialNumber;

    @JsonProperty("dInsurancePremium")
    private  double insurancePremium;

    @JsonProperty("dInsuranceEmi")
    private double insuranceEmi;

    @JsonProperty("bActiveFlag")
    private boolean activeFlag;

    @JsonProperty("ePaymentMethod")
    private PaymentMethod paymentMethod;

    @JsonProperty("dInsuranceDPAmt")
    private double insuranceDPAmt;


    @JsonProperty("eInsuranceType")
    @NotNull(groups = InsurancePremiumDetails.UpdateGrp.class)
    private InsuranceType insuranceType;

    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(InsuranceType insuranceType) {
        this.insuranceType = insuranceType;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public double getInsuranceDPAmt() {
        return insuranceDPAmt;
    }

    public void setInsuranceDPAmt(double insuranceDPAmt) {
        this.insuranceDPAmt = insuranceDPAmt;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public double getInsurancePremium() {
        return insurancePremium;
    }

    public void setInsurancePremium(double insurancePremium) {
        this.insurancePremium = insurancePremium;
    }

    public double getInsuranceEmi() {
        return insuranceEmi;
    }

    public void setInsuranceEmi(double insuranceEmi) {
        this.insuranceEmi = insuranceEmi;
    }

    public String getGngAssetCategory() {
        return gngAssetCategory;
    }

    public void setGngAssetCategory(String gngAssetCategory) {
        this.gngAssetCategory = gngAssetCategory;
    }

    public String getInsAssetCategory() {
        return insAssetCategory;
    }

    public void setInsAssetCategory(String insAssetCategory) {
        this.insAssetCategory = insAssetCategory;
    }

    public String getAssetSerialNumber() {
        return assetSerialNumber;
    }

    public void setAssetSerialNumber(String assetSerialNumber) {
        this.assetSerialNumber = assetSerialNumber;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InsurancePremiumDetails{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", gngAssetCategory='").append(gngAssetCategory).append('\'');
        sb.append(", insAssetCategory='").append(insAssetCategory).append('\'');
        sb.append(", assetSerialNumber='").append(assetSerialNumber).append('\'');
        sb.append(", insurancePremium=").append(insurancePremium);
        sb.append(", insuranceEmi=").append(insuranceEmi);
        sb.append(", activeFlag=").append(activeFlag);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp{

    }

    public interface UpdateGrp{

    }

}
