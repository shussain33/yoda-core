package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author mahesh
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetAllDealerAgainstCityRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("aCityWithStateName")
    @NotEmpty(groups = {GetAllDealerAgainstCityRequest.FetchGrp.class})
    private List<InclCityWithStateName> inclCityWithStateNames;

    public interface FetchGrp{

    }

}
