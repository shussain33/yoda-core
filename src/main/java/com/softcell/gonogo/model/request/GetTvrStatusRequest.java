package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by Softcell on 24/07/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetTvrStatusRequest
{
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = GetTvrStatusRequestGrp.class)
    private String referenceId;

    public interface GetTvrStatusRequestGrp
    {

    }
}
