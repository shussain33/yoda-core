package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SaveFinancialInfoBankingDetails  {

    @JsonProperty("BANKING_ID")
    String bankingId;

    @JsonProperty("ACCOUNT_TYPE")
    String accountType;

    @JsonProperty("BENEFICIARY_NAME")
    String beneficiaryName;

    @JsonProperty("ACCOUNT")
    String account;

    @JsonProperty("IFSC_CODE")
    String ifscCode;

    @JsonProperty("BANKING_SINCE_MONTHS")
    String bankingSinceMonths;

    @JsonProperty("COMPUTATION_FLAG")
    String computationFlag;

    @JsonProperty("EXISTING_EMIS")
    String existingEmis;

    @JsonProperty("AVG_CASH_CREDIT_INTEREST")
    String avgCashCreditInterest;

    @JsonProperty("PROPOSED_LOAN_EMI")
    String proposedLoanEmi;

    @JsonProperty("ABB_CALCULATION")
    String abbCalculation;

    @JsonProperty("DELETE_FLAG")
    String deleteFlag;

    @JsonProperty("BANK_DETAILS")
    List<BankDetails> bankDetailsList;

}
