package com.softcell.gonogo.model.request.AmbitMifinRequest.SearchExistingApplicantSearch;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchExistingApplicantRequest {

    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authenticationDetails;

    @JsonProperty("BASICINFO")
    private SearchApplicantBasicInfo searchApplicantBasicInfo;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;

}
