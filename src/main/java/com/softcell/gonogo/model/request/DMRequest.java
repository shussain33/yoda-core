package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 23/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DMRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oDisbursementMemo")
    @NotNull(groups = {DMRequest.InsertGrp.class})
    private DisbursementMemo disbursementMemo;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {DMRequest.InsertGrp.class,DMRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
