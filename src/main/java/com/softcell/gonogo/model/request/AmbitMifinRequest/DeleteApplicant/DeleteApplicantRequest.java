package com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeleteApplicantRequest {

    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authenticationDetails;

    @JsonProperty("APPLICANTINFO")
    private List<ApplicantInfoRequest> applicantInfoRequestList;

    @JsonProperty("BASICINFO")
    private DeleteApplicantBasicInfo deleteApplicantBasicInfo;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;
}
