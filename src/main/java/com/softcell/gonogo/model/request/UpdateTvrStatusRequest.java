package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.user.TvrDetails;
import com.softcell.gonogo.model.request.core.Header;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by anupamad on 21/7/17.
 */
@Data
public class UpdateTvrStatusRequest {

   @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {UpdateTvrStatusRequest.FetchGrp.class, UpdateTvrStatusRequest.UpdateOrDelete.class})
    private String referenceId;

    @JsonProperty("oTvrDetails")
    @NotNull(groups = {Header.FetchGrp.class})
    private TvrDetails tvrDetails;

    public interface FetchGrp {
    }

    public interface UpdateOrDelete {
    }
}
