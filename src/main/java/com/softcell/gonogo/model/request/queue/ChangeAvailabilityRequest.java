package com.softcell.gonogo.model.request.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by archana on 19/9/17.
 */
public class ChangeAvailabilityRequest {

    @JsonProperty("sUserId")
    @NotNull
    private String userId;

    @JsonProperty("bAvailable")
    private boolean availabilityFlag;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.AppSourceGrp.class})
    @Valid
    private Header header;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public boolean isAvailabilityFlag() {
        return availabilityFlag;
    }

    public void setAvailabilityFlag(boolean availabilityFlag) {
        this.availabilityFlag = availabilityFlag;
    }

}
