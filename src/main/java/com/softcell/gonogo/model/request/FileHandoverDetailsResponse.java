package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.FileHandoverDetails;

/**
 * @author vinodk
 */
public class FileHandoverDetailsResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sErrMsg")
    private String errorMessage;

    @JsonProperty("oFileHandoverDetails")
    private FileHandoverDetails fileHandoverDetails;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the fileHandoverDetails
     */
    public FileHandoverDetails getFileHandoverDetails() {
        return fileHandoverDetails;
    }

    /**
     * @param fileHandoverDetails the fileHandoverDetails to set
     */
    public void setFileHandoverDetails(FileHandoverDetails fileHandoverDetails) {
        this.fileHandoverDetails = fileHandoverDetails;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FileHandoverDetailsResponse [status=");
        builder.append(status);
        builder.append(", errorMessage=");
        builder.append(errorMessage);
        builder.append(", fileHandoverDetails=");
        builder.append(fileHandoverDetails);
        builder.append("]");
        return builder.toString();
    }
}
