package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 8/3/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationLockRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("bLocked")
    public boolean isLocked;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {ApplicationLockRequest.InsertGrp.class})
    public String refId;

    public interface InsertGrp {
    }
}
