package com.softcell.gonogo.model.request.AmbitMifinRequest.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcessDedupeBasicInfo {
    @JsonProperty("APPLICANT_CODE")
    private String applicantCode;

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;
}
