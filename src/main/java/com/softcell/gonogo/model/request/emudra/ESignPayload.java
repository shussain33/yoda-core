package com.softcell.gonogo.model.request.emudra;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogeshb on 18/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ESignPayload {
    private String institutionId;
    private String uniqueTransactionID;
    private EKycChannel eKycChannel;
    private String kycXml;
    private List<Document> documents;
}
