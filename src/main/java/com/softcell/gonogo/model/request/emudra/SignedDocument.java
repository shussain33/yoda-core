package com.softcell.gonogo.model.request.emudra;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 19/9/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class SignedDocument {
    private String file;
    private String fileName;
    private String fileType;
}
