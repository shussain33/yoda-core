package com.softcell.gonogo.model.request.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by archana on 19/9/17.
 */
public class CroAuditRequest {

    @JsonProperty("sUserIdToAudit")
    @NotNull
    private String userIdToAudit;

    @JsonProperty("bToday")
    private boolean todayFlag;

    @JsonProperty("dtFrom")
    private Date fromDate;

    @JsonProperty("dtTo")
    private Date toDate;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.AppSourceGrp.class})
    @Valid
    private Header header;

    public String getUserIdToAudit() {
        return userIdToAudit;
    }

    public void setUserIdToAudit(String userIdToAudit) {
        this.userIdToAudit = userIdToAudit;
    }


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public boolean isTodayFlag() {
        return todayFlag;
    }

    public void setTodayFlag(boolean todayFlag) {
        this.todayFlag = todayFlag;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
