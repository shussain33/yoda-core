package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.ops.ListOfDocs;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 26/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ListOfDocsRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class, Header.InsertGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oListOfDocs")
    @NotNull(groups = {ListOfDocsRequest.InsertGrp.class})
    private ListOfDocs listOfDocs;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {ListOfDocsRequest.InsertGrp.class,ListOfDocsRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
