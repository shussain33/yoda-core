package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by anupamad on 8/8/17.
 */
@Data
public class AppDisbursalRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {com.softcell.gonogo.model.request.UpdateTvrStatusRequest.FetchGrp.class, com.softcell.gonogo.model.request.UpdateTvrStatusRequest.UpdateOrDelete.class})
    private String refID;

    public interface FetchGrp {
    }

    public interface UpdateOrDelete {
    }
}
