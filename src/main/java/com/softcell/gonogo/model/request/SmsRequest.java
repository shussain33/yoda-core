package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Arrays;
import java.util.Date;

/**
 * @author vinodk
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("aMobileNumbers")
    @NotNull(groups = {SmsRequest.FetchGrp.class})
    @Size(min = 1,groups = {SmsRequest.FetchGrp.class})
    private String[] mobileNumber;

    @JsonProperty("sMessageContent")
    private String messageContent;

    @JsonProperty("bOtpSms")
    private Boolean otpSms;

    @JsonIgnore
    private Date dateTime = new Date();

    @JsonProperty("sRefId")
    private String refId;

    private String otp;

    private String uuid;

    @JsonProperty("sIdentifier")
    private String identifier;
    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the mobileNumber
     */
    public String[] getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String[] mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the messageContent
     */
    public String getMessageContent() {
        return messageContent;
    }

    /**
     * @param messageContent the messageContent to set
     */
    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    /**
     * @return the otpSms
     */
    public Boolean isOtpSms() {
        return otpSms;
    }

    /**
     * @param otpSms the otpSms to set
     */
    public void setOtpSms(Boolean otpSms) {
        this.otpSms = otpSms;
    }

    /**
     * @return the dateTime
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Boolean getOtpSms() {
        return otpSms;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsRequest [header=");
        builder.append(header);
        builder.append(", mobileNumber=");
        builder.append(Arrays.toString(mobileNumber));
        builder.append(", messageContent=");
        builder.append(messageContent);
        builder.append(", otpSms=");
        builder.append(otpSms);
        builder.append(", dateTime=");
        builder.append(dateTime);
        builder.append(", refId=");
        builder.append(refId);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp{}
}
