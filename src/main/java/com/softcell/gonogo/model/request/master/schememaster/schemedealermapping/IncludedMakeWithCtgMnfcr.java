package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class IncludedMakeWithCtgMnfcr {

    @JsonProperty("sIncldMnfcr")
    private String includeManfcr;

    @JsonProperty("sIncldAsstCtg")
    private String includedAsstCtg;

    @JsonProperty("sIncldAsstMk")
    private String includeAsstMake;

    public String getIncludeManfcr() {
        return includeManfcr;
    }

    public void setIncludeManfcr(String includeManfcr) {
        this.includeManfcr = includeManfcr;
    }

    public String getIncludedAsstCtg() {
        return includedAsstCtg;
    }

    public void setIncludedAsstCtg(String includedAsstCtg) {
        this.includedAsstCtg = includedAsstCtg;
    }

    public String getIncludeAsstMake() {
        return includeAsstMake;
    }

    public void setIncludeAsstMake(String includeAsstMake) {
        this.includeAsstMake = includeAsstMake;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("IncludedMakeWithCtgMnfcr [includeManfcr=");
        builder.append(includeManfcr);
        builder.append(", includedAsstCtg=");
        builder.append(includedAsstCtg);
        builder.append(", includeAsstMake=");
        builder.append(includeAsstMake);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncludedMakeWithCtgMnfcr that = (IncludedMakeWithCtgMnfcr) o;

        if (includeManfcr != null ? !includeManfcr.equals(that.includeManfcr) : that.includeManfcr != null)
            return false;
        if (includedAsstCtg != null ? !includedAsstCtg.equals(that.includedAsstCtg) : that.includedAsstCtg != null)
            return false;
        return !(includeAsstMake != null ? !includeAsstMake.equals(that.includeAsstMake) : that.includeAsstMake != null);

    }

    @Override
    public int hashCode() {
        int result = includeManfcr != null ? includeManfcr.hashCode() : 0;
        result = 31 * result + (includedAsstCtg != null ? includedAsstCtg.hashCode() : 0);
        result = 31 * result + (includeAsstMake != null ? includeAsstMake.hashCode() : 0);
        return result;
    }
}
