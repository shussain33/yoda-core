package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.DeviceInfo;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Objects;

@Document(collection = "customerFinancialProfile")
public class CustomerFinancialProfileRequest extends AuditEntity implements Serializable {

    @Id
    @JsonProperty("sRefID")
    @NotBlank(groups = {CustomerFinancialProfileRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("oDeviceInfo")
    private DeviceInfo deviceInfo;

    @JsonProperty("sWeeklyCreditAmt")
    private double weeklyCreditAmt;

    @JsonProperty("sWeeklyDebitAmt")
    private double weeklyDebitAmt;

    @JsonProperty("sMonthlyCreditAmt")
    private double monthlyCreditAmt;

    @JsonProperty("sMonthlyDebitAmt")
    private double monthlyDebitAmt;

    @JsonProperty("sNumberOfAccounts")
    private int numberOfAccounts;

    @JsonProperty("sLatePaymentCountForLast30")
    private int latePaymentNoticeCntForLast30Days;

    @JsonProperty("sLatePaymentCountForLast60")
    private int latePaymentNoticeCntForLast60Days;

    @JsonProperty("sLatePaymentCountForLast90")
    private int latePaymentNoticeCntForLast90Days;

    @JsonProperty("sNumberOfTransactionByATM")
    private int numberOfTransactionByATM;

    @JsonProperty("sNumberOfTransactionByNEFT")
    private int numberOfTransactionByNEFT;

    @JsonProperty("sNumberOfTransactionByIMPS")
    private int numberOfTransactionByIMPS;

    @JsonProperty("sNumberOfTransactionByRTGS")
    private int numberOfTransactionByRTGS;

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public double getWeeklyCreditAmt() {
        return weeklyCreditAmt;
    }

    public void setWeeklyCreditAmt(double weeklyCreditAmt) {
        this.weeklyCreditAmt = weeklyCreditAmt;
    }

    public double getWeeklyDebitAmt() {
        return weeklyDebitAmt;
    }

    public void setWeeklyDebitAmt(double weeklyDebitAmt) {
        this.weeklyDebitAmt = weeklyDebitAmt;
    }

    public double getMonthlyCreditAmt() {
        return monthlyCreditAmt;
    }

    public void setMonthlyCreditAmt(double monthlyCreditAmt) {
        this.monthlyCreditAmt = monthlyCreditAmt;
    }

    public double getMonthlyDebitAmt() {
        return monthlyDebitAmt;
    }

    public void setMonthlyDebitAmt(double monthlyDebitAmt) {
        this.monthlyDebitAmt = monthlyDebitAmt;
    }

    public int getNumberOfAccounts() {
        return numberOfAccounts;
    }

    public void setNumberOfAccounts(int numberOfAccounts) {
        this.numberOfAccounts = numberOfAccounts;
    }

    public int getLatePaymentNoticeCntForLast30Days() {
        return latePaymentNoticeCntForLast30Days;
    }

    public void setLatePaymentNoticeCntForLast30Days(int latePaymentNoticeCntForLast30Days) {
        this.latePaymentNoticeCntForLast30Days = latePaymentNoticeCntForLast30Days;
    }

    public int getLatePaymentNoticeCntForLast60Days() {
        return latePaymentNoticeCntForLast60Days;
    }

    public void setLatePaymentNoticeCntForLast60Days(int latePaymentNoticeCntForLast60Days) {
        this.latePaymentNoticeCntForLast60Days = latePaymentNoticeCntForLast60Days;
    }

    public int getLatePaymentNoticeCntForLast90Days() {
        return latePaymentNoticeCntForLast90Days;
    }

    public void setLatePaymentNoticeCntForLast90Days(int latePaymentNoticeCntForLast90Days) {
        this.latePaymentNoticeCntForLast90Days = latePaymentNoticeCntForLast90Days;
    }

    public int getNumberOfTransactionByATM() {
        return numberOfTransactionByATM;
    }

    public void setNumberOfTransactionByATM(int numberOfTransactionByATM) {
        this.numberOfTransactionByATM = numberOfTransactionByATM;
    }

    public int getNumberOfTransactionByNEFT() {
        return numberOfTransactionByNEFT;
    }

    public void setNumberOfTransactionByNEFT(int numberOfTransactionByNEFT) {
        this.numberOfTransactionByNEFT = numberOfTransactionByNEFT;
    }

    public int getNumberOfTransactionByIMPS() {
        return numberOfTransactionByIMPS;
    }

    public void setNumberOfTransactionByIMPS(int numberOfTransactionByIMPS) {
        this.numberOfTransactionByIMPS = numberOfTransactionByIMPS;
    }

    public int getNumberOfTransactionByRTGS() {
        return numberOfTransactionByRTGS;
    }

    public void setNumberOfTransactionByRTGS(int numberOfTransactionByRTGS) {
        this.numberOfTransactionByRTGS = numberOfTransactionByRTGS;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CustomerFinancialProfileRequest{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", deviceInfo=").append(deviceInfo);
        sb.append(", weeklyCreditAmt=").append(weeklyCreditAmt);
        sb.append(", weeklyDebitAmt=").append(weeklyDebitAmt);
        sb.append(", monthlyCreditAmt=").append(monthlyCreditAmt);
        sb.append(", monthlyDebitAmt=").append(monthlyDebitAmt);
        sb.append(", numberOfAccounts=").append(numberOfAccounts);
        sb.append(", latePaymentNoticeCntForLast30Days=").append(latePaymentNoticeCntForLast30Days);
        sb.append(", latePaymentNoticeCntForLast60Days=").append(latePaymentNoticeCntForLast60Days);
        sb.append(", latePaymentNoticeCntForLast90Days=").append(latePaymentNoticeCntForLast90Days);
        sb.append(", numberOfTransactionByATM=").append(numberOfTransactionByATM);
        sb.append(", numberOfTransactionByNEFT=").append(numberOfTransactionByNEFT);
        sb.append(", numberOfTransactionByIMPS=").append(numberOfTransactionByIMPS);
        sb.append(", numberOfTransactionByRTGS=").append(numberOfTransactionByRTGS);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CustomerFinancialProfileRequest that = (CustomerFinancialProfileRequest) o;
        return Double.compare(that.weeklyCreditAmt, weeklyCreditAmt) == 0 &&
                Double.compare(that.weeklyDebitAmt, weeklyDebitAmt) == 0 &&
                Double.compare(that.monthlyCreditAmt, monthlyCreditAmt) == 0 &&
                Double.compare(that.monthlyDebitAmt, monthlyDebitAmt) == 0 &&
                numberOfAccounts == that.numberOfAccounts &&
                latePaymentNoticeCntForLast30Days == that.latePaymentNoticeCntForLast30Days &&
                latePaymentNoticeCntForLast60Days == that.latePaymentNoticeCntForLast60Days &&
                latePaymentNoticeCntForLast90Days == that.latePaymentNoticeCntForLast90Days &&
                numberOfTransactionByATM == that.numberOfTransactionByATM &&
                numberOfTransactionByNEFT == that.numberOfTransactionByNEFT &&
                numberOfTransactionByIMPS == that.numberOfTransactionByIMPS &&
                numberOfTransactionByRTGS == that.numberOfTransactionByRTGS &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(deviceInfo, that.deviceInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), refId, deviceInfo, weeklyCreditAmt, weeklyDebitAmt, monthlyCreditAmt,
                            monthlyDebitAmt, numberOfAccounts, latePaymentNoticeCntForLast30Days,
                            latePaymentNoticeCntForLast60Days, latePaymentNoticeCntForLast90Days,
                            numberOfTransactionByATM, numberOfTransactionByNEFT, numberOfTransactionByIMPS,
                            numberOfTransactionByRTGS);
    }

    public interface FetchGrp { }
}
