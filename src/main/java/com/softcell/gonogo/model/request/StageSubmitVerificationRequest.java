package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by amit on 5/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StageSubmitVerificationRequest {
    @JsonProperty("oHeader")
    @NotEmpty(groups = {ApprovalVerificationRequest.InsertGrp.class})
    public Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {ApprovalVerificationRequest.InsertGrp.class})
    private String refId;

    @JsonProperty("sAction")
    private String action;
}
