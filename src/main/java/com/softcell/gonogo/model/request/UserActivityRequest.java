package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserActivityRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("dtFromDate")
    @NotNull(groups = {UserActivityRequest.FetchGrp.class})
    private Date fromDate;

    @JsonProperty("dtToDate")
    @NotNull(groups = {UserActivityRequest.FetchGrp.class})
    private Date toDate;

    @JsonProperty("sRefId")
    @NotNull(groups = {UserActivityRequest.FetchGrp.class})
    private String refId;

    public interface FetchGrp {

    }
}
