package com.softcell.gonogo.model.request.master;

/**
 * @author mahesh
 */
public class ApplicableSchemeReportDetails {

    private String schemeID;

    private String schemeDesc;

    private String validFromDate;

    private String validToDate;

    private String excludedDate;

    private String includedDate;

    private String maufacturer;

    private String assetCategory;

    private String assetMake;

    private String modelNo;

    private String stateName;

    private String cityName;

    private String dealerName;

    private boolean allStateCityDealer;

    private String makerId;

    private String makeDate;

    private String checkerId;

    private String checkDate;

    private String schemeStatus;

    private String productFlag;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private ApplicableSchemeReportDetails applicableSchemeReportDetails = new ApplicableSchemeReportDetails();

        public ApplicableSchemeReportDetails build() {
            return applicableSchemeReportDetails;
        }

        public Builder schemeID(String schemeID) {
            this.applicableSchemeReportDetails.schemeID = schemeID;
            return this;
        }

        public Builder schemeDesc(String schemeDesc) {
            this.applicableSchemeReportDetails.schemeDesc = schemeDesc;
            return this;
        }


        public Builder validFromDate(String validFromDate) {
            this.applicableSchemeReportDetails.validFromDate = validFromDate;
            return this;
        }

        public Builder validToDate(String validToDate) {
            this.applicableSchemeReportDetails.validToDate = validToDate;
            return this;
        }

        public Builder excludedDate(String excludedDate) {
            this.applicableSchemeReportDetails.excludedDate = excludedDate;
            return this;
        }

        public Builder includedDate(String includedDate) {
            this.applicableSchemeReportDetails.includedDate = includedDate;
            return this;
        }


        public Builder makeDate(String makeDate) {
            this.applicableSchemeReportDetails.makeDate = makeDate;
            return this;
        }

        public Builder makerId(String makerId) {
            this.applicableSchemeReportDetails.makerId = makerId;
            return this;
        }

        public Builder checkerId(String checkerId) {
            this.applicableSchemeReportDetails.checkerId = checkerId;
            return this;
        }

        public Builder checkDate(String checkDate) {
            this.applicableSchemeReportDetails.checkDate = checkDate;
            return this;
        }


        public Builder schemeStatus(String schemeStatus) {
            this.applicableSchemeReportDetails.schemeStatus = schemeStatus;
            return this;
        }


        public Builder productFlag(String productFlag) {
            this.applicableSchemeReportDetails.productFlag = productFlag;
            return this;
        }

        public Builder manufacturer(String maufacturer) {
            this.applicableSchemeReportDetails.maufacturer = maufacturer;
            return this;
        }

        public Builder assetCategory(String assetCategory) {
            this.applicableSchemeReportDetails.assetCategory = assetCategory;
            return this;
        }


        public Builder assetMake(String assetMake) {
            this.applicableSchemeReportDetails.assetMake = assetMake;
            return this;
        }

        public Builder modelNo(String modelNo) {
            this.applicableSchemeReportDetails.modelNo = modelNo;
            return this;
        }

        public Builder stateName(String stateName) {
            this.applicableSchemeReportDetails.stateName = stateName;
            return this;
        }


        public Builder cityName(String cityName) {
            this.applicableSchemeReportDetails.cityName = cityName;
            return this;
        }

        public Builder dealerName(String dealerName) {
            this.applicableSchemeReportDetails.dealerName = dealerName;
            return this;
        }

        public Builder allStateCityDealer(boolean allStateCityDealer) {
            this.applicableSchemeReportDetails.allStateCityDealer = allStateCityDealer;
            return this;
        }


    }




    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApplicableSchemeReportDetails{");
        sb.append("allStateCityDealer=").append(allStateCityDealer);
        sb.append(", schemeID='").append(schemeID).append('\'');
        sb.append(", schemeDesc='").append(schemeDesc).append('\'');
        sb.append(", validFromDate='").append(validFromDate).append('\'');
        sb.append(", validToDate='").append(validToDate).append('\'');
        sb.append(", excludedDate='").append(excludedDate).append('\'');
        sb.append(", includedDate='").append(includedDate).append('\'');
        sb.append(", maufacturer='").append(maufacturer).append('\'');
        sb.append(", assetCategory='").append(assetCategory).append('\'');
        sb.append(", assetMake='").append(assetMake).append('\'');
        sb.append(", modelNo='").append(modelNo).append('\'');
        sb.append(", stateName='").append(stateName).append('\'');
        sb.append(", cityName='").append(cityName).append('\'');
        sb.append(", dealerName='").append(dealerName).append('\'');
        sb.append(", makerId='").append(makerId).append('\'');
        sb.append(", makeDate='").append(makeDate).append('\'');
        sb.append(", checkerId='").append(checkerId).append('\'');
        sb.append(", checkDate='").append(checkDate).append('\'');
        sb.append(", schemeStatus='").append(schemeStatus).append('\'');
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public String getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    public String getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    public String getExcludedDate() {
        return excludedDate;
    }

    public void setExcludedDate(String excludedDate) {
        this.excludedDate = excludedDate;
    }

    public String getIncludedDate() {
        return includedDate;
    }

    public void setIncludedDate(String includedDate) {
        this.includedDate = includedDate;
    }

    public String getMaufacturer() {
        return maufacturer;
    }

    public void setMaufacturer(String maufacturer) {
        this.maufacturer = maufacturer;
    }

    public String getAssetCategory() {
        return assetCategory;
    }

    public void setAssetCategory(String assetCategory) {
        this.assetCategory = assetCategory;
    }

    public String getAssetMake() {
        return assetMake;
    }

    public void setAssetMake(String assetMake) {
        this.assetMake = assetMake;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public boolean isAllStateCityDealer() {
        return allStateCityDealer;
    }

    public void setAllStateCityDealer(boolean allStateCityDealer) {
        this.allStateCityDealer = allStateCityDealer;
    }

    public String getMakerId() {
        return makerId;
    }

    public void setMakerId(String makerId) {
        this.makerId = makerId;
    }

    public String getCheckerId() {
        return checkerId;
    }

    public void setCheckerId(String checkerId) {
        this.checkerId = checkerId;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getSchemeStatus() {
        return schemeStatus;
    }

    public void setSchemeStatus(String schemeStatus) {
        this.schemeStatus = schemeStatus;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }
}
