package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.core.CroJustification;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Cro approve request comes here
 *
 * @author yogeshb
 */
public class CroApprovalRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {CroApprovalRequest.FetchGrp.class, CroApprovalRequest.UpdateOrDelete.class})
    private String referenceId;

    @JsonProperty("sAppStat")
    @NotBlank(groups = {CroApprovalRequest.FetchGrp.class, CroApprovalRequest.UpdateOrDelete.class})
    private String applicationStatus;

    @JsonProperty("aCroJustification")
    @NotNull(groups = {CroApprovalRequest.UpdateOrDelete.class})
    @Size(min = 1, groups = {CroApprovalRequest.UpdateOrDelete.class})
    private List<CroJustification> croJustification;

    @JsonProperty("aDedupeRefID")
    @Size(groups = {CroApprovalRequest.UpdateOrDelete.class})
    private List<String> dedupeRefrenceIds;

    /**
     * Newley added field by Yogesh for update following fields .
     */

    //If true then following fields must be update.
    @JsonProperty("bApprAmtExist")
    private boolean ApprovedAmountExist;

    @JsonProperty("dApprAmt")
    private double approvedAmount;

    @JsonProperty("dLtv")
    private double ltv;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("dEmi")
    private double emi;

    /**
     * @return
     */
    public List<CroJustification> getCroJustification() {
        return croJustification;
    }

    /**
     * @param croJustification
     */
    public void setCroJustification(List<CroJustification> croJustification) {
        this.croJustification = croJustification;
    }

    /**
     * @return
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * @param referenceId
     */
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    /**
     * @return
     */
    public String getApplicationStatus() {
        return applicationStatus;
    }

    /**
     * @param applicationStatus
     */
    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    /**
     * @return the dedupeRefrenceId
     */
    public List<String> getDedupeRefrenceIds() {
        return dedupeRefrenceIds;
    }

    /**
     * @param dedupeRefrenceIds
     */
    public void setDedupeRefrenceIds(List<String> dedupeRefrenceIds) {
        this.dedupeRefrenceIds = dedupeRefrenceIds;
    }

    /**
     * @return
     */
    public double getApprovedAmount() {
        return approvedAmount;
    }

    /**
     * @param approvedAmount
     */
    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    /**
     * @return
     */
    public int getTenor() {
        return tenor;
    }

    /**
     * @param tenor
     */
    public void setTenor(int tenor) {
        this.tenor = tenor;
    }

    /**
     * @return
     */
    public double getEmi() {
        return emi;
    }

    /**
     * @param emi
     */
    public void setEmi(double emi) {
        this.emi = emi;
    }

    /**
     * @return
     */
    public boolean isApprovedAmountExist() {
        return ApprovedAmountExist;
    }

    /**
     * @param approvedAmountExist
     */
    public void setApprovedAmountExist(boolean approvedAmountExist) {
        ApprovedAmountExist = approvedAmountExist;
    }

    public double getLtv() { return ltv; }

    public void setLtv(double ltv) { this.ltv = ltv; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CroApprovalRequest{");
        sb.append("header=").append(header);
        sb.append(", referenceId='").append(referenceId).append('\'');
        sb.append(", applicationStatus='").append(applicationStatus).append('\'');
        sb.append(", croJustification=").append(croJustification);
        sb.append(", dedupeRefrenceIds=").append(dedupeRefrenceIds);
        sb.append(", ApprovedAmountExist=").append(ApprovedAmountExist);
        sb.append(", approvedAmount=").append(approvedAmount);
        sb.append(", ltv=").append(ltv);
        sb.append(", tenor=").append(tenor);
        sb.append(", emi=").append(emi);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CroApprovalRequest)) return false;
        CroApprovalRequest that = (CroApprovalRequest) o;
        return Objects.equal(isApprovedAmountExist(), that.isApprovedAmountExist()) &&
                Objects.equal(getApprovedAmount(), that.getApprovedAmount()) &&
                Objects.equal(getTenor(), that.getTenor()) &&
                Objects.equal(getEmi(), that.getEmi()) &&
                Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getReferenceId(), that.getReferenceId()) &&
                Objects.equal(getApplicationStatus(), that.getApplicationStatus()) &&
                Objects.equal(getCroJustification(), that.getCroJustification()) &&
                Objects.equal(getDedupeRefrenceIds(), that.getDedupeRefrenceIds());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHeader(), getReferenceId(), getApplicationStatus(), getCroJustification(), getDedupeRefrenceIds(), isApprovedAmountExist(), getApprovedAmount(), getTenor(), getEmi());
    }

    public interface FetchGrp {
    }

    public interface UpdateOrDelete {
    }
}
