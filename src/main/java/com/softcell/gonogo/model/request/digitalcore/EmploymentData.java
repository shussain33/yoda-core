package com.softcell.gonogo.model.request.digitalcore;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced.Match;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 2/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentData {

    @JsonProperty("sEmploymentName")
    private String employmentName;

    @JsonProperty("sEmploymentCategory")
    private String employmentCategory;

    @JsonProperty("iTotalYearsOfExperience")
    private Integer totalYearsOfExperience; // in year and month

    @JsonProperty("iTimeWithEmployer")
    private Integer timeWithEmployer; // in year and month

    @JsonProperty("sEmailAddress")
    private String emailAddress;

    @JsonProperty("sModePayment")
    private String modePayment;

    @JsonProperty("bEmailStatus")
    private boolean emailStatus;

    @JsonProperty("bDomainStatus")
    private boolean domainStatus;

    @JsonProperty("sEmplType")
    private String employmentType;

    @JsonProperty("bEmployerStatus")
    private boolean employerStatus;

    @JsonProperty("oEpfHistory")
    private Match epfHistory;

}
