package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 7/2/17.
 */
public class NegativeAreaFundingRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sPinCode")
    @NotEmpty(groups = {NegativeAreaFundingRequest.FetchGrp.class})
    private String pinCode;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NegativeAreaFundingRequest{");
        sb.append("header=").append(header);
        sb.append(", pinCode='").append(pinCode).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {
    }

}
