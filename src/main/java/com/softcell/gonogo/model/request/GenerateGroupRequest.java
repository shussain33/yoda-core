package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 11/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GenerateGroupRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("bBranchWise")
    private boolean branchWise;

    @JsonProperty("bRoleWise")
    private boolean roleWise;

    @JsonProperty("bBranchRoleWise")
    private boolean branchRoleWise;

    @JsonProperty("bProductWise")
    private boolean productWise;
}
