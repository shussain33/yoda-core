package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by mahesh on 16/2/17.
 */
public class SerialNumberInfoLogRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("sVendor")
    @NotEmpty(
            groups = {
                    SerialNumberInfoLogRequest.FetchGrp.class
            }
    )
    private String vendor;

    @JsonProperty("dtStartFrom")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    SerialNumberInfoLogRequest.FetchGrp.class
            }
    )
    private Date dateStartFrom;

    @JsonProperty("dtEnd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    SerialNumberInfoLogRequest.FetchGrp.class
            }
    )
    private Date endDate;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Date getDateStartFrom() {
        return dateStartFrom;
    }

    public void setDateStartFrom(Date dateStartFrom) {
        this.dateStartFrom = dateStartFrom;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public interface FetchGrp {

    }
}
