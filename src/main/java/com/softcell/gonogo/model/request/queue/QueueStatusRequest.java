package com.softcell.gonogo.model.request.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by archana on 17/7/17.
 */
@Validated
public class QueueStatusRequest implements Serializable {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.AppSourceGrp.class})
    @Valid
    private Header header;

    @JsonProperty("bDbStatus")
    private boolean dbStatus;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public boolean isDbStatus() {
        return dbStatus;
    }

    public void setDbStatus(boolean dbStatus) {
        this.dbStatus = dbStatus;
    }

}
