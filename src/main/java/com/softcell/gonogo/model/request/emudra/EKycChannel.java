package com.softcell.gonogo.model.request.emudra;

/**
 * Created by yogeshb on 14/9/17.
 */
public enum EKycChannel {
    OTP,
    Biometric,
    IRIS
}
