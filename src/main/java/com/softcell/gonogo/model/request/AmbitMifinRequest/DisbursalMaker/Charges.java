package com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
public class Charges {

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("LOANCHARGEID")
    private String loanChargeId;

    @JsonProperty("CHARGE_NAME")
    private String chargeName;

    @JsonProperty("CHARGE_DATE")
    private String chargeDate;

    @JsonProperty("CHARGE_AMT_CALCULATION")
    private String chargeAmtCalculation;

    @JsonProperty("CHARGE_PERCENTAGE")
    private String chargePercentage;

    @JsonProperty("CHARGE_AMOUNT")
    private String chargeAmount;

}
