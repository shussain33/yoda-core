package com.softcell.gonogo.model.request.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by archana on 16/11/17.
 */
public class SchedulerInfoRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.AppSourceGrp.class})
    @Valid
    private Header header;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }
}
