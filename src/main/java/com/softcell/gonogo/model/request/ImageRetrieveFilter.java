package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImageRetrieveFilter {

    @JsonProperty("01")
    private String referenceID;

    @JsonProperty("02")
    private String docName;

    @JsonProperty("03")
    private String applicantID;

    @JsonProperty("04")
    private String fileType;

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getApplicantID() {
        return applicantID;
    }

    public void setApplicantID(String applicantID) {
        this.applicantID = applicantID;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    @Override
    public String toString() {
        return "ImageRetrieveFilter [referenceID=" + referenceID + ", docName="
                + docName + ", applicantID=" + applicantID + ", fileType="
                + fileType + "]";
    }
}
