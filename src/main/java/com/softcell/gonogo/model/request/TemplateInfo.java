package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.templates.TemplateConfiguration;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg222 on 12/1/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TemplateInfo {

    @JsonProperty("aDefaultList")
    private List<TemplateDetails> templateDetailsList;

    @JsonProperty("aVelocityList")
    private List<TemplateConfiguration> templateConfigurationList;
}
