package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveApplicantRequest {

    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authentication;

    @JsonProperty("BASICINFO")
    private SaveApplicantBasicInfo saveApplicantBasicInfo;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;

    @JsonProperty("P_FORM60")
    private String p_form;
}