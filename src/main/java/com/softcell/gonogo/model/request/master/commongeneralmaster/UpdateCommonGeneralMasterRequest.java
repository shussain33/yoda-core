package com.softcell.gonogo.model.request.master.commongeneralmaster;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.CommonGeneralParameterMaster;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 16/5/17.
 */
public class UpdateCommonGeneralMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sUniqueId")
    @NotEmpty(groups = {UpdateCommonGeneralMasterRequest.FetchGrp.class})
    private String uniqueId;

    @JsonProperty("oCommonGeneralParameterMaster")
    @NotNull(groups = {UpdateCommonGeneralMasterRequest.FetchGrp.class})
    @Valid
    private CommonGeneralParameterMaster commonGeneralParameterMaster;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public CommonGeneralParameterMaster getCommonGeneralParameterMaster() {
        return commonGeneralParameterMaster;
    }

    public void setCommonGeneralParameterMaster(CommonGeneralParameterMaster commonGeneralParameterMaster) {
        this.commonGeneralParameterMaster = commonGeneralParameterMaster;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdateCommonGeneralMasterRequest{");
        sb.append("header=").append(header);
        sb.append(", uniqueId='").append(uniqueId).append('\'');
        sb.append(", commonGeneralParameterMaster=").append(commonGeneralParameterMaster);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {
    }

}
