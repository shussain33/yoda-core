package com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
public class DisbursalSchedule {

    @JsonProperty("DISBURSAL_DATE")
    private String disbDate;

    @JsonProperty("DISBURSAL_AMOUNT")
    private String disbAmount;

    @JsonProperty("DISBURSAL_STAGE")
    private String disbStage;
}
