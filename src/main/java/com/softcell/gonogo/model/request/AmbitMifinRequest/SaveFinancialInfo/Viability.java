package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Viability {

    @JsonProperty("SEGMENT")
    String segment;

    @JsonProperty("MANUFACTURER")
    String manufacturer;

    @JsonProperty("MAKE_N_MODEL")
    String makeNModel;

    @JsonProperty("VARIANT")
    String variant;

    @JsonProperty("PRODUCT_TYPE")
    String productType;

    @JsonProperty("MFG_YEAR")
    String mfgYear;

    @JsonProperty("PROPOSED_EMI")
    String proposedEmi;

    @JsonProperty("LOAD_CAPACITY_IN_TON_KL_G")
    String loadCapacityInTonKlG;

    @JsonProperty("ROUTE")
    String route;

    @JsonProperty("NATURE_OF_GOODS_G")
    String natureOfGoodsG;

    @JsonProperty("DISTANCE_IN_KM_PER_TRIP_G")
    String distanceInKmPerTripG;

    @JsonProperty("NO_OF_TRIPS_PER_MONTH_G")
    String noOfTripsPerMonthG;

    @JsonProperty("AVG_LOAD_IN_TON_KL_G")
    String avgLoadInTonKlG;

    @JsonProperty("RATE_PER_TON_OR_KL_G")
    String ratePerTonOrKlG;

    @JsonProperty("FUEL_AVG_KM_PER_LITRE")
    String fuelAvgKmPerLitre;

    @JsonProperty("COST_PER_LITRE_OF_FUEL")
    String costPerLitreOfFuel;

    @JsonProperty("NO_OF_TYRES")
    String noOfTyres;

    @JsonProperty("COST_OF_ONE_TYRE")
    String costOfOneTyre;

    @JsonProperty("LIFE_NEW_TYRES_IN_KM")
    String lifeNewTyresInKm;

    @JsonProperty("DRIVERS_SALARY_N_ALLOWANCES")
    String driversSalaryNAllowances;

    @JsonProperty("CLEANERS_SALARY_N_ALLOWANCES")
    String cleanersSalaryNAllowances;

    @JsonProperty("PERMIT_COST")
    String permitCost;

    @JsonProperty("FC_CHARGES")
    String fcCharges;

    @JsonProperty("TOLL_TAX_PAID")
    String tollTaxPaid;

    @JsonProperty("TAXES")
    String taxes;

    @JsonProperty("MAINTENANCE")
    String maintenance;

    @JsonProperty("MISCELLANEOUS")
    String miscellaneous;

    @JsonProperty("INSURANCE_EXPENSES")
    String insuranceExpenses;

    @JsonProperty("REMARKS")
    String remarks;

    @JsonProperty("NATURE_OF_SERVICE_P")
    String natureOfServicep;

    @JsonProperty("DISTANCE_IN_KM_BOTH_SIDE_P")
    String distanceInKmBothSidep;

    @JsonProperty("NO_OF_TRIPS_IN_DAY_P")
    String noOfTripsInDayp;

    @JsonProperty("NO_OF_TRIPS_IN_MONTH_P")
    String noOfTripsInMonthp;

    @JsonProperty("SEATING_CAPACITY_P")
    String seatingCapacityp;

    @JsonProperty("SLEEPER_CAPACITY_P")
    String sleeperCapacityp;

    @JsonProperty("AVG_OCCUPANCY_IN_PERCENT_P")
    String avgOccupancyInPercentp;

    @JsonProperty("SEATING_RATE_P")
    String seatingRatep;

    @JsonProperty("SLEEPING_RATE_P")
    String sleepingRatep;

}
