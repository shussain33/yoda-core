package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 28/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyMasterRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;
}
