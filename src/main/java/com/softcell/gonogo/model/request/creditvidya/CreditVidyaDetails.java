package com.softcell.gonogo.model.request.creditvidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.PaymentMethod;
import com.softcell.gonogo.model.request.core.Header;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by Softcell on 30/08/17.
 * creditVidyaDetails
 */


@Document(collection = "creditVidyaDetails")
public class CreditVidyaDetails {

    @JsonProperty("oHeader")
    @NotNull(groups = {CreditVidyaDetails.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("dCVAmount")
    private double cVAmount;

    //Above enum type{@PaymentMethod} is used to set the value here.
    @JsonProperty("ePaymentMethod")
    private PaymentMethod paymentMethod;

    @JsonProperty("bActiveFlag")
    private boolean activeFlag;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public double getcVAmount() {
        return cVAmount;
    }

    public void setcVAmount(double cVAmount) {
        this.cVAmount = cVAmount;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public interface FetchGrp {

    }
}
