package com.softcell.gonogo.model.request.emudra;

import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by yogeshb on 20/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@org.springframework.data.mongodb.core.mapping.Document(collection = "eSignedLog")
public class ESignedLog extends AuditEntity {
    private Date requestDate;
    private Date responseDate;
    private String institutionId;
    private String refId;
    private String aadhaarNumber;
    private String authMode;
    private String transactionId;
    private int numberOfDocSend;
    private int numberOfDocSigned;
    private String status;
    private boolean consentToESigned;
    private String fileType;
    private String requestXml;
    private String responseXml;
    private String requestJson;
    private String responseJson;
    private String requestType;
     private Date date;

}
