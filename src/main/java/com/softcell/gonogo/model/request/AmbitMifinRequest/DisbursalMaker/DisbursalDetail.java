package com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
public class DisbursalDetail {

    @JsonProperty("SEND_TO_AUTHOR")
    private String sendToAuthor;

    @JsonProperty("FILE_ACKNOWLEDGEMENT_FLAG")
    private String fileAckFlag;

    @JsonProperty("FILE_ACKNOWLEDGEMENT_DATE")
    private String fileAckDate;

    @JsonProperty("FILE_ACKNOWLEDGEMENT_HOURS")
    private String fileAckHours;

    @JsonProperty("FILE_ACKNOWLEDGEMENT_MIN")
    private String fileAckMin;

    @JsonProperty("FINAL_DISBURSAL")
    private String finalDisbursal;

}
