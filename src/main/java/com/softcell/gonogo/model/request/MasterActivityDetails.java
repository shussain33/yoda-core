package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

import static com.softcell.constants.Constant.NON_TRANSACTIONAL;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class MasterActivityDetails {

    @JsonProperty("sUserId")
    private String userName;

    @JsonProperty("sUserRole")
    private String userRole;

    @JsonProperty("sEmailId")
    private String emailId;

    @JsonProperty("sDeviceInfo")
    private String deviceInfo;

    @JsonProperty("sMasterClassName")
    private String masterClassName;

    @JsonProperty("sCollectionName")
    private String collectionName;

    @JsonProperty("sError")
    private String error;

    @JsonProperty("oScriptId")
    private Object scriptId;

    @Id
    @JsonProperty("oUuid")
    private String uuid;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sMasterName")
    private String masterName; // Done

    @JsonProperty("sFileType")
    private String fileType; // Done

    @JsonProperty("sFileId")
    private String fileId;  // Done

    @JsonProperty("sFileLocation")
    private String fileLocation;  //Done

    @JsonProperty("sMasterCategory")
    private String masterCategory = NON_TRANSACTIONAL;

    @JsonProperty("sStatus")
    private String status = Status.PENDING.name();

    @JsonProperty("iStageRecordsProcessed")
    private int stageRecordsProcessed;

    @JsonProperty("iStageRecordsFailed")
    private int stageRecordsFailed;  // DONE

    @JsonProperty("iRecordsProcessed")
    private int recordsProcessed;  // MDM

    @JsonProperty("iRecordsFailed")
    private int recordsFailed;   // MDM

    @JsonProperty("oResponsePayload")
    private Object responsePayload;

    @JsonProperty("dtCreatedDate")
    private Date createdDate;

    @JsonProperty("dtUpdateDate")
    private Date updateDate;

    @JsonProperty("bFTPUploadStatus")
    private boolean ftpUploadStatus;

    @JsonProperty("sFTPLocation")
    private String ftpLocation;

    @JsonProperty("sFileName")
    private String fileName;
}
