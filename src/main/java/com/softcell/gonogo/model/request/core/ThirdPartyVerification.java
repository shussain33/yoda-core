package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Transient;
import java.util.Date;
import java.util.stream.Stream;

/**
 * Created by amit on 27/2/18.
 */
public class ThirdPartyVerification extends ApplicationVerification {

    public enum Status {
        PENDING,
        SUBMITTED,
        VERIFIED;

        public static Status get(String statusCode) {
            return Stream.of(values())
                    .filter(statusVal -> StringUtils.equalsIgnoreCase( statusCode, statusVal.name()))
                    .findFirst().orElse(null);
        }
    }

    @JsonProperty("sMifinVerificationId")
    private String mifinVerificationId;

    @JsonProperty("sAgencyCode")
    private String agencyCode;

    @JsonProperty("sAgencyName")
    private String agencyName;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("dtSubDate")
    private Date submissionTime;

    @JsonProperty("dtVeriDate")
    private Date verificationTime;

    @JsonProperty("sTriggerBy")
    private String triggerBy;

    @JsonProperty("sVerifiedBy")
    private String verifiedBy;

    @JsonProperty("sTriggerByName")
    private String triggerByName;

    @JsonProperty("sVerifiedByName")
    private String verifiedByName;

    @Transient
    @JsonProperty("bReInitiate")
    private boolean reinitiate;

       //for topup cases
    @Transient
    @JsonProperty("bWaived")
    private boolean waived;

    public void setMifinVerificationId(String mifinVerificationId) { this.mifinVerificationId = mifinVerificationId; }

    public String getMifinVerificationId() { return mifinVerificationId; }

    public boolean isWaived() {
        return waived;
    }

    public void setWaived(boolean waived) {
        this.waived = waived;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getVerificationTime() {        return verificationTime;    }

    public void setVerificationTime(Date verificationTime) {        this.verificationTime = verificationTime;    }

    public Date getSubmissionTime() {        return submissionTime;    }

    public void setSubmissionTime(Date submissionTime) {        this.submissionTime = submissionTime;    }

    public boolean isReinitiate() {        return reinitiate;    }

    public void setReinitiate(boolean reinitiate) {        this.reinitiate = reinitiate;    }

    public String getTriggerBy() {
        return triggerBy;
    }

    public void setTriggerBy(String triggerBy) {
        this.triggerBy = triggerBy;
    }

    public String getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public String getTriggerByName() {
        return triggerByName;
    }

    public void setTriggerByName(String triggerByName) {
        this.triggerByName = triggerByName;
    }

    public String getVerifiedByName() {
        return verifiedByName;
    }

    public void setVerifiedByName(String verifiedByName) {
        this.verifiedByName = verifiedByName;
    }

    public String getAgencyName() {   return agencyName;   }

    public void setAgencyName(String agencyName) {    this.agencyName = agencyName;  }
}
