package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.BankDetailsMaster;
import com.softcell.gonogo.model.masters.BankMaster;
import com.softcell.gonogo.model.masters.EmployerMaster;
import com.softcell.gonogo.model.masters.PinCodeMaster;
import com.softcell.gonogo.model.security.v2.Branch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0268 on 2/9/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataToUpdate {
    //Branch Manager Name
    @JsonProperty("oBranch")
    private Branch branchV2;

    //for updation For of prospectCode and status
    @JsonProperty("sProspectCode")
    private String prospectCode;

    @JsonProperty("sApplicantCode")
    private String applicantCode;

    @JsonProperty("sIfscCode")
    private String ifscCode;

    @JsonProperty("sMICRCode")
    private String micrCode;

    @JsonProperty("sPassword")
    private String password;

    @JsonProperty("bUpdate")
    boolean update;

    @JsonProperty("oBankDetailMaster")
    BankDetailsMaster bankDetailsMaster;

    @JsonProperty("oPincodeMaster")
    private PinCodeMaster pinCodeMaster;

    @JsonProperty("oCompnyMaster")
    EmployerMaster employmentMaster;

    @JsonProperty("oBankMaster")
    BankMaster bankMaster;

    @JsonProperty("aAppId")
    List<String> appId;
}
