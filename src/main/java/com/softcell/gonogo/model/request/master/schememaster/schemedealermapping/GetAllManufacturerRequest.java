package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class GetAllManufacturerRequest {

    @JsonProperty("sManfcId")
    private String manufacturerId;

    @JsonProperty("oHeader")
    @NotNull(groups = Header.FetchGrp.class)
    @Valid
    private Header header;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GetAllManufacturerRequest [header=");
        builder.append(header);
        builder.append("]");
        return builder.toString();
    }

}
