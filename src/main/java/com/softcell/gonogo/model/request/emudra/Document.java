package com.softcell.gonogo.model.request.emudra;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 19/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Document {
    private String file;
    private String fileName;
    private String location;
    private String fileType;
    @JsonIgnore
    private Object metaData;
}
