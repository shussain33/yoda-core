package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Applicant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by amit on 28/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "insuranceData")
public class InsuranceData {

   /* @JsonProperty("oAppReq")
    ApplicationRequest applicationRequest;*/

   @Id
    public String refId;

    public String institutionId;

    public String product;

    @JsonProperty("sInitiationPoint")
    private String initiationPoint;

    @JsonProperty("sChangedLoanAmount")
    private String changedLoanAmount;

   @JsonProperty("sChangedTenorAmount")
    private String changedTenorAmount;

    @JsonProperty("aApplicant")
    private List<Applicant> applicantList;

    @JsonProperty("icurrentHit")
   private int currentCnt = 0;

    public interface InsertGrp {
    }
}
