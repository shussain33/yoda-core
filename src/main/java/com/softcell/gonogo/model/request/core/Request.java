package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.*;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request implements Serializable {

    @JsonProperty("oApplicant")
    private Applicant applicant;

    @JsonProperty("aCoApplicant")
    private List<CoApplicant> coApplicant;

    @JsonProperty("sPanSearchNo")
    private String panSearchNo;

    @JsonProperty("oApplication")
    private Application application;

    @JsonProperty("sSuspAct")
    private String suspiciousActivity;

    @JsonProperty("oGstDetails")
    private GstDetails gstDetails;

    @JsonProperty("oPersonalDiscussion")
    private PersonalDiscussion personalDiscussion;

    //Added for FiveStar - by Somasekhar on 08/09/2018.
    @JsonProperty("aPersonalDiscussionList")
    private List<PersonalDiscussion> personalDiscussionList;

    /*@JsonProperty("sAgencyCode")
    private String agencyCode;

    @JsonProperty("sStatus")
    private String status;



    @JsonProperty("sCharacterStatus")
    private String characterStatus;

    @JsonProperty("sCapabilityAgency")
    private String capabilityAgency;
*/
    @JsonProperty("oTopUpInfo")
    private TopUpInfo topupInfo;

    @JsonProperty("sDedupeParameterName")
    @NotEmpty
    public String fieldName;

    @JsonProperty("bMifinDedupeReinitiateFlag")
    private boolean mifinDedupeReinitiateFlag;

    @JsonProperty("sMifinDedupeDecision")
    private String mifinDedupeDecision;

    @JsonProperty("sMifinDedupeReason")
    private String mifinReason;

    @JsonProperty("sDedupeParameterValue")
    @NotEmpty
    public String fieldValue;

    @JsonProperty("bDedupeParamsChange")
    public boolean dedupeParamsChange = false;

    //DigiPl Fields Added
    @JsonProperty("sInitiatedBy")
    private String initiatedBy;

    @JsonProperty(value = "bIsCaseInitiated")
    private boolean isCaseInitiated;

    @JsonProperty(value = "sInitiatedByRole")
    private String initiatedByRole;

    public String getInitiatedBy() {
        return initiatedBy;
    }

    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public boolean isCaseInitiated() {
        return isCaseInitiated;
    }

    public void setCaseInitiated(boolean caseInitiated) {
        isCaseInitiated = caseInitiated;
    }

    public String getInitiatedByRole() {
        return initiatedByRole;
    }

    public void setInitiatedByRole(String initiatedByRole) {
        this.initiatedByRole = initiatedByRole;
    }

    public TopUpInfo getTopupInfo() {
        return topupInfo;
    }

    public void setTopupInfo(TopUpInfo topupInfo) {
        this.topupInfo = topupInfo;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public boolean isDedupeParamsChange() {
        return dedupeParamsChange;
    }

    public void setDedupeParamsChange(boolean dedupeParamsChange) {
        this.dedupeParamsChange = dedupeParamsChange;
    }

    public List<PersonalDiscussion> getPersonalDiscussionList() {   return personalDiscussionList;    }

    public void setPersonalDiscussionList(List<PersonalDiscussion> personalDiscussionList) {  this.personalDiscussionList = personalDiscussionList;   }

    public String getSuspiciousActivity() {
        return suspiciousActivity;
    }

    public void setSuspiciousActivity(String suspiciousActivity) {
        this.suspiciousActivity = suspiciousActivity;
    }

    public String getPanSearchNo(){return  panSearchNo;}

    public void setPanSearchNo(String panSearchNo) {this.panSearchNo=panSearchNo;}

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public List<CoApplicant> getCoApplicant() {
        return coApplicant;
    }

    public void setCoApplicant(List<CoApplicant> coApplicant) {
        this.coApplicant = coApplicant;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public GstDetails getGstDetails() {
        return gstDetails;
    }

    public void setGstDetails(GstDetails gstDetails) {
        this.gstDetails = gstDetails;
    }

    public PersonalDiscussion getPersonalDiscussion() {
        return personalDiscussion;
    }

    public void setPersonalDiscussion(PersonalDiscussion personalDiscussion) {
        this.personalDiscussion = personalDiscussion;
    }

    public void setMifinDedupeReinitiateFlag(Boolean flag){this.mifinDedupeReinitiateFlag=flag;}

    public Boolean getMifinDedupeReinitiateFlag(){return  mifinDedupeReinitiateFlag;}

    public String getMifinDedupeDecision() {
        return mifinDedupeDecision;
    }

    public void setMifinDedupeDecision(String mifinDedupeDecision) {
        this.mifinDedupeDecision = mifinDedupeDecision;
    }

    public String getMifinReason() {
        return mifinReason;
    }

    public void setMifinReason(String mifinReason) {
        this.mifinReason = mifinReason;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Request{");
        sb.append("applicant=").append(applicant);
        sb.append(", executeForCoApplicant=").append(coApplicant);
        sb.append(", application=").append(application);
        sb.append(", suspiciousActivity='").append(suspiciousActivity).append('\'');
        sb.append(", gstDetails=").append(gstDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Request request = (Request) o;

        if (applicant != null ? !applicant.equals(request.applicant) : request.applicant != null) return false;
        if (coApplicant != null ? !coApplicant.equals(request.coApplicant) : request.coApplicant != null) return false;
        if (application != null ? !application.equals(request.application) : request.application != null) return false;
        if (suspiciousActivity != null ? !suspiciousActivity.equals(request.suspiciousActivity) : request.suspiciousActivity != null)
            return false;
        return gstDetails != null ? gstDetails.equals(request.gstDetails) : request.gstDetails == null;
    }

    @Override
    public int hashCode() {
        int result = applicant != null ? applicant.hashCode() : 0;
        result = 31 * result + (coApplicant != null ? coApplicant.hashCode() : 0);
        result = 31 * result + (application != null ? application.hashCode() : 0);
        result = 31 * result + (suspiciousActivity != null ? suspiciousActivity.hashCode() : 0);
        result = 31 * result + (gstDetails != null ? gstDetails.hashCode() : 0);
        return result;
    }
}
