package com.softcell.gonogo.model.request.AmbitMifinRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AmbitMifinLog {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sAPI")
    private String api;
}
