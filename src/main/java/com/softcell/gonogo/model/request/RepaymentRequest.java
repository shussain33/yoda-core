package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 26/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RepaymentRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oRepayment")
    @NotNull(groups = {RepaymentRequest.InsertGrp.class})
    private Repayment repayment;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {RepaymentRequest.InsertGrp.class,RepaymentRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
