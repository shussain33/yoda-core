package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.BankingDetails;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by yogeshb on 26/4/17.
 */
public class BankingDetailsRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {BankingDetailsRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("aBanking")
    @NotNull(groups = {BankingDetailsRequest.FetchGrp.class})
    @Size(min = 1, groups = {BankingDetailsRequest.FetchGrp.class})
    @Valid
    private List<BankingDetails> bankingDetails;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public List<BankingDetails> getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(List<BankingDetails> bankingDetails) {
        this.bankingDetails = bankingDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BankingDetailsRequest{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", bankingDetails=").append(bankingDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankingDetailsRequest that = (BankingDetailsRequest) o;

        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (refId != null ? !refId.equals(that.refId) : that.refId != null) return false;
        return bankingDetails != null ? bankingDetails.equals(that.bankingDetails) : that.bankingDetails == null;
    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + (refId != null ? refId.hashCode() : 0);
        result = 31 * result + (bankingDetails != null ? bankingDetails.hashCode() : 0);
        return result;
    }

    public interface FetchGrp {
    }
}
