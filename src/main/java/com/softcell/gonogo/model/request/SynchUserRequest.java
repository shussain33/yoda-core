package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by amit on 10/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SynchUserRequest {

    @NotNull
    @JsonProperty("sInstitutionId")
    private String institutionId;

    @NotNull
    @JsonProperty("oUserResponse")
    LoginServiceResponse userData;
}
