package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0268 on 19/4/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TopUpInfo {

    @JsonProperty("aMiFinOldProspectCode")
    private List<String> miFinOldProspectCode;

    @JsonProperty("sMiFinOldCustomerCode")
    private String miFinOldCustomerCode;

    @JsonProperty("bLoanDetailApi")
    public boolean apiHit;

    @JsonProperty("sExistingDisbDate")
    public String disbursalDate ;

    @JsonProperty("dTotalExposure")
    public double totalExposure ;

    @JsonProperty("dValueToBeConsider")
    private  double valueToBeConsider;

    //principal outstanding sum (sbfc lap top-up-gross)
    @JsonProperty("dPrincipalOutstandingTotal")
    private double principalOutstandingTotal;
}
