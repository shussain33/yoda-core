package com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
public class TrancheDisbursalSchedule {

    @JsonProperty("PRE_EMI_ASCHARGE")
    private String preEmiAsCharge;

    @JsonProperty("DISBURSAL_AMOUNT")
    private String disbAmount;

    @JsonProperty("DISBURSAL_STAGE")
    private String disbStage;

}
