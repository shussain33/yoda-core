package com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class sanctionTermsrequest {

    @JsonProperty("SANCTION_TERMS_CONDITIONS")
    private String sanctionTermsConditions;

    @JsonProperty("SANCTION_TERMS_CONDITIONS_REMARKS")
    private String sanctionTermsConditionsRemarks;

}