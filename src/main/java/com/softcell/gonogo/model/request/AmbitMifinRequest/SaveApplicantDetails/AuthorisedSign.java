package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthorisedSign {

    @JsonProperty("FIRST_NAME_AUTHORISED_SIGN")
    String firstNameAuthorisedSign;

    @JsonProperty("MIDDLE_NAME_AUTHORISED_SIGN")
    String middleNameAuthorisedSign;

    @JsonProperty("LAST_NAME__AUTHORISED_SIGN")
    String lastNameAuthorisedSign;

    @JsonProperty("DESIGNATION__AUTHORISED_SIGN")
    String designationAuthorisedSign;

    @JsonProperty("DINNO__AUTHORISED_SIGN")
    String dinnoAuthorisedSign;

    @JsonProperty("EMAIL__AUTHORISED_SIGN")
    String emailAuthorisedSign;

    @JsonProperty("CONTACTNO__AUTHORISED_SIGN")
    String contactnoAuthorisedSign;

    @JsonProperty("DELEGATION__AUTHORISED_SIGN")
    String delegationAuthorisedSign;

}
