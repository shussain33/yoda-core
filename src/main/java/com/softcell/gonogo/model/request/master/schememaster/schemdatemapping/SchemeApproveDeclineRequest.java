package com.softcell.gonogo.model.request.master.schememaster.schemdatemapping;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author mahesh
 */
public class SchemeApproveDeclineRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sSchemeID")
    @NotEmpty(groups = {SchemeApproveDeclineRequest.FetchGrp.class})
    private String schemeID;

    @JsonProperty("sSchemeStatus")
    @NotEmpty(groups = {SchemeApproveDeclineRequest.FetchGrp.class})
    private String schemeStatus;

    @JsonProperty("sCheckerId")
    @NotEmpty(groups = {SchemeApproveDeclineRequest.FetchGrp.class})
    private String checkerId;

    @JsonProperty("dtCheckDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date checkDate;

    @JsonProperty("sUserRole")
    @NotEmpty(groups = {SchemeApproveDeclineRequest.FetchGrp.class})
    private String userRole;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeStatus() {
        return schemeStatus;
    }

    public void setSchemeStatus(String schemeStatus) {
        this.schemeStatus = schemeStatus;
    }

    public String getCheckerId() {
        return checkerId;
    }

    public void setCheckerId(String checkerId) {
        this.checkerId = checkerId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchemeApproveDeclineRequest [header=");
        builder.append(header);
        builder.append(", schemeID=");
        builder.append(schemeID);
        builder.append(", schemeStatus=");
        builder.append(schemeStatus);
        builder.append(", checkerId=");
        builder.append(checkerId);
        builder.append(", checkDate=");
        builder.append(checkDate);
        builder.append(", userRole=");
        builder.append(userRole);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp {
    }

}
