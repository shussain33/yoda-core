/**
 * yogeshb4:22:41 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
public class SchemeMetadataRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchWithDealerCriteriaGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sModelNo")
    @NotEmpty(groups = {SchemeMetadataRequest.FetchGrp.class})
    private String modelNo;

    @JsonProperty("sCatDsc")
    @NotEmpty(groups = {SchemeMetadataRequest.FetchGrp.class})
    private String catgDesc;

    @JsonProperty("sMfrDscr")
    @NotEmpty(groups = {SchemeMetadataRequest.FetchGrp.class})
    private String manufacturerDesc;

    @JsonProperty("sMake")
    @NotEmpty(groups = {SchemeMetadataRequest.FetchGrp.class})
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getCatgDesc() {
        return catgDesc;
    }

    public void setCatgDesc(String catgDesc) {
        this.catgDesc = catgDesc;
    }

    public String getManufacturerDesc() {
        return manufacturerDesc;
    }

    public void setManufacturerDesc(String manufacturerDesc) {
        this.manufacturerDesc = manufacturerDesc;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchemeMetadataRequest [header=");
        builder.append(header);
        builder.append(", refID=");
        builder.append(refID);
        builder.append(", modelNo=");
        builder.append(modelNo);
        builder.append(", catgDesc=");
        builder.append(catgDesc);
        builder.append(", manufacturerDesc=");
        builder.append(manufacturerDesc);
        builder.append(", make=");
        builder.append(make);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp {
    }
}
