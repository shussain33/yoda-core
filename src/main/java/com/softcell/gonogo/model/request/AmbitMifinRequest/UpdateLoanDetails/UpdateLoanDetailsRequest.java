package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateLoanDetails;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 21/12/20.
 */
@Data
@Builder
public class UpdateLoanDetailsRequest {

    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authentication;

    @JsonProperty("BASICINFO")
    private LoanDetailsBasicInfo loanDetailsBasicInfo;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;

}
