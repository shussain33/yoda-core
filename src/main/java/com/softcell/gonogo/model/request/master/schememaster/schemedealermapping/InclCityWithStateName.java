package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class InclCityWithStateName {

    @JsonProperty("sCityName")
    private String cityName;

    @JsonProperty("sStateName")
    private String stateName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InclCityWithStateName [cityName=");
        builder.append(cityName);
        builder.append(", stateName=");
        builder.append(stateName);
        builder.append("]");
        return builder.toString();
    }

}
