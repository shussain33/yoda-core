package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by anupamad on 9/10/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TKycLogRequest {
    @NotNull
    @JsonProperty("sKycId")
    private String kycId;

    @NotNull
    @JsonProperty("sAction")
    private String action;

}
