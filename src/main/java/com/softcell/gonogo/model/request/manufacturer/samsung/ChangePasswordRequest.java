package com.softcell.gonogo.model.request.manufacturer.samsung;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

/**
 * @author mahesh
 */
public class ChangePasswordRequest {

    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("sOldPassword")
    private String oldpassword;

    @JsonProperty("sNewPassword")
    private String newPassword;

    @JsonProperty("sConfirmedPassword")
    private String confirmedPassword;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String toString() {
        return "ChangePasswordRequest [userName=" + userName + ", oldpassword=" + oldpassword + ",newPassword=" + newPassword + ",confirmedPassword=" + confirmedPassword + "]";
    }


}
