package com.softcell.gonogo.model.request.manufacturer.samsung;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class SerialNumberApplicableCheckRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.InstDealerProductGrp.class,
            }
    )
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(
            groups = {
                    SerialNumberApplicableCheckRequest.FetchGrp.class
            }
    )
    private String refId;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }


    public interface FetchGrp {
    }

}
