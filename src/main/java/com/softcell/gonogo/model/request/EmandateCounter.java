/**
 * bhuvneshk12:05:17 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author bhuvneshk
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Document(collection = "EmandateCounter")
public class EmandateCounter extends AuditEntity {

    @JsonProperty("sSeq")
    private String sequence;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sSequenceType")
    private String sequenceType;


}
