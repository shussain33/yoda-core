package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg208 on 11/12/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetGngByMobRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {UpdateGNGRequest.InsertGrp.class,UpdateGNGRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("sPhoneNumber")
    private String phoneNumber;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
