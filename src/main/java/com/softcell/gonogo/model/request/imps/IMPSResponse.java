package com.softcell.gonogo.model.request.imps;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.BankingDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 3/11/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IMPSResponse {

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sAccountHolderName")
    private String accountHolderName;

    @JsonProperty("sTimerValue")
    private String timerValue;

    @JsonProperty("oBankingDetails")
    private BankingDetails bankingDetails;

}
