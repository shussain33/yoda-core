package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.InvoiceDetails;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
public class InvoiceDetailsRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {InvoiceDetailsRequest.UpdateOrDeleteGrp.class})
    private String refID;

    @JsonProperty("oInvDtls")
    @NotNull(groups = {InvoiceDetailsRequest.UpdateOrDeleteGrp.class})
    @Valid
    private InvoiceDetails invoiceDetails;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public InvoiceDetails getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(InvoiceDetails invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InvoiceDetailsRequest [header=");
        builder.append(header);
        builder.append(", refID=");
        builder.append(refID);
        builder.append(", invoiceDetails=");
        builder.append(invoiceDetails);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result
                + ((invoiceDetails == null) ? 0 : invoiceDetails.hashCode());
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof InvoiceDetailsRequest))
            return false;
        InvoiceDetailsRequest other = (InvoiceDetailsRequest) obj;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (invoiceDetails == null) {
            if (other.invoiceDetails != null)
                return false;
        } else if (!invoiceDetails.equals(other.invoiceDetails))
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        return true;
    }

    public interface UpdateOrDeleteGrp {
    }

}
