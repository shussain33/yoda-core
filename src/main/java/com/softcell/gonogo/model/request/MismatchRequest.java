package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Mismatch;
import com.softcell.gonogo.model.request.core.Header;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by monu on 17/9/20.
 */
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Ignore
    @Document(collection = "mismatchInfo")
    public class MismatchRequest {

        @JsonProperty("oHeader")
        @Valid
        @NotNull(groups = {Header.FetchGrp.class})
        private Header header;

        @Id
        @JsonProperty("sRefID")
        private String refID;

        @JsonProperty("aMismatchList")
        private List<Mismatch> mismatchList;

}
