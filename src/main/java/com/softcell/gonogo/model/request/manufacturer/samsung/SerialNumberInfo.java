package com.softcell.gonogo.model.request.manufacturer.samsung;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author mahesh
 */
@Document(collection = "serialNumberInfo")
public class SerialNumberInfo extends AuditEntity {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("eProduct")
    private Product product;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("dtDateTime")
    private Date dateTime = new Date();

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sMaterialName")
    private String materialName;

    @JsonProperty("sProductCode")
    private String productCode;

    @JsonProperty("sStoreCode")
    private String storeCode;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sOriginalResponse")
    private String originalResponse;

    @JsonProperty("sMpn")
    private String mpn;

    @JsonProperty("lStoreAppleId")
    private long storeAppleId;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("iTenure")
    private int tenure;

    @JsonProperty("sScheme")
    private String scheme;

    @JsonProperty("sChannelCode")
    private String channelCode;

    /**
     * oppoDealerId is only used in Oppo
     */
    @JsonProperty("sOppoDealerId")
    private String oppoDealerId;

    /**
     * distributorId is used to log Apple DistributorId
     */
    @JsonProperty("sDistributorId")
    private String distributorId;

    @JsonProperty("sCustMsg")
    private String customMsg;

    /**
     * Mandatory field is only used for Google
     */
    @JsonProperty("sRDSCode")
    private String rdsCode;

    @JsonProperty("sMaterialCode")
    private String materialCode;

    /**
     * Mandatory field for Voltas
     */
    @JsonProperty(value = "sSRNumber")
    private String serviceRequestNumber;

    @JsonProperty("sCustomerName")
    private String customerName;

    @JsonProperty("sProductCategory")
    private String productCategory;

    @JsonProperty("sUnitStatus")
    private String unitStatus;

    @JsonProperty("sPurchaseDate")
    private String purchaseDate;

    @JsonProperty("sAlternateContact")
    private String alternateContact;

    @JsonProperty("sCustomerEmail")
    private String customerEmail;

    @JsonProperty("sArea")
    private String area;

    @JsonProperty("sBranchName")
    private String branchName;

    @JsonProperty("sCallType")
    private String callType;

    @JsonProperty("sClosedDate")
    private String closedDate;

    @JsonProperty("sFranchisee")
    private String franchisee;

    @JsonProperty("sLotNumber")
    private String lotNumber;

    @JsonProperty("sOpenDate")
    private String openDate;

    @JsonProperty("sSRStatus")
    private String serviceRequestStatus;

    @JsonProperty("sPostalCode")
    private String postalCode;

    @JsonProperty("sOutput")
    private String output;

    /**
     *  used for LG
     */
    @JsonProperty("sDealId")
    private String dealId;




    public static Builder builder() {
        return new Builder();
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getOriginalResponse() {
        return originalResponse;
    }

    public void setOriginalResponse(String originalResponse) {
        this.originalResponse = originalResponse;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "dd-MM-yyyy hh:mm a")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public long getStoreAppleId() {
        return storeAppleId;
    }

    public void setStoreAppleId(long storeAppleId) {
        this.storeAppleId = storeAppleId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getTenure() {
        return tenure;
    }

    public void setTenure(int tenure) {
        this.tenure = tenure;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getOppoDealerId() {
        return oppoDealerId;
    }

    public void setOppoDealerId(String oppoDealerId) {
        this.oppoDealerId = oppoDealerId;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getCustomMsg() {
        return customMsg;
    }

    public void setCustomMsg(String customMsg) {
        this.customMsg = customMsg;
    }

    public String getRdsCode() {
        return rdsCode;
    }

    public void setRdsCode(String rdsCode) {
        this.rdsCode = rdsCode;
    }

    public String getServiceRequestNumber() { return serviceRequestNumber; }

    public void setServiceRequestNumber(String serviceRequestNumber) { this.serviceRequestNumber = serviceRequestNumber; }

    public String getMaterialCode() { return materialCode; }

    public void setMaterialCode(String materialCode) { this.materialCode = materialCode; }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getUnitStatus() {
        return unitStatus;
    }

    public void setUnitStatus(String unitStatus) {
        this.unitStatus = unitStatus;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getAlternateContact() {
        return alternateContact;
    }

    public void setAlternateContact(String alternateContact) {
        this.alternateContact = alternateContact;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(String closedDate) {
        this.closedDate = closedDate;
    }

    public String getFranchisee() {
        return franchisee;
    }

    public void setFranchisee(String franchisee) {
        this.franchisee = franchisee;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getServiceRequestStatus() {
        return serviceRequestStatus;
    }

    public void setServiceRequestStatus(String serviceRequestStatus) {
        this.serviceRequestStatus = serviceRequestStatus;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }



    public static class Builder {
        private SerialNumberInfo serialNumberInfo = new SerialNumberInfo();

        public SerialNumberInfo build() {
            return serialNumberInfo;
        }

        public Builder institutionId(String institutionId) {
            this.serialNumberInfo.institutionId = institutionId;
            return this;
        }

        public Builder product(Product product) {
            this.serialNumberInfo.product = product;
            return this;
        }

        public Builder refID(String refID) {
            this.serialNumberInfo.refID = refID;
            return this;
        }

        public Builder vendor(String vendor) {
            this.serialNumberInfo.vendor = vendor;
            return this;
        }


        public Builder skuCode(String skuCode) {
            this.serialNumberInfo.skuCode = skuCode;
            return this;
        }

        public Builder serialNumber(String serialNumber) {
            this.serialNumberInfo.serialNumber = serialNumber;
            return this;
        }

        public Builder status(String status) {
            this.serialNumberInfo.status = status;
            return this;
        }

        public Builder originalResponse(String originalResponse) {
            this.serialNumberInfo.originalResponse = originalResponse;
            return this;
        }

        public Builder imeiNumber(String imeiNumber) {
            this.serialNumberInfo.imeiNumber = imeiNumber;
            return this;
        }

        public Builder productCode(String productCode) {
            this.serialNumberInfo.productCode = productCode;
            return this;
        }

        public Builder storeCode(String storeCode) {
            this.serialNumberInfo.storeCode = storeCode;
            return this;
        }

        public Builder materialName(String materialName) {
            this.serialNumberInfo.materialName = materialName;
            return this;
        }

        public Builder mpn(String mpn) {
            this.serialNumberInfo.mpn = mpn;
            return this;
        }

        public Builder storeAppleId(long storeAppleId) {
            this.serialNumberInfo.storeAppleId = storeAppleId;
            return this;
        }

        public Builder bankName(String bankName) {
            this.serialNumberInfo.bankName = bankName;
            return this;
        }

        public Builder tenure(int tenure) {
            this.serialNumberInfo.tenure = tenure;
            return this;
        }

        public Builder dealerId(String dealerId) {
            this.serialNumberInfo.dealerId = dealerId;
            return this;
        }

        public Builder scheme(String scheme) {
            this.serialNumberInfo.scheme = scheme;
            return this;
        }

        public Builder channelCode(String channelCode) {
            this.serialNumberInfo.channelCode = channelCode;
            return this;
        }

        public Builder oppoDealerId(String oppoDealerId) {
            this.serialNumberInfo.oppoDealerId = oppoDealerId;
            return this;
        }

        public Builder distributorId(String distributorId) {
            this.serialNumberInfo.distributorId = distributorId;
            return this;
        }

        public Builder customMsg(String customMsg) {
            this.serialNumberInfo.customMsg = customMsg;
            return this;
        }

        public Builder rdsCode(String rdsCode) {
            this.serialNumberInfo.rdsCode = rdsCode;
            return this;
        }

        public Builder serviceRequestNumber(String serviceRequestNumber) {
            this.serialNumberInfo.serviceRequestNumber = serviceRequestNumber;
            return this;
        }

        public Builder materialCode(String materialCode) {
            this.serialNumberInfo.materialCode = materialCode;
            return this;
        }

        public Builder customerName(String customerName) {
            this.serialNumberInfo.customerName = customerName;
            return this;
        }

        public Builder productCategory(String productCategory) {
            this.serialNumberInfo.productCategory = productCategory;
            return this;
        }

        public Builder unitStatus(String unitStatus) {
            this.serialNumberInfo.unitStatus = unitStatus;
            return this;
        }

        public Builder purchaseDate(String purchaseDate) {
            this.serialNumberInfo.purchaseDate = purchaseDate;
            return this;
        }

        public Builder alternateContact(String alternateContact) {
            this.serialNumberInfo.alternateContact = alternateContact;
            return this;
        }

        public Builder customerEmail(String customerEmail) {
            this.serialNumberInfo.customerEmail = customerEmail;
            return this;
        }

        public Builder area(String area) {
            this.serialNumberInfo.area = area;
            return this;
        }

        public Builder branchName(String branchName) {
            this.serialNumberInfo.branchName = branchName;
            return this;
        }

        public Builder callType(String callType) {
            this.serialNumberInfo.callType = callType;
            return this;
        }

        public Builder closedDate(String closedDate) {
            this.serialNumberInfo.closedDate = closedDate;
            return this;
        }

        public Builder franchisee(String franchisee) {
            this.serialNumberInfo.franchisee = franchisee;
            return this;
        }

        public Builder lotNumber(String lotNumber) {
            this.serialNumberInfo.lotNumber = lotNumber;
            return this;
        }

        public Builder openDate(String openDate) {
            this.serialNumberInfo.openDate = openDate;
            return this;
        }

        public Builder serviceRequestStatus(String serviceRequestStatus) {
            this.serialNumberInfo.serviceRequestStatus = serviceRequestStatus;
            return this;
        }

        public Builder postalCode(String postalCode) {
            this.serialNumberInfo.postalCode = postalCode;
            return this;
        }

        public Builder output(String output) {
            this.serialNumberInfo.output = output;
            return this;
        }

        public Builder dealId(String dealId) {
            this.serialNumberInfo.dealId = dealId;
            return this;
        }


    }

}
