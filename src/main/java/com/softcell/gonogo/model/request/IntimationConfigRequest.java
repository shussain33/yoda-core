package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.configuration.admin.ActionGroup;
import com.softcell.gonogo.model.configuration.admin.IntimationGroup;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 9/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IntimationConfigRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sProduct")
    @Valid
    private String productName;

    @JsonProperty("oActionConfig")
    private ActionGroup config;

    @JsonProperty("oGroups")
    private IntimationGroup group;
}
