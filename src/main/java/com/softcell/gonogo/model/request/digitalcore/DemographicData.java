package com.softcell.gonogo.model.request.digitalcore;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0302 on 2/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DemographicData {

    @JsonProperty("aAddress")
    private List<DemographicAddress> address;

}
