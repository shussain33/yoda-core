package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssguser on 21/12/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveVerificationBasicInfo {

    @JsonProperty("VERIFICATIONID")
    String verificationId;

    @JsonProperty("PROSPECTCODE")
    String prospectCode;

    @JsonProperty("VERIFICATIONTYPE")
    String verificationType;

    @JsonProperty("VERIFICATIONPOINT_RCU_STATUS")
    String verificationRCUStatus;

    @JsonProperty("APPLICANTCODE")
    String applicantCode;

    @JsonProperty("VERIFICATIONSTATUS")
    String verificationStatus;

    @JsonProperty("INITAITEDDATE")
    String initiatedDate;

    @JsonProperty("AGENCYNAME")
    String agencyName;

    @JsonProperty("AGENTNAME")
    String agentName;

    @JsonProperty("PERSONCONTACTED")
    String personContacted;

    @JsonProperty("NOOFATTEMPTS")
    String noOfAttempts;

    @JsonProperty("VERIFICATIONRESULT")
    String verificationResult;

    @JsonProperty("COMPLETIONDATE")
    String completionDate;

    @JsonProperty("SUPERVISOR")
    String supervisor;

    @JsonProperty("REMARKS")
    String remarks;

    @JsonProperty("CONTACT_PERSON")
    String contactPerson;

    @JsonProperty("MOBNO")
    String mobNo;

    @JsonProperty("BUSINESSADDRESS")
    String businessAddress;
}
