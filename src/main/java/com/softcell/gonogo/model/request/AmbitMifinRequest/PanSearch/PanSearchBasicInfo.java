package com.softcell.gonogo.model.request.AmbitMifinRequest.PanSearch;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssguser on 1/12/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PanSearchBasicInfo {

    @JsonProperty("PANCARD")
    private String pan;

    @JsonProperty("ENTITY_TYPE")
    private String entityType;

}
