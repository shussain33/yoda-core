package com.softcell.gonogo.model.request.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by archana on 7/11/17.
 */
public class CaseAllocationRequest {

    @JsonProperty("sCaseIds")
    @NotNull
    private List<String> caseIds;

    @JsonProperty("sAllocateToUserId")
    private String allocateToUserId;

    @JsonProperty("sAllocateFromUserId")
    private String allocateFromUserId;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.AppSourceGrp.class})
    @Valid
    private Header header;

    public List<String> getCaseIds() {
        return caseIds;
    }

    public void setCaseIds(List<String> caseIds) {
        this.caseIds = caseIds;
    }

    public String getAllocateToUserId() {
        return allocateToUserId;
    }

    public void setAllocateToUserId(String allocateToUserId) {
        this.allocateToUserId = allocateToUserId;
    }

    public String getAllocateFromUserId() {
        return allocateFromUserId;
    }

    public void setAllocateFromUserId(String allocateFromUserId) {
        this.allocateFromUserId = allocateFromUserId;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }
}
