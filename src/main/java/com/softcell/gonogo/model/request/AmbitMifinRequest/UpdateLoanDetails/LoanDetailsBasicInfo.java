package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateLoanDetails;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 21/12/20.
 */
@Data
@Builder
public class LoanDetailsBasicInfo {

    @JsonProperty("APP_FORM_NUMBER")
    String appFormNumber;

    @JsonProperty("SCHEME")
    String scheme;

    @JsonProperty("INSTL_TYPE")
    String  installmentType;

    @JsonProperty("INCOME_METHOD")
    String incomeMethod;

    @JsonProperty("INS_RATE")
    String instRate;

    @JsonProperty("TYPE_OF_LOAN")
    String loanType;

    @JsonProperty("DUE_DAY")
    String dueDay;

    @JsonProperty("SANCTION_AMOUNT")
    String sanctionAmt;

    @JsonProperty("LOAN_AMOUNT")
    String loanAmt;

    @JsonProperty("REPAYMENT_MODE")
    String repaymentMode;

    @JsonProperty("PURPOSE_SUB_SCHEME")
    String purposeSubScheme;

    @JsonProperty("PROMO_SCHEME_CUSTOMER_TYPE")
    String promoSchemeCustomerType;

    @JsonProperty("MATURITY_DT")
    String maturityDate;

    @JsonProperty("INSTL_NUMBER")
    String instlNumber;

    @JsonProperty("CHANNEL")
    String channel;

    @JsonProperty("AGENT")
    String agent;

    @JsonProperty("RELATIONSHIP_MANAGER")
    String realtionshipManager;

    @JsonProperty("CHANNEL_TYPE")
    String channelType;

    @JsonProperty("MAX_LTV")
    String maxLtv;

    @JsonProperty("PROSPECT_CODE")
    String prospectCode;

    @JsonProperty("TARGET_IRR")
    String targetIRR;

    @JsonProperty("TENOR_MONTHS")
    String tenorMonths;

    @JsonProperty("TENOR_DAYS")
    String tenorDays;

    @JsonProperty("ADV_INST")
    String advEmi;

    @JsonProperty("REPAYMENT_FREQUENCY")
    String repaymentFrequency;

    @JsonProperty("SEC_SOURCING_CHANNEL_TYPE")
    String secSourcingChannelType;

    @JsonProperty("SEC_SOURCING_CHANNEL")
    String secSourcingChannel;

    @JsonProperty("SEC_SOURCING_AGENT")
    String secSourcingAgent;

    @JsonProperty("ADD_TO_LOAN_AMT")
    String addToLoanAmt;

}
