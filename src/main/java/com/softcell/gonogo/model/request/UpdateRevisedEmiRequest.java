package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by mahesh on 31/5/17.
 */
public class UpdateRevisedEmiRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {UpdateRevisedEmiRequest.FetchGrp.class} )
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {UpdateRevisedEmiRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("dRevisedEmi")
    @DecimalMin(value = "0.0", groups = {UpdateRevisedEmiRequest.FetchGrp.class})
    private double revisedEmi;

    @JsonProperty("dRevisedLoanAmt")
    @DecimalMin(value = "0.0", groups = {UpdateRevisedEmiRequest.FetchGrp.class})
    private double revisedLoanAmt;

    @JsonProperty("dtEmiStartDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    @NotNull(groups = {UpdateRevisedEmiRequest.AddOnGrp.class})
    private Date emiStartDate;

    @JsonProperty("dtEmiEndDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    @NotNull(groups = {UpdateRevisedEmiRequest.AddOnGrp.class})
    private Date emiEndDate;

    @JsonProperty("sDownPaymentReceiptNo")
    private String downPaymentReceiptNo;

    @JsonProperty("dtDownPaymentDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date downPaymentDate;

    @JsonProperty("dDownPaymentAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double downPaymentAmt;

    @JsonProperty("dNetDisbursalAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double netDisbursalAmount;

    @JsonProperty("dUmfc")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double umfc;

    @JsonProperty("dTotalRecvAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double totalRecvAmt;

    @JsonProperty("dTotalRecDwnAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double totalRecDwnAmt;

    @JsonProperty("dLoanAccAgrAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double loanAccAgrAmt;

    @JsonProperty("dFinanceAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double financeAmount;

    @JsonProperty("dAdvEmi")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double advanceEmi;

    @JsonProperty("dLoanDisbursalAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double loanDisbursalAmt;

    public double getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(double financeAmount) {
        this.financeAmount = financeAmount;
    }

    public double getAdvanceEmi() {
        return advanceEmi;
    }

    public void setAdvanceEmi(double advanceEmi) {
        this.advanceEmi = advanceEmi;
    }

    public Date getEmiStartDate() {
        return emiStartDate;
    }

    public void setEmiStartDate(Date emiStartDate) {
        this.emiStartDate = emiStartDate;
    }

    public Date getEmiEndDate() {
        return emiEndDate;
    }

    public void setEmiEndDate(Date emiEndDate) {
        this.emiEndDate = emiEndDate;
    }

    public String getDownPaymentReceiptNo() {
        return downPaymentReceiptNo;
    }

    public void setDownPaymentReceiptNo(String downPaymentReceiptNo) {
        this.downPaymentReceiptNo = downPaymentReceiptNo;
    }

    public Date getDownPaymentDate() {
        return downPaymentDate;
    }

    public void setDownPaymentDate(Date downPaymentDate) {
        this.downPaymentDate = downPaymentDate;
    }

    public double getDownPaymentAmt() {
        return downPaymentAmt;
    }

    public void setDownPaymentAmt(double downPaymentAmt) {
        this.downPaymentAmt = downPaymentAmt;
    }

    public interface FetchGrp{

    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public double getRevisedEmi() {
        return revisedEmi;
    }

    public double getRevisedLoanAmt() {
        return revisedLoanAmt;
    }

    public void setRevisedLoanAmt(double revisedLoanAmt) {
        this.revisedLoanAmt = revisedLoanAmt;
    }

    public void setRevisedEmi(double revisedEmi) {
        this.revisedEmi = revisedEmi;
    }

    public double getNetDisbursalAmount() {
        return netDisbursalAmount;
    }

    public void setNetDisbursalAmount(double netDisbursalAmount) {
        this.netDisbursalAmount = netDisbursalAmount;
    }

    public double getUmfc() {
        return umfc;
    }

    public void setUmfc(double umfc) {
        this.umfc = umfc;
    }

    public double getTotalRecvAmt() {
        return totalRecvAmt;
    }

    public void setTotalRecvAmt(double totalRecvAmt) {
        this.totalRecvAmt = totalRecvAmt;
    }

    public double getTotalRecDwnAmt() {
        return totalRecDwnAmt;
    }

    public void setTotalRecDwnAmt(double totalRecDwnAmt) {
        this.totalRecDwnAmt = totalRecDwnAmt;
    }

    public double getLoanAccAgrAmt() {
        return loanAccAgrAmt;
    }

    public void setLoanAccAgrAmt(double loanAccAgrAmt) {
        this.loanAccAgrAmt = loanAccAgrAmt;
    }

    public double getLoanDisbursalAmt() { return loanDisbursalAmt; }

    public void setLoanDisbursalAmt(double loanDisbursalAmt) { this.loanDisbursalAmt = loanDisbursalAmt; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdateRevisedEmiRequest{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", revisedEmi=").append(revisedEmi);
        sb.append(", revisedLoanAmt=").append(revisedLoanAmt);
        sb.append(", emiStartDate=").append(emiStartDate);
        sb.append(", emiEndDate=").append(emiEndDate);
        sb.append(", downPaymentReceiptNo=").append(downPaymentReceiptNo);
        sb.append(", downPaymentDate=").append(downPaymentDate);
        sb.append(", downPaymentAmt=").append(downPaymentAmt);
        sb.append(", netDisbursalAmount=").append(netDisbursalAmount);
        sb.append(", loanAccAgrAmt=").append(loanAccAgrAmt);
        sb.append(", financeAmount=").append(financeAmount);
        sb.append(", advanceEmi=").append(advanceEmi);
        sb.append(", loanDisbursalAmt=").append(loanDisbursalAmt);
        sb.append('}');
        return sb.toString();
    }

    public interface AddOnGrp{

    }
}
