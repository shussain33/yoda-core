package com.softcell.gonogo.model.request.emudra;

import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogeshb on 18/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ESignResponse {
    private String status;
    private List<SignedDocument> signedDocuments;
    private ThirdPartyException error;
}
