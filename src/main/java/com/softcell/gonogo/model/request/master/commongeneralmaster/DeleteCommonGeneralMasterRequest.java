package com.softcell.gonogo.model.request.master.commongeneralmaster;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by mahesh on 15/5/17.
 */
public class DeleteCommonGeneralMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("aUniqueId")
    @NotEmpty(groups = {DeleteCommonGeneralMasterRequest.FetchGrp.class})
    private List<String> uniqueId;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public List<String> getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(List<String> uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeleteCommonGeneralMasterRequest{");
        sb.append("header=").append(header);
        sb.append(", uniqueId=").append(uniqueId);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {
    }
}
