package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SaveFinancialInfoBasicInfoDetails {

    @JsonProperty("EMPLOYMENTDETAILS_ID")
    String employmentDetailsId;

    @JsonProperty("PROSPECT_CODE")
    String prospectCode;

    @JsonProperty("APPLICANT_CODE")
    String applicantCode;

    @JsonProperty("SECTOR")
    String sector;

    @JsonProperty("INDUSTRY_CLASSIFACTION")
    String industryClassifaction;

    @JsonProperty("YEARS_IN_SERVICE")
    String yearsInService;

    @JsonProperty("TOTAL_EMPLOYEE")
    String totalEmployee;

    @JsonProperty("EXP_INEXISTING_LINE_OFBUSINESS")
    String expInExistingLineOfBusiness;

    @JsonProperty("PURCHASE")
    String purchase;

    @JsonProperty("VALUE_OF_PLANT")
    String valueOfPlant;
}
