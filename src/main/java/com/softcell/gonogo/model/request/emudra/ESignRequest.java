package com.softcell.gonogo.model.request.emudra;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by yogeshb on 11/9/17.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ESignRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    ESignRequest.FetchGrp.class,
                    ESignRequest.EMandateGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotBlank(groups = {ESignRequest.FetchGrp.class,ESignRequest.EMandateGrp.class})
    private String refId;

    @JsonProperty("sAadhaarNumber")
    @NotBlank(groups = {ESignRequest.FetchGrp.class,ESignRequest.EMandateGrp.class})
    private String aadhaarNumber;

    @JsonProperty("sLocation")
    @NotBlank(groups = {ESignRequest.FetchGrp.class,ESignRequest.EMandateGrp.class})
    private String location;

    @JsonProperty("bConsentToESign")
    private boolean consentToESign;

    @JsonProperty("sEKycChannel")
    @NotNull(groups = {ESignRequest.FetchGrp.class,ESignRequest.EMandateGrp.class})
    private EKycChannel ekycChannel;

    @JsonProperty("sKycResXml")
    @NotBlank(groups = {ESignRequest.FetchGrp.class,ESignRequest.EMandateGrp.class})
    private String kycResponseXml;

    @JsonProperty("aDocNumber")
    @NotNull(groups = {ESignRequest.FetchGrp.class})
    @Size(min = 1, groups = {ESignRequest.FetchGrp.class})
    private Set<String> docNumberList;

    public interface FetchGrp {
    }

    public interface EMandateGrp{}
}
