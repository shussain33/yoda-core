package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Has flags for various external services whether to call those.
 * Created by archana on 17/10/17.
 */
public class ExternalServiceCalls {

    @JsonProperty("bCallSaathi")
    private boolean callSaathi;

    @JsonProperty("bPushToMifin")
    private boolean pushToMifin;

    @JsonProperty("bPushToLms")
    private boolean pushToLms;

    @JsonProperty("sLmsErrorMsg")
    private String lmsMsg;

    public boolean isCallSaathi() {
        return callSaathi;
    }

    public void setCallSaathi(boolean callSaathi) {
        this.callSaathi = callSaathi;
    }

    public boolean isPushToMifin() {
        return pushToMifin;
    }

    public void setPushToMifin(boolean pushToMifin) {
        this.pushToMifin = pushToMifin;
    }

    public String getLmsMsg() {return lmsMsg;}

    public void setLmsMsg(String lmsMsg) {this.lmsMsg = lmsMsg;}

    public boolean isPushToLms() {return pushToLms;}

    public void setPushToLms(boolean pushToLms) {this.pushToLms = pushToLms;}

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExternalServiceCalls{");
        sb.append("callSaathi=").append(callSaathi);
        sb.append('}');
        return sb.toString();
    }
}
