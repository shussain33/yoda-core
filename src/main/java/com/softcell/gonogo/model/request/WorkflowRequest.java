package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.configuration.admin.WorkflowMaster;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 21/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkflowRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.InsertGrp.class, Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oWorkflowMaster")
    WorkflowMaster workflowMaster;
}