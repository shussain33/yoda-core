package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.ops.SanctionConditions;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 28/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SanctionConditionRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class, Header.InsertGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oSanctionConditions")
    @NotNull(groups = {SanctionConditions.InsertGrp.class})
    private SanctionConditions sanctionConditions;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {SanctionConditions.InsertGrp.class,SanctionConditions.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
