package com.softcell.gonogo.model.request.AmbitMifinRequest.SavePersonalInsurance;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SavePersonalInsuranceBasicInfo {

    @JsonProperty("PROSPECT_CODE")
    String prospectCode;

    @JsonProperty("APPLICANT_CODE")
    String applicantCode;

    @JsonProperty("PERSONAL_INSURANCE_ID")
    String personalInsuranceId;

    @JsonProperty("INSURANCE_TYPE")
    String insuranceType;

    @JsonProperty("COMPANY_NAME")
    String companyName;

    @JsonProperty("POLICY_NAME")
    String policyName;

    @JsonProperty("POLICY_NUMBER")
    String policyNumber;

    @JsonProperty("INSURED_AMOUNT")
    String insuredAmount;

    @JsonProperty("PAYMENT_OPTION")
    String paymentOption;

    @JsonProperty("PREMIUM_AMOUNT")
    String premiumAmount;

    @JsonProperty("ST_ON_PREMIUM_AMOUNT")
    String stOnPremiumAmount;

    @JsonProperty("POLICY_STARTDATE")
    String policyStartDate;

    @JsonProperty("POLICY_ENDDATE")
    String policyEndDate;

    @JsonProperty("NOMINEE_NAME")
    String nomineeName;

    @JsonProperty("NOMINEE_RELATIONSHIP")
    String nomineeRelationship;

    @JsonProperty("DELETE_FLAG")
    String deleteFlag;
}
