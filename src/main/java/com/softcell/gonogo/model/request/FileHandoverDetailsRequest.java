package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author vinodk
 */
public class FileHandoverDetailsRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {FileHandoverDetailsRequest.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {FileHandoverDetailsRequest.FetchGrp.class})
    private String refId;

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FileHandoverDetailsRequest [header=");
        builder.append(header);
        builder.append(", refId=");
        builder.append(refId);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp {
    }

    public interface InsertGrp {
    }
}
