package com.softcell.gonogo.model.request.master.commongeneralmaster;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.CommonGeneralParameterMaster;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 16/5/17.
 */
public class InsertCommonGeneralMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oCommonGeneralParameterMaster")
    @NotNull(groups = {UpdateCommonGeneralMasterRequest.FetchGrp.class})
    @Valid
    private CommonGeneralParameterMaster commonGeneralParameterMaster;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public CommonGeneralParameterMaster getCommonGeneralParameterMaster() {
        return commonGeneralParameterMaster;
    }

    public void setCommonGeneralParameterMaster(CommonGeneralParameterMaster commonGeneralParameterMaster) {
        this.commonGeneralParameterMaster = commonGeneralParameterMaster;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InsertCommonGeneralMasterRequest{");
        sb.append("header=").append(header);
        sb.append(", commonGeneralParameterMaster=").append(commonGeneralParameterMaster);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {

    }
}
