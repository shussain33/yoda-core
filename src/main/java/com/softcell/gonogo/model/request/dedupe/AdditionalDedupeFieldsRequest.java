package com.softcell.gonogo.model.request.dedupe;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogeshb on 11/9/17.
 */
public class AdditionalDedupeFieldsRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = Header.FetchGrp.class)
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotBlank(groups = AdditionalDedupeFieldsRequest.FetchGrp.class)
    private String refId;

    @JsonProperty("dDedupeEmiPaid")
    private double dedupeEmiPaid;

    @JsonProperty("iDedupeTenor")
    private int dedupeTenor;

    @JsonProperty("sDedupeRemark")
    @NotBlank(groups = AdditionalDedupeFieldsRequest.FetchGrp.class)
    private String dedupeRemark;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public double getDedupeEmiPaid() {
        return dedupeEmiPaid;
    }

    public void setDedupeEmiPaid(double dedupeEmiPaid) {
        this.dedupeEmiPaid = dedupeEmiPaid;
    }

    public int getDedupeTenor() {
        return dedupeTenor;
    }

    public void setDedupeTenor(int dedupeTenor) {
        this.dedupeTenor = dedupeTenor;
    }

    public String getDedupeRemark() {
        return dedupeRemark;
    }

    public void setDedupeRemark(String dedupeRemark) {
        this.dedupeRemark = dedupeRemark;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AdditionalDedupeFieldsRequest{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", dedupeEmiPaid=").append(dedupeEmiPaid);
        sb.append(", dedupeTenor=").append(dedupeTenor);
        sb.append(", dedupeRemark='").append(dedupeRemark).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdditionalDedupeFieldsRequest that = (AdditionalDedupeFieldsRequest) o;

        if (Double.compare(that.dedupeEmiPaid, dedupeEmiPaid) != 0) return false;
        if (dedupeTenor != that.dedupeTenor) return false;
        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (refId != null ? !refId.equals(that.refId) : that.refId != null) return false;
        return dedupeRemark != null ? dedupeRemark.equals(that.dedupeRemark) : that.dedupeRemark == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = header != null ? header.hashCode() : 0;
        result = 31 * result + (refId != null ? refId.hashCode() : 0);
        temp = Double.doubleToLongBits(dedupeEmiPaid);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + dedupeTenor;
        result = 31 * result + (dedupeRemark != null ? dedupeRemark.hashCode() : 0);
        return result;
    }

    public interface FetchGrp {
    }
}
