package com.softcell.gonogo.model.request.extendedwarranty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.PaymentMethod;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 25/5/17.
 */
@Document(collection = "extendedWarrantyDetails")
public class ExtendedWarrantyDetails {

    @JsonProperty("oHeader")
    @NotNull(groups = {ExtendedWarrantyDetails.FetchGrp.class,ExtendedWarrantyDetails.UpdateGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {ExtendedWarrantyDetails.FetchGrp.class,ExtendedWarrantyDetails.UpdateGrp.class})
    private String refId;

    @JsonProperty("sGngAssetCat")
    @NotEmpty(groups = {ExtendedWarrantyDetails.FetchGrp.class,ExtendedWarrantyDetails.UpdateGrp.class})
    private String gngAssetCategory;

    @JsonProperty("sEwAssetCat")
    @NotEmpty(groups = {ExtendedWarrantyDetails.FetchGrp.class,ExtendedWarrantyDetails.UpdateGrp.class})
    private String eWAssetCategory;

    /**
     * oem warranty is the manufacturer warranty
     */
    @JsonProperty("dOemWarranty")
    private double  oemWarranty;

    @JsonProperty("dEWWarrantyTenure")
    private double ewWarrantyTenure;

    @JsonProperty("dEWWarrantyPremium")
    private  double ewWarrantyPremium;

    @JsonProperty("sAssetSrNumber")
    @NotEmpty(groups = {ExtendedWarrantyDetails.FetchGrp.class})
    private String assetSerialNumber;

    @JsonProperty("dRevisedTotalEmi")
    private double revisedTotalEmi;

    @JsonProperty("bActiveFlag")
    private boolean activeFlag;

    @JsonProperty("ePaymentMethod")
    private PaymentMethod paymentMethod;

    @JsonProperty("dEWDPAmt")
    private double ewDPAmt;


    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public double getEwDPAmt() {
        return ewDPAmt;
    }

    public void setEwDPAmt(double ewDPAmt) {
        this.ewDPAmt = ewDPAmt;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public double getOemWarranty() {
        return oemWarranty;
    }

    public void setOemWarranty(double oemWarranty) {
        this.oemWarranty = oemWarranty;
    }

    public double getEwWarrantyTenure() {
        return ewWarrantyTenure;
    }

    public void setEwWarrantyTenure(double ewWarrantyTenure) {
        this.ewWarrantyTenure = ewWarrantyTenure;
    }

    public double getEwWarrantyPremium() {
        return ewWarrantyPremium;
    }

    public void setEwWarrantyPremium(double ewWarrantyPremium) {
        this.ewWarrantyPremium = ewWarrantyPremium;
    }

    public String getAssetSerialNumber() {
        return assetSerialNumber;
    }

    public void setAssetSerialNumber(String assetSerialNumber) {
        this.assetSerialNumber = assetSerialNumber;
    }

    public double getRevisedTotalEmi() {
        return revisedTotalEmi;
    }

    public void setRevisedTotalEmi(double revisedTotalEmi) {
        this.revisedTotalEmi = revisedTotalEmi;
    }

    public String getGngAssetCategory() {
        return gngAssetCategory;
    }

    public void setGngAssetCategory(String gngAssetCategory) {
        this.gngAssetCategory = gngAssetCategory;
    }

    public String geteWAssetCategory() {
        return eWAssetCategory;
    }

    public void seteWAssetCategory(String eWAssetCategory) {
        this.eWAssetCategory = eWAssetCategory;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExtendedWarrantyDetails{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", gngAssetCategory='").append(gngAssetCategory).append('\'');
        sb.append(", eWAssetCategory='").append(eWAssetCategory).append('\'');
        sb.append(", oemWarranty=").append(oemWarranty);
        sb.append(", ewWarrantyTenure=").append(ewWarrantyTenure);
        sb.append(", ewWarrantyPremium=").append(ewWarrantyPremium);
        sb.append(", assetSerialNumber='").append(assetSerialNumber).append('\'');
        sb.append(", revisedTotalEmi=").append(revisedTotalEmi);
        sb.append(", activeFlag=").append(activeFlag);
        sb.append(", paymentMethod=").append(paymentMethod);
        sb.append(", ewDPAmt=").append(ewDPAmt);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp{

    }

    public interface UpdateGrp{

    }
}
