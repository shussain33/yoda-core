package com.softcell.gonogo.model.request.emudra;

import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by shyamk on 27/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FTPResponse {
    private String status;
    private ThirdPartyException error;
}
