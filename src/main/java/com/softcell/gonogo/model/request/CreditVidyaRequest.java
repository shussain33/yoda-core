package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 21/10/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditVidyaRequest {

    @JsonProperty("sReqParams")
    String requetsParams;
}
