package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.request.InvoiceDetailsRequest;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author yogeshb
 */
public class InvoiceDetails {
    @JsonProperty("sInvNumber")
    @NotBlank(groups = {InvoiceDetailsRequest.UpdateOrDeleteGrp.class})
    private String invoiceNumber;

    @JsonProperty("sInvType")
    @NotBlank(groups = {InvoiceDetailsRequest.UpdateOrDeleteGrp.class})
    private String invoiceType;

    @JsonProperty("dtInv")
    @NotNull(groups = {InvoiceDetailsRequest.UpdateOrDeleteGrp.class})
    private Date invoiceDate;

    @JsonProperty("oDeliveryAddr")
    @NotNull(groups = {CustomerAddress.FetchGrp.class})
    private CustomerAddress deliveryAddress;

    @JsonProperty("sSerialOrImeiNumber")
    private String serialOrImeiNumber;


    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public CustomerAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(CustomerAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getSerialOrImeiNumber() {
        return serialOrImeiNumber;
    }

    public void setSerialOrImeiNumber(String serialOrImeiNumber) {
        this.serialOrImeiNumber = serialOrImeiNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InvoiceDetails{");
        sb.append("invoiceNumber='").append(invoiceNumber).append('\'');
        sb.append(", invoiceType='").append(invoiceType).append('\'');
        sb.append(", invoiceDate=").append(invoiceDate);
        sb.append(", deliveryAddress=").append(deliveryAddress);
        sb.append(", serialOrImeiNumber='").append(serialOrImeiNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceDetails that = (InvoiceDetails) o;

        if (invoiceNumber != null ? !invoiceNumber.equals(that.invoiceNumber) : that.invoiceNumber != null)
            return false;
        if (invoiceType != null ? !invoiceType.equals(that.invoiceType) : that.invoiceType != null) return false;
        if (invoiceDate != null ? !invoiceDate.equals(that.invoiceDate) : that.invoiceDate != null) return false;
        if (deliveryAddress != null ? !deliveryAddress.equals(that.deliveryAddress) : that.deliveryAddress != null)
            return false;
        return serialOrImeiNumber != null ? serialOrImeiNumber.equals(that.serialOrImeiNumber) : that.serialOrImeiNumber == null;
    }

    @Override
    public int hashCode() {
        int result = invoiceNumber != null ? invoiceNumber.hashCode() : 0;
        result = 31 * result + (invoiceType != null ? invoiceType.hashCode() : 0);
        result = 31 * result + (invoiceDate != null ? invoiceDate.hashCode() : 0);
        result = 31 * result + (deliveryAddress != null ? deliveryAddress.hashCode() : 0);
        result = 31 * result + (serialOrImeiNumber != null ? serialOrImeiNumber.hashCode() : 0);
        return result;
    }
}
