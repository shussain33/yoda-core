package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.topup.ProspectDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExistingLoanInfo
{



    @JsonProperty("CUSTOMERCODE")
    @NotEmpty
    public String customerCode;

    @JsonProperty("PROSPECTLIST")
    @NotEmpty
    public List<ProspectDetails> PROSPECTLIST;

}