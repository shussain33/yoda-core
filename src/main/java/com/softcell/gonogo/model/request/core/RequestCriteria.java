package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.request.CroQueueRequest;
import com.softcell.gonogo.model.security.v2.Hierarchy;

import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * This class represent the criteria based on which we can get the queues of
 * application
 *
 * @author bhuvneshk
 */

public class RequestCriteria {

    /**
     * This value is used to get the branches in request
     */
    @JsonProperty("aBranches")
    private List<String> branches;

    @JsonProperty("aBranchList")
    private List<Integer> branchList;

    /**
     * This value is used to get the products in request
     */
    @JsonProperty("aProducts")
    @Size(min = 1, groups = {CroQueueRequest.FetchGrp.class})
    private Collection<Product> products;

    @JsonProperty("oHierarchy")
    private Hierarchy hierarchy;

    @JsonProperty("cCity")
    private Collection<String> cities;

    @JsonProperty("cStages")
    private Collection<String> stages;

    @JsonProperty("cStatuses")
    private Collection<String> statuses;

    @JsonProperty("cDealerIds")
    private Collection<String> dealerIds;

    @JsonProperty("cDsaIds")
    private Collection<String> dsaIds;

    public List<String> getBranches() {
        return branches;
    }

    public void setBranches(List<String> branches) {
        this.branches = branches;
    }



    public Hierarchy getHierarchy() {
        return hierarchy;
    }

    public void setHierarchy(Hierarchy hierarchy) {
        this.hierarchy = hierarchy;
    }

    public Collection<String> getCities() {
        return cities;
    }

    public void setCities(Collection<String> cities) {
        this.cities = cities;
    }

    public Collection<String> getStages() {
        return stages;
    }

    public void setStages(Collection<String> stages) {
        this.stages = stages;
    }

    public Collection<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(Collection<String> statuses) {
        this.statuses = statuses;
    }

    public Collection<String> getDealerIds() {
        return dealerIds;
    }

    public void setDealerIds(Collection<String> dealerIds) {
        this.dealerIds = dealerIds;
    }

    public Collection<String> getDsaIds() {
        return dsaIds;
    }

    public void setDsaIds(Collection<String> dsaIds) {
        this.dsaIds = dsaIds;
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public List<Integer> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Integer> branchList) {
        this.branchList = branchList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestCriteria{");
        sb.append("branches=").append(branches);
        sb.append(", products=").append(products);
        sb.append(", hierarchy=").append(hierarchy);
        sb.append(", cities=").append(cities);
        sb.append(", stages=").append(stages);
        sb.append(", statuses=").append(statuses);
        sb.append(", dealerIds=").append(dealerIds);
        sb.append(", dsaIds=").append(dsaIds);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestCriteria that = (RequestCriteria) o;
        return Objects.equals(branches, that.branches) &&
                Objects.equals(products, that.products) &&
                Objects.equals(hierarchy, that.hierarchy) &&
                Objects.equals(cities, that.cities) &&
                Objects.equals(stages, that.stages) &&
                Objects.equals(statuses, that.statuses) &&
                Objects.equals(dealerIds, that.dealerIds) &&
                Objects.equals(dsaIds, that.dsaIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(branches, products, hierarchy, cities, stages, statuses, dealerIds, dsaIds);
    }
}
