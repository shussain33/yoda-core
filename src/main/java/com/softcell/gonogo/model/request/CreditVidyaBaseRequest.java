package com.softcell.gonogo.model.request;

import com.softcell.gonogo.service.factory.CustomerDataBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 21/10/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditVidyaBaseRequest {

    String requestType;
    String apiAccessKey;
    String partnerCode;
    String sendEmail;
    CustomerData customerData;
}
