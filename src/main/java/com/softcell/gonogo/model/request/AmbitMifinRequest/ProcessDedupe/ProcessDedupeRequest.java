package com.softcell.gonogo.model.request.AmbitMifinRequest.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcessDedupeRequest {
    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authentication;

    @JsonProperty("BASICINFO")
    private ProcessDedupeBasicInfo processDedupeBasicInfo;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;
}
