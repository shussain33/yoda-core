package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by amit on 10/1/19.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
public class DataMigrationRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("dtStartDate")
    private Date startDate;

    @JsonProperty("dtEndDate")
    private Date endDate;

    @JsonProperty("bUpdateCases")
    private boolean updateCases;
}
