package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.PropertyVisit;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PropertyVisitRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oPropertyVisit")
    @NotNull(groups = {PropertyVisitRequest.InsertGrp.class})
    private PropertyVisit propertyVisit;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {PropertyVisitRequest.InsertGrp.class,PropertyVisitRequest.FetchGrp.class})
    public String refId;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}

