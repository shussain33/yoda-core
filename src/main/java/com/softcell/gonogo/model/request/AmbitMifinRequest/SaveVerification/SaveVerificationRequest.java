package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssguser on 21/12/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveVerificationRequest {

    @JsonProperty("AUTHENTICATION")
    AuthenticationDetails authentication;

    @JsonProperty("QUEST_ANS")
    List<VerificationQuesAns> verificationQuesAnsList;

    @JsonProperty("BASICINFO")
    List<SaveVerificationBasicInfo> saveVerificationBasicInfoList;

    @JsonProperty("institutionId")
    String institutionId;

    @JsonProperty("referenceId")
    String referenceId;

    @JsonProperty("@type")
    String type;

    @JsonProperty("product")
    String product;
}
