package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateStatusDedupeBasicInfo {

    @JsonProperty("APPLICANT_CODE")
    private String applicantCode;

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

    @JsonProperty("CUSTOMER_DEDUPE_STATUS")
    private String customerDedupeStatus;

    @JsonProperty("DEDUPE_DECISION")
    private String dedupeDecision;

    @JsonProperty("REASON")
    private String reason;

    @JsonProperty("REMARKS")
    private String remarks;
}
