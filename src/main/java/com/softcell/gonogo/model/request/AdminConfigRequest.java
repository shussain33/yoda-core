package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 28/5/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminConfigRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sInstId")
    @NotEmpty(groups = {AdminConfigRequest.InsertGrp.class,AdminConfigRequest.FetchGrp.class})
    private String institutionId;

    @JsonProperty("sConfigType")
    private String configType;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }

}
