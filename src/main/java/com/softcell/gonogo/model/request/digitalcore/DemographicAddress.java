package com.softcell.gonogo.model.request.digitalcore;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 2/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DemographicAddress {

    @JsonProperty("sAddressType")
    private String addressType;//RV or OV

    @JsonProperty("sOwnershipType")
    private String ownershipType;//Rented or Owned

    @JsonProperty("iTimeAtAddr")
    private Integer timeAtAddress; //in years

    @JsonProperty("sLine1")
    private String addressLine1;

    @JsonProperty("sLine2")
    private String addressLine2;

    @JsonProperty("sLandMark")
    private String landMark;

    @JsonProperty("iPinCode")
    private Long pin;

    @JsonProperty("sCity")
    private String city;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sNegativeArea")
    private String negativeArea;

    @JsonProperty("sTopSevenCity")
    private String topSevenCity;

    @JsonProperty("sOutOfGeoLimit")
    private String outOfGeoLimit;

    @JsonProperty("sServiceablePin")
    private String serviceablePin;
}
