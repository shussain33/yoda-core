package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 22/3/17.
 */
public class ApplicationStatusLogRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {ApplicationStatusLogRequest.FetchGrp.class})
    private String refId;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApplicationStatusLogRequest{");
        sb.append("header=").append(header);
        sb.append(", refId='").append(refId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {
    }
}
