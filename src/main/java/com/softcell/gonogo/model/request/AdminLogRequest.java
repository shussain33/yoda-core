/**
 * yogeshb3:03:26 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.ActionName;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 *
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminLogRequest {

    @JsonProperty("sRefID")
    @NotEmpty(
            groups = {
                    AdminLogRequest.FetchGrp.class,
                    RawResponseGrp.class
        }
    )
    private String refID;

    @JsonProperty("sVendor")
    @NotEmpty(
            groups = {
                    AdminLogRequest.FetchGrp.class
            }
    )
    private String vendor;

    @JsonProperty("sUserID")
    private String userID;

    @JsonProperty("sPass")
    private String password;

    @JsonProperty("sResponseType")
    @NotNull(groups = {RawResponseGrp.class})
    private ActionName responseType;

    @JsonProperty("sCurrentStageId")
    private String currentStageId;

    @JsonProperty("sCurrentStatus")
    private String applicationStatus;

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AdminLogRequest [refID=");
        builder.append(refID);
        builder.append(", vendor=");
        builder.append(vendor);
        builder.append(", userID=");
        builder.append(userID);
        builder.append(", password=");
        builder.append(password);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        result = prime * result + ((userID == null) ? 0 : userID.hashCode());
        result = prime * result + ((vendor == null) ? 0 : vendor.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof AdminLogRequest))
            return false;
        AdminLogRequest other = (AdminLogRequest) obj;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        if (userID == null) {
            if (other.userID != null)
                return false;
        } else if (!userID.equals(other.userID))
            return false;
        if (vendor == null) {
            if (other.vendor != null)
                return false;
        } else if (!vendor.equals(other.vendor))
            return false;
        return true;
    }

    public interface FetchGrp{}
    public interface RawResponseGrp {}
}
