package com.softcell.gonogo.model.request.digitization;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
public class DigitizeApplicationFormRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid

    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {DigitizeApplicationFormRequest.FetchGrp.class})
    private String refID;

    @JsonProperty("sProductId")
    @NotEmpty(groups = {DigitizeApplicationFormRequest.FetchGrp.class})
    private String productId;

    @JsonProperty("sFileOperationType")
    private String fileOperationType;

    public String getFileOperationType() {
        return fileOperationType;
    }

    public void setFileOperationType(String fileOperationType) {
        this.fileOperationType = fileOperationType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DigitizeApplicationFormRequest{");
        sb.append("header=").append(header);
        sb.append(", refID='").append(refID).append('\'');
        sb.append(", productId='").append(productId).append('\'');
        sb.append(", fileOperationType='").append(fileOperationType).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DigitizeApplicationFormRequest that = (DigitizeApplicationFormRequest) o;

        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (refID != null ? !refID.equals(that.refID) : that.refID != null) return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        return !(fileOperationType != null ? !fileOperationType.equals(that.fileOperationType) : that.fileOperationType != null);

    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + (refID != null ? refID.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (fileOperationType != null ? fileOperationType.hashCode() : 0);
        return result;
    }

    public interface FetchGrp {

    }
}
