package com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
public class InterestRateSchedule {

    @JsonProperty("START_DATE")
    private String startDate;

    @JsonProperty("END_DATE")
    private String endDate;

    @JsonProperty("INTEREST_RATE")
    private String interestRate;

}
