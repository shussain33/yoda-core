package com.softcell.gonogo.model.request.AmbitMifinRequest.NewLoan;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewLoanBasicInfo {

    @JsonProperty("BRANCH")
    private String branch;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

    @JsonProperty("APPLICANT_CODE")
    private String applicantCode;

    @JsonProperty("FNAME")
    private String fname;

    @JsonProperty("MNAME")
    private String mname;

    @JsonProperty("LNAME")
    private String lname;

    @JsonProperty("DATEOFBIRTH")
    private String dateOfBirth;

    @JsonProperty("GENDER")
    private String gender;

    @JsonProperty("RESIHOUSENO")
    private String resiHouseNo;

    @JsonProperty("RESISTREETNO")
    private String resiStreetNo;

    @JsonProperty("RESIAREANAME")
    private String resiAreaName;

    @JsonProperty("RESILANDMARK")
    private String resiLandmark;

    @JsonProperty("RESIDISTRICT")
    private String resiDistrict;

    @JsonProperty("RESICITY")
    private String resiCity;

    @JsonProperty("RESISTATE")
    private String resiState;

    @JsonProperty("RESIMOBILE")
    private String resiMobile;

    @JsonProperty("LOANAMOUNT")
    private String loanAmount;

    @JsonProperty("FORMNO")
    private String formNo;

    @JsonProperty("RESIPINCODE")
    private String resiPincode;

    @JsonProperty("PURPOSE_SUB_SCHEME")
    private String purposesubscheme;

    @JsonProperty("SEC_SOURCING_CHANNEL_TYPE")
    private String secsourcingchanneltype;

    @JsonProperty("SEC_SOURCING_CHANNEL")
    private String secsourcingchannel;

    @JsonProperty("SEC_SOURCING_AGENT")
    private String secsourcingagent;

    @JsonProperty("USER_ID")
    private String userId;

    @JsonProperty("TYPE_OF_LOAN")
    private String typeOfLoan;

    @JsonProperty("YEARSATRESI")
    private String yearAtResi;

    @JsonProperty("MONTHSATRESI")
    private String monthAtResi;

    @JsonProperty("RESIACCOMODATIONTYPE")
    private String resiAccomodationType;

    @JsonProperty("RESIEMAIL")
    private String resiEmail;

    @JsonProperty("PANNO")
    private String panNo;

    @JsonProperty("ADHAAR_AS_KYC")
    private String adhaar;

    @JsonProperty("RELATIONSHIP_MGR")
    private String relationManager;

    @JsonProperty("NO_OF_OTHER_DEPENDENTS")
    private String noOfOtherDependents;

    @JsonProperty("ALIAS")
    private String alias;

    @JsonProperty("TYPE_OF_BUSINESS")
    private String typeOfBusiness;

    @JsonProperty("SEGMENT")
    private String segment;

    @JsonProperty("CKYC_ID")
    private String kycId;

    @JsonProperty("CUSTOMER_CREDIT_INFO")
    private String customerCreditInfo;

    @JsonProperty("CLUSTER_VAL")
    private String clusterVal;

    @JsonProperty("CLUSTER")
    private String cluster;

    @JsonProperty("RESIMOBILE2")
    private String resiMobile2;

    @JsonProperty("PERMHOUSENO")
    private String permHouseNo;

    @JsonProperty("PERMSTREETNO")
    private String permStreetNo;

    @JsonProperty("PERMAREANAME")
    private String permAreaName;

    @JsonProperty("PERMLANDMARK")
    private String permLandmark;

    @JsonProperty("PERMDISTRICT")
    private String permDistrict;

    @JsonProperty("PERMSTATE")
    private String permState;

    @JsonProperty("PERMCITY")
    private String permCity;

    @JsonProperty("PERMPINCODE")
    private String permPincode;

    @JsonProperty("PERMMOBILE")
    private String permMobile;

    @JsonProperty("YEARSATPERM")
    private String yearsAtPerm;

    @JsonProperty("MONTHSATPERM")
    private String monthsAtPerm;

    @JsonProperty("PERMACCOMODATIONTYPE")
    private String permAccomodationType;

    @JsonProperty("OFFHOUSENO")
    private String offHouseNo;

    @JsonProperty("OFFSTREETNO")
    private String offStreetNo;

    @JsonProperty("OFFAREANAME")
    private String offAreaName;

    @JsonProperty("OFFLANDMARK")
    private String offLandmark;

    @JsonProperty("OFFDISTRICT")
    private String offDistrict;

    @JsonProperty("OFFSTATE")
    private String offState;

    @JsonProperty("OFFCITY")
    private String offCity;

    @JsonProperty("OFFPINCODE")
    private String offPincode;

    @JsonProperty("OFFMOBILE")
    private String offMobile;

    @JsonProperty("YEARSATOFFICE")
    private String yearsAtOffice;

    @JsonProperty("MONTHSATOFFICE")
    private String monthsAtOffice;

    @JsonProperty("OFFACCOMODATIONTYPE")
    private String offAccomodationType;

    @JsonProperty("OFFPHONE")
    private String offPhone;

    @JsonProperty("OFFEMAIL")
    private String offMail;

    @JsonProperty("OFFCOMPANYNAME")
    private String offCompanyName;

    @JsonProperty("FATHER_FNAME")
    private String fatherFName;

    @JsonProperty("FATHER_MNAME")
    private String fatherMName;

    @JsonProperty("FATHER_LNAME")
    private String fatherLName;

    @JsonProperty("MARITAL_STATUS")
    private String maritalStatus;

    @JsonProperty("SALUATION")
    private String saluation;

    @JsonProperty("SOCIAL_CATEGORY")
    private String socialCategory;

    @JsonProperty("EDUQULIFICATION")
    private String eduqulification;

    @JsonProperty("NO_OF_DEP_CHILD")
    private String noOfDepChild;

    @JsonProperty("RESIPHONE")
    private String resiPhone;

    @JsonProperty("RESIPHONE2")
    private String resiPhone2;

    @JsonProperty("CUSTENTITYTYPE")
    private String custEntityType;

    @JsonProperty("TENOR")
    private String tenor;

    @JsonProperty("AFFORD_EMI")
    private String affordEMI;

    @JsonProperty("CUST_CATEGORY")
    private String custCategory;

    @JsonProperty("DATE_OF_INCORPORATION")
    private String dateOfIncorporation;

    @JsonProperty("COMPANY_PAN")
    private String companyPan;

    @JsonProperty("RES_GST_APPLICABLE")
    private String resGstApplicable;

    @JsonProperty("OFF_GST_APPLICABLE")
    private String offGstApplicable;

    @JsonProperty("PERM_GST_APPLICABLE")
    private String permGstApplicable;

    @JsonProperty("REGISTRATION_NO")
    private String registrationNo;

    @JsonProperty("KEY_CONTACT_PERSON")
    private String keyContactPerson;

    @JsonProperty("AUTHORIZED_CAPITAL")
    private String authorizedCapital;

    @JsonProperty("TINNO")
    private String tinNo;

    @JsonProperty("CIN")
    private String cin;

    @JsonProperty("CONSTITUTION")
    private String constitution;

    @JsonProperty("GROUP_NAME")
    private String groupName;

    @JsonProperty("RES_YROFSTAYATCURRAREA")
    private String resYrOfStayAtCurrentArea;


    @JsonProperty("OFF_YROFSTAYATCURRAREA")
    private String offYrOfStayAtCurrentArea;

    @JsonProperty("SCHEME")
    private String scheme;

    @JsonProperty("RESGSTINNO")
    private String resGSTINNo;

    @JsonProperty("OFF_GSTINNO")
    private String offGSTINNo;

    @JsonProperty("PERM_GSTINNO")
    private String permGSTINNo;

    @JsonProperty("DESTINATION_ADDRESS")
    private String destinationAddress;

    @JsonProperty("PERMPHONE")
    private String permPhone;

    @JsonProperty("COMPANY_NAME")
    private String companyName;

    @JsonProperty("NAME_AS_PER_AADHAAR")
    private String nameAsPerAadhaar;

    @JsonProperty("CLUSTER_DESCRIPTION")
    private String clusterDescription;

    @JsonProperty("INDIVIDUAL_PERMEMAIL")
    private String individualPermemail;

    @JsonProperty("OFFFAX")
    private String offFax;

    @JsonProperty("OFFEXT")
    private String offExt;

    @JsonProperty("PROMOTIONSCHEME")
    private String promotionScheme;

    @JsonProperty("PROMO_SCHEME_CUSTOMER_TYPE")
    private String promoschemecustomertype;

    @JsonProperty("SOURCING_CHANNEL")
    private String sourcingChannel;

    @JsonProperty("SOURCING_CHANNEL_TYPE")
    private String sourcingChannelType;

    @JsonProperty("SOURCING_AGENT")
    private String sourcingAgent;

    @JsonProperty("MAILING_ADDRESS")
    private String mailingAddress;

    @JsonProperty("PURPOSEOFLOAN")
    private String loanPurpose;

    @JsonProperty("APPLICANT_TYPE")
    private String applicantType;

    @JsonProperty("CUSTCATEGORY")
    private String custCat;

    @JsonProperty("APPLICANT_CATEGORY")
    private String applicantCategory;

    @JsonProperty("SALUTATION")
    private String salutation;

    @JsonProperty("DOB")
    private String dob;

    @JsonProperty("NO_OF_DEPENDENTS")
    private String noOfDependents;

    @JsonProperty("SPOUSE_FNAME")
    private String spouseFname;

    @JsonProperty("SPOUSE_MNAME")
    private String spouseMname;

    @JsonProperty("SPOUSE_LNAME")
    private String spouseLname;

    @JsonProperty("EDUCATIONAL_QUALIFICATION")
    private String educationalQualification;

    @JsonProperty("RELIGION")
    private String religion;

    @JsonProperty("PSL")
    private String psl;

    @JsonProperty("DNS_REQUIRED")
    private String dns_required;

    @JsonProperty("DNS_REASON")
    private String dns_reason;

    @JsonProperty("PREF_TIME_TO_CALL")
    private String pref_time_to_call;

    @JsonProperty("VOTER_ID")
    private String voter_id;

    @JsonProperty("DRIVING_LICENCE_NO")
    private String drivingLicNo;

    @JsonProperty("PASSPORTNO")
    private String passportNo;

    @JsonProperty("TAN_NO")
    private String tan_no;

    @JsonProperty("OTHER_NAME")
    private String other_name;

    @JsonProperty("OTHERS_ID_NUMBER")
    private String others_id_no;

    @JsonProperty("REG_NUMBER")
    private String reg_no;

    @JsonProperty("FIRST_NAME_AUTHORISED_SIGN")
    private String firstName_Authorised_sign;

    @JsonProperty("MIDDLE_NAME_AUTHORISED_SIGN")
    private String middleName_Authorised_sign;

    @JsonProperty("LAST_NAME__AUTHORISED_SIGN")
    private String lastName_Authorised_sign;

    @JsonProperty("DESIGNATION__AUTHORISED_SIGN")
    private String designation_Authorised_sign;

    @JsonProperty("DINNO__AUTHORISED_SIGN")
    private String dinno_Authorised_sign;

    @JsonProperty("EMAIL__AUTHORISED_SIGN")
    private String email_Authorised_sign;

    @JsonProperty("CONTACTNO__AUTHORISED_SIGN")
    private String contactNo_Authorised_sign;

    @JsonProperty("DELEGATION__AUTHORISED_SIGN")
    private String delegation_Authorised_sign;

    @JsonProperty("IS_EXISTING")
    private String is_existing;

    @JsonProperty("PREF_COMMUNICATION_MODE")
    private String pref_comm_mode;

    @JsonProperty("IS_PAN_AVAILABLE")
    private String ispanavailable;

    @JsonProperty("FORM_60_NAME")
    private String form_name;
}