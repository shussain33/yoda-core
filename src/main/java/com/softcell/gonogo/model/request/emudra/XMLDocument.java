package com.softcell.gonogo.model.request.emudra;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by shyamk on 26/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class XMLDocument {

    private String msgId;//UUID
    private String creDtTme;//credit date time 2012-01-18T15:12:39
    private String instructedAgentMmbId;//Borrower IFSC
    private String instructedBankName;//Borrower Bank Name
    private String mndtReqId;//msgId
    private String mndtSvcLvlPrtry;//still not confirmed "CODE"
    private String mndtLclInstrmPrtry;//Still not confirmed but maybe "DEBIT"
    private String mndtFirstColltnDt;//emi start date IST yyyy-MM-dd
    private String mndtFinalColltnDt;//emi end date IST yyyy-MM-dd
    private String maxAmt;//emi Amount
    private String dbtrName;//consumer name
    private String dbtrId;//consumer ref no
    private String dbtrPrtry;//scheme/plan ref no
    private String aadhaarNo;
    private String phNo;
    private String mblNo;
    private String email;
    private String pan;
    private String dbtrAccntId;//borrowers bank acc no
    private String dbtrAccntPrtry;//borrowers bank acc type "SAVINGS"
    private String dbtrAccntMemId;//borrowers bank IFSC code
    private String dbtrAccntMemName;//borrowers bank name

    private String initiatingPartyId;//HDFC00081000006252
    private String instructingAgentMmbId;//HSBC02INDIA
    private String instructingBankName;//HSBC Bank
    private String mndtSeqTp;//RCUR
    private String mdthFreqTp;//MNTH
    private String cdtrName;//TVS Credit Services
    private String cdtrAccId;//HDFC00081000006252
    private String cdtrAgntMemId;//HSBC02INDIA
    private String cdtrAgntMemBankName;//HSBC Bank
    private List<String> errorMessage;

}
