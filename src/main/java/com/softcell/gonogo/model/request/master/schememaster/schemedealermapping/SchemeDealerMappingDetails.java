package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;

/**
 * @author mahesh
 */
@Document(collection = "schemeDealerMappingDetails")
public class SchemeDealerMappingDetails extends AuditEntity {

    @Transient
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sSchID")
    @NotEmpty(groups = {SchemeDealerMappingDetails.FetchGrp.class})
    private String schemeID;

    @JsonProperty("sSchDes")
    private String schemeDesc;

    @JsonProperty("sIncldMnfcr")
    private Set<String> includedMnfcr;

    @Transient
    @JsonProperty("sExcldMnfcr")
    private Set<String> excludedMnfcr;

    @JsonProperty("aoIncldACtgrWithMnf")
    private IncludedAssetCtgWithManfc[] inclAsstCtgWithManf;

    @JsonProperty("aoIncldMakeWithAsstCtgMnfc")
    private IncludedMakeWithCtgMnfcr[] includedMakeWithCtgMnfcr;

    @JsonProperty("aoIncldMdlIdWithInfo")
    private IncludedModelIdWithInformation[] includedModelIdWithInformation;

    @JsonProperty("sIncldState")
    private Set<String> includedState;

    @Transient
    @JsonProperty("sExcldState")
    private Set<String> excludedState;

    @JsonProperty("aoInclCityWithStateName")
    private InclCityWithStateName[] inclCityWithStateName;

    @JsonProperty("aoInclDealerIdWithStateCity")
    private InclDealerIdWithStateCity[] inclDealerIdWithStateCity;

    @JsonProperty("bAllStateCityDealer")
    private boolean allStateCityDealer;

    @JsonProperty("sMkrID")
    private String makerID;

    @JsonProperty("dtMakeDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date makeDate;

    @JsonProperty("bApprvSchmUpdteStatus")
    private boolean approveSchemeUpdateStatus;
    /**
     * This flag are used to maintain most recently updated scheme. i.e. newly
     * scheme status will be "false" and after update it will be "true"
     */
    @JsonProperty("bIsUpdated")
    private boolean updateStatus;
    /**
     * this string are used to store scheme status i.e. approved ,declined.
     */
    @JsonProperty("sSchemeStatus")
    private String schemeStatus;
    /**
     * This status flag are used to inform the user that scheme is already
     * created or not. if not created the status will be false otherwise send
     * the scheme details along with status true.
     */
    @Transient
    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sInstId")
    private String institutionId;

    @Transient
    @NotEmpty(groups = {SchemeDealerMappingDetails.FetchGrp.class})
    @JsonProperty("sUserRole")
    private String userRole;

    /**
     * newly added field for multiple product purpose. ie .cdl ,dpl
     */
    @JsonProperty("sProductFlag")
    private String productFlag;

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public Set<String> getIncludedMnfcr() {
        return includedMnfcr;
    }

    public void setIncludedMnfcr(Set<String> includedMnfcr) {
        this.includedMnfcr = includedMnfcr;
    }

    public Set<String> getExcludedMnfcr() {
        return excludedMnfcr;
    }

    public void setExcludedMnfcr(Set<String> excludedMnfcr) {
        this.excludedMnfcr = excludedMnfcr;
    }

    public IncludedAssetCtgWithManfc[] getInclAsstCtgWithManf() {
        return inclAsstCtgWithManf;
    }

    public void setInclAsstCtgWithManf(
            IncludedAssetCtgWithManfc[] inclAsstCtgWithManf) {
        this.inclAsstCtgWithManf = inclAsstCtgWithManf;
    }

    public IncludedMakeWithCtgMnfcr[] getIncludedMakeWithCtgMnfcr() {
        return includedMakeWithCtgMnfcr;
    }

    public void setIncludedMakeWithCtgMnfcr(
            IncludedMakeWithCtgMnfcr[] includedMakeWithCtgMnfcr) {
        this.includedMakeWithCtgMnfcr = includedMakeWithCtgMnfcr;
    }

    public IncludedModelIdWithInformation[] getIncludedModelIdWithInformation() {
        return includedModelIdWithInformation;
    }

    public void setIncludedModelIdWithInformation(
            IncludedModelIdWithInformation[] includedModelIdWithInformation) {
        this.includedModelIdWithInformation = includedModelIdWithInformation;
    }

    public Set<String> getIncludedState() {
        return includedState;
    }

    public void setIncludedState(Set<String> includedState) {
        this.includedState = includedState;
    }

    public Set<String> getExcludedState() {
        return excludedState;
    }

    public void setExcludedState(Set<String> excludedState) {
        this.excludedState = excludedState;
    }

    public InclCityWithStateName[] getInclCityWithStateName() {
        return inclCityWithStateName;
    }

    public void setInclCityWithStateName(
            InclCityWithStateName[] inclCityWithStateName) {
        this.inclCityWithStateName = inclCityWithStateName;
    }

    public InclDealerIdWithStateCity[] getInclDealerIdWithStateCity() {
        return inclDealerIdWithStateCity;
    }

    public void setInclDealerIdWithStateCity(
            InclDealerIdWithStateCity[] inclDealerIdWithStateCity) {
        this.inclDealerIdWithStateCity = inclDealerIdWithStateCity;
    }

    public boolean isAllStateCityDealer() {
        return allStateCityDealer;
    }

    public void setAllStateCityDealer(boolean allStateCityDealer) {
        this.allStateCityDealer = allStateCityDealer;
    }

    public String getMakerID() {
        return makerID;
    }

    public void setMakerID(String makerID) {
        this.makerID = makerID;
    }

    public Date getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(Date makeDate) {
        this.makeDate = makeDate;
    }

    public boolean isApproveSchemeUpdateStatus() {
        return approveSchemeUpdateStatus;
    }

    public void setApproveSchemeUpdateStatus(boolean approveSchemeUpdateStatus) {
        this.approveSchemeUpdateStatus = approveSchemeUpdateStatus;
    }

    public boolean isUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(boolean updateStatus) {
        this.updateStatus = updateStatus;
    }

    public String getSchemeStatus() {
        return schemeStatus;
    }

    public void setSchemeStatus(String schemeStatus) {
        this.schemeStatus = schemeStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SchemeDealerMappingDetails{");
        sb.append("allStateCityDealer=").append(allStateCityDealer);
        sb.append(", header=").append(header);
        sb.append(", schemeID='").append(schemeID).append('\'');
        sb.append(", schemeDesc='").append(schemeDesc).append('\'');
        sb.append(", includedMnfcr=").append(includedMnfcr);
        sb.append(", excludedMnfcr=").append(excludedMnfcr);
        sb.append(", inclAsstCtgWithManf=").append(Arrays.toString(inclAsstCtgWithManf));
        sb.append(", includedMakeWithCtgMnfcr=").append(Arrays.toString(includedMakeWithCtgMnfcr));
        sb.append(", includedModelIdWithInformation=").append(Arrays.toString(includedModelIdWithInformation));
        sb.append(", includedState=").append(includedState);
        sb.append(", excludedState=").append(excludedState);
        sb.append(", inclCityWithStateName=").append(Arrays.toString(inclCityWithStateName));
        sb.append(", inclDealerIdWithStateCity=").append(Arrays.toString(inclDealerIdWithStateCity));
        sb.append(", makerID='").append(makerID).append('\'');
        sb.append(", makeDate=").append(makeDate);
        sb.append(", approveSchemeUpdateStatus=").append(approveSchemeUpdateStatus);
        sb.append(", updateStatus=").append(updateStatus);
        sb.append(", schemeStatus='").append(schemeStatus).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", userRole='").append(userRole).append('\'');
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp{

    }
}