package com.softcell.gonogo.model.request.master.schememaster.schemdatemapping;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.Set;

/**
 * @author mahesh
 */
public class ApplicableSchemeDetails {

    @JsonProperty("sSchemeID")
    private String schemeID;

    @JsonProperty("sSchDes")
    private String schemeDesc;

    @JsonProperty("dtValidFrDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date validFromDate;

    @JsonProperty("dtValidToDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date validToDate;

    @JsonProperty("adtExcldDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Set<Date> excludedDate;

    @JsonProperty("adtIncldDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Set<Date> includedDate;

    @JsonProperty("asManfctr")
    private Set<String> maufacturer;

    @JsonProperty("asAsstCtgry")
    private Set<String> assetCategory;

    @JsonProperty("asAsstMake")
    private Set<String> assetMake;

    @JsonProperty("asModelNo")
    private Set<String> modelNo;

    @JsonProperty("asState")
    private Set<String> state;

    @JsonProperty("asCity")
    private Set<String> city;

    @JsonProperty("asDealerName")
    private Set<String> dealerName;

    @JsonProperty("bAllStateCityDealer")
    private boolean allStateCityDealer;

    @JsonProperty("sMkrId")
    private String makerId;

    @JsonProperty("dtMakeDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date makeDate;

    @JsonProperty("sCheckerId")
    private String checkerId;

    @JsonProperty("dtCheckDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date checkDate;

    @JsonProperty("sSchemeStatus")
    private String schemeStatus;

    @JsonProperty("sProductFlag")
    String productFlag;

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public Date getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(Date validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Date getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(Date validToDate) {
        this.validToDate = validToDate;
    }

    public Set<Date> getExcludedDate() {
        return excludedDate;
    }

    public void setExcludedDate(Set<Date> excludedDate) {
        this.excludedDate = excludedDate;
    }

    public Set<Date> getIncludedDate() {
        return includedDate;
    }

    public void setIncludedDate(Set<Date> includedDate) {
        this.includedDate = includedDate;
    }

    public Set<String> getMaufacturer() {
        return maufacturer;
    }

    public void setMaufacturer(Set<String> maufacturer) {
        this.maufacturer = maufacturer;
    }

    public Set<String> getAssetCategory() {
        return assetCategory;
    }

    public void setAssetCategory(Set<String> assetCategory) {
        this.assetCategory = assetCategory;
    }

    public Set<String> getAssetMake() {
        return assetMake;
    }

    public void setAssetMake(Set<String> assetMake) {
        this.assetMake = assetMake;
    }

    public Set<String> getModelNo() {
        return modelNo;
    }

    public void setModelNo(Set<String> modelNo) {
        this.modelNo = modelNo;
    }

    public Set<String> getState() {
        return state;
    }

    public void setState(Set<String> state) {
        this.state = state;
    }

    public Set<String> getCity() {
        return city;
    }

    public void setCity(Set<String> city) {
        this.city = city;
    }

    public Set<String> getDealerName() {
        return dealerName;
    }

    public void setDealerName(Set<String> dealerName) {
        this.dealerName = dealerName;
    }

    public boolean isAllStateCityDealer() {
        return allStateCityDealer;
    }

    public void setAllStateCityDealer(boolean allStateCityDealer) {
        this.allStateCityDealer = allStateCityDealer;
    }

    public String getMakerId() {
        return makerId;
    }

    public void setMakerId(String makerId) {
        this.makerId = makerId;
    }

    public Date getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(Date makeDate) {
        this.makeDate = makeDate;
    }

    public String getCheckerId() {
        return checkerId;
    }

    public void setCheckerId(String checkerId) {
        this.checkerId = checkerId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getSchemeStatus() {
        return schemeStatus;
    }

    public void setSchemeStatus(String schemeStatus) {
        this.schemeStatus = schemeStatus;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApplicableSchemeDetails{");
        sb.append("allStateCityDealer=").append(allStateCityDealer);
        sb.append(", schemeID='").append(schemeID).append('\'');
        sb.append(", schemeDesc='").append(schemeDesc).append('\'');
        sb.append(", validFromDate=").append(validFromDate);
        sb.append(", validToDate=").append(validToDate);
        sb.append(", excludedDate=").append(excludedDate);
        sb.append(", includedDate=").append(includedDate);
        sb.append(", maufacturer=").append(maufacturer);
        sb.append(", assetCategory=").append(assetCategory);
        sb.append(", assetMake=").append(assetMake);
        sb.append(", modelNo=").append(modelNo);
        sb.append(", state=").append(state);
        sb.append(", city=").append(city);
        sb.append(", dealerName=").append(dealerName);
        sb.append(", makerId='").append(makerId).append('\'');
        sb.append(", makeDate=").append(makeDate);
        sb.append(", checkerId='").append(checkerId).append('\'');
        sb.append(", checkDate=").append(checkDate);
        sb.append(", schemeStatus='").append(schemeStatus).append('\'');
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
