package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by anupamad on 30/9/17.
 */
@Data
public class GenSapFileRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {FetchGrp.class})
    @Valid
    private Header header;

    @NotNull(groups = {FetchGrp.class})
    @Valid
    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("bGenCsvFlag")
    private boolean genCsvFlag;

    @JsonProperty("sBranchCode")
    private String branchCode;

    @JsonProperty("sPath")
    private String path;

    public interface FetchGrp {
    }

}
