package com.softcell.gonogo.model.request.master.schememaster.schemdatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class GetSchemeDateMappingDetailsRequest {

    @JsonProperty("sSchemeID")
    @NotBlank(groups = {GetSchemeDateMappingDetailsRequest.FetchGrp.class})
    private String schemeID;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchemeDetailsRequest [schemeID=");
        builder.append(schemeID);
        builder.append(", header=");
        builder.append(header);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp{

    }


}
