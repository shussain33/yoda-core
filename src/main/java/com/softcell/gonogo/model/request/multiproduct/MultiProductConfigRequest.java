package com.softcell.gonogo.model.request.multiproduct;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.MultiProductConfiguration;
import com.softcell.gonogo.model.request.core.Header;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class MultiProductConfigRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(message = "Header Object must not be null")
    private Header header;

    @JsonProperty("oMultiProductConfig")
    @Valid
    private MultiProductConfiguration multiProductConfiguration;

    public static Builder getBuilder(Header header) {
        return new Builder(header);
    }

    public Header getHeader() {
        return header;
    }

    public MultiProductConfiguration getMultiProductConfiguration() {
        return multiProductConfiguration;
    }

    public void setMultiProductConfiguration(
            MultiProductConfiguration multiProductConfiguration) {
        this.multiProductConfiguration = multiProductConfiguration;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("header", header)
                .append("multiProductConfiguration", multiProductConfiguration)
                .toString();
    }

    public static class Builder {

        private MultiProductConfigRequest multiProductConfigRequest = new MultiProductConfigRequest();

        public Builder(Header header) {
            this.multiProductConfigRequest.header = header;
        }

        public MultiProductConfigRequest build() {
            return this.multiProductConfigRequest;
        }

        public Builder multiProductConfiguration(MultiProductConfiguration multiProductConfiguration) {
            this.multiProductConfigRequest.setMultiProductConfiguration(multiProductConfiguration);
            return this;
        }
    }

}
