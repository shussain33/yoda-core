package com.softcell.gonogo.model.request;

import com.softcell.service.impl.CreditVidyaMBObj;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 21/10/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerData {

    String clientId;
    String customerSource;
    String firstName;
    String lastName;
    String emailAddress1;
    CreditVidyaMBObj cibilString;
}
