/**
 * kishorp10:56:55 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * @author kishorp
 */
public class CroQueueRequest {

    @JsonProperty("sCroID")
    @NotBlank(groups = {CroQueueRequest.FetchGrp.class})
    private String croId;

    @JsonProperty("sInstID")
    @NotBlank(groups = {CroQueueRequest.FetchGrp.class})
    private String institutionId;

    @JsonProperty("sGrpID")
    private String groupId;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("iSkip")
    @DecimalMin(value = "0", groups = {CroQueueRequest.FetchGrp.class})
    private int skip;

    @JsonProperty("iLimit")
    @DecimalMin(value = "0", groups = {CroQueueRequest.FetchGrp.class})
    private int limit;

    /**
     * Request Criteria is for getting application request
     * based on different criteria like products or branches
     */
    @JsonProperty("oCriteria")
    @NotNull(groups = {CroQueueRequest.FetchGrp.class})
    @Valid
    private RequestCriteria criteria;

    @JsonProperty("sCurrentStageId")
    private String currentStageId;
    /**
     * @return
     */
    public int getSkip() {
        return skip;
    }

    /**
     * @param skip
     */
    public void setSkip(int skip) {
        this.skip = skip;
    }

    /**
     * @return
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * @return
     */
    public String getCroId() {
        return croId;
    }

    /**
     * @param croId
     */
    public void setCroId(String croId) {
        this.croId = croId;
    }

    /**
     * @return
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * @param groupId
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * @return
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the criteria
     */
    public RequestCriteria getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(RequestCriteria criteria) {
        this.criteria = criteria;
    }

    public String getCurrentStageId() {
        return currentStageId;
    }

    public void setCurrentStageId(String currentStageId) {
        this.currentStageId = currentStageId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CroQueueRequest{");
        sb.append("croId='").append(croId).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", groupId='").append(groupId).append('\'');
        sb.append(", header=").append(header);
        sb.append(", skip=").append(skip);
        sb.append(", limit=").append(limit);
        sb.append(", criteria=").append(criteria);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CroQueueRequest)) return false;
        CroQueueRequest that = (CroQueueRequest) o;
        return Objects.equal(getSkip(), that.getSkip()) &&
                Objects.equal(getLimit(), that.getLimit()) &&
                Objects.equal(getCroId(), that.getCroId()) &&
                Objects.equal(getInstitutionId(), that.getInstitutionId()) &&
                Objects.equal(getGroupId(), that.getGroupId()) &&
                Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getCriteria(), that.getCriteria());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getCroId(), getInstitutionId(), getGroupId(), getHeader(), getSkip(), getLimit(), getCriteria());
    }

    public interface FetchGrp {
    }

}
