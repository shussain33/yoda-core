package com.softcell.gonogo.model.request.master;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.EmployerMaster;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author mahesh
 */
public class EmployerMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("oEmployerMaster")
    private EmployerMaster employerMaster;

    @JsonProperty("iLimit")
    private int limit;

    @JsonProperty("iSkip")
    private int skip;



    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public EmployerMaster getEmployerMaster() {
        return employerMaster;
    }

    public void setEmployerMaster(EmployerMaster employerMaster) {
        this.employerMaster = employerMaster;
    }

    @Override
    public String toString() {
        return "EmployerMasterRequest{" +
                "header=" + header +
                ", employerMaster=" + employerMaster +
                ", limit=" + limit +
                ", skip=" + skip +
                '}';
    }
}
