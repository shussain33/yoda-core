package com.softcell.gonogo.model.request.master.schememaster.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class ModelNoWithId {

    @JsonProperty("sModelNo")
    private String modelNo;

    @JsonProperty("sModelId")
    private String modelId;

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

}
