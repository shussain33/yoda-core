package com.softcell.gonogo.model.request.multiproduct;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
public class MultiProductRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {MultiProductRequest.FetchGrp.class})
    private String refID;

    @JsonProperty("sProduct")
    @NotBlank(groups = {MultiProductRequest.FetchGrp.class})
    private String product;

    @JsonProperty("oPostIPA")
    @NotNull(groups = {MultiProductRequest.ProductSelectionGrp.class})
    @Valid
    private PostIPA postIPA;

    public PostIPA getPostIPA() {
        return postIPA;
    }

    public void setPostIPA(PostIPA postIPA) {
        this.postIPA = postIPA;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MultiProductRequest [header=");
        builder.append(header);
        builder.append(", refID=");
        builder.append(refID);
        builder.append(", product=");
        builder.append(product);
        builder.append(", postIPA=");
        builder.append(postIPA);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result + ((postIPA == null) ? 0 : postIPA.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof MultiProductRequest))
            return false;
        MultiProductRequest other = (MultiProductRequest) obj;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (postIPA == null) {
            if (other.postIPA != null)
                return false;
        } else if (!postIPA.equals(other.postIPA))
            return false;
        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        return true;
    }


    public interface FetchGrp {
    }

    public interface ProductSelectionGrp {
    }
}
