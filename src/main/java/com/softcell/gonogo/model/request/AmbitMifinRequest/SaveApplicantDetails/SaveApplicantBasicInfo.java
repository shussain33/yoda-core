package com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveApplicantBasicInfo {

    @JsonProperty("APPLICANT_TYPE")
    String applicantType;

    @JsonProperty("RELATION")
    String relation;

    @JsonProperty("SALUTATION")
    String salutation;

    @JsonProperty("DOB")
    String dob;

    @JsonProperty("SOCIAL_CATEGORY")
    String socialCategory;

    @JsonProperty("FNAME")
    String fname;

    @JsonProperty("MNAME")
    String mname;

    @JsonProperty("LNAME")
    String lname;

    @JsonProperty("FATHER_FNAME")
    String fatherFname;

    @JsonProperty("FATHER_MNAME")
    String fatherMname;

    @JsonProperty("FATHER_LNAME")
    String fatherLname;

    @JsonProperty("SPOUSE_FNAME")
    String spouseFname;

    @JsonProperty("SPOUSE_MNAME")
    String spouseMname;

    @JsonProperty("SPOUSE_LNAME")
    String spouseLname;

    @JsonProperty("CUSTCATEGORY")
    String custCategory;

    @JsonProperty("VOTER_ID")
    String voterId;

    @JsonProperty("DATE_OF_INCORPORATION")
    String dateOfIncorporation;

    @JsonProperty("CUSTOMER_CREDIT_INFO")
    String customerCreditInfo;

    @JsonProperty("PREF_COMMUNICATION_MODE")
    String prefCommunicationMode;

    @JsonProperty("AUTHORIZED_CAPITAL")
    String authorizedCapital;

    @JsonProperty("CUSTENTITYTYPE")
    String custEntityType;

    @JsonProperty("CKYC_ID")
    String ckycId;

    @JsonProperty("TAN_NO")
    String tanNo;

    @JsonProperty("CONSTITUTION")
    String constitution;

    @JsonProperty("DNS_REQUIRED")
    String dnsRequired;

    @JsonProperty("DNS_REASON")
    String dnsReason;

    @JsonProperty("PROSPECT_CODE")
    String prospectCode;

    @JsonProperty("APPLICANT_CODE")
    String applicantCode;

    @JsonProperty("PAN_NO")
    String panNo;

    @JsonProperty("SEGMENT")
    String segment;

    @JsonProperty("PSL")
    String psl;

    @JsonProperty("MARITAL_STATUS")
    String maritalStatus;

    @JsonProperty("PASSPORTNO")
    String passportNo;

    @JsonProperty("AADHAR_AS_KYC")
    String aadharAsKyc;

    @JsonProperty("COMPANY_NAME")
    String companyName;

    @JsonProperty("EDUCATIONAL_QUALIFICATION")
    String educationalQualification;

    @JsonProperty("NO_OF_DEPENDENTS")
    String noOfDependents;

    @JsonProperty("KEY_CONTACT_PERSON")
    String keyContactPerson;

    @JsonProperty("CLUSTER_DESCRIPTION")
    String clusterDescription;

    @JsonProperty("GROUP_NAME")
    String groupName;

    @JsonProperty("NAME_AS_PER_AADHAR")
    String nameAsPerAadhaar;

    @JsonProperty("OTHERS_ID_NUMBER")
    String othersIdNumber;

    @JsonProperty("OTHER_NAME")
    String otherName;

    @JsonProperty("DRIVING_LICENCE_NO")
    String drivingLicenceNo;

    @JsonProperty("N0_OF_DEPENDENT_CHILDREN")
    String noOfDependentChildren;

    @JsonProperty("ALIAS")
    String alias;

    @JsonProperty("TINNO")
    String tinNo;

    @JsonProperty("IS_EXISTING")
    String isExisting;

    @JsonProperty("CLUSTER")
    String cluster;

    @JsonProperty("APPLICANT_CATEGORY")
    String applicantCategory;

    @JsonProperty("GENDER")
    String gender;

    @JsonProperty("RELIGION")
    String religion;

    @JsonProperty("CIN")
    String cin;

    @JsonProperty("REG_NUMBER")
    String regNumber;

    @JsonProperty("PREF_TIME_TO_CALL")
    String prefTimeToCall;

    @JsonProperty("TYPE_OF_BUSINESS")
    String typeOfBusiness;

    @JsonProperty("IS_PAN_AVAILABLE")
    String isPanAvailable;

    @JsonProperty("FORM_60_NAME")
    String form60Name;

    @JsonProperty("AUTHORISED_SIGN")
    List<AuthorisedSign> authorisedSignList;
}