package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe.ApplicantMatchedAddress;
import com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe.ApplicantMatchedBankAccount;
import com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe.ApplicantMatchedInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateDedupeBaseRequest {
    @JsonProperty("APPLICANT_MATCHED_DETAILS")
    private ApplicantMatchedInfo applicantMatchedInfoList;

    @JsonProperty("APPLICANT_MATCHED_ADDRESS")
    private List<ApplicantMatchedAddress> applicantMatchedAddressesList;

    @JsonProperty("APPLICANT_MATCHED_BANKACCOUNT")
    private ApplicantMatchedBankAccount applicantMatchedBankAccountList;
}
