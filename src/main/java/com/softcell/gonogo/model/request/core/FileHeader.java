package com.softcell.gonogo.model.request.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class FileHeader extends Header {
    @JsonProperty("sApplID")
    private String applicantId;

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    @Override
    public String toString() {
        return "FileHeader [applicantId=" + applicantId + "]";
    }
}
