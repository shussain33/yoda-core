package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.configuration.admin.ProductConfig;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by amit on 25/6/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InstitutionModuleRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sProduct")
    @Valid
    private String productName;

    @JsonProperty("oProdConfig")
    @NotNull(groups = {InstitutionModuleRequest.InsertGrp.class})
    @Valid
    private ProductConfig productConfig;

    public interface InsertGrp {
    }
    public interface FetchGrp{
    }
}
