package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by ssg0302 on 2/8/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KycUpdateRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("aKycList")
    private List<Kyc> kycList;

    @JsonProperty("sApplID")
    private String applicantId;

}
