package com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DedupeUpdate {

    @JsonProperty("MATCHED_APPLICANTCODE")
    private String mathcedApplications;
}
