package com.softcell.gonogo.model.request.emudra;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by shyamk on 27/9/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FTPRequest {
    private String institutionId;
    private String file;
    private String fileName;
}
