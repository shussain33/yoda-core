package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by suhasini on 17/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VerificationRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sVerificationType")
    public String verificationType;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {VerificationRequest.InsertGrp.class,VerificationRequest.FetchGrp.class})
    public String refId;

    @JsonProperty("oVerificationDetails")
    @NotNull(groups = {VerificationRequest.InsertGrp.class})
    private VerificationDetails verificationDetails;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
