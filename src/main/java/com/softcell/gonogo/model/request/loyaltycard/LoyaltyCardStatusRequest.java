package com.softcell.gonogo.model.request.loyaltycard;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 2/5/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoyaltyCardStatusRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {LoyaltyCardStatusRequest.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sLoyaltyCardType")
    private LoyaltyCardType loyaltyCardType;

    @NotEmpty(groups = {LoyaltyCardStatusRequest.FetchGrp.class})
    @JsonProperty("sLoyaltyCardNo")
    private String loyaltyCardNo;

    @NotEmpty(groups = {LoyaltyCardStatusRequest.FetchGrp.class})
    @JsonProperty("sRefId")
    private String refId;

    public interface FetchGrp{

    }



}
