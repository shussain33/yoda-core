package com.softcell.gonogo.model.request.dooperation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.FileHeader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by mahesh on 1/9/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "doOperationLog")
public class DoOperationLog {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("oHeader")
    private FileHeader header;

    @JsonProperty("sDoStatus")
    private String deliveryOrderStatus;



}
