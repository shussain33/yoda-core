package com.softcell.gonogo.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 13/8/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AggregatorData {

    @JsonProperty("sAppID")
    private String applicationId;

    @JsonProperty("sLoginId")
    private String loginId;

    @JsonProperty("sLoginRole")
    private String loginRole;

    @JsonProperty("sPassword")
    private String password;

    @JsonProperty("sUri")
    private String uri;

    @JsonProperty("sContextPath")
    private String contextPath;

    @JsonProperty("sRemoteAddress")
    private String remoteAddress;

    @JsonProperty("sPathInfo")
    private String pathInfo;

    @JsonProperty("sRemoteUser")
    private String remoteUser;

    //For EncryptedPassword
    @JsonProperty("sEncryptKey")
    private String encryptPassword;

    @JsonProperty("bcibilHit")
    private boolean cibilHit;
}