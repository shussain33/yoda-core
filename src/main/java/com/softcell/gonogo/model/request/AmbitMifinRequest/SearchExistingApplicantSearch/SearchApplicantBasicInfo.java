package com.softcell.gonogo.model.request.AmbitMifinRequest.SearchExistingApplicantSearch;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchApplicantBasicInfo {

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

    @JsonProperty("APPLICANT_CODE")
    private String applicantCode;

    @JsonProperty("COMPANY_NAME_OR_FIRST_NAME")
    private String companyName;

    @JsonProperty("PAN_NO")
    private String panNo;

    @JsonProperty("GST_NO")
    private String gstNo;

    @JsonProperty("VOTER_ID")
    private String voterId;

    @JsonProperty("PASSPORT_NO")
    private String passportNo;

    @JsonProperty("LAST_NAME")
    private String lastName;

    @JsonProperty("MOBILE_NO")
    private String mobileNo;

    @JsonProperty("DOB_OR_INCORP")
    private String dobOfIncorp;

    @JsonProperty("EMAIL")
    private String email;

}
