package com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
public class SaveDisbursalMakerRequest {

    @JsonProperty("AUTHENTICATION")
    private AuthenticationDetails authentication;

    @JsonProperty("BASICINFO")
    private DisbursalMakerBasicInfo disbursalMakerBasicInfo;

    @JsonProperty("DISBURSAL_SCHEDULE")
    private List<DisbursalSchedule> disbursalScheduleList;

    @JsonProperty("INTEREST_RATE_SCHEDULE")
    private List<InterestRateSchedule> interestRateScheduleList;

    @JsonProperty("CHARGES")
    private List<Charges> chargesList;

    @JsonProperty("TRANCHE_DISBURSAL_SCHEDULE")
    private TrancheDisbursalSchedule trancheDisbursalSchedule;

    @JsonProperty("DISBURSAL_DTL")
    private DisbursalDetail disbursalDetail;

    @JsonProperty("DISBURSAL_DECISION_INFO")
    private List<DisbursalDecisionInfo> disbursalDecisionInfoList;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("product")
    private String product;
}
