package com.softcell.gonogo.model.request.digitization;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
public class DigitizationApplicationReportRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {DigitizationApplicationReportRequest.FetchGrp.class})
    private String refID;

    @JsonProperty("sProductId")
    @NotEmpty(groups = {DigitizationApplicationReportRequest.FetchGrp.class})
    private String productId;

    @JsonProperty("sFileOperationType")
    private String fileOperationType;

    @JsonProperty("sTemplateId")
    private String templateId;

    @JsonProperty("sAppId")
    private String appId;

    @JsonProperty("sInsuranceId")
    private String insuranceId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitizationApplicationReportRequest)) return false;

        DigitizationApplicationReportRequest that = (DigitizationApplicationReportRequest) o;

        if (getHeader() != null ? !getHeader().equals(that.getHeader()) : that.getHeader() != null) return false;
        if (getRefID() != null ? !getRefID().equals(that.getRefID()) : that.getRefID() != null) return false;
        if (getProductId() != null ? !getProductId().equals(that.getProductId()) : that.getProductId() != null)
            return false;
        return getFileOperationType() != null ? getFileOperationType().equals(that.getFileOperationType()) : that.getFileOperationType() == null;
    }

    @Override
    public int hashCode() {
        int result = getHeader() != null ? getHeader().hashCode() : 0;
        result = 31 * result + (getRefID() != null ? getRefID().hashCode() : 0);
        result = 31 * result + (getProductId() != null ? getProductId().hashCode() : 0);
        result = 31 * result + (getFileOperationType() != null ? getFileOperationType().hashCode() : 0);
        return result;
    }

    public interface FetchGrp {
    }
}
