package com.softcell.gonogo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.MultiBureauDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;

import java.util.List;

/**
 * @author kishorp
 */
public class CoApplicantResponse {
    @JsonProperty("sStatus")
    private String status;
    @JsonProperty("sErrorMessage")
    private String erorMessage;
    @JsonProperty("oApplicant")
    private Applicant applicant;
    @JsonProperty("oResponse")
    private ResponseMultiJsonDomain multiJsonDomain;
    @JsonProperty("aImgMap")
    private List<KycImageDetails> imageMap;
    @JsonProperty("aMultiBeauroDetails")
    private MultiBureauDetails multiBeauroDetails;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErorMessage() {
        return erorMessage;
    }

    public void setErorMessage(String erorMessage) {
        this.erorMessage = erorMessage;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public List<KycImageDetails> getImageMap() {
        return imageMap;
    }

    public void setImageMap(List<KycImageDetails> imageMap) {
        this.imageMap = imageMap;
    }

    public MultiBureauDetails getMultiBeauroDetails() {
        return multiBeauroDetails;
    }

    public void setMultiBeauroDetails(MultiBureauDetails multiBeauroDetails) {
        this.multiBeauroDetails = multiBeauroDetails;
    }

    /**
     * @return the multiJsonDomain
     */
    public ResponseMultiJsonDomain getMultiJsonDomain() {
        return multiJsonDomain;
    }

    /**
     * @param multiJsonDomain the multiJsonDomain to set
     */
    public void setMultiJsonDomain(ResponseMultiJsonDomain multiJsonDomain) {
        this.multiJsonDomain = multiJsonDomain;
    }


}
