package com.softcell.gonogo.model.salesforce;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;


@Document(collection = "SalesForceInfo")
public class SalesForceInfo extends AuditEntity {

    @JsonProperty("sRefId")
    private String applicationRefId;

    @JsonProperty("sLeadId")
    private String leadId;


    @JsonProperty("sStatus")
    private String status;

    //Applicant Id and contact ID against it.
    private Map<String, String> applicantIdcontactIdMap;

    /**
     * @return the applicationRefId
     */
    public String getApplicationRefId() {
        return applicationRefId;
    }


    /**
     * @param applicationRefId the applicationRefId to set
     */
    public void setApplicationRefId(String applicationRefId) {
        this.applicationRefId = applicationRefId;
    }


    /**
     * @return the leadId
     */
    public String getLeadId() {
        return leadId;
    }


    /**
     * @param leadId the leadId to set
     */
    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }


    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }


    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }


    /**
     * @return the contactIdApplicantIdMap
     */
    public Map<String, String> getContactIdApplicantIdMap() {
        return applicantIdcontactIdMap;
    }


    /**
     * @param contactIdApplicantIdMap the contactIdApplicantIdMap to set
     */
    public void setContactIdApplicantIdMap(
            Map<String, String> contactIdApplicantIdMap) {
        this.applicantIdcontactIdMap = contactIdApplicantIdMap;
    }
}
