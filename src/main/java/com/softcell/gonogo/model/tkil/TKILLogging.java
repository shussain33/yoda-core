package com.softcell.gonogo.model.tkil;


import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by sampat on 23/8/17.
 */
@Document(collection = "TKILLogging")
public class TKILLogging extends AuditEntity{

    private String institutionId;
    private Date insertDate;
    private TKILRequest request;
    private DMZResponse response;
    private ThirdPartyException thirdPartyException;
    private String rawResponse;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public TKILRequest getRequest() {
        return request;
    }

    public void setRequest(TKILRequest request) {
        this.request = request;
    }

    public DMZResponse getResponse() {
        return response;
    }

    public void setResponse(DMZResponse response) {
        this.response = response;
    }

    public ThirdPartyException getThirdPartyException() {
        return thirdPartyException;
    }

    public void setThirdPartyException(ThirdPartyException thirdPartyException) {
        this.thirdPartyException = thirdPartyException;
    }

    public String getRawResponse() {
        return rawResponse;
    }

    public void setRawResponse(String rawResponse) {
        this.rawResponse = rawResponse;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TKILLogging{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", insertDate=").append(insertDate);
        sb.append(", request=").append(request);
        sb.append(", response=").append(response);
        sb.append(", exception=").append(thirdPartyException);
        sb.append(", rawResponse='").append(rawResponse).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
