package com.softcell.gonogo.model.tkil;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

/**
 * Created by sampat on 23/8/17.
 */
public class TKILRequest {

    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sDoNumber")
    private String doNumber;

    @JsonProperty("sDoDate")
    private String doDate;

    @JsonProperty("sStoreCode")
    private String storeCode;

    @JsonProperty("sCustomerName")
    private String customerName;

    @JsonProperty("sCustomerAddress")
    private String customerAddress;

    @JsonProperty("sProductBrand")
    private String productBrand;

    @JsonProperty("sProductCatgMake")
    private String productCatgMake;

    @JsonProperty("sProductModel")
    private String productModel;

    @JsonProperty("sSchemeCodeEMI")
    private String schemeCodeEMI;

    @JsonProperty("sProductCost")
    private String productCost;

    @JsonProperty("sMarginMoney")
    private String marginMoney;

    @JsonProperty("sFinanceAmount")
    private String financeAmount;

    @JsonProperty("sDeductionbyHDBFS")
    private String deductionbyHDBFS;

    @JsonProperty("sCustomerAmount")
    private String customerAmount;

    @JsonProperty("sNetLoanAmount")
    private String netLoanAmount;

    @JsonProperty("sDisbursementAmount")
    private String disbursementAmount;

    @JsonProperty("sProcessingFees")
    private String processingFees;

    @JsonProperty("sAdvanceFees")
    private String advanceFees;

    @JsonProperty("sAdvanceEMI")
    private String advanceEMI;

    @JsonProperty("sDealersubvention")
    private String dealersubvention;

    @JsonProperty("sManufacturerSubvention")
    private String manufacturerSubvention;

    @JsonProperty("sOtherCharges")
    private String otherCharges;

    @JsonProperty("sSumDeduction")
    private String sumDeduction;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getDoNumber() {
        return doNumber;
    }

    public void setDoNumber(String doNumber) {
        this.doNumber = doNumber;
    }

    public String getDoDate() {
        return doDate;
    }

    public void setDoDate(String doDate) {
        this.doDate = doDate;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getProductCatgMake() {
        return productCatgMake;
    }

    public void setProductCatgMake(String productCatgMake) {
        this.productCatgMake = productCatgMake;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getSchemeCodeEMI() {
        return schemeCodeEMI;
    }

    public void setSchemeCodeEMI(String schemeCodeEMI) {
        this.schemeCodeEMI = schemeCodeEMI;
    }

    public String getProductCost() {
        return productCost;
    }

    public void setProductCost(String productCost) {
        this.productCost = productCost;
    }

    public String getMarginMoney() {
        return marginMoney;
    }

    public void setMarginMoney(String marginMoney) {
        this.marginMoney = marginMoney;
    }

    public String getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(String financeAmount) {
        this.financeAmount = financeAmount;
    }

    public String getDeductionbyHDBFS() {
        return deductionbyHDBFS;
    }

    public void setDeductionbyHDBFS(String deductionbyHDBFS) {
        this.deductionbyHDBFS = deductionbyHDBFS;
    }

    public String getCustomerAmount() {
        return customerAmount;
    }

    public void setCustomerAmount(String customerAmount) {
        this.customerAmount = customerAmount;
    }

    public String getNetLoanAmount() {
        return netLoanAmount;
    }

    public void setNetLoanAmount(String netLoanAmount) {
        this.netLoanAmount = netLoanAmount;
    }

    public String getDisbursementAmount() {
        return disbursementAmount;
    }

    public void setDisbursementAmount(String disbursementAmount) {
        this.disbursementAmount = disbursementAmount;
    }

    public String getProcessingFees() {
        return processingFees;
    }

    public void setProcessingFees(String processingFees) {
        this.processingFees = processingFees;
    }

    public String getAdvanceFees() {
        return advanceFees;
    }

    public void setAdvanceFees(String advanceFees) {
        this.advanceFees = advanceFees;
    }

    public String getAdvanceEMI() {
        return advanceEMI;
    }

    public void setAdvanceEMI(String advanceEMI) {
        this.advanceEMI = advanceEMI;
    }

    public String getDealersubvention() {
        return dealersubvention;
    }

    public void setDealersubvention(String dealersubvention) {
        this.dealersubvention = dealersubvention;
    }

    public String getManufacturerSubvention() {
        return manufacturerSubvention;
    }

    public void setManufacturerSubvention(String manufacturerSubvention) {
        this.manufacturerSubvention = manufacturerSubvention;
    }

    public String getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(String otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String getSumDeduction() {
        return sumDeduction;
    }

    public void setSumDeduction(String sumDeduction) {
        this.sumDeduction = sumDeduction;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TKILRequest{");
        sb.append("header=").append(header);
        sb.append(", refID='").append(refID).append('\'');
        sb.append(", doNumber='").append(doNumber).append('\'');
        sb.append(", doDate='").append(doDate).append('\'');
        sb.append(", storeCode='").append(storeCode).append('\'');
        sb.append(", customerName='").append(customerName).append('\'');
        sb.append(", customerAddress='").append(customerAddress).append('\'');
        sb.append(", productBrand='").append(productBrand).append('\'');
        sb.append(", productCatgMake='").append(productCatgMake).append('\'');
        sb.append(", productModel='").append(productModel).append('\'');
        sb.append(", schemeCodeEMI='").append(schemeCodeEMI).append('\'');
        sb.append(", productCost='").append(productCost).append('\'');
        sb.append(", marginMoney='").append(marginMoney).append('\'');
        sb.append(", financeAmount='").append(financeAmount).append('\'');
        sb.append(", deductionbyHDBFS='").append(deductionbyHDBFS).append('\'');
        sb.append(", customerAmount='").append(customerAmount).append('\'');
        sb.append(", netLoanAmount='").append(netLoanAmount).append('\'');
        sb.append(", disbursementAmount='").append(disbursementAmount).append('\'');
        sb.append(", processingFees='").append(processingFees).append('\'');
        sb.append(", advanceFees='").append(advanceFees).append('\'');
        sb.append(", advanceEMI='").append(advanceEMI).append('\'');
        sb.append(", dealersubvention='").append(dealersubvention).append('\'');
        sb.append(", manufacturerSubvention='").append(manufacturerSubvention).append('\'');
        sb.append(", otherCharges='").append(otherCharges).append('\'');
        sb.append(", sumDeduction='").append(sumDeduction).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TKILRequest)) return false;

        TKILRequest tkilRequest = (TKILRequest) o;

        if (getHeader() != null ? !getHeader().equals(tkilRequest.getHeader()) : tkilRequest.getHeader() != null) return false;
        if (getRefID() != null ? !getRefID().equals(tkilRequest.getRefID()) : tkilRequest.getRefID() != null) return false;
        if (getDoNumber() != null ? !getDoNumber().equals(tkilRequest.getDoNumber()) : tkilRequest.getDoNumber() != null)
            return false;
        if (getDoDate() != null ? !getDoDate().equals(tkilRequest.getDoDate()) : tkilRequest.getDoDate() != null) return false;
        if (getStoreCode() != null ? !getStoreCode().equals(tkilRequest.getStoreCode()) : tkilRequest.getStoreCode() != null)
            return false;
        if (getCustomerName() != null ? !getCustomerName().equals(tkilRequest.getCustomerName()) : tkilRequest.getCustomerName() != null)
            return false;
        if (getCustomerAddress() != null ? !getCustomerAddress().equals(tkilRequest.getCustomerAddress()) : tkilRequest.getCustomerAddress() != null)
            return false;
        if (getProductBrand() != null ? !getProductBrand().equals(tkilRequest.getProductBrand()) : tkilRequest.getProductBrand() != null)
            return false;
        if (getProductCatgMake() != null ? !getProductCatgMake().equals(tkilRequest.getProductCatgMake()) : tkilRequest.getProductCatgMake() != null)
            return false;
        if (getProductModel() != null ? !getProductModel().equals(tkilRequest.getProductModel()) : tkilRequest.getProductModel() != null)
            return false;
        if (getSchemeCodeEMI() != null ? !getSchemeCodeEMI().equals(tkilRequest.getSchemeCodeEMI()) : tkilRequest.getSchemeCodeEMI() != null)
            return false;
        if (getProductCost() != null ? !getProductCost().equals(tkilRequest.getProductCost()) : tkilRequest.getProductCost() != null)
            return false;
        if (getMarginMoney() != null ? !getMarginMoney().equals(tkilRequest.getMarginMoney()) : tkilRequest.getMarginMoney() != null)
            return false;
        if (getFinanceAmount() != null ? !getFinanceAmount().equals(tkilRequest.getFinanceAmount()) : tkilRequest.getFinanceAmount() != null)
            return false;
        if (getDeductionbyHDBFS() != null ? !getDeductionbyHDBFS().equals(tkilRequest.getDeductionbyHDBFS()) : tkilRequest.getDeductionbyHDBFS() != null)
            return false;
        if (getCustomerAmount() != null ? !getCustomerAmount().equals(tkilRequest.getCustomerAmount()) : tkilRequest.getCustomerAmount() != null)
            return false;
        if (getNetLoanAmount() != null ? !getNetLoanAmount().equals(tkilRequest.getNetLoanAmount()) : tkilRequest.getNetLoanAmount() != null)
            return false;
        if (getDisbursementAmount() != null ? !getDisbursementAmount().equals(tkilRequest.getDisbursementAmount()) : tkilRequest.getDisbursementAmount() != null)
            return false;
        if (getProcessingFees() != null ? !getProcessingFees().equals(tkilRequest.getProcessingFees()) : tkilRequest.getProcessingFees() != null)
            return false;
        if (getAdvanceFees() != null ? !getAdvanceFees().equals(tkilRequest.getAdvanceFees()) : tkilRequest.getAdvanceFees() != null)
            return false;
        if (getAdvanceEMI() != null ? !getAdvanceEMI().equals(tkilRequest.getAdvanceEMI()) : tkilRequest.getAdvanceEMI() != null)
            return false;
        if (getDealersubvention() != null ? !getDealersubvention().equals(tkilRequest.getDealersubvention()) : tkilRequest.getDealersubvention() != null)
            return false;
        if (getManufacturerSubvention() != null ? !getManufacturerSubvention().equals(tkilRequest.getManufacturerSubvention()) : tkilRequest.getManufacturerSubvention() != null)
            return false;
        if (getOtherCharges() != null ? !getOtherCharges().equals(tkilRequest.getOtherCharges()) : tkilRequest.getOtherCharges() != null)
            return false;
        return getSumDeduction() != null ? getSumDeduction().equals(tkilRequest.getSumDeduction()) : tkilRequest.getSumDeduction() == null;
    }

    @Override
    public int hashCode() {
        int result = getHeader() != null ? getHeader().hashCode() : 0;
        result = 31 * result + (getRefID() != null ? getRefID().hashCode() : 0);
        result = 31 * result + (getDoNumber() != null ? getDoNumber().hashCode() : 0);
        result = 31 * result + (getDoDate() != null ? getDoDate().hashCode() : 0);
        result = 31 * result + (getStoreCode() != null ? getStoreCode().hashCode() : 0);
        result = 31 * result + (getCustomerName() != null ? getCustomerName().hashCode() : 0);
        result = 31 * result + (getCustomerAddress() != null ? getCustomerAddress().hashCode() : 0);
        result = 31 * result + (getProductBrand() != null ? getProductBrand().hashCode() : 0);
        result = 31 * result + (getProductCatgMake() != null ? getProductCatgMake().hashCode() : 0);
        result = 31 * result + (getProductModel() != null ? getProductModel().hashCode() : 0);
        result = 31 * result + (getSchemeCodeEMI() != null ? getSchemeCodeEMI().hashCode() : 0);
        result = 31 * result + (getProductCost() != null ? getProductCost().hashCode() : 0);
        result = 31 * result + (getMarginMoney() != null ? getMarginMoney().hashCode() : 0);
        result = 31 * result + (getFinanceAmount() != null ? getFinanceAmount().hashCode() : 0);
        result = 31 * result + (getDeductionbyHDBFS() != null ? getDeductionbyHDBFS().hashCode() : 0);
        result = 31 * result + (getCustomerAmount() != null ? getCustomerAmount().hashCode() : 0);
        result = 31 * result + (getNetLoanAmount() != null ? getNetLoanAmount().hashCode() : 0);
        result = 31 * result + (getDisbursementAmount() != null ? getDisbursementAmount().hashCode() : 0);
        result = 31 * result + (getProcessingFees() != null ? getProcessingFees().hashCode() : 0);
        result = 31 * result + (getAdvanceFees() != null ? getAdvanceFees().hashCode() : 0);
        result = 31 * result + (getAdvanceEMI() != null ? getAdvanceEMI().hashCode() : 0);
        result = 31 * result + (getDealersubvention() != null ? getDealersubvention().hashCode() : 0);
        result = 31 * result + (getManufacturerSubvention() != null ? getManufacturerSubvention().hashCode() : 0);
        result = 31 * result + (getOtherCharges() != null ? getOtherCharges().hashCode() : 0);
        result = 31 * result + (getSumDeduction() != null ? getSumDeduction().hashCode() : 0);
        return result;
    }
}
