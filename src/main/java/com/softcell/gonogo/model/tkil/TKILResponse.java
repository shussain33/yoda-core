package com.softcell.gonogo.model.tkil;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.response.core.Error;

/**
 * Created by sampat on 23/8/17.
 */
public class TKILResponse {

    @JsonProperty("bOriginalResponse")
    private boolean originalResponse;

    @JsonProperty("oError")
    private Error error;

    public boolean isOriginalResponse() {
        return originalResponse;
    }

    public void setOriginalResponse(boolean originalResponse) {
        this.originalResponse = originalResponse;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TKILResponse{");
        sb.append("originalResponse=").append(originalResponse);
        sb.append(", error=").append(error);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TKILResponse)) return false;

        TKILResponse tkilResponse = (TKILResponse) o;

        if (isOriginalResponse() != tkilResponse.isOriginalResponse()) return false;
        return getError() != null ? getError().equals(tkilResponse.getError()) : tkilResponse.getError() == null;
    }

    @Override
    public int hashCode() {
        int result = (isOriginalResponse() ? 1 : 0);
        result = 31 * result + (getError() != null ? getError().hashCode() : 0);
        return result;
    }
}
