package com.softcell.gonogo.model.adroit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 15/10/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CheckRequest {
    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;

    @JsonProperty("sInstitutionId")
    private String institutionId;
}
