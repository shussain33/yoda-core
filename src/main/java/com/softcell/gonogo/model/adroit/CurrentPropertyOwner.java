package com.softcell.gonogo.model.adroit;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sFirstName",
        "sMiddleName",
        "sLastName",
        "sPrefix",
        "sSuffix"
})
public class CurrentPropertyOwner {

    @JsonProperty("sFirstName")
    public String firstName;
    @JsonProperty("sMiddleName")
    public String middleName;
    @JsonProperty("sLastName")
    public String lastName;
    @JsonProperty("sPrefix")
    public String prefix;
    @JsonProperty("sSuffix")
    public String suffix;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}