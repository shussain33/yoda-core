package com.softcell.gonogo.model.adroit;

/**
 * Created by archana on 3/10/18.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sBranchId",
        "sBranchName",
        "sBranchPersonName",
        "sApplicantName",
        "sPropertyAddress1",
        "sPropertyAddress2",
        "sCity",
        "sLocation",
        "sPincode",
        "sContactPersonName",
        "sContactPersonMob1",
        "sContactPersonMob2",
        "sTypeOfCase",
        "sArea",
        "sCollateralId",
        "sAgentCode",
        "sClientId",
        "sOwnerName",
        "sPropertyType",
        "bDocumentProvided",
        "bMapProvided",
        "deed_provided",
        "sDocuments",
        "sDocumentExt",
        "sDocumentName",
        "sState",
        "bDeedProvided"
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdroitRequest {
    @JsonProperty("iBranchId")
    private int branchId;
    @JsonProperty("sBranchName")
    private String branchName;
    @JsonProperty("sBranchPersonName")
    private String branchPersonName;
    @JsonProperty("sApplicantName")
    private String applicantName;
    @JsonProperty("sPropertyAddress1")
    private String propertyAddress1;
    @JsonProperty("sPropertyAddress2")
    private String propertyAddress2;
    @JsonProperty("sCity")
    private String city;
    @JsonProperty("sLocation")
    private String location;
    @JsonProperty("sPincode")
    private String pincode;
    @JsonProperty("sContactPersonName")
    private String contactPersonName;
    @JsonProperty("sContactPersonMob1")
    private String contactPersonMob1;
    @JsonProperty("sContactPersonMob2")
    private String contactPersonMob2;
    @JsonProperty("sTypeOfCase")
    private String typeOfCase;
    @JsonProperty("sArea")
    private String area;
    @JsonProperty("sCollateralId")
    private String collateralId;
    @JsonProperty("sAgentCode")
    private String agentCode;
    @JsonProperty("sClientId")
    private String clientId;
    @JsonProperty("sOwnerName")
    private String ownerName;
    @JsonProperty("sPropertyType")
    private String propertyType;
    @JsonProperty("bDocumentProvided")
    private String documentProvided;
    @JsonProperty("bMapProvided")
    private String mapProvided;
    @JsonProperty("deed_provided")
    private String deed_Provided;
    @JsonProperty("sDocuments")
    private String documents;
    @JsonProperty("sDocumentExt")
    private String documentExt;
    @JsonProperty("sDocumentName")
    private String documentName;
    @JsonProperty("sState")
    private String state;
    @JsonProperty("bDeedProvided")
    private String deedProvided;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
