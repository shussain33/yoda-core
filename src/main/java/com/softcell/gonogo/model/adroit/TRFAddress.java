package com.softcell.gonogo.model.adroit;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sLine1",
        "sLine2",
        "sCity",
        "iPinCode",
        "sState",
        "sCountry",
        "sVillage",
        "sDistrict",
        "sLandMark",
        "sAccm",
        "iTimeAtAddr",
        "sAddrType",
        "sResAddrType",
        "sOfficeType",
        "iMonthAtCity",
        "iMonthAtAddr",
        "dRentAmt",
        "sFlatNo",
        "sStreet",
        "sLocality",
        "iYearAtCity",
        "sNegativeArea",
        "sNegativeAreaReason",
        "dLatitude",
        "dLongitude",
        "sLocation"
})
public class TRFAddress {

    @JsonProperty("sLine1")
    private String line1;
    @JsonProperty("sLine2")
    private String line2;
    @JsonProperty("sCity")
    private String city;
    @JsonProperty("iPinCode")
    private int pinCode;
    @JsonProperty("sState")
    private String state;
    @JsonProperty("sCountry")
    private String country;
    @JsonProperty("sVillage")
    private String village;
    @JsonProperty("sDistrict")
    private String district;
    @JsonProperty("sLandMark")
    private String landMark;
    @JsonProperty("sAccm")
    private String accm;
    @JsonProperty("iTimeAtAddr")
    private int timeAtAddr;
    @JsonProperty("sAddrType")
    private String addrType;
    @JsonProperty("sResAddrType")
    private String resAddrType;
    @JsonProperty("sOfficeType")
    private String officeType;
    @JsonProperty("iMonthAtCity")
    private int monthAtCity;
    @JsonProperty("iMonthAtAddr")
    private int monthAtAddr;
    @JsonProperty("dRentAmt")
    private int rentAmt;
    @JsonProperty("sFlatNo")
    private String flatNo;
    @JsonProperty("sStreet")
    private String street;
    @JsonProperty("sLocality")
    private String locality;
    @JsonProperty("iYearAtCity")
    private int yearAtCity;
    @JsonProperty("sNegativeArea")
    private String negativeArea;
    @JsonProperty("sNegativeAreaReason")
    private String negativeAreaReason;
    @JsonProperty("dLatitude")
    private int latitude;
    @JsonProperty("dLongitude")
    private int longitude;
    @JsonProperty("sLocation")
    private String location;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    private Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    private void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}