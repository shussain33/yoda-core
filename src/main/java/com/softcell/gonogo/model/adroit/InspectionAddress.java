
package com.softcell.gonogo.model.adroit;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "sLine1",
        "sLine2",
        "sCity",
        "iPinCode",
        "sState",
        "sCountry",
        "sVillage",
        "sDistrict",
        "sLandMark",
        "sAccm",
        "iTimeAtAddr",
        "sAddrType",
        "sResAddrType",
        "sOfficeType",
        "iMonthAtCity",
        "iMonthAtAddr",
        "dRentAmt",
        "sFlatNo",
        "sStreet",
        "sLocality",
        "iYearAtCity",
        "sNegativeArea",
        "sNegativeAreaReason",
        "dLatitude",
        "dLongitude",
        "sLocation"
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InspectionAddress {

    @JsonProperty("sLine1")
    private String line1;
    @JsonProperty("sLine2")
    private String line2;
    @JsonProperty("sCity")
    private String city;
    @JsonProperty("iPinCode")
    private Integer pinCode;
    @JsonProperty("sState")
    private String state;
    @JsonProperty("sCountry")
    private String country;
    @JsonProperty("sVillage")
    private String village;
    @JsonProperty("sDistrict")
    private String district;
    @JsonProperty("sLandMark")
    private String landMark;
    @JsonProperty("sAccm")
    private String accm;
    @JsonProperty("iTimeAtAddr")
    private Integer timeAtAddr;
    @JsonProperty("sAddrType")
    private String addrType;
    @JsonProperty("sResAddrType")
    private String resAddrType;
    @JsonProperty("sOfficeType")
    private String officeType;
    @JsonProperty("iMonthAtCity")
    private Integer monthAtCity;
    @JsonProperty("iMonthAtAddr")
    private Integer monthAtAddr;
    @JsonProperty("dRentAmt")
    private Integer rentAmt;
    @JsonProperty("sFlatNo")
    private String flatNo;
    @JsonProperty("sStreet")
    private String street;
    @JsonProperty("sLocality")
    private String locality;
    @JsonProperty("iYearAtCity")
    private Integer yearAtCity;
    @JsonProperty("sNegativeArea")
    private String negativeArea;
    @JsonProperty("sNegativeAreaReason")
    private String negativeAreaReason;
    @JsonProperty("dLatitude")
    private Integer latitude;
    @JsonProperty("dLongitude")
    private Integer longitude;
    @JsonProperty("sLocation")
    private String location;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    private Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    private void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}