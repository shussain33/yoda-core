package com.softcell.gonogo.model.adroit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 12/10/18.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerificationRequest {

    @JsonProperty("sAcknowledgementId")
    private String ackId;

    @JsonProperty("sInstitutionId")
    private String institutionId;
}
