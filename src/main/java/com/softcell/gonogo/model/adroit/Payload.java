package com.softcell.gonogo.model.adroit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Payload {

    @JsonProperty("message")
    private String message;

    @JsonProperty("oCurrentPropertyOwner")
    private CurrentPropertyOwner currentPropertyOwner;
    @JsonProperty("oPropertyAddress")
    private PropertyAddress propertyAddress;
    @JsonProperty("oInspectionAddress")
    private InspectionAddress inspectionAddress;
    @JsonProperty("bPropertyWithinMC")
    private boolean propertyWithinMC;
    @JsonProperty("bDemarcation")
    private boolean demarcation;
    @JsonProperty("sDistanceFromBranch")
    private String distanceFromBranch;
    @JsonProperty("bPlanSanctioned")
    private boolean planSanctioned;
    @JsonProperty("bBuildingSanctioned")
    private boolean buildingSanctioned;
    @JsonProperty("sPropertyUsage")
    private String propertyUsage;

    @JsonProperty("sPropertyNature")
    private String propertyNature;

    @JsonProperty("sPropertyOccupancy")
    private String propertyOccupancy;

    @JsonProperty("iNoOfTenants")
    private int noOfTenants;
    @JsonProperty("sLocalityType")
    private String localityType;

    @JsonProperty("sPropertyCondition")
    private String propertyCondition ;
    @JsonProperty("sResidualAge")
    private String  residualAge;
    @JsonProperty("sLandArea")
    private String  landArea;
    @JsonProperty("sLandRate")
    private String  landRate;
    @JsonProperty("sBuiltUpArea")
    private String  builtUpArea;
    @JsonProperty("sPermissibleArea")
    private String  permissibleArea;
    @JsonProperty("sAdoptedArea")
    private String  adoptedArea;
    @JsonProperty("sConstructionRate")
    private String constructionRate ;
    @JsonProperty("sGovtApprovedRate")
    private String  govtApprovedRate;
    @JsonProperty("sCurrentMarketValue")
    private String currentMarketValue ;

    @JsonProperty("sRemarks")
    private String remarks;
    @JsonProperty("bAvailabilityOfTransport")
    private boolean availabilityOfTransport ;
    @JsonProperty("sClassOfLocality")
    private String classOfLocality ;
    @JsonProperty("sBoundariesMatching")
    private String  boundariesMatching;
    @JsonProperty("oAccoDetails")
    private AccoDetails accoDetails;
    @JsonProperty("sApprovedUsageOfProp")
    private String  approvedUsageOfProp;
    @JsonProperty("sActualUsageOfProp")
    private String actualUsageOfProp ;
    @JsonProperty("sTypeOfStructure")
    private String typeOfStructure ;
    @JsonProperty("iNoOfFloors")
    private int noOfFloors;

    @JsonProperty("sLocationOfPlot")
    private String locationOfPlot ;
    @JsonProperty("sPropertyConnections")
    private String  propertyConnections;
    @JsonProperty("sAmenitiesTransportProximity")
    private String  amenitiesTransportProximity;
    @JsonProperty("sDevOfSurroundingArea")
    private String  devOfSurroundingArea;

    @JsonProperty("oApprovalDetails")
    private ApprovalDetails approvalDetails;

    @JsonProperty("sTotalBuiltUpArea")
    private String  totalBuiltUpArea;
    @JsonProperty("sDeviationBylaw")
    private String  deviationBylaw;
    @JsonProperty("sIsMcdDemolition")
    private String  isMcdDemolition;

    @JsonProperty("sQualityOfConstruction")
    private String qualityOfConstruction;
    @JsonProperty("iAgeOfProperty")
    private int ageOfProperty;

    @JsonProperty("sMaintenanceOfProperty")
    private String maintenanceOfProperty;

    @JsonProperty("iProjectedLife")
    private int projectedLife;
    @JsonProperty("iBuildingHeight")
    private int buildingHeight;
    @JsonProperty("oConstructionDetails")
    private ConstructionDetails constructionDetails;
    @JsonProperty("sLandApplicableRate")
    private String  landApplicableRate;
    @JsonProperty("sLandShareValue")
    private String  landShareValue;
    @JsonProperty("sConstructionApplicableRate")
    private String  constructionApplicableRate;
    @JsonProperty("sConstructionValue")
    private String  constructionValue;
    @JsonProperty("sFairMarketValue")
    private String  fairMarketValue;
    @JsonProperty("sRentedValue")
    private String  rentedValue;
    @JsonProperty("sForcedSaleValue")
    private String  forcedSaleValue;
    @JsonProperty("sInsurableValue")
    private String  insurableValue;
    @JsonProperty("sPropAreaOfConstruction")
    private String  propAreaOfConstruction;
    @JsonProperty("sPropRateOfConstruction")
    private String propRateOfConstruction ;
    @JsonProperty("oTRFAddress")
    private TRFAddress tRFAddress;
    @JsonProperty("sAddrLandmark")
    private String  addrLandmark;
    @JsonProperty("sDistanceFromCity")
    private String  distanceFromCity;
    @JsonProperty("sAddrZone")
    private String  addrZone;
    @JsonProperty("sCurrentMarketRate")
    private String currentMarketRate ;

    @JsonProperty("sAuthorizedPerson")
    private String authorizedPerson;
    @JsonProperty("aImageList")
    private List<ImageList> imageList = null;

    // Coming in get-image response ; Do not save in DB for log purpose since it is too huge
    @Transient
    @JsonProperty("imageData")
    private String filedata ; //"5bc077971700f01b05dca969-filedata1"

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    private Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    private void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}