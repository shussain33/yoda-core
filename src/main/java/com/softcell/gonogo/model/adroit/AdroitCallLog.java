package com.softcell.gonogo.model.adroit;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.softcell.gonogo.model.ThirdPartyCallLog;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by archana on 12/10/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "adroitCallLog")
public class AdroitCallLog extends ThirdPartyCallLog {

    private Date callDate = new Date();

    private Object request;

    private AdroitVerificationResponse response;

    private String strResponse;
}
