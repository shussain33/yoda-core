package com.softcell.gonogo.model.git;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Objects;
import java.util.Properties;

/**
 * Created by prateek on 13/3/17.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class GitRepositoryState {

    private String tags;
    private String branch;
    private String dirty;
    private String remoteOriginUrl;

    private String commitId;
    private String commitIdAbbrev;
    private String describe;
    private String describeShort;
    private String commitUserName;
    private String commitUserEmail;
    private String commitMessageFull;
    private String commitMessageShort;
    private String commitTime;
    private String closestTagName;
    private String closestTagCommitCount;

    private String buildUserName;
    private String buildUserEmail;
    private String buildTime;
    private String buildHost;
    private String buildVersion;

    public GitRepositoryState(Properties properties) {

        this.tags = String.valueOf(properties.get("git.tags"));
        this.branch = String.valueOf(properties.get("git.branch"));
        this.dirty = String.valueOf(properties.get("git.dirty"));
        this.remoteOriginUrl = String.valueOf(properties.get("git.remote.origin.url"));

        this.commitId = String.valueOf(properties.get("git.commit.id.full"));
        this.commitIdAbbrev = String.valueOf(properties.get("git.commit.id.abbrev"));
        this.describe = String.valueOf(properties.get("git.commit.id.describe"));
        this.describeShort = String.valueOf(properties.get("git.commit.id.describe-short"));
        this.commitUserName = String.valueOf(properties.get("git.commit.user.name"));
        this.commitUserEmail = String.valueOf(properties.get("git.commit.user.email"));
        this.commitMessageFull = String.valueOf(properties.get("git.commit.message.full"));
        this.commitMessageShort = String.valueOf(properties.get("git.commit.message.short"));
        this.commitTime = String.valueOf(properties.get("git.commit.time"));
        this.closestTagName = String.valueOf(properties.get("git.closest.tag.name"));
        this.closestTagCommitCount = String.valueOf(properties.get("git.closest.tag.commit.count"));

        this.buildUserName = String.valueOf(properties.get("git.build.user.name"));
        this.buildUserEmail = String.valueOf(properties.get("git.build.user.email"));
        this.buildTime = String.valueOf(properties.get("git.build.time"));
        this.buildHost = String.valueOf(properties.get("git.build.host"));
        this.buildVersion = String.valueOf(properties.get("git.build.version"));
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getDirty() {
        return dirty;
    }

    public void setDirty(String dirty) {
        this.dirty = dirty;
    }

    public String getRemoteOriginUrl() {
        return remoteOriginUrl;
    }

    public void setRemoteOriginUrl(String remoteOriginUrl) {
        this.remoteOriginUrl = remoteOriginUrl;
    }

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    public String getCommitIdAbbrev() {
        return commitIdAbbrev;
    }

    public void setCommitIdAbbrev(String commitIdAbbrev) {
        this.commitIdAbbrev = commitIdAbbrev;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getDescribeShort() {
        return describeShort;
    }

    public void setDescribeShort(String describeShort) {
        this.describeShort = describeShort;
    }

    public String getCommitUserName() {
        return commitUserName;
    }

    public void setCommitUserName(String commitUserName) {
        this.commitUserName = commitUserName;
    }

    public String getCommitUserEmail() {
        return commitUserEmail;
    }

    public void setCommitUserEmail(String commitUserEmail) {
        this.commitUserEmail = commitUserEmail;
    }

    public String getCommitMessageFull() {
        return commitMessageFull;
    }

    public void setCommitMessageFull(String commitMessageFull) {
        this.commitMessageFull = commitMessageFull;
    }

    public String getCommitMessageShort() {
        return commitMessageShort;
    }

    public void setCommitMessageShort(String commitMessageShort) {
        this.commitMessageShort = commitMessageShort;
    }

    public String getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(String commitTime) {
        this.commitTime = commitTime;
    }

    public String getClosestTagName() {
        return closestTagName;
    }

    public void setClosestTagName(String closestTagName) {
        this.closestTagName = closestTagName;
    }

    public String getClosestTagCommitCount() {
        return closestTagCommitCount;
    }

    public void setClosestTagCommitCount(String closestTagCommitCount) {
        this.closestTagCommitCount = closestTagCommitCount;
    }

    public String getBuildUserName() {
        return buildUserName;
    }

    public void setBuildUserName(String buildUserName) {
        this.buildUserName = buildUserName;
    }

    public String getBuildUserEmail() {
        return buildUserEmail;
    }

    public void setBuildUserEmail(String buildUserEmail) {
        this.buildUserEmail = buildUserEmail;
    }

    public String getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(String buildTime) {
        this.buildTime = buildTime;
    }

    public String getBuildHost() {
        return buildHost;
    }

    public void setBuildHost(String buildHost) {
        this.buildHost = buildHost;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GitRepositoryState)) return false;
        GitRepositoryState that = (GitRepositoryState) o;
        return Objects.equals(getTags(), that.getTags()) &&
                Objects.equals(getBranch(), that.getBranch()) &&
                Objects.equals(getDirty(), that.getDirty()) &&
                Objects.equals(getRemoteOriginUrl(), that.getRemoteOriginUrl()) &&
                Objects.equals(getCommitId(), that.getCommitId()) &&
                Objects.equals(getCommitIdAbbrev(), that.getCommitIdAbbrev()) &&
                Objects.equals(getDescribe(), that.getDescribe()) &&
                Objects.equals(getDescribeShort(), that.getDescribeShort()) &&
                Objects.equals(getCommitUserName(), that.getCommitUserName()) &&
                Objects.equals(getCommitUserEmail(), that.getCommitUserEmail()) &&
                Objects.equals(getCommitMessageFull(), that.getCommitMessageFull()) &&
                Objects.equals(getCommitMessageShort(), that.getCommitMessageShort()) &&
                Objects.equals(getCommitTime(), that.getCommitTime()) &&
                Objects.equals(getClosestTagName(), that.getClosestTagName()) &&
                Objects.equals(getClosestTagCommitCount(), that.getClosestTagCommitCount()) &&
                Objects.equals(getBuildUserName(), that.getBuildUserName()) &&
                Objects.equals(getBuildUserEmail(), that.getBuildUserEmail()) &&
                Objects.equals(getBuildTime(), that.getBuildTime()) &&
                Objects.equals(getBuildHost(), that.getBuildHost()) &&
                Objects.equals(getBuildVersion(), that.getBuildVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTags(), getBranch(), getDirty(), getRemoteOriginUrl(), getCommitId(), getCommitIdAbbrev(), getDescribe(), getDescribeShort(), getCommitUserName(), getCommitUserEmail(), getCommitMessageFull(), getCommitMessageShort(), getCommitTime(), getClosestTagName(), getClosestTagCommitCount(), getBuildUserName(), getBuildUserEmail(), getBuildTime(), getBuildHost(), getBuildVersion());
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GitRepositoryState{");
        sb.append("tags='").append(tags).append('\'');
        sb.append(", branch='").append(branch).append('\'');
        sb.append(", dirty='").append(dirty).append('\'');
        sb.append(", remoteOriginUrl='").append(remoteOriginUrl).append('\'');
        sb.append(", commitId='").append(commitId).append('\'');
        sb.append(", commitIdAbbrev='").append(commitIdAbbrev).append('\'');
        sb.append(", describe='").append(describe).append('\'');
        sb.append(", describeShort='").append(describeShort).append('\'');
        sb.append(", commitUserName='").append(commitUserName).append('\'');
        sb.append(", commitUserEmail='").append(commitUserEmail).append('\'');
        sb.append(", commitMessageFull='").append(commitMessageFull).append('\'');
        sb.append(", commitMessageShort='").append(commitMessageShort).append('\'');
        sb.append(", commitTime='").append(commitTime).append('\'');
        sb.append(", closestTagName='").append(closestTagName).append('\'');
        sb.append(", closestTagCommitCount='").append(closestTagCommitCount).append('\'');
        sb.append(", buildUserName='").append(buildUserName).append('\'');
        sb.append(", buildUserEmail='").append(buildUserEmail).append('\'');
        sb.append(", buildTime='").append(buildTime).append('\'');
        sb.append(", buildHost='").append(buildHost).append('\'');
        sb.append(", buildVersion='").append(buildVersion).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
