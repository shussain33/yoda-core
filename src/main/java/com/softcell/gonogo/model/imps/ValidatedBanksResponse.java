package com.softcell.gonogo.model.imps;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 23/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValidatedBanksResponse {

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sBankBranch")
    private String bankBranch;

    @JsonProperty("sAccountNumber")
    private String accountNumber;

    @JsonProperty("sAccountType")
    private String accountType;

    @JsonProperty("sAccountHolderFname")
    private String accountHolderFname;

    @JsonProperty("sAccountHolderMname")
    private String accountHolderMname;

    @JsonProperty("sAccountHolderLname")
    private String accountHolderLname;

    @JsonProperty("sIfscCode")
    private String ifscCode;

    @JsonProperty("sYearsHeld")
    private int yearsHeld;

    @JsonProperty("sBankingType")
    private String bankingType;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sValidFlag")
    private boolean validFlag;


}
