package com.softcell.gonogo.model.imps;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by sampat on 3/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "accountNumberInfo")
public class AccountNumberInfo extends AuditEntity {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sAttemptID")
    private String attemptID;

    @JsonProperty("eProduct")
    private Product product;

    @JsonProperty("dtDateTime")
    private Date dateTime = new Date();

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sBankBranch")
    private String bankBranch;

    @JsonProperty("sAccountNumber")
    private String accountNumber;

    @JsonProperty("sAccountType")
    private String accountType;

    @JsonProperty("sAccountHolderFname")
    private String accountHolderFname;

    @JsonProperty("sAccountHolderFname")
    private String accountHolderMname;

    @JsonProperty("sAccountHolderLname")
    private String accountHolderLname;

    @JsonProperty("sIfscCode")
    private String ifscCode;

    @JsonProperty("sYearsHeld")
    private int yearsHeld;

    @JsonProperty("sBankingType")
    private String bankingType;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sAccountIdentifier")
    private String accountIdentifier;

    @JsonProperty("sValidFlag")
    private boolean validFlag;

    @JsonProperty("sOriginalResponseCode")
    private String originalResponseCode;

    @JsonProperty("sOriginalResponseMsg")
    private String originalResponseMsg;

    @JsonProperty("sResponseAccHolderName")
    private String responseAccHolderName;

    @JsonProperty("sNameMatchingResult")
    private String nameMatchingResult;

    @JsonProperty("sCustMsg")
    private String customMsg;

}
