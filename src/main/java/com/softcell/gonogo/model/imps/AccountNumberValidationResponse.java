package com.softcell.gonogo.model.imps;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 3/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountNumberValidationResponse {

    @JsonProperty("sPayOutUploadDetailsId")
    private String payOutUploadDetailsId;

    @JsonProperty("sPayOutUploadMasterId")
    private String payOutUploadMasterId;

    @JsonProperty("sMerchantMID")
    private String merchantMID;

    @JsonProperty("sTxnAmount")
    private String txnAmount;

    @JsonProperty("sIsInstDetProvided")
    private String isInstDetProvided;

    @JsonProperty("sInstType")
    private String instType;

    @JsonProperty("sInstToken")
    private String instToken;

    @JsonProperty("sInstNumber")
    private String instNumber;

    @JsonProperty("sInstHolderName")
    private String instHolderName;

    @JsonProperty("sAccountHolderName")
    private String accountHolderName;

    @JsonProperty("sInstIFSC")
    private String instIFSC;

    @JsonProperty("sInstExpiryMonth")
    private String instExpiryMonth;

    @JsonProperty("sInstExpiryYear")
    private String instExpiryYear;

    @JsonProperty("sRefundAmount")
    private String refundAmount;

    @JsonProperty("sTranDesc")
    private String tranDesc;

    @JsonProperty("sTranDate")
    private String tranDate;

    @JsonProperty("sTranToken")
    private String tranToken;

    @JsonProperty("sTranType")
    private String tranType;

    @JsonProperty("sMerchantTranId")
    private String merchantTranId;

    @JsonProperty("sSMSSending")
    private String smsSending;

    @JsonProperty("sEmailSending")
    private String emailSending;

    @JsonProperty("sMobileNumber")
    private String mobileNumber;

    @JsonProperty("sEmailID")
    private String emailID;

    @JsonProperty("sPaynimoTranId")
    private String paynimoTranId;

    @JsonProperty("sOctId")
    private String octId;

    @JsonProperty("sTranStatus")
    private String tranStatus;

    @JsonProperty("sApprovalCode")
    private String approvalCode;

    @JsonProperty("sNetworkServiceType")
    private String networkServiceType;

    @JsonProperty("sTxnErrorCode")
    private String txnErrorCode;

    @JsonProperty("sTxnErrorDesc")
    private String txnErrorDesc;

    @JsonProperty("oError")
    private ThirdPartyException error;

}
