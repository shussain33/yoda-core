package com.softcell.gonogo.model.imps;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 21/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountValidationAttemptResponse {

    @JsonProperty("lRemainingAttempt")
    private long remainingAttempt;

    @JsonProperty("sStatus")
    private String status;
}
