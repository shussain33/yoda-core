package com.softcell.gonogo.model.imps;

import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by sampat on 6/11/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "impsConfig")
public class IMPSConfigDomain extends AuditEntity {

    @NotNull
    @Field("institutionId")
    private String institutionId;

    @NotNull
    @Field("merchantIdentifier")
    private String merchantIdentifier;

    @NotNull
    @Field("merchantTxnPGMID")
    private String merchantTxnPGMID;

    @NotNull
    @Field("instrumentDetails")
    private String instrumentDetails;

    @NotNull
    @Field("instrumentType")
    private String instrumentType;

    @NotNull
    @Field("txnChannelType")
    private String txnChannelType;

    @NotNull
    @Field("txnChannelName")
    private String txnChannelName;

    @NotNull
    @Field("txnAmount")
    private String txnAmount;

    @NotNull
    @Field("txnType")
    private String txnType;

    @NotNull
    @Field("txnSubtype")
    private String txnSubtype;

    @NotNull
    @Field("refundAmount")
    private String refundAmount;

    @NotNull
    @Field("txnRequestType")
    private String txnRequestType;

    @NotNull
    @Field("txnDescription")
    private String txnDescription;

    @NotNull
    @Field("smsSending")
    private String smsSending;

    @NotNull
    @Field("emailSending")
    private String emailSending;

    @NotNull
    @Field("noOfRetry")
    private Integer noOfRetry;

    @NotNull
    @Field("nameMatchingValue")
    private Integer nameMatchingValue;

    @NotNull
    @Field("sTimerValue")
    private String timerValue;

    @Field("isEnabled")
    private Boolean isEnabled = true;

    @Field("updatedDt")
    private Date updatedDt;

    @Field("insertDt")
    private Date insertDt;

    //Below time duration will be used to reject the Account validation attempt of invalid account.
    @Field("iRejDuration")
    private int rejectDuration;

}