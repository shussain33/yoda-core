package com.softcell.gonogo.model.imps;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 3/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountNumberValidationRequest {

    @JsonProperty("sMerchantIdentifier")
    private String merchantIdentifier;

    @JsonProperty("sMerchantTxnPGMID")
    private String merchantTxnPGMID;

    @JsonProperty("sChannelType")
    private String channelType;

    @JsonProperty("sChannelName")
    private String channelName;

    @JsonProperty("sTxnAmount")
    private String txnAmount;

    @JsonProperty("sTxnType")
    private String txnType;

    @JsonProperty("sTxnSubtype")
    private String txnSubtype;

    @JsonProperty("sInstrumentDetails")
    private String instrumentDetails;

    @JsonProperty("sInstrumentType")
    private String instrumentType;

    @JsonProperty("sInstrumentToken")
    private String instrumentToken;

    @JsonProperty("sInstrumentIdentifier")
    private String instrumentIdentifier;

    @JsonProperty("sInstrumentHolderName")
    private String instrumentHolderName;

    @JsonProperty("sInstrumentIFSC")
    private String instrumentIFSC;

    @JsonProperty("sInstrumentExpiryMonth")
    private String instrumentExpiryMonth;

    @JsonProperty("sInstrumentExpiryYear")
    private String instrumentExpiryYear;

    @JsonProperty("sRefundAmount")
    private String refundAmount;

    @JsonProperty("sTxnDescription")
    private String txnDescription;

    @JsonProperty("sTxnDateTime")
    private String txnDateTime;

    @JsonProperty("sTxnRequestType")
    private String txnRequestType;

    @JsonProperty("sTxnMerchantRefNo")
    private String txnMerchantRefNo;

    @JsonProperty("sConsIdentifier")
    private String consIdentifier;

    @JsonProperty("sSmsSending")
    private String smsSending;

    @JsonProperty("sEmailSending")
    private String emailSending;

    @JsonProperty("sConsMobileNumber")
    private String consMobileNumber;

    @JsonProperty("sConsEmailID")
    private String consEmailID;

}
