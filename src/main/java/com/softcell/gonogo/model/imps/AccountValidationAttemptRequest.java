package com.softcell.gonogo.model.imps;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 18/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountValidationAttemptRequest {

    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefID")
    private String refID;

    public interface FetchGrp {
    }
}
