package com.softcell.gonogo.model.imps;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 3/11/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ConsumerAlias {

    private String custAliasSource;

    private String custAlias;

    private String custAliasType;

    private String custAliasId;
}
