package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthTokenPayLoad implements Serializable {

    @JsonProperty("Status")
    private String status;

    @JsonProperty("institutionId")
    private Integer institutionId;

    @JsonProperty("Message")
    private String message;

    @JsonProperty("exp")
    private long exp;

    @JsonProperty("client_id")
    private String client_id;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("authorities")
    private List<String> authorities;

    @JsonProperty("authorityList")
    private Authorities[] authorityList;
}
