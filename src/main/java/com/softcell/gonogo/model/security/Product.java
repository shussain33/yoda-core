package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {


    @JsonProperty("PRODUCT_CODE")
    private String productCode;

    @JsonProperty("PRODUCT_NAME")
    private String productName;


    @JsonProperty("ABBREVIATION")
    private String abbreviation;

    @JsonProperty("DISPLAY_NAME")
    private String displayName;


    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Product{");
        sb.append("productCode='").append(productCode).append('\'');
        sb.append(", productName='").append(productName).append('\'');
        sb.append(", abbreviation='").append(abbreviation).append('\'');
        sb.append(", displayName='").append(displayName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
