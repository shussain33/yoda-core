package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bhuvneshk on 25/5/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordBaseResponse {



    @JsonProperty("oBody")
    Body body;

    @JsonProperty("oStatus")
    Object status;

    @JsonProperty("aError")
    Error error;

    public PasswordBaseResponse() {

    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Body {

        @JsonProperty("payLoad")
        ChangePasswordResponse changePasswordResponse;

        public Body() {
        }

        public ChangePasswordResponse getChangePasswordResponse() {
            return changePasswordResponse;
        }

        public void setChangePasswordResponse(ChangePasswordResponse changePasswordResponse) {
            this.changePasswordResponse = changePasswordResponse;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Error {

        @JsonProperty("oStatus")
        String status;

        @JsonProperty("oDeveloperMessage")
        String message;

        public Error() {

        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    @Override
    public String toString() {
        return "PasswordBaseResponse{" +
                "body=" + body +
                ", status=" + status +
                ", error=" + error +
                '}';
    }
}
