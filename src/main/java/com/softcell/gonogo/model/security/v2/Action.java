package com.softcell.gonogo.model.security.v2;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Action {


    @Id
    private String id;

    @JsonProperty("iInstId")
    private Integer institutionId;

    @JsonProperty("bActive")
    private Boolean active;

    @JsonProperty("sActionName")
    private String actionName;

    @JsonProperty("sActionAlias")
    private String actionAlias;


}
