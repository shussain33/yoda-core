package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bhuvneshk on 13/6/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hierarchy implements Serializable {

    @JsonProperty("iInstiId")
    private Integer institutionId;

    @JsonProperty("sHierarchyType")
    private String hierarchyType;

    @JsonProperty("sHierarchylevel")
    private String hierarchyLevel;

    @JsonProperty("aHierarchyVal")
    private List<String> hierarchyValue;

}

