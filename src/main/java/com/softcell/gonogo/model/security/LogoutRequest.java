/**
 * yogeshb4:37:48 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author yogeshb
 */
public class LogoutRequest {

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sUserID")
    @NotBlank(groups = {LogoutRequest.FetchGrp.class})
    private String userId;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LogoutRequest [institutionId=");
        builder.append(institutionId);
        builder.append(", header=");
        builder.append(header);
        builder.append(", userId=");
        builder.append(userId);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp {
    }

}
