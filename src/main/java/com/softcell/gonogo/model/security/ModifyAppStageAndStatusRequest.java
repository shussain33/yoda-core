package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.ActionName;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 11/1/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ModifyAppStageAndStatusRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {ModifyAppStageAndStatusRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("sNewAppStatus")
    @NotBlank(groups = {ModifyAppStageAndStatusRequest.FetchGrp.class})
    private String appStatus;

    @JsonProperty("sNewAppStage")
    @NotBlank(groups = {ModifyAppStageAndStatusRequest.FetchGrp.class})
    private String appStage;

    @JsonProperty("sActionName")
    private ActionName actionName;

    public interface FetchGrp{

    }

}
