/**
 * yogeshb1:25:05 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * @author yogeshb
 *
 */
public class MessagingServiceResponse {

    private String status;



    @JsonProperty("oError")
    private ThirdPartyException error;


    public String getStatus() {
        return status;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MessagingServiceResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", error=").append(error);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessagingServiceResponse that = (MessagingServiceResponse) o;

        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        return error != null ? error.equals(that.error) : that.error == null;
    }

    @Override
    public int hashCode() {
        int result = status != null ? status.hashCode() : 0;
        result = 31 * result + (error != null ? error.hashCode() : 0);
        return result;
    }
}
