package com.softcell.gonogo.model.security;

import com.softcell.gonogo.model.AuditEntity;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Iterator;
import java.util.List;


@Document(collection = "user")
public class User extends AuditEntity {
    @Id
    private ObjectId id;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private boolean enabled;
    private boolean loginStatus;
    private List<Role> roles;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(boolean loginStatus) {
        this.loginStatus = loginStatus;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", userName=" + userName + ", password="
                + password + ", firstName=" + firstName + ", lastName="
                + lastName + ", middleName=" + middleName + ", enabled="
                + enabled + ", loginStatus=" + loginStatus + ", roles=" + roles
                + "]";
    }

    public String getRolesCSV() {
        StringBuilder sb = new StringBuilder();
        for (Iterator<Role> iter = this.roles.iterator(); iter.hasNext(); ) {
            sb.append(iter.next().getId());
            if (iter.hasNext()) {
                sb.append(',');
            }
        }
        return sb.toString();
    }
}