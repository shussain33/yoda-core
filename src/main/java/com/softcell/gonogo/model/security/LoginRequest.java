package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author kishor
 */
public class LoginRequest {

    @NotBlank(groups = {LoginRequest.FetchGrp.class, LoginRequest.LoginWebV3Grp.class})
    private String userName;

    @NotBlank(groups = {LoginRequest.FetchGrp.class, LoginRequest.LoginWebV3Grp.class})
    private String password;

    @NotBlank(groups = {LoginRequest.LoginWebV3Grp.class})
    private String instituteName;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.AppSourceGrp.class})
    @Valid
    private Header header;

    private String instId;

    @JsonProperty("sRefID")
    private String refId;


    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getInstId() {
        return instId;
    }

    public void setInstId(String instId) {
        this.instId = instId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result + ((instId == null) ? 0 : instId.hashCode());
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((refId == null) ? 0 : refId.hashCode());
        result = prime * result
                + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof LoginRequest))
            return false;
        LoginRequest other = (LoginRequest) obj;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (instId == null) {
            if (other.instId != null)
                return false;
        } else if (!instId.equals(other.instId))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (refId == null) {
            if (other.refId != null)
                return false;
        } else if (!refId.equals(other.refId))
            return false;
        if (userName == null) {
            if (other.userName != null)
                return false;
        } else if (!userName.equals(other.userName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LoginRequest [userName=");
        builder.append(userName);
        builder.append(", password=");
        builder.append(password);
        builder.append(", instituteName=");
        builder.append(instituteName);
        builder.append(", header=");
        builder.append(header);
        builder.append(", instId=");
        builder.append(instId);
        builder.append(", refId=");
        builder.append(refId);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp {
    }

    public interface LoginWebV3Grp {}

}
