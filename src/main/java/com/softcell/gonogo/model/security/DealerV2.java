package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

/**
 * Created by bhuvneshk on 13/6/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DealerV2 {

    @Id
    private String id;

    @NotNull
    @JsonProperty("iInstId")
    private Integer institutionId;

    @NotNull
    @JsonProperty("iDlrId")
    private Integer dealerId;

    @NotNull
    @JsonProperty("sDlrName")
    private String dealerName;

    @NotNull
    @JsonProperty("iBranchId")
    private Integer branchId;

    @NotNull
    @JsonProperty("sBranchName")
    private String branchName;

    @NotNull
    @JsonProperty("sProduct")
    private String product;

    @NotNull
    @JsonProperty("bActive")
    private boolean active;


}
