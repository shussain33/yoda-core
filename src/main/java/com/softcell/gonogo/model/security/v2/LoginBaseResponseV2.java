package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by bhuvneshk on 25/5/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginBaseResponseV2 {

    @JsonProperty("oBody")
    Body body;

    @JsonProperty("oStatus")
    Object status;

    @JsonProperty("aError")
    Error error;

    public LoginBaseResponseV2() {

    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Body {

        @JsonProperty("payLoad")
        List<LoginServiceResponse> loginServiceResponse;

        public Body() {
        }

        public List<LoginServiceResponse> getLoginServiceResponse() {
            return loginServiceResponse;
        }

        public void setLoginServiceResponse(List<LoginServiceResponse> loginServiceResponse) {
            this.loginServiceResponse = loginServiceResponse;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Error {

        @JsonProperty("oStatus")
        String status;

        @JsonProperty("oDeveloperMessage")
        String message;

        public Error() {

        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}