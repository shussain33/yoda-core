/**
 * yogeshb3:58:05 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Status;

/**
 * @author yogeshb
 *
 */
public class GetOtpResponse {
    @JsonProperty("STATUS")
    private Status status;

    @JsonProperty("OTP")
    private String otp;


    public Status getStatus() {
        return status;
    }


    public void setStatus(Status status) {
        this.status = status;
    }


    public String getOtp() {
        return otp;
    }


    public void setOtp(String otp) {
        this.otp = otp;
    }


    @Override
    public String toString() {
        return "GetOtpResponse [status=" + status + ", otp=" + otp + "]";
    }


}
