package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This is the response we will get from UM module
 * It will consist of status and error list
 *
 * @author bhuvneshk
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePasswordResponse {

    @JsonProperty("sStatus")
    String status;

    @JsonProperty("aErrors")
    private Object errors;

    @JsonProperty("oError")
    private Object error;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public Object getErrors() { return errors; }

    public void setErrors(Object errors) { this.errors = errors; }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ChangePasswordResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", error=").append(error);
        sb.append(", errors=").append(errors);
        sb.append('}');
        return sb.toString();
    }
}
