package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Hierarchy {

    @JsonProperty("HIERARCHY_TYPE")
    private String hierarchyType;

    @JsonProperty("HIERARCHY_LEVEL")
    private String hierarchyLevel;


    @JsonProperty("HIERARCHY_VALUE")
    private String[] hierarchyValue;


    /**
     * @return the hierarchyType
     */
    public String getHierarchyType() {
        return hierarchyType;
    }


    /**
     * @param hierarchyType the hierarchyType to set
     */
    public void setHierarchyType(String hierarchyType) {
        this.hierarchyType = hierarchyType;
    }


    /**
     * @return the hierarchyLevel
     */
    public String getHierarchyLevel() {
        return hierarchyLevel;
    }


    /**
     * @param hierarchyLevel the hierarchyLevel to set
     */
    public void setHierarchyLevel(String hierarchyLevel) {
        this.hierarchyLevel = hierarchyLevel;
    }


    /**
     * @return the hierarchyValue
     */
    public String[] getHierarchyValue() {
        return hierarchyValue;
    }


    /**
     * @param hierarchyValue the hierarchyValue to set
     */
    public void setHierarchyValue(String[] hierarchyValue) {
        this.hierarchyValue = hierarchyValue;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Hierarchy [hierarchyType=" + hierarchyType
                + ", hierarchyLevel=" + hierarchyLevel + ", hierarchyValue="
                + hierarchyValue + "]";
    }


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((hierarchyLevel == null) ? 0 : hierarchyLevel.hashCode());
        result = prime * result
                + ((hierarchyType == null) ? 0 : hierarchyType.hashCode());
        result = prime * result
                + ((hierarchyValue == null) ? 0 : hierarchyValue.hashCode());
        return result;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Hierarchy other = (Hierarchy) obj;
        if (hierarchyLevel == null) {
            if (other.hierarchyLevel != null)
                return false;
        } else if (!hierarchyLevel.equals(other.hierarchyLevel))
            return false;
        if (hierarchyType == null) {
            if (other.hierarchyType != null)
                return false;
        } else if (!hierarchyType.equals(other.hierarchyType))
            return false;
        if (hierarchyValue == null) {
            if (other.hierarchyValue != null)
                return false;
        } else if (!hierarchyValue.equals(other.hierarchyValue))
            return false;
        return true;
    }


}
