package com.softcell.gonogo.model.security;

import org.apache.axis2.transport.http.security.SSLProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.GeneralSecurityException;

@Deprecated
public class SoapByPassCert {


    public static Protocol byPassCertificate() {

        Protocol prot = null;
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs,
                            String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs,
                            String authType) {
                    }
                }
        };


        try {

            SSLContext sc = SSLContext.getInstance("SSL");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            SSLProtocolSocketFactory sslFactory = new SSLProtocolSocketFactory(sc);
            prot = new Protocol("https",
                    (ProtocolSocketFactory) sslFactory, 443);


        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

        return prot;

    }
}