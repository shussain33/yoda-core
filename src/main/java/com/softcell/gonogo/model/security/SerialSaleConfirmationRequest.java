package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

/**
 * @author mahesh
 */

public class SerialSaleConfirmationRequest {
    @JsonProperty("oHeader")
    Header header;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sSaleDate")
    private String saleDate;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("sSaleType")
    private String saleType;

    @JsonProperty("sPartnerCode")
    private String partnerCode;

    @JsonProperty("sReferenceID")
    private String referenceID;

    @JsonProperty("sProductType")
    private String productType;


    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

}
