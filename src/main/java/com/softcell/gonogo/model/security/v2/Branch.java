package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by bhuvneshk on 13/6/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Branch implements Serializable {

    @Id
    private String id;

    @NotNull
    @JsonProperty("iInstId")
    private Integer institutionId;

    @NotNull
    @JsonProperty("iBranchId")
    private Integer branchId;

    @NotEmpty
    @JsonProperty("sBranchName")
    private String branchName;

    @NotEmpty
    @JsonProperty("sBranchManagerName")
    private String branchManagerName;

    @JsonProperty("sBranchManagerId")
    private String branchManagerId;

    @NotEmpty
    @JsonProperty("sLocation")
    private String location;

    @NotEmpty
    @JsonProperty("sRegion")
    private String region;

    @NotEmpty
    @JsonProperty("sZone")
    private String zone;

    @NotEmpty
    @JsonProperty("sCountry")
    private String country;

    @NotNull
    @JsonProperty("bActive")
    private Boolean active;

    @JsonProperty("sBranchEmail")
    private String branchEmail;

    @JsonProperty("sBranchType")
    private String branchType;

    @JsonProperty("sHubBranchId")
    private Integer hubBranchId;

    @JsonProperty("sHubBranchName")
    private String hubBranchName;

    @JsonProperty("sSpokeBranchId")
    private Integer spokeBranchId;

    @JsonProperty("sSpokeBranchname")
    private  String spokeBranchName;

    @JsonProperty("aSpokeIds")
    private Set<Integer> spokeBranchIds;

    @JsonProperty("iLmsBranchId")
    private Integer lmsBranchId;

    @JsonProperty("sMifinBranchId")
    private String mifinBranchId;

    @JsonProperty("sMifinBranchManagerId")
    private String mifinBranchManagerId;

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        Branch that = (Branch)o;
        return StringUtils.equals(branchId.toString(), that.branchId.toString()) && StringUtils.equals(branchName, that.branchName);
    }

    @Override
    public int hashCode() {
        int result = branchId.hashCode();
        result = 31 * result + (branchName != null ? branchName.hashCode() : 0);
        return result;
    }
}
