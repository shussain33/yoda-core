/**
 * kishorp6:24:30 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author kishorp
 *
 */
public class Dealer {
    @JsonProperty("DEALER_CODE")
    private String dealerCode;

    @JsonProperty("DEALER_NAME")
    private String dealerName;


    @JsonProperty("PRODUCT")
    private String product;

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Dealer{");
        sb.append("dealerCode='").append(dealerCode).append('\'');
        sb.append(", dealerName='").append(dealerName).append('\'');
        sb.append(", product='").append(product).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
