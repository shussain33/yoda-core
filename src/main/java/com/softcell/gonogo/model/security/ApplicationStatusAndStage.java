package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahesh on 11/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationStatusAndStage {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("sAppStatus")
    private String appStatus;

    @JsonProperty("sAppStage")
    private String appStage;


}
