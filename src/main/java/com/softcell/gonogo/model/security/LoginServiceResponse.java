/**
 * yogeshb5:57:00 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Set;

/**
 * @author yogeshb
 *
 */
//TODO Change json property to hungarian notation same in UM
public class LoginServiceResponse {
    @JsonProperty("USERNAME")
    private String userName;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("ACTION")
    private Set<String> action;
    @JsonProperty("ROLES")
    private Object roles;
    @JsonProperty("USER_DETAILS")
    private List<UserDetails> userDetails;
    @JsonProperty("SELF_CHECKER")
    private String selfChecker;
    @JsonProperty("MAKER_CHECKER")
    private String makerChecker;
    @JsonProperty("ERRORS")
    private Object error;
    @JsonProperty("DEALERS")
    private List<Dealer> dealerList;
    @JsonProperty("BRANCHES")
    private List<Branch> branchList;
    @JsonProperty("PRODUCTS")
    private List<Product> productList;
    @JsonProperty("HIERARCHY")
    private Hierarchy hierarchy;

    public List<Dealer> getDealerList() {
        return dealerList;
    }

    public void setDealerList(List<Dealer> dealerList) {
        this.dealerList = dealerList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<String> getAction() {
        return action;
    }

    public void setAction(Set<String> action) {
        this.action = action;
    }

    public Object getRoles() {
        return roles;
    }

    public void setRoles(Object roles) {
        this.roles = roles;
    }

    public List<UserDetails> getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(List<UserDetails> userDetails) {
        this.userDetails = userDetails;
    }

    public String getSelfChecker() {
        return selfChecker;
    }

    public void setSelfChecker(String selfChecker) {
        this.selfChecker = selfChecker;
    }

    public String getMakerChecker() {
        return makerChecker;
    }

    public void setMakerChecker(String makerChecker) {
        this.makerChecker = makerChecker;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    /**
     * @return the branchList
     */
    public List<Branch> getBranchList() {
        return branchList;
    }

    /**
     * @param branchList the branchList to set
     */
    public void setBranchList(List<Branch> branchList) {
        this.branchList = branchList;
    }

    /**
     * @return the productList
     */
    public List<Product> getProductList() {
        return productList;
    }

    /**
     * @param productList the productList to set
     */
    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    /**
     * @return the hierarchy
     */
    public Hierarchy getHierarchy() {
        return hierarchy;
    }

    /**
     * @param hierarchy the hierarchy to set
     */
    public void setHierarchy(Hierarchy hierarchy) {
        this.hierarchy = hierarchy;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LoginServiceResponse [userName=");
        builder.append(userName);
        builder.append(", status=");
        builder.append(status);
        builder.append(", action=");
        builder.append(action);
        builder.append(", roles=");
        builder.append(roles);
        builder.append(", userDetails=");
        builder.append(userDetails);
        builder.append(", selfChecker=");
        builder.append(selfChecker);
        builder.append(", makerChecker=");
        builder.append(makerChecker);
        builder.append(", error=");
        builder.append(error);
        builder.append(", dealerList=");
        builder.append(dealerList);
        builder.append(", branchList=");
        builder.append(branchList);
        builder.append(", productList=");
        builder.append(productList);
        builder.append(", hierarchy=");
        builder.append(hierarchy);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result
                + ((branchList == null) ? 0 : branchList.hashCode());
        result = prime * result
                + ((dealerList == null) ? 0 : dealerList.hashCode());
        result = prime * result + ((error == null) ? 0 : error.hashCode());
        result = prime * result
                + ((hierarchy == null) ? 0 : hierarchy.hashCode());
        result = prime * result
                + ((makerChecker == null) ? 0 : makerChecker.hashCode());
        result = prime * result
                + ((productList == null) ? 0 : productList.hashCode());
        result = prime * result + ((roles == null) ? 0 : roles.hashCode());
        result = prime * result
                + ((selfChecker == null) ? 0 : selfChecker.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result
                + ((userDetails == null) ? 0 : userDetails.hashCode());
        result = prime * result
                + ((userName == null) ? 0 : userName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof LoginServiceResponse))
            return false;
        LoginServiceResponse other = (LoginServiceResponse) obj;
        if (action == null) {
            if (other.action != null)
                return false;
        } else if (!action.equals(other.action))
            return false;
        if (branchList == null) {
            if (other.branchList != null)
                return false;
        } else if (!branchList.equals(other.branchList))
            return false;
        if (dealerList == null) {
            if (other.dealerList != null)
                return false;
        } else if (!dealerList.equals(other.dealerList))
            return false;
        if (error == null) {
            if (other.error != null)
                return false;
        } else if (!error.equals(other.error))
            return false;
        if (hierarchy == null) {
            if (other.hierarchy != null)
                return false;
        } else if (!hierarchy.equals(other.hierarchy))
            return false;
        if (makerChecker == null) {
            if (other.makerChecker != null)
                return false;
        } else if (!makerChecker.equals(other.makerChecker))
            return false;
        if (productList == null) {
            if (other.productList != null)
                return false;
        } else if (!productList.equals(other.productList))
            return false;
        if (roles == null) {
            if (other.roles != null)
                return false;
        } else if (!roles.equals(other.roles))
            return false;
        if (selfChecker == null) {
            if (other.selfChecker != null)
                return false;
        } else if (!selfChecker.equals(other.selfChecker))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (userDetails == null) {
            if (other.userDetails != null)
                return false;
        } else if (!userDetails.equals(other.userDetails))
            return false;
        if (userName == null) {
            if (other.userName != null)
                return false;
        } else if (!userName.equals(other.userName))
            return false;
        return true;
    }


}
