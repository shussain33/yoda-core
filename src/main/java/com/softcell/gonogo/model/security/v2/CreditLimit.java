package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditLimit {

    @Id
    private Long id;

    @JsonProperty("sLoginId")
    private String loginId;

    @JsonProperty("sEmpId")
    private String employeeId;

    @JsonProperty("iInstId")
    private Integer institutionId;

    @JsonProperty("sProductName")
    private String productName;

    @JsonProperty("sProductCreditLimit")
    private String productCreditLimit;
}
