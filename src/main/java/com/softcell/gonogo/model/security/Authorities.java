package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Authorities implements Serializable {

    @JsonProperty("institutionId")
    private Integer institutionId;

    @JsonProperty("active")
    private boolean active;

    @JsonProperty("roleName")
    private String roleName;

    @JsonProperty("roleAlias")
    private String roleAlias;
}
