package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.response.core.Status;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthTokenValidateResponse {

    @JsonProperty("oBody")
    Body body;

    @JsonProperty("oStatus")
    Status status;

    @JsonProperty("aError")
    Error error;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Body {

        @JsonProperty("payLoad")
        private AuthTokenPayLoad authTokenPayLoad;

        public Body() {

        }

        public AuthTokenPayLoad getAuthTokenPayLoad() {
            return authTokenPayLoad;
        }

        public void setAuthTokenPayLoad(AuthTokenPayLoad authTokenPayLoad) {
            this.authTokenPayLoad = authTokenPayLoad;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Error {

        @JsonProperty("oStatus")
        String status;

        @JsonProperty("oDeveloperMessage")
        String message;

        @JsonProperty("oUser")
        private Object user;

        public Error() {

        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object getUser() {
            return user;
        }

        public void setUser(Object user) {
            this.user = user;
        }
    }
}
