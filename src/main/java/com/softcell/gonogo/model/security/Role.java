package com.softcell.gonogo.model.security;

public class Role {
    private int id;
    private String role;
    private String roleDescription;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    @Override
    public String toString() {
        return "Role [id=" + id + ", role=" + role + ", roleDescription="
                + roleDescription + "]";
    }


}