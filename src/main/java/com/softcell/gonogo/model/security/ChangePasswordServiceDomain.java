package com.softcell.gonogo.model.security;


public class ChangePasswordServiceDomain {

    private String userName;

    private String oldPassword;

    private String newPassword;

    private String institutionId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ChangePasswordRequest [userName=");
        builder.append(userName);
        builder.append(", oldPassword=");
        builder.append(oldPassword);
        builder.append(", newPassword=");
        builder.append(newPassword);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }


}
