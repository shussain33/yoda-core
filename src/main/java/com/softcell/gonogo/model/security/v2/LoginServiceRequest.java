package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Created by bhuvneshk on 23/5/17.
 */
public class LoginServiceRequest {

    @JsonProperty("sLoginId")
    private String loginId;

    @JsonProperty("sPassword")
    private String password;

    @JsonProperty("sInstituteName")
    private String instituteName;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginServiceRequest)) return false;
        LoginServiceRequest that = (LoginServiceRequest) o;
        return Objects.equals(getLoginId(), that.getLoginId()) &&
                Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLoginId(), getPassword());
    }
}
