package com.softcell.gonogo.model.security.v2;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoleDetail {

    @Id
    private String id;

    @JsonProperty("iInstId")
    private Integer institutionId;

    @JsonProperty("bActive")
    private Boolean active;

    @JsonProperty("sRoleName")
    private String roleName;

    @JsonProperty("sAlias")
    private String roleAlias;

    @JsonProperty("sDepartment")
    private String department;

    @JsonProperty("aActions")
    private Set<Action> actions;

    @JsonProperty("aActionNames")
    private List<String> actionNames = new ArrayList<>();

    @JsonProperty("aProductCreditLimit")
    private Set<CreditLimit> productCreditLimit;

    @JsonProperty("aProduct")
    private Set<Product> productDetails;

    @JsonProperty("bIsCredit")
    private Boolean isCredit;
}
