package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LogoutBaseResponse {

    @JsonProperty("oBody")
    LogoutBaseResponse.Body body;

    @JsonProperty("oStatus")
    LogoutBaseResponse.Status status;

    @JsonProperty("aError")
    LogoutBaseResponse.Error error;

    public LogoutBaseResponse() {

    }

    public LogoutBaseResponse.Body getBody() {
        return body;
    }

    public void setBody(LogoutBaseResponse.Body body) {
        this.body = body;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LogoutBaseResponse.Error getError() {
        return error;
    }

    public void setError(LogoutBaseResponse.Error error) {
        this.error = error;
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Body {

        @JsonProperty("payLoad")
        Object payload;

        public Body() {
        }


    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Error {

        @JsonProperty("oStatus")
        String status;

        @JsonProperty("oDeveloperMessage")
        String message;

        public Error() {

        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Status {

        @JsonProperty("iStatus")
        private Integer statusCode;

        @JsonProperty("sStatus")
        private String statusMessage;

        public Integer getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(Integer statusCode) {
            this.statusCode = statusCode;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        public void setStatusMessage(String statusMessage) {
            this.statusMessage = statusMessage;
        }
    }
}
