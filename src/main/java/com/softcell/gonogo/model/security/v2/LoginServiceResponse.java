package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Created by bhuvneshk on 23/5/17.
 */
@Document(collection = "Users")
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginServiceResponse implements Serializable {

    @Id
    @JsonProperty("id")
    private String id;

    @JsonProperty("sLoginId")
    private String loginId;

    @JsonProperty("iInstId")
    private Integer institutionId;

    @JsonProperty("instituteName")
    private String instituteName;

    @JsonProperty("sFirstName")
    private String firstName;

    @JsonProperty("sMiddleName")
    private String middleName;

    @JsonProperty("sLastName")
    private String lastName;

    @JsonProperty("sFullName")
    String fullName;

    @JsonProperty("sUserName")
    String userName;

    @JsonProperty("sEmail")
    private String email;

    @JsonProperty("sEmpId")
    private String employeeId;

    @JsonProperty("sMobile")
    private String mobile;

    @JsonProperty("bActive")
    private Boolean active;

    @JsonProperty("aRoles")
    private Set<String> roles;

    @JsonProperty("aDealers")
    private Set<Dealer> dealers;

    @JsonProperty("aBranches")
    private Set<Branch> branches;

    @JsonProperty("aActions")
    private Set<String> actions;

    @JsonProperty("status")
    private int statusCode;

    @JsonProperty("message")
    private String message;

    @JsonProperty("aProduct")
    private Set<Product> product;

    @JsonProperty("oHierarchy")
    private Hierarchy hierarchy;

    //@NotNull
    @JsonProperty("sInstImg")
    private String institutionImage;

    //@NotNull
    @JsonProperty("sColorCode")
    private String colorCode;

    @JsonProperty("iCreditAmount")
    private Integer creditAmount;

    @JsonProperty("iLowerCreditLimit")
    private int lowerCreditLimit;

    @JsonProperty("dDBR")
    private double DBR;

    @JsonProperty("dLTV")
    private double LTV;

    @JsonProperty("sBranchEmail")
    private String branchEmail;

    @JsonProperty("sPhoneExtension")
    private String phoneExtension;

    @JsonProperty("aBaseBranches")
    private List<Branch> baseBranches;

    @JsonProperty("aReportingUsers")
    private Set<String> reportingUsers;

    @JsonProperty("sSSLUserName")
    private String sslUserName;

    @JsonProperty("sSSLPassword")
    private String sslPassword;

    @JsonProperty("aSubProduct")
    private Set<Product> subProductList;

    @JsonProperty("sDesignation")
    private String designation;

    @JsonProperty("sSapCode")
    private String sapCode;

    @JsonProperty("aRoleInfo")
    private Set<RoleDetail> rolesInfo;

    @JsonProperty("sAuthorizationToken")
    private String authorizationToken;

    @JsonProperty("sUserStatus")
    private String userStatus;

    @JsonProperty("aProductCreditLimit")
    private Set<CreditLimit> productCreditLimit;

    public String getUserStatus() { return userStatus; }

    public void setUserStatus(String userStatus) { this.userStatus = userStatus; }

    public String getAuthorizationToken() { return authorizationToken;  }

    public void setAuthorizationToken(String authorizationToken) {  this.authorizationToken = authorizationToken;   }

    public String getId() {        return id;    }

    public void setId(String id) {        this.id = id;    }

    public Set<String> getReportingUsers() {
        return reportingUsers;
    }

    public void setReportingUsers(Set<String> reportingUsers) {
        this.reportingUsers = reportingUsers;
    }

    public String getSslUserName() {
        return sslUserName;
    }

    public void setSslUserName(String sslUserName) {
        this.sslUserName = sslUserName;
    }

    public String getSslPassword() {
        return sslPassword;
    }

    public void setSslPassword(String sslPassword) {
        this.sslPassword = sslPassword;
    }

    public String getBranchEmail() {    return branchEmail; }

    public void setBranchEmail(String branchEmail) {    this.branchEmail = branchEmail;         }

    public int getLowerCreditLimit() {
        return lowerCreditLimit;
    }

    public void setLowerCreditLimit(int lowerCreditLimit) {
        this.lowerCreditLimit = lowerCreditLimit;
    }

    public double getDBR() {
        return DBR;
    }

    public void setDBR(double DBR) {
        this.DBR = DBR;
    }

    public double getLTV() {
        return LTV;
    }

    public void setLTV(double LTV) {
        this.LTV = LTV;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public Integer getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(Integer institutionId) {
        this.institutionId = institutionId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() { return userName; }

    public void setUserName(String userName){
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public Set<Dealer> getDealers() {
        return dealers;
    }

    public void setDealers(Set<Dealer> dealers) {
        this.dealers = dealers;
    }

    public Set<Branch> getBranches() {
        return branches;
    }

    public void setBranches(Set<Branch> branches) {
        this.branches = branches;
    }

    public Set<String> getActions() {
        return actions;
    }

    public void setActions(Set<String> actions) {
        this.actions = actions;
    }

    public Set<Product> getProduct() {
        return product;
    }

    public void setProduct(Set<Product> product) {
        this.product = product;
    }

    public Hierarchy getHierarchy() {
        return hierarchy;
    }

    public void setHierarchy(Hierarchy hierarchy) {
        this.hierarchy = hierarchy;
    }

    public String getInstitutionImage() {
        return institutionImage;
    }

    public void setInstitutionImage(String institutionImage) {
        this.institutionImage = institutionImage;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public Integer getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Integer creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getPhoneExtension() {
        return phoneExtension;
    }

    public void setPhoneExtension(String phoneExtension) {
        this.phoneExtension = phoneExtension;
    }

    public List<Branch> getBaseBranches() {
        return baseBranches;
    }

    public void setBaseBranches(List<Branch> baseBranches) {
        this.baseBranches = baseBranches;
    }

    public Set<Product> getSubProductList() {
        return subProductList;
    }

    public void setSubProductList(Set<Product> subProductList) {
        this.subProductList = subProductList;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getSapCode() {
        return sapCode;
    }

    public void setSapCode(String sapCode) {
        this.sapCode = sapCode;
    }

    public Set<RoleDetail> getRolesInfo() {
        return rolesInfo;
    }

    public void setRolesInfo(Set<RoleDetail> rolesInfo) {
        this.rolesInfo = rolesInfo;
    }

    public Set<CreditLimit> getProductCreditLimit() {
        return productCreditLimit;
    }

    public void setProductCreditLimit(Set<CreditLimit> productCreditLimit) {
        this.productCreditLimit = productCreditLimit;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {

        final StringBuilder sb = new StringBuilder("LoginServiceResponse{");
        sb.append("loginId='").append(loginId).append('\'');
        sb.append(", institutionId=").append(institutionId);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", employeeId='").append(employeeId).append('\'');
        sb.append(", mobile='").append(mobile).append('\'');
        sb.append(", active=").append(active);
        sb.append(", roles=").append(roles);
        sb.append(", dealers=").append(dealers);
        sb.append(", branches=").append(branches);
        sb.append(", actions=").append(actions);
        sb.append(", product=").append(product);
        sb.append(", hierarchy=").append(hierarchy);
        sb.append(", institutionImage='").append(institutionImage).append('\'');
        sb.append(", colorCode='").append(colorCode).append('\'');
        sb.append(", creditAmount='").append(creditAmount).append('\'');
        sb.append(", productCreditLimit=").append(productCreditLimit).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
