package com.softcell.gonogo.model.security;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

/**
 * @author yogeshb
 */
@Document(collection = "AppVersionControl")
public class Versions extends AuditEntity {
    @Id
    private String institution;
    private Set<String> versionName;

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Set<String> getVersionName() {
        return versionName;
    }

    public void setVersionName(Set<String> versionName) {
        this.versionName = versionName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Verions [institution=");
        builder.append(institution);
        builder.append(", versionName=");
        builder.append(versionName);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((institution == null) ? 0 : institution.hashCode());
        result = prime * result
                + ((versionName == null) ? 0 : versionName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Versions))
            return false;
        Versions other = (Versions) obj;
        if (institution == null) {
            if (other.institution != null)
                return false;
        } else if (!institution.equals(other.institution))
            return false;
        if (versionName == null) {
            if (other.versionName != null)
                return false;
        } else if (!versionName.equals(other.versionName))
            return false;
        return true;
    }
}
