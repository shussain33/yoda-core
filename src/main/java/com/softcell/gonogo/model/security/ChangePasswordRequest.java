package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * It will be use to change password for user
 *
 * @author bhuvneshk
 */
public class ChangePasswordRequest {

    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sUserName")
    @NotEmpty(groups = {ChangePasswordRequest.FetchGrp.class, ChangePasswordRequest.ResetGrp.class, ChangePasswordRequest.ResetGrpV3.class})
    private String userName;

    @JsonProperty("sOldPassword")
    @NotEmpty(groups = {ChangePasswordRequest.FetchGrp.class})
    private String oldpassword;

    @JsonProperty("sNewPassword")
    @NotEmpty(groups = {ChangePasswordRequest.FetchGrp.class})
    private String newPassword;

    @JsonProperty("sInstituteName")
    @NotEmpty(groups = {ChangePasswordRequest.ResetGrpV3.class})
    private String instituteName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ChangePasswordRequest [userName=");
        builder.append(userName);
        builder.append(", oldpassword=");
        builder.append(oldpassword);
        builder.append(", newPassword=");
        builder.append(newPassword);
        builder.append(", header=");
        builder.append(header);
        builder.append("]");
        return builder.toString();
    }

    public interface FetchGrp {
    }

    public interface ResetGrp {
    }

    public interface ResetGrpV3 {
    }
}
