package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Created by bhuvneshk on 25/5/17.
 */

public class ChangePasswordServiceDomainV2 {

    @JsonProperty("sLoginId")
    private String loginId;

    @JsonProperty("sOldPassword")
    private String oldPassword;

    @JsonProperty("sNewPassword")
    private String newPassword;

    @JsonProperty("iInstId")
    private String institutionId;

    @JsonProperty("sInstituteName")
    private String instituteName;

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChangePasswordServiceDomainV2)) return false;
        ChangePasswordServiceDomainV2 that = (ChangePasswordServiceDomainV2) o;
        return Objects.equals(getLoginId(), that.getLoginId()) &&
                Objects.equals(getOldPassword(), that.getOldPassword()) &&
                Objects.equals(getNewPassword(), that.getNewPassword()) &&
                Objects.equals(getInstitutionId(), that.getInstitutionId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLoginId(), getOldPassword(), getNewPassword(), getInstitutionId());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChangePasswordServiceDomainV2{");
        sb.append("loginId='").append(loginId).append('\'');
        sb.append(", oldPassword='").append(oldPassword).append('\'');
        sb.append(", newPassword='").append(newPassword).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
