package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class LogoutRequestV2 {

    @JsonProperty("sLoginId")
    @NotEmpty(message = "LoginId is compulsory Field!!", groups = {ChangePasswordGrp.class, ResetPasswordGrp.class,
            ChangeUserPasswordGrp.class, ChangeUserIdGrp.class, ResetPasswordGrpV2.class, LogoutWebV2Grp.class,CpvLogoutGrp.class,
            CpvChangePasswordGrp.class,CpvChangeUserPasswordGrp.class,ResetCpvPasswordGrp.class,resetUserPasswordGrp.class})
    private String loginId;

    @JsonProperty("sOldPassword")
    @NotEmpty(message = "Old Password is compulsory Field!!", groups = {ChangePasswordGrp.class,CpvChangePasswordGrp.class})
    private String oldPassword;

    @JsonProperty("sNewPassword")
    @NotEmpty(message = "New Password is compulsory Field!!", groups = {ChangePasswordGrp.class, ChangeUserPasswordGrp.class,CpvChangePasswordGrp.class,CpvChangeUserPasswordGrp.class,resetUserPasswordGrp.class})
    private String newPassword;

    @JsonProperty("sNewLoginId")
    @NotEmpty(message = "New user id is compulsory Field!!", groups = {ChangeUserIdGrp.class})
    private String newLoginId;

    @JsonProperty("iInstId")
    @NotNull(message = "Institution Id is compulsory Field!!", groups = {ChangePasswordGrp.class,
            ChangeUserIdGrp.class, LogoutWebV2Grp.class,CpvLogoutGrp.class,CpvChangePasswordGrp.class,CpvChangeUserPasswordGrp.class, ResetPasswordGrp.class,resetUserPasswordGrp.class})
    private Integer institutionId;

    @JsonProperty("sInstitutionName")
    @NotEmpty(message = "Institute name must not be blank !!", groups = {LogoutRequestV2.ResetPasswordGrpV2.class,CpvLogoutGrp.class,ResetCpvPasswordGrp.class})
    private String instituteName;

    @JsonProperty("sApplication")
    @NotEmpty(message = "Application name must not be blank !!")
    private String application;

    public interface ChangePasswordGrp {
    }


    public interface ResetPasswordGrp {
    }


    public interface ChangeUserPasswordGrp {
    }

    public interface ChangeUserIdGrp {
    }

    public interface ResetPasswordGrpV2 {
    }

    public interface LogoutWebV2Grp {}

    public interface CpvLogoutGrp {}

    public interface ResetCpvPasswordGrp { }

    public  interface CpvChangePasswordGrp{}

    public  interface CpvChangeUserPasswordGrp{}

    public  interface resetUserPasswordGrp{}
}
