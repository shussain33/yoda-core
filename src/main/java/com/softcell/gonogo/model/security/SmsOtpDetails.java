/**
 * yogeshb12:36:18 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

/**
 * @author yogeshb
 */
public class SmsOtpDetails {

    @JsonProperty("MOBILE-NUMBER")
    @NotBlank(groups = {GetOtpRequest.FetchGrp.class})
    @Pattern(regexp="(^$|[0-9]{10,12})",groups = {GetOtpRequest.FetchGrp.class})
    private String mobileNumber;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SmsOtpDetails{");
        sb.append("mobileNumber='").append(mobileNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SmsOtpDetails)) return false;
        SmsOtpDetails that = (SmsOtpDetails) o;
        return Objects.equal(getMobileNumber(), that.getMobileNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getMobileNumber());
    }
}
