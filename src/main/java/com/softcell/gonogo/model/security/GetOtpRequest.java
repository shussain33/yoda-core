/**
 * yogeshb12:24:52 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * @author yogeshb
 */
public class GetOtpRequest {
    @JsonProperty("USER_ID")
    private String userId;

    @JsonProperty("PASSWORD")
    private String password;

    @JsonProperty("INSTITUTION_ID")
    private String institutionId;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("inputJson_")
    @NotNull(groups = {GetOtpRequest.FetchGrp.class})
    @Valid
    private SmsOtpDetails otpDetails;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public SmsOtpDetails getOtpDetails() {
        return otpDetails;
    }

    public void setOtpDetails(SmsOtpDetails otpDetails) {
        this.otpDetails = otpDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetOtpRequest)) return false;
        GetOtpRequest that = (GetOtpRequest) o;
        return Objects.equal(getUserId(), that.getUserId()) &&
                Objects.equal(getPassword(), that.getPassword()) &&
                Objects.equal(getInstitutionId(), that.getInstitutionId()) &&
                Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getOtpDetails(), that.getOtpDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUserId(), getPassword(), getInstitutionId(), getHeader(), getOtpDetails());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GetOtpRequest{");
        sb.append("userId='").append(userId).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", header=").append(header);
        sb.append(", otpDetails=").append(otpDetails);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {
    }


}
