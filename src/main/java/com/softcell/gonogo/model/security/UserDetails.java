package com.softcell.gonogo.model.security;


import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;


@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDetails {

    @JsonProperty("STATE")
    private String sTATE;
    @JsonProperty("USER_ID")
    private String uSERID;
    @JsonProperty("DOB")
    private String dOB;
    @JsonProperty("ADDRESS")
    private String aDDRESS;
    @JsonProperty("INSTITUTION_ID")
    private String iNSTITUTIONID;
    @JsonProperty("DOJ")
    private String dOJ;
    @JsonProperty("USER_IMAGE")
    private String uSERIMAGE;
    @JsonProperty("MOBILE")
    private String mOBILE;
    @JsonProperty("GENDER")
    private String gENDER;
    @JsonProperty("INSTITUTE_IMAGE")
    private String iNSTITUTEIMAGE;
    @JsonProperty("USER_NAME")
    private String uSERNAME;
    @JsonProperty("EMAIL")
    private String eMAIL;
    @JsonProperty("PIN")
    private Integer pIN;
    @JsonProperty("CITY")
    private String cITY;
    @JsonProperty("COLOR")
    private String cOLOR;
    @JsonProperty("FIRST_NAME")
    private String firstName;
    @JsonProperty("LAST_NAME")
    private String lastName;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The sTATE
     */
    @JsonProperty("STATE")
    public String getSTATE() {
        return sTATE;
    }

    /**
     * @param sTATE The STATE
     */
    @JsonProperty("STATE")
    public void setSTATE(String sTATE) {
        this.sTATE = sTATE;
    }

    /**
     * @return The uSERID
     */
    @JsonProperty("USER_ID")
    public String getUSERID() {
        return uSERID;
    }

    /**
     * @param uSERID The USER_ID
     */
    @JsonProperty("USER_ID")
    public void setUSERID(String uSERID) {
        this.uSERID = uSERID;
    }

    /**
     * @return The dOB
     */
    @JsonProperty("DOB")
    public String getDOB() {
        return dOB;
    }

    /**
     * @param dOB The DOB
     */
    @JsonProperty("DOB")
    public void setDOB(String dOB) {
        this.dOB = dOB;
    }

    /**
     * @return The aDDRESS
     */
    @JsonProperty("ADDRESS")
    public String getADDRESS() {
        return aDDRESS;
    }

    /**
     * @param aDDRESS The ADDRESS
     */
    @JsonProperty("ADDRESS")
    public void setADDRESS(String aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    /**
     * @return The iNSTITUTIONID
     */
    @JsonProperty("INSTITUTION_ID")
    public String getINSTITUTIONID() {
        return iNSTITUTIONID;
    }

    /**
     * @param iNSTITUTIONID The INSTITUTION_ID
     */
    @JsonProperty("INSTITUTION_ID")
    public void setINSTITUTIONID(String iNSTITUTIONID) {
        this.iNSTITUTIONID = iNSTITUTIONID;
    }

    /**
     * @return The dOJ
     */
    @JsonProperty("DOJ")
    public String getDOJ() {
        return dOJ;
    }

    /**
     * @param dOJ The DOJ
     */
    @JsonProperty("DOJ")
    public void setDOJ(String dOJ) {
        this.dOJ = dOJ;
    }

    /**
     * @return The uSERIMAGE
     */
    @JsonProperty("USER_IMAGE")
    public String getUSERIMAGE() {
        return uSERIMAGE;
    }

    /**
     * @param uSERIMAGE The USER_IMAGE
     */
    @JsonProperty("USER_IMAGE")
    public void setUSERIMAGE(String uSERIMAGE) {
        this.uSERIMAGE = uSERIMAGE;
    }

    /**
     * @return The mOBILE
     */
    @JsonProperty("MOBILE")
    public String getMOBILE() {
        return mOBILE;
    }

    /**
     * @param mOBILE The MOBILE
     */
    @JsonProperty("MOBILE")
    public void setMOBILE(String mOBILE) {
        this.mOBILE = mOBILE;
    }

    /**
     * @return The gENDER
     */
    @JsonProperty("GENDER")
    public String getGENDER() {
        return gENDER;
    }

    /**
     * @param gENDER The GENDER
     */
    @JsonProperty("GENDER")
    public void setGENDER(String gENDER) {
        this.gENDER = gENDER;
    }

    /**
     * @return The iNSTITUTEIMAGE
     */
    @JsonProperty("INSTITUTE_IMAGE")
    public String getINSTITUTEIMAGE() {
        return iNSTITUTEIMAGE;
    }

    /**
     * @param iNSTITUTEIMAGE The INSTITUTE_IMAGE
     */
    @JsonProperty("INSTITUTE_IMAGE")
    public void setINSTITUTEIMAGE(String iNSTITUTEIMAGE) {
        this.iNSTITUTEIMAGE = iNSTITUTEIMAGE;
    }

    /**
     * @return The uSERNAME
     */
    @JsonProperty("USER_NAME")
    public String getUSERNAME() {
        return uSERNAME;
    }

    /**
     * @param uSERNAME The USER_NAME
     */
    @JsonProperty("USER_NAME")
    public void setUSERNAME(String uSERNAME) {
        this.uSERNAME = uSERNAME;
    }

    /**
     * @return The eMAIL
     */
    @JsonProperty("EMAIL")
    public String getEMAIL() {
        return eMAIL;
    }

    /**
     * @param eMAIL The EMAIL
     */
    @JsonProperty("EMAIL")
    public void setEMAIL(String eMAIL) {
        this.eMAIL = eMAIL;
    }

    /**
     * @return The pIN
     */
    @JsonProperty("PIN")
    public Integer getPIN() {
        return pIN;
    }

    /**
     * @param pIN The PIN
     */
    @JsonProperty("PIN")
    public void setPIN(Integer pIN) {
        this.pIN = pIN;
    }

    /**
     * @return The cITY
     */
    @JsonProperty("CITY")
    public String getCITY() {
        return cITY;
    }

    /**
     * @param cITY The CITY
     */
    @JsonProperty("CITY")
    public void setCITY(String cITY) {
        this.cITY = cITY;
    }

    /**
     * @return The cOLOR
     */
    @JsonProperty("COLOR")
    public String getCOLOR() {
        return cOLOR;
    }

    /**
     * @param cOLOR The COLOR
     */
    @JsonProperty("COLOR")
    public void setCOLOR(String cOLOR) {
        this.cOLOR = cOLOR;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
