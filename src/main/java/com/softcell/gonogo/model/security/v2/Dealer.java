package com.softcell.gonogo.model.security.v2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by bhuvneshk on 13/6/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Dealer implements Serializable {

    @Id
    private String id;

    @NotNull
    @JsonProperty("iInstId")
    private Integer institutionId;

    @NotNull
    @JsonProperty("iDlrId")
    private String dealerId;

    @NotNull
    @JsonProperty("sDlrName")
    private String dealerName;

    @NotNull
    @JsonProperty("iBranchId")
    private Integer branchId;

    @NotNull
    @JsonProperty("sBranchName")
    private String branchName;

    @NotNull
    @JsonProperty("sProduct")
    private String product;

    @NotNull
    @JsonProperty("bActive")
    private boolean active;

    @JsonProperty("sVerificationType")
    private String verificationType;
}
