package com.softcell.gonogo.model.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Branch {

    @JsonProperty("BRANCH_CODE")
    private String branchCode;

    @JsonProperty("BRANCH_NAME")
    private String branchName;


    /**
     * @return the branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * @param branchCode the branchCode to set
     */
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Branch [branchCode=" + branchCode + ", branchName="
                + branchName + "]";
    }


}
