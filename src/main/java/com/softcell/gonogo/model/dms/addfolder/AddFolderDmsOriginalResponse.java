package com.softcell.gonogo.model.dms.addfolder;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 21/6/17.
 */
public class AddFolderDmsOriginalResponse {

    @JsonProperty("sLosId")
    private String losId;

    @JsonProperty("sErrorMsg")
    private String errorMessage;

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("sFolderName")
    private String folderName;

    @JsonProperty("sFolderType")
    private String folderType;

    @JsonProperty("lParentFolderIndex")
    private long parentFolderIndex;

    @JsonProperty("sAccessType")
    private String accessType;

    @JsonProperty("lFolderIndex")
    private long folderIndex;

    @JsonProperty("sOwner")
    private String owner;

    @JsonProperty("sOwnerIndex")
    private long ownerIndex;

    @JsonProperty("sCreationDateTime")
    private String creationDateTime;


    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderType() {
        return folderType;
    }

    public void setFolderType(String folderType) {
        this.folderType = folderType;
    }

    public long getParentFolderIndex() {
        return parentFolderIndex;
    }

    public void setParentFolderIndex(long parentFolderIndex) {
        this.parentFolderIndex = parentFolderIndex;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public long getFolderIndex() {
        return folderIndex;
    }

    public void setFolderIndex(long folderIndex) {
        this.folderIndex = folderIndex;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getOwnerIndex() {
        return ownerIndex;
    }

    public void setOwnerIndex(long ownerIndex) {
        this.ownerIndex = ownerIndex;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }
}
