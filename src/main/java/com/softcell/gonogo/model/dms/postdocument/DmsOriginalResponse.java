package com.softcell.gonogo.model.dms.postdocument;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 19/6/17.
 */
public class DmsOriginalResponse {

    @JsonProperty("iStatusCode")
    private int statusCode;

    @JsonProperty("sDmsMessage")
    private String dmsMessage;

    @JsonProperty("sAuthor")
    private String author;

    @JsonProperty("iDocumentIndex")
    private int documentIndex;

    @JsonProperty("sDocumentLock")
    private String documentLock;

    @JsonProperty("sDocumentName")
    private String documentName;

    @JsonProperty("iDocumentSize")
    private int documentSize;

    @JsonProperty("sDocumentType")
    private String documentType;

    @JsonProperty("fDocumentVersionNo")
    private float documentVersionNo;

    @JsonProperty("iParentFolderIndex")
    private int parentFolderIndex;

    @JsonProperty("dtCreatedDate")
    private String createdDate;


    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getDmsMessage() {
        return dmsMessage;
    }

    public void setDmsMessage(String dmsMessage) {
        this.dmsMessage = dmsMessage;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getDocumentIndex() {
        return documentIndex;
    }

    public void setDocumentIndex(int documentIndex) {
        this.documentIndex = documentIndex;
    }

    public String getDocumentLock() {
        return documentLock;
    }

    public void setDocumentLock(String documentLock) {
        this.documentLock = documentLock;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public int getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(int documentSize) {
        this.documentSize = documentSize;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public float getDocumentVersionNo() {
        return documentVersionNo;
    }

    public void setDocumentVersionNo(float documentVersionNo) {
        this.documentVersionNo = documentVersionNo;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getParentFolderIndex() {
        return parentFolderIndex;
    }

    public void setParentFolderIndex(int parentFolderIndex) {
        this.parentFolderIndex = parentFolderIndex;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DmsOriginalResponse{");
        sb.append("statusCode=").append(statusCode);
        sb.append(", dmsMessage='").append(dmsMessage).append('\'');
        sb.append(", author='").append(author).append('\'');
        sb.append(", documentIndex=").append(documentIndex);
        sb.append(", documentLock='").append(documentLock).append('\'');
        sb.append(", documentName='").append(documentName).append('\'');
        sb.append(", documentSize=").append(documentSize);
        sb.append(", documentType='").append(documentType).append('\'');
        sb.append(", documentVersionNo=").append(documentVersionNo);
        sb.append(", parentFolderIndex=").append(parentFolderIndex);
        sb.append(", createdDate='").append(createdDate).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
