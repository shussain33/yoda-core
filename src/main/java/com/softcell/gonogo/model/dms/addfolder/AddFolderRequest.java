package com.softcell.gonogo.model.dms.addfolder;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 21/6/17.
 */
public class AddFolderRequest {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sLosId")
    private String losId;

    @JsonProperty("sFolderName")
    private String folderName;

    @JsonProperty("sFolderType")
    private String folderType;

    @JsonProperty("lParentFolderIndex")
    private long parentFolderIndex;

    @JsonProperty("sAccessType")
    private String accessType;

    @JsonProperty("sUserDbId")
    private String userDbId;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderType() {
        return folderType;
    }

    public void setFolderType(String folderType) {
        this.folderType = folderType;
    }

    public long getParentFolderIndex() {
        return parentFolderIndex;
    }

    public void setParentFolderIndex(long parentFolderIndex) {
        this.parentFolderIndex = parentFolderIndex;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    public String getUserDbId() {
        return userDbId;
    }

    public void setUserDbId(String userDbId) {
        this.userDbId = userDbId;
    }
}
