package com.softcell.gonogo.model.dms.connectcabinet;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by mahesh on 22/6/17.
 */
public class ConnectCabinetResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("oConnectCabinetOriginalResponse")
    private ConnectCabinetOriginalResponse connectCabinetOriginalResponse;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public ConnectCabinetOriginalResponse getConnectCabinetOriginalResponse() {
        return connectCabinetOriginalResponse;
    }

    public void setConnectCabinetOriginalResponse(ConnectCabinetOriginalResponse connectCabinetOriginalResponse) {
        this.connectCabinetOriginalResponse = connectCabinetOriginalResponse;
    }


    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        ConnectCabinetResponse connectCabinetResponse = new ConnectCabinetResponse();

        public ConnectCabinetResponse build() {
            return connectCabinetResponse;
        }

        public Builder status(String status) {
            this.connectCabinetResponse.status = status;
            return this;
        }

        public Builder error(ThirdPartyException error) {
            this.connectCabinetResponse.error = error;
            return this;
        }

        public Builder connectCabinetOriginalResponse(ConnectCabinetOriginalResponse connectCabinetOriginalResponse) {
            this.connectCabinetResponse.connectCabinetOriginalResponse = connectCabinetOriginalResponse;
            return this;
        }
    }
}
