package com.softcell.gonogo.model.dms.addfolder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by mahesh on 21/6/17.
 */
public class AddFolderResult {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("oAddFolderDmsOriginalResponse")
    AddFolderDmsOriginalResponse addFolderDmsOriginalResponse;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public AddFolderDmsOriginalResponse getAddFolderDmsOriginalResponse() {
        return addFolderDmsOriginalResponse;
    }

    public void setAddFolderDmsOriginalResponse(AddFolderDmsOriginalResponse addFolderDmsOriginalResponse) {
        this.addFolderDmsOriginalResponse = addFolderDmsOriginalResponse;
    }

    public static Builder builder() {
        return new Builder();
    }


    public static class Builder {

        AddFolderResult addFolderResult = new AddFolderResult();

        public AddFolderResult build() {
            return addFolderResult;
        }

        public Builder status(String status) {
            this.addFolderResult.status = status;
            return this;
        }

        public Builder error(ThirdPartyException error) {
            this.addFolderResult.error = error;
            return this;
        }

        public Builder addFolderDmsOriginalResponse(AddFolderDmsOriginalResponse addFolderDmsOriginalResponse) {
            this.addFolderResult.addFolderDmsOriginalResponse = addFolderDmsOriginalResponse;
            return this;
        }

    }
}
