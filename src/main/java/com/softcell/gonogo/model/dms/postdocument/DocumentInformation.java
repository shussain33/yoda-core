package com.softcell.gonogo.model.dms.postdocument;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by mahesh on 28/6/17.
 */
public class DocumentInformation {

    /**
     * this is the Documents objectId in GoNoGo.
     */
    @JsonProperty("sDocObjectId")
    private String docObjectId;

    @JsonProperty("sDocumentName")
    private String documentName;

    @JsonProperty("iDocumentId")
    private int documentId;

    @JsonProperty("sDocumentType")
    private String documentType;

    @JsonProperty("sParentFolderId")
    private int parentFolderId;

    @JsonProperty("iDocumentSize")
    private int documentSize;

    @JsonProperty("sParentFolderName")
    private String parentFolderName;

    @JsonProperty("sDocPushStatus")
    private String docPushStatus;

    @JsonProperty("sDocPushStatusCode")
    private String docPushStatusCode;

    @JsonProperty("sFailedReason")
    private String failedReason;

    @JsonProperty("dtCreateDt")
    private Date createDate;

    @JsonProperty("dtUpdateDt")
    private Date updateDate;


    public int getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(int documentSize) {
        this.documentSize = documentSize;
    }

    public String getDocObjectId() {
        return docObjectId;
    }

    public void setDocObjectId(String docObjectId) {
        this.docObjectId = docObjectId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public int getDocumentId() {
        return documentId;
    }

    public void setDocumentId(int documentId) {
        this.documentId = documentId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public int getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(int parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDocPushStatus() {
        return docPushStatus;
    }

    public void setDocPushStatus(String docPushStatus) {
        this.docPushStatus = docPushStatus;
    }

    public String getFailedReason() {
        return failedReason;
    }

    public void setFailedReason(String failedReason) {
        this.failedReason = failedReason;
    }

    public String getDocPushStatusCode() {
        return docPushStatusCode;
    }

    public void setDocPushStatusCode(String docPushStatusCode) {
        this.docPushStatusCode = docPushStatusCode;
    }

    public String getParentFolderName() {
        return parentFolderName;
    }

    public void setParentFolderName(String parentFolderName) {
        this.parentFolderName = parentFolderName;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
