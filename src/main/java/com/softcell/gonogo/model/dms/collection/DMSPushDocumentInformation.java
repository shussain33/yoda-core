package com.softcell.gonogo.model.dms.collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.dms.postdocument.DocumentInformation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by mahesh on 25/6/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "dMSPushDocumentInformation")
public class DMSPushDocumentInformation {

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("iTotalDocCount")
    private int totalNoOfDocumentCount;

    @JsonProperty("iFailedDocCount")
    private int failedDocumentCount;

    @JsonProperty("iSuccessDocCount")
    private int successDocumentCount;

    @JsonProperty("aFailedDocInfo")
    List<DocumentInformation> failedDocumentInformation;

    @JsonProperty("aSuccessDocInfo")
    List<DocumentInformation> successDocumentInformation;

}
