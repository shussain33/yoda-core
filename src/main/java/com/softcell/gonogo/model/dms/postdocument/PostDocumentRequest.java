package com.softcell.gonogo.model.dms.postdocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.dms.additionaldoc.DmsAdditionalInformation;

import java.util.Set;

/**
 * Created by vinod on 12/6/17.
 */
public class PostDocumentRequest {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sBase64Doc")
    private String base64Document;

    @JsonProperty("sDocExt")
    private String documentExtension;

    @JsonProperty("sDocName")
    private String documentName;

    @JsonProperty("sDocSize")
    private String documentSize;

    @JsonProperty("iFolderIndex")
    private int folderIndex;

    @JsonProperty("iVolumeId")
    private int volumeId;

    @JsonProperty("sUserDbId")
    private String userDbId;

    @JsonProperty("sLosId")
    private String losId;

    /**
     * This doc type currently referred to DMS doc classification type(eg. Sourcing or Underwriter)
     */

    @JsonProperty("sDocType")
    private String documentType;

    @JsonProperty("iNoOfPages")
    private int noOfPages;

    /**
     * this object contain the information like pan number ,adhhar card number ,product , driving licence number etc
     * depending on the document type i.e. sourcing,underwriting ,disbursal
     */
    @JsonProperty("aDmsAdditionalInformations")
    Set<DmsAdditionalInformation> dmsAdditionalInformations;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getBase64Document() {
        return base64Document;
    }

    public void setBase64Document(String base64Document) {
        this.base64Document = base64Document;
    }

    public String getDocumentExtension() {
        return documentExtension;
    }

    public void setDocumentExtension(String documentExtension) {
        this.documentExtension = documentExtension;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getDocumentSize() {
        return documentSize;
    }

    public void setDocumentSize(String documentSize) {
        this.documentSize = documentSize;
    }

    public int getFolderIndex() {
        return folderIndex;
    }

    public void setFolderIndex(int folderIndex) {
        this.folderIndex = folderIndex;
    }

    public int getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(int volumeId) {
        this.volumeId = volumeId;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    public String getUserDbId() {
        return userDbId;
    }

    public void setUserDbId(String userDbId) {
        this.userDbId = userDbId;
    }

    public int getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(int noOfPages) {
        this.noOfPages = noOfPages;
    }

    public Set<DmsAdditionalInformation> getDmsAdditionalInformations() {
        return dmsAdditionalInformations;
    }

    public void setDmsAdditionalInformations(Set<DmsAdditionalInformation> dmsAdditionalInformations) {
        this.dmsAdditionalInformations = dmsAdditionalInformations;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PostDocumentRequest{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", base64Document='").append(base64Document).append('\'');
        sb.append(", documentExtension='").append(documentExtension).append('\'');
        sb.append(", documentName='").append(documentName).append('\'');
        sb.append(", documentSize='").append(documentSize).append('\'');
        sb.append(", folderIndex=").append(folderIndex);
        sb.append(", volumeId=").append(volumeId);
        sb.append(", userDbId='").append(userDbId).append('\'');
        sb.append(", losId='").append(losId).append('\'');
        sb.append(", documentType='").append(documentType).append('\'');
        sb.append(", noOfPages=").append(noOfPages);
        sb.append(", dmsAdditionalInformations=").append(dmsAdditionalInformations);
        sb.append('}');
        return sb.toString();
    }
}
