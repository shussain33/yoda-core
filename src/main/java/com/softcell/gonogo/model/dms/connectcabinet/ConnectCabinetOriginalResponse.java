package com.softcell.gonogo.model.dms.connectcabinet;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 22/6/17.
 */
public class ConnectCabinetOriginalResponse {

    @JsonProperty("sErrorMsg")
    private String errorMessage;

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("sCabinetName")
    private String cabinetName;

    @JsonProperty("sUserDbId")
    private int userDbId;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getCabinetName() {
        return cabinetName;
    }

    public void setCabinetName(String cabinetName) {
        this.cabinetName = cabinetName;
    }

    public int getUserDbId() {
        return userDbId;
    }

    public void setUserDbId(int userDbId) {
        this.userDbId = userDbId;
    }
}
