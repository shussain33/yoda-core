package com.softcell.gonogo.model.dms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;
import com.softcell.gonogo.model.dms.searchfolder.FolderInformation;

import java.util.List;

/**
 * Created by mahesh on 3/7/17.
 */
public class DMSLogResponse {


    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("aFolderInfoList")
    private List<FolderInformation> folderInformationList;

    @JsonProperty("oDocumentInfo")
    private DMSPushDocumentInformation dmsPushDocumentInformation;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public List<FolderInformation> getFolderInformationList() {
        return folderInformationList;
    }

    public void setFolderInformationList(List<FolderInformation> folderInformationList) {
        this.folderInformationList = folderInformationList;
    }

    public DMSPushDocumentInformation getDmsPushDocumentInformation() {
        return dmsPushDocumentInformation;
    }

    public void setDmsPushDocumentInformation(DMSPushDocumentInformation dmsPushDocumentInformation) {
        this.dmsPushDocumentInformation = dmsPushDocumentInformation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DMSLogResponse{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", folderInformationList=").append(folderInformationList);
        sb.append(", dmsPushDocumentInformation=").append(dmsPushDocumentInformation);
        sb.append('}');
        return sb.toString();
    }
}
