package com.softcell.gonogo.model.dms.searchfolder;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by mahesh on 21/6/17.
 */
public class FolderInformation {

    @JsonProperty("lFolderIndex")
    private long folderIndex;

    @JsonProperty("sFolderType")
    private String folderType;

    @JsonProperty("sFolderName")
    private String folderName;

    @JsonProperty("lNoOfSubfolder")
    private long noOfSubFolder;

    @JsonProperty("lNoOfDocument")
    private long noOfDocument;

    @JsonProperty("lParentFolderIndex")
    private long parentFolderIndex;

    @JsonProperty("sOwner")
    private String owner;

    @JsonProperty("lOwnerIndex")
    private long ownerIndex;

    @JsonProperty("sInsertDt")
    private Date insertDate =new Date();

    @JsonProperty("dtUpdateDt")
    private Date updateDate;

    public long getFolderIndex() {
        return folderIndex;
    }

    public void setFolderIndex(long folderIndex) {
        this.folderIndex = folderIndex;
    }

    public String getFolderType() {
        return folderType;
    }

    public void setFolderType(String folderType) {
        this.folderType = folderType;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public long getNoOfSubFolder() {
        return noOfSubFolder;
    }

    public void setNoOfSubFolder(long noOfSubFolder) {
        this.noOfSubFolder = noOfSubFolder;
    }

    public long getNoOfDocument() {
        return noOfDocument;
    }

    public void setNoOfDocument(long noOfDocument) {
        this.noOfDocument = noOfDocument;
    }

    public long getParentFolderIndex() {
        return parentFolderIndex;
    }

    public void setParentFolderIndex(long parentFolderIndex) {
        this.parentFolderIndex = parentFolderIndex;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getOwnerIndex() {
        return ownerIndex;
    }

    public void setOwnerIndex(long ownerIndex) {
        this.ownerIndex = ownerIndex;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
