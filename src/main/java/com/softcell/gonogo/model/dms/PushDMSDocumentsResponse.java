package com.softcell.gonogo.model.dms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;

/**
 * Created by mahesh on 24/6/17.
 */
public class PushDMSDocumentsResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResponseMsg")
    private String responseMsg;

    @JsonProperty("oDmsPushDocumentInformation")
    private DMSPushDocumentInformation dmsPushDocumentInformation;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public DMSPushDocumentInformation getDmsPushDocumentInformation() {
        return dmsPushDocumentInformation;
    }

    public void setDmsPushDocumentInformation(DMSPushDocumentInformation dmsPushDocumentInformation) {
        this.dmsPushDocumentInformation = dmsPushDocumentInformation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PushDMSDocumentsResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", responseMsg='").append(responseMsg).append('\'');
        sb.append(", dmsPushDocumentInformation=").append(dmsPushDocumentInformation);
        sb.append('}');
        return sb.toString();
    }
}
