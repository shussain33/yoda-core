package com.softcell.gonogo.model.dms.searchfolder;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 19/6/17.
 */
public class SearchFolderResult {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private Error error;

    @JsonProperty("oSearchFolderOriginalResponse")
    SearchFolderOriginalResponse  searchFolderOriginalResponse;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public static Builder builder() {
        return new Builder();
    }

    public SearchFolderOriginalResponse getSearchFolderOriginalResponse() {
        return searchFolderOriginalResponse;
    }

    public void setSearchFolderOriginalResponse(SearchFolderOriginalResponse searchFolderOriginalResponse) {
        this.searchFolderOriginalResponse = searchFolderOriginalResponse;
    }

    public static class Builder {

        SearchFolderResult searchFolderResult = new SearchFolderResult();

        public SearchFolderResult build() {
            return searchFolderResult;
        }

        public Builder status(String status) {
            this.searchFolderResult.status = status;
            return this;
        }

        public Builder error(Error error) {
            this.searchFolderResult.error = error;
            return this;
        }

        public Builder searchFolderOriginalResponse(SearchFolderOriginalResponse searchFolderOriginalResponse) {
            this.searchFolderResult.searchFolderOriginalResponse = searchFolderOriginalResponse;
            return this;
        }

    }
}
