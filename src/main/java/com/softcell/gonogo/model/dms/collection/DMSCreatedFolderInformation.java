package com.softcell.gonogo.model.dms.collection;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.dms.searchfolder.FolderInformation;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by mahesh on 25/6/17.
 */
@Document(collection = "dMSCreatedFolderInformation")
public class DMSCreatedFolderInformation {

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("aFolderInformation")
    private List<FolderInformation> folderInformation;


    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }


    public List<FolderInformation> getFolderInformation() {
        return folderInformation;
    }

    public void setFolderInformation(List<FolderInformation> folderInformation) {
        this.folderInformation = folderInformation;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DMSCreatedFolderInformation{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", folderInformation=").append(folderInformation);
        sb.append('}');
        return sb.toString();
    }
}
