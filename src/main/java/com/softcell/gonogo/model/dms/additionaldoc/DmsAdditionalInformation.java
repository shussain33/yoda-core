package com.softcell.gonogo.model.dms.additionaldoc;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahesh on 29/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DmsAdditionalInformation {

    @JsonProperty("iIndexId")
    private int indexId;

    @JsonProperty("sIndexType")
    private String indexType;

    @JsonProperty("sIndexValue")
    private String indexValue;

    @JsonProperty("sInformationType")
    private String informationType;
}
