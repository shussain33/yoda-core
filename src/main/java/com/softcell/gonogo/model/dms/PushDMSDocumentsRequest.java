package com.softcell.gonogo.model.dms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.sms.Error;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 24/6/17.
 */
@Document(collection = "dmsErrorLog")
public class PushDMSDocumentsRequest {


    @JsonProperty("sRefId")
    @NotEmpty(groups = PushDMSDocumentsRequest.FetchGrp.class)
    private String refId;

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    private Header header;

    @JsonProperty("oErrorInfo")
    private Error error;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PushDMSDocumentsRequest{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", header=").append(header);
        sb.append(", error=").append(error);
        sb.append('}');
        return sb.toString();
    }


    public interface FetchGrp{

    }
}
