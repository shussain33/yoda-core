package com.softcell.gonogo.model.dms.connectcabinet;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 22/6/17.
 */
public class ConnectCabinetRequest {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sLosId")
    private String losId;

    /**
     * type of request i.e connect or disconnect
     */
    @JsonProperty("sRequestType")
    private String requestType;

    @JsonProperty("sCabinetName")
    private String cabinetName;

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("sUserPassword")
    private String userPassword;

    @JsonProperty("sUserDbId")
    private String userDbId;

    public String getCabinetName() {
        return cabinetName;
    }

    public void setCabinetName(String cabinetName) {
        this.cabinetName = cabinetName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getUserDbId() {
        return userDbId;
    }

    public void setUserDbId(String userDbId) {
        this.userDbId = userDbId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ConnectCabinetRequest{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", losId='").append(losId).append('\'');
        sb.append(", requestType='").append(requestType).append('\'');
        sb.append(", cabinetName='").append(cabinetName).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", userPassword='").append(userPassword).append('\'');
        sb.append(", userDbId='").append(userDbId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
