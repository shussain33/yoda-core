package com.softcell.gonogo.model.dms.searchfolder;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 21/6/17.
 */
public class SearchFolderOriginalResponse {

    @JsonProperty("sErrorMsg")
    private String errorMessage;

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("lTotalNoOfRecord")
    private long totalNoOfRecord;

    @JsonProperty("ltotalNoOfRecordFetch")
    private long totalNoOfRecordFetch;

    @JsonProperty("oFolderInformation")
    private FolderInformation folderInformation;


    public long getTotalNoOfRecord() {
        return totalNoOfRecord;
    }

    public void setTotalNoOfRecord(long totalNoOfRecord) {
        this.totalNoOfRecord = totalNoOfRecord;
    }

    public long getTotalNoOfRecordFetch() {
        return totalNoOfRecordFetch;
    }

    public void setTotalNoOfRecordFetch(long totalNoOfRecordFetch) {
        this.totalNoOfRecordFetch = totalNoOfRecordFetch;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public FolderInformation getFolderInformation() {
        return folderInformation;
    }

    public void setFolderInformation(FolderInformation folderInformation) {
        this.folderInformation = folderInformation;
    }
}
