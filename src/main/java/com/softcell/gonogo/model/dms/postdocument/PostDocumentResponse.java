package com.softcell.gonogo.model.dms.postdocument;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by vinod on 12/6/17.
 */
public class PostDocumentResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("oDmsOriginalResponse")
    private DmsOriginalResponse dmsOriginalResponse;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public DmsOriginalResponse getDmsOriginalResponse() {
        return dmsOriginalResponse;
    }

    public void setDmsOriginalResponse(DmsOriginalResponse dmsOriginalResponse) {
        this.dmsOriginalResponse = dmsOriginalResponse;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        PostDocumentResponse postDocumentResponse = new PostDocumentResponse();

        public PostDocumentResponse build() {
            return postDocumentResponse;
        }

        public Builder status(String status) {
            this.postDocumentResponse.status = status;
            return this;
        }

        public Builder error(ThirdPartyException error) {
            this.postDocumentResponse.error = error;
            return this;
        }

        public Builder originalResponse(DmsOriginalResponse dmsOriginalResponse) {
            this.postDocumentResponse.dmsOriginalResponse = dmsOriginalResponse;
            return this;
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PostDocumentResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", error=").append(error);
        sb.append(", dmsOriginalResponse=").append(dmsOriginalResponse);
        sb.append('}');
        return sb.toString();
    }
}
