package com.softcell.gonogo.model.dms.searchfolder;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 19/6/17.
 */
public class SearchFolderRequest {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sIndexValue")
    private String indexValue;

    @JsonProperty("iIndexId")
    private int indexId;

    @JsonProperty("sSearchInsideFolder")
    private String searchInsideFolder;

    @JsonProperty("sFolderType")
    private String folderType;


    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(String indexValue) {
        this.indexValue = indexValue;
    }

    public int getIndexId() {
        return indexId;
    }

    public void setIndexId(int indexId) {
        this.indexId = indexId;
    }

    public String getSearchInsideFolder() {
        return searchInsideFolder;
    }

    public void setSearchInsideFolder(String searchInsideFolder) {
        this.searchInsideFolder = searchInsideFolder;
    }

    public String getFolderType() {
        return folderType;
    }

    public void setFolderType(String folderType) {
        this.folderType = folderType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SearchFolderRequest{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", indexValue='").append(indexValue).append('\'');
        sb.append(", indexId=").append(indexId);
        sb.append(", searchInsideFolder='").append(searchInsideFolder).append('\'');
        sb.append(", folderType='").append(folderType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
