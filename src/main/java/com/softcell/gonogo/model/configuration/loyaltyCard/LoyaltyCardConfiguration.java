package com.softcell.gonogo.model.configuration.loyaltyCard;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.LoyaltyCardType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 31/7/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "loyaltyCardConfiguration")
public class LoyaltyCardConfiguration {


    @NotEmpty(groups = {LoyaltyCardConfiguration.AddGrp.class ,LoyaltyCardConfiguration.DeleteGrp.class ,
            LoyaltyCardConfiguration.UpdateGrp.class})
    @JsonProperty("sInstitutionId")
    private String institutionId;

    @NotEmpty(groups = {LoyaltyCardConfiguration.AddGrp.class})
    @JsonProperty("sInstitutionName")
    private String institutionName;

    @NotEmpty(groups = {LoyaltyCardConfiguration.AddGrp.class ,LoyaltyCardConfiguration.DeleteGrp.class ,
            LoyaltyCardConfiguration.UpdateGrp.class})
    @JsonProperty("sProductId")
    private String productId;

    @NotEmpty(groups = {LoyaltyCardConfiguration.AddGrp.class})
    @JsonProperty("sProductName")
    private String productName;

    @JsonProperty("sLoyaltyCardType")
    private LoyaltyCardType loyaltyCardType;

    @JsonProperty("dLoyaltyCardPrice")
    private double loyaltyCardPrice;

    @JsonProperty("bEnable")
    private boolean enable = true;

    @JsonProperty("dtCreateDate")
    private Date createDate = new Date();

    @JsonProperty("dtLastUpdateDate")
    private Date lastUpdateDate;


    public interface AddGrp{

    }

    public interface DeleteGrp{

    }

    public interface UpdateGrp{
    }

    public interface FetchGrp{

    }
}
