package com.softcell.gonogo.model.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by anupamad on 9/8/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "smsTemplateConfiguration")
public class SmsTemplateConfiguration extends AuditEntity{

    @JsonProperty("sInstitutionId")
    @NotEmpty(groups = {
            SmsTemplateConfiguration.InsertGrp.class,
            SmsTemplateConfiguration.DeleteGrp.class,
            SmsTemplateConfiguration.UpdateGrp.class,
            SmsTemplateConfiguration.FetchGrp.class
    })
    private String institutionId;

    @JsonProperty("sProductId")
    @NotEmpty(groups = {
            SmsTemplateConfiguration.InsertGrp.class,
            SmsTemplateConfiguration.DeleteGrp.class,
            SmsTemplateConfiguration.UpdateGrp.class
    })
    private String productId;

    @JsonProperty("sSmsType")
    @NotEmpty(groups = {
            SmsTemplateConfiguration.InsertGrp.class,
            SmsTemplateConfiguration.DeleteGrp.class,
            SmsTemplateConfiguration.UpdateGrp.class
    })
    private String smsType;

    @JsonProperty("sSmsText")
    @NotEmpty(groups = {
            SmsTemplateConfiguration.InsertGrp.class,
            SmsTemplateConfiguration.UpdateGrp.class,
    })
    private String smsText;

    @JsonProperty("bEnable")
    private boolean enable = true;

    public interface UpdateGrp{
    }

    public interface InsertGrp {
    }

    public interface DeleteGrp {
    }

    public interface FetchGrp {
    }

    public String getFormattedSMSText(String... args) {
        String formattedSmsText = String.format(this.smsText, args);
        return formattedSmsText;
    }
}