package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.ConfigurationConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * Created by amit on 10/9/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NodeCondition {

    /*@JsonProperty("oCondition")
    Map<String, Object> conditionExpression;*/
    @JsonProperty("oCondition")
    List<ConditionNode> conditionNodeList;

    @JsonProperty("sImpactAttr")
    private String impactAttribute;

    @JsonProperty("sTrueVal")
    private String trueValue;

    @JsonProperty("sFalseVal")
    private String falseValue;

}
