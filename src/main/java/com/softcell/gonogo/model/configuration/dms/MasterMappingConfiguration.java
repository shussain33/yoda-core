package com.softcell.gonogo.model.configuration.dms;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * Created by bhuvneshk on 26/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "masterMappingConfiguration")
public class MasterMappingConfiguration {

    @NotEmpty(groups = {AddGrp.class})
    @JsonProperty("sInstitutionId")
    private String institutionId;

    @NotEmpty(groups = {AddGrp.class})
    @JsonProperty("sSrcMasterName")
    private String sourceMasterName;

    @NotEmpty(groups = {AddGrp.class})
    @JsonProperty("aSrcColumnName")
    private Set<String> sourceColumnName;

    @NotEmpty(groups = {AddGrp.class})
    @JsonProperty("sDestMasterName")
    private String destinationMasterName;

    @NotEmpty(groups = {AddGrp.class})
    @JsonProperty("aDestColumnName")
    private Set<String> destinationColumnName;

    @NotEmpty(groups = {AddGrp.class})
    @JsonProperty("aColMapping")
    private Map<String, String> columnMapping;

    @JsonProperty("bEnable")
    private boolean enable = true;

    @JsonIgnore
    private Date insertDate = new Date();

    public interface AddGrp {

    }
}
