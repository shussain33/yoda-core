package com.softcell.gonogo.model.configuration.insurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsurancePolicy {

    @JsonProperty("sPolicyName")
    private String policy;

   @JsonProperty("sBenefit")
   private String benefit;

    @JsonProperty("lPremiumAmount")
    private List<InsuranceScheme> insuranceSchemes = new ArrayList<>();
}
