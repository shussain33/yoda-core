package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 10/12/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HierarchNodes {

    @JsonProperty("sId")
    private String nodeId;

    @JsonProperty("sName")
    private String nodeName;

    @JsonProperty("sValue")
    private String value;

    @JsonProperty("sPreviousNode")
    private String previousNode;

    @JsonProperty("sNextNode")
    private String nextNode;

    @JsonProperty("sIndex")
    private String index;

}
