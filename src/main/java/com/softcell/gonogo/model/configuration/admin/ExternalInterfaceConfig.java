package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.workflow.component.Component;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * Created by amit on 30/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExternalInterfaceConfig {
    @JsonProperty("bMainThreadWait")
    private boolean mainThreadWait;

    @JsonProperty("oComponentMap")
    private Map<Integer, Component> componentMap;

    @JsonProperty("aInterfaces")
    private List<String> interfaces;
}
