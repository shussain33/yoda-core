package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by amit on 9/7/18.
 */
public class ActionData{
    @JsonProperty("sActionName")
    private String actionName;

    @JsonProperty("aGroups")
    private List<String> groups;
}
