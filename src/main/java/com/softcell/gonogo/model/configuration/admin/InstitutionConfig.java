package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by amit on 20/6/18.
 */
@Document(collection = "institutionConfig")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InstitutionConfig {

    @JsonProperty("sInstId")
    @Id
    private String institutionId;

    @JsonProperty("bCallMifinForDedupe")
    private boolean callMifinForDedupe;

    @JsonProperty("sInstName")
    private String instituteName ;

    @JsonProperty("aProducts")
    private List<String> products;

    @JsonProperty("aProductConfigs")
    private List<ProductConfig> productConfigs;

    @JsonProperty("bSecondaryDBEnable")
    private boolean secondaryDBEnable = false;
}
