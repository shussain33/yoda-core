package com.softcell.gonogo.model.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "authSkipConfiguration")
public class AuthSkipConfiguration {

    @Id
    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("dtInsertDate")
    private Date insertDate = new Date();

    @JsonProperty("dtUpdateDate")
    private Date updateDate;

    @JsonProperty("bEnable")
    private Boolean enable;
}
