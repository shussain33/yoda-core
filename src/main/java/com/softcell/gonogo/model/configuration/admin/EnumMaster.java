package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 22/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnumMaster {
    @JsonProperty("sFieldName")
    private String name;

    @JsonProperty("aValues")
    private List<EnumValue> values;

}
