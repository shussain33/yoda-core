package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 20/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowNode {
    @JsonProperty("sId")
    private String nodeId;

    @JsonProperty("sName")
    private String nodeName;

    @JsonProperty("sType")
    private String type;

    @JsonProperty("sEvent")
    private String event;

    @JsonProperty("sValue")
    private String value;

    @JsonProperty("sNextStage")
    private String nextStage;

    @JsonProperty("aCurrentStages")
    private List<String> currentStages;

    @JsonProperty("sNextStatus")
    private String nextStatus;

    @JsonProperty("aCurrentStatus")
    private List<String> currentStatuses;

    @JsonProperty("oInterfaceConfig")
    private ExternalInterfaceConfig externalInterfacesConfig;

    @JsonProperty("oSubWorkflow")
    private WorkflowMaster subWorkflow;

    @JsonProperty("aConditions")
    List<NodeCondition> nodeCondition;

    @JsonProperty("sNextNode")
    private String nextNode;
}
