package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 25/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ModuleConfig {

    @JsonProperty("sModuleName")
    private String moduleName;

    //Screens enabled for current module
    @JsonProperty("aScreenConfig")
    private List<ScreenConfig> screenFieldConfigList;
}
