package com.softcell.gonogo.model.configuration.insurance;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsuranceScheme {

    @JsonProperty("sInsuranceName")
    private String insuranceName;

    @JsonProperty("dPremium")
    private double premium;

    @JsonProperty("sSchemeBenefit")
    private String schemeBenefit;

    @JsonProperty("sMasterCode")
    private String masterCode;

    @JsonProperty("sBenefitOption")
    private String benefitOption;

}
