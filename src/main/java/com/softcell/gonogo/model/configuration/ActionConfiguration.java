/**
 *
 */
package com.softcell.gonogo.model.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.actions.User;
import com.softcell.constants.ActionName;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author prateek
 */
@Document(collection = "actionConfiguration")
public class ActionConfiguration extends AuditEntity {

    @NotNull
    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sSourceId")
    private String sourceId;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sProductId")
    private String productId;

    @JsonProperty("sProductName")
    private Product productName;

    @JsonProperty("sActionName")
    private ActionName actionName;

    @JsonProperty("sActionDescription")
    private String actionDescription;

    @JsonProperty("bEnable")
    private boolean enable;

    @JsonProperty("dtCreateDate")
    private Date createDate = new Date();

    @JsonProperty("dtLastUpdateDate")
    private Date lastUpdateDate;

    @JsonProperty("oUser")
    private User user;

    @JsonProperty("iRetryCount")
    private int retryCount;

    @JsonProperty("iLimit")
    private Integer limit;

    @JsonProperty("sAuthenticationMode")
    private String authenticationMode;

    @JsonProperty("sAuthorisationMode")
    private String authorisationMode;

    @JsonProperty("aSkipApiList")
    private Set<String> skipApiList;

    @JsonProperty("aSkipApiListAgainstSourceId")
    private Map<String, Set<String>> skipApiListAgainstSourceId;

    @JsonProperty("bCheckUserId")
    private boolean checkUserId;

    @JsonProperty("sStepId")
    private String stepId;

    @JsonProperty("aStages")
    private List<String> stringList;

    @JsonProperty("dExpiry")
    private long expiry;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product getProductName() {
        return productName;
    }

    public void setProductName(Product productName) {
        this.productName = productName;
    }


    public ActionName getActionName() {
        return actionName;
    }

    public void setActionName(ActionName actionName) {
        this.actionName = actionName;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getAuthenticationMode() {
        return authenticationMode;
    }

    public void setAuthenticationMode(String authenticationMode) {
        this.authenticationMode = authenticationMode;
    }

    public String getAuthorisationMode() {
        return authorisationMode;
    }

    public void setAuthorisationMode(String authorisationMode) {
        this.authorisationMode = authorisationMode;
    }

    public Set<String> getSkipApiList() {
        return skipApiList;
    }

    public void setSkipApiList(Set<String> skipApiList) {
        this.skipApiList = skipApiList;
    }

    public Map<String, Set<String>> getSkipApiListAgainstSourceId() {
        return skipApiListAgainstSourceId;
    }

    public void setSkipApiListAgainstSourceId(Map<String, Set<String>> skipApiListAgainstSourceId) {
        this.skipApiListAgainstSourceId = skipApiListAgainstSourceId;
    }

    public boolean isCheckUserId() {
        return checkUserId;
    }

    public void setCheckUserId(boolean checkUserId) {
        this.checkUserId = checkUserId;
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ActionConfiguration [institutionId=");
        builder.append(institutionId);
        builder.append(", institutionName=");
        builder.append(institutionName);
        builder.append(", productId=");
        builder.append(productId);
        builder.append(", productName=");
        builder.append(productName);
        builder.append(", actionName=");
        builder.append(actionName);
        builder.append(", actionDescription=");
        builder.append(actionDescription);
        builder.append(", enable=");
        builder.append(enable);
        builder.append(", createDate=");
        builder.append(createDate);
        builder.append(", lastUpdateDate=");
        builder.append(lastUpdateDate);
        builder.append(", user=");
        builder.append(user);
        builder.append(", authenticationMode=");
        builder.append(authenticationMode);
        builder.append(", authorisationMode=");
        builder.append(authorisationMode);
        builder.append(", checkUserId=");
        builder.append(checkUserId);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((actionDescription == null) ? 0 : actionDescription
                .hashCode());
        result = prime * result
                + ((actionName == null) ? 0 : actionName.hashCode());
        result = prime * result
                + ((createDate == null) ? 0 : createDate.hashCode());
        result = prime * result + (enable ? 1231 : 1237);
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((institutionName == null) ? 0 : institutionName.hashCode());
        result = prime * result
                + ((lastUpdateDate == null) ? 0 : lastUpdateDate.hashCode());
        result = prime * result
                + ((productId == null) ? 0 : productId.hashCode());
        result = prime * result
                + ((productName == null) ? 0 : productName.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ActionConfiguration other = (ActionConfiguration) obj;
        if (actionDescription == null) {
            if (other.actionDescription != null)
                return false;
        } else if (!actionDescription.equals(other.actionDescription))
            return false;
        if (actionName != other.actionName)
            return false;
        if (createDate == null) {
            if (other.createDate != null)
                return false;
        } else if (!createDate.equals(other.createDate))
            return false;
        if (enable != other.enable)
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (institutionName == null) {
            if (other.institutionName != null)
                return false;
        } else if (!institutionName.equals(other.institutionName))
            return false;
        if (lastUpdateDate == null) {
            if (other.lastUpdateDate != null)
                return false;
        } else if (!lastUpdateDate.equals(other.lastUpdateDate))
            return false;
        if (productId == null) {
            if (other.productId != null)
                return false;
        } else if (!productId.equals(other.productId))
            return false;
        if (productName != other.productName)
            return false;
        if (user == null) {
            if (other.user != null)
                return false;
        } else if (!user.equals(other.user))
            return false;
        return true;
    }

}
