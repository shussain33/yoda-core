package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 10/9/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StageConfig {

    @JsonProperty("sStage")
    private String stage;

    @JsonProperty("iSequence")
    private int sequence;
}
