package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 20/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductConfig {

    @JsonProperty("sProduct")
    private String productName;

    @JsonProperty("aRoles")
    List<String> roles;

    // Modules enabled in config for current product
    @JsonProperty("aModuleConfig")
    private List<ModuleConfig> moduleConfigList;

    // Screen wise dropdown value masters for current product
    @JsonProperty("aScreenConfig")
    private List<ScreenConfig> screenValueMasterConfigs;

    @JsonProperty("aRoleConfig")
    private List<RoleConfig> roleConfigsList;

    @JsonProperty("aStageConfig")
    private List<StageConfig> stageConfigList;

    @JsonProperty("aIntimationConfig")
    private IntimationConfig intimationConfig;

    @JsonProperty("aHierarchymaster")
    private List<HierarchyMaster> hierarchyMasterList;

    @JsonProperty("aWorkflowMasters")
    private List<WorkflowMaster> workflowMasterList;
}
