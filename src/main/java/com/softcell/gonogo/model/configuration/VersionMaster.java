package com.softcell.gonogo.model.configuration;

/**
 * @author yogeshb
 */
public class VersionMaster {
    private String instition;
    private String version;
    private String action;

    public String getInstition() {
        return instition;
    }

    public void setInstition(String instition) {
        this.instition = instition;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result
                + ((instition == null) ? 0 : instition.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof VersionMaster))
            return false;
        VersionMaster other = (VersionMaster) obj;
        if (action == null) {
            if (other.action != null)
                return false;
        } else if (!action.equals(other.action))
            return false;
        if (instition == null) {
            if (other.instition != null)
                return false;
        } else if (!instition.equals(other.instition))
            return false;
        if (version == null) {
            if (other.version != null)
                return false;
        } else if (!version.equals(other.version))
            return false;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("VersionMaster [instition=");
        builder.append(instition);
        builder.append(", version=");
        builder.append(version);
        builder.append(", action=");
        builder.append(action);
        builder.append("]");
        return builder.toString();
    }


}
