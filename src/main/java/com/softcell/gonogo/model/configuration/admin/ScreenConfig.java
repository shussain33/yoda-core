package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 22/6/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScreenConfig {

    @JsonProperty("sScreenName")
    private String screenName;

    // fields selected in configuration enable for current screen
    @JsonProperty("aFields")
    private List<Field> fields;

    // Dropdown with values in configuration added for current screen
    @JsonProperty("aEnumMaster")
    private List<EnumMaster> enumMasterList;

}