package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by amit on 9/7/18.
 */
public class Field{
    @JsonProperty("sFieldName")
    private String fieldName;

    @JsonProperty("bMendatory")
    private boolean mendatory = false;

    private String type;

    private Map<String, Object> validations;
}
