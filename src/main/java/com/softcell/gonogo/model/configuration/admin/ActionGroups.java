package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 9/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActionGroups {

    @JsonProperty("aActions")
    private ActionData events;
}
