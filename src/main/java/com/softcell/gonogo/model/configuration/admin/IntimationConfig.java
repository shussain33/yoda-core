package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * Created by amit on 9/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IntimationConfig {

    @JsonProperty("bDfltGrpCreated")
    boolean defaultGroupCreated = false;

    @JsonProperty("aDefaultGroups")
    Set<String> defaultGroups;

    @JsonProperty("aGroups")
    private List<IntimationGroup> groups;

    @JsonProperty("aConfig")
    private List<ActionGroup> actionGroupList;

    @JsonProperty("aGroupType")
    private Set<String> groupType;
}
