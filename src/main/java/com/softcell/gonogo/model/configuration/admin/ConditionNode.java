package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 26/12/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConditionNode {
    @JsonProperty("sType")
    private String type;

    @JsonProperty("sAppCondKey")
    private String appConditionKey;

    @JsonProperty("sOperator")
    private String operator;

    @JsonProperty("sCondValue")
    private String condValue;
}
