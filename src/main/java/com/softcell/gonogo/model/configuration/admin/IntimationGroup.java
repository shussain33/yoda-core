package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.HierarchyUser;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.List;

/**
 * Created by amit on 9/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntimationGroup{
    @JsonProperty("sGroupName")
    private String groupName;

    @JsonProperty("sGroupType")
    private String type;
    // mandatory only if group type is general
    @JsonProperty("sTypeValue")
    private String typeValue;

    @JsonProperty("aUsers")
    private List<HierarchyUser> users;

    @JsonProperty("aUsersList")
    private List<LoginServiceResponse> usersList;

    @Override
    public String toString() {
        return "IntimationGroup{" +
                "groupName='" + groupName + '\'' +
                ", type='" + type + '\'' +
                ", typeValue='" + typeValue + '\'' +
                ", users=" + users +
                '}';
    }
}
