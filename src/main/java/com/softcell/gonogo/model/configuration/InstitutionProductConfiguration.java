package com.softcell.gonogo.model.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "InstProdConfig")
public class InstitutionProductConfiguration extends AuditEntity {

    @JsonProperty("sInstID")
    @NotEmpty(groups = {
            InstitutionProductConfiguration.InsertGrp.class,
            InstitutionProductConfiguration.DeleteGrp.class,
            InstitutionProductConfiguration.UpdateGrp.class
    })
    private String institutionID;

    @JsonProperty("sInstName")
    @NotEmpty(groups = {InstitutionProductConfiguration.InsertGrp.class})
    private String institutionName;

    @JsonProperty("sProductID")
    @NotEmpty(
            groups = {
                    InstitutionProductConfiguration.InsertGrp.class,
                    InstitutionProductConfiguration.DeleteGrp.class
            }
    )
    private String productID;

    @JsonProperty("sProductName")
    @NotEmpty(groups = {
            InstitutionProductConfiguration.InsertGrp.class,
            InstitutionProductConfiguration.UpdateGrp.class
    })
    private String productName;


    @JsonProperty("sStatus")
    private boolean status;

    @JsonProperty("sAliasName")
    private String aliasName;

    /**
     * @return the institutionID
     */
    public String getInstitutionID() {
        return institutionID;
    }

    /**
     * @param institutionID the institutionID to set
     */
    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    /**
     * @return the institutionName
     */
    public String getInstitutionName() {
        return institutionName;
    }

    /**
     * @param institutionName the institutionName to set
     */
    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    /**
     * @return the productID
     */
    public String getProductID() {
        return productID;
    }

    /**
     * @param productID the productID to set
     */
    public void setProductID(String productID) {
        this.productID = productID;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }


    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InstitutionProductConfiguration{");
        sb.append("aliasName='").append(aliasName).append('\'');
        sb.append(", institutionID='").append(institutionID).append('\'');
        sb.append(", institutionName='").append(institutionName).append('\'');
        sb.append(", productID='").append(productID).append('\'');
        sb.append(", productName='").append(productName).append('\'');
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InstitutionProductConfiguration that = (InstitutionProductConfiguration) o;

        if (status != that.status) return false;
        if (institutionID != null ? !institutionID.equals(that.institutionID) : that.institutionID != null)
            return false;
        if (institutionName != null ? !institutionName.equals(that.institutionName) : that.institutionName != null)
            return false;
        if (productID != null ? !productID.equals(that.productID) : that.productID != null) return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        return !(aliasName != null ? !aliasName.equals(that.aliasName) : that.aliasName != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (institutionID != null ? institutionID.hashCode() : 0);
        result = 31 * result + (institutionName != null ? institutionName.hashCode() : 0);
        result = 31 * result + (productID != null ? productID.hashCode() : 0);
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (status ? 1 : 0);
        result = 31 * result + (aliasName != null ? aliasName.hashCode() : 0);
        return result;
    }

    public interface InsertGrp {
    }

    public interface DeleteGrp {
    }

    public interface UpdateGrp {
    }
}
