package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by amit on 9/7/18.
 */
public class EnumValue {
    @JsonProperty("sValue")
    private String value;

    @JsonProperty("sLabel")
    private String label;
}
