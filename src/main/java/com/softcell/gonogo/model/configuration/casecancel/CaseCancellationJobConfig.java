package com.softcell.gonogo.model.configuration.casecancel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 7/12/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "caseCancellationJobConfig")
public class CaseCancellationJobConfig {

    @JsonProperty("sInstitutionId")
    @NotEmpty(groups = {CaseCancellationJobConfig.AddGrp.class})
    private String institutionId;  //

    @JsonProperty("dtStartDate")  // optional
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date startDate;

    @JsonProperty("dtEndDate")   // optional
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date endDate;

    @JsonProperty("iPastNoOfDays")  //7 no of days
    private int pastNoOfDays;

    @JsonProperty("sAppStat")
    @NotEmpty(groups = {CaseCancellationJobConfig.AddGrp.class})
    private String appStatus; //Cancelled

    @JsonProperty("sAppStage")
    @NotEmpty(groups = {CaseCancellationJobConfig.AddGrp.class})
    private String appStage;   //CNCLD

    @JsonProperty("sBatchCount")
    private int batchCount;    // max 100

    @JsonProperty("iLimit")   //0
    private int limit;

    @JsonProperty("iSkip")     //0
    private int skip;

    @JsonProperty("bEnable")
    private boolean enable = true;

    @JsonProperty("dtCreateDate")
    private Date createDate = new Date();

    @JsonProperty("dtLstUpdDate")
    private Date lastUpdateDate = new Date();


    public interface AddGrp{

    }



}
