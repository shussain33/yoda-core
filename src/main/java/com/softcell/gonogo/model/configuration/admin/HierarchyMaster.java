package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 10/12/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyMaster {

    @JsonProperty("sHierarchyType")
    private String hierarchyType;

    @JsonProperty("sHierarchySubType")
    private String hierarchySubType;

    @JsonProperty("bActive")
    private boolean active;

    @JsonProperty("aNodes")
    private List<HierarchNodes> nodes;

}
