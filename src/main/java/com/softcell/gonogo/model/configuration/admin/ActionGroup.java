package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 9/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActionGroup {
    @JsonProperty("sActionName")
    private String actionName;

    @JsonProperty("aMsgGroups")
    private List<String> msgGroups;

    @JsonProperty("aEmailGroups")
    private List<String> emailGroups;

    @JsonProperty("bMessage")
    private boolean smsIntimation;

    @JsonProperty("sMsgTemplate")
    private String messageTemplate;

    @JsonProperty("bEmail")
    private boolean emailIntimation;

    @JsonProperty("sEmailTemplate")
    private String emailTemplate;
}
