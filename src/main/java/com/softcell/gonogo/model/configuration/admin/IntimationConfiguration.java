package com.softcell.gonogo.model.configuration.admin;

/**
 * Created by amit on 10/10/19.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by amit on 20/6/18.
 */
@Document(collection = "intimationConfiguration")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntimationConfiguration {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("aIntimationConfig")
    private IntimationConfig intimationConfig;
}
