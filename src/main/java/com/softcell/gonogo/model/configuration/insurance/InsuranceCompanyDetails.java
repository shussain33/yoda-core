package com.softcell.gonogo.model.configuration.insurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsuranceCompanyDetails {

    @JsonProperty("sCompanyName")
    private String companyName;

    @JsonProperty("lInsurancePolicy")
    private List<InsurancePolicy> insurancePolicyList = new ArrayList<InsurancePolicy>();
}
