package com.softcell.gonogo.model.configuration.dms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 7/8/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "dmsFolderConfiguration")
public class DmsFolderConfiguration {

    @NotEmpty(groups = {DmsFolderConfiguration.AddGrp.class, DmsFolderConfiguration.DeleteGrp.class,
            DmsFolderConfiguration.UpdateGrp.class})
    @JsonProperty("sInstitutionId")
    private String institutionId;

    @NotEmpty(groups = {DmsFolderConfiguration.AddGrp.class , DmsFolderConfiguration.UpdateGrp.class})
    @JsonProperty("sInstitutionName")
    private String institutionName;

    @NotEmpty(groups = {DmsFolderConfiguration.AddGrp.class , DmsFolderConfiguration.UpdateGrp.class})
    @JsonProperty("sIndexName")
    private String indexName;

    @NotEmpty(groups = {DmsFolderConfiguration.AddGrp.class , DmsFolderConfiguration.UpdateGrp.class})
    @JsonProperty("sIndexType")
    private String indexType;

    @JsonProperty("sIndexId")
    private int indexId;

    @JsonProperty("bEnable")
    private boolean enable = true;

    @JsonProperty("dtCreateDate")
    private Date createDate = new Date();

    @JsonProperty("dtLastUpdateDate")
    private Date lastUpdateDate =new Date();


    public interface AddGrp {

    }

    public interface DeleteGrp {

    }

    public interface UpdateGrp {
    }

    public interface FetchGrp {

    }

}
