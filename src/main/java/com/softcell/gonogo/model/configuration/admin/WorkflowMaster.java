package com.softcell.gonogo.model.configuration.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 20/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowMaster {

    @JsonProperty("sWorkflowName")
    private String workflowName;

    @JsonProperty("sWorkflowType")
    private String workflowType;

    @JsonProperty("bActive")
    private boolean active;

    @JsonProperty("aNodes")
    private List<WorkflowNode> nodes;
}
