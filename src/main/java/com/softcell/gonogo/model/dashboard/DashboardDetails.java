/**
 * bhuvneshk12:53:22 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.dashboard;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.AllocationInfo;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.CroJustification;
import com.softcell.gonogo.model.core.LOSDetails;
import com.softcell.gonogo.model.request.core.InvoiceDetails;

import java.util.Date;
import java.util.List;

/**
 * @author bhuvneshk
 *
 */
public class DashboardDetails {

    /**
     * POJO: Dashboard Detail JSON POJO
     */

    @JsonProperty("sRefId")
    private String referenceID;

    @JsonProperty("sAgreementNum")
    private String agreementNum;

    @JsonProperty("sName")
    private String name;

    @JsonProperty("dDate")
    private Date date;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sBucket")
    private String bucket;

    @JsonProperty("sStages")
    private String stages;

    @JsonProperty("sScheme")
    private String scheme;

    @JsonProperty("dAmtApprvd")
    private double amountApproved;

    @JsonProperty("dAmtAppld")
    private double amountApplied;

    @JsonProperty("dFinanceAmt")
    private double financeAmount;

    @JsonProperty("sMobNo")
    private String mobileNumber;

    @JsonProperty("sPan")
    private String PAN;

    @JsonProperty("sAadhar")
    private String aadhaarNumber;

    @JsonProperty("sDob")
    private String dob;

    @JsonProperty("bQdeDecision")
    private boolean qdeDecision;

    @JsonProperty("oLosDtls")
    private LOSDetails losDetails;

    @JsonProperty("aCroJustification")
    private List<CroJustification> croJustificationList;

    @JsonProperty("aCroDec")
    private List<CroDecision> croDecisions;

    @JsonProperty("iReApprsCnt")
    private int reAppraiseCount;

    @JsonProperty("oInvDtls")
    private InvoiceDetails invoiceDetails;

    @JsonProperty("sApplicantType")
    private String applicantType;

    @JsonProperty("sBureauScore")
    private String bureauScore;

    @JsonProperty("sAppScore")
    private String appScore;

    @JsonProperty("sStp")
    private String isStp;

    @JsonProperty("sDealerName")
    private String dealerName;

    @JsonProperty("sDsaName")
    private String dsaName;

    @JsonProperty("sBranch")
    private String branchName;

    @JsonProperty("sCurrentUser")
    private String currentUser;

    @JsonProperty("oAllocationStatus")
    private AllocationInfo allocationInfo;

    @JsonProperty("sPromocode")
    private String promocode;



    /**
     * @return the referenceID
     */
    public String getReferenceID() {
        return referenceID;
    }

    /**
     * @param referenceID
     *            the referenceID to set
     */
    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    public String getAgreementNum() {
        return agreementNum;
    }

    public void setAgreementNum(String agreementNum) {
        this.agreementNum = agreementNum;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    public String getBucket() {
        return bucket;
    }

    /**
     * @param bucket
     *            the bucket to set
     */
    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the scheme
     */
    public String getScheme() {
        return scheme;
    }

    /**
     * @param scheme
     *            the scheme to set
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * @return the amountApproved
     */
    public double getAmountApproved() {
        return amountApproved;
    }

    /**
     * @param amountApproved
     *            the amountApproved to set
     */
    public void setAmountApproved(double amountApproved) {
        this.amountApproved = amountApproved;
    }

    public double getAmountApplied() {
        return amountApplied;
    }

    public void setAmountApplied(double amountApplied) {
        this.amountApplied = amountApplied;
    }

    public double getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(double financeAmount) {
        this.financeAmount = financeAmount;
    }
    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber
     *            the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the pAN
     */
    public String getPAN() {
        return PAN;
    }

    /**
     * @param pAN
     *            the pAN to set
     */
    public void setPAN(String pAN) {
        PAN = pAN;
    }

    /**
     * @return the aadhaarNumber
     */
    public String getAadhaarNumber() {
        return aadhaarNumber;
    }

    /**
     * @param aadhaarNumber
     *            the aadhaarNumber to set
     */
    public void setAadhaarNumber(String aadhaarNumber) {
        this.aadhaarNumber = aadhaarNumber;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob
     *            the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    public boolean isQdeDecision() {
        return qdeDecision;
    }

    public void setQdeDecision(boolean qdeDecision) {
        this.qdeDecision = qdeDecision;
    }

    public String getStages() {
        return stages;
    }

    public void setStages(String stages) {
        this.stages = stages;
    }

    public LOSDetails getLosDetails() {
        return losDetails;
    }

    public void setLosDetails(LOSDetails losDetails) {
        this.losDetails = losDetails;
    }

    public List<CroJustification> getCroJustificationList() {
        return croJustificationList;
    }

    public void setCroJustificationList(List<CroJustification> croJustificationList) {
        this.croJustificationList = croJustificationList;
    }

    /**
     * @return the reAppraiseCount
     */
    public int getReAppraiseCount() {
        return reAppraiseCount;
    }

    /**
     * @param reAppraiseCount the reAppraiseCount to set
     */
    public void setReAppraiseCount(int reAppraiseCount) {
        this.reAppraiseCount = reAppraiseCount;
    }

    /**
     * @return the invoiceDetails
     */
    public InvoiceDetails getInvoiceDetails() {
        return invoiceDetails;
    }

    /**
     * @param invoiceDetails the invoiceDetails to set
     */
    public void setInvoiceDetails(InvoiceDetails invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    public List<CroDecision> getCroDecisions() {
        return croDecisions;
    }

    public void setCroDecisions(List<CroDecision> croDecisions) {
        this.croDecisions = croDecisions;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getBureauScore() {        return bureauScore;    }

    public void setBureauScore(String bureauScore) {        this.bureauScore = bureauScore;    }

    public String getAppScore() {        return appScore;    }

    public void setAppScore(String appScore) {        this.appScore = appScore;    }

    public String getIsStp() {        return isStp;    }

    public void setIsStp(String isStp) {        this.isStp = isStp;    }

    public String getDealerName() {        return dealerName;    }

    public void setDealerName(String dealerName) {        this.dealerName = dealerName;    }

    public String getDsaName() {        return dsaName;    }

    public void setDsaName(String dsaName) {        this.dsaName = dsaName;    }

    public String getBranchName() {        return branchName;    }

    public void setBranchName(String branchName) {        this.branchName = branchName;    }

    public String getCurrentUser() {        return currentUser;    }

    public void setCurrentUser(String currentUser) {        this.currentUser = currentUser;    }

    public AllocationInfo getAllocationInfo() {        return allocationInfo;    }

    public void setAllocationInfo(AllocationInfo allocationInfo) {        this.allocationInfo = allocationInfo;    }


    public String getPromocode() {      return promocode;    }

    public void setPromocode(String promocode) {        this.promocode = promocode;     }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DashboardDetails{");
        sb.append("referenceID='").append(referenceID).append('\'');
        sb.append(", agreementNum='").append(agreementNum).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", date=").append(date);
        sb.append(", status='").append(status).append('\'');
        sb.append(", stages='").append(stages).append('\'');
        sb.append(", scheme='").append(scheme).append('\'');
        sb.append(", amountApproved=").append(amountApproved);
        sb.append(", financeAmount=").append(financeAmount);
        sb.append(", mobileNumber='").append(mobileNumber).append('\'');
        sb.append(", PAN='").append(PAN).append('\'');
        sb.append(", aadhaarNumber='").append(aadhaarNumber).append('\'');
        sb.append(", dob='").append(dob).append('\'');
        sb.append(", qdeDecision=").append(qdeDecision);
        sb.append(", losDetails=").append(losDetails);
        sb.append(", croJustificationList=").append(croJustificationList);
        sb.append(", croDecisions=").append(croDecisions);
        sb.append(", reAppraiseCount=").append(reAppraiseCount);
        sb.append(", invoiceDetails=").append(invoiceDetails);
        sb.append(", applicantType=").append(applicantType);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DashboardDetails that = (DashboardDetails) o;

        if (Double.compare(that.amountApproved, amountApproved) != 0) return false;
        if (Double.compare(that.financeAmount, financeAmount) != 0) return false;
        if (qdeDecision != that.qdeDecision) return false;
        if (reAppraiseCount != that.reAppraiseCount) return false;
        if (referenceID != null ? !referenceID.equals(that.referenceID) : that.referenceID != null) return false;
        if (agreementNum != null ? !agreementNum.equals(that.agreementNum) : that.agreementNum != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (stages != null ? !stages.equals(that.stages) : that.stages != null) return false;
        if (scheme != null ? !scheme.equals(that.scheme) : that.scheme != null) return false;
        if (mobileNumber != null ? !mobileNumber.equals(that.mobileNumber) : that.mobileNumber != null) return false;
        if (PAN != null ? !PAN.equals(that.PAN) : that.PAN != null) return false;
        if (aadhaarNumber != null ? !aadhaarNumber.equals(that.aadhaarNumber) : that.aadhaarNumber != null)
            return false;
        if (dob != null ? !dob.equals(that.dob) : that.dob != null) return false;
        if (losDetails != null ? !losDetails.equals(that.losDetails) : that.losDetails != null) return false;
        if (croJustificationList != null ? !croJustificationList.equals(that.croJustificationList) : that.croJustificationList != null)
            return false;
        if (croDecisions != null ? !croDecisions.equals(that.croDecisions) : that.croDecisions != null) return false;
        if (invoiceDetails != null ? !invoiceDetails.equals(that.invoiceDetails) : that.invoiceDetails != null)
            return false;
        return applicantType == that.applicantType;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = referenceID != null ? referenceID.hashCode() : 0;
        result = 31 * result + (agreementNum != null ? agreementNum.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (stages != null ? stages.hashCode() : 0);
        result = 31 * result + (scheme != null ? scheme.hashCode() : 0);
        temp = Double.doubleToLongBits(amountApproved);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(financeAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (mobileNumber != null ? mobileNumber.hashCode() : 0);
        result = 31 * result + (PAN != null ? PAN.hashCode() : 0);
        result = 31 * result + (aadhaarNumber != null ? aadhaarNumber.hashCode() : 0);
        result = 31 * result + (dob != null ? dob.hashCode() : 0);
        result = 31 * result + (qdeDecision ? 1 : 0);
        result = 31 * result + (losDetails != null ? losDetails.hashCode() : 0);
        result = 31 * result + (croJustificationList != null ? croJustificationList.hashCode() : 0);
        result = 31 * result + (croDecisions != null ? croDecisions.hashCode() : 0);
        result = 31 * result + reAppraiseCount;
        result = 31 * result + (invoiceDetails != null ? invoiceDetails.hashCode() : 0);
        result = 31 * result + (applicantType != null ? applicantType.hashCode() : 0);
        return result;
    }
}
