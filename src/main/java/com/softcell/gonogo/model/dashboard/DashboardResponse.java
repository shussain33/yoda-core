package com.softcell.gonogo.model.dashboard;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by amit on 25/4/18.
 */
public class DashboardResponse {

    @JsonProperty("aResultData")
    List<DashboardDetails> dashboardDetailsList;

    @JsonProperty("iRecordCount")
    long recordCount;

    public List<DashboardDetails> getDashboardDetailsList() {        return dashboardDetailsList;    }

    public void setDashboardDetailsList(List<DashboardDetails> dashboardDetailsList) {
        this.dashboardDetailsList = dashboardDetailsList;
    }

    public long getRecordCount() {        return recordCount;    }

    public void setRecordCount(long recordCount) {        this.recordCount = recordCount;    }
}
