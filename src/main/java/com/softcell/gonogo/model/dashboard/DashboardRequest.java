/**
 * bhuvneshk12:44:40 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.dashboard;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * @author bhuvneshk
 */
public class DashboardRequest {

    /**
     * POJO: Dashboard Request JSON POJO
     */
    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.InstWithProductGrp.class})
    private Header header;

    @JsonProperty("sDsaId")
    @NotEmpty(groups = {DashboardRequest.FetchGrp.class})
    private String dsaId;

    @JsonProperty("dtFromDate")
    @NotNull(groups = {DashboardRequest.FetchGrp.class})
    private Date fromDate;

    @JsonProperty("dtToDate")
    @NotNull(groups = {DashboardRequest.FetchGrp.class})
    private Date toDate;


    @JsonProperty("iSkip")
    @Min(value = 0, groups = {DashboardRequest.FetchGrp.class})
    private int skip;


    @JsonProperty("iLimit")
    @Min(value = 1, groups = {DashboardRequest.FetchGrp.class})
    private int limit;

    /**
     * Request Criteria is for getting application request
     * based on different criteria like products or branches
     */
    @JsonProperty("oCriteria")
    @NotNull(groups = {DashboardRequest.FetchGrp.class})
    @Valid
    private RequestCriteria criteria;

    @JsonProperty("bShowMyRec")
    private boolean showMyRecords = false;

    @JsonProperty("mFilters")
    private Map<String, String> filters;

    /**
     * @return the dsaId
     */
    public String getDsaId() {
        return dsaId;
    }

    /**
     * @param dsaId the dsaId to set
     */
    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the skip
     */
    public int getSkip() {
        return skip;
    }

    /**
     * @param skip the skip to set
     */
    public void setSkip(int skip) {
        this.skip = skip;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public RequestCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(RequestCriteria criteria) {
        this.criteria = criteria;
    }

    public boolean isShowMyRecords() {        return showMyRecords;    }

    public void setShowMyRecords(boolean showMyRecords) {        this.showMyRecords = showMyRecords;    }

    public Map<String, String> getFilters() {        return filters;    }

    public void setFilters(Map<String, String> filters) {        this.filters = filters;    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DashboardRequest{");
        sb.append("header=").append(header);
        sb.append(", dsaId='").append(dsaId).append('\'');
        sb.append(", fromDate=").append(fromDate);
        sb.append(", toDate=").append(toDate);
        sb.append(", skip=").append(skip);
        sb.append(", limit=").append(limit);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardRequest that = (DashboardRequest) o;
        return Objects.equals(skip, that.skip) &&
                Objects.equals(limit, that.limit) &&
                Objects.equals(header, that.header) &&
                Objects.equals(dsaId, that.dsaId) &&
                Objects.equals(fromDate, that.fromDate) &&
                Objects.equals(toDate, that.toDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, dsaId, fromDate, toDate, skip, limit);
    }

    public interface FetchGrp {

    }
}
