package com.softcell.gonogo.model;

import java.util.*;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Remark{

	@JsonProperty("sComment")
	private String comment;

	@JsonProperty("sUserId")
	private String by;

	@JsonProperty("sUserRole")
	private String role;

	@JsonProperty("dtRemarkDate")
	Date remarkDate = new Date();
}
