package com.softcell.gonogo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.aadhar.AddressSegment;
import com.softcell.gonogo.model.core.kyc.response.aadhar.ProofOfIdentity;

/**
 * Created by vinod on 28/12/17.
 */
public class ApplicantAadharDetails {

    public enum KycRequestType {
        IRIS,
        BFD,
        OTP
    }

    @JsonProperty("sKycReqType")
    private KycRequestType kycRequestType;

    @JsonProperty("sAuthReqType")
    private String authRequestType = "NA";

    @JsonProperty("sTxnId")
    private String transactionIdentifier;

    @JsonProperty("oProofOfIdentity")
    private ProofOfIdentity proofOfIdentity;

    @JsonProperty("oAddressSegment")
    private AddressSegment addressSegment;


    public KycRequestType getKycRequestType() {
        return kycRequestType;
    }

    public void setKycRequestType(KycRequestType kycRequestType) {
        this.kycRequestType = kycRequestType;
    }

    public String getAuthRequestType() {
        return authRequestType;
    }

    public void setAuthRequestType(String authRequestType) {
        this.authRequestType = authRequestType;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public ProofOfIdentity getProofOfIdentity() {
        return proofOfIdentity;
    }

    public void setProofOfIdentity(ProofOfIdentity proofOfIdentity) {
        this.proofOfIdentity = proofOfIdentity;
    }

    public AddressSegment getAddressSegment() {
        return addressSegment;
    }

    public void setAddressSegment(AddressSegment addressSegment) {
        this.addressSegment = addressSegment;
    }
}
