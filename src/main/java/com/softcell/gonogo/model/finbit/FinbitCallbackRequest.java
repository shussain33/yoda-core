package com.softcell.gonogo.model.finbit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg237 on 27/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FinbitCallbackRequest {

    private String acknowledgementId;

    private String error;

    private String path;

    private int statusCode;

    private String timestamp;

    private FinbitCallbackResponse payload;

    private List<ErrorDetails> errors;

}
