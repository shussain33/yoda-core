package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by ssg237 on 16/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "finbitData")
public class FinbitData {

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("aStatements")
    private List<BankStatement> statements;

    @JsonProperty("oData")
    private Object data;

    @JsonProperty("aSummaryData")
    private List<Object> summaryData;
}
