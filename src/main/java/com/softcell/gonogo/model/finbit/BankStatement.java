package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Name;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg237 on 10/9/19.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include. NON_NULL)
public class BankStatement {

    @JsonProperty("bank")
    private String bank;

    @JsonProperty("sMifinBankID")
    private String mifinBankCode;

    @JsonProperty("accountType")
    private String accountType;

    @JsonProperty("oApplicantName")
    private Name applicantName;

    @JsonProperty("statement")
    private Statement statement;

    @JsonProperty("accountUID")
    private String accountUID;

    @JsonProperty("password")
    private String password;

    @JsonProperty("status")
    private String status;

    @JsonProperty("error")
    private String error;

    @JsonProperty("sAppId")
    private  String appId;

    @JsonProperty("MonthlyResponse")
    private MonthlyResponse monthlyData;

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;

    @JsonProperty("bFinbitStatus")
    private boolean finbitStatus;

    @JsonProperty("aBankStatementDetails")
    private List<Statement> bankStatementDetails;

}
