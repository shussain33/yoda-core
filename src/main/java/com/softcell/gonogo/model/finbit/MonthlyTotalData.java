package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
/**
 * Created by ssg237 on 23/10/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MonthlyTotalData {

    @JsonProperty("totalValue")
    private double totalValue;

    @JsonProperty("average")
    private double average;

    @JsonProperty("total")
    private double total;

    @JsonProperty("monthlyDetails")
    private List<MonthWiseData> monthlyDetails;
}
