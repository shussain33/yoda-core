package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg237 on 14/10/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FinbitResponse {

    @JsonProperty("path")
    private String path;

    @JsonProperty("statusCode")
    private int status;

    @JsonProperty("payload")
    private FinbitCallbackResponse payload;

    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("errors")
    private List<ErrorDetails> errors;

    @JsonProperty("error")
    private String error;
}
