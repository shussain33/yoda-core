package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg237 on 30/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountListDetails {

    @JsonProperty("accountDetails")
    private String accountDetails;

    @JsonProperty("parseMessage")
    private String parseMessage;

    @JsonProperty("accountUID")
    private String accountUID;

    @JsonProperty("parseStatus")
    private String parseStatus;
}
