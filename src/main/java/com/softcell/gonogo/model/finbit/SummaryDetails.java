package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg237 on 23/10/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SummaryDetails {

    @JsonProperty("outwBounce")
    private MonthlyTotalData outwBounce;

    @JsonProperty("configurableMonthlyAvgBal")
    private MonthlyTotalData configurableMonthlyAvgBal;

    @JsonProperty("inwBounce")
    private MonthlyTotalData inwBounce;

    @JsonProperty("credits")
    private MonthlyTotalData credits;

    @JsonProperty("monthlyAvgBal")
    private MonthlyTotalData monthlyAvgBal;

    @JsonProperty("interestPaid")
    private MonthlyTotalData interestPaid;

    @JsonProperty("debits")
    private MonthlyTotalData debits;

    @JsonProperty("interestReceived")
    private MonthlyTotalData interestReceived;

    @JsonProperty("cashDeposit")
    private MonthlyTotalData cashDeposit;

    @JsonProperty("totalNetCredit")
    private MonthlyTotalData totalNetCredit;

    @JsonProperty("totalNetDedit")
    private MonthlyTotalData totalNetDedit;

    @JsonProperty("chqDeposit")
    private MonthlyTotalData chqDeposit;

    @JsonProperty("peakUtilization")
    private MonthlyTotalData peakUtilization;
}

