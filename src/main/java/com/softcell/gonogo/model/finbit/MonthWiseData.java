package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Created by ssg237 on 23/10/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MonthWiseData {

    @JsonProperty("total")
    private double total;

    @JsonProperty("monthYear")
    private String monthYear;

    @JsonProperty("value")
    private double value;

    @JsonProperty("netAverageBalance")
    private double netAverageBalance;

    @JsonProperty("monthAndYear")
    private String monthAndYear;

    @JsonProperty("dayBalanceMap")
    private Object dayBalanceMap;
}
