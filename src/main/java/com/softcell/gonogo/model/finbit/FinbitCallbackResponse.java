package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg237 on 30/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FinbitCallbackResponse {

    @JsonProperty("accountList")
    private List<AccountListDetails> accountList;

    @JsonProperty("statusCode")
    private String uploadStatus;

    @JsonProperty("message")
    private String message;

    @JsonProperty("status")
    private String status;

    @JsonProperty("errors")
    private List<ErrorDetails> errors;

}
