package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg237 on 13/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadBankRequest {

    @JsonProperty("statements")
    private List<BankStatement> statements;

    @JsonProperty("callbackUrl")
    private String callbackUrl;
}
