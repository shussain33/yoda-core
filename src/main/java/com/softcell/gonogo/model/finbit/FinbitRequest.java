package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg237 on 10/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FinbitRequest {

    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sAcknowledgementId")
    private  String acknowledgementId;

    @JsonProperty("sAccountUID")
    private  String accountUID;

    @JsonProperty("sType")
    private String type;

    @JsonProperty("aStatements")
    private List<BankStatement> statements;

    @JsonProperty("oData")
    private Object data;

    @JsonProperty("aSummaryData")
    private List<Object> summaryData;

    @JsonProperty("bFinbitStatus")
    private boolean finbitStatus;
}
