package com.softcell.gonogo.model.finbit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg237 on 10/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Statement {

    @JsonProperty("fileName")
    private String fileName;

    @JsonProperty("content")
    private String content;

    @JsonProperty("password")
    private String password;
}
