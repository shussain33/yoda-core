package com.softcell.gonogo.model;

import com.softcell.gonogo.model.configuration.VersionMaster;
import com.softcell.gonogo.model.masters.*;

/**
 * Created by ssg237 on 28/5/21.
 */
public enum MasterMappingDetails {

    PINCODE_MASTER("pincodeMaster", PinCodeMaster.class),
    BRANCH_MASTER("branchMaster", BranchMaster.class),
    DEALER_EMAIL_MASTER("DealerEmailMaster", DealerEmailMaster.class),
    INSURANCE_PREMIUM_MASTER("insurancePremiumMaster", InsurancePremiumMaster.class),
    STATE_MASTER("stateMaster", StateMaster.class),
    CITY_STATE_MASTER("cityStateMappingMaster", CityStateMappingMaster.class),
    SCHEME_MASTER("schemeMaster", SchemeMasterData.class),
    ASSET_MASTER("assetModelMaster", AssetModelMaster.class),
    ASSET_CAT_MASTER("assetCategory", AssetCategoryMaster.class),
    DEALER_BRANCH_MASTER("dealerBranchMaster", DealerBranchMaster.class),
    SCHEME_MODEL_DEALER_CITY_MASTER("SchemeModelDealerCityMapping",SchemeModelDealerCityMapping.class),
    SCHEME_DATE_MAPPING("SchemeDateMapping",SchemeDateMapping.class),
    ORGANIZATION_HIERARCHY_MASTER("organizationalHierarchyMaster",OrganizationalHierarchyMaster.class),
    ORGANIZATION_HIERARCHY_MASTER_V2("organizationalHierarchyMasterV2",OrganizationalHierarchyMasterV2.class),
    ROI_SCHEME_MASTER("roiSchemeMaster",RoiSchemeMaster.class),
    NEGATIVE_AREA_MASTER("negativeAreaFundingMaster", NegativeAreaGeoLimitMaster.class),
    EMPLOYER_MASTER("employerMaster", EmployerMaster.class),
    BANK_DETAIL_MASTER("bankDetailsMaster", BankDetailsMaster.class),
    DEVIATION_MASTER("deviationMaster",DeviationMaster.class),
    ELECTRICITY_BOARD_MASTER("elecServProvMaster",ElecServProvMaster.class),
    RELIANCE_DEALER_BRANCH_MASTER("relianceDealerBranchMaster",RelianceDealerBranchMaster.class),
    CITY_MASTER("cityMaster",CityMaster.class),
    CITY_MASTER_V1("cityMaster",CityMaster.class),
    PAYNIMO_BANK_MASTER("paynimoBankMaster",PaynimoBankMaster.class),
    SUPPLIER_LOCATION_MASTER("supplierLocationMaster",SupplierLocationMaster.class),
    DIGITIZATION_DEALER_EMAIL_MASTER("digitizationDealerEmailMaster",DigitizationDealerEmailMaster.class),
    MANUFACTURER_MASTER("manufacturerMaster",ManufacturerMaster.class),
    TCLOS_GNG_MAPPING_MASTER("tCLosGngMappingMaster",TCLosGngMappingMaster.class),
    STATE_BRANCH_MASTER("stateBranchMaster",StateBranchMaster.class),
    ACCOUNTS_HEAD_MASTER("accountsHeadMaster",AccountsHeadMaster.class),
    TAX_CODE_MASTER("taxCodeMaster",TaxCodeMaster.class),
    BANK_MASTER("bankMaster",BankMaster.class),
    PROMOTIONAL_SCHEME_MASTER("promotionalSchemeMaster",PromotionalSchemeMaster.class),
    TKIL_DEALER_BRANCH_MASTER("tkilDealerBranchMaster",TkilDealerBranchMaster.class),
    SOURCING_DETAIL_MASTER("sourcingDetailMaster",SourcingDetailMaster.class),
    ELEC_SERV_PROV_MASTER("elecServProvMaster",ElecServProvMaster.class),
    EW_PREMIUM_MASTER("eWPremiumMaster",EWPremiumMaster.class),
    EW_INS_GNG_ASSET_CAT_MASTER("ewInsGngAssetCategoryMaster",EwInsGngAssetCategoryMaster.class),
    COMMON_GENERAL_MASTER("commonGeneralParameterMaster",CommonGeneralParameterMaster.class),
    LOS_BANKING_MASTER("losBankMaster",LosBankMaster.class),
    BANKING_MASTER("bankDetailsMaster",BankDetailsMaster.class),
    LOYALTY_CARD_MASTER("loyaltyCardMaster",LoyaltyCardMaster.class),
    REFERENCE_MASTER("referenceDetailsMaster",ReferenceDetailsMaster.class),
    NET_DISBURSAL_DETAILS_MASTER("netDisbursalDetailsMaster",NetDisbursalDetailsMaster.class),
    LOS_UTR_DETAILS_MASTER("losUtrDetailsMaster",LosUtrDetailsMaster.class),
    NEGATIVE_AREA_GEO_LIMIT_MASTER("negativeAreaFundingMaster",NegativeAreaGeoLimitMaster.class),
    DSA_PRODUCT_MASTER("dsaProductMaster",DSAProductMaster.class),
    DSA_BRANCH_MASTER("dsaBranchMaster",DSABranchMaster.class),
    CREDIT_PROMOTION_MASTER("creditPromotionMaster",CreditPromotionMaster.class),
    MODEL_VARIANT_MASTER("ModelVariantMaster",ModelVariantMaster.class),
    VERSION_MASTER("AppVersionControl", VersionMaster.class),
    CDL_HIERARCHY_MASTER("CDLHierarchyMaster",CDLHierarchyMaster.class),
    HIERARCHY_MASTER("HierarchyMaster",HierarchyMaster.class),
    BANK_DETAILS_MASTER("bankDetailsMaster",BankDetailsMaster.class),
    PL_PINCODE_EMAIL_MASTER("PlPincodeEmailMaster",PlPincodeEmailMaster.class),
    CAR_SURROGATE_MASTER("CarSurrogateMaster",CarSurrogateMaster.class),
    GNG_CC_DEALEREMAIL_MASTER("GNGDealerEmailMaster",GNGDealerEmailMaster.class),
    UPDATE_LOS_UTR_DETAILS_MASTER("losUtrDetailsMaster",LosUtrDetailsMaster.class),
    /**
     * this master is a transaction master
     */

    POTENTIAL_MASTER("potentialMatser",Object.class),
    MIGRATE_DATA_FOR_CYOR("migrateDataForCyor",Object.class),
    DROP_DOWN_MASTER("dropDownMaster",Object.class);

    String masterName;
    Class masterClass;

    MasterMappingDetails(String masterNm, Class masterClss) {
        masterName = masterNm;
        masterClass = masterClss;
    }


    public String getMasterName() {
        return masterName;
    }


    public Class getMasterClass() {
        return masterClass;
    }
}
