/**
 * yogeshb4:04:02 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.scoring;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb
 *
 */
@Document(collection = "addressScoringResult")
public class AddressScoringResult extends AuditEntity {
    private float score;
    private String addType;
    private String add1;
    private String add2;
    private String gngRefNo;
    private int cibilAddSubdateToDateDays;


    public int getCibilAddSubdateToDateDays() {
        return cibilAddSubdateToDateDays;
    }

    public void setCibilAddSubdateToDateDays(int cibilAddSubdateToDateDays) {
        this.cibilAddSubdateToDateDays = cibilAddSubdateToDateDays;
    }

    public String getAddType() {
        return addType;
    }

    public void setAddType(String addType) {
        this.addType = addType;
    }

    public String getAdd1() {
        return add1;
    }

    public void setAdd1(String add1) {
        this.add1 = add1;
    }

    public String getAdd2() {
        return add2;
    }

    public void setAdd2(String add2) {
        this.add2 = add2;
    }

    public String getGngRefNo() {
        return gngRefNo;
    }

    public void setGngRefNo(String gngRefNo) {
        this.gngRefNo = gngRefNo;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }


}
