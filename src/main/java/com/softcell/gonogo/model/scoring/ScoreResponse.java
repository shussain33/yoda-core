/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 5, 2016 2:49:46 PM
 */
package com.softcell.gonogo.model.scoring;

/**
 * @author kishorp
 *
 */
public class ScoreResponse {
    private int applicationScore;
    private String scoreJsonString;

    /**
     * @return the applicationScore
     */
    public int getApplicationScore() {
        return applicationScore;
    }

    /**
     * @param applicationScore the applicationScore to set
     */
    public void setApplicationScore(int applicationScore) {
        this.applicationScore = applicationScore;
    }

    /**
     * @return the scoreJsonString
     */
    public String getScoreJsonString() {
        return scoreJsonString;
    }

    /**
     * @param scoreJsonString the scoreJsonString to set
     */
    public void setScoreJsonString(String scoreJsonString) {
        this.scoreJsonString = scoreJsonString;
    }

}
