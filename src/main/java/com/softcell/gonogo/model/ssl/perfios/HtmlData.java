package com.softcell.gonogo.model.ssl.perfios;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0293 on 25/7/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HtmlData {
    @JsonProperty("htmlData")
    private String htmlData;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("vendorTransactionId")
    private String vendorTransactionId;

    @JsonProperty("path")
    private String path;

    @JsonProperty("timestamp")
    private String timestamp;
}
