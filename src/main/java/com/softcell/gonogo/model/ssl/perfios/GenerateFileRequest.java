package com.softcell.gonogo.model.ssl.perfios;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

/**
 * Created by ssg0293 on 25/7/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GenerateFileRequest {

    @JsonProperty("oHeader")
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sEmploymentType")
    private String employmentType;

    @JsonProperty("sDestination")
    private String destination;

    @JsonProperty("sReturnUrl")
    private String returnUrl;

    @JsonProperty("sYearMonthTo")
    private String yearMonthTo;

    @JsonProperty("sYearMonthFrom")
    private String yearMonthFrom;

    @JsonProperty("iLoanAmount")
    private Integer loanAmount;

    @JsonProperty("iLoanDuration")
    private Integer loanDuration;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("sIncomeTaxDocuments")
    private IncomeTaxDocuments incomeTaxDocuments;

    @JsonProperty("sAcceptancePolicy")
    private String acceptancePolicy;

    @JsonProperty("sForm26ASDOB")
    private String form26ASDOB;

    @JsonProperty("sAccountNumber")
    private String accountNumber;

    @JsonProperty("sProductType")
    private String productType;

    @JsonProperty("sFacility")
    private String facility;

    @JsonProperty("sSanctionLimitFixed")
    private String sanctionLimitFixed;

    @JsonProperty("sSanctionLimitFixedAmount")
    private String sanctionLimitFixedAmount;

    @JsonProperty("oSanctionLimitVariableAmounts")
    private SanctionLimitVariableAmnt sanctionLimitVariableAmounts;

    @JsonProperty("oDrawingPowerVariableAmounts")
    private DrawingPowerVariableAmnts drawingPowerVariableAmounts;

    @JsonProperty("oSisterCompanyPatterns")
    private SisterCompanyPatterns sisterCompanyPatterns;

    @JsonProperty("oCompanyNames")
    private CompanyNames companyNames;
}
