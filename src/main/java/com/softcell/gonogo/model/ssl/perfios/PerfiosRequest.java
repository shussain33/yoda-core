package com.softcell.gonogo.model.ssl.perfios;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Date;
import java.util.Map;

/**
 * Created by archana on 26/9/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PerfiosRequest {
    @JsonProperty("oHeader")
    @NotEmpty(groups = {Header.InsertGroup.class,Header.FetchGrp.class,PerfiosRequest.InsertGrp.class})
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {PerfiosRequest.InsertGrp.class})
    private String refId;

    @JsonProperty("sAckId")
    private String acknowledgeId;

    @JsonProperty("oData")
    private Map<String, Object> data;

    @JsonProperty("sStatement")
    private String statement;

    @JsonProperty("sAppName")
    private String applIcantName;

    @JsonProperty("sAppId")
    private String appId;

    @JsonProperty("dtFromDate")
    private Date fromDate = new Date();

    @JsonProperty("dtToDate")
    private Date toDate = new Date();
    public interface InsertGrp {
    }
}
