package com.softcell.gonogo.model.tclos.log;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.tclos.request.TCLosInputRequest;
import com.softcell.gonogo.model.tclos.response.TCLosResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tcLOSReqResLog")
public class TcLosReqReslog {
    @JsonProperty("ref_id")
    private String refId;

    @JsonProperty("request")
    private TCLosInputRequest request;


    @JsonProperty("response")
    private TCLosResponse response;

    @JsonProperty("status")
    private String status;

    @JsonProperty("requestDt")
    private Date requestDt;

    @JsonProperty("responseDt")
    private Date responseDt;

    @JsonProperty("failureReason")
    private String failureReason;

}
