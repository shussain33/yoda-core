package com.softcell.gonogo.model.tclos.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.TCLosData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TCLosInputRequest {
    @JsonProperty("name")
    private String name;

    @JsonProperty("key")
    private String key;

    @JsonProperty("data")
    private List<TCLosData> tcLosData;

    @JsonProperty("action")
    private String action;

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public List<TCLosData> getTcLosData() {
        return tcLosData;
    }

    public String getAction() {
        return action;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setTcLosData(List<TCLosData> tcLosData) {
        this.tcLosData = tcLosData;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
