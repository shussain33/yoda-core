package com.softcell.gonogo.model.insurance.religare;
import java.util.HashMap;
 import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
        import com.fasterxml.jackson.annotation.JsonIgnore;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.fasterxml.jackson.annotation.JsonProperty;
        import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "guid",
        "firstName",
        "lastName",
        "roleCd",
        "birthDt",
        "genderCd",
        "titleCd",
        "relationCd",
        "partyIdentityDOList",
        "partyAddressDOList",
        "partyContactDOList",
        "partyEmailDOList",
        "partyQuestionDOList",
        "partyEmploymentDOList"
})
public class PartyDOList {

    @JsonProperty("guid")
    private String guid;
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("roleCd")
    private String roleCd;
    @JsonProperty("birthDt")
    private String birthDt;
    @JsonProperty("genderCd")
    private String genderCd;
    @JsonProperty("titleCd")
    private String titleCd;
    @JsonProperty("relationCd")
    private String relationCd;
    @JsonProperty("partyIdentityDOList")
    private List<PartyIdentityDOList> partyIdentityDOList = null;
    @JsonProperty("partyAddressDOList")
    private List<PartyAddressDOList> partyAddressDOList = null;
    @JsonProperty("partyContactDOList")
    private List<PartyContactDOList> partyContactDOList = null;
    @JsonProperty("partyEmailDOList")
    private List<PartyEmailDOList> partyEmailDOList = null;
    @JsonProperty("partyQuestionDOList")
    private List<PartyQuestionDOList> partyQuestionDOList = null;
    @JsonProperty("partyEmploymentDOList")
    private List<PartyEmploymentDOList> partyEmploymentDOList = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("guid")
    public String getGuid() {
        return guid;
    }

    @JsonProperty("guid")
    public void setGuid(String guid) {
        this.guid = guid;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("roleCd")
    public String getRoleCd() {
        return roleCd;
    }

    @JsonProperty("roleCd")
    public void setRoleCd(String roleCd) {
        this.roleCd = roleCd;
    }

    @JsonProperty("birthDt")
    public String getBirthDt() {
        return birthDt;
    }

    @JsonProperty("birthDt")
    public void setBirthDt(String birthDt) {
        this.birthDt = birthDt;
    }

    @JsonProperty("genderCd")
    public String getGenderCd() {
        return genderCd;
    }

    @JsonProperty("genderCd")
    public void setGenderCd(String genderCd) {
        this.genderCd = genderCd;
    }

    @JsonProperty("titleCd")
    public String getTitleCd() {
        return titleCd;
    }

    @JsonProperty("titleCd")
    public void setTitleCd(String titleCd) {
        this.titleCd = titleCd;
    }

    @JsonProperty("relationCd")
    public String getRelationCd() {
        return relationCd;
    }

    @JsonProperty("relationCd")
    public void setRelationCd(String relationCd) {
        this.relationCd = relationCd;
    }

    @JsonProperty("partyIdentityDOList")
    public List<PartyIdentityDOList> getPartyIdentityDOList() {
        return partyIdentityDOList;
    }

    @JsonProperty("partyIdentityDOList")
    public void setPartyIdentityDOList(List<PartyIdentityDOList> partyIdentityDOList) {
        this.partyIdentityDOList = partyIdentityDOList;
    }

    @JsonProperty("partyAddressDOList")
    public List<PartyAddressDOList> getPartyAddressDOList() {
        return partyAddressDOList;
    }

    @JsonProperty("partyAddressDOList")
    public void setPartyAddressDOList(List<PartyAddressDOList> partyAddressDOList) {
        this.partyAddressDOList = partyAddressDOList;
    }

    @JsonProperty("partyContactDOList")
    public List<PartyContactDOList> getPartyContactDOList() {
        return partyContactDOList;
    }

    @JsonProperty("partyContactDOList")
    public void setPartyContactDOList(List<PartyContactDOList> partyContactDOList) {
        this.partyContactDOList = partyContactDOList;
    }

    @JsonProperty("partyEmailDOList")
    public List<PartyEmailDOList> getPartyEmailDOList() {
        return partyEmailDOList;
    }

    @JsonProperty("partyEmailDOList")
    public void setPartyEmailDOList(List<PartyEmailDOList> partyEmailDOList) {
        this.partyEmailDOList = partyEmailDOList;
    }

    @JsonProperty("partyQuestionDOList")
    public List<PartyQuestionDOList> getPartyQuestionDOList() {
        return partyQuestionDOList;
    }

    @JsonProperty("partyQuestionDOList")
    public void setPartyQuestionDOList(List<PartyQuestionDOList> partyQuestionDOList) {
        this.partyQuestionDOList = partyQuestionDOList;
    }

    @JsonProperty("partyEmploymentDOList")
    public List<PartyEmploymentDOList> getPartyEmploymentDOList() {
        return partyEmploymentDOList;
    }

    @JsonProperty("partyEmploymentDOList")
    public void setPartyEmploymentDOList(List<PartyEmploymentDOList> partyEmploymentDOList) {
        this.partyEmploymentDOList = partyEmploymentDOList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}