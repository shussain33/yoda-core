package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by archana on 19/12/18.
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.softcell.gonogo.model.core.kyc.request.aadhar.OtpDetails;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "appNo",
        "advisorCode",
        "source",
        "sourceKey",
        "salesDataReqd",
        "dependentFlag",
        "sourceTransactionId",
        "sourceOfFund",
        "sourceOfFundDesc",
        "buyersPinCode",
        "sellersPinCode",
        "clientId",
        "uidId",
        "proposerInfos",
        "healthDetails",
        "productSelection",
        "nomineeInfos",
        "advisorSalesDetails",
        "otpDetails",
        "paymentData"
})
public class IciciRequest {

    @JsonProperty("appNo")
    public String appNo;
    @JsonProperty("advisorCode")
    public String advisorCode;
    @JsonProperty("source")
    public String source;
    @JsonProperty("sourceKey")
    public String sourceKey;
    @JsonProperty("salesDataReqd")
    public String salesDataReqd;
    @JsonProperty("dependentFlag")
    public String dependentFlag;
    @JsonProperty("sourceTransactionId")
    public String sourceTransactionId;
    @JsonProperty("sourceOfFund")
    public String sourceOfFund;
    @JsonProperty("sourceOfFundDesc")
    public String sourceOfFundDesc;
    @JsonProperty("buyersPinCode")
    public String buyersPinCode;
    @JsonProperty("sellersPinCode")
    public String sellersPinCode;
    @JsonProperty("clientId")
    public String clientId;
    @JsonProperty("uidId")
    public String uidId;
    @JsonProperty("proposerInfos")
    public ProposerInfos proposerInfos;
    @JsonProperty("healthDetails")
    public List<HealthDetail> healthDetails = null;
    @JsonProperty("productSelection")
    public ProductSelection productSelection;
    @JsonProperty("nomineeInfos")
    public NomineeInfos nomineeInfos;
    @JsonProperty("advisorSalesDetails")
    public AdvisorSalesDetails advisorSalesDetails;
    @JsonProperty("otpDetails")
    public com.softcell.gonogo.model.insurance.icici.OtpDetails otpDetails;
    @JsonProperty("paymentData")
    public PaymentData paymentData;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
