package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by ssg0268 on 8/11/19.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "masterCode",
        "benefitOption",
        "premiumPayingFrequency",
        "policyTerm",
        "premiumPayingTerm",
        "modalPremium",
        "sumAssured",
        "totalPremium",
        "productName",
        "productId",
        "LifeCoverOption",
        "premiumpaymentoption",
        "loanAmount",
        "loanTenure",
        "CIBenefit",
        "ADHB",
        "rider1",
        "coverageOption"
})
public class ProductSelection {

    @JsonProperty("masterCode")
    public String masterCode;
    @JsonProperty("benefitOption")
    public String benefitOption;
    @JsonProperty("premiumPayingFrequency")
    public String premiumPayingFrequency;
    @JsonProperty("policyTerm")
    public String policyTerm;
    @JsonProperty("premiumPayingTerm")
    public String premiumPayingTerm;
    @JsonProperty("modalPremium")
    public String modalPremium;
    @JsonProperty("sumAssured")
    public String sumAssured;
    @JsonProperty("totalPremium")
    public String totalPremium;
    @JsonProperty("productName")
    public String productName;
    @JsonProperty("productId")
    public String productId;
    @JsonProperty("LifeCoverOption")
    public String lifeCoverOption;
    @JsonProperty("premiumpaymentoption")
    public String premiumpaymentoption;
    @JsonProperty("loanAmount")
    public String loanAmount;
    @JsonProperty("loanTenure")
    public String loanTenure;
    @JsonProperty("CIBenefit")
    public String cIBenefit;
    @JsonProperty("ADHB")
    public String aDHB;
    @JsonProperty("rider1")
    public Rider1 rider1;
    @JsonProperty("coverageOption")
    public String coverageOption;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}