package com.softcell.gonogo.model.insurance.icici;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 28/2/19.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IciciPremiumCalculationRequest {
    @JsonProperty("FirstName")
    public String FirstName;
    @JsonProperty("LastName")
    public String LastName;
    @JsonProperty("DateOfBirth")
    public String DateOfBirth;
    @JsonProperty("Gender")
    public String Gender;
    @JsonProperty("MaritalStatus")
    public String MaritalStatus;
    @JsonProperty("ProductDetails")
    public ProductDetails ProductDetails;
}
