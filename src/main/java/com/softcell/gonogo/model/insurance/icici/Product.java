package com.softcell.gonogo.model.insurance.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 8/11/19.
 */
public class Product {
    @JsonProperty("ProductType")
    public String ProductType;
    @JsonProperty("ProductName")
    public String ProductName;
    @JsonProperty("ProductCode")
    public String ProductCode;
    @JsonProperty("ModeOfPayment")
    public String ModeOfPayment;
    @JsonProperty("ModalPremium")
    public String ModalPremium;
    @JsonProperty("AnnualPremium")
    public String AnnualPremium;
    @JsonProperty("Term")
    public String Term;
    @JsonProperty("DeathBenefit")
    public String DeathBenefit;
    @JsonProperty("PremiumPaymentTerm")
    public String PremiumPaymentTerm;
    @JsonProperty("LoanTenure")
    public String LoanTenure;
    @JsonProperty("LoanAmount")
    public String LoanAmount;
    @JsonProperty("PremiumPaymentOption")
    public String PremiumPaymentOption;
    @JsonProperty("CoverageOption")
    public String CoverageOption;
    @JsonProperty("BenefitOption")
    public String BenefitOption;
    @JsonProperty("IPAddress")
    public String IPAddress = "0:0:0:0:0:0:0:1";
    @JsonProperty("MasterCode")
    public String MasterCode;
}
