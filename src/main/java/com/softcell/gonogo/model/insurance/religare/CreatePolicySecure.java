
package com.softcell.gonogo.model.insurance.religare;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "intPolicyDataIO"
})
public class CreatePolicySecure {

    @JsonProperty("intPolicyDataIO")
    private IntPolicyDataIO intPolicyDataIO;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("intPolicyDataIO")
    public IntPolicyDataIO getIntPolicyDataIO() {
        return intPolicyDataIO;
    }

    @JsonProperty("intPolicyDataIO")
    public void setIntPolicyDataIO(IntPolicyDataIO intPolicyDataIO) {
        this.intPolicyDataIO = intPolicyDataIO;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}