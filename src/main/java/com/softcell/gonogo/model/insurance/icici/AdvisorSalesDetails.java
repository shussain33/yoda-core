package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by ssg0268 on 8/11/19.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "channelType",
        "cusBankAccNo",
        "bankName",
        "needRiskProfile",
        "csrLimCode",
        "cafosCode",
        "oppId",
        "fscCode",
        "spCode",
        "bankBrnch",
        "source",
        "lanNo",
        "selectedTab",
        "subChannel"
})
public class AdvisorSalesDetails {

    @JsonProperty("channelType")
    public String channelType;
    @JsonProperty("cusBankAccNo")
    public String cusBankAccNo;
    @JsonProperty("bankName")
    public String bankName;
    @JsonProperty("needRiskProfile")
    public String needRiskProfile;
    @JsonProperty("csrLimCode")
    public String csrLimCode;
    @JsonProperty("cafosCode")
    public String cafosCode;
    @JsonProperty("oppId")
    public String oppId;
    @JsonProperty("fscCode")
    public String fscCode;
    @JsonProperty("spCode")
    public String spCode;
    @JsonProperty("bankBrnch")
    public String bankBrnch;
    @JsonProperty("source")
    public String source;
    @JsonProperty("lanNo")
    public String lanNo;
    @JsonProperty("selectedTab")
    public String selectedTab;
    @JsonProperty("subChannel")
    public String subChannel;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}