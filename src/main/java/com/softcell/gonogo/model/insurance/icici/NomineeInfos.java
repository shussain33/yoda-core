package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by archana on 19/12/18.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "apnteDtls",
        "frstNm",
        "relationship",
        "gender",
        "dob",
        "lstNm"
})
public class NomineeInfos {

    @JsonProperty("apnteDtls")
    public ApnteDtls apnteDtls;
    @JsonProperty("frstNm")
    public String frstNm;
    @JsonProperty("relationship")
    public String relationship;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("dob")
    public String dob;
    @JsonProperty("lstNm")
    public String lstNm;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
