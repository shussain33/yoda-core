
package com.softcell.gonogo.model.insurance.religare;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "partyDOList",
        "policyAdditionalFieldsDOList",
        "isPremiumCalculation",
        "businessTypeCd",
        "premium",
        "baseAgentId",
        "proposalNum",
        "baseProductId",
        "sumInsured",
        "term",
        "coverType",
        "uwDecisionCd",
        "loanTenure",
        "loanTenureUnitCd",
        "loanDisbursalDt",
        "loanAccNum",
        "loanAmount"
})
public class Policy {

    @JsonProperty("partyDOList")
    private List<PartyDOList> partyDOList = null;
    @JsonProperty("policyAdditionalFieldsDOList")
    private List<PolicyAdditionalFieldsDOList> policyAdditionalFieldsDOList = null;
    @JsonProperty("isPremiumCalculation")
    private String isPremiumCalculation;
    @JsonProperty("businessTypeCd")
    private String businessTypeCd;
    @JsonProperty("premium")
    private Integer premium;
    @JsonProperty("baseAgentId")
    private String baseAgentId;
    @JsonProperty("proposalNum")
    private String proposalNum;
    @JsonProperty("baseProductId")
    private String baseProductId;
    @JsonProperty("sumInsured")
    private String sumInsured;
    @JsonProperty("term")
    private Integer term;
    @JsonProperty("coverType")
    private String coverType;
    @JsonProperty("uwDecisionCd")
    private String uwDecisionCd;
    @JsonProperty("loanTenure")
    private String loanTenure;
    @JsonProperty("loanTenureUnitCd")
    private String loanTenureUnitCd;
    @JsonProperty("loanDisbursalDt")
    private String loanDisbursalDt;
    @JsonProperty("loanAccNum")
    private String loanAccNum;
    @JsonProperty("loanAmount")
    private String loanAmount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("partyDOList")
    public List<PartyDOList> getPartyDOList() {
        return partyDOList;
    }

    @JsonProperty("partyDOList")
    public void setPartyDOList(List<PartyDOList> partyDOList) {
        this.partyDOList = partyDOList;
    }

    @JsonProperty("policyAdditionalFieldsDOList")
    public List<PolicyAdditionalFieldsDOList> getPolicyAdditionalFieldsDOList() {
        return policyAdditionalFieldsDOList;
    }

    @JsonProperty("policyAdditionalFieldsDOList")
    public void setPolicyAdditionalFieldsDOList(List<PolicyAdditionalFieldsDOList> policyAdditionalFieldsDOList) {
        this.policyAdditionalFieldsDOList = policyAdditionalFieldsDOList;
    }

    @JsonProperty("isPremiumCalculation")
    public String getIsPremiumCalculation() {
        return isPremiumCalculation;
    }

    @JsonProperty("isPremiumCalculation")
    public void setIsPremiumCalculation(String isPremiumCalculation) {
        this.isPremiumCalculation = isPremiumCalculation;
    }

    @JsonProperty("businessTypeCd")
    public String getBusinessTypeCd() {
        return businessTypeCd;
    }

    @JsonProperty("businessTypeCd")
    public void setBusinessTypeCd(String businessTypeCd) {
        this.businessTypeCd = businessTypeCd;
    }

    @JsonProperty("premium")
    public Integer getPremium() {
        return premium;
    }

    @JsonProperty("premium")
    public void setPremium(Integer premium) {
        this.premium = premium;
    }

    @JsonProperty("baseAgentId")
    public String getBaseAgentId() {
        return baseAgentId;
    }

    @JsonProperty("baseAgentId")
    public void setBaseAgentId(String baseAgentId) {
        this.baseAgentId = baseAgentId;
    }

    @JsonProperty("proposalNum")
    public String getProposalNum() {
        return proposalNum;
    }

    @JsonProperty("proposalNum")
    public void setProposalNum(String proposalNum) {
        this.proposalNum = proposalNum;
    }

    @JsonProperty("baseProductId")
    public String getBaseProductId() {
        return baseProductId;
    }

    @JsonProperty("baseProductId")
    public void setBaseProductId(String baseProductId) {
        this.baseProductId = baseProductId;
    }

    @JsonProperty("sumInsured")
    public String getSumInsured() {
        return sumInsured;
    }

    @JsonProperty("sumInsured")
    public void setSumInsured(String sumInsured) {
        this.sumInsured = sumInsured;
    }

    @JsonProperty("term")
    public Integer getTerm() {
        return term;
    }

    @JsonProperty("term")
    public void setTerm(Integer term) {
        this.term = term;
    }

    @JsonProperty("coverType")
    public String getCoverType() {
        return coverType;
    }

    @JsonProperty("coverType")
    public void setCoverType(String coverType) {
        this.coverType = coverType;
    }

    @JsonProperty("uwDecisionCd")
    public String getUwDecisionCd() {
        return uwDecisionCd;
    }

    @JsonProperty("uwDecisionCd")
    public void setUwDecisionCd(String uwDecisionCd) {
        this.uwDecisionCd = uwDecisionCd;
    }

    @JsonProperty("loanTenure")
    public String getLoanTenure() {
        return loanTenure;
    }

    @JsonProperty("loanTenure")
    public void setLoanTenure(String loanTenure) {
        this.loanTenure = loanTenure;
    }

    @JsonProperty("loanTenureUnitCd")
    public String getLoanTenureUnitCd() {
        return loanTenureUnitCd;
    }

    @JsonProperty("loanTenureUnitCd")
    public void setLoanTenureUnitCd(String loanTenureUnitCd) {
        this.loanTenureUnitCd = loanTenureUnitCd;
    }

    @JsonProperty("loanDisbursalDt")
    public String getLoanDisbursalDt() {
        return loanDisbursalDt;
    }

    @JsonProperty("loanDisbursalDt")
    public void setLoanDisbursalDt(String loanDisbursalDt) {
        this.loanDisbursalDt = loanDisbursalDt;
    }

    @JsonProperty("loanAccNum")
    public String getLoanAccNum() {
        return loanAccNum;
    }

    @JsonProperty("loanAccNum")
    public void setLoanAccNum(String loanAccNum) {
        this.loanAccNum = loanAccNum;
    }

    @JsonProperty("loanAmount")
    public String getLoanAmount() {
        return loanAmount;
    }

    @JsonProperty("loanAmount")
    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}