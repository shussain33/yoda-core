package com.softcell.gonogo.model.insurance.religare.religareResponse;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.softcell.gonogo.model.insurance.religare.IntPolicyDataIO;


/**
 * Created by ssg0268 on 21/3/19.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "responseData",
        "intPolicyDataIO"
})
public class Religare {

    @JsonProperty("responseData")
    private ResponseData responseData;
    @JsonProperty("intPolicyDataIO")
    private IntPolicyDataIO intPolicyDataIO;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("responseData")
    public ResponseData getResponseData() {
        return responseData;
    }

    @JsonProperty("responseData")
    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    @JsonProperty("intPolicyDataIO")
    public IntPolicyDataIO getIntPolicyDataIO() {
        return intPolicyDataIO;
    }

    @JsonProperty("intPolicyDataIO")
    public void setIntPolicyDataIO(IntPolicyDataIO intPolicyDataIO) {
        this.intPolicyDataIO = intPolicyDataIO;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}