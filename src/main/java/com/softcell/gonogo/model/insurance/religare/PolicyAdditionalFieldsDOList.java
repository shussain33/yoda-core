package com.softcell.gonogo.model.insurance.religare;

 import java.util.HashMap;
 import java.util.Map;
       import com.fasterxml.jackson.annotation.JsonAnyGetter;
        import com.fasterxml.jackson.annotation.JsonAnySetter;
        import com.fasterxml.jackson.annotation.JsonIgnore;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.fasterxml.jackson.annotation.JsonProperty;
        import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "field1",
        "field10",
        "field12",
        "fieldAgree",
        "fieldAlerts",
        "fieldTc"
})
public class PolicyAdditionalFieldsDOList {

    @JsonProperty("field1")
    private String field1;
    @JsonProperty("field10")
    private String field10;
    @JsonProperty("field12")
    private String field12;
    @JsonProperty("fieldAgree")
    private String fieldAgree;
    @JsonProperty("fieldAlerts")
    private String fieldAlerts;
    @JsonProperty("fieldTc")
    private String fieldTc;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("field1")
    public String getField1() {
        return field1;
    }

    @JsonProperty("field1")
    public void setField1(String field1) {
        this.field1 = field1;
    }

    @JsonProperty("field10")
    public String getField10() {
        return field10;
    }

    @JsonProperty("field10")
    public void setField10(String field10) {
        this.field10 = field10;
    }

    @JsonProperty("field12")
    public String getField12() {
        return field12;
    }

    @JsonProperty("field12")
    public void setField12(String field12) {
        this.field12 = field12;
    }

    @JsonProperty("fieldAgree")
    public String getFieldAgree() {
        return fieldAgree;
    }

    @JsonProperty("fieldAgree")
    public void setFieldAgree(String fieldAgree) {
        this.fieldAgree = fieldAgree;
    }

    @JsonProperty("fieldAlerts")
    public String getFieldAlerts() {
        return fieldAlerts;
    }

    @JsonProperty("fieldAlerts")
    public void setFieldAlerts(String fieldAlerts) {
        this.fieldAlerts = fieldAlerts;
    }

    @JsonProperty("fieldTc")
    public String getFieldTc() {
        return fieldTc;
    }

    @JsonProperty("fieldTc")
    public void setFieldTc(String fieldTc) {
        this.fieldTc = fieldTc;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}