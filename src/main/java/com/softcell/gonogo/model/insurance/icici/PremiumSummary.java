package com.softcell.gonogo.model.insurance.icici;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 1/3/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PremiumSummary {

    @JsonProperty("ProductType")
    private String ProductType;
    @JsonProperty("ProductName")
    private String ProductName;
    @JsonProperty("ProductCode")
    private String ProductCode;
    @JsonProperty("State")
    private Integer State;
    @JsonProperty("City")
    private Integer City;
    @JsonProperty("ModeOfPayment")
    private String ModeOfPayment;
    @JsonProperty("PremiumPaymentOption")
    private String PremiumPaymentOption;
    @JsonProperty("PremiumPaymentTerm")
    private Integer PremiumPaymentTerm;
    @JsonProperty("MasterCode")
    private String MasterCode;
    @JsonProperty("Age")
    private Integer Age;
    @JsonProperty("AnnualPremium")
    private Integer AnnualPremium;
    @JsonProperty("Term")
    private Integer Term;
    @JsonProperty("DeathBenefit")
    private Integer DeathBenefit;
    @JsonProperty("BasePremium")
    private Integer BasePremium;
    @JsonProperty("CoverageOption")
    private String CoverageOption;
    @JsonProperty("ADBRPremium")
    private Integer ADBRPremium;
    @JsonProperty("ABRPremium")
    private Integer ABRPremium;
    @JsonProperty("CIRPremium")
    private Integer CIRPremium;
    @JsonProperty("TPDPremium")
    private Integer TPDPremium;
    @JsonProperty("IBRPremium")
    private Integer IBRPremium;
    @JsonProperty("WOPPremium")
    private Integer WOPPremium;
    @JsonProperty("PremiumInstallment")
    private Integer PremiumInstallment;
    @JsonProperty("PremiumInstallmentWithTax")
    private Integer PremiumInstallmentWithTax;
    @JsonProperty("ServiceTax")
    private Integer ServiceTax;
    @JsonProperty("EduCess")
    private Integer EduCess;
    @JsonProperty("KrishiKalyan")
    private Integer KrishiKalyan;
    @JsonProperty("TotalFirstPremium")
    private Integer TotalFirstPremium;
    @JsonProperty("TotalFirstPremiumShow")
    private Integer TotalFirstPremiumShow;
    @JsonProperty("AnnualPremiumwithoutTax")
    private Integer AnnualPremiumwithoutTax;
    @JsonProperty("AnnualPremiumWithTax")
    private Integer AnnualPremiumWithTax;
    @JsonProperty("ADBSumAssured")
    private Integer ADBSumAssured;
    @JsonProperty("CIRSumAssured")
    private Integer CIRSumAssured;
    @JsonProperty("TPDSumAssured")
    private Integer TPDSumAssured;
    @JsonProperty("ServiceTax_US")
    private Integer ServiceTax_US;
    @JsonProperty("EduCess_US")
    private Integer EduCess_US;
    @JsonProperty("KrishiKalyan_US")
    private Integer KrishiKalyan_US;
    @JsonProperty("ModalPremium")
    private Integer ModalPremium;
    @JsonProperty("SalesChannel")
    private Integer SalesChannel;
    @JsonProperty("StatisticalCode")
    private Object StatisticalCode;
    @JsonProperty("BenefitOption")
    private Integer BenefitOption;
    @JsonProperty("LoanTenure")
    private Integer LoanTenure;
    @JsonProperty("LoanAmount")
    private Integer LoanAmount;
    @JsonProperty("ADHB")
    private Integer ADHB;
}
