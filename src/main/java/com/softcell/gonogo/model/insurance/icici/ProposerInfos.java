package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by archana on 19/12/18.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "frstNm",
        "lstNm",
        "dob",
        "gender",
        "mobNo",
        "comunctnAddress",
        "prmntAddress",
        "email",
        "occ",
        "lan",
        "loanStatus",
        "panNo",
        "aadhaarOptionSelected",
        "aadharCardNo",
        "dateOfCommencementOfLoan"
})
public class ProposerInfos {

    @JsonProperty("frstNm")
    public String frstNm;
    @JsonProperty("lstNm")
    public String lstNm;
    @JsonProperty("dob")
    public String dob;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("mobNo")
    public String mobNo;
    @JsonProperty("comunctnAddress")
    public Address comunctnAddress;
    @JsonProperty("prmntAddress")
    public Address prmntAddress;
    @JsonProperty("email")
    public String email;
    @JsonProperty("occ")
    public String occ;
    @JsonProperty("lan")
    public String lan;
    @JsonProperty("loanStatus")
    public String loanStatus;
    @JsonProperty("panNo")
    public String panNo;
    @JsonProperty("aadhaarOptionSelected")
    public String aadhaarOptionSelected;
    @JsonProperty("aadharCardNo")
    public String aadharCardNo;
    @JsonProperty("dateOfCommencementOfLoan")
    public String dateOfCommencementOfLoan;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
