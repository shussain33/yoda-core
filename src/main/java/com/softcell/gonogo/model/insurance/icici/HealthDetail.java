package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by ssg0268 on 8/11/19.
 */


        import java.util.HashMap;
        import java.util.Map;
        import com.fasterxml.jackson.annotation.JsonAnyGetter;
        import com.fasterxml.jackson.annotation.JsonAnySetter;
        import com.fasterxml.jackson.annotation.JsonIgnore;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.fasterxml.jackson.annotation.JsonProperty;
        import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "answer1",
        "answer3",
        "answer2",
        "answer4",
        "address2",
        "answer5",
        "answer6",
        "answer7",
        "answer8",
        "answer9",
        "answer10",
        "answer11",
        "answer12",
        "answer13",
        "answer14",
        "answer15"
})
public class HealthDetail {

    @JsonProperty("code")
    public String code;
    @JsonProperty("answer1")
    public String answer1;
    @JsonProperty("answer3")
    public String answer3;
    @JsonProperty("answer2")
    public String answer2;
    @JsonProperty("answer4")
    public String answer4;
    @JsonProperty("address2")
    public String address2;
    @JsonProperty("answer5")
    public String answer5;
    @JsonProperty("answer6")
    public String answer6;
    @JsonProperty("answer7")
    public String answer7;
    @JsonProperty("answer8")
    public String answer8;
    @JsonProperty("answer9")
    public String answer9;
    @JsonProperty("answer10")
    public String answer10;
    @JsonProperty("answer11")
    public String answer11;
    @JsonProperty("answer12")
    public String answer12;
    @JsonProperty("answer13")
    public String answer13;
    @JsonProperty("answer14")
    public String answer14;
    @JsonProperty("answer15")
    public String answer15;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
