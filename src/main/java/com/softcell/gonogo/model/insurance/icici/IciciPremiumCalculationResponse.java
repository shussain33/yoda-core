package com.softcell.gonogo.model.insurance.icici;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 1/3/19.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IciciPremiumCalculationResponse {

    @JsonProperty("FirstName")
    private Object FirstName;
    @JsonProperty("LastName")
    private Object LastName;
    @JsonProperty("DateOfBirth")
    private String DateOfBirth;
    @JsonProperty("Gender")
    private String Gender;
    @JsonProperty("MaritalStatus")
    private Object MaritalStatus;
    @JsonProperty("Staff")
    private String Staff;
    @JsonProperty("AgeProof")
    private Object AgeProof;
    @JsonProperty("IPAddress")
    private String IPAddress;
    @JsonProperty("Illustration")
    private Object Illustration;
    @JsonProperty("PremiumSummary")
    protected PremiumSummary PremiumSummary;
    @JsonProperty("ErrorCode")
    private String ErrorCode;
    @JsonProperty("ErrorMessage")
    private String ErrorMessage;
}
