
package com.softcell.gonogo.model.insurance.religare;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "policy",
        "listErrorListList",
       "errorLists"
})
public class IntPolicyDataIO {

    @JsonProperty("policy")
    private Policy policy;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("listErrorListList")
    private List<ListErrorList> listErrorListList = new ArrayList<>();
    @JsonProperty("errorLists")
    private List<ErrorList> errorLists = new ArrayList<>();

    @JsonProperty("policy")
    public Policy getPolicy() {
        return policy;
    }

    @JsonProperty("policy")
    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public List<ListErrorList> getListErrorListList() {
        return listErrorListList;
    }

    public void setListErrorListList(List<ListErrorList> listErrorListList) {
        this.listErrorListList = listErrorListList;
    }

    public List<ErrorList> getErrorLists() {
        return errorLists;
    }

    public void setErrorLists(List<ErrorList> errorLists) {
        this.errorLists = errorLists;
    }

}