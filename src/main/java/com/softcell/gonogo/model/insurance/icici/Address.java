package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by ssg0268 on 8/11/19.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "pincode",
        "state",
        "line1",
        "city",
        "country"
})
public class Address {

    @JsonProperty("pincode")
    public String pincode;
    @JsonProperty("state")
    public String state;
    @JsonProperty("line1")
    public String line1;
    @JsonProperty("city")
    public String city;
    @JsonProperty("country")
    public String country;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
