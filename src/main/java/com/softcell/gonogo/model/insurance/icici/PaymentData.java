package com.softcell.gonogo.model.insurance.icici;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by archana on 30/1/19.
 */
public class PaymentData {

    @JsonProperty("payAmount")
    public String payAmount;
    @JsonProperty("payFinalPremiumAmnt")
    public String payFinalPremiumAmnt;
}
