package com.softcell.gonogo.model.insurance.icici;

/**
 * Created by ssg0268 on 8/11/19.
 */
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "otpReqCreatedTime",
        "otpReqUpdatedTime",
        "otpReqOtpNo",
        "otpReqStatus",
        "otpReqExpiryTimeInMin"
})
public class OtpDetails {

    //  "28-02-2017 05:59:05"
    @JsonProperty("otpReqCreatedTime")
    public String otpReqCreatedTime ;

    // "28-02-2017 06:00:38"
    @JsonProperty("otpReqUpdatedTime")
    public String  otpReqUpdatedTime;

    // Four digit number "4515"
    @JsonProperty("otpReqOtpNo")
    public String  otpReqOtpNo;
    // : "Verified",
    @JsonProperty("otpReqStatus")
    public String  otpReqStatus;

    @JsonProperty("otpReqExpiryTimeInMin")
    public String  otpReqExpiryTimeInMin;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
