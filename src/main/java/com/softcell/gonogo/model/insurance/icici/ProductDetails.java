package com.softcell.gonogo.model.insurance.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 28/2/19.
 */

public class ProductDetails {
    @JsonProperty("Product")
    public Product Product;

}
