package com.softcell.gonogo.model.insurance.religare;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "addressLine1Lang1",
        "addressLine2Lang1",
        "addressTypeCd",
        "areaCd",
        "cityCd",
        "pinCode",
        "stateCd"
})
public class PartyAddressDOList {

    @JsonProperty("addressLine1Lang1")
    private String addressLine1Lang1;
    @JsonProperty("addressLine2Lang1")
    private String addressLine2Lang1;
    @JsonProperty("addressTypeCd")
    private String addressTypeCd;
    @JsonProperty("areaCd")
    private String areaCd;
    @JsonProperty("cityCd")
    private String cityCd;
    @JsonProperty("pinCode")
    private String pinCode;
    @JsonProperty("stateCd")
    private String stateCd;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("addressLine1Lang1")
    public String getAddressLine1Lang1() {
        return addressLine1Lang1;
    }

    @JsonProperty("addressLine1Lang1")
    public void setAddressLine1Lang1(String addressLine1Lang1) {
        this.addressLine1Lang1 = addressLine1Lang1;
    }

    @JsonProperty("addressLine2Lang1")
    public String getAddressLine2Lang1() {
        return addressLine2Lang1;
    }

    @JsonProperty("addressLine2Lang1")
    public void setAddressLine2Lang1(String addressLine2Lang1) {
        this.addressLine2Lang1 = addressLine2Lang1;
    }

    @JsonProperty("addressTypeCd")
    public String getAddressTypeCd() {
        return addressTypeCd;
    }

    @JsonProperty("addressTypeCd")
    public void setAddressTypeCd(String addressTypeCd) {
        this.addressTypeCd = addressTypeCd;
    }

    @JsonProperty("areaCd")
    public String getAreaCd() {
        return areaCd;
    }

    @JsonProperty("areaCd")
    public void setAreaCd(String areaCd) {
        this.areaCd = areaCd;
    }

    @JsonProperty("cityCd")
    public String getCityCd() {
        return cityCd;
    }

    @JsonProperty("cityCd")
    public void setCityCd(String cityCd) {
        this.cityCd = cityCd;
    }

    @JsonProperty("pinCode")
    public String getPinCode() {
        return pinCode;
    }

    @JsonProperty("pinCode")
    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    @JsonProperty("stateCd")
    public String getStateCd() {
        return stateCd;
    }

    @JsonProperty("stateCd")
    public void setStateCd(String stateCd) {
        this.stateCd = stateCd;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
