package com.softcell.gonogo.model.insurance.religare;
 import java.util.HashMap;
       import java.util.Map;
        import com.fasterxml.jackson.annotation.JsonAnyGetter;
        import com.fasterxml.jackson.annotation.JsonAnySetter;
        import com.fasterxml.jackson.annotation.JsonIgnore;
        import com.fasterxml.jackson.annotation.JsonInclude;
        import com.fasterxml.jackson.annotation.JsonProperty;
        import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "contactNum",
        "contactTypeCd",
        "stdCode"
})
public class PartyContactDOList {

    @JsonProperty("contactNum")
    private long contactNum;
    @JsonProperty("contactTypeCd")
    private String contactTypeCd;
    @JsonProperty("stdCode")
    private String stdCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("contactNum")
    public long getContactNum() {
        return contactNum;
    }

    @JsonProperty("contactNum")
    public void setContactNum(long contactNum) {
        this.contactNum = contactNum;
    }

    @JsonProperty("contactTypeCd")
    public String getContactTypeCd() {
        return contactTypeCd;
    }

    @JsonProperty("contactTypeCd")
    public void setContactTypeCd(String contactTypeCd) {
        this.contactTypeCd = contactTypeCd;
    }

    @JsonProperty("stdCode")
    public String getStdCode() {
        return stdCode;
    }

    @JsonProperty("stdCode")
    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}