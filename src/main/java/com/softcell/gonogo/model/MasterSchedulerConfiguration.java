package com.softcell.gonogo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
/**
 * Created by ssg237 on 28/5/21.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "masterSchedularConfiguration")
public class MasterSchedulerConfiguration {
    /**
     * according to master name we can set master expiry time
     * default value is "DEFAULT"
     */

    @NotEmpty(groups = {MasterSchedulerConfiguration.InsertGrp.class})
    private String masterName;

    /**
     * We can set configuration basis on institutionId
     * default value is "9999"
     */

    @NotEmpty(groups = {MasterSchedulerConfiguration.InsertGrp.class})
    private String institutionId;

    /**
     * fromTime and toTime this is used for
     * setting restriction time for master uploading
     */
    @NotEmpty(groups = {MasterSchedulerConfiguration.InsertGrp.class})
    private String fromTime;

    @NotEmpty(groups = {MasterSchedulerConfiguration.InsertGrp.class})
    private String toTime;

    /**
     * configuration is enable or disable
     * basis on this flag
     */
    private boolean active;

    /**
     * insertedDate: add document insertion time
     */
    private Date insertedDate;
    /**
     * updatedDate: updated document time
     */
    private Date updatedDate;
    /**
     * This field is used for to set expiry time
     * for a specific document
     */
    private Date expiryTime;

    public interface InsertGrp {
    }

}
