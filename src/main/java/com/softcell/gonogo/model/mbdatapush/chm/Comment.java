package com.softcell.gonogo.model.mbdatapush.chm;

public class Comment {
	private String commentText;
	private String commentDate ;
	private String bureauComment;
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getCommentDate() {
		return commentDate;
	}
	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}
	public String getBureauComment() {
		return bureauComment;
	}
	public void setBureauComment(String bureauComment) {
		this.bureauComment = bureauComment;
	}
	@Override
	public String toString() {
		return "Comments [commentText=" + commentText + ", commentDate="
				+ commentDate + ", bureauComment=" + bureauComment + "]";
	}

}
