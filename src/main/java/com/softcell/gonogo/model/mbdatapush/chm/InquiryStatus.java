package com.softcell.gonogo.model.mbdatapush.chm;


public class InquiryStatus {

	/**
	 * @param args
	 */
	private Inquiry inquiry;

	public Inquiry getInquiry() {
		return inquiry;
	}

	public void setInquiry(Inquiry inquiry) {
		this.inquiry = inquiry;
	}

	@Override
	public String toString() {
		return "InquiryStatus [inquiry=" + inquiry + "]";
	}

		
}
