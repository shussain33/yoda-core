package com.softcell.gonogo.model.mbdatapush.chm;

public class PrintableReport {
	
	private String type;
	private String fileName;
	private String content;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "PrintableReport [type=" + type + ", fileName=" + fileName
				+ ", content=" + content + "]";
	}
}
