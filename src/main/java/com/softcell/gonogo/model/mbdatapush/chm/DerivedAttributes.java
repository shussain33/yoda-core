package com.softcell.gonogo.model.mbdatapush.chm;

public class DerivedAttributes {
	
	private String inquiriesInLastSixMonth;
	private String lengthOfCreditHistoryYear;
	private String lengthOfCreditHistoryMonth;
	private String averageAccountAgeYear;
	private String averageAccountAgeMonth;
	private String newAccountInLastSixMonths;
	private String newDlinqAccountInLastSixMonths;
	
	
	public String getInquiriesInLastSixMonth() {
		return inquiriesInLastSixMonth;
	}
	public void setInquiriesInLastSixMonth(String inquiriesInLastSixMonth) {
		this.inquiriesInLastSixMonth = inquiriesInLastSixMonth;
	}
	public String getLengthOfCreditHistoryYear() {
		return lengthOfCreditHistoryYear;
	}
	public void setLengthOfCreditHistoryYear(String lengthOfCreditHistoryYear) {
		this.lengthOfCreditHistoryYear = lengthOfCreditHistoryYear;
	}
	public String getLengthOfCreditHistoryMonth() {
		return lengthOfCreditHistoryMonth;
	}
	public void setLengthOfCreditHistoryMonth(String lengthOfCreditHistoryMonth) {
		this.lengthOfCreditHistoryMonth = lengthOfCreditHistoryMonth;
	}
	public String getAverageAccountAgeYear() {
		return averageAccountAgeYear;
	}
	public void setAverageAccountAgeYear(String averageAccountAgeYear) {
		this.averageAccountAgeYear = averageAccountAgeYear;
	}
	public String getAverageAccountAgeMonth() {
		return averageAccountAgeMonth;
	}
	public void setAverageAccountAgeMonth(String averageAccountAgeMonth) {
		this.averageAccountAgeMonth = averageAccountAgeMonth;
	}
	public String getNewAccountInLastSixMonths() {
		return newAccountInLastSixMonths;
	}
	public void setNewAccountInLastSixMonths(String newAccountInLastSixMonths) {
		this.newAccountInLastSixMonths = newAccountInLastSixMonths;
	}
	public String getNewDlinqAccountInLastSixMonths() {
		return newDlinqAccountInLastSixMonths;
	}
	public void setNewDlinqAccountInLastSixMonths(
			String newDlinqAccountInLastSixMonths) {
		this.newDlinqAccountInLastSixMonths = newDlinqAccountInLastSixMonths;
	}
	@Override
	public String toString() {
		return "DrivedAttributes [inquiriesInLastSixMonth="
				+ inquiriesInLastSixMonth + ", lengthOfCreditHistoryYear="
				+ lengthOfCreditHistoryYear + ", lengthOfCreditHistoryMonth="
				+ lengthOfCreditHistoryMonth + ", averageAccountAgeYear="
				+ averageAccountAgeYear + ", averageAccountAgeMonth="
				+ averageAccountAgeMonth + ", newAccountInLastSixMonths="
				+ newAccountInLastSixMonths
				+ ", newDlinqAccountInLastSixMonths="
				+ newDlinqAccountInLastSixMonths + "]";
	}
	
	
	

}
