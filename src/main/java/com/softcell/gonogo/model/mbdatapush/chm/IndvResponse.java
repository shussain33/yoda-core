package com.softcell.gonogo.model.mbdatapush.chm;

public class IndvResponse {
	
	private Summary primarySummary;
	private Summary secondarySummary;
	private Summary summary;
	private Responses responses;

	
	public Summary getPrimarySummary() {
		return primarySummary;
	}
	public void setPrimarySummary(Summary primarySummary) {
		this.primarySummary = primarySummary;
	}
	public Summary getSecondarySummary() {
		return secondarySummary;
	}
	public void setSecondarySummary(Summary secondarySummary) {
		this.secondarySummary = secondarySummary;
	}
	public Summary getSummary() {
		return summary;
	}
	public void setSummary(Summary summary) {
		this.summary = summary;
	}
	public Responses getResponses() {
		return responses;
	}
	public void setResponses(Responses responses) {
		this.responses = responses;
	}
	@Override
	public String toString() {
		return "IndvResponse [primarySummary=" + primarySummary
				+ ", secondarySummary=" + secondarySummary + ", summary="
				+ summary + ", responses=" + responses + "]";
	}
}
