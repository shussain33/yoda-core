package com.softcell.gonogo.model.mbdatapush.chm;

public class OverlapReports {
	private OverlapReport overlapReport;

	public OverlapReport getOverlapReport() {
		return overlapReport;
	}

	public void setOverlapReport(OverlapReport overlapReport) {
		this.overlapReport = overlapReport;
	}

	@Override
	public String toString() {
		return "OverlapReports [overlapReport=" + overlapReport + "]";
	}
}
