package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.List;



public class Errors {

	/**
	 * @param args
	 */
	private List<ErrorDomain> errorList;

	public List<ErrorDomain> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<ErrorDomain> errorList) {
		this.errorList = errorList;
	}

	@Override
	public String toString() {
		return "Errors [errorList=" + errorList + "]";
	}

	
	
}
