package com.softcell.gonogo.model.mbdatapush.chm;

public class Header {
	private String dateOfRequest;
	private String preparedFor;
	private String preparedForId;
	private String dateOfIssue;
	private String reportId;
	private String batchId;
	private String status;
	
	

	public String getDateOfRequest() {
		return dateOfRequest;
	}
	public void setDateOfRequest(String dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}
	public String getPreparedFor() {
		return preparedFor;
	}
	public void setPreparedFor(String preparedFor) {
		this.preparedFor = preparedFor;
	}
	public String getDateOfIssue() {
		return dateOfIssue;
	}
	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getPreparedForId() {
		return preparedForId;
	}
	public void setPreparedForId(String preparedForId) {
		this.preparedForId = preparedForId;
	}
	@Override
	public String toString() {
		return "Header [dateOfRequest=" + dateOfRequest + ", preparedFor="
				+ preparedFor + ", preparedForId=" + preparedForId
				+ ", dateOfIssue=" + dateOfIssue + ", reportId=" + reportId
				+ ", batchId=" + batchId + ", status=" + status + "]";
	}
	
}
