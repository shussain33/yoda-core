/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush.chm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author Dipak
 * 
 */
public class HibHighmarkAOREropDomain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	@Expose
	@SerializedName("SRNO")
	private  Integer srno;
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private  String memberReferenceNumber;
	@Expose
	@SerializedName("SOA_SOURCE_NAME")
	private  String soaSourceName;
	@Expose
	@SerializedName("DATE_OF_REQUEST")
	private  Date dateOfRequest;
	@Expose
	@SerializedName("PREPARED_FOR")
	private  String preparedFor;
	@Expose
	@SerializedName("PREPARED_FOR_ID")
	private  String preparedForId;
	@Expose
	@SerializedName("DATE_OF_ISSUE")
	private  Date dateOfIssue;
	@Expose
	@SerializedName("REPORT_ID")
	private  String reportId;
	@Expose
	@SerializedName("NAME")
	private  String name;
	@Expose
	@SerializedName("SPOUSE")
	private  String spouse;
	@Expose
	@SerializedName("FATHER")
	private  String father;
	@Expose
	@SerializedName("MOTHER")
	private  String mother;
	@Expose
	@SerializedName("DOB")
	private  Date dob;
	@Expose
	@SerializedName("AGE")
	private  Integer age;
	@Expose
	@SerializedName("AGE_AS_ON")
	private  Date ageAsOn;
	@Expose
	@SerializedName("GENDER")
	private  String gender;
	@Expose
	@SerializedName("PHONE_1")
	private  BigInteger phone1;
	@Expose
	@SerializedName("PHONE_2")
	private  BigInteger phone2;
	@Expose
	@SerializedName("PHONE_3")
	private  BigInteger phone3;
	@Expose
	@SerializedName("ADDRESS_1")
	private  String address1;
	@Expose
	@SerializedName("ADDRESS_2")
	private  String address2;
	@Expose
	@SerializedName("REL_TYP_1")
	private  String relTyp1;
	@Expose
	@SerializedName("REL_NM_1")
	private  String relNm1;
	@Expose
	@SerializedName("REL_TYP_2")
	private  String relTyp2;
	@Expose
	@SerializedName("REL_NM_2")
	private  String relNm2;
	@Expose
	@SerializedName("REL_TYP_3")
	private  String relTyp3;
	@Expose
	@SerializedName("REL_NM_3")
	private  String relNm3;
	@Expose
	@SerializedName("REL_TYP_4")
	private  String relTyp4;
	@Expose
	@SerializedName("REL_NM_4")
	private  String relNm4;
	@Expose
	@SerializedName("ID_TYPE_1")
	private  String idType1;
	@Expose
	@SerializedName("ID_VALUE_1")
	private  String idValue1;
	@Expose
	@SerializedName("ID_TYPE_2")
	private  String idType2;
	@Expose
	@SerializedName("ID_VALUE_2")
	private  String idValue2;
	@Expose
	@SerializedName("RATION_CARD")
	private  String rationCard;
	@Expose
	@SerializedName("VOTERS_ID")
	private  String votersId;
	@Expose
	@SerializedName("DRIVING_LICENCE_NO")
	private  String drivingLicenceNo;
	@Expose
	@SerializedName("PAN")
	private  String pan;
	@Expose
	@SerializedName("PASSPORT")
	private  String passport;
	@Expose
	@SerializedName("OTHER_ID")
	private  String otherId;
	@Expose
	@SerializedName("BRANCH")
	private  String branch;
	@Expose
	@SerializedName("KENDRA")
	private  String kendra;
	@Expose
	@SerializedName("MBR_ID")
	private  String mbrId;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP")
	private  String credtPurpsTyp;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP_DESC")
	private  String credtInqPurpsTypDesc;
	@Expose
	@SerializedName("CREDIT_INQUIRY_STAGE")
	private  String creditInquiryStage;
	@Expose
	@SerializedName("CREDT_RPT_ID")
	private  String credtRptId;
	@Expose
	@SerializedName("CREDT_REQ_TYP")
	private  String credtReqTyp;
	@Expose
	@SerializedName("CREDT_RPT_TRN_DT_TM")
	private  String credtRptTrnDtTm;
	@Expose
	@SerializedName("AC_OPEN_DT")
	private  Date acOpenDt;
	@Expose
	@SerializedName("LOAN_AMOUNT")
	private  BigInteger loanAmount;
	@Expose
	@SerializedName("ENTITY_ID")
	private  String entityId;
	@Expose
	@SerializedName("STATUS")
	private  String status;
	@Expose
	@SerializedName("ERRORS")
	private  String errors;
	@Expose
	@SerializedName("RESPONSE_TYPE")
	private  String responseType;
	@Expose
	@SerializedName("ACK_CODE")
	private  String ackCode;
	@Expose
	@SerializedName("ACK_DESCRIPTION")
	private  String ackDesc;
	@Expose
	@SerializedName("DATA_EXPORTED_FLAG")
	private  String dataExportedFlag;
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private  String outputWriteFlag;
	@Expose
	@SerializedName("OUTPUT_WRITE_TIME")
	private  String outputWriteTime;
	@Expose
	@SerializedName("OUTPUT_READ_TIME")
	private  String outputReadTime;

	public HibHighmarkAOREropDomain(Integer srNumber,
			String soaSourceName, String memberReferenceNumber,
			String soaFileName) {
		this.srno = srNumber;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = memberReferenceNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSrno() {
		return srno;
	}

	public void setSrno(Integer srno) {
		this.srno = srno;
	}

	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}

	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}

	public String getSoaSourceName() {
		return soaSourceName;
	}

	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}

	public Date getDateOfRequest() {
		return dateOfRequest;
	}

	public void setDateOfRequest(Date dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}

	public String getPreparedFor() {
		return preparedFor;
	}

	public void setPreparedFor(String preparedFor) {
		this.preparedFor = preparedFor;
	}

	public String getPreparedForId() {
		return preparedForId;
	}

	public void setPreparedForId(String preparedForId) {
		this.preparedForId = preparedForId;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpouse() {
		return spouse;
	}

	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	public String getFather() {
		return father;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public String getMother() {
		return mother;
	}

	public void setMother(String mother) {
		this.mother = mother;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getAgeAsOn() {
		return ageAsOn;
	}

	public void setAgeAsOn(Date ageAsOn) {
		this.ageAsOn = ageAsOn;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public BigInteger getPhone1() {
		return phone1;
	}

	public void setPhone1(BigInteger phone1) {
		this.phone1 = phone1;
	}

	public BigInteger getPhone2() {
		return phone2;
	}

	public void setPhone2(BigInteger phone2) {
		this.phone2 = phone2;
	}

	public BigInteger getPhone3() {
		return phone3;
	}

	public void setPhone3(BigInteger phone3) {
		this.phone3 = phone3;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getRelTyp1() {
		return relTyp1;
	}

	public void setRelTyp1(String relTyp1) {
		this.relTyp1 = relTyp1;
	}

	public String getRelNm1() {
		return relNm1;
	}

	public void setRelNm1(String relNm1) {
		this.relNm1 = relNm1;
	}

	public String getRelTyp2() {
		return relTyp2;
	}

	public void setRelTyp2(String relTyp2) {
		this.relTyp2 = relTyp2;
	}

	public String getRelNm2() {
		return relNm2;
	}

	public void setRelNm2(String relNm2) {
		this.relNm2 = relNm2;
	}

	public String getRelTyp3() {
		return relTyp3;
	}

	public void setRelTyp3(String relTyp3) {
		this.relTyp3 = relTyp3;
	}

	public String getRelNm3() {
		return relNm3;
	}

	public void setRelNm3(String relNm3) {
		this.relNm3 = relNm3;
	}

	public String getRelTyp4() {
		return relTyp4;
	}

	public void setRelTyp4(String relTyp4) {
		this.relTyp4 = relTyp4;
	}

	public String getRelNm4() {
		return relNm4;
	}

	public void setRelNm4(String relNm4) {
		this.relNm4 = relNm4;
	}

	public String getIdType1() {
		return idType1;
	}

	public void setIdType1(String idType1) {
		this.idType1 = idType1;
	}

	public String getIdValue1() {
		return idValue1;
	}

	public void setIdValue1(String idValue1) {
		this.idValue1 = idValue1;
	}

	public String getIdType2() {
		return idType2;
	}

	public void setIdType2(String idType2) {
		this.idType2 = idType2;
	}

	public String getIdValue2() {
		return idValue2;
	}

	public void setIdValue2(String idValue2) {
		this.idValue2 = idValue2;
	}

	public String getRationCard() {
		return rationCard;
	}

	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}

	public String getVotersId() {
		return votersId;
	}

	public void setVotersId(String votersId) {
		this.votersId = votersId;
	}

	public String getDrivingLicenceNo() {
		return drivingLicenceNo;
	}

	public void setDrivingLicenceNo(String drivingLicenceNo) {
		this.drivingLicenceNo = drivingLicenceNo;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getOtherId() {
		return otherId;
	}

	public void setOtherId(String otherId) {
		this.otherId = otherId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getKendra() {
		return kendra;
	}

	public void setKendra(String kendra) {
		this.kendra = kendra;
	}

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public String getCredtPurpsTyp() {
		return credtPurpsTyp;
	}

	public void setCredtPurpsTyp(String credtPurpsTyp) {
		this.credtPurpsTyp = credtPurpsTyp;
	}

	public String getCredtInqPurpsTypDesc() {
		return credtInqPurpsTypDesc;
	}

	public void setCredtInqPurpsTypDesc(String credtInqPurpsTypDesc) {
		this.credtInqPurpsTypDesc = credtInqPurpsTypDesc;
	}

	public String getCreditInquiryStage() {
		return creditInquiryStage;
	}

	public void setCreditInquiryStage(String creditInquiryStage) {
		this.creditInquiryStage = creditInquiryStage;
	}

	public String getCredtRptId() {
		return credtRptId;
	}

	public void setCredtRptId(String credtRptId) {
		this.credtRptId = credtRptId;
	}

	public String getCredtReqTyp() {
		return credtReqTyp;
	}

	public void setCredtReqTyp(String credtReqTyp) {
		this.credtReqTyp = credtReqTyp;
	}

	public String getCredtRptTrnDtTm() {
		return credtRptTrnDtTm;
	}

	public void setCredtRptTrnDtTm(String credtRptTrnDtTm) {
		this.credtRptTrnDtTm = credtRptTrnDtTm;
	}

	public Date getAcOpenDt() {
		return acOpenDt;
	}

	public void setAcOpenDt(Date acOpenDt) {
		this.acOpenDt = acOpenDt;
	}

	public BigInteger getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(BigInteger loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrors() {
		return errors;
	}

	public void setErrors(String errors) {
		this.errors = errors;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getAckCode() {
		return ackCode;
	}

	public void setAckCode(String ackCode) {
		this.ackCode = ackCode;
	}

	public String getAckDesc() {
		return ackDesc;
	}

	public void setAckDesc(String ackDesc) {
		this.ackDesc = ackDesc;
	}

	public String getDataExportedFlag() {
		return dataExportedFlag;
	}

	public void setDataExportedFlag(String dataExportedFlag) {
		this.dataExportedFlag = dataExportedFlag;
	}

	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}

	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}

	public String getOutputWriteTime() {
		return outputWriteTime;
	}

	public void setOutputWriteTime(String outputWriteTime) {
		this.outputWriteTime = outputWriteTime;
	}

	public String getOutputReadTime() {
		return outputReadTime;
	}

	public void setOutputReadTime(String outputReadTime) {
		this.outputReadTime = outputReadTime;
	}

	@Override
	public String toString() {
		return "HibHighmarkAOREropDomain [srno=" + srno
				+ ", memberReferenceNumber=" + memberReferenceNumber
				+ ", soaSourceName=" + soaSourceName + ", dateOfRequest="
				+ dateOfRequest + ", preparedFor=" + preparedFor
				+ ", preparedForId=" + preparedForId + ", dateOfIssue="
				+ dateOfIssue + ", reportId=" + reportId + ", name=" + name
				+ ", spouse=" + spouse + ", father=" + father
				+ ", mother=" + mother + ", dob=" + dob + ", age="
				+ age + ", ageAsOn=" + ageAsOn + ", gender=" + gender
				+ ", phone1=" + phone1 + ", phone2=" + phone2
				+ ", phone3=" + phone3 + ", address1=" + address1
				+ ", address2=" + address2 + ", relTyp1=" + relTyp1
				+ ", relNm1=" + relNm1 + ", relTyp2=" + relTyp2
				+ ", relNm2=" + relNm2 + ", relTyp3=" + relTyp3
				+ ", relNm3=" + relNm3 + ", relTyp4=" + relTyp4
				+ ", relNm4=" + relNm4 + ", idType1=" + idType1
				+ ", idValue1=" + idValue1 + ", idType2=" + idType2
				+ ", idValue2=" + idValue2 + ", rationCard="
				+ rationCard + ", votersId=" + votersId
				+ ", drivingLicenceNo=" + drivingLicenceNo + ", pan="
				+ pan + ", passport=" + passport + ", otherId="
				+ otherId + ", branch=" + branch + ", kendra="
				+ kendra + ", mbrId=" + mbrId + ", credtPurpsTyp="
				+ credtPurpsTyp + ", credtInqPurpsTypDesc="
				+ credtInqPurpsTypDesc + ", creditInquiryStage="
				+ creditInquiryStage + ", credtRptId=" + credtRptId
				+ ", credtReqTyp=" + credtReqTyp + ", credtRptTrnDtTm="
				+ credtRptTrnDtTm + ", acOpenDt=" + acOpenDt + ", loanAmount="
				+ loanAmount + ", entityId=" + entityId + ", status=" + status
				+ ", errors=" + errors + ", responseType=" + responseType
				+ ", ackCode=" + ackCode + ", ackDesc=" + ackDesc
				+ ", dataExportedFlag=" + dataExportedFlag
				+ ", outputWriteFlag=" + outputWriteFlag + ", outputWriteTime="
				+ outputWriteTime + ", outputReadTime=" + outputReadTime + "]";
	}
	
	
}
