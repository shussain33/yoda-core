/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class EquifaxSropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("EQUIFAX_SROP_DOMAIN_LIST")
	List<HibEquifaxSropDomain> equifaxSropDomainList;
	
	public List<HibEquifaxSropDomain> getEquifaxSropDomainList() {
		return equifaxSropDomainList;
	}
	public void setEquifaxSropDomainList(
			List<HibEquifaxSropDomain> equifaxSropDomainList) {
		this.equifaxSropDomainList = equifaxSropDomainList;
	}
	
	@Override
	public String toString() {
		return "EquifaxSropDomainObject [equifaxSropDomainList="
				+ equifaxSropDomainList + "]";
	}
}
