/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.softcell.gonogo.model.mbdatapush.chm.HibHighmarkAOREropDomain;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class CHMAdvEropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("CHM_AOR_EROP_DOMAIN_LIST")
	List<HibHighmarkAOREropDomain> hmAdvEropDomainList;
	
	public List<HibHighmarkAOREropDomain> getHmAdvEropDomainList() {
		return hmAdvEropDomainList;
	}
	public void setHmAdvEropDomainList(
			List<HibHighmarkAOREropDomain> hmAdvEropDomainList) {
		this.hmAdvEropDomainList = hmAdvEropDomainList;
	}
	
	@Override
	public String toString() {
		return "CHMAdvEropDomainObject [hmAdvEropDomainList="
				+ hmAdvEropDomainList + "]";
	}
	
	
	
}
