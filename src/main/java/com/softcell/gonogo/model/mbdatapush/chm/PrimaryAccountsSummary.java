package com.softcell.gonogo.model.mbdatapush.chm;

public class PrimaryAccountsSummary {
	
	private String primaryNumberOfAccounts;
	private String primaryActiveNumberOfAccounts;
	private String primaryOverdueNumberOfAccounts;
	private String primarySecuredNumberOfAccounts;
	private String primaryUnsecuredNumberOfAccounts;
	private String primaryUntaggedNumberOfAccounts;
	private String primaryCurrentBalance;
	private String primarySanctionedAmount;
	private String primaryDisbursedAmount;
	
	public String getPrimaryNumberOfAccounts() {
		return primaryNumberOfAccounts;
	}
	public void setPrimaryNumberOfAccounts(String primaryNumberOfAccounts) {
		this.primaryNumberOfAccounts = primaryNumberOfAccounts;
	}
	public String getPrimaryActiveNumberOfAccounts() {
		return primaryActiveNumberOfAccounts;
	}
	public void setPrimaryActiveNumberOfAccounts(
			String primaryActiveNumberOfAccounts) {
		this.primaryActiveNumberOfAccounts = primaryActiveNumberOfAccounts;
	}
	public String getPrimaryOverdueNumberOfAccounts() {
		return primaryOverdueNumberOfAccounts;
	}
	public void setPrimaryOverdueNumberOfAccounts(
			String primaryOverdueNumberOfAccounts) {
		this.primaryOverdueNumberOfAccounts = primaryOverdueNumberOfAccounts;
	}
	public String getPrimarySecuredNumberOfAccounts() {
		return primarySecuredNumberOfAccounts;
	}
	public void setPrimarySecuredNumberOfAccounts(
			String primarySecuredNumberOfAccounts) {
		this.primarySecuredNumberOfAccounts = primarySecuredNumberOfAccounts;
	}
	public String getPrimaryUnsecuredNumberOfAccounts() {
		return primaryUnsecuredNumberOfAccounts;
	}
	public void setPrimaryUnsecuredNumberOfAccounts(
			String primaryUnsecuredNumberOfAccounts) {
		this.primaryUnsecuredNumberOfAccounts = primaryUnsecuredNumberOfAccounts;
	}
	public String getPrimaryUntaggedNumberOfAccounts() {
		return primaryUntaggedNumberOfAccounts;
	}
	public void setPrimaryUntaggedNumberOfAccounts(
			String primaryUntaggedNumberOfAccounts) {
		this.primaryUntaggedNumberOfAccounts = primaryUntaggedNumberOfAccounts;
	}
	public String getPrimaryCurrentBalance() {
		return primaryCurrentBalance;
	}
	public void setPrimaryCurrentBalance(String primaryCurrentBalance) {
		this.primaryCurrentBalance = primaryCurrentBalance;
	}
	public String getPrimarySanctionedAmount() {
		return primarySanctionedAmount;
	}
	public void setPrimarySanctionedAmount(String primarySanctionedAmount) {
		this.primarySanctionedAmount = primarySanctionedAmount;
	}
	public String getPrimaryDisbursedAmount() {
		return primaryDisbursedAmount;
	}
	public void setPrimaryDisbursedAmount(String primaryDisbursedAmount) {
		this.primaryDisbursedAmount = primaryDisbursedAmount;
	}
	@Override
	public String toString() {
		return "PrimaryAccountsSummary [primaryNumberOfAccounts="
				+ primaryNumberOfAccounts + ", primaryActiveNumberOfAccounts="
				+ primaryActiveNumberOfAccounts
				+ ", primaryOverdueNumberOfAccounts="
				+ primaryOverdueNumberOfAccounts
				+ ", primarySecuredNumberOfAccounts="
				+ primarySecuredNumberOfAccounts
				+ ", primaryUnsecuredNumberOfAccounts="
				+ primaryUnsecuredNumberOfAccounts
				+ ", primaryUntaggedNumberOfAccounts="
				+ primaryUntaggedNumberOfAccounts + ", primaryCurrentBalance="
				+ primaryCurrentBalance + ", primarySanctionedAmount="
				+ primarySanctionedAmount + ", primaryDisbursedAmount="
				+ primaryDisbursedAmount + "]";
	}
	
	
	

}
