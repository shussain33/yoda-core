/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class CHMAdvSropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("CHM_AOR_SROP_DOMAIN_LIST")
	List<HibHighmarkAdOverlapSropDomain> hmAdvSropDomainList;
	public List<HibHighmarkAdOverlapSropDomain> getHmAdvSropDomainList() {
		return hmAdvSropDomainList;
	}
	public void setHmAdvSropDomainList(
			List<HibHighmarkAdOverlapSropDomain> hmAdvSropDomainList) {
		this.hmAdvSropDomainList = hmAdvSropDomainList;
	}
	
	@Override
	public String toString() {
		return "CHMAdvSropDomainObject [hmAdvSropDomainList="
				+ hmAdvSropDomainList + "]";
	}
	
	
	
}
