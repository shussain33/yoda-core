/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class ExperianSropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("EXPERIAN_SROP_DOMAIN_LIST")
	List<HibExperianSropDomain> experianSropDomainList;
	public List<HibExperianSropDomain> getExperianSropDomainList() {
		return experianSropDomainList;
	}
	public void setExperianSropDomainList(
			List<HibExperianSropDomain> experianSropDomainList) {
		this.experianSropDomainList = experianSropDomainList;
	}
	@Override
	public String toString() {
		return "ExperianSropDomainObject [experianSropDomainList="
				+ experianSropDomainList + "]";
	}
}
