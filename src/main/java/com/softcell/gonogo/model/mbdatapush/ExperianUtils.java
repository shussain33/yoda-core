package com.softcell.gonogo.model.mbdatapush;

import org.apache.commons.lang.StringUtils;



public class ExperianUtils implements EXPERIAN_METADATA{
	
	public String getGender(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.genderMap.containsKey(key)){
				return EXPERIAN_METADATA.genderMap.get(key);
			}
		}
		return key; 
	}
	
	
	
	public String getMaritalStatus(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.maritalStatusMap.containsKey(key)){
				return EXPERIAN_METADATA.maritalStatusMap.get(key);
			}
		}
		return key; 
	}
	
	public String getAccountType(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.accountTypeMap.containsKey(key)){
				return EXPERIAN_METADATA.accountTypeMap.get(key);
			}
		}
		return key; 
	}
	
	public String getAccountStatus(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.accountStatusMap.containsKey(key)){
				return EXPERIAN_METADATA.accountStatusMap.get(key);
			}
		}
		return key; 
	}
	
	public String getAccountHolderType(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.accountHolderTypeNameMap.containsKey(key)){
				return EXPERIAN_METADATA.accountHolderTypeNameMap.get(key);
			}
		}
		return key; 
	}
	
	public String getState(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.stateMap.containsKey(key)){
				return EXPERIAN_METADATA.stateMap.get(key);
			}
		}
		return key; 
	}
	
	public String getFrequencyOfPayment(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.frequencyOfPaymentMap.containsKey(key)){
				return EXPERIAN_METADATA.frequencyOfPaymentMap.get(key);
			}
		}
		return key; 
	}
	
	public String getEnquiryReason(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.enquiryReasonMap.containsKey(key)){
				return EXPERIAN_METADATA.enquiryReasonMap.get(key);
			}
		}
		return key; 
	}
	
	public String getFinancialPurpose(String key){

		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.financialPurposeMap.containsKey(key)){
				return EXPERIAN_METADATA.financialPurposeMap.get(key);
			}
		}
		return key; 
	}
	
	public String getEmploymentStatus(String key){
		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.employmentStatusMap.containsKey(key)){
				return EXPERIAN_METADATA.employmentStatusMap.get(key);
			}
		}
		return key; 
	}
	
	public String getAssetClassificationStatus(String key){
		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.assetClassificationMap.containsKey(key)){
				return EXPERIAN_METADATA.assetClassificationMap.get(key);
			}
		}
		return key; 
	}
	
	public String getPhoneType(String key){
		if(StringUtils.isNotBlank(key)){
			if(EXPERIAN_METADATA.phoneTypeMap.containsKey(key)){
				return EXPERIAN_METADATA.phoneTypeMap.get(key);
			}
		}
		return key; 
	}
}
