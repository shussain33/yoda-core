package com.softcell.gonogo.model.mbdatapush.chm;

public class BaseRespReportFile {

	/**
	 * @param args
	 */
	InquiryStatus inquiryStatus;

	public InquiryStatus getInquiryStatus() {
		return inquiryStatus;
	}

	public void setInquiryStatus(InquiryStatus inquiryStatus) {
		this.inquiryStatus = inquiryStatus;
	}

	@Override
	public String toString() {
		return "ReportFile [inquiryStatus=" + inquiryStatus + "]";
	}
	

}
