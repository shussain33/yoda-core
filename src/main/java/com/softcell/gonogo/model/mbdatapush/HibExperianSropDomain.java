/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Dipak
 * 
 */

public class HibExperianSropDomain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private  Integer id;
	@Expose
	@SerializedName("SRNO")
	private  Integer srNo;
	@Expose
	@SerializedName("SOA_SOURCE_NAME")
	private  String soaSourceName;
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private  String memberReferenceNumber;
	@Expose
	@SerializedName("ENQUIRY_DATE")
	private  Date enquiryDate;
	@Expose
	@SerializedName("SYSTEMCODE_HEADER")
	private  String systemcodeHeader;
	@Expose
	@SerializedName("MESSAGETEXT_HEADER")
	private  String messageTextHeader;
	@Expose
	@SerializedName("REPORTDATE")
	private  Date reportdate;
	@Expose
	@SerializedName("REPORTIME_HEADER")
	private  String reportimeHeader;
	@Expose
	@SerializedName("USERMESSAGETEXT")
	private  String userMessageText;
	@Expose
	@SerializedName("ENQUIRYUSERNAME")
	private  String enquiryUsername;
	@Expose
	@SerializedName("CREDIT_PROFILE_REPORT_DATE")
	private  Date creditProfileReportDate;
	@Expose
	@SerializedName("CREDIT_PROFILE_REPORT_TIME")
	private  String CreditProfileReportTime;
	@Expose
	@SerializedName("VERSION")
	private  String version;
	@Expose
	@SerializedName("REPORTNUMBER")
	private  String reportNumber;
	@Expose
	@SerializedName("SUBSCRIBER")
	private  String subscriber;
	@Expose
	@SerializedName("SUBSCRIBERNAME")
	private  String subscriberName;
	@Expose
	@SerializedName("ENQUIRYREASON")
	private  String enquiryReason;
	@Expose
	@SerializedName("FINANCEPURPOSE")
	private  String financePurpose;
	@Expose
	@SerializedName("AMOUNTFINANCED")
	private  Integer amountFinanced;
	@Expose
	@SerializedName("DURATIONOFAGREEMENT")
	private  Integer durationOfAgreement;
	@Expose
	@SerializedName("LASTNAME")
	private  String lastname;
	@Expose
	@SerializedName("FIRSTNAME")
	private  String firstname;
	@Expose
	@SerializedName("MIDDLENAME1")
	private  String middleName1;
	@Expose
	@SerializedName("MIDDLENAME2")
	private  String middleName2;
	@Expose
	@SerializedName("MIDDLENAME3")
	private  String middleName3;
	@Expose
	@SerializedName("GENDERCODE")
	private  String genderCode;
	@Expose
	@SerializedName("GENDER")
	private  String gender;
	@Expose
	@SerializedName("INCOME_TAX_PAN_APP")
	private  String incomeTaxPanApp;
	@Expose
	@SerializedName("PAN_ISSUE_DATE_APP")
	private  Date panIssueDateApp;
	@Expose
	@SerializedName("PAN_EXP_DATE_APP")
	private  Date panExpDateApp;
	@Expose
	@SerializedName("PASSPORT_NUMBER_APP")
	private  String passportNumberApp;
	@Expose
	@SerializedName("PASSPORT_ISSUE_DATE_APP")
	private  Date passportIssueDateApp;
	@Expose
	@SerializedName("PASSPORT_EXP_DATE_APP")
	private  Date passportExpDateApp;
	@Expose
	@SerializedName("VOTER_IDENTITY_CARD_APP")
	private  String voterIdentityCardApp;
	@Expose
	@SerializedName("VOTER_ID_ISSUE_DATE_APP")
	private  Date voterIdIssueDateApp;
	@Expose
	@SerializedName("VOTER_ID_EXP_DATE_APP")
	private  Date voterIdExpDateApp;
	@Expose
	@SerializedName("DRIVER_LICENSE_NUMBER_APP")
	private  String driverLicenseNumberApp;
	@Expose
	@SerializedName("DRIVER_LICENSE_ISSUE_DATE_APP")
	private  Date driverLicenseIssueDateApp;
	@Expose
	@SerializedName("DRIVER_LICENSE_EXP_DATE_APP")
	private  Date driverLicenseExpDateApp;
	@Expose
	@SerializedName("RATION_CARD_NUMBER_APP")
	private  String rationCardNumberApp;
	@Expose
	@SerializedName("RATION_CARD_ISSUE_DATE_APP")
	private  Date rationCardIssueDateApp;
	@Expose
	@SerializedName("RATION_CARD_EXP_DATE_APP")
	private  Date rationCardExpDateApp;
	@Expose
	@SerializedName("UNIVERSAL_ID_NUMBER_APP")
	private  String universalIdNumberApp;
	@Expose
	@SerializedName("UNIVERSAL_ID_ISSUE_DATE_APP")
	private  Date universalIdIssueDateApp;
	@Expose
	@SerializedName("UNIVERSAL_ID_EXP_DATE_APP")
	private  Date universalIdExpDateApp;
	@Expose
	@SerializedName("DATEOFBIRTHAPPLICANT")
	private  Date dateOfBirthApplicant;
	@Expose
	@SerializedName("TELEPHONENUMBERAPPLICANT1ST")
	private  String telephoneNumberApplicant1st;
	@Expose
	@SerializedName("TELEEXTENSION_APP")
	private  String teleExtensionApp;
	@Expose
	@SerializedName("TELETYPE_APP")
	private  String teleTypeApp;
	@Expose
	@SerializedName("MOBILEPHONENUMBER")
	private  String mobilePhoneNumber;
	@Expose
	@SerializedName("EMAILID_APP")
	private  String emailIdApp;
	@Expose
	@SerializedName("INCOME")
	private  String income;
	@Expose
	@SerializedName("MARITALSTATUSCODE")
	private  String maritalStatusCode;
	@Expose
	@SerializedName("MARITALSTATUS")
	private  String maritalStatus;
	@Expose
	@SerializedName("EMPLOYMENTSTATUS")
	private  String employmentStatus;
	@Expose
	@SerializedName("DATEWITHEMPLOYER")
	private  Integer dateWithEmployer;
	@Expose
	@SerializedName("NUMBEROFMAJORCREDITCARDHELD")
	private  String numberOfMajorCreditCardHeld;
	@Expose
	@SerializedName("FLATNOPLOTNOHOUSENO")
	private  String flatNoPlotNoHouseNo;
	@Expose
	@SerializedName("BLDGNOSOCIETYNAME")
	private  String bldgNoSocietyName;
	@Expose
	@SerializedName("ROADNONAMEAREALOCALITY")
	private  String roadNoNameAreaLocality;
	@Expose
	@SerializedName("CITY")
	private  String city;
	@Expose
	@SerializedName("LANDMARK")
	private  String landmark;
	@Expose
	@SerializedName("STATE")
	private  String state;
	@Expose
	@SerializedName("PINCODE")
	private  String pincode;
	@Expose
	@SerializedName("COUNTRY_CODE")
	private  String countryCode;
	@Expose
	@SerializedName("ADD_FLATNOPLOTNOHOUSENO")
	private  String addFlatNoPlotNoHouseNo;
	@Expose
	@SerializedName("ADD_BLDGNOSOCIETYNAME")
	private  String addBldgNoSocietyName;
	@Expose
	@SerializedName("ADD_ROADNONAMEAREALOCALITY")
	private  String addRoadNoNameAreaLocality;
	@Expose
	@SerializedName("ADD_CITY")
	private  String addCity;
	@Expose
	@SerializedName("ADD_LANDMARK")
	private  String addLandmark;
	@Expose
	@SerializedName("ADD_STATE")
	private  String addState;
	@Expose
	@SerializedName("ADD_PINCODE")
	private  String addPincode;
	@Expose
	@SerializedName("ADD_COUNTRYCODE")
	private  String addCountrycode;
	@Expose
	@SerializedName("CREDITACCOUNTTOTAL")
	private  String creditAccountTotal;
	@Expose
	@SerializedName("CREDITACCOUNTACTIVE")
	private  Integer creditAccountActive;
	@Expose
	@SerializedName("CREDITACCOUNTDEFAULT")
	private  Integer creditAccountDefault;
	@Expose
	@SerializedName("CREDITACCOUNTCLOSED")
	private  Integer creditAccountClosed;
	@Expose
	@SerializedName("CADSUITFILEDCURRENTBALANCE")
	private  Integer cadsuitfiledCurrentBalance;
	@Expose
	@SerializedName("OUTSTANDINGBALANCESECURED")
	private  String outstandingBalanceSecured;
	@Expose
	@SerializedName("OUTSTANDINGBALSECUREDPER")
	private  String outstandingBalSecuredPer;
	@Expose
	@SerializedName("OUTSTANDINGBALANCEUNSECURED")
	private  String outstandingBalanceUnsecured;
	@Expose
	@SerializedName("OUTSTANDINGBALUNSECPER")
	private  String outstandingBalUnsecPer;
	@Expose
	@SerializedName("OUTSTANDINGBALANCEALL")
	private  String outstandingBalanceAll;
	@Expose
	@SerializedName("IDENTIFICATIONNUMBER")
	private  String identificationNumber;
	@Expose
	@SerializedName("CAISSUBSCRIBERNAME")
	private  String caisSubscriberName;
	@Expose
	@SerializedName("ACCOUNTNUMBER")
	private  String accountNumber;
	@Expose
	@SerializedName("PORTFOLIOTYPE")
	private  String portfolioType;
	@Expose
	@SerializedName("ACCOUNTTYPE_CODE")
	private  String accountTypeCode;
	@Expose
	@SerializedName("ACCOUNTTYPE")
	private  String accountType;
	@Expose
	@SerializedName("OPENDATE")
	private  Date openDate;
	@Expose
	@SerializedName("HIGHESTCREDITORORGNLOANAMT")
	private  String highestCreditOrOrgnLoanAmt;
	@Expose
	@SerializedName("TERMSDURATION")
	private  String termsDuration;
	@Expose
	@SerializedName("TERMSFREQUENCY")
	private  String termsFrequency;
	@Expose
	@SerializedName("SCHEDULEDMONTHLYPAYAMT")
	private  String scheduledMonthlyPayamt;
	@Expose
	@SerializedName("ACCOUNTSTATUS_CODE")
	private  String accountStatusCode;
	@Expose
	@SerializedName("ACCOUNTSTATUS")
	private  String accountStatus;
	@Expose
	@SerializedName("PAYMENTRATING")
	private  String paymentRating;
	@Expose
	@SerializedName("PAYMENTHISTORYPROFILE")
	private  String paymentHistoryProfile;
	@Expose
	@SerializedName("SPECIALCOMMENT")
	private  String specialComment;
	@Expose
	@SerializedName("CURRENTBALANCE_TL")
	private  Integer currentBalanceTl;
	@Expose
	@SerializedName("AMOUNTPASTDUE_TL")
	private  Integer amountPastdueTl;
	@Expose
	@SerializedName("ORIGINALCHARGEOFFAMOUNT")
	private  Integer originalChargeOffAmount;
	@Expose
	@SerializedName("DATEREPORTED")
	private  Date dateReported;
	@Expose
	@SerializedName("DATEOFFIRSTDELINQUENCY")
	private  Date dateOfFirstDelinquency;
	@Expose
	@SerializedName("DATECLOSED")
	private  Date dateClosed;
	@Expose
	@SerializedName("DATEOFLASTPAYMENT")
	private  Date dateOfLastPayment;
	@Expose
	@SerializedName("SUITFWILFULDFTWRITTENOFFSTS")
	private  String suitfilledWillFullDefaultWrittenOffStatus;
	@Expose
	@SerializedName("SUITFILEDWILFULDEF")
	private  String suitFiledWilfulDef;
	@Expose
	@SerializedName("WRITTENOFFSETTLEDSTATUS")
	private  String writtenOffSettledStatus;
	@Expose
	@SerializedName("VALUEOFCREDITSLASTMONTH")
	private  String valueOfCreditsLastMonth;
	@Expose
	@SerializedName("OCCUPATIONCODE")
	private  String occupationCode;
	@Expose
	@SerializedName("SATTLEMENTAMOUNT")
	private  Integer sattlementAmount;
	@Expose
	@SerializedName("VALUEOFCOLATERAL")
	private  Integer valueOfColateral;
	@Expose
	@SerializedName("TYPEOFCOLATERAL")
	private  String typeOfColateral;
	@Expose
	@SerializedName("WRITTENOFFAMTTOTAL")
	private  Integer writtenOffAmtTotal;
	@Expose
	@SerializedName("WRITTENOFFAMTPRINCIPAL")
	private  Integer writtenOffAmtPrincipal;
	@Expose
	@SerializedName("RATEOFINTEREST")
	private  Integer rateOfInterest;
	@Expose
	@SerializedName("REPAYMENTTENURE")
	private  Integer repaymentTenure;
	@Expose
	@SerializedName("PROMOTIONALRATEFLAG")
	private  String promotionalRateFlag;
	@Expose
	@SerializedName("CAISINCOME")
	private  Integer caisIncome;
	@Expose
	@SerializedName("INCOMEINDICATOR")
	private  String incomeIndicator;
	@Expose
	@SerializedName("INCOMEFREQUENCYINDICATOR")
	private  String incomeFrequencyIndicator;
	@Expose
	@SerializedName("DEFAULTSTATUSDATE")
	private  Date defaultStatusDate;
	@Expose
	@SerializedName("LITIGATIONSTATUSDATE")
	private  Date litigationStatusDate;
	@Expose
	@SerializedName("WRITEOFFSTATUSDATE")
	private  Date writeOffStatusDate;
	@Expose
	@SerializedName("CURRENCYCODE")
	private  String currencyCode;
	@Expose
	@SerializedName("SUBSCRIBERCOMMENTS")
	private  String subscriberComments;
	@Expose
	@SerializedName("CONSUMERCOMMENTS")
	private  String consumerComments;
	@Expose
	@SerializedName("ACCOUNTHOLDERTYPECODE")
	private  String accountHolderTypeCode;
	@Expose
	@SerializedName("ACCOUNTHOLDER_TYPENAME")
	private  String accountHolderTypeName;
	@Expose
	@SerializedName("YEAR_HIST")
	private  String yearHist;
	@Expose
	@SerializedName("MONTH_HIST")
	private  String monthHist;
	@Expose
	@SerializedName("DAYSPASTDUE")
	private  String daysPastDue;
	@Expose
	@SerializedName("ASSETCLASSIFICATION")
	private  String assetClassification;
	@Expose
	@SerializedName("YEAR_ADVHIST")
	private  String yearAdvHist;
	@Expose
	@SerializedName("MONTH_ADVHIST")
	private  String monthAdvHist;
	@Expose
	@SerializedName("CASHLIMIT")
	private  Integer cashLimit;
	@Expose
	@SerializedName("CREDITLIMITAMT")
	private  Integer creditLimitAmt;
	@Expose
	@SerializedName("ACTUALPAYMENTAMT")
	private  Integer actualPaymentAmt;
	@Expose
	@SerializedName("EMIAMT")
	private  Integer emiAmt;
	@Expose
	@SerializedName("CURRENTBALANCE_ADVHIST")
	private  Integer currentBalanceAdvHist;
	@Expose
	@SerializedName("AMOUNTPASTDUE_ADVHIST")
	private  Integer amountPastDueAdvHist;
	@Expose
	@SerializedName("SURNAMENONNORMALIZED")
	private  String surNameNonNormalized;
	@Expose
	@SerializedName("FIRSTNAMENONNORMALIZED")
	private  String firstNameNonNormalized;
	@Expose
	@SerializedName("MIDDLENAME1NONNORMALIZED")
	private  String middleName1NonNormalized;
	@Expose
	@SerializedName("MIDDLENAME2NONNORMALIZED")
	private  String middleName2NonNormalized;
	@Expose
	@SerializedName("MIDDLENAME3NONNORMALIZED")
	private  String middleName3NonNormalized;
	@Expose
	@SerializedName("ALIAS")
	private  String alias;
	@Expose
	@SerializedName("CAISGENDERCODE")
	private  String caisGenderCode;
	@Expose
	@SerializedName("CAISGENDER")
	private  String caisGender;
	@Expose
	@SerializedName("CAISINCOMETAXPAN")
	private  String caisIncomeTaxPan;
	@Expose
	@SerializedName("CAISPASSPORTNUMBER")
	private  String caisPassportNumber;
	@Expose
	@SerializedName("VOTERIDNUMBER")
	private  String voterIdNumber;
	@Expose
	@SerializedName("DATEOFBIRTH")
	private  Date dateOfBirth;
	@Expose
	@SerializedName("FIRSTLINEOFADDNONNORMALIZED")
	private  String firstLineOfAddNonNormalized;
	@Expose
	@SerializedName("SECONDLINEOFADDNONNORMALIZED")
	private  String secondLineOfAddNonNormalized;
	@Expose
	@SerializedName("THIRDLINEOFADDNONNORMALIZED")
	private  String thirdLineOfAddNonNormalized;
	@Expose
	@SerializedName("CITYNONNORMALIZED")
	private  String cityNonNormalized;
	@Expose
	@SerializedName("FIFTHLINEOFADDNONNORMALIZED")
	private  String fifthLineOfAddNonNormalized;
	@Expose
	@SerializedName("STATECODENONNORMALIZED")
	private  String stateCodeNonNormalized;
	@Expose
	@SerializedName("STATENONNORMALIZED")
	private  String stateNonNormalized;
	@Expose
	@SerializedName("ZIPPOSTALCODENONNORMALIZED")
	private  String zipPostalCodeNonNormalized;
	@Expose
	@SerializedName("COUNTRYCODENONNORMALIZED")
	private  String countryCodeNonNormalized;
	@Expose
	@SerializedName("ADDINDICATORNONNORMALIZED")
	private  String addIndicatorNonNormalized;
	@Expose
	@SerializedName("RESIDENCECODENONNORMALIZED")
	private  String residenceCodeNonNormalized;
	@Expose
	@SerializedName("RESIDENCENONNORMALIZED")
	private  String residenceNonNormalized;
	@Expose
	@SerializedName("TELEPHONENUMBER")
	private  String telephoneNumber;
	@Expose
	@SerializedName("TELETYPE_PHONE")
	private  String teleTypePhone;
	@Expose
	@SerializedName("TELEEXTENSION_PHONE")
	private  String teleExtensionPhone;
	@Expose
	@SerializedName("MOBILETELEPHONENUMBER")
	private  String mobileTelephoneNumber;
	@Expose
	@SerializedName("FAXNUMBER")
	private  String faxNumber;
	@Expose
	@SerializedName("EMAILID_PHONE")
	private  String emailIdPhone;
	@Expose
	@SerializedName("INCOME_TAX_PAN_ID")
	private  String incomeTaxPanId;
	@Expose
	@SerializedName("PAN_ISSUE_DATE_ID")
	private  Date panIssueDateId;
	@Expose
	@SerializedName("PAN_EXP_DATE_ID")
	private  Date panExpDateId;
	@Expose
	@SerializedName("PASSPORT_NUMBER_ID")
	private  String passportNumberId;
	@Expose
	@SerializedName("PASSPORT_ISSUE_DATE_ID")
	private  Date passportIssueDateId;
	@Expose
	@SerializedName("PASSPORT_EXP_DATE_ID")
	private  Date passportExpDateId;
	@Expose
	@SerializedName("VOTER_IDENTITY_CARD_ID")
	private  String voterIdentityCardId;
	@Expose
	@SerializedName("VOTER_ID_ISSUE_DATE_ID")
	private  Date voterIdIssueDateId;
	@Expose
	@SerializedName("VOTER_ID_EXP_DATE_ID")
	private  Date voterIdExpDateId;
	@Expose
	@SerializedName("DRIVER_LICENSE_NUMBER_ID")
	private  String driverLicenseNumberId;
	@Expose
	@SerializedName("DRIVER_LICENSE_ISSUE_DATE_ID")
	private  Date driverLicenseIssueDateId;
	@Expose
	@SerializedName("DRIVER_LICENSE_EXP_DATE_ID")
	private  Date driverLicenseExpDateId;
	@Expose
	@SerializedName("RATION_CARD_NUMBER_ID")
	private  String rationCardNumberId;
	@Expose
	@SerializedName("RATION_CARD_ISSUE_DATE_ID")
	private  Date rationCardIssueDateId;
	@Expose
	@SerializedName("RATION_CARD_EXP_DATE_ID")
	private  Date rationCardExpDateId;
	@Expose
	@SerializedName("UNIVERSAL_ID_NUMBER_ID")
	private  String universalIdNumberId;
	@Expose
	@SerializedName("UNIVERSAL_ID_ISSUE_DATE_ID")
	private  Date universalIdIssueDateId;
	@Expose
	@SerializedName("UNIVERSAL_ID_EXP_DATE_ID")
	private  Date universalIdExpDateId;
	@Expose
	@SerializedName("EMAILID_ID")
	private  String emailidId;
	@Expose
	@SerializedName("EXACTMATCH")
	private  String exactMatch;
	@Expose
	@SerializedName("CAPSLAST7DAYS")
	private  Integer capsLast7Days;
	@Expose
	@SerializedName("CAPSLAST30DAYS")
	private  Integer capsLast30Days;
	@Expose
	@SerializedName("CAPSLAST90DAYS")
	private  Integer capsLast90Days;
	@Expose
	@SerializedName("CAPSLAST180DAYS")
	private  Integer capsLast180Days;
	@Expose
	@SerializedName("CAPSSUBSCRIBERCODE")
	private  String capsSubscriberCode;
	@Expose
	@SerializedName("CAPSSUBSCRIBERNAME")
	private  String capsSubscriberName;
	@Expose
	@SerializedName("CAPSDATEOFREQUEST")
	private  Date capsdateofRequest;
	@Expose
	@SerializedName("CAPSREPORTDATE")
	private  Date capsReportDate;
	@Expose
	@SerializedName("CAPSREPORTTIME")
	private  String capsReportTime;
	@Expose
	@SerializedName("CAPSREPORTNUMBER")
	private  String capsReportNumber;
	@Expose
	@SerializedName("ENQUIRY_REASON_CODE")
	private  String enquiryReasonCode;
	@Expose
	@SerializedName("CAPSENQUIRYREASON")
	private  String capsEnquiryReason;
	@Expose
	@SerializedName("FINANCE_PURPOSE_CODE")
	private  String financePurposeCode;
	@Expose
	@SerializedName("CAPSFINANCEPURPOSE")
	private  String capsFinancePurpose;
	@Expose
	@SerializedName("CAPSAMOUNTFINANCED")
	private  String capsAmountFinanced;
	@Expose
	@SerializedName("CAPSDURATIONOFAGREEMENT")
	private  String capsDurationOfAgreement;
	@Expose
	@SerializedName("CAPSAPPLICANTLASTNAME")
	private  String capsApplicantLastName;
	@Expose
	@SerializedName("CAPSAPPLICANTFIRSTNAME")
	private  String capsApplicantFirstName;
	@Expose
	@SerializedName("CAPSAPPLICANTMIDDLENAME1")
	private  String capsApplicantMiddleName1;
	@Expose
	@SerializedName("CAPSAPPLICANTMIDDLENAME2")
	private  String capsApplicantMiddleName2;
	@Expose
	@SerializedName("CAPSAPPLICANTMIDDLENAME3")
	private  String capsApplicantMiddleName3;
	@Expose
	@SerializedName("CAPSAPPLICANTGENDERCODE")
	private  String capsApplicantgenderCode;
	@Expose
	@SerializedName("CAPSAPPLICANTGENDER")
	private  String capsApplicantGender;
	@Expose
	@SerializedName("CAPSINCOME_TAX_PAN")
	private  String capsIncomeTaxPan;
	@Expose
	@SerializedName("CAPSPAN_ISSUE_DATE")
	private  Date capsPanIssueDate;
	@Expose
	@SerializedName("CAPSPAN_EXP_DATE")
	private  Date capsPanExpDate;
	@Expose
	@SerializedName("CAPSPASSPORT_NUMBER")
	private  String capsPassportNumber;
	@Expose
	@SerializedName("CAPSPASSPORT_ISSUE_DATE")
	private  Date capsPassportIssueDate;
	@Expose
	@SerializedName("CAPSPASSPORT_EXP_DATE")
	private  Date capsPassportExpDate;
	@Expose
	@SerializedName("CAPSVOTER_IDENTITY_CARD")
	private  String capsVoterIdentityCard;
	@Expose
	@SerializedName("CAPSVOTER_ID_ISSUE_DATE")
	private  Date capsVoterIdIssueDate;
	@Expose
	@SerializedName("CAPSVOTER_ID_EXP_DATE")
	private  Date capsVoterIdExpDate;
	@Expose
	@SerializedName("CAPSDRIVER_LICENSE_NUMBER")
	private  String capsDriverLicenseNumber;
	@Expose
	@SerializedName("CAPSDRIVER_LICENSE_ISSUE_DATE")
	private  Date capsDriverLicenseIssueDate;
	@Expose
	@SerializedName("CAPSDRIVER_LICENSE_EXP_DATE")
	private  Date capsDriverLicenseExpDate;
	@Expose
	@SerializedName("CAPSRATION_CARD_NUMBER")
	private  String capsRationCardNumber;
	@Expose
	@SerializedName("CAPSRATION_CARD_ISSUE_DATE")
	private  Date capsRationCardIssueDate;
	@Expose
	@SerializedName("CAPSRATION_CARD_EXP_DATE")
	private  Date capsRationCardExpDate;
	@Expose
	@SerializedName("CAPSUNIVERSAL_ID_NUMBER")
	private  String capsUniversalIdNumber;
	@Expose
	@SerializedName("CAPSUNIVERSAL_ID_ISSUE_DATE")
	private  Date capsUniversalIdIssueDate;
	@Expose
	@SerializedName("CAPSUNIVERSAL_ID_EXP_DATE")
	private  Date capsUniversalIdExpDate;
	@Expose
	@SerializedName("CAPSAPPLICANTDOBAPPLICANT")
	private  Date capsApplicantDobApplicant;
	@Expose
	@SerializedName("CAPSAPPLICANTTELNOAPPLICANT1ST")
	private  String capsApplicantTelNoApplicant1st;
	@Expose
	@SerializedName("CAPSAPPLICANTTELEXT")
	private  String capsApplicantTelExt;
	@Expose
	@SerializedName("CAPSAPPLICANTTELTYPE")
	private  String capsApplicantTelType;
	@Expose
	@SerializedName("CAPSAPPLICANTMOBILEPHONENUMBER")
	private  String capsApplicantMobilePhoneNumber;
	@Expose
	@SerializedName("CAPSAPPLICANTEMAILID")
	private  String capsApplicantEmailId;
	@Expose
	@SerializedName("CAPSAPPLICANTINCOME")
	private  Integer capsApplicantIncome;
	@Expose
	@SerializedName("APPLICANT_MARITALSTATUSCODE")
	private  String applicantMaritalStatusCode;
	@Expose
	@SerializedName("CAPSAPPLICANTMARITALSTATUS")
	private  String capsApplicantMaritalStatus;
	@Expose
	@SerializedName("CAPSAPPLICANTEMPLMTSTATUSCODE")
	private  String capsApplicantEmploymentStatusCode;
	@Expose
	@SerializedName("CAPSAPPLICANTEMPLOYMENTSTATUS")
	private  String capsApplicantEmploymentStatus;
	@Expose
	@SerializedName("CAPSAPPLICANTDATEWITHEMPLOYER")
	private  String capsApplicantDateWithEmployer;
	@Expose
	@SerializedName("CAPSAPPLTNOMAJORCRDTCARDHELD")
	private  String capsApplicantNoMajorCrditCardHeld;
	@Expose
	@SerializedName("CAPSAPPLTFLATNOPLOTNOHOUSENO")
	private  String capsApplicantFlatNoPlotNoHouseNo;
	@Expose
	@SerializedName("CAPSAPPLICANTBLDGNOSOCIETYNAME")
	private  String capsApplicantBldgNoSocietyName;
	@Expose
	@SerializedName("CAPSAPPLTRDNONAMEAREALOCALITY")
	private  String capsApplicantRoadNoNameAreaLocality;
	@Expose
	@SerializedName("CAPSAPPLICANTCITY")
	private  String capsApplicantCity;
	@Expose
	@SerializedName("CAPSAPPLICANTLANDMARK")
	private  String capsApplicantLandmark;
	@Expose
	@SerializedName("CAPSAPPLICANTSTATECODE")
	private  String capsApplicantStateCode;
	@Expose
	@SerializedName("CAPSAPPLICANTSTATE")
	private  String capsApplicantState;
	@Expose
	@SerializedName("CAPSAPPLICANTPINCODE")
	private  String capsApplicantPinCode;
	@Expose
	@SerializedName("CAPSAPPLICANTCOUNTRYCODE")
	private  String capsApplicantCountryCode;
	@Expose
	@SerializedName("BUREAUSCORE")
	private  Integer bureauScore;
	@Expose
	@SerializedName("BUREAUSCORECONFIDLEVEL")
	private  String bureauScoreConfidLevel;
	@Expose
	@SerializedName("CREDITRATING")
	private  String creditRating;
	@Expose
	@SerializedName("TNOFBFHLCADEXHL")
	private  Integer tnOfBFHLCADExhl;
	@Expose
	@SerializedName("TOTVALOFBFHLCAD")
	private  Integer totValOfBFHLCAD;
	@Expose
	@SerializedName("MNTSMRBFHLCAD")
	private  Integer MNTSMRBFHLCAD;
	@Expose
	@SerializedName("TNOFHLCAD")
	private  Integer tnOfHLCAD;
	@Expose
	@SerializedName("TOTVALOFHLCAD")
	private  Integer totValOfHLCAD;
	@Expose
	@SerializedName("MNTSMRHLCAD")
	private  Integer mntsmrHLCAD;
	@Expose
	@SerializedName("TNOFTELCOSCAD")
	private  Integer tnOfTelcosCAD;
	@Expose
	@SerializedName("TOTVALOFTELCOSCAD")
	private  Integer totValOfTelcosCad;
	@Expose
	@SerializedName("MNTSMRTELCOSCAD")
	private  Integer mntsmrTelcosCad;
	@Expose
	@SerializedName("TNOFMFCAD")
	private  Integer tnOfmfCAD;
	@Expose
	@SerializedName("TOTVALOFMFCAD")
	private  Integer totValOfmfCAD;
	@Expose
	@SerializedName("MNTSMRMFCAD")
	private  Integer mntsmrmfCAD;
	@Expose
	@SerializedName("TNOFRETAILCAD")
	private  Integer tnOfRetailCAD;
	@Expose
	@SerializedName("TOTVALOFRETAILCAD")
	private  Integer totValOfRetailCAD;
	@Expose
	@SerializedName("MNTSMRRETAILCAD")
	private  Integer mntsmrRetailCAD;
	@Expose
	@SerializedName("TNOFALLCAD")
	private  Integer tnOfAllCAD;
	@Expose
	@SerializedName("TOTVALOFALLCAD")
	private  Integer totValOfAllCAD;
	@Expose
	@SerializedName("MNTSMRCADALL")
	private  Integer mntsmrCADAll;
	@Expose
	@SerializedName("TNOFBFHLACAEXHL")
	private  Integer tnOfBFHLACAExhl;
	@Expose
	@SerializedName("BALBFHLACAEXHL")
	private  Integer balBFHLACAExhl;
	@Expose
	@SerializedName("WCDSTBFHLACAEXHL")
	private  Integer wcdstBFHLACAExhl;
	@Expose
	@SerializedName("WDSPR6MNTBFHLACAEXHL")
	private  Integer wdspr6MntBFHLACAExhl;
	@Expose
	@SerializedName("WDSPR712MNTBFHLACAEXHL")
	private  Integer wdspr712MntBFHLACAExhl;
	@Expose
	@SerializedName("AGEOFOLDESTBFHLACAEXHL")
	private  Integer ageOfOldestBFHLACAExhl;
	@Expose
	@SerializedName("HCBPERREVACCBFHLACAEXHL")
	private  Integer hcbperrevaccBFHLACAExhl;
	@Expose
	@SerializedName("TCBPERREVACCBFHLACAEXHL")
	private  Integer tcbperrevaccBFHLACAExhl;
	@Expose
	@SerializedName("TNOFHLACA")
	private  Integer tnOfHlACA;
	@Expose
	@SerializedName("BALHLACA")
	private  Integer balHlACA;
	@Expose
	@SerializedName("WCDSTHLACA")
	private  Integer wcdstHlACA;
	@Expose
	@SerializedName("WDSPR6MNTHLACA")
	private  Integer wdspr6MnthlACA;
	@Expose
	@SerializedName("WDSPR712MNTHLACA")
	private  Integer wdspr712mnthlACA;
	@Expose
	@SerializedName("AGEOFOLDESTHLACA")
	private  Integer ageOfOldesthlACA;
	@Expose
	@SerializedName("TNOFMFACA")
	private  Integer tnOfMfACA;
	@Expose
	@SerializedName("TOTALBALMFACA")
	private  Integer totalBalMfACA;
	@Expose
	@SerializedName("WCDSTMFACA")
	private  Integer wcdstMfACA;
	@Expose
	@SerializedName("WDSPR6MNTMFACA")
	private  Integer wdspr6MntMfACA;
	@Expose
	@SerializedName("WDSPR712MNTMFACA")
	private  Integer wdspr712mntMfACA;
	@Expose
	@SerializedName("AGEOFOLDESTMFACA")
	private  Integer ageOfOldestMfACA;
	@Expose
	@SerializedName("TNOFTELCOSACA")
	private  Integer tnOfTelcosACA;
	@Expose
	@SerializedName("TOTALBALTELCOSACA")
	private  Integer totalBalTelcosACA;
	@Expose
	@SerializedName("WCDSTTELCOSACA")
	private  Integer wcdstTelcosACA;
	@Expose
	@SerializedName("WDSPR6MNTTELCOSACA")
	private  Integer wdspr6mntTelcosACA;
	@Expose
	@SerializedName("WDSPR712MNTTELCOSACA")
	private  Integer wdspr712mntTelcosACA;
	@Expose
	@SerializedName("AGEOFOLDESTTELCOSACA")
	private  Integer ageOfOldestTelcosACA;
	@Expose
	@SerializedName("TNOFRETAILACA")
	private  Integer tnOfRetailACA;
	@Expose
	@SerializedName("TOTALBALRETAILACA")
	private  Integer totalBalRetailACA;
	@Expose
	@SerializedName("WCDSTRETAILACA")
	private  Integer wcdstRetailACA;
	@Expose
	@SerializedName("WDSPR6MNTRETAILACA")
	private  Integer wdspr6mntRetailACA;
	@Expose
	@SerializedName("WDSPR712MNTRETAILACA")
	private  Integer wdspr712mntRetailACA;
	@Expose
	@SerializedName("AGEOFOLDESTRETAILACA")
	private  Integer ageOfOldestRetailACA;
	@Expose
	@SerializedName("HCBLMPERREVACCRET")
	private  Integer hcblmperrevaccret;
	@Expose
	@SerializedName("TOTCURBALLMPERREVACCRET")
	private  Integer totCurBallmperrevaccret;
	@Expose
	@SerializedName("TNOFALLACA")
	private  Integer tnOfallACA;
	@Expose
	@SerializedName("BALALLACAEXHL")
	private  Integer balAllACAExhl;
	@Expose
	@SerializedName("WCDSTALLACA")
	private  Integer wcdstAllACA;
	@Expose
	@SerializedName("WDSPR6MNTALLACA")
	private  Integer wdspr6mntallACA;
	@Expose
	@SerializedName("WDSPR712MNTALLACA")
	private  Integer wdspr712mntAllACA;
	@Expose
	@SerializedName("AGEOFOLDESTALLACA")
	private  Integer ageOfOldestAllACA;
	@Expose
	@SerializedName("TNOFNDELBFHLINACAEXHL")
	private  Integer tnOfndelBFHLinACAExhl;
	@Expose
	@SerializedName("TNOFDELBFHLINACAEXHL")
	private  Integer tnOfDelBFHLInACAExhl;
	@Expose
	@SerializedName("TNOFNDELHLINACA")
	private  Integer tnOfnDelHLInACA;
	@Expose
	@SerializedName("TNOFDELHLINACA")
	private  Integer tnOfDelhlInACA;
	@Expose
	@SerializedName("TNOFNDELMFINACA")
	private  Integer tnOfnDelmfinACA;
	@Expose
	@SerializedName("TNOFDELMFINACA")
	private  Integer tnOfDelmfinACA;
	@Expose
	@SerializedName("TNOFNDELTELCOSINACA")
	private  Integer tnOfnDelTelcosInACA;
	@Expose
	@SerializedName("TNOFDELTELCOSINACA")
	private  Integer tnOfdelTelcosInACA;
	@Expose
	@SerializedName("TNOFNDELRETAILINACA")
	private  Integer tnOfndelRetailInACA;
	@Expose
	@SerializedName("TNOFDELRETAILINACA")
	private  Integer tnOfDelRetailInACA;
	@Expose
	@SerializedName("BFHLCAPSLAST90DAYS")
	private  Integer BFHLCapsLast90Days;
	@Expose
	@SerializedName("MFCAPSLAST90DAYS")
	private  Integer mfCapsLast90Days;
	@Expose
	@SerializedName("TELCOSCAPSLAST90DAYS")
	private  Integer telcosCapsLast90Days;
	@Expose
	@SerializedName("RETAILCAPSLAST90DAYS")
	private  Integer retailCapsLast90Days;
	@Expose
	@SerializedName("TNOFOCOMCAD")
	private  Integer tnOfocomcad;
	@Expose
	@SerializedName("TOTVALOFOCOMCAD")
	private  Integer totValOfocomCAD;
	@Expose
	@SerializedName("MNTSMROCOMCAD")
	private  Integer mntsmrocomCAD;
	@Expose
	@SerializedName("TNOFOCOMACA")
	private  Integer tnOfocomACA;
	@Expose
	@SerializedName("BALOCOMACAEXHL")
	private  Integer balocomACAExhl;
	@Expose
	@SerializedName("BALOCOMACAHLONLY")
	private  Integer balOcomacahlonly;
	@Expose
	@SerializedName("WCDSTOCOMACA")
	private  Integer wcdstocomACA;
	@Expose
	@SerializedName("HCBLMPERREVOCOMACA")
	private  Integer hcblmperrevocomACA;
	@Expose
	@SerializedName("TNOFNDELOCOMINACA")
	private  Integer tnOfnDelocominACA;
	@Expose
	@SerializedName("TNOFDELOCOMINACA")
	private  Integer tnOfDelocominACA;
	@Expose
	@SerializedName("TNOFOCOMCAPSLAST90DAYS")
	private  Integer tnOfocomCapsLast90Days;
	@Expose
	@SerializedName("ANYRELCBDATADISYN")
	private  Integer anyRelcbdatadisyn;
	@Expose
	@SerializedName("OTHRELCBDFCPOSMATYN")
	private  Integer othrelcbdfcposmatyn;
	@Expose
	@SerializedName("TNOFCADCLASSEDASSFWDWO")
	private  Integer tnOfCADclassedassfwdwo;
	@Expose
	@SerializedName("MNTSMRCADCLASSEDASSFWDWO")
	private  Integer mntsmrcadclassedassfwdwo;
	@Expose
	@SerializedName("NUMOFCADSFWDWOLAST24MNT")
	private  Integer numOfCadsfwdwolast24mnt;
	@Expose
	@SerializedName("TOTCURBALLIVESACC")
	private  Integer totCurBalLivesAcc;
	@Expose
	@SerializedName("TOTCURBALLIVEUACC")
	private  Integer totCurBalLiveuAcc;
	@Expose
	@SerializedName("TOTCURBALMAXBALLIVESACC")
	private  Integer totCurBalMaxBalLivesAcc;
	@Expose
	@SerializedName("TOTCURBALMAXBALLIVEUACC")
	private  Integer totCurBalMaxBalLiveuAcc;
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private  String outputWriteFlag;
	@Expose
	@SerializedName("OUTPUT_WRITE_DATE")
	private  String outputWriteDate;
	@Expose
	@SerializedName("OUTPUT_READ_DATE")
	private  String outputReadDate;
	@Expose
	@SerializedName("ACCOUNT_KEY")
	private  Integer accountKey;
	@Expose
	@SerializedName("DATEOFADDITION")
	private  Date dateOfAddition;
	
	public HibExperianSropDomain(Integer srNo, String soaSourceName, String memberReferenceNumber, Date pickupDate) {
		this.srNo = srNo;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = memberReferenceNumber;
		this.enquiryDate=pickupDate;
	}
	

	public HibExperianSropDomain(Integer srNumber, String soaSourceName, String memberReferenceNumber, String accountNumber,Date pickupDate) {
		this.srNo = srNumber;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = memberReferenceNumber;
		this.accountNumber = accountNumber;
		this.enquiryDate=pickupDate;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSrNo() {
		return srNo;
	}
	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}
	public String getSoaSourceName() {
		return soaSourceName;
	}
	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}
	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}
	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}
	public Date getEnquiryDate() {
		return enquiryDate;
	}
	public void setEnquiryDate(Date enquiryDate) {
		this.enquiryDate = enquiryDate;
	}
	public String getSystemcodeHeader() {
		return systemcodeHeader;
	}
	public void setSystemcodeHeader(String systemcodeHeader) {
		this.systemcodeHeader = systemcodeHeader;
	}
	public String getMessageTextHeader() {
		return messageTextHeader;
	}
	public void setMessageTextHeader(String messageTextHeader) {
		this.messageTextHeader = messageTextHeader;
	}
	public Date getReportdate() {
		return reportdate;
	}
	public void setReportdate(Date reportdate) {
		this.reportdate = reportdate;
	}
	public String getReportimeHeader() {
		return reportimeHeader;
	}
	public void setReportimeHeader(String reportimeHeader) {
		this.reportimeHeader = reportimeHeader;
	}
	public String getUserMessageText() {
		return userMessageText;
	}
	public void setUserMessageText(String userMessageText) {
		this.userMessageText = userMessageText;
	}
	public String getEnquiryUsername() {
		return enquiryUsername;
	}
	public void setEnquiryUsername(String enquiryUsername) {
		this.enquiryUsername = enquiryUsername;
	}
	public Date getCreditProfileReportDate() {
		return creditProfileReportDate;
	}
	public void setCreditProfileReportDate(Date creditProfileReportDate) {
		this.creditProfileReportDate = creditProfileReportDate;
	}
	public String getCreditProfileReportTime() {
		return CreditProfileReportTime;
	}
	public void setCreditProfileReportTime(String creditProfileReportTime) {
		CreditProfileReportTime = creditProfileReportTime;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getReportNumber() {
		return reportNumber;
	}
	public void setReportNumber(String reportNumber) {
		this.reportNumber = reportNumber;
	}
	public String getSubscriber() {
		return subscriber;
	}
	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}
	public String getSubscriberName() {
		return subscriberName;
	}
	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
	public String getEnquiryReason() {
		return enquiryReason;
	}
	public void setEnquiryReason(String enquiryReason) {
		this.enquiryReason = enquiryReason;
	}
	public String getFinancePurpose() {
		return financePurpose;
	}
	public void setFinancePurpose(String financePurpose) {
		this.financePurpose = financePurpose;
	}
	public Integer getAmountFinanced() {
		return amountFinanced;
	}
	public void setAmountFinanced(Integer amountFinanced) {
		this.amountFinanced = amountFinanced;
	}
	public Integer getDurationOfAgreement() {
		return durationOfAgreement;
	}
	public void setDurationOfAgreement(Integer durationOfAgreement) {
		this.durationOfAgreement = durationOfAgreement;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddleName1() {
		return middleName1;
	}
	public void setMiddleName1(String middleName1) {
		this.middleName1 = middleName1;
	}
	public String getMiddleName2() {
		return middleName2;
	}
	public void setMiddleName2(String middleName2) {
		this.middleName2 = middleName2;
	}
	public String getMiddleName3() {
		return middleName3;
	}
	public void setMiddleName3(String middleName3) {
		this.middleName3 = middleName3;
	}
	public String getGenderCode() {
		return genderCode;
	}
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIncomeTaxPanApp() {
		return incomeTaxPanApp;
	}
	public void setIncomeTaxPanApp(String incomeTaxPanApp) {
		this.incomeTaxPanApp = incomeTaxPanApp;
	}
	public Date getPanIssueDateApp() {
		return panIssueDateApp;
	}
	public void setPanIssueDateApp(Date panIssueDateApp) {
		this.panIssueDateApp = panIssueDateApp;
	}
	public Date getPanExpDateApp() {
		return panExpDateApp;
	}
	public void setPanExpDateApp(Date panExpDateApp) {
		this.panExpDateApp = panExpDateApp;
	}
	public String getPassportNumberApp() {
		return passportNumberApp;
	}
	public void setPassportNumberApp(String passportNumberApp) {
		this.passportNumberApp = passportNumberApp;
	}
	public Date getPassportIssueDateApp() {
		return passportIssueDateApp;
	}
	public void setPassportIssueDateApp(Date passportIssueDateApp) {
		this.passportIssueDateApp = passportIssueDateApp;
	}
	public Date getPassportExpDateApp() {
		return passportExpDateApp;
	}
	public void setPassportExpDateApp(Date passportExpDateApp) {
		this.passportExpDateApp = passportExpDateApp;
	}
	public String getVoterIdentityCardApp() {
		return voterIdentityCardApp;
	}
	public void setVoterIdentityCardApp(String voterIdentityCardApp) {
		this.voterIdentityCardApp = voterIdentityCardApp;
	}
	public Date getVoterIdIssueDateApp() {
		return voterIdIssueDateApp;
	}
	public void setVoterIdIssueDateApp(Date voterIdIssueDateApp) {
		this.voterIdIssueDateApp = voterIdIssueDateApp;
	}
	public Date getVoterIdExpDateApp() {
		return voterIdExpDateApp;
	}
	public void setVoterIdExpDateApp(Date voterIdExpDateApp) {
		this.voterIdExpDateApp = voterIdExpDateApp;
	}
	public String getDriverLicenseNumberApp() {
		return driverLicenseNumberApp;
	}
	public void setDriverLicenseNumberApp(String driverLicenseNumberApp) {
		this.driverLicenseNumberApp = driverLicenseNumberApp;
	}
	public Date getDriverLicenseIssueDateApp() {
		return driverLicenseIssueDateApp;
	}
	public void setDriverLicenseIssueDateApp(Date driverLicenseIssueDateApp) {
		this.driverLicenseIssueDateApp = driverLicenseIssueDateApp;
	}
	public Date getDriverLicenseExpDateApp() {
		return driverLicenseExpDateApp;
	}
	public void setDriverLicenseExpDateApp(Date driverLicenseExpDateApp) {
		this.driverLicenseExpDateApp = driverLicenseExpDateApp;
	}
	public String getRationCardNumberApp() {
		return rationCardNumberApp;
	}
	public void setRationCardNumberApp(String rationCardNumberApp) {
		this.rationCardNumberApp = rationCardNumberApp;
	}
	public Date getRationCardIssueDateApp() {
		return rationCardIssueDateApp;
	}
	public void setRationCardIssueDateApp(Date rationCardIssueDateApp) {
		this.rationCardIssueDateApp = rationCardIssueDateApp;
	}
	public Date getRationCardExpDateApp() {
		return rationCardExpDateApp;
	}
	public void setRationCardExpDateApp(Date rationCardExpDateApp) {
		this.rationCardExpDateApp = rationCardExpDateApp;
	}
	public String getUniversalIdNumberApp() {
		return universalIdNumberApp;
	}
	public void setUniversalIdNumberApp(String universalIdNumberApp) {
		this.universalIdNumberApp = universalIdNumberApp;
	}
	public Date getUniversalIdIssueDateApp() {
		return universalIdIssueDateApp;
	}
	public void setUniversalIdIssueDateApp(Date universalIdIssueDateApp) {
		this.universalIdIssueDateApp = universalIdIssueDateApp;
	}
	public Date getUniversalIdExpDateApp() {
		return universalIdExpDateApp;
	}
	public void setUniversalIdExpDateApp(Date universalIdExpDateApp) {
		this.universalIdExpDateApp = universalIdExpDateApp;
	}
	public Date getDateOfBirthApplicant() {
		return dateOfBirthApplicant;
	}
	public void setDateOfBirthApplicant(Date dateOfBirthApplicant) {
		this.dateOfBirthApplicant = dateOfBirthApplicant;
	}
	public String getTelephoneNumberApplicant1st() {
		return telephoneNumberApplicant1st;
	}
	public void setTelephoneNumberApplicant1st(String telephoneNumberApplicant1st) {
		this.telephoneNumberApplicant1st = telephoneNumberApplicant1st;
	}
	public String getTeleExtensionApp() {
		return teleExtensionApp;
	}
	public void setTeleExtensionApp(String teleExtensionApp) {
		this.teleExtensionApp = teleExtensionApp;
	}
	public String getTeleTypeApp() {
		return teleTypeApp;
	}
	public void setTeleTypeApp(String teleTypeApp) {
		this.teleTypeApp = teleTypeApp;
	}
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}
	public String getEmailIdApp() {
		return emailIdApp;
	}
	public void setEmailIdApp(String emailIdApp) {
		this.emailIdApp = emailIdApp;
	}
	public String getIncome() {
		return income;
	}
	public void setIncome(String income) {
		this.income = income;
	}
	public String getMaritalStatusCode() {
		return maritalStatusCode;
	}
	public void setMaritalStatusCode(String maritalStatusCode) {
		this.maritalStatusCode = maritalStatusCode;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public Integer getDateWithEmployer() {
		return dateWithEmployer;
	}
	public void setDateWithEmployer(Integer dateWithEmployer) {
		this.dateWithEmployer = dateWithEmployer;
	}
	public String getNumberOfMajorCreditCardHeld() {
		return numberOfMajorCreditCardHeld;
	}
	public void setNumberOfMajorCreditCardHeld(String numberOfMajorCreditCardHeld) {
		this.numberOfMajorCreditCardHeld = numberOfMajorCreditCardHeld;
	}
	public String getFlatNoPlotNoHouseNo() {
		return flatNoPlotNoHouseNo;
	}
	public void setFlatNoPlotNoHouseNo(String flatNoPlotNoHouseNo) {
		this.flatNoPlotNoHouseNo = flatNoPlotNoHouseNo;
	}
	public String getBldgNoSocietyName() {
		return bldgNoSocietyName;
	}
	public void setBldgNoSocietyName(String bldgNoSocietyName) {
		this.bldgNoSocietyName = bldgNoSocietyName;
	}
	public String getRoadNoNameAreaLocality() {
		return roadNoNameAreaLocality;
	}
	public void setRoadNoNameAreaLocality(String roadNoNameAreaLocality) {
		this.roadNoNameAreaLocality = roadNoNameAreaLocality;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getAddFlatNoPlotNoHouseNo() {
		return addFlatNoPlotNoHouseNo;
	}
	public void setAddFlatNoPlotNoHouseNo(String addFlatNoPlotNoHouseNo) {
		this.addFlatNoPlotNoHouseNo = addFlatNoPlotNoHouseNo;
	}
	public String getAddBldgNoSocietyName() {
		return addBldgNoSocietyName;
	}
	public void setAddBldgNoSocietyName(String addBldgNoSocietyName) {
		this.addBldgNoSocietyName = addBldgNoSocietyName;
	}
	public String getAddRoadNoNameAreaLocality() {
		return addRoadNoNameAreaLocality;
	}
	public void setAddRoadNoNameAreaLocality(String addRoadNoNameAreaLocality) {
		this.addRoadNoNameAreaLocality = addRoadNoNameAreaLocality;
	}
	public String getAddCity() {
		return addCity;
	}
	public void setAddCity(String addCity) {
		this.addCity = addCity;
	}
	public String getAddLandmark() {
		return addLandmark;
	}
	public void setAddLandmark(String addLandmark) {
		this.addLandmark = addLandmark;
	}
	public String getAddState() {
		return addState;
	}
	public void setAddState(String addState) {
		this.addState = addState;
	}
	public String getAddPincode() {
		return addPincode;
	}
	public void setAddPincode(String addPincode) {
		this.addPincode = addPincode;
	}
	public String getAddCountrycode() {
		return addCountrycode;
	}
	public void setAddCountrycode(String addCountrycode) {
		this.addCountrycode = addCountrycode;
	}
	public String getCreditAccountTotal() {
		return creditAccountTotal;
	}
	public void setCreditAccountTotal(String creditAccountTotal) {
		this.creditAccountTotal = creditAccountTotal;
	}
	public Integer getCreditAccountActive() {
		return creditAccountActive;
	}
	public void setCreditAccountActive(Integer creditAccountActive) {
		this.creditAccountActive = creditAccountActive;
	}
	public Integer getCreditAccountDefault() {
		return creditAccountDefault;
	}
	public void setCreditAccountDefault(Integer creditAccountDefault) {
		this.creditAccountDefault = creditAccountDefault;
	}
	public Integer getCreditAccountClosed() {
		return creditAccountClosed;
	}
	public void setCreditAccountClosed(Integer creditAccountClosed) {
		this.creditAccountClosed = creditAccountClosed;
	}
	public Integer getCadsuitfiledCurrentBalance() {
		return cadsuitfiledCurrentBalance;
	}
	public void setCadsuitfiledCurrentBalance(Integer cadsuitfiledCurrentBalance) {
		this.cadsuitfiledCurrentBalance = cadsuitfiledCurrentBalance;
	}
	public String getOutstandingBalanceSecured() {
		return outstandingBalanceSecured;
	}
	public void setOutstandingBalanceSecured(String outstandingBalanceSecured) {
		this.outstandingBalanceSecured = outstandingBalanceSecured;
	}
	public String getOutstandingBalSecuredPer() {
		return outstandingBalSecuredPer;
	}
	public void setOutstandingBalSecuredPer(String outstandingBalSecuredPer) {
		this.outstandingBalSecuredPer = outstandingBalSecuredPer;
	}
	public String getOutstandingBalanceUnsecured() {
		return outstandingBalanceUnsecured;
	}
	public void setOutstandingBalanceUnsecured(String outstandingBalanceUnsecured) {
		this.outstandingBalanceUnsecured = outstandingBalanceUnsecured;
	}
	public String getOutstandingBalUnsecPer() {
		return outstandingBalUnsecPer;
	}
	public void setOutstandingBalUnsecPer(String outstandingBalUnsecPer) {
		this.outstandingBalUnsecPer = outstandingBalUnsecPer;
	}
	public String getOutstandingBalanceAll() {
		return outstandingBalanceAll;
	}
	public void setOutstandingBalanceAll(String outstandingBalanceAll) {
		this.outstandingBalanceAll = outstandingBalanceAll;
	}
	public String getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	public String getCaisSubscriberName() {
		return caisSubscriberName;
	}
	public void setCaisSubscriberName(String caisSubscriberName) {
		this.caisSubscriberName = caisSubscriberName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getPortfolioType() {
		return portfolioType;
	}
	public void setPortfolioType(String portfolioType) {
		this.portfolioType = portfolioType;
	}
	public String getAccountTypeCode() {
		return accountTypeCode;
	}
	public void setAccountTypeCode(String accountTypeCode) {
		this.accountTypeCode = accountTypeCode;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	public String getHighestCreditOrOrgnLoanAmt() {
		return highestCreditOrOrgnLoanAmt;
	}
	public void setHighestCreditOrOrgnLoanAmt(String highestCreditOrOrgnLoanAmt) {
		this.highestCreditOrOrgnLoanAmt = highestCreditOrOrgnLoanAmt;
	}
	public String getTermsDuration() {
		return termsDuration;
	}
	public void setTermsDuration(String termsDuration) {
		this.termsDuration = termsDuration;
	}
	public String getTermsFrequency() {
		return termsFrequency;
	}
	public void setTermsFrequency(String termsFrequency) {
		this.termsFrequency = termsFrequency;
	}
	public String getScheduledMonthlyPayamt() {
		return scheduledMonthlyPayamt;
	}
	public void setScheduledMonthlyPayamt(String scheduledMonthlyPayamt) {
		this.scheduledMonthlyPayamt = scheduledMonthlyPayamt;
	}
	public String getAccountStatusCode() {
		return accountStatusCode;
	}
	public void setAccountStatusCode(String accountStatusCode) {
		this.accountStatusCode = accountStatusCode;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getPaymentRating() {
		return paymentRating;
	}
	public void setPaymentRating(String paymentRating) {
		this.paymentRating = paymentRating;
	}
	public String getPaymentHistoryProfile() {
		return paymentHistoryProfile;
	}
	public void setPaymentHistoryProfile(String paymentHistoryProfile) {
		this.paymentHistoryProfile = paymentHistoryProfile;
	}
	public String getSpecialComment() {
		return specialComment;
	}
	public void setSpecialComment(String specialComment) {
		this.specialComment = specialComment;
	}
	public Integer getCurrentBalanceTl() {
		return currentBalanceTl;
	}
	public void setCurrentBalanceTl(Integer currentBalanceTl) {
		this.currentBalanceTl = currentBalanceTl;
	}
	public Integer getAmountPastdueTl() {
		return amountPastdueTl;
	}
	public void setAmountPastdueTl(Integer amountPastdueTl) {
		this.amountPastdueTl = amountPastdueTl;
	}
	public Integer getOriginalChargeOffAmount() {
		return originalChargeOffAmount;
	}
	public void setOriginalChargeOffAmount(Integer originalChargeOffAmount) {
		this.originalChargeOffAmount = originalChargeOffAmount;
	}
	public Date getDateReported() {
		return dateReported;
	}
	public void setDateReported(Date dateReported) {
		this.dateReported = dateReported;
	}
	public Date getDateOfFirstDelinquency() {
		return dateOfFirstDelinquency;
	}
	public void setDateOfFirstDelinquency(Date dateOfFirstDelinquency) {
		this.dateOfFirstDelinquency = dateOfFirstDelinquency;
	}
	public Date getDateClosed() {
		return dateClosed;
	}
	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}
	public Date getDateOfLastPayment() {
		return dateOfLastPayment;
	}
	public void setDateOfLastPayment(Date dateOfLastPayment) {
		this.dateOfLastPayment = dateOfLastPayment;
	}
	public String getSuitfilledWillFullDefaultWrittenOffStatus() {
		return suitfilledWillFullDefaultWrittenOffStatus;
	}
	public void setSuitfilledWillFullDefaultWrittenOffStatus(
			String suitfilledWillFullDefaultWrittenOffStatus) {
		this.suitfilledWillFullDefaultWrittenOffStatus = suitfilledWillFullDefaultWrittenOffStatus;
	}
	public String getSuitFiledWilfulDef() {
		return suitFiledWilfulDef;
	}
	public void setSuitFiledWilfulDef(String suitFiledWilfulDef) {
		this.suitFiledWilfulDef = suitFiledWilfulDef;
	}
	public String getWrittenOffSettledStatus() {
		return writtenOffSettledStatus;
	}
	public void setWrittenOffSettledStatus(String writtenOffSettledStatus) {
		this.writtenOffSettledStatus = writtenOffSettledStatus;
	}
	public String getValueOfCreditsLastMonth() {
		return valueOfCreditsLastMonth;
	}
	public void setValueOfCreditsLastMonth(String valueOfCreditsLastMonth) {
		this.valueOfCreditsLastMonth = valueOfCreditsLastMonth;
	}
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public Integer getSattlementAmount() {
		return sattlementAmount;
	}
	public void setSattlementAmount(Integer sattlementAmount) {
		this.sattlementAmount = sattlementAmount;
	}
	public Integer getValueOfColateral() {
		return valueOfColateral;
	}
	public void setValueOfColateral(Integer valueOfColateral) {
		this.valueOfColateral = valueOfColateral;
	}
	public String getTypeOfColateral() {
		return typeOfColateral;
	}
	public void setTypeOfColateral(String typeOfColateral) {
		this.typeOfColateral = typeOfColateral;
	}
	public Integer getWrittenOffAmtTotal() {
		return writtenOffAmtTotal;
	}
	public void setWrittenOffAmtTotal(Integer writtenOffAmtTotal) {
		this.writtenOffAmtTotal = writtenOffAmtTotal;
	}
	public Integer getWrittenOffAmtPrincipal() {
		return writtenOffAmtPrincipal;
	}
	public void setWrittenOffAmtPrincipal(Integer writtenOffAmtPrincipal) {
		this.writtenOffAmtPrincipal = writtenOffAmtPrincipal;
	}
	public Integer getRateOfInterest() {
		return rateOfInterest;
	}
	public void setRateOfInterest(Integer rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}
	public Integer getRepaymentTenure() {
		return repaymentTenure;
	}
	public void setRepaymentTenure(Integer repaymentTenure) {
		this.repaymentTenure = repaymentTenure;
	}
	public String getPromotionalRateFlag() {
		return promotionalRateFlag;
	}
	public void setPromotionalRateFlag(String promotionalRateFlag) {
		this.promotionalRateFlag = promotionalRateFlag;
	}
	public Integer getCaisIncome() {
		return caisIncome;
	}
	public void setCaisIncome(Integer caisIncome) {
		this.caisIncome = caisIncome;
	}
	public String getIncomeIndicator() {
		return incomeIndicator;
	}
	public void setIncomeIndicator(String incomeIndicator) {
		this.incomeIndicator = incomeIndicator;
	}
	public String getIncomeFrequencyIndicator() {
		return incomeFrequencyIndicator;
	}
	public void setIncomeFrequencyIndicator(String incomeFrequencyIndicator) {
		this.incomeFrequencyIndicator = incomeFrequencyIndicator;
	}
	public Date getDefaultStatusDate() {
		return defaultStatusDate;
	}
	public void setDefaultStatusDate(Date defaultStatusDate) {
		this.defaultStatusDate = defaultStatusDate;
	}
	public Date getLitigationStatusDate() {
		return litigationStatusDate;
	}
	public void setLitigationStatusDate(Date litigationStatusDate) {
		this.litigationStatusDate = litigationStatusDate;
	}
	public Date getWriteOffStatusDate() {
		return writeOffStatusDate;
	}
	public void setWriteOffStatusDate(Date writeOffStatusDate) {
		this.writeOffStatusDate = writeOffStatusDate;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getSubscriberComments() {
		return subscriberComments;
	}
	public void setSubscriberComments(String subscriberComments) {
		this.subscriberComments = subscriberComments;
	}
	public String getConsumerComments() {
		return consumerComments;
	}
	public void setConsumerComments(String consumerComments) {
		this.consumerComments = consumerComments;
	}
	public String getAccountHolderTypeCode() {
		return accountHolderTypeCode;
	}
	public void setAccountHolderTypeCode(String accountHolderTypeCode) {
		this.accountHolderTypeCode = accountHolderTypeCode;
	}
	public String getAccountHolderTypeName() {
		return accountHolderTypeName;
	}
	public void setAccountHolderTypeName(String accountHolderTypeName) {
		this.accountHolderTypeName = accountHolderTypeName;
	}
	public String getYearHist() {
		return yearHist;
	}
	public void setYearHist(String yearHist) {
		this.yearHist = yearHist;
	}
	public String getMonthHist() {
		return monthHist;
	}
	public void setMonthHist(String monthHist) {
		this.monthHist = monthHist;
	}
	public String getDaysPastDue() {
		return daysPastDue;
	}
	public void setDaysPastDue(String daysPastDue) {
		this.daysPastDue = daysPastDue;
	}
	public String getAssetClassification() {
		return assetClassification;
	}
	public void setAssetClassification(String assetClassification) {
		this.assetClassification = assetClassification;
	}
	public String getYearAdvHist() {
		return yearAdvHist;
	}
	public void setYearAdvHist(String yearAdvHist) {
		this.yearAdvHist = yearAdvHist;
	}
	public String getMonthAdvHist() {
		return monthAdvHist;
	}
	public void setMonthAdvHist(String monthAdvHist) {
		this.monthAdvHist = monthAdvHist;
	}
	public Integer getCashLimit() {
		return cashLimit;
	}
	public void setCashLimit(Integer cashLimit) {
		this.cashLimit = cashLimit;
	}
	public Integer getCreditLimitAmt() {
		return creditLimitAmt;
	}
	public void setCreditLimitAmt(Integer creditLimitAmt) {
		this.creditLimitAmt = creditLimitAmt;
	}
	public Integer getActualPaymentAmt() {
		return actualPaymentAmt;
	}
	public void setActualPaymentAmt(Integer actualPaymentAmt) {
		this.actualPaymentAmt = actualPaymentAmt;
	}
	public Integer getEmiAmt() {
		return emiAmt;
	}
	public void setEmiAmt(Integer emiAmt) {
		this.emiAmt = emiAmt;
	}
	public Integer getCurrentBalanceAdvHist() {
		return currentBalanceAdvHist;
	}
	public void setCurrentBalanceAdvHist(Integer currentBalanceAdvHist) {
		this.currentBalanceAdvHist = currentBalanceAdvHist;
	}
	public Integer getAmountPastDueAdvHist() {
		return amountPastDueAdvHist;
	}
	public void setAmountPastDueAdvHist(Integer amountPastDueAdvHist) {
		this.amountPastDueAdvHist = amountPastDueAdvHist;
	}
	public String getSurNameNonNormalized() {
		return surNameNonNormalized;
	}
	public void setSurNameNonNormalized(String surNameNonNormalized) {
		this.surNameNonNormalized = surNameNonNormalized;
	}
	public String getFirstNameNonNormalized() {
		return firstNameNonNormalized;
	}
	public void setFirstNameNonNormalized(String firstNameNonNormalized) {
		this.firstNameNonNormalized = firstNameNonNormalized;
	}
	public String getMiddleName1NonNormalized() {
		return middleName1NonNormalized;
	}
	public void setMiddleName1NonNormalized(String middleName1NonNormalized) {
		this.middleName1NonNormalized = middleName1NonNormalized;
	}
	public String getMiddleName2NonNormalized() {
		return middleName2NonNormalized;
	}
	public void setMiddleName2NonNormalized(String middleName2NonNormalized) {
		this.middleName2NonNormalized = middleName2NonNormalized;
	}
	public String getMiddleName3NonNormalized() {
		return middleName3NonNormalized;
	}
	public void setMiddleName3NonNormalized(String middleName3NonNormalized) {
		this.middleName3NonNormalized = middleName3NonNormalized;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getCaisGenderCode() {
		return caisGenderCode;
	}
	public void setCaisGenderCode(String caisGenderCode) {
		this.caisGenderCode = caisGenderCode;
	}
	public String getCaisGender() {
		return caisGender;
	}
	public void setCaisGender(String caisGender) {
		this.caisGender = caisGender;
	}
	public String getCaisIncomeTaxPan() {
		return caisIncomeTaxPan;
	}
	public void setCaisIncomeTaxPan(String caisIncomeTaxPan) {
		this.caisIncomeTaxPan = caisIncomeTaxPan;
	}
	public String getCaisPassportNumber() {
		return caisPassportNumber;
	}
	public void setCaisPassportNumber(String caisPassportNumber) {
		this.caisPassportNumber = caisPassportNumber;
	}
	public String getVoterIdNumber() {
		return voterIdNumber;
	}
	public void setVoterIdNumber(String voterIdNumber) {
		this.voterIdNumber = voterIdNumber;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getFirstLineOfAddNonNormalized() {
		return firstLineOfAddNonNormalized;
	}
	public void setFirstLineOfAddNonNormalized(String firstLineOfAddNonNormalized) {
		this.firstLineOfAddNonNormalized = firstLineOfAddNonNormalized;
	}
	public String getSecondLineOfAddNonNormalized() {
		return secondLineOfAddNonNormalized;
	}
	public void setSecondLineOfAddNonNormalized(String secondLineOfAddNonNormalized) {
		this.secondLineOfAddNonNormalized = secondLineOfAddNonNormalized;
	}
	public String getThirdLineOfAddNonNormalized() {
		return thirdLineOfAddNonNormalized;
	}
	public void setThirdLineOfAddNonNormalized(String thirdLineOfAddNonNormalized) {
		this.thirdLineOfAddNonNormalized = thirdLineOfAddNonNormalized;
	}
	public String getCityNonNormalized() {
		return cityNonNormalized;
	}
	public void setCityNonNormalized(String cityNonNormalized) {
		this.cityNonNormalized = cityNonNormalized;
	}
	public String getFifthLineOfAddNonNormalized() {
		return fifthLineOfAddNonNormalized;
	}
	public void setFifthLineOfAddNonNormalized(String fifthLineOfAddNonNormalized) {
		this.fifthLineOfAddNonNormalized = fifthLineOfAddNonNormalized;
	}
	public String getStateCodeNonNormalized() {
		return stateCodeNonNormalized;
	}
	public void setStateCodeNonNormalized(String stateCodeNonNormalized) {
		this.stateCodeNonNormalized = stateCodeNonNormalized;
	}
	public String getStateNonNormalized() {
		return stateNonNormalized;
	}
	public void setStateNonNormalized(String stateNonNormalized) {
		this.stateNonNormalized = stateNonNormalized;
	}
	public String getZipPostalCodeNonNormalized() {
		return zipPostalCodeNonNormalized;
	}
	public void setZipPostalCodeNonNormalized(String zipPostalCodeNonNormalized) {
		this.zipPostalCodeNonNormalized = zipPostalCodeNonNormalized;
	}
	public String getCountryCodeNonNormalized() {
		return countryCodeNonNormalized;
	}
	public void setCountryCodeNonNormalized(String countryCodeNonNormalized) {
		this.countryCodeNonNormalized = countryCodeNonNormalized;
	}
	public String getAddIndicatorNonNormalized() {
		return addIndicatorNonNormalized;
	}
	public void setAddIndicatorNonNormalized(String addIndicatorNonNormalized) {
		this.addIndicatorNonNormalized = addIndicatorNonNormalized;
	}
	public String getResidenceCodeNonNormalized() {
		return residenceCodeNonNormalized;
	}
	public void setResidenceCodeNonNormalized(String residenceCodeNonNormalized) {
		this.residenceCodeNonNormalized = residenceCodeNonNormalized;
	}
	public String getResidenceNonNormalized() {
		return residenceNonNormalized;
	}
	public void setResidenceNonNormalized(String residenceNonNormalized) {
		this.residenceNonNormalized = residenceNonNormalized;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getTeleTypePhone() {
		return teleTypePhone;
	}
	public void setTeleTypePhone(String teleTypePhone) {
		this.teleTypePhone = teleTypePhone;
	}
	public String getTeleExtensionPhone() {
		return teleExtensionPhone;
	}
	public void setTeleExtensionPhone(String teleExtensionPhone) {
		this.teleExtensionPhone = teleExtensionPhone;
	}
	public String getMobileTelephoneNumber() {
		return mobileTelephoneNumber;
	}
	public void setMobileTelephoneNumber(String mobileTelephoneNumber) {
		this.mobileTelephoneNumber = mobileTelephoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmailIdPhone() {
		return emailIdPhone;
	}
	public void setEmailIdPhone(String emailIdPhone) {
		this.emailIdPhone = emailIdPhone;
	}
	public String getIncomeTaxPanId() {
		return incomeTaxPanId;
	}
	public void setIncomeTaxPanId(String incomeTaxPanId) {
		this.incomeTaxPanId = incomeTaxPanId;
	}
	public Date getPanIssueDateId() {
		return panIssueDateId;
	}
	public void setPanIssueDateId(Date panIssueDateId) {
		this.panIssueDateId = panIssueDateId;
	}
	public Date getPanExpDateId() {
		return panExpDateId;
	}
	public void setPanExpDateId(Date panExpDateId) {
		this.panExpDateId = panExpDateId;
	}
	public String getPassportNumberId() {
		return passportNumberId;
	}
	public void setPassportNumberId(String passportNumberId) {
		this.passportNumberId = passportNumberId;
	}
	public Date getPassportIssueDateId() {
		return passportIssueDateId;
	}
	public void setPassportIssueDateId(Date passportIssueDateId) {
		this.passportIssueDateId = passportIssueDateId;
	}
	public Date getPassportExpDateId() {
		return passportExpDateId;
	}
	public void setPassportExpDateId(Date passportExpDateId) {
		this.passportExpDateId = passportExpDateId;
	}
	public String getVoterIdentityCardId() {
		return voterIdentityCardId;
	}
	public void setVoterIdentityCardId(String voterIdentityCardId) {
		this.voterIdentityCardId = voterIdentityCardId;
	}
	public Date getVoterIdIssueDateId() {
		return voterIdIssueDateId;
	}
	public void setVoterIdIssueDateId(Date voterIdIssueDateId) {
		this.voterIdIssueDateId = voterIdIssueDateId;
	}
	public Date getVoterIdExpDateId() {
		return voterIdExpDateId;
	}
	public void setVoterIdExpDateId(Date voterIdExpDateId) {
		this.voterIdExpDateId = voterIdExpDateId;
	}
	public String getDriverLicenseNumberId() {
		return driverLicenseNumberId;
	}
	public void setDriverLicenseNumberId(String driverLicenseNumberId) {
		this.driverLicenseNumberId = driverLicenseNumberId;
	}
	public Date getDriverLicenseIssueDateId() {
		return driverLicenseIssueDateId;
	}
	public void setDriverLicenseIssueDateId(Date driverLicenseIssueDateId) {
		this.driverLicenseIssueDateId = driverLicenseIssueDateId;
	}
	public Date getDriverLicenseExpDateId() {
		return driverLicenseExpDateId;
	}
	public void setDriverLicenseExpDateId(Date driverLicenseExpDateId) {
		this.driverLicenseExpDateId = driverLicenseExpDateId;
	}
	public String getRationCardNumberId() {
		return rationCardNumberId;
	}
	public void setRationCardNumberId(String rationCardNumberId) {
		this.rationCardNumberId = rationCardNumberId;
	}
	public Date getRationCardIssueDateId() {
		return rationCardIssueDateId;
	}
	public void setRationCardIssueDateId(Date rationCardIssueDateId) {
		this.rationCardIssueDateId = rationCardIssueDateId;
	}
	public Date getRationCardExpDateId() {
		return rationCardExpDateId;
	}
	public void setRationCardExpDateId(Date rationCardExpDateId) {
		this.rationCardExpDateId = rationCardExpDateId;
	}
	public String getUniversalIdNumberId() {
		return universalIdNumberId;
	}
	public void setUniversalIdNumberId(String universalIdNumberId) {
		this.universalIdNumberId = universalIdNumberId;
	}
	public Date getUniversalIdIssueDateId() {
		return universalIdIssueDateId;
	}
	public void setUniversalIdIssueDateId(Date universalIdIssueDateId) {
		this.universalIdIssueDateId = universalIdIssueDateId;
	}
	public Date getUniversalIdExpDateId() {
		return universalIdExpDateId;
	}
	public void setUniversalIdExpDateId(Date universalIdExpDateId) {
		this.universalIdExpDateId = universalIdExpDateId;
	}
	public String getEmailidId() {
		return emailidId;
	}
	public void setEmailidId(String emailidId) {
		this.emailidId = emailidId;
	}
	public String getExactMatch() {
		return exactMatch;
	}
	public void setExactMatch(String exactMatch) {
		this.exactMatch = exactMatch;
	}
	public Integer getCapsLast7Days() {
		return capsLast7Days;
	}
	public void setCapsLast7Days(Integer capsLast7Days) {
		this.capsLast7Days = capsLast7Days;
	}
	public Integer getCapsLast30Days() {
		return capsLast30Days;
	}
	public void setCapsLast30Days(Integer capsLast30Days) {
		this.capsLast30Days = capsLast30Days;
	}
	public Integer getCapsLast90Days() {
		return capsLast90Days;
	}
	public void setCapsLast90Days(Integer capsLast90Days) {
		this.capsLast90Days = capsLast90Days;
	}
	public Integer getCapsLast180Days() {
		return capsLast180Days;
	}
	public void setCapsLast180Days(Integer capsLast180Days) {
		this.capsLast180Days = capsLast180Days;
	}
	public String getCapsSubscriberCode() {
		return capsSubscriberCode;
	}
	public void setCapsSubscriberCode(String capsSubscriberCode) {
		this.capsSubscriberCode = capsSubscriberCode;
	}
	public String getCapsSubscriberName() {
		return capsSubscriberName;
	}
	public void setCapsSubscriberName(String capsSubscriberName) {
		this.capsSubscriberName = capsSubscriberName;
	}
	public Date getCapsdateofRequest() {
		return capsdateofRequest;
	}
	public void setCapsdateofRequest(Date capsdateofRequest) {
		this.capsdateofRequest = capsdateofRequest;
	}
	public Date getCapsReportDate() {
		return capsReportDate;
	}
	public void setCapsReportDate(Date capsReportDate) {
		this.capsReportDate = capsReportDate;
	}
	public String getCapsReportTime() {
		return capsReportTime;
	}
	public void setCapsReportTime(String capsReportTime) {
		this.capsReportTime = capsReportTime;
	}
	public String getCapsReportNumber() {
		return capsReportNumber;
	}
	public void setCapsReportNumber(String capsReportNumber) {
		this.capsReportNumber = capsReportNumber;
	}
	public String getEnquiryReasonCode() {
		return enquiryReasonCode;
	}
	public void setEnquiryReasonCode(String enquiryReasonCode) {
		this.enquiryReasonCode = enquiryReasonCode;
	}
	public String getCapsEnquiryReason() {
		return capsEnquiryReason;
	}
	public void setCapsEnquiryReason(String capsEnquiryReason) {
		this.capsEnquiryReason = capsEnquiryReason;
	}
	public String getFinancePurposeCode() {
		return financePurposeCode;
	}
	public void setFinancePurposeCode(String financePurposeCode) {
		this.financePurposeCode = financePurposeCode;
	}
	public String getCapsFinancePurpose() {
		return capsFinancePurpose;
	}
	public void setCapsFinancePurpose(String capsFinancePurpose) {
		this.capsFinancePurpose = capsFinancePurpose;
	}
	public String getCapsAmountFinanced() {
		return capsAmountFinanced;
	}
	public void setCapsAmountFinanced(String capsAmountFinanced) {
		this.capsAmountFinanced = capsAmountFinanced;
	}
	public String getCapsDurationOfAgreement() {
		return capsDurationOfAgreement;
	}
	public void setCapsDurationOfAgreement(String capsDurationOfAgreement) {
		this.capsDurationOfAgreement = capsDurationOfAgreement;
	}
	public String getCapsApplicantLastName() {
		return capsApplicantLastName;
	}
	public void setCapsApplicantLastName(String capsApplicantLastName) {
		this.capsApplicantLastName = capsApplicantLastName;
	}
	public String getCapsApplicantFirstName() {
		return capsApplicantFirstName;
	}
	public void setCapsApplicantFirstName(String capsApplicantFirstName) {
		this.capsApplicantFirstName = capsApplicantFirstName;
	}
	public String getCapsApplicantMiddleName1() {
		return capsApplicantMiddleName1;
	}
	public void setCapsApplicantMiddleName1(String capsApplicantMiddleName1) {
		this.capsApplicantMiddleName1 = capsApplicantMiddleName1;
	}
	public String getCapsApplicantMiddleName2() {
		return capsApplicantMiddleName2;
	}
	public void setCapsApplicantMiddleName2(String capsApplicantMiddleName2) {
		this.capsApplicantMiddleName2 = capsApplicantMiddleName2;
	}
	public String getCapsApplicantMiddleName3() {
		return capsApplicantMiddleName3;
	}
	public void setCapsApplicantMiddleName3(String capsApplicantMiddleName3) {
		this.capsApplicantMiddleName3 = capsApplicantMiddleName3;
	}
	public String getCapsApplicantgenderCode() {
		return capsApplicantgenderCode;
	}
	public void setCapsApplicantgenderCode(String capsApplicantgenderCode) {
		this.capsApplicantgenderCode = capsApplicantgenderCode;
	}
	public String getCapsApplicantGender() {
		return capsApplicantGender;
	}
	public void setCapsApplicantGender(String capsApplicantGender) {
		this.capsApplicantGender = capsApplicantGender;
	}
	public String getCapsIncomeTaxPan() {
		return capsIncomeTaxPan;
	}
	public void setCapsIncomeTaxPan(String capsIncomeTaxPan) {
		this.capsIncomeTaxPan = capsIncomeTaxPan;
	}
	public Date getCapsPanIssueDate() {
		return capsPanIssueDate;
	}
	public void setCapsPanIssueDate(Date capsPanIssueDate) {
		this.capsPanIssueDate = capsPanIssueDate;
	}
	public Date getCapsPanExpDate() {
		return capsPanExpDate;
	}
	public void setCapsPanExpDate(Date capsPanExpDate) {
		this.capsPanExpDate = capsPanExpDate;
	}
	public String getCapsPassportNumber() {
		return capsPassportNumber;
	}
	public void setCapsPassportNumber(String capsPassportNumber) {
		this.capsPassportNumber = capsPassportNumber;
	}
	public Date getCapsPassportIssueDate() {
		return capsPassportIssueDate;
	}
	public void setCapsPassportIssueDate(Date capsPassportIssueDate) {
		this.capsPassportIssueDate = capsPassportIssueDate;
	}
	public Date getCapsPassportExpDate() {
		return capsPassportExpDate;
	}
	public void setCapsPassportExpDate(Date capsPassportExpDate) {
		this.capsPassportExpDate = capsPassportExpDate;
	}
	public String getCapsVoterIdentityCard() {
		return capsVoterIdentityCard;
	}
	public void setCapsVoterIdentityCard(String capsVoterIdentityCard) {
		this.capsVoterIdentityCard = capsVoterIdentityCard;
	}
	public Date getCapsVoterIdIssueDate() {
		return capsVoterIdIssueDate;
	}
	public void setCapsVoterIdIssueDate(Date capsVoterIdIssueDate) {
		this.capsVoterIdIssueDate = capsVoterIdIssueDate;
	}
	public Date getCapsVoterIdExpDate() {
		return capsVoterIdExpDate;
	}
	public void setCapsVoterIdExpDate(Date capsVoterIdExpDate) {
		this.capsVoterIdExpDate = capsVoterIdExpDate;
	}
	public String getCapsDriverLicenseNumber() {
		return capsDriverLicenseNumber;
	}
	public void setCapsDriverLicenseNumber(String capsDriverLicenseNumber) {
		this.capsDriverLicenseNumber = capsDriverLicenseNumber;
	}
	public Date getCapsDriverLicenseIssueDate() {
		return capsDriverLicenseIssueDate;
	}
	public void setCapsDriverLicenseIssueDate(Date capsDriverLicenseIssueDate) {
		this.capsDriverLicenseIssueDate = capsDriverLicenseIssueDate;
	}
	public Date getCapsDriverLicenseExpDate() {
		return capsDriverLicenseExpDate;
	}
	public void setCapsDriverLicenseExpDate(Date capsDriverLicenseExpDate) {
		this.capsDriverLicenseExpDate = capsDriverLicenseExpDate;
	}
	public String getCapsRationCardNumber() {
		return capsRationCardNumber;
	}
	public void setCapsRationCardNumber(String capsRationCardNumber) {
		this.capsRationCardNumber = capsRationCardNumber;
	}
	public Date getCapsRationCardIssueDate() {
		return capsRationCardIssueDate;
	}
	public void setCapsRationCardIssueDate(Date capsRationCardIssueDate) {
		this.capsRationCardIssueDate = capsRationCardIssueDate;
	}
	public Date getCapsRationCardExpDate() {
		return capsRationCardExpDate;
	}
	public void setCapsRationCardExpDate(Date capsRationCardExpDate) {
		this.capsRationCardExpDate = capsRationCardExpDate;
	}
	public String getCapsUniversalIdNumber() {
		return capsUniversalIdNumber;
	}
	public void setCapsUniversalIdNumber(String capsUniversalIdNumber) {
		this.capsUniversalIdNumber = capsUniversalIdNumber;
	}
	public Date getCapsUniversalIdIssueDate() {
		return capsUniversalIdIssueDate;
	}
	public void setCapsUniversalIdIssueDate(Date capsUniversalIdIssueDate) {
		this.capsUniversalIdIssueDate = capsUniversalIdIssueDate;
	}
	public Date getCapsUniversalIdExpDate() {
		return capsUniversalIdExpDate;
	}
	public void setCapsUniversalIdExpDate(Date capsUniversalIdExpDate) {
		this.capsUniversalIdExpDate = capsUniversalIdExpDate;
	}
	public Date getCapsApplicantDobApplicant() {
		return capsApplicantDobApplicant;
	}
	public void setCapsApplicantDobApplicant(Date capsApplicantDobApplicant) {
		this.capsApplicantDobApplicant = capsApplicantDobApplicant;
	}
	public String getCapsApplicantTelNoApplicant1st() {
		return capsApplicantTelNoApplicant1st;
	}
	public void setCapsApplicantTelNoApplicant1st(
			String capsApplicantTelNoApplicant1st) {
		this.capsApplicantTelNoApplicant1st = capsApplicantTelNoApplicant1st;
	}
	public String getCapsApplicantTelExt() {
		return capsApplicantTelExt;
	}
	public void setCapsApplicantTelExt(String capsApplicantTelExt) {
		this.capsApplicantTelExt = capsApplicantTelExt;
	}
	public String getCapsApplicantTelType() {
		return capsApplicantTelType;
	}
	public void setCapsApplicantTelType(String capsApplicantTelType) {
		this.capsApplicantTelType = capsApplicantTelType;
	}
	public String getCapsApplicantMobilePhoneNumber() {
		return capsApplicantMobilePhoneNumber;
	}
	public void setCapsApplicantMobilePhoneNumber(
			String capsApplicantMobilePhoneNumber) {
		this.capsApplicantMobilePhoneNumber = capsApplicantMobilePhoneNumber;
	}
	public String getCapsApplicantEmailId() {
		return capsApplicantEmailId;
	}
	public void setCapsApplicantEmailId(String capsApplicantEmailId) {
		this.capsApplicantEmailId = capsApplicantEmailId;
	}
	public Integer getCapsApplicantIncome() {
		return capsApplicantIncome;
	}
	public void setCapsApplicantIncome(Integer capsApplicantIncome) {
		this.capsApplicantIncome = capsApplicantIncome;
	}
	public String getApplicantMaritalStatusCode() {
		return applicantMaritalStatusCode;
	}
	public void setApplicantMaritalStatusCode(String applicantMaritalStatusCode) {
		this.applicantMaritalStatusCode = applicantMaritalStatusCode;
	}
	public String getCapsApplicantMaritalStatus() {
		return capsApplicantMaritalStatus;
	}
	public void setCapsApplicantMaritalStatus(String capsApplicantMaritalStatus) {
		this.capsApplicantMaritalStatus = capsApplicantMaritalStatus;
	}
	public String getCapsApplicantEmploymentStatusCode() {
		return capsApplicantEmploymentStatusCode;
	}
	public void setCapsApplicantEmploymentStatusCode(
			String capsApplicantEmploymentStatusCode) {
		this.capsApplicantEmploymentStatusCode = capsApplicantEmploymentStatusCode;
	}
	public String getCapsApplicantEmploymentStatus() {
		return capsApplicantEmploymentStatus;
	}
	public void setCapsApplicantEmploymentStatus(
			String capsApplicantEmploymentStatus) {
		this.capsApplicantEmploymentStatus = capsApplicantEmploymentStatus;
	}
	public String getCapsApplicantDateWithEmployer() {
		return capsApplicantDateWithEmployer;
	}
	public void setCapsApplicantDateWithEmployer(
			String capsApplicantDateWithEmployer) {
		this.capsApplicantDateWithEmployer = capsApplicantDateWithEmployer;
	}
	public String getCapsApplicantNoMajorCrditCardHeld() {
		return capsApplicantNoMajorCrditCardHeld;
	}
	public void setCapsApplicantNoMajorCrditCardHeld(
			String capsApplicantNoMajorCrditCardHeld) {
		this.capsApplicantNoMajorCrditCardHeld = capsApplicantNoMajorCrditCardHeld;
	}
	public String getCapsApplicantFlatNoPlotNoHouseNo() {
		return capsApplicantFlatNoPlotNoHouseNo;
	}
	public void setCapsApplicantFlatNoPlotNoHouseNo(
			String capsApplicantFlatNoPlotNoHouseNo) {
		this.capsApplicantFlatNoPlotNoHouseNo = capsApplicantFlatNoPlotNoHouseNo;
	}
	public String getCapsApplicantBldgNoSocietyName() {
		return capsApplicantBldgNoSocietyName;
	}
	public void setCapsApplicantBldgNoSocietyName(
			String capsApplicantBldgNoSocietyName) {
		this.capsApplicantBldgNoSocietyName = capsApplicantBldgNoSocietyName;
	}
	public String getCapsApplicantRoadNoNameAreaLocality() {
		return capsApplicantRoadNoNameAreaLocality;
	}
	public void setCapsApplicantRoadNoNameAreaLocality(
			String capsApplicantRoadNoNameAreaLocality) {
		this.capsApplicantRoadNoNameAreaLocality = capsApplicantRoadNoNameAreaLocality;
	}
	public String getCapsApplicantCity() {
		return capsApplicantCity;
	}
	public void setCapsApplicantCity(String capsApplicantCity) {
		this.capsApplicantCity = capsApplicantCity;
	}
	public String getCapsApplicantLandmark() {
		return capsApplicantLandmark;
	}
	public void setCapsApplicantLandmark(String capsApplicantLandmark) {
		this.capsApplicantLandmark = capsApplicantLandmark;
	}
	public String getCapsApplicantStateCode() {
		return capsApplicantStateCode;
	}
	public void setCapsApplicantStateCode(String capsApplicantStateCode) {
		this.capsApplicantStateCode = capsApplicantStateCode;
	}
	public String getCapsApplicantState() {
		return capsApplicantState;
	}
	public void setCapsApplicantState(String capsApplicantState) {
		this.capsApplicantState = capsApplicantState;
	}
	public String getCapsApplicantPinCode() {
		return capsApplicantPinCode;
	}
	public void setCapsApplicantPinCode(String capsApplicantPinCode) {
		this.capsApplicantPinCode = capsApplicantPinCode;
	}
	public String getCapsApplicantCountryCode() {
		return capsApplicantCountryCode;
	}
	public void setCapsApplicantCountryCode(String capsApplicantCountryCode) {
		this.capsApplicantCountryCode = capsApplicantCountryCode;
	}
	public Integer getBureauScore() {
		return bureauScore;
	}
	public void setBureauScore(Integer bureauScore) {
		this.bureauScore = bureauScore;
	}
	public String getBureauScoreConfidLevel() {
		return bureauScoreConfidLevel;
	}
	public void setBureauScoreConfidLevel(String bureauScoreConfidLevel) {
		this.bureauScoreConfidLevel = bureauScoreConfidLevel;
	}
	public String getCreditRating() {
		return creditRating;
	}
	public void setCreditRating(String creditRating) {
		this.creditRating = creditRating;
	}
	public Integer getTnOfBFHLCADExhl() {
		return tnOfBFHLCADExhl;
	}
	public void setTnOfBFHLCADExhl(Integer tnOfBFHLCADExhl) {
		this.tnOfBFHLCADExhl = tnOfBFHLCADExhl;
	}
	public Integer getTotValOfBFHLCAD() {
		return totValOfBFHLCAD;
	}
	public void setTotValOfBFHLCAD(Integer totValOfBFHLCAD) {
		this.totValOfBFHLCAD = totValOfBFHLCAD;
	}
	public Integer getMNTSMRBFHLCAD() {
		return MNTSMRBFHLCAD;
	}
	public void setMNTSMRBFHLCAD(Integer mNTSMRBFHLCAD) {
		MNTSMRBFHLCAD = mNTSMRBFHLCAD;
	}
	public Integer getTnOfHLCAD() {
		return tnOfHLCAD;
	}
	public void setTnOfHLCAD(Integer tnOfHLCAD) {
		this.tnOfHLCAD = tnOfHLCAD;
	}
	public Integer getTotValOfHLCAD() {
		return totValOfHLCAD;
	}
	public void setTotValOfHLCAD(Integer totValOfHLCAD) {
		this.totValOfHLCAD = totValOfHLCAD;
	}
	public Integer getMntsmrHLCAD() {
		return mntsmrHLCAD;
	}
	public void setMntsmrHLCAD(Integer mntsmrHLCAD) {
		this.mntsmrHLCAD = mntsmrHLCAD;
	}
	public Integer getTnOfTelcosCAD() {
		return tnOfTelcosCAD;
	}
	public void setTnOfTelcosCAD(Integer tnOfTelcosCAD) {
		this.tnOfTelcosCAD = tnOfTelcosCAD;
	}
	public Integer getTotValOfTelcosCad() {
		return totValOfTelcosCad;
	}
	public void setTotValOfTelcosCad(Integer totValOfTelcosCad) {
		this.totValOfTelcosCad = totValOfTelcosCad;
	}
	public Integer getMntsmrTelcosCad() {
		return mntsmrTelcosCad;
	}
	public void setMntsmrTelcosCad(Integer mntsmrTelcosCad) {
		this.mntsmrTelcosCad = mntsmrTelcosCad;
	}
	public Integer getTnOfmfCAD() {
		return tnOfmfCAD;
	}
	public void setTnOfmfCAD(Integer tnOfmfCAD) {
		this.tnOfmfCAD = tnOfmfCAD;
	}
	public Integer getTotValOfmfCAD() {
		return totValOfmfCAD;
	}
	public void setTotValOfmfCAD(Integer totValOfmfCAD) {
		this.totValOfmfCAD = totValOfmfCAD;
	}
	public Integer getMntsmrmfCAD() {
		return mntsmrmfCAD;
	}
	public void setMntsmrmfCAD(Integer mntsmrmfCAD) {
		this.mntsmrmfCAD = mntsmrmfCAD;
	}
	public Integer getTnOfRetailCAD() {
		return tnOfRetailCAD;
	}
	public void setTnOfRetailCAD(Integer tnOfRetailCAD) {
		this.tnOfRetailCAD = tnOfRetailCAD;
	}
	public Integer getTotValOfRetailCAD() {
		return totValOfRetailCAD;
	}
	public void setTotValOfRetailCAD(Integer totValOfRetailCAD) {
		this.totValOfRetailCAD = totValOfRetailCAD;
	}
	public Integer getMntsmrRetailCAD() {
		return mntsmrRetailCAD;
	}
	public void setMntsmrRetailCAD(Integer mntsmrRetailCAD) {
		this.mntsmrRetailCAD = mntsmrRetailCAD;
	}
	public Integer getTnOfAllCAD() {
		return tnOfAllCAD;
	}
	public void setTnOfAllCAD(Integer tnOfAllCAD) {
		this.tnOfAllCAD = tnOfAllCAD;
	}
	public Integer getTotValOfAllCAD() {
		return totValOfAllCAD;
	}
	public void setTotValOfAllCAD(Integer totValOfAllCAD) {
		this.totValOfAllCAD = totValOfAllCAD;
	}
	public Integer getMntsmrCADAll() {
		return mntsmrCADAll;
	}
	public void setMntsmrCADAll(Integer mntsmrCADAll) {
		this.mntsmrCADAll = mntsmrCADAll;
	}
	public Integer getTnOfBFHLACAExhl() {
		return tnOfBFHLACAExhl;
	}
	public void setTnOfBFHLACAExhl(Integer tnOfBFHLACAExhl) {
		this.tnOfBFHLACAExhl = tnOfBFHLACAExhl;
	}
	public Integer getBalBFHLACAExhl() {
		return balBFHLACAExhl;
	}
	public void setBalBFHLACAExhl(Integer balBFHLACAExhl) {
		this.balBFHLACAExhl = balBFHLACAExhl;
	}
	public Integer getWcdstBFHLACAExhl() {
		return wcdstBFHLACAExhl;
	}
	public void setWcdstBFHLACAExhl(Integer wcdstBFHLACAExhl) {
		this.wcdstBFHLACAExhl = wcdstBFHLACAExhl;
	}
	public Integer getWdspr6MntBFHLACAExhl() {
		return wdspr6MntBFHLACAExhl;
	}
	public void setWdspr6MntBFHLACAExhl(Integer wdspr6MntBFHLACAExhl) {
		this.wdspr6MntBFHLACAExhl = wdspr6MntBFHLACAExhl;
	}
	public Integer getWdspr712MntBFHLACAExhl() {
		return wdspr712MntBFHLACAExhl;
	}
	public void setWdspr712MntBFHLACAExhl(Integer wdspr712MntBFHLACAExhl) {
		this.wdspr712MntBFHLACAExhl = wdspr712MntBFHLACAExhl;
	}
	public Integer getAgeOfOldestBFHLACAExhl() {
		return ageOfOldestBFHLACAExhl;
	}
	public void setAgeOfOldestBFHLACAExhl(Integer ageOfOldestBFHLACAExhl) {
		this.ageOfOldestBFHLACAExhl = ageOfOldestBFHLACAExhl;
	}
	public Integer getHcbperrevaccBFHLACAExhl() {
		return hcbperrevaccBFHLACAExhl;
	}
	public void setHcbperrevaccBFHLACAExhl(Integer hcbperrevaccBFHLACAExhl) {
		this.hcbperrevaccBFHLACAExhl = hcbperrevaccBFHLACAExhl;
	}
	public Integer getTcbperrevaccBFHLACAExhl() {
		return tcbperrevaccBFHLACAExhl;
	}
	public void setTcbperrevaccBFHLACAExhl(Integer tcbperrevaccBFHLACAExhl) {
		this.tcbperrevaccBFHLACAExhl = tcbperrevaccBFHLACAExhl;
	}
	public Integer getTnOfHlACA() {
		return tnOfHlACA;
	}
	public void setTnOfHlACA(Integer tnOfHlACA) {
		this.tnOfHlACA = tnOfHlACA;
	}
	public Integer getBalHlACA() {
		return balHlACA;
	}
	public void setBalHlACA(Integer balHlACA) {
		this.balHlACA = balHlACA;
	}
	public Integer getWcdstHlACA() {
		return wcdstHlACA;
	}
	public void setWcdstHlACA(Integer wcdstHlACA) {
		this.wcdstHlACA = wcdstHlACA;
	}
	public Integer getWdspr6MnthlACA() {
		return wdspr6MnthlACA;
	}
	public void setWdspr6MnthlACA(Integer wdspr6MnthlACA) {
		this.wdspr6MnthlACA = wdspr6MnthlACA;
	}
	public Integer getWdspr712mnthlACA() {
		return wdspr712mnthlACA;
	}
	public void setWdspr712mnthlACA(Integer wdspr712mnthlACA) {
		this.wdspr712mnthlACA = wdspr712mnthlACA;
	}
	public Integer getAgeOfOldesthlACA() {
		return ageOfOldesthlACA;
	}
	public void setAgeOfOldesthlACA(Integer ageOfOldesthlACA) {
		this.ageOfOldesthlACA = ageOfOldesthlACA;
	}
	public Integer getTnOfMfACA() {
		return tnOfMfACA;
	}
	public void setTnOfMfACA(Integer tnOfMfACA) {
		this.tnOfMfACA = tnOfMfACA;
	}
	public Integer getTotalBalMfACA() {
		return totalBalMfACA;
	}
	public void setTotalBalMfACA(Integer totalBalMfACA) {
		this.totalBalMfACA = totalBalMfACA;
	}
	public Integer getWcdstMfACA() {
		return wcdstMfACA;
	}
	public void setWcdstMfACA(Integer wcdstMfACA) {
		this.wcdstMfACA = wcdstMfACA;
	}
	public Integer getWdspr6MntMfACA() {
		return wdspr6MntMfACA;
	}
	public void setWdspr6MntMfACA(Integer wdspr6MntMfACA) {
		this.wdspr6MntMfACA = wdspr6MntMfACA;
	}
	public Integer getWdspr712mntMfACA() {
		return wdspr712mntMfACA;
	}
	public void setWdspr712mntMfACA(Integer wdspr712mntMfACA) {
		this.wdspr712mntMfACA = wdspr712mntMfACA;
	}
	public Integer getAgeOfOldestMfACA() {
		return ageOfOldestMfACA;
	}
	public void setAgeOfOldestMfACA(Integer ageOfOldestMfACA) {
		this.ageOfOldestMfACA = ageOfOldestMfACA;
	}
	public Integer getTnOfTelcosACA() {
		return tnOfTelcosACA;
	}
	public void setTnOfTelcosACA(Integer tnOfTelcosACA) {
		this.tnOfTelcosACA = tnOfTelcosACA;
	}
	public Integer getTotalBalTelcosACA() {
		return totalBalTelcosACA;
	}
	public void setTotalBalTelcosACA(Integer totalBalTelcosACA) {
		this.totalBalTelcosACA = totalBalTelcosACA;
	}
	public Integer getWcdstTelcosACA() {
		return wcdstTelcosACA;
	}
	public void setWcdstTelcosACA(Integer wcdstTelcosACA) {
		this.wcdstTelcosACA = wcdstTelcosACA;
	}
	public Integer getWdspr6mntTelcosACA() {
		return wdspr6mntTelcosACA;
	}
	public void setWdspr6mntTelcosACA(Integer wdspr6mntTelcosACA) {
		this.wdspr6mntTelcosACA = wdspr6mntTelcosACA;
	}
	public Integer getWdspr712mntTelcosACA() {
		return wdspr712mntTelcosACA;
	}
	public void setWdspr712mntTelcosACA(Integer wdspr712mntTelcosACA) {
		this.wdspr712mntTelcosACA = wdspr712mntTelcosACA;
	}
	public Integer getAgeOfOldestTelcosACA() {
		return ageOfOldestTelcosACA;
	}
	public void setAgeOfOldestTelcosACA(Integer ageOfOldestTelcosACA) {
		this.ageOfOldestTelcosACA = ageOfOldestTelcosACA;
	}
	public Integer getTnOfRetailACA() {
		return tnOfRetailACA;
	}
	public void setTnOfRetailACA(Integer tnOfRetailACA) {
		this.tnOfRetailACA = tnOfRetailACA;
	}
	public Integer getTotalBalRetailACA() {
		return totalBalRetailACA;
	}
	public void setTotalBalRetailACA(Integer totalBalRetailACA) {
		this.totalBalRetailACA = totalBalRetailACA;
	}
	public Integer getWcdstRetailACA() {
		return wcdstRetailACA;
	}
	public void setWcdstRetailACA(Integer wcdstRetailACA) {
		this.wcdstRetailACA = wcdstRetailACA;
	}
	public Integer getWdspr6mntRetailACA() {
		return wdspr6mntRetailACA;
	}
	public void setWdspr6mntRetailACA(Integer wdspr6mntRetailACA) {
		this.wdspr6mntRetailACA = wdspr6mntRetailACA;
	}
	public Integer getWdspr712mntRetailACA() {
		return wdspr712mntRetailACA;
	}
	public void setWdspr712mntRetailACA(Integer wdspr712mntRetailACA) {
		this.wdspr712mntRetailACA = wdspr712mntRetailACA;
	}
	public Integer getAgeOfOldestRetailACA() {
		return ageOfOldestRetailACA;
	}
	public void setAgeOfOldestRetailACA(Integer ageOfOldestRetailACA) {
		this.ageOfOldestRetailACA = ageOfOldestRetailACA;
	}
	public Integer getHcblmperrevaccret() {
		return hcblmperrevaccret;
	}
	public void setHcblmperrevaccret(Integer hcblmperrevaccret) {
		this.hcblmperrevaccret = hcblmperrevaccret;
	}
	public Integer getTotCurBallmperrevaccret() {
		return totCurBallmperrevaccret;
	}
	public void setTotCurBallmperrevaccret(Integer totCurBallmperrevaccret) {
		this.totCurBallmperrevaccret = totCurBallmperrevaccret;
	}
	public Integer getTnOfallACA() {
		return tnOfallACA;
	}
	public void setTnOfallACA(Integer tnOfallACA) {
		this.tnOfallACA = tnOfallACA;
	}
	public Integer getBalAllACAExhl() {
		return balAllACAExhl;
	}
	public void setBalAllACAExhl(Integer balAllACAExhl) {
		this.balAllACAExhl = balAllACAExhl;
	}
	public Integer getWcdstAllACA() {
		return wcdstAllACA;
	}
	public void setWcdstAllACA(Integer wcdstAllACA) {
		this.wcdstAllACA = wcdstAllACA;
	}
	public Integer getWdspr6mntallACA() {
		return wdspr6mntallACA;
	}
	public void setWdspr6mntallACA(Integer wdspr6mntallACA) {
		this.wdspr6mntallACA = wdspr6mntallACA;
	}
	public Integer getWdspr712mntAllACA() {
		return wdspr712mntAllACA;
	}
	public void setWdspr712mntAllACA(Integer wdspr712mntAllACA) {
		this.wdspr712mntAllACA = wdspr712mntAllACA;
	}
	public Integer getAgeOfOldestAllACA() {
		return ageOfOldestAllACA;
	}
	public void setAgeOfOldestAllACA(Integer ageOfOldestAllACA) {
		this.ageOfOldestAllACA = ageOfOldestAllACA;
	}
	public Integer getTnOfndelBFHLinACAExhl() {
		return tnOfndelBFHLinACAExhl;
	}
	public void setTnOfndelBFHLinACAExhl(Integer tnOfndelBFHLinACAExhl) {
		this.tnOfndelBFHLinACAExhl = tnOfndelBFHLinACAExhl;
	}
	public Integer getTnOfDelBFHLInACAExhl() {
		return tnOfDelBFHLInACAExhl;
	}
	public void setTnOfDelBFHLInACAExhl(Integer tnOfDelBFHLInACAExhl) {
		this.tnOfDelBFHLInACAExhl = tnOfDelBFHLInACAExhl;
	}
	public Integer getTnOfnDelHLInACA() {
		return tnOfnDelHLInACA;
	}
	public void setTnOfnDelHLInACA(Integer tnOfnDelHLInACA) {
		this.tnOfnDelHLInACA = tnOfnDelHLInACA;
	}
	public Integer getTnOfDelhlInACA() {
		return tnOfDelhlInACA;
	}
	public void setTnOfDelhlInACA(Integer tnOfDelhlInACA) {
		this.tnOfDelhlInACA = tnOfDelhlInACA;
	}
	public Integer getTnOfnDelmfinACA() {
		return tnOfnDelmfinACA;
	}
	public void setTnOfnDelmfinACA(Integer tnOfnDelmfinACA) {
		this.tnOfnDelmfinACA = tnOfnDelmfinACA;
	}
	public Integer getTnOfDelmfinACA() {
		return tnOfDelmfinACA;
	}
	public void setTnOfDelmfinACA(Integer tnOfDelmfinACA) {
		this.tnOfDelmfinACA = tnOfDelmfinACA;
	}
	public Integer getTnOfnDelTelcosInACA() {
		return tnOfnDelTelcosInACA;
	}
	public void setTnOfnDelTelcosInACA(Integer tnOfnDelTelcosInACA) {
		this.tnOfnDelTelcosInACA = tnOfnDelTelcosInACA;
	}
	public Integer getTnOfdelTelcosInACA() {
		return tnOfdelTelcosInACA;
	}
	public void setTnOfdelTelcosInACA(Integer tnOfdelTelcosInACA) {
		this.tnOfdelTelcosInACA = tnOfdelTelcosInACA;
	}
	public Integer getTnOfndelRetailInACA() {
		return tnOfndelRetailInACA;
	}
	public void setTnOfndelRetailInACA(Integer tnOfndelRetailInACA) {
		this.tnOfndelRetailInACA = tnOfndelRetailInACA;
	}
	public Integer getTnOfDelRetailInACA() {
		return tnOfDelRetailInACA;
	}
	public void setTnOfDelRetailInACA(Integer tnOfDelRetailInACA) {
		this.tnOfDelRetailInACA = tnOfDelRetailInACA;
	}
	public Integer getBFHLCapsLast90Days() {
		return BFHLCapsLast90Days;
	}
	public void setBFHLCapsLast90Days(Integer bFHLCapsLast90Days) {
		BFHLCapsLast90Days = bFHLCapsLast90Days;
	}
	public Integer getMfCapsLast90Days() {
		return mfCapsLast90Days;
	}
	public void setMfCapsLast90Days(Integer mfCapsLast90Days) {
		this.mfCapsLast90Days = mfCapsLast90Days;
	}
	public Integer getTelcosCapsLast90Days() {
		return telcosCapsLast90Days;
	}
	public void setTelcosCapsLast90Days(Integer telcosCapsLast90Days) {
		this.telcosCapsLast90Days = telcosCapsLast90Days;
	}
	public Integer getRetailCapsLast90Days() {
		return retailCapsLast90Days;
	}
	public void setRetailCapsLast90Days(Integer retailCapsLast90Days) {
		this.retailCapsLast90Days = retailCapsLast90Days;
	}
	public Integer getTnOfocomcad() {
		return tnOfocomcad;
	}
	public void setTnOfocomcad(Integer tnOfocomcad) {
		this.tnOfocomcad = tnOfocomcad;
	}
	public Integer getTotValOfocomCAD() {
		return totValOfocomCAD;
	}
	public void setTotValOfocomCAD(Integer totValOfocomCAD) {
		this.totValOfocomCAD = totValOfocomCAD;
	}
	public Integer getMntsmrocomCAD() {
		return mntsmrocomCAD;
	}
	public void setMntsmrocomCAD(Integer mntsmrocomCAD) {
		this.mntsmrocomCAD = mntsmrocomCAD;
	}
	public Integer getTnOfocomACA() {
		return tnOfocomACA;
	}
	public void setTnOfocomACA(Integer tnOfocomACA) {
		this.tnOfocomACA = tnOfocomACA;
	}
	public Integer getBalocomACAExhl() {
		return balocomACAExhl;
	}
	public void setBalocomACAExhl(Integer balocomACAExhl) {
		this.balocomACAExhl = balocomACAExhl;
	}
	public Integer getBalOcomacahlonly() {
		return balOcomacahlonly;
	}
	public void setBalOcomacahlonly(Integer balOcomacahlonly) {
		this.balOcomacahlonly = balOcomacahlonly;
	}
	public Integer getWcdstocomACA() {
		return wcdstocomACA;
	}
	public void setWcdstocomACA(Integer wcdstocomACA) {
		this.wcdstocomACA = wcdstocomACA;
	}
	public Integer getHcblmperrevocomACA() {
		return hcblmperrevocomACA;
	}
	public void setHcblmperrevocomACA(Integer hcblmperrevocomACA) {
		this.hcblmperrevocomACA = hcblmperrevocomACA;
	}
	public Integer getTnOfnDelocominACA() {
		return tnOfnDelocominACA;
	}
	public void setTnOfnDelocominACA(Integer tnOfnDelocominACA) {
		this.tnOfnDelocominACA = tnOfnDelocominACA;
	}
	public Integer getTnOfDelocominACA() {
		return tnOfDelocominACA;
	}
	public void setTnOfDelocominACA(Integer tnOfDelocominACA) {
		this.tnOfDelocominACA = tnOfDelocominACA;
	}
	public Integer getTnOfocomCapsLast90Days() {
		return tnOfocomCapsLast90Days;
	}
	public void setTnOfocomCapsLast90Days(Integer tnOfocomCapsLast90Days) {
		this.tnOfocomCapsLast90Days = tnOfocomCapsLast90Days;
	}
	public Integer getAnyRelcbdatadisyn() {
		return anyRelcbdatadisyn;
	}
	public void setAnyRelcbdatadisyn(Integer anyRelcbdatadisyn) {
		this.anyRelcbdatadisyn = anyRelcbdatadisyn;
	}
	public Integer getOthrelcbdfcposmatyn() {
		return othrelcbdfcposmatyn;
	}
	public void setOthrelcbdfcposmatyn(Integer othrelcbdfcposmatyn) {
		this.othrelcbdfcposmatyn = othrelcbdfcposmatyn;
	}
	public Integer getTnOfCADclassedassfwdwo() {
		return tnOfCADclassedassfwdwo;
	}
	public void setTnOfCADclassedassfwdwo(Integer tnOfCADclassedassfwdwo) {
		this.tnOfCADclassedassfwdwo = tnOfCADclassedassfwdwo;
	}
	public Integer getMntsmrcadclassedassfwdwo() {
		return mntsmrcadclassedassfwdwo;
	}
	public void setMntsmrcadclassedassfwdwo(Integer mntsmrcadclassedassfwdwo) {
		this.mntsmrcadclassedassfwdwo = mntsmrcadclassedassfwdwo;
	}
	public Integer getNumOfCadsfwdwolast24mnt() {
		return numOfCadsfwdwolast24mnt;
	}
	public void setNumOfCadsfwdwolast24mnt(Integer numOfCadsfwdwolast24mnt) {
		this.numOfCadsfwdwolast24mnt = numOfCadsfwdwolast24mnt;
	}
	public Integer getTotCurBalLivesAcc() {
		return totCurBalLivesAcc;
	}
	public void setTotCurBalLivesAcc(Integer totCurBalLivesAcc) {
		this.totCurBalLivesAcc = totCurBalLivesAcc;
	}
	public Integer getTotCurBalLiveuAcc() {
		return totCurBalLiveuAcc;
	}
	public void setTotCurBalLiveuAcc(Integer totCurBalLiveuAcc) {
		this.totCurBalLiveuAcc = totCurBalLiveuAcc;
	}
	public Integer getTotCurBalMaxBalLivesAcc() {
		return totCurBalMaxBalLivesAcc;
	}
	public void setTotCurBalMaxBalLivesAcc(Integer totCurBalMaxBalLivesAcc) {
		this.totCurBalMaxBalLivesAcc = totCurBalMaxBalLivesAcc;
	}
	public Integer getTotCurBalMaxBalLiveuAcc() {
		return totCurBalMaxBalLiveuAcc;
	}
	public void setTotCurBalMaxBalLiveuAcc(Integer totCurBalMaxBalLiveuAcc) {
		this.totCurBalMaxBalLiveuAcc = totCurBalMaxBalLiveuAcc;
	}
	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}
	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}
	public String getOutputWriteDate() {
		return outputWriteDate;
	}
	public void setOutputWriteDate(String outputWriteDate) {
		this.outputWriteDate = outputWriteDate;
	}
	public String getOutputReadDate() {
		return outputReadDate;
	}
	public void setOutputReadDate(String outputReadDate) {
		this.outputReadDate = outputReadDate;
	}
	public Integer getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(Integer accountKey) {
		this.accountKey = accountKey;
	}
	public Date getDateOfAddition() {
		return dateOfAddition;
	}


	public void setDateOfAddition(Date dateOfAddition) {
		this.dateOfAddition = dateOfAddition;
	}


	@Override
	public String toString() {
		return "HibExperianSropDomain [id=" + id + ", srNo=" + srNo
				+ ", soaSourceName=" + soaSourceName
				+ ", memberReferenceNumber=" + memberReferenceNumber
				+ ", enquiryDate=" + enquiryDate + ", systemcodeHeader="
				+ systemcodeHeader + ", messageTextHeader=" + messageTextHeader
				+ ", reportdate=" + reportdate + ", reportimeHeader="
				+ reportimeHeader + ", userMessageText=" + userMessageText
				+ ", enquiryUsername=" + enquiryUsername
				+ ", creditProfileReportDate=" + creditProfileReportDate
				+ ", CreditProfileReportTime=" + CreditProfileReportTime
				+ ", version=" + version + ", reportNumber=" + reportNumber
				+ ", subscriber=" + subscriber + ", subscriberName="
				+ subscriberName + ", enquiryReason=" + enquiryReason
				+ ", financePurpose=" + financePurpose + ", amountFinanced="
				+ amountFinanced + ", durationOfAgreement="
				+ durationOfAgreement + ", lastname=" + lastname
				+ ", firstname=" + firstname + ", middleName1=" + middleName1
				+ ", middleName2=" + middleName2 + ", middleName3="
				+ middleName3 + ", genderCode=" + genderCode + ", gender="
				+ gender + ", incomeTaxPanApp=" + incomeTaxPanApp
				+ ", panIssueDateApp=" + panIssueDateApp + ", panExpDateApp="
				+ panExpDateApp + ", passportNumberApp=" + passportNumberApp
				+ ", passportIssueDateApp=" + passportIssueDateApp
				+ ", passportExpDateApp=" + passportExpDateApp
				+ ", voterIdentityCardApp=" + voterIdentityCardApp
				+ ", voterIdIssueDateApp=" + voterIdIssueDateApp
				+ ", voterIdExpDateApp=" + voterIdExpDateApp
				+ ", driverLicenseNumberApp=" + driverLicenseNumberApp
				+ ", driverLicenseIssueDateApp=" + driverLicenseIssueDateApp
				+ ", driverLicenseExpDateApp=" + driverLicenseExpDateApp
				+ ", rationCardNumberApp=" + rationCardNumberApp
				+ ", rationCardIssueDateApp=" + rationCardIssueDateApp
				+ ", rationCardExpDateApp=" + rationCardExpDateApp
				+ ", universalIdNumberApp=" + universalIdNumberApp
				+ ", universalIdIssueDateApp=" + universalIdIssueDateApp
				+ ", universalIdExpDateApp=" + universalIdExpDateApp
				+ ", dateOfBirthApplicant=" + dateOfBirthApplicant
				+ ", telephoneNumberApplicant1st="
				+ telephoneNumberApplicant1st + ", teleExtensionApp="
				+ teleExtensionApp + ", teleTypeApp=" + teleTypeApp
				+ ", mobilePhoneNumber=" + mobilePhoneNumber + ", emailIdApp="
				+ emailIdApp + ", income=" + income + ", maritalStatusCode="
				+ maritalStatusCode + ", maritalStatus=" + maritalStatus
				+ ", employmentStatus=" + employmentStatus
				+ ", dateWithEmployer=" + dateWithEmployer
				+ ", numberOfMajorCreditCardHeld="
				+ numberOfMajorCreditCardHeld + ", flatNoPlotNoHouseNo="
				+ flatNoPlotNoHouseNo + ", bldgNoSocietyName="
				+ bldgNoSocietyName + ", roadNoNameAreaLocality="
				+ roadNoNameAreaLocality + ", city=" + city + ", landmark="
				+ landmark + ", state=" + state + ", pincode=" + pincode
				+ ", countryCode=" + countryCode + ", addFlatNoPlotNoHouseNo="
				+ addFlatNoPlotNoHouseNo + ", addBldgNoSocietyName="
				+ addBldgNoSocietyName + ", addRoadNoNameAreaLocality="
				+ addRoadNoNameAreaLocality + ", addCity=" + addCity
				+ ", addLandmark=" + addLandmark + ", addState=" + addState
				+ ", addPincode=" + addPincode + ", addCountrycode="
				+ addCountrycode + ", creditAccountTotal=" + creditAccountTotal
				+ ", creditAccountActive=" + creditAccountActive
				+ ", creditAccountDefault=" + creditAccountDefault
				+ ", creditAccountClosed=" + creditAccountClosed
				+ ", cadsuitfiledCurrentBalance=" + cadsuitfiledCurrentBalance
				+ ", outstandingBalanceSecured=" + outstandingBalanceSecured
				+ ", outstandingBalSecuredPer=" + outstandingBalSecuredPer
				+ ", outstandingBalanceUnsecured="
				+ outstandingBalanceUnsecured + ", outstandingBalUnsecPer="
				+ outstandingBalUnsecPer + ", outstandingBalanceAll="
				+ outstandingBalanceAll + ", identificationNumber="
				+ identificationNumber + ", caisSubscriberName="
				+ caisSubscriberName + ", accountNumber=" + accountNumber
				+ ", portfolioType=" + portfolioType + ", accountTypeCode="
				+ accountTypeCode + ", accountType=" + accountType
				+ ", openDate=" + openDate + ", highestCreditOrOrgnLoanAmt="
				+ highestCreditOrOrgnLoanAmt + ", termsDuration="
				+ termsDuration + ", termsFrequency=" + termsFrequency
				+ ", scheduledMonthlyPayamt=" + scheduledMonthlyPayamt
				+ ", accountStatusCode=" + accountStatusCode
				+ ", accountStatus=" + accountStatus + ", paymentRating="
				+ paymentRating + ", paymentHistoryProfile="
				+ paymentHistoryProfile + ", specialComment=" + specialComment
				+ ", currentBalanceTl=" + currentBalanceTl
				+ ", amountPastdueTl=" + amountPastdueTl
				+ ", originalChargeOffAmount=" + originalChargeOffAmount
				+ ", dateReported=" + dateReported
				+ ", dateOfFirstDelinquency=" + dateOfFirstDelinquency
				+ ", dateClosed=" + dateClosed + ", dateOfLastPayment="
				+ dateOfLastPayment
				+ ", suitfilledWillFullDefaultWrittenOffStatus="
				+ suitfilledWillFullDefaultWrittenOffStatus
				+ ", suitFiledWilfulDef=" + suitFiledWilfulDef
				+ ", writtenOffSettledStatus=" + writtenOffSettledStatus
				+ ", valueOfCreditsLastMonth=" + valueOfCreditsLastMonth
				+ ", occupationCode=" + occupationCode + ", sattlementAmount="
				+ sattlementAmount + ", valueOfColateral=" + valueOfColateral
				+ ", typeOfColateral=" + typeOfColateral
				+ ", writtenOffAmtTotal=" + writtenOffAmtTotal
				+ ", writtenOffAmtPrincipal=" + writtenOffAmtPrincipal
				+ ", rateOfInterest=" + rateOfInterest + ", repaymentTenure="
				+ repaymentTenure + ", promotionalRateFlag="
				+ promotionalRateFlag + ", caisIncome=" + caisIncome
				+ ", incomeIndicator=" + incomeIndicator
				+ ", incomeFrequencyIndicator=" + incomeFrequencyIndicator
				+ ", defaultStatusDate=" + defaultStatusDate
				+ ", litigationStatusDate=" + litigationStatusDate
				+ ", writeOffStatusDate=" + writeOffStatusDate
				+ ", currencyCode=" + currencyCode + ", subscriberComments="
				+ subscriberComments + ", consumerComments=" + consumerComments
				+ ", accountHolderTypeCode=" + accountHolderTypeCode
				+ ", accountHolderTypeName=" + accountHolderTypeName
				+ ", yearHist=" + yearHist + ", monthHist=" + monthHist
				+ ", daysPastDue=" + daysPastDue + ", assetClassification="
				+ assetClassification + ", yearAdvHist=" + yearAdvHist
				+ ", monthAdvHist=" + monthAdvHist + ", cashLimit=" + cashLimit
				+ ", creditLimitAmt=" + creditLimitAmt + ", actualPaymentAmt="
				+ actualPaymentAmt + ", emiAmt=" + emiAmt
				+ ", currentBalanceAdvHist=" + currentBalanceAdvHist
				+ ", amountPastDueAdvHist=" + amountPastDueAdvHist
				+ ", surNameNonNormalized=" + surNameNonNormalized
				+ ", firstNameNonNormalized=" + firstNameNonNormalized
				+ ", middleName1NonNormalized=" + middleName1NonNormalized
				+ ", middleName2NonNormalized=" + middleName2NonNormalized
				+ ", middleName3NonNormalized=" + middleName3NonNormalized
				+ ", alias=" + alias + ", caisGenderCode=" + caisGenderCode
				+ ", caisGender=" + caisGender + ", caisIncomeTaxPan="
				+ caisIncomeTaxPan + ", caisPassportNumber="
				+ caisPassportNumber + ", voterIdNumber=" + voterIdNumber
				+ ", dateOfBirth=" + dateOfBirth
				+ ", firstLineOfAddNonNormalized="
				+ firstLineOfAddNonNormalized
				+ ", secondLineOfAddNonNormalized="
				+ secondLineOfAddNonNormalized
				+ ", thirdLineOfAddNonNormalized="
				+ thirdLineOfAddNonNormalized + ", cityNonNormalized="
				+ cityNonNormalized + ", fifthLineOfAddNonNormalized="
				+ fifthLineOfAddNonNormalized + ", stateCodeNonNormalized="
				+ stateCodeNonNormalized + ", stateNonNormalized="
				+ stateNonNormalized + ", zipPostalCodeNonNormalized="
				+ zipPostalCodeNonNormalized + ", countryCodeNonNormalized="
				+ countryCodeNonNormalized + ", addIndicatorNonNormalized="
				+ addIndicatorNonNormalized + ", residenceCodeNonNormalized="
				+ residenceCodeNonNormalized + ", residenceNonNormalized="
				+ residenceNonNormalized + ", telephoneNumber="
				+ telephoneNumber + ", teleTypePhone=" + teleTypePhone
				+ ", teleExtensionPhone=" + teleExtensionPhone
				+ ", mobileTelephoneNumber=" + mobileTelephoneNumber
				+ ", faxNumber=" + faxNumber + ", emailIdPhone=" + emailIdPhone
				+ ", incomeTaxPanId=" + incomeTaxPanId + ", panIssueDateId="
				+ panIssueDateId + ", panExpDateId=" + panExpDateId
				+ ", passportNumberId=" + passportNumberId
				+ ", passportIssueDateId=" + passportIssueDateId
				+ ", passportExpDateId=" + passportExpDateId
				+ ", voterIdentityCardId=" + voterIdentityCardId
				+ ", voterIdIssueDateId=" + voterIdIssueDateId
				+ ", voterIdExpDateId=" + voterIdExpDateId
				+ ", driverLicenseNumberId=" + driverLicenseNumberId
				+ ", driverLicenseIssueDateId=" + driverLicenseIssueDateId
				+ ", driverLicenseExpDateId=" + driverLicenseExpDateId
				+ ", rationCardNumberId=" + rationCardNumberId
				+ ", rationCardIssueDateId=" + rationCardIssueDateId
				+ ", rationCardExpDateId=" + rationCardExpDateId
				+ ", universalIdNumberId=" + universalIdNumberId
				+ ", universalIdIssueDateId=" + universalIdIssueDateId
				+ ", universalIdExpDateId=" + universalIdExpDateId
				+ ", emailidId=" + emailidId + ", exactMatch=" + exactMatch
				+ ", capsLast7Days=" + capsLast7Days + ", capsLast30Days="
				+ capsLast30Days + ", capsLast90Days=" + capsLast90Days
				+ ", capsLast180Days=" + capsLast180Days
				+ ", capsSubscriberCode=" + capsSubscriberCode
				+ ", capsSubscriberName=" + capsSubscriberName
				+ ", capsdateofRequest=" + capsdateofRequest
				+ ", capsReportDate=" + capsReportDate + ", capsReportTime="
				+ capsReportTime + ", capsReportNumber=" + capsReportNumber
				+ ", enquiryReasonCode=" + enquiryReasonCode
				+ ", capsEnquiryReason=" + capsEnquiryReason
				+ ", financePurposeCode=" + financePurposeCode
				+ ", capsFinancePurpose=" + capsFinancePurpose
				+ ", capsAmountFinanced=" + capsAmountFinanced
				+ ", capsDurationOfAgreement=" + capsDurationOfAgreement
				+ ", capsApplicantLastName=" + capsApplicantLastName
				+ ", capsApplicantFirstName=" + capsApplicantFirstName
				+ ", capsApplicantMiddleName1=" + capsApplicantMiddleName1
				+ ", capsApplicantMiddleName2=" + capsApplicantMiddleName2
				+ ", capsApplicantMiddleName3=" + capsApplicantMiddleName3
				+ ", capsApplicantgenderCode=" + capsApplicantgenderCode
				+ ", capsApplicantGender=" + capsApplicantGender
				+ ", capsIncomeTaxPan=" + capsIncomeTaxPan
				+ ", capsPanIssueDate=" + capsPanIssueDate
				+ ", capsPanExpDate=" + capsPanExpDate
				+ ", capsPassportNumber=" + capsPassportNumber
				+ ", capsPassportIssueDate=" + capsPassportIssueDate
				+ ", capsPassportExpDate=" + capsPassportExpDate
				+ ", capsVoterIdentityCard=" + capsVoterIdentityCard
				+ ", capsVoterIdIssueDate=" + capsVoterIdIssueDate
				+ ", capsVoterIdExpDate=" + capsVoterIdExpDate
				+ ", capsDriverLicenseNumber=" + capsDriverLicenseNumber
				+ ", capsDriverLicenseIssueDate=" + capsDriverLicenseIssueDate
				+ ", capsDriverLicenseExpDate=" + capsDriverLicenseExpDate
				+ ", capsRationCardNumber=" + capsRationCardNumber
				+ ", capsRationCardIssueDate=" + capsRationCardIssueDate
				+ ", capsRationCardExpDate=" + capsRationCardExpDate
				+ ", capsUniversalIdNumber=" + capsUniversalIdNumber
				+ ", capsUniversalIdIssueDate=" + capsUniversalIdIssueDate
				+ ", capsUniversalIdExpDate=" + capsUniversalIdExpDate
				+ ", capsApplicantDobApplicant=" + capsApplicantDobApplicant
				+ ", capsApplicantTelNoApplicant1st="
				+ capsApplicantTelNoApplicant1st + ", capsApplicantTelExt="
				+ capsApplicantTelExt + ", capsApplicantTelType="
				+ capsApplicantTelType + ", capsApplicantMobilePhoneNumber="
				+ capsApplicantMobilePhoneNumber + ", capsApplicantEmailId="
				+ capsApplicantEmailId + ", capsApplicantIncome="
				+ capsApplicantIncome + ", applicantMaritalStatusCode="
				+ applicantMaritalStatusCode + ", capsApplicantMaritalStatus="
				+ capsApplicantMaritalStatus
				+ ", capsApplicantEmploymentStatusCode="
				+ capsApplicantEmploymentStatusCode
				+ ", capsApplicantEmploymentStatus="
				+ capsApplicantEmploymentStatus
				+ ", capsApplicantDateWithEmployer="
				+ capsApplicantDateWithEmployer
				+ ", capsApplicantNoMajorCrditCardHeld="
				+ capsApplicantNoMajorCrditCardHeld
				+ ", capsApplicantFlatNoPlotNoHouseNo="
				+ capsApplicantFlatNoPlotNoHouseNo
				+ ", capsApplicantBldgNoSocietyName="
				+ capsApplicantBldgNoSocietyName
				+ ", capsApplicantRoadNoNameAreaLocality="
				+ capsApplicantRoadNoNameAreaLocality + ", capsApplicantCity="
				+ capsApplicantCity + ", capsApplicantLandmark="
				+ capsApplicantLandmark + ", capsApplicantStateCode="
				+ capsApplicantStateCode + ", capsApplicantState="
				+ capsApplicantState + ", capsApplicantPinCode="
				+ capsApplicantPinCode + ", capsApplicantCountryCode="
				+ capsApplicantCountryCode + ", bureauScore=" + bureauScore
				+ ", bureauScoreConfidLevel=" + bureauScoreConfidLevel
				+ ", creditRating=" + creditRating + ", tnOfBFHLCADExhl="
				+ tnOfBFHLCADExhl + ", totValOfBFHLCAD=" + totValOfBFHLCAD
				+ ", MNTSMRBFHLCAD=" + MNTSMRBFHLCAD + ", tnOfHLCAD="
				+ tnOfHLCAD + ", totValOfHLCAD=" + totValOfHLCAD
				+ ", mntsmrHLCAD=" + mntsmrHLCAD + ", tnOfTelcosCAD="
				+ tnOfTelcosCAD + ", totValOfTelcosCad=" + totValOfTelcosCad
				+ ", mntsmrTelcosCad=" + mntsmrTelcosCad + ", tnOfmfCAD="
				+ tnOfmfCAD + ", totValOfmfCAD=" + totValOfmfCAD
				+ ", mntsmrmfCAD=" + mntsmrmfCAD + ", tnOfRetailCAD="
				+ tnOfRetailCAD + ", totValOfRetailCAD=" + totValOfRetailCAD
				+ ", mntsmrRetailCAD=" + mntsmrRetailCAD + ", tnOfAllCAD="
				+ tnOfAllCAD + ", totValOfAllCAD=" + totValOfAllCAD
				+ ", mntsmrCADAll=" + mntsmrCADAll + ", tnOfBFHLACAExhl="
				+ tnOfBFHLACAExhl + ", balBFHLACAExhl=" + balBFHLACAExhl
				+ ", wcdstBFHLACAExhl=" + wcdstBFHLACAExhl
				+ ", wdspr6MntBFHLACAExhl=" + wdspr6MntBFHLACAExhl
				+ ", wdspr712MntBFHLACAExhl=" + wdspr712MntBFHLACAExhl
				+ ", ageOfOldestBFHLACAExhl=" + ageOfOldestBFHLACAExhl
				+ ", hcbperrevaccBFHLACAExhl=" + hcbperrevaccBFHLACAExhl
				+ ", tcbperrevaccBFHLACAExhl=" + tcbperrevaccBFHLACAExhl
				+ ", tnOfHlACA=" + tnOfHlACA + ", balHlACA=" + balHlACA
				+ ", wcdstHlACA=" + wcdstHlACA + ", wdspr6MnthlACA="
				+ wdspr6MnthlACA + ", wdspr712mnthlACA=" + wdspr712mnthlACA
				+ ", ageOfOldesthlACA=" + ageOfOldesthlACA + ", tnOfMfACA="
				+ tnOfMfACA + ", totalBalMfACA=" + totalBalMfACA
				+ ", wcdstMfACA=" + wcdstMfACA + ", wdspr6MntMfACA="
				+ wdspr6MntMfACA + ", wdspr712mntMfACA=" + wdspr712mntMfACA
				+ ", ageOfOldestMfACA=" + ageOfOldestMfACA + ", tnOfTelcosACA="
				+ tnOfTelcosACA + ", totalBalTelcosACA=" + totalBalTelcosACA
				+ ", wcdstTelcosACA=" + wcdstTelcosACA
				+ ", wdspr6mntTelcosACA=" + wdspr6mntTelcosACA
				+ ", wdspr712mntTelcosACA=" + wdspr712mntTelcosACA
				+ ", ageOfOldestTelcosACA=" + ageOfOldestTelcosACA
				+ ", tnOfRetailACA=" + tnOfRetailACA + ", totalBalRetailACA="
				+ totalBalRetailACA + ", wcdstRetailACA=" + wcdstRetailACA
				+ ", wdspr6mntRetailACA=" + wdspr6mntRetailACA
				+ ", wdspr712mntRetailACA=" + wdspr712mntRetailACA
				+ ", ageOfOldestRetailACA=" + ageOfOldestRetailACA
				+ ", hcblmperrevaccret=" + hcblmperrevaccret
				+ ", totCurBallmperrevaccret=" + totCurBallmperrevaccret
				+ ", tnOfallACA=" + tnOfallACA + ", balAllACAExhl="
				+ balAllACAExhl + ", wcdstAllACA=" + wcdstAllACA
				+ ", wdspr6mntallACA=" + wdspr6mntallACA
				+ ", wdspr712mntAllACA=" + wdspr712mntAllACA
				+ ", ageOfOldestAllACA=" + ageOfOldestAllACA
				+ ", tnOfndelBFHLinACAExhl=" + tnOfndelBFHLinACAExhl
				+ ", tnOfDelBFHLInACAExhl=" + tnOfDelBFHLInACAExhl
				+ ", tnOfnDelHLInACA=" + tnOfnDelHLInACA + ", tnOfDelhlInACA="
				+ tnOfDelhlInACA + ", tnOfnDelmfinACA=" + tnOfnDelmfinACA
				+ ", tnOfDelmfinACA=" + tnOfDelmfinACA
				+ ", tnOfnDelTelcosInACA=" + tnOfnDelTelcosInACA
				+ ", tnOfdelTelcosInACA=" + tnOfdelTelcosInACA
				+ ", tnOfndelRetailInACA=" + tnOfndelRetailInACA
				+ ", tnOfDelRetailInACA=" + tnOfDelRetailInACA
				+ ", BFHLCapsLast90Days=" + BFHLCapsLast90Days
				+ ", mfCapsLast90Days=" + mfCapsLast90Days
				+ ", telcosCapsLast90Days=" + telcosCapsLast90Days
				+ ", retailCapsLast90Days=" + retailCapsLast90Days
				+ ", tnOfocomcad=" + tnOfocomcad + ", totValOfocomCAD="
				+ totValOfocomCAD + ", mntsmrocomCAD=" + mntsmrocomCAD
				+ ", tnOfocomACA=" + tnOfocomACA + ", balocomACAExhl="
				+ balocomACAExhl + ", balOcomacahlonly=" + balOcomacahlonly
				+ ", wcdstocomACA=" + wcdstocomACA + ", hcblmperrevocomACA="
				+ hcblmperrevocomACA + ", tnOfnDelocominACA="
				+ tnOfnDelocominACA + ", tnOfDelocominACA=" + tnOfDelocominACA
				+ ", tnOfocomCapsLast90Days=" + tnOfocomCapsLast90Days
				+ ", anyRelcbdatadisyn=" + anyRelcbdatadisyn
				+ ", othrelcbdfcposmatyn=" + othrelcbdfcposmatyn
				+ ", tnOfCADclassedassfwdwo=" + tnOfCADclassedassfwdwo
				+ ", mntsmrcadclassedassfwdwo=" + mntsmrcadclassedassfwdwo
				+ ", numOfCadsfwdwolast24mnt=" + numOfCadsfwdwolast24mnt
				+ ", totCurBalLivesAcc=" + totCurBalLivesAcc
				+ ", totCurBalLiveuAcc=" + totCurBalLiveuAcc
				+ ", totCurBalMaxBalLivesAcc=" + totCurBalMaxBalLivesAcc
				+ ", totCurBalMaxBalLiveuAcc=" + totCurBalMaxBalLiveuAcc
				+ ", outputWriteFlag=" + outputWriteFlag + ", outputWriteDate="
				+ outputWriteDate + ", outputReadDate=" + outputReadDate
				+ ", accountKey=" + accountKey + ", dateOfAddition="
				+ dateOfAddition + "]";
	}


	
}
