package com.softcell.gonogo.model.mbdatapush.chm;


public class PersonalInfoVariation {
	private NameVariations nameVariations;
	private AddressVariations addressVariations;
	private PanVariations panVariations;
	private DateOfBirthVariations dateOfBirthVariations;
	private VoterIdVariations voterIdVariations;
	private PhoneNumberVariations phoneNumberVariations;
	private RationCardVariation rationCardVariation;
	private PassportVariation passportVariation;
	private EmailVariation emailVariation;
	private DrivingLicenseVariations drivingLicenseVariations;
	public NameVariations getNameVariations() {
		return nameVariations;
	}
	public void setNameVariations(NameVariations nameVariations) {
		this.nameVariations = nameVariations;
	}
	public AddressVariations getAddressVariations() {
		return addressVariations;
	}
	public void setAddressVariations(AddressVariations addressVariations) {
		this.addressVariations = addressVariations;
	}
	public PanVariations getPanVariations() {
		return panVariations;
	}
	public void setPanVariations(PanVariations panVariations) {
		this.panVariations = panVariations;
	}
	public DateOfBirthVariations getDateOfBirthVariations() {
		return dateOfBirthVariations;
	}
	public void setDateOfBirthVariations(DateOfBirthVariations dateOfBirthVariations) {
		this.dateOfBirthVariations = dateOfBirthVariations;
	}
	public VoterIdVariations getVoterIdVariations() {
		return voterIdVariations;
	}
	public void setVoterIdVariations(VoterIdVariations voterIdVariations) {
		this.voterIdVariations = voterIdVariations;
	}
	public PhoneNumberVariations getPhoneNumberVariations() {
		return phoneNumberVariations;
	}
	public void setPhoneNumberVariations(PhoneNumberVariations phoneNumberVariations) {
		this.phoneNumberVariations = phoneNumberVariations;
	}
	public RationCardVariation getRationCardVariation() {
		return rationCardVariation;
	}
	public void setRationCardVariation(RationCardVariation rationCardVariation) {
		this.rationCardVariation = rationCardVariation;
	}
	public PassportVariation getPassportVariation() {
		return passportVariation;
	}
	public void setPassportVariation(PassportVariation passportVariation) {
		this.passportVariation = passportVariation;
	}
	public EmailVariation getEmailVariation() {
		return emailVariation;
	}
	public void setEmailVariation(EmailVariation emailVariation) {
		this.emailVariation = emailVariation;
	}
	public DrivingLicenseVariations getDrivingLicenseVariations() {
		return drivingLicenseVariations;
	}
	public void setDrivingLicenseVariations(
			DrivingLicenseVariations drivingLicenseVariations) {
		this.drivingLicenseVariations = drivingLicenseVariations;
	}
	@Override
	public String toString() {
		return "PersonalInfoVariation [nameVariations=" + nameVariations
				+ ", addressVariations=" + addressVariations
				+ ", panVariations=" + panVariations
				+ ", dateOfBirthVariations=" + dateOfBirthVariations
				+ ", voterIdVariations=" + voterIdVariations
				+ ", phoneNumberVariations=" + phoneNumberVariations
				+ ", rationCardVariation=" + rationCardVariation
				+ ", passportVariation=" + passportVariation
				+ ", emailVariation=" + emailVariation
				+ ", drivingLicenseVariations=" + drivingLicenseVariations
				+ "]";
	}
}
