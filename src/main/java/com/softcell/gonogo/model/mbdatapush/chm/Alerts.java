package com.softcell.gonogo.model.mbdatapush.chm;

public class Alerts {
	
	private Alert alert;

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	@Override
	public String toString() {
		return "Alerts [alert=" + alert + "]";
	}
}
