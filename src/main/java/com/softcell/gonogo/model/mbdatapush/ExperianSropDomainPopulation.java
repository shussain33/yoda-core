package com.softcell.gonogo.model.mbdatapush;

import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.multibureau.experian.*;
import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ExperianSropDomainPopulation {
	
	private ExperianUtils experianUtils ;
	
	public ExperianSropDomainPopulation() {
		this.experianUtils = new ExperianUtils();
	}
	
	
	
	public List<HibExperianSropDomain> generateSropDomain(InProfileResponse inProfileResponse ,String srNo,
			String soaSourceName, String memberReferenceNumber, Date pickupDate
			) throws Exception{
		try {
			return generateSropDomain_(inProfileResponse , srNo , soaSourceName , memberReferenceNumber,pickupDate );
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(""+e.getMessage());
		}
	}

	
//	public static void main(String[] args) {
//		ExperianSropDomainPopulation experianSropDomainPopulation = new ExperianSropDomainPopulation();
//		ExperianResponseParserEngine test = new ExperianResponseParserEngine();
//		try {
//			InProfileResponse experianParsedData = test.ExperianParsedData(FileUtils.readFileToString(new File("C:\\Users\\Dev 2\\Desktop\\exp_6.txt")));
//			System.out.println(new Gson().toJson(experianParsedData));
//			List<HibExperianSropDomain> generateSropDomain = experianSropDomainPopulation.generateSropDomain(experianParsedData,"10111","SOA_SOURCE_NAME","M_REF_NO",new Date());
//			ExperianSropDomainObject domainObj = new ExperianSropDomainObject();
//			domainObj.setExperianSropDomainList(generateSropDomain);
//			GsonBuilder builder = new GsonBuilder();
//			builder.excludeFieldsWithoutExposeAnnotation();
//			String clientResponse = builder.create().toJson(domainObj);
//			System.out.println(clientResponse);
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//		
//		
//	}
	
	private List<HibExperianSropDomain> generateSropDomain_(InProfileResponse inProfileResponse ,String srNo,String soaSourceName, String memberReferenceNumber, Date pickupDate) throws Exception {
		
		try{
			
			List<HibExperianSropDomain>  experianSROPDomains = new LinkedList<HibExperianSropDomain>();
			
			Integer srNumber = null;
			if(StringUtils.isNotBlank(srNo) && StringUtils.isNumeric(srNo)){
				try{
					srNumber = Integer.parseInt(srNo);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			HibExperianSropDomain experianSROPDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber,pickupDate);
			experianSROPDomains.add(experianSROPDomain);
			
			if(null != inProfileResponse){
				
				
				Header header = inProfileResponse.getHeader();
				if(null != header){
					experianSROPDomains.get(0).setSystemcodeHeader(header.getSystemCode());
					experianSROPDomains.get(0).setMessageTextHeader(header.getMessageText());
					experianSROPDomains.get(0).setReportdate(ExperianSropDomainPopulation.parseDate(header.getReportDate()));
					experianSROPDomains.get(0).setReportimeHeader(header.getReportTime());
					experianSROPDomains.get(0).setOutputWriteDate(DateUtils.getFormattedDate(new Date(), "ddMMyyyy HH:mm:ss"));
					experianSROPDomains.get(0).setOutputWriteFlag("0");
				}
				
				UserMessage userMessage = inProfileResponse.getUserMessage();
				if(null != userMessage){
					experianSROPDomains.get(0).setUserMessageText(userMessage.getUserMessageText());
				}
				
				CreditProfileHeader creditProfileHeader = inProfileResponse.getCreditProfileHeader();
				if(null != creditProfileHeader){
					experianSROPDomains.get(0).setEnquiryUsername(creditProfileHeader.getEnquiryUserName());
					experianSROPDomains.get(0).setCreditProfileReportDate(ExperianSropDomainPopulation.parseDate(creditProfileHeader.getReportDate()));
					experianSROPDomains.get(0).setCreditProfileReportTime(creditProfileHeader.getReporttime());
					experianSROPDomains.get(0).setVersion(creditProfileHeader.getVersion());
					experianSROPDomains.get(0).setReportNumber(creditProfileHeader.getReportNumber());
					experianSROPDomains.get(0).setSubscriber(creditProfileHeader.getSubscriber());
					experianSROPDomains.get(0).setSubscriberName(creditProfileHeader.getSubscriberName());
				}
				
				CurrentApplication currentApplication = inProfileResponse.getCurrentApplication();
				if(null != currentApplication){
					
					CurrentApplicationDetails currentApplicationDetails_ = currentApplication.getCurrentApplicationDetails();
					if(null != currentApplicationDetails_){
						experianSROPDomains.get(0).setEnquiryReason(currentApplicationDetails_.getEnquiryReason());
						experianSROPDomains.get(0).setFinancePurpose(currentApplicationDetails_.getFinancePurpose());
						experianSROPDomains.get(0).setAmountFinanced(ExperianSropDomainPopulation.parseInteger(currentApplicationDetails_.getAmountFinanced()));
						experianSROPDomains.get(0).setDurationOfAgreement(ExperianSropDomainPopulation.parseInteger(currentApplicationDetails_.getDurationOfAgreement()));
						
						CurrentApplicantdetails currentApplicantdetails_ = currentApplicationDetails_.getCurrentApplicantdetails();
						if(null != currentApplicantdetails_){
							experianSROPDomains.get(0).setLastname(currentApplicantdetails_.getLastName());
							experianSROPDomains.get(0).setFirstname(currentApplicantdetails_.getFirstName());
							experianSROPDomains.get(0).setMiddleName1(currentApplicantdetails_.getMiddleName1());
							experianSROPDomains.get(0).setMiddleName2(currentApplicantdetails_.getMiddleName2());
							experianSROPDomains.get(0).setMiddleName3(currentApplicantdetails_.getMiddleName3());
							experianSROPDomains.get(0).setGenderCode(currentApplicantdetails_.getGenderCode());
							experianSROPDomains.get(0).setGender(experianUtils.getGender(currentApplicantdetails_.getGenderCode())); 
							experianSROPDomains.get(0).setIncomeTaxPanApp(currentApplicantdetails_.getIncometaxPan());
							experianSROPDomains.get(0).setPanIssueDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getPanIssueDate()));
							experianSROPDomains.get(0).setPanExpDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getPanExpirationDate()));
							experianSROPDomains.get(0).setPassportNumberApp(currentApplicantdetails_.getPassportNumber());
							experianSROPDomains.get(0).setPassportIssueDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getPassportIssueDate()));
							experianSROPDomains.get(0).setPassportExpDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getPassportExpirationDate()));
							experianSROPDomains.get(0).setVoterIdentityCardApp(currentApplicantdetails_.getVotersIdentityCard());
							experianSROPDomains.get(0).setVoterIdIssueDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getVoterIdIssueDate()));
							experianSROPDomains.get(0).setVoterIdExpDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getVoterIdExpirationdate()));
							experianSROPDomains.get(0).setDriverLicenseNumberApp(currentApplicantdetails_.getDriverLicenceNumber());
							experianSROPDomains.get(0).setDriverLicenseIssueDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getDriverLicenceIssueDate()));
							experianSROPDomains.get(0).setDriverLicenseExpDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getDriverLicenceExpirationdate()));
							experianSROPDomains.get(0).setRationCardNumberApp(currentApplicantdetails_.getRationCardNumber());
							experianSROPDomains.get(0).setRationCardIssueDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getRationCardIssueDate()));
							experianSROPDomains.get(0).setRationCardExpDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getRationCardExpirationDate()));
							experianSROPDomains.get(0).setUniversalIdNumberApp(currentApplicantdetails_.getUniversalIdNumber());
							experianSROPDomains.get(0).setUniversalIdIssueDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getUniversalIdIssueDate()));
							experianSROPDomains.get(0).setUniversalIdExpDateApp(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getUniversalIdExpirationDate()));
							experianSROPDomains.get(0).setDateOfBirthApplicant(ExperianSropDomainPopulation.parseDate(currentApplicantdetails_.getDateofBirthApplicant()));
							experianSROPDomains.get(0).setTelephoneNumberApplicant1st(currentApplicantdetails_.getTelephoneNumberApplicant1st());	
							experianSROPDomains.get(0).setTeleExtensionApp(currentApplicantdetails_.getTelephoneExtension());
							experianSROPDomains.get(0).setTeleTypeApp(currentApplicantdetails_.getTelephoneType());
							experianSROPDomains.get(0).setMobilePhoneNumber(currentApplicantdetails_.getMobilePhoneNumber());
							experianSROPDomains.get(0).setEmailIdApp(currentApplicantdetails_.getEmailid());
						}
						
						CurrentOtherDetails currentOtherDetails_ = currentApplicationDetails_.getCurrentOtherDetails();
						if(null != currentOtherDetails_){
							experianSROPDomains.get(0).setIncome(currentOtherDetails_.getIncome());
							experianSROPDomains.get(0).setMaritalStatusCode(currentOtherDetails_.getMaritialStatus());
							experianSROPDomains.get(0).setMaritalStatus(experianUtils.getMaritalStatus(currentOtherDetails_.getMaritialStatus()));
							experianSROPDomains.get(0).setEmploymentStatus(currentOtherDetails_.getEmploymentStatus());
							experianSROPDomains.get(0).setDateWithEmployer(ExperianSropDomainPopulation.parseInteger(currentOtherDetails_.getTimeWithEmployer()));
							experianSROPDomains.get(0).setNumberOfMajorCreditCardHeld(currentOtherDetails_.getNumberofMajorcreditCardHeld());
							
						}
						
						CurrentApplicantAddressDetails currentApplicantAddressDetails_ = currentApplicationDetails_.getCurrentApplicantAddressDetails();
						if(null != currentApplicantAddressDetails_){
							experianSROPDomains.get(0).setFlatNoPlotNoHouseNo(currentApplicantAddressDetails_.getFlatNoPlotNoHouseNo());
							experianSROPDomains.get(0).setBldgNoSocietyName(currentApplicantAddressDetails_.getBldgNumberSocietyName());
							experianSROPDomains.get(0).setRoadNoNameAreaLocality(currentApplicantAddressDetails_.getRoadNumbernameAreaLocality());
							experianSROPDomains.get(0).setCity(currentApplicantAddressDetails_.getCity());
							experianSROPDomains.get(0).setLandmark(currentApplicantAddressDetails_.getLandmark());
							experianSROPDomains.get(0).setState(currentApplicantAddressDetails_.getState());
							experianSROPDomains.get(0).setPincode(currentApplicantAddressDetails_.getPinCode());
							experianSROPDomains.get(0).setCountryCode(currentApplicantAddressDetails_.getCountryCode());
						}
						
						CurrentApplicantAditionalAddressDetails currentApplicantAditionalAddressDetails_ = currentApplicationDetails_.getCurrentApplicantAditionalAddressDetails();
						if(null != currentApplicantAditionalAddressDetails_){
							experianSROPDomains.get(0).setAddFlatNoPlotNoHouseNo(currentApplicantAditionalAddressDetails_.getFlatNoPlotNoHouseNo());
							experianSROPDomains.get(0).setAddBldgNoSocietyName(currentApplicantAditionalAddressDetails_.getBldgNumberSocietyName());
							experianSROPDomains.get(0).setAddRoadNoNameAreaLocality(currentApplicantAditionalAddressDetails_.getRoadNumbernameAreaLocality());
							experianSROPDomains.get(0).setAddCity(currentApplicantAditionalAddressDetails_.getCity());
							experianSROPDomains.get(0).setAddLandmark(currentApplicantAditionalAddressDetails_.getLandmark());
							experianSROPDomains.get(0).setAddState(currentApplicantAditionalAddressDetails_.getState());
							experianSROPDomains.get(0).setAddPincode(currentApplicantAditionalAddressDetails_.getPinCode());
							experianSROPDomains.get(0).setAddCountrycode(currentApplicantAditionalAddressDetails_.getCountryCode());
						}
					}
					
				}
				
				
				CAISAccount caisAccount = inProfileResponse.getCaisAccount();
				if(null != caisAccount){
					
					CAISSummary caisSummary = caisAccount.getCaisSummary();
					if(null != caisSummary){
						
						CreditAccount creditAccount_ = caisSummary.getCreditAccount();
						if(null !=creditAccount_){
							experianSROPDomains.get(0).setCreditAccountTotal(creditAccount_.getCreditAccountTotal());
							experianSROPDomains.get(0).setCreditAccountActive(ExperianSropDomainPopulation.parseInteger(creditAccount_.getCreditAccountActive()));
							experianSROPDomains.get(0).setCreditAccountDefault(ExperianSropDomainPopulation.parseInteger(creditAccount_.getCreditAccountDefault()));
							experianSROPDomains.get(0).setCreditAccountClosed(ExperianSropDomainPopulation.parseInteger(creditAccount_.getCreditAccountClosed()));
							experianSROPDomains.get(0).setCadsuitfiledCurrentBalance(ExperianSropDomainPopulation.parseInteger(creditAccount_.getCADSuitFiledCurrentBalance()));
							
						}
						
						TotalOutStandingBalance totalOutStandingBalance_ = caisSummary.getTotalOutStandingBalance();
						if(null != totalOutStandingBalance_){
							experianSROPDomains.get(0).setOutstandingBalanceSecured(totalOutStandingBalance_.getOutstandingBalanceSecured());
							experianSROPDomains.get(0).setOutstandingBalSecuredPer(totalOutStandingBalance_.getOutstandingBalanceSecuredPercentage());
							experianSROPDomains.get(0).setOutstandingBalanceUnsecured(totalOutStandingBalance_.getOutstandingBalanceUnsecured());
							experianSROPDomains.get(0).setOutstandingBalUnsecPer(totalOutStandingBalance_.getOutstandingBalanceUnsecuredPercentage());
							experianSROPDomains.get(0).setOutstandingBalanceAll(totalOutStandingBalance_.getOutstandingBalanceAll());
						}
					}
					
					Integer counter =0;
					Integer actLevelCntr = -1;
					Integer accountKey = 1;
					
					List<CAISAccountDetails> caisAccountDetails = caisAccount.getCaisAccountDetails();
					if(null != caisAccountDetails  && caisAccountDetails.size() > 0){
						for(Integer i=0;i<caisAccountDetails.size();i++){
							actLevelCntr++;
							if (actLevelCntr > counter) {
								HibExperianSropDomain newDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber,pickupDate);
								experianSROPDomains.add(newDomain);
								counter ++;
							}
							String accountNumber = "ACCT"+accountKey+"-"+StringUtils.trim(caisAccountDetails.get(i).getAccountNumber());
							experianSROPDomains.get(actLevelCntr).setAccountKey(accountKey);
							experianSROPDomains.get(actLevelCntr).setIdentificationNumber(caisAccountDetails.get(i).getIdentificationNumber());
							experianSROPDomains.get(actLevelCntr).setCaisSubscriberName(caisAccountDetails.get(i).getSubscriberName());
							experianSROPDomains.get(actLevelCntr).setAccountNumber(accountNumber);
							experianSROPDomains.get(actLevelCntr).setPortfolioType(caisAccountDetails.get(i).getPortfolioType());
							experianSROPDomains.get(actLevelCntr).setAccountTypeCode(caisAccountDetails.get(i).getAccountType());
							experianSROPDomains.get(actLevelCntr).setAccountType(experianUtils.getAccountType(caisAccountDetails.get(i).getAccountType()));
							experianSROPDomains.get(actLevelCntr).setOpenDate(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getOpenDate()));
							experianSROPDomains.get(actLevelCntr).setHighestCreditOrOrgnLoanAmt(caisAccountDetails.get(i).getHighestCreditOrOrignalLoanAmount());
							experianSROPDomains.get(actLevelCntr).setTermsDuration(caisAccountDetails.get(i).getTermsDuration());
							experianSROPDomains.get(actLevelCntr).setTermsFrequency(caisAccountDetails.get(i).getTermsFrequency());
							experianSROPDomains.get(actLevelCntr).setScheduledMonthlyPayamt(caisAccountDetails.get(i).getScheduledMonthlyPaymentAmount());
							experianSROPDomains.get(actLevelCntr).setAccountStatusCode(caisAccountDetails.get(i).getAccountStatus());
							experianSROPDomains.get(actLevelCntr).setAccountStatus(experianUtils.getAccountStatus(caisAccountDetails.get(i).getAccountStatus()));
							experianSROPDomains.get(actLevelCntr).setPaymentRating(caisAccountDetails.get(i).getPaymentRating());
							experianSROPDomains.get(actLevelCntr).setPaymentHistoryProfile(caisAccountDetails.get(i).getPaymentHistoryProfile());
							experianSROPDomains.get(actLevelCntr).setSpecialComment(caisAccountDetails.get(i).getSpecialComment());
							experianSROPDomains.get(actLevelCntr).setCurrentBalanceTl(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getCurrentBalance()));
							experianSROPDomains.get(actLevelCntr).setAmountPastdueTl(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getAmountPastDue()));
							experianSROPDomains.get(actLevelCntr).setOriginalChargeOffAmount(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getOriginalChargeOffAmount()));
							experianSROPDomains.get(actLevelCntr).setDateReported(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getDateReported()));
							experianSROPDomains.get(actLevelCntr).setDateOfFirstDelinquency(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getDateOfFirstDeleinquency()));
							experianSROPDomains.get(actLevelCntr).setDateClosed(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getDateClosed()));
							experianSROPDomains.get(actLevelCntr).setDateOfLastPayment(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getDateOfLastPayment()));
							experianSROPDomains.get(actLevelCntr).setSuitfilledWillFullDefaultWrittenOffStatus(caisAccountDetails.get(i).getSuitFiledWillfulDefaultWrittenOffStatus());
							experianSROPDomains.get(actLevelCntr).setSuitFiledWilfulDef(caisAccountDetails.get(i).getSuitFiledWilfulDefault());
							experianSROPDomains.get(actLevelCntr).setWrittenOffSettledStatus(caisAccountDetails.get(i).getWrittenOffSettledStatus());
							experianSROPDomains.get(actLevelCntr).setValueOfCreditsLastMonth(caisAccountDetails.get(i).getValueOfCreditsLastMonth());
							experianSROPDomains.get(actLevelCntr).setOccupationCode(caisAccountDetails.get(i).getOccupationCode());
							experianSROPDomains.get(actLevelCntr).setSattlementAmount(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getSettelmentAmount()));
							experianSROPDomains.get(actLevelCntr).setValueOfColateral(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getValueOfCollateral()));
							experianSROPDomains.get(actLevelCntr).setTypeOfColateral(caisAccountDetails.get(i).getTypeOfCollateral());
							experianSROPDomains.get(actLevelCntr).setWrittenOffAmtTotal(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getWrittenOffAmountTotal()));
							experianSROPDomains.get(actLevelCntr).setWrittenOffAmtPrincipal(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getWrittenOffAmountPrincipal()));
							experianSROPDomains.get(actLevelCntr).setRateOfInterest(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getRateOfInterest()));
							experianSROPDomains.get(actLevelCntr).setRepaymentTenure(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getRepaymentTenure()));
							experianSROPDomains.get(actLevelCntr).setPromotionalRateFlag(caisAccountDetails.get(i).getPramotionalRateFlag());
							experianSROPDomains.get(actLevelCntr).setCaisIncome(ExperianSropDomainPopulation.parseInteger(caisAccountDetails.get(i).getIncome()));
							experianSROPDomains.get(actLevelCntr).setIncomeIndicator(caisAccountDetails.get(i).getIncomeIndicator());
							experianSROPDomains.get(actLevelCntr).setIncomeFrequencyIndicator(caisAccountDetails.get(i).getIncomeFrequencyIndicator());
							experianSROPDomains.get(actLevelCntr).setDefaultStatusDate(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getDefaultStatusDate()));
							experianSROPDomains.get(actLevelCntr).setLitigationStatusDate(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getLitigationStatusDate()));
							experianSROPDomains.get(actLevelCntr).setWriteOffStatusDate(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getWriteOffStatusDate()));
							experianSROPDomains.get(actLevelCntr).setCurrencyCode(caisAccountDetails.get(i).getCurrencyCode());
							experianSROPDomains.get(actLevelCntr).setSubscriberComments(caisAccountDetails.get(i).getSubscriberComments());
							experianSROPDomains.get(actLevelCntr).setConsumerComments(caisAccountDetails.get(i).getConsumerComments());
							experianSROPDomains.get(actLevelCntr).setAccountHolderTypeCode(caisAccountDetails.get(i).getAccountHoldertypecode());
							experianSROPDomains.get(actLevelCntr).setAccountHolderTypeName(experianUtils.getAccountHolderType(caisAccountDetails.get(i).getAccountHoldertypecode()));
							experianSROPDomains.get(actLevelCntr).setDateOfAddition(ExperianSropDomainPopulation.parseDate(caisAccountDetails.get(i).getDateOfAddition()));
							
							Integer actHistCntr = actLevelCntr-1;
							
							//Account History
							List<CAISAccountHistory> caisAccountHistoryList = caisAccountDetails.get(i).getCaisAccountHistoryList();
							if (null != caisAccountHistoryList  && caisAccountHistoryList.size() > 0){
								for (Integer j=0; j<caisAccountHistoryList.size(); j++) {
									actHistCntr++;
									
									if (actHistCntr > counter) {
										HibExperianSropDomain newDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber, accountNumber,pickupDate);
										experianSROPDomains.add(newDomain);
										counter ++;
									}
									
									//Add all account history fields here.
									experianSROPDomains.get(actHistCntr).setAccountKey(ExperianSropDomainPopulation.parseInteger(accountKey.toString()));
									experianSROPDomains.get(actHistCntr).setYearHist(caisAccountHistoryList.get(j).getYear()); 
									experianSROPDomains.get(actHistCntr).setMonthHist(caisAccountHistoryList.get(j).getMonth());
									experianSROPDomains.get(actHistCntr).setDaysPastDue(caisAccountHistoryList.get(j).getDaysPastDue());
									experianSROPDomains.get(actHistCntr).setAssetClassification(experianUtils.getAssetClassificationStatus(caisAccountHistoryList.get(j).getAssetClassification()));
								}
							}
							
							Integer advActHistCntr = actLevelCntr-1;
							//Advanced Account History
							List<AdvancedAccountHistory> advancedAccountHistoryList = caisAccountDetails.get(i).getAdvancedAccountHistoryList();
							if (null != advancedAccountHistoryList  && advancedAccountHistoryList.size() > 0){
								for (Integer j=0; j<advancedAccountHistoryList.size(); j++) {
									advActHistCntr++;
									
									if (advActHistCntr > counter) {
										HibExperianSropDomain newDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber, accountNumber,pickupDate);
										experianSROPDomains.add(newDomain);
										counter ++;
									}
									//Add all account history fields here.
									experianSROPDomains.get(advActHistCntr).setAccountKey(ExperianSropDomainPopulation.parseInteger(accountKey.toString()));
									experianSROPDomains.get(advActHistCntr).setYearAdvHist(advancedAccountHistoryList.get(j).getYear());
									experianSROPDomains.get(advActHistCntr).setMonthAdvHist(advancedAccountHistoryList.get(j).getMonth());
									experianSROPDomains.get(advActHistCntr).setCashLimit(ExperianSropDomainPopulation.parseInteger(advancedAccountHistoryList.get(j).getCashLimit()));
									experianSROPDomains.get(advActHistCntr).setCreditLimitAmt(ExperianSropDomainPopulation.parseInteger(advancedAccountHistoryList.get(j).getCreditLimitAmount()));
									experianSROPDomains.get(advActHistCntr).setActualPaymentAmt(ExperianSropDomainPopulation.parseInteger(advancedAccountHistoryList.get(j).getActualPaymentAmount()));
									experianSROPDomains.get(advActHistCntr).setEmiAmt(ExperianSropDomainPopulation.parseInteger(advancedAccountHistoryList.get(j).getEmiAmount()));
									experianSROPDomains.get(advActHistCntr).setCurrentBalanceAdvHist(ExperianSropDomainPopulation.parseInteger(advancedAccountHistoryList.get(j).getCurrentBalance()));
									experianSROPDomains.get(advActHistCntr).setAmountPastDueAdvHist(ExperianSropDomainPopulation.parseInteger(advancedAccountHistoryList.get(j).getAmountPastDue()));
									
								}
							}
							
							Integer holderDetailCntr = actLevelCntr-1;
							
							List<CAISHolderDetails> caisHolderDetailsList = caisAccountDetails.get(i).getCaisHolderDetailsList();
							if(null != caisHolderDetailsList && caisHolderDetailsList.size() > 0){
								for(Integer j =0; j<caisHolderDetailsList.size();j++){
									holderDetailCntr++;
									
									if(holderDetailCntr > counter){
										HibExperianSropDomain newDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber, accountNumber,pickupDate);
										experianSROPDomains.add(newDomain);
										counter ++;
									}
									experianSROPDomains.get(holderDetailCntr).setSurNameNonNormalized(caisHolderDetailsList.get(j).getSurnameNonNormalized());
									experianSROPDomains.get(holderDetailCntr).setFirstNameNonNormalized(caisHolderDetailsList.get(j).getFirstNameNonNormalized());
									experianSROPDomains.get(holderDetailCntr).setMiddleName1NonNormalized(caisHolderDetailsList.get(j).getMiddleName1NonNormalized());
									experianSROPDomains.get(holderDetailCntr).setMiddleName2NonNormalized(caisHolderDetailsList.get(j).getMiddleName2NonNormalized());
									experianSROPDomains.get(holderDetailCntr).setMiddleName3NonNormalized(caisHolderDetailsList.get(j).getMiddleName3NonNormalized());
									experianSROPDomains.get(holderDetailCntr).setAlias(caisHolderDetailsList.get(j).getAlias());
									experianSROPDomains.get(holderDetailCntr).setCaisGenderCode(caisHolderDetailsList.get(j).getGenderCode());
									experianSROPDomains.get(holderDetailCntr).setCaisGender(experianUtils.getGender(caisHolderDetailsList.get(j).getGenderCode()));
									experianSROPDomains.get(holderDetailCntr).setCaisIncomeTaxPan(caisHolderDetailsList.get(j).getIncomeTaxPan());
									experianSROPDomains.get(holderDetailCntr).setCaisPassportNumber(caisHolderDetailsList.get(j).getPassportNumber());
									experianSROPDomains.get(holderDetailCntr).setVoterIdNumber(caisHolderDetailsList.get(j).getVoterIdNumber());
									experianSROPDomains.get(holderDetailCntr).setDateOfBirth(ExperianSropDomainPopulation.parseDate(caisHolderDetailsList.get(j).getDateOfBirth()));
								}
							}
							
							
							Integer  holderAddressDetailsCnt = actLevelCntr-1;
							List<CAISHolderAddressDetails> caisHolderAddressDetailsList = caisAccountDetails.get(i).getCaisHolderAddressDetailsList();
							if(null != caisHolderAddressDetailsList  && caisHolderAddressDetailsList.size() > 0){
								for(Integer j=0;j<caisHolderAddressDetailsList.size();j++){
									holderAddressDetailsCnt++;
									
									if(holderAddressDetailsCnt > counter){
										HibExperianSropDomain newDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber, accountNumber,pickupDate);
										experianSROPDomains.add(newDomain);
										counter ++;
									}
									
									experianSROPDomains.get(holderAddressDetailsCnt).setFirstLineOfAddNonNormalized(caisHolderAddressDetailsList.get(j).getFirstLineOfAddressNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setSecondLineOfAddNonNormalized(caisHolderAddressDetailsList.get(j).getSecondLineOfAddressNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setThirdLineOfAddNonNormalized(caisHolderAddressDetailsList.get(j).getThirdLineOfAddressNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setCityNonNormalized(caisHolderAddressDetailsList.get(j).getCityNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setFifthLineOfAddNonNormalized(caisHolderAddressDetailsList.get(j).getFifthLineOfAddressNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setStateCodeNonNormalized(caisHolderAddressDetailsList.get(j).getStateNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setStateNonNormalized(experianUtils.getState(caisHolderAddressDetailsList.get(j).getStateNonNormalized()));
									experianSROPDomains.get(holderAddressDetailsCnt).setZipPostalCodeNonNormalized(caisHolderAddressDetailsList.get(j).getZipPostalCodeNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setCountryCodeNonNormalized(caisHolderAddressDetailsList.get(j).getCountryCodenonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setAddIndicatorNonNormalized(caisHolderAddressDetailsList.get(j).getAddressIndicatorNonNormalized());
									experianSROPDomains.get(holderAddressDetailsCnt).setResidenceCodeNonNormalized(caisHolderAddressDetailsList.get(j).getResidenceCodeNonNormalized());
//									experianSROPDomains.get(holderAddressDetailsCnt).setResidenceNonNormalized();
								}
							}
							
							Integer caisHolderPhoneCntr = actLevelCntr-1;
							List<CAISHolderPhoneDetails> caisHolderPhoneDetailsList = caisAccountDetails.get(i).getCaisHolderPhoneDetailsList();
							if(null != caisHolderPhoneDetailsList && caisHolderPhoneDetailsList.size() > 0){
								for(Integer j=0;j<caisHolderPhoneDetailsList.size();j++){
								caisHolderPhoneCntr++ ;
								
								if(caisHolderPhoneCntr > counter){
									HibExperianSropDomain newDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber, accountNumber,pickupDate);
									experianSROPDomains.add(newDomain);
									counter ++;
								}
								experianSROPDomains.get(caisHolderPhoneCntr).setTelephoneNumber(caisHolderPhoneDetailsList.get(j).getTelephoneNumber());
								experianSROPDomains.get(caisHolderPhoneCntr).setTeleTypePhone(caisHolderPhoneDetailsList.get(j).getTelephoneType());
								experianSROPDomains.get(caisHolderPhoneCntr).setTeleExtensionPhone(caisHolderPhoneDetailsList.get(j).getTelephoneExtension());
								experianSROPDomains.get(caisHolderPhoneCntr).setMobileTelephoneNumber(caisHolderPhoneDetailsList.get(j).getMobileTelephoneNumber());
								experianSROPDomains.get(caisHolderPhoneCntr).setFaxNumber(caisHolderPhoneDetailsList.get(j).getFaxNumber());
								experianSROPDomains.get(caisHolderPhoneCntr).setEmailIdPhone(caisHolderPhoneDetailsList.get(j).getEmailId());
							  }
							}
							
							// This is a single object (No list). So use account level counter directly.
							CAISHolderIdDetails caisHolderIdDetails = caisAccountDetails.get(i).getCaisHolderIdDetails();
							if(caisHolderIdDetails!=null){
								experianSROPDomains.get(actLevelCntr).setIncomeTaxPanId(caisHolderIdDetails.getIncomeTaxPan());
								experianSROPDomains.get(actLevelCntr).setPanIssueDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getPanIssueDate()));
								experianSROPDomains.get(actLevelCntr).setPanExpDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getPanExpirationDate()));
								experianSROPDomains.get(actLevelCntr).setPassportNumberId(caisHolderIdDetails.getPassportNumber());
								experianSROPDomains.get(actLevelCntr).setPassportIssueDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getPassportissueDate()));
								experianSROPDomains.get(actLevelCntr).setPassportExpDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getPassportExpirationDate()));
								experianSROPDomains.get(actLevelCntr).setVoterIdentityCardId(caisHolderIdDetails.getVoterIdNumber());
								experianSROPDomains.get(actLevelCntr).setVoterIdIssueDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getVoteridIssueDate()));
								experianSROPDomains.get(actLevelCntr).setVoterIdExpDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getVoterIdExpirationDate()));
								experianSROPDomains.get(actLevelCntr).setDriverLicenseNumberId(caisHolderIdDetails.getDriverLicenceNumber());
								experianSROPDomains.get(actLevelCntr).setDriverLicenseIssueDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getDriverLicenceIssueDate()));
								experianSROPDomains.get(actLevelCntr).setDriverLicenseExpDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getDriverLicenceExpirationDate()));
								experianSROPDomains.get(actLevelCntr).setRationCardNumberId(caisHolderIdDetails.getRationCardNumber());
								experianSROPDomains.get(actLevelCntr).setRationCardIssueDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getRationCardissueDate()));
								experianSROPDomains.get(actLevelCntr).setRationCardExpDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getRationCardExpirationDate()));
								experianSROPDomains.get(actLevelCntr).setUniversalIdNumberId(caisHolderIdDetails.getUniversalIdNumber());
								experianSROPDomains.get(actLevelCntr).setUniversalIdIssueDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getUniversalIdIssueDate()));
								experianSROPDomains.get(actLevelCntr).setUniversalIdExpDateId(ExperianSropDomainPopulation.parseDate(caisHolderIdDetails.getUniversalIdExpirationdate()));
								experianSROPDomains.get(actLevelCntr).setEmailidId(caisHolderIdDetails.getEmailId());
							}
							
//								tempAccntCounter = actLevelCntr;
							actLevelCntr = getMaxAccLevel(actLevelCntr,actHistCntr,advActHistCntr,holderDetailCntr,holderAddressDetailsCnt,caisHolderPhoneCntr);
 							accountKey++;
 							
 							
						}
					}
						
						MatchResult matchResult = inProfileResponse.getMatchResult();
						if(matchResult != null){
							experianSROPDomains.get(0).setExactMatch(matchResult.getExactMatch());
						}
						
						
						
						Integer count = 0;
						Integer capsCounter =-1; 
						
						
						CAPS caps = inProfileResponse.getCaps();
						if(caps != null){
							CAPSSummary capsSummary = caps.getCapsSummary();
							if(capsSummary != null){
								experianSROPDomains.get(count).setCapsLast7Days(ExperianSropDomainPopulation.parseInteger(capsSummary.getCapsLast7Days()));
								experianSROPDomains.get(count).setCapsLast30Days(ExperianSropDomainPopulation.parseInteger(capsSummary.getCapsLast30Days()));
								experianSROPDomains.get(count).setCapsLast90Days(ExperianSropDomainPopulation.parseInteger(capsSummary.getCapsLast90Days()));
								experianSROPDomains.get(count).setCapsLast180Days(ExperianSropDomainPopulation.parseInteger(capsSummary.getCapsLast180Days()));
							}
							
							List<CAPSApplicationDetails> capsApplicationDetailList = caps.getCapsApplicationDetailList();
							if(null != capsApplicationDetailList && capsApplicationDetailList.size() > 0){
								for(Integer i=  0 ; i<capsApplicationDetailList.size() ; i++){	
									
									CAPSApplicationDetails capsApplicationDetails = capsApplicationDetailList.get(i);
									
									capsCounter++;
									
									if(capsCounter > counter){
										HibExperianSropDomain newDomain = new HibExperianSropDomain(srNumber, soaSourceName, memberReferenceNumber,pickupDate);
										experianSROPDomains.add(newDomain);
										count ++;
									}
									
									experianSROPDomains.get(capsCounter).setCapsSubscriberCode(capsApplicationDetails.getSubscriberCode());
									experianSROPDomains.get(capsCounter).setCapsSubscriberName(capsApplicationDetails.getSubscriberName());
									experianSROPDomains.get(capsCounter).setCapsdateofRequest(ExperianSropDomainPopulation.parseDate(capsApplicationDetails.getDateOfRequest()));
									experianSROPDomains.get(capsCounter).setCapsReportTime(capsApplicationDetails.getReportTime());
									experianSROPDomains.get(capsCounter).setCapsReportNumber(capsApplicationDetails.getReportNumber());
									
									experianSROPDomains.get(capsCounter).setEnquiryReasonCode(capsApplicationDetails.getEnquiryReason());
									
									experianSROPDomains.get(capsCounter).setCapsEnquiryReason(experianUtils.getEnquiryReason(capsApplicationDetails.getEnquiryReason()));
									
									experianSROPDomains.get(capsCounter).setFinancePurposeCode(capsApplicationDetails.getFinancePurpose());
									experianSROPDomains.get(capsCounter).setFinancePurpose(experianUtils.getFinancialPurpose(capsApplicationDetails.getFinancePurpose()));
									experianSROPDomains.get(capsCounter).setCapsAmountFinanced(capsApplicationDetails.getAmountFinanced());
									experianSROPDomains.get(capsCounter).setCapsDurationOfAgreement(capsApplicationDetails.getDurationOfAgreement());
									
									
									CAPSApplicantDetails capsApplicantDetails = capsApplicationDetails.getCapsApplicantDetails();
									
									if(null != capsApplicantDetails){
										experianSROPDomains.get(capsCounter).setCapsApplicantLastName(capsApplicantDetails.getLastName());
										experianSROPDomains.get(capsCounter).setCapsApplicantFirstName(capsApplicantDetails.getFirstname());
										experianSROPDomains.get(capsCounter).setCapsApplicantMiddleName1(capsApplicantDetails.getMiddleName1());
										experianSROPDomains.get(capsCounter).setCapsApplicantMiddleName2(capsApplicantDetails.getMiddleName2());
										experianSROPDomains.get(capsCounter).setCapsApplicantMiddleName3(capsApplicantDetails.getMiddleName3());
										experianSROPDomains.get(capsCounter).setCapsApplicantgenderCode(capsApplicantDetails.getGenderCode());
										experianSROPDomains.get(capsCounter).setCapsApplicantGender(experianUtils.getGender(capsApplicantDetails.getGenderCode()));
										experianSROPDomains.get(capsCounter).setCapsIncomeTaxPan(capsApplicantDetails.getIncomeTaxPan());
										experianSROPDomains.get(capsCounter).setCapsPanIssueDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getPanIssuedate()));
										experianSROPDomains.get(capsCounter).setCapsPanExpDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getPanExpirationDate()));
										experianSROPDomains.get(capsCounter).setCapsPassportNumber(capsApplicantDetails.getPassportNumber());
										experianSROPDomains.get(capsCounter).setCapsPassportIssueDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getPassportissueDate()));
										experianSROPDomains.get(capsCounter).setCapsPassportExpDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getPassportExpirationDate()));
										experianSROPDomains.get(capsCounter).setCapsVoterIdentityCard(capsApplicantDetails.getVotersidentityCard());
										experianSROPDomains.get(capsCounter).setCapsVoterIdIssueDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getVoterIdIssueDate()));
										experianSROPDomains.get(capsCounter).setCapsVoterIdExpDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getVoterIdExpirationDate()));
										experianSROPDomains.get(capsCounter).setCapsDriverLicenseNumber(capsApplicantDetails.getDriverLincenceNumber());
										experianSROPDomains.get(capsCounter).setCapsDriverLicenseIssueDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getDriverLincenceIssueDate()));
										experianSROPDomains.get(capsCounter).setCapsDriverLicenseExpDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getDriverLincenceExpirationDate()));
										experianSROPDomains.get(capsCounter).setCapsRationCardNumber(capsApplicantDetails.getRationCardNumber());
										experianSROPDomains.get(capsCounter).setCapsRationCardIssueDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getRationCardIssueDate()));
										experianSROPDomains.get(capsCounter).setCapsRationCardExpDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getRationCardExpirationDate()));
										experianSROPDomains.get(capsCounter).setCapsUniversalIdNumber(capsApplicantDetails.getUniversalIdNumber());
										experianSROPDomains.get(capsCounter).setCapsUniversalIdIssueDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getUniversalIdIssueDate()));
										experianSROPDomains.get(capsCounter).setCapsUniversalIdExpDate(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getUniversalIdExpirationDate()));
										experianSROPDomains.get(capsCounter).setCapsApplicantDobApplicant(ExperianSropDomainPopulation.parseDate(capsApplicantDetails.getDateOfBirthapplicant()));
										experianSROPDomains.get(capsCounter).setCapsApplicantTelNoApplicant1st(capsApplicantDetails.getTelephoneNumberApplicant1st());
										experianSROPDomains.get(capsCounter).setCapsApplicantTelExt(capsApplicantDetails.getTelephoneExtension());
										experianSROPDomains.get(capsCounter).setCapsApplicantTelType(capsApplicantDetails.getTelephonetype());
										experianSROPDomains.get(capsCounter).setCapsApplicantMobilePhoneNumber(capsApplicantDetails.getMobilePhoneNumber());
										experianSROPDomains.get(capsCounter).setCapsApplicantEmailId(capsApplicantDetails.getEmailId());
										
										CAPSOtherDetails capsOtherDetails = capsApplicationDetails.getCapsOtherDetails();
										if(null != capsOtherDetails){
											
											experianSROPDomains.get(capsCounter).setCapsApplicantIncome(ExperianSropDomainPopulation.parseInteger(capsOtherDetails.getIncome()));
											experianSROPDomains.get(capsCounter).setApplicantMaritalStatusCode(capsOtherDetails.getMaritialStatus());
											experianSROPDomains.get(capsCounter).setCapsApplicantMaritalStatus(experianUtils.getMaritalStatus(capsOtherDetails.getMaritialStatus()));
											experianSROPDomains.get(capsCounter).setCapsApplicantEmploymentStatusCode(capsOtherDetails.getEmploymentStatus());
											experianSROPDomains.get(capsCounter).setCapsApplicantEmploymentStatus(experianUtils.getEmploymentStatus(capsOtherDetails.getEmploymentStatus()));
											experianSROPDomains.get(capsCounter).setCapsApplicantDateWithEmployer(capsOtherDetails.getTimeWithEmployer());
											experianSROPDomains.get(capsCounter).setCapsApplicantNoMajorCrditCardHeld(capsOtherDetails.getNumberOfMajorCreditCardHeld());
											
										}
										
										CAPSApplicantAddressDetails capsApplicantAddressDetail = capsApplicationDetails.getCapsApplicantAddressDetails();
										if(null != capsApplicantAddressDetail){
											experianSROPDomains.get(capsCounter).setCapsApplicantFlatNoPlotNoHouseNo(capsApplicantAddressDetail.getFlatNoPlotNoHouseNo());
											experianSROPDomains.get(capsCounter).setCapsApplicantBldgNoSocietyName(capsApplicantAddressDetail.getBldgNoSocietyName());
											experianSROPDomains.get(capsCounter).setCapsApplicantRoadNoNameAreaLocality(capsApplicantAddressDetail.getRoadNoNameAreaLocality());
											experianSROPDomains.get(capsCounter).setCapsApplicantCity(capsApplicantAddressDetail.getCity());
											experianSROPDomains.get(capsCounter).setCapsApplicantLandmark(capsApplicantAddressDetail.getLandmark());
											experianSROPDomains.get(capsCounter).setCapsApplicantStateCode(capsApplicantAddressDetail.getState());
											experianSROPDomains.get(capsCounter).setCapsApplicantState(experianUtils.getState(capsApplicantAddressDetail.getState()));
											experianSROPDomains.get(capsCounter).setCapsApplicantPinCode(capsApplicantAddressDetail.getPinCode());
											experianSROPDomains.get(capsCounter).setCapsApplicantCountryCode(capsApplicantAddressDetail.getCountryCode());
										}
									}
								}
							}
						}
							
							
							
							Score  score  = inProfileResponse.getScore();
							if(score != null){
								experianSROPDomains.get(count).setBureauScore(ExperianSropDomainPopulation.parseInteger(score.getBureauScore()));
								experianSROPDomains.get(count).setBureauScoreConfidLevel(score.getBureauScoreConfidLevel());
								experianSROPDomains.get(count).setCreditRating(score.getCreditRating());
							}
							
							PSV psv  = inProfileResponse.getPsv();
							if(psv != null){
								BFHLExHl bfhlExHl_ = psv.getBfhlExHl();
								if(bfhlExHl_!=null){
									experianSROPDomains.get(count).setTnOfBFHLCADExhl(ExperianSropDomainPopulation.parseInteger(bfhlExHl_.getTNOfBFHLCADExHl()));
									experianSROPDomains.get(count).setTotValOfBFHLCAD(ExperianSropDomainPopulation.parseInteger(bfhlExHl_.getTotValOfBFHLCAD()));
									experianSROPDomains.get(count).setMNTSMRBFHLCAD(ExperianSropDomainPopulation.parseInteger(bfhlExHl_.getMNTSMRBFHLCAD()));
								}
								HLCAD hlcad = psv.getHlcad();
								if(hlcad != null){
									experianSROPDomains.get(count).setTnOfHLCAD(ExperianSropDomainPopulation.parseInteger(hlcad.getTNOfHLCAD()));
									experianSROPDomains.get(count).setTotValOfHLCAD(ExperianSropDomainPopulation.parseInteger(hlcad.getTotValOfHLCAD()));
									experianSROPDomains.get(count).setMntsmrHLCAD(ExperianSropDomainPopulation.parseInteger(hlcad.getMNTSMRHLCAD()));
								}
								
								TelcosCAD telcosCAD = psv.getTelcosCAD();
								if(telcosCAD != null){
									experianSROPDomains.get(count).setTnOfTelcosCAD(ExperianSropDomainPopulation.parseInteger(telcosCAD.getTNOfTelcosCAD()));
									experianSROPDomains.get(count).setTotValOfTelcosCad(ExperianSropDomainPopulation.parseInteger(telcosCAD.getTotValOfTelcosCAD()));
									experianSROPDomains.get(count).setMntsmrTelcosCad(ExperianSropDomainPopulation.parseInteger(telcosCAD.getMNTSMRTelcosCAD()));
								}
								
								MFCAD mfcad = psv.getMfcad();
								if(mfcad != null){
									experianSROPDomains.get(count).setTnOfmfCAD(ExperianSropDomainPopulation.parseInteger(mfcad.getTNOfMFCAD()));
									experianSROPDomains.get(count).setTotValOfmfCAD(ExperianSropDomainPopulation.parseInteger(mfcad.getTotValOfMFCAD()));
									experianSROPDomains.get(count).setMntsmrmfCAD(ExperianSropDomainPopulation.parseInteger(mfcad.getMNTSMRMFCAD()));
								}
								
								RetailCAD retailCAD = psv.getRetailCAD();
								if(retailCAD != null){
									experianSROPDomains.get(count).setTnOfRetailCAD(ExperianSropDomainPopulation.parseInteger(retailCAD.getTNOfRetailCAD()));
								    experianSROPDomains.get(count).setTotValOfRetailCAD(ExperianSropDomainPopulation.parseInteger(retailCAD.getTotValOfRetailCAD()));
								    experianSROPDomains.get(count).setMntsmrRetailCAD(ExperianSropDomainPopulation.parseInteger(retailCAD.getMNTSMRRetailCAD()));
								}    
								
								TotalCAD totalCAD = psv.getTotalCAD();
								if(totalCAD != null){
								   experianSROPDomains.get(count).setTnOfAllCAD(ExperianSropDomainPopulation.parseInteger(totalCAD.getTNOfAllCAD()));
								   experianSROPDomains.get(count).setTotValOfAllCAD(ExperianSropDomainPopulation.parseInteger(totalCAD.getTotValOfAllCAD()));
							       experianSROPDomains.get(count).setMntsmrCADAll(ExperianSropDomainPopulation.parseInteger(totalCAD.getMNTSMRCADAll()));
							    }
								
								BFHLACAExHL bfhlacaExHL = psv.getBfhlacaExHL();
								if(bfhlacaExHL != null){
									experianSROPDomains.get(count).setTnOfBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getTNOfBFHLACAExHL()));
								    experianSROPDomains.get(count).setBalBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getBalBFHLACAExHL()));
								    experianSROPDomains.get(count).setWcdstBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getWCDStBFHLACAExHL()));
								    experianSROPDomains.get(count).setWdspr6MntBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getWDSPr6MNTBFHLACAExHL()));
								    experianSROPDomains.get(count).setWdspr712MntBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getWDSPr712MNTBFHLACAExHL()));
								    experianSROPDomains.get(count).setAgeOfOldestBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getAgeOfOldestBFHLACAExHL()));
								    experianSROPDomains.get(count).setHcbperrevaccBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getHCBPerRevAccBFHLACAExHL()));
								    experianSROPDomains.get(count).setTcbperrevaccBFHLACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlacaExHL.getTCBPerRevAccBFHLACAExHL()));
								}
								
								HlACA hlACA = psv.getHlACA();
								if(hlACA != null){
									experianSROPDomains.get(count).setTnOfHlACA(ExperianSropDomainPopulation.parseInteger(hlACA.getTNOfHLACA()));
									experianSROPDomains.get(count).setBalHlACA(ExperianSropDomainPopulation.parseInteger(hlACA.getBalHLACA()));
									experianSROPDomains.get(count).setWcdstHlACA(ExperianSropDomainPopulation.parseInteger(hlACA.getWCDStHLACA()));
									experianSROPDomains.get(count).setWdspr6MnthlACA(ExperianSropDomainPopulation.parseInteger(hlACA.getWDSPr6MNTHLACA()));
									experianSROPDomains.get(count).setWdspr712mnthlACA(ExperianSropDomainPopulation.parseInteger(hlACA.getWDSPr712MNTHLACA()));
									experianSROPDomains.get(count).setAgeOfOldesthlACA(ExperianSropDomainPopulation.parseInteger(hlACA.getAgeOfOldestHLACA()));
								}
								
								MFACA mfaca = psv.getMfaca();
								if(mfaca != null){
									experianSROPDomains.get(count).setTnOfMfACA(ExperianSropDomainPopulation.parseInteger(mfaca.getTNOfMFACA()));
									experianSROPDomains.get(count).setTotalBalMfACA(ExperianSropDomainPopulation.parseInteger(mfaca.getTotalBalMFACA()));
									experianSROPDomains.get(count).setWcdstMfACA(ExperianSropDomainPopulation.parseInteger(mfaca.getWCDStMFACA()));
									experianSROPDomains.get(count).setWdspr6MntMfACA(ExperianSropDomainPopulation.parseInteger(mfaca.getWDSPr6MNTMFACA()));
									experianSROPDomains.get(count).setWdspr712mntMfACA(ExperianSropDomainPopulation.parseInteger(mfaca.getWDSPr712MNTMFACA()));
									experianSROPDomains.get(count).setAgeOfOldestMfACA(ExperianSropDomainPopulation.parseInteger(mfaca.getAgeOfOldestMFACA()));
								}
								
								TelcosACA telcosACA  = psv.getTelcosACA();
								if(telcosACA != null){
									experianSROPDomains.get(count).setTnOfTelcosACA(ExperianSropDomainPopulation.parseInteger(telcosACA.getTNofTelcosACA()));
									experianSROPDomains.get(count).setTotalBalTelcosACA(ExperianSropDomainPopulation.parseInteger(telcosACA.getTotalBalTelcosACA()));
									experianSROPDomains.get(count).setWcdstTelcosACA(ExperianSropDomainPopulation.parseInteger(telcosACA.getWCDStTelcosACA()));
									experianSROPDomains.get(count).setWdspr6mntTelcosACA(ExperianSropDomainPopulation.parseInteger(telcosACA.getWDSPr6MNTTelcosACA()));
									experianSROPDomains.get(count).setWdspr712mntTelcosACA(ExperianSropDomainPopulation.parseInteger(telcosACA.getWDSPr712MNTTelcosACA()));
									experianSROPDomains.get(count).setAgeOfOldestTelcosACA(ExperianSropDomainPopulation.parseInteger(telcosACA.getAgeOfOldestTelcosACA()));	
								}
								
								RetailACA retailACA = psv.getRetailACA();
								if(retailACA != null){
									experianSROPDomains.get(count).setTnOfRetailACA(ExperianSropDomainPopulation.parseInteger(retailACA.getTNOfRetailACA()));
									experianSROPDomains.get(count).setTotalBalRetailACA(ExperianSropDomainPopulation.parseInteger(retailACA.getTotalBalRetailACA()));
									experianSROPDomains.get(count).setWcdstRetailACA(ExperianSropDomainPopulation.parseInteger(retailACA.getWCDStRetailACA()));
									experianSROPDomains.get(count).setWdspr6mntRetailACA(ExperianSropDomainPopulation.parseInteger(retailACA.getWDSPr6MNTRetailACA()));
									experianSROPDomains.get(count).setWdspr712mntRetailACA(ExperianSropDomainPopulation.parseInteger(retailACA.getWDSPr712MNTRetailACA()));
									experianSROPDomains.get(count).setAgeOfOldestRetailACA(ExperianSropDomainPopulation.parseInteger(retailACA.getAgeOfOldestRetail()));
									experianSROPDomains.get(count).setHcblmperrevaccret(ExperianSropDomainPopulation.parseInteger(retailACA.getHCBLmPerRevAccRet()));
									experianSROPDomains.get(count).setTotCurBallmperrevaccret(ExperianSropDomainPopulation.parseInteger(retailACA.getTotCurBalLmPerRevAccRet()));
								}
									
								TotalACA totalACA = psv.getTotalACA();
								if(totalACA != null){
									experianSROPDomains.get(count).setTnOfallACA(ExperianSropDomainPopulation.parseInteger(totalACA.getTNOfAllACA()));
									experianSROPDomains.get(count).setBalAllACAExhl(ExperianSropDomainPopulation.parseInteger(totalACA.getBalAllACAExHL()));
									experianSROPDomains.get(count).setWcdstAllACA(ExperianSropDomainPopulation.parseInteger(totalACA.getWCDStAllACA()));
									experianSROPDomains.get(count).setWdspr6mntallACA(ExperianSropDomainPopulation.parseInteger(totalACA.getWDSPr6MNTAllACA()));
									experianSROPDomains.get(count).setWdspr712mntAllACA(ExperianSropDomainPopulation.parseInteger(totalACA.getWDSPr712MNTAllACA()));
									experianSROPDomains.get(count).setAgeOfOldestAllACA(ExperianSropDomainPopulation.parseInteger(totalACA.getAgeOfOldestAllACA()));
								}
								
								BFHLICAExHL bfhlicaExHL  = psv.getBfhlicaExHL();
								if(bfhlicaExHL != null){
									experianSROPDomains.get(count).setTnOfndelBFHLinACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlicaExHL.getTNOfNDelBFHLInACAExHL()));
									experianSROPDomains.get(count).setTnOfDelBFHLInACAExhl(ExperianSropDomainPopulation.parseInteger(bfhlicaExHL.getTNOfDelBFHLInACAExHL()));
								}
								
								HLInACA  hlInACA = psv.getHlInACA();
								if(hlInACA != null){
									experianSROPDomains.get(count).setTnOfnDelHLInACA(ExperianSropDomainPopulation.parseInteger(hlInACA.getTNOfNDelHLInACA()));
									experianSROPDomains.get(count).setTnOfDelhlInACA(ExperianSropDomainPopulation.parseInteger(hlInACA.getTNOfDelHLInACA()));
								}
								
								MFICA mfica = psv.getMfica();
								if(mfica != null){
									experianSROPDomains.get(count).setTnOfnDelmfinACA(ExperianSropDomainPopulation.parseInteger(mfica.getTNOfNDelMFInACA()));
									experianSROPDomains.get(count).setTnOfDelmfinACA(ExperianSropDomainPopulation.parseInteger(mfica.getTNOfDelMFInACA()));
								}
								
								TelcosICA telcosICA = psv.getTelcosICA();
								if(telcosICA != null){
									experianSROPDomains.get(count).setTnOfnDelTelcosInACA(ExperianSropDomainPopulation.parseInteger(telcosICA.getTNOfNDelTelcosInACA()));
									experianSROPDomains.get(count).setTnOfdelTelcosInACA(ExperianSropDomainPopulation.parseInteger(telcosICA.getTNOfDelTelcosInACA()));
								}
								
								RetailICA retailICA  = psv.getRetailICA();
								if(retailICA != null){
									experianSROPDomains.get(count).setTnOfndelRetailInACA(ExperianSropDomainPopulation.parseInteger(retailICA.getTNOfNDelRetailInACA()));
									experianSROPDomains.get(count).setTnOfDelRetailInACA(ExperianSropDomainPopulation.parseInteger(retailICA.getTNOfDelRetailInACA()));
								}
								
								PSVCAPS psvcaps = psv.getPsvcaps();
								if(psvcaps != null){
									experianSROPDomains.get(count).setBFHLCapsLast90Days(ExperianSropDomainPopulation.parseInteger(psvcaps.getBFHLCAPSLast90Days()));
									experianSROPDomains.get(count).setMfCapsLast90Days(ExperianSropDomainPopulation.parseInteger(psvcaps.getMFCAPSLast90Days()));
									experianSROPDomains.get(count).setTelcosCapsLast90Days(ExperianSropDomainPopulation.parseInteger(psvcaps.getTelcosCAPSLast90Days()));
									experianSROPDomains.get(count).setRetailCapsLast90Days(ExperianSropDomainPopulation.parseInteger(psvcaps.getRetailCAPSLast90Days()));
								}
								
								OwnCompanyData ownCompanyData  = psv.getOwnCompanyData();
								if(ownCompanyData != null){
									experianSROPDomains.get(count).setTnOfocomcad(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getTNOfOComCAD()));
									experianSROPDomains.get(count).setTotValOfocomCAD(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getTotValOfOComCAD()));
									experianSROPDomains.get(count).setMntsmrocomCAD(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getMNTSMROComCAD()));
									experianSROPDomains.get(count).setTnOfocomACA(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getTNOfOComACA()));
									experianSROPDomains.get(count).setBalocomACAExhl(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getBalOComACAExHL()));
									experianSROPDomains.get(count).setBalOcomacahlonly(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getBalOComACAHLOnly()));
									experianSROPDomains.get(count).setWcdstocomACA(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getWCDStOComACA()));
									experianSROPDomains.get(count).setHcblmperrevocomACA(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getHCBLmPerRevOComACA()));
									experianSROPDomains.get(count).setTnOfnDelocominACA(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getTNOfNDelOComInACA()));
									experianSROPDomains.get(count).setTnOfDelocominACA(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getTNOfDelOComInACA()));
									experianSROPDomains.get(count).setTnOfocomCapsLast90Days(ExperianSropDomainPopulation.parseInteger(ownCompanyData.getTNOfOComCAPSLast90Days()));
								}
								
								OthCBInformation othCBInformation  = psv.getOthCBInformation();
								if(othCBInformation != null){
									experianSROPDomains.get(count).setAnyRelcbdatadisyn(ExperianSropDomainPopulation.parseInteger(othCBInformation.getAnyRelCBDataDisYN()));
									experianSROPDomains.get(count).setOthrelcbdfcposmatyn(ExperianSropDomainPopulation.parseInteger(othCBInformation.getOthRelCBDFCPosMatYN()));
									
								}
								
								IndianMarketSpecificVar  indianMarketSpecificVar = psv.getIndianMarketSpecificVar();
								if(indianMarketSpecificVar != null){
									experianSROPDomains.get(count).setTnOfCADclassedassfwdwo(ExperianSropDomainPopulation.parseInteger(indianMarketSpecificVar.getTNOfCADClassedAsSFWDWO()));
									experianSROPDomains.get(count).setMntsmrcadclassedassfwdwo(ExperianSropDomainPopulation.parseInteger(indianMarketSpecificVar.getMNTSMRCADClassedAsSFWDWO()));
									experianSROPDomains.get(count).setNumOfCadsfwdwolast24mnt(ExperianSropDomainPopulation.parseInteger(indianMarketSpecificVar.getNumOfCADSFWDWOLast24MNT()));	
									experianSROPDomains.get(count).setTotCurBalLivesAcc(ExperianSropDomainPopulation.parseInteger(indianMarketSpecificVar.getTotCurBalLiveSAcc()));
									experianSROPDomains.get(count).setTotCurBalLiveuAcc(ExperianSropDomainPopulation.parseInteger(indianMarketSpecificVar.getTotCurBalLiveUAcc()));
									experianSROPDomains.get(count).setTotCurBalMaxBalLivesAcc(ExperianSropDomainPopulation.parseInteger(indianMarketSpecificVar.getTotCurBalMaxBalLiveSAcc()));
									experianSROPDomains.get(count).setTotCurBalMaxBalLiveuAcc(ExperianSropDomainPopulation.parseInteger(indianMarketSpecificVar.getTotCurBalMaxBalLiveUAcc()));
								}
								count++;
						  }
						
					}
				}
			
			return experianSROPDomains;
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception(""+e.getMessage());
		}
		
	}


	private static Date parseDate(String dateString){
		Date value = null;
		try{
			if(StringUtils.isNotBlank(dateString)){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				value = sdf.parse(dateString);
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Date Not parse with format YYYYMMdd "+dateString);
		}
		return value;
	}
	
	private static Integer parseInteger(String integerValue){
		Integer value = null;
		try{
			if(StringUtils.isNotBlank(integerValue) && StringUtils.isNumeric(integerValue)){
				value = Integer.parseInt(integerValue);
			}else{
				if(StringUtils.isNotBlank(integerValue)){
					System.out.println("SKIP PARSEING INTEGER "+integerValue);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("NOT PARSEING INTEGER "+integerValue);
		}
		return value;
	}
	

	private Integer getMaxAccLevel(Integer actLevelCntr, Integer actHistCntr,
			Integer advActHistCntr, Integer holderDetailCntr,
			Integer holderAddressDetailsCnt, Integer caisHolderPhoneCntr) {
			actLevelCntr = Math.max(actHistCntr, actLevelCntr);
			actLevelCntr = Math.max(advActHistCntr, actLevelCntr);
			actLevelCntr = Math.max(holderDetailCntr, actLevelCntr);
			actLevelCntr = Math.max(holderAddressDetailsCnt, actLevelCntr);
			actLevelCntr = Math.max(caisHolderPhoneCntr, actLevelCntr);
			return actLevelCntr;
	}
	
	
	
}
