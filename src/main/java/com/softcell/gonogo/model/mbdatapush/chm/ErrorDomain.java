package com.softcell.gonogo.model.mbdatapush.chm;

public class ErrorDomain {

	String field;
	String errorCategory;
	String errorType;
	String errorCode;
	String errorDescription;
	String runTimeErrorStackTrace;

	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public String getErrorCategory() {
		return errorCategory;
	}
	public void setErrorCategory(String errorCategory) {
		this.errorCategory = errorCategory;
	}
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public String getRunTimeErrorStackTrace() {
		return runTimeErrorStackTrace;
	}
	public void setRunTimeErrorStackTrace(String runTimeErrorStackTrace) {
		this.runTimeErrorStackTrace = runTimeErrorStackTrace;
	}
	@Override
	public String toString() {
		return "ErrorDomain [field=" + field + ", errorCategory="
				+ errorCategory + ", errorType=" + errorType + ", errorCode="
				+ errorCode + ", errorDescription=" + errorDescription
				+ ", runTimeErrorStackTrace=" + runTimeErrorStackTrace + "]";
	}
}
