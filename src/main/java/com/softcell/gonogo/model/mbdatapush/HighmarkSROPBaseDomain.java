package com.softcell.gonogo.model.mbdatapush;

public class HighmarkSROPBaseDomain {

	private String srNo_;
	private String soaSourceName_;
	private String memberReferenceNumber_;
	private String requestDateTime_;
	private String responseDateTime_;
	private String responseType_;
	private String description_;
	private String reportType_;
	private String reportDate_;
	private String inqCountFile_;
	private String responseCountFile_;
	private String dateOfRequest_;
	private String preparedFor_;
	private String preparedForId_;
	private String dateOfIssue_;
	private String reportId_;
	private String batchId_;
	private String status_;
	private String nameIQ_;
	private String aka_;
	private String spouse_;
	private String father_;
	private String mother_;
	private String dobIQ_;
	private String age_;
	private String ageAsOn_;
	private String rationCard_;
	private String passport_;
	private String votersId_;
	private String drivingLicenceNo_;
	private String pan_;
	private String gender_;
	private String ownership_;
	private String address1_;
	private String address2_;
	private String address3_;
	private String phone1_;
	private String phone2_;
	private String phone3_;
	private String emailId1_;
	private String emailId2_;
	private String branch_;
	private String kendra_;
	private String mbrId_;
	private String losAppId_;
	private String creditInquiryPurposeType_;
	private String creditInquiryPurposeTypeDesc_;
	private String creditInquiryStage_;
	private String creditReportId_;
	private String creditRequestType_;
	private String creditReportTransectionDateTime_;
	private String accountOpenDate_;
	private String loanAmount_;
	private String primaryNumberOfAccount_;
	private String primaryActiveNoOfAccount_;
	private String primaryOverdueNumberOfAccount_;
	private String primaryCurrentBalance_;
	private String primarySanctionedAmount_;
	private String primarySecuredNumberOfAccount_;
	private String primaryUnsecuredNumberOfAccount_;
	private String primaryUntaggedNumberOfAccount_;
	private String secondaryVNumberOfAccount_;
	private String secondaryActiveNumberOfAccount_;
	private String secondaryOverdueNumberOfAccount_;
	private String secondaryCurrentBalance_;
	private String secondarySanctionedAmount_;
	private String secondarySecuredNumberOfAccount_;
	private String secondaryUnsecuredNumberOfAccount_;
	private String secondaryUntaggedNumberOfAccount_;
	private String inquriesInLastSixMonth_;
	private String lengthOfCreditHistoryYear_;
	private String lengthOfCreditHistoryMonth_;
	private String averageAccountAgeYear_;
	private String averageAccountAgeMonth_;
	private String newAccountInLastSixMonth_;
	private String newDelinqAccountInLastSixMonth_;
	private String variationType_;
	private String value_;
	private String reportedDateVrtn_;
	private String matchedType_;
	private String accountNumber_;
	private String creditGuarantor_;
	private String acctType_;
	private String dateReported_;
	private String ownershipIndicator_;
	private String accountStatus_;
	private String disbursedAmount_;
	private String disbursedDate_;
	private String lastPaymentDate_;
	private String closedDate_;
	private String installmentAmount_;
	private String overdueAmount_;
	private String writeOffAmount_;
	private String currentBalance_;
	private String creditLimit_;
	private String accountRemarks_;
	private String frequency_;
	private String securityStatus_;
	private String originalTerm_;
	private String termToMaturity_;
	private String accountInDispute_;
	private String settlementAmount_;
	private String principalWriteOffAmount_;
	private String combinedPaymentHistory_;
	private String linkedAccounts_;
	private String securityType_;
	private String ownerName_;
	private String securityValue_;
	private String dateOfValue_;
	private String securityCharge_;
	private String propertyAddress_;
	private String automobileType_;
	private String yearOfManufacture_;
	private String registrationNumber_;
	private String engineNumber_;
	private String chassisNumber_;
	private String nameIndtl_;
	private String address_;
	private String dobIndtl_;
	private String phoneIndtl_;
	private String panIndtl_;
	private String passportIndtl_;
	private String drivingLicenseIndtl_;
	private String voterIdIndtl_;
	private String emailIndtl_;
	private String rationCardIndtl_;
	private String memberName_;
	private String inquiryDate_;
	private String purpose_;
	private String ownershipType_;
	private String amount_;
	private String remark_;
	private String commentText_;
	private String commentDate_;
	private String bureauComment_;
	private String alertType_;
	private String alertDescription_;
	private Long manualUploadBatchId_;
	private Long requestId_;
	private String applicationId_;
	private String customerId_;
	private String fileName_;
	private String accountKey_;
	private String accountParentKey_;
	
	public HighmarkSROPBaseDomain(String srNo_, String soaSourceName_,String memberReferenceNumber_, Long manualUploadBatchId_,Long requestId_, String fileName_, String applicationId, String customerId) {
		this.srNo_ = srNo_;
		this.soaSourceName_ = soaSourceName_;
		this.memberReferenceNumber_ = memberReferenceNumber_;
		this.manualUploadBatchId_ = manualUploadBatchId_;
		this.requestId_ = requestId_;
		this.fileName_ = fileName_;
		this.applicationId_ = applicationId;
		this.customerId_ = customerId;
	}
	
	public String getSrNo_() {
		return srNo_;
	}
	public void setSrNo_(String srNo_) {
		this.srNo_ = srNo_;
	}
	public String getSoaSourceName_() {
		return soaSourceName_;
	}
	public void setSoaSourceName_(String soaSourceName_) {
		this.soaSourceName_ = soaSourceName_;
	}
	public String getMemberReferenceNumber_() {
		return memberReferenceNumber_;
	}
	public void setMemberReferenceNumber_(String memberReferenceNumber_) {
		this.memberReferenceNumber_ = memberReferenceNumber_;
	}
	public String getRequestDateTime_() {
		return requestDateTime_;
	}
	public void setRequestDateTime_(String requestDateTime_) {
		this.requestDateTime_ = requestDateTime_;
	}
	public String getResponseDateTime_() {
		return responseDateTime_;
	}
	public void setResponseDateTime_(String responseDateTime_) {
		this.responseDateTime_ = responseDateTime_;
	}
	public String getResponseType_() {
		return responseType_;
	}
	public void setResponseType_(String responseType_) {
		this.responseType_ = responseType_;
	}
	public String getDescription_() {
		return description_;
	}
	public void setDescription_(String description_) {
		this.description_ = description_;
	}
	public String getReportType_() {
		return reportType_;
	}
	public void setReportType_(String reportType_) {
		this.reportType_ = reportType_;
	}
	public String getReportDate_() {
		return reportDate_;
	}
	public void setReportDate_(String reportDate_) {
		this.reportDate_ = reportDate_;
	}
	public String getInqCountFile_() {
		return inqCountFile_;
	}
	public void setInqCountFile_(String inqCountFile_) {
		this.inqCountFile_ = inqCountFile_;
	}
	public String getResponseCountFile_() {
		return responseCountFile_;
	}
	public void setResponseCountFile_(String responseCountFile_) {
		this.responseCountFile_ = responseCountFile_;
	}
	public String getDateOfRequest_() {
		return dateOfRequest_;
	}
	public void setDateOfRequest_(String dateOfRequest_) {
		this.dateOfRequest_ = dateOfRequest_;
	}
	public String getPreparedFor_() {
		return preparedFor_;
	}
	public void setPreparedFor_(String preparedFor_) {
		this.preparedFor_ = preparedFor_;
	}
	public String getPreparedForId_() {
		return preparedForId_;
	}
	public void setPreparedForId_(String preparedForId_) {
		this.preparedForId_ = preparedForId_;
	}
	public String getDateOfIssue_() {
		return dateOfIssue_;
	}
	public void setDateOfIssue_(String dateOfIssue_) {
		this.dateOfIssue_ = dateOfIssue_;
	}
	public String getReportId_() {
		return reportId_;
	}
	public void setReportId_(String reportId_) {
		this.reportId_ = reportId_;
	}
	public String getBatchId_() {
		return batchId_;
	}
	public void setBatchId_(String batchId_) {
		this.batchId_ = batchId_;
	}
	public String getStatus_() {
		return status_;
	}
	public void setStatus_(String status_) {
		this.status_ = status_;
	}
	public String getNameIQ_() {
		return nameIQ_;
	}
	public void setNameIQ_(String nameIQ_) {
		this.nameIQ_ = nameIQ_;
	}
	public String getAka_() {
		return aka_;
	}
	public void setAka_(String aka_) {
		this.aka_ = aka_;
	}
	public String getSpouse_() {
		return spouse_;
	}
	public void setSpouse_(String spouse_) {
		this.spouse_ = spouse_;
	}
	public String getFather_() {
		return father_;
	}
	public void setFather_(String father_) {
		this.father_ = father_;
	}
	public String getMother_() {
		return mother_;
	}
	public void setMother_(String mother_) {
		this.mother_ = mother_;
	}
	public String getDobIQ_() {
		return dobIQ_;
	}
	public void setDobIQ_(String dobIQ_) {
		this.dobIQ_ = dobIQ_;
	}
	public String getAge_() {
		return age_;
	}
	public void setAge_(String age_) {
		this.age_ = age_;
	}
	public String getAgeAsOn_() {
		return ageAsOn_;
	}
	public void setAgeAsOn_(String ageAsOn_) {
		this.ageAsOn_ = ageAsOn_;
	}
	public String getRationCard_() {
		return rationCard_;
	}
	public void setRationCard_(String rationCard_) {
		this.rationCard_ = rationCard_;
	}
	public String getPassport_() {
		return passport_;
	}
	public void setPassport_(String passport_) {
		this.passport_ = passport_;
	}
	public String getVotersId_() {
		return votersId_;
	}
	public void setVotersId_(String votersId_) {
		this.votersId_ = votersId_;
	}
	public String getDrivingLicenceNo_() {
		return drivingLicenceNo_;
	}
	public void setDrivingLicenceNo_(String drivingLicenceNo_) {
		this.drivingLicenceNo_ = drivingLicenceNo_;
	}
	public String getPan_() {
		return pan_;
	}
	public void setPan_(String pan_) {
		this.pan_ = pan_;
	}
	public String getGender_() {
		return gender_;
	}
	public void setGender_(String gender_) {
		this.gender_ = gender_;
	}
	public String getOwnership_() {
		return ownership_;
	}
	public void setOwnership_(String ownership_) {
		this.ownership_ = ownership_;
	}
	public String getAddress1_() {
		return address1_;
	}
	public void setAddress1_(String address1_) {
		this.address1_ = address1_;
	}
	public String getAddress2_() {
		return address2_;
	}
	public void setAddress2_(String address2_) {
		this.address2_ = address2_;
	}
	public String getAddress3_() {
		return address3_;
	}
	public void setAddress3_(String address3_) {
		this.address3_ = address3_;
	}
	public String getPhone1_() {
		return phone1_;
	}
	public void setPhone1_(String phone1_) {
		this.phone1_ = phone1_;
	}
	public String getPhone2_() {
		return phone2_;
	}
	public void setPhone2_(String phone2_) {
		this.phone2_ = phone2_;
	}
	public String getPhone3_() {
		return phone3_;
	}
	public void setPhone3_(String phone3_) {
		this.phone3_ = phone3_;
	}
	public String getEmailId1_() {
		return emailId1_;
	}
	public void setEmailId1_(String emailId1_) {
		this.emailId1_ = emailId1_;
	}
	public String getEmailId2_() {
		return emailId2_;
	}
	public void setEmailId2_(String emailId2_) {
		this.emailId2_ = emailId2_;
	}
	public String getBranch_() {
		return branch_;
	}
	public void setBranch_(String branch_) {
		this.branch_ = branch_;
	}
	public String getKendra_() {
		return kendra_;
	}
	public void setKendra_(String kendra_) {
		this.kendra_ = kendra_;
	}
	public String getMbrId_() {
		return mbrId_;
	}
	public void setMbrId_(String mbrId_) {
		this.mbrId_ = mbrId_;
	}
	public String getLosAppId_() {
		return losAppId_;
	}
	public void setLosAppId_(String losAppId_) {
		this.losAppId_ = losAppId_;
	}
	public String getCreditInquiryPurposeType_() {
		return creditInquiryPurposeType_;
	}
	public void setCreditInquiryPurposeType_(String creditInquiryPurposeType_) {
		this.creditInquiryPurposeType_ = creditInquiryPurposeType_;
	}
	public String getCreditInquiryPurposeTypeDesc_() {
		return creditInquiryPurposeTypeDesc_;
	}
	public void setCreditInquiryPurposeTypeDesc_(
			String creditInquiryPurposeTypeDesc_) {
		this.creditInquiryPurposeTypeDesc_ = creditInquiryPurposeTypeDesc_;
	}
	public String getCreditInquiryStage_() {
		return creditInquiryStage_;
	}
	public void setCreditInquiryStage_(String creditInquiryStage_) {
		this.creditInquiryStage_ = creditInquiryStage_;
	}
	public String getCreditReportId_() {
		return creditReportId_;
	}
	public void setCreditReportId_(String creditReportId_) {
		this.creditReportId_ = creditReportId_;
	}
	public String getCreditRequestType_() {
		return creditRequestType_;
	}
	public void setCreditRequestType_(String creditRequestType_) {
		this.creditRequestType_ = creditRequestType_;
	}
	public String getCreditReportTransectionDateTime_() {
		return creditReportTransectionDateTime_;
	}
	public void setCreditReportTransectionDateTime_(
			String creditReportTransectionDateTime_) {
		this.creditReportTransectionDateTime_ = creditReportTransectionDateTime_;
	}
	public String getAccountOpenDate_() {
		return accountOpenDate_;
	}
	public void setAccountOpenDate_(String accountOpenDate_) {
		this.accountOpenDate_ = accountOpenDate_;
	}
	public String getLoanAmount_() {
		return loanAmount_;
	}
	public void setLoanAmount_(String loanAmount_) {
		this.loanAmount_ = loanAmount_;
	}
	public String getPrimaryNumberOfAccount_() {
		return primaryNumberOfAccount_;
	}
	public void setPrimaryNumberOfAccount_(String primaryNumberOfAccount_) {
		this.primaryNumberOfAccount_ = primaryNumberOfAccount_;
	}
	public String getPrimaryActiveNoOfAccount_() {
		return primaryActiveNoOfAccount_;
	}
	public void setPrimaryActiveNoOfAccount_(String primaryActiveNoOfAccount_) {
		this.primaryActiveNoOfAccount_ = primaryActiveNoOfAccount_;
	}
	public String getPrimaryOverdueNumberOfAccount_() {
		return primaryOverdueNumberOfAccount_;
	}
	public void setPrimaryOverdueNumberOfAccount_(
			String primaryOverdueNumberOfAccount_) {
		this.primaryOverdueNumberOfAccount_ = primaryOverdueNumberOfAccount_;
	}
	public String getPrimaryCurrentBalance_() {
		return primaryCurrentBalance_;
	}
	public void setPrimaryCurrentBalance_(String primaryCurrentBalance_) {
		this.primaryCurrentBalance_ = primaryCurrentBalance_;
	}
	public String getPrimarySanctionedAmount_() {
		return primarySanctionedAmount_;
	}
	public void setPrimarySanctionedAmount_(String primarySanctionedAmount_) {
		this.primarySanctionedAmount_ = primarySanctionedAmount_;
	}
	public String getPrimarySecuredNumberOfAccount_() {
		return primarySecuredNumberOfAccount_;
	}
	public void setPrimarySecuredNumberOfAccount_(
			String primarySecuredNumberOfAccount_) {
		this.primarySecuredNumberOfAccount_ = primarySecuredNumberOfAccount_;
	}
	public String getPrimaryUnsecuredNumberOfAccount_() {
		return primaryUnsecuredNumberOfAccount_;
	}
	public void setPrimaryUnsecuredNumberOfAccount_(
			String primaryUnsecuredNumberOfAccount_) {
		this.primaryUnsecuredNumberOfAccount_ = primaryUnsecuredNumberOfAccount_;
	}
	public String getPrimaryUntaggedNumberOfAccount_() {
		return primaryUntaggedNumberOfAccount_;
	}
	public void setPrimaryUntaggedNumberOfAccount_(
			String primaryUntaggedNumberOfAccount_) {
		this.primaryUntaggedNumberOfAccount_ = primaryUntaggedNumberOfAccount_;
	}
	public String getSecondaryVNumberOfAccount_() {
		return secondaryVNumberOfAccount_;
	}
	public void setSecondaryVNumberOfAccount_(String secondaryVNumberOfAccount_) {
		this.secondaryVNumberOfAccount_ = secondaryVNumberOfAccount_;
	}
	public String getSecondaryActiveNumberOfAccount_() {
		return secondaryActiveNumberOfAccount_;
	}
	public void setSecondaryActiveNumberOfAccount_(
			String secondaryActiveNumberOfAccount_) {
		this.secondaryActiveNumberOfAccount_ = secondaryActiveNumberOfAccount_;
	}
	public String getSecondaryOverdueNumberOfAccount_() {
		return secondaryOverdueNumberOfAccount_;
	}
	public void setSecondaryOverdueNumberOfAccount_(
			String secondaryOverdueNumberOfAccount_) {
		this.secondaryOverdueNumberOfAccount_ = secondaryOverdueNumberOfAccount_;
	}
	public String getSecondaryCurrentBalance_() {
		return secondaryCurrentBalance_;
	}
	public void setSecondaryCurrentBalance_(String secondaryCurrentBalance_) {
		this.secondaryCurrentBalance_ = secondaryCurrentBalance_;
	}
	public String getSecondarySanctionedAmount_() {
		return secondarySanctionedAmount_;
	}
	public void setSecondarySanctionedAmount_(String secondarySanctionedAmount_) {
		this.secondarySanctionedAmount_ = secondarySanctionedAmount_;
	}
	public String getSecondarySecuredNumberOfAccount_() {
		return secondarySecuredNumberOfAccount_;
	}
	public void setSecondarySecuredNumberOfAccount_(
			String secondarySecuredNumberOfAccount_) {
		this.secondarySecuredNumberOfAccount_ = secondarySecuredNumberOfAccount_;
	}
	public String getSecondaryUnsecuredNumberOfAccount_() {
		return secondaryUnsecuredNumberOfAccount_;
	}
	public void setSecondaryUnsecuredNumberOfAccount_(
			String secondaryUnsecuredNumberOfAccount_) {
		this.secondaryUnsecuredNumberOfAccount_ = secondaryUnsecuredNumberOfAccount_;
	}
	public String getSecondaryUntaggedNumberOfAccount_() {
		return secondaryUntaggedNumberOfAccount_;
	}
	public void setSecondaryUntaggedNumberOfAccount_(
			String secondaryUntaggedNumberOfAccount_) {
		this.secondaryUntaggedNumberOfAccount_ = secondaryUntaggedNumberOfAccount_;
	}
	public String getInquriesInLastSixMonth_() {
		return inquriesInLastSixMonth_;
	}
	public void setInquriesInLastSixMonth_(String inquriesInLastSixMonth_) {
		this.inquriesInLastSixMonth_ = inquriesInLastSixMonth_;
	}
	public String getLengthOfCreditHistoryYear_() {
		return lengthOfCreditHistoryYear_;
	}
	public void setLengthOfCreditHistoryYear_(String lengthOfCreditHistoryYear_) {
		this.lengthOfCreditHistoryYear_ = lengthOfCreditHistoryYear_;
	}
	public String getLengthOfCreditHistoryMonth_() {
		return lengthOfCreditHistoryMonth_;
	}
	public void setLengthOfCreditHistoryMonth_(String lengthOfCreditHistoryMonth_) {
		this.lengthOfCreditHistoryMonth_ = lengthOfCreditHistoryMonth_;
	}
	public String getAverageAccountAgeYear_() {
		return averageAccountAgeYear_;
	}
	public void setAverageAccountAgeYear_(String averageAccountAgeYear_) {
		this.averageAccountAgeYear_ = averageAccountAgeYear_;
	}
	public String getAverageAccountAgeMonth_() {
		return averageAccountAgeMonth_;
	}
	public void setAverageAccountAgeMonth_(String averageAccountAgeMonth_) {
		this.averageAccountAgeMonth_ = averageAccountAgeMonth_;
	}
	public String getNewAccountInLastSixMonth_() {
		return newAccountInLastSixMonth_;
	}
	public void setNewAccountInLastSixMonth_(String newAccountInLastSixMonth_) {
		this.newAccountInLastSixMonth_ = newAccountInLastSixMonth_;
	}
	public String getNewDelinqAccountInLastSixMonth_() {
		return newDelinqAccountInLastSixMonth_;
	}
	public void setNewDelinqAccountInLastSixMonth_(
			String newDelinqAccountInLastSixMonth_) {
		this.newDelinqAccountInLastSixMonth_ = newDelinqAccountInLastSixMonth_;
	}
	public String getVariationType_() {
		return variationType_;
	}
	public void setVariationType_(String variationType_) {
		this.variationType_ = variationType_;
	}
	public String getValue_() {
		return value_;
	}
	public void setValue_(String value_) {
		this.value_ = value_;
	}
	public String getReportedDateVrtn_() {
		return reportedDateVrtn_;
	}
	public void setReportedDateVrtn_(String reportedDateVrtn_) {
		this.reportedDateVrtn_ = reportedDateVrtn_;
	}
	public String getMatchedType_() {
		return matchedType_;
	}
	public void setMatchedType_(String matchedType_) {
		this.matchedType_ = matchedType_;
	}
	public String getAccountNumber_() {
		return accountNumber_;
	}
	public void setAccountNumber_(String accountNumber_) {
		this.accountNumber_ = accountNumber_;
	}
	public String getCreditGuarantor_() {
		return creditGuarantor_;
	}
	public void setCreditGuarantor_(String creditGuarantor_) {
		this.creditGuarantor_ = creditGuarantor_;
	}
	public String getAcctType_() {
		return acctType_;
	}
	public void setAcctType_(String acctType_) {
		this.acctType_ = acctType_;
	}
	public String getDateReported_() {
		return dateReported_;
	}
	public void setDateReported_(String dateReported_) {
		this.dateReported_ = dateReported_;
	}
	public String getOwnershipIndicator_() {
		return ownershipIndicator_;
	}
	public void setOwnershipIndicator_(String ownershipIndicator_) {
		this.ownershipIndicator_ = ownershipIndicator_;
	}
	public String getAccountStatus_() {
		return accountStatus_;
	}
	public void setAccountStatus_(String accountStatus_) {
		this.accountStatus_ = accountStatus_;
	}
	public String getDisbursedAmount_() {
		return disbursedAmount_;
	}
	public void setDisbursedAmount_(String disbursedAmount_) {
		this.disbursedAmount_ = disbursedAmount_;
	}
	public String getDisbursedDate_() {
		return disbursedDate_;
	}
	public void setDisbursedDate_(String disbursedDate_) {
		this.disbursedDate_ = disbursedDate_;
	}
	public String getLastPaymentDate_() {
		return lastPaymentDate_;
	}
	public void setLastPaymentDate_(String lastPaymentDate_) {
		this.lastPaymentDate_ = lastPaymentDate_;
	}
	public String getClosedDate_() {
		return closedDate_;
	}
	public void setClosedDate_(String closedDate_) {
		this.closedDate_ = closedDate_;
	}
	public String getInstallmentAmount_() {
		return installmentAmount_;
	}
	public void setInstallmentAmount_(String installmentAmount_) {
		this.installmentAmount_ = installmentAmount_;
	}
	public String getOverdueAmount_() {
		return overdueAmount_;
	}
	public void setOverdueAmount_(String overdueAmount_) {
		this.overdueAmount_ = overdueAmount_;
	}
	public String getWriteOffAmount_() {
		return writeOffAmount_;
	}
	public void setWriteOffAmount_(String writeOffAmount_) {
		this.writeOffAmount_ = writeOffAmount_;
	}
	public String getCurrentBalance_() {
		return currentBalance_;
	}
	public void setCurrentBalance_(String currentBalance_) {
		this.currentBalance_ = currentBalance_;
	}
	public String getCreditLimit_() {
		return creditLimit_;
	}
	public void setCreditLimit_(String creditLimit_) {
		this.creditLimit_ = creditLimit_;
	}
	public String getAccountRemarks_() {
		return accountRemarks_;
	}
	public void setAccountRemarks_(String accountRemarks_) {
		this.accountRemarks_ = accountRemarks_;
	}
	public String getFrequency_() {
		return frequency_;
	}
	public void setFrequency_(String frequency_) {
		this.frequency_ = frequency_;
	}
	public String getSecurityStatus_() {
		return securityStatus_;
	}
	public void setSecurityStatus_(String securityStatus_) {
		this.securityStatus_ = securityStatus_;
	}
	public String getOriginalTerm_() {
		return originalTerm_;
	}
	public void setOriginalTerm_(String originalTerm_) {
		this.originalTerm_ = originalTerm_;
	}
	public String getTermToMaturity_() {
		return termToMaturity_;
	}
	public void setTermToMaturity_(String termToMaturity_) {
		this.termToMaturity_ = termToMaturity_;
	}
	public String getAccountInDispute_() {
		return accountInDispute_;
	}
	public void setAccountInDispute_(String accountInDispute_) {
		this.accountInDispute_ = accountInDispute_;
	}
	public String getSettlementAmount_() {
		return settlementAmount_;
	}
	public void setSettlementAmount_(String settlementAmount_) {
		this.settlementAmount_ = settlementAmount_;
	}
	public String getPrincipalWriteOffAmount_() {
		return principalWriteOffAmount_;
	}
	public void setPrincipalWriteOffAmount_(String principalWriteOffAmount_) {
		this.principalWriteOffAmount_ = principalWriteOffAmount_;
	}
	public String getCombinedPaymentHistory_() {
		return combinedPaymentHistory_;
	}
	public void setCombinedPaymentHistory_(String combinedPaymentHistory_) {
		this.combinedPaymentHistory_ = combinedPaymentHistory_;
	}
	public String getLinkedAccounts_() {
		return linkedAccounts_;
	}
	public void setLinkedAccounts_(String linkedAccounts_) {
		this.linkedAccounts_ = linkedAccounts_;
	}
	public String getSecurityType_() {
		return securityType_;
	}
	public void setSecurityType_(String securityType_) {
		this.securityType_ = securityType_;
	}
	public String getOwnerName_() {
		return ownerName_;
	}
	public void setOwnerName_(String ownerName_) {
		this.ownerName_ = ownerName_;
	}
	public String getSecurityValue_() {
		return securityValue_;
	}
	public void setSecurityValue_(String securityValue_) {
		this.securityValue_ = securityValue_;
	}
	public String getDateOfValue_() {
		return dateOfValue_;
	}
	public void setDateOfValue_(String dateOfValue_) {
		this.dateOfValue_ = dateOfValue_;
	}
	public String getSecurityCharge_() {
		return securityCharge_;
	}
	public void setSecurityCharge_(String securityCharge_) {
		this.securityCharge_ = securityCharge_;
	}
	public String getPropertyAddress_() {
		return propertyAddress_;
	}
	public void setPropertyAddress_(String propertyAddress_) {
		this.propertyAddress_ = propertyAddress_;
	}
	public String getAutomobileType_() {
		return automobileType_;
	}
	public void setAutomobileType_(String automobileType_) {
		this.automobileType_ = automobileType_;
	}
	public String getYearOfManufacture_() {
		return yearOfManufacture_;
	}
	public void setYearOfManufacture_(String yearOfManufacture_) {
		this.yearOfManufacture_ = yearOfManufacture_;
	}
	public String getRegistrationNumber_() {
		return registrationNumber_;
	}
	public void setRegistrationNumber_(String registrationNumber_) {
		this.registrationNumber_ = registrationNumber_;
	}
	public String getEngineNumber_() {
		return engineNumber_;
	}
	public void setEngineNumber_(String engineNumber_) {
		this.engineNumber_ = engineNumber_;
	}
	public String getChassisNumber_() {
		return chassisNumber_;
	}
	public void setChassisNumber_(String chassisNumber_) {
		this.chassisNumber_ = chassisNumber_;
	}
	public String getNameIndtl_() {
		return nameIndtl_;
	}
	public void setNameIndtl_(String nameIndtl_) {
		this.nameIndtl_ = nameIndtl_;
	}
	public String getAddress_() {
		return address_;
	}
	public void setAddress_(String address_) {
		this.address_ = address_;
	}
	public String getDobIndtl_() {
		return dobIndtl_;
	}
	public void setDobIndtl_(String dobIndtl_) {
		this.dobIndtl_ = dobIndtl_;
	}
	public String getPhoneIndtl_() {
		return phoneIndtl_;
	}
	public void setPhoneIndtl_(String phoneIndtl_) {
		this.phoneIndtl_ = phoneIndtl_;
	}
	public String getPanIndtl_() {
		return panIndtl_;
	}
	public void setPanIndtl_(String panIndtl_) {
		this.panIndtl_ = panIndtl_;
	}
	public String getPassportIndtl_() {
		return passportIndtl_;
	}
	public void setPassportIndtl_(String passportIndtl_) {
		this.passportIndtl_ = passportIndtl_;
	}
	public String getDrivingLicenseIndtl_() {
		return drivingLicenseIndtl_;
	}
	public void setDrivingLicenseIndtl_(String drivingLicenseIndtl_) {
		this.drivingLicenseIndtl_ = drivingLicenseIndtl_;
	}
	public String getVoterIdIndtl_() {
		return voterIdIndtl_;
	}
	public void setVoterIdIndtl_(String voterIdIndtl_) {
		this.voterIdIndtl_ = voterIdIndtl_;
	}
	public String getEmailIndtl_() {
		return emailIndtl_;
	}
	public void setEmailIndtl_(String emailIndtl_) {
		this.emailIndtl_ = emailIndtl_;
	}
	public String getRationCardIndtl_() {
		return rationCardIndtl_;
	}
	public void setRationCardIndtl_(String rationCardIndtl_) {
		this.rationCardIndtl_ = rationCardIndtl_;
	}
	public String getMemberName_() {
		return memberName_;
	}
	public void setMemberName_(String memberName_) {
		this.memberName_ = memberName_;
	}
	public String getInquiryDate_() {
		return inquiryDate_;
	}
	public void setInquiryDate_(String inquiryDate_) {
		this.inquiryDate_ = inquiryDate_;
	}
	public String getPurpose_() {
		return purpose_;
	}
	public void setPurpose_(String purpose_) {
		this.purpose_ = purpose_;
	}
	public String getOwnershipType_() {
		return ownershipType_;
	}
	public void setOwnershipType_(String ownershipType_) {
		this.ownershipType_ = ownershipType_;
	}
	public String getAmount_() {
		return amount_;
	}
	public void setAmount_(String amount_) {
		this.amount_ = amount_;
	}
	public String getRemark_() {
		return remark_;
	}
	public void setRemark_(String remark_) {
		this.remark_ = remark_;
	}
	public String getCommentText_() {
		return commentText_;
	}
	public void setCommentText_(String commentText_) {
		this.commentText_ = commentText_;
	}
	public String getCommentDate_() {
		return commentDate_;
	}
	public void setCommentDate_(String commentDate_) {
		this.commentDate_ = commentDate_;
	}
	public String getBureauComment_() {
		return bureauComment_;
	}
	public void setBureauComment_(String bureauComment_) {
		this.bureauComment_ = bureauComment_;
	}
	public String getAlertType_() {
		return alertType_;
	}
	public void setAlertType_(String alertType_) {
		this.alertType_ = alertType_;
	}
	public String getAlertDescription_() {
		return alertDescription_;
	}
	public void setAlertDescription_(String alertDescription_) {
		this.alertDescription_ = alertDescription_;
	}
	public Long getManualUploadBatchId_() {
		return manualUploadBatchId_;
	}
	public void setManualUploadBatchId_(Long manualUploadBatchId_) {
		this.manualUploadBatchId_ = manualUploadBatchId_;
	}
	
	public Long getRequestId_() {
		return requestId_;
	}

	public void setRequestId_(Long requestId_) {
		this.requestId_ = requestId_;
	}

	public String getFileName_() {
		return fileName_;
	}
	public void setFileName_(String fileName_) {
		this.fileName_ = fileName_;
	}
	
	public String getAccountKey_() {
		return accountKey_;
	}

	public void setAccountKey_(String accountKey_) {
		this.accountKey_ = accountKey_;
	}

	public String getAccountParentKey_() {
		return accountParentKey_;
	}

	public void setAccountParentKey_(String accountParentKey_) {
		this.accountParentKey_ = accountParentKey_;
	}

	public String getApplicationId() {
		return applicationId_;
	}

	public void setApplicationId(String applicationId) {
		applicationId_ = applicationId;
	}

	public String getCustomerId() {
		return customerId_;
	}

	public void setCustomerId(String customerId) {
		customerId_ = customerId;
	}

	@Override
	public String toString() {
		return "HighmarkSROPBaseDomain [srNo_=" + srNo_ + ", soaSourceName_="
				+ soaSourceName_ + ", memberReferenceNumber_="
				+ memberReferenceNumber_ + ", requestDateTime_="
				+ requestDateTime_ + ", responseDateTime_=" + responseDateTime_
				+ ", responseType_=" + responseType_ + ", description_="
				+ description_ + ", reportType_=" + reportType_
				+ ", reportDate_=" + reportDate_ + ", inqCountFile_="
				+ inqCountFile_ + ", responseCountFile_=" + responseCountFile_
				+ ", dateOfRequest_=" + dateOfRequest_ + ", preparedFor_="
				+ preparedFor_ + ", preparedForId_=" + preparedForId_
				+ ", dateOfIssue_=" + dateOfIssue_ + ", reportId_=" + reportId_
				+ ", batchId_=" + batchId_ + ", status_=" + status_
				+ ", nameIQ_=" + nameIQ_ + ", aka_=" + aka_ + ", spouse_="
				+ spouse_ + ", father_=" + father_ + ", mother_=" + mother_
				+ ", dobIQ_=" + dobIQ_ + ", age_=" + age_ + ", ageAsOn_="
				+ ageAsOn_ + ", rationCard_=" + rationCard_ + ", passport_="
				+ passport_ + ", votersId_=" + votersId_
				+ ", drivingLicenceNo_=" + drivingLicenceNo_ + ", pan_=" + pan_
				+ ", gender_=" + gender_ + ", ownership_=" + ownership_
				+ ", address1_=" + address1_ + ", address2_=" + address2_
				+ ", address3_=" + address3_ + ", phone1_=" + phone1_
				+ ", phone2_=" + phone2_ + ", phone3_=" + phone3_
				+ ", emailId1_=" + emailId1_ + ", emailId2_=" + emailId2_
				+ ", branch_=" + branch_ + ", kendra_=" + kendra_ + ", mbrId_="
				+ mbrId_ + ", losAppId_=" + losAppId_
				+ ", creditInquiryPurposeType_=" + creditInquiryPurposeType_
				+ ", creditInquiryPurposeTypeDesc_="
				+ creditInquiryPurposeTypeDesc_ + ", creditInquiryStage_="
				+ creditInquiryStage_ + ", creditReportId_=" + creditReportId_
				+ ", creditRequestType_=" + creditRequestType_
				+ ", creditReportTransectionDateTime_="
				+ creditReportTransectionDateTime_ + ", accountOpenDate_="
				+ accountOpenDate_ + ", loanAmount_=" + loanAmount_
				+ ", primaryNumberOfAccount_=" + primaryNumberOfAccount_
				+ ", primaryActiveNoOfAccount_=" + primaryActiveNoOfAccount_
				+ ", primaryOverdueNumberOfAccount_="
				+ primaryOverdueNumberOfAccount_ + ", primaryCurrentBalance_="
				+ primaryCurrentBalance_ + ", primarySanctionedAmount_="
				+ primarySanctionedAmount_
				+ ", primarySecuredNumberOfAccount_="
				+ primarySecuredNumberOfAccount_
				+ ", primaryUnsecuredNumberOfAccount_="
				+ primaryUnsecuredNumberOfAccount_
				+ ", primaryUntaggedNumberOfAccount_="
				+ primaryUntaggedNumberOfAccount_
				+ ", secondaryVNumberOfAccount_=" + secondaryVNumberOfAccount_
				+ ", secondaryActiveNumberOfAccount_="
				+ secondaryActiveNumberOfAccount_
				+ ", secondaryOverdueNumberOfAccount_="
				+ secondaryOverdueNumberOfAccount_
				+ ", secondaryCurrentBalance_=" + secondaryCurrentBalance_
				+ ", secondarySanctionedAmount_=" + secondarySanctionedAmount_
				+ ", secondarySecuredNumberOfAccount_="
				+ secondarySecuredNumberOfAccount_
				+ ", secondaryUnsecuredNumberOfAccount_="
				+ secondaryUnsecuredNumberOfAccount_
				+ ", secondaryUntaggedNumberOfAccount_="
				+ secondaryUntaggedNumberOfAccount_
				+ ", inquriesInLastSixMonth_=" + inquriesInLastSixMonth_
				+ ", lengthOfCreditHistoryYear_=" + lengthOfCreditHistoryYear_
				+ ", lengthOfCreditHistoryMonth_="
				+ lengthOfCreditHistoryMonth_ + ", averageAccountAgeYear_="
				+ averageAccountAgeYear_ + ", averageAccountAgeMonth_="
				+ averageAccountAgeMonth_ + ", newAccountInLastSixMonth_="
				+ newAccountInLastSixMonth_
				+ ", newDelinqAccountInLastSixMonth_="
				+ newDelinqAccountInLastSixMonth_ + ", variationType_="
				+ variationType_ + ", value_=" + value_
				+ ", reportedDateVrtn_=" + reportedDateVrtn_
				+ ", matchedType_=" + matchedType_ + ", accountNumber_="
				+ accountNumber_ + ", creditGuarantor_=" + creditGuarantor_
				+ ", acctType_=" + acctType_ + ", dateReported_="
				+ dateReported_ + ", ownershipIndicator_="
				+ ownershipIndicator_ + ", accountStatus_=" + accountStatus_
				+ ", disbursedAmount_=" + disbursedAmount_
				+ ", disbursedDate_=" + disbursedDate_ + ", lastPaymentDate_="
				+ lastPaymentDate_ + ", closedDate_=" + closedDate_
				+ ", installmentAmount_=" + installmentAmount_
				+ ", overdueAmount_=" + overdueAmount_ + ", writeOffAmount_="
				+ writeOffAmount_ + ", currentBalance_=" + currentBalance_
				+ ", creditLimit_=" + creditLimit_ + ", accountRemarks_="
				+ accountRemarks_ + ", frequency_=" + frequency_
				+ ", securityStatus_=" + securityStatus_ + ", originalTerm_="
				+ originalTerm_ + ", termToMaturity_=" + termToMaturity_
				+ ", accountInDispute_=" + accountInDispute_
				+ ", settlementAmount_=" + settlementAmount_
				+ ", principalWriteOffAmount_=" + principalWriteOffAmount_
				+ ", combinedPaymentHistory_=" + combinedPaymentHistory_
				+ ", linkedAccounts_=" + linkedAccounts_ + ", securityType_="
				+ securityType_ + ", ownerName_=" + ownerName_
				+ ", securityValue_=" + securityValue_ + ", dateOfValue_="
				+ dateOfValue_ + ", securityCharge_=" + securityCharge_
				+ ", propertyAddress_=" + propertyAddress_
				+ ", automobileType_=" + automobileType_
				+ ", yearOfManufacture_=" + yearOfManufacture_
				+ ", registrationNumber_=" + registrationNumber_
				+ ", engineNumber_=" + engineNumber_ + ", chassisNumber_="
				+ chassisNumber_ + ", nameIndtl_=" + nameIndtl_ + ", address_="
				+ address_ + ", dobIndtl_=" + dobIndtl_ + ", phoneIndtl_="
				+ phoneIndtl_ + ", panIndtl_=" + panIndtl_
				+ ", passportIndtl_=" + passportIndtl_
				+ ", drivingLicenseIndtl_=" + drivingLicenseIndtl_
				+ ", voterIdIndtl_=" + voterIdIndtl_ + ", emailIndtl_="
				+ emailIndtl_ + ", rationCardIndtl_=" + rationCardIndtl_
				+ ", memberName_=" + memberName_ + ", inquiryDate_="
				+ inquiryDate_ + ", purpose_=" + purpose_ + ", ownershipType_="
				+ ownershipType_ + ", amount_=" + amount_ + ", remark_="
				+ remark_ + ", commentText_=" + commentText_
				+ ", commentDate_=" + commentDate_ + ", bureauComment_="
				+ bureauComment_ + ", alertType_=" + alertType_
				+ ", alertDescription_=" + alertDescription_
				+ ", manualUploadBatchId_=" + manualUploadBatchId_
				+ ", requestId_=" + requestId_ + ", fileName_=" + fileName_
				+ ", accountKey_=" + accountKey_ + ", accountParentKey_="
				+ accountParentKey_ + "]";
	}

}