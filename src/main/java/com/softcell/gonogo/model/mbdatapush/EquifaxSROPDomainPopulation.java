package com.softcell.gonogo.model.mbdatapush;

public class EquifaxSROPDomainPopulation {
	/*private EquifaxUtils equifaxUtils ;
	
	
	public EquifaxSROPDomainPopulation() {
		this.equifaxUtils = new EquifaxUtils();
	}

	
	public List<EquifaxSROPDomain> generateEquifaxDomainFlatStrcture(com.softcell.codexs.EquifaxResponse.InquiryResponse inquiryResponse, RequestDomain domain,Date pickipDate)throws Exception{
		return generateEquifaxDomainFlatStrcture_(inquiryResponse, domain,pickipDate);
	}
	
	private List<EquifaxSROPDomain> generateEquifaxDomainFlatStrcture_(com.softcell.codexs.EquifaxResponse.InquiryResponse inquiryResponse, RequestDomain domain,Date pickipDate) throws Exception{
		List<EquifaxSROPDomain>  equifaxSROPDomains = new ArrayList<EquifaxSROPDomain>();
		final String enquiryDateFormat = "yyyyMMdd";
		try{
			
			String srNo = domain.getRequestId().toString();
			String soaSourceName = domain.getSourceSystemName();
			String memberReferenceNumber = domain.getUnqRefNo();
			String enquiryDate = DateUtils.getFormattedDate(pickipDate,enquiryDateFormat);
			String customerCode =null;
			
			
			if(inquiryResponse!=null && inquiryResponse.getInquiryResponseHeader()!=null ){
				com.softcell.codexs.EquifaxResponse.InquiryResponseHeader inquiryResponseHeader = inquiryResponse.getInquiryResponseHeader();
				customerCode  = inquiryResponseHeader.getCustomerCode();
			}
			
			EquifaxSROPDomain  equifaxSROPDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
			
			if(inquiryResponse!=null && inquiryResponse.getInquiryResponseHeader()!=null ){
				com.softcell.codexs.EquifaxResponse.InquiryResponseHeader inquiryResponseHeader = inquiryResponse.getInquiryResponseHeader();
				equifaxSROPDomain.setClientIdRsHd(inquiryResponseHeader.getClientID());
				equifaxSROPDomain.setCustRefFieldRsHd(inquiryResponseHeader.getCustRefField());
				equifaxSROPDomain.setReportOrderNoRsHd(inquiryResponseHeader.getReportOrderNO());
				equifaxSROPDomain.setProductCodeRsHd(inquiryResponseHeader.getProductCode());
				equifaxSROPDomain.setProductVersionRsHd(inquiryResponseHeader.getProductVersion());
				equifaxSROPDomain.setSuccessCodeRsHd(inquiryResponseHeader.getSuccessCode());
				equifaxSROPDomain.setMatchTypeRsHd(inquiryResponseHeader.getMatchType());
				equifaxSROPDomain.setResDateRsHd(inquiryResponseHeader.getDate());
				equifaxSROPDomain.setResTimeRsHd(inquiryResponseHeader.getTime());
				equifaxSROPDomain.setOutputWriteTime(DateUtils.getFormattedDate(new Date(), "ddMMyyyy HH:mm:ss"));
				equifaxSROPDomain.setOutputWriteFlag("0");
			}
			
			
			if(inquiryResponse!=null){
				if(inquiryResponse.getReportData()!=null){
					
					com.softcell.codexs.EquifaxResponse.ReportData reportData = inquiryResponse.getReportData();
					
					if(reportData!=null){
						Integer counter = 0;
						
						com.softcell.codexs.EquifaxResponse.IDAndContactInfo idAndContactInfo = reportData.getIdAndContactInfo();
						if(idAndContactInfo!=null){
							com.softcell.codexs.EquifaxResponse.PersonalInfo personalInfo = idAndContactInfo.getPersonalInfo();
							if(personalInfo!=null){
								if(personalInfo.getName() != null){
									com.softcell.codexs.EquifaxResponse.Name name = personalInfo.getName();
									equifaxSROPDomain.setFirstnameNmTp(name.getFirstName());
									equifaxSROPDomain.setMidlleNameNmTp(name.getMiddleName());
									equifaxSROPDomain.setLastNameNmTp(name.getLastName());
									equifaxSROPDomain.setAddMidlleNameNmTp(name.getAdditionalMiddleName());
									equifaxSROPDomain.setSuffixNmTp(name.getSuffix());	
								}
								equifaxSROPDomains.add(equifaxSROPDomain);
								
								com.softcell.codexs.EquifaxResponse.AgeInfo ageInfo = personalInfo.getAgeInfo();
								if(null != ageInfo){
									equifaxSROPDomains.get(0).setReportedDateAgIf(ageInfo.getReportedDate());
									equifaxSROPDomains.get(0).setAgeAgIf(ageInfo.getAge());
								}
								
								com.softcell.codexs.EquifaxResponse.AliasNameInfo aliasNameInfo = personalInfo.getAliasNameInfo();
								
								if(aliasNameInfo != null){
									equifaxSROPDomains.get(0).setAliasNameAlNm(aliasNameInfo.getAliasName());
									equifaxSROPDomains.get(0).setReportedDateAlNm(aliasNameInfo.getReportedDate());
								}
								
								equifaxSROPDomains.get(0).setDobRsPerIf(personalInfo.getDateOfBirth());
								equifaxSROPDomains.get(0).setGenderRsPerIf(personalInfo.getGender());
								equifaxSROPDomains.get(0).setTotalIncomeRsPerIf(personalInfo.getTotalIncome());
								equifaxSROPDomains.get(0).setOccupationRsPerIf(personalInfo.getOccupation());
								equifaxSROPDomains.get(0).setMaritalStausRsPerIf(personalInfo.getMaritalStatus());
								
							}
							
							com.softcell.codexs.EquifaxResponse.FamilyInfo familyDetails = idAndContactInfo.getFamilyDetails();
							if(null != familyDetails){
								List<com.softcell.codexs.EquifaxResponse.AdditionalNameInfo> additionalNameInfoList = familyDetails.getAdditionalNameInfoList();
								if(null != additionalNameInfoList && additionalNameInfoList.size() > 0){
									for (Integer i = 0; i < additionalNameInfoList.size() ; i++) {
										if( i > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(i).setAdditionalNameInfoAdNmIf(additionalNameInfoList.get(i).getAdditionalName());
									}
								}
								equifaxSROPDomains.get(0).setNoOfDependentsAdNmIf(familyDetails.getNoOfDependents());
							}
							
							*//*if(familyDetails!=null && familyDetails.getAdditionalNameInfo()!=null){
								AdditionalNameInfo additionalNameInfo = familyDetails.getAdditionalNameInfo();
								if(additionalNameInfo!=null){
									equifaxSROPDomains.get(0).setAdditionalNameInfoAdNmIf(additionalNameInfo.getAdditionalName());
								}
								equifaxSROPDomains.get(0).setNoOfDependentsAdNmIf(familyDetails.getNoOfDependents());
							}*//*

							List<com.softcell.codexs.EquifaxResponse.AddressInfo> addressInfo = idAndContactInfo.getAddressInfo();
							if(addressInfo!=null && addressInfo.size() > 0){
								for(Integer i=0;i<addressInfo.size();i++){
									if(i > counter){
										EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
										equifaxSROPDomains.add(equifaxSROPNewDomain);
										counter++;
									}
									equifaxSROPDomains.get(i).setReportedDateRsAdd(addressInfo.get(i).getReportedDate());
									equifaxSROPDomains.get(i).setAddressRsAdd(addressInfo.get(i).getAddress());
									equifaxSROPDomains.get(i).setStateRsAdd(addressInfo.get(i).getState());
									equifaxSROPDomains.get(i).setPostalRsAdd(addressInfo.get(i).getPostal());
									equifaxSROPDomains.get(i).setAddTypeRsAdd(addressInfo.get(i).getType());
								}
						    }
							com.softcell.codexs.EquifaxResponse.IdentityInfo identityInfo= idAndContactInfo.getIdentityInfo();
							if(null != identityInfo ){
								
								Integer idCnt = -1;
								
								List<com.softcell.codexs.EquifaxResponse.DriverLicence> driverLicenceList = identityInfo.getDriverLicence();
								if(driverLicenceList !=null && driverLicenceList.size() > 0){
									for(Integer i=0;i<driverLicenceList.size();i++){
										
										idCnt++;
										
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("DRIVING");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(driverLicenceList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(driverLicenceList.get(i).getIdNumber());
									}
								}
								List<com.softcell.codexs.EquifaxResponse.NationalIDCard>  nationalIDCardList = identityInfo.getNationalIDCard();
								if(nationalIDCardList !=null && nationalIDCardList.size() > 0){
									for(Integer i=0;i<nationalIDCardList.size();i++){
										
										idCnt++;
										
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("NATIONAL");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(nationalIDCardList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(nationalIDCardList.get(i).getIdNumber());
									}
								}
								List<com.softcell.codexs.EquifaxResponse.IDCard> idCardList = identityInfo.getIdCard();
								if(idCardList !=null && idCardList.size() > 0){
									for(Integer i=0;i<idCardList.size();i++){
										
										idCnt++;
										
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("ID");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(idCardList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(idCardList.get(i).getIdNumber());
									}
								}
								
								List<com.softcell.codexs.EquifaxResponse.IDOther> idOtherList = identityInfo.getIdOther();
								if(idOtherList !=null && idOtherList.size() > 0){
									for(Integer i=0;i<idOtherList.size();i++){
										
										idCnt++;
										
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("OTHER");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(idOtherList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(idOtherList.get(i).getIdNumber());
									}
								}
								List<com.softcell.codexs.EquifaxResponse.PanId> panIdList = identityInfo.getPanId();
								if(panIdList !=null && panIdList.size() > 0){
									for(Integer i=0;i<panIdList.size();i++){
										idCnt++;
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("PAN");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(panIdList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(panIdList.get(i).getIdNumber());
									}
								}
								
								List<com.softcell.codexs.EquifaxResponse.PassportId> passportIdList = identityInfo.getPassportID();
								if(passportIdList !=null && passportIdList.size() > 0){
									for(Integer i=0;i<passportIdList.size();i++){
										
										idCnt++;
										
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("PASSPORT");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(passportIdList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(passportIdList.get(i).getIdNumber());
									}
								}
								List<com.softcell.codexs.EquifaxResponse.PhotoCreditCard> photoCreditCardList = identityInfo.getPhotoCreditCard();
								if(photoCreditCardList !=null && photoCreditCardList.size() > 0){
									for(Integer i=0;i<photoCreditCardList.size();i++){
										idCnt++;
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("PHOTO");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(photoCreditCardList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(photoCreditCardList.get(i).getIdNumber());
									}
								}
								List<com.softcell.codexs.EquifaxResponse.RationCard> rationCardList = identityInfo.getRationCard();
								if(rationCardList !=null && rationCardList.size() > 0){
									for(Integer i=0;i<rationCardList.size();i++){
										idCnt++;
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("RATION");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(rationCardList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(rationCardList.get(i).getIdNumber());
									}
								}
								List<com.softcell.codexs.EquifaxResponse.VoterId> voterIdList = identityInfo.getVoterId(); 
								if(voterIdList !=null && voterIdList.size() > 0){
									for(Integer i=0;i<voterIdList.size();i++){
										
										idCnt++;
										
										if(idCnt > counter){
											EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
											equifaxSROPDomains.add(equifaxSROPNewDomain);
											counter++;
										}
										equifaxSROPDomains.get(idCnt).setIdTypeRsId("VOTER");
										equifaxSROPDomains.get(idCnt).setReportedDateRsId(voterIdList.get(i).getReportedDate());
										equifaxSROPDomains.get(idCnt).setIdNumberRsId(voterIdList.get(i).getIdNumber());
									}
								}
							}
							List<com.softcell.codexs.EquifaxResponse.EmailAddressInfo> emailAddressInfoList = idAndContactInfo.getEmailAddressInfo();
							if(emailAddressInfoList!=null && emailAddressInfoList.size() > 0){
								for(Integer i = 0 ; i < emailAddressInfoList.size() ; i++){
									if(i> counter){
										EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
										equifaxSROPDomains.add(equifaxSROPNewDomain);
										counter++;
									}
									equifaxSROPDomains.get(i).setEmailReportedDateRsMl(emailAddressInfoList.get(i).getReportedDate());
									equifaxSROPDomains.get(i).setEmailAddressRsMl(emailAddressInfoList.get(i).getEmailAddress());
								}
							}
							List<com.softcell.codexs.EquifaxResponse.PhoneInfo> phoneInfoList = idAndContactInfo.getPhoneInfo();
							if(phoneInfoList!=null && phoneInfoList.size() > 0){
								for(Integer i = 0 ; i < phoneInfoList.size() ; i++){
									if(i> counter){
										EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
										equifaxSROPDomains.add(equifaxSROPNewDomain);
										counter++;
									}
									equifaxSROPDomains.get(i).setReportedDateRsPh(phoneInfoList.get(i).getPhoneReportedDate());
									equifaxSROPDomains.get(i).setTypeCodeRsPh(phoneInfoList.get(i).getPhoneTypeCode());
									equifaxSROPDomains.get(i).setCountryCodeRsPh(phoneInfoList.get(i).getCountryCode());
									equifaxSROPDomains.get(i).setAreaCodeRsPh(phoneInfoList.get(i).getAreaCode());
									equifaxSROPDomains.get(i).setPhNumberRsPh(phoneInfoList.get(i).getNumber());
									equifaxSROPDomains.get(i).setPhNoExtentionRsPh(phoneInfoList.get(i).getPhoneNumberExtension());
									
								}
							}
						}
						
						List<com.softcell.codexs.EquifaxResponse.IncomeDetails> incomeDetailList = reportData.getIncomeDetailList();
						if(incomeDetailList!=null && incomeDetailList.size() > 0 ){
							for(Integer i=0 ; i<incomeDetailList.size();i++){
								if(i > counter){
									EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
									equifaxSROPDomains.add(equifaxSROPNewDomain);
									counter++;
								}
								equifaxSROPDomains.get(i).setOccupationIcDl(incomeDetailList.get(i).getOccupation());
								equifaxSROPDomains.get(i).setMonthlyIncomeIcDl(incomeDetailList.get(i).getMonthlyIncome());
								equifaxSROPDomains.get(i).setMonthlyExpenseIcDl(incomeDetailList.get(i).getMonthlyExpense());
								equifaxSROPDomains.get(i).setPovertyIndexIcDl(incomeDetailList.get(i).getPovertyIndex());
								equifaxSROPDomains.get(i).setAssetOwnershipIcDl(incomeDetailList.get(i).getAssetOwnership());
							}
						}
						
						List<com.softcell.codexs.EquifaxResponse.EmployerDetails> employmentInfoList = reportData.getEmploymentInfoList();
						if(null != employmentInfoList && employmentInfoList.size() > 0 ){
							for (Integer i = 0; i < employmentInfoList.size() ; i++) {
								if(i > counter){
									EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
									equifaxSROPDomains.add(equifaxSROPNewDomain);
									counter++;
								}
								equifaxSROPDomains.get(i).setEmployerNameRsEmdl(employmentInfoList.get(i).getEmployerName());
								equifaxSROPDomains.get(i).setEmpNMRepDateRsEMDL(employmentInfoList.get(i).getReportedDate());
								equifaxSROPDomains.get(i).setEmpPositionRsEmdl(employmentInfoList.get(i).getPosition());
								com.softcell.codexs.EquifaxResponse.Address address = employmentInfoList.get(i).getAddress();
								if(null != address){
									equifaxSROPDomains.get(i).setEmpAddressRsEmdl(address.getAddress());
								}
							}
						
						}
						
						com.softcell.codexs.EquifaxResponse.AccountDetails accountDetails = reportData.getAccountDetails();
						if(null != accountDetails){
							List<com.softcell.codexs.EquifaxResponse.Account> accountList = accountDetails.getAccountList();
							if(accountList!=null && accountList.size() > 0){
								Integer accountKey = 0;
								Integer accountPos = -1;
								
								for (Integer i = 0; i < accountList.size(); i++) {
									
									accountPos++;
									
									String accountNumber = accountList.get(i).getAccountNumber();
									if(accountPos > counter){
										EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate, customerCode, accountNumber, accountKey.toString());
										equifaxSROPDomains.add(equifaxSROPNewDomain);
										counter++;
									}
									equifaxSROPDomains.get(accountPos).setAccountKey(accountKey.toString());
									equifaxSROPDomains.get(accountPos).setAccountNumberRsAc(accountList.get(i).getAccountNumber()); 
									equifaxSROPDomains.get(accountPos).setClientNameRsAc(accountList.get(i).getClientName());
									equifaxSROPDomains.get(accountPos).setCurrentBalanceRsAc(accountList.get(i).getCurrentBalance());
									equifaxSROPDomains.get(accountPos).setInstitutionRsAc(accountList.get(i).getInstitution());
									equifaxSROPDomains.get(accountPos).setAccountTypeRsAc(accountList.get(i).getAccountType());
									equifaxSROPDomains.get(accountPos).setOwnershipTypeRsAc(accountList.get(i).getOwnershipType());
									equifaxSROPDomains.get(accountPos).setBalanceRsAc(accountList.get(i).getBalance());
									equifaxSROPDomains.get(accountPos).setPastDueAmtRsAc(accountList.get(i).getPastDueAmount());
									equifaxSROPDomains.get(accountPos).setDisbursedAmountRsAc(accountList.get(i).getDisbursedAmount());
									equifaxSROPDomains.get(accountPos).setLoanCategoryRsAc(accountList.get(i).getLoanCategory());
									equifaxSROPDomains.get(accountPos).setLoanPurposeRsAc(accountList.get(i).getLoanPurpose());
									equifaxSROPDomains.get(accountPos).setLastPaymentRsAc(accountList.get(i).getLastPayment());
									equifaxSROPDomains.get(accountPos).setWriteOffAmtRsAc(accountList.get(i).getWriteOffAmount());
									equifaxSROPDomains.get(accountPos).setAccOpenRsAc(accountList.get(i).getOpen());
									equifaxSROPDomains.get(accountPos).setSactionAmtRsAc(accountList.get(i).getSanctionAmount());
									equifaxSROPDomains.get(accountPos).setLastPaymentDateRsAc(accountList.get(i).getLastPaymentDate());
									equifaxSROPDomains.get(accountPos).setHighCreditRsAc(accountList.get(i).getHighCredit());
									equifaxSROPDomains.get(accountPos).setDateReportedRsAc(accountList.get(i).getDateReported());
									equifaxSROPDomains.get(accountPos).setDateOpenedRsAc(accountList.get(i).getDateOpened());
									equifaxSROPDomains.get(accountPos).setDateClosedRsAc(accountList.get(i).getDateClosed());
									equifaxSROPDomains.get(accountPos).setReasonRsAc(accountList.get(i).getReason());
									equifaxSROPDomains.get(accountPos).setDateWrittenOffRsAc(accountList.get(i).getDateWrittenOff());
									equifaxSROPDomains.get(accountPos).setLoanCycleIdRsAc(accountList.get(i).getLoanCycleID());
									equifaxSROPDomains.get(accountPos).setDateSanctionedRsAc(accountList.get(i).getDateSanctioned());
									equifaxSROPDomains.get(accountPos).setDateAppliedRsAc(accountList.get(i).getDateApplied());
									equifaxSROPDomains.get(accountPos).setIntrestRateRSAC(accountList.get(i).getInterestRate());
									equifaxSROPDomains.get(accountPos).setAppliedAmountRsAc(accountList.get(i).getAppliedAmount());
									equifaxSROPDomains.get(accountPos).setNoOfInstallmentsRsAc(accountList.get(i).getNoOfInstallments());
									equifaxSROPDomains.get(accountPos).setRepaymentTenureRsAc(accountList.get(i).getRepaymentTenure());
									equifaxSROPDomains.get(accountPos).setDisputeCodeRsAc(accountList.get(i).getDisputeCode());
									equifaxSROPDomains.get(accountPos).setInstallmentAmtRsAc(accountList.get(i).getInstallmentAmount());
									equifaxSROPDomains.get(accountPos).setTermFrequencyRsAc(accountList.get(i).getTermFrequency());
									equifaxSROPDomains.get(accountPos).setCreditLimitRsAc(accountList.get(i).getCreditLimit());
									equifaxSROPDomains.get(accountPos).setCollateralValueRsAc(accountList.get(i).getCollateralValue());
									equifaxSROPDomains.get(accountPos).setCollateralTypeRsAc(accountList.get(i).getCollateralType());
									equifaxSROPDomains.get(accountPos).setAccountStatusRsAc(accountList.get(i).getAccountStatus());
									equifaxSROPDomains.get(accountPos).setAssetClassificationRsAc(accountList.get(i).getAssetClassification());
									equifaxSROPDomains.get(accountPos).setSuitFiledStatusRsAc(accountList.get(i).getSuitFiledStatus());
									equifaxSROPDomains.get(accountPos).setReportedDateRsAc(accountList.get(i).getDateReported());
									
									
									
									com.softcell.codexs.EquifaxResponse.KeyPerson keyPerson = accountList.get(i).getKeyPerson();
									if(keyPerson!=null && keyPerson.getRelationInfoType()!=null){
										com.softcell.codexs.EquifaxResponse.RelationInfo relationInfoType = keyPerson.getRelationInfoType();
										if(null != relationInfoType){
											equifaxSROPDomains.get(accountPos).setKeyPersonRsAc(relationInfoType.getName());
										}
										
									}
									
									com.softcell.codexs.EquifaxResponse.Nominee nominee = accountList.get(i).getNominee();
									if(null != nominee){
										com.softcell.codexs.EquifaxResponse.RelationInfo relationInfoType = nominee.getRelationInfoType();
										if(null != relationInfoType){
											equifaxSROPDomains.get(accountPos).setNomineeRsAc(relationInfoType.getName());
										}
									}
									
									Integer history24Pos = accountPos;
									Integer history48Pos = accountPos;
									
									com.softcell.codexs.EquifaxResponse.History24Months history24Months = accountList.get(i).getHistory24Months();
									if(null != history24Months){
										 List<com.softcell.codexs.EquifaxResponse.Month> month = history24Months.getMonth();
										if(null != month && month.size() > 0){
											for(Integer j = 0 ; j<month.size() ; j++){
												
												if(history24Pos > counter){
													EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate, customerCode, accountNumber, accountKey.toString());
													equifaxSROPDomains.add(equifaxSROPNewDomain);
													counter++;
												}
												equifaxSROPDomains.get(history24Pos).setAccountKey(accountKey.toString());
												equifaxSROPDomains.get(history24Pos).setAccountNumberRsAc(accountList.get(i).getAccountNumber()); 
												equifaxSROPDomains.get(history24Pos).setMonthKeyRsAcHis24(month.get(j).getMonthKey());
												equifaxSROPDomains.get(history24Pos).setPaymentStatusRsAcHis24(month.get(j).getPaymentStatus());
												equifaxSROPDomains.get(history24Pos).setSuitFiledStatusRsAcHis24(month.get(j).getSuitFiledStatus());
												equifaxSROPDomains.get(history24Pos).setAssettClsStatusRsAcHis24(month.get(j).getAssetClassificationStatus());
												
												history24Pos++;
											}
										}
									}
									
									com.softcell.codexs.EquifaxResponse.History48Months history48Months = accountList.get(i).getHistory48Months();
									if(null != history48Months){
										List<com.softcell.codexs.EquifaxResponse.Month> month = history48Months.getMonth();
										if(null != month && month.size() > 0){
											for(Integer j = 0 ; j<month.size() ; j++){
												if(history48Pos > counter){
													EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate, customerCode, accountNumber, accountKey.toString());
													equifaxSROPDomains.add(equifaxSROPNewDomain);
													counter++;
												}
												equifaxSROPDomains.get(history48Pos).setAccountKey(accountKey.toString());
												equifaxSROPDomains.get(history48Pos).setAccountNumberRsAc(accountList.get(i).getAccountNumber()); 
												equifaxSROPDomains.get(history48Pos).setMonthKeyRsAcHis48(month.get(j).getMonthKey());
												equifaxSROPDomains.get(history48Pos).setpAYMENTSTATUSRsAcHis48(month.get(j).getPaymentStatus());   
												equifaxSROPDomains.get(history48Pos).setsUITFILEDSTATUSRsAcHis48(month.get(j).getSuitFiledStatus()); 
												equifaxSROPDomains.get(history48Pos).setaSSETCLSSTATUSRsAcHis48(month.get(j).getAssetClassificationStatus());
												
												history48Pos++;
											}                             
										}
									}
									
									Integer newProposedActPos = history24Pos>history48Pos?history24Pos:history48Pos;
									
									if (newProposedActPos > accountPos) {
										accountPos = newProposedActPos - 1;
									}
									
									accountKey ++;
								}
							}
						}
						
						com.softcell.codexs.EquifaxResponse.AccountSummary accountSummary = reportData.getAccountSummary();
						if(null != accountSummary){
							equifaxSROPDomains.get(0).setNoOfAccountsRsAcSum(accountSummary.getNoOfAccounts());
							equifaxSROPDomains.get(0).setNoOfActiveAccountsRsAcSum(accountSummary.getNoOfActiveAccounts());
							equifaxSROPDomains.get(0).setNoOfWriteOffsRsAcSum(accountSummary.getNoOfWriteOffs());
							equifaxSROPDomains.get(0).setTotalPastDueRsAcSum(accountSummary.getTotalPastDue());
							equifaxSROPDomains.get(0).setMostSrvrStaWhin24MonRsAcSum(accountSummary.getMostSevereStatusWithIn24Months());
							equifaxSROPDomains.get(0).setSingleHighestCreditRsAcSum(accountSummary.getSingleHighestCredit());
							equifaxSROPDomains.get(0).setSingleHighSanctAmtRsAcSum(accountSummary.getSingleHighestSanctionAmount());
							equifaxSROPDomains.get(0).setTotalHighCreditRsAcSum(accountSummary.getTotalHighCredit());
							equifaxSROPDomains.get(0).setAverageOpenBalanceRsAcSum(accountSummary.getAverageOpenBalance());
							equifaxSROPDomains.get(0).setSingleHighestBalanceRsAcSum(accountSummary.getSingleHighestBalance());
							equifaxSROPDomains.get(0).setNoOfPastDueAcctsRsAcSum(accountSummary.getNoOfPastDueAccounts());
							equifaxSROPDomains.get(0).setNoOfZeroBalAcctsRsAcSum(accountSummary.getNoOfZeroBalanceAccounts());
							equifaxSROPDomains.get(0).setRecentAccountRsAcSum(accountSummary.getRecentAccount());
							equifaxSROPDomains.get(0).setOldestAccountRsAcSum(accountSummary.getOldestAccount());
							equifaxSROPDomains.get(0).setTotalBalAmtRsAcSum(accountSummary.getTotalBalanceAmount());
							equifaxSROPDomains.get(0).setTotalSanctAmtRsAcSum(accountSummary.getTotalSanctionAmount());
							equifaxSROPDomains.get(0).setTotalCrdtLimitRsAcSum(accountSummary.getTotalCreditLimit());
							equifaxSROPDomains.get(0).setTotalMonlyPymntAmtRsAcSum(accountSummary.getTotalMonthlyPaymentAmount());
							equifaxSROPDomains.get(0).setTotalWrittenOffAmntRsAcSum(accountSummary.getTotalWrittenOffAmount());
						}
						
						com.softcell.codexs.EquifaxResponse.OtherKeyInd otherKeyInd = reportData.getOtherKeyInd();
						if(null != otherKeyInd){
							equifaxSROPDomains.get(0).setAgeOfOldestTradeRsOtk(otherKeyInd.getAgeOfOldestTrade());
							equifaxSROPDomains.get(0).setNumberOfOpenTradesRsOtk(otherKeyInd.getNumberOfOpenTrades());
							equifaxSROPDomains.get(0).setAlineSeverWrittenRsOtk(otherKeyInd.getAllLinesEVERWritten());
							equifaxSROPDomains.get(0).setAlineSvWrtnIn9MnthsRsOtk(otherKeyInd.getAllLinesEVERWrittenIn9Months());
							equifaxSROPDomains.get(0).setAlineSvrWrTnIn6MnthsRsOtk(otherKeyInd.getAllLinesEVERWrittenIn6Months());
						}
						
						com.softcell.codexs.EquifaxResponse.EnquirySummary enquirySummary = reportData.getEnquirySummary();
						if(null != enquirySummary){
							equifaxSROPDomains.get(0).setReportedDateRsEqSm(enquirySummary.getReportedDate());
							equifaxSROPDomains.get(0).setPurposeRsEqSm(enquirySummary.getPurpose());
							equifaxSROPDomains.get(0).setTotalRsEqSm(enquirySummary.getTotal());
							equifaxSROPDomains.get(0).setPast30DaysRsEqSm(enquirySummary.getPast30Days());
							equifaxSROPDomains.get(0).setPast12MonthsRsEqSm(enquirySummary.getPast12Months());
							equifaxSROPDomains.get(0).setPast24MonthsRsEqSm(enquirySummary.getPast24Months());
							equifaxSROPDomains.get(0).setRecentRsEqSm(enquirySummary.getRecent());
						} 
						
						List<com.softcell.codexs.EquifaxResponse.Enquiries> enquiriesList = reportData.getEnquiriesList();
						if(enquiriesList!=null && enquiriesList.size() >  0){
							for(Integer i =0 ; i <enquiriesList.size() ; i++){
								if(i > counter){
									EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
									equifaxSROPDomains.add(equifaxSROPNewDomain);
									counter++;
								}
								equifaxSROPDomains.get(i).setReportedDateRsEq(enquiriesList.get(i).getReportedDate());
								equifaxSROPDomains.get(i).setEnqReferenceRsEq(enquiriesList.get(i).getInstitution());
								equifaxSROPDomains.get(i).setEnqDateRsEq(enquiriesList.get(i).getDate());
								equifaxSROPDomains.get(i).setEnqTimeRsEq(enquiriesList.get(i).getTime());
								equifaxSROPDomains.get(i).setRequestPurposeRsEq(equifaxUtils.getInquiryPurpose(enquiriesList.get(i).getRequestPurpose()));
								equifaxSROPDomains.get(i).setAmountRsEq(enquiriesList.get(i).getAmount());
							}
						}
						
						com.softcell.codexs.EquifaxResponse.RecentActivities recentActivities = reportData.getRecentActivities();
						if(null != recentActivities){
							equifaxSROPDomains.get(0).setAccountsDeliquentRsReAct(recentActivities.getAccountsDeliquent());
							equifaxSROPDomains.get(0).setAccountsOpenedRsReAct(recentActivities.getAccountsOpened());
							equifaxSROPDomains.get(0).setTotalInquiriesRsReAct(recentActivities.getTotalInquiries());
							equifaxSROPDomains.get(0).setAccountsUpdatedRsReAct(recentActivities.getAccountsUpdated());
						}
						
						List<com.softcell.codexs.EquifaxResponse.GroupCreditSummary> groupCreditSummaryList = reportData.getGroupCreditSummaryList();
						if(groupCreditSummaryList!=null && groupCreditSummaryList.size() >  0){
							for(Integer i =0 ; i <groupCreditSummaryList.size() ; i++){
								if(i > counter){
									EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
									equifaxSROPDomains.add(equifaxSROPNewDomain);
									counter++;
								}
								equifaxSROPDomains.get(i).setInstitutionGrpSm(groupCreditSummaryList.get(i).getInstitution());
								equifaxSROPDomains.get(i).setCurrentBalanceGrpSm(groupCreditSummaryList.get(i).getCurrentBalance());
								equifaxSROPDomains.get(i).setStatusGrpSm(groupCreditSummaryList.get(i).getStatus());
								equifaxSROPDomains.get(i).setDateReportedGrpSm(groupCreditSummaryList.get(i).getDateReported());
								equifaxSROPDomains.get(i).setNoOfMembersGrpSm(groupCreditSummaryList.get(i).getNoOfMembers());
								equifaxSROPDomains.get(i).setPastDueAmountGrpSm(groupCreditSummaryList.get(i).getPastDueAmount());
								equifaxSROPDomains.get(i).setSanctionAmountGrpSm(groupCreditSummaryList.get(i).getSanctionAmount());
								equifaxSROPDomains.get(i).setDateOpenedGrpSm(groupCreditSummaryList.get(i).getDateOpened());
								equifaxSROPDomains.get(i).setAccountNoGrpSm(groupCreditSummaryList.get(i).getAccountNo());
								equifaxSROPDomains.get(i).setMembersPastDueGrpSm(groupCreditSummaryList.get(i).getMembersPastDue());
								equifaxSROPDomains.get(i).setWriteOffAmountGrpSm(groupCreditSummaryList.get(i).getWriteOffAmount());
								equifaxSROPDomains.get(i).setWriteOffDateGrpSm(groupCreditSummaryList.get(i).getWriteOffDate());
							}
						}
						
						*//*List<com.softcell.codexs.EquifaxResponse.Score> scoreList = reportData.getScore();
						if(null != scoreList && scoreList.size() > 0 ){
							for (Integer i = 0; i < scoreList.size(); i++) {
								if(i > counter){
									EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
									equifaxSROPDomains.add(equifaxSROPNewDomain);
									counter++;
								}
								equifaxSROPDomains.get(i).setReasonCodeSc(scoreList.get(i).getReasonCode());	
						        equifaxSROPDomains.get(i).setScoreDescriptionSc(scoreList.get(i).getDescription());
								equifaxSROPDomains.get(i).setScoreNameSc(scoreList.get(i).getName());
								equifaxSROPDomains.get(i).setScoreValueSc(scoreList.get(i).getValue());
							}
						}*//*
						
						Score score = reportData.getScore();
						if(score!=null){
							equifaxSROPDomains.get(0).setScoreDescriptionSc(score.getDescription());
							equifaxSROPDomains.get(0).setScoreNameSc(score.getName());
							equifaxSROPDomains.get(0).setScoreValueSc(score.getValue());
						}
						
						ScoringElements scoringElement = reportData.getScoringElement();
						if(scoringElement !=null){
							List<ScoringElement> scoringElementList = scoringElement.getScoringElementList();
							if(scoringElementList!=null&& scoringElementList.size()>0 ){
								for(Integer i=0;i<scoringElementList.size();i++){
									if(i > counter){
										EquifaxSROPDomain  equifaxSROPNewDomain  = new EquifaxSROPDomain(srNo, soaSourceName, memberReferenceNumber, enquiryDate,customerCode);
										equifaxSROPDomains.add(equifaxSROPNewDomain);
										counter++;
									}									
									equifaxSROPDomains.get(i).setCode(scoringElementList.get(i).getCode());
									equifaxSROPDomains.get(i).setDescription(scoringElementList.get(i).getDescription());
									
								}
																
							}
						}
						
						
						ConsumerDisputes consumerDisputes = reportData.getConsumerDisputes();
						List<Dispute> disputeList = consumerDisputes.getDisputeList();
						String accntNum = null;
						String disputeComments = null;
						String status = null;
						String resolvedDate = null;
					if(	disputeList !=null && disputeList.size() > 0)
					{
						for(Integer i=0;i<disputeList.size();i++)
						{
							List<DisputeDetails> disputeDetailsList = disputeList.get(i).getDisputeDetailsList();
							if(disputeDetailsList != null && disputeDetailsList.size()>0){
							for(Integer j = 0 ; j<disputeDetailsList.size() ; j++){
							
								if(StringUtils.isBlank(accntNum)){
									accntNum = disputeDetailsList.get(j).getAccntNum();
								}else{
									accntNum += ","+disputeDetailsList.get(j).getAccntNum();
								}
								if(StringUtils.isBlank(disputeComments)){
									disputeComments = disputeDetailsList.get(j).getDisputeComments();
								}else{
									disputeComments += ","+disputeDetailsList.get(j).getDisputeComments();
								}
								if(StringUtils.isBlank(status)){
									status = disputeDetailsList.get(j).getStatus();
								}else{
									status += ","+disputeDetailsList.get(j).getStatus();
								}
								if(StringUtils.isBlank(resolvedDate)){
									resolvedDate = disputeDetailsList.get(j).getResolvedDate();
								}else{
									resolvedDate += ","+disputeDetailsList.get(j).getResolvedDate();
								}
							  }   
							
							}
							equifaxSROPDomains.get(i).setAccntNum(accntNum);
							equifaxSROPDomains.get(i).setDisputeComments(disputeComments);
							equifaxSROPDomains.get(i).setStatus(status);
							equifaxSROPDomains.get(i).setResolvedDate(resolvedDate);
							
						}
						
					} 
						
						
						
					}
				}
				equifaxSROPDomains.get(0).setOutputWriteFlag("Y"); 
			}
			return equifaxSROPDomains;
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception(""+e.getMessage()); 
		}
	}*/

	
}
