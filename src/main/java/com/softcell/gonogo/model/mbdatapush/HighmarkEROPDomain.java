package com.softcell.gonogo.model.mbdatapush;

public class HighmarkEROPDomain {

	private String  srNo_;                           
	private String  soaSourceName_;                
	private String  memberReferenceNo_;        
	private String  enquiryDate_;                   
	private String  errorCode_;                     
	private String  errorMessage_;                  
	private String  errorDetails_;                  
	private String  outputWriteFlag_;              
	private String  outputWriteTime_;              
	private String  outputReadTime_;
	
	
	
	public String getSrNo() {
		return srNo_;
	}
	public void setSrNo(String srNo) {
		srNo_ = srNo;
	}
	public String getSoaSourceName() {
		return soaSourceName_;
	}
	public void setSoaSourceName(String soaSourceName) {
		soaSourceName_ = soaSourceName;
	}
	public String getMemberReferenceNo() {
		return memberReferenceNo_;
	}
	public void setMemberReferenceNo(String memberReferenceNo) {
		memberReferenceNo_ = memberReferenceNo;
	}
	public String getEnquiryDate() {
		return enquiryDate_;
	}
	public void setEnquiryDate(String enquiryDate) {
		enquiryDate_ = enquiryDate;
	}
	public String getErrorCode() {
		return errorCode_;
	}
	public void setErrorCode(String errorCode) {
		errorCode_ = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage_;
	}
	public void setErrorMessage(String errorMessage) {
		errorMessage_ = errorMessage;
	}
	public String getErrorDetails() {
		return errorDetails_;
	}
	public void setErrorDetails(String errorDetails) {
		errorDetails_ = errorDetails;
	}
	public String getOutputWriteFlag() {
		return outputWriteFlag_;
	}
	public void setOutputWriteFlag(String outputWriteFlag) {
		outputWriteFlag_ = outputWriteFlag;
	}
	public String getOutputWriteTime() {
		return outputWriteTime_;
	}
	public void setOutputWriteTime(String outputWriteTime) {
		outputWriteTime_ = outputWriteTime;
	}
	public String getOutputReadTime() {
		return outputReadTime_;
	}
	public void setOutputReadTime(String outputReadTime) {
		outputReadTime_ = outputReadTime;
	}

	
	@Override
	public String toString() {
		return "HighmarkEROPDomain [srNo_=" + srNo_ + ", soaSourceName_="
				+ soaSourceName_ + ", memberReferenceNo_=" + memberReferenceNo_
				+ ", enquiryDate_=" + enquiryDate_ + ", errorCode_="
				+ errorCode_ + ", errorMessage_=" + errorMessage_
				+ ", errorDetails_=" + errorDetails_ + ", outputWriteFlag_="
				+ outputWriteFlag_ + ", outputWriteTime_=" + outputWriteTime_
				+ ", outputReadTime_=" + outputReadTime_ + "]";
	}
}
