package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.List;

public class InquiryHistory {
	
	private List<History> history;

	public List<History> getHistory() {
		return history;
	}

	public void setHistory(List<History> history) {
		this.history = history;
	}

	@Override
	public String toString() {
		return "InquiryHistory [history=" + history + "]";
	}
	
	
	
	
	

}
