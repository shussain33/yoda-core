package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.List;

public class Scores {
	
	private List<Score> scoreList ;

	public List<Score> getScoreList() {
		return scoreList;
	}

	public void setScoreList(List<Score> scoreList) {
		this.scoreList = scoreList;
	}

	@Override
	public String toString() {
		return "Scores [scoreList=" + scoreList + "]";
	}
}