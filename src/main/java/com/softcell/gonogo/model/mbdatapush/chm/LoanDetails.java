package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.ArrayList;
import java.util.List;


public class LoanDetails {
	
	private String accountNumber;
	private String creditGuarantor;
	private String accountType;
	private String dateReported;
	private String ownershipIndicator;
	private String accountStatus;
	private String disbursedAmount;
	private String disbursedDate;
	private String lastPaymentDate;
	private String closedDate;
	private String installmentAmount;
	private String writeOffAmount;
	private String overdueAmount;
	private String currentBalance;
	private String creditLimit;
	private String accountRemarks; 
	private String frequency ;
	private String securityStatus ;
	private String originalTerm ;
	private String termToMaturity;
	private String accountInDispute;
	private String settlementAmt;
	private String principalWriteOffAmt;
	private String combinedPaymentHistory;
	private String matchedType;
	/*private LoanDetails linkedAccounts;
	private SecurityDetails securityDetails;
	*/
	private List<LoanDetails> accountVariations_ = new ArrayList<LoanDetails>();
	private List<SecurityDetails> securityDetailList = new ArrayList<SecurityDetails>();
	private String repaymentTenure;
	private String cashLimit;
	private String actualPayment;
	private String suitFiledWilfulDefault;
	private String writtenOffSetteldStatus;
	
	
	public String getRepaymentTenure() {
		return repaymentTenure;
	}
	public void setRepaymentTenure(String repaymentTenure) {
		this.repaymentTenure = repaymentTenure;
	}
	public String getCashLimit() {
		return cashLimit;
	}
	public void setCashLimit(String cashLimit) {
		this.cashLimit = cashLimit;
	}
	public String getActualPayment() {
		return actualPayment;
	}
	public void setActualPayment(String actualPayment) {
		this.actualPayment = actualPayment;
	}
	public String getSuitFiledWilfulDefault() {
		return suitFiledWilfulDefault;
	}
	public void setSuitFiledWilfulDefault(String suitFiledWilfulDefault) {
		this.suitFiledWilfulDefault = suitFiledWilfulDefault;
	}
	public String getWrittenOffSetteldStatus() {
		return writtenOffSetteldStatus;
	}
	public void setWrittenOffSetteldStatus(String writtenOffSetteldStatus) {
		this.writtenOffSetteldStatus = writtenOffSetteldStatus;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCreditGuarantor() {
		return creditGuarantor;
	}
	public void setCreditGuarantor(String creditGuarantor) {
		this.creditGuarantor = creditGuarantor;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getDateReported() {
		return dateReported;
	}
	public void setDateReported(String dateReported) {
		this.dateReported = dateReported;
	}
	public String getOwnershipIndicator() {
		return ownershipIndicator;
	}
	public void setOwnershipIndicator(String ownershipIndicator) {
		this.ownershipIndicator = ownershipIndicator;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getDisbursedAmount() {
		return disbursedAmount;
	}
	public void setDisbursedAmount(String disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}
	public String getDisbursedDate() {
		return disbursedDate;
	}
	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}
	public String getLastPaymentDate() {
		return lastPaymentDate;
	}
	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}
	public String getClosedDate() {
		return closedDate;
	}
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}
	public String getOverdueAmount() {
		return overdueAmount;
	}
	public void setOverdueAmount(String overdueAmount) {
		this.overdueAmount = overdueAmount;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}
	public String getAccountRemarks() {
		return accountRemarks;
	}
	public void setAccountRemarks(String accountRemarks) {
		this.accountRemarks = accountRemarks;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getSecurityStatus() {
		return securityStatus;
	}
	public void setSecurityStatus(String securityStatus) {
		this.securityStatus = securityStatus;
	}
	public String getOriginalTerm() {
		return originalTerm;
	}
	public void setOriginalTerm(String originalTerm) {
		this.originalTerm = originalTerm;
	}
	public String getTermToMaturity() {
		return termToMaturity;
	}
	public void setTermToMaturity(String termToMaturity) {
		this.termToMaturity = termToMaturity;
	}
	public String getAccounttInDispute() {
		return accountInDispute;
	}
	public void setAccountInDispute(String accountInDispute) {
		this.accountInDispute = accountInDispute;
	}
	public String getSettlementAmt() {
		return settlementAmt;
	}
	public void setSettlementAmt(String settlementAmt) {
		this.settlementAmt = settlementAmt;
	}
	public String getPrincipalWriteOffAmt() {
		return principalWriteOffAmt;
	}
	public void setPrincipalWriteOffAmt(String principalWriteOffAmt) {
		this.principalWriteOffAmt = principalWriteOffAmt;
	}
	public String getCombinedPaymentHistory() {
		return combinedPaymentHistory;
	}
	public void setCombinedPaymentHistory(String combinedPaymentHistory) {
		this.combinedPaymentHistory = combinedPaymentHistory;
	}
	public String getMatchedType() {
		return matchedType;
	}
	public void setMatchedType(String matchedType) {
		this.matchedType = matchedType;
	}
	
	/*public LoanDetails getLinkedAccounts() {
		return linkedAccounts;
	}
	public void setLinkedAccounts(LoanDetails linkedAccounts) {
		this.linkedAccounts = linkedAccounts;
	}
	public SecurityDetails getSecurityDetails() {
		return securityDetails;
	}
	public void setSecurityDetails(SecurityDetails securityDetails) {
		this.securityDetails = securityDetails;
	}*/
	public String getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	
	public String getWriteOffAmount() {
		return writeOffAmount;
	}
	public void setWriteOffAmount(String writeOffAmount) {
		this.writeOffAmount = writeOffAmount;
	}
	public String getAccountInDispute() {
		return accountInDispute;
	}
	public List<LoanDetails> getAccountVariations_() {
		return accountVariations_;
	}
	public void setAccountVariations_(List<LoanDetails> accountVariations_) {
		this.accountVariations_ = accountVariations_;
	}
	public List<SecurityDetails> getSecurityDetailList() {
		return securityDetailList;
	}
	public void setSecurityDetailList(List<SecurityDetails> securityDetailList) {
		this.securityDetailList = securityDetailList;
	}
	/*@Override
	public String toString() {
		return "LoanDetails [accountNumber=" + accountNumber
				+ ", creditGuarantor=" + creditGuarantor + ", accountType="
				+ accountType + ", dateReported=" + dateReported
				+ ", ownershipIndicator=" + ownershipIndicator
				+ ", accountStatus=" + accountStatus + ", disbursedAmount="
				+ disbursedAmount + ", disbursedDate=" + disbursedDate
				+ ", lastPaymentDate=" + lastPaymentDate + ", closedDate="
				+ closedDate + ", installmentAmount=" + installmentAmount
				+ ", writeOffAmount=" + writeOffAmount + ", overdueAmount="
				+ overdueAmount + ", currentBalance=" + currentBalance
				+ ", creditLimit=" + creditLimit + ", accountRemarks="
				+ accountRemarks + ", frequency=" + frequency
				+ ", securityStatus=" + securityStatus + ", originalTerm="
				+ originalTerm + ", termToMaturity=" + termToMaturity
				+ ", accountInDispute=" + accountInDispute + ", settlementAmt="
				+ settlementAmt + ", principalWriteOffAmt="
				+ principalWriteOffAmt + ", combinedPaymentHistory="
				+ combinedPaymentHistory + ", matchedType=" + matchedType
				+ ", linkedAccounts=" + linkedAccounts + ", securityDetails="
				+ securityDetails + "]";
	}*/

	@Override
	public String toString() {
		return "LoanDetails [accountNumber=" + accountNumber
				+ ", creditGuarantor=" + creditGuarantor + ", accountType="
				+ accountType + ", dateReported=" + dateReported
				+ ", ownershipIndicator=" + ownershipIndicator
				+ ", accountStatus=" + accountStatus + ", disbursedAmount="
				+ disbursedAmount + ", disbursedDate=" + disbursedDate
				+ ", lastPaymentDate=" + lastPaymentDate + ", closedDate="
				+ closedDate + ", installmentAmount=" + installmentAmount
				+ ", writeOffAmount=" + writeOffAmount + ", overdueAmount="
				+ overdueAmount + ", currentBalance=" + currentBalance
				+ ", creditLimit=" + creditLimit + ", accountRemarks="
				+ accountRemarks + ", frequency=" + frequency
				+ ", securityStatus=" + securityStatus + ", originalTerm="
				+ originalTerm + ", termToMaturity=" + termToMaturity
				+ ", accountInDispute=" + accountInDispute + ", settlementAmt="
				+ settlementAmt + ", principalWriteOffAmt="
				+ principalWriteOffAmt + ", combinedPaymentHistory="
				+ combinedPaymentHistory + ", matchedType=" + matchedType
				+ ", accountVariations_=" + accountVariations_
				+ ", securityDetailList=" + securityDetailList
				+ ", repaymentTenure=" + repaymentTenure + ", cashLimit="
				+ cashLimit + ", actualPayment=" + actualPayment
				+ ", suitFiledWilfulDefault=" + suitFiledWilfulDefault
				+ ", writtenOffSetteldStatus=" + writtenOffSetteldStatus + "]";
	}
	
	
}
