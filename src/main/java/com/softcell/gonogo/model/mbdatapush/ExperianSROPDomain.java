package com.softcell.gonogo.model.mbdatapush;


public class ExperianSROPDomain {
	
	private String  srNo;
	private String  soaSourceName;
    private String  memberReferenceNumber;
    private String  enquiryDate;
    private String  systemcodeHeader;
    private String  messageTextHeader;
    private String  reportdate;
    private String  reportimeHeader;
    private String  userMessageText;
    private String  enquiryUsername;
    private String  creditProfileReportDate;
    private String  CreditProfileReportTime;
    private String  version;
    private String  reportNumber;
    private String  subscriber;
    private String  subscriberName;
    private String  enquiryReason;
    private String  financePurpose;
    private String  amountFinanced;
    private String  durationOfAgreement;
    private String  lastname;
    private String  firstname;
    private String  middleName1;
    private String  middleName2;
    private String  middleName3;
    private String  genderCode;
    private String  gender;
    private String  incomeTaxPanApp;
    private String  panIssueDateApp;
    private String  panExpDateApp;
    private String  passportNumberApp;
    private String  passportIssueDateApp;
    private String  passportExpDateApp;
    private String  voterIdentityCardApp;
    private String  voterIdIssueDateApp;
    private String  voterIdExpDateApp;
    private String  driverLicenseNumberApp;
    private String  driverLicenseIssueDateApp;
    private String  driverLicenseExpDateApp;
    private String  rationCardNumberApp;
    private String  rationCardIssueDateApp;
    private String  rationCardExpDateApp;
    private String  universalIdNumberApp;
    private String  universalIdIssueDateApp;
    private String  universalIdExpDateApp;
    private String  dateOfBirthApplicant;
    private String  telephoneNumberApplicant1st;
    private String  teleExtensionApp;
    private String  teleTypeApp;
    private String  mobilePhoneNumber;
    private String  emailIdApp;
    private String  income;
    private String  maritalStatusCode;
    private String  maritalStatus;
    private String  employmentStatus;
    private String  dateWithEmployer;
    private String  numberOfMajorCreditCardHeld;
    private String  flatNoPlotNoHouseNo;
    private String  bldgNoSocietyName;
    private String  roadNoNameAreaLocality;
    private String  city;
    private String  landmark;
    private String  state;
    private String  pincode;
    private String  countryCode;
    private String  addFlatNoPlotNoHouseNo;
    private String  addBldgNoSocietyName;
    private String  addRoadNoNameAreaLocality;
    private String  addCity;
    private String  addLandmark;
    private String  addState;
    private String  addPincode;
    private String  addCountrycode;
    private String  creditAccountTotal;
    private String  creditAccountActive;
    private String  creditAccountDefault;
    private String  creditAccountClosed;
    private String  cadsuitfiledCurrentBalance;
    private String  outstandingBalanceSecured;
    private String  outstandingBalSecuredPer;
    private String  outstandingBalanceUnsecured;
    private String  outstandingBalUnsecPer;
    private String  outstandingBalanceAll;
    private String  identificationNumber;
    private String  caisSubscriberName;
    private String  accountNumber;
    private String  portfolioType;
    private String  accountTypeCode;
    private String  accountType;
    private String  openDate;
    private String  highestCreditOrOrgnLoanAmt;
    private String  termsDuration;
    private String  termsFrequency;
    private String  scheduledMonthlyPayamt;
    private String  accountStatusCode;
    private String  accountStatus;
    private String  paymentRating;
    private String  paymentHistoryProfile;
    private String  specialComment;
    private String  currentBalanceTl;
    private String  amountPastdueTl;
    private String  originalChargeOffAmount;
    private String  dateReported;
    private String  dateOfFirstDelinquency;
    private String  dateClosed;
    private String  dateOfLastPayment;
    private String  suitfilledWillFullDefaultWrittenOffStatus;
    private String  suitFiledWilfulDef;
    private String  writtenOffSettledStatus;
    private String  valueOfCreditsLastMonth;
    private String  occupationCode;
    private String  sattlementAmount;
    private String  valueOfColateral;
    private String  typeOfColateral;
    private String  writtenOffAmtTotal;
    private String  writtenOffAmtPrincipal;
    private String  rateOfInterest;
    private String  repaymentTenure;
    private String  promotionalRateFlag;
    private String  caisIncome;
    private String  incomeIndicator;
    private String  incomeFrequencyIndicator;
    private String  defaultStatusDate;
    private String  litigationStatusDate;
    private String  writeOffStatusDate;
    private String  currencyCode;
    private String  subscriberComments;
    private String  consumerComments;
    private String  accountHolderTypeCode;
    private String  accountHolderTypeName;
    private String  yearHist;
    private String  monthHist;
    private String  daysPastDue;
    private String  assetClassification;
    private String  yearAdvHist;
    private String  monthAdvHist;
    private String  cashLimit;
    private String  creditLimitAmt;
    private String  actualPaymentAmt;
    private String  emiAmt;
    private String  currentBalanceAdvHist;
    private String  amountPastDueAdvHist;
    private String  surNameNonNormalized;
    private String  firstNameNonNormalized;
    private String  middleName1NonNormalized;
    private String  middleName2NonNormalized;
    private String  middleName3NonNormalized;
    private String  alias;
    private String  caisGenderCode;
    private String  caisGender;
    private String  caisIncomeTaxPan;
    private String  caisPassportNumber;
    private String  voterIdNumber;
    private String  dateOfBirth;
    private String  firstLineOfAddNonNormalized;
    private String  secondLineOfAddNonNormalized;
    private String  thirdLineOfAddNonNormalized;
    private String  cityNonNormalized;
    private String  fifthLineOfAddNonNormalized;
    private String  stateCodeNonNormalized;
    private String  stateNonNormalized;
    private String  zipPostalCodeNonNormalized;
    private String  countryCodeNonNormalized;
    private String  addIndicatorNonNormalized;
    private String  residenceCodeNonNormalized;
    private String  residenceNonNormalized;
    private String  telephoneNumber;
    private String  teleTypePhone;
    private String  teleExtensionPhone;
    private String  mobileTelephoneNumber;
    private String  faxNumber;
    private String  emailIdPhone;
    private String  incomeTaxPanId;
    private String  panIssueDateId;
    private String  panExpDateId;
    private String  passportNumberId;
    private String  passportIssueDateId;
    private String  passportExpDateId;
    private String  voterIdentityCardId;
    private String  voterIdIssueDateId;
    private String  voterIdExpDateId;
    private String  driverLicenseNumberId;
    private String  driverLicenseIssueDateId;
    private String  driverLicenseExpDateId;
    private String  rationCardNumberId;
    private String  rationCardIssueDateId;
    private String  rationCardExpDateId;
    private String  universalIdNumberId;
    private String  universalIdIssueDateId;
    private String  universalIdExpDateId;
    private String  emailidId;
    private String  exactMatch;
    private String  capsLast7Days;
    private String  capsLast30Days;
    private String  capsLast90Days;
    private String  capsLast180Days;
    private String  capsSubscriberCode;
    private String  capsSubscriberName;
    private String  capsdateofRequest;
    private String  capsReportDate;
    private String  capsReportTime;
    private String  capsReportNumber;
    private String  enquiryReasonCode;
    private String  capsEnquiryReason;
    private String  financePurposeCode;
    private String  capsFinancePurpose;
    private String  capsAmountFinanced;
    private String  capsDurationOfAgreement;
    private String  capsApplicantLastName;
    private String  capsApplicantFirstName;
    private String  capsApplicantMiddleName1;
    private String  capsApplicantMiddleName2;
    private String  capsApplicantMiddleName3;
    private String  capsApplicantgenderCode;
    private String  capsApplicantGender;
    private String  capsIncomeTaxPan;
    private String  capsPanIssueDate;
    private String  capsPanExpDate;
    private String  capsPassportNumber;
    private String  capsPassportIssueDate;
    private String  capsPassportExpDate;
    private String  capsVoterIdentityCard;
    private String  capsVoterIdIssueDate;
    private String  capsVoterIdExpDate;
    private String  capsDriverLicenseNumber;
    private String  capsDriverLicenseIssueDate;
    private String  capsDriverLicenseExpDate;
    private String  capsRationCardNumber;
    private String  capsRationCardIssueDate;
    private String  capsRationCardExpDate;
    private String  capsUniversalIdNumber;
    private String  capsUniversalIdIssueDate;
    private String  capsUniversalIdExpDate;
    private String  capsApplicantDobApplicant;
    private String  capsApplicantTelNoApplicant1st;
    private String  capsApplicantTelExt;
    private String  capsApplicantTelType;
    private String  capsApplicantMobilePhoneNumber;
    private String  capsApplicantEmailId;
    private String  capsApplicantIncome;
    private String  applicantMaritalStatusCode;
    private String  capsApplicantMaritalStatus;
    private String  capsApplicantEmploymentStatusCode;
    private String  capsApplicantEmploymentStatus;
    private String  capsApplicantDateWithEmployer;
    private String  capsApplicantNoMajorCrditCardHeld;
    private String  capsApplicantFlatNoPlotNoHouseNo;
    private String  capsApplicantBldgNoSocietyName;
    private String  capsApplicantRoadNoNameAreaLocality;
    private String  capsApplicantCity;
    private String  capsApplicantLandmark;
    private String  capsApplicantStateCode;
    private String  capsApplicantState;
    private String  capsApplicantPinCode;
    private String  capsApplicantCountryCode;
    private String  bureauScore;
    private String  bureauScoreConfidLevel;
    private String  creditRating;
    private String  tnOfBFHLCADExhl;
    private String  totValOfBFHLCAD;
    private String  MNTSMRBFHLCAD;
    private String  tnOfHLCAD;
    private String  totValOfHLCAD;
    private String  mntsmrHLCAD;
    private String  tnOfTelcosCAD;
    private String  totValOfTelcosCad;
    private String  mntsmrTelcosCad;
    private String  tnOfmfCAD;
    private String  totValOfmfCAD;
    private String  mntsmrmfCAD;
    private String  tnOfRetailCAD;
    private String  totValOfRetailCAD;
    private String  mntsmrRetailCAD;
    private String  tnOfAllCAD;
    private String  totValOfAllCAD;
    private String  mntsmrCADAll;
    private String  tnOfBFHLACAExhl;
    private String  balBFHLACAExhl;
    private String  wcdstBFHLACAExhl;
    private String  wdspr6MntBFHLACAExhl;
    private String  wdspr712MntBFHLACAExhl;
    private String  ageOfOldestBFHLACAExhl;
    private String  hcbperrevaccBFHLACAExhl;
    private String  tcbperrevaccBFHLACAExhl;
    private String  tnOfHlACA;
    private String  balHlACA;
    private String  wcdstHlACA;
    private String  wdspr6MnthlACA;
    private String  wdspr712mnthlACA;
    private String  ageOfOldesthlACA;
    private String  tnOfMfACA;
    private String  totalBalMfACA;
    private String  wcdstMfACA;
    private String  wdspr6MntMfACA;
    private String  wdspr712mntMfACA;
    private String  ageOfOldestMfACA;
    private String  tnOfTelcosACA;
    private String  totalBalTelcosACA;
    private String  wcdstTelcosACA;
    private String  wdspr6mntTelcosACA;
    private String  wdspr712mntTelcosACA;
    private String  ageOfOldestTelcosACA;
    private String  tnOfRetailACA;
    private String  totalBalRetailACA;
    private String  wcdstRetailACA;
    private String  wdspr6mntRetailACA;
    private String  wdspr712mntRetailACA;
    private String  ageOfOldestRetailACA;
    private String  hcblmperrevaccret;
    private String  totCurBallmperrevaccret;
    private String  tnOfallACA;
    private String  balAllACAExhl;
    private String  wcdstAllACA;
    private String  wdspr6mntallACA;
    private String  wdspr712mntAllACA;
    private String  ageOfOldestAllACA;
    private String  tnOfndelBFHLinACAExhl;
    private String  tnOfDelBFHLInACAExhl;
    private String  tnOfnDelHLInACA;
    private String  tnOfDelhlInACA;
    private String  tnOfnDelmfinACA;
    private String  tnOfDelmfinACA;
    private String  tnOfnDelTelcosInACA;
    private String  tnOfdelTelcosInACA;
    private String  tnOfndelRetailInACA;
    private String  tnOfDelRetailInACA;
    private String  BFHLCapsLast90Days;
    private String  mfCapsLast90Days;
    private String  telcosCapsLast90Days;
    private String  retailCapsLast90Days;
    private String  tnOfocomcad;
    private String  totValOfocomCAD;
    private String  mntsmrocomCAD;
    private String  tnOfocomACA;
    private String  balocomACAExhl;
    private String  balOcomacahlonly;
    private String  wcdstocomACA;
    private String  hcblmperrevocomACA;
    private String  tnOfnDelocominACA;
    private String  tnOfDelocominACA;
    private String  tnOfocomCapsLast90Days;
    private String  anyRelcbdatadisyn;
    private String  othrelcbdfcposmatyn;
    private String  tnOfCADclassedassfwdwo;
    private String  mntsmrcadclassedassfwdwo;
    private String  numOfCadsfwdwolast24mnt;
    private String  totCurBalLivesAcc;
    private String  totCurBalLiveuAcc;
    private String  totCurBalMaxBalLivesAcc;
    private String  totCurBalMaxBalLiveuAcc;
    private String  outputWriteFlag;
    private String  outputWriteDate;
    private String  outputReadDate;
    private String accountKey;
    
    public ExperianSROPDomain(String srNo, String soaSourceName,String memberReferenceNumber,String enquiryDate) {
		this.srNo=srNo;
		this.soaSourceName=soaSourceName;
		this.memberReferenceNumber=memberReferenceNumber;
		this.enquiryDate=enquiryDate;
	}
    
    
    public ExperianSROPDomain(String srNo, String soaSourceName,String memberReferenceNumber,String enquiryDate,String accountNumber) {
		this.srNo=srNo;
		this.soaSourceName=soaSourceName;
		this.memberReferenceNumber=memberReferenceNumber;
		this.enquiryDate=enquiryDate;
		this.accountNumber=accountNumber;
		
	}
    
    
    
	public String getSrNo() {
		return srNo;
	}
	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}
	public String getSoaSourceName() {
		return soaSourceName;
	}
	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}
	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}
	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}
	public String getEnquiryDate() {
		return enquiryDate;
	}
	public void setEnquiryDate(String enquiryDate) {
		this.enquiryDate = enquiryDate;
	}
	public String getSystemcodeHeader() {
		return systemcodeHeader;
	}
	public void setSystemcodeHeader(String systemcodeHeader) {
		this.systemcodeHeader = systemcodeHeader;
	}
	public String getMessageTextHeader() {
		return messageTextHeader;
	}
	public void setMessageTextHeader(String messageTextHeader) {
		this.messageTextHeader = messageTextHeader;
	}
	public String getReportdate() {
		return reportdate;
	}
	public void setReportdate(String reportdate) {
		this.reportdate = reportdate;
	}
	public String getReportimeHeader() {
		return reportimeHeader;
	}
	public void setReportimeHeader(String reportimeHeader) {
		this.reportimeHeader = reportimeHeader;
	}
	public String getUserMessageText() {
		return userMessageText;
	}
	public void setUserMessageText(String userMessageText) {
		this.userMessageText = userMessageText;
	}
	public String getEnquiryUsername() {
		return enquiryUsername;
	}
	public void setEnquiryUsername(String enquiryUsername) {
		this.enquiryUsername = enquiryUsername;
	}
	public String getCreditProfileReportDate() {
		return creditProfileReportDate;
	}
	public void setCreditProfileReportDate(String creditProfileReportDate) {
		this.creditProfileReportDate = creditProfileReportDate;
	}
	public String getCreditProfileReportTime() {
		return CreditProfileReportTime;
	}
	public void setCreditProfileReportTime(String creditProfileReportTime) {
		CreditProfileReportTime = creditProfileReportTime;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getReportNumber() {
		return reportNumber;
	}
	public void setReportNumber(String reportNumber) {
		this.reportNumber = reportNumber;
	}
	public String getSubscriber() {
		return subscriber;
	}
	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}
	public String getSubscriberName() {
		return subscriberName;
	}
	public void setSubscriberName(String subscriberName) {
		this.subscriberName = subscriberName;
	}
	public String getEnquiryReason() {
		return enquiryReason;
	}
	public void setEnquiryReason(String enquiryReason) {
		this.enquiryReason = enquiryReason;
	}
	public String getFinancePurpose() {
		return financePurpose;
	}
	public void setFinancePurpose(String financePurpose) {
		this.financePurpose = financePurpose;
	}
	public String getAmountFinanced() {
		return amountFinanced;
	}
	public void setAmountFinanced(String amountFinanced) {
		this.amountFinanced = amountFinanced;
	}
	public String getDurationOfAgreement() {
		return durationOfAgreement;
	}
	public void setDurationOfAgreement(String durationOfAgreement) {
		this.durationOfAgreement = durationOfAgreement;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddleName1() {
		return middleName1;
	}
	public void setMiddleName1(String middleName1) {
		this.middleName1 = middleName1;
	}
	public String getMiddleName2() {
		return middleName2;
	}
	public void setMiddleName2(String middleName2) {
		this.middleName2 = middleName2;
	}
	public String getMiddleName3() {
		return middleName3;
	}
	public void setMiddleName3(String middleName3) {
		this.middleName3 = middleName3;
	}
	public String getGenderCode() {
		return genderCode;
	}
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIncomeTaxPanApp() {
		return incomeTaxPanApp;
	}
	public void setIncomeTaxPanApp(String incomeTaxPanApp) {
		this.incomeTaxPanApp = incomeTaxPanApp;
	}
	public String getPanIssueDateApp() {
		return panIssueDateApp;
	}
	public void setPanIssueDateApp(String panIssueDateApp) {
		this.panIssueDateApp = panIssueDateApp;
	}
	public String getPanExpDateApp() {
		return panExpDateApp;
	}
	public void setPanExpDateApp(String panExpDateApp) {
		this.panExpDateApp = panExpDateApp;
	}
	public String getPassportNumberApp() {
		return passportNumberApp;
	}
	public void setPassportNumberApp(String passportNumberApp) {
		this.passportNumberApp = passportNumberApp;
	}
	public String getPassportIssueDateApp() {
		return passportIssueDateApp;
	}
	public void setPassportIssueDateApp(String passportIssueDateApp) {
		this.passportIssueDateApp = passportIssueDateApp;
	}
	public String getPassportExpDateApp() {
		return passportExpDateApp;
	}
	public void setPassportExpDateApp(String passportExpDateApp) {
		this.passportExpDateApp = passportExpDateApp;
	}
	public String getVoterIdentityCardApp() {
		return voterIdentityCardApp;
	}
	public void setVoterIdentityCardApp(String voterIdentityCardApp) {
		this.voterIdentityCardApp = voterIdentityCardApp;
	}
	public String getVoterIdIssueDateApp() {
		return voterIdIssueDateApp;
	}
	public void setVoterIdIssueDateApp(String voterIdIssueDateApp) {
		this.voterIdIssueDateApp = voterIdIssueDateApp;
	}
	public String getVoterIdExpDateApp() {
		return voterIdExpDateApp;
	}
	public void setVoterIdExpDateApp(String voterIdExpDateApp) {
		this.voterIdExpDateApp = voterIdExpDateApp;
	}
	public String getDriverLicenseNumberApp() {
		return driverLicenseNumberApp;
	}
	public void setDriverLicenseNumberApp(String driverLicenseNumberApp) {
		this.driverLicenseNumberApp = driverLicenseNumberApp;
	}
	public String getDriverLicenseIssueDateApp() {
		return driverLicenseIssueDateApp;
	}
	public void setDriverLicenseIssueDateApp(String driverLicenseIssueDateApp) {
		this.driverLicenseIssueDateApp = driverLicenseIssueDateApp;
	}
	public String getDriverLicenseExpDateApp() {
		return driverLicenseExpDateApp;
	}
	public void setDriverLicenseExpDateApp(String driverLicenseExpDateApp) {
		this.driverLicenseExpDateApp = driverLicenseExpDateApp;
	}
	public String getRationCardNumberApp() {
		return rationCardNumberApp;
	}
	public void setRationCardNumberApp(String rationCardNumberApp) {
		this.rationCardNumberApp = rationCardNumberApp;
	}
	public String getRationCardIssueDateApp() {
		return rationCardIssueDateApp;
	}
	public void setRationCardIssueDateApp(String rationCardIssueDateApp) {
		this.rationCardIssueDateApp = rationCardIssueDateApp;
	}
	public String getRationCardExpDateApp() {
		return rationCardExpDateApp;
	}
	public void setRationCardExpDateApp(String rationCardExpDateApp) {
		this.rationCardExpDateApp = rationCardExpDateApp;
	}
	public String getUniversalIdNumberApp() {
		return universalIdNumberApp;
	}
	public void setUniversalIdNumberApp(String universalIdNumberApp) {
		this.universalIdNumberApp = universalIdNumberApp;
	}
	public String getUniversalIdIssueDateApp() {
		return universalIdIssueDateApp;
	}
	public void setUniversalIdIssueDateApp(String universalIdIssueDateApp) {
		this.universalIdIssueDateApp = universalIdIssueDateApp;
	}
	public String getUniversalIdExpDateApp() {
		return universalIdExpDateApp;
	}
	public void setUniversalIdExpDateApp(String universalIdExpDateApp) {
		this.universalIdExpDateApp = universalIdExpDateApp;
	}
	public String getDateOfBirthApplicant() {
		return dateOfBirthApplicant;
	}
	public void setDateOfBirthApplicant(String dateOfBirthApplicant) {
		this.dateOfBirthApplicant = dateOfBirthApplicant;
	}
	public String getTelephoneNumberApplicant1st() {
		return telephoneNumberApplicant1st;
	}
	public void setTelephoneNumberApplicant1st(String telephoneNumberApplicant1st) {
		this.telephoneNumberApplicant1st = telephoneNumberApplicant1st;
	}
	public String getTeleExtensionApp() {
		return teleExtensionApp;
	}
	public void setTeleExtensionApp(String teleExtensionApp) {
		this.teleExtensionApp = teleExtensionApp;
	}
	public String getTeleTypeApp() {
		return teleTypeApp;
	}
	public void setTeleTypeApp(String teleTypeApp) {
		this.teleTypeApp = teleTypeApp;
	}
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}
	public String getEmailIdApp() {
		return emailIdApp;
	}
	public void setEmailIdApp(String emailIdApp) {
		this.emailIdApp = emailIdApp;
	}
	public String getIncome() {
		return income;
	}
	public void setIncome(String income) {
		this.income = income;
	}
	public String getMaritalStatusCode() {
		return maritalStatusCode;
	}
	public void setMaritalStatusCode(String maritalStatusCode) {
		this.maritalStatusCode = maritalStatusCode;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public String getDateWithEmployer() {
		return dateWithEmployer;
	}
	public void setDateWithEmployer(String dateWithEmployer) {
		this.dateWithEmployer = dateWithEmployer;
	}
	public String getNumberOfMajorCreditCardHeld() {
		return numberOfMajorCreditCardHeld;
	}
	public void setNumberOfMajorCreditCardHeld(String numberOfMajorCreditCardHeld) {
		this.numberOfMajorCreditCardHeld = numberOfMajorCreditCardHeld;
	}
	public String getFlatNoPlotNoHouseNo() {
		return flatNoPlotNoHouseNo;
	}
	public void setFlatNoPlotNoHouseNo(String flatNoPlotNoHouseNo) {
		this.flatNoPlotNoHouseNo = flatNoPlotNoHouseNo;
	}
	public String getBldgNoSocietyName() {
		return bldgNoSocietyName;
	}
	public void setBldgNoSocietyName(String bldgNoSocietyName) {
		this.bldgNoSocietyName = bldgNoSocietyName;
	}
	public String getRoadNoNameAreaLocality() {
		return roadNoNameAreaLocality;
	}
	public void setRoadNoNameAreaLocality(String roadNoNameAreaLocality) {
		this.roadNoNameAreaLocality = roadNoNameAreaLocality;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getAddFlatNoPlotNoHouseNo() {
		return addFlatNoPlotNoHouseNo;
	}
	public void setAddFlatNoPlotNoHouseNo(String addFlatNoPlotNoHouseNo) {
		this.addFlatNoPlotNoHouseNo = addFlatNoPlotNoHouseNo;
	}
	public String getAddBldgNoSocietyName() {
		return addBldgNoSocietyName;
	}
	public void setAddBldgNoSocietyName(String addBldgNoSocietyName) {
		this.addBldgNoSocietyName = addBldgNoSocietyName;
	}
	public String getAddRoadNoNameAreaLocality() {
		return addRoadNoNameAreaLocality;
	}
	public void setAddRoadNoNameAreaLocality(String addRoadNoNameAreaLocality) {
		this.addRoadNoNameAreaLocality = addRoadNoNameAreaLocality;
	}
	public String getAddCity() {
		return addCity;
	}
	public void setAddCity(String addCity) {
		this.addCity = addCity;
	}
	public String getAddLandmark() {
		return addLandmark;
	}
	public void setAddLandmark(String addLandmark) {
		this.addLandmark = addLandmark;
	}
	public String getAddState() {
		return addState;
	}
	public void setAddState(String addState) {
		this.addState = addState;
	}
	public String getAddPincode() {
		return addPincode;
	}
	public void setAddPincode(String addPincode) {
		this.addPincode = addPincode;
	}
	public String getAddCountrycode() {
		return addCountrycode;
	}
	public void setAddCountrycode(String addCountrycode) {
		this.addCountrycode = addCountrycode;
	}
	public String getCreditAccountTotal() {
		return creditAccountTotal;
	}
	public void setCreditAccountTotal(String creditAccountTotal) {
		this.creditAccountTotal = creditAccountTotal;
	}
	public String getCreditAccountActive() {
		return creditAccountActive;
	}
	public void setCreditAccountActive(String creditAccountActive) {
		this.creditAccountActive = creditAccountActive;
	}
	public String getCreditAccountDefault() {
		return creditAccountDefault;
	}
	public void setCreditAccountDefault(String creditAccountDefault) {
		this.creditAccountDefault = creditAccountDefault;
	}
	public String getCreditAccountClosed() {
		return creditAccountClosed;
	}
	public void setCreditAccountClosed(String creditAccountClosed) {
		this.creditAccountClosed = creditAccountClosed;
	}
	public String getCadsuitfiledCurrentBalance() {
		return cadsuitfiledCurrentBalance;
	}
	public void setCadsuitfiledCurrentBalance(String cadsuitfiledCurrentBalance) {
		this.cadsuitfiledCurrentBalance = cadsuitfiledCurrentBalance;
	}
	public String getOutstandingBalanceSecured() {
		return outstandingBalanceSecured;
	}
	public void setOutstandingBalanceSecured(String outstandingBalanceSecured) {
		this.outstandingBalanceSecured = outstandingBalanceSecured;
	}
	public String getOutstandingBalSecuredPer() {
		return outstandingBalSecuredPer;
	}
	public void setOutstandingBalSecuredPer(String outstandingBalSecuredPer) {
		this.outstandingBalSecuredPer = outstandingBalSecuredPer;
	}
	
	public String getOutstandingBalanceUnsecured() {
		return outstandingBalanceUnsecured;
	}

	public void setOutstandingBalanceUnsecured(String outstandingBalanceUnsecured) {
		this.outstandingBalanceUnsecured = outstandingBalanceUnsecured;
	}

	public String getOutstandingBalUnsecPer() {
		return outstandingBalUnsecPer;
	}
	public void setOutstandingBalUnsecPer(String outstandingBalUnsecPer) {
		this.outstandingBalUnsecPer = outstandingBalUnsecPer;
	}
	public String getOutstandingBalanceAll() {
		return outstandingBalanceAll;
	}
	public void setOutstandingBalanceAll(String outstandingBalanceAll) {
		this.outstandingBalanceAll = outstandingBalanceAll;
	}
	public String getIdentificationNumber() {
		return identificationNumber;
	}
	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	public String getCaisSubscriberName() {
		return caisSubscriberName;
	}
	public void setCaisSubscriberName(String caisSubscriberName) {
		this.caisSubscriberName = caisSubscriberName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getPortfolioType() {
		return portfolioType;
	}
	public void setPortfolioType(String portfolioType) {
		this.portfolioType = portfolioType;
	}
	public String getAccountTypeCode() {
		return accountTypeCode;
	}
	public void setAccountTypeCode(String accountTypeCode) {
		this.accountTypeCode = accountTypeCode;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getOpenDate() {
		return openDate;
	}
	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}
	public String getHighestCreditOrOrgnLoanAmt() {
		return highestCreditOrOrgnLoanAmt;
	}
	public void setHighestCreditOrOrgnLoanAmt(String highestCreditOrOrgnLoanAmt) {
		this.highestCreditOrOrgnLoanAmt = highestCreditOrOrgnLoanAmt;
	}
	public String getTermsDuration() {
		return termsDuration;
	}
	public void setTermsDuration(String termsDuration) {
		this.termsDuration = termsDuration;
	}
	public String getTermsFrequency() {
		return termsFrequency;
	}
	public void setTermsFrequency(String termsFrequency) {
		this.termsFrequency = termsFrequency;
	}
	public String getScheduledMonthlyPayamt() {
		return scheduledMonthlyPayamt;
	}
	public void setScheduledMonthlyPayamt(String scheduledMonthlyPayamt) {
		this.scheduledMonthlyPayamt = scheduledMonthlyPayamt;
	}
	public String getAccountStatusCode() {
		return accountStatusCode;
	}
	public void setAccountStatusCode(String accountStatusCode) {
		this.accountStatusCode = accountStatusCode;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getPaymentRating() {
		return paymentRating;
	}
	public void setPaymentRating(String paymentRating) {
		this.paymentRating = paymentRating;
	}
	public String getPaymentHistoryProfile() {
		return paymentHistoryProfile;
	}
	public void setPaymentHistoryProfile(String paymentHistoryProfile) {
		this.paymentHistoryProfile = paymentHistoryProfile;
	}
	public String getSpecialComment() {
		return specialComment;
	}
	public void setSpecialComment(String specialComment) {
		this.specialComment = specialComment;
	}
	public String getCurrentBalanceTl() {
		return currentBalanceTl;
	}
	public void setCurrentBalanceTl(String currentBalanceTl) {
		this.currentBalanceTl = currentBalanceTl;
	}
	public String getAmountPastdueTl() {
		return amountPastdueTl;
	}
	public void setAmountPastdueTl(String amountPastdueTl) {
		this.amountPastdueTl = amountPastdueTl;
	}
	public String getOriginalChargeOffAmount() {
		return originalChargeOffAmount;
	}
	public void setOriginalChargeOffAmount(String originalChargeOffAmount) {
		this.originalChargeOffAmount = originalChargeOffAmount;
	}
	public String getDateReported() {
		return dateReported;
	}
	public void setDateReported(String dateReported) {
		this.dateReported = dateReported;
	}
	public String getDateOfFirstDelinquency() {
		return dateOfFirstDelinquency;
	}
	public void setDateOfFirstDelinquency(String dateOfFirstDelinquency) {
		this.dateOfFirstDelinquency = dateOfFirstDelinquency;
	}
	public String getDateClosed() {
		return dateClosed;
	}
	public void setDateClosed(String dateClosed) {
		this.dateClosed = dateClosed;
	}
	public String getDateOfLastPayment() {
		return dateOfLastPayment;
	}
	public void setDateOfLastPayment(String dateOfLastPayment) {
		this.dateOfLastPayment = dateOfLastPayment;
	}
	public String getSuitfilledWillFullDefaultWrittenOffStatus() {
		return suitfilledWillFullDefaultWrittenOffStatus;
	}
	public void setSuitfilledWillFullDefaultWrittenOffStatus(
			String suitfilledWillFullDefaultWrittenOffStatus) {
		this.suitfilledWillFullDefaultWrittenOffStatus = suitfilledWillFullDefaultWrittenOffStatus;
	}
	public String getSuitFiledWilfulDef() {
		return suitFiledWilfulDef;
	}
	public void setSuitFiledWilfulDef(String suitFiledWilfulDef) {
		this.suitFiledWilfulDef = suitFiledWilfulDef;
	}
	public String getWrittenOffSettledStatus() {
		return writtenOffSettledStatus;
	}
	public void setWrittenOffSettledStatus(String writtenOffSettledStatus) {
		this.writtenOffSettledStatus = writtenOffSettledStatus;
	}
	public String getValueOfCreditsLastMonth() {
		return valueOfCreditsLastMonth;
	}
	public void setValueOfCreditsLastMonth(String valueOfCreditsLastMonth) {
		this.valueOfCreditsLastMonth = valueOfCreditsLastMonth;
	}
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public String getSattlementAmount() {
		return sattlementAmount;
	}
	public void setSattlementAmount(String sattlementAmount) {
		this.sattlementAmount = sattlementAmount;
	}
	public String getValueOfColateral() {
		return valueOfColateral;
	}
	public void setValueOfColateral(String valueOfColateral) {
		this.valueOfColateral = valueOfColateral;
	}
	public String getTypeOfColateral() {
		return typeOfColateral;
	}
	public void setTypeOfColateral(String typeOfColateral) {
		this.typeOfColateral = typeOfColateral;
	}
	public String getWrittenOffAmtTotal() {
		return writtenOffAmtTotal;
	}
	public void setWrittenOffAmtTotal(String writtenOffAmtTotal) {
		this.writtenOffAmtTotal = writtenOffAmtTotal;
	}
	public String getWrittenOffAmtPrincipal() {
		return writtenOffAmtPrincipal;
	}
	public void setWrittenOffAmtPrincipal(String writtenOffAmtPrincipal) {
		this.writtenOffAmtPrincipal = writtenOffAmtPrincipal;
	}
	public String getRateOfInterest() {
		return rateOfInterest;
	}
	public void setRateOfInterest(String rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}
	public String getRepaymentTenure() {
		return repaymentTenure;
	}
	public void setRepaymentTenure(String repaymentTenure) {
		this.repaymentTenure = repaymentTenure;
	}
	public String getPromotionalRateFlag() {
		return promotionalRateFlag;
	}
	public void setPromotionalRateFlag(String promotionalRateFlag) {
		this.promotionalRateFlag = promotionalRateFlag;
	}
	public String getCaisIncome() {
		return caisIncome;
	}
	public void setCaisIncome(String caisIncome) {
		this.caisIncome = caisIncome;
	}
	public String getIncomeIndicator() {
		return incomeIndicator;
	}
	public void setIncomeIndicator(String incomeIndicator) {
		this.incomeIndicator = incomeIndicator;
	}
	public String getIncomeFrequencyIndicator() {
		return incomeFrequencyIndicator;
	}
	public void setIncomeFrequencyIndicator(String incomeFrequencyIndicator) {
		this.incomeFrequencyIndicator = incomeFrequencyIndicator;
	}
	public String getDefaultStatusDate() {
		return defaultStatusDate;
	}
	public void setDefaultStatusDate(String defaultStatusDate) {
		this.defaultStatusDate = defaultStatusDate;
	}
	public String getLitigationStatusDate() {
		return litigationStatusDate;
	}
	public void setLitigationStatusDate(String litigationStatusDate) {
		this.litigationStatusDate = litigationStatusDate;
	}
	public String getWriteOffStatusDate() {
		return writeOffStatusDate;
	}
	public void setWriteOffStatusDate(String writeOffStatusDate) {
		this.writeOffStatusDate = writeOffStatusDate;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getSubscriberComments() {
		return subscriberComments;
	}
	public void setSubscriberComments(String subscriberComments) {
		this.subscriberComments = subscriberComments;
	}
	public String getConsumerComments() {
		return consumerComments;
	}
	public void setConsumerComments(String consumerComments) {
		this.consumerComments = consumerComments;
	}
	public String getAccountHolderTypeCode() {
		return accountHolderTypeCode;
	}
	public void setAccountHolderTypeCode(String accountHolderTypeCode) {
		this.accountHolderTypeCode = accountHolderTypeCode;
	}
	public String getAccountHolderTypeName() {
		return accountHolderTypeName;
	}
	public void setAccountHolderTypeName(String accountHolderTypeName) {
		this.accountHolderTypeName = accountHolderTypeName;
	}
	public String getYearHist() {
		return yearHist;
	}
	public void setYearHist(String yearHist) {
		this.yearHist = yearHist;
	}
	public String getMonthHist() {
		return monthHist;
	}
	public void setMonthHist(String monthHist) {
		this.monthHist = monthHist;
	}
	public String getDaysPastDue() {
		return daysPastDue;
	}
	public void setDaysPastDue(String daysPastDue) {
		this.daysPastDue = daysPastDue;
	}
	public String getAssetClassification() {
		return assetClassification;
	}
	public void setAssetClassification(String assetClassification) {
		this.assetClassification = assetClassification;
	}
	public String getYearAdvHist() {
		return yearAdvHist;
	}
	public void setYearAdvHist(String yearAdvHist) {
		this.yearAdvHist = yearAdvHist;
	}
	public String getMonthAdvHist() {
		return monthAdvHist;
	}
	public void setMonthAdvHist(String monthAdvHist) {
		this.monthAdvHist = monthAdvHist;
	}
	public String getCashLimit() {
		return cashLimit;
	}
	public void setCashLimit(String cashLimit) {
		this.cashLimit = cashLimit;
	}
	public String getCreditLimitAmt() {
		return creditLimitAmt;
	}
	public void setCreditLimitAmt(String creditLimitAmt) {
		this.creditLimitAmt = creditLimitAmt;
	}
	public String getActualPaymentAmt() {
		return actualPaymentAmt;
	}
	public void setActualPaymentAmt(String actualPaymentAmt) {
		this.actualPaymentAmt = actualPaymentAmt;
	}
	public String getEmiAmt() {
		return emiAmt;
	}
	public void setEmiAmt(String emiAmt) {
		this.emiAmt = emiAmt;
	}
	public String getCurrentBalanceAdvHist() {
		return currentBalanceAdvHist;
	}
	public void setCurrentBalanceAdvHist(String currentBalanceAdvHist) {
		this.currentBalanceAdvHist = currentBalanceAdvHist;
	}
	public String getAmountPastDueAdvHist() {
		return amountPastDueAdvHist;
	}
	public void setAmountPastDueAdvHist(String amountPastDueAdvHist) {
		this.amountPastDueAdvHist = amountPastDueAdvHist;
	}
	public String getSurNameNonNormalized() {
		return surNameNonNormalized;
	}
	public void setSurNameNonNormalized(String surNameNonNormalized) {
		this.surNameNonNormalized = surNameNonNormalized;
	}
	public String getFirstNameNonNormalized() {
		return firstNameNonNormalized;
	}
	public void setFirstNameNonNormalized(String firstNameNonNormalized) {
		this.firstNameNonNormalized = firstNameNonNormalized;
	}
	public String getMiddleName1NonNormalized() {
		return middleName1NonNormalized;
	}
	public void setMiddleName1NonNormalized(String middleName1NonNormalized) {
		this.middleName1NonNormalized = middleName1NonNormalized;
	}
	public String getMiddleName2NonNormalized() {
		return middleName2NonNormalized;
	}
	public void setMiddleName2NonNormalized(String middleName2NonNormalized) {
		this.middleName2NonNormalized = middleName2NonNormalized;
	}
	public String getMiddleName3NonNormalized() {
		return middleName3NonNormalized;
	}
	public void setMiddleName3NonNormalized(String middleName3NonNormalized) {
		this.middleName3NonNormalized = middleName3NonNormalized;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getCaisGenderCode() {
		return caisGenderCode;
	}
	public void setCaisGenderCode(String caisGenderCode) {
		this.caisGenderCode = caisGenderCode;
	}
	public String getCaisGender() {
		return caisGender;
	}
	public void setCaisGender(String caisGender) {
		this.caisGender = caisGender;
	}
	public String getCaisIncomeTaxPan() {
		return caisIncomeTaxPan;
	}
	public void setCaisIncomeTaxPan(String caisIncomeTaxPan) {
		this.caisIncomeTaxPan = caisIncomeTaxPan;
	}
	public String getCaisPassportNumber() {
		return caisPassportNumber;
	}
	public void setCaisPassportNumber(String caisPassportNumber) {
		this.caisPassportNumber = caisPassportNumber;
	}
	public String getVoterIdNumber() {
		return voterIdNumber;
	}
	public void setVoterIdNumber(String voterIdNumber) {
		this.voterIdNumber = voterIdNumber;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getFirstLineOfAddNonNormalized() {
		return firstLineOfAddNonNormalized;
	}
	public void setFirstLineOfAddNonNormalized(String firstLineOfAddNonNormalized) {
		this.firstLineOfAddNonNormalized = firstLineOfAddNonNormalized;
	}
	public String getSecondLineOfAddNonNormalized() {
		return secondLineOfAddNonNormalized;
	}
	public void setSecondLineOfAddNonNormalized(String secondLineOfAddNonNormalized) {
		this.secondLineOfAddNonNormalized = secondLineOfAddNonNormalized;
	}
	public String getThirdLineOfAddNonNormalized() {
		return thirdLineOfAddNonNormalized;
	}
	public void setThirdLineOfAddNonNormalized(String thirdLineOfAddNonNormalized) {
		this.thirdLineOfAddNonNormalized = thirdLineOfAddNonNormalized;
	}
	public String getCityNonNormalized() {
		return cityNonNormalized;
	}
	public void setCityNonNormalized(String cityNonNormalized) {
		this.cityNonNormalized = cityNonNormalized;
	}
	public String getFifthLineOfAddNonNormalized() {
		return fifthLineOfAddNonNormalized;
	}
	public void setFifthLineOfAddNonNormalized(String fifthLineOfAddNonNormalized) {
		this.fifthLineOfAddNonNormalized = fifthLineOfAddNonNormalized;
	}
	public String getStateCodeNonNormalized() {
		return stateCodeNonNormalized;
	}
	public void setStateCodeNonNormalized(String stateCodeNonNormalized) {
		this.stateCodeNonNormalized = stateCodeNonNormalized;
	}
	public String getStateNonNormalized() {
		return stateNonNormalized;
	}
	public void setStateNonNormalized(String stateNonNormalized) {
		this.stateNonNormalized = stateNonNormalized;
	}
	public String getZipPostalCodeNonNormalized() {
		return zipPostalCodeNonNormalized;
	}
	public void setZipPostalCodeNonNormalized(String zipPostalCodeNonNormalized) {
		this.zipPostalCodeNonNormalized = zipPostalCodeNonNormalized;
	}
	public String getCountryCodeNonNormalized() {
		return countryCodeNonNormalized;
	}
	public void setCountryCodeNonNormalized(String countryCodeNonNormalized) {
		this.countryCodeNonNormalized = countryCodeNonNormalized;
	}
	public String getAddIndicatorNonNormalized() {
		return addIndicatorNonNormalized;
	}
	public void setAddIndicatorNonNormalized(String addIndicatorNonNormalized) {
		this.addIndicatorNonNormalized = addIndicatorNonNormalized;
	}
	public String getResidenceCodeNonNormalized() {
		return residenceCodeNonNormalized;
	}
	public void setResidenceCodeNonNormalized(String residenceCodeNonNormalized) {
		this.residenceCodeNonNormalized = residenceCodeNonNormalized;
	}
	public String getResidenceNonNormalized() {
		return residenceNonNormalized;
	}
	public void setResidenceNonNormalized(String residenceNonNormalized) {
		this.residenceNonNormalized = residenceNonNormalized;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getTeleTypePhone() {
		return teleTypePhone;
	}
	public void setTeleTypePhone(String teleTypePhone) {
		this.teleTypePhone = teleTypePhone;
	}
	public String getTeleExtensionPhone() {
		return teleExtensionPhone;
	}
	public void setTeleExtensionPhone(String teleExtensionPhone) {
		this.teleExtensionPhone = teleExtensionPhone;
	}
	public String getMobileTelephoneNumber() {
		return mobileTelephoneNumber;
	}
	public void setMobileTelephoneNumber(String mobileTelephoneNumber) {
		this.mobileTelephoneNumber = mobileTelephoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmailIdPhone() {
		return emailIdPhone;
	}
	public void setEmailIdPhone(String emailIdPhone) {
		this.emailIdPhone = emailIdPhone;
	}
	public String getIncomeTaxPanId() {
		return incomeTaxPanId;
	}
	public void setIncomeTaxPanId(String incomeTaxPanId) {
		this.incomeTaxPanId = incomeTaxPanId;
	}
	public String getPanIssueDateId() {
		return panIssueDateId;
	}
	public void setPanIssueDateId(String panIssueDateId) {
		this.panIssueDateId = panIssueDateId;
	}
	public String getPanExpDateId() {
		return panExpDateId;
	}
	public void setPanExpDateId(String panExpDateId) {
		this.panExpDateId = panExpDateId;
	}
	public String getPassportNumberId() {
		return passportNumberId;
	}
	public void setPassportNumberId(String passportNumberId) {
		this.passportNumberId = passportNumberId;
	}
	public String getPassportIssueDateId() {
		return passportIssueDateId;
	}
	public void setPassportIssueDateId(String passportIssueDateId) {
		this.passportIssueDateId = passportIssueDateId;
	}
	public String getPassportExpDateId() {
		return passportExpDateId;
	}
	public void setPassportExpDateId(String passportExpDateId) {
		this.passportExpDateId = passportExpDateId;
	}
	public String getVoterIdentityCardId() {
		return voterIdentityCardId;
	}
	public void setVoterIdentityCardId(String voterIdentityCardId) {
		this.voterIdentityCardId = voterIdentityCardId;
	}
	public String getVoterIdIssueDateId() {
		return voterIdIssueDateId;
	}
	public void setVoterIdIssueDateId(String voterIdIssueDateId) {
		this.voterIdIssueDateId = voterIdIssueDateId;
	}
	public String getVoterIdExpDateId() {
		return voterIdExpDateId;
	}
	public void setVoterIdExpDateId(String voterIdExpDateId) {
		this.voterIdExpDateId = voterIdExpDateId;
	}
	public String getDriverLicenseNumberId() {
		return driverLicenseNumberId;
	}
	public void setDriverLicenseNumberId(String driverLicenseNumberId) {
		this.driverLicenseNumberId = driverLicenseNumberId;
	}
	public String getDriverLicenseIssueDateId() {
		return driverLicenseIssueDateId;
	}
	public void setDriverLicenseIssueDateId(String driverLicenseIssueDateId) {
		this.driverLicenseIssueDateId = driverLicenseIssueDateId;
	}
	public String getDriverLicenseExpDateId() {
		return driverLicenseExpDateId;
	}
	public void setDriverLicenseExpDateId(String driverLicenseExpDateId) {
		this.driverLicenseExpDateId = driverLicenseExpDateId;
	}
	public String getRationCardNumberId() {
		return rationCardNumberId;
	}
	public void setRationCardNumberId(String rationCardNumberId) {
		this.rationCardNumberId = rationCardNumberId;
	}
	public String getRationCardIssueDateId() {
		return rationCardIssueDateId;
	}
	public void setRationCardIssueDateId(String rationCardIssueDateId) {
		this.rationCardIssueDateId = rationCardIssueDateId;
	}
	public String getRationCardExpDateId() {
		return rationCardExpDateId;
	}
	public void setRationCardExpDateId(String rationCardExpDateId) {
		this.rationCardExpDateId = rationCardExpDateId;
	}
	public String getUniversalIdNumberId() {
		return universalIdNumberId;
	}
	public void setUniversalIdNumberId(String universalIdNumberId) {
		this.universalIdNumberId = universalIdNumberId;
	}
	public String getUniversalIdIssueDateId() {
		return universalIdIssueDateId;
	}
	public void setUniversalIdIssueDateId(String universalIdIssueDateId) {
		this.universalIdIssueDateId = universalIdIssueDateId;
	}
	public String getUniversalIdExpDateId() {
		return universalIdExpDateId;
	}
	public void setUniversalIdExpDateId(String universalIdExpDateId) {
		this.universalIdExpDateId = universalIdExpDateId;
	}
	public String getEmailidId() {
		return emailidId;
	}
	public void setEmailidId(String emailidId) {
		this.emailidId = emailidId;
	}
	public String getExactMatch() {
		return exactMatch;
	}
	public void setExactMatch(String exactMatch) {
		this.exactMatch = exactMatch;
	}
	public String getCapsLast7Days() {
		return capsLast7Days;
	}
	public void setCapsLast7Days(String capsLast7Days) {
		this.capsLast7Days = capsLast7Days;
	}
	public String getCapsLast30Days() {
		return capsLast30Days;
	}
	public void setCapsLast30Days(String capsLast30Days) {
		this.capsLast30Days = capsLast30Days;
	}
	public String getCapsLast90Days() {
		return capsLast90Days;
	}
	public void setCapsLast90Days(String capsLast90Days) {
		this.capsLast90Days = capsLast90Days;
	}
	public String getCapsLast180Days() {
		return capsLast180Days;
	}
	public void setCapsLast180Days(String capsLast180Days) {
		this.capsLast180Days = capsLast180Days;
	}
	public String getCapsSubscriberCode() {
		return capsSubscriberCode;
	}
	public void setCapsSubscriberCode(String capsSubscriberCode) {
		this.capsSubscriberCode = capsSubscriberCode;
	}
	public String getCapsSubscriberName() {
		return capsSubscriberName;
	}
	public void setCapsSubscriberName(String capsSubscriberName) {
		this.capsSubscriberName = capsSubscriberName;
	}
	public String getCapsdateofRequest() {
		return capsdateofRequest;
	}
	public void setCapsdateofRequest(String capsdateofRequest) {
		this.capsdateofRequest = capsdateofRequest;
	}
	public String getCapsReportDate() {
		return capsReportDate;
	}
	public void setCapsReportDate(String capsReportDate) {
		this.capsReportDate = capsReportDate;
	}
	public String getCapsReportTime() {
		return capsReportTime;
	}
	public void setCapsReportTime(String capsReportTime) {
		this.capsReportTime = capsReportTime;
	}
	public String getCapsReportNumber() {
		return capsReportNumber;
	}
	public void setCapsReportNumber(String capsReportNumber) {
		this.capsReportNumber = capsReportNumber;
	}
	public String getEnquiryReasonCode() {
		return enquiryReasonCode;
	}
	public void setEnquiryReasonCode(String enquiryReasonCode) {
		this.enquiryReasonCode = enquiryReasonCode;
	}
	public String getCapsEnquiryReason() {
		return capsEnquiryReason;
	}
	public void setCapsEnquiryReason(String capsEnquiryReason) {
		this.capsEnquiryReason = capsEnquiryReason;
	}
	public String getFinancePurposeCode() {
		return financePurposeCode;
	}
	public void setFinancePurposeCode(String financePurposeCode) {
		this.financePurposeCode = financePurposeCode;
	}
	public String getCapsFinancePurpose() {
		return capsFinancePurpose;
	}
	public void setCapsFinancePurpose(String capsFinancePurpose) {
		this.capsFinancePurpose = capsFinancePurpose;
	}
	public String getCapsAmountFinanced() {
		return capsAmountFinanced;
	}
	public void setCapsAmountFinanced(String capsAmountFinanced) {
		this.capsAmountFinanced = capsAmountFinanced;
	}
	public String getCapsDurationOfAgreement() {
		return capsDurationOfAgreement;
	}
	public void setCapsDurationOfAgreement(String capsDurationOfAgreement) {
		this.capsDurationOfAgreement = capsDurationOfAgreement;
	}
	public String getCapsApplicantLastName() {
		return capsApplicantLastName;
	}
	public void setCapsApplicantLastName(String capsApplicantLastName) {
		this.capsApplicantLastName = capsApplicantLastName;
	}
	public String getCapsApplicantFirstName() {
		return capsApplicantFirstName;
	}
	public void setCapsApplicantFirstName(String capsApplicantFirstName) {
		this.capsApplicantFirstName = capsApplicantFirstName;
	}
	public String getCapsApplicantMiddleName1() {
		return capsApplicantMiddleName1;
	}
	public void setCapsApplicantMiddleName1(String capsApplicantMiddleName1) {
		this.capsApplicantMiddleName1 = capsApplicantMiddleName1;
	}
	public String getCapsApplicantMiddleName2() {
		return capsApplicantMiddleName2;
	}
	public void setCapsApplicantMiddleName2(String capsApplicantMiddleName2) {
		this.capsApplicantMiddleName2 = capsApplicantMiddleName2;
	}
	public String getCapsApplicantMiddleName3() {
		return capsApplicantMiddleName3;
	}
	public void setCapsApplicantMiddleName3(String capsApplicantMiddleName3) {
		this.capsApplicantMiddleName3 = capsApplicantMiddleName3;
	}
	public String getCapsApplicantgenderCode() {
		return capsApplicantgenderCode;
	}
	public void setCapsApplicantgenderCode(String capsApplicantgenderCode) {
		this.capsApplicantgenderCode = capsApplicantgenderCode;
	}
	public String getCapsApplicantGender() {
		return capsApplicantGender;
	}
	public void setCapsApplicantGender(String capsApplicantGender) {
		this.capsApplicantGender = capsApplicantGender;
	}
	public String getCapsIncomeTaxPan() {
		return capsIncomeTaxPan;
	}
	public void setCapsIncomeTaxPan(String capsIncomeTaxPan) {
		this.capsIncomeTaxPan = capsIncomeTaxPan;
	}
	public String getCapsPanIssueDate() {
		return capsPanIssueDate;
	}
	public void setCapsPanIssueDate(String capsPanIssueDate) {
		this.capsPanIssueDate = capsPanIssueDate;
	}
	public String getCapsPanExpDate() {
		return capsPanExpDate;
	}
	public void setCapsPanExpDate(String capsPanExpDate) {
		this.capsPanExpDate = capsPanExpDate;
	}
	public String getCapsPassportNumber() {
		return capsPassportNumber;
	}
	public void setCapsPassportNumber(String capsPassportNumber) {
		this.capsPassportNumber = capsPassportNumber;
	}
	public String getCapsPassportIssueDate() {
		return capsPassportIssueDate;
	}
	public void setCapsPassportIssueDate(String capsPassportIssueDate) {
		this.capsPassportIssueDate = capsPassportIssueDate;
	}
	public String getCapsPassportExpDate() {
		return capsPassportExpDate;
	}
	public void setCapsPassportExpDate(String capsPassportExpDate) {
		this.capsPassportExpDate = capsPassportExpDate;
	}
	public String getCapsVoterIdentityCard() {
		return capsVoterIdentityCard;
	}
	public void setCapsVoterIdentityCard(String capsVoterIdentityCard) {
		this.capsVoterIdentityCard = capsVoterIdentityCard;
	}
	public String getCapsVoterIdIssueDate() {
		return capsVoterIdIssueDate;
	}
	public void setCapsVoterIdIssueDate(String capsVoterIdIssueDate) {
		this.capsVoterIdIssueDate = capsVoterIdIssueDate;
	}
	public String getCapsVoterIdExpDate() {
		return capsVoterIdExpDate;
	}
	public void setCapsVoterIdExpDate(String capsVoterIdExpDate) {
		this.capsVoterIdExpDate = capsVoterIdExpDate;
	}
	public String getCapsDriverLicenseNumber() {
		return capsDriverLicenseNumber;
	}
	public void setCapsDriverLicenseNumber(String capsDriverLicenseNumber) {
		this.capsDriverLicenseNumber = capsDriverLicenseNumber;
	}
	public String getCapsDriverLicenseIssueDate() {
		return capsDriverLicenseIssueDate;
	}
	public void setCapsDriverLicenseIssueDate(String capsDriverLicenseIssueDate) {
		this.capsDriverLicenseIssueDate = capsDriverLicenseIssueDate;
	}
	public String getCapsDriverLicenseExpDate() {
		return capsDriverLicenseExpDate;
	}
	public void setCapsDriverLicenseExpDate(String capsDriverLicenseExpDate) {
		this.capsDriverLicenseExpDate = capsDriverLicenseExpDate;
	}
	public String getCapsRationCardNumber() {
		return capsRationCardNumber;
	}
	public void setCapsRationCardNumber(String capsRationCardNumber) {
		this.capsRationCardNumber = capsRationCardNumber;
	}
	public String getCapsRationCardIssueDate() {
		return capsRationCardIssueDate;
	}
	public void setCapsRationCardIssueDate(String capsRationCardIssueDate) {
		this.capsRationCardIssueDate = capsRationCardIssueDate;
	}
	public String getCapsRationCardExpDate() {
		return capsRationCardExpDate;
	}
	public void setCapsRationCardExpDate(String capsRationCardExpDate) {
		this.capsRationCardExpDate = capsRationCardExpDate;
	}
	public String getCapsUniversalIdNumber() {
		return capsUniversalIdNumber;
	}
	public void setCapsUniversalIdNumber(String capsUniversalIdNumber) {
		this.capsUniversalIdNumber = capsUniversalIdNumber;
	}
	public String getCapsUniversalIdIssueDate() {
		return capsUniversalIdIssueDate;
	}
	public void setCapsUniversalIdIssueDate(String capsUniversalIdIssueDate) {
		this.capsUniversalIdIssueDate = capsUniversalIdIssueDate;
	}
	public String getCapsUniversalIdExpDate() {
		return capsUniversalIdExpDate;
	}
	public void setCapsUniversalIdExpDate(String capsUniversalIdExpDate) {
		this.capsUniversalIdExpDate = capsUniversalIdExpDate;
	}
	public String getCapsApplicantDobApplicant() {
		return capsApplicantDobApplicant;
	}
	public void setCapsApplicantDobApplicant(String capsApplicantDobApplicant) {
		this.capsApplicantDobApplicant = capsApplicantDobApplicant;
	}
	public String getCapsApplicantTelNoApplicant1st() {
		return capsApplicantTelNoApplicant1st;
	}
	public void setCapsApplicantTelNoApplicant1st(
			String capsApplicantTelNoApplicant1st) {
		this.capsApplicantTelNoApplicant1st = capsApplicantTelNoApplicant1st;
	}
	public String getCapsApplicantTelExt() {
		return capsApplicantTelExt;
	}
	public void setCapsApplicantTelExt(String capsApplicantTelExt) {
		this.capsApplicantTelExt = capsApplicantTelExt;
	}
	public String getCapsApplicantTelType() {
		return capsApplicantTelType;
	}
	public void setCapsApplicantTelType(String capsApplicantTelType) {
		this.capsApplicantTelType = capsApplicantTelType;
	}
	public String getCapsApplicantMobilePhoneNumber() {
		return capsApplicantMobilePhoneNumber;
	}
	public void setCapsApplicantMobilePhoneNumber(
			String capsApplicantMobilePhoneNumber) {
		this.capsApplicantMobilePhoneNumber = capsApplicantMobilePhoneNumber;
	}
	public String getCapsApplicantEmailId() {
		return capsApplicantEmailId;
	}
	public void setCapsApplicantEmailId(String capsApplicantEmailId) {
		this.capsApplicantEmailId = capsApplicantEmailId;
	}
	public String getCapsApplicantIncome() {
		return capsApplicantIncome;
	}
	public void setCapsApplicantIncome(String capsApplicantIncome) {
		this.capsApplicantIncome = capsApplicantIncome;
	}
	public String getApplicantMaritalStatusCode() {
		return applicantMaritalStatusCode;
	}
	public void setApplicantMaritalStatusCode(String applicantMaritalStatusCode) {
		this.applicantMaritalStatusCode = applicantMaritalStatusCode;
	}
	public String getCapsApplicantMaritalStatus() {
		return capsApplicantMaritalStatus;
	}
	public void setCapsApplicantMaritalStatus(String capsApplicantMaritalStatus) {
		this.capsApplicantMaritalStatus = capsApplicantMaritalStatus;
	}
	public String getCapsApplicantEmploymentStatusCode() {
		return capsApplicantEmploymentStatusCode;
	}
	public void setCapsApplicantEmploymentStatusCode(
			String capsApplicantEmploymentStatusCode) {
		this.capsApplicantEmploymentStatusCode = capsApplicantEmploymentStatusCode;
	}
	public String getCapsApplicantEmploymentStatus() {
		return capsApplicantEmploymentStatus;
	}
	public void setCapsApplicantEmploymentStatus(
			String capsApplicantEmploymentStatus) {
		this.capsApplicantEmploymentStatus = capsApplicantEmploymentStatus;
	}
	public String getCapsApplicantDateWithEmployer() {
		return capsApplicantDateWithEmployer;
	}
	public void setCapsApplicantDateWithEmployer(
			String capsApplicantDateWithEmployer) {
		this.capsApplicantDateWithEmployer = capsApplicantDateWithEmployer;
	}
	public String getCapsApplicantNoMajorCrditCardHeld() {
		return capsApplicantNoMajorCrditCardHeld;
	}
	public void setCapsApplicantNoMajorCrditCardHeld(
			String capsApplicantNoMajorCrditCardHeld) {
		this.capsApplicantNoMajorCrditCardHeld = capsApplicantNoMajorCrditCardHeld;
	}
	public String getCapsApplicantFlatNoPlotNoHouseNo() {
		return capsApplicantFlatNoPlotNoHouseNo;
	}
	public void setCapsApplicantFlatNoPlotNoHouseNo(
			String capsApplicantFlatNoPlotNoHouseNo) {
		this.capsApplicantFlatNoPlotNoHouseNo = capsApplicantFlatNoPlotNoHouseNo;
	}
	public String getCapsApplicantBldgNoSocietyName() {
		return capsApplicantBldgNoSocietyName;
	}
	public void setCapsApplicantBldgNoSocietyName(
			String capsApplicantBldgNoSocietyName) {
		this.capsApplicantBldgNoSocietyName = capsApplicantBldgNoSocietyName;
	}
	public String getCapsApplicantRoadNoNameAreaLocality() {
		return capsApplicantRoadNoNameAreaLocality;
	}
	public void setCapsApplicantRoadNoNameAreaLocality(
			String capsApplicantRoadNoNameAreaLocality) {
		this.capsApplicantRoadNoNameAreaLocality = capsApplicantRoadNoNameAreaLocality;
	}
	public String getCapsApplicantCity() {
		return capsApplicantCity;
	}
	public void setCapsApplicantCity(String capsApplicantCity) {
		this.capsApplicantCity = capsApplicantCity;
	}
	public String getCapsApplicantLandmark() {
		return capsApplicantLandmark;
	}
	public void setCapsApplicantLandmark(String capsApplicantLandmark) {
		this.capsApplicantLandmark = capsApplicantLandmark;
	}
	public String getCapsApplicantStateCode() {
		return capsApplicantStateCode;
	}
	public void setCapsApplicantStateCode(String capsApplicantStateCode) {
		this.capsApplicantStateCode = capsApplicantStateCode;
	}
	public String getCapsApplicantState() {
		return capsApplicantState;
	}
	public void setCapsApplicantState(String capsApplicantState) {
		this.capsApplicantState = capsApplicantState;
	}
	public String getCapsApplicantPinCode() {
		return capsApplicantPinCode;
	}
	public void setCapsApplicantPinCode(String capsApplicantPinCode) {
		this.capsApplicantPinCode = capsApplicantPinCode;
	}
	public String getCapsApplicantCountryCode() {
		return capsApplicantCountryCode;
	}
	public void setCapsApplicantCountryCode(String capsApplicantCountryCode) {
		this.capsApplicantCountryCode = capsApplicantCountryCode;
	}
	public String getBureauScore() {
		return bureauScore;
	}
	public void setBureauScore(String bureauScore) {
		this.bureauScore = bureauScore;
	}
	public String getBureauScoreConfidLevel() {
		return bureauScoreConfidLevel;
	}
	public void setBureauScoreConfidLevel(String bureauScoreConfidLevel) {
		this.bureauScoreConfidLevel = bureauScoreConfidLevel;
	}
	public String getCreditRating() {
		return creditRating;
	}
	public void setCreditRating(String creditRating) {
		this.creditRating = creditRating;
	}
	public String getTnOfBFHLCADExhl() {
		return tnOfBFHLCADExhl;
	}
	public void setTnOfBFHLCADExhl(String tnOfBFHLCADExhl) {
		this.tnOfBFHLCADExhl = tnOfBFHLCADExhl;
	}
	public String getTotValOfBFHLCAD() {
		return totValOfBFHLCAD;
	}
	public void setTotValOfBFHLCAD(String totValOfBFHLCAD) {
		this.totValOfBFHLCAD = totValOfBFHLCAD;
	}
	public String getMNTSMRBFHLCAD() {
		return MNTSMRBFHLCAD;
	}
	public void setMNTSMRBFHLCAD(String mNTSMRBFHLCAD) {
		MNTSMRBFHLCAD = mNTSMRBFHLCAD;
	}
	public String getTnOfHLCAD() {
		return tnOfHLCAD;
	}
	public void setTnOfHLCAD(String tnOfHLCAD) {
		this.tnOfHLCAD = tnOfHLCAD;
	}
	public String getTotValOfHLCAD() {
		return totValOfHLCAD;
	}
	public void setTotValOfHLCAD(String totValOfHLCAD) {
		this.totValOfHLCAD = totValOfHLCAD;
	}
	public String getMntsmrHLCAD() {
		return mntsmrHLCAD;
	}
	public void setMntsmrHLCAD(String mntsmrHLCAD) {
		this.mntsmrHLCAD = mntsmrHLCAD;
	}
	public String getTnOfTelcosCAD() {
		return tnOfTelcosCAD;
	}
	public void setTnOfTelcosCAD(String tnOfTelcosCAD) {
		this.tnOfTelcosCAD = tnOfTelcosCAD;
	}
	public String getTotValOfTelcosCad() {
		return totValOfTelcosCad;
	}
	public void setTotValOfTelcosCad(String totValOfTelcosCad) {
		this.totValOfTelcosCad = totValOfTelcosCad;
	}
	public String getMntsmrTelcosCad() {
		return mntsmrTelcosCad;
	}
	public void setMntsmrTelcosCad(String mntsmrTelcosCad) {
		this.mntsmrTelcosCad = mntsmrTelcosCad;
	}
	public String getTnOfmfCAD() {
		return tnOfmfCAD;
	}
	public void setTnOfmfCAD(String tnOfmfCAD) {
		this.tnOfmfCAD = tnOfmfCAD;
	}
	public String getTotValOfmfCAD() {
		return totValOfmfCAD;
	}
	public void setTotValOfmfCAD(String totValOfmfCAD) {
		this.totValOfmfCAD = totValOfmfCAD;
	}
	public String getMntsmrmfCAD() {
		return mntsmrmfCAD;
	}
	public void setMntsmrmfCAD(String mntsmrmfCAD) {
		this.mntsmrmfCAD = mntsmrmfCAD;
	}
	public String getTnOfRetailCAD() {
		return tnOfRetailCAD;
	}
	public void setTnOfRetailCAD(String tnOfRetailCAD) {
		this.tnOfRetailCAD = tnOfRetailCAD;
	}
	public String getTotValOfRetailCAD() {
		return totValOfRetailCAD;
	}
	public void setTotValOfRetailCAD(String totValOfRetailCAD) {
		this.totValOfRetailCAD = totValOfRetailCAD;
	}
	public String getMntsmrRetailCAD() {
		return mntsmrRetailCAD;
	}
	public void setMntsmrRetailCAD(String mntsmrRetailCAD) {
		this.mntsmrRetailCAD = mntsmrRetailCAD;
	}
	public String getTnOfAllCAD() {
		return tnOfAllCAD;
	}
	public void setTnOfAllCAD(String tnOfAllCAD) {
		this.tnOfAllCAD = tnOfAllCAD;
	}
	public String getTotValOfAllCAD() {
		return totValOfAllCAD;
	}
	public void setTotValOfAllCAD(String totValOfAllCAD) {
		this.totValOfAllCAD = totValOfAllCAD;
	}
	public String getMntsmrCADAll() {
		return mntsmrCADAll;
	}
	public void setMntsmrCADAll(String mntsmrCADAll) {
		this.mntsmrCADAll = mntsmrCADAll;
	}
	public String getTnOfBFHLACAExhl() {
		return tnOfBFHLACAExhl;
	}
	public void setTnOfBFHLACAExhl(String tnOfBFHLACAExhl) {
		this.tnOfBFHLACAExhl = tnOfBFHLACAExhl;
	}
	public String getBalBFHLACAExhl() {
		return balBFHLACAExhl;
	}
	public void setBalBFHLACAExhl(String balBFHLACAExhl) {
		this.balBFHLACAExhl = balBFHLACAExhl;
	}
	public String getWcdstBFHLACAExhl() {
		return wcdstBFHLACAExhl;
	}
	public void setWcdstBFHLACAExhl(String wcdstBFHLACAExhl) {
		this.wcdstBFHLACAExhl = wcdstBFHLACAExhl;
	}
	public String getWdspr6MntBFHLACAExhl() {
		return wdspr6MntBFHLACAExhl;
	}
	public void setWdspr6MntBFHLACAExhl(String wdspr6MntBFHLACAExhl) {
		this.wdspr6MntBFHLACAExhl = wdspr6MntBFHLACAExhl;
	}
	public String getWdspr712MntBFHLACAExhl() {
		return wdspr712MntBFHLACAExhl;
	}
	public void setWdspr712MntBFHLACAExhl(String wdspr712MntBFHLACAExhl) {
		this.wdspr712MntBFHLACAExhl = wdspr712MntBFHLACAExhl;
	}
	public String getAgeOfOldestBFHLACAExhl() {
		return ageOfOldestBFHLACAExhl;
	}
	public void setAgeOfOldestBFHLACAExhl(String ageOfOldestBFHLACAExhl) {
		this.ageOfOldestBFHLACAExhl = ageOfOldestBFHLACAExhl;
	}
	public String getHcbperrevaccBFHLACAExhl() {
		return hcbperrevaccBFHLACAExhl;
	}
	public void setHcbperrevaccBFHLACAExhl(String hcbperrevaccBFHLACAExhl) {
		this.hcbperrevaccBFHLACAExhl = hcbperrevaccBFHLACAExhl;
	}
	public String getTcbperrevaccBFHLACAExhl() {
		return tcbperrevaccBFHLACAExhl;
	}
	public void setTcbperrevaccBFHLACAExhl(String tcbperrevaccBFHLACAExhl) {
		this.tcbperrevaccBFHLACAExhl = tcbperrevaccBFHLACAExhl;
	}
	public String getTnOfHlACA() {
		return tnOfHlACA;
	}
	public void setTnOfHlACA(String tnOfHlACA) {
		this.tnOfHlACA = tnOfHlACA;
	}
	public String getBalHlACA() {
		return balHlACA;
	}
	public void setBalHlACA(String balHlACA) {
		this.balHlACA = balHlACA;
	}
	public String getWcdstHlACA() {
		return wcdstHlACA;
	}
	public void setWcdstHlACA(String wcdstHlACA) {
		this.wcdstHlACA = wcdstHlACA;
	}
	public String getWdspr6MnthlACA() {
		return wdspr6MnthlACA;
	}
	public void setWdspr6MnthlACA(String wdspr6MnthlACA) {
		this.wdspr6MnthlACA = wdspr6MnthlACA;
	}
	public String getWdspr712mnthlACA() {
		return wdspr712mnthlACA;
	}
	public void setWdspr712mnthlACA(String wdspr712mnthlACA) {
		this.wdspr712mnthlACA = wdspr712mnthlACA;
	}
	public String getAgeOfOldesthlACA() {
		return ageOfOldesthlACA;
	}
	public void setAgeOfOldesthlACA(String ageOfOldesthlACA) {
		this.ageOfOldesthlACA = ageOfOldesthlACA;
	}
	public String getTnOfMfACA() {
		return tnOfMfACA;
	}
	public void setTnOfMfACA(String tnOfMfACA) {
		this.tnOfMfACA = tnOfMfACA;
	}
	public String getTotalBalMfACA() {
		return totalBalMfACA;
	}
	public void setTotalBalMfACA(String totalBalMfACA) {
		this.totalBalMfACA = totalBalMfACA;
	}
	public String getWcdstMfACA() {
		return wcdstMfACA;
	}
	public void setWcdstMfACA(String wcdstMfACA) {
		this.wcdstMfACA = wcdstMfACA;
	}
	public String getWdspr6MntMfACA() {
		return wdspr6MntMfACA;
	}
	public void setWdspr6MntMfACA(String wdspr6MntMfACA) {
		this.wdspr6MntMfACA = wdspr6MntMfACA;
	}
	public String getWdspr712mntMfACA() {
		return wdspr712mntMfACA;
	}
	public void setWdspr712mntMfACA(String wdspr712mntMfACA) {
		this.wdspr712mntMfACA = wdspr712mntMfACA;
	}
	public String getAgeOfOldestMfACA() {
		return ageOfOldestMfACA;
	}
	public void setAgeOfOldestMfACA(String ageOfOldestMfACA) {
		this.ageOfOldestMfACA = ageOfOldestMfACA;
	}
	public String getTnOfTelcosACA() {
		return tnOfTelcosACA;
	}
	public void setTnOfTelcosACA(String tnOfTelcosACA) {
		this.tnOfTelcosACA = tnOfTelcosACA;
	}
	public String getTotalBalTelcosACA() {
		return totalBalTelcosACA;
	}
	public void setTotalBalTelcosACA(String totalBalTelcosACA) {
		this.totalBalTelcosACA = totalBalTelcosACA;
	}
	public String getWcdstTelcosACA() {
		return wcdstTelcosACA;
	}
	public void setWcdstTelcosACA(String wcdstTelcosACA) {
		this.wcdstTelcosACA = wcdstTelcosACA;
	}
	public String getWdspr6mntTelcosACA() {
		return wdspr6mntTelcosACA;
	}
	public void setWdspr6mntTelcosACA(String wdspr6mntTelcosACA) {
		this.wdspr6mntTelcosACA = wdspr6mntTelcosACA;
	}
	public String getWdspr712mntTelcosACA() {
		return wdspr712mntTelcosACA;
	}
	public void setWdspr712mntTelcosACA(String wdspr712mntTelcosACA) {
		this.wdspr712mntTelcosACA = wdspr712mntTelcosACA;
	}
	public String getAgeOfOldestTelcosACA() {
		return ageOfOldestTelcosACA;
	}
	public void setAgeOfOldestTelcosACA(String ageOfOldestTelcosACA) {
		this.ageOfOldestTelcosACA = ageOfOldestTelcosACA;
	}
	public String getTnOfRetailACA() {
		return tnOfRetailACA;
	}
	public void setTnOfRetailACA(String tnOfRetailACA) {
		this.tnOfRetailACA = tnOfRetailACA;
	}
	public String getTotalBalRetailACA() {
		return totalBalRetailACA;
	}
	public void setTotalBalRetailACA(String totalBalRetailACA) {
		this.totalBalRetailACA = totalBalRetailACA;
	}
	public String getWcdstRetailACA() {
		return wcdstRetailACA;
	}
	public void setWcdstRetailACA(String wcdstRetailACA) {
		this.wcdstRetailACA = wcdstRetailACA;
	}
	public String getWdspr6mntRetailACA() {
		return wdspr6mntRetailACA;
	}
	public void setWdspr6mntRetailACA(String wdspr6mntRetailACA) {
		this.wdspr6mntRetailACA = wdspr6mntRetailACA;
	}
	public String getWdspr712mntRetailACA() {
		return wdspr712mntRetailACA;
	}
	public void setWdspr712mntRetailACA(String wdspr712mntRetailACA) {
		this.wdspr712mntRetailACA = wdspr712mntRetailACA;
	}
	public String getAgeOfOldestRetailACA() {
		return ageOfOldestRetailACA;
	}
	public void setAgeOfOldestRetailACA(String ageOfOldestRetailACA) {
		this.ageOfOldestRetailACA = ageOfOldestRetailACA;
	}
	public String getHcblmperrevaccret() {
		return hcblmperrevaccret;
	}
	public void setHcblmperrevaccret(String hcblmperrevaccret) {
		this.hcblmperrevaccret = hcblmperrevaccret;
	}
	public String getTotCurBallmperrevaccret() {
		return totCurBallmperrevaccret;
	}
	public void setTotCurBallmperrevaccret(String totCurBallmperrevaccret) {
		this.totCurBallmperrevaccret = totCurBallmperrevaccret;
	}
	public String getTnOfallACA() {
		return tnOfallACA;
	}
	public void setTnOfallACA(String tnOfallACA) {
		this.tnOfallACA = tnOfallACA;
	}
	public String getBalAllACAExhl() {
		return balAllACAExhl;
	}
	public void setBalAllACAExhl(String balAllACAExhl) {
		this.balAllACAExhl = balAllACAExhl;
	}
	public String getWcdstAllACA() {
		return wcdstAllACA;
	}
	public void setWcdstAllACA(String wcdstAllACA) {
		this.wcdstAllACA = wcdstAllACA;
	}
	public String getWdspr6mntallACA() {
		return wdspr6mntallACA;
	}
	public void setWdspr6mntallACA(String wdspr6mntallACA) {
		this.wdspr6mntallACA = wdspr6mntallACA;
	}
	public String getWdspr712mntAllACA() {
		return wdspr712mntAllACA;
	}
	public void setWdspr712mntAllACA(String wdspr712mntAllACA) {
		this.wdspr712mntAllACA = wdspr712mntAllACA;
	}
	public String getAgeOfOldestAllACA() {
		return ageOfOldestAllACA;
	}
	public void setAgeOfOldestAllACA(String ageOfOldestAllACA) {
		this.ageOfOldestAllACA = ageOfOldestAllACA;
	}
	public String getTnOfndelBFHLinACAExhl() {
		return tnOfndelBFHLinACAExhl;
	}
	public void setTnOfndelBFHLinACAExhl(String tnOfndelBFHLinACAExhl) {
		this.tnOfndelBFHLinACAExhl = tnOfndelBFHLinACAExhl;
	}
	public String getTnOfDelBFHLInACAExhl() {
		return tnOfDelBFHLInACAExhl;
	}
	public void setTnOfDelBFHLInACAExhl(String tnOfDelBFHLInACAExhl) {
		this.tnOfDelBFHLInACAExhl = tnOfDelBFHLInACAExhl;
	}
	public String getTnOfnDelHLInACA() {
		return tnOfnDelHLInACA;
	}
	public void setTnOfnDelHLInACA(String tnOfnDelHLInACA) {
		this.tnOfnDelHLInACA = tnOfnDelHLInACA;
	}
	public String getTnOfDelhlInACA() {
		return tnOfDelhlInACA;
	}
	public void setTnOfDelhlInACA(String tnOfDelhlInACA) {
		this.tnOfDelhlInACA = tnOfDelhlInACA;
	}
	public String getTnOfnDelmfinACA() {
		return tnOfnDelmfinACA;
	}
	public void setTnOfnDelmfinACA(String tnOfnDelmfinACA) {
		this.tnOfnDelmfinACA = tnOfnDelmfinACA;
	}
	public String getTnOfDelmfinACA() {
		return tnOfDelmfinACA;
	}
	public void setTnOfDelmfinACA(String tnOfDelmfinACA) {
		this.tnOfDelmfinACA = tnOfDelmfinACA;
	}
	public String getTnOfnDelTelcosInACA() {
		return tnOfnDelTelcosInACA;
	}
	public void setTnOfnDelTelcosInACA(String tnOfnDelTelcosInACA) {
		this.tnOfnDelTelcosInACA = tnOfnDelTelcosInACA;
	}
	public String getTnOfdelTelcosInACA() {
		return tnOfdelTelcosInACA;
	}
	public void setTnOfdelTelcosInACA(String tnOfdelTelcosInACA) {
		this.tnOfdelTelcosInACA = tnOfdelTelcosInACA;
	}
	public String getTnOfndelRetailInACA() {
		return tnOfndelRetailInACA;
	}
	public void setTnOfndelRetailInACA(String tnOfndelRetailInACA) {
		this.tnOfndelRetailInACA = tnOfndelRetailInACA;
	}
	public String getTnOfDelRetailInACA() {
		return tnOfDelRetailInACA;
	}
	public void setTnOfDelRetailInACA(String tnOfDelRetailInACA) {
		this.tnOfDelRetailInACA = tnOfDelRetailInACA;
	}
	public String getBFHLCapsLast90Days() {
		return BFHLCapsLast90Days;
	}
	public void setBFHLCapsLast90Days(String bFHLCapsLast90Days) {
		BFHLCapsLast90Days = bFHLCapsLast90Days;
	}
	public String getMfCapsLast90Days() {
		return mfCapsLast90Days;
	}
	public void setMfCapsLast90Days(String mfCapsLast90Days) {
		this.mfCapsLast90Days = mfCapsLast90Days;
	}
	public String getTelcosCapsLast90Days() {
		return telcosCapsLast90Days;
	}
	public void setTelcosCapsLast90Days(String telcosCapsLast90Days) {
		this.telcosCapsLast90Days = telcosCapsLast90Days;
	}
	public String getRetailCapsLast90Days() {
		return retailCapsLast90Days;
	}
	public void setRetailCapsLast90Days(String retailCapsLast90Days) {
		this.retailCapsLast90Days = retailCapsLast90Days;
	}
	public String getTnOfocomcad() {
		return tnOfocomcad;
	}
	public void setTnOfocomcad(String tnOfocomcad) {
		this.tnOfocomcad = tnOfocomcad;
	}
	public String getTotValOfocomCAD() {
		return totValOfocomCAD;
	}
	public void setTotValOfocomCAD(String totValOfocomCAD) {
		this.totValOfocomCAD = totValOfocomCAD;
	}
	public String getMntsmrocomCAD() {
		return mntsmrocomCAD;
	}
	public void setMntsmrocomCAD(String mntsmrocomCAD) {
		this.mntsmrocomCAD = mntsmrocomCAD;
	}
	public String getTnOfocomACA() {
		return tnOfocomACA;
	}
	public void setTnOfocomACA(String tnOfocomACA) {
		this.tnOfocomACA = tnOfocomACA;
	}
	public String getBalocomACAExhl() {
		return balocomACAExhl;
	}
	public void setBalocomACAExhl(String balocomACAExhl) {
		this.balocomACAExhl = balocomACAExhl;
	}
	public String getBalOcomacahlonly() {
		return balOcomacahlonly;
	}
	public void setBalOcomacahlonly(String balOcomacahlonly) {
		this.balOcomacahlonly = balOcomacahlonly;
	}
	public String getWcdstocomACA() {
		return wcdstocomACA;
	}
	public void setWcdstocomACA(String wcdstocomACA) {
		this.wcdstocomACA = wcdstocomACA;
	}
	public String getHcblmperrevocomACA() {
		return hcblmperrevocomACA;
	}
	public void setHcblmperrevocomACA(String hcblmperrevocomACA) {
		this.hcblmperrevocomACA = hcblmperrevocomACA;
	}
	public String getTnOfnDelocominACA() {
		return tnOfnDelocominACA;
	}
	public void setTnOfnDelocominACA(String tnOfnDelocominACA) {
		this.tnOfnDelocominACA = tnOfnDelocominACA;
	}
	public String getTnOfDelocominACA() {
		return tnOfDelocominACA;
	}
	public void setTnOfDelocominACA(String tnOfDelocominACA) {
		this.tnOfDelocominACA = tnOfDelocominACA;
	}
	public String getTnOfocomCapsLast90Days() {
		return tnOfocomCapsLast90Days;
	}
	public void setTnOfocomCapsLast90Days(String tnOfocomCapsLast90Days) {
		this.tnOfocomCapsLast90Days = tnOfocomCapsLast90Days;
	}
	public String getAnyRelcbdatadisyn() {
		return anyRelcbdatadisyn;
	}
	public void setAnyRelcbdatadisyn(String anyRelcbdatadisyn) {
		this.anyRelcbdatadisyn = anyRelcbdatadisyn;
	}
	public String getOthrelcbdfcposmatyn() {
		return othrelcbdfcposmatyn;
	}
	public void setOthrelcbdfcposmatyn(String othrelcbdfcposmatyn) {
		this.othrelcbdfcposmatyn = othrelcbdfcposmatyn;
	}
	public String getTnOfCADclassedassfwdwo() {
		return tnOfCADclassedassfwdwo;
	}
	public void setTnOfCADclassedassfwdwo(String tnOfCADclassedassfwdwo) {
		this.tnOfCADclassedassfwdwo = tnOfCADclassedassfwdwo;
	}
	public String getMntsmrcadclassedassfwdwo() {
		return mntsmrcadclassedassfwdwo;
	}
	public void setMntsmrcadclassedassfwdwo(String mntsmrcadclassedassfwdwo) {
		this.mntsmrcadclassedassfwdwo = mntsmrcadclassedassfwdwo;
	}
	public String getNumOfCadsfwdwolast24mnt() {
		return numOfCadsfwdwolast24mnt;
	}
	public void setNumOfCadsfwdwolast24mnt(String numOfCadsfwdwolast24mnt) {
		this.numOfCadsfwdwolast24mnt = numOfCadsfwdwolast24mnt;
	}
	public String getTotCurBalLivesAcc() {
		return totCurBalLivesAcc;
	}
	public void setTotCurBalLivesAcc(String totCurBalLivesAcc) {
		this.totCurBalLivesAcc = totCurBalLivesAcc;
	}
	public String getTotCurBalLiveuAcc() {
		return totCurBalLiveuAcc;
	}
	public void setTotCurBalLiveuAcc(String totCurBalLiveuAcc) {
		this.totCurBalLiveuAcc = totCurBalLiveuAcc;
	}
	public String getTotCurBalMaxBalLivesAcc() {
		return totCurBalMaxBalLivesAcc;
	}
	public void setTotCurBalMaxBalLivesAcc(String totCurBalMaxBalLivesAcc) {
		this.totCurBalMaxBalLivesAcc = totCurBalMaxBalLivesAcc;
	}
	public String getTotCurBalMaxBalLiveuAcc() {
		return totCurBalMaxBalLiveuAcc;
	}
	public void setTotCurBalMaxBalLiveuAcc(String totCurBalMaxBalLiveuAcc) {
		this.totCurBalMaxBalLiveuAcc = totCurBalMaxBalLiveuAcc;
	}
	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}
	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}
	public String getOutputWriteDate() {
		return outputWriteDate;
	}
	public void setOutputWriteDate(String outputWriteDate) {
		this.outputWriteDate = outputWriteDate;
	}
	public String getOutputReadDate() {
		return outputReadDate;
	}
	public void setOutputReadDate(String outputReadDate) {
		this.outputReadDate = outputReadDate;
	}


	@Override
	public String toString() {
		return "ExperianSROPDomain [srNo=" + srNo + ", soaSourceName="
				+ soaSourceName + ", memberReferenceNumber="
				+ memberReferenceNumber + ", enquiryDate=" + enquiryDate
				+ ", systemcodeHeader=" + systemcodeHeader
				+ ", messageTextHeader=" + messageTextHeader + ", reportdate="
				+ reportdate + ", reportimeHeader=" + reportimeHeader
				+ ", userMessageText=" + userMessageText + ", enquiryUsername="
				+ enquiryUsername + ", creditProfileReportDate="
				+ creditProfileReportDate + ", CreditProfileReportTime="
				+ CreditProfileReportTime + ", version=" + version
				+ ", reportNumber=" + reportNumber + ", subscriber="
				+ subscriber + ", subscriberName=" + subscriberName
				+ ", enquiryReason=" + enquiryReason + ", financePurpose="
				+ financePurpose + ", amountFinanced=" + amountFinanced
				+ ", durationOfAgreement=" + durationOfAgreement
				+ ", lastname=" + lastname + ", firstname=" + firstname
				+ ", middleName1=" + middleName1 + ", middleName2="
				+ middleName2 + ", middleName3=" + middleName3
				+ ", genderCode=" + genderCode + ", gender=" + gender
				+ ", incomeTaxPanApp=" + incomeTaxPanApp + ", panIssueDateApp="
				+ panIssueDateApp + ", panExpDateApp=" + panExpDateApp
				+ ", passportNumberApp=" + passportNumberApp
				+ ", passportIssueDateApp=" + passportIssueDateApp
				+ ", passportExpDateApp=" + passportExpDateApp
				+ ", voterIdentityCardApp=" + voterIdentityCardApp
				+ ", voterIdIssueDateApp=" + voterIdIssueDateApp
				+ ", voterIdExpDateApp=" + voterIdExpDateApp
				+ ", driverLicenseNumberApp=" + driverLicenseNumberApp
				+ ", driverLicenseIssueDateApp=" + driverLicenseIssueDateApp
				+ ", driverLicenseExpDateApp=" + driverLicenseExpDateApp
				+ ", rationCardNumberApp=" + rationCardNumberApp
				+ ", rationCardIssueDateApp=" + rationCardIssueDateApp
				+ ", rationCardExpDateApp=" + rationCardExpDateApp
				+ ", universalIdNumberApp=" + universalIdNumberApp
				+ ", universalIdIssueDateApp=" + universalIdIssueDateApp
				+ ", universalIdExpDateApp=" + universalIdExpDateApp
				+ ", dateOfBirthApplicant=" + dateOfBirthApplicant
				+ ", telephoneNumberApplicant1st="
				+ telephoneNumberApplicant1st + ", teleExtensionApp="
				+ teleExtensionApp + ", teleTypeApp=" + teleTypeApp
				+ ", mobilePhoneNumber=" + mobilePhoneNumber + ", emailIdApp="
				+ emailIdApp + ", income=" + income + ", maritalStatusCode="
				+ maritalStatusCode + ", maritalStatus=" + maritalStatus
				+ ", employmentStatus=" + employmentStatus
				+ ", dateWithEmployer=" + dateWithEmployer
				+ ", numberOfMajorCreditCardHeld="
				+ numberOfMajorCreditCardHeld + ", flatNoPlotNoHouseNo="
				+ flatNoPlotNoHouseNo + ", bldgNoSocietyName="
				+ bldgNoSocietyName + ", roadNoNameAreaLocality="
				+ roadNoNameAreaLocality + ", city=" + city + ", landmark="
				+ landmark + ", state=" + state + ", pincode=" + pincode
				+ ", countryCode=" + countryCode + ", addFlatNoPlotNoHouseNo="
				+ addFlatNoPlotNoHouseNo + ", addBldgNoSocietyName="
				+ addBldgNoSocietyName + ", addRoadNoNameAreaLocality="
				+ addRoadNoNameAreaLocality + ", addCity=" + addCity
				+ ", addLandmark=" + addLandmark + ", addState=" + addState
				+ ", addPincode=" + addPincode + ", addCountrycode="
				+ addCountrycode + ", creditAccountTotal=" + creditAccountTotal
				+ ", creditAccountActive=" + creditAccountActive
				+ ", creditAccountDefault=" + creditAccountDefault
				+ ", creditAccountClosed=" + creditAccountClosed
				+ ", cadsuitfiledCurrentBalance=" + cadsuitfiledCurrentBalance
				+ ", outstandingBalanceSecured=" + outstandingBalanceSecured
				+ ", outstandingBalSecuredPer=" + outstandingBalSecuredPer
				+ ", outstandingBalanceUnsecured="
				+ outstandingBalanceUnsecured + ", outstandingBalUnsecPer="
				+ outstandingBalUnsecPer + ", outstandingBalanceAll="
				+ outstandingBalanceAll + ", identificationNumber="
				+ identificationNumber + ", caisSubscriberName="
				+ caisSubscriberName + ", accountNumber=" + accountNumber
				+ ", portfolioType=" + portfolioType + ", accountTypeCode="
				+ accountTypeCode + ", accountType=" + accountType
				+ ", openDate=" + openDate + ", highestCreditOrOrgnLoanAmt="
				+ highestCreditOrOrgnLoanAmt + ", termsDuration="
				+ termsDuration + ", termsFrequency=" + termsFrequency
				+ ", scheduledMonthlyPayamt=" + scheduledMonthlyPayamt
				+ ", accountStatusCode=" + accountStatusCode
				+ ", accountStatus=" + accountStatus + ", paymentRating="
				+ paymentRating + ", paymentHistoryProfile="
				+ paymentHistoryProfile + ", specialComment=" + specialComment
				+ ", currentBalanceTl=" + currentBalanceTl
				+ ", amountPastdueTl=" + amountPastdueTl
				+ ", originalChargeOffAmount=" + originalChargeOffAmount
				+ ", dateReported=" + dateReported
				+ ", dateOfFirstDelinquency=" + dateOfFirstDelinquency
				+ ", dateClosed=" + dateClosed + ", dateOfLastPayment="
				+ dateOfLastPayment
				+ ", suitfilledWillFullDefaultWrittenOffStatus="
				+ suitfilledWillFullDefaultWrittenOffStatus
				+ ", suitFiledWilfulDef=" + suitFiledWilfulDef
				+ ", writtenOffSettledStatus=" + writtenOffSettledStatus
				+ ", valueOfCreditsLastMonth=" + valueOfCreditsLastMonth
				+ ", occupationCode=" + occupationCode + ", sattlementAmount="
				+ sattlementAmount + ", valueOfColateral=" + valueOfColateral
				+ ", typeOfColateral=" + typeOfColateral
				+ ", writtenOffAmtTotal=" + writtenOffAmtTotal
				+ ", writtenOffAmtPrincipal=" + writtenOffAmtPrincipal
				+ ", rateOfInterest=" + rateOfInterest + ", repaymentTenure="
				+ repaymentTenure + ", promotionalRateFlag="
				+ promotionalRateFlag + ", caisIncome=" + caisIncome
				+ ", incomeIndicator=" + incomeIndicator
				+ ", incomeFrequencyIndicator=" + incomeFrequencyIndicator
				+ ", defaultStatusDate=" + defaultStatusDate
				+ ", litigationStatusDate=" + litigationStatusDate
				+ ", writeOffStatusDate=" + writeOffStatusDate
				+ ", currencyCode=" + currencyCode + ", subscriberComments="
				+ subscriberComments + ", consumerComments=" + consumerComments
				+ ", accountHolderTypeCode=" + accountHolderTypeCode
				+ ", accountHolderTypeName=" + accountHolderTypeName
				+ ", yearHist=" + yearHist + ", monthHist=" + monthHist
				+ ", daysPastDue=" + daysPastDue + ", assetClassification="
				+ assetClassification + ", yearAdvHist=" + yearAdvHist
				+ ", monthAdvHist=" + monthAdvHist + ", cashLimit=" + cashLimit
				+ ", creditLimitAmt=" + creditLimitAmt + ", actualPaymentAmt="
				+ actualPaymentAmt + ", emiAmt=" + emiAmt
				+ ", currentBalanceAdvHist=" + currentBalanceAdvHist
				+ ", amountPastDueAdvHist=" + amountPastDueAdvHist
				+ ", surNameNonNormalized=" + surNameNonNormalized
				+ ", firstNameNonNormalized=" + firstNameNonNormalized
				+ ", middleName1NonNormalized=" + middleName1NonNormalized
				+ ", middleName2NonNormalized=" + middleName2NonNormalized
				+ ", middleName3NonNormalized=" + middleName3NonNormalized
				+ ", alias=" + alias + ", caisGenderCode=" + caisGenderCode
				+ ", caisGender=" + caisGender + ", caisIncomeTaxPan="
				+ caisIncomeTaxPan + ", caisPassportNumber="
				+ caisPassportNumber + ", voterIdNumber=" + voterIdNumber
				+ ", dateOfBirth=" + dateOfBirth
				+ ", firstLineOfAddNonNormalized="
				+ firstLineOfAddNonNormalized
				+ ", secondLineOfAddNonNormalized="
				+ secondLineOfAddNonNormalized
				+ ", thirdLineOfAddNonNormalized="
				+ thirdLineOfAddNonNormalized + ", cityNonNormalized="
				+ cityNonNormalized + ", fifthLineOfAddNonNormalized="
				+ fifthLineOfAddNonNormalized + ", stateCodeNonNormalized="
				+ stateCodeNonNormalized + ", stateNonNormalized="
				+ stateNonNormalized + ", zipPostalCodeNonNormalized="
				+ zipPostalCodeNonNormalized + ", countryCodeNonNormalized="
				+ countryCodeNonNormalized + ", addIndicatorNonNormalized="
				+ addIndicatorNonNormalized + ", residenceCodeNonNormalized="
				+ residenceCodeNonNormalized + ", residenceNonNormalized="
				+ residenceNonNormalized + ", telephoneNumber="
				+ telephoneNumber + ", teleTypePhone=" + teleTypePhone
				+ ", teleExtensionPhone=" + teleExtensionPhone
				+ ", mobileTelephoneNumber=" + mobileTelephoneNumber
				+ ", faxNumber=" + faxNumber + ", emailIdPhone=" + emailIdPhone
				+ ", incomeTaxPanId=" + incomeTaxPanId + ", panIssueDateId="
				+ panIssueDateId + ", panExpDateId=" + panExpDateId
				+ ", passportNumberId=" + passportNumberId
				+ ", passportIssueDateId=" + passportIssueDateId
				+ ", passportExpDateId=" + passportExpDateId
				+ ", voterIdentityCardId=" + voterIdentityCardId
				+ ", voterIdIssueDateId=" + voterIdIssueDateId
				+ ", voterIdExpDateId=" + voterIdExpDateId
				+ ", driverLicenseNumberId=" + driverLicenseNumberId
				+ ", driverLicenseIssueDateId=" + driverLicenseIssueDateId
				+ ", driverLicenseExpDateId=" + driverLicenseExpDateId
				+ ", rationCardNumberId=" + rationCardNumberId
				+ ", rationCardIssueDateId=" + rationCardIssueDateId
				+ ", rationCardExpDateId=" + rationCardExpDateId
				+ ", universalIdNumberId=" + universalIdNumberId
				+ ", universalIdIssueDateId=" + universalIdIssueDateId
				+ ", universalIdExpDateId=" + universalIdExpDateId
				+ ", emailidId=" + emailidId + ", exactMatch=" + exactMatch
				+ ", capsLast7Days=" + capsLast7Days + ", capsLast30Days="
				+ capsLast30Days + ", capsLast90Days=" + capsLast90Days
				+ ", capsLast180Days=" + capsLast180Days
				+ ", capsSubscriberCode=" + capsSubscriberCode
				+ ", capsSubscriberName=" + capsSubscriberName
				+ ", capsdateofRequest=" + capsdateofRequest
				+ ", capsReportDate=" + capsReportDate + ", capsReportTime="
				+ capsReportTime + ", capsReportNumber=" + capsReportNumber
				+ ", enquiryReasonCode=" + enquiryReasonCode
				+ ", capsEnquiryReason=" + capsEnquiryReason
				+ ", financePurposeCode=" + financePurposeCode
				+ ", capsFinancePurpose=" + capsFinancePurpose
				+ ", capsAmountFinanced=" + capsAmountFinanced
				+ ", capsDurationOfAgreement=" + capsDurationOfAgreement
				+ ", capsApplicantLastName=" + capsApplicantLastName
				+ ", capsApplicantFirstName=" + capsApplicantFirstName
				+ ", capsApplicantMiddleName1=" + capsApplicantMiddleName1
				+ ", capsApplicantMiddleName2=" + capsApplicantMiddleName2
				+ ", capsApplicantMiddleName3=" + capsApplicantMiddleName3
				+ ", capsApplicantgenderCode=" + capsApplicantgenderCode
				+ ", capsApplicantGender=" + capsApplicantGender
				+ ", capsIncomeTaxPan=" + capsIncomeTaxPan
				+ ", capsPanIssueDate=" + capsPanIssueDate
				+ ", capsPanExpDate=" + capsPanExpDate
				+ ", capsPassportNumber=" + capsPassportNumber
				+ ", capsPassportIssueDate=" + capsPassportIssueDate
				+ ", capsPassportExpDate=" + capsPassportExpDate
				+ ", capsVoterIdentityCard=" + capsVoterIdentityCard
				+ ", capsVoterIdIssueDate=" + capsVoterIdIssueDate
				+ ", capsVoterIdExpDate=" + capsVoterIdExpDate
				+ ", capsDriverLicenseNumber=" + capsDriverLicenseNumber
				+ ", capsDriverLicenseIssueDate=" + capsDriverLicenseIssueDate
				+ ", capsDriverLicenseExpDate=" + capsDriverLicenseExpDate
				+ ", capsRationCardNumber=" + capsRationCardNumber
				+ ", capsRationCardIssueDate=" + capsRationCardIssueDate
				+ ", capsRationCardExpDate=" + capsRationCardExpDate
				+ ", capsUniversalIdNumber=" + capsUniversalIdNumber
				+ ", capsUniversalIdIssueDate=" + capsUniversalIdIssueDate
				+ ", capsUniversalIdExpDate=" + capsUniversalIdExpDate
				+ ", capsApplicantDobApplicant=" + capsApplicantDobApplicant
				+ ", capsApplicantTelNoApplicant1st="
				+ capsApplicantTelNoApplicant1st + ", capsApplicantTelExt="
				+ capsApplicantTelExt + ", capsApplicantTelType="
				+ capsApplicantTelType + ", capsApplicantMobilePhoneNumber="
				+ capsApplicantMobilePhoneNumber + ", capsApplicantEmailId="
				+ capsApplicantEmailId + ", capsApplicantIncome="
				+ capsApplicantIncome + ", applicantMaritalStatusCode="
				+ applicantMaritalStatusCode + ", capsApplicantMaritalStatus="
				+ capsApplicantMaritalStatus
				+ ", capsApplicantEmploymentStatusCode="
				+ capsApplicantEmploymentStatusCode
				+ ", capsApplicantEmploymentStatus="
				+ capsApplicantEmploymentStatus
				+ ", capsApplicantDateWithEmployer="
				+ capsApplicantDateWithEmployer
				+ ", capsApplicantNoMajorCrditCardHeld="
				+ capsApplicantNoMajorCrditCardHeld
				+ ", capsApplicantFlatNoPlotNoHouseNo="
				+ capsApplicantFlatNoPlotNoHouseNo
				+ ", capsApplicantBldgNoSocietyName="
				+ capsApplicantBldgNoSocietyName
				+ ", capsApplicantRoadNoNameAreaLocality="
				+ capsApplicantRoadNoNameAreaLocality + ", capsApplicantCity="
				+ capsApplicantCity + ", capsApplicantLandmark="
				+ capsApplicantLandmark + ", capsApplicantStateCode="
				+ capsApplicantStateCode + ", capsApplicantState="
				+ capsApplicantState + ", capsApplicantPinCode="
				+ capsApplicantPinCode + ", capsApplicantCountryCode="
				+ capsApplicantCountryCode + ", bureauScore=" + bureauScore
				+ ", bureauScoreConfidLevel=" + bureauScoreConfidLevel
				+ ", creditRating=" + creditRating + ", tnOfBFHLCADExhl="
				+ tnOfBFHLCADExhl + ", totValOfBFHLCAD=" + totValOfBFHLCAD
				+ ", MNTSMRBFHLCAD=" + MNTSMRBFHLCAD + ", tnOfHLCAD="
				+ tnOfHLCAD + ", totValOfHLCAD=" + totValOfHLCAD
				+ ", mntsmrHLCAD=" + mntsmrHLCAD + ", tnOfTelcosCAD="
				+ tnOfTelcosCAD + ", totValOfTelcosCad=" + totValOfTelcosCad
				+ ", mntsmrTelcosCad=" + mntsmrTelcosCad + ", tnOfmfCAD="
				+ tnOfmfCAD + ", totValOfmfCAD=" + totValOfmfCAD
				+ ", mntsmrmfCAD=" + mntsmrmfCAD + ", tnOfRetailCAD="
				+ tnOfRetailCAD + ", totValOfRetailCAD=" + totValOfRetailCAD
				+ ", mntsmrRetailCAD=" + mntsmrRetailCAD + ", tnOfAllCAD="
				+ tnOfAllCAD + ", totValOfAllCAD=" + totValOfAllCAD
				+ ", mntsmrCADAll=" + mntsmrCADAll + ", tnOfBFHLACAExhl="
				+ tnOfBFHLACAExhl + ", balBFHLACAExhl=" + balBFHLACAExhl
				+ ", wcdstBFHLACAExhl=" + wcdstBFHLACAExhl
				+ ", wdspr6MntBFHLACAExhl=" + wdspr6MntBFHLACAExhl
				+ ", wdspr712MntBFHLACAExhl=" + wdspr712MntBFHLACAExhl
				+ ", ageOfOldestBFHLACAExhl=" + ageOfOldestBFHLACAExhl
				+ ", hcbperrevaccBFHLACAExhl=" + hcbperrevaccBFHLACAExhl
				+ ", tcbperrevaccBFHLACAExhl=" + tcbperrevaccBFHLACAExhl
				+ ", tnOfHlACA=" + tnOfHlACA + ", balHlACA=" + balHlACA
				+ ", wcdstHlACA=" + wcdstHlACA + ", wdspr6MnthlACA="
				+ wdspr6MnthlACA + ", wdspr712mnthlACA=" + wdspr712mnthlACA
				+ ", ageOfOldesthlACA=" + ageOfOldesthlACA + ", tnOfMfACA="
				+ tnOfMfACA + ", totalBalMfACA=" + totalBalMfACA
				+ ", wcdstMfACA=" + wcdstMfACA + ", wdspr6MntMfACA="
				+ wdspr6MntMfACA + ", wdspr712mntMfACA=" + wdspr712mntMfACA
				+ ", ageOfOldestMfACA=" + ageOfOldestMfACA + ", tnOfTelcosACA="
				+ tnOfTelcosACA + ", totalBalTelcosACA=" + totalBalTelcosACA
				+ ", wcdstTelcosACA=" + wcdstTelcosACA
				+ ", wdspr6mntTelcosACA=" + wdspr6mntTelcosACA
				+ ", wdspr712mntTelcosACA=" + wdspr712mntTelcosACA
				+ ", ageOfOldestTelcosACA=" + ageOfOldestTelcosACA
				+ ", tnOfRetailACA=" + tnOfRetailACA + ", totalBalRetailACA="
				+ totalBalRetailACA + ", wcdstRetailACA=" + wcdstRetailACA
				+ ", wdspr6mntRetailACA=" + wdspr6mntRetailACA
				+ ", wdspr712mntRetailACA=" + wdspr712mntRetailACA
				+ ", ageOfOldestRetailACA=" + ageOfOldestRetailACA
				+ ", hcblmperrevaccret=" + hcblmperrevaccret
				+ ", totCurBallmperrevaccret=" + totCurBallmperrevaccret
				+ ", tnOfallACA=" + tnOfallACA + ", balAllACAExhl="
				+ balAllACAExhl + ", wcdstAllACA=" + wcdstAllACA
				+ ", wdspr6mntallACA=" + wdspr6mntallACA
				+ ", wdspr712mntAllACA=" + wdspr712mntAllACA
				+ ", ageOfOldestAllACA=" + ageOfOldestAllACA
				+ ", tnOfndelBFHLinACAExhl=" + tnOfndelBFHLinACAExhl
				+ ", tnOfDelBFHLInACAExhl=" + tnOfDelBFHLInACAExhl
				+ ", tnOfnDelHLInACA=" + tnOfnDelHLInACA + ", tnOfDelhlInACA="
				+ tnOfDelhlInACA + ", tnOfnDelmfinACA=" + tnOfnDelmfinACA
				+ ", tnOfDelmfinACA=" + tnOfDelmfinACA
				+ ", tnOfnDelTelcosInACA=" + tnOfnDelTelcosInACA
				+ ", tnOfdelTelcosInACA=" + tnOfdelTelcosInACA
				+ ", tnOfndelRetailInACA=" + tnOfndelRetailInACA
				+ ", tnOfDelRetailInACA=" + tnOfDelRetailInACA
				+ ", BFHLCapsLast90Days=" + BFHLCapsLast90Days
				+ ", mfCapsLast90Days=" + mfCapsLast90Days
				+ ", telcosCapsLast90Days=" + telcosCapsLast90Days
				+ ", retailCapsLast90Days=" + retailCapsLast90Days
				+ ", tnOfocomcad=" + tnOfocomcad + ", totValOfocomCAD="
				+ totValOfocomCAD + ", mntsmrocomCAD=" + mntsmrocomCAD
				+ ", tnOfocomACA=" + tnOfocomACA + ", balocomACAExhl="
				+ balocomACAExhl + ", balOcomacahlonly=" + balOcomacahlonly
				+ ", wcdstocomACA=" + wcdstocomACA + ", hcblmperrevocomACA="
				+ hcblmperrevocomACA + ", tnOfnDelocominACA="
				+ tnOfnDelocominACA + ", tnOfDelocominACA=" + tnOfDelocominACA
				+ ", tnOfocomCapsLast90Days=" + tnOfocomCapsLast90Days
				+ ", anyRelcbdatadisyn=" + anyRelcbdatadisyn
				+ ", othrelcbdfcposmatyn=" + othrelcbdfcposmatyn
				+ ", tnOfCADclassedassfwdwo=" + tnOfCADclassedassfwdwo
				+ ", mntsmrcadclassedassfwdwo=" + mntsmrcadclassedassfwdwo
				+ ", numOfCadsfwdwolast24mnt=" + numOfCadsfwdwolast24mnt
				+ ", totCurBalLivesAcc=" + totCurBalLivesAcc
				+ ", totCurBalLiveuAcc=" + totCurBalLiveuAcc
				+ ", totCurBalMaxBalLivesAcc=" + totCurBalMaxBalLivesAcc
				+ ", totCurBalMaxBalLiveuAcc=" + totCurBalMaxBalLiveuAcc
				+ ", outputWriteFlag=" + outputWriteFlag + ", outputWriteDate="
				+ outputWriteDate + ", outputReadDate=" + outputReadDate + "]";
	}


	public String getAccountKey() {
		return accountKey;
	}


	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}

}    