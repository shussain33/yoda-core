package com.softcell.gonogo.model.mbdatapush;

import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.mbdatapush.chm.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class HighmarkBaseDomainPopulation {
	private static Logger logger_=LoggerFactory.getLogger(HighmarkBaseDomainPopulation.class);
			
	String hmDateTimeFormat = "dd-MM-yyyy HH:mm:ss";
	String reqDomainDateTimeFormat = "dd-MM-yyyy";
	
	
	private static Integer parseInteger(String integerValue){
		Integer value = null;
		try{
			if(StringUtils.isNotBlank(integerValue)){
					if(StringUtils.contains(integerValue, ".")){
						integerValue = StringUtils.substring(StringUtils.trim(integerValue),0, StringUtils.indexOf(StringUtils.trim(integerValue), "."));
					}
					if(StringUtils.isNumeric(integerValue)){
						value = new Integer(integerValue);
					}else{
						System.out.println("SKIP PARSEING INTEGER "+integerValue);
					}
			}
		}catch(Exception e){
			logger_.error("**"+e.getMessage(), e);
			e.printStackTrace();
			System.out.println("NOT PARSEING INTEGER "+integerValue);
		}
		return value;
	}
	
	private static BigInteger parseBigInteger(String integerValue){
		BigInteger value = null;
		try{
			if(StringUtils.isNotBlank(integerValue)){
					if(StringUtils.contains(integerValue, ".")){
						integerValue = StringUtils.substring(StringUtils.trim(integerValue),0, StringUtils.indexOf(StringUtils.trim(integerValue), "."));
					}
					if(StringUtils.isNumeric(integerValue)){
						value = new BigInteger(integerValue);
					}else{
						System.out.println("SKIP PARSEING INTEGER "+integerValue);
					}
			}
		}catch(Exception e){
			logger_.error("**"+e.getMessage(), e);
			e.printStackTrace();
			System.out.println("NOT PARSEING INTEGER " + integerValue);
		}
		return value;
	}
	

	public List<HibHighmarkBaseSropDomain> highmarkBaseSropDomainGenerator(BaseReportFile baseReportFile, String  srNo, String soaSourceName, String memberReferenceNumber, String soaFileName){
		
		List<HibHighmarkBaseSropDomain> highmarkSROPBaseDomainsList= new ArrayList<HibHighmarkBaseSropDomain>();
		
		Integer srNumber = null;
		try{
			srNumber = Integer.parseInt(srNo);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(null != baseReportFile){
			HibHighmarkBaseSropDomain highmarkSROPBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
			
			
			/*if( null != baseReportFile.getBaseFileHeader()){
				BaseFileHeader baseFileHeader = baseReportFile.getBaseFileHeader();
				highmarkSROPBaseDomain.setReportType(baseFileHeader.getReportType());
				highmarkSROPBaseDomain.setReportDate(baseFileHeader.getReportDate());
				highmarkSROPBaseDomain.setInqCountFile(parseBigInteger(baseFileHeader.getInqCntFile()));
				
			}*/
			
			BaseReports baseReports = baseReportFile.getBaseReports();
			
			if(null != baseReports  && null != baseReports.getBaseReport()){
				BaseReport baseReport = baseReports.getBaseReport();
				
				if (null != baseReport) {
					Header header = baseReport.getHeader();
					if(null != header){
						highmarkSROPBaseDomain.setDateOfRequest(parseDate(header.getDateOfRequest()));
						highmarkSROPBaseDomain.setPreparedFor(header.getPreparedFor());
						highmarkSROPBaseDomain.setPreparedForId(header.getPreparedForId());
						highmarkSROPBaseDomain.setDateOfIssue(parseDate(header.getDateOfIssue()));
						highmarkSROPBaseDomain.setReportId(header.getReportId());
						highmarkSROPBaseDomain.setBatchId(parseBigInteger(header.getBatchId()));
						highmarkSROPBaseDomain.setStatus(header.getStatus());
						highmarkSROPBaseDomain.setOutputWriteFlag("0");
						highmarkSROPBaseDomain.setOutputWritetime(DateUtils.getFormattedDate(new Date(), "ddMMyyyy HH:mm:ss"));
					}
					
					Request request = baseReport.getRequest();
					if(null != request){
						highmarkSROPBaseDomain.setNameIQ(request.getName());
						highmarkSROPBaseDomain.setAka(request.getAka());
						highmarkSROPBaseDomain.setSpouse(request.getSpouse());
						highmarkSROPBaseDomain.setFather(request.getFather());
						highmarkSROPBaseDomain.setMother(request.getMother());
						highmarkSROPBaseDomain.setDobIQ(parseDate(request.getDob()));
						highmarkSROPBaseDomain.setAge(parseInteger(request.getAge()));
						highmarkSROPBaseDomain.setAgeAsOn(parseDate(request.getAgeAsOn()));
						highmarkSROPBaseDomain.setRationCard(request.getRationCard());
						highmarkSROPBaseDomain.setPassport(request.getPassport());
						highmarkSROPBaseDomain.setVotersId(request.getVotersId());
						highmarkSROPBaseDomain.setDrivingLicenceNo(request.getDrivingLicenseNo());
						highmarkSROPBaseDomain.setPan(request.getPan());
						highmarkSROPBaseDomain.setGender(request.getGender());
						highmarkSROPBaseDomain.setOwnership(request.getOwnership());
						highmarkSROPBaseDomain.setAddress1(request.getAddress1());
						highmarkSROPBaseDomain.setAddress2(request.getAddress2());
						highmarkSROPBaseDomain.setAddress3(request.getAddress3());
						highmarkSROPBaseDomain.setPhone1(parseBigInteger(request.getPhone1()));
						highmarkSROPBaseDomain.setPhone2(parseBigInteger(request.getPhone2()));
						highmarkSROPBaseDomain.setPhone3(parseBigInteger(request.getPhone3()));
						highmarkSROPBaseDomain.setEmailId1(request.getEmail1());
						highmarkSROPBaseDomain.setEmailId2(request.getEmail2());
						highmarkSROPBaseDomain.setBranch(request.getBranch());
						highmarkSROPBaseDomain.setKendra(request.getKendra());
						highmarkSROPBaseDomain.setMbrId(request.getMemberId());
						highmarkSROPBaseDomain.setLosAppId(request.getLosApplicationId());
						highmarkSROPBaseDomain.setCreditInquiryPurposeType(request.getCreditInquiryPurposeType());
						highmarkSROPBaseDomain.setCreditInquiryPurposeTypeDesc(request.getCreditInquiryPurposeTypeDescription());
						highmarkSROPBaseDomain.setCreditInquiryStage(request.getCreditInquiryStage());
						highmarkSROPBaseDomain.setCreditReportId(request.getCreditReportId());
						highmarkSROPBaseDomain.setCreditRequestType(request.getCreditRequestType());
						highmarkSROPBaseDomain.setCreditReportTransectionDateTime(request.getCreditReportTransectionDatetime());
						highmarkSROPBaseDomain.setAccountOpenDate(request.getAccountOpenDate());
						highmarkSROPBaseDomain.setLoanAmount(parseBigInteger(request.getLoanAmount()));
					}
					
					AccountSummary accountSummary = baseReport.getAccountSummary();
					if(null != accountSummary){
						PrimaryAccountsSummary primaryAccountsSummary = accountSummary.getPrimaryAccountsSummary();
						if(null != primaryAccountsSummary){
							highmarkSROPBaseDomain.setPrimaryNumberOfAccount(parseInt(primaryAccountsSummary.getPrimaryNumberOfAccounts()));
							highmarkSROPBaseDomain.setPrimaryActiveNoOfAccount(parseInt(primaryAccountsSummary.getPrimaryActiveNumberOfAccounts()));
							highmarkSROPBaseDomain.setPrimaryOverdueNumberOfAccount(parseInt(primaryAccountsSummary.getPrimaryOverdueNumberOfAccounts()));
							highmarkSROPBaseDomain.setPrimaryCurrentBalance(primaryAccountsSummary.getPrimaryCurrentBalance());
							highmarkSROPBaseDomain.setPrimarySanctionedAmount(primaryAccountsSummary.getPrimarySanctionedAmount());
							highmarkSROPBaseDomain.setPrimarySecuredNumberOfAccount(parseInt(primaryAccountsSummary.getPrimarySecuredNumberOfAccounts()));
							highmarkSROPBaseDomain.setPrimaryUnsecuredNumberOfAccount(parseInt(primaryAccountsSummary.getPrimaryUnsecuredNumberOfAccounts()));
							highmarkSROPBaseDomain.setPrimaryUntaggedNumberOfAccount(parseInt(primaryAccountsSummary.getPrimaryUntaggedNumberOfAccounts()));
						}
						
						SecondaryAccountSummary secondaryAccountSummary = accountSummary.getSecondaryAccountSummary();
						if(null != secondaryAccountSummary){
							highmarkSROPBaseDomain.setSecondaryVNumberOfAccount(parseInt(secondaryAccountSummary.getSecondaryNumberOfAccounts()));
							highmarkSROPBaseDomain.setSecondaryActiveNumberOfAccount(parseInt(secondaryAccountSummary.getSecondaryActiveNumberOfAccounts()));
							highmarkSROPBaseDomain.setSecondaryOverdueNumberOfAccount(parseInt(secondaryAccountSummary.getSecondaryOverdueNumberOfAccounts()));
							highmarkSROPBaseDomain.setSecondaryCurrentBalance(secondaryAccountSummary.getSecondaryCurrentBalance());
							highmarkSROPBaseDomain.setSecondarySanctionedAmount(secondaryAccountSummary.getSecondarySanctionedAmount());
							highmarkSROPBaseDomain.setSecondarySecuredNumberOfAccount(parseInt(secondaryAccountSummary.getSecondarySecuredNumberOfAccounts()));
							highmarkSROPBaseDomain.setSecondaryUnsecuredNumberOfAccount(parseInt(secondaryAccountSummary.getSecondaryUnsecuredNumberOfAccounts()));
							highmarkSROPBaseDomain.setSecondaryUntaggedNumberOfAccount(parseInt(secondaryAccountSummary.getSecondaryUntaggedNumberOfAccounts()));
						}
						
						DerivedAttributes drivedAttributes = accountSummary.getDrivedAttributes();
						if(null != drivedAttributes){
							highmarkSROPBaseDomain.setInquriesInLastSixMonth(parseBigInteger(drivedAttributes.getInquiriesInLastSixMonth()));
							highmarkSROPBaseDomain.setLengthOfCreditHistoryYear(parseBigInteger(drivedAttributes.getLengthOfCreditHistoryYear()));
							highmarkSROPBaseDomain.setLengthOfCreditHistoryMonth(parseBigInteger(drivedAttributes.getLengthOfCreditHistoryMonth()));
							highmarkSROPBaseDomain.setAverageAccountAgeYear(parseBigInteger(drivedAttributes.getAverageAccountAgeYear()));
							highmarkSROPBaseDomain.setAverageAccountAgeMonth(parseBigInteger(drivedAttributes.getAverageAccountAgeMonth()));
							highmarkSROPBaseDomain.setNewAccountInLastSixMonth(parseBigInteger(drivedAttributes.getNewAccountInLastSixMonths()));
							highmarkSROPBaseDomain.setNewDelinqAccountInLastSixMonth(parseBigInteger(drivedAttributes.getNewDlinqAccountInLastSixMonths()));
						}
					}
					
					Comments comments = baseReport.getComments();
					if(null != comments){
						Comment comment = comments.getComment();
						if(null != comment){
							highmarkSROPBaseDomain.setBureauComment(comment.getBureauComment());
							highmarkSROPBaseDomain.setCommentText(comment.getCommentText());
							highmarkSROPBaseDomain.setCommentDate(parseDate(comment.getCommentDate()));
						}
					}
					
					Alerts alerts = baseReport.getAlerts();
					if(null != alerts){
						Alert alert = alerts.getAlert();
						if(null != alert){
							highmarkSROPBaseDomain.setAlertDescription(alert.getAlertDescription());
							highmarkSROPBaseDomain.setAlertType(alert.getAlertType());
						}
					}
					
					highmarkSROPBaseDomainsList.add(highmarkSROPBaseDomain);
					
					
					
					/*InquiryStatus inquiryStatus = baseReportFile.getInquiryStatus();
					if(null != inquiryStatus){
						Inquiry inquiry = inquiryStatus.getInquiry();
						if(null != inquiry){
							highmarkSROPBaseDomain.setRequestDateTime(inquiry.getRequestDateTime());
							highmarkSROPBaseDomain.setResponseDateTime(inquiry.getResponseDateTime());
							highmarkSROPBaseDomain.setResponseType(inquiry.getResponseType());
							highmarkSROPBaseDomain.setDescription(inquiry.getDescription());
						}
					  }*/
					}
					Integer counter = 0;
					PersonalInfoVariation personalInfoVariation = baseReport.getPersonalInfoVariation();
					if(null != personalInfoVariation){
						Integer varCounter = -1;
						AddressVariations addressVariations = personalInfoVariation.getAddressVariations();
						
						if(null != addressVariations){
							List<Variation> variations = addressVariations.getVariations();
							if(null != variations && variations.size() > 0){
								for (Integer i = 0; i < variations.size(); i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain = new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setAddressVar(variations.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setAddressVarDate(parseDate(variations.get(i).getReportedDate()));
								 }
							}
						}
						
						DateOfBirthVariations dateOfBirthVariations = personalInfoVariation.getDateOfBirthVariations();
						
						if(null != dateOfBirthVariations){
							varCounter = -1;
							List<Variation> variation = dateOfBirthVariations.getVariations();
							if(null != variation && variation.size() > 0){
								for (Integer i = 0; i < variation.size(); i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain = new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setDobVar(variation.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setDobVarDate(parseDate(variation.get(i).getReportedDate()));
								 }
							}
						}
						
						DrivingLicenseVariations drivingLicenseVariations = personalInfoVariation.getDrivingLicenseVariations();
						
						if(null != drivingLicenseVariations){
							varCounter = -1;
							List<Variation> variation = drivingLicenseVariations.getVariations();
							if(null != variation && variation.size() > 0){
								for (Integer i = 0; i < variation.size(); i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setDlVar(variation.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setDlVarDate(parseDate(variation.get(i).getReportedDate()));
								 }
							}
						}
						
						EmailVariation emailVariation = personalInfoVariation.getEmailVariation();
						
						if(null != emailVariation){
							varCounter = -1;
							List<Variation> variation = emailVariation.getVariations();
							if(null != variation && variation.size() > 0){
								for (Integer i = 0; i < variation.size(); i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setEmailVar(variation.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setEmailVarDate(parseDate(variation.get(i).getReportedDate()));
								 }
							}
						}
						
						NameVariations nameVariations = personalInfoVariation.getNameVariations();
						
						if(null != nameVariations){
							varCounter = -1;
							List<Variation> variations = nameVariations.getVariations();
							if(null != variations && variations.size() > 0){
								for (Integer i = 0; i < variations.size(); i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setNameVar(variations.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setNameVarDate(parseDate(variations.get(i).getReportedDate()));
								 }
							}
						}
						
						PanVariations panVariations = personalInfoVariation.getPanVariations();
						
						if(null != panVariations){
							varCounter = -1;
							List<Variation> variation = panVariations.getVariations();
							if(null != variation && variation.size() > 0 ){
								for (Integer i = 0; i < variation.size(); i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setPanVar(variation.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setPanVarDate(parseDate(variation.get(i).getReportedDate()));
								 }
							}
						}
						
						PassportVariation passportVariation = personalInfoVariation.getPassportVariation();
						
						if(null != passportVariation){
							varCounter = -1;
							List<Variation> variation = passportVariation.getVariations();
							if(null != variation && variation.size() > 0){
								for (Integer i = 0; i < variation.size() ; i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setPassportVar(variation.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setPassportVarDate(parseDate(variation.get(i).getReportedDate()));
								 }
							}
						}
						
						PhoneNumberVariations phoneNumberVariations = personalInfoVariation.getPhoneNumberVariations();
						
						if(null != phoneNumberVariations){
							varCounter = -1;
							List<Variation> variations = phoneNumberVariations.getVariations();
							if(null != variations && variations.size() > 0){
								for (Integer i = 0; i < variations.size() ; i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setPhoneVar(variations.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setPhoneVarDate(parseDate(variations.get(i).getReportedDate()));
								 }
							}
						}
						
						RationCardVariation rationCardVariation = personalInfoVariation.getRationCardVariation();
						
						if(null != rationCardVariation){
							varCounter = -1;
							List<Variation> variation = rationCardVariation.getVariations();
							if(null != variation && variation.size() > 0){
								for (Integer i = 0; i < variation.size() ; i++) {
									varCounter++;
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setRationVar(variation.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setRationVarDate(parseDate(variation.get(i).getReportedDate()));
								 }
							}
						}
						
						VoterIdVariations voterIdVariations = personalInfoVariation.getVoterIdVariations();
						
						if(null != voterIdVariations){
							varCounter = -1;
							List<Variation> variation = voterIdVariations.getVariations();
							if(null != variation && variation.size() > 0){
								for (Integer i = 0; i < variation.size() ; i++) {
									
									varCounter++;
									
									if(varCounter > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									 }
									highmarkSROPBaseDomainsList.get(varCounter).setVoterVar(variation.get(i).getValue());
									highmarkSROPBaseDomainsList.get(varCounter).setVoterVarDate(parseDate(variation.get(i).getReportedDate()));
								 }
							}
						}
					}
					
					Responses responses = baseReport.getResponses();
					Integer accountKey = 0;
					if(null != responses){
						List<Response> response = responses.getResponse();
						if(null != response && response.size() > 0 ){
							
							Integer actPos = -1;
							for (Integer i = 0; i < response.size() ; i ++) {
								accountKey ++;
								actPos ++;
								LoanDetails loanDetail = response.get(i).getLoanDetail();
								
								if (null != loanDetail) {
									
									if(actPos > counter){
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									}
									
									highmarkSROPBaseDomainsList.get(actPos).setMatchedType(loanDetail.getMatchedType());
									highmarkSROPBaseDomainsList.get(actPos).setAccountNumber("ACCT"+accountKey+"-"+loanDetail.getAccountNumber());
									highmarkSROPBaseDomainsList.get(actPos).setAcctType(loanDetail.getAccountType());
									highmarkSROPBaseDomainsList.get(actPos).setCreditGuarantor(loanDetail.getCreditGuarantor());
									highmarkSROPBaseDomainsList.get(actPos).setAcctType(loanDetail.getAccountType());
									highmarkSROPBaseDomainsList.get(actPos).setDateReported(parseDate(loanDetail.getDateReported()));
									highmarkSROPBaseDomainsList.get(actPos).setOwnershipIndicator(loanDetail.getOwnershipIndicator());
									highmarkSROPBaseDomainsList.get(actPos).setAccountStatus(loanDetail.getAccountStatus());
									highmarkSROPBaseDomainsList.get(actPos).setDisbursedAmount(loanDetail.getDisbursedAmount());
									highmarkSROPBaseDomainsList.get(actPos).setDisbursedDate(parseDate(loanDetail.getDisbursedDate()));
									highmarkSROPBaseDomainsList.get(actPos).setLastPaymentDate(parseDate(loanDetail.getLastPaymentDate()));
									highmarkSROPBaseDomainsList.get(actPos).setClosedDate(parseDate(loanDetail.getClosedDate()));
									highmarkSROPBaseDomainsList.get(actPos).setInstallmentAmount(loanDetail.getInstallmentAmount());
									highmarkSROPBaseDomainsList.get(actPos).setOverdueAmount(loanDetail.getOverdueAmount());
									highmarkSROPBaseDomainsList.get(actPos).setWriteOffAmount(loanDetail.getWriteOffAmount());
									highmarkSROPBaseDomainsList.get(actPos).setCurrentBalance(loanDetail.getCurrentBalance());
									highmarkSROPBaseDomainsList.get(actPos).setCreditLimit(loanDetail.getCreditLimit());
									highmarkSROPBaseDomainsList.get(actPos).setAccountRemarks(loanDetail.getAccountRemarks());
									highmarkSROPBaseDomainsList.get(actPos).setFrequency(loanDetail.getFrequency());
									highmarkSROPBaseDomainsList.get(actPos).setSecurityStatus(loanDetail.getSecurityStatus());
									highmarkSROPBaseDomainsList.get(actPos).setOriginalTerm(parseInt(loanDetail.getOriginalTerm()));
									highmarkSROPBaseDomainsList.get(actPos).setTermToMaturity(parseInt(loanDetail.getTermToMaturity()));
									highmarkSROPBaseDomainsList.get(actPos).setAccountInDispute(loanDetail.getAccountInDispute());
									highmarkSROPBaseDomainsList.get(actPos).setSettlementAmount(loanDetail.getSettlementAmt());
									highmarkSROPBaseDomainsList.get(actPos).setPrincipalWriteOffAmount(loanDetail.getPrincipalWriteOffAmt());
									highmarkSROPBaseDomainsList.get(actPos).setCombinedPaymentHistory(loanDetail.getCombinedPaymentHistory());
									highmarkSROPBaseDomainsList.get(actPos).setCashLimit(loanDetail.getCashLimit());
									highmarkSROPBaseDomainsList.get(actPos).setActualPayment(loanDetail.getActualPayment());
									highmarkSROPBaseDomainsList.get(actPos).setSuitFiledWilfulDefault(loanDetail.getSuitFiledWilfulDefault());
									if (null != loanDetail.getSecurityDetailList() && loanDetail.getSecurityDetailList().size() > 0) {
										SecurityDetails securityDetails = loanDetail.getSecurityDetailList().get(0);
										
										if (null != securityDetails) { 
											highmarkSROPBaseDomainsList.get(actPos).setOwnerName(securityDetails.getOwnerName());
											highmarkSROPBaseDomainsList.get(actPos).setSecurityValue(securityDetails.getSecurityValue());
											highmarkSROPBaseDomainsList.get(actPos).setDateOfValue(parseDate(securityDetails.getDateOfValue()));
											highmarkSROPBaseDomainsList.get(actPos).setSecurityType(securityDetails.getSecurityType());
											highmarkSROPBaseDomainsList.get(actPos).setSecurityCharge(securityDetails.getSecurityCharge());
											highmarkSROPBaseDomainsList.get(actPos).setPropertyAddress(securityDetails.getPropertyAddress());
											highmarkSROPBaseDomainsList.get(actPos).setAutomobileType(securityDetails.getAutomobileType());
											highmarkSROPBaseDomainsList.get(actPos).setYearOfManufacture(parseInt(securityDetails.getYearOfManufacture()));
											highmarkSROPBaseDomainsList.get(actPos).setRegistrationNumber(securityDetails.getRegistrationNumber());
											highmarkSROPBaseDomainsList.get(actPos).setEngineNumber(securityDetails.getEngineNumber());
											highmarkSROPBaseDomainsList.get(actPos).setChassisNumber(securityDetails.getChassisNumber());
										}
									}
									int linkedAaccountKey = 0;
									int linkedAcctPos = actPos-1;
									if (null != loanDetail.getAccountVariations_() && loanDetail.getAccountVariations_().size() > 0) {
										List<LoanDetails> variationsList = loanDetail.getAccountVariations_() ;
										for (LoanDetails linkedAccounts : variationsList) {
											
											linkedAaccountKey++;
											linkedAcctPos ++;
											if(null != linkedAccounts){

												if(linkedAcctPos > counter){
													HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
													highmarkSROPBaseDomainsList.add(sropBaseDomain);
													counter++;
												}
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountNumber("ACCT"+accountKey+"-"+loanDetail.getAccountNumber());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountNumberLn("ACCT"+linkedAaccountKey+"-"+linkedAccounts.getAccountNumber());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setCreditGuarantorLn(linkedAccounts.getCreditGuarantor());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountTypeLn(linkedAccounts.getAccountType());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setDateReportedLn(parseDate(linkedAccounts.getDateReported()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setOwnershipIndicatorLn(linkedAccounts.getOwnershipIndicator());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountStatusLn(linkedAccounts.getAccountStatus());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setDisbursedAmountLn(linkedAccounts.getDisbursedAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setDisbursedDateLn(parseDate(linkedAccounts.getDisbursedDate()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setLastPaymentDateLn(parseDate(linkedAccounts.getLastPaymentDate()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setClosedDateLn(parseDate(linkedAccounts.getClosedDate()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setInstallmentAmountLn(linkedAccounts.getInstallmentAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setOverdueAmountLn(linkedAccounts.getOverdueAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setWriteOffAmountLn(linkedAccounts.getWriteOffAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setCurrentBalanceLn(linkedAccounts.getCurrentBalance());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setCreditLimitLn(linkedAccounts.getCreditLimit());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountRemarksLn(linkedAccounts.getAccountRemarks());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setFrequencyLn(linkedAccounts.getFrequency());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityStatusLn(linkedAccounts.getSecurityStatus());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setOriginalTermLn(parseInt(linkedAccounts.getOriginalTerm()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setTermToMaturityLn(parseInt(linkedAccounts.getTermToMaturity()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountInDisputeLn(linkedAccounts.getAccountInDispute());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setSettlementAmtLn(linkedAccounts.getSettlementAmt());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setPrincipalWriteOffAmtLn(linkedAccounts.getPrincipalWriteOffAmt());
												
												if (null != linkedAccounts.getSecurityDetailList() && linkedAccounts.getSecurityDetailList().size() > 0) {
													SecurityDetails linkedAccountSecurityDetails = linkedAccounts.getSecurityDetailList().get(0);
													if(null != linkedAccountSecurityDetails){
														
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setOwnerNameLn(linkedAccountSecurityDetails.getOwnerName());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityValueLn(linkedAccountSecurityDetails.getSecurityValue());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setDateOfValueLn(parseDate(linkedAccountSecurityDetails.getDateOfValue()));
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityTypeLn(linkedAccountSecurityDetails.getSecurityType());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityChargeLn(linkedAccountSecurityDetails.getSecurityCharge());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setPropertyAddressLn(linkedAccountSecurityDetails.getPropertyAddress());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setAutomobileTypeLn(linkedAccountSecurityDetails.getAutomobileType());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setYearOfManufactureLn(parseInt(linkedAccountSecurityDetails.getYearOfManufacture()));
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setRegistrationNumberLn(linkedAccountSecurityDetails.getRegistrationNumber());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setEngineNumberLn(linkedAccountSecurityDetails.getEngineNumber());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setChassisNumberLn(linkedAccountSecurityDetails.getChassisNumber());
													}
												}
												
												
											
											}
										}
										
										
									}
									
								}
								
							}
						}
					}
					SecondaryMatches secondaryMatches = baseReport.getSecondaryMatches();
					
					if(null != secondaryMatches){
						List<SecondaryMatch> secondaryMatchList = secondaryMatches.getSecondaryMatchList();
						if(null != secondaryMatchList && secondaryMatchList.size() > 0){
							for (Integer i = 0; i < secondaryMatchList.size(); i++) {
								if (i > counter) {
									HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
									highmarkSROPBaseDomainsList.add(sropBaseDomain);
									counter++;
								}
								highmarkSROPBaseDomainsList.get(i).setNameIndtl(secondaryMatchList.get(i).getName());
								highmarkSROPBaseDomainsList.get(i).setAddress(secondaryMatchList.get(i).getAddress());
								highmarkSROPBaseDomainsList.get(i).setDobIndtl(secondaryMatchList.get(i).getDob());
								highmarkSROPBaseDomainsList.get(i).setPhoneIndtl(secondaryMatchList.get(i).getPhone());
								highmarkSROPBaseDomainsList.get(i).setPanIndtl(secondaryMatchList.get(i).getPan());
								highmarkSROPBaseDomainsList.get(i).setPassportIndtl(secondaryMatchList.get(i).getPassport());
								highmarkSROPBaseDomainsList.get(i).setDrivingLicenseIndtl(secondaryMatchList.get(i).getDrivingLicense());
								highmarkSROPBaseDomainsList.get(i).setVoterIdIndtl(secondaryMatchList.get(i).getVoterId());
								highmarkSROPBaseDomainsList.get(i).setEmailIndtl(secondaryMatchList.get(i).getEmail());
								highmarkSROPBaseDomainsList.get(i).setRationCardIndtl(secondaryMatchList.get(i).getRationCard());
								
								LoanDetails loanDetail = secondaryMatchList.get(i).getLoanDetails();
								if (loanDetail != null) {

									
									
									highmarkSROPBaseDomainsList.get(i).setMatchedTypeSe(loanDetail.getMatchedType());
									highmarkSROPBaseDomainsList.get(i).setAccountNumberSe("ACCT"+accountKey+"-"+loanDetail.getAccountNumber());
									highmarkSROPBaseDomainsList.get(i).setAcctTypeSe(loanDetail.getAccountType());
									highmarkSROPBaseDomainsList.get(i).setCreditGuarantorSe(loanDetail.getCreditGuarantor());
									highmarkSROPBaseDomainsList.get(i).setAcctTypeSe(loanDetail.getAccountType());
									highmarkSROPBaseDomainsList.get(i).setDateReportedSe(parseDate(loanDetail.getDateReported()));
									highmarkSROPBaseDomainsList.get(i).setOwnershipIndicatorSe(loanDetail.getOwnershipIndicator());
									highmarkSROPBaseDomainsList.get(i).setAccountStatusSe(loanDetail.getAccountStatus());
									highmarkSROPBaseDomainsList.get(i).setDisbursedAmountSe(loanDetail.getDisbursedAmount());
									highmarkSROPBaseDomainsList.get(i).setDisbursedDateSe(parseDate(loanDetail.getDisbursedDate()));
									highmarkSROPBaseDomainsList.get(i).setLastPaymentDateSe(parseDate(loanDetail.getLastPaymentDate()));
									highmarkSROPBaseDomainsList.get(i).setClosedDateSe(parseDate(loanDetail.getClosedDate()));
									highmarkSROPBaseDomainsList.get(i).setInstallmentAmountSe(loanDetail.getInstallmentAmount());
									highmarkSROPBaseDomainsList.get(i).setOverdueAmountSe(loanDetail.getOverdueAmount());
									highmarkSROPBaseDomainsList.get(i).setWriteOffAmountSe(loanDetail.getWriteOffAmount());
									highmarkSROPBaseDomainsList.get(i).setCurrentBalanceSe(loanDetail.getCurrentBalance());
									highmarkSROPBaseDomainsList.get(i).setCreditLimitSe(loanDetail.getCreditLimit());
									highmarkSROPBaseDomainsList.get(i).setAccountRemarksSe(loanDetail.getAccountRemarks());
									highmarkSROPBaseDomainsList.get(i).setFrequencySe(loanDetail.getFrequency());
									highmarkSROPBaseDomainsList.get(i).setSecurityStatusSe(loanDetail.getSecurityStatus());
									highmarkSROPBaseDomainsList.get(i).setOriginalTermSe(parseInt(loanDetail.getOriginalTerm()));
									highmarkSROPBaseDomainsList.get(i).setTermToMaturitySe(parseInt(loanDetail.getTermToMaturity()));
									highmarkSROPBaseDomainsList.get(i).setAccountInDisputeSe(loanDetail.getAccountInDispute());
									highmarkSROPBaseDomainsList.get(i).setSettlementAmountSe(loanDetail.getSettlementAmt());
									highmarkSROPBaseDomainsList.get(i).setPrincipalWriteOffAmountSe(loanDetail.getPrincipalWriteOffAmt());
									highmarkSROPBaseDomainsList.get(i).setCombinedPaymentHistorySe(loanDetail.getCombinedPaymentHistory());
									
									if (null != loanDetail.getSecurityDetailList() && loanDetail.getSecurityDetailList().size() > 0) {
										SecurityDetails securityDetails = loanDetail.getSecurityDetailList().get(0);
										
										if (null != securityDetails) { 
											highmarkSROPBaseDomainsList.get(i).setOwnerNameSe(securityDetails.getOwnerName());
											highmarkSROPBaseDomainsList.get(i).setSecurityValueSe(securityDetails.getSecurityValue());
											highmarkSROPBaseDomainsList.get(i).setDateOfValueSe(parseDate(securityDetails.getDateOfValue()));
											highmarkSROPBaseDomainsList.get(i).setSecurityTypeSe(securityDetails.getSecurityType());
											highmarkSROPBaseDomainsList.get(i).setSecurityChargeSe(securityDetails.getSecurityCharge());
											highmarkSROPBaseDomainsList.get(i).setPropertyAddressSe(securityDetails.getPropertyAddress());
											highmarkSROPBaseDomainsList.get(i).setAutomobileTypeSe(securityDetails.getAutomobileType());
											highmarkSROPBaseDomainsList.get(i).setYearOfManufactureSe(parseInt(securityDetails.getYearOfManufacture()));
											highmarkSROPBaseDomainsList.get(i).setRegistrationNumberSe(securityDetails.getRegistrationNumber());
											highmarkSROPBaseDomainsList.get(i).setEngineNumberSe(securityDetails.getEngineNumber());
											highmarkSROPBaseDomainsList.get(i).setChassisNumberSe(securityDetails.getChassisNumber());
										}
									}
									int linkedAaccountKey = 0;
									int linkedAcctPos = i-1;
									if (null != loanDetail.getAccountVariations_() && loanDetail.getAccountVariations_().size() > 0) {
										List<LoanDetails> variationsList = loanDetail.getAccountVariations_() ;
										for (LoanDetails linkedAccounts : variationsList) {
											
											linkedAaccountKey++;
											linkedAcctPos ++;
											if(null != linkedAccounts){

												if(linkedAcctPos > counter){
													HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
													highmarkSROPBaseDomainsList.add(sropBaseDomain);
													counter++;
												}
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountNumberSe("ACCT"+accountKey+"-"+loanDetail.getAccountNumber());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountNumberSeLn("ACCT"+linkedAaccountKey+"-"+linkedAccounts.getAccountNumber());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setCreditGuarantorSeLn(linkedAccounts.getCreditGuarantor());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountTypeSeLn(linkedAccounts.getAccountType());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setDateReportedSeLn(parseDate(linkedAccounts.getDateReported()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setOwnershipIndicatorSeLn(linkedAccounts.getOwnershipIndicator());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountStatusSeLn(linkedAccounts.getAccountStatus());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setDisbursedAmountSeLn(linkedAccounts.getDisbursedAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setDisbursedDateSeLn(parseDate(linkedAccounts.getDisbursedDate()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setLastPaymentDateSeLn(parseDate(linkedAccounts.getLastPaymentDate()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setClosedDateSeLn(parseDate(linkedAccounts.getClosedDate()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setInstallmentAmountSeLn(linkedAccounts.getInstallmentAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setOverdueAmountSeLn(linkedAccounts.getOverdueAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setWriteOffAmountSeLn(linkedAccounts.getWriteOffAmount());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setCurrentBalanceSeLn(linkedAccounts.getCurrentBalance());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setCreditLimitSeLn(linkedAccounts.getCreditLimit());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountRemarksSeLn(linkedAccounts.getAccountRemarks());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setFrequencySeLn(linkedAccounts.getFrequency());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityStatusSeLn(linkedAccounts.getSecurityStatus());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setOriginalTermSeLn(parseInt(linkedAccounts.getOriginalTerm()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setTermToMaturitySeLn(parseInt(linkedAccounts.getTermToMaturity()));
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setAccountInDisputeSeLn(linkedAccounts.getAccountInDispute());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setSettlementAmtSeLn(linkedAccounts.getSettlementAmt());
												highmarkSROPBaseDomainsList.get(linkedAcctPos).setPrincipalWriteOffAmtSeLn(linkedAccounts.getPrincipalWriteOffAmt());
												
												if (null != linkedAccounts.getSecurityDetailList() && linkedAccounts.getSecurityDetailList().size() > 0) {
													SecurityDetails linkedAccountSecurityDetails = linkedAccounts.getSecurityDetailList().get(0);
													if(null != linkedAccountSecurityDetails){
														
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setOwnerNameSeLn(linkedAccountSecurityDetails.getOwnerName());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityValueSeLn(linkedAccountSecurityDetails.getSecurityValue());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setDateOfValueSeLn(parseDate(linkedAccountSecurityDetails.getDateOfValue()));
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityTypeSeLn(linkedAccountSecurityDetails.getSecurityType());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setSecurityChargeSeLn(linkedAccountSecurityDetails.getSecurityCharge());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setPropertyAddressSeLn(linkedAccountSecurityDetails.getPropertyAddress());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setAutomobileTypeSeLn(linkedAccountSecurityDetails.getAutomobileType());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setYearOfManufactureSeLn(parseInt(linkedAccountSecurityDetails.getYearOfManufacture()));
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setRegistrationNumberSeLn(linkedAccountSecurityDetails.getRegistrationNumber());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setEngineNumberSeLn(linkedAccountSecurityDetails.getEngineNumber());
														highmarkSROPBaseDomainsList.get(linkedAcctPos).setChassisNumberSeLn(linkedAccountSecurityDetails.getChassisNumber());
													}
												}
												
												
											
											}
										}
										
										
									}
									
								
								}
							}
						}
						
					}
					
					// next element from here
					InquiryHistory inquiryHistory = baseReport.getInquiryHistory();
					
					if(null != inquiryHistory){
						List<History> history = inquiryHistory.getHistory();
						if(null != history && history.size() > 0){
							for (Integer i = 0; i < history.size(); i++) {
								if (i > counter) {
									HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
									highmarkSROPBaseDomainsList.add(sropBaseDomain);
									counter++;
								}
								highmarkSROPBaseDomainsList.get(i).setMemberName(history.get(i).getMemberName());
								highmarkSROPBaseDomainsList.get(i).setInquiryDate(parseDate(history.get(i).getInquiryDate()));
								highmarkSROPBaseDomainsList.get(i).setPurpose(history.get(i).getPurpose());
								highmarkSROPBaseDomainsList.get(i).setOwnershipType(history.get(i).getOwnershipType());
								highmarkSROPBaseDomainsList.get(i).setAmount(history.get(i).getAmount());
								highmarkSROPBaseDomainsList.get(i).setRemark(history.get(i).getRemark());
							}
						}
					}
					
					Scores scores = baseReport.getScores();
					if(scores!=null){
							List<Score> scoreList = scores.getScoreList();
							if(scoreList!=null && scoreList.size()>0){
								for (Integer i = 0; i < scoreList.size(); i++) {
									if (i > counter) {
										HibHighmarkBaseSropDomain sropBaseDomain= new HibHighmarkBaseSropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName);
										highmarkSROPBaseDomainsList.add(sropBaseDomain);
										counter++;
									}
									highmarkSROPBaseDomainsList.get(i).setScoreType(scoreList.get(i).getScoreType());
									highmarkSROPBaseDomainsList.get(i).setScoreValue(parseBigInteger(scoreList.get(i).getScoreValue()));
									highmarkSROPBaseDomainsList.get(i).setScoreComments(scoreList.get(i).getScoreComments());
									
								}
							}
							
					}
					
				}
			}
	return highmarkSROPBaseDomainsList;
   }
	
	public List<HibHighmarkBaseEropDomain> highmarkBaseEropDomainGenerator(AckReportFile baseAckReportFile, String  srNo, String soaSourceName, String memberReferenceNumber, String soaFileName){
		
		List<HibHighmarkBaseEropDomain> highmarkEROPBaseDomainsList= new ArrayList<HibHighmarkBaseEropDomain>();
		
		Integer srNumber = null;
		try{
			srNumber = Integer.parseInt(srNo);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		if (baseAckReportFile != null && baseAckReportFile.getInquiryStatus() != null) {
			
			Inquiry inquiryObj = baseAckReportFile.getInquiryStatus().getInquiry();
			if (inquiryObj != null && inquiryObj.getErrors() != null) {
				
				 List<ErrorDomain> errorDomainList = inquiryObj.getErrors().getErrorList();
				 if (errorDomainList != null && errorDomainList.size() > 0) {
					 
					 HibHighmarkBaseEropDomain hmBaseErrorDomain = new HibHighmarkBaseEropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName) ;
					 highmarkEROPBaseDomainsList.add(hmBaseErrorDomain);
					 
					 /*hmBaseErrorDomain.setAccountOpenDate(requestDomain.getActOpeningDt());
					 hmBaseErrorDomain.setAddress1(requestDomain.getAddress1());*/
					 int counter = 0;
					 for (ErrorDomain errorDomainObj : errorDomainList) {
						 
						 if(counter == 0){
							 highmarkEROPBaseDomainsList.get(0).setError(errorDomainObj.getErrorDescription());
							} else {
								HibHighmarkBaseEropDomain localHmBaseErrorDomain = new HibHighmarkBaseEropDomain(srNumber, soaSourceName, memberReferenceNumber, soaFileName) ;
								localHmBaseErrorDomain.setError(errorDomainObj.getErrorDescription());
								highmarkEROPBaseDomainsList.add(localHmBaseErrorDomain);
							}
							counter++;
						 
					 }
				 }
			}
			
		}
		
		return highmarkEROPBaseDomainsList;
   }
	
	private static Date parseDate(String dateString){
		Date value = null;
		try{
			if(StringUtils.isNotBlank(dateString)){
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
				value = sdf.parse(dateString);
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Date Not parse with format YYYYMMdd "+dateString);
		}
		return value;
	}
	
	private static int parseInt(String numberString){
		try{
			if(StringUtils.isNotBlank(numberString)){
				return Integer.parseInt(numberString);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
}