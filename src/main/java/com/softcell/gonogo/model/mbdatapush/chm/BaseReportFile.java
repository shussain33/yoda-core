package com.softcell.gonogo.model.mbdatapush.chm;

public class BaseReportFile {

	private BaseReports baseReports =null;
	private BaseFileHeader baseFileHeader=null;
	private InquiryStatus inquiryStatus= null;
	
	public BaseReports getBaseReports() {
		return baseReports;
	}
	public void setBaseReports(BaseReports baseReports) {
		this.baseReports = baseReports;
	}
	public BaseFileHeader getBaseFileHeader() {
		return baseFileHeader;
	}
	public void setBaseFileHeader(BaseFileHeader baseFileHeader) {
		this.baseFileHeader = baseFileHeader;
	}
	public InquiryStatus getInquiryStatus() {
		return inquiryStatus;
	}
	public void setInquiryStatus(InquiryStatus inquiryStatus) {
		this.inquiryStatus = inquiryStatus;
	}

	@Override
	public String toString() {
		return "BaseReportFile [baseReports=" + baseReports
				+ ", baseFileHeader=" + baseFileHeader + ", inquiryStatus="
				+ inquiryStatus + "]";
	}
}
