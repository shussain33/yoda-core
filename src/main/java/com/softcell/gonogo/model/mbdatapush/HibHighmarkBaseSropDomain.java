/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author Dipak
 * 
 */
public class HibHighmarkBaseSropDomain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	@Expose
	@SerializedName("SRNO")
	private Integer srNo;
	@Expose
	@SerializedName("SOA_SOURCE_NAME")
	private String soaSourceName;
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private String memberReferenceNumber;

	@Expose
	@SerializedName("DATE_OF_REQUEST")
	private Date dateOfRequest;
	@Expose
	@SerializedName("PREPARED_FOR")
	private String preparedFor;
	@Expose
	@SerializedName("PREPARED_FOR_ID")
	private String preparedForId;
	@Expose
	@SerializedName("DATE_OF_ISSUE")
	private Date dateOfIssue;
	@Expose
	@SerializedName("REPORT_ID")
	private String reportId;
	@Expose
	@SerializedName("BATCH_ID")
	private BigInteger batchId;
	@Expose
	@SerializedName("STATUS_IQ")
	private String status;
	@Expose
	@SerializedName("NAME_IQ")
	private String nameIQ;
	@Expose
	@SerializedName("AKA_IQ")
	private String aka;
	@Expose
	@SerializedName("SPOUSE_IQ")
	private String spouse;
	@Expose
	@SerializedName("FATHER_IQ")
	private String father;
	@Expose
	@SerializedName("MOTHER_IQ")
	private String mother;
	@Expose
	@SerializedName("DOB_IQ")
	private Date dobIQ;
	@Expose
	@SerializedName("AGE_IQ")
	private Integer age;
	@Expose
	@SerializedName("AGE_AS_ON_IQ")
	private Date ageAsOn;
	@Expose
	@SerializedName("RATION_CARD_IQ")
	private String rationCard;
	@Expose
	@SerializedName("PASSPORT_IQ")
	private String passport;
	@Expose
	@SerializedName("VOTERS_ID_IQ")
	private String votersId;
	@Expose
	@SerializedName("DRIVING_LICENSE_NO_IQ")
	private String drivingLicenceNo;
	@Expose
	@SerializedName("PAN_IQ")
	private String pan;
	@Expose
	@SerializedName("GENDER_IQ")
	private String gender;
	@Expose
	@SerializedName("OWNERSHIP_IQ")
	private String ownership;
	@Expose
	@SerializedName("ADDRESS_1_IQ")
	private String address1;
	@Expose
	@SerializedName("ADDRESS_2_IQ")
	private String address2;
	@Expose
	@SerializedName("ADDRESS_3_IQ")
	private String address3;
	@Expose
	@SerializedName("PHONE_1_IQ")
	private BigInteger phone1;
	@Expose
	@SerializedName("PHONE_2_IQ")
	private BigInteger phone2;
	@Expose
	@SerializedName("PHONE_3_IQ")
	private BigInteger phone3;
	@Expose
	@SerializedName("EMAIL_1_IQ")
	private String emailId1;
	@Expose
	@SerializedName("EMAIL_2_IQ")
	private String emailId2;
	@Expose
	@SerializedName("BRANCH_IQ")
	private String branch;
	@Expose
	@SerializedName("KENDRA_IQ")
	private String kendra;
	@Expose
	@SerializedName("MBR_ID_IQ")
	private String mbrId;
	@Expose
	@SerializedName("LOS_APP_ID_IQ")
	private String losAppId;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP_IQ")
	private String creditInquiryPurposeType;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP_DESC_IQ")
	private String creditInquiryPurposeTypeDesc;
	@Expose
	@SerializedName("CREDIT_INQUIRY_STAGE_IQ")
	private String creditInquiryStage;
	@Expose
	@SerializedName("CREDT_RPT_ID_IQ")
	private String creditReportId;
	@Expose
	@SerializedName("CREDT_REQ_TYP_IQ")
	private String creditRequestType;
	@Expose
	@SerializedName("CREDT_RPT_TRN_DT_TM_IQ")
	private String creditReportTransectionDateTime;
	@Expose
	@SerializedName("AC_OPEN_DT_IQ")
	private String accountOpenDate;
	@Expose
	@SerializedName("LOAN_AMOUNT_IQ")
	private BigInteger loanAmount;
	@Expose
	@SerializedName("ENTITY_ID_IQ")
	private String entityId;
	
	@Expose
	@SerializedName("PR_NO_OF_ACCOUNTS")
	private Integer primaryNumberOfAccount;
	@Expose
	@SerializedName("PR_ACTIVE_NO_OF_ACCOUNTS")
	private Integer primaryActiveNoOfAccount;
	@Expose
	@SerializedName("PR_OVERDUE_NO_OF_ACCOUNTS")
	private Integer primaryOverdueNumberOfAccount;
	@Expose
	@SerializedName("PR_CURRENT_BALANCE")
	private String primaryCurrentBalance;
	@Expose
	@SerializedName("PR_SANCTIONED_AMOUNT")
	private String primarySanctionedAmount;
	@Expose
	@SerializedName("PR_DISBURSED_AMOUNT")
	private String primaryDisbursedAmount;
	@Expose
	@SerializedName("PR_SECURED_NO_OF_ACCOUNTS")
	private Integer primarySecuredNumberOfAccount;
	@Expose
	@SerializedName("PR_UNSECURED_NO_OF_ACCOUNTS")
	private Integer primaryUnsecuredNumberOfAccount;
	@Expose
	@SerializedName("PR_UNTAGGED_NO_OF_ACCOUNTS")
	private Integer primaryUntaggedNumberOfAccount;
	
	@Expose
	@SerializedName("SE_NUMBER_OF_ACCOUNTS")
	private Integer secondaryVNumberOfAccount;
	@Expose
	@SerializedName("SE_ACTIVE_NO_OF_ACCOUNTS")
	private Integer secondaryActiveNumberOfAccount;
	@Expose
	@SerializedName("SE_OVERDUE_NO_OF_ACCOUNTS")
	private Integer secondaryOverdueNumberOfAccount;
	@Expose
	@SerializedName("SE_CURRENT_BALANCE")
	private String secondaryCurrentBalance;
	@Expose
	@SerializedName("SE_SANCTIONED_AMOUNT")
	private String secondarySanctionedAmount;
	@Expose
	@SerializedName("SE_DISBURSED_AMOUNT")
	private String secondaryDisbursedAmount;
	@Expose
	@SerializedName("SE_SECURED_NO_OF_ACCOUNTS")
	private Integer secondarySecuredNumberOfAccount;
	@Expose
	@SerializedName("SE_UNSECURED_NO_OF_ACCOUNTS")
	private Integer secondaryUnsecuredNumberOfAccount;
	@Expose
	@SerializedName("SE_UNTAGGED_NO_OF_ACCOUNTS")
	private Integer secondaryUntaggedNumberOfAccount;
	
	@Expose
	@SerializedName("INQURIES_IN_LAST_SIX_MONTHS")
	private BigInteger inquriesInLastSixMonth;
	@Expose
	@SerializedName("LENGTH_OF_CREDIT_HIS_YEAR")
	private BigInteger lengthOfCreditHistoryYear;
	@Expose
	@SerializedName("LENGTH_OF_CREDIT_HIS_MON")
	private BigInteger lengthOfCreditHistoryMonth;
	@Expose
	@SerializedName("AVG_ACC_AGE_YEAR")
	private BigInteger averageAccountAgeYear;
	@Expose
	@SerializedName("AVG_ACC_AGE_MON")
	private BigInteger averageAccountAgeMonth;
	@Expose
	@SerializedName("NEW_ACC_IN_LAST_SIX_MON")
	private BigInteger newAccountInLastSixMonth;
	@Expose
	@SerializedName("NEW_DELINQ_ACC_LAST_SIX_MON")
	private BigInteger newDelinqAccountInLastSixMonth;

	@Expose
	@SerializedName("NAME")
	private String nameVar;
	@Expose
	@SerializedName("NAME_REPORTED_DATE")
	private Date nameVarDate;
	@Expose
	@SerializedName("ADDRESS")
	private String addressVar;
	@Expose
	@SerializedName("ADDRESS_REPORTED_DATE")
	private Date addressVarDate;
	@Expose
	@SerializedName("PAN_NO")
	private String panVar;
	@Expose
	@SerializedName("PAN_NO_REPORTED_DATE")
	private Date panVarDate;
	@Expose
	@SerializedName("DRIVING_LICENSE")
	private String dlVar;
	@Expose
	@SerializedName("DRIVING_LICENSE_REPORTED_DATE")
	private Date dlVarDate;
	@Expose
	@SerializedName("DOB")
	private String dobVar;
	@Expose
	@SerializedName("DOB_REPORTED_DATE")
	private Date dobVarDate;
	@Expose
	@SerializedName("VOTER_ID")
	private String voterVar;
	@Expose
	@SerializedName("VOTER_ID_REPORTED_DATE")
	private Date voterVarDate;
	@Expose
	@SerializedName("PASSPORT_NUMBER")
	private String passportVar;
	@Expose
	@SerializedName("PASSPORT_REPORTED_DATE")
	private Date passportVarDate;
	@Expose
	@SerializedName("PHONE_NUMBER")
	private String phoneVar;
	@Expose
	@SerializedName("PHONE_REPORTED_DATE")
	private Date phoneVarDate;
	@Expose
	@SerializedName("RATION_CARD")
	private String rationVar;
	@Expose
	@SerializedName("RATION_REPORTED_DATE")
	private Date rationVarDate;
	@Expose
	@SerializedName("EMAIL_ID")
	private String emailVar;
	@Expose
	@SerializedName("EMAIL_ID_REPORTED_DATE")
	private Date emailVarDate;
	
	@Expose
	@SerializedName("MATCHED_TYPE")
	private String matchedType;
	@Expose
	@SerializedName("ACCT_NUMBER")
	private String accountNumber;
	@Expose
	@SerializedName("CREDIT_GUARANTOR")
	private String creditGuarantor;
	@Expose
	@SerializedName("ACCT_TYPE")
	private String acctType;
	@Expose
	@SerializedName("DATE_REPORTED")
	private Date dateReported;
	@Expose
	@SerializedName("OWNERSHIP_IND")
	private String ownershipIndicator;
	@Expose
	@SerializedName("ACCOUNT_STATUS")
	private String accountStatus;
	@Expose
	@SerializedName("DISBURSED_AMT")
	private String disbursedAmount;
	@Expose
	@SerializedName("DISBURSED_DT")
	private Date disbursedDate;
	@Expose
	@SerializedName("LAST_PAYMENT_DATE")
	private Date lastPaymentDate;
	@Expose
	@SerializedName("CLOSE_DT")
	private Date closedDate;
	@Expose
	@SerializedName("INSTALLMENT_AMT")
	private String installmentAmount;
	@Expose
	@SerializedName("OVERDUE_AMT")
	private String overdueAmount;
	@Expose
	@SerializedName("WRITE_OFF_AMT")
	private String writeOffAmount;
	@Expose
	@SerializedName("CURRENT_BAL")
	private String currentBalance;
	@Expose
	@SerializedName("CREDIT_LIMIT")
	private String creditLimit;
	@Expose
	@SerializedName("ACCOUNT_REMARKS")
	private String accountRemarks;
	@Expose
	@SerializedName("FREQUENCY")
	private String frequency;
	@Expose
	@SerializedName("SECURITY_STATUS")
	private String securityStatus;
	@Expose
	@SerializedName("ORIGINAL_TERM")
	private Integer originalTerm;
	@Expose
	@SerializedName("TERM_TO_MATURITY")
	private Integer termToMaturity;
	@Expose
	@SerializedName("ACCT_IN_DISPUTE")
	private String accountInDispute;
	@Expose
	@SerializedName("SETTLEMENT_AMT")
	private String settlementAmount;
	@Expose
	@SerializedName("PRINCIPAL_WRITE_OFF_AMT")
	private String principalWriteOffAmount;
	@Expose
	@SerializedName("COMBINED_PAYMENT_HISTORY")
	private String combinedPaymentHistory;
	@Expose
	@SerializedName("REPAYMENT_TENURE")
	private String repaymentTenure;
	@Expose
	@SerializedName("CASH_LIMIT")
	private String cashLimit;
	@Expose
	@SerializedName("ACTUAL_PAYMENT")
	private String actualPayment;
	@Expose
	@SerializedName("INTEREST_RATE")
	private Integer interestRate;
	@Expose
	@SerializedName("SUIT_FILED_WILFUL_DEFAULT")
	private String suitFiledWilfulDefault;
	@Expose
	@SerializedName("WRITTEN_OFF_SETTLED_STATUS")
	private String writtenOffSetteldStatus;
	@Expose
	@SerializedName("SECURITY_TYPE_LD")
	private String securityType;
	@Expose
	@SerializedName("OWNER_NAME_LD")
	private String ownerName;
	@Expose
	@SerializedName("SECURITY_VALUE_LD")
	private String securityValue;
	@Expose
	@SerializedName("DATE_OF_VALUE_LD")
	private Date dateOfValue;
	@Expose
	@SerializedName("SECURITY_CHARGE_LD")
	private String securityCharge;
	@Expose
	@SerializedName("PROPERTY_ADDRESS_LD")
	private String propertyAddress;
	@Expose
	@SerializedName("AUTOMOBILE_TYPE_LD")
	private String automobileType;
	@Expose
	@SerializedName("YEAR_OF_MANUFACTURE_LD")
	private Integer yearOfManufacture;
	@Expose
	@SerializedName("REGISTRATION_NUMBER_LD")
	private String registrationNumber;
	@Expose
	@SerializedName("ENGINE_NUMBER_LD")
	private String engineNumber;
	@Expose
	@SerializedName("CHASIS_NUMBER_LD")
	private String chassisNumber;
	
	@Expose
	@SerializedName("LINKED_ACCT_NUMBER")
	private String accountNumberLn;
	@Expose
	@SerializedName("LINKED_CREDIT_GUARANTOR")
	private String creditGuarantorLn;
	@Expose
	@SerializedName("LINKED_ACCT_TYPE")
	private String accountTypeLn;
	@Expose
	@SerializedName("LINKED_DATE_REPORTED")
	private Date dateReportedLn;
	@Expose
	@SerializedName("LINKED_OWNERSHIP_IND")
	private String ownershipIndicatorLn;
	@Expose
	@SerializedName("LINKED_ACCOUNT_STATUS")
	private String accountStatusLn;
	@Expose
	@SerializedName("LINKED_DISBURSED_AMT")
	private String disbursedAmountLn;
	@Expose
	@SerializedName("LINKED_DISBURSED_DT")
	private Date disbursedDateLn;
	@Expose
	@SerializedName("LINKED_LAST_PAYMENT_DATE")
	private Date lastPaymentDateLn;
	@Expose
	@SerializedName("LINKED_CLOSED_DATE")
	private Date closedDateLn;
	@Expose
	@SerializedName("LINKED_INSTALLMENT_AMT")
	private String installmentAmountLn;
	@Expose
	@SerializedName("LINKED_WRITE_OFF_AMT")
	private String writeOffAmountLn;
	@Expose
	@SerializedName("LINKED_OVERDUE_AMT")
	private String overdueAmountLn;
	@Expose
	@SerializedName("LINKED_CURRENT_BAL")
	private String currentBalanceLn;
	@Expose
	@SerializedName("LINKED_CREDIT_LIMIT")
	private String creditLimitLn;
	@Expose
	@SerializedName("LINKED_ACCOUNT_REMARKS")
	private String accountRemarksLn; 
	@Expose
	@SerializedName("LINKED_FREQUENCY")
	private String frequencyLn;
	@Expose
	@SerializedName("LINKED_SECURITY_STATUS")
	private String securityStatusLn;
	@Expose
	@SerializedName("LINKED_ORIGINAL_TERM")
	private Integer originalTermLn;
	@Expose
	@SerializedName("LINKED_TERM_TO_MATURITY")
	private Integer termToMaturityLn;
	@Expose
	@SerializedName("LINKED_ACCT_IN_DISPUTE")
	private String accountInDisputeLn;
	@Expose
	@SerializedName("LINKED_SETTLEMENT_AMT")
	private String settlementAmtLn;
	@Expose
	@SerializedName("LINKED_PRNPAL_WRITE_OFF_AMT")
	private String principalWriteOffAmtLn;
	@Expose
	@SerializedName("LINKED_REPAYMENT_TENURE")
	private String repaymentTenureLn;
	@Expose
	@SerializedName("LINKED_CASH_LIMIT")
	private String cashLimitLn;
	@Expose
	@SerializedName("LINKED_ACTUAL_PAYMENT")
	private String actualPaymentLn;
	@Expose
	@SerializedName("LINKED_INTEREST_RATE")
	private Integer interestRateLn;
	@Expose
	@SerializedName("LINKED_SUIT_F_WILFUL_DEFAULT")
	private String suitFiledWilfulDefaultLn;
	@Expose
	@SerializedName("LINKED_WRTN_OFF_SETTLD_STATUS")
	private String writtenOffSetteldStatusLn;
	@Expose
	@SerializedName("SECURITY_TYPE_LA")
	private String securityTypeLn;
	@Expose
	@SerializedName("OWNER_NAME_LA")
	private String ownerNameLn;
	@Expose
	@SerializedName("SECURITY_VALUE_LA")
	private String securityValueLn;
	@Expose
	@SerializedName("DATE_OF_VALUE_LA")
	private Date dateOfValueLn;
	@Expose
	@SerializedName("SECURITY_CHARGE_LA")
	private String securityChargeLn;
	@Expose
	@SerializedName("PROPERTY_ADDRESS_LA")
	private String propertyAddressLn;
	@Expose
	@SerializedName("AUTOMOBILE_TYPE_LA")
	private String automobileTypeLn;
	@Expose
	@SerializedName("YEAR_OF_MANUFACTURE_LA")
	private Integer yearOfManufactureLn;
	@Expose
	@SerializedName("REGISTRATION_NUMBER_LA")
	private String registrationNumberLn;
	@Expose
	@SerializedName("ENGINE_NUMBER_LA")
	private String engineNumberLn;
	@Expose
	@SerializedName("CHASIS_NUMBER_LA")
	private String chassisNumberLn;
	
	@Expose
	@SerializedName("SM_NAME")
	private String nameIndtl;
	@Expose
	@SerializedName("SM_ADDRESS")
	private String address;
	@Expose
	@SerializedName("SM_DOB")
	private String dobIndtl;
	@Expose
	@SerializedName("SM_PHONE")
	private String phoneIndtl;
	@Expose
	@SerializedName("SM_PAN")
	private String panIndtl;
	@Expose
	@SerializedName("SM_PASSPORT")
	private String passportIndtl;
	@Expose
	@SerializedName("SM_DRIVING_LICENSE")
	private String drivingLicenseIndtl;
	@Expose
	@SerializedName("SM_VOTER_ID")
	private String voterIdIndtl;
	@Expose
	@SerializedName("SM_E_MAIL")
	private String emailIndtl;
	@Expose
	@SerializedName("SM_RATION_CARD")
	private String rationCardIndtl;
	
	@Expose
	@SerializedName("SM_MATCHED_TYPE")
	private String matchedTypeSe;
	@Expose
	@SerializedName("SM_ACCT_NUMBER")
	private String accountNumberSe;
	@Expose
	@SerializedName("SM_CREDIT_GUARANTOR")
	private String creditGuarantorSe;
	@Expose
	@SerializedName("SM_ACCT_TYPE")
	private String acctTypeSe;
	@Expose
	@SerializedName("SM_DATE_REPORTED")
	private Date dateReportedSe;
	@Expose
	@SerializedName("SM_OWNERSHIP_IND")
	private String ownershipIndicatorSe;
	@Expose
	@SerializedName("SM_ACCOUNT_STATUS")
	private String accountStatusSe;
	@Expose
	@SerializedName("SM_DISBURSED_AMT")
	private String disbursedAmountSe;
	@Expose
	@SerializedName("SM_DISBURSED_DT")
	private Date disbursedDateSe;
	@Expose
	@SerializedName("SM_LAST_PAYMENT_DATE")
	private Date lastPaymentDateSe;
	@Expose
	@SerializedName("SM_CLOSE_DT")
	private Date closedDateSe;
	@Expose
	@SerializedName("SM_INSTALLMENT_AMT")
	private String installmentAmountSe;
	@Expose
	@SerializedName("SM_OVERDUE_AMT")
	private String overdueAmountSe;
	@Expose
	@SerializedName("SM_WRITE_OFF_AMT")
	private String writeOffAmountSe;
	@Expose
	@SerializedName("SM_CURRENT_BAL")
	private String currentBalanceSe;
	@Expose
	@SerializedName("SM_CREDIT_LIMIT")
	private String creditLimitSe;
	@Expose
	@SerializedName("SM_ACCOUNT_REMARKS")
	private String accountRemarksSe;
	@Expose
	@SerializedName("SM_FREQUENCY")
	private String frequencySe;
	@Expose
	@SerializedName("SM_SECURITY_STATUS")
	private String securityStatusSe;
	@Expose
	@SerializedName("SM_ORIGINAL_TERM")
	private Integer originalTermSe;
	@Expose
	@SerializedName("SM_TERM_TO_MATURITY")
	private Integer termToMaturitySe;
	@Expose
	@SerializedName("SM_ACCT_IN_DISPUTE")
	private String accountInDisputeSe;
	@Expose
	@SerializedName("SM_SETTLEMENT_AMT")
	private String settlementAmountSe;
	@Expose
	@SerializedName("SM_PRINCIPAL_WRITE_OFF_AMT")
	private String principalWriteOffAmountSe;
	@Expose
	@SerializedName("SM_COMBINED_PAYMENT_HISTORY")
	private String combinedPaymentHistorySe;
	@Expose
	@SerializedName("SM_REPAYMENT_TENURE")
	private String repaymentTenureSe;
	@Expose
	@SerializedName("SM_CASH_LIMIT")
	private String cashLimitSe;
	@Expose
	@SerializedName("SM_ACTUAL_PAYMENT")
	private String actualPaymentSe;
	@Expose
	@SerializedName("SM_INTEREST_RATE")
	private Integer interestRateSe;
	@Expose
	@SerializedName("SM_SUIT_FILED_WILFUL_DEFAULT")
	private String suitFiledWilfulDefaultSe;
	@Expose
	@SerializedName("SM_WRITTEN_OFF_SETTLED_STATUS")
	private String writtenOffSetteldStatusSe;
	@Expose
	@SerializedName("SM_SECURITY_TYPE_LD")
	private String securityTypeSe;
	@Expose
	@SerializedName("SM_OWNER_NAME_LD")
	private String ownerNameSe;
	@Expose
	@SerializedName("SM_SECURITY_VALUE_LD")
	private String securityValueSe;
	@Expose
	@SerializedName("SM_DATE_OF_VALUE_LD")
	private Date dateOfValueSe;
	@Expose
	@SerializedName("SM_SECURITY_CHARGE_LD")
	private String securityChargeSe;
	@Expose
	@SerializedName("SM_PROPERTY_ADDRESS_LD")
	private String propertyAddressSe;
	@Expose
	@SerializedName("SM_AUTOMOBILE_TYPE_LD")
	private String automobileTypeSe;
	@Expose
	@SerializedName("SM_YEAR_OF_MANUFACTURE_LD")
	private Integer yearOfManufactureSe;
	@Expose
	@SerializedName("SM_REGISTRATION_NUMBER_LD")
	private String registrationNumberSe;
	@Expose
	@SerializedName("SM_ENGINE_NUMBER_LD")
	private String engineNumberSe;
	@Expose
	@SerializedName("SM_CHASIS_NUMBER_LD")
	private String chassisNumberSe;
	
	@Expose
	@SerializedName("SM_LINKED_ACCT_NUMBER")
	private String accountNumberSeLn;
	@Expose
	@SerializedName("SM_LINKED_CREDIT_GUARANTOR")
	private String creditGuarantorSeLn;
	@Expose
	@SerializedName("SM_LINKED_ACCT_TYPE")
	private String accountTypeSeLn;
	@Expose
	@SerializedName("SM_LINKED_DATE_REPORTED")
	private Date dateReportedSeLn;
	@Expose
	@SerializedName("SM_LINKED_OWNERSHIP_IND")
	private String ownershipIndicatorSeLn;
	@Expose
	@SerializedName("SM_LINKED_ACCOUNT_STATUS")
	private String accountStatusSeLn;
	@Expose
	@SerializedName("SM_LINKED_DISBURSED_AMT")
	private String disbursedAmountSeLn;
	@Expose
	@SerializedName("SM_LINKED_DISBURSED_DT")
	private Date disbursedDateSeLn;
	@Expose
	@SerializedName("SM_LINKED_LAST_PAYMENT_DATE")
	private Date lastPaymentDateSeLn;
	@Expose
	@SerializedName("SM_LINKED_CLOSED_DATE")
	private Date closedDateSeLn;
	@Expose
	@SerializedName("SM_LINKED_INSTALLMENT_AMT")
	private String installmentAmountSeLn;
	@Expose
	@SerializedName("SM_LINKED_WRITE_OFF_AMT")
	private String writeOffAmountSeLn;
	@Expose
	@SerializedName("SM_LINKED_OVERDUE_AMT")
	private String overdueAmountSeLn;
	@Expose
	@SerializedName("SM_LINKED_CURRENT_BAL")
	private String currentBalanceSeLn;
	@Expose
	@SerializedName("SM_LINKED_CREDIT_LIMIT")
	private String creditLimitSeLn;
	@Expose
	@SerializedName("SM_LINKED_ACCOUNT_REMARKS")
	private String accountRemarksSeLn; 
	@Expose
	@SerializedName("SM_LINKED_FREQUENCY")
	private String frequencySeLn;
	@Expose
	@SerializedName("SM_LINKED_SECURITY_STATUS")
	private String securityStatusSeLn;
	@Expose
	@SerializedName("SM_LINKED_ORIGINAL_TERM")
	private Integer originalTermSeLn;
	@Expose
	@SerializedName("SM_LINKED_TERM_TO_MATURITY")
	private Integer termToMaturitySeLn;
	@Expose
	@SerializedName("SM_LINKED_ACCT_IN_DISPUTE")
	private String accountInDisputeSeLn;
	@Expose
	@SerializedName("SM_LINKED_SETTLEMENT_AMT")
	private String settlementAmtSeLn;
	@Expose
	@SerializedName("SM_LINKED_PRNPAL_WRITE_OFF_AMT")
	private String principalWriteOffAmtSeLn;
	@Expose
	@SerializedName("SM_LINKED_REPAYMENT_TENURE")
	private String repaymentTenureSeLn;
	@Expose
	@SerializedName("SM_LINKED_CASH_LIMIT")
	private String cashLimitSeLn;
	@Expose
	@SerializedName("SM_LINKED_ACTUAL_PAYMENT")
	private String actualPaymentSeLn;
	@Expose
	@SerializedName("SM_LINKED_INTEREST_RATE")
	private Integer interestRateSeLn;
	@Expose
	@SerializedName("SM_LINKED_SUIT_F_WILFUL_DEFAULT")
	private String suitFiledWilfulDefaultSeLn;
	@Expose
	@SerializedName("SM_LINKED_WRTN_OFF_SETTLD_STATUS")
	private String writtenOffSetteldStatusSeLn;
	@Expose
	@SerializedName("SM_SECURITY_TYPE_LA")
	private String securityTypeSeLn;
	@Expose
	@SerializedName("SM_OWNER_NAME_LA")
	private String ownerNameSeLn;
	@Expose
	@SerializedName("SM_SECURITY_VALUE_LA")
	private String securityValueSeLn;
	@Expose
	@SerializedName("SM_DATE_OF_VALUE_LA")
	private Date dateOfValueSeLn;
	@Expose
	@SerializedName("SM_SECURITY_CHARGE_LA")
	private String securityChargeSeLn;
	@Expose
	@SerializedName("SM_PROPERTY_ADDRESS_LA")
	private String propertyAddressSeLn;
	@Expose
	@SerializedName("SM_AUTOMOBILE_TYPE_LA")
	private String automobileTypeSeLn;
	@Expose
	@SerializedName("SM_YEAR_OF_MANUFACTURE_LA")
	private Integer yearOfManufactureSeLn;
	@Expose
	@SerializedName("SM_REGISTRATION_NUMBER_LA")
	private String registrationNumberSeLn;
	@Expose
	@SerializedName("SM_ENGINE_NUMBER_LA")
	private String engineNumberSeLn;
	@Expose
	@SerializedName("SM_CHASIS_NUMBER_LA")
	private String chassisNumberSeLn;

	@Expose
	@SerializedName("SCORE_TYPE")
	private String scoreType;	
	@Expose
	@SerializedName("SCORE_VERSION")
	private String scoreVersion;
	@Expose
	@SerializedName("SCORE_VALUE")
	private BigInteger scoreValue;
	@Expose
	@SerializedName("SCORE_FACTORS")
	private String scoreFactors;
	@Expose
	@SerializedName("SCORE_COMMENTS")
	private String scoreComments;
	
	@Expose
	@SerializedName("MEMBER_NAME")
	private String memberName;
	@Expose
	@SerializedName("INQUIRY_DATE")
	private Date inquiryDate;
	@Expose
	@SerializedName("PURPOSE")
	private String purpose;
	@Expose
	@SerializedName("OWNERSHIP_TYPE")
	private String ownershipType;
	@Expose
	@SerializedName("AMOUNT")
	private String amount;
	@Expose
	@SerializedName("REMARK")
	private String remark;
	
	@Expose
	@SerializedName("COMMENT_TEXT")
	private String commentText;
	@Expose
	@SerializedName("COMMENT_DATE")
	private Date commentDate;
	@Expose
	@SerializedName("BUREAU_COMMENT")
	private String bureauComment;
	
	@Expose
	@SerializedName("ALERT_TYPE")
	private String alertType;
	@Expose
	@SerializedName("ALERT_DESC")
	private String alertDescription;
	
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private String outputWriteFlag;
	@Expose
	@SerializedName("OUTPUT_WRITE_TIME")
	private String outputWritetime;
	@Expose
	@SerializedName("OUTPUT_READ_TIME")
	private String outputReadTime;
	
	public HibHighmarkBaseSropDomain(Integer srNumber, String soaSourceName, String memberReferenceNumber, String soaFileName) {
		this.srNo = srNumber;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = soaFileName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSrNo() {
		return srNo;
	}

	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}

	public String getSoaSourceName() {
		return soaSourceName;
	}

	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}

	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}

	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}

	public Date getDateOfRequest() {
		return dateOfRequest;
	}

	public void setDateOfRequest(Date dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}

	public String getPreparedFor() {
		return preparedFor;
	}

	public void setPreparedFor(String preparedFor) {
		this.preparedFor = preparedFor;
	}

	public String getPreparedForId() {
		return preparedForId;
	}

	public void setPreparedForId(String preparedForId) {
		this.preparedForId = preparedForId;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public BigInteger getBatchId() {
		return batchId;
	}

	public void setBatchId(BigInteger batchId) {
		this.batchId = batchId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNameIQ() {
		return nameIQ;
	}

	public void setNameIQ(String nameIQ) {
		this.nameIQ = nameIQ;
	}

	public String getAka() {
		return aka;
	}

	public void setAka(String aka) {
		this.aka = aka;
	}

	public String getSpouse() {
		return spouse;
	}

	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	public String getFather() {
		return father;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public String getMother() {
		return mother;
	}

	public void setMother(String mother) {
		this.mother = mother;
	}

	public Date getDobIQ() {
		return dobIQ;
	}

	public void setDobIQ(Date dobIQ) {
		this.dobIQ = dobIQ;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getAgeAsOn() {
		return ageAsOn;
	}

	public void setAgeAsOn(Date ageAsOn) {
		this.ageAsOn = ageAsOn;
	}

	public String getRationCard() {
		return rationCard;
	}

	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getVotersId() {
		return votersId;
	}

	public void setVotersId(String votersId) {
		this.votersId = votersId;
	}

	public String getDrivingLicenceNo() {
		return drivingLicenceNo;
	}

	public void setDrivingLicenceNo(String drivingLicenceNo) {
		this.drivingLicenceNo = drivingLicenceNo;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOwnership() {
		return ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public BigInteger getPhone1() {
		return phone1;
	}

	public void setPhone1(BigInteger phone1) {
		this.phone1 = phone1;
	}

	public BigInteger getPhone2() {
		return phone2;
	}

	public void setPhone2(BigInteger phone2) {
		this.phone2 = phone2;
	}

	public BigInteger getPhone3() {
		return phone3;
	}

	public void setPhone3(BigInteger phone3) {
		this.phone3 = phone3;
	}

	public String getEmailId1() {
		return emailId1;
	}

	public void setEmailId1(String emailId1) {
		this.emailId1 = emailId1;
	}

	public String getEmailId2() {
		return emailId2;
	}

	public void setEmailId2(String emailId2) {
		this.emailId2 = emailId2;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getKendra() {
		return kendra;
	}

	public void setKendra(String kendra) {
		this.kendra = kendra;
	}

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public String getLosAppId() {
		return losAppId;
	}

	public void setLosAppId(String losAppId) {
		this.losAppId = losAppId;
	}

	public String getCreditInquiryPurposeType() {
		return creditInquiryPurposeType;
	}

	public void setCreditInquiryPurposeType(String creditInquiryPurposeType) {
		this.creditInquiryPurposeType = creditInquiryPurposeType;
	}

	public String getCreditInquiryPurposeTypeDesc() {
		return creditInquiryPurposeTypeDesc;
	}

	public void setCreditInquiryPurposeTypeDesc(String creditInquiryPurposeTypeDesc) {
		this.creditInquiryPurposeTypeDesc = creditInquiryPurposeTypeDesc;
	}

	public String getCreditInquiryStage() {
		return creditInquiryStage;
	}

	public void setCreditInquiryStage(String creditInquiryStage) {
		this.creditInquiryStage = creditInquiryStage;
	}

	public String getCreditReportId() {
		return creditReportId;
	}

	public void setCreditReportId(String creditReportId) {
		this.creditReportId = creditReportId;
	}

	public String getCreditRequestType() {
		return creditRequestType;
	}

	public void setCreditRequestType(String creditRequestType) {
		this.creditRequestType = creditRequestType;
	}

	public String getCreditReportTransectionDateTime() {
		return creditReportTransectionDateTime;
	}

	public void setCreditReportTransectionDateTime(
			String creditReportTransectionDateTime) {
		this.creditReportTransectionDateTime = creditReportTransectionDateTime;
	}

	public String getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(String accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public BigInteger getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(BigInteger loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Integer getPrimaryNumberOfAccount() {
		return primaryNumberOfAccount;
	}

	public void setPrimaryNumberOfAccount(Integer primaryNumberOfAccount) {
		this.primaryNumberOfAccount = primaryNumberOfAccount;
	}

	public Integer getPrimaryActiveNoOfAccount() {
		return primaryActiveNoOfAccount;
	}

	public void setPrimaryActiveNoOfAccount(Integer primaryActiveNoOfAccount) {
		this.primaryActiveNoOfAccount = primaryActiveNoOfAccount;
	}

	public Integer getPrimaryOverdueNumberOfAccount() {
		return primaryOverdueNumberOfAccount;
	}

	public void setPrimaryOverdueNumberOfAccount(
			Integer primaryOverdueNumberOfAccount) {
		this.primaryOverdueNumberOfAccount = primaryOverdueNumberOfAccount;
	}

	public String getPrimaryCurrentBalance() {
		return primaryCurrentBalance;
	}

	public void setPrimaryCurrentBalance(String primaryCurrentBalance) {
		this.primaryCurrentBalance = primaryCurrentBalance;
	}

	public String getPrimarySanctionedAmount() {
		return primarySanctionedAmount;
	}

	public void setPrimarySanctionedAmount(String primarySanctionedAmount) {
		this.primarySanctionedAmount = primarySanctionedAmount;
	}

	public String getPrimaryDisbursedAmount() {
		return primaryDisbursedAmount;
	}

	public void setPrimaryDisbursedAmount(String primaryDisbursedAmount) {
		this.primaryDisbursedAmount = primaryDisbursedAmount;
	}

	public Integer getPrimarySecuredNumberOfAccount() {
		return primarySecuredNumberOfAccount;
	}

	public void setPrimarySecuredNumberOfAccount(
			Integer primarySecuredNumberOfAccount) {
		this.primarySecuredNumberOfAccount = primarySecuredNumberOfAccount;
	}

	public Integer getPrimaryUnsecuredNumberOfAccount() {
		return primaryUnsecuredNumberOfAccount;
	}

	public void setPrimaryUnsecuredNumberOfAccount(
			Integer primaryUnsecuredNumberOfAccount) {
		this.primaryUnsecuredNumberOfAccount = primaryUnsecuredNumberOfAccount;
	}

	public Integer getPrimaryUntaggedNumberOfAccount() {
		return primaryUntaggedNumberOfAccount;
	}

	public void setPrimaryUntaggedNumberOfAccount(
			Integer primaryUntaggedNumberOfAccount) {
		this.primaryUntaggedNumberOfAccount = primaryUntaggedNumberOfAccount;
	}

	public Integer getSecondaryVNumberOfAccount() {
		return secondaryVNumberOfAccount;
	}

	public void setSecondaryVNumberOfAccount(Integer secondaryVNumberOfAccount) {
		this.secondaryVNumberOfAccount = secondaryVNumberOfAccount;
	}

	public Integer getSecondaryActiveNumberOfAccount() {
		return secondaryActiveNumberOfAccount;
	}

	public void setSecondaryActiveNumberOfAccount(
			Integer secondaryActiveNumberOfAccount) {
		this.secondaryActiveNumberOfAccount = secondaryActiveNumberOfAccount;
	}

	public Integer getSecondaryOverdueNumberOfAccount() {
		return secondaryOverdueNumberOfAccount;
	}

	public void setSecondaryOverdueNumberOfAccount(
			Integer secondaryOverdueNumberOfAccount) {
		this.secondaryOverdueNumberOfAccount = secondaryOverdueNumberOfAccount;
	}

	public String getSecondaryCurrentBalance() {
		return secondaryCurrentBalance;
	}

	public void setSecondaryCurrentBalance(String secondaryCurrentBalance) {
		this.secondaryCurrentBalance = secondaryCurrentBalance;
	}

	public String getSecondarySanctionedAmount() {
		return secondarySanctionedAmount;
	}

	public void setSecondarySanctionedAmount(String secondarySanctionedAmount) {
		this.secondarySanctionedAmount = secondarySanctionedAmount;
	}

	public String getSecondaryDisbursedAmount() {
		return secondaryDisbursedAmount;
	}

	public void setSecondaryDisbursedAmount(String secondaryDisbursedAmount) {
		this.secondaryDisbursedAmount = secondaryDisbursedAmount;
	}

	public Integer getSecondarySecuredNumberOfAccount() {
		return secondarySecuredNumberOfAccount;
	}

	public void setSecondarySecuredNumberOfAccount(
			Integer secondarySecuredNumberOfAccount) {
		this.secondarySecuredNumberOfAccount = secondarySecuredNumberOfAccount;
	}

	public Integer getSecondaryUnsecuredNumberOfAccount() {
		return secondaryUnsecuredNumberOfAccount;
	}

	public void setSecondaryUnsecuredNumberOfAccount(
			Integer secondaryUnsecuredNumberOfAccount) {
		this.secondaryUnsecuredNumberOfAccount = secondaryUnsecuredNumberOfAccount;
	}

	public Integer getSecondaryUntaggedNumberOfAccount() {
		return secondaryUntaggedNumberOfAccount;
	}

	public void setSecondaryUntaggedNumberOfAccount(
			Integer secondaryUntaggedNumberOfAccount) {
		this.secondaryUntaggedNumberOfAccount = secondaryUntaggedNumberOfAccount;
	}

	public BigInteger getInquriesInLastSixMonth() {
		return inquriesInLastSixMonth;
	}

	public void setInquriesInLastSixMonth(BigInteger inquriesInLastSixMonth) {
		this.inquriesInLastSixMonth = inquriesInLastSixMonth;
	}

	public BigInteger getLengthOfCreditHistoryYear() {
		return lengthOfCreditHistoryYear;
	}

	public void setLengthOfCreditHistoryYear(BigInteger lengthOfCreditHistoryYear) {
		this.lengthOfCreditHistoryYear = lengthOfCreditHistoryYear;
	}

	public BigInteger getLengthOfCreditHistoryMonth() {
		return lengthOfCreditHistoryMonth;
	}

	public void setLengthOfCreditHistoryMonth(BigInteger lengthOfCreditHistoryMonth) {
		this.lengthOfCreditHistoryMonth = lengthOfCreditHistoryMonth;
	}

	public BigInteger getAverageAccountAgeYear() {
		return averageAccountAgeYear;
	}

	public void setAverageAccountAgeYear(BigInteger averageAccountAgeYear) {
		this.averageAccountAgeYear = averageAccountAgeYear;
	}

	public BigInteger getAverageAccountAgeMonth() {
		return averageAccountAgeMonth;
	}

	public void setAverageAccountAgeMonth(BigInteger averageAccountAgeMonth) {
		this.averageAccountAgeMonth = averageAccountAgeMonth;
	}

	public BigInteger getNewAccountInLastSixMonth() {
		return newAccountInLastSixMonth;
	}

	public void setNewAccountInLastSixMonth(BigInteger newAccountInLastSixMonth) {
		this.newAccountInLastSixMonth = newAccountInLastSixMonth;
	}

	public BigInteger getNewDelinqAccountInLastSixMonth() {
		return newDelinqAccountInLastSixMonth;
	}

	public void setNewDelinqAccountInLastSixMonth(
			BigInteger newDelinqAccountInLastSixMonth) {
		this.newDelinqAccountInLastSixMonth = newDelinqAccountInLastSixMonth;
	}

	public String getNameVar() {
		return nameVar;
	}

	public void setNameVar(String nameVar) {
		this.nameVar = nameVar;
	}

	public Date getNameVarDate() {
		return nameVarDate;
	}

	public void setNameVarDate(Date nameVarDate) {
		this.nameVarDate = nameVarDate;
	}

	public String getAddressVar() {
		return addressVar;
	}

	public void setAddressVar(String addressVar) {
		this.addressVar = addressVar;
	}

	public Date getAddressVarDate() {
		return addressVarDate;
	}

	public void setAddressVarDate(Date addressVarDate) {
		this.addressVarDate = addressVarDate;
	}

	public String getPanVar() {
		return panVar;
	}

	public void setPanVar(String panVar) {
		this.panVar = panVar;
	}

	public Date getPanVarDate() {
		return panVarDate;
	}

	public void setPanVarDate(Date panVarDate) {
		this.panVarDate = panVarDate;
	}

	public String getDlVar() {
		return dlVar;
	}

	public void setDlVar(String dlVar) {
		this.dlVar = dlVar;
	}

	public Date getDlVarDate() {
		return dlVarDate;
	}

	public void setDlVarDate(Date dlVarDate) {
		this.dlVarDate = dlVarDate;
	}

	public String getDobVar() {
		return dobVar;
	}

	public void setDobVar(String dobVar) {
		this.dobVar = dobVar;
	}

	public Date getDobVarDate() {
		return dobVarDate;
	}

	public void setDobVarDate(Date dobVarDate) {
		this.dobVarDate = dobVarDate;
	}

	public String getVoterVar() {
		return voterVar;
	}

	public void setVoterVar(String voterVar) {
		this.voterVar = voterVar;
	}

	public Date getVoterVarDate() {
		return voterVarDate;
	}

	public void setVoterVarDate(Date voterVarDate) {
		this.voterVarDate = voterVarDate;
	}

	public String getPassportVar() {
		return passportVar;
	}

	public void setPassportVar(String passportVar) {
		this.passportVar = passportVar;
	}

	public Date getPassportVarDate() {
		return passportVarDate;
	}

	public void setPassportVarDate(Date passportVarDate) {
		this.passportVarDate = passportVarDate;
	}

	public String getPhoneVar() {
		return phoneVar;
	}

	public void setPhoneVar(String phoneVar) {
		this.phoneVar = phoneVar;
	}

	public Date getPhoneVarDate() {
		return phoneVarDate;
	}

	public void setPhoneVarDate(Date phoneVarDate) {
		this.phoneVarDate = phoneVarDate;
	}

	public String getRationVar() {
		return rationVar;
	}

	public void setRationVar(String rationVar) {
		this.rationVar = rationVar;
	}

	public Date getRationVarDate() {
		return rationVarDate;
	}

	public void setRationVarDate(Date rationVarDate) {
		this.rationVarDate = rationVarDate;
	}

	public String getEmailVar() {
		return emailVar;
	}

	public void setEmailVar(String emailVar) {
		this.emailVar = emailVar;
	}

	public Date getEmailVarDate() {
		return emailVarDate;
	}

	public void setEmailVarDate(Date emailVarDate) {
		this.emailVarDate = emailVarDate;
	}

	public String getMatchedType() {
		return matchedType;
	}

	public void setMatchedType(String matchedType) {
		this.matchedType = matchedType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCreditGuarantor() {
		return creditGuarantor;
	}

	public void setCreditGuarantor(String creditGuarantor) {
		this.creditGuarantor = creditGuarantor;
	}

	public String getAcctType() {
		return acctType;
	}

	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}

	public Date getDateReported() {
		return dateReported;
	}

	public void setDateReported(Date dateReported) {
		this.dateReported = dateReported;
	}

	public String getOwnershipIndicator() {
		return ownershipIndicator;
	}

	public void setOwnershipIndicator(String ownershipIndicator) {
		this.ownershipIndicator = ownershipIndicator;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(String disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public Date getDisbursedDate() {
		return disbursedDate;
	}

	public void setDisbursedDate(Date disbursedDate) {
		this.disbursedDate = disbursedDate;
	}

	public Date getLastPaymentDate() {
		return lastPaymentDate;
	}

	public void setLastPaymentDate(Date lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	public Date getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(Date closedDate) {
		this.closedDate = closedDate;
	}

	public String getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	public String getOverdueAmount() {
		return overdueAmount;
	}

	public void setOverdueAmount(String overdueAmount) {
		this.overdueAmount = overdueAmount;
	}

	public String getWriteOffAmount() {
		return writeOffAmount;
	}

	public void setWriteOffAmount(String writeOffAmount) {
		this.writeOffAmount = writeOffAmount;
	}

	public String getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getAccountRemarks() {
		return accountRemarks;
	}

	public void setAccountRemarks(String accountRemarks) {
		this.accountRemarks = accountRemarks;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getSecurityStatus() {
		return securityStatus;
	}

	public void setSecurityStatus(String securityStatus) {
		this.securityStatus = securityStatus;
	}

	public Integer getOriginalTerm() {
		return originalTerm;
	}

	public void setOriginalTerm(Integer originalTerm) {
		this.originalTerm = originalTerm;
	}

	public Integer getTermToMaturity() {
		return termToMaturity;
	}

	public void setTermToMaturity(Integer termToMaturity) {
		this.termToMaturity = termToMaturity;
	}

	public String getAccountInDispute() {
		return accountInDispute;
	}

	public void setAccountInDispute(String accountInDispute) {
		this.accountInDispute = accountInDispute;
	}

	public String getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(String settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	public String getPrincipalWriteOffAmount() {
		return principalWriteOffAmount;
	}

	public void setPrincipalWriteOffAmount(String principalWriteOffAmount) {
		this.principalWriteOffAmount = principalWriteOffAmount;
	}

	public String getCombinedPaymentHistory() {
		return combinedPaymentHistory;
	}

	public void setCombinedPaymentHistory(String combinedPaymentHistory) {
		this.combinedPaymentHistory = combinedPaymentHistory;
	}

	public String getRepaymentTenure() {
		return repaymentTenure;
	}

	public void setRepaymentTenure(String repaymentTenure) {
		this.repaymentTenure = repaymentTenure;
	}

	public String getCashLimit() {
		return cashLimit;
	}

	public void setCashLimit(String cashLimit) {
		this.cashLimit = cashLimit;
	}

	public String getActualPayment() {
		return actualPayment;
	}

	public void setActualPayment(String actualPayment) {
		this.actualPayment = actualPayment;
	}

	public Integer getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Integer interestRate) {
		this.interestRate = interestRate;
	}

	public String getSuitFiledWilfulDefault() {
		return suitFiledWilfulDefault;
	}

	public void setSuitFiledWilfulDefault(String suitFiledWilfulDefault) {
		this.suitFiledWilfulDefault = suitFiledWilfulDefault;
	}

	public String getWrittenOffSetteldStatus() {
		return writtenOffSetteldStatus;
	}

	public void setWrittenOffSetteldStatus(String writtenOffSetteldStatus) {
		this.writtenOffSetteldStatus = writtenOffSetteldStatus;
	}

	public String getSecurityType() {
		return securityType;
	}

	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getSecurityValue() {
		return securityValue;
	}

	public void setSecurityValue(String securityValue) {
		this.securityValue = securityValue;
	}

	public Date getDateOfValue() {
		return dateOfValue;
	}

	public void setDateOfValue(Date dateOfValue) {
		this.dateOfValue = dateOfValue;
	}

	public String getSecurityCharge() {
		return securityCharge;
	}

	public void setSecurityCharge(String securityCharge) {
		this.securityCharge = securityCharge;
	}

	public String getPropertyAddress() {
		return propertyAddress;
	}

	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}

	public String getAutomobileType() {
		return automobileType;
	}

	public void setAutomobileType(String automobileType) {
		this.automobileType = automobileType;
	}

	public Integer getYearOfManufacture() {
		return yearOfManufacture;
	}

	public void setYearOfManufacture(Integer yearOfManufacture) {
		this.yearOfManufacture = yearOfManufacture;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getChassisNumber() {
		return chassisNumber;
	}

	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}

	public String getAccountNumberLn() {
		return accountNumberLn;
	}

	public void setAccountNumberLn(String accountNumberLn) {
		this.accountNumberLn = accountNumberLn;
	}

	public String getCreditGuarantorLn() {
		return creditGuarantorLn;
	}

	public void setCreditGuarantorLn(String creditGuarantorLn) {
		this.creditGuarantorLn = creditGuarantorLn;
	}

	public String getAccountTypeLn() {
		return accountTypeLn;
	}

	public void setAccountTypeLn(String accountTypeLn) {
		this.accountTypeLn = accountTypeLn;
	}

	public Date getDateReportedLn() {
		return dateReportedLn;
	}

	public void setDateReportedLn(Date dateReportedLn) {
		this.dateReportedLn = dateReportedLn;
	}

	public String getOwnershipIndicatorLn() {
		return ownershipIndicatorLn;
	}

	public void setOwnershipIndicatorLn(String ownershipIndicatorLn) {
		this.ownershipIndicatorLn = ownershipIndicatorLn;
	}

	public String getAccountStatusLn() {
		return accountStatusLn;
	}

	public void setAccountStatusLn(String accountStatusLn) {
		this.accountStatusLn = accountStatusLn;
	}

	public String getDisbursedAmountLn() {
		return disbursedAmountLn;
	}

	public void setDisbursedAmountLn(String disbursedAmountLn) {
		this.disbursedAmountLn = disbursedAmountLn;
	}

	public Date getDisbursedDateLn() {
		return disbursedDateLn;
	}

	public void setDisbursedDateLn(Date disbursedDateLn) {
		this.disbursedDateLn = disbursedDateLn;
	}

	public Date getLastPaymentDateLn() {
		return lastPaymentDateLn;
	}

	public void setLastPaymentDateLn(Date lastPaymentDateLn) {
		this.lastPaymentDateLn = lastPaymentDateLn;
	}

	public Date getClosedDateLn() {
		return closedDateLn;
	}

	public void setClosedDateLn(Date closedDateLn) {
		this.closedDateLn = closedDateLn;
	}

	public String getInstallmentAmountLn() {
		return installmentAmountLn;
	}

	public void setInstallmentAmountLn(String installmentAmountLn) {
		this.installmentAmountLn = installmentAmountLn;
	}

	public String getWriteOffAmountLn() {
		return writeOffAmountLn;
	}

	public void setWriteOffAmountLn(String writeOffAmountLn) {
		this.writeOffAmountLn = writeOffAmountLn;
	}

	public String getOverdueAmountLn() {
		return overdueAmountLn;
	}

	public void setOverdueAmountLn(String overdueAmountLn) {
		this.overdueAmountLn = overdueAmountLn;
	}

	public String getCurrentBalanceLn() {
		return currentBalanceLn;
	}

	public void setCurrentBalanceLn(String currentBalanceLn) {
		this.currentBalanceLn = currentBalanceLn;
	}

	public String getCreditLimitLn() {
		return creditLimitLn;
	}

	public void setCreditLimitLn(String creditLimitLn) {
		this.creditLimitLn = creditLimitLn;
	}

	public String getAccountRemarksLn() {
		return accountRemarksLn;
	}

	public void setAccountRemarksLn(String accountRemarksLn) {
		this.accountRemarksLn = accountRemarksLn;
	}

	public String getFrequencyLn() {
		return frequencyLn;
	}

	public void setFrequencyLn(String frequencyLn) {
		this.frequencyLn = frequencyLn;
	}

	public String getSecurityStatusLn() {
		return securityStatusLn;
	}

	public void setSecurityStatusLn(String securityStatusLn) {
		this.securityStatusLn = securityStatusLn;
	}

	public Integer getOriginalTermLn() {
		return originalTermLn;
	}

	public void setOriginalTermLn(Integer originalTermLn) {
		this.originalTermLn = originalTermLn;
	}

	public Integer getTermToMaturityLn() {
		return termToMaturityLn;
	}

	public void setTermToMaturityLn(Integer termToMaturityLn) {
		this.termToMaturityLn = termToMaturityLn;
	}

	public String getAccountInDisputeLn() {
		return accountInDisputeLn;
	}

	public void setAccountInDisputeLn(String accountInDisputeLn) {
		this.accountInDisputeLn = accountInDisputeLn;
	}

	public String getSettlementAmtLn() {
		return settlementAmtLn;
	}

	public void setSettlementAmtLn(String settlementAmtLn) {
		this.settlementAmtLn = settlementAmtLn;
	}

	public String getPrincipalWriteOffAmtLn() {
		return principalWriteOffAmtLn;
	}

	public void setPrincipalWriteOffAmtLn(String principalWriteOffAmtLn) {
		this.principalWriteOffAmtLn = principalWriteOffAmtLn;
	}

	public String getRepaymentTenureLn() {
		return repaymentTenureLn;
	}

	public void setRepaymentTenureLn(String repaymentTenureLn) {
		this.repaymentTenureLn = repaymentTenureLn;
	}

	public String getCashLimitLn() {
		return cashLimitLn;
	}

	public void setCashLimitLn(String cashLimitLn) {
		this.cashLimitLn = cashLimitLn;
	}

	public String getActualPaymentLn() {
		return actualPaymentLn;
	}

	public void setActualPaymentLn(String actualPaymentLn) {
		this.actualPaymentLn = actualPaymentLn;
	}

	public Integer getInterestRateLn() {
		return interestRateLn;
	}

	public void setInterestRateLn(Integer interestRateLn) {
		this.interestRateLn = interestRateLn;
	}

	public String getSuitFiledWilfulDefaultLn() {
		return suitFiledWilfulDefaultLn;
	}

	public void setSuitFiledWilfulDefaultLn(String suitFiledWilfulDefaultLn) {
		this.suitFiledWilfulDefaultLn = suitFiledWilfulDefaultLn;
	}

	public String getWrittenOffSetteldStatusLn() {
		return writtenOffSetteldStatusLn;
	}

	public void setWrittenOffSetteldStatusLn(String writtenOffSetteldStatusLn) {
		this.writtenOffSetteldStatusLn = writtenOffSetteldStatusLn;
	}

	public String getSecurityTypeLn() {
		return securityTypeLn;
	}

	public void setSecurityTypeLn(String securityTypeLn) {
		this.securityTypeLn = securityTypeLn;
	}

	public String getOwnerNameLn() {
		return ownerNameLn;
	}

	public void setOwnerNameLn(String ownerNameLn) {
		this.ownerNameLn = ownerNameLn;
	}

	public String getSecurityValueLn() {
		return securityValueLn;
	}

	public void setSecurityValueLn(String securityValueLn) {
		this.securityValueLn = securityValueLn;
	}

	public Date getDateOfValueLn() {
		return dateOfValueLn;
	}

	public void setDateOfValueLn(Date dateOfValueLn) {
		this.dateOfValueLn = dateOfValueLn;
	}

	public String getSecurityChargeLn() {
		return securityChargeLn;
	}

	public void setSecurityChargeLn(String securityChargeLn) {
		this.securityChargeLn = securityChargeLn;
	}

	public String getPropertyAddressLn() {
		return propertyAddressLn;
	}

	public void setPropertyAddressLn(String propertyAddressLn) {
		this.propertyAddressLn = propertyAddressLn;
	}

	public String getAutomobileTypeLn() {
		return automobileTypeLn;
	}

	public void setAutomobileTypeLn(String automobileTypeLn) {
		this.automobileTypeLn = automobileTypeLn;
	}

	public Integer getYearOfManufactureLn() {
		return yearOfManufactureLn;
	}

	public void setYearOfManufactureLn(Integer yearOfManufactureLn) {
		this.yearOfManufactureLn = yearOfManufactureLn;
	}

	public String getRegistrationNumberLn() {
		return registrationNumberLn;
	}

	public void setRegistrationNumberLn(String registrationNumberLn) {
		this.registrationNumberLn = registrationNumberLn;
	}

	public String getEngineNumberLn() {
		return engineNumberLn;
	}

	public void setEngineNumberLn(String engineNumberLn) {
		this.engineNumberLn = engineNumberLn;
	}

	public String getChassisNumberLn() {
		return chassisNumberLn;
	}

	public void setChassisNumberLn(String chassisNumberLn) {
		this.chassisNumberLn = chassisNumberLn;
	}

	public String getNameIndtl() {
		return nameIndtl;
	}

	public void setNameIndtl(String nameIndtl) {
		this.nameIndtl = nameIndtl;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDobIndtl() {
		return dobIndtl;
	}

	public void setDobIndtl(String dobIndtl) {
		this.dobIndtl = dobIndtl;
	}

	public String getPhoneIndtl() {
		return phoneIndtl;
	}

	public void setPhoneIndtl(String phoneIndtl) {
		this.phoneIndtl = phoneIndtl;
	}

	public String getPanIndtl() {
		return panIndtl;
	}

	public void setPanIndtl(String panIndtl) {
		this.panIndtl = panIndtl;
	}

	public String getPassportIndtl() {
		return passportIndtl;
	}

	public void setPassportIndtl(String passportIndtl) {
		this.passportIndtl = passportIndtl;
	}

	public String getDrivingLicenseIndtl() {
		return drivingLicenseIndtl;
	}

	public void setDrivingLicenseIndtl(String drivingLicenseIndtl) {
		this.drivingLicenseIndtl = drivingLicenseIndtl;
	}

	public String getVoterIdIndtl() {
		return voterIdIndtl;
	}

	public void setVoterIdIndtl(String voterIdIndtl) {
		this.voterIdIndtl = voterIdIndtl;
	}

	public String getEmailIndtl() {
		return emailIndtl;
	}

	public void setEmailIndtl(String emailIndtl) {
		this.emailIndtl = emailIndtl;
	}

	public String getRationCardIndtl() {
		return rationCardIndtl;
	}

	public void setRationCardIndtl(String rationCardIndtl) {
		this.rationCardIndtl = rationCardIndtl;
	}

	public String getMatchedTypeSe() {
		return matchedTypeSe;
	}

	public void setMatchedTypeSe(String matchedTypeSe) {
		this.matchedTypeSe = matchedTypeSe;
	}

	public String getAccountNumberSe() {
		return accountNumberSe;
	}

	public void setAccountNumberSe(String accountNumberSe) {
		this.accountNumberSe = accountNumberSe;
	}

	public String getCreditGuarantorSe() {
		return creditGuarantorSe;
	}

	public void setCreditGuarantorSe(String creditGuarantorSe) {
		this.creditGuarantorSe = creditGuarantorSe;
	}

	public String getAcctTypeSe() {
		return acctTypeSe;
	}

	public void setAcctTypeSe(String acctTypeSe) {
		this.acctTypeSe = acctTypeSe;
	}

	public Date getDateReportedSe() {
		return dateReportedSe;
	}

	public void setDateReportedSe(Date dateReportedSe) {
		this.dateReportedSe = dateReportedSe;
	}

	public String getOwnershipIndicatorSe() {
		return ownershipIndicatorSe;
	}

	public void setOwnershipIndicatorSe(String ownershipIndicatorSe) {
		this.ownershipIndicatorSe = ownershipIndicatorSe;
	}

	public String getAccountStatusSe() {
		return accountStatusSe;
	}

	public void setAccountStatusSe(String accountStatusSe) {
		this.accountStatusSe = accountStatusSe;
	}

	public String getDisbursedAmountSe() {
		return disbursedAmountSe;
	}

	public void setDisbursedAmountSe(String disbursedAmountSe) {
		this.disbursedAmountSe = disbursedAmountSe;
	}

	public Date getDisbursedDateSe() {
		return disbursedDateSe;
	}

	public void setDisbursedDateSe(Date disbursedDateSe) {
		this.disbursedDateSe = disbursedDateSe;
	}

	public Date getLastPaymentDateSe() {
		return lastPaymentDateSe;
	}

	public void setLastPaymentDateSe(Date lastPaymentDateSe) {
		this.lastPaymentDateSe = lastPaymentDateSe;
	}

	public Date getClosedDateSe() {
		return closedDateSe;
	}

	public void setClosedDateSe(Date closedDateSe) {
		this.closedDateSe = closedDateSe;
	}

	public String getInstallmentAmountSe() {
		return installmentAmountSe;
	}

	public void setInstallmentAmountSe(String installmentAmountSe) {
		this.installmentAmountSe = installmentAmountSe;
	}

	public String getOverdueAmountSe() {
		return overdueAmountSe;
	}

	public void setOverdueAmountSe(String overdueAmountSe) {
		this.overdueAmountSe = overdueAmountSe;
	}

	public String getWriteOffAmountSe() {
		return writeOffAmountSe;
	}

	public void setWriteOffAmountSe(String writeOffAmountSe) {
		this.writeOffAmountSe = writeOffAmountSe;
	}

	public String getCurrentBalanceSe() {
		return currentBalanceSe;
	}

	public void setCurrentBalanceSe(String currentBalanceSe) {
		this.currentBalanceSe = currentBalanceSe;
	}

	public String getCreditLimitSe() {
		return creditLimitSe;
	}

	public void setCreditLimitSe(String creditLimitSe) {
		this.creditLimitSe = creditLimitSe;
	}

	public String getAccountRemarksSe() {
		return accountRemarksSe;
	}

	public void setAccountRemarksSe(String accountRemarksSe) {
		this.accountRemarksSe = accountRemarksSe;
	}

	public String getFrequencySe() {
		return frequencySe;
	}

	public void setFrequencySe(String frequencySe) {
		this.frequencySe = frequencySe;
	}

	public String getSecurityStatusSe() {
		return securityStatusSe;
	}

	public void setSecurityStatusSe(String securityStatusSe) {
		this.securityStatusSe = securityStatusSe;
	}

	public Integer getOriginalTermSe() {
		return originalTermSe;
	}

	public void setOriginalTermSe(Integer originalTermSe) {
		this.originalTermSe = originalTermSe;
	}

	public Integer getTermToMaturitySe() {
		return termToMaturitySe;
	}

	public void setTermToMaturitySe(Integer termToMaturitySe) {
		this.termToMaturitySe = termToMaturitySe;
	}

	public String getAccountInDisputeSe() {
		return accountInDisputeSe;
	}

	public void setAccountInDisputeSe(String accountInDisputeSe) {
		this.accountInDisputeSe = accountInDisputeSe;
	}

	public String getSettlementAmountSe() {
		return settlementAmountSe;
	}

	public void setSettlementAmountSe(String settlementAmountSe) {
		this.settlementAmountSe = settlementAmountSe;
	}

	public String getPrincipalWriteOffAmountSe() {
		return principalWriteOffAmountSe;
	}

	public void setPrincipalWriteOffAmountSe(String principalWriteOffAmountSe) {
		this.principalWriteOffAmountSe = principalWriteOffAmountSe;
	}

	public String getCombinedPaymentHistorySe() {
		return combinedPaymentHistorySe;
	}

	public void setCombinedPaymentHistorySe(String combinedPaymentHistorySe) {
		this.combinedPaymentHistorySe = combinedPaymentHistorySe;
	}

	public String getRepaymentTenureSe() {
		return repaymentTenureSe;
	}

	public void setRepaymentTenureSe(String repaymentTenureSe) {
		this.repaymentTenureSe = repaymentTenureSe;
	}

	public String getCashLimitSe() {
		return cashLimitSe;
	}

	public void setCashLimitSe(String cashLimitSe) {
		this.cashLimitSe = cashLimitSe;
	}

	public String getActualPaymentSe() {
		return actualPaymentSe;
	}

	public void setActualPaymentSe(String actualPaymentSe) {
		this.actualPaymentSe = actualPaymentSe;
	}

	public Integer getInterestRateSe() {
		return interestRateSe;
	}

	public void setInterestRateSe(Integer interestRateSe) {
		this.interestRateSe = interestRateSe;
	}

	public String getSuitFiledWilfulDefaultSe() {
		return suitFiledWilfulDefaultSe;
	}

	public void setSuitFiledWilfulDefaultSe(String suitFiledWilfulDefaultSe) {
		this.suitFiledWilfulDefaultSe = suitFiledWilfulDefaultSe;
	}

	public String getWrittenOffSetteldStatusSe() {
		return writtenOffSetteldStatusSe;
	}

	public void setWrittenOffSetteldStatusSe(String writtenOffSetteldStatusSe) {
		this.writtenOffSetteldStatusSe = writtenOffSetteldStatusSe;
	}

	public String getSecurityTypeSe() {
		return securityTypeSe;
	}

	public void setSecurityTypeSe(String securityTypeSe) {
		this.securityTypeSe = securityTypeSe;
	}

	public String getOwnerNameSe() {
		return ownerNameSe;
	}

	public void setOwnerNameSe(String ownerNameSe) {
		this.ownerNameSe = ownerNameSe;
	}

	public String getSecurityValueSe() {
		return securityValueSe;
	}

	public void setSecurityValueSe(String securityValueSe) {
		this.securityValueSe = securityValueSe;
	}

	public Date getDateOfValueSe() {
		return dateOfValueSe;
	}

	public void setDateOfValueSe(Date dateOfValueSe) {
		this.dateOfValueSe = dateOfValueSe;
	}

	public String getSecurityChargeSe() {
		return securityChargeSe;
	}

	public void setSecurityChargeSe(String securityChargeSe) {
		this.securityChargeSe = securityChargeSe;
	}

	public String getPropertyAddressSe() {
		return propertyAddressSe;
	}

	public void setPropertyAddressSe(String propertyAddressSe) {
		this.propertyAddressSe = propertyAddressSe;
	}

	public String getAutomobileTypeSe() {
		return automobileTypeSe;
	}

	public void setAutomobileTypeSe(String automobileTypeSe) {
		this.automobileTypeSe = automobileTypeSe;
	}

	public Integer getYearOfManufactureSe() {
		return yearOfManufactureSe;
	}

	public void setYearOfManufactureSe(Integer yearOfManufactureSe) {
		this.yearOfManufactureSe = yearOfManufactureSe;
	}

	public String getRegistrationNumberSe() {
		return registrationNumberSe;
	}

	public void setRegistrationNumberSe(String registrationNumberSe) {
		this.registrationNumberSe = registrationNumberSe;
	}

	public String getEngineNumberSe() {
		return engineNumberSe;
	}

	public void setEngineNumberSe(String engineNumberSe) {
		this.engineNumberSe = engineNumberSe;
	}

	public String getChassisNumberSe() {
		return chassisNumberSe;
	}

	public void setChassisNumberSe(String chassisNumberSe) {
		this.chassisNumberSe = chassisNumberSe;
	}

	public String getAccountNumberSeLn() {
		return accountNumberSeLn;
	}

	public void setAccountNumberSeLn(String accountNumberSeLn) {
		this.accountNumberSeLn = accountNumberSeLn;
	}

	public String getCreditGuarantorSeLn() {
		return creditGuarantorSeLn;
	}

	public void setCreditGuarantorSeLn(String creditGuarantorSeLn) {
		this.creditGuarantorSeLn = creditGuarantorSeLn;
	}

	public String getAccountTypeSeLn() {
		return accountTypeSeLn;
	}

	public void setAccountTypeSeLn(String accountTypeSeLn) {
		this.accountTypeSeLn = accountTypeSeLn;
	}

	public Date getDateReportedSeLn() {
		return dateReportedSeLn;
	}

	public void setDateReportedSeLn(Date dateReportedSeLn) {
		this.dateReportedSeLn = dateReportedSeLn;
	}

	public String getOwnershipIndicatorSeLn() {
		return ownershipIndicatorSeLn;
	}

	public void setOwnershipIndicatorSeLn(String ownershipIndicatorSeLn) {
		this.ownershipIndicatorSeLn = ownershipIndicatorSeLn;
	}

	public String getAccountStatusSeLn() {
		return accountStatusSeLn;
	}

	public void setAccountStatusSeLn(String accountStatusSeLn) {
		this.accountStatusSeLn = accountStatusSeLn;
	}

	public String getDisbursedAmountSeLn() {
		return disbursedAmountSeLn;
	}

	public void setDisbursedAmountSeLn(String disbursedAmountSeLn) {
		this.disbursedAmountSeLn = disbursedAmountSeLn;
	}

	public Date getDisbursedDateSeLn() {
		return disbursedDateSeLn;
	}

	public void setDisbursedDateSeLn(Date disbursedDateSeLn) {
		this.disbursedDateSeLn = disbursedDateSeLn;
	}

	public Date getLastPaymentDateSeLn() {
		return lastPaymentDateSeLn;
	}

	public void setLastPaymentDateSeLn(Date lastPaymentDateSeLn) {
		this.lastPaymentDateSeLn = lastPaymentDateSeLn;
	}

	public Date getClosedDateSeLn() {
		return closedDateSeLn;
	}

	public void setClosedDateSeLn(Date closedDateSeLn) {
		this.closedDateSeLn = closedDateSeLn;
	}

	public String getInstallmentAmountSeLn() {
		return installmentAmountSeLn;
	}

	public void setInstallmentAmountSeLn(String installmentAmountSeLn) {
		this.installmentAmountSeLn = installmentAmountSeLn;
	}

	public String getWriteOffAmountSeLn() {
		return writeOffAmountSeLn;
	}

	public void setWriteOffAmountSeLn(String writeOffAmountSeLn) {
		this.writeOffAmountSeLn = writeOffAmountSeLn;
	}

	public String getOverdueAmountSeLn() {
		return overdueAmountSeLn;
	}

	public void setOverdueAmountSeLn(String overdueAmountSeLn) {
		this.overdueAmountSeLn = overdueAmountSeLn;
	}

	public String getCurrentBalanceSeLn() {
		return currentBalanceSeLn;
	}

	public void setCurrentBalanceSeLn(String currentBalanceSeLn) {
		this.currentBalanceSeLn = currentBalanceSeLn;
	}

	public String getCreditLimitSeLn() {
		return creditLimitSeLn;
	}

	public void setCreditLimitSeLn(String creditLimitSeLn) {
		this.creditLimitSeLn = creditLimitSeLn;
	}

	public String getAccountRemarksSeLn() {
		return accountRemarksSeLn;
	}

	public void setAccountRemarksSeLn(String accountRemarksSeLn) {
		this.accountRemarksSeLn = accountRemarksSeLn;
	}

	public String getFrequencySeLn() {
		return frequencySeLn;
	}

	public void setFrequencySeLn(String frequencySeLn) {
		this.frequencySeLn = frequencySeLn;
	}

	public String getSecurityStatusSeLn() {
		return securityStatusSeLn;
	}

	public void setSecurityStatusSeLn(String securityStatusSeLn) {
		this.securityStatusSeLn = securityStatusSeLn;
	}

	public Integer getOriginalTermSeLn() {
		return originalTermSeLn;
	}

	public void setOriginalTermSeLn(Integer originalTermSeLn) {
		this.originalTermSeLn = originalTermSeLn;
	}

	public Integer getTermToMaturitySeLn() {
		return termToMaturitySeLn;
	}

	public void setTermToMaturitySeLn(Integer termToMaturitySeLn) {
		this.termToMaturitySeLn = termToMaturitySeLn;
	}

	public String getAccountInDisputeSeLn() {
		return accountInDisputeSeLn;
	}

	public void setAccountInDisputeSeLn(String accountInDisputeSeLn) {
		this.accountInDisputeSeLn = accountInDisputeSeLn;
	}

	public String getSettlementAmtSeLn() {
		return settlementAmtSeLn;
	}

	public void setSettlementAmtSeLn(String settlementAmtSeLn) {
		this.settlementAmtSeLn = settlementAmtSeLn;
	}

	public String getPrincipalWriteOffAmtSeLn() {
		return principalWriteOffAmtSeLn;
	}

	public void setPrincipalWriteOffAmtSeLn(String principalWriteOffAmtSeLn) {
		this.principalWriteOffAmtSeLn = principalWriteOffAmtSeLn;
	}

	public String getRepaymentTenureSeLn() {
		return repaymentTenureSeLn;
	}

	public void setRepaymentTenureSeLn(String repaymentTenureSeLn) {
		this.repaymentTenureSeLn = repaymentTenureSeLn;
	}

	public String getCashLimitSeLn() {
		return cashLimitSeLn;
	}

	public void setCashLimitSeLn(String cashLimitSeLn) {
		this.cashLimitSeLn = cashLimitSeLn;
	}

	public String getActualPaymentSeLn() {
		return actualPaymentSeLn;
	}

	public void setActualPaymentSeLn(String actualPaymentSeLn) {
		this.actualPaymentSeLn = actualPaymentSeLn;
	}

	public Integer getInterestRateSeLn() {
		return interestRateSeLn;
	}

	public void setInterestRateSeLn(Integer interestRateSeLn) {
		this.interestRateSeLn = interestRateSeLn;
	}

	public String getSuitFiledWilfulDefaultSeLn() {
		return suitFiledWilfulDefaultSeLn;
	}

	public void setSuitFiledWilfulDefaultSeLn(String suitFiledWilfulDefaultSeLn) {
		this.suitFiledWilfulDefaultSeLn = suitFiledWilfulDefaultSeLn;
	}

	public String getWrittenOffSetteldStatusSeLn() {
		return writtenOffSetteldStatusSeLn;
	}

	public void setWrittenOffSetteldStatusSeLn(String writtenOffSetteldStatusSeLn) {
		this.writtenOffSetteldStatusSeLn = writtenOffSetteldStatusSeLn;
	}

	public String getSecurityTypeSeLn() {
		return securityTypeSeLn;
	}

	public void setSecurityTypeSeLn(String securityTypeSeLn) {
		this.securityTypeSeLn = securityTypeSeLn;
	}

	public String getOwnerNameSeLn() {
		return ownerNameSeLn;
	}

	public void setOwnerNameSeLn(String ownerNameSeLn) {
		this.ownerNameSeLn = ownerNameSeLn;
	}

	public String getSecurityValueSeLn() {
		return securityValueSeLn;
	}

	public void setSecurityValueSeLn(String securityValueSeLn) {
		this.securityValueSeLn = securityValueSeLn;
	}

	public Date getDateOfValueSeLn() {
		return dateOfValueSeLn;
	}

	public void setDateOfValueSeLn(Date dateOfValueSeLn) {
		this.dateOfValueSeLn = dateOfValueSeLn;
	}

	public String getSecurityChargeSeLn() {
		return securityChargeSeLn;
	}

	public void setSecurityChargeSeLn(String securityChargeSeLn) {
		this.securityChargeSeLn = securityChargeSeLn;
	}

	public String getPropertyAddressSeLn() {
		return propertyAddressSeLn;
	}

	public void setPropertyAddressSeLn(String propertyAddressSeLn) {
		this.propertyAddressSeLn = propertyAddressSeLn;
	}

	public String getAutomobileTypeSeLn() {
		return automobileTypeSeLn;
	}

	public void setAutomobileTypeSeLn(String automobileTypeSeLn) {
		this.automobileTypeSeLn = automobileTypeSeLn;
	}

	public Integer getYearOfManufactureSeLn() {
		return yearOfManufactureSeLn;
	}

	public void setYearOfManufactureSeLn(Integer yearOfManufactureSeLn) {
		this.yearOfManufactureSeLn = yearOfManufactureSeLn;
	}

	public String getRegistrationNumberSeLn() {
		return registrationNumberSeLn;
	}

	public void setRegistrationNumberSeLn(String registrationNumberSeLn) {
		this.registrationNumberSeLn = registrationNumberSeLn;
	}

	public String getEngineNumberSeLn() {
		return engineNumberSeLn;
	}

	public void setEngineNumberSeLn(String engineNumberSeLn) {
		this.engineNumberSeLn = engineNumberSeLn;
	}

	public String getChassisNumberSeLn() {
		return chassisNumberSeLn;
	}

	public void setChassisNumberSeLn(String chassisNumberSeLn) {
		this.chassisNumberSeLn = chassisNumberSeLn;
	}

	public String getScoreType() {
		return scoreType;
	}

	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}

	public String getScoreVersion() {
		return scoreVersion;
	}

	public void setScoreVersion(String scoreVersion) {
		this.scoreVersion = scoreVersion;
	}

	public BigInteger getScoreValue() {
		return scoreValue;
	}

	public void setScoreValue(BigInteger scoreValue) {
		this.scoreValue = scoreValue;
	}

	public String getScoreFactors() {
		return scoreFactors;
	}

	public void setScoreFactors(String scoreFactors) {
		this.scoreFactors = scoreFactors;
	}

	public String getScoreComments() {
		return scoreComments;
	}

	public void setScoreComments(String scoreComments) {
		this.scoreComments = scoreComments;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public Date getInquiryDate() {
		return inquiryDate;
	}

	public void setInquiryDate(Date inquiryDate) {
		this.inquiryDate = inquiryDate;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getOwnershipType() {
		return ownershipType;
	}

	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public String getBureauComment() {
		return bureauComment;
	}

	public void setBureauComment(String bureauComment) {
		this.bureauComment = bureauComment;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public String getAlertDescription() {
		return alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}

	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}

	public String getOutputWritetime() {
		return outputWritetime;
	}

	public void setOutputWritetime(String outputWritetime) {
		this.outputWritetime = outputWritetime;
	}

	public String getOutputReadTime() {
		return outputReadTime;
	}

	public void setOutputReadTime(String outputReadTime) {
		this.outputReadTime = outputReadTime;
	}

	@Override
	public String toString() {
		return "HibHighmarkBaseSropDomain [id=" + id + ", srNo=" + srNo
				+ ", soaSourceName=" + soaSourceName
				+ ", memberReferenceNumber=" + memberReferenceNumber
				+ ", dateOfRequest=" + dateOfRequest + ", preparedFor="
				+ preparedFor + ", preparedForId=" + preparedForId
				+ ", dateOfIssue=" + dateOfIssue + ", reportId=" + reportId
				+ ", batchId=" + batchId + ", status=" + status + ", nameIQ="
				+ nameIQ + ", aka=" + aka + ", spouse=" + spouse + ", father="
				+ father + ", mother=" + mother + ", dobIQ=" + dobIQ + ", age="
				+ age + ", ageAsOn=" + ageAsOn + ", rationCard=" + rationCard
				+ ", passport=" + passport + ", votersId=" + votersId
				+ ", drivingLicenceNo=" + drivingLicenceNo + ", pan=" + pan
				+ ", gender=" + gender + ", ownership=" + ownership
				+ ", address1=" + address1 + ", address2=" + address2
				+ ", address3=" + address3 + ", phone1=" + phone1 + ", phone2="
				+ phone2 + ", phone3=" + phone3 + ", emailId1=" + emailId1
				+ ", emailId2=" + emailId2 + ", branch=" + branch + ", kendra="
				+ kendra + ", mbrId=" + mbrId + ", losAppId=" + losAppId
				+ ", creditInquiryPurposeType=" + creditInquiryPurposeType
				+ ", creditInquiryPurposeTypeDesc="
				+ creditInquiryPurposeTypeDesc + ", creditInquiryStage="
				+ creditInquiryStage + ", creditReportId=" + creditReportId
				+ ", creditRequestType=" + creditRequestType
				+ ", creditReportTransectionDateTime="
				+ creditReportTransectionDateTime + ", accountOpenDate="
				+ accountOpenDate + ", loanAmount=" + loanAmount
				+ ", entityId=" + entityId + ", primaryNumberOfAccount="
				+ primaryNumberOfAccount + ", primaryActiveNoOfAccount="
				+ primaryActiveNoOfAccount + ", primaryOverdueNumberOfAccount="
				+ primaryOverdueNumberOfAccount + ", primaryCurrentBalance="
				+ primaryCurrentBalance + ", primarySanctionedAmount="
				+ primarySanctionedAmount + ", primaryDisbursedAmount="
				+ primaryDisbursedAmount + ", primarySecuredNumberOfAccount="
				+ primarySecuredNumberOfAccount
				+ ", primaryUnsecuredNumberOfAccount="
				+ primaryUnsecuredNumberOfAccount
				+ ", primaryUntaggedNumberOfAccount="
				+ primaryUntaggedNumberOfAccount
				+ ", secondaryVNumberOfAccount=" + secondaryVNumberOfAccount
				+ ", secondaryActiveNumberOfAccount="
				+ secondaryActiveNumberOfAccount
				+ ", secondaryOverdueNumberOfAccount="
				+ secondaryOverdueNumberOfAccount
				+ ", secondaryCurrentBalance=" + secondaryCurrentBalance
				+ ", secondarySanctionedAmount=" + secondarySanctionedAmount
				+ ", secondaryDisbursedAmount=" + secondaryDisbursedAmount
				+ ", secondarySecuredNumberOfAccount="
				+ secondarySecuredNumberOfAccount
				+ ", secondaryUnsecuredNumberOfAccount="
				+ secondaryUnsecuredNumberOfAccount
				+ ", secondaryUntaggedNumberOfAccount="
				+ secondaryUntaggedNumberOfAccount
				+ ", inquriesInLastSixMonth=" + inquriesInLastSixMonth
				+ ", lengthOfCreditHistoryYear=" + lengthOfCreditHistoryYear
				+ ", lengthOfCreditHistoryMonth=" + lengthOfCreditHistoryMonth
				+ ", averageAccountAgeYear=" + averageAccountAgeYear
				+ ", averageAccountAgeMonth=" + averageAccountAgeMonth
				+ ", newAccountInLastSixMonth=" + newAccountInLastSixMonth
				+ ", newDelinqAccountInLastSixMonth="
				+ newDelinqAccountInLastSixMonth + ", nameVar=" + nameVar
				+ ", nameVarDate=" + nameVarDate + ", addressVar=" + addressVar
				+ ", addressVarDate=" + addressVarDate + ", panVar=" + panVar
				+ ", panVarDate=" + panVarDate + ", dlVar=" + dlVar
				+ ", dlVarDate=" + dlVarDate + ", dobVar=" + dobVar
				+ ", dobVarDate=" + dobVarDate + ", voterVar=" + voterVar
				+ ", voterVarDate=" + voterVarDate + ", passportVar="
				+ passportVar + ", passportVarDate=" + passportVarDate
				+ ", phoneVar=" + phoneVar + ", phoneVarDate=" + phoneVarDate
				+ ", rationVar=" + rationVar + ", rationVarDate="
				+ rationVarDate + ", emailVar=" + emailVar + ", emailVarDate="
				+ emailVarDate + ", matchedType=" + matchedType
				+ ", accountNumber=" + accountNumber + ", creditGuarantor="
				+ creditGuarantor + ", acctType=" + acctType
				+ ", dateReported=" + dateReported + ", ownershipIndicator="
				+ ownershipIndicator + ", accountStatus=" + accountStatus
				+ ", disbursedAmount=" + disbursedAmount + ", disbursedDate="
				+ disbursedDate + ", lastPaymentDate=" + lastPaymentDate
				+ ", closedDate=" + closedDate + ", installmentAmount="
				+ installmentAmount + ", overdueAmount=" + overdueAmount
				+ ", writeOffAmount=" + writeOffAmount + ", currentBalance="
				+ currentBalance + ", creditLimit=" + creditLimit
				+ ", accountRemarks=" + accountRemarks + ", frequency="
				+ frequency + ", securityStatus=" + securityStatus
				+ ", originalTerm=" + originalTerm + ", termToMaturity="
				+ termToMaturity + ", accountInDispute=" + accountInDispute
				+ ", settlementAmount=" + settlementAmount
				+ ", principalWriteOffAmount=" + principalWriteOffAmount
				+ ", combinedPaymentHistory=" + combinedPaymentHistory
				+ ", repaymentTenure=" + repaymentTenure + ", cashLimit="
				+ cashLimit + ", actualPayment=" + actualPayment
				+ ", interestRate=" + interestRate
				+ ", suitFiledWilfulDefault=" + suitFiledWilfulDefault
				+ ", writtenOffSetteldStatus=" + writtenOffSetteldStatus
				+ ", securityType=" + securityType + ", ownerName=" + ownerName
				+ ", securityValue=" + securityValue + ", dateOfValue="
				+ dateOfValue + ", securityCharge=" + securityCharge
				+ ", propertyAddress=" + propertyAddress + ", automobileType="
				+ automobileType + ", yearOfManufacture=" + yearOfManufacture
				+ ", registrationNumber=" + registrationNumber
				+ ", engineNumber=" + engineNumber + ", chassisNumber="
				+ chassisNumber + ", accountNumberLn=" + accountNumberLn
				+ ", creditGuarantorLn=" + creditGuarantorLn
				+ ", accountTypeLn=" + accountTypeLn + ", dateReportedLn="
				+ dateReportedLn + ", ownershipIndicatorLn="
				+ ownershipIndicatorLn + ", accountStatusLn=" + accountStatusLn
				+ ", disbursedAmountLn=" + disbursedAmountLn
				+ ", disbursedDateLn=" + disbursedDateLn
				+ ", lastPaymentDateLn=" + lastPaymentDateLn
				+ ", closedDateLn=" + closedDateLn + ", installmentAmountLn="
				+ installmentAmountLn + ", writeOffAmountLn="
				+ writeOffAmountLn + ", overdueAmountLn=" + overdueAmountLn
				+ ", currentBalanceLn=" + currentBalanceLn + ", creditLimitLn="
				+ creditLimitLn + ", accountRemarksLn=" + accountRemarksLn
				+ ", frequencyLn=" + frequencyLn + ", securityStatusLn="
				+ securityStatusLn + ", originalTermLn=" + originalTermLn
				+ ", termToMaturityLn=" + termToMaturityLn
				+ ", accountInDisputeLn=" + accountInDisputeLn
				+ ", settlementAmtLn=" + settlementAmtLn
				+ ", principalWriteOffAmtLn=" + principalWriteOffAmtLn
				+ ", repaymentTenureLn=" + repaymentTenureLn + ", cashLimitLn="
				+ cashLimitLn + ", actualPaymentLn=" + actualPaymentLn
				+ ", interestRateLn=" + interestRateLn
				+ ", suitFiledWilfulDefaultLn=" + suitFiledWilfulDefaultLn
				+ ", writtenOffSetteldStatusLn=" + writtenOffSetteldStatusLn
				+ ", securityTypeLn=" + securityTypeLn + ", ownerNameLn="
				+ ownerNameLn + ", securityValueLn=" + securityValueLn
				+ ", dateOfValueLn=" + dateOfValueLn + ", securityChargeLn="
				+ securityChargeLn + ", propertyAddressLn=" + propertyAddressLn
				+ ", automobileTypeLn=" + automobileTypeLn
				+ ", yearOfManufactureLn=" + yearOfManufactureLn
				+ ", registrationNumberLn=" + registrationNumberLn
				+ ", engineNumberLn=" + engineNumberLn + ", chassisNumberLn="
				+ chassisNumberLn + ", nameIndtl=" + nameIndtl + ", address="
				+ address + ", dobIndtl=" + dobIndtl + ", phoneIndtl="
				+ phoneIndtl + ", panIndtl=" + panIndtl + ", passportIndtl="
				+ passportIndtl + ", drivingLicenseIndtl="
				+ drivingLicenseIndtl + ", voterIdIndtl=" + voterIdIndtl
				+ ", emailIndtl=" + emailIndtl + ", rationCardIndtl="
				+ rationCardIndtl + ", matchedTypeSe=" + matchedTypeSe
				+ ", accountNumberSe=" + accountNumberSe
				+ ", creditGuarantorSe=" + creditGuarantorSe + ", acctTypeSe="
				+ acctTypeSe + ", dateReportedSe=" + dateReportedSe
				+ ", ownershipIndicatorSe=" + ownershipIndicatorSe
				+ ", accountStatusSe=" + accountStatusSe
				+ ", disbursedAmountSe=" + disbursedAmountSe
				+ ", disbursedDateSe=" + disbursedDateSe
				+ ", lastPaymentDateSe=" + lastPaymentDateSe
				+ ", closedDateSe=" + closedDateSe + ", installmentAmountSe="
				+ installmentAmountSe + ", overdueAmountSe=" + overdueAmountSe
				+ ", writeOffAmountSe=" + writeOffAmountSe
				+ ", currentBalanceSe=" + currentBalanceSe + ", creditLimitSe="
				+ creditLimitSe + ", accountRemarksSe=" + accountRemarksSe
				+ ", frequencySe=" + frequencySe + ", securityStatusSe="
				+ securityStatusSe + ", originalTermSe=" + originalTermSe
				+ ", termToMaturitySe=" + termToMaturitySe
				+ ", accountInDisputeSe=" + accountInDisputeSe
				+ ", settlementAmountSe=" + settlementAmountSe
				+ ", principalWriteOffAmountSe=" + principalWriteOffAmountSe
				+ ", combinedPaymentHistorySe=" + combinedPaymentHistorySe
				+ ", repaymentTenureSe=" + repaymentTenureSe + ", cashLimitSe="
				+ cashLimitSe + ", actualPaymentSe=" + actualPaymentSe
				+ ", interestRateSe=" + interestRateSe
				+ ", suitFiledWilfulDefaultSe=" + suitFiledWilfulDefaultSe
				+ ", writtenOffSetteldStatusSe=" + writtenOffSetteldStatusSe
				+ ", securityTypeSe=" + securityTypeSe + ", ownerNameSe="
				+ ownerNameSe + ", securityValueSe=" + securityValueSe
				+ ", dateOfValueSe=" + dateOfValueSe + ", securityChargeSe="
				+ securityChargeSe + ", propertyAddressSe=" + propertyAddressSe
				+ ", automobileTypeSe=" + automobileTypeSe
				+ ", yearOfManufactureSe=" + yearOfManufactureSe
				+ ", registrationNumberSe=" + registrationNumberSe
				+ ", engineNumberSe=" + engineNumberSe + ", chassisNumberSe="
				+ chassisNumberSe + ", accountNumberSeLn=" + accountNumberSeLn
				+ ", creditGuarantorSeLn=" + creditGuarantorSeLn
				+ ", accountTypeSeLn=" + accountTypeSeLn
				+ ", dateReportedSeLn=" + dateReportedSeLn
				+ ", ownershipIndicatorSeLn=" + ownershipIndicatorSeLn
				+ ", accountStatusSeLn=" + accountStatusSeLn
				+ ", disbursedAmountSeLn=" + disbursedAmountSeLn
				+ ", disbursedDateSeLn=" + disbursedDateSeLn
				+ ", lastPaymentDateSeLn=" + lastPaymentDateSeLn
				+ ", closedDateSeLn=" + closedDateSeLn
				+ ", installmentAmountSeLn=" + installmentAmountSeLn
				+ ", writeOffAmountSeLn=" + writeOffAmountSeLn
				+ ", overdueAmountSeLn=" + overdueAmountSeLn
				+ ", currentBalanceSeLn=" + currentBalanceSeLn
				+ ", creditLimitSeLn=" + creditLimitSeLn
				+ ", accountRemarksSeLn=" + accountRemarksSeLn
				+ ", frequencySeLn=" + frequencySeLn + ", securityStatusSeLn="
				+ securityStatusSeLn + ", originalTermSeLn=" + originalTermSeLn
				+ ", termToMaturitySeLn=" + termToMaturitySeLn
				+ ", accountInDisputeSeLn=" + accountInDisputeSeLn
				+ ", settlementAmtSeLn=" + settlementAmtSeLn
				+ ", principalWriteOffAmtSeLn=" + principalWriteOffAmtSeLn
				+ ", repaymentTenureSeLn=" + repaymentTenureSeLn
				+ ", cashLimitSeLn=" + cashLimitSeLn + ", actualPaymentSeLn="
				+ actualPaymentSeLn + ", interestRateSeLn=" + interestRateSeLn
				+ ", suitFiledWilfulDefaultSeLn=" + suitFiledWilfulDefaultSeLn
				+ ", writtenOffSetteldStatusSeLn="
				+ writtenOffSetteldStatusSeLn + ", securityTypeSeLn="
				+ securityTypeSeLn + ", ownerNameSeLn=" + ownerNameSeLn
				+ ", securityValueSeLn=" + securityValueSeLn
				+ ", dateOfValueSeLn=" + dateOfValueSeLn
				+ ", securityChargeSeLn=" + securityChargeSeLn
				+ ", propertyAddressSeLn=" + propertyAddressSeLn
				+ ", automobileTypeSeLn=" + automobileTypeSeLn
				+ ", yearOfManufactureSeLn=" + yearOfManufactureSeLn
				+ ", registrationNumberSeLn=" + registrationNumberSeLn
				+ ", engineNumberSeLn=" + engineNumberSeLn
				+ ", chassisNumberSeLn=" + chassisNumberSeLn + ", scoreType="
				+ scoreType + ", scoreVersion=" + scoreVersion
				+ ", scoreValue=" + scoreValue + ", scoreFactors="
				+ scoreFactors + ", scoreComments=" + scoreComments
				+ ", memberName=" + memberName + ", inquiryDate=" + inquiryDate
				+ ", purpose=" + purpose + ", ownershipType=" + ownershipType
				+ ", amount=" + amount + ", remark=" + remark
				+ ", commentText=" + commentText + ", commentDate="
				+ commentDate + ", bureauComment=" + bureauComment
				+ ", alertType=" + alertType + ", alertDescription="
				+ alertDescription + ", outputWriteFlag=" + outputWriteFlag
				+ ", outputWritetime=" + outputWritetime + ", outputReadTime="
				+ outputReadTime + "]";
	}

	

}
