package com.softcell.gonogo.model.mbdatapush.chm;

public class SecondaryAccountSummary {

	
	private String secondaryNumberOfAccounts;
	private String secondaryActiveNumberOfAccounts;
	private String secondaryOverdueNumberOfAccounts;
	private String secondarySecuredNumberOfAccounts;
	private String secondaryUnsecuredNumberOfAccounts;
	private String secondaryUntaggedNumberOfAccounts;
	private String secondaryCurrentBalance;
	private String secondarySanctionedAmount;
	private String secondaryDisbursedAmount;
	
	public String getSecondaryNumberOfAccounts() {
		return secondaryNumberOfAccounts;
	}
	public void setSecondaryNumberOfAccounts(String secondaryNumberOfAccounts) {
		this.secondaryNumberOfAccounts = secondaryNumberOfAccounts;
	}
	public String getSecondaryActiveNumberOfAccounts() {
		return secondaryActiveNumberOfAccounts;
	}
	public void setSecondaryActiveNumberOfAccounts(
			String secondaryActiveNumberOfAccounts) {
		this.secondaryActiveNumberOfAccounts = secondaryActiveNumberOfAccounts;
	}
	public String getSecondaryOverdueNumberOfAccounts() {
		return secondaryOverdueNumberOfAccounts;
	}
	public void setSecondaryOverdueNumberOfAccounts(
			String secondaryOverdueNumberOfAccounts) {
		this.secondaryOverdueNumberOfAccounts = secondaryOverdueNumberOfAccounts;
	}
	public String getSecondarySecuredNumberOfAccounts() {
		return secondarySecuredNumberOfAccounts;
	}
	public void setSecondarySecuredNumberOfAccounts(
			String secondarySecuredNumberOfAccounts) {
		this.secondarySecuredNumberOfAccounts = secondarySecuredNumberOfAccounts;
	}
	public String getSecondaryUnsecuredNumberOfAccounts() {
		return secondaryUnsecuredNumberOfAccounts;
	}
	public void setSecondaryUnsecuredNumberOfAccounts(
			String secondaryUnsecuredNumberOfAccounts) {
		this.secondaryUnsecuredNumberOfAccounts = secondaryUnsecuredNumberOfAccounts;
	}
	public String getSecondaryUntaggedNumberOfAccounts() {
		return secondaryUntaggedNumberOfAccounts;
	}
	public void setSecondaryUntaggedNumberOfAccounts(
			String secondaryUntaggedNumberOfAccounts) {
		this.secondaryUntaggedNumberOfAccounts = secondaryUntaggedNumberOfAccounts;
	}
	public String getSecondaryCurrentBalance() {
		return secondaryCurrentBalance;
	}
	public void setSecondaryCurrentBalance(String secondaryCurrentBalance) {
		this.secondaryCurrentBalance = secondaryCurrentBalance;
	}
	public String getSecondarySanctionedAmount() {
		return secondarySanctionedAmount;
	}
	public void setSecondarySanctionedAmount(String secondarySanctionedAmount) {
		this.secondarySanctionedAmount = secondarySanctionedAmount;
	}
	public String getSecondaryDisbursedAmount() {
		return secondaryDisbursedAmount;
	}
	public void setSecondaryDisbursedAmount(String secondaryDisbursedAmount) {
		this.secondaryDisbursedAmount = secondaryDisbursedAmount;
	}
	@Override
	public String toString() {
		return "SecondaryAccountSummary [secondaryNumberOfAccounts="
				+ secondaryNumberOfAccounts
				+ ", secondaryActiveNumberOfAccounts="
				+ secondaryActiveNumberOfAccounts
				+ ", secondaryOverdueNumberOfAccounts="
				+ secondaryOverdueNumberOfAccounts
				+ ", secondarySecuredNumberOfAccounts="
				+ secondarySecuredNumberOfAccounts
				+ ", secondaryUnsecuredNumberOfAccounts="
				+ secondaryUnsecuredNumberOfAccounts
				+ ", secondaryUntaggedNumberOfAccounts="
				+ secondaryUntaggedNumberOfAccounts
				+ ", secondaryCurrentBalance=" + secondaryCurrentBalance
				+ ", secondarySanctionedAmount=" + secondarySanctionedAmount
				+ ", secondaryDisbursedAmount=" + secondaryDisbursedAmount
				+ "]";
	}
	
	
}
