package com.softcell.gonogo.model.mbdatapush.chm;


public class Response {
	
	private LoanDetails loanDetail;

	public LoanDetails getLoanDetail() {
		return loanDetail;
	}

	public void setLoanDetail(LoanDetails loanDetail) {
		this.loanDetail = loanDetail;
	}

	@Override
	public String toString() {
		return "Response [loanDetail=" + loanDetail + "]";
	}

}
