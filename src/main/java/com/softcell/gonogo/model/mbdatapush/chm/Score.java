package com.softcell.gonogo.model.mbdatapush.chm;

public class Score {
	private String scoreType;	
	private String scoreVersion;
	private String scoreValue;
	private String scoreFactors;
	private String scoreComments;
	public String getScoreType() {
		return scoreType;
	}
	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}
	public String getScoreVersion() {
		return scoreVersion;
	}
	public void setScoreVersion(String scoreVersion) {
		this.scoreVersion = scoreVersion;
	}
	public String getScoreValue() {
		return scoreValue;
	}
	public void setScoreValue(String scoreValue) {
		this.scoreValue = scoreValue;
	}
	public String getScoreFactors() {
		return scoreFactors;
	}
	public void setScoreFactors(String scoreFactors) {
		this.scoreFactors = scoreFactors;
	}
	public String getScoreComments() {
		return scoreComments;
	}
	public void setScoreComments(String scoreComments) {
		this.scoreComments = scoreComments;
	}
	@Override
	public String toString() {
		return "Score [scoreType=" + scoreType + ", scoreVersion="
				+ scoreVersion + ", scoreValue=" + scoreValue
				+ ", scoreFactors=" + scoreFactors + ", scoreComments="
				+ scoreComments + "]";
	}
}
