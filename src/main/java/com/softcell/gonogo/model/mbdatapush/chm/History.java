package com.softcell.gonogo.model.mbdatapush.chm;

public class History {
	
	private String  memberName;
	private String 	inquiryDate;
	private String 	purpose;
	private String 	ownershipType;
	private String 	amount;
	private String 	remark;
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getInquiryDate() {
		return inquiryDate;
	}
	public void setInquiryDate(String inquiryDate) {
		this.inquiryDate = inquiryDate;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getOwnershipType() {
		return ownershipType;
	}
	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "History [memberName=" + memberName + ", inquiryDate="
				+ inquiryDate + ", purpose=" + purpose + ", ownershipType="
				+ ownershipType + ", amount=" + amount + ", remark=" + remark
				+ "]";
	}
	
	


}
