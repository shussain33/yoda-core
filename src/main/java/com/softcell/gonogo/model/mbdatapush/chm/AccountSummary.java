package com.softcell.gonogo.model.mbdatapush.chm;

public class AccountSummary {
	
	private DerivedAttributes drivedAttributes=null;
	private PrimaryAccountsSummary primaryAccountsSummary=null;
	private SecondaryAccountSummary secondaryAccountSummary=null;
	
	
	public DerivedAttributes getDrivedAttributes() {
		return drivedAttributes;
	}
	public void setDrivedAttributes(DerivedAttributes drivedAttributes) {
		this.drivedAttributes = drivedAttributes;
	}
	public PrimaryAccountsSummary getPrimaryAccountsSummary() {
		return primaryAccountsSummary;
	}
	public void setPrimaryAccountsSummary(PrimaryAccountsSummary primaryAccountSummary) {
		this.primaryAccountsSummary = primaryAccountSummary;
	}
	public SecondaryAccountSummary getSecondaryAccountSummary() {
		return secondaryAccountSummary;
	}
	public void setSecondaryAccountSummary(
			SecondaryAccountSummary secondaryAccountSummary) {
		this.secondaryAccountSummary = secondaryAccountSummary;
	}

	@Override
	public String toString() {
		return "AccountSummary [drivedAttributes=" + drivedAttributes
				+ ", primaryAccountsSummary=" + primaryAccountsSummary
				+ ", secondaryAccountSummary=" + secondaryAccountSummary + "]";
	}
}
