/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;


/**
 * @author Dipak
 * 
 */
public class HibEquifaxSropDomain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	@Expose
	@SerializedName("SRNO")
	private Integer srNo;
	@Expose
	@SerializedName("SOA_SOURCE_NAME")
	private String soaSourceName;
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private String memberReferenceNumber;
	@Expose
	@SerializedName("ENQUIRY_DATE")
	private Date enquiryDate;
	@Expose
	@SerializedName("CUSTOMERCODE")
	private String customerCode;
	@Expose
	@SerializedName("FIRSTNAME_NM_TP")
	private String firstnameNmTp;
	@Expose
	@SerializedName("MIDLLENAME_NM_TP")
	private String midlleNameNmTp;
	@Expose
	@SerializedName("LASTNAME_NM_TP")
	private String lastNameNmTp;
	@Expose
	@SerializedName("ADDMIDLLENAME_NM_TP")
	private String addMidlleNameNmTp;
	@Expose
	@SerializedName("SUFFIX_NM_TP")
	private String suffixNmTp;
	@Expose
	@SerializedName("REPORTEDDATE_AG_IF")
	private String reportedDateAgIf;
	@Expose
	@SerializedName("AGE_AG_IF")
	private String ageAgIf;
	@Expose
	@SerializedName("REPORTEDDATE_AL_NM")
	private String reportedDateAlNm;
	@Expose
	@SerializedName("ALIASNAME_AL_NM")
	private String aliasNameAlNm;
	@Expose
	@SerializedName("ADDITIONALNAMEINFO_AD_NM_IF")
	private String additionalNameInfoAdNmIf;
	@Expose
	@SerializedName("NOOFDEPENDENTS_AD_NM_IF")
	private String noOfDependentsAdNmIf;
	@Expose
	@SerializedName("REPORTEDDATE_RS_ADD")
	private String reportedDateRsAdd;
	@Expose
	@SerializedName("ADDRESS_RS_ADD")
	private String addressRsAdd;
	@Expose
	@SerializedName("STATE_RS_ADD")
	private String stateRsAdd;
	@Expose
	@SerializedName("POSTAL_RS_ADD")
	private String postalRsAdd;
	@Expose
	@SerializedName("ADDTYPE_RS_ADD")
	private String addTypeRsAdd;
	@Expose
	@SerializedName("IDTYPE_RS_ID")
	private String idTypeRsId;
	@Expose
	@SerializedName("REPORTEDDATE_RS_ID")
	private String reportedDateRsId;
	@Expose
	@SerializedName("IDNUMBER_RS_ID")
	private String idNumberRsId;
	@Expose
	@SerializedName("EMAILREPORTEDDATE_RS_ML")
	private String emailReportedDateRsMl;
	@Expose
	@SerializedName("EMAILADDRESS_RS_ML")
	private String emailAddressRsMl;
	@Expose
	@SerializedName("EMPNMREPDATE_RS_EMDL")
	private String empNMRepDateRsEMDL;
	@Expose
	@SerializedName("EMPLOYERNAME_RS_EMDL")
	private String employerNameRsEmdl;
	@Expose
	@SerializedName("EMPPOSITION_RS_EMDL")
	private String empPositionRsEmdl;
	@Expose
	@SerializedName("EMPADDRESS_RS_EMDL")
	private String empAddressRsEmdl;
	@Expose
	@SerializedName("DOB_RS_PER_IF")
	private String dobRsPerIf;
	@Expose
	@SerializedName("GENDER_RS_PER_IF")
	private String genderRsPerIf;
	@Expose
	@SerializedName("TOTALINCOME_RS_PER_IF")
	private String totalIncomeRsPerIf;
	@Expose
	@SerializedName("OCCUPATION_RS_PER_IF")
	private String occupationRsPerIf;
	@Expose
	@SerializedName("MARITALSTAUS_RS_PER_IF")
	private String maritalStausRsPerIf;
	@Expose
	@SerializedName("OCCUPATION_IC_DL")
	private String occupationIcDl;
	@Expose
	@SerializedName("MONTHLYINCOME_IC_DL")
	private String monthlyIncomeIcDl;
	@Expose
	@SerializedName("MONTHLYEXPENSE_IC_DL")
	private String monthlyExpenseIcDl;
	@Expose
	@SerializedName("POVERTYINDEX_IC_DL")
	private String povertyIndexIcDl;
	@Expose
	@SerializedName("ASSETOWNERSHIP_IC_DL")
	private String assetOwnershipIcDl;
	@Expose
	@SerializedName("REPORTEDDATE_RS_PH")
	private String reportedDateRsPh;
	@Expose
	@SerializedName("TYPECODE_RS_PH")
	private String typeCodeRsPh;
	@Expose
	@SerializedName("COUNTRYCODE_RS_PH")
	private String countryCodeRsPh;
	@Expose
	@SerializedName("AREACODE_RS_PH")
	private String areaCodeRsPh;
	@Expose
	@SerializedName("PHNUMBER_RS_PH")
	private String phNumberRsPh;
	@Expose
	@SerializedName("PHNOEXTENTION_RS_PH")
	private String phNoExtentionRsPh;
	@Expose
	@SerializedName("CUSTOMERCODE_RS_HD")
	private String customercodeRsHd;
	@Expose
	@SerializedName("CLIENTID_RS_HD")
	private String clientIdRsHd;
	@Expose
	@SerializedName("CUSTREFFIELD_RS_HD")
	private String custRefFieldRsHd;
	@Expose
	@SerializedName("REPORTORDERNO_RS_HD")
	private String reportOrderNoRsHd;
	@Expose
	@SerializedName("PRODUCTCODE_RS_HD")
	private String productCodeRsHd;
	@Expose
	@SerializedName("PRODUCTVERSION_RS_HD")
	private String productVersionRsHd;
	@Expose
	@SerializedName("SUCCESSCODE_RS_HD")
	private String successCodeRsHd;
	@Expose
	@SerializedName("MATCHTYPE_RS_HD")
	private String matchTypeRsHd;
	@Expose
	@SerializedName("RESDATE_RS_HD")
	private String resDateRsHd;
	@Expose
	@SerializedName("RESTIME_RS_HD")
	private String resTimeRsHd;
	@Expose
	@SerializedName("REPORTEDDATE_RS_AC")
	private String reportedDateRsAc;
	@Expose
	@SerializedName("CLIENTNAME_RS_AC")
	private String clientNameRsAc;
	@Expose
	@SerializedName("ACCOUNTNUMBER_RS_AC")
	private String accountNumberRsAc;
	@Expose
	@SerializedName("CURRENTBALANCE_RS_AC")
	private String currentBalanceRsAc;
	@Expose
	@SerializedName("INSTITUTION_RS_AC")
	private String institutionRsAc;
	@Expose
	@SerializedName("ACCOUNTTYPE_RS_AC")
	private String accountTypeRsAc;
	@Expose
	@SerializedName("OWNERSHIPTYPE_RS_AC")
	private String ownershipTypeRsAc;
	@Expose
	@SerializedName("BALANCE_RS_AC")
	private Integer balanceRsAc;
	@Expose
	@SerializedName("PASTDUEAMT_RS_AC")
	private Integer pastDueAmtRsAc;
	@Expose
	@SerializedName("DISBURSEDAMOUNT_RS_AC")
	private String disbursedAmountRsAc;
	@Expose
	@SerializedName("LOANCATEGORY_RS_AC")
	private String loanCategoryRsAc;
	@Expose
	@SerializedName("LOANPURPOSE_RS_AC")
	private String loanPurposeRsAc;
	@Expose
	@SerializedName("LASTPAYMENT_RS_AC")
	private Integer lastPaymentRsAc;
	@Expose
	@SerializedName("WRITEOFFAMT_RS_AC")
	private Integer writeOffAmtRsAc;
	@Expose
	@SerializedName("ACCOPEN_RS_AC")
	private String accOpenRsAc;
	@Expose
	@SerializedName("SACTIONAMT_RS_AC")
	private Integer sactionAmtRsAc;
	@Expose
	@SerializedName("LASTPAYMENTDATE_RS_AC")
	private String lastPaymentDateRsAc;
	@Expose
	@SerializedName("HIGHCREDIT_RS_AC")
	private Integer highCreditRsAc;
	@Expose
	@SerializedName("DATEREPORTED_RS_AC")
	private String dateReportedRsAc;
	@Expose
	@SerializedName("DATEOPENED_RS_AC")
	private String dateOpenedRsAc;
	@Expose
	@SerializedName("DATECLOSED_RS_AC")
	private String dateClosedRsAc;
	@Expose
	@SerializedName("REASON_RS_AC")
	private String reasonRsAc;
	@Expose
	@SerializedName("DATEWRITTENOFF_RS_AC")
	private String dateWrittenOffRsAc;
	@Expose
	@SerializedName("LOANCYCLEID_RS_AC")
	private String loanCycleIdRsAc;
	@Expose
	@SerializedName("DATESANCTIONED_RS_AC")
	private String dateSanctionedRsAc;
	@Expose
	@SerializedName("DATEAPPLIED_RS_AC")
	private String dateAppliedRsAc;
	@Expose
	@SerializedName("INTRESTRATE_RS_AC")
	private String intrestRateRSAC;
	@Expose
	@SerializedName("APPLIEDAMOUNT_RS_AC")
	private String appliedAmountRsAc;
	@Expose
	@SerializedName("NOOFINSTALLMENTS_RS_AC")
	private String noOfInstallmentsRsAc;
	@Expose
	@SerializedName("REPAYMENTTENURE_RS_AC")
	private String repaymentTenureRsAc;
	@Expose
	@SerializedName("DISPUTECODE_RS_AC")
	private String disputeCodeRsAc;
	@Expose
	@SerializedName("INSTALLMENTAMT_RS_AC")
	private String installmentAmtRsAc;
	@Expose
	@SerializedName("KEYPERSON_RS_AC")
	private String keyPersonRsAc;
	@Expose
	@SerializedName("NOMINEE_RS_AC")
	private String nomineeRsAc;
	@Expose
	@SerializedName("TERMFREQUENCY_RS_AC")
	private String termFrequencyRsAc;
	@Expose
	@SerializedName("CREDITLIMIT_RS_AC")
	private Integer creditLimitRsAc;
	@Expose
	@SerializedName("COLLATERALVALUE_RS_AC")
	private String collateralValueRsAc;
	@Expose
	@SerializedName("COLLATERALTYPE_RS_AC")
	private String collateralTypeRsAc;
	@Expose
	@SerializedName("ACCOUNTSTATUS_RS_AC")
	private String accountStatusRsAc;
	@Expose
	@SerializedName("ASSETCLASSIFICATION_RS_AC")
	private String assetClassificationRsAc;
	@Expose
	@SerializedName("SUITFILEDSTATUS_RS_AC")
	private String suitFiledStatusRsAc;
	@Expose
	@SerializedName("MONTHKEY_RS_AC_HIS24")
	private String monthKeyRsAcHis24;
	@Expose
	@SerializedName("PAYMENTSTATUS_RS_AC_HIS24")
	private String paymentStatusRsAcHis24;
	@Expose
	@SerializedName("SUITFILEDSTATUS_RS_AC_HIS24")
	private String suitFiledStatusRsAcHis24;
	@Expose
	@SerializedName("ASSETTCLSSTATUS_RS_AC_HIS24")
	private String assettClsStatusRsAcHis24;
	@Expose
	@SerializedName("MONTHKEY_RS_AC_HIS48")
	private String monthKeyRsAcHis48;
	@Expose
	@SerializedName("PAYMENTSTATUS_RS_AC_HIS48")
	private String pAYMENTSTATUSRsAcHis48;
	@Expose
	@SerializedName("SUITFILEDSTATUS_RS_AC_HIS48")
	private String sUITFILEDSTATUSRsAcHis48;
	@Expose
	@SerializedName("ASSETCLSSTATUS_RS_AC_HIS48")
	private String aSSETCLSSTATUSRsAcHis48;
	@Expose
	@SerializedName("NOOFACCOUNTS_RS_AC_SUM")
	private Integer noOfAccountsRsAcSum;
	@Expose
	@SerializedName("NOOFACTIVEACCOUNTS_RS_AC_SUM")
	private Integer noOfActiveAccountsRsAcSum;
	@Expose
	@SerializedName("NOOFWRITEOFFS_RS_AC_SUM")
	private Integer noOfWriteOffsRsAcSum;
	@Expose
	@SerializedName("TOTALPASTDUE_RS_AC_SUM")
	private Integer totalPastDueRsAcSum;
	@Expose
	@SerializedName("MOSTSRVRSTAWHIN24MON_RS_AC_SUM")
	private String mostSrvrStaWhin24MonRsAcSum;
	@Expose
	@SerializedName("SINGLEHIGHESTCREDIT_RS_AC_SUM")
	private Integer singleHighestCreditRsAcSum;
	@Expose
	@SerializedName("SINGLEHIGHSANCTAMT_RS_AC_SUM")
	private Integer singleHighSanctAmtRsAcSum;
	@Expose
	@SerializedName("TOTALHIGHCREDIT_RS_AC_SUM")
	private Integer totalHighCreditRsAcSum;
	@Expose
	@SerializedName("AVERAGEOPENBALANCE_RS_AC_SUM")
	private Integer averageOpenBalanceRsAcSum;
	@Expose
	@SerializedName("SINGLEHIGHESTBALANCE_RS_AC_SUM")
	private Integer singleHighestBalanceRsAcSum;
	@Expose
	@SerializedName("NOOFPASTDUEACCTS_RS_AC_SUM")
	private Integer noOfPastDueAcctsRsAcSum;
	@Expose
	@SerializedName("NOOFZEROBALACCTS_RS_AC_SUM")
	private Integer noOfZeroBalAcctsRsAcSum;
	@Expose
	@SerializedName("RECENTACCOUNT_RS_AC_SUM")
	private String recentAccountRsAcSum;
	@Expose
	@SerializedName("OLDESTACCOUNT_RS_AC_SUM")
	private String oldestAccountRsAcSum;
	@Expose
	@SerializedName("TOTALBALAMT_RS_AC_SUM")
	private Integer totalBalAmtRsAcSum;
	@Expose
	@SerializedName("TOTALSANCTAMT_RS_AC_SUM")
	private Integer totalSanctAmtRsAcSum;
	@Expose
	@SerializedName("TOTALCRDTLIMIT_RS_AC_SUM")
	private Integer totalCrdtLimitRsAcSum;
	@Expose
	@SerializedName("TOTALMONLYPYMNTAMT_RS_AC_SUM")
	private Integer totalMonlyPymntAmtRsAcSum;
	@Expose
	@SerializedName("TOTALWRITTENOFFAMNT_RS_AC_SUM")
	private Integer totalWrittenOffAmntRsAcSum;
	@Expose
	@SerializedName("AGEOFOLDESTTRADE_RS_OTK")
	private String ageOfOldestTradeRsOtk;
	@Expose
	@SerializedName("NUMBEROFOPENTRADES_RS_OTK")
	private String numberOfOpenTradesRsOtk;
	@Expose
	@SerializedName("ALINESEVERWRITTEN_RS_OTK")
	private String alineSeverWrittenRsOtk;
	@Expose
	@SerializedName("ALINESVWRTNIN9MNTHS_RS_OTK")
	private String alineSvWrtnIn9MnthsRsOtk;
	@Expose
	@SerializedName("ALINESVRWRTNIN6MNTHS_RS_OTK")
	private String alineSvrWrTnIn6MnthsRsOtk;
	@Expose
	@SerializedName("REPORTEDDATE_RS_EQ_SM")
	private String reportedDateRsEqSm;
	@Expose
	@SerializedName("PURPOSE_RS_EQ_SM")
	private String purposeRsEqSm;
	@Expose
	@SerializedName("TOTAL_RS_EQ_SM")
	private String totalRsEqSm;
	@Expose
	@SerializedName("PAST30DAYS_RS_EQ_SM")
	private String past30DaysRsEqSm;
	@Expose
	@SerializedName("PAST12MONTHS_RS_EQ_SM")
	private String past12MonthsRsEqSm;
	@Expose
	@SerializedName("PAST24MONTHS_RS_EQ_SM")
	private String past24MonthsRsEqSm;
	@Expose
	@SerializedName("RECENT_RS_EQ_SM")
	private String recentRsEqSm;
	@Expose
	@SerializedName("REPORTEDDATE_RS_EQ")
	private String reportedDateRsEq;
	@Expose
	@SerializedName("ENQREFERENCE_RS_EQ")
	private String enqReferenceRsEq;
	@Expose
	@SerializedName("ENQDATE_RS_EQ")
	private String enqDateRsEq;
	@Expose
	@SerializedName("ENQTIME_RS_EQ")
	private String enqTimeRsEq;
	@Expose
	@SerializedName("REQUESTPURPOSE_RS_EQ")
	private String requestPurposeRsEq;
	@Expose
	@SerializedName("AMOUNT_RS_EQ")
	private Integer amountRsEq;
	@Expose
	@SerializedName("ACCOUNTSDELIQUENT_RS_RE_ACT")
	private Integer accountsDeliquentRsReAct;
	@Expose
	@SerializedName("ACCOUNTSOPENED_RS_RE_ACT")
	private Integer accountsOpenedRsReAct;
	@Expose
	@SerializedName("TOTALINQUIRIES_RS_RE_ACT")
	private Integer totalInquiriesRsReAct;
	@Expose
	@SerializedName("ACCOUNTSUPDATED_RS_RE_ACT")
	private Integer accountsUpdatedRsReAct;
	@Expose
	@SerializedName("INSTITUTION_GRP_SM")
	private String institutionGrpSm;
	@Expose
	@SerializedName("CURRENTBALANCE_GRP_SM")
	private String currentBalanceGrpSm;
	@Expose
	@SerializedName("STATUS_GRP_SM")
	private String statusGrpSm;
	@Expose
	@SerializedName("DATEREPORTED_GRP_SM")
	private String dateReportedGrpSm;
	@Expose
	@SerializedName("NOOFMEMBERS_GRP_SM")
	private String noOfMembersGrpSm;
	@Expose
	@SerializedName("PASTDUEAMOUNT_GRP_SM")
	private String pastDueAmountGrpSm;
	@Expose
	@SerializedName("SANCTIONAMOUNT_GRP_SM")
	private String sanctionAmountGrpSm;
	@Expose
	@SerializedName("DATEOPENED_GRP_SM")
	private String dateOpenedGrpSm;
	@Expose
	@SerializedName("ACCOUNTNO_GRP_SM")
	private String accountNoGrpSm;
	@Expose
	@SerializedName("MEMBERSPASTDUE_GRP_SM")
	private String membersPastDueGrpSm;
	@Expose
	@SerializedName("WRITEOFFAMOUNT_GRP_SM")
	private String writeOffAmountGrpSm;
	@Expose
	@SerializedName("WRITEOFFDATE_GRP_SM")
	private String writeOffDateGrpSm;
	@Expose
	@SerializedName("REASONCODE_SC")
	private String reasonCodeSc;
	@Expose
	@SerializedName("SCOREDESCRIPTION_SC")
	private String scoreDescriptionSc;
	@Expose
	@SerializedName("SCORENAME_SC")
	private String scoreNameSc;
	@Expose
	@SerializedName("SCOREVALUE_SC")
	private String scoreValueSc;
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private String outputWriteFlag;
	@Expose
	@SerializedName("OUTPUT_WRITE_TIME")
	private String outputWriteTime;
	@Expose
	@SerializedName("OUTPUT_READ_TIME")
	private String outputReadTime;
	@Expose
	@SerializedName("ACCOUNT_KEY")
	private String accountKey;
	@Expose
	@SerializedName("ACCOUNT_NUM")
	private String accntNum;
	@Expose
	@SerializedName("DISPUTE_COMMENTS")
	private String disputeComments;
	@Expose
	@SerializedName("STATUS")
	private String status;
	@Expose
	@SerializedName("RESOLVED_DATE")
	private String resolvedDate;
	@Expose
	@SerializedName("CODE")
	private String code;
	@Expose
	@SerializedName("DESCRIPTION")
	private String description;
	@Expose
	@SerializedName("HITCODE_HD")
	private String hitCodeHd;
	@Expose
	@SerializedName("FULLNAME_NM_TP")
	private String fullnameNmTp;
	@Expose
	@SerializedName("EMPPHONE_RS_EMDL")
	private String empPhoneRsEmdl;
	
	public HibEquifaxSropDomain(Integer srNumber, String soaSourceName, String memberReferenceNumber,Date enquiryDate) {
		this.srNo = srNumber;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = memberReferenceNumber;
		this.enquiryDate=enquiryDate;
	}
	
	
	public HibEquifaxSropDomain(Integer srNumber, String soaSourceName, String memberReferenceNumber, String accountNumber, String accountKey,Date enquiryDate) {
		this.srNo = srNumber;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = memberReferenceNumber;
		this.accountKey = accountKey;
		this.accountNumberRsAc = accountNumber;
		this.enquiryDate=enquiryDate;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSrNo() {
		return srNo;
	}
	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}
	public String getSoaSourceName() {
		return soaSourceName;
	}
	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}
	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}
	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}
	public Date getEnquiryDate() {
		return enquiryDate;
	}
	public void setEnquiryDate(Date enquiryDate) {
		this.enquiryDate = enquiryDate;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getFirstnameNmTp() {
		return firstnameNmTp;
	}
	public void setFirstnameNmTp(String firstnameNmTp) {
		this.firstnameNmTp = firstnameNmTp;
	}
	public String getMidlleNameNmTp() {
		return midlleNameNmTp;
	}
	public void setMidlleNameNmTp(String midlleNameNmTp) {
		this.midlleNameNmTp = midlleNameNmTp;
	}
	public String getLastNameNmTp() {
		return lastNameNmTp;
	}
	public void setLastNameNmTp(String lastNameNmTp) {
		this.lastNameNmTp = lastNameNmTp;
	}
	public String getAddMidlleNameNmTp() {
		return addMidlleNameNmTp;
	}
	public void setAddMidlleNameNmTp(String addMidlleNameNmTp) {
		this.addMidlleNameNmTp = addMidlleNameNmTp;
	}
	public String getSuffixNmTp() {
		return suffixNmTp;
	}
	public void setSuffixNmTp(String suffixNmTp) {
		this.suffixNmTp = suffixNmTp;
	}
	public String getReportedDateAgIf() {
		return reportedDateAgIf;
	}
	public void setReportedDateAgIf(String reportedDateAgIf) {
		this.reportedDateAgIf = reportedDateAgIf;
	}
	public String getAgeAgIf() {
		return ageAgIf;
	}
	public void setAgeAgIf(String ageAgIf) {
		this.ageAgIf = ageAgIf;
	}
	public String getReportedDateAlNm() {
		return reportedDateAlNm;
	}
	public void setReportedDateAlNm(String reportedDateAlNm) {
		this.reportedDateAlNm = reportedDateAlNm;
	}
	public String getAliasNameAlNm() {
		return aliasNameAlNm;
	}
	public void setAliasNameAlNm(String aliasNameAlNm) {
		this.aliasNameAlNm = aliasNameAlNm;
	}
	public String getAdditionalNameInfoAdNmIf() {
		return additionalNameInfoAdNmIf;
	}
	public void setAdditionalNameInfoAdNmIf(String additionalNameInfoAdNmIf) {
		this.additionalNameInfoAdNmIf = additionalNameInfoAdNmIf;
	}
	public String getNoOfDependentsAdNmIf() {
		return noOfDependentsAdNmIf;
	}
	public void setNoOfDependentsAdNmIf(String noOfDependentsAdNmIf) {
		this.noOfDependentsAdNmIf = noOfDependentsAdNmIf;
	}
	public String getReportedDateRsAdd() {
		return reportedDateRsAdd;
	}
	public void setReportedDateRsAdd(String reportedDateRsAdd) {
		this.reportedDateRsAdd = reportedDateRsAdd;
	}
	public String getAddressRsAdd() {
		return addressRsAdd;
	}
	public void setAddressRsAdd(String addressRsAdd) {
		this.addressRsAdd = addressRsAdd;
	}
	public String getStateRsAdd() {
		return stateRsAdd;
	}
	public void setStateRsAdd(String stateRsAdd) {
		this.stateRsAdd = stateRsAdd;
	}
	public String getPostalRsAdd() {
		return postalRsAdd;
	}
	public void setPostalRsAdd(String postalRsAdd) {
		this.postalRsAdd = postalRsAdd;
	}
	public String getAddTypeRsAdd() {
		return addTypeRsAdd;
	}
	public void setAddTypeRsAdd(String addTypeRsAdd) {
		this.addTypeRsAdd = addTypeRsAdd;
	}
	public String getIdTypeRsId() {
		return idTypeRsId;
	}
	public void setIdTypeRsId(String idTypeRsId) {
		this.idTypeRsId = idTypeRsId;
	}
	public String getReportedDateRsId() {
		return reportedDateRsId;
	}
	public void setReportedDateRsId(String reportedDateRsId) {
		this.reportedDateRsId = reportedDateRsId;
	}
	public String getIdNumberRsId() {
		return idNumberRsId;
	}
	public void setIdNumberRsId(String idNumberRsId) {
		this.idNumberRsId = idNumberRsId;
	}
	public String getEmailReportedDateRsMl() {
		return emailReportedDateRsMl;
	}
	public void setEmailReportedDateRsMl(String emailReportedDateRsMl) {
		this.emailReportedDateRsMl = emailReportedDateRsMl;
	}
	public String getEmailAddressRsMl() {
		return emailAddressRsMl;
	}
	public void setEmailAddressRsMl(String emailAddressRsMl) {
		this.emailAddressRsMl = emailAddressRsMl;
	}
	public String getEmpNMRepDateRsEMDL() {
		return empNMRepDateRsEMDL;
	}
	public void setEmpNMRepDateRsEMDL(String empNMRepDateRsEMDL) {
		this.empNMRepDateRsEMDL = empNMRepDateRsEMDL;
	}
	public String getEmployerNameRsEmdl() {
		return employerNameRsEmdl;
	}
	public void setEmployerNameRsEmdl(String employerNameRsEmdl) {
		this.employerNameRsEmdl = employerNameRsEmdl;
	}
	public String getEmpPositionRsEmdl() {
		return empPositionRsEmdl;
	}
	public void setEmpPositionRsEmdl(String empPositionRsEmdl) {
		this.empPositionRsEmdl = empPositionRsEmdl;
	}
	public String getEmpAddressRsEmdl() {
		return empAddressRsEmdl;
	}
	public void setEmpAddressRsEmdl(String empAddressRsEmdl) {
		this.empAddressRsEmdl = empAddressRsEmdl;
	}
	public String getDobRsPerIf() {
		return dobRsPerIf;
	}
	public void setDobRsPerIf(String dobRsPerIf) {
		this.dobRsPerIf = dobRsPerIf;
	}
	public String getGenderRsPerIf() {
		return genderRsPerIf;
	}
	public void setGenderRsPerIf(String genderRsPerIf) {
		this.genderRsPerIf = genderRsPerIf;
	}
	public String getTotalIncomeRsPerIf() {
		return totalIncomeRsPerIf;
	}
	public void setTotalIncomeRsPerIf(String totalIncomeRsPerIf) {
		this.totalIncomeRsPerIf = totalIncomeRsPerIf;
	}
	public String getOccupationRsPerIf() {
		return occupationRsPerIf;
	}
	public void setOccupationRsPerIf(String occupationRsPerIf) {
		this.occupationRsPerIf = occupationRsPerIf;
	}
	public String getMaritalStausRsPerIf() {
		return maritalStausRsPerIf;
	}
	public void setMaritalStausRsPerIf(String maritalStausRsPerIf) {
		this.maritalStausRsPerIf = maritalStausRsPerIf;
	}
	public String getOccupationIcDl() {
		return occupationIcDl;
	}
	public void setOccupationIcDl(String occupationIcDl) {
		this.occupationIcDl = occupationIcDl;
	}
	public String getMonthlyIncomeIcDl() {
		return monthlyIncomeIcDl;
	}
	public void setMonthlyIncomeIcDl(String monthlyIncomeIcDl) {
		this.monthlyIncomeIcDl = monthlyIncomeIcDl;
	}
	public String getMonthlyExpenseIcDl() {
		return monthlyExpenseIcDl;
	}
	public void setMonthlyExpenseIcDl(String monthlyExpenseIcDl) {
		this.monthlyExpenseIcDl = monthlyExpenseIcDl;
	}
	public String getPovertyIndexIcDl() {
		return povertyIndexIcDl;
	}
	public void setPovertyIndexIcDl(String povertyIndexIcDl) {
		this.povertyIndexIcDl = povertyIndexIcDl;
	}
	public String getAssetOwnershipIcDl() {
		return assetOwnershipIcDl;
	}
	public void setAssetOwnershipIcDl(String assetOwnershipIcDl) {
		this.assetOwnershipIcDl = assetOwnershipIcDl;
	}
	public String getReportedDateRsPh() {
		return reportedDateRsPh;
	}
	public void setReportedDateRsPh(String reportedDateRsPh) {
		this.reportedDateRsPh = reportedDateRsPh;
	}
	public String getTypeCodeRsPh() {
		return typeCodeRsPh;
	}
	public void setTypeCodeRsPh(String typeCodeRsPh) {
		this.typeCodeRsPh = typeCodeRsPh;
	}
	public String getCountryCodeRsPh() {
		return countryCodeRsPh;
	}
	public void setCountryCodeRsPh(String countryCodeRsPh) {
		this.countryCodeRsPh = countryCodeRsPh;
	}
	public String getAreaCodeRsPh() {
		return areaCodeRsPh;
	}
	public void setAreaCodeRsPh(String areaCodeRsPh) {
		this.areaCodeRsPh = areaCodeRsPh;
	}
	public String getPhNumberRsPh() {
		return phNumberRsPh;
	}
	public void setPhNumberRsPh(String phNumberRsPh) {
		this.phNumberRsPh = phNumberRsPh;
	}
	public String getPhNoExtentionRsPh() {
		return phNoExtentionRsPh;
	}
	public void setPhNoExtentionRsPh(String phNoExtentionRsPh) {
		this.phNoExtentionRsPh = phNoExtentionRsPh;
	}
	public String getCustomercodeRsHd() {
		return customercodeRsHd;
	}
	public void setCustomercodeRsHd(String customercodeRsHd) {
		this.customercodeRsHd = customercodeRsHd;
	}
	public String getClientIdRsHd() {
		return clientIdRsHd;
	}
	public void setClientIdRsHd(String clientIdRsHd) {
		this.clientIdRsHd = clientIdRsHd;
	}
	public String getCustRefFieldRsHd() {
		return custRefFieldRsHd;
	}
	public void setCustRefFieldRsHd(String custRefFieldRsHd) {
		this.custRefFieldRsHd = custRefFieldRsHd;
	}
	public String getReportOrderNoRsHd() {
		return reportOrderNoRsHd;
	}
	public void setReportOrderNoRsHd(String reportOrderNoRsHd) {
		this.reportOrderNoRsHd = reportOrderNoRsHd;
	}
	public String getProductCodeRsHd() {
		return productCodeRsHd;
	}
	public void setProductCodeRsHd(String productCodeRsHd) {
		this.productCodeRsHd = productCodeRsHd;
	}
	public String getProductVersionRsHd() {
		return productVersionRsHd;
	}
	public void setProductVersionRsHd(String productVersionRsHd) {
		this.productVersionRsHd = productVersionRsHd;
	}
	public String getSuccessCodeRsHd() {
		return successCodeRsHd;
	}
	public void setSuccessCodeRsHd(String successCodeRsHd) {
		this.successCodeRsHd = successCodeRsHd;
	}
	public String getMatchTypeRsHd() {
		return matchTypeRsHd;
	}
	public void setMatchTypeRsHd(String matchTypeRsHd) {
		this.matchTypeRsHd = matchTypeRsHd;
	}
	public String getResDateRsHd() {
		return resDateRsHd;
	}
	public void setResDateRsHd(String resDateRsHd) {
		this.resDateRsHd = resDateRsHd;
	}
	public String getResTimeRsHd() {
		return resTimeRsHd;
	}
	public void setResTimeRsHd(String resTimeRsHd) {
		this.resTimeRsHd = resTimeRsHd;
	}
	public String getReportedDateRsAc() {
		return reportedDateRsAc;
	}
	public void setReportedDateRsAc(String reportedDateRsAc) {
		this.reportedDateRsAc = reportedDateRsAc;
	}
	public String getClientNameRsAc() {
		return clientNameRsAc;
	}
	public void setClientNameRsAc(String clientNameRsAc) {
		this.clientNameRsAc = clientNameRsAc;
	}
	public String getAccountNumberRsAc() {
		return accountNumberRsAc;
	}
	public void setAccountNumberRsAc(String accountNumberRsAc) {
		this.accountNumberRsAc = accountNumberRsAc;
	}
	public String getCurrentBalanceRsAc() {
		return currentBalanceRsAc;
	}
	public void setCurrentBalanceRsAc(String currentBalanceRsAc) {
		this.currentBalanceRsAc = currentBalanceRsAc;
	}
	public String getInstitutionRsAc() {
		return institutionRsAc;
	}
	public void setInstitutionRsAc(String institutionRsAc) {
		this.institutionRsAc = institutionRsAc;
	}
	public String getAccountTypeRsAc() {
		return accountTypeRsAc;
	}
	public void setAccountTypeRsAc(String accountTypeRsAc) {
		this.accountTypeRsAc = accountTypeRsAc;
	}
	public String getOwnershipTypeRsAc() {
		return ownershipTypeRsAc;
	}
	public void setOwnershipTypeRsAc(String ownershipTypeRsAc) {
		this.ownershipTypeRsAc = ownershipTypeRsAc;
	}
	public Integer getBalanceRsAc() {
		return balanceRsAc;
	}
	public void setBalanceRsAc(Integer balanceRsAc) {
		this.balanceRsAc = balanceRsAc;
	}
	public Integer getPastDueAmtRsAc() {
		return pastDueAmtRsAc;
	}
	public void setPastDueAmtRsAc(Integer pastDueAmtRsAc) {
		this.pastDueAmtRsAc = pastDueAmtRsAc;
	}
	public String getDisbursedAmountRsAc() {
		return disbursedAmountRsAc;
	}
	public void setDisbursedAmountRsAc(String disbursedAmountRsAc) {
		this.disbursedAmountRsAc = disbursedAmountRsAc;
	}
	public String getLoanCategoryRsAc() {
		return loanCategoryRsAc;
	}
	public void setLoanCategoryRsAc(String loanCategoryRsAc) {
		this.loanCategoryRsAc = loanCategoryRsAc;
	}
	public String getLoanPurposeRsAc() {
		return loanPurposeRsAc;
	}
	public void setLoanPurposeRsAc(String loanPurposeRsAc) {
		this.loanPurposeRsAc = loanPurposeRsAc;
	}
	public Integer getLastPaymentRsAc() {
		return lastPaymentRsAc;
	}
	public void setLastPaymentRsAc(Integer lastPaymentRsAc) {
		this.lastPaymentRsAc = lastPaymentRsAc;
	}
	public Integer getWriteOffAmtRsAc() {
		return writeOffAmtRsAc;
	}
	public void setWriteOffAmtRsAc(Integer writeOffAmtRsAc) {
		this.writeOffAmtRsAc = writeOffAmtRsAc;
	}
	public String getAccOpenRsAc() {
		return accOpenRsAc;
	}
	public void setAccOpenRsAc(String accOpenRsAc) {
		this.accOpenRsAc = accOpenRsAc;
	}
	public Integer getSactionAmtRsAc() {
		return sactionAmtRsAc;
	}
	public void setSactionAmtRsAc(Integer sactionAmtRsAc) {
		this.sactionAmtRsAc = sactionAmtRsAc;
	}
	public String getLastPaymentDateRsAc() {
		return lastPaymentDateRsAc;
	}
	public void setLastPaymentDateRsAc(String lastPaymentDateRsAc) {
		this.lastPaymentDateRsAc = lastPaymentDateRsAc;
	}
	public Integer getHighCreditRsAc() {
		return highCreditRsAc;
	}
	public void setHighCreditRsAc(Integer highCreditRsAc) {
		this.highCreditRsAc = highCreditRsAc;
	}
	public String getDateReportedRsAc() {
		return dateReportedRsAc;
	}
	public void setDateReportedRsAc(String dateReportedRsAc) {
		this.dateReportedRsAc = dateReportedRsAc;
	}
	public String getDateOpenedRsAc() {
		return dateOpenedRsAc;
	}
	public void setDateOpenedRsAc(String dateOpenedRsAc) {
		this.dateOpenedRsAc = dateOpenedRsAc;
	}
	public String getDateClosedRsAc() {
		return dateClosedRsAc;
	}
	public void setDateClosedRsAc(String dateClosedRsAc) {
		this.dateClosedRsAc = dateClosedRsAc;
	}
	public String getReasonRsAc() {
		return reasonRsAc;
	}
	public void setReasonRsAc(String reasonRsAc) {
		this.reasonRsAc = reasonRsAc;
	}
	public String getDateWrittenOffRsAc() {
		return dateWrittenOffRsAc;
	}
	public void setDateWrittenOffRsAc(String dateWrittenOffRsAc) {
		this.dateWrittenOffRsAc = dateWrittenOffRsAc;
	}
	public String getLoanCycleIdRsAc() {
		return loanCycleIdRsAc;
	}
	public void setLoanCycleIdRsAc(String loanCycleIdRsAc) {
		this.loanCycleIdRsAc = loanCycleIdRsAc;
	}
	public String getDateSanctionedRsAc() {
		return dateSanctionedRsAc;
	}
	public void setDateSanctionedRsAc(String dateSanctionedRsAc) {
		this.dateSanctionedRsAc = dateSanctionedRsAc;
	}
	public String getDateAppliedRsAc() {
		return dateAppliedRsAc;
	}
	public void setDateAppliedRsAc(String dateAppliedRsAc) {
		this.dateAppliedRsAc = dateAppliedRsAc;
	}
	public String getIntrestRateRSAC() {
		return intrestRateRSAC;
	}
	public void setIntrestRateRSAC(String intrestRateRSAC) {
		this.intrestRateRSAC = intrestRateRSAC;
	}
	public String getAppliedAmountRsAc() {
		return appliedAmountRsAc;
	}
	public void setAppliedAmountRsAc(String appliedAmountRsAc) {
		this.appliedAmountRsAc = appliedAmountRsAc;
	}
	public String getNoOfInstallmentsRsAc() {
		return noOfInstallmentsRsAc;
	}
	public void setNoOfInstallmentsRsAc(String noOfInstallmentsRsAc) {
		this.noOfInstallmentsRsAc = noOfInstallmentsRsAc;
	}
	public String getRepaymentTenureRsAc() {
		return repaymentTenureRsAc;
	}
	public void setRepaymentTenureRsAc(String repaymentTenureRsAc) {
		this.repaymentTenureRsAc = repaymentTenureRsAc;
	}
	public String getDisputeCodeRsAc() {
		return disputeCodeRsAc;
	}
	public void setDisputeCodeRsAc(String disputeCodeRsAc) {
		this.disputeCodeRsAc = disputeCodeRsAc;
	}
	public String getInstallmentAmtRsAc() {
		return installmentAmtRsAc;
	}
	public void setInstallmentAmtRsAc(String installmentAmtRsAc) {
		this.installmentAmtRsAc = installmentAmtRsAc;
	}
	public String getKeyPersonRsAc() {
		return keyPersonRsAc;
	}
	public void setKeyPersonRsAc(String keyPersonRsAc) {
		this.keyPersonRsAc = keyPersonRsAc;
	}
	public String getNomineeRsAc() {
		return nomineeRsAc;
	}
	public void setNomineeRsAc(String nomineeRsAc) {
		this.nomineeRsAc = nomineeRsAc;
	}
	public String getTermFrequencyRsAc() {
		return termFrequencyRsAc;
	}
	public void setTermFrequencyRsAc(String termFrequencyRsAc) {
		this.termFrequencyRsAc = termFrequencyRsAc;
	}
	public Integer getCreditLimitRsAc() {
		return creditLimitRsAc;
	}
	public void setCreditLimitRsAc(Integer creditLimitRsAc) {
		this.creditLimitRsAc = creditLimitRsAc;
	}
	public String getCollateralValueRsAc() {
		return collateralValueRsAc;
	}
	public void setCollateralValueRsAc(String collateralValueRsAc) {
		this.collateralValueRsAc = collateralValueRsAc;
	}
	public String getCollateralTypeRsAc() {
		return collateralTypeRsAc;
	}
	public void setCollateralTypeRsAc(String collateralTypeRsAc) {
		this.collateralTypeRsAc = collateralTypeRsAc;
	}
	public String getAccountStatusRsAc() {
		return accountStatusRsAc;
	}
	public void setAccountStatusRsAc(String accountStatusRsAc) {
		this.accountStatusRsAc = accountStatusRsAc;
	}
	public String getAssetClassificationRsAc() {
		return assetClassificationRsAc;
	}
	public void setAssetClassificationRsAc(String assetClassificationRsAc) {
		this.assetClassificationRsAc = assetClassificationRsAc;
	}
	public String getSuitFiledStatusRsAc() {
		return suitFiledStatusRsAc;
	}
	public void setSuitFiledStatusRsAc(String suitFiledStatusRsAc) {
		this.suitFiledStatusRsAc = suitFiledStatusRsAc;
	}
	public String getMonthKeyRsAcHis24() {
		return monthKeyRsAcHis24;
	}
	public void setMonthKeyRsAcHis24(String monthKeyRsAcHis24) {
		this.monthKeyRsAcHis24 = monthKeyRsAcHis24;
	}
	public String getPaymentStatusRsAcHis24() {
		return paymentStatusRsAcHis24;
	}
	public void setPaymentStatusRsAcHis24(String paymentStatusRsAcHis24) {
		this.paymentStatusRsAcHis24 = paymentStatusRsAcHis24;
	}
	public String getSuitFiledStatusRsAcHis24() {
		return suitFiledStatusRsAcHis24;
	}
	public void setSuitFiledStatusRsAcHis24(String suitFiledStatusRsAcHis24) {
		this.suitFiledStatusRsAcHis24 = suitFiledStatusRsAcHis24;
	}
	public String getAssettClsStatusRsAcHis24() {
		return assettClsStatusRsAcHis24;
	}
	public void setAssettClsStatusRsAcHis24(String assettClsStatusRsAcHis24) {
		this.assettClsStatusRsAcHis24 = assettClsStatusRsAcHis24;
	}
	public String getMonthKeyRsAcHis48() {
		return monthKeyRsAcHis48;
	}
	public void setMonthKeyRsAcHis48(String monthKeyRsAcHis48) {
		this.monthKeyRsAcHis48 = monthKeyRsAcHis48;
	}
	public String getpAYMENTSTATUSRsAcHis48() {
		return pAYMENTSTATUSRsAcHis48;
	}
	public void setpAYMENTSTATUSRsAcHis48(String pAYMENTSTATUSRsAcHis48) {
		this.pAYMENTSTATUSRsAcHis48 = pAYMENTSTATUSRsAcHis48;
	}
	public String getsUITFILEDSTATUSRsAcHis48() {
		return sUITFILEDSTATUSRsAcHis48;
	}
	public void setsUITFILEDSTATUSRsAcHis48(String sUITFILEDSTATUSRsAcHis48) {
		this.sUITFILEDSTATUSRsAcHis48 = sUITFILEDSTATUSRsAcHis48;
	}
	public String getaSSETCLSSTATUSRsAcHis48() {
		return aSSETCLSSTATUSRsAcHis48;
	}
	public void setaSSETCLSSTATUSRsAcHis48(String aSSETCLSSTATUSRsAcHis48) {
		this.aSSETCLSSTATUSRsAcHis48 = aSSETCLSSTATUSRsAcHis48;
	}
	public Integer getNoOfAccountsRsAcSum() {
		return noOfAccountsRsAcSum;
	}
	public void setNoOfAccountsRsAcSum(Integer noOfAccountsRsAcSum) {
		this.noOfAccountsRsAcSum = noOfAccountsRsAcSum;
	}
	public Integer getNoOfActiveAccountsRsAcSum() {
		return noOfActiveAccountsRsAcSum;
	}
	public void setNoOfActiveAccountsRsAcSum(Integer noOfActiveAccountsRsAcSum) {
		this.noOfActiveAccountsRsAcSum = noOfActiveAccountsRsAcSum;
	}
	public Integer getNoOfWriteOffsRsAcSum() {
		return noOfWriteOffsRsAcSum;
	}
	public void setNoOfWriteOffsRsAcSum(Integer noOfWriteOffsRsAcSum) {
		this.noOfWriteOffsRsAcSum = noOfWriteOffsRsAcSum;
	}
	public Integer getTotalPastDueRsAcSum() {
		return totalPastDueRsAcSum;
	}
	public void setTotalPastDueRsAcSum(Integer totalPastDueRsAcSum) {
		this.totalPastDueRsAcSum = totalPastDueRsAcSum;
	}
	public String getMostSrvrStaWhin24MonRsAcSum() {
		return mostSrvrStaWhin24MonRsAcSum;
	}
	public void setMostSrvrStaWhin24MonRsAcSum(String mostSrvrStaWhin24MonRsAcSum) {
		this.mostSrvrStaWhin24MonRsAcSum = mostSrvrStaWhin24MonRsAcSum;
	}
	public Integer getSingleHighestCreditRsAcSum() {
		return singleHighestCreditRsAcSum;
	}
	public void setSingleHighestCreditRsAcSum(Integer singleHighestCreditRsAcSum) {
		this.singleHighestCreditRsAcSum = singleHighestCreditRsAcSum;
	}
	public Integer getSingleHighSanctAmtRsAcSum() {
		return singleHighSanctAmtRsAcSum;
	}
	public void setSingleHighSanctAmtRsAcSum(Integer singleHighSanctAmtRsAcSum) {
		this.singleHighSanctAmtRsAcSum = singleHighSanctAmtRsAcSum;
	}
	public Integer getTotalHighCreditRsAcSum() {
		return totalHighCreditRsAcSum;
	}
	public void setTotalHighCreditRsAcSum(Integer totalHighCreditRsAcSum) {
		this.totalHighCreditRsAcSum = totalHighCreditRsAcSum;
	}
	public Integer getAverageOpenBalanceRsAcSum() {
		return averageOpenBalanceRsAcSum;
	}
	public void setAverageOpenBalanceRsAcSum(Integer averageOpenBalanceRsAcSum) {
		this.averageOpenBalanceRsAcSum = averageOpenBalanceRsAcSum;
	}
	public Integer getSingleHighestBalanceRsAcSum() {
		return singleHighestBalanceRsAcSum;
	}
	public void setSingleHighestBalanceRsAcSum(Integer singleHighestBalanceRsAcSum) {
		this.singleHighestBalanceRsAcSum = singleHighestBalanceRsAcSum;
	}
	public Integer getNoOfPastDueAcctsRsAcSum() {
		return noOfPastDueAcctsRsAcSum;
	}
	public void setNoOfPastDueAcctsRsAcSum(Integer noOfPastDueAcctsRsAcSum) {
		this.noOfPastDueAcctsRsAcSum = noOfPastDueAcctsRsAcSum;
	}
	public Integer getNoOfZeroBalAcctsRsAcSum() {
		return noOfZeroBalAcctsRsAcSum;
	}
	public void setNoOfZeroBalAcctsRsAcSum(Integer noOfZeroBalAcctsRsAcSum) {
		this.noOfZeroBalAcctsRsAcSum = noOfZeroBalAcctsRsAcSum;
	}
	public String getRecentAccountRsAcSum() {
		return recentAccountRsAcSum;
	}
	public void setRecentAccountRsAcSum(String recentAccountRsAcSum) {
		this.recentAccountRsAcSum = recentAccountRsAcSum;
	}
	public String getOldestAccountRsAcSum() {
		return oldestAccountRsAcSum;
	}
	public void setOldestAccountRsAcSum(String oldestAccountRsAcSum) {
		this.oldestAccountRsAcSum = oldestAccountRsAcSum;
	}
	public Integer getTotalBalAmtRsAcSum() {
		return totalBalAmtRsAcSum;
	}
	public void setTotalBalAmtRsAcSum(Integer totalBalAmtRsAcSum) {
		this.totalBalAmtRsAcSum = totalBalAmtRsAcSum;
	}
	public Integer getTotalSanctAmtRsAcSum() {
		return totalSanctAmtRsAcSum;
	}
	public void setTotalSanctAmtRsAcSum(Integer totalSanctAmtRsAcSum) {
		this.totalSanctAmtRsAcSum = totalSanctAmtRsAcSum;
	}
	public Integer getTotalCrdtLimitRsAcSum() {
		return totalCrdtLimitRsAcSum;
	}
	public void setTotalCrdtLimitRsAcSum(Integer totalCrdtLimitRsAcSum) {
		this.totalCrdtLimitRsAcSum = totalCrdtLimitRsAcSum;
	}
	public Integer getTotalMonlyPymntAmtRsAcSum() {
		return totalMonlyPymntAmtRsAcSum;
	}
	public void setTotalMonlyPymntAmtRsAcSum(Integer totalMonlyPymntAmtRsAcSum) {
		this.totalMonlyPymntAmtRsAcSum = totalMonlyPymntAmtRsAcSum;
	}
	public Integer getTotalWrittenOffAmntRsAcSum() {
		return totalWrittenOffAmntRsAcSum;
	}
	public void setTotalWrittenOffAmntRsAcSum(Integer totalWrittenOffAmntRsAcSum) {
		this.totalWrittenOffAmntRsAcSum = totalWrittenOffAmntRsAcSum;
	}
	public String getAgeOfOldestTradeRsOtk() {
		return ageOfOldestTradeRsOtk;
	}
	public void setAgeOfOldestTradeRsOtk(String ageOfOldestTradeRsOtk) {
		this.ageOfOldestTradeRsOtk = ageOfOldestTradeRsOtk;
	}
	public String getNumberOfOpenTradesRsOtk() {
		return numberOfOpenTradesRsOtk;
	}
	public void setNumberOfOpenTradesRsOtk(String numberOfOpenTradesRsOtk) {
		this.numberOfOpenTradesRsOtk = numberOfOpenTradesRsOtk;
	}
	public String getAlineSeverWrittenRsOtk() {
		return alineSeverWrittenRsOtk;
	}
	public void setAlineSeverWrittenRsOtk(String alineSeverWrittenRsOtk) {
		this.alineSeverWrittenRsOtk = alineSeverWrittenRsOtk;
	}
	public String getAlineSvWrtnIn9MnthsRsOtk() {
		return alineSvWrtnIn9MnthsRsOtk;
	}
	public void setAlineSvWrtnIn9MnthsRsOtk(String alineSvWrtnIn9MnthsRsOtk) {
		this.alineSvWrtnIn9MnthsRsOtk = alineSvWrtnIn9MnthsRsOtk;
	}
	public String getAlineSvrWrTnIn6MnthsRsOtk() {
		return alineSvrWrTnIn6MnthsRsOtk;
	}
	public void setAlineSvrWrTnIn6MnthsRsOtk(String alineSvrWrTnIn6MnthsRsOtk) {
		this.alineSvrWrTnIn6MnthsRsOtk = alineSvrWrTnIn6MnthsRsOtk;
	}
	public String getReportedDateRsEqSm() {
		return reportedDateRsEqSm;
	}
	public void setReportedDateRsEqSm(String reportedDateRsEqSm) {
		this.reportedDateRsEqSm = reportedDateRsEqSm;
	}
	public String getPurposeRsEqSm() {
		return purposeRsEqSm;
	}
	public void setPurposeRsEqSm(String purposeRsEqSm) {
		this.purposeRsEqSm = purposeRsEqSm;
	}
	public String getTotalRsEqSm() {
		return totalRsEqSm;
	}
	public void setTotalRsEqSm(String totalRsEqSm) {
		this.totalRsEqSm = totalRsEqSm;
	}
	public String getPast30DaysRsEqSm() {
		return past30DaysRsEqSm;
	}
	public void setPast30DaysRsEqSm(String past30DaysRsEqSm) {
		this.past30DaysRsEqSm = past30DaysRsEqSm;
	}
	public String getPast12MonthsRsEqSm() {
		return past12MonthsRsEqSm;
	}
	public void setPast12MonthsRsEqSm(String past12MonthsRsEqSm) {
		this.past12MonthsRsEqSm = past12MonthsRsEqSm;
	}
	public String getPast24MonthsRsEqSm() {
		return past24MonthsRsEqSm;
	}
	public void setPast24MonthsRsEqSm(String past24MonthsRsEqSm) {
		this.past24MonthsRsEqSm = past24MonthsRsEqSm;
	}
	public String getRecentRsEqSm() {
		return recentRsEqSm;
	}
	public void setRecentRsEqSm(String recentRsEqSm) {
		this.recentRsEqSm = recentRsEqSm;
	}
	public String getReportedDateRsEq() {
		return reportedDateRsEq;
	}
	public void setReportedDateRsEq(String reportedDateRsEq) {
		this.reportedDateRsEq = reportedDateRsEq;
	}
	public String getEnqReferenceRsEq() {
		return enqReferenceRsEq;
	}
	public void setEnqReferenceRsEq(String enqReferenceRsEq) {
		this.enqReferenceRsEq = enqReferenceRsEq;
	}
	public String getEnqDateRsEq() {
		return enqDateRsEq;
	}
	public void setEnqDateRsEq(String enqDateRsEq) {
		this.enqDateRsEq = enqDateRsEq;
	}
	public String getEnqTimeRsEq() {
		return enqTimeRsEq;
	}
	public void setEnqTimeRsEq(String enqTimeRsEq) {
		this.enqTimeRsEq = enqTimeRsEq;
	}
	public String getRequestPurposeRsEq() {
		return requestPurposeRsEq;
	}
	public void setRequestPurposeRsEq(String requestPurposeRsEq) {
		this.requestPurposeRsEq = requestPurposeRsEq;
	}
	public Integer getAmountRsEq() {
		return amountRsEq;
	}
	public void setAmountRsEq(Integer amountRsEq) {
		this.amountRsEq = amountRsEq;
	}
	public Integer getAccountsDeliquentRsReAct() {
		return accountsDeliquentRsReAct;
	}
	public void setAccountsDeliquentRsReAct(Integer accountsDeliquentRsReAct) {
		this.accountsDeliquentRsReAct = accountsDeliquentRsReAct;
	}
	public Integer getAccountsOpenedRsReAct() {
		return accountsOpenedRsReAct;
	}
	public void setAccountsOpenedRsReAct(Integer accountsOpenedRsReAct) {
		this.accountsOpenedRsReAct = accountsOpenedRsReAct;
	}
	public Integer getTotalInquiriesRsReAct() {
		return totalInquiriesRsReAct;
	}
	public void setTotalInquiriesRsReAct(Integer totalInquiriesRsReAct) {
		this.totalInquiriesRsReAct = totalInquiriesRsReAct;
	}
	public Integer getAccountsUpdatedRsReAct() {
		return accountsUpdatedRsReAct;
	}
	public void setAccountsUpdatedRsReAct(Integer accountsUpdatedRsReAct) {
		this.accountsUpdatedRsReAct = accountsUpdatedRsReAct;
	}
	public String getInstitutionGrpSm() {
		return institutionGrpSm;
	}
	public void setInstitutionGrpSm(String institutionGrpSm) {
		this.institutionGrpSm = institutionGrpSm;
	}
	public String getCurrentBalanceGrpSm() {
		return currentBalanceGrpSm;
	}
	public void setCurrentBalanceGrpSm(String currentBalanceGrpSm) {
		this.currentBalanceGrpSm = currentBalanceGrpSm;
	}
	public String getStatusGrpSm() {
		return statusGrpSm;
	}
	public void setStatusGrpSm(String statusGrpSm) {
		this.statusGrpSm = statusGrpSm;
	}
	public String getDateReportedGrpSm() {
		return dateReportedGrpSm;
	}
	public void setDateReportedGrpSm(String dateReportedGrpSm) {
		this.dateReportedGrpSm = dateReportedGrpSm;
	}
	public String getNoOfMembersGrpSm() {
		return noOfMembersGrpSm;
	}
	public void setNoOfMembersGrpSm(String noOfMembersGrpSm) {
		this.noOfMembersGrpSm = noOfMembersGrpSm;
	}
	public String getPastDueAmountGrpSm() {
		return pastDueAmountGrpSm;
	}
	public void setPastDueAmountGrpSm(String pastDueAmountGrpSm) {
		this.pastDueAmountGrpSm = pastDueAmountGrpSm;
	}
	public String getSanctionAmountGrpSm() {
		return sanctionAmountGrpSm;
	}
	public void setSanctionAmountGrpSm(String sanctionAmountGrpSm) {
		this.sanctionAmountGrpSm = sanctionAmountGrpSm;
	}
	public String getDateOpenedGrpSm() {
		return dateOpenedGrpSm;
	}
	public void setDateOpenedGrpSm(String dateOpenedGrpSm) {
		this.dateOpenedGrpSm = dateOpenedGrpSm;
	}
	public String getAccountNoGrpSm() {
		return accountNoGrpSm;
	}
	public void setAccountNoGrpSm(String accountNoGrpSm) {
		this.accountNoGrpSm = accountNoGrpSm;
	}
	public String getMembersPastDueGrpSm() {
		return membersPastDueGrpSm;
	}
	public void setMembersPastDueGrpSm(String membersPastDueGrpSm) {
		this.membersPastDueGrpSm = membersPastDueGrpSm;
	}
	public String getWriteOffAmountGrpSm() {
		return writeOffAmountGrpSm;
	}
	public void setWriteOffAmountGrpSm(String writeOffAmountGrpSm) {
		this.writeOffAmountGrpSm = writeOffAmountGrpSm;
	}
	public String getWriteOffDateGrpSm() {
		return writeOffDateGrpSm;
	}
	public void setWriteOffDateGrpSm(String writeOffDateGrpSm) {
		this.writeOffDateGrpSm = writeOffDateGrpSm;
	}
	public String getReasonCodeSc() {
		return reasonCodeSc;
	}
	public void setReasonCodeSc(String reasonCodeSc) {
		this.reasonCodeSc = reasonCodeSc;
	}
	public String getScoreDescriptionSc() {
		return scoreDescriptionSc;
	}
	public void setScoreDescriptionSc(String scoreDescriptionSc) {
		this.scoreDescriptionSc = scoreDescriptionSc;
	}
	public String getScoreNameSc() {
		return scoreNameSc;
	}
	public void setScoreNameSc(String scoreNameSc) {
		this.scoreNameSc = scoreNameSc;
	}
	public String getScoreValueSc() {
		return scoreValueSc;
	}
	public void setScoreValueSc(String scoreValueSc) {
		this.scoreValueSc = scoreValueSc;
	}
	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}
	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}
	public String getOutputWriteTime() {
		return outputWriteTime;
	}
	public void setOutputWriteTime(String outputWriteTime) {
		this.outputWriteTime = outputWriteTime;
	}
	public String getOutputReadTime() {
		return outputReadTime;
	}
	public void setOutputReadTime(String outputReadTime) {
		this.outputReadTime = outputReadTime;
	}
	public String getAccountKey() {
		return accountKey;
	}
	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}


	public String getAccntNum() {
		return accntNum;
	}


	public void setAccntNum(String accntNum) {
		this.accntNum = accntNum;
	}


	public String getDisputeComments() {
		return disputeComments;
	}


	public void setDisputeComments(String disputeComments) {
		this.disputeComments = disputeComments;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getResolvedDate() {
		return resolvedDate;
	}


	public void setResolvedDate(String resolvedDate) {
		this.resolvedDate = resolvedDate;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getHitCodeHd() {
		return hitCodeHd;
	}


	public void setHitCodeHd(String hitCodeHd) {
		this.hitCodeHd = hitCodeHd;
	}


	public String getFullnameNmTp() {
		return fullnameNmTp;
	}


	public void setFullnameNmTp(String fullnameNmTp) {
		this.fullnameNmTp = fullnameNmTp;
	}


	public String getEmpPhoneRsEmdl() {
		return empPhoneRsEmdl;
	}


	public void setEmpPhoneRsEmdl(String empPhoneRsEmdl) {
		this.empPhoneRsEmdl = empPhoneRsEmdl;
	}


	@Override
	public String toString() {
		return "HibEquifaxSropDomain [id=" + id + ", srNo=" + srNo
				+ ", soaSourceName=" + soaSourceName
				+ ", memberReferenceNumber=" + memberReferenceNumber
				+ ", enquiryDate=" + enquiryDate + ", customerCode="
				+ customerCode + ", firstnameNmTp=" + firstnameNmTp
				+ ", midlleNameNmTp=" + midlleNameNmTp + ", lastNameNmTp="
				+ lastNameNmTp + ", addMidlleNameNmTp=" + addMidlleNameNmTp
				+ ", suffixNmTp=" + suffixNmTp + ", reportedDateAgIf="
				+ reportedDateAgIf + ", ageAgIf=" + ageAgIf
				+ ", reportedDateAlNm=" + reportedDateAlNm + ", aliasNameAlNm="
				+ aliasNameAlNm + ", additionalNameInfoAdNmIf="
				+ additionalNameInfoAdNmIf + ", noOfDependentsAdNmIf="
				+ noOfDependentsAdNmIf + ", reportedDateRsAdd="
				+ reportedDateRsAdd + ", addressRsAdd=" + addressRsAdd
				+ ", stateRsAdd=" + stateRsAdd + ", postalRsAdd=" + postalRsAdd
				+ ", addTypeRsAdd=" + addTypeRsAdd + ", idTypeRsId="
				+ idTypeRsId + ", reportedDateRsId=" + reportedDateRsId
				+ ", idNumberRsId=" + idNumberRsId + ", emailReportedDateRsMl="
				+ emailReportedDateRsMl + ", emailAddressRsMl="
				+ emailAddressRsMl + ", empNMRepDateRsEMDL="
				+ empNMRepDateRsEMDL + ", employerNameRsEmdl="
				+ employerNameRsEmdl + ", empPositionRsEmdl="
				+ empPositionRsEmdl + ", empAddressRsEmdl=" + empAddressRsEmdl
				+ ", dobRsPerIf=" + dobRsPerIf + ", genderRsPerIf="
				+ genderRsPerIf + ", totalIncomeRsPerIf=" + totalIncomeRsPerIf
				+ ", occupationRsPerIf=" + occupationRsPerIf
				+ ", maritalStausRsPerIf=" + maritalStausRsPerIf
				+ ", occupationIcDl=" + occupationIcDl + ", monthlyIncomeIcDl="
				+ monthlyIncomeIcDl + ", monthlyExpenseIcDl="
				+ monthlyExpenseIcDl + ", povertyIndexIcDl=" + povertyIndexIcDl
				+ ", assetOwnershipIcDl=" + assetOwnershipIcDl
				+ ", reportedDateRsPh=" + reportedDateRsPh + ", typeCodeRsPh="
				+ typeCodeRsPh + ", countryCodeRsPh=" + countryCodeRsPh
				+ ", areaCodeRsPh=" + areaCodeRsPh + ", phNumberRsPh="
				+ phNumberRsPh + ", phNoExtentionRsPh=" + phNoExtentionRsPh
				+ ", customercodeRsHd=" + customercodeRsHd + ", clientIdRsHd="
				+ clientIdRsHd + ", custRefFieldRsHd=" + custRefFieldRsHd
				+ ", reportOrderNoRsHd=" + reportOrderNoRsHd
				+ ", productCodeRsHd=" + productCodeRsHd
				+ ", productVersionRsHd=" + productVersionRsHd
				+ ", successCodeRsHd=" + successCodeRsHd + ", matchTypeRsHd="
				+ matchTypeRsHd + ", resDateRsHd=" + resDateRsHd
				+ ", resTimeRsHd=" + resTimeRsHd + ", reportedDateRsAc="
				+ reportedDateRsAc + ", clientNameRsAc=" + clientNameRsAc
				+ ", accountNumberRsAc=" + accountNumberRsAc
				+ ", currentBalanceRsAc=" + currentBalanceRsAc
				+ ", institutionRsAc=" + institutionRsAc + ", accountTypeRsAc="
				+ accountTypeRsAc + ", ownershipTypeRsAc=" + ownershipTypeRsAc
				+ ", balanceRsAc=" + balanceRsAc + ", pastDueAmtRsAc="
				+ pastDueAmtRsAc + ", disbursedAmountRsAc="
				+ disbursedAmountRsAc + ", loanCategoryRsAc="
				+ loanCategoryRsAc + ", loanPurposeRsAc=" + loanPurposeRsAc
				+ ", lastPaymentRsAc=" + lastPaymentRsAc + ", writeOffAmtRsAc="
				+ writeOffAmtRsAc + ", accOpenRsAc=" + accOpenRsAc
				+ ", sactionAmtRsAc=" + sactionAmtRsAc
				+ ", lastPaymentDateRsAc=" + lastPaymentDateRsAc
				+ ", highCreditRsAc=" + highCreditRsAc + ", dateReportedRsAc="
				+ dateReportedRsAc + ", dateOpenedRsAc=" + dateOpenedRsAc
				+ ", dateClosedRsAc=" + dateClosedRsAc + ", reasonRsAc="
				+ reasonRsAc + ", dateWrittenOffRsAc=" + dateWrittenOffRsAc
				+ ", loanCycleIdRsAc=" + loanCycleIdRsAc
				+ ", dateSanctionedRsAc=" + dateSanctionedRsAc
				+ ", dateAppliedRsAc=" + dateAppliedRsAc + ", intrestRateRSAC="
				+ intrestRateRSAC + ", appliedAmountRsAc=" + appliedAmountRsAc
				+ ", noOfInstallmentsRsAc=" + noOfInstallmentsRsAc
				+ ", repaymentTenureRsAc=" + repaymentTenureRsAc
				+ ", disputeCodeRsAc=" + disputeCodeRsAc
				+ ", installmentAmtRsAc=" + installmentAmtRsAc
				+ ", keyPersonRsAc=" + keyPersonRsAc + ", nomineeRsAc="
				+ nomineeRsAc + ", termFrequencyRsAc=" + termFrequencyRsAc
				+ ", creditLimitRsAc=" + creditLimitRsAc
				+ ", collateralValueRsAc=" + collateralValueRsAc
				+ ", collateralTypeRsAc=" + collateralTypeRsAc
				+ ", accountStatusRsAc=" + accountStatusRsAc
				+ ", assetClassificationRsAc=" + assetClassificationRsAc
				+ ", suitFiledStatusRsAc=" + suitFiledStatusRsAc
				+ ", monthKeyRsAcHis24=" + monthKeyRsAcHis24
				+ ", paymentStatusRsAcHis24=" + paymentStatusRsAcHis24
				+ ", suitFiledStatusRsAcHis24=" + suitFiledStatusRsAcHis24
				+ ", assettClsStatusRsAcHis24=" + assettClsStatusRsAcHis24
				+ ", monthKeyRsAcHis48=" + monthKeyRsAcHis48
				+ ", pAYMENTSTATUSRsAcHis48=" + pAYMENTSTATUSRsAcHis48
				+ ", sUITFILEDSTATUSRsAcHis48=" + sUITFILEDSTATUSRsAcHis48
				+ ", aSSETCLSSTATUSRsAcHis48=" + aSSETCLSSTATUSRsAcHis48
				+ ", noOfAccountsRsAcSum=" + noOfAccountsRsAcSum
				+ ", noOfActiveAccountsRsAcSum=" + noOfActiveAccountsRsAcSum
				+ ", noOfWriteOffsRsAcSum=" + noOfWriteOffsRsAcSum
				+ ", totalPastDueRsAcSum=" + totalPastDueRsAcSum
				+ ", mostSrvrStaWhin24MonRsAcSum="
				+ mostSrvrStaWhin24MonRsAcSum + ", singleHighestCreditRsAcSum="
				+ singleHighestCreditRsAcSum + ", singleHighSanctAmtRsAcSum="
				+ singleHighSanctAmtRsAcSum + ", totalHighCreditRsAcSum="
				+ totalHighCreditRsAcSum + ", averageOpenBalanceRsAcSum="
				+ averageOpenBalanceRsAcSum + ", singleHighestBalanceRsAcSum="
				+ singleHighestBalanceRsAcSum + ", noOfPastDueAcctsRsAcSum="
				+ noOfPastDueAcctsRsAcSum + ", noOfZeroBalAcctsRsAcSum="
				+ noOfZeroBalAcctsRsAcSum + ", recentAccountRsAcSum="
				+ recentAccountRsAcSum + ", oldestAccountRsAcSum="
				+ oldestAccountRsAcSum + ", totalBalAmtRsAcSum="
				+ totalBalAmtRsAcSum + ", totalSanctAmtRsAcSum="
				+ totalSanctAmtRsAcSum + ", totalCrdtLimitRsAcSum="
				+ totalCrdtLimitRsAcSum + ", totalMonlyPymntAmtRsAcSum="
				+ totalMonlyPymntAmtRsAcSum + ", totalWrittenOffAmntRsAcSum="
				+ totalWrittenOffAmntRsAcSum + ", ageOfOldestTradeRsOtk="
				+ ageOfOldestTradeRsOtk + ", numberOfOpenTradesRsOtk="
				+ numberOfOpenTradesRsOtk + ", alineSeverWrittenRsOtk="
				+ alineSeverWrittenRsOtk + ", alineSvWrtnIn9MnthsRsOtk="
				+ alineSvWrtnIn9MnthsRsOtk + ", alineSvrWrTnIn6MnthsRsOtk="
				+ alineSvrWrTnIn6MnthsRsOtk + ", reportedDateRsEqSm="
				+ reportedDateRsEqSm + ", purposeRsEqSm=" + purposeRsEqSm
				+ ", totalRsEqSm=" + totalRsEqSm + ", past30DaysRsEqSm="
				+ past30DaysRsEqSm + ", past12MonthsRsEqSm="
				+ past12MonthsRsEqSm + ", past24MonthsRsEqSm="
				+ past24MonthsRsEqSm + ", recentRsEqSm=" + recentRsEqSm
				+ ", reportedDateRsEq=" + reportedDateRsEq
				+ ", enqReferenceRsEq=" + enqReferenceRsEq + ", enqDateRsEq="
				+ enqDateRsEq + ", enqTimeRsEq=" + enqTimeRsEq
				+ ", requestPurposeRsEq=" + requestPurposeRsEq
				+ ", amountRsEq=" + amountRsEq + ", accountsDeliquentRsReAct="
				+ accountsDeliquentRsReAct + ", accountsOpenedRsReAct="
				+ accountsOpenedRsReAct + ", totalInquiriesRsReAct="
				+ totalInquiriesRsReAct + ", accountsUpdatedRsReAct="
				+ accountsUpdatedRsReAct + ", institutionGrpSm="
				+ institutionGrpSm + ", currentBalanceGrpSm="
				+ currentBalanceGrpSm + ", statusGrpSm=" + statusGrpSm
				+ ", dateReportedGrpSm=" + dateReportedGrpSm
				+ ", noOfMembersGrpSm=" + noOfMembersGrpSm
				+ ", pastDueAmountGrpSm=" + pastDueAmountGrpSm
				+ ", sanctionAmountGrpSm=" + sanctionAmountGrpSm
				+ ", dateOpenedGrpSm=" + dateOpenedGrpSm + ", accountNoGrpSm="
				+ accountNoGrpSm + ", membersPastDueGrpSm="
				+ membersPastDueGrpSm + ", writeOffAmountGrpSm="
				+ writeOffAmountGrpSm + ", writeOffDateGrpSm="
				+ writeOffDateGrpSm + ", reasonCodeSc=" + reasonCodeSc
				+ ", scoreDescriptionSc=" + scoreDescriptionSc
				+ ", scoreNameSc=" + scoreNameSc + ", scoreValueSc="
				+ scoreValueSc + ", outputWriteFlag=" + outputWriteFlag
				+ ", outputWriteTime=" + outputWriteTime + ", outputReadTime="
				+ outputReadTime + ", accountKey=" + accountKey + ", accntNum="
				+ accntNum + ", disputeComments=" + disputeComments
				+ ", status=" + status + ", resolvedDate=" + resolvedDate
				+ ", code=" + code + ", description=" + description
				+ ", hitCodeHd=" + hitCodeHd + ", fullnameNmTp=" + fullnameNmTp
				+ ", empPhoneRsEmdl=" + empPhoneRsEmdl + "]";
	}

}
