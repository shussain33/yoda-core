package com.softcell.gonogo.model.mbdatapush;

import java.util.HashMap;
import java.util.Map;

public interface EXPERIAN_METADATA{
	
	Map<String, String> genderMap = new HashMap<String, String>(){
	private static final long serialVersionUID = 1L;{
			put("2", "FEMALE");
			put("1", "MALE");
	}};
	
	Map<String, String> maritalStatusMap = new HashMap<String, String>(){
	private static final long serialVersionUID = 1L;{
			put("1", "SINGLE");
			put("2", "MARRIED");
			put("4", "DIVORCED");
			put("3", "WIDOW(ER)");
	}};
	
	
	Map<String, String> accountTypeMap = new HashMap<String, String>(){
		private static final long serialVersionUID = 1L;{
				put("01", "AUTO LOAN");
				put("02", "HOUSING LOAN");
				put("03", "PROPERTY LOAN");
				put("04", "LOAN AGAINST SHARES/SECURITIES");
				put("05", "PERSONAL LOAN");
				put("06", "CONSUMER LOAN");
				put("07", "GOLD LOAN");
				put("08", "EDUCATIONAL LOAN");
				put("09", "LOAN TO PROFESSIONAL");
				put("10", "CREDIT CARD");
				put("11", "LEASING");
				put("12", "OVERDRAFT");
				put("13", "TWO-WHEELER LOAN");
				put("14", "NON-FUNDED CREDIT FACILITY");
				put("15", "LOAN AGAINST BANK DEPOSITS");
				put("16", "FLEET CARD");
				put("17", "Commercial Vehicle Loan");
				put("18", "Telco - Wireless");
				put("19", "Telco - Broadband");
				put("20", "Telco - Landline");
				put("40", "Microfinance - Business Loan");
				put("41", "Microfinance - Personal Loan");
				put("42", "Microfinance - Housing Loan");
				put("43", "Microfinance - Others");
				put("51", "BUSINESS LOAN - GENERAL");
				put("52", "BUSINESS LOAN - PRIORITY SECTOR - SMALL BUSINESS");
				put("53", "BUSINESS LOAN - PRIORITY SECTOR - AGRICULTURE");
				put("54", "BUSINESS LOAN - PRIORITY SECTOR - OTHERS");
				put("55", "BUSINESS NON-FUNDED CREDIT FACILITY - GENERAL");
				put("56", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - SMALL BUSINESS");
				put("57", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - AGRICULTURE");
				put("58", "BUSINESS NON-FUNDED CREDIT FACILITY - PRIORITY SECTOR - OTHERS");
				put("59", "BUSINESS LOANS AGAINST BANK DEPOSITS");
				put("60", "Staff Loan");
				put("00", "Other");
		}};
	
		Map<String, String> accountStatusMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;{
					put("00", "SF/WD/WO/SETTLED");
					put("30", "SF/WD/WO/SETTLED");
					put("31", "SF/WD/WO/SETTLED");
					put("32", "SF/WD/WO/SETTLED");
					put("33", "SF/WD/WO/SETTLED");
					put("34", "SF/WD/WO/SETTLED");
					put("35", "SF/WD/WO/SETTLED");
					put("36", "SF/WD/WO/SETTLED");
					put("37", "SF/WD/WO/SETTLED");
					put("38", "SF/WD/WO/SETTLED");
					put("39", "SF/WD/WO/SETTLED");
					put("40", "SF/WD/WO/SETTLED");
					put("41", "SF/WD/WO/SETTLED");
					put("42", "SF/WD/WO/SETTLED");
					put("43", "SF/WD/WO/SETTLED");
					put("44", "SF/WD/WO/SETTLED");
					put("45", "SF/WD/WO/SETTLED");
					put("46", "SF/WD/WO/SETTLED");
					put("47", "SF/WD/WO/SETTLED");
					put("48", "SF/WD/WO/SETTLED");
					put("49", "SF/WD/WO/SETTLED");
					put("50", "SF/WD/WO/SETTLED");
					put("51", "SF/WD/WO/SETTLED");
					put("52", "SF/WD/WO/SETTLED");
					put("53", "SF/WD/WO/SETTLED");
					put("54", "SF/WD/WO/SETTLED");
					put("55", "SF/WD/WO/SETTLED");
					put("56", "SF/WD/WO/SETTLED");
					put("57", "SF/WD/WO/SETTLED");
					put("58", "SF/WD/WO/SETTLED");
					put("59", "SF/WD/WO/SETTLED");
					put("60", "SF/WD/WO/SETTLED");
					put("61", "SF/WD/WO/SETTLED");
					put("62", "SF/WD/WO/SETTLED");
					put("63", "SF/WD/WO/SETTLED");
					put("64", "SF/WD/WO/SETTLED");
					put("65", "SF/WD/WO/SETTLED");
					put("66", "SF/WD/WO/SETTLED");
					put("67", "SF/WD/WO/SETTLED");
					put("68", "SF/WD/WO/SETTLED");
					put("69", "SF/WD/WO/SETTLED");
					put("70", "SF/WD/WO/SETTLED");
					put("72", "SF/WD/WO/SETTLED");
					put("73", "SF/WD/WO/SETTLED");
					put("74", "SF/WD/WO/SETTLED");
					put("75", "SF/WD/WO/SETTLED");
					put("76", "SF/WD/WO/SETTLED");
					put("77", "SF/WD/WO/SETTLED");
					put("79", "SF/WD/WO/SETTLED");
					put("81", "SF/WD/WO/SETTLED");
					put("85", "SF/WD/WO/SETTLED");
					put("86", "SF/WD/WO/SETTLED");
					put("87", "SF/WD/WO/SETTLED");
					put("88", "SF/WD/WO/SETTLED");
					put("90", "SF/WD/WO/SETTLED");
					put("91", "SF/WD/WO/SETTLED");
					put("93", "SF/WD/WO/SETTLED");
					put("97", "SF/WD/WO/SETTLED");
					put("11", "ACTIVE");
					put("21", "ACTIVE");
					put("22", "ACTIVE");
					put("23", "ACTIVE");
					put("24", "ACTIVE");
					put("25", "ACTIVE");
					put("71", "ACTIVE");
					put("78", "ACTIVE");
					put("80", "ACTIVE");
					put("82", "ACTIVE");
					put("83", "ACTIVE");
					put("84", "ACTIVE");
					put("13", "CLOSED");
					put("14", "CLOSED");
					put("15", "CLOSED");
					put("16", "CLOSED");
					put("17", "CLOSED");
		}};
		
		Map<String, String> accountHolderTypeNameMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;{
					put("1", "Individual");
					put("2", "Joint");
					put("3", "Authorized User");
					put("7", "Guarantor");
					put("otherwise", "Individual");
			}};
			
		Map<String, String> stateMap = new HashMap<String, String>(){
			private static final long serialVersionUID = 1L;{
					put("01", "JAMMU and KASHMIR");
					put("02", "HIMACHAL PRADESH");
					put("3 ", "PUNJAB");
					put("04", "CHANDIGARH");
					put("05", "UTTRANCHAL");
					put("06", "HARAYANA");
					put("07", "DELHI");
					put("08", "RAJASTHAN");
					put("09", "UTTAR PRADESH");
					put("10", "BIHAR");
					put("11", "SIKKIM");
					put("12", "ARUNACHAL PRADESH");
					put("13", "NAGALAND");
					put("14", "MANIPUR");
					put("15", "MIZORAM");
					put("16", "TRIPURA");
					put("17", "MEGHALAYA");
					put("18", "ASSAM");
					put("19", "WEST BENGAL");
					put("20", "JHARKHAND");
					put("21", "ORRISA");
					put("22", "CHHATTISGARH");
					put("23", "MADHYA PRADESH");
					put("24", "GUJRAT");
					put("25", "DAMAN and DIU");
					put("26", "DADARA and NAGAR HAVELI");
					put("27", "MAHARASHTRA");
					put("28", "ANDHRA PRADESH");
					put("29", "KARNATAKA");
					put("30", "GOA");
					put("31", "LAKSHADWEEP");
					put("32", "KERALA");
					put("33", "TAMIL NADU");
					put("34", "PONDICHERRY");
					put("35", "ANDAMAN and NICOBAR ISLANDS");
			}};	
			
			Map<String, String> frequencyOfPaymentMap = new HashMap<String, String>(){
				private static final long serialVersionUID = 1L;{
						put("D", "Deferred");
						put("P", "Single payment loan");
						put("W", "Weekly");
						put("B", "Bi-Weekly");
						put("E", "Semi-monthly");
						put("M", "Monthly");
						put("L", "2 monthly (bimonthly)");
						put("Q", "3 monthly (quarterly)");
				}};	
			
			Map<String, String> enquiryReasonMap = new HashMap<String, String>(){
				private static final long serialVersionUID = 1L;{
						put("01", "Agriculture Loan");
						put("02", "Auto Loan");
						put("3 ", "Business Loan");
						put("04", "Commercial Vehicle Loans");
						put("05", "Construction Equipment loan");
						put("06", "Consumer Loan");
						put("07", "Credit Card");
						put("08", "Education Loan");
						put("09", "Leasing");
						put("10", "Loan against collateral");
						put("11", "Microfinance");
						put("12", "Non-funded Credit Facility");
						put("13", "Personal Loan");
						put("14", "Property Loan");
						put("15", "Telecom");
						put("16", "Two/Three Wheeler Loan");
						put("17", "Working Capital Loan");
						put("99", "Others");
				}};	 
				 
				Map<String, String> financialPurposeMap = new HashMap<String, String>(){
					private static final long serialVersionUID = 1L;{
							put("01", "Agricultural Machinery");
							put("02", "Animal Husbandry");
							put("03", "Aquaculture");
							put("04", "Biogas Plant");
							put("05", "Crop Loan");
							put("06", "Horticulture");
							put("07", "Irrigation System");
							put("8 ", "New Car");
							put("9 ", "Overdraft against Car");
							put("10", "Used Car");
							put("11", "General");
							put("12", "Small & Medium Business");
							put("13", "Professionals");
							put("15", "Bus");
							put("16", "Tempo");
							put("17", "Tipper");
							put("18", "Truck");
							put("19", "Excavators");
							put("20", "Forklift");
							put("21", "Wheel Loaders");
							put("22", "Consumer Search");
							put("23", "Credit Card");
							put("24", "Fleet Card");
							put("25", "For Working Executives");
							put("26", "Study Abroad");
							put("27", "Study in India");
							put("28", "Leasing");
							put("29", "Bank Deposits");
							put("30", "Gold");
							put("31", "Govt. Bonds / PPF / NSC / KVP / FD");
							put("32", "Shares and Mutual Funds");
							put("33", "Business Loan");
							put("34", "Housing Loan");
							put("35", "Personal Loan");
							put("36", "Agriculture");
							put("37", "General");
							put("38", "Small Business");
							put("39", "Computers / Laptops");
							put("40", "Consumer Durables");
							put("41", "Marriage / Religious Ceremonies");
							put("42", "Travel");
							put("43", "Balance Transfer");
							put("44", "Home Improvement / Extension");
							put("45", "Land");
							put("46", "Lease Rental Discounting");
							put("47", "Loan against Property");
							put("48", "New Home");
							put("49", "Office Premises");
							put("50", "Under construction");
							put("51", "Broadband");
							put("52", "Landline");
							put("53", "Mobile");
							put("54", "Three Wheeler");
							put("55", "Two Wheeler");
							put("99", "Others");
					}};	 
				 
			Map<String, String> employmentStatusMap = new HashMap<String, String>(){
				private static final long serialVersionUID = 1L;{
						put("S", "Salaried");
						put("N", "Non-Salaried");
						put("E", "Self-employed");
						put("P", "Self-employed Professional");
						put("U", "Unemployed");
				}}; 
				
			Map<String, String> assetClassificationMap = new HashMap<String, String>(){
				private static final long serialVersionUID = 1L;{
					put("?","?");
					put("-1","?");
					put("1","S");
					put("01","S");
					put("S","S");
					put("2","B");
					put("02","B");
					put("B","B");
					put("3","D");
					put("03","D");
					put("D","D");
					put("4","L");
					put("04","L");
					put("L","L");
					put("5","M");
					put("05","M");
					put("M","M");
				}};
				
				Map<String, String> phoneTypeMap = new HashMap<String, String>(){
					private static final long serialVersionUID = 1L;{
						put("00","Unknown");
						put("01","Mobile Phone");
						put("02","Home Phone");
						put("03","Office Phone");
					}}; 
}                         
