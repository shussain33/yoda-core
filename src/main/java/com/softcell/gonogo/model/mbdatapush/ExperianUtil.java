package com.softcell.gonogo.model.mbdatapush;

import com.softcell.gonogo.model.los.ReverseConversionConstant;
import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;


public class ExperianUtil {


	private static Map<String, Map<String, String>> experianMetadata = new HashMap<String, Map<String, String>>() {{

		put(ReverseConversionConstant.ConversionConstant.MARITAL_STATUS, EXPERIAN_METADATA.maritalStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_STATUS, EXPERIAN_METADATA.accountStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_HOLDER_TYPE, EXPERIAN_METADATA.accountHolderTypeNameMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_STATUS, EXPERIAN_METADATA.accountStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ADDRESS_STATE, EXPERIAN_METADATA.stateMap);
		put(ReverseConversionConstant.ConversionConstant.FREQUENCY_OF_PAYMENT, EXPERIAN_METADATA.frequencyOfPaymentMap);
		put(ReverseConversionConstant.ConversionConstant.GENDER, EXPERIAN_METADATA.genderMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_TYPE, EXPERIAN_METADATA.accountTypeMap);
		put(ReverseConversionConstant.ConversionConstant.ENQUIRY_REASON, EXPERIAN_METADATA.enquiryReasonMap);
		put(ReverseConversionConstant.ConversionConstant.FINANCIAL_PURPOSE, EXPERIAN_METADATA.financialPurposeMap);
		put(ReverseConversionConstant.ConversionConstant.EMPLOYMENT_STATUS, EXPERIAN_METADATA.employmentStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ASSET_CLASSIFICATION, EXPERIAN_METADATA.assetClassificationMap);
		put(ReverseConversionConstant.ConversionConstant.PHONE_TYPE, EXPERIAN_METADATA.phoneTypeMap);

	}};



	private static Map<String, String> getCodeMap(Long institutionId, String sourceSystem, String mapForCode){
		Map<String, String> map = null;
		try {
			if (null != experianMetadata) {
				map = experianMetadata.get(mapForCode);
			}
		} catch (Exception e) {
			return null;
		}
		return map;
	}
	
 	
	public static String getValue(String code, String sourceSystem, Long institutionId, String mapForCode){
		if (null != code && StringUtils.isNotBlank(sourceSystem) && institutionId > 0) {
			Map<String, String> codeMap = getCodeMap(institutionId,sourceSystem,mapForCode);
			if(null != codeMap){
				if (codeMap.containsKey(code)) {
					return codeMap.get(code);
				}
			}
		}
		return code;
	}
	
	public static String format(String value) {
		if(StringUtils.isBlank(value) ){
			return "";
		}
		boolean stripped = false;
		if (StringUtils.endsWith(value, ".00") || StringUtils.endsWith(value, ".0")) {
			value = StringUtils.substringBeforeLast(value, ".");
			stripped = true;
			if(StringUtils.isBlank(value) || StringUtils.equalsIgnoreCase(value, "0")){
				return "0";
			}
		}
		if(StringUtils.isNumeric(value)){
			double parseDouble = Double.parseDouble(value);
			if(parseDouble < 1000) {
				return format("###", parseDouble);
		        
		    } else {
		        double hundreds = parseDouble % 1000;
		        int other = (int) (parseDouble / 1000);
		        return format(",##", other) + ',' + format("000", hundreds);
		       
		    }
		}else{
			return value;
		}
	}

	private static String format(String pattern, Object value) {
	    return new DecimalFormat(pattern).format(value);
	}
}
