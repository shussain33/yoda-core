/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import java.io.Serializable;

/**
 * @author Dipak
 * 
 */
public class HibRequestDomain implements Serializable {

	
	private String reqApplicationId;
	private String reqConsumerId;
	private String reqFirstName;
	private String reqMiddleName;
	private String reqLastName;
	private String reqName4;
	private String reqName5;
	private String reqGender;
	private String reqMaritalStatus;
	private String reqFatherName;
	private String reqSpouseName;
	private String reqMotherName;
	private String reqRelationType1;
	private String reqRelationType1Value;
	private String reqRelationType2;
	private String reqRelationType2Value;
	private String reqKeyPersonName;
	private String reqKeyPersonRelation;
	private String reqNomineeName;
	private String reqNomineeRelationType;
	private int	   reqAge;
	private String reqAgeAsOnDt;
	private String reqBirthDt;
	private String reqAddress1ResidenceCode;
	private String reqAddress1Type;
	private String reqAddress1;
	private String reqAddress1City;
	private String reqAddress1Pin;
	private String reqAddress1State;
	private String reqAddress2ResidenceCode;
	private String reqAddress2Type;
	private String reqAddress2;
	private String reqAddress2City;
	private String reqAddress2Pin;
	private String reqAddress2State;
	private String reqAddress3ResidenceCode;
	private String reqAddress3Type;
	private String reqAddress3;
	private String reqAddress3City;
	private String reqAddress3Pin;
	private String reqAddress3State;
	private String reqPanId;
	private String reqPassportId;
	private String reqVoterId;
	private String reqDrivingLicenseNo;
	private String reqUidNo;
	private String reqRationCard;
	private String reqIdType1;
	private String reqIdType1Value;
	private String reqIdType2;
	private String reqIdType2Value;
	private String reqPhoneType1;
	private String reqPhoneNumber1;
	private String reqPhoneType2;
	private String reqPhoneNumber2;
	private String reqPhoneType3;
	private String reqPhoneNumber3;
	private String reqPhoneType4;
	private String reqPhoneNumber4;
	private String reqEmailId1;
	private String reqEmailId;
	private String reqAlias;
	private String reqLoanType;
	private String reqLoanAmount;
	private String reqRequestType;
	private String reqLoanPurposeDesc;
	private String reqKendra;
	private String reqInquiryStage;
	private String reqAuthroizedBy;
	private String reqCredtInqPurpsTypDesc;
	private String reqCredtRptTrnDtTm;
	private String reqActOpeningDt;
	private String reqAccountNumber1;
	private String reqAccountNumber2;
	private String reqAccountNumber3;
	private String reqAccountNumber4;
	private String reqIndividualCorporateFlag;
	private String reqSoaFileNameC;
	private String reqTenure;
	private String reqConstitution;
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	
	public HibRequestDomain() {
		
		super();
	}

	public String getReqApplicationId() {
		return reqApplicationId;
	}

	public void setReqApplicationId(String reqApplicationId) {
		this.reqApplicationId = reqApplicationId;
	}

	public String getReqConsumerId() {
		return reqConsumerId;
	}

	public void setReqConsumerId(String reqConsumerId) {
		this.reqConsumerId = reqConsumerId;
	}

	public String getReqFirstName() {
		return reqFirstName;
	}

	public void setReqFirstName(String reqFirstName) {
		this.reqFirstName = reqFirstName;
	}

	public String getReqMiddleName() {
		return reqMiddleName;
	}

	public void setReqMiddleName(String reqMiddleName) {
		this.reqMiddleName = reqMiddleName;
	}

	public String getReqLastName() {
		return reqLastName;
	}

	public void setReqLastName(String reqLastName) {
		this.reqLastName = reqLastName;
	}

	public String getReqName4() {
		return reqName4;
	}

	public void setReqName4(String reqName4) {
		this.reqName4 = reqName4;
	}

	public String getReqName5() {
		return reqName5;
	}

	public void setReqName5(String reqName5) {
		this.reqName5 = reqName5;
	}

	public String getReqGender() {
		return reqGender;
	}

	public void setReqGender(String reqGender) {
		this.reqGender = reqGender;
	}

	public String getReqMaritalStatus() {
		return reqMaritalStatus;
	}

	public void setReqMaritalStatus(String reqMaritalStatus) {
		this.reqMaritalStatus = reqMaritalStatus;
	}

	public String getReqFatherName() {
		return reqFatherName;
	}

	public void setReqFatherName(String reqFatherName) {
		this.reqFatherName = reqFatherName;
	}

	public String getReqSpouseName() {
		return reqSpouseName;
	}

	public void setReqSpouseName(String reqSpouseName) {
		this.reqSpouseName = reqSpouseName;
	}

	public String getReqMotherName() {
		return reqMotherName;
	}

	public void setReqMotherName(String reqMotherName) {
		this.reqMotherName = reqMotherName;
	}

	public String getReqRelationType1() {
		return reqRelationType1;
	}

	public void setReqRelationType1(String reqRelationType1) {
		this.reqRelationType1 = reqRelationType1;
	}

	public String getReqRelationType1Value() {
		return reqRelationType1Value;
	}

	public void setReqRelationType1Value(String reqRelationType1Value) {
		this.reqRelationType1Value = reqRelationType1Value;
	}

	public String getReqRelationType2() {
		return reqRelationType2;
	}

	public void setReqRelationType2(String reqRelationType2) {
		this.reqRelationType2 = reqRelationType2;
	}

	public String getReqRelationType2Value() {
		return reqRelationType2Value;
	}

	public void setReqRelationType2Value(String reqRelationType2Value) {
		this.reqRelationType2Value = reqRelationType2Value;
	}

	public String getReqKeyPersonName() {
		return reqKeyPersonName;
	}

	public void setReqKeyPersonName(String reqKeyPersonName) {
		this.reqKeyPersonName = reqKeyPersonName;
	}

	public String getReqKeyPersonRelation() {
		return reqKeyPersonRelation;
	}

	public void setReqKeyPersonRelation(String reqKeyPersonRelation) {
		this.reqKeyPersonRelation = reqKeyPersonRelation;
	}

	public String getReqNomineeName() {
		return reqNomineeName;
	}

	public void setReqNomineeName(String reqNomineeName) {
		this.reqNomineeName = reqNomineeName;
	}

	public String getReqNomineeRelationType() {
		return reqNomineeRelationType;
	}

	public void setReqNomineeRelationType(String reqNomineeRelationType) {
		this.reqNomineeRelationType = reqNomineeRelationType;
	}

	public int getReqAge() {
		return reqAge;
	}

	public void setReqAge(int reqAge) {
		this.reqAge = reqAge;
	}

	public String getReqAgeAsOnDt() {
		return reqAgeAsOnDt;
	}

	public void setReqAgeAsOnDt(String reqAgeAsOnDt) {
		this.reqAgeAsOnDt = reqAgeAsOnDt;
	}

	public String getReqBirthDt() {
		return reqBirthDt;
	}

	public void setReqBirthDt(String reqBirthDt) {
		this.reqBirthDt = reqBirthDt;
	}

	public String getReqAddress1ResidenceCode() {
		return reqAddress1ResidenceCode;
	}

	public void setReqAddress1ResidenceCode(String reqAddress1ResidenceCode) {
		this.reqAddress1ResidenceCode = reqAddress1ResidenceCode;
	}

	public String getReqAddress1Type() {
		return reqAddress1Type;
	}

	public void setReqAddress1Type(String reqAddress1Type) {
		this.reqAddress1Type = reqAddress1Type;
	}

	public String getReqAddress1() {
		return reqAddress1;
	}

	public void setReqAddress1(String reqAddress1) {
		this.reqAddress1 = reqAddress1;
	}

	public String getReqAddress1City() {
		return reqAddress1City;
	}

	public void setReqAddress1City(String reqAddress1City) {
		this.reqAddress1City = reqAddress1City;
	}

	public String getReqAddress1Pin() {
		return reqAddress1Pin;
	}

	public void setReqAddress1Pin(String reqAddress1Pin) {
		this.reqAddress1Pin = reqAddress1Pin;
	}

	public String getReqAddress1State() {
		return reqAddress1State;
	}

	public void setReqAddress1State(String reqAddress1State) {
		this.reqAddress1State = reqAddress1State;
	}

	public String getReqAddress2ResidenceCode() {
		return reqAddress2ResidenceCode;
	}

	public void setReqAddress2ResidenceCode(String reqAddress2ResidenceCode) {
		this.reqAddress2ResidenceCode = reqAddress2ResidenceCode;
	}

	public String getReqAddress2Type() {
		return reqAddress2Type;
	}

	public void setReqAddress2Type(String reqAddress2Type) {
		this.reqAddress2Type = reqAddress2Type;
	}

	public String getReqAddress2() {
		return reqAddress2;
	}

	public void setReqAddress2(String reqAddress2) {
		this.reqAddress2 = reqAddress2;
	}

	public String getReqAddress2City() {
		return reqAddress2City;
	}

	public void setReqAddress2City(String reqAddress2City) {
		this.reqAddress2City = reqAddress2City;
	}

	public String getReqAddress2Pin() {
		return reqAddress2Pin;
	}

	public void setReqAddress2Pin(String reqAddress2Pin) {
		this.reqAddress2Pin = reqAddress2Pin;
	}

	public String getReqAddress2State() {
		return reqAddress2State;
	}

	public void setReqAddress2State(String reqAddress2State) {
		this.reqAddress2State = reqAddress2State;
	}

	public String getReqAddress3ResidenceCode() {
		return reqAddress3ResidenceCode;
	}

	public void setReqAddress3ResidenceCode(String reqAddress3ResidenceCode) {
		this.reqAddress3ResidenceCode = reqAddress3ResidenceCode;
	}

	public String getReqAddress3Type() {
		return reqAddress3Type;
	}

	public void setReqAddress3Type(String reqAddress3Type) {
		this.reqAddress3Type = reqAddress3Type;
	}

	public String getReqAddress3() {
		return reqAddress3;
	}

	public void setReqAddress3(String reqAddress3) {
		this.reqAddress3 = reqAddress3;
	}

	public String getReqAddress3City() {
		return reqAddress3City;
	}

	public void setReqAddress3City(String reqAddress3City) {
		this.reqAddress3City = reqAddress3City;
	}

	public String getReqAddress3Pin() {
		return reqAddress3Pin;
	}

	public void setReqAddress3Pin(String reqAddress3Pin) {
		this.reqAddress3Pin = reqAddress3Pin;
	}

	public String getReqAddress3State() {
		return reqAddress3State;
	}

	public void setReqAddress3State(String reqAddress3State) {
		this.reqAddress3State = reqAddress3State;
	}

	public String getReqPanId() {
		return reqPanId;
	}

	public void setReqPanId(String reqPanId) {
		this.reqPanId = reqPanId;
	}

	public String getReqPassportId() {
		return reqPassportId;
	}

	public void setReqPassportId(String reqPassportId) {
		this.reqPassportId = reqPassportId;
	}

	public String getReqVoterId() {
		return reqVoterId;
	}

	public void setReqVoterId(String reqVoterId) {
		this.reqVoterId = reqVoterId;
	}

	public String getReqDrivingLicenseNo() {
		return reqDrivingLicenseNo;
	}

	public void setReqDrivingLicenseNo(String reqDrivingLicenseNo) {
		this.reqDrivingLicenseNo = reqDrivingLicenseNo;
	}

	public String getReqUidNo() {
		return reqUidNo;
	}

	public void setReqUidNo(String reqUidNo) {
		this.reqUidNo = reqUidNo;
	}

	public String getReqRationCard() {
		return reqRationCard;
	}

	public void setReqRationCard(String reqRationCard) {
		this.reqRationCard = reqRationCard;
	}

	public String getReqIdType1() {
		return reqIdType1;
	}

	public void setReqIdType1(String reqIdType1) {
		this.reqIdType1 = reqIdType1;
	}

	public String getReqIdType1Value() {
		return reqIdType1Value;
	}

	public void setReqIdType1Value(String reqIdType1Value) {
		this.reqIdType1Value = reqIdType1Value;
	}

	public String getReqIdType2() {
		return reqIdType2;
	}

	public void setReqIdType2(String reqIdType2) {
		this.reqIdType2 = reqIdType2;
	}

	public String getReqIdType2Value() {
		return reqIdType2Value;
	}

	public void setReqIdType2Value(String reqIdType2Value) {
		this.reqIdType2Value = reqIdType2Value;
	}

	public String getReqPhoneType1() {
		return reqPhoneType1;
	}

	public void setReqPhoneType1(String reqPhoneType1) {
		this.reqPhoneType1 = reqPhoneType1;
	}

	public String getReqPhoneNumber1() {
		return reqPhoneNumber1;
	}

	public void setReqPhoneNumber1(String reqPhoneNumber1) {
		this.reqPhoneNumber1 = reqPhoneNumber1;
	}

	public String getReqPhoneType2() {
		return reqPhoneType2;
	}

	public void setReqPhoneType2(String reqPhoneType2) {
		this.reqPhoneType2 = reqPhoneType2;
	}

	public String getReqPhoneNumber2() {
		return reqPhoneNumber2;
	}

	public void setReqPhoneNumber2(String reqPhoneNumber2) {
		this.reqPhoneNumber2 = reqPhoneNumber2;
	}

	public String getReqPhoneType3() {
		return reqPhoneType3;
	}

	public void setReqPhoneType3(String reqPhoneType3) {
		this.reqPhoneType3 = reqPhoneType3;
	}

	public String getReqPhoneNumber3() {
		return reqPhoneNumber3;
	}

	public void setReqPhoneNumber3(String reqPhoneNumber3) {
		this.reqPhoneNumber3 = reqPhoneNumber3;
	}

	public String getReqPhoneType4() {
		return reqPhoneType4;
	}

	public void setReqPhoneType4(String reqPhoneType4) {
		this.reqPhoneType4 = reqPhoneType4;
	}

	public String getReqPhoneNumber4() {
		return reqPhoneNumber4;
	}

	public void setReqPhoneNumber4(String reqPhoneNumber4) {
		this.reqPhoneNumber4 = reqPhoneNumber4;
	}

	public String getReqEmailId1() {
		return reqEmailId1;
	}

	public void setReqEmailId1(String reqEmailId1) {
		this.reqEmailId1 = reqEmailId1;
	}

	public String getReqEmailId() {
		return reqEmailId;
	}

	public void setReqEmailId(String reqEmailId) {
		this.reqEmailId = reqEmailId;
	}

	public String getReqAlias() {
		return reqAlias;
	}

	public void setReqAlias(String reqAlias) {
		this.reqAlias = reqAlias;
	}

	public String getReqLoanType() {
		return reqLoanType;
	}

	public void setReqLoanType(String reqLoanType) {
		this.reqLoanType = reqLoanType;
	}

	public String getReqLoanAmount() {
		return reqLoanAmount;
	}

	public void setReqLoanAmount(String reqLoanAmount) {
		this.reqLoanAmount = reqLoanAmount;
	}

	public String getReqRequestType() {
		return reqRequestType;
	}

	public void setReqRequestType(String reqRequestType) {
		this.reqRequestType = reqRequestType;
	}

	public String getReqLoanPurposeDesc() {
		return reqLoanPurposeDesc;
	}

	public void setReqLoanPurposeDesc(String reqLoanPurposeDesc) {
		this.reqLoanPurposeDesc = reqLoanPurposeDesc;
	}

	public String getReqKendra() {
		return reqKendra;
	}

	public void setReqKendra(String reqKendra) {
		this.reqKendra = reqKendra;
	}

	public String getReqInquiryStage() {
		return reqInquiryStage;
	}

	public void setReqInquiryStage(String reqInquiryStage) {
		this.reqInquiryStage = reqInquiryStage;
	}

	public String getReqAuthroizedBy() {
		return reqAuthroizedBy;
	}

	public void setReqAuthroizedBy(String reqAuthroizedBy) {
		this.reqAuthroizedBy = reqAuthroizedBy;
	}

	public String getReqCredtInqPurpsTypDesc() {
		return reqCredtInqPurpsTypDesc;
	}

	public void setReqCredtInqPurpsTypDesc(String reqCredtInqPurpsTypDesc) {
		this.reqCredtInqPurpsTypDesc = reqCredtInqPurpsTypDesc;
	}

	public String getReqCredtRptTrnDtTm() {
		return reqCredtRptTrnDtTm;
	}

	public void setReqCredtRptTrnDtTm(String reqCredtRptTrnDtTm) {
		this.reqCredtRptTrnDtTm = reqCredtRptTrnDtTm;
	}

	public String getReqActOpeningDt() {
		return reqActOpeningDt;
	}

	public void setReqActOpeningDt(String reqActOpeningDt) {
		this.reqActOpeningDt = reqActOpeningDt;
	}

	public String getReqAccountNumber1() {
		return reqAccountNumber1;
	}

	public void setReqAccountNumber1(String reqAccountNumber1) {
		this.reqAccountNumber1 = reqAccountNumber1;
	}

	public String getReqAccountNumber2() {
		return reqAccountNumber2;
	}

	public void setReqAccountNumber2(String reqAccountNumber2) {
		this.reqAccountNumber2 = reqAccountNumber2;
	}

	public String getReqAccountNumber3() {
		return reqAccountNumber3;
	}

	public void setReqAccountNumber3(String reqAccountNumber3) {
		this.reqAccountNumber3 = reqAccountNumber3;
	}

	public String getReqAccountNumber4() {
		return reqAccountNumber4;
	}

	public void setReqAccountNumber4(String reqAccountNumber4) {
		this.reqAccountNumber4 = reqAccountNumber4;
	}

	public String getReqIndividualCorporateFlag() {
		return reqIndividualCorporateFlag;
	}

	public void setReqIndividualCorporateFlag(String reqIndividualCorporateFlag) {
		this.reqIndividualCorporateFlag = reqIndividualCorporateFlag;
	}

	public String getReqSoaFileNameC() {
		return reqSoaFileNameC;
	}

	public void setReqSoaFileNameC(String reqSoaFileNameC) {
		this.reqSoaFileNameC = reqSoaFileNameC;
	}

	public String getReqTenure() {
		return reqTenure;
	}

	public void setReqTenure(String reqTenure) {
		this.reqTenure = reqTenure;
	}

	public String getReqConstitution() {
		return reqConstitution;
	}

	public void setReqConstitution(String reqConstitution) {
		this.reqConstitution = reqConstitution;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "HibRequestDomain [reqApplicationId=" + reqApplicationId
				+ ", reqConsumerId=" + reqConsumerId + ", reqFirstName="
				+ reqFirstName + ", reqMiddleName=" + reqMiddleName
				+ ", reqLastName=" + reqLastName + ", reqName4=" + reqName4
				+ ", reqName5=" + reqName5 + ", reqGender=" + reqGender
				+ ", reqMaritalStatus=" + reqMaritalStatus + ", reqFatherName="
				+ reqFatherName + ", reqSpouseName=" + reqSpouseName
				+ ", reqMotherName=" + reqMotherName + ", reqRelationType1="
				+ reqRelationType1 + ", reqRelationType1Value="
				+ reqRelationType1Value + ", reqRelationType2="
				+ reqRelationType2 + ", reqRelationType2Value="
				+ reqRelationType2Value + ", reqKeyPersonName="
				+ reqKeyPersonName + ", reqKeyPersonRelation="
				+ reqKeyPersonRelation + ", reqNomineeName=" + reqNomineeName
				+ ", reqNomineeRelationType=" + reqNomineeRelationType
				+ ", reqAge=" + reqAge + ", reqAgeAsOnDt=" + reqAgeAsOnDt
				+ ", reqBirthDt=" + reqBirthDt + ", reqAddress1ResidenceCode="
				+ reqAddress1ResidenceCode + ", reqAddress1Type="
				+ reqAddress1Type + ", reqAddress1=" + reqAddress1
				+ ", reqAddress1City=" + reqAddress1City + ", reqAddress1Pin="
				+ reqAddress1Pin + ", reqAddress1State=" + reqAddress1State
				+ ", reqAddress2ResidenceCode=" + reqAddress2ResidenceCode
				+ ", reqAddress2Type=" + reqAddress2Type + ", reqAddress2="
				+ reqAddress2 + ", reqAddress2City=" + reqAddress2City
				+ ", reqAddress2Pin=" + reqAddress2Pin + ", reqAddress2State="
				+ reqAddress2State + ", reqAddress3ResidenceCode="
				+ reqAddress3ResidenceCode + ", reqAddress3Type="
				+ reqAddress3Type + ", reqAddress3=" + reqAddress3
				+ ", reqAddress3City=" + reqAddress3City + ", reqAddress3Pin="
				+ reqAddress3Pin + ", reqAddress3State=" + reqAddress3State
				+ ", reqPanId=" + reqPanId + ", reqPassportId=" + reqPassportId
				+ ", reqVoterId=" + reqVoterId + ", reqDrivingLicenseNo="
				+ reqDrivingLicenseNo + ", reqUidNo=" + reqUidNo
				+ ", reqRationCard=" + reqRationCard + ", reqIdType1="
				+ reqIdType1 + ", reqIdType1Value=" + reqIdType1Value
				+ ", reqIdType2=" + reqIdType2 + ", reqIdType2Value="
				+ reqIdType2Value + ", reqPhoneType1=" + reqPhoneType1
				+ ", reqPhoneNumber1=" + reqPhoneNumber1 + ", reqPhoneType2="
				+ reqPhoneType2 + ", reqPhoneNumber2=" + reqPhoneNumber2
				+ ", reqPhoneType3=" + reqPhoneType3 + ", reqPhoneNumber3="
				+ reqPhoneNumber3 + ", reqPhoneType4=" + reqPhoneType4
				+ ", reqPhoneNumber4=" + reqPhoneNumber4 + ", reqEmailId1="
				+ reqEmailId1 + ", reqEmailId=" + reqEmailId + ", reqAlias="
				+ reqAlias + ", reqLoanType=" + reqLoanType
				+ ", reqLoanAmount=" + reqLoanAmount + ", reqRequestType="
				+ reqRequestType + ", reqLoanPurposeDesc=" + reqLoanPurposeDesc
				+ ", reqKendra=" + reqKendra + ", reqInquiryStage="
				+ reqInquiryStage + ", reqAuthroizedBy=" + reqAuthroizedBy
				+ ", reqCredtInqPurpsTypDesc=" + reqCredtInqPurpsTypDesc
				+ ", reqCredtRptTrnDtTm=" + reqCredtRptTrnDtTm
				+ ", reqActOpeningDt=" + reqActOpeningDt
				+ ", reqAccountNumber1=" + reqAccountNumber1
				+ ", reqAccountNumber2=" + reqAccountNumber2
				+ ", reqAccountNumber3=" + reqAccountNumber3
				+ ", reqAccountNumber4=" + reqAccountNumber4
				+ ", reqIndividualCorporateFlag=" + reqIndividualCorporateFlag
				+ ", reqSoaFileNameC=" + reqSoaFileNameC + ", reqTenure="
				+ reqTenure + ", reqConstitution=" + reqConstitution + ", id="
				+ id + "]";
	}

	

	

}
