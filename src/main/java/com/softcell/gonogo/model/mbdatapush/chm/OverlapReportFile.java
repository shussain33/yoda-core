package com.softcell.gonogo.model.mbdatapush.chm;

public class OverlapReportFile {
	private InquiryStatus inquiryStatus;
	private OverlapReports overlapReports;

	public InquiryStatus getInquiryStatus() {
		return inquiryStatus;
	}
	public void setInquiryStatus(InquiryStatus inquiryStatus) {
		this.inquiryStatus = inquiryStatus;
	}
	public OverlapReports getOverlapReports() {
		return overlapReports;
	}
	public void setOverlapReports(OverlapReports overlapReports) {
		this.overlapReports = overlapReports;
	}
	@Override
	public String toString() {
		return "OverlapReportFile [inquiryStatus=" + inquiryStatus
				+ ", overlapReports=" + overlapReports + "]";
	}
}
