package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class ExperianEROPDomain implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("SRNO")
	private Long srno ;
	@Expose
	@SerializedName("SOURCE_NAME")
	private String sourceName;
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private String memberrefrenceNumber;
	@Expose
	@SerializedName("ENQUIRY_DATE")
	private Date enquiryDate;
	@Expose
	@SerializedName("SYSTEMCODE")
	private String systemCode;
	@Expose
	@SerializedName("MESSAGETEXT")
	private String messageText;
	@Expose
	@SerializedName("REPORTDATE")
	private String reportDate;
	@Expose
	@SerializedName("REPORTTIME")
	private String reportTime;
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private String  outputWriteFlag_;        
	@Expose
	@SerializedName("OUTPUT_WRITE_DATE")
	private String  outputWriteDate_;        
	@Expose
	@SerializedName("OUTPUT_READ_TIME")
	private String  outputReadTime_; 
	
	public ExperianEROPDomain(Long srNo,String sourceName,String memberRefrenceNumber,Date enquiryDate) {
		this.srno = srNo;
		this.sourceName = sourceName;
		this.memberrefrenceNumber = memberRefrenceNumber;
		this.enquiryDate = enquiryDate;
	}

	
	public ExperianEROPDomain() {
		// TODO Auto-generated constructor stub
	}


	public String getSystemCode() {
		return systemCode;
	}
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getReportTime() {
		return reportTime;
	}
	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}
	public Long getSrno() {
		return srno;
	}
	public void setSrno(Long srno) {
		this.srno = srno;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	public String getMemberrefrenceNumber() {
		return memberrefrenceNumber;
	}
	public void setMemberrefrenceNumber(String memberrefrenceNumber) {
		this.memberrefrenceNumber = memberrefrenceNumber;
	}
	public Date getEnquiryDate() {
		return enquiryDate;
	}
	public void setEnquiryDate(Date enquiryDate) {
		this.enquiryDate = enquiryDate;
	}


	public String getOutputWriteFlag_() {
		return outputWriteFlag_;
	}


	public void setOutputWriteFlag_(String outputWriteFlag_) {
		this.outputWriteFlag_ = outputWriteFlag_;
	}


	public String getOutputWriteDate_() {
		return outputWriteDate_;
	}


	public void setOutputWriteDate_(String outputWriteDate_) {
		this.outputWriteDate_ = outputWriteDate_;
	}


	public String getOutputReadTime_() {
		return outputReadTime_;
	}


	public void setOutputReadTime_(String outputReadTime_) {
		this.outputReadTime_ = outputReadTime_;
	}


	@Override
	public String toString() {
		return "ExperianEROPDomain [srno=" + srno + ", sourceName="
				+ sourceName + ", memberrefrenceNumber=" + memberrefrenceNumber
				+ ", enquiryDate=" + enquiryDate + ", systemCode=" + systemCode
				+ ", messageText=" + messageText + ", reportDate=" + reportDate
				+ ", reportTime=" + reportTime + ", outputWriteFlag_="
				+ outputWriteFlag_ + ", outputWriteDate_=" + outputWriteDate_
				+ ", outputReadTime_=" + outputReadTime_ + "]";
	}
	
}
