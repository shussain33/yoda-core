package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.List;



public class PanVariations {
	
	private List<Variation> variations;


	public List<Variation> getVariations() {
		return variations;
	}
	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}

	@Override
	public String toString() {
		return "PanVariations [variations=" + variations + "]";
	}

 
	
	

}
