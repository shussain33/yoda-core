package com.softcell.gonogo.model.mbdatapush.chm;

public class Variation {
	private String value;
	private String reportedDate;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getReportedDate() {
		return reportedDate;
	}
	public void setReportedDate(String reportedDate) {
		this.reportedDate = reportedDate;
	}
	@Override
	public String toString() {
		return "Variation [value=" + value + ", reportedDate=" + reportedDate
				+ "]";
	}
	
}
