package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.List;

public class SecondaryMatches {

	private List<SecondaryMatch> secondaryMatchList ;

	public List<SecondaryMatch> getSecondaryMatchList() {
		return secondaryMatchList;
	}

	public void setSecondaryMatchList(List<SecondaryMatch> secondaryMatchList) {
		this.secondaryMatchList = secondaryMatchList;
	}

	@Override
	public String toString() {
		return "SecondaryMatches [secondaryMatchList=" + secondaryMatchList
				+ "]";
	}
}
