package com.softcell.gonogo.model.mbdatapush;

import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.los.ReverseConversionConstant;
import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class CHMUtils {


	private static Map<String, Map<String, String>> highmarkMetadata = new HashMap<String, Map<String, String>>() {{

		put(ReverseConversionConstant.ConversionConstant.MARITAL_STATUS, EXPERIAN_METADATA.maritalStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_STATUS, EXPERIAN_METADATA.accountStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_HOLDER_TYPE, EXPERIAN_METADATA.accountHolderTypeNameMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_STATUS, EXPERIAN_METADATA.accountStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ADDRESS_STATE, EXPERIAN_METADATA.stateMap);
		put(ReverseConversionConstant.ConversionConstant.FREQUENCY_OF_PAYMENT, EXPERIAN_METADATA.frequencyOfPaymentMap);
		put(ReverseConversionConstant.ConversionConstant.GENDER, EXPERIAN_METADATA.genderMap);
		put(ReverseConversionConstant.ConversionConstant.ACCOUNT_TYPE, EXPERIAN_METADATA.accountTypeMap);
		put(ReverseConversionConstant.ConversionConstant.ENQUIRY_REASON, EXPERIAN_METADATA.enquiryReasonMap);
		put(ReverseConversionConstant.ConversionConstant.FINANCIAL_PURPOSE, EXPERIAN_METADATA.financialPurposeMap);
		put(ReverseConversionConstant.ConversionConstant.EMPLOYMENT_STATUS, EXPERIAN_METADATA.employmentStatusMap);
		put(ReverseConversionConstant.ConversionConstant.ASSET_CLASSIFICATION, EXPERIAN_METADATA.assetClassificationMap);
		put(ReverseConversionConstant.ConversionConstant.PHONE_TYPE, EXPERIAN_METADATA.phoneTypeMap);

	}};


	private static Map<String, String> getCodeMap(Long institutionId, String sourceSystem, String mapForCode){
		Map<String, String> map = new HashMap<String, String>();
		Map<String,Map<String, String>> tempMap = null;
		try {
			
			/*tempMap = LookUpService.getTransLookValues(GenConsts.Highmark.NAME, GenConsts.Highmark.BASE2_PRODUCT, "2.0", "REQUEST", institutionId, sourceSystem);*/
			if(null != tempMap){
				map = tempMap.get(mapForCode);
			}else{
				return null;
			}
			
		} catch (Exception e) {
			return null;
		}
		return map;
	}
	
 	
	public static String getValue(String code, String sourceSystem, Long institutionId, String mapForCode){
		Map<String, String> codeMap = getCodeMap(institutionId,sourceSystem,mapForCode);
		if (null != code) {
			if(null != codeMap){
				if (codeMap.containsKey(StringUtils.lowerCase(code))) {
					return codeMap.get(StringUtils.lowerCase(code));
				}
			}
		}
		return code;
	}

	public static  List<Map<String, String>> getDpd(String refDate, List<String> pmtHist) {
		List<Map<String, String>>  pmtHistLst = new ArrayList<Map<String,String>>();
		Date time = DateUtils.getDateFromString(refDate, "ddMMyyyy");
		Calendar c = Calendar.getInstance();
		DateFormat sdf = new SimpleDateFormat("dd-MM-yy");
		if(null != pmtHist && pmtHist.size() > 0){
			for(String st : pmtHist){
				if(StringUtils.isNotBlank(st)){
					int length = st.length();
					int size = 3;
					for (int start = 0; start < length ; start += size) {
						Map<String, String> payMap = new HashMap<String, String>();

						String substring = st.substring(start, Math.min(length, start+size));
						payMap.put("dpd", substring);

						String  monthVal  ="";
					    if(StringUtils.isNotBlank(refDate)){
					    	String format = sdf.format(time);
					    	String[] split = StringUtils.split(format, "-");
					    	monthVal = split[1]+"-"+split[2];

					    	c.setTime(time);
					    	c.add(Calendar.MONTH, -1);
					    	time = c.getTime();
					    }
					    payMap.put("month", monthVal);
					    pmtHistLst.add(payMap);

					}
				}
			}
		}

		if(pmtHistLst.size() < 18){
			while(pmtHistLst.size()!=18){
				Map<String, String> payMap = new HashMap<String, String>();
				pmtHistLst.add(payMap);
			}
			return pmtHistLst;
		}

		if(pmtHistLst.size() < 36){
			while(pmtHistLst.size() != 36){
				Map<String, String> payMap = new HashMap<String, String>();
				pmtHistLst.add(payMap);
			}
			return pmtHistLst;
		}
		return pmtHistLst;
	}

	public static Long DateDiff(String startdate,String enddate)
	{
		int sday=0;
		int smonth=0;
		int syear=0;
		int eday=0;
		int emonth=0;
		int eyear=0;
		 sday=Integer.parseInt(startdate.split("-")[0]);
		 smonth=Integer.parseInt(startdate.split("-")[1]);
		 syear=Integer.parseInt(startdate.split("-")[2]);

		 eday=Integer.parseInt(enddate.split("-")[0]);
		 emonth=Integer.parseInt(enddate.split("-")[1]);
		 eyear=Integer.parseInt(enddate.split("-")[2]);

		  Calendar calendar1 = Calendar.getInstance();
	      Calendar calendar2 = Calendar.getInstance();
	      calendar1.set(syear, smonth, sday);
	      calendar2.set(eyear, emonth, eday);

	      long milsecs1= calendar1.getTimeInMillis();
	      long milsecs2 = calendar2.getTimeInMillis();
	      long diff = milsecs2 - milsecs1;
	      long ddays = diff / (24 * 60 * 60 * 1000);
		return ddays;
	}


	/*public static CalculatedDomain getCalculatedDomain (List<CibilRespAccount> accntList , List<CibilRespEnquiry> inquiryList , String endDate){
		try{
			CalculatedDomain  calculatedDomain =  new CalculatedDomain();

			Long accountCounter = 0L;
			Long overdueCounter = 0L;
			Long sanctionedAmt = 0L;
			Long currentBalanceAmt =0L;
			Long overdueAmt = 0L;
			Long zeroBalanceCounter = 0L;
			Long inquiryCounter = 0L;
			Long inquiryPast30Counter =0L;
			Long inquiryPast12Month = 0L;
			Long inquiryPast24Month = 0L;
			Long inquiryPast60Counter =0L;

			List<Date> accountDates = new ArrayList<Date>();
			List<Date> inquiryDates = new ArrayList<Date>();
			List<Date> accountClosedDates = new ArrayList<Date>();

			if(null != accntList && accntList.size() >  0){

				for (CibilRespAccount cibilRespAccount : accntList) {

					Date accountOpenedDate = DateUtils.getDateFromString(cibilRespAccount.getDateOpenedOrDisbursed(), "ddMMyyyy");
					if(accountOpenedDate!=null){
						accountDates.add(accountOpenedDate);
					}

					Date accountClosedDate = DateUtils.getDateFromString(cibilRespAccount.getDateClosed(), "ddMMyyyy");
					if(accountClosedDate!=null){
						accountClosedDates.add(accountClosedDate);
					}

					accountCounter++;

					String overdueAmount = cibilRespAccount.getOverdueAmount();

					if(StringUtils.isNotBlank(overdueAmount) && StringUtils.isNumeric(overdueAmount)){
						overdueCounter++;
						overdueAmt += Long.parseLong(overdueAmount);
					}

					String highCreditOrSanctionedAmount = cibilRespAccount.getHighCreditOrSanctionedAmount();
					if(StringUtils.isNotBlank(highCreditOrSanctionedAmount) && StringUtils.isNumeric(highCreditOrSanctionedAmount)){
						sanctionedAmt += Long.parseLong(highCreditOrSanctionedAmount);
					}

					String currentBalance = cibilRespAccount.getCurrentBalance();

					if(StringUtils.isNotBlank(currentBalance) && StringUtils.isNumeric(currentBalance)){
						Long balance = Long.parseLong(currentBalance);
						if(balance == 0){
							zeroBalanceCounter++;
						}
						currentBalanceAmt += balance;
					}
				}
			}

			try{

				Collections.sort(accountDates);
				if(accountDates.size() > 0){
					SimpleDateFormat sdf =  new SimpleDateFormat("dd-MM-yyyy");
					calculatedDomain.setRecentAccountDt(sdf.format(accountDates.get(accountDates.size()-1)));
					calculatedDomain.setOldestAccountDt(sdf.format(accountDates.get(0)));
				}

			}catch(Exception e ){
				e.printStackTrace();
			}

			try{

				Collections.sort(accountClosedDates);
				if(accountClosedDates.size() > 0){
					SimpleDateFormat sdf =  new SimpleDateFormat("dd-MM-yyyy");
					calculatedDomain.setRecentClosedDt(sdf.format(accountClosedDates.get(accountClosedDates.size()-1)));
				}

			}catch(Exception e ){
				e.printStackTrace();
			}

			if(null != inquiryList && inquiryList.size() > 0){

				for (CibilRespEnquiry resEnquiry : inquiryList) {

					Date inquiryDate = DateUtils.getDateFromString(resEnquiry.getDateReported(), "ddMMyyyy");
					if(inquiryDate!=null){
						inquiryDates.add(inquiryDate);
					}

					inquiryCounter++;
					String dateReported = resEnquiry.getDateReported();
					dateReported = DateUtils.convert(dateReported, "ddMMyyyy", "dd-MM-yyyy");
					Long dateDiff = DateDiff(dateReported,endDate);
					if(dateDiff<30){
						inquiryPast30Counter++;
					}
					if(dateDiff>30 && dateDiff<60){
						inquiryPast60Counter++;
					}
					if(dateDiff>30 && dateDiff<365){
						inquiryPast12Month++;
					}
					if(dateDiff>365 && dateDiff<730){
						inquiryPast24Month++;
					}
				}
			}

			calculatedDomain.setAccountCounter(accountCounter);
			calculatedDomain.setCurrentBalanceAmt(format(currentBalanceAmt+""));
			calculatedDomain.setOverdueAmt(format(overdueAmt+""));
			calculatedDomain.setOverdueCounter(overdueCounter);
			calculatedDomain.setSanctionedAmt(format(sanctionedAmt+""));
			calculatedDomain.setZeroBalanceCounter(zeroBalanceCounter);
			calculatedDomain.setInquiryCounter(inquiryCounter);
			calculatedDomain.setInquiryPast30(inquiryPast30Counter);
			calculatedDomain.setInquiryPast12Month(inquiryPast12Month);
			calculatedDomain.setInquiryPast24Month(inquiryPast24Month);
			calculatedDomain.setInquiryPast60(inquiryPast60Counter);
			try{
				Collections.sort(inquiryDates);
				if(inquiryDates.size() > 0){
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
					calculatedDomain.setRecentInquiryDt(sdf.format(inquiryDates.get(inquiryDates.size()-1)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			return calculatedDomain;

		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/

	public static String format(String value) {
		if(StringUtils.isBlank(value) ){
			return "";
		}
		boolean stripped = false;
		if (StringUtils.endsWith(value, ".00") || StringUtils.endsWith(value, ".0")) {
			value = StringUtils.substringBeforeLast(value, ".");
			stripped = true;
			if(StringUtils.isBlank(value) || StringUtils.equalsIgnoreCase(value, "0")){
				return "0";
			}
		}
		if(StringUtils.isNumeric(value)){
			double parseDouble = Double.parseDouble(value);
			if(parseDouble < 1000) {
				return format("###", parseDouble);
		        
		    } else {
		        double hundreds = parseDouble % 1000;
		        int other = (int) (parseDouble / 1000);
		        return format(",##", other) + ',' + format("000", hundreds);
		       
		    }
		}else{
			return value;
		}
	}

	private static String format(String pattern, Object value) {
	    return new DecimalFormat(pattern).format(value);
	}
}
