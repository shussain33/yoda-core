/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author Dipak
 * 
 */
public class HibHighmarkBaseEropDomain implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	@Expose
	@SerializedName("SRNO")
	private Integer srNo;
	@Expose
	@SerializedName("SOA_SOURCE_NAME")
	private String soaSourceName;
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private String memberReferenceNumber;

	@Expose
	@SerializedName("DATE_OF_REQUEST")
	private Date dateOfRequest;
	@Expose
	@SerializedName("PREPARED_FOR")
	private String preparedFor;
	@Expose
	@SerializedName("PREPARED_FOR_ID")
	private String preparedForId;
	@Expose
	@SerializedName("DATE_OF_ISSUE")
	private Date dateOfIssue;
	@Expose
	@SerializedName("REPORT_ID")
	private String reportId;
	@Expose
	@SerializedName("BATCH_ID")
	private BigInteger batchId;
	@Expose
	@SerializedName("STATUS")
	private String status;
	@Expose
	@SerializedName("ERROR")
	private String error;
	
	@Expose
	@SerializedName("NAME")
	private String name;
	@Expose
	@SerializedName("AKA")
	private String aka;
	@Expose
	@SerializedName("SPOUSE")
	private String spouse;
	@Expose
	@SerializedName("FATHER")
	private String father;
	@Expose
	@SerializedName("MOTHER")
	private String mother;
	@Expose
	@SerializedName("DOB")
	private Date dobIQ;
	@Expose
	@SerializedName("AGE")
	private Integer age;
	@Expose
	@SerializedName("AGE_AS_ON")
	private String ageAsOn;
	@Expose
	@SerializedName("RATION_CARD")
	private String rationCard;
	@Expose
	@SerializedName("PASSPORT")
	private String passport;
	@Expose
	@SerializedName("VOTER_ID")
	private String votersId;
	@Expose
	@SerializedName("DRIVING_LICENSE_NO")
	private String drivingLicenceNo;
	@Expose
	@SerializedName("PAN")
	private String pan;
	@Expose
	@SerializedName("GENDER")
	private String gender;
	@Expose
	@SerializedName("OWNERSHIP")
	private String ownership;
	@Expose
	@SerializedName("ADDRESS_1")
	private String address1;
	@Expose
	@SerializedName("ADDRESS_2")
	private String address2;
	@Expose
	@SerializedName("ADDRESS_3")
	private String address3;
	@Expose
	@SerializedName("PHONE_1")
	private BigInteger phone1;
	@Expose
	@SerializedName("PHONE_2")
	private BigInteger phone2;
	@Expose
	@SerializedName("PHONE_3")
	private BigInteger phone3;
	@Expose
	@SerializedName("EMAIL_1")
	private String emailId1;
	@Expose
	@SerializedName("EMAIL_2")
	private String emailId2;
	@Expose
	@SerializedName("BRANCH")
	private String branch;
	@Expose
	@SerializedName("KENDRA")
	private String kendra;
	@Expose
	@SerializedName("MBR_ID")
	private String mbrId;
	@Expose
	@SerializedName("LOS_APP_ID")
	private String losAppId;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP")
	private String creditInquiryPurposeType;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP_DESC")
	private String creditInquiryPurposeTypeDesc;
	@Expose
	@SerializedName("CREDIT_INQUIRY_STAGE")
	private String creditInquiryStage;
	@Expose
	@SerializedName("CREDT_RPT_ID")
	private String creditReportId;
	@Expose
	@SerializedName("CREDT_REQ_TYP")
	private String creditRequestType;
	@Expose
	@SerializedName("CREDT_RPT_TRN_DT_TM")
	private String creditReportTransectionDateTime;
	@Expose
	@SerializedName("AC_OPEN_DT")
	private String accountOpenDate;
	@Expose
	@SerializedName("LOAN_AMOUNT")
	private BigInteger loanAmount;
	@Expose
	@SerializedName("ENTITY_ID")
	private String entityId;
	@Expose
	@SerializedName("RESPONSE_TYPE")
	private String responseType;
	@Expose
	@SerializedName("ACK_CODE")
	private String ackCode;
	@Expose
	@SerializedName("ACK_DESCRIPTION")
	private String ackDesc;
	
	
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private String outputWriteFlag;
	@Expose
	@SerializedName("OUTPUT_WRITE_TIME")
	private String outputWritetime;
	@Expose
	@SerializedName("OUTPUT_READ_TIME")
	private String outputReadTime;
	
	public HibHighmarkBaseEropDomain(Integer srNumber, String soaSourceName, String memberReferenceNumber, String soaFileName) {
		this.srNo = srNumber;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = memberReferenceNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSrNo() {
		return srNo;
	}

	public void setSrNo(Integer srNo) {
		this.srNo = srNo;
	}

	public String getSoaSourceName() {
		return soaSourceName;
	}

	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}

	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}

	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}

	public Date getDateOfRequest() {
		return dateOfRequest;
	}

	public void setDateOfRequest(Date dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}

	public String getPreparedFor() {
		return preparedFor;
	}

	public void setPreparedFor(String preparedFor) {
		this.preparedFor = preparedFor;
	}

	public String getPreparedForId() {
		return preparedForId;
	}

	public void setPreparedForId(String preparedForId) {
		this.preparedForId = preparedForId;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public BigInteger getBatchId() {
		return batchId;
	}

	public void setBatchId(BigInteger batchId) {
		this.batchId = batchId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAka() {
		return aka;
	}

	public void setAka(String aka) {
		this.aka = aka;
	}

	public String getSpouse() {
		return spouse;
	}

	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	public String getFather() {
		return father;
	}

	public void setFather(String father) {
		this.father = father;
	}

	public String getMother() {
		return mother;
	}

	public void setMother(String mother) {
		this.mother = mother;
	}

	public Date getDobIQ() {
		return dobIQ;
	}

	public void setDobIQ(Date dobIQ) {
		this.dobIQ = dobIQ;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAgeAsOn() {
		return ageAsOn;
	}

	public void setAgeAsOn(String ageAsOn) {
		this.ageAsOn = ageAsOn;
	}

	public String getRationCard() {
		return rationCard;
	}

	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getVotersId() {
		return votersId;
	}

	public void setVotersId(String votersId) {
		this.votersId = votersId;
	}

	public String getDrivingLicenceNo() {
		return drivingLicenceNo;
	}

	public void setDrivingLicenceNo(String drivingLicenceNo) {
		this.drivingLicenceNo = drivingLicenceNo;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOwnership() {
		return ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public BigInteger getPhone1() {
		return phone1;
	}

	public void setPhone1(BigInteger phone1) {
		this.phone1 = phone1;
	}

	public BigInteger getPhone2() {
		return phone2;
	}

	public void setPhone2(BigInteger phone2) {
		this.phone2 = phone2;
	}

	public BigInteger getPhone3() {
		return phone3;
	}

	public void setPhone3(BigInteger phone3) {
		this.phone3 = phone3;
	}

	public String getEmailId1() {
		return emailId1;
	}

	public void setEmailId1(String emailId1) {
		this.emailId1 = emailId1;
	}

	public String getEmailId2() {
		return emailId2;
	}

	public void setEmailId2(String emailId2) {
		this.emailId2 = emailId2;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getKendra() {
		return kendra;
	}

	public void setKendra(String kendra) {
		this.kendra = kendra;
	}

	public String getMbrId() {
		return mbrId;
	}

	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}

	public String getLosAppId() {
		return losAppId;
	}

	public void setLosAppId(String losAppId) {
		this.losAppId = losAppId;
	}

	public String getCreditInquiryPurposeType() {
		return creditInquiryPurposeType;
	}

	public void setCreditInquiryPurposeType(String creditInquiryPurposeType) {
		this.creditInquiryPurposeType = creditInquiryPurposeType;
	}

	public String getCreditInquiryPurposeTypeDesc() {
		return creditInquiryPurposeTypeDesc;
	}

	public void setCreditInquiryPurposeTypeDesc(String creditInquiryPurposeTypeDesc) {
		this.creditInquiryPurposeTypeDesc = creditInquiryPurposeTypeDesc;
	}

	public String getCreditInquiryStage() {
		return creditInquiryStage;
	}

	public void setCreditInquiryStage(String creditInquiryStage) {
		this.creditInquiryStage = creditInquiryStage;
	}

	public String getCreditReportId() {
		return creditReportId;
	}

	public void setCreditReportId(String creditReportId) {
		this.creditReportId = creditReportId;
	}

	public String getCreditRequestType() {
		return creditRequestType;
	}

	public void setCreditRequestType(String creditRequestType) {
		this.creditRequestType = creditRequestType;
	}

	public String getCreditReportTransectionDateTime() {
		return creditReportTransectionDateTime;
	}

	public void setCreditReportTransectionDateTime(
			String creditReportTransectionDateTime) {
		this.creditReportTransectionDateTime = creditReportTransectionDateTime;
	}

	public String getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(String accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public BigInteger getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(BigInteger loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getAckCode() {
		return ackCode;
	}

	public void setAckCode(String ackCode) {
		this.ackCode = ackCode;
	}

	public String getAckDesc() {
		return ackDesc;
	}

	public void setAckDesc(String ackDesc) {
		this.ackDesc = ackDesc;
	}

	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}

	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}

	public String getOutputWritetime() {
		return outputWritetime;
	}

	public void setOutputWritetime(String outputWritetime) {
		this.outputWritetime = outputWritetime;
	}

	public String getOutputReadTime() {
		return outputReadTime;
	}

	public void setOutputReadTime(String outputReadTime) {
		this.outputReadTime = outputReadTime;
	}

	@Override
	public String toString() {
		return "HibHighmarkBaseEropDomain [id=" + id + ", srNo=" + srNo
				+ ", soaSourceName=" + soaSourceName
				+ ", memberReferenceNumber=" + memberReferenceNumber
				+ ", dateOfRequest=" + dateOfRequest + ", preparedFor="
				+ preparedFor + ", preparedForId=" + preparedForId
				+ ", dateOfIssue=" + dateOfIssue + ", reportId=" + reportId
				+ ", batchId=" + batchId + ", status=" + status + ", error="
				+ error + ", name=" + name + ", aka=" + aka + ", spouse="
				+ spouse + ", father=" + father + ", mother=" + mother
				+ ", dobIQ=" + dobIQ + ", age=" + age + ", ageAsOn=" + ageAsOn
				+ ", rationCard=" + rationCard + ", passport=" + passport
				+ ", votersId=" + votersId + ", drivingLicenceNo="
				+ drivingLicenceNo + ", pan=" + pan + ", gender=" + gender
				+ ", ownership=" + ownership + ", address1=" + address1
				+ ", address2=" + address2 + ", address3=" + address3
				+ ", phone1=" + phone1 + ", phone2=" + phone2 + ", phone3="
				+ phone3 + ", emailId1=" + emailId1 + ", emailId2=" + emailId2
				+ ", branch=" + branch + ", kendra=" + kendra + ", mbrId="
				+ mbrId + ", losAppId=" + losAppId
				+ ", creditInquiryPurposeType=" + creditInquiryPurposeType
				+ ", creditInquiryPurposeTypeDesc="
				+ creditInquiryPurposeTypeDesc + ", creditInquiryStage="
				+ creditInquiryStage + ", creditReportId=" + creditReportId
				+ ", creditRequestType=" + creditRequestType
				+ ", creditReportTransectionDateTime="
				+ creditReportTransectionDateTime + ", accountOpenDate="
				+ accountOpenDate + ", loanAmount=" + loanAmount
				+ ", entityId=" + entityId + ", responseType=" + responseType
				+ ", ackCode=" + ackCode + ", ackDesc=" + ackDesc
				+ ", outputWriteFlag=" + outputWriteFlag + ", outputWritetime="
				+ outputWritetime + ", outputReadTime=" + outputReadTime + "]";
	}

}
