/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class CHMBaseEropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("CHM_BASE_EROP_DOMAIN_LIST")
	List<HibHighmarkBaseEropDomain> hmBaseEropDomainList;
	
	public List<HibHighmarkBaseEropDomain> getHmBaseEropDomainList() {
		return hmBaseEropDomainList;
	}
	public void setHmBaseEropDomainList(
			List<HibHighmarkBaseEropDomain> hmBaseEropDomainList) {
		this.hmBaseEropDomainList = hmBaseEropDomainList;
	}

	@Override
	public String toString() {
		return "CHMBaseEropDomainObject [hmBaseEropDomainList="
				+ hmBaseEropDomainList + "]";
	}
}
