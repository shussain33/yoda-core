package com.softcell.gonogo.model.mbdatapush.chm;

public interface HM_ADV_OVERLAP_RES_CONST {
	
	public interface ROOT{
		public static final String OVERLAP_REPORT_FILE = "OVERLAP-REPORT-FILE";
	}
	
	public interface  OVERLAP_REPORT_FILE{
		public static final String INQUIRY_STATUS = "INQUIRY-STATUS";
		public static final String OVERLAP_REPORTS = "OVERLAP-REPORTS";
	} 
	
	public interface INQUIRY_STATUS {
		public static final String INQUIRY = "INQUIRY";
		public static final String ERRORS = "ERRORS";
	}
	
	public interface ERRORS{
		public static final String ERROR = "ERROR";
	}
	
	public interface ERROR{
		public static final String CODE = "CODE";
		public static final String DESCRIPTION = "DESCRIPTION";
	}
	
	public interface INQUIRY{
		public static final String  INQUIRY_UNIQUE_REF_NO = "INQUIRY-UNIQUE-REF-NO";
		public static final String  REQUEST_DT_TM         = "REQUEST-DT-TM";
		public static final String  RESPONSE_DT_TM        = "RESPONSE-DT-TM";
		public static final String  RESPONSE_TYPE         = "RESPONSE-TYPE";
		public static final String  DESCRIPTION           = "DESCRIPTION";
		public static final String REPORT_ID 			  = "REPORT-ID";
	}
	
	
	
	public interface OVERLAP_REPORTS{
		public static final String OVERLAP_REPORT = "OVERLAP-REPORT";
	}
	
	public interface OVERLAP_REPORT{
		public static final String HEADER = "HEADER";
		public static final String REQUEST = "REQUEST";
		public static final String INDV_RESPONSES = "INDV-RESPONSES";
		public static final String GRP_RESPONSES = "GRP-RESPONSES";
		public static final String INQUIRY_HISTORY = "INQUIRY-HISTORY";
		public static final String PRINTABLE_REPORT = "PRINTABLE-REPORT";
		
	}
	
	public interface HEADER{
		public static final String DATE_OF_REQUEST   = "DATE-OF-REQUEST"; 
		public static final String PREPARED_FOR      = "PREPARED-FOR"; 
		public static final String PREPARED_FOR_ID   = "PREPARED-FOR-ID";
		public static final String DATE_OF_ISSUE     = "DATE-OF-ISSUE";
		public static final String REPORT_ID         = "REPORT-ID";
		public static final String BATCH_ID         = "BATCH-ID";
	}
	
	public interface REQUEST{
		public static final String NAME                     = "NAME";
		public static final String SPOUSE                   = "SPOUSE";
		public static final String FATHER                   = "FATHER";
		public static final String MOTHER                   = "MOTHER";
		public static final String DOB                      = "DOB";
		public static final String AGE                      = "AGE";
		public static final String AGE_AS_ON                = "AGE-AS-ON";
		public static final String GENDER                   = "GENDER";
		public static final String PHONE_1                  = "PHONE-1";
		public static final String PHONE_2                  = "PHONE-2";
		public static final String PHONE_3                  = "PHONE-3";
		public static final String ADDRESS_1                = "ADDRESS-1";
		public static final String ADDRESS_2                = "ADDRESS-2";
		public static final String REL_TYP_1                = "REL-TYP-1";
		public static final String REL_NM_1                 = "REL-NM-1";
		public static final String REL_TYP_2                = "REL-TYP-2";
		public static final String REL_NM_2                 = "REL-NM-2";
		public static final String REL_TYP_3                = "REL-TYP-3";
		public static final String REL_NM_3	                = "REL-NM-3";
		public static final String REL_TYP_4                = "REL-TYP-4";
		public static final String REL_NM_4                 = "REL-NM-4";
		public static final String ID_TYPE_1                = "ID-TYPE-1";
		public static final String ID_VALUE_1               = "ID-VALUE-1";
		public static final String ID_TYPE_2                = "ID-TYPE-2";
		public static final String ID_VALUE_2               = "ID-VALUE-2";
		public static final String RATION_CARD              = "RATION-CARD";
		public static final String APP_ID                   = "APP-ID";
		public static final String LOS_APP_ID                   = "LOS-APP-ID";
		public static final String VOTERS_ID                = "VOTERS-ID";
		public static final String DRIVING_LICENCE_NO       = "DRIVING-LICENCE-NO";
		public static final String PAN                      = "PAN";
		public static final String PASSPORT                 = "PASSPORT";
		public static final String OTHER_ID                 = "OTHER-ID";
		public static final String BRANCH                   = "BRANCH";
		public static final String KENDRA                   = "KENDRA";
		public static final String MBR_ID                   = "MBR-ID";
		public static final String CREDT_INQ_PURPS_TYP      = "CREDT-INQ-PURPS-TYP";
		public static final String CREDT_INQ_PURPS_TYP_DESC = "CREDT-INQ-PURPS-TYP-DESC";
		public static final String CREDIT_INQUIRY_STAGE     = "CREDIT-INQUIRY-STAGE";
		public static final String CREDT_RPT_ID             = "CREDT-RPT-ID";
		public static final String CREDT_REQ_TYP            = "CREDT-REQ-TYP";
		public static final String CREDT_RPT_TRN_DT_TM      = "CREDT-RPT-TRN-DT-TM";
		public static final String AC_OPEN_DT               = "AC-OPEN-DT";
		public static final String LOAN_AMOUNT              = "LOAN-AMOUNT";
		public static final String ENTITY_ID                = "ENTITY-ID";
	}
	
	public interface INDV_RESPONSES{
		public static final String PRIMARY_SUMMARY    = "PRIMARY-SUMMARY";
		public static final String SECONDARY_SUMMARY  = "SECONDARY-SUMMARY";
		public static final String SUMMARY            = "SUMMARY";
		public static final String RESPONSES          = "RESPONSES"; 
	}
	
	public interface GRP_RESPONSES{
		public static final String PRIMARY_SUMMARY    = "PRIMARY-SUMMARY";
		public static final String SECONDARY_SUMMARY  = "SECONDARY-SUMMARY";
		public static final String SUMMARY            = "SUMMARY";
		public static final String RESPONSES          = "RESPONSES"; 
	}
	
	public interface RESPONSES{
		public static final String RESPONSE = "RESPONSE";
	}
	
	public interface SUMMARY{
		public static final String STATUS                          = "STATUS";                        
		public static final String NO_OF_DEFAULT_ACCOUNTS          = "NO-OF-DEFAULT-ACCOUNTS";
		public static final String TOTAL_RESPONSES                 = "TOTAL-RESPONSES";
		public static final String NO_OF_CLOSED_ACCOUNTS           = "NO-OF-CLOSED-ACCOUNTS";
		public static final String NO_OF_ACTIVE_ACCOUNTS           = "NO-OF-ACTIVE-ACCOUNTS";
		public static final String NO_OF_OTHER_MFIS                = "NO-OF-OTHER-MFIS";
		public static final String OWN_MFI_INDECATOR               = "OWN-MFI-INDECATOR";
		public static final String TOTAL_OWN_DISBURSED_AMOUNT      = "TOTAL-OWN-DISBURSED-AMOUNT";
		public static final String TOTAL_OTHER_DISBURSED_AMOUNT    = "TOTAL-OTHER-DISBURSED-AMOUNT";
		public static final String TOTAL_OWN_CURRENT_BALANCE       = "TOTAL-OWN-CURRENT-BALANCE";
		public static final String TOTAL_OTHER_CURRENT_BALANCE     = "TOTAL-OTHER-CURRENT-BALANCE";
		public static final String TOTAL_OWN_INSTALLMENT_AMOUNT    = "TOTAL-OWN-INSTALLMENT-AMOUNT";
		public static final String TOTAL_OTHER_INSTALLMENT_AMOUNT  = "TOTAL-OTHER-INSTALLMENT-AMOUNT";
		public static final String MAX_WORST_DELEQUENCY            = "MAX-WORST-DELEQUENCY";
		public static final String ERRORS                          = "ERRORS";
		public static final String NO_OF_OWN_MFIS                          = "NO-OF-OWN-MFIS";
		
	}
	
	public interface RESPONSE{
		public static final String  MATCHED_TYPE            = "MATCHED-TYPE";         
		public static final String  MFI                     = "MFI";
		public static final String  MFI_ID                  = "MFI-ID";
		public static final String  BRANCH                  = "BRANCH";
		public static final String  KENDRA                  = "KENDRA";
		public static final String  NAME                    = "NAME";
		public static final String  SPOUSE	                = "SPOUSE"; 
		public static final String  FATHER                  = "FATHER";
		public static final String  CNSMR_MBR_ID            = "CNSMR-MBR-ID";
		public static final String  DOB                     = "DOB";
		public static final String  AGE                     = "AGE";
		public static final String  AGE_AS_ON               = "AGE-AS-ON";
		public static final String  PHONE                   = "PHONE";
		public static final String  ADDRESS_1               = "ADDRESS-1";
		public static final String  ADDRESS_2               = "ADDRESS-2";
		public static final String  REL_TYP_1               = "REL-TYP-1";
		public static final String  REL_NM_1                = "REL-NM-1";
		public static final String  REL_TYP_2               = "REL-TYP-2";
		public static final String  REL_NM_2                = "REL-NM-2";
		public static final String  ID_TYP_1                = "ID-TYP-1";
		public static final String  ID_VALUE_1              = "ID-VALUE-1";
		public static final String  ID_TYP_2                = "ID-TYP-2";
		public static final String  ID_VALUE_2              = "ID-VALUE-2";
		public static final String  GROUP_ID                = "GROUP-ID";
		public static final String  GROUP_CREATION_DATE     = "GROUP-CREATION-DATE";
		public static final String  INSERT_DATE             = "INSERT-DATE";
		public static final String  LOAN_DETAILS            = "LOAN-DETAILS";
		
	}
	
	public interface LOAN_DETAILS{
		public static final String ACCT_TYPE                 = "ACCT-TYPE";     
		public static final String FREQ                      = "FREQ";
		public static final String STATUS                    = "STATUS";
		public static final String ACCT_NUMBER               = "ACCT-NUMBER";
		public static final String DISBURSED_AMT             = "DISBURSED-AMT";
		public static final String CURRENT_BAL               = "CURRENT-BAL";
		public static final String INSTALLMENT_AMT           = "INSTALLMENT-AMT";
		public static final String OVERDUE_AMT               = "OVERDUE-AMT";
		public static final String WRITE_OFF_AMT             = "WRITE-OFF-AMT";
		public static final String DISBURSED_DT              = "DISBURSED-DT";
		public static final String CLOSED_DT                 = "CLOSED-DT";
		public static final String RECENT_DELINQ_DT          = "RECENT-DELINQ-DT";
		public static final String DPD                       = "DPD";
		public static final String INQ_CNT                   = "INQ-CNT";
		public static final String INFO_AS_ON                = "INFO-AS-ON";
		public static final String  WORST_DELEQUENCY_AMOUNT  = "WORST-DELEQUENCY-AMOUNT";
		public static final String  PAYMENT_HISTORY          = "PAYMENT-HISTORY";
		public static final String  IS_ACTIVE_BORROWER       = "IS-ACTIVE-BORROWER";
		public static final String  NO_OF_BORROWERS          = "NO-OF-BORROWERS";
		public static final String  COMMENT                  = "COMMENT";
		
	}
	
	
	public interface INQUIRY_HISTORY{
		public static final String  HISTORY = "HISTORY";
	}
	
	public interface HISTORY{
		public static final String MEMBER_NAME  = "MEMBER-NAME";
		public static final String INQUIRY_DATE	= "INQUIRY-DATE";
		public static final String PURPOSE      = "PURPOSE";
		public static final String AMOUNT       = "AMOUNT";
		public static final String REMARK       = "REMARK";
	}
	
	public interface PRINTABLE_REPORT{
		public static final String TYPE   = "TYPE";
		public static final String FILE_NAME = "FILE-NAME";
		public static final String CONTENT   = "CONTENT";
		
	}
}
