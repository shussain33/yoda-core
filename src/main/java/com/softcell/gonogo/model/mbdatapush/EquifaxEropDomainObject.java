/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class EquifaxEropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("EQUIFAX_EROP_DOMAIN_LIST")
	List<EquifaxEropDomain> equifaxEropDomainList;
	
	public List<EquifaxEropDomain> getEquifaxEropDomainList() {
		return equifaxEropDomainList;
	}
	public void setEquifaxEropDomainList(
			List<EquifaxEropDomain> equifaxEropDomainList) {
		this.equifaxEropDomainList = equifaxEropDomainList;
	}
	
	@Override
	public String toString() {
		return "EquifaxEropDomainObject [equifaxEropDomainList="
				+ equifaxEropDomainList + "]";
	}
	

	
	
}
