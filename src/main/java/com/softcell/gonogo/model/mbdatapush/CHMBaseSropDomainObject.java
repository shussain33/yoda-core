/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class CHMBaseSropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("CHM_BASE_SROP_DOMAIN_LIST")
	List<HibHighmarkBaseSropDomain> hmBaseSropDomainList;
	
	public List<HibHighmarkBaseSropDomain> getHmBaseSropDomainList() {
		return hmBaseSropDomainList;
	}
	public void setHmBaseSropDomainList(
			List<HibHighmarkBaseSropDomain> hmBaseSropDomainList) {
		this.hmBaseSropDomainList = hmBaseSropDomainList;
	}

	@Override
	public String toString() {
		return "CHMBaseSropDomainObject [hmBaseSropDomainList="
				+ hmBaseSropDomainList + "]";
	}
}
