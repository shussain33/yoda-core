package com.softcell.gonogo.model.mbdatapush.chm;


public class BaseReport {

		
	private Header header;
	private Request request;
	private PersonalInfoVariation personalInfoVariation;
	private AccountSummary accountSummary;
	private Responses responses;
	private Scores scores;
	private InquiryHistory inquiryHistory;
	private Comments comments;
	private Alerts alerts;
	private SecondaryMatches secondaryMatches;
	private PrintableReport printableReport;
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public PersonalInfoVariation getPersonalInfoVariation() {
		return personalInfoVariation;
	}
	public void setPersonalInfoVariation(PersonalInfoVariation personalInfoVariation) {
		this.personalInfoVariation = personalInfoVariation;
	}
	public AccountSummary getAccountSummary() {
		return accountSummary;
	}
	public void setAccountSummary(AccountSummary accountSummary) {
		this.accountSummary = accountSummary;
	}
	public Responses getResponses() {
		return responses;
	}
	public void setResponses(Responses responses) {
		this.responses = responses;
	}
	public Scores getScores() {
		return scores;
	}
	public void setScores(Scores scores) {
		this.scores = scores;
	}
	public InquiryHistory getInquiryHistory() {
		return inquiryHistory;
	}
	public void setInquiryHistory(InquiryHistory inquiryHistory) {
		this.inquiryHistory = inquiryHistory;
	}
	public Comments getComments() {
		return comments;
	}
	public void setComments(Comments comments) {
		this.comments = comments;
	}
	public Alerts getAlerts() {
		return alerts;
	}
	public void setAlerts(Alerts alerts) {
		this.alerts = alerts;
	}
	public SecondaryMatches getSecondaryMatches() {
		return secondaryMatches;
	}
	public void setSecondaryMatches(SecondaryMatches secondaryMatches) {
		this.secondaryMatches = secondaryMatches;
	}
	public PrintableReport getPrintableReport() {
		return printableReport;
	}
	public void setPrintableReport(PrintableReport printableReport) {
		this.printableReport = printableReport;
	}
	@Override
	public String toString() {
		return "BaseReport [header=" + header + ", request=" + request
				+ ", personalInfoVariation=" + personalInfoVariation
				+ ", accountSummary=" + accountSummary + ", responses="
				+ responses + ", scores=" + scores + ", inquiryHistory="
				+ inquiryHistory + ", comments=" + comments + ", alerts="
				+ alerts + ", secondaryMatches=" + secondaryMatches
				+ ", printableReport=" + printableReport + "]";
	}
}
