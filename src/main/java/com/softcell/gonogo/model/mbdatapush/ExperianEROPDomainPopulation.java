package com.softcell.gonogo.model.mbdatapush;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.multibureau.experian.Header;
import com.softcell.gonogo.model.multibureau.experian.InProfileResponse;
import com.softcell.gonogo.model.multibureau.experian.UserMessage;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ExperianEROPDomainPopulation {
		
	public List<ExperianEROPDomain> generateExperianEropDomain(InProfileResponse inProfileResponse,String srNo,
															   String soaSourceName, String memberReferenceNumber, Long institutionId,Date pickUpDate) throws SystemException{
		return generateExperianEropDomain_(inProfileResponse, srNo, soaSourceName, memberReferenceNumber, institutionId, pickUpDate);
	}

	private List<ExperianEROPDomain> generateExperianEropDomain_(InProfileResponse inProfileResponse, String srNo,
																 String soaSourceName, String memberReferenceNumber, Long institutionId, Date pickUpDate) throws SystemException {
		try {
			List<ExperianEROPDomain> experianEROPDomainList = new ArrayList<ExperianEROPDomain>();
			ExperianEROPDomain experianEROPDomain = new ExperianEROPDomain();

				experianEROPDomain.setMemberrefrenceNumber(memberReferenceNumber);
		  	    experianEROPDomain.setSrno(StringUtils.isEmpty(srNo) ? Long.MIN_VALUE : new Long(srNo));
				experianEROPDomain.setSourceName(soaSourceName);
				experianEROPDomain.setEnquiryDate(pickUpDate);
				experianEROPDomain.setOutputWriteFlag_("0");
				experianEROPDomain.setOutputWriteDate_(DateUtils.getFormattedDate(new Date(), "ddMMyyyy HH:mm:ss"));

			    Header header = inProfileResponse.getHeader();
			if (header != null) {
				experianEROPDomain.setMessageText(header.getMessageText());
				experianEROPDomain.setReportDate(header.getReportDate());
				experianEROPDomain.setReportTime(header.getReportTime());
				experianEROPDomain.setSystemCode(header.getSystemCode());
			}
			UserMessage userMsg = inProfileResponse.getUserMessage();
			if (userMsg != null) {
				experianEROPDomain.setMessageText(userMsg.getUserMessageText());
			}
			experianEROPDomainList.add(experianEROPDomain);
			return experianEROPDomainList;

		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("" + e.getMessage());
		}
	}
	
}
