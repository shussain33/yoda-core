package com.softcell.gonogo.model.mbdatapush.chm;

public class BaseFileHeader {
	
	private String reportType;
	private String reportDate;
	private String bulkFileName;
	private String inqCntFile;
	private String responseCntFile;
	
	
	
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getBulkFileName() {
		return bulkFileName;
	}
	public void setBulkFileName(String fileName) {
		this.bulkFileName = fileName;
	}
	public String getInqCntFile() {
		return inqCntFile;
	}
	public void setInqCntFile(String inqCntFile) {
		this.inqCntFile = inqCntFile;
	}
	public String getResponseCntFile() {
		return responseCntFile;
	}
	public void setResponseCntFile(String responseCntFile) {
		this.responseCntFile = responseCntFile;
	}

	
	@Override
	public String toString() {
		return "BaseFileHeader [reportType=" + reportType + ", reportDate="
				+ reportDate + ", fileName=" + bulkFileName + ", inqCntFile="
				+ inqCntFile + ", responseCntFile=" + responseCntFile + "]";
	}
}
