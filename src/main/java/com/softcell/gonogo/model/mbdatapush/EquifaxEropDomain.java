package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class EquifaxEropDomain implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("SRNO")
	private String  srNo_;                     
	@Expose
	@SerializedName("SOURCE_NAME")
	private String  sourceName_;              
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private String  memberReferenceNo_;  
	@Expose
	@SerializedName("ENQUIRY_DATE")
	private Date  enquiryDate_;             
	@Expose
	@SerializedName("ERRORCODE")
	private String  errorCode_;                
	@Expose
	@SerializedName("ERRORMSG")
	private String  errorMsg_;                 
	@Expose
	@SerializedName("ERRORDETAILS")
	private String  errorDetails_;             
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private String  outputWriteFlag_;        
	@Expose
	@SerializedName("OUTPUT_WRITE_DATE")
	private String  outputWriteDate_;        
	@Expose
	@SerializedName("OUTPUT_READ_TIME")
	private String  outputReadTime_;        
	

	
	public EquifaxEropDomain(String srNo,String sourceName,String memberRefrenceNumber,Date enquiryDate) {
		this.srNo_ = srNo;
		this.sourceName_ = sourceName;
		this.memberReferenceNo_ = memberRefrenceNumber;
		this.enquiryDate_ = enquiryDate;
	}



	public String getSrNo_() {
		return srNo_;
	}



	public void setSrNo_(String srNo_) {
		this.srNo_ = srNo_;
	}



	public String getSourceName_() {
		return sourceName_;
	}



	public void setSourceName_(String sourceName_) {
		this.sourceName_ = sourceName_;
	}



	public String getMemberReferenceNo_() {
		return memberReferenceNo_;
	}



	public void setMemberReferenceNo_(String memberReferenceNo_) {
		this.memberReferenceNo_ = memberReferenceNo_;
	}



	public Date getEnquiryDate_() {
		return enquiryDate_;
	}



	public void setEnquiryDate_(Date enquiryDate_) {
		this.enquiryDate_ = enquiryDate_;
	}



	public String getErrorCode_() {
		return errorCode_;
	}



	public void setErrorCode_(String errorCode_) {
		this.errorCode_ = errorCode_;
	}



	public String getErrorMsg_() {
		return errorMsg_;
	}



	public void setErrorMsg_(String errorMsg_) {
		this.errorMsg_ = errorMsg_;
	}



	public String getErrorDetails_() {
		return errorDetails_;
	}



	public void setErrorDetails_(String errorDetails_) {
		this.errorDetails_ = errorDetails_;
	}



	public String getOutputWriteFlag_() {
		return outputWriteFlag_;
	}



	public void setOutputWriteFlag_(String outputWriteFlag_) {
		this.outputWriteFlag_ = outputWriteFlag_;
	}



	public String getOutputWriteDate_() {
		return outputWriteDate_;
	}



	public void setOutputWriteDate_(String outputWriteDate_) {
		this.outputWriteDate_ = outputWriteDate_;
	}



	public String getOutputReadTime_() {
		return outputReadTime_;
	}



	public void setOutputReadTime_(String outputReadTime_) {
		this.outputReadTime_ = outputReadTime_;
	}



	@Override
	public String toString() {
		return "EquifaxEropDomain [srNo_=" + srNo_ + ", sourceName_="
				+ sourceName_ + ", memberReferenceNo_=" + memberReferenceNo_
				+ ", enquiryDate_=" + enquiryDate_ + ", errorCode_="
				+ errorCode_ + ", errorMsg_=" + errorMsg_ + ", errorDetails_="
				+ errorDetails_ + ", outputWriteFlag_=" + outputWriteFlag_
				+ ", outputWriteDate_=" + outputWriteDate_
				+ ", outputReadTime_=" + outputReadTime_ + "]";
	}
	
	

}
