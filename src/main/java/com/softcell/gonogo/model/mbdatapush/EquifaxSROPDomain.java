package com.softcell.gonogo.model.mbdatapush;


public class EquifaxSROPDomain {

	private String srNo;
	private String soaSourceName;
	private String memberReferenceNumber;
	private String enquiryDate;
	private String customerCode;
	private String firstnameNmTp;
	private String midlleNameNmTp;
	private String lastNameNmTp;
	private String addMidlleNameNmTp;
	private String suffixNmTp;
	private String reportedDateAgIf;
	private String ageAgIf;
	private String reportedDateAlNm;
	private String aliasNameAlNm;
	private String additionalNameInfoAdNmIf;
	private String noOfDependentsAdNmIf;
	private String reportedDateRsAdd;
	private String addressRsAdd;
	private String stateRsAdd;
	private String postalRsAdd;
	private String addTypeRsAdd;
	private String idTypeRsId;
	private String reportedDateRsId;
	private String idNumberRsId;
	private String emailReportedDateRsMl;
	private String emailAddressRsMl;
	private String empNMRepDateRsEMDL;
	private String employerNameRsEmdl;
	private String empPositionRsEmdl;
	private String empAddressRsEmdl;
	private String dobRsPerIf;
	private String genderRsPerIf;
	private String totalIncomeRsPerIf;
	private String occupationRsPerIf;
	private String maritalStausRsPerIf;
	private String occupationIcDl;
	private String monthlyIncomeIcDl;
	private String monthlyExpenseIcDl;
	private String povertyIndexIcDl;
	private String assetOwnershipIcDl;
	private String reportedDateRsPh;
	private String typeCodeRsPh;
	private String countryCodeRsPh;
	private String areaCodeRsPh;
	private String phNumberRsPh;
	private String phNoExtentionRsPh;
	private String customercodeRsHd;
	private String clientIdRsHd;
	private String custRefFieldRsHd;
	private String reportOrderNoRsHd;
	private String productCodeRsHd;
	private String productVersionRsHd;
	private String successCodeRsHd;
	private String matchTypeRsHd;
	private String resDateRsHd;
	private String resTimeRsHd;
	private String reportedDateRsAc;
	private String clientNameRsAc;
	private String accountNumberRsAc;
	private String currentBalanceRsAc;
	private String institutionRsAc;
	private String accountTypeRsAc;
	private String ownershipTypeRsAc;
	private String balanceRsAc;
	private String pastDueAmtRsAc;
	private String disbursedAmountRsAc;
	private String loanCategoryRsAc;
	private String loanPurposeRsAc;
	private String lastPaymentRsAc;
	private String writeOffAmtRsAc;
	private String accOpenRsAc;
	private String sactionAmtRsAc;
	private String lastPaymentDateRsAc;
	private String highCreditRsAc;
	private String dateReportedRsAc;
	private String dateOpenedRsAc;
	private String dateClosedRsAc;
	private String reasonRsAc;
	private String dateWrittenOffRsAc;
	private String loanCycleIdRsAc;
	private String dateSanctionedRsAc;
	private String dateAppliedRsAc;
	private String intrestRateRSAC;
	private String appliedAmountRsAc;
	private String noOfInstallmentsRsAc;
	private String repaymentTenureRsAc;
	private String disputeCodeRsAc;
	private String installmentAmtRsAc;
	private String keyPersonRsAc;
	private String nomineeRsAc;
	private String termFrequencyRsAc;
	private String creditLimitRsAc;
	private String collateralValueRsAc;
	private String collateralTypeRsAc;
	private String accountStatusRsAc;
	private String assetClassificationRsAc;
	private String suitFiledStatusRsAc;
	private String monthKeyRsAcHis24;
	private String paymentStatusRsAcHis24;
	private String suitFiledStatusRsAcHis24;
	private String assettClsStatusRsAcHis24;
	private String monthKeyRsAcHis48;
	private String pAYMENTSTATUSRsAcHis48;
	private String sUITFILEDSTATUSRsAcHis48;
	private String aSSETCLSSTATUSRsAcHis48;
	private String noOfAccountsRsAcSum;
	private String noOfActiveAccountsRsAcSum;
	private String noOfWriteOffsRsAcSum;
	private String totalPastDueRsAcSum;
	private String mostSrvrStaWhin24MonRsAcSum;
	private String singleHighestCreditRsAcSum;
	private String singleHighSanctAmtRsAcSum;
	private String totalHighCreditRsAcSum;
	private String averageOpenBalanceRsAcSum;
	private String singleHighestBalanceRsAcSum;
	private String noOfPastDueAcctsRsAcSum;
	private String noOfZeroBalAcctsRsAcSum;
	private String recentAccountRsAcSum;
	private String oldestAccountRsAcSum;
	private String totalBalAmtRsAcSum;
	private String totalSanctAmtRsAcSum;
	private String totalCrdtLimitRsAcSum;
	private String totalMonlyPymntAmtRsAcSum;
	private String totalWrittenOffAmntRsAcSum;
	private String ageOfOldestTradeRsOtk;
	private String numberOfOpenTradesRsOtk;
	private String alineSeverWrittenRsOtk;
	private String alineSvWrtnIn9MnthsRsOtk;
	private String alineSvrWrTnIn6MnthsRsOtk;
	private String reportedDateRsEqSm;
	private String purposeRsEqSm;
	private String totalRsEqSm;
	private String past30DaysRsEqSm;
	private String past12MonthsRsEqSm;
	private String past24MonthsRsEqSm;
	private String recentRsEqSm;
	private String reportedDateRsEq;
	private String enqReferenceRsEq;
	private String enqDateRsEq;
	private String enqTimeRsEq;
	private String requestPurposeRsEq;
	private String amountRsEq;
	private String accountsDeliquentRsReAct;
	private String accountsOpenedRsReAct;
	private String totalInquiriesRsReAct;
	private String accountsUpdatedRsReAct;
	private String institutionGrpSm;
	private String currentBalanceGrpSm;
	private String statusGrpSm;
	private String dateReportedGrpSm;
	private String noOfMembersGrpSm;
	private String pastDueAmountGrpSm;
	private String sanctionAmountGrpSm;
	private String dateOpenedGrpSm;
	private String accountNoGrpSm;
	private String membersPastDueGrpSm;
	private String writeOffAmountGrpSm;
	private String writeOffDateGrpSm;
	private String reasonCodeSc;
	private String scoreDescriptionSc;
	private String scoreNameSc;
	private String scoreValueSc;
	private String outputWriteFlag;
	private String outputWriteTime;
	private String outputReadTime;
	private String accountKey;
	private String code;
	private String description;

	private String accntNum;
	private String disputeComments;
	private String status;
	private String resolvedDate;
	
	
	public String getAccntNum() {
		return accntNum;
	}

	public void setAccntNum(String accntNum) {
		this.accntNum = accntNum;
	}

	public String getDisputeComments() {
		return disputeComments;
	}

	public void setDisputeComments(String disputeComments) {
		this.disputeComments = disputeComments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(String resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EquifaxSROPDomain() {
		
	}
	
	public EquifaxSROPDomain(String srNo, String soaSourceName,String memberReferenceNumber,String enquiryDate,String customerCode) {
		this.srNo=srNo;                  
		this.soaSourceName=soaSourceName;         
		this.memberReferenceNumber=memberReferenceNumber; 
		this.enquiryDate=enquiryDate;    
		this.customercodeRsHd = customerCode;
	}
	
	public EquifaxSROPDomain(String srNo, String soaSourceName,String memberReferenceNumber,String enquiryDate,String customerCode,String accountNumber, String accountKey) {
		this.srNo=srNo;                  
		this.soaSourceName=soaSourceName;         
		this.memberReferenceNumber=memberReferenceNumber; 
		this.enquiryDate=enquiryDate;    
		this.customercodeRsHd = customerCode;
		this.accountNumberRsAc = accountNumber;
		this.accountKey = accountKey;
	}
	
	public String getSrNo() {
		return srNo;
	}
	public void setSrNo(String srNo) {
		this.srNo = srNo;
	}
	public String getSoaSourceName() {
		return soaSourceName;
	}
	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}
	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}
	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}
	
	public String getEnquiryDate() {
		return enquiryDate;
	}

	public void setEnquiryDate(String enquiryDate) {
		this.enquiryDate = enquiryDate;
	}

	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getFirstnameNmTp() {
		return firstnameNmTp;
	}
	public void setFirstnameNmTp(String firstnameNmTp) {
		this.firstnameNmTp = firstnameNmTp;
	}
	public String getMidlleNameNmTp() {
		return midlleNameNmTp;
	}
	public void setMidlleNameNmTp(String midlleNameNmTp) {
		this.midlleNameNmTp = midlleNameNmTp;
	}
	public String getLastNameNmTp() {
		return lastNameNmTp;
	}
	public void setLastNameNmTp(String lastNameNmTp) {
		this.lastNameNmTp = lastNameNmTp;
	}
	public String getAddMidlleNameNmTp() {
		return addMidlleNameNmTp;
	}

	public void setAddMidlleNameNmTp(String addMidlleNameNmTp) {
		this.addMidlleNameNmTp = addMidlleNameNmTp;
	}

	public String getSuffixNmTp() {
		return suffixNmTp;
	}
	public void setSuffixNmTp(String suffixNmTp) {
		this.suffixNmTp = suffixNmTp;
	}
	public String getReportedDateAgIf() {
		return reportedDateAgIf;
	}
	public void setReportedDateAgIf(String reportedDateAgIf) {
		this.reportedDateAgIf = reportedDateAgIf;
	}
	public String getAgeAgIf() {
		return ageAgIf;
	}
	public void setAgeAgIf(String ageAgIf) {
		this.ageAgIf = ageAgIf;
	}
	public String getReportedDateAlNm() {
		return reportedDateAlNm;
	}
	public void setReportedDateAlNm(String reportedDateAlNm) {
		this.reportedDateAlNm = reportedDateAlNm;
	}
	public String getAliasNameAlNm() {
		return aliasNameAlNm;
	}
	public void setAliasNameAlNm(String aliasNameAlNm) {
		this.aliasNameAlNm = aliasNameAlNm;
	}
	public String getAdditionalNameInfoAdNmIf() {
		return additionalNameInfoAdNmIf;
	}
	public void setAdditionalNameInfoAdNmIf(String additionalNameInfoAdNmIf) {
		this.additionalNameInfoAdNmIf = additionalNameInfoAdNmIf;
	}
	public String getNoOfDependentsAdNmIf() {
		return noOfDependentsAdNmIf;
	}
	public void setNoOfDependentsAdNmIf(String noOfDependentsAdNmIf) {
		this.noOfDependentsAdNmIf = noOfDependentsAdNmIf;
	}
	public String getReportedDateRsAdd() {
		return reportedDateRsAdd;
	}
	public void setReportedDateRsAdd(String reportedDateRsAdd) {
		this.reportedDateRsAdd = reportedDateRsAdd;
	}
	public String getAddressRsAdd() {
		return addressRsAdd;
	}
	public void setAddressRsAdd(String addressRsAdd) {
		this.addressRsAdd = addressRsAdd;
	}
	public String getStateRsAdd() {
		return stateRsAdd;
	}
	public void setStateRsAdd(String stateRsAdd) {
		this.stateRsAdd = stateRsAdd;
	}
	public String getPostalRsAdd() {
		return postalRsAdd;
	}
	public void setPostalRsAdd(String postalRsAdd) {
		this.postalRsAdd = postalRsAdd;
	}
	public String getAddTypeRsAdd() {
		return addTypeRsAdd;
	}
	public void setAddTypeRsAdd(String addTypeRsAdd) {
		this.addTypeRsAdd = addTypeRsAdd;
	}
	public String getIdTypeRsId() {
		return idTypeRsId;
	}
	public void setIdTypeRsId(String idTypeRsId) {
		this.idTypeRsId = idTypeRsId;
	}
	public String getReportedDateRsId() {
		return reportedDateRsId;
	}
	public void setReportedDateRsId(String reportedDateRsId) {
		this.reportedDateRsId = reportedDateRsId;
	}
	public String getIdNumberRsId() {
		return idNumberRsId;
	}
	public void setIdNumberRsId(String idNumberRsId) {
		this.idNumberRsId = idNumberRsId;
	}
	public String getEmailReportedDateRsMl() {
		return emailReportedDateRsMl;
	}
	public void setEmailReportedDateRsMl(String emailReportedDateRsMl) {
		this.emailReportedDateRsMl = emailReportedDateRsMl;
	}
	public String getEmailAddressRsMl() {
		return emailAddressRsMl;
	}
	public void setEmailAddressRsMl(String emailAddressRsMl) {
		this.emailAddressRsMl = emailAddressRsMl;
	}
	public String getEmpNMRepDateRsEMDL() {
		return empNMRepDateRsEMDL;
	}
	public void setEmpNMRepDateRsEMDL(String empNMRepDateRsEMDL) {
		this.empNMRepDateRsEMDL = empNMRepDateRsEMDL;
	}
	public String getEmployerNameRsEmdl() {
		return employerNameRsEmdl;
	}
	public void setEmployerNameRsEmdl(String employerNameRsEmdl) {
		this.employerNameRsEmdl = employerNameRsEmdl;
	}
	public String getEmpPositionRsEmdl() {
		return empPositionRsEmdl;
	}
	public void setEmpPositionRsEmdl(String empPositionRsEmdl) {
		this.empPositionRsEmdl = empPositionRsEmdl;
	}
	public String getEmpAddressRsEmdl() {
		return empAddressRsEmdl;
	}
	public void setEmpAddressRsEmdl(String empAddressRsEmdl) {
		this.empAddressRsEmdl = empAddressRsEmdl;
	}
	public String getDobRsPerIf() {
		return dobRsPerIf;
	}
	public void setDobRsPerIf(String dobRsPerIf) {
		this.dobRsPerIf = dobRsPerIf;
	}
	public String getGenderRsPerIf() {
		return genderRsPerIf;
	}
	public void setGenderRsPerIf(String genderRsPerIf) {
		this.genderRsPerIf = genderRsPerIf;
	}
	public String getTotalIncomeRsPerIf() {
		return totalIncomeRsPerIf;
	}
	public void setTotalIncomeRsPerIf(String totalIncomeRsPerIf) {
		this.totalIncomeRsPerIf = totalIncomeRsPerIf;
	}
	public String getOccupationRsPerIf() {
		return occupationRsPerIf;
	}
	public void setOccupationRsPerIf(String occupationRsPerIf) {
		this.occupationRsPerIf = occupationRsPerIf;
	}
	public String getMaritalStausRsPerIf() {
		return maritalStausRsPerIf;
	}
	public void setMaritalStausRsPerIf(String maritalStausRsPerIf) {
		this.maritalStausRsPerIf = maritalStausRsPerIf;
	}
	public String getOccupationIcDl() {
		return occupationIcDl;
	}
	public void setOccupationIcDl(String occupationIcDl) {
		this.occupationIcDl = occupationIcDl;
	}
	public String getMonthlyIncomeIcDl() {
		return monthlyIncomeIcDl;
	}
	public void setMonthlyIncomeIcDl(String monthlyIncomeIcDl) {
		this.monthlyIncomeIcDl = monthlyIncomeIcDl;
	}
	public String getMonthlyExpenseIcDl() {
		return monthlyExpenseIcDl;
	}
	public void setMonthlyExpenseIcDl(String monthlyExpenseIcDl) {
		this.monthlyExpenseIcDl = monthlyExpenseIcDl;
	}
	public String getPovertyIndexIcDl() {
		return povertyIndexIcDl;
	}
	public void setPovertyIndexIcDl(String povertyIndexIcDl) {
		this.povertyIndexIcDl = povertyIndexIcDl;
	}
	public String getAssetOwnershipIcDl() {
		return assetOwnershipIcDl;
	}
	public void setAssetOwnershipIcDl(String assetOwnershipIcDl) {
		this.assetOwnershipIcDl = assetOwnershipIcDl;
	}
	public String getReportedDateRsPh() {
		return reportedDateRsPh;
	}
	public void setReportedDateRsPh(String reportedDateRsPh) {
		this.reportedDateRsPh = reportedDateRsPh;
	}
	public String getTypeCodeRsPh() {
		return typeCodeRsPh;
	}
	public void setTypeCodeRsPh(String typeCodeRsPh) {
		this.typeCodeRsPh = typeCodeRsPh;
	}
	public String getCountryCodeRsPh() {
		return countryCodeRsPh;
	}
	public void setCountryCodeRsPh(String countryCodeRsPh) {
		this.countryCodeRsPh = countryCodeRsPh;
	}
	public String getAreaCodeRsPh() {
		return areaCodeRsPh;
	}
	public void setAreaCodeRsPh(String areaCodeRsPh) {
		this.areaCodeRsPh = areaCodeRsPh;
	}
	public String getPhNumberRsPh() {
		return phNumberRsPh;
	}
	public void setPhNumberRsPh(String phNumberRsPh) {
		this.phNumberRsPh = phNumberRsPh;
	}
	public String getPhNoExtentionRsPh() {
		return phNoExtentionRsPh;
	}
	public void setPhNoExtentionRsPh(String phNoExtentionRsPh) {
		this.phNoExtentionRsPh = phNoExtentionRsPh;
	}
	public String getCustomercodeRsHd() {
		return customercodeRsHd;
	}
	public void setCustomercodeRsHd(String customercodeRsHd) {
		this.customercodeRsHd = customercodeRsHd;
	}
	public String getClientIdRsHd() {
		return clientIdRsHd;
	}
	public void setClientIdRsHd(String clientIdRsHd) {
		this.clientIdRsHd = clientIdRsHd;
	}
	public String getCustRefFieldRsHd() {
		return custRefFieldRsHd;
	}
	public void setCustRefFieldRsHd(String custRefFieldRsHd) {
		this.custRefFieldRsHd = custRefFieldRsHd;
	}
	public String getReportOrderNoRsHd() {
		return reportOrderNoRsHd;
	}
	public void setReportOrderNoRsHd(String reportOrderNoRsHd) {
		this.reportOrderNoRsHd = reportOrderNoRsHd;
	}
	public String getProductCodeRsHd() {
		return productCodeRsHd;
	}
	public void setProductCodeRsHd(String productCodeRsHd) {
		this.productCodeRsHd = productCodeRsHd;
	}
	public String getProductVersionRsHd() {
		return productVersionRsHd;
	}
	public void setProductVersionRsHd(String productVersionRsHd) {
		this.productVersionRsHd = productVersionRsHd;
	}
	public String getSuccessCodeRsHd() {
		return successCodeRsHd;
	}
	public void setSuccessCodeRsHd(String successCodeRsHd) {
		this.successCodeRsHd = successCodeRsHd;
	}
	public String getMatchTypeRsHd() {
		return matchTypeRsHd;
	}
	public void setMatchTypeRsHd(String matchTypeRsHd) {
		this.matchTypeRsHd = matchTypeRsHd;
	}
	public String getResDateRsHd() {
		return resDateRsHd;
	}
	public void setResDateRsHd(String resDateRsHd) {
		this.resDateRsHd = resDateRsHd;
	}
	public String getResTimeRsHd() {
		return resTimeRsHd;
	}
	public void setResTimeRsHd(String resTimeRsHd) {
		this.resTimeRsHd = resTimeRsHd;
	}
	public String getReportedDateRsAc() {
		return reportedDateRsAc;
	}
	public void setReportedDateRsAc(String reportedDateRsAc) {
		this.reportedDateRsAc = reportedDateRsAc;
	}
	public String getClientNameRsAc() {
		return clientNameRsAc;
	}
	public void setClientNameRsAc(String clientNameRsAc) {
		this.clientNameRsAc = clientNameRsAc;
	}
	public String getAccountNumberRsAc() {
		return accountNumberRsAc;
	}
	public void setAccountNumberRsAc(String accountNumberRsAc) {
		this.accountNumberRsAc = accountNumberRsAc;
	}
	public String getCurrentBalanceRsAc() {
		return currentBalanceRsAc;
	}
	public void setCurrentBalanceRsAc(String currentBalanceRsAc) {
		this.currentBalanceRsAc = currentBalanceRsAc;
	}
	public String getInstitutionRsAc() {
		return institutionRsAc;
	}
	public void setInstitutionRsAc(String institutionRsAc) {
		this.institutionRsAc = institutionRsAc;
	}
	public String getAccountTypeRsAc() {
		return accountTypeRsAc;
	}
	public void setAccountTypeRsAc(String accountTypeRsAc) {
		this.accountTypeRsAc = accountTypeRsAc;
	}
	public String getOwnershipTypeRsAc() {
		return ownershipTypeRsAc;
	}
	public void setOwnershipTypeRsAc(String ownershipTypeRsAc) {
		this.ownershipTypeRsAc = ownershipTypeRsAc;
	}
	public String getBalanceRsAc() {
		return balanceRsAc;
	}
	public void setBalanceRsAc(String balanceRsAc) {
		this.balanceRsAc = balanceRsAc;
	}
	public String getPastDueAmtRsAc() {
		return pastDueAmtRsAc;
	}
	public void setPastDueAmtRsAc(String pastDueAmtRsAc) {
		this.pastDueAmtRsAc = pastDueAmtRsAc;
	}
	public String getDisbursedAmountRsAc() {
		return disbursedAmountRsAc;
	}
	public void setDisbursedAmountRsAc(String disbursedAmountRsAc) {
		this.disbursedAmountRsAc = disbursedAmountRsAc;
	}
	public String getLoanCategoryRsAc() {
		return loanCategoryRsAc;
	}
	public void setLoanCategoryRsAc(String loanCategoryRsAc) {
		this.loanCategoryRsAc = loanCategoryRsAc;
	}
	public String getLoanPurposeRsAc() {
		return loanPurposeRsAc;
	}
	public void setLoanPurposeRsAc(String loanPurposeRsAc) {
		this.loanPurposeRsAc = loanPurposeRsAc;
	}
	public String getLastPaymentRsAc() {
		return lastPaymentRsAc;
	}
	public void setLastPaymentRsAc(String lastPaymentRsAc) {
		this.lastPaymentRsAc = lastPaymentRsAc;
	}
	public String getWriteOffAmtRsAc() {
		return writeOffAmtRsAc;
	}
	public void setWriteOffAmtRsAc(String writeOffAmtRsAc) {
		this.writeOffAmtRsAc = writeOffAmtRsAc;
	}
	public String getAccOpenRsAc() {
		return accOpenRsAc;
	}
	public void setAccOpenRsAc(String accOpenRsAc) {
		this.accOpenRsAc = accOpenRsAc;
	}
	public String getSactionAmtRsAc() {
		return sactionAmtRsAc;
	}
	public void setSactionAmtRsAc(String sactionAmtRsAc) {
		this.sactionAmtRsAc = sactionAmtRsAc;
	}
	public String getLastPaymentDateRsAc() {
		return lastPaymentDateRsAc;
	}
	public void setLastPaymentDateRsAc(String lastPaymentDateRsAc) {
		this.lastPaymentDateRsAc = lastPaymentDateRsAc;
	}
	public String getHighCreditRsAc() {
		return highCreditRsAc;
	}
	public void setHighCreditRsAc(String highCreditRsAc) {
		this.highCreditRsAc = highCreditRsAc;
	}
	public String getDateReportedRsAc() {
		return dateReportedRsAc;
	}
	public void setDateReportedRsAc(String dateReportedRsAc) {
		this.dateReportedRsAc = dateReportedRsAc;
	}
	public String getDateOpenedRsAc() {
		return dateOpenedRsAc;
	}
	public void setDateOpenedRsAc(String dateOpenedRsAc) {
		this.dateOpenedRsAc = dateOpenedRsAc;
	}
	public String getDateClosedRsAc() {
		return dateClosedRsAc;
	}
	public void setDateClosedRsAc(String dateClosedRsAc) {
		this.dateClosedRsAc = dateClosedRsAc;
	}
	public String getReasonRsAc() {
		return reasonRsAc;
	}
	public void setReasonRsAc(String reasonRsAc) {
		this.reasonRsAc = reasonRsAc;
	}
	public String getDateWrittenOffRsAc() {
		return dateWrittenOffRsAc;
	}
	public void setDateWrittenOffRsAc(String dateWrittenOffRsAc) {
		this.dateWrittenOffRsAc = dateWrittenOffRsAc;
	}
	public String getLoanCycleIdRsAc() {
		return loanCycleIdRsAc;
	}
	public void setLoanCycleIdRsAc(String loanCycleIdRsAc) {
		this.loanCycleIdRsAc = loanCycleIdRsAc;
	}
	public String getDateSanctionedRsAc() {
		return dateSanctionedRsAc;
	}
	public void setDateSanctionedRsAc(String dateSanctionedRsAc) {
		this.dateSanctionedRsAc = dateSanctionedRsAc;
	}
	public String getDateAppliedRsAc() {
		return dateAppliedRsAc;
	}
	public void setDateAppliedRsAc(String dateAppliedRsAc) {
		this.dateAppliedRsAc = dateAppliedRsAc;
	}
	public String getIntrestRateRSAC() {
		return intrestRateRSAC;
	}
	public void setIntrestRateRSAC(String intrestRateRSAC) {
		this.intrestRateRSAC = intrestRateRSAC;
	}
	public String getAppliedAmountRsAc() {
		return appliedAmountRsAc;
	}
	public void setAppliedAmountRsAc(String appliedAmountRsAc) {
		this.appliedAmountRsAc = appliedAmountRsAc;
	}
	public String getNoOfInstallmentsRsAc() {
		return noOfInstallmentsRsAc;
	}
	public void setNoOfInstallmentsRsAc(String noOfInstallmentsRsAc) {
		this.noOfInstallmentsRsAc = noOfInstallmentsRsAc;
	}
	public String getRepaymentTenureRsAc() {
		return repaymentTenureRsAc;
	}
	public void setRepaymentTenureRsAc(String repaymentTenureRsAc) {
		this.repaymentTenureRsAc = repaymentTenureRsAc;
	}
	public String getDisputeCodeRsAc() {
		return disputeCodeRsAc;
	}
	public void setDisputeCodeRsAc(String disputeCodeRsAc) {
		this.disputeCodeRsAc = disputeCodeRsAc;
	}
	public String getInstallmentAmtRsAc() {
		return installmentAmtRsAc;
	}
	public void setInstallmentAmtRsAc(String installmentAmtRsAc) {
		this.installmentAmtRsAc = installmentAmtRsAc;
	}
	public String getKeyPersonRsAc() {
		return keyPersonRsAc;
	}
	public void setKeyPersonRsAc(String keyPersonRsAc) {
		this.keyPersonRsAc = keyPersonRsAc;
	}
	public String getNomineeRsAc() {
		return nomineeRsAc;
	}
	public void setNomineeRsAc(String nomineeRsAc) {
		this.nomineeRsAc = nomineeRsAc;
	}
	public String getTermFrequencyRsAc() {
		return termFrequencyRsAc;
	}
	public void setTermFrequencyRsAc(String termFrequencyRsAc) {
		this.termFrequencyRsAc = termFrequencyRsAc;
	}
	public String getCreditLimitRsAc() {
		return creditLimitRsAc;
	}
	public void setCreditLimitRsAc(String creditLimitRsAc) {
		this.creditLimitRsAc = creditLimitRsAc;
	}
	public String getCollateralValueRsAc() {
		return collateralValueRsAc;
	}
	public void setCollateralValueRsAc(String collateralValueRsAc) {
		this.collateralValueRsAc = collateralValueRsAc;
	}
	public String getCollateralTypeRsAc() {
		return collateralTypeRsAc;
	}
	public void setCollateralTypeRsAc(String collateralTypeRsAc) {
		this.collateralTypeRsAc = collateralTypeRsAc;
	}
	public String getAccountStatusRsAc() {
		return accountStatusRsAc;
	}
	public void setAccountStatusRsAc(String accountStatusRsAc) {
		this.accountStatusRsAc = accountStatusRsAc;
	}
	public String getAssetClassificationRsAc() {
		return assetClassificationRsAc;
	}
	public void setAssetClassificationRsAc(String assetClassificationRsAc) {
		this.assetClassificationRsAc = assetClassificationRsAc;
	}
	public String getSuitFiledStatusRsAc() {
		return suitFiledStatusRsAc;
	}
	public void setSuitFiledStatusRsAc(String suitFiledStatusRsAc) {
		this.suitFiledStatusRsAc = suitFiledStatusRsAc;
	}
	public String getMonthKeyRsAcHis24() {
		return monthKeyRsAcHis24;
	}
	public void setMonthKeyRsAcHis24(String monthKeyRsAcHis24) {
		this.monthKeyRsAcHis24 = monthKeyRsAcHis24;
	}
	public String getPaymentStatusRsAcHis24() {
		return paymentStatusRsAcHis24;
	}
	public void setPaymentStatusRsAcHis24(String paymentStatusRsAcHis24) {
		this.paymentStatusRsAcHis24 = paymentStatusRsAcHis24;
	}
	public String getSuitFiledStatusRsAcHis24() {
		return suitFiledStatusRsAcHis24;
	}
	public void setSuitFiledStatusRsAcHis24(String suitFiledStatusRsAcHis24) {
		this.suitFiledStatusRsAcHis24 = suitFiledStatusRsAcHis24;
	}
	public String getAssettClsStatusRsAcHis24() {
		return assettClsStatusRsAcHis24;
	}
	public void setAssettClsStatusRsAcHis24(String assettClsStatusRsAcHis24) {
		this.assettClsStatusRsAcHis24 = assettClsStatusRsAcHis24;
	}
	public String getMonthKeyRsAcHis48() {
		return monthKeyRsAcHis48;
	}
	public void setMonthKeyRsAcHis48(String monthKeyRsAcHis48) {
		this.monthKeyRsAcHis48 = monthKeyRsAcHis48;
	}
	public String getpAYMENTSTATUSRsAcHis48() {
		return pAYMENTSTATUSRsAcHis48;
	}
	public void setpAYMENTSTATUSRsAcHis48(String pAYMENTSTATUSRsAcHis48) {
		this.pAYMENTSTATUSRsAcHis48 = pAYMENTSTATUSRsAcHis48;
	}
	public String getsUITFILEDSTATUSRsAcHis48() {
		return sUITFILEDSTATUSRsAcHis48;
	}
	public void setsUITFILEDSTATUSRsAcHis48(String sUITFILEDSTATUSRsAcHis48) {
		this.sUITFILEDSTATUSRsAcHis48 = sUITFILEDSTATUSRsAcHis48;
	}
	public String getaSSETCLSSTATUSRsAcHis48() {
		return aSSETCLSSTATUSRsAcHis48;
	}
	public void setaSSETCLSSTATUSRsAcHis48(String aSSETCLSSTATUSRsAcHis48) {
		this.aSSETCLSSTATUSRsAcHis48 = aSSETCLSSTATUSRsAcHis48;
	}
	public String getNoOfAccountsRsAcSum() {
		return noOfAccountsRsAcSum;
	}
	public void setNoOfAccountsRsAcSum(String noOfAccountsRsAcSum) {
		this.noOfAccountsRsAcSum = noOfAccountsRsAcSum;
	}
	public String getNoOfActiveAccountsRsAcSum() {
		return noOfActiveAccountsRsAcSum;
	}
	public void setNoOfActiveAccountsRsAcSum(String noOfActiveAccountsRsAcSum) {
		this.noOfActiveAccountsRsAcSum = noOfActiveAccountsRsAcSum;
	}
	public String getNoOfWriteOffsRsAcSum() {
		return noOfWriteOffsRsAcSum;
	}
	public void setNoOfWriteOffsRsAcSum(String noOfWriteOffsRsAcSum) {
		this.noOfWriteOffsRsAcSum = noOfWriteOffsRsAcSum;
	}
	public String getTotalPastDueRsAcSum() {
		return totalPastDueRsAcSum;
	}
	public void setTotalPastDueRsAcSum(String totalPastDueRsAcSum) {
		this.totalPastDueRsAcSum = totalPastDueRsAcSum;
	}
	public String getMostSrvrStaWhin24MonRsAcSum() {
		return mostSrvrStaWhin24MonRsAcSum;
	}
	public void setMostSrvrStaWhin24MonRsAcSum(String mostSrvrStaWhin24MonRsAcSum) {
		this.mostSrvrStaWhin24MonRsAcSum = mostSrvrStaWhin24MonRsAcSum;
	}
	public String getSingleHighestCreditRsAcSum() {
		return singleHighestCreditRsAcSum;
	}
	public void setSingleHighestCreditRsAcSum(String singleHighestCreditRsAcSum) {
		this.singleHighestCreditRsAcSum = singleHighestCreditRsAcSum;
	}
	public String getSingleHighSanctAmtRsAcSum() {
		return singleHighSanctAmtRsAcSum;
	}
	public void setSingleHighSanctAmtRsAcSum(String singleHighSanctAmtRsAcSum) {
		this.singleHighSanctAmtRsAcSum = singleHighSanctAmtRsAcSum;
	}
	public String getTotalHighCreditRsAcSum() {
		return totalHighCreditRsAcSum;
	}
	public void setTotalHighCreditRsAcSum(String totalHighCreditRsAcSum) {
		this.totalHighCreditRsAcSum = totalHighCreditRsAcSum;
	}
	public String getAverageOpenBalanceRsAcSum() {
		return averageOpenBalanceRsAcSum;
	}
	public void setAverageOpenBalanceRsAcSum(String averageOpenBalanceRsAcSum) {
		this.averageOpenBalanceRsAcSum = averageOpenBalanceRsAcSum;
	}
	public String getSingleHighestBalanceRsAcSum() {
		return singleHighestBalanceRsAcSum;
	}
	public void setSingleHighestBalanceRsAcSum(String singleHighestBalanceRsAcSum) {
		this.singleHighestBalanceRsAcSum = singleHighestBalanceRsAcSum;
	}
	public String getNoOfPastDueAcctsRsAcSum() {
		return noOfPastDueAcctsRsAcSum;
	}
	public void setNoOfPastDueAcctsRsAcSum(String noOfPastDueAcctsRsAcSum) {
		this.noOfPastDueAcctsRsAcSum = noOfPastDueAcctsRsAcSum;
	}
	public String getNoOfZeroBalAcctsRsAcSum() {
		return noOfZeroBalAcctsRsAcSum;
	}
	public void setNoOfZeroBalAcctsRsAcSum(String noOfZeroBalAcctsRsAcSum) {
		this.noOfZeroBalAcctsRsAcSum = noOfZeroBalAcctsRsAcSum;
	}
	public String getRecentAccountRsAcSum() {
		return recentAccountRsAcSum;
	}
	public void setRecentAccountRsAcSum(String recentAccountRsAcSum) {
		this.recentAccountRsAcSum = recentAccountRsAcSum;
	}
	public String getOldestAccountRsAcSum() {
		return oldestAccountRsAcSum;
	}
	public void setOldestAccountRsAcSum(String oldestAccountRsAcSum) {
		this.oldestAccountRsAcSum = oldestAccountRsAcSum;
	}
	public String getTotalBalAmtRsAcSum() {
		return totalBalAmtRsAcSum;
	}
	public void setTotalBalAmtRsAcSum(String totalBalAmtRsAcSum) {
		this.totalBalAmtRsAcSum = totalBalAmtRsAcSum;
	}
	public String getTotalSanctAmtRsAcSum() {
		return totalSanctAmtRsAcSum;
	}
	public void setTotalSanctAmtRsAcSum(String totalSanctAmtRsAcSum) {
		this.totalSanctAmtRsAcSum = totalSanctAmtRsAcSum;
	}
	public String getTotalCrdtLimitRsAcSum() {
		return totalCrdtLimitRsAcSum;
	}
	public void setTotalCrdtLimitRsAcSum(String totalCrdtLimitRsAcSum) {
		this.totalCrdtLimitRsAcSum = totalCrdtLimitRsAcSum;
	}
	public String getTotalMonlyPymntAmtRsAcSum() {
		return totalMonlyPymntAmtRsAcSum;
	}
	public void setTotalMonlyPymntAmtRsAcSum(String totalMonlyPymntAmtRsAcSum) {
		this.totalMonlyPymntAmtRsAcSum = totalMonlyPymntAmtRsAcSum;
	}
	public String getTotalWrittenOffAmntRsAcSum() {
		return totalWrittenOffAmntRsAcSum;
	}
	public void setTotalWrittenOffAmntRsAcSum(String totalWrittenOffAmntRsAcSum) {
		this.totalWrittenOffAmntRsAcSum = totalWrittenOffAmntRsAcSum;
	}
	public String getAgeOfOldestTradeRsOtk() {
		return ageOfOldestTradeRsOtk;
	}
	public void setAgeOfOldestTradeRsOtk(String ageOfOldestTradeRsOtk) {
		this.ageOfOldestTradeRsOtk = ageOfOldestTradeRsOtk;
	}
	public String getNumberOfOpenTradesRsOtk() {
		return numberOfOpenTradesRsOtk;
	}
	public void setNumberOfOpenTradesRsOtk(String numberOfOpenTradesRsOtk) {
		this.numberOfOpenTradesRsOtk = numberOfOpenTradesRsOtk;
	}
	public String getAlineSeverWrittenRsOtk() {
		return alineSeverWrittenRsOtk;
	}
	public void setAlineSeverWrittenRsOtk(String alineSeverWrittenRsOtk) {
		this.alineSeverWrittenRsOtk = alineSeverWrittenRsOtk;
	}
	public String getAlineSvWrtnIn9MnthsRsOtk() {
		return alineSvWrtnIn9MnthsRsOtk;
	}
	public void setAlineSvWrtnIn9MnthsRsOtk(String alineSvWrtnIn9MnthsRsOtk) {
		this.alineSvWrtnIn9MnthsRsOtk = alineSvWrtnIn9MnthsRsOtk;
	}
	public String getAlineSvrWrTnIn6MnthsRsOtk() {
		return alineSvrWrTnIn6MnthsRsOtk;
	}
	public void setAlineSvrWrTnIn6MnthsRsOtk(String alineSvrWrTnIn6MnthsRsOtk) {
		this.alineSvrWrTnIn6MnthsRsOtk = alineSvrWrTnIn6MnthsRsOtk;
	}
	public String getReportedDateRsEqSm() {
		return reportedDateRsEqSm;
	}
	public void setReportedDateRsEqSm(String reportedDateRsEqSm) {
		this.reportedDateRsEqSm = reportedDateRsEqSm;
	}
	public String getPurposeRsEqSm() {
		return purposeRsEqSm;
	}
	public void setPurposeRsEqSm(String purposeRsEqSm) {
		this.purposeRsEqSm = purposeRsEqSm;
	}
	public String getTotalRsEqSm() {
		return totalRsEqSm;
	}
	public void setTotalRsEqSm(String totalRsEqSm) {
		this.totalRsEqSm = totalRsEqSm;
	}
	public String getPast30DaysRsEqSm() {
		return past30DaysRsEqSm;
	}
	public void setPast30DaysRsEqSm(String past30DaysRsEqSm) {
		this.past30DaysRsEqSm = past30DaysRsEqSm;
	}
	public String getPast12MonthsRsEqSm() {
		return past12MonthsRsEqSm;
	}
	public void setPast12MonthsRsEqSm(String past12MonthsRsEqSm) {
		this.past12MonthsRsEqSm = past12MonthsRsEqSm;
	}
	public String getPast24MonthsRsEqSm() {
		return past24MonthsRsEqSm;
	}
	public void setPast24MonthsRsEqSm(String past24MonthsRsEqSm) {
		this.past24MonthsRsEqSm = past24MonthsRsEqSm;
	}
	public String getRecentRsEqSm() {
		return recentRsEqSm;
	}
	public void setRecentRsEqSm(String recentRsEqSm) {
		this.recentRsEqSm = recentRsEqSm;
	}
	public String getReportedDateRsEq() {
		return reportedDateRsEq;
	}
	public void setReportedDateRsEq(String reportedDateRsEq) {
		this.reportedDateRsEq = reportedDateRsEq;
	}
	public String getEnqReferenceRsEq() {
		return enqReferenceRsEq;
	}
	public void setEnqReferenceRsEq(String enqReferenceRsEq) {
		this.enqReferenceRsEq = enqReferenceRsEq;
	}
	public String getEnqDateRsEq() {
		return enqDateRsEq;
	}
	public void setEnqDateRsEq(String enqDateRsEq) {
		this.enqDateRsEq = enqDateRsEq;
	}
	public String getEnqTimeRsEq() {
		return enqTimeRsEq;
	}
	public void setEnqTimeRsEq(String enqTimeRsEq) {
		this.enqTimeRsEq = enqTimeRsEq;
	}
	public String getRequestPurposeRsEq() {
		return requestPurposeRsEq;
	}
	public void setRequestPurposeRsEq(String requestPurposeRsEq) {
		this.requestPurposeRsEq = requestPurposeRsEq;
	}
	public String getAmountRsEq() {
		return amountRsEq;
	}
	public void setAmountRsEq(String amountRsEq) {
		this.amountRsEq = amountRsEq;
	}
	public String getAccountsDeliquentRsReAct() {
		return accountsDeliquentRsReAct;
	}
	public void setAccountsDeliquentRsReAct(String accountsDeliquentRsReAct) {
		this.accountsDeliquentRsReAct = accountsDeliquentRsReAct;
	}
	public String getAccountsOpenedRsReAct() {
		return accountsOpenedRsReAct;
	}
	public void setAccountsOpenedRsReAct(String accountsOpenedRsReAct) {
		this.accountsOpenedRsReAct = accountsOpenedRsReAct;
	}
	public String getTotalInquiriesRsReAct() {
		return totalInquiriesRsReAct;
	}
	public void setTotalInquiriesRsReAct(String totalInquiriesRsReAct) {
		this.totalInquiriesRsReAct = totalInquiriesRsReAct;
	}
	public String getAccountsUpdatedRsReAct() {
		return accountsUpdatedRsReAct;
	}
	public void setAccountsUpdatedRsReAct(String accountsUpdatedRsReAct) {
		this.accountsUpdatedRsReAct = accountsUpdatedRsReAct;
	}
	public String getInstitutionGrpSm() {
		return institutionGrpSm;
	}
	public void setInstitutionGrpSm(String institutionGrpSm) {
		this.institutionGrpSm = institutionGrpSm;
	}
	public String getCurrentBalanceGrpSm() {
		return currentBalanceGrpSm;
	}
	public void setCurrentBalanceGrpSm(String currentBalanceGrpSm) {
		this.currentBalanceGrpSm = currentBalanceGrpSm;
	}
	public String getStatusGrpSm() {
		return statusGrpSm;
	}
	public void setStatusGrpSm(String statusGrpSm) {
		this.statusGrpSm = statusGrpSm;
	}
	public String getDateReportedGrpSm() {
		return dateReportedGrpSm;
	}
	public void setDateReportedGrpSm(String dateReportedGrpSm) {
		this.dateReportedGrpSm = dateReportedGrpSm;
	}
	public String getNoOfMembersGrpSm() {
		return noOfMembersGrpSm;
	}
	public void setNoOfMembersGrpSm(String noOfMembersGrpSm) {
		this.noOfMembersGrpSm = noOfMembersGrpSm;
	}
	public String getPastDueAmountGrpSm() {
		return pastDueAmountGrpSm;
	}
	public void setPastDueAmountGrpSm(String pastDueAmountGrpSm) {
		this.pastDueAmountGrpSm = pastDueAmountGrpSm;
	}
	public String getSanctionAmountGrpSm() {
		return sanctionAmountGrpSm;
	}
	public void setSanctionAmountGrpSm(String sanctionAmountGrpSm) {
		this.sanctionAmountGrpSm = sanctionAmountGrpSm;
	}
	public String getDateOpenedGrpSm() {
		return dateOpenedGrpSm;
	}
	public void setDateOpenedGrpSm(String dateOpenedGrpSm) {
		this.dateOpenedGrpSm = dateOpenedGrpSm;
	}
	public String getAccountNoGrpSm() {
		return accountNoGrpSm;
	}
	public void setAccountNoGrpSm(String accountNoGrpSm) {
		this.accountNoGrpSm = accountNoGrpSm;
	}
	public String getMembersPastDueGrpSm() {
		return membersPastDueGrpSm;
	}
	public void setMembersPastDueGrpSm(String membersPastDueGrpSm) {
		this.membersPastDueGrpSm = membersPastDueGrpSm;
	}
	public String getWriteOffAmountGrpSm() {
		return writeOffAmountGrpSm;
	}
	public void setWriteOffAmountGrpSm(String writeOffAmountGrpSm) {
		this.writeOffAmountGrpSm = writeOffAmountGrpSm;
	}
	public String getWriteOffDateGrpSm() {
		return writeOffDateGrpSm;
	}
	public void setWriteOffDateGrpSm(String writeOffDateGrpSm) {
		this.writeOffDateGrpSm = writeOffDateGrpSm;
	}
	public String getReasonCodeSc() {
		return reasonCodeSc;
	}
	public void setReasonCodeSc(String reasonCodeSc) {
		this.reasonCodeSc = reasonCodeSc;
	}
	public String getScoreDescriptionSc() {
		return scoreDescriptionSc;
	}
	public void setScoreDescriptionSc(String scoreDescriptionSc) {
		this.scoreDescriptionSc = scoreDescriptionSc;
	}
	public String getScoreNameSc() {
		return scoreNameSc;
	}
	public void setScoreNameSc(String scoreNameSc) {
		this.scoreNameSc = scoreNameSc;
	}
	public String getScoreValueSc() {
		return scoreValueSc;
	}
	public void setScoreValueSc(String scoreValueSc) {
		this.scoreValueSc = scoreValueSc;
	}
	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}
	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}
	public String getOutputWriteTime() {
		return outputWriteTime;
	}
	public void setOutputWriteTime(String outputWriteTime) {
		this.outputWriteTime = outputWriteTime;
	}
	public String getOutputReadTime() {
		return outputReadTime;
	}
	public void setOutputReadTime(String outputReadTime) {
		this.outputReadTime = outputReadTime;
	}
	
	public String getAccountKey() {
		return accountKey;
	}

	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}

	@Override
	public String toString() {
		return "EquifaxSROPDomain [srNo=" + srNo + ", soaSourceName="
				+ soaSourceName + ", memberReferenceNumber="
				+ memberReferenceNumber + ", enquiryDate=" + enquiryDate
				+ ", customerCode=" + customerCode + ", firstnameNmTp="
				+ firstnameNmTp + ", midlleNameNmTp=" + midlleNameNmTp
				+ ", lastNameNmTp=" + lastNameNmTp + ", addMidlleNameNmTp="
				+ addMidlleNameNmTp + ", suffixNmTp=" + suffixNmTp
				+ ", reportedDateAgIf=" + reportedDateAgIf + ", ageAgIf="
				+ ageAgIf + ", reportedDateAlNm=" + reportedDateAlNm
				+ ", aliasNameAlNm=" + aliasNameAlNm
				+ ", additionalNameInfoAdNmIf=" + additionalNameInfoAdNmIf
				+ ", noOfDependentsAdNmIf=" + noOfDependentsAdNmIf
				+ ", reportedDateRsAdd=" + reportedDateRsAdd
				+ ", addressRsAdd=" + addressRsAdd + ", stateRsAdd="
				+ stateRsAdd + ", postalRsAdd=" + postalRsAdd
				+ ", addTypeRsAdd=" + addTypeRsAdd + ", idTypeRsId="
				+ idTypeRsId + ", reportedDateRsId=" + reportedDateRsId
				+ ", idNumberRsId=" + idNumberRsId + ", emailReportedDateRsMl="
				+ emailReportedDateRsMl + ", emailAddressRsMl="
				+ emailAddressRsMl + ", empNMRepDateRsEMDL="
				+ empNMRepDateRsEMDL + ", employerNameRsEmdl="
				+ employerNameRsEmdl + ", empPositionRsEmdl="
				+ empPositionRsEmdl + ", empAddressRsEmdl=" + empAddressRsEmdl
				+ ", dobRsPerIf=" + dobRsPerIf + ", genderRsPerIf="
				+ genderRsPerIf + ", totalIncomeRsPerIf=" + totalIncomeRsPerIf
				+ ", occupationRsPerIf=" + occupationRsPerIf
				+ ", maritalStausRsPerIf=" + maritalStausRsPerIf
				+ ", occupationIcDl=" + occupationIcDl + ", monthlyIncomeIcDl="
				+ monthlyIncomeIcDl + ", monthlyExpenseIcDl="
				+ monthlyExpenseIcDl + ", povertyIndexIcDl=" + povertyIndexIcDl
				+ ", assetOwnershipIcDl=" + assetOwnershipIcDl
				+ ", reportedDateRsPh=" + reportedDateRsPh + ", typeCodeRsPh="
				+ typeCodeRsPh + ", countryCodeRsPh=" + countryCodeRsPh
				+ ", areaCodeRsPh=" + areaCodeRsPh + ", phNumberRsPh="
				+ phNumberRsPh + ", phNoExtentionRsPh=" + phNoExtentionRsPh
				+ ", customercodeRsHd=" + customercodeRsHd + ", clientIdRsHd="
				+ clientIdRsHd + ", custRefFieldRsHd=" + custRefFieldRsHd
				+ ", reportOrderNoRsHd=" + reportOrderNoRsHd
				+ ", productCodeRsHd=" + productCodeRsHd
				+ ", productVersionRsHd=" + productVersionRsHd
				+ ", successCodeRsHd=" + successCodeRsHd + ", matchTypeRsHd="
				+ matchTypeRsHd + ", resDateRsHd=" + resDateRsHd
				+ ", resTimeRsHd=" + resTimeRsHd + ", reportedDateRsAc="
				+ reportedDateRsAc + ", clientNameRsAc=" + clientNameRsAc
				+ ", accountNumberRsAc=" + accountNumberRsAc
				+ ", currentBalanceRsAc=" + currentBalanceRsAc
				+ ", institutionRsAc=" + institutionRsAc + ", accountTypeRsAc="
				+ accountTypeRsAc + ", ownershipTypeRsAc=" + ownershipTypeRsAc
				+ ", balanceRsAc=" + balanceRsAc + ", pastDueAmtRsAc="
				+ pastDueAmtRsAc + ", disbursedAmountRsAc="
				+ disbursedAmountRsAc + ", loanCategoryRsAc="
				+ loanCategoryRsAc + ", loanPurposeRsAc=" + loanPurposeRsAc
				+ ", lastPaymentRsAc=" + lastPaymentRsAc + ", writeOffAmtRsAc="
				+ writeOffAmtRsAc + ", accOpenRsAc=" + accOpenRsAc
				+ ", sactionAmtRsAc=" + sactionAmtRsAc
				+ ", lastPaymentDateRsAc=" + lastPaymentDateRsAc
				+ ", highCreditRsAc=" + highCreditRsAc + ", dateReportedRsAc="
				+ dateReportedRsAc + ", dateOpenedRsAc=" + dateOpenedRsAc
				+ ", dateClosedRsAc=" + dateClosedRsAc + ", reasonRsAc="
				+ reasonRsAc + ", dateWrittenOffRsAc=" + dateWrittenOffRsAc
				+ ", loanCycleIdRsAc=" + loanCycleIdRsAc
				+ ", dateSanctionedRsAc=" + dateSanctionedRsAc
				+ ", dateAppliedRsAc=" + dateAppliedRsAc + ", intrestRateRSAC="
				+ intrestRateRSAC + ", appliedAmountRsAc=" + appliedAmountRsAc
				+ ", noOfInstallmentsRsAc=" + noOfInstallmentsRsAc
				+ ", repaymentTenureRsAc=" + repaymentTenureRsAc
				+ ", disputeCodeRsAc=" + disputeCodeRsAc
				+ ", installmentAmtRsAc=" + installmentAmtRsAc
				+ ", keyPersonRsAc=" + keyPersonRsAc + ", nomineeRsAc="
				+ nomineeRsAc + ", termFrequencyRsAc=" + termFrequencyRsAc
				+ ", creditLimitRsAc=" + creditLimitRsAc
				+ ", collateralValueRsAc=" + collateralValueRsAc
				+ ", collateralTypeRsAc=" + collateralTypeRsAc
				+ ", accountStatusRsAc=" + accountStatusRsAc
				+ ", assetClassificationRsAc=" + assetClassificationRsAc
				+ ", suitFiledStatusRsAc=" + suitFiledStatusRsAc
				+ ", monthKeyRsAcHis24=" + monthKeyRsAcHis24
				+ ", paymentStatusRsAcHis24=" + paymentStatusRsAcHis24
				+ ", suitFiledStatusRsAcHis24=" + suitFiledStatusRsAcHis24
				+ ", assettClsStatusRsAcHis24=" + assettClsStatusRsAcHis24
				+ ", monthKeyRsAcHis48=" + monthKeyRsAcHis48
				+ ", pAYMENTSTATUSRsAcHis48=" + pAYMENTSTATUSRsAcHis48
				+ ", sUITFILEDSTATUSRsAcHis48=" + sUITFILEDSTATUSRsAcHis48
				+ ", aSSETCLSSTATUSRsAcHis48=" + aSSETCLSSTATUSRsAcHis48
				+ ", noOfAccountsRsAcSum=" + noOfAccountsRsAcSum
				+ ", noOfActiveAccountsRsAcSum=" + noOfActiveAccountsRsAcSum
				+ ", noOfWriteOffsRsAcSum=" + noOfWriteOffsRsAcSum
				+ ", totalPastDueRsAcSum=" + totalPastDueRsAcSum
				+ ", mostSrvrStaWhin24MonRsAcSum="
				+ mostSrvrStaWhin24MonRsAcSum + ", singleHighestCreditRsAcSum="
				+ singleHighestCreditRsAcSum + ", singleHighSanctAmtRsAcSum="
				+ singleHighSanctAmtRsAcSum + ", totalHighCreditRsAcSum="
				+ totalHighCreditRsAcSum + ", averageOpenBalanceRsAcSum="
				+ averageOpenBalanceRsAcSum + ", singleHighestBalanceRsAcSum="
				+ singleHighestBalanceRsAcSum + ", noOfPastDueAcctsRsAcSum="
				+ noOfPastDueAcctsRsAcSum + ", noOfZeroBalAcctsRsAcSum="
				+ noOfZeroBalAcctsRsAcSum + ", recentAccountRsAcSum="
				+ recentAccountRsAcSum + ", oldestAccountRsAcSum="
				+ oldestAccountRsAcSum + ", totalBalAmtRsAcSum="
				+ totalBalAmtRsAcSum + ", totalSanctAmtRsAcSum="
				+ totalSanctAmtRsAcSum + ", totalCrdtLimitRsAcSum="
				+ totalCrdtLimitRsAcSum + ", totalMonlyPymntAmtRsAcSum="
				+ totalMonlyPymntAmtRsAcSum + ", totalWrittenOffAmntRsAcSum="
				+ totalWrittenOffAmntRsAcSum + ", ageOfOldestTradeRsOtk="
				+ ageOfOldestTradeRsOtk + ", numberOfOpenTradesRsOtk="
				+ numberOfOpenTradesRsOtk + ", alineSeverWrittenRsOtk="
				+ alineSeverWrittenRsOtk + ", alineSvWrtnIn9MnthsRsOtk="
				+ alineSvWrtnIn9MnthsRsOtk + ", alineSvrWrTnIn6MnthsRsOtk="
				+ alineSvrWrTnIn6MnthsRsOtk + ", reportedDateRsEqSm="
				+ reportedDateRsEqSm + ", purposeRsEqSm=" + purposeRsEqSm
				+ ", totalRsEqSm=" + totalRsEqSm + ", past30DaysRsEqSm="
				+ past30DaysRsEqSm + ", past12MonthsRsEqSm="
				+ past12MonthsRsEqSm + ", past24MonthsRsEqSm="
				+ past24MonthsRsEqSm + ", recentRsEqSm=" + recentRsEqSm
				+ ", reportedDateRsEq=" + reportedDateRsEq
				+ ", enqReferenceRsEq=" + enqReferenceRsEq + ", enqDateRsEq="
				+ enqDateRsEq + ", enqTimeRsEq=" + enqTimeRsEq
				+ ", requestPurposeRsEq=" + requestPurposeRsEq
				+ ", amountRsEq=" + amountRsEq + ", accountsDeliquentRsReAct="
				+ accountsDeliquentRsReAct + ", accountsOpenedRsReAct="
				+ accountsOpenedRsReAct + ", totalInquiriesRsReAct="
				+ totalInquiriesRsReAct + ", accountsUpdatedRsReAct="
				+ accountsUpdatedRsReAct + ", institutionGrpSm="
				+ institutionGrpSm + ", currentBalanceGrpSm="
				+ currentBalanceGrpSm + ", statusGrpSm=" + statusGrpSm
				+ ", dateReportedGrpSm=" + dateReportedGrpSm
				+ ", noOfMembersGrpSm=" + noOfMembersGrpSm
				+ ", pastDueAmountGrpSm=" + pastDueAmountGrpSm
				+ ", sanctionAmountGrpSm=" + sanctionAmountGrpSm
				+ ", dateOpenedGrpSm=" + dateOpenedGrpSm + ", accountNoGrpSm="
				+ accountNoGrpSm + ", membersPastDueGrpSm="
				+ membersPastDueGrpSm + ", writeOffAmountGrpSm="
				+ writeOffAmountGrpSm + ", writeOffDateGrpSm="
				+ writeOffDateGrpSm + ", reasonCodeSc=" + reasonCodeSc
				+ ", scoreDescriptionSc=" + scoreDescriptionSc
				+ ", scoreNameSc=" + scoreNameSc + ", scoreValueSc="
				+ scoreValueSc + ", outputWriteFlag=" + outputWriteFlag
				+ ", outputWriteTime=" + outputWriteTime + ", outputReadTime="
				+ outputReadTime + ", accountKey=" + accountKey + ", code="
				+ code + ", description=" + description + ", accntNum="
				+ accntNum + ", disputeComments=" + disputeComments
				+ ", status=" + status + ", resolvedDate=" + resolvedDate + "]";
	}

	
}
