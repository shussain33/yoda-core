package com.softcell.gonogo.model.mbdatapush.chm;

public class Summary {
	private String status;
	private String noOfDefaultAccounts;
	private String totalResponses;
	private String noOfClosedAccounts;
	private String noOfActiveAccounts;
	private String noOfOtherMfis;
	private String noOfOwnMfi;
	private String ownMfiIndicator;
	private String totalOwnDisbursedAmount;
	private String totalOtherDisbursedAmount;
	private String totalOwnCurrentBalance;
	private String totalOtherCurrentBalance;
	private String totalOwnInstallmentAmount;
	private String totalOtherInstallmentAmount;
	private String maxWorstDelequency;
	private String errors;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNoOfDefaultAccounts() {
		return noOfDefaultAccounts;
	}
	public void setNoOfDefaultAccounts(String noOfDefaultAccounts) {
		this.noOfDefaultAccounts = noOfDefaultAccounts;
	}
	public String getTotalResponses() {
		return totalResponses;
	}
	public void setTotalResponses(String totalResponses) {
		this.totalResponses = totalResponses;
	}
	public String getNoOfClosedAccounts() {
		return noOfClosedAccounts;
	}
	public void setNoOfClosedAccounts(String noOfClosedAccounts) {
		this.noOfClosedAccounts = noOfClosedAccounts;
	}
	public String getNoOfActiveAccounts() {
		return noOfActiveAccounts;
	}
	public void setNoOfActiveAccounts(String noOfActiveAccounts) {
		this.noOfActiveAccounts = noOfActiveAccounts;
	}
	public String getNoOfOtherMfis() {
		return noOfOtherMfis;
	}
	public String getNoOfOwnMfi() {
		return noOfOwnMfi;
	}
	public void setNoOfOwnMfi(String noOfOwnMfi) {
		this.noOfOwnMfi = noOfOwnMfi;
	}
	public void setNoOfOtherMfis(String noOfOtherMfis) {
		this.noOfOtherMfis = noOfOtherMfis;
	}
	public String getOwnMfiIndicator() {
		return ownMfiIndicator;
	}
	public void setOwnMfiIndicator(String ownMfiIndicator) {
		this.ownMfiIndicator = ownMfiIndicator;
	}
	public String getTotalOwnDisbursedAmount() {
		return totalOwnDisbursedAmount;
	}
	public void setTotalOwnDisbursedAmount(String totalOwnDisbursedAmount) {
		this.totalOwnDisbursedAmount = totalOwnDisbursedAmount;
	}
	public String getTotalOtherDisbursedAmount() {
		return totalOtherDisbursedAmount;
	}
	public void setTotalOtherDisbursedAmount(String totalOtherDisbursedAmount) {
		this.totalOtherDisbursedAmount = totalOtherDisbursedAmount;
	}
	public String getTotalOwnCurrentBalance() {
		return totalOwnCurrentBalance;
	}
	public void setTotalOwnCurrentBalance(String totalOwnCurrentBalance) {
		this.totalOwnCurrentBalance = totalOwnCurrentBalance;
	}
	public String getTotalOtherCurrentBalance() {
		return totalOtherCurrentBalance;
	}
	public void setTotalOtherCurrentBalance(String totalOtherCurrentBalance) {
		this.totalOtherCurrentBalance = totalOtherCurrentBalance;
	}
	public String getTotalOwnInstallmentAmount() {
		return totalOwnInstallmentAmount;
	}
	public void setTotalOwnInstallmentAmount(String totalOwnInstallmentAmount) {
		this.totalOwnInstallmentAmount = totalOwnInstallmentAmount;
	}
	public String getTotalOtherInstallmentAmount() {
		return totalOtherInstallmentAmount;
	}
	public void setTotalOtherInstallmentAmount(String totalOtherInstallmentAmount) {
		this.totalOtherInstallmentAmount = totalOtherInstallmentAmount;
	}
	public String getMaxWorstDelequency() {
		return maxWorstDelequency;
	}
	public void setMaxWorstDelequency(String maxWorstDelequency) {
		this.maxWorstDelequency = maxWorstDelequency;
	}
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	@Override
	public String toString() {
		return "Summary [status=" + status + ", noOfDefaultAccounts="
				+ noOfDefaultAccounts + ", totalResponses=" + totalResponses
				+ ", noOfClosedAccounts=" + noOfClosedAccounts
				+ ", noOfActiveAccounts=" + noOfActiveAccounts
				+ ", noOfOtherMfis=" + noOfOtherMfis + ", noOfOwnMfi="
				+ noOfOwnMfi + ", ownMfiIndicator=" + ownMfiIndicator
				+ ", totalOwnDisbursedAmount=" + totalOwnDisbursedAmount
				+ ", totalOtherDisbursedAmount=" + totalOtherDisbursedAmount
				+ ", totalOwnCurrentBalance=" + totalOwnCurrentBalance
				+ ", totalOtherCurrentBalance=" + totalOtherCurrentBalance
				+ ", totalOwnInstallmentAmount=" + totalOwnInstallmentAmount
				+ ", totalOtherInstallmentAmount="
				+ totalOtherInstallmentAmount + ", maxWorstDelequency="
				+ maxWorstDelequency + ", errors=" + errors + "]";
	}
}
