package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.List;

public class Responses {
	
	List<Response> response;

	public List<Response> getResponse() {
		return response;
	}

	public void setResponse(List<Response> response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "Responses [response=" + response + "]";
	}
	
	
	

}
