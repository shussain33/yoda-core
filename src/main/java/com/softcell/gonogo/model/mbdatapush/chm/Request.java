package com.softcell.gonogo.model.mbdatapush.chm;


public class Request {
	private String name;
	private String aka;
	private String spouse;
	private String father;
	private String mother;
	private String dob;
	private String age;
	private String ageAsOn;
	private String rationCard;
	private String passport;
	private String voterId;
	private String drivingLicenseNo;
	private String pan;
	private String gender;
	private String ownership;
	private String address1;
	private String address2;
	private String address3;
	private String phone1;
	private String phone2;
	private String phone3;
	private String email1;
	private String email2;
	private String kendra;
	private String branch;
	private String memberId;
	private String creditInquiryPurposeType;
	private String creditInquiryPurposeTypeDescription;
	private String creditInquiryStage;
	private String creditReportId;
	private String creditRequestType;
	private String creditReportTransectionDatetime;
	private String losApplicationId;
	private String accountOpenDate;
	private String loanAmount;
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAka() {
		return aka;
	}


	public void setAka(String aka) {
		this.aka = aka;
	}


	public String getSpouse() {
		return spouse;
	}


	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}


	public String getFather() {
		return father;
	}


	public void setFather(String father) {
		this.father = father;
	}


	public String getMother() {
		return mother;
	}


	public void setMother(String mother) {
		this.mother = mother;
	}


	public String getDob() {
		return dob;
	}


	public void setDob(String dob) {
		this.dob = dob;
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public String getAgeAsOn() {
		return ageAsOn;
	}


	public void setAgeAsOn(String ageAsOn) {
		this.ageAsOn = ageAsOn;
	}


	public String getRationCard() {
		return rationCard;
	}


	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}


	public String getPassport() {
		return passport;
	}


	public void setPassport(String passport) {
		this.passport = passport;
	}


	public String getVotersId() {
		return voterId;
	}


	public void setVotersId(String votersId) {
		this.voterId = votersId;
	}


	public String getDrivingLicenseNo() {
		return drivingLicenseNo;
	}


	public void setDrivingLicenseNo(String drivingLicenseNo) {
		this.drivingLicenseNo = drivingLicenseNo;
	}


	public String getPan() {
		return pan;
	}


	public void setPan(String pan) {
		this.pan = pan;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getOwnership() {
		return ownership;
	}


	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getAddress3() {
		return address3;
	}


	public void setAddress3(String address3) {
		this.address3 = address3;
	}


	public String getPhone1() {
		return phone1;
	}


	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}


	public String getPhone2() {
		return phone2;
	}


	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}


	public String getPhone3() {
		return phone3;
	}


	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}


	public String getEmail1() {
		return email1;
	}


	public void setEmail1(String email1) {
		this.email1 = email1;
	}


	public String getEmail2() {
		return email2;
	}


	public void setEmail2(String email2) {
		this.email2 = email2;
	}


	public String getKendra() {
		return kendra;
	}


	public void setKendra(String kendra) {
		this.kendra = kendra;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getMemberId() {
		return memberId;
	}


	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}


	public String getCreditInquiryPurposeType() {
		return creditInquiryPurposeType;
	}


	public void setCreditInquiryPurposeType(String creditInquiryPurposeType) {
		this.creditInquiryPurposeType = creditInquiryPurposeType;
	}


	public String getCreditInquiryPurposeTypeDescription() {
		return creditInquiryPurposeTypeDescription;
	}


	public void setCreditInquiryPurposeTypeDescription(
			String creditInquiryPurposeTypeDescription) {
		this.creditInquiryPurposeTypeDescription = creditInquiryPurposeTypeDescription;
	}


	public String getCreditInquiryStage() {
		return creditInquiryStage;
	}


	public void setCreditInquiryStage(String creditInquiryStage) {
		this.creditInquiryStage = creditInquiryStage;
	}


	public String getCreditReportId() {
		return creditReportId;
	}


	public void setCreditReportId(String creditReportId) {
		this.creditReportId = creditReportId;
	}


	public String getCreditRequestType() {
		return creditRequestType;
	}


	public void setCreditRequestType(String creditRequestType) {
		this.creditRequestType = creditRequestType;
	}


	public String getCreditReportTransectionDatetime() {
		return creditReportTransectionDatetime;
	}


	public void setCreditReportTransectionDatetime(
			String creditReportTransectionDatetime) {
		this.creditReportTransectionDatetime = creditReportTransectionDatetime;
	}


	public String getLosApplicationId() {
		return losApplicationId;
	}


	public void setLosApplicationId(String losApplicationId) {
		this.losApplicationId = losApplicationId;
	}


	public String getAccountOpenDate() {
		return accountOpenDate;
	}


	public void setAccountOpenDate(String accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}


	public String getLoanAmount() {
		return loanAmount;
	}


	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}


	@Override
	public String toString() {
		return "Request [name=" + name + ", aka=" + aka + ", spouse=" + spouse
				+ ", father=" + father + ", mother=" + mother + ", dob=" + dob
				+ ", age=" + age + ", ageAsOn=" + ageAsOn + ", rationCard="
				+ rationCard + ", passport=" + passport + ", votersId="
				+ voterId + ", drivingLicenseNo=" + drivingLicenseNo
				+ ", pan=" + pan + ", gender=" + gender + ", ownership="
				+ ownership + ", address1=" + address1 + ", address2="
				+ address2 + ", address3=" + address3 + ", phone1=" + phone1
				+ ", phone2=" + phone2 + ", phone3=" + phone3 + ", email1="
				+ email1 + ", email2=" + email2 + ", kendra=" + kendra
				+ ", branch=" + branch + ", memberId=" + memberId
				+ ", creditInquiryPurposeType=" + creditInquiryPurposeType
				+ ", creditInquiryPurposeTypeDescription="
				+ creditInquiryPurposeTypeDescription + ", creditInquiryStage="
				+ creditInquiryStage + ", creditReportId=" + creditReportId
				+ ", creditRequestType=" + creditRequestType
				+ ", creditReportTransectionDatetime="
				+ creditReportTransectionDatetime + ", losApplicationId="
				+ losApplicationId + ", accountOpenDate=" + accountOpenDate
				+ ", loanAmount=" + loanAmount + "]";
	}


	
	

}
