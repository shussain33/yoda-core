package com.softcell.gonogo.model.mbdatapush.chm;

public class OverlapReport {
	private Header header;
	private Request request;
	private IndvResponse indvResponse;
	private GrpResponses grpResponses;
	private InquiryHistory inquiryHistory;
	private PrintableReport printableReport;

	
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public IndvResponse getIndvResponse() {
		return indvResponse;
	}
	public void setIndvResponse(IndvResponse indvResponse) {
		this.indvResponse = indvResponse;
	}
	public GrpResponses getGrpResponses() {
		return grpResponses;
	}
	public void setGrpResponses(GrpResponses grpResponses) {
		this.grpResponses = grpResponses;
	}
	public InquiryHistory getInquiryHistory() {
		return inquiryHistory;
	}
	public void setInquiryHistory(InquiryHistory inquiryHistory) {
		this.inquiryHistory = inquiryHistory;
	}
	public PrintableReport getPrintableReport() {
		return printableReport;
	}
	public void setPrintableReport(PrintableReport printableReport) {
		this.printableReport = printableReport;
	}
	@Override
	public String toString() {
		return "OverlapReport [header=" + header + ", request=" + request
				+ ", indvResponse=" + indvResponse + ", grpResponses="
				+ grpResponses + ", inquiryHistory=" + inquiryHistory
				+ ", printableReport=" + printableReport + "]";
	}
}
