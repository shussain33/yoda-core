package com.softcell.gonogo.model.mbdatapush.chm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author Dipak
 *
 */
public class HibHighmarkAdOverlapSropDomain implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  Integer id;
	@Expose
	@SerializedName("SRNO")
	private  Integer srno;
	@Expose
	@SerializedName("MEMBER_REFERENCE_NUMBER")
	private  String memberReferenceNumber;
	@Expose
	@SerializedName("SOA_SOURCE_NAME")
	private  String soaSourceName;
	@Expose
	@SerializedName("DATE_OF_REQUEST")
	private  Date dateOfRequest;
	@Expose
	@SerializedName("PREPARED_FOR")
	private  String preparedFor;
	@Expose
	@SerializedName("PREPARED_FOR_ID")
	private  String preparedForId;
	@Expose
	@SerializedName("DATE_OF_ISSUE")
	private  Date dateOfIssue;
	@Expose
	@SerializedName("REPORT_ID")
	private  String reportId;
	@Expose
	@SerializedName("NAME_IQ")
	private  String nameIq;
	@Expose
	@SerializedName("SPOUSE_IQ")
	private  String spouseIq;
	@Expose
	@SerializedName("FATHER_IQ")
	private  String fatherIq;
	@Expose
	@SerializedName("MOTHER_IQ")
	private  String motherIq;
	@Expose
	@SerializedName("DOB_IQ")
	private  Date dobIq;
	@Expose
	@SerializedName("AGE_IQ")
	private  Integer ageIq;
	@Expose
	@SerializedName("AGE_AS_ON_IQ")
	private  Date ageAsOnIq;
	@Expose
	@SerializedName("GENDER_IQ")
	private  String genderIq;
	@Expose
	@SerializedName("PHONE_1_IQ")
	private  BigInteger phone1Iq;
	@Expose
	@SerializedName("PHONE_2_IQ")
	private  BigInteger phone2Iq;
	@Expose
	@SerializedName("PHONE_3_IQ")
	private  BigInteger phone3Iq;
	@Expose
	@SerializedName("ADDRESS_1_IQ")
	private  String address1Iq;
	@Expose
	@SerializedName("ADDRESS_2_IQ")
	private  String address2Iq;
	@Expose
	@SerializedName("REL_TYP_1_IQ")
	private  String relTyp1Iq;
	@Expose
	@SerializedName("REL_NM_1_IQ")
	private  String relNm1Iq;
	@Expose
	@SerializedName("REL_TYP_2_IQ")
	private  String relTyp2Iq;
	@Expose
	@SerializedName("REL_NM_2_IQ")
	private  String relNm2Iq;
	@Expose
	@SerializedName("REL_TYP_3_IQ")
	private  String relTyp3Iq;
	@Expose
	@SerializedName("REL_NM_3_IQ")
	private  String relNm3Iq;
	@Expose
	@SerializedName("REL_TYP_4_IQ")
	private  String relTyp4Iq;
	@Expose
	@SerializedName("REL_NM_4_IQ")
	private  String relNm4Iq;
	@Expose
	@SerializedName("ID_TYPE_1_IQ")
	private  String idType1Iq;
	@Expose
	@SerializedName("ID_VALUE_1_IQ")
	private  String idValue1Iq;
	@Expose
	@SerializedName("ID_TYPE_2_IQ")
	private  String idType2Iq;
	@Expose
	@SerializedName("ID_VALUE_2_IQ")
	private  String idValue2Iq;
	@Expose
	@SerializedName("RATION_CARD_IQ")
	private  String rationCardIq;
	@Expose
	@SerializedName("VOTERS_ID_IQ")
	private  String votersIdIq;
	@Expose
	@SerializedName("DRIVING_LICENCE_NO_IQ")
	private  String drivingLicenceNoIq;
	@Expose
	@SerializedName("PAN_IQ")
	private  String panIq;
	@Expose
	@SerializedName("PASSPORT_IQ")
	private  String passportIq;
	@Expose
	@SerializedName("OTHER_ID_IQ")
	private  String otherIdIq;
	@Expose
	@SerializedName("BRANCH_IQ")
	private  String branchIq;
	@Expose
	@SerializedName("KENDRA_IQ")
	private  String kendraIq;
	@Expose
	@SerializedName("MBR_ID_IQ")
	private  String mbrIdIq;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP")
	private  String credtIqPurpsTyp;
	@Expose
	@SerializedName("CREDT_INQ_PURPS_TYP_DESC")
	private  String credtInqPurpsTypDesc;
	@Expose
	@SerializedName("CREDIT_INQUIRY_STAGE")
	private  String creditInquiryStage;
	@Expose
	@SerializedName("CREDT_RPT_ID")
	private  String credtRptId;
	@Expose
	@SerializedName("CREDT_REQ_TYP")
	private  String credtReqTyp;
	@Expose
	@SerializedName("CREDT_RPT_TRN_DT_TM")
	private  String credtRptTrnDtTm;
	@Expose
	@SerializedName("AC_OPEN_DT")
	private  Date acOpenDt;
	@Expose
	@SerializedName("LOAN_AMOUNT")
	private  BigInteger loanAmount;
	@Expose
	@SerializedName("ENTITY_ID")
	private  String entityId;
	@Expose
	@SerializedName("IND_STATUS_PR")
	private  String statusIndPr;
	@Expose
	@SerializedName("IND_NO_OF_DEFAULT_ACCOUNTS_PR")
	private  BigInteger noOfDefaultAccountsIndPr;
	@Expose
	@SerializedName("IND_TOTAL_RESPONSES_PR")
	private  BigInteger totalResponsesIndIndPr;
	@Expose
	@SerializedName("IND_NO_OF_CLOSED_ACCOUNTS_PR")
	private  BigInteger noOfClosedAccountsIndPr;
	@Expose
	@SerializedName("IND_NO_OF_ACTIVE_ACCOUNTS_PR")
	private  BigInteger noOfActiveAccountsIndPr;
	@Expose
	@SerializedName("IND_NO_OF_OTHER_MFIS_PR")
	private  BigInteger noOfOtherMfisIndPr;
	@Expose
	@SerializedName("IND_OWN_MFI_INDECATOR_PR")
	private  String ownMfiIndecatorIndPr;
	@Expose
	@SerializedName("IND_TTL_OWN_DISBURSD_AMT_PR")
	private  BigInteger totalOwnDisbursedAmountIndPr;
	@Expose
	@SerializedName("IND_TTL_OTHER_DISBURSD_AMT_PR")
	private  BigInteger totalOtherDisbursedAmountIndPr;
	@Expose
	@SerializedName("IND_TTL_OWN_CURRENT_BAL_PR")
	private  BigInteger totalOwnCurrentBalanceIndPr;
	@Expose
	@SerializedName("IND_TTL_OTHER_CURRENT_BAL_PR")
	private  BigInteger totalOtherCurrentBalanceIndPr;
	@Expose
	@SerializedName("IND_TTL_OWN_INSTLMNT_AMT_PR")
	private  BigInteger totalOwnInstallmentAmountIndPr;
	@Expose
	@SerializedName("IND_TTL_OTHER_INSTLMNT_AMT_PR")
	private  BigInteger totalOtherInstallmentAmountIndPr;
	@Expose
	@SerializedName("IND_MAX_WORST_DELEQUENCY_PR")
	private  BigInteger maxWorstDelequencyIndPr;
	@Expose
	@SerializedName("IND_ERRORS_PR")
	private  String errorsIndPr;
	@Expose
	@SerializedName("IND_STATUS_SE")
	private  String statusIndSe;
	@Expose
	@SerializedName("IND_NO_OF_DEFAULT_ACCOUNTS_SE")
	private  BigInteger noOfDefaultAccountsIndSe;
	@Expose
	@SerializedName("IND_TOTAL_RESPONSES_SE")
	private  BigInteger totalResponsesIndIndSe;
	@Expose
	@SerializedName("IND_NO_OF_CLOSED_ACCOUNTS_SE")
	private  BigInteger noOfClosedAccountsIndSe;
	@Expose
	@SerializedName("IND_NO_OF_ACTIVE_ACCOUNTS_SE")
	private  BigInteger noOfActiveAccountsIndSe;
	@Expose
	@SerializedName("IND_NO_OF_OTHER_MFIS_SE")
	private  BigInteger noOfOtherMfisIndSe;
	@Expose
	@SerializedName("IND_OWN_MFI_INDECATOR_SE")
	private  String ownMfiIndecatorIndSe;
	@Expose
	@SerializedName("IND_TTL_OWN_DISBURSD_AMT_SE")
	private  BigInteger totalOwnDisbursedAmountIndSe;
	@Expose
	@SerializedName("IND_TTL_OTHER_DISBURSD_AMT_SE")
	private  BigInteger totalOtherDisbursedAmountIndSe;
	@Expose
	@SerializedName("IND_TTL_OWN_CURRENT_BAL_SE")
	private  BigInteger totalOwnCurrentBalanceIndSe;
	@Expose
	@SerializedName("IND_TTL_OTHER_CURRENT_BAL_SE")
	private  BigInteger totalOtherCurrentBalanceIndSe;
	@Expose
	@SerializedName("IND_TTL_OWN_INSTLMNT_AMT_SE")
	private  BigInteger totalOwnInstallmentAmountIndSe;
	@Expose
	@SerializedName("IND_TTL_OTHER_INSTLMNT_AMT_SE")
	private  BigInteger totalOtherInstallmentAmountIndSe;
	@Expose
	@SerializedName("IND_MAX_WORST_DELEQUENCY_SE")
	private  BigInteger maxWorstDelequencyIndSe;
	@Expose
	@SerializedName("IND_ERRORS_SE")
	private  String errorsIndSe;
	@Expose
	@SerializedName("IND_STATUS_SM")
	private  String statusInd;
	@Expose
	@SerializedName("IND_NO_OF_DEFAULT_ACCOUNTS_SM")
	private  BigInteger noOfDefaultAccountsInd;
	@Expose
	@SerializedName("IND_TOTAL_RESPONSES_SM")
	private  BigInteger totalResponsesInd;
	@Expose
	@SerializedName("IND_NO_OF_CLOSED_ACCOUNTS_SM")
	private  BigInteger noOfClosedAccountsInd;
	@Expose
	@SerializedName("IND_NO_OF_ACTIVE_ACCOUNTS_SM")
	private  BigInteger noOfActiveAccountsInd;
	@Expose
	@SerializedName("IND_NO_OF_OTHER_MFIS_SM")
	private  BigInteger noOfOtherMfisInd;
	@Expose
	@SerializedName("IND_OWN_MFI_INDECATOR_SM")
	private  String ownMfiIndecatorInd;
	@Expose
	@SerializedName("IND_TTL_OWN_DISBURSD_AMT_SM")
	private  BigInteger totalOwnDisbursedAmountInd;
	@Expose
	@SerializedName("IND_TTL_OTHER_DISBURSD_AMT_SM")
	private  BigInteger totalOtherDisbursedAmountInd;
	@Expose
	@SerializedName("IND_TTL_OWN_CURRENT_BAL_SM")
	private  BigInteger totalOwnCurrentBalanceInd;
	@Expose
	@SerializedName("IND_TTL_OTHER_CURRENT_BAL_SM")
	private  BigInteger totalOtherCurrentBalanceInd;
	@Expose
	@SerializedName("IND_TTL_OWN_INSTLMNT_AMT_SM")
	private  BigInteger totalOwnInstallmentAmountInd;
	@Expose
	@SerializedName("IND_TTL_OTHER_INSTLMNT_AMT_SM")
	private  BigInteger totalOtherInstallmentAmountInd;
	@Expose
	@SerializedName("IND_MAX_WORST_DELEQUENCY_SM")
	private  BigInteger maxWorstDelequencyInd;
	@Expose
	@SerializedName("IND_ERRORS_SM")
	private  String errorsInd;
	@Expose
	@SerializedName("IND_MATCHED_TYPE")
	private  String matchedTypeInd;
	@Expose
	@SerializedName("IND_MFI")
	private  String mfiInd;
	@Expose
	@SerializedName("IND_MFI_ID")
	private  String mfiIdInd;
	@Expose
	@SerializedName("IND_BRANCH")
	private  String branchInd;
	@Expose
	@SerializedName("IND_KENDRA")
	private  String kendraInd;
	@Expose
	@SerializedName("IND_NAME")
	private  String nameInd;
	@Expose
	@SerializedName("IND_SPOUSE")
	private  String spouseInd;
	@Expose
	@SerializedName("IND_FATHER")
	private  String fatherInd;
	@Expose
	@SerializedName("IND_CNSMR_MBR_ID")
	private  String cnsmrMbrIdInd;
	@Expose
	@SerializedName("IND_DOB")
	private  Date dobInd;
	@Expose
	@SerializedName("IND_AGE")
	private  Integer ageInd;
	@Expose
	@SerializedName("IND_AGE_AS_ON")
	private  Date ageAsOnInd;
	@Expose
	@SerializedName("IND_PHONE")
	private  String phoneInd;
	@Expose
	@SerializedName("IND_ADDRESS_1")
	private  String address1Ind;
	@Expose
	@SerializedName("IND_ADDRESS_2")
	private  String address2Ind;
	@Expose
	@SerializedName("IND_REL_TYP_1")
	private  String relTyp1Ind;
	@Expose
	@SerializedName("IND_REL_NM_1")
	private  String relNm1Ind;
	@Expose
	@SerializedName("IND_REL_TYP_2")
	private  String relTyp2Ind;
	@Expose
	@SerializedName("IND_REL_NM_2")
	private  String relNm2Ind;
	@Expose
	@SerializedName("IND_ID_TYP_1")
	private  String idTyp1Ind;
	@Expose
	@SerializedName("IND_ID_VALUE_1")
	private  String idValue1Ind;
	@Expose
	@SerializedName("IND_ID_TYP_2")
	private  String idTyp2Ind;
	@Expose
	@SerializedName("IND_ID_VALUE_2")
	private  String idValue2Ind;
	@Expose
	@SerializedName("IND_GROUP_ID")
	private  String groupIdInd;
	@Expose
	@SerializedName("IND_GROUP_CREATION_DATE")
	private  Date groupCreationDateInd;
	@Expose
	@SerializedName("IND_INSERT_DATE")
	private  Date insertDateInd;
	@Expose
	@SerializedName("IND_ACCT_TYPE")
	private  String acctTypeInd;
	@Expose
	@SerializedName("IND_FREQ")
	private  String freqInd;
	@Expose
	@SerializedName("IND_STATUS")
	private  String statusAccountInd;
	@Expose
	@SerializedName("IND_ACCT_NUMBER")
	private  String acctNumberInd;
	@Expose
	@SerializedName("IND_DISBURSED_AMT")
	private  String disbursedAmtInd;
	@Expose
	@SerializedName("IND_CURRENT_BAL")
	private  String currentBalInd;
	@Expose
	@SerializedName("IND_INSTALLMENT_AMT")
	private  String installmentAmtInd;
	@Expose
	@SerializedName("IND_OVERDUE_AMT")
	private  String overdueAmtInd;
	@Expose
	@SerializedName("IND_WRITE_OFF_AMT")
	private  String writeOffAmtInd;
	@Expose
	@SerializedName("IND_DISBURSED_DT")
	private  Date disbursedDtInd;
	@Expose
	@SerializedName("IND_CLOSED_DT")
	private  Date closedDtInd;
	@Expose
	@SerializedName("IND_RECENT_DELINQ_DT")
	private  Date recentDelinqDtInd;
	@Expose
	@SerializedName("IND_DPD")
	private  BigInteger dpdInd;
	@Expose
	@SerializedName("IND_INQ_CNT")
	private  Integer inqCntInd;
	@Expose
	@SerializedName("IND_INFO_AS_ON")
	private  Date infoAsOnInd;
	@Expose
	@SerializedName("IND_WORST_DELEQUENCY_AMOUNT")
	private  BigInteger worstDelequencyAmountInd;
	@Expose
	@SerializedName("IND_PAYMENT_HISTORY")
	private  String paymentHistoryInd;
	@Expose
	@SerializedName("IND_IS_ACTIVE_BORROWER")
	private  String isActiveBorrowerInd;
	@Expose
	@SerializedName("IND_NO_OF_BORROWERS")
	private  Integer noOfBorrowersInd;
	@Expose
	@SerializedName("IND_COMMENT_RES")
	private  String commentResInd;
	@Expose
	@SerializedName("GRP_STATUS_PR")
	private  String statusGrpPr;
	@Expose
	@SerializedName("GRP_NO_OF_DEFAULT_ACCOUNTS_PR")
	private  BigInteger noOfDefaultAccountsGrpPr;
	@Expose
	@SerializedName("GRP_TOTAL_RESPONSES_PR")
	private  BigInteger totalResponsesIndGrpPr;
	@Expose
	@SerializedName("GRP_NO_OF_CLOSED_ACCOUNTS_PR")
	private  BigInteger noOfClosedAccountsGrpPr;
	@Expose
	@SerializedName("GRP_NO_OF_ACTIVE_ACCOUNTS_PR")
	private  BigInteger noOfActiveAccountsGrpPr;
	@Expose
	@SerializedName("GRP_NO_OF_OTHER_MFIS_PR")
	private  BigInteger noOfOtherMfisGrpPr;
	@Expose
	@SerializedName("GRP_OWN_MFI_INDECATOR_PR")
	private  String ownMfiIndecatorGrpPr;
	@Expose
	@SerializedName("GRP_TTL_OWN_DISBURSD_AMT_PR")
	private  BigInteger totalOwnDisbursedAmountGrpPr;
	@Expose
	@SerializedName("GRP_TTL_OTHER_DISBURSD_AMT_PR")
	private  BigInteger totalOtherDisbursedAmountGrpPr;
	@Expose
	@SerializedName("GRP_TTL_OWN_CURRENT_BAL_PR")
	private  BigInteger totalOwnCurrentBalanceGrpPr;
	@Expose
	@SerializedName("GRP_TTL_OTHER_CURRENT_BAL_PR")
	private  BigInteger totalOtherCurrentBalanceGrpPr;
	@Expose
	@SerializedName("GRP_TTL_OWN_INSTLMNT_AMT_PR")
	private  BigInteger totalOwnInstallmentAmountGrpPr;
	@Expose
	@SerializedName("GRP_TTL_OTHER_INSTLMNT_AMT_PR")
	private  BigInteger totalOtherInstallmentAmountGrpPr;
	@Expose
	@SerializedName("GRP_MAX_WORST_DELEQUENCY_PR")
	private  BigInteger maxWorstDelequencyGrpPr;
	@Expose
	@SerializedName("GRP_ERRORS_PR")
	private  String errorsGrpPr;
	@Expose
	@SerializedName("GRP_STATUS_SE")
	private  String statusGrpSe;
	@Expose
	@SerializedName("GRP_NO_OF_DEFAULT_ACCOUNTS_SE")
	private  BigInteger noOfDefaultAccountsGrpSe;
	@Expose
	@SerializedName("GRP_TOTAL_RESPONSES_SE")
	private  BigInteger totalResponsesIndGrpSe;
	@Expose
	@SerializedName("GRP_NO_OF_CLOSED_ACCOUNTS_SE")
	private  BigInteger noOfClosedAccountsGrpSe;
	@Expose
	@SerializedName("GRP_NO_OF_ACTIVE_ACCOUNTS_SE")
	private  BigInteger noOfActiveAccountsGrpSe;
	@Expose
	@SerializedName("GRP_NO_OF_OTHER_MFIS_SE")
	private  BigInteger noOfOtherMfisGrpSe;
	@Expose
	@SerializedName("GRP_OWN_MFI_INDECATOR_SE")
	private  String ownMfiIndecatorGrpSe;
	@Expose
	@SerializedName("GRP_TTL_OWN_DISBURSD_AMT_SE")
	private  BigInteger totalOwnDisbursedAmountGrpSe;
	@Expose
	@SerializedName("GRP_TTL_OTHER_DISBURSD_AMT_SE")
	private  BigInteger totalOtherDisbursedAmountGrpSe;
	@Expose
	@SerializedName("GRP_TTL_OWN_CURRENT_BAL_SE")
	private  BigInteger totalOwnCurrentBalanceGrpSe;
	@Expose
	@SerializedName("GRP_TTL_OTHER_CURRENT_BAL_SE")
	private  BigInteger totalOtherCurrentBalanceGrpSe;
	@Expose
	@SerializedName("GRP_TTL_OWN_INSTLMNT_AMT_SE")
	private  BigInteger totalOwnInstallmentAmountGrpSe;
	@Expose
	@SerializedName("GRP_TTL_OTHER_INSTLMNT_AMT_SE")
	private  BigInteger totalOtherInstallmentAmountGrpSe;
	@Expose
	@SerializedName("GRP_MAX_WORST_DELEQUENCY_SE")
	private  BigInteger maxWorstDelequencyGrpSe;
	@Expose
	@SerializedName("GRP_ERRORS_SE")
	private  String errorsGrpSe;
	@Expose
	@SerializedName("GRP_STATUS_SM")
	private  String statusGrp;
	@Expose
	@SerializedName("GRP_NO_OF_DEFAULT_ACCOUNTS_SM")
	private  BigInteger noOfDefaultAccountsGrp;
	@Expose
	@SerializedName("GRP_TOTAL_RESPONSES_SM")
	private  BigInteger totalResponsesGrp;
	@Expose
	@SerializedName("GRP_NO_OF_CLOSED_ACCOUNTS_SM")
	private  BigInteger noOfClosedAccountsGrp;
	@Expose
	@SerializedName("GRP_NO_OF_ACTIVE_ACCOUNTS_SM")
	private  BigInteger noOfActiveAccountsGrp;
	@Expose
	@SerializedName("GRP_NO_OF_OTHER_MFIS_SM")
	private  BigInteger noOfOtherMfisGrp;
	@Expose
	@SerializedName("GRP_OWN_MFI_INDECATOR_SM")
	private  String ownMfiIndecatorGrp;
	@Expose
	@SerializedName("GRP_TTL_OWN_DISBURSD_AMT_SM")
	private  BigInteger totalOwnDisbursedAmountGrp;
	@Expose
	@SerializedName("GRP_TTL_OTHER_DISBURSD_AMT_SM")
	private  BigInteger totalOtherDisbursedAmountGrp;
	@Expose
	@SerializedName("GRP_TTL_OWN_CURRENT_BAL_SM")
	private  BigInteger totalOwnCurrentBalanceGrp;
	@Expose
	@SerializedName("GRP_TTL_OTHER_CURRENT_BAL_SM")
	private  BigInteger totalOtherCurrentBalanceGrp;
	@Expose
	@SerializedName("GRP_TTL_OWN_INSTLMNT_AMT_SM")
	private  BigInteger totalOwnInstallmentAmountGrp;
	@Expose
	@SerializedName("GRP_TTL_OTHER_INSTLMNT_AMT_SM")
	private  BigInteger totalOtherInstallmentAmountGrp;
	@Expose
	@SerializedName("GRP_MAX_WORST_DELEQUENCY_SM")
	private  BigInteger maxWorstDelequencyGrp;
	@Expose
	@SerializedName("GRP_ERRORS_SM")
	private  String errorsGrp;
	@Expose
	@SerializedName("GRP_MATCHED_TYPE")
	private  String matchedTypeGrp;
	@Expose
	@SerializedName("GRP_MFI")
	private  String mfiGrp;
	@Expose
	@SerializedName("GRP_MFI_ID")
	private  String mfiIdGrp;
	@Expose
	@SerializedName("GRP_BRANCH")
	private  String branchGrp;
	@Expose
	@SerializedName("GRP_KENDRA")
	private  String kendraGrp;
	@Expose
	@SerializedName("GRP_NAME")
	private  String nameGrp;
	@Expose
	@SerializedName("GRP_SPOUSE")
	private  String spouseGrp;
	@Expose
	@SerializedName("GRP_FATHER")
	private  String fatherGrp;
	@Expose
	@SerializedName("GRP_CNSMR_MBR_ID")
	private  String cnsmrMbrIdGrp;
	@Expose
	@SerializedName("GRP_DOB")
	private  Date dobGrp;
	@Expose
	@SerializedName("GRP_AGE")
	private  Integer ageGrp;
	@Expose
	@SerializedName("GRP_AGE_AS_ON")
	private  Date ageAsOnGrp;
	@Expose
	@SerializedName("GRP_PHONE")
	private  String phoneGrp;
	@Expose
	@SerializedName("GRP_ADDRESS_1")
	private  String address1Grp;
	@Expose
	@SerializedName("GRP_ADDRESS_2")
	private  String address2Grp;
	@Expose
	@SerializedName("GRP_REL_TYP_1")
	private  String relTyp1Grp;
	@Expose
	@SerializedName("GRP_REL_NM_1")
	private  String relNm1Grp;
	@Expose
	@SerializedName("GRP_REL_TYP_2")
	private  String relTyp2Grp;
	@Expose
	@SerializedName("GRP_REL_NM_2")
	private  String relNm2Grp;
	@Expose
	@SerializedName("GRP_ID_TYP_1")
	private  String idTyp1Grp;
	@Expose
	@SerializedName("GRP_ID_VALUE_1")
	private  String idValue1Grp;
	@Expose
	@SerializedName("GRP_ID_TYP_2")
	private  String idTyp2Grp;
	@Expose
	@SerializedName("GRP_ID_VALUE_2")
	private  String idValue2Grp;
	@Expose
	@SerializedName("GRP_GROUP_ID")
	private  String groupIdGrp;
	@Expose
	@SerializedName("GRP_GROUP_CREATION_DATE")
	private  Date groupCreationDateGrp;
	@Expose
	@SerializedName("GRP_INSERT_DATE")
	private  Date insertDateGrp;
	@Expose
	@SerializedName("GRP_ACCT_TYPE")
	private  String acctTypeGrp;
	@Expose
	@SerializedName("GRP_FREQ")
	private  String freqGrp;
	@Expose
	@SerializedName("GRP_STATUS")
	private  String statusAccountGrp;
	@Expose
	@SerializedName("GRP_ACCT_NUMBER")
	private  String acctNumberGrp;
	@Expose
	@SerializedName("GRP_DISBURSED_AMT")
	private  String disbursedAmtGrp;
	@Expose
	@SerializedName("GRP_CURRENT_BAL")
	private  String currentBalGrp;
	@Expose
	@SerializedName("GRP_INSTALLMENT_AMT")
	private  String installmentAmtGrp;
	@Expose
	@SerializedName("GRP_OVERDUE_AMT")
	private  String overdueAmtGrp;
	@Expose
	@SerializedName("GRP_WRITE_OFF_AMT")
	private  String writeOffAmtGrp;
	@Expose
	@SerializedName("GRP_DISBURSED_DT")
	private  Date disbursedDtGrp;
	@Expose
	@SerializedName("GRP_CLOSED_DT")
	private  Date closedDtGrp;
	@Expose
	@SerializedName("GRP_RECENT_DELINQ_DT")
	private  Date recentDelinqDtGrp;
	@Expose
	@SerializedName("GRP_DPD")
	private  BigInteger dpdGrp;
	@Expose
	@SerializedName("GRP_INQ_CNT")
	private  Integer inqCntGrp;
	@Expose
	@SerializedName("GRP_INFO_AS_ON")
	private  Date infoAsOnGrp;
	@Expose
	@SerializedName("GRP_WORST_DELEQUENCY_AMOUNT")
	private  BigInteger worstDelequencyAmountGrp;
	@Expose
	@SerializedName("GRP_PAYMENT_HISTORY")
	private  String paymentHistoryGrp;
	@Expose
	@SerializedName("GRP_IS_ACTIVE_BORROWER")
	private  String isActiveBorrowerGrp;
	@Expose
	@SerializedName("GRP_NO_OF_BORROWERS")
	private  Integer noOfBorrowersGrp;
	@Expose
	@SerializedName("GRP_COMMENT_RES")
	private  String commentResGrp;
	@Expose
	@SerializedName("MEMBER_NAME")
	private  String memberName;
	@Expose
	@SerializedName("INQUIRY_DATE")
	private  Date inquiryDate;
	@Expose
	@SerializedName("PURPOSE")
	private  String purpose;
	@Expose
	@SerializedName("AMOUNT")
	private  BigInteger amount;
	@Expose
	@SerializedName("IND_REMARK")
	private  String remark;
	@Expose
	@SerializedName("OUTPUT_WRITE_FLAG")
	private  String outputWriteFlag;
	@Expose
	@SerializedName("OUTPUT_WRITE_TIME")
	private  String outputWriteTime;
	@Expose
	@SerializedName("OUTPUT_READ_TIME")
	private  String outputReadTime;

	public HibHighmarkAdOverlapSropDomain(Integer srNumber,
			String soaSourceName, String memberReferenceNumber,
			String soaFileName) {
		this.srno = srNumber;
		this.soaSourceName = soaSourceName;
		this.memberReferenceNumber = soaFileName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSrno() {
		return srno;
	}

	public void setSrno(Integer srno) {
		this.srno = srno;
	}

	public String getMemberReferenceNumber() {
		return memberReferenceNumber;
	}

	public void setMemberReferenceNumber(String memberReferenceNumber) {
		this.memberReferenceNumber = memberReferenceNumber;
	}

	public String getSoaSourceName() {
		return soaSourceName;
	}

	public void setSoaSourceName(String soaSourceName) {
		this.soaSourceName = soaSourceName;
	}

	public Date getDateOfRequest() {
		return dateOfRequest;
	}

	public void setDateOfRequest(Date dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}

	public String getPreparedFor() {
		return preparedFor;
	}

	public void setPreparedFor(String preparedFor) {
		this.preparedFor = preparedFor;
	}

	public String getPreparedForId() {
		return preparedForId;
	}

	public void setPreparedForId(String preparedForId) {
		this.preparedForId = preparedForId;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getNameIq() {
		return nameIq;
	}

	public void setNameIq(String nameIq) {
		this.nameIq = nameIq;
	}

	public String getSpouseIq() {
		return spouseIq;
	}

	public void setSpouseIq(String spouseIq) {
		this.spouseIq = spouseIq;
	}

	public String getFatherIq() {
		return fatherIq;
	}

	public void setFatherIq(String fatherIq) {
		this.fatherIq = fatherIq;
	}

	public String getMotherIq() {
		return motherIq;
	}

	public void setMotherIq(String motherIq) {
		this.motherIq = motherIq;
	}

	public Date getDobIq() {
		return dobIq;
	}

	public void setDobIq(Date dobIq) {
		this.dobIq = dobIq;
	}

	public Integer getAgeIq() {
		return ageIq;
	}

	public void setAgeIq(Integer ageIq) {
		this.ageIq = ageIq;
	}

	public Date getAgeAsOnIq() {
		return ageAsOnIq;
	}

	public void setAgeAsOnIq(Date ageAsOnIq) {
		this.ageAsOnIq = ageAsOnIq;
	}

	public String getGenderIq() {
		return genderIq;
	}

	public void setGenderIq(String genderIq) {
		this.genderIq = genderIq;
	}

	public BigInteger getPhone1Iq() {
		return phone1Iq;
	}

	public void setPhone1Iq(BigInteger phone1Iq) {
		this.phone1Iq = phone1Iq;
	}

	public BigInteger getPhone2Iq() {
		return phone2Iq;
	}

	public void setPhone2Iq(BigInteger phone2Iq) {
		this.phone2Iq = phone2Iq;
	}

	public BigInteger getPhone3Iq() {
		return phone3Iq;
	}

	public void setPhone3Iq(BigInteger phone3Iq) {
		this.phone3Iq = phone3Iq;
	}

	public String getAddress1Iq() {
		return address1Iq;
	}

	public void setAddress1Iq(String address1Iq) {
		this.address1Iq = address1Iq;
	}

	public String getAddress2Iq() {
		return address2Iq;
	}

	public void setAddress2Iq(String address2Iq) {
		this.address2Iq = address2Iq;
	}

	public String getRelTyp1Iq() {
		return relTyp1Iq;
	}

	public void setRelTyp1Iq(String relTyp1Iq) {
		this.relTyp1Iq = relTyp1Iq;
	}

	public String getRelNm1Iq() {
		return relNm1Iq;
	}

	public void setRelNm1Iq(String relNm1Iq) {
		this.relNm1Iq = relNm1Iq;
	}

	public String getRelTyp2Iq() {
		return relTyp2Iq;
	}

	public void setRelTyp2Iq(String relTyp2Iq) {
		this.relTyp2Iq = relTyp2Iq;
	}

	public String getRelNm2Iq() {
		return relNm2Iq;
	}

	public void setRelNm2Iq(String relNm2Iq) {
		this.relNm2Iq = relNm2Iq;
	}

	public String getRelTyp3Iq() {
		return relTyp3Iq;
	}

	public void setRelTyp3Iq(String relTyp3Iq) {
		this.relTyp3Iq = relTyp3Iq;
	}

	public String getRelNm3Iq() {
		return relNm3Iq;
	}

	public void setRelNm3Iq(String relNm3Iq) {
		this.relNm3Iq = relNm3Iq;
	}

	public String getRelTyp4Iq() {
		return relTyp4Iq;
	}

	public void setRelTyp4Iq(String relTyp4Iq) {
		this.relTyp4Iq = relTyp4Iq;
	}

	public String getRelNm4Iq() {
		return relNm4Iq;
	}

	public void setRelNm4Iq(String relNm4Iq) {
		this.relNm4Iq = relNm4Iq;
	}

	public String getIdType1Iq() {
		return idType1Iq;
	}

	public void setIdType1Iq(String idType1Iq) {
		this.idType1Iq = idType1Iq;
	}

	public String getIdValue1Iq() {
		return idValue1Iq;
	}

	public void setIdValue1Iq(String idValue1Iq) {
		this.idValue1Iq = idValue1Iq;
	}

	public String getIdType2Iq() {
		return idType2Iq;
	}

	public void setIdType2Iq(String idType2Iq) {
		this.idType2Iq = idType2Iq;
	}

	public String getIdValue2Iq() {
		return idValue2Iq;
	}

	public void setIdValue2Iq(String idValue2Iq) {
		this.idValue2Iq = idValue2Iq;
	}

	public String getRationCardIq() {
		return rationCardIq;
	}

	public void setRationCardIq(String rationCardIq) {
		this.rationCardIq = rationCardIq;
	}

	public String getVotersIdIq() {
		return votersIdIq;
	}

	public void setVotersIdIq(String votersIdIq) {
		this.votersIdIq = votersIdIq;
	}

	public String getDrivingLicenceNoIq() {
		return drivingLicenceNoIq;
	}

	public void setDrivingLicenceNoIq(String drivingLicenceNoIq) {
		this.drivingLicenceNoIq = drivingLicenceNoIq;
	}

	public String getPanIq() {
		return panIq;
	}

	public void setPanIq(String panIq) {
		this.panIq = panIq;
	}

	public String getPassportIq() {
		return passportIq;
	}

	public void setPassportIq(String passportIq) {
		this.passportIq = passportIq;
	}

	public String getOtherIdIq() {
		return otherIdIq;
	}

	public void setOtherIdIq(String otherIdIq) {
		this.otherIdIq = otherIdIq;
	}

	public String getBranchIq() {
		return branchIq;
	}

	public void setBranchIq(String branchIq) {
		this.branchIq = branchIq;
	}

	public String getKendraIq() {
		return kendraIq;
	}

	public void setKendraIq(String kendraIq) {
		this.kendraIq = kendraIq;
	}

	public String getMbrIdIq() {
		return mbrIdIq;
	}

	public void setMbrIdIq(String mbrIdIq) {
		this.mbrIdIq = mbrIdIq;
	}

	public String getCredtIqPurpsTyp() {
		return credtIqPurpsTyp;
	}

	public void setCredtIqPurpsTyp(String credtIqPurpsTyp) {
		this.credtIqPurpsTyp = credtIqPurpsTyp;
	}

	public String getCredtInqPurpsTypDesc() {
		return credtInqPurpsTypDesc;
	}

	public void setCredtInqPurpsTypDesc(String credtInqPurpsTypDesc) {
		this.credtInqPurpsTypDesc = credtInqPurpsTypDesc;
	}

	public String getCreditInquiryStage() {
		return creditInquiryStage;
	}

	public void setCreditInquiryStage(String creditInquiryStage) {
		this.creditInquiryStage = creditInquiryStage;
	}

	public String getCredtRptId() {
		return credtRptId;
	}

	public void setCredtRptId(String credtRptId) {
		this.credtRptId = credtRptId;
	}

	public String getCredtReqTyp() {
		return credtReqTyp;
	}

	public void setCredtReqTyp(String credtReqTyp) {
		this.credtReqTyp = credtReqTyp;
	}

	public String getCredtRptTrnDtTm() {
		return credtRptTrnDtTm;
	}

	public void setCredtRptTrnDtTm(String credtRptTrnDtTm) {
		this.credtRptTrnDtTm = credtRptTrnDtTm;
	}

	public Date getAcOpenDt() {
		return acOpenDt;
	}

	public void setAcOpenDt(Date acOpenDt) {
		this.acOpenDt = acOpenDt;
	}

	public BigInteger getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(BigInteger loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getStatusIndPr() {
		return statusIndPr;
	}

	public void setStatusIndPr(String statusIndPr) {
		this.statusIndPr = statusIndPr;
	}

	public BigInteger getNoOfDefaultAccountsIndPr() {
		return noOfDefaultAccountsIndPr;
	}

	public void setNoOfDefaultAccountsIndPr(BigInteger noOfDefaultAccountsIndPr) {
		this.noOfDefaultAccountsIndPr = noOfDefaultAccountsIndPr;
	}

	public BigInteger getTotalResponsesIndIndPr() {
		return totalResponsesIndIndPr;
	}

	public void setTotalResponsesIndIndPr(BigInteger totalResponsesIndIndPr) {
		this.totalResponsesIndIndPr = totalResponsesIndIndPr;
	}

	public BigInteger getNoOfClosedAccountsIndPr() {
		return noOfClosedAccountsIndPr;
	}

	public void setNoOfClosedAccountsIndPr(BigInteger noOfClosedAccountsIndPr) {
		this.noOfClosedAccountsIndPr = noOfClosedAccountsIndPr;
	}

	public BigInteger getNoOfActiveAccountsIndPr() {
		return noOfActiveAccountsIndPr;
	}

	public void setNoOfActiveAccountsIndPr(BigInteger noOfActiveAccountsIndPr) {
		this.noOfActiveAccountsIndPr = noOfActiveAccountsIndPr;
	}

	public BigInteger getNoOfOtherMfisIndPr() {
		return noOfOtherMfisIndPr;
	}

	public void setNoOfOtherMfisIndPr(BigInteger noOfOtherMfisIndPr) {
		this.noOfOtherMfisIndPr = noOfOtherMfisIndPr;
	}

	public String getOwnMfiIndecatorIndPr() {
		return ownMfiIndecatorIndPr;
	}

	public void setOwnMfiIndecatorIndPr(String ownMfiIndecatorIndPr) {
		this.ownMfiIndecatorIndPr = ownMfiIndecatorIndPr;
	}

	public BigInteger getTotalOwnDisbursedAmountIndPr() {
		return totalOwnDisbursedAmountIndPr;
	}

	public void setTotalOwnDisbursedAmountIndPr(
			BigInteger totalOwnDisbursedAmountIndPr) {
		this.totalOwnDisbursedAmountIndPr = totalOwnDisbursedAmountIndPr;
	}

	public BigInteger getTotalOtherDisbursedAmountIndPr() {
		return totalOtherDisbursedAmountIndPr;
	}

	public void setTotalOtherDisbursedAmountIndPr(
			BigInteger totalOtherDisbursedAmountIndPr) {
		this.totalOtherDisbursedAmountIndPr = totalOtherDisbursedAmountIndPr;
	}

	public BigInteger getTotalOwnCurrentBalanceIndPr() {
		return totalOwnCurrentBalanceIndPr;
	}

	public void setTotalOwnCurrentBalanceIndPr(
			BigInteger totalOwnCurrentBalanceIndPr) {
		this.totalOwnCurrentBalanceIndPr = totalOwnCurrentBalanceIndPr;
	}

	public BigInteger getTotalOtherCurrentBalanceIndPr() {
		return totalOtherCurrentBalanceIndPr;
	}

	public void setTotalOtherCurrentBalanceIndPr(
			BigInteger totalOtherCurrentBalanceIndPr) {
		this.totalOtherCurrentBalanceIndPr = totalOtherCurrentBalanceIndPr;
	}

	public BigInteger getTotalOwnInstallmentAmountIndPr() {
		return totalOwnInstallmentAmountIndPr;
	}

	public void setTotalOwnInstallmentAmountIndPr(
			BigInteger totalOwnInstallmentAmountIndPr) {
		this.totalOwnInstallmentAmountIndPr = totalOwnInstallmentAmountIndPr;
	}

	public BigInteger getTotalOtherInstallmentAmountIndPr() {
		return totalOtherInstallmentAmountIndPr;
	}

	public void setTotalOtherInstallmentAmountIndPr(
			BigInteger totalOtherInstallmentAmountIndPr) {
		this.totalOtherInstallmentAmountIndPr = totalOtherInstallmentAmountIndPr;
	}

	public BigInteger getMaxWorstDelequencyIndPr() {
		return maxWorstDelequencyIndPr;
	}

	public void setMaxWorstDelequencyIndPr(BigInteger maxWorstDelequencyIndPr) {
		this.maxWorstDelequencyIndPr = maxWorstDelequencyIndPr;
	}

	public String getErrorsIndPr() {
		return errorsIndPr;
	}

	public void setErrorsIndPr(String errorsIndPr) {
		this.errorsIndPr = errorsIndPr;
	}

	public String getStatusIndSe() {
		return statusIndSe;
	}

	public void setStatusIndSe(String statusIndSe) {
		this.statusIndSe = statusIndSe;
	}

	public BigInteger getNoOfDefaultAccountsIndSe() {
		return noOfDefaultAccountsIndSe;
	}

	public void setNoOfDefaultAccountsIndSe(BigInteger noOfDefaultAccountsIndSe) {
		this.noOfDefaultAccountsIndSe = noOfDefaultAccountsIndSe;
	}

	public BigInteger getTotalResponsesIndIndSe() {
		return totalResponsesIndIndSe;
	}

	public void setTotalResponsesIndIndSe(BigInteger totalResponsesIndIndSe) {
		this.totalResponsesIndIndSe = totalResponsesIndIndSe;
	}

	public BigInteger getNoOfClosedAccountsIndSe() {
		return noOfClosedAccountsIndSe;
	}

	public void setNoOfClosedAccountsIndSe(BigInteger noOfClosedAccountsIndSe) {
		this.noOfClosedAccountsIndSe = noOfClosedAccountsIndSe;
	}

	public BigInteger getNoOfActiveAccountsIndSe() {
		return noOfActiveAccountsIndSe;
	}

	public void setNoOfActiveAccountsIndSe(BigInteger noOfActiveAccountsIndSe) {
		this.noOfActiveAccountsIndSe = noOfActiveAccountsIndSe;
	}

	public BigInteger getNoOfOtherMfisIndSe() {
		return noOfOtherMfisIndSe;
	}

	public void setNoOfOtherMfisIndSe(BigInteger noOfOtherMfisIndSe) {
		this.noOfOtherMfisIndSe = noOfOtherMfisIndSe;
	}

	public String getOwnMfiIndecatorIndSe() {
		return ownMfiIndecatorIndSe;
	}

	public void setOwnMfiIndecatorIndSe(String ownMfiIndecatorIndSe) {
		this.ownMfiIndecatorIndSe = ownMfiIndecatorIndSe;
	}

	public BigInteger getTotalOwnDisbursedAmountIndSe() {
		return totalOwnDisbursedAmountIndSe;
	}

	public void setTotalOwnDisbursedAmountIndSe(
			BigInteger totalOwnDisbursedAmountIndSe) {
		this.totalOwnDisbursedAmountIndSe = totalOwnDisbursedAmountIndSe;
	}

	public BigInteger getTotalOtherDisbursedAmountIndSe() {
		return totalOtherDisbursedAmountIndSe;
	}

	public void setTotalOtherDisbursedAmountIndSe(
			BigInteger totalOtherDisbursedAmountIndSe) {
		this.totalOtherDisbursedAmountIndSe = totalOtherDisbursedAmountIndSe;
	}

	public BigInteger getTotalOwnCurrentBalanceIndSe() {
		return totalOwnCurrentBalanceIndSe;
	}

	public void setTotalOwnCurrentBalanceIndSe(
			BigInteger totalOwnCurrentBalanceIndSe) {
		this.totalOwnCurrentBalanceIndSe = totalOwnCurrentBalanceIndSe;
	}

	public BigInteger getTotalOtherCurrentBalanceIndSe() {
		return totalOtherCurrentBalanceIndSe;
	}

	public void setTotalOtherCurrentBalanceIndSe(
			BigInteger totalOtherCurrentBalanceIndSe) {
		this.totalOtherCurrentBalanceIndSe = totalOtherCurrentBalanceIndSe;
	}

	public BigInteger getTotalOwnInstallmentAmountIndSe() {
		return totalOwnInstallmentAmountIndSe;
	}

	public void setTotalOwnInstallmentAmountIndSe(
			BigInteger totalOwnInstallmentAmountIndSe) {
		this.totalOwnInstallmentAmountIndSe = totalOwnInstallmentAmountIndSe;
	}

	public BigInteger getTotalOtherInstallmentAmountIndSe() {
		return totalOtherInstallmentAmountIndSe;
	}

	public void setTotalOtherInstallmentAmountIndSe(
			BigInteger totalOtherInstallmentAmountIndSe) {
		this.totalOtherInstallmentAmountIndSe = totalOtherInstallmentAmountIndSe;
	}

	public BigInteger getMaxWorstDelequencyIndSe() {
		return maxWorstDelequencyIndSe;
	}

	public void setMaxWorstDelequencyIndSe(BigInteger maxWorstDelequencyIndSe) {
		this.maxWorstDelequencyIndSe = maxWorstDelequencyIndSe;
	}

	public String getErrorsIndSe() {
		return errorsIndSe;
	}

	public void setErrorsIndSe(String errorsIndSe) {
		this.errorsIndSe = errorsIndSe;
	}

	public String getStatusInd() {
		return statusInd;
	}

	public void setStatusInd(String statusInd) {
		this.statusInd = statusInd;
	}

	public BigInteger getNoOfDefaultAccountsInd() {
		return noOfDefaultAccountsInd;
	}

	public void setNoOfDefaultAccountsInd(BigInteger noOfDefaultAccountsInd) {
		this.noOfDefaultAccountsInd = noOfDefaultAccountsInd;
	}

	public BigInteger getTotalResponsesInd() {
		return totalResponsesInd;
	}

	public void setTotalResponsesInd(BigInteger totalResponsesInd) {
		this.totalResponsesInd = totalResponsesInd;
	}

	public BigInteger getNoOfClosedAccountsInd() {
		return noOfClosedAccountsInd;
	}

	public void setNoOfClosedAccountsInd(BigInteger noOfClosedAccountsInd) {
		this.noOfClosedAccountsInd = noOfClosedAccountsInd;
	}

	public BigInteger getNoOfActiveAccountsInd() {
		return noOfActiveAccountsInd;
	}

	public void setNoOfActiveAccountsInd(BigInteger noOfActiveAccountsInd) {
		this.noOfActiveAccountsInd = noOfActiveAccountsInd;
	}

	public BigInteger getNoOfOtherMfisInd() {
		return noOfOtherMfisInd;
	}

	public void setNoOfOtherMfisInd(BigInteger noOfOtherMfisInd) {
		this.noOfOtherMfisInd = noOfOtherMfisInd;
	}

	public String getOwnMfiIndecatorInd() {
		return ownMfiIndecatorInd;
	}

	public void setOwnMfiIndecatorInd(String ownMfiIndecatorInd) {
		this.ownMfiIndecatorInd = ownMfiIndecatorInd;
	}

	public BigInteger getTotalOwnDisbursedAmountInd() {
		return totalOwnDisbursedAmountInd;
	}

	public void setTotalOwnDisbursedAmountInd(
			BigInteger totalOwnDisbursedAmountInd) {
		this.totalOwnDisbursedAmountInd = totalOwnDisbursedAmountInd;
	}

	public BigInteger getTotalOtherDisbursedAmountInd() {
		return totalOtherDisbursedAmountInd;
	}

	public void setTotalOtherDisbursedAmountInd(
			BigInteger totalOtherDisbursedAmountInd) {
		this.totalOtherDisbursedAmountInd = totalOtherDisbursedAmountInd;
	}

	public BigInteger getTotalOwnCurrentBalanceInd() {
		return totalOwnCurrentBalanceInd;
	}

	public void setTotalOwnCurrentBalanceInd(
			BigInteger totalOwnCurrentBalanceInd) {
		this.totalOwnCurrentBalanceInd = totalOwnCurrentBalanceInd;
	}

	public BigInteger getTotalOtherCurrentBalanceInd() {
		return totalOtherCurrentBalanceInd;
	}

	public void setTotalOtherCurrentBalanceInd(
			BigInteger totalOtherCurrentBalanceInd) {
		this.totalOtherCurrentBalanceInd = totalOtherCurrentBalanceInd;
	}

	public BigInteger getTotalOwnInstallmentAmountInd() {
		return totalOwnInstallmentAmountInd;
	}

	public void setTotalOwnInstallmentAmountInd(
			BigInteger totalOwnInstallmentAmountInd) {
		this.totalOwnInstallmentAmountInd = totalOwnInstallmentAmountInd;
	}

	public BigInteger getTotalOtherInstallmentAmountInd() {
		return totalOtherInstallmentAmountInd;
	}

	public void setTotalOtherInstallmentAmountInd(
			BigInteger totalOtherInstallmentAmountInd) {
		this.totalOtherInstallmentAmountInd = totalOtherInstallmentAmountInd;
	}

	public BigInteger getMaxWorstDelequencyInd() {
		return maxWorstDelequencyInd;
	}

	public void setMaxWorstDelequencyInd(BigInteger maxWorstDelequencyInd) {
		this.maxWorstDelequencyInd = maxWorstDelequencyInd;
	}

	public String getErrorsInd() {
		return errorsInd;
	}

	public void setErrorsInd(String errorsInd) {
		this.errorsInd = errorsInd;
	}

	public String getMatchedTypeInd() {
		return matchedTypeInd;
	}

	public void setMatchedTypeInd(String matchedTypeInd) {
		this.matchedTypeInd = matchedTypeInd;
	}

	public String getMfiInd() {
		return mfiInd;
	}

	public void setMfiInd(String mfiInd) {
		this.mfiInd = mfiInd;
	}

	public String getMfiIdInd() {
		return mfiIdInd;
	}

	public void setMfiIdInd(String mfiIdInd) {
		this.mfiIdInd = mfiIdInd;
	}

	public String getBranchInd() {
		return branchInd;
	}

	public void setBranchInd(String branchInd) {
		this.branchInd = branchInd;
	}

	public String getKendraInd() {
		return kendraInd;
	}

	public void setKendraInd(String kendraInd) {
		this.kendraInd = kendraInd;
	}

	public String getNameInd() {
		return nameInd;
	}

	public void setNameInd(String nameInd) {
		this.nameInd = nameInd;
	}

	public String getSpouseInd() {
		return spouseInd;
	}

	public void setSpouseInd(String spouseInd) {
		this.spouseInd = spouseInd;
	}

	public String getFatherInd() {
		return fatherInd;
	}

	public void setFatherInd(String fatherInd) {
		this.fatherInd = fatherInd;
	}

	public String getCnsmrMbrIdInd() {
		return cnsmrMbrIdInd;
	}

	public void setCnsmrMbrIdInd(String cnsmrMbrIdInd) {
		this.cnsmrMbrIdInd = cnsmrMbrIdInd;
	}

	public Date getDobInd() {
		return dobInd;
	}

	public void setDobInd(Date dobInd) {
		this.dobInd = dobInd;
	}

	public Integer getAgeInd() {
		return ageInd;
	}

	public void setAgeInd(Integer ageInd) {
		this.ageInd = ageInd;
	}

	public Date getAgeAsOnInd() {
		return ageAsOnInd;
	}

	public void setAgeAsOnInd(Date ageAsOnInd) {
		this.ageAsOnInd = ageAsOnInd;
	}

	public String getPhoneInd() {
		return phoneInd;
	}

	public void setPhoneInd(String phoneInd) {
		this.phoneInd = phoneInd;
	}

	public String getAddress1Ind() {
		return address1Ind;
	}

	public void setAddress1Ind(String address1Ind) {
		this.address1Ind = address1Ind;
	}

	public String getAddress2Ind() {
		return address2Ind;
	}

	public void setAddress2Ind(String address2Ind) {
		this.address2Ind = address2Ind;
	}

	public String getRelTyp1Ind() {
		return relTyp1Ind;
	}

	public void setRelTyp1Ind(String relTyp1Ind) {
		this.relTyp1Ind = relTyp1Ind;
	}

	public String getRelNm1Ind() {
		return relNm1Ind;
	}

	public void setRelNm1Ind(String relNm1Ind) {
		this.relNm1Ind = relNm1Ind;
	}

	public String getRelTyp2Ind() {
		return relTyp2Ind;
	}

	public void setRelTyp2Ind(String relTyp2Ind) {
		this.relTyp2Ind = relTyp2Ind;
	}

	public String getRelNm2Ind() {
		return relNm2Ind;
	}

	public void setRelNm2Ind(String relNm2Ind) {
		this.relNm2Ind = relNm2Ind;
	}

	public String getIdTyp1Ind() {
		return idTyp1Ind;
	}

	public void setIdTyp1Ind(String idTyp1Ind) {
		this.idTyp1Ind = idTyp1Ind;
	}

	public String getIdValue1Ind() {
		return idValue1Ind;
	}

	public void setIdValue1Ind(String idValue1Ind) {
		this.idValue1Ind = idValue1Ind;
	}

	public String getIdTyp2Ind() {
		return idTyp2Ind;
	}

	public void setIdTyp2Ind(String idTyp2Ind) {
		this.idTyp2Ind = idTyp2Ind;
	}

	public String getIdValue2Ind() {
		return idValue2Ind;
	}

	public void setIdValue2Ind(String idValue2Ind) {
		this.idValue2Ind = idValue2Ind;
	}

	public String getGroupIdInd() {
		return groupIdInd;
	}

	public void setGroupIdInd(String groupIdInd) {
		this.groupIdInd = groupIdInd;
	}

	public Date getGroupCreationDateInd() {
		return groupCreationDateInd;
	}

	public void setGroupCreationDateInd(Date groupCreationDateInd) {
		this.groupCreationDateInd = groupCreationDateInd;
	}

	public Date getInsertDateInd() {
		return insertDateInd;
	}

	public void setInsertDateInd(Date insertDateInd) {
		this.insertDateInd = insertDateInd;
	}

	public String getAcctTypeInd() {
		return acctTypeInd;
	}

	public void setAcctTypeInd(String acctTypeInd) {
		this.acctTypeInd = acctTypeInd;
	}

	public String getFreqInd() {
		return freqInd;
	}

	public void setFreqInd(String freqInd) {
		this.freqInd = freqInd;
	}

	public String getStatusAccountInd() {
		return statusAccountInd;
	}

	public void setStatusAccountInd(String statusAccountInd) {
		this.statusAccountInd = statusAccountInd;
	}

	public String getAcctNumberInd() {
		return acctNumberInd;
	}

	public void setAcctNumberInd(String acctNumberInd) {
		this.acctNumberInd = acctNumberInd;
	}

	public String getDisbursedAmtInd() {
		return disbursedAmtInd;
	}

	public void setDisbursedAmtInd(String disbursedAmtInd) {
		this.disbursedAmtInd = disbursedAmtInd;
	}

	public String getCurrentBalInd() {
		return currentBalInd;
	}

	public void setCurrentBalInd(String currentBalInd) {
		this.currentBalInd = currentBalInd;
	}

	public String getInstallmentAmtInd() {
		return installmentAmtInd;
	}

	public void setInstallmentAmtInd(String installmentAmtInd) {
		this.installmentAmtInd = installmentAmtInd;
	}

	public String getOverdueAmtInd() {
		return overdueAmtInd;
	}

	public void setOverdueAmtInd(String overdueAmtInd) {
		this.overdueAmtInd = overdueAmtInd;
	}

	public String getWriteOffAmtInd() {
		return writeOffAmtInd;
	}

	public void setWriteOffAmtInd(String writeOffAmtInd) {
		this.writeOffAmtInd = writeOffAmtInd;
	}

	public Date getDisbursedDtInd() {
		return disbursedDtInd;
	}

	public void setDisbursedDtInd(Date disbursedDtInd) {
		this.disbursedDtInd = disbursedDtInd;
	}

	public Date getClosedDtInd() {
		return closedDtInd;
	}

	public void setClosedDtInd(Date closedDtInd) {
		this.closedDtInd = closedDtInd;
	}

	public Date getRecentDelinqDtInd() {
		return recentDelinqDtInd;
	}

	public void setRecentDelinqDtInd(Date recentDelinqDtInd) {
		this.recentDelinqDtInd = recentDelinqDtInd;
	}

	public BigInteger getDpdInd() {
		return dpdInd;
	}

	public void setDpdInd(BigInteger dpdInd) {
		this.dpdInd = dpdInd;
	}

	public Integer getInqCntInd() {
		return inqCntInd;
	}

	public void setInqCntInd(Integer inqCntInd) {
		this.inqCntInd = inqCntInd;
	}

	public Date getInfoAsOnInd() {
		return infoAsOnInd;
	}

	public void setInfoAsOnInd(Date infoAsOnInd) {
		this.infoAsOnInd = infoAsOnInd;
	}

	public BigInteger getWorstDelequencyAmountInd() {
		return worstDelequencyAmountInd;
	}

	public void setWorstDelequencyAmountInd(BigInteger worstDelequencyAmountInd) {
		this.worstDelequencyAmountInd = worstDelequencyAmountInd;
	}

	public String getPaymentHistoryInd() {
		return paymentHistoryInd;
	}

	public void setPaymentHistoryInd(String paymentHistoryInd) {
		this.paymentHistoryInd = paymentHistoryInd;
	}

	public String getIsActiveBorrowerInd() {
		return isActiveBorrowerInd;
	}

	public void setIsActiveBorrowerInd(String isActiveBorrowerInd) {
		this.isActiveBorrowerInd = isActiveBorrowerInd;
	}

	public Integer getNoOfBorrowersInd() {
		return noOfBorrowersInd;
	}

	public void setNoOfBorrowersInd(Integer noOfBorrowersInd) {
		this.noOfBorrowersInd = noOfBorrowersInd;
	}

	public String getCommentResInd() {
		return commentResInd;
	}

	public void setCommentResInd(String commentResInd) {
		this.commentResInd = commentResInd;
	}

	public String getStatusGrpPr() {
		return statusGrpPr;
	}

	public void setStatusGrpPr(String statusGrpPr) {
		this.statusGrpPr = statusGrpPr;
	}

	public BigInteger getNoOfDefaultAccountsGrpPr() {
		return noOfDefaultAccountsGrpPr;
	}

	public void setNoOfDefaultAccountsGrpPr(BigInteger noOfDefaultAccountsGrpPr) {
		this.noOfDefaultAccountsGrpPr = noOfDefaultAccountsGrpPr;
	}

	public BigInteger getTotalResponsesIndGrpPr() {
		return totalResponsesIndGrpPr;
	}

	public void setTotalResponsesIndGrpPr(BigInteger totalResponsesIndGrpPr) {
		this.totalResponsesIndGrpPr = totalResponsesIndGrpPr;
	}

	public BigInteger getNoOfClosedAccountsGrpPr() {
		return noOfClosedAccountsGrpPr;
	}

	public void setNoOfClosedAccountsGrpPr(BigInteger noOfClosedAccountsGrpPr) {
		this.noOfClosedAccountsGrpPr = noOfClosedAccountsGrpPr;
	}

	public BigInteger getNoOfActiveAccountsGrpPr() {
		return noOfActiveAccountsGrpPr;
	}

	public void setNoOfActiveAccountsGrpPr(BigInteger noOfActiveAccountsGrpPr) {
		this.noOfActiveAccountsGrpPr = noOfActiveAccountsGrpPr;
	}

	public BigInteger getNoOfOtherMfisGrpPr() {
		return noOfOtherMfisGrpPr;
	}

	public void setNoOfOtherMfisGrpPr(BigInteger noOfOtherMfisGrpPr) {
		this.noOfOtherMfisGrpPr = noOfOtherMfisGrpPr;
	}

	public String getOwnMfiIndecatorGrpPr() {
		return ownMfiIndecatorGrpPr;
	}

	public void setOwnMfiIndecatorGrpPr(String ownMfiIndecatorGrpPr) {
		this.ownMfiIndecatorGrpPr = ownMfiIndecatorGrpPr;
	}

	public BigInteger getTotalOwnDisbursedAmountGrpPr() {
		return totalOwnDisbursedAmountGrpPr;
	}

	public void setTotalOwnDisbursedAmountGrpPr(
			BigInteger totalOwnDisbursedAmountGrpPr) {
		this.totalOwnDisbursedAmountGrpPr = totalOwnDisbursedAmountGrpPr;
	}

	public BigInteger getTotalOtherDisbursedAmountGrpPr() {
		return totalOtherDisbursedAmountGrpPr;
	}

	public void setTotalOtherDisbursedAmountGrpPr(
			BigInteger totalOtherDisbursedAmountGrpPr) {
		this.totalOtherDisbursedAmountGrpPr = totalOtherDisbursedAmountGrpPr;
	}

	public BigInteger getTotalOwnCurrentBalanceGrpPr() {
		return totalOwnCurrentBalanceGrpPr;
	}

	public void setTotalOwnCurrentBalanceGrpPr(
			BigInteger totalOwnCurrentBalanceGrpPr) {
		this.totalOwnCurrentBalanceGrpPr = totalOwnCurrentBalanceGrpPr;
	}

	public BigInteger getTotalOtherCurrentBalanceGrpPr() {
		return totalOtherCurrentBalanceGrpPr;
	}

	public void setTotalOtherCurrentBalanceGrpPr(
			BigInteger totalOtherCurrentBalanceGrpPr) {
		this.totalOtherCurrentBalanceGrpPr = totalOtherCurrentBalanceGrpPr;
	}

	public BigInteger getTotalOwnInstallmentAmountGrpPr() {
		return totalOwnInstallmentAmountGrpPr;
	}

	public void setTotalOwnInstallmentAmountGrpPr(
			BigInteger totalOwnInstallmentAmountGrpPr) {
		this.totalOwnInstallmentAmountGrpPr = totalOwnInstallmentAmountGrpPr;
	}

	public BigInteger getTotalOtherInstallmentAmountGrpPr() {
		return totalOtherInstallmentAmountGrpPr;
	}

	public void setTotalOtherInstallmentAmountGrpPr(
			BigInteger totalOtherInstallmentAmountGrpPr) {
		this.totalOtherInstallmentAmountGrpPr = totalOtherInstallmentAmountGrpPr;
	}

	public BigInteger getMaxWorstDelequencyGrpPr() {
		return maxWorstDelequencyGrpPr;
	}

	public void setMaxWorstDelequencyGrpPr(BigInteger maxWorstDelequencyGrpPr) {
		this.maxWorstDelequencyGrpPr = maxWorstDelequencyGrpPr;
	}

	public String getErrorsGrpPr() {
		return errorsGrpPr;
	}

	public void setErrorsGrpPr(String errorsGrpPr) {
		this.errorsGrpPr = errorsGrpPr;
	}

	public String getStatusGrpSe() {
		return statusGrpSe;
	}

	public void setStatusGrpSe(String statusGrpSe) {
		this.statusGrpSe = statusGrpSe;
	}

	public BigInteger getNoOfDefaultAccountsGrpSe() {
		return noOfDefaultAccountsGrpSe;
	}

	public void setNoOfDefaultAccountsGrpSe(BigInteger noOfDefaultAccountsGrpSe) {
		this.noOfDefaultAccountsGrpSe = noOfDefaultAccountsGrpSe;
	}

	public BigInteger getTotalResponsesIndGrpSe() {
		return totalResponsesIndGrpSe;
	}

	public void setTotalResponsesIndGrpSe(BigInteger totalResponsesIndGrpSe) {
		this.totalResponsesIndGrpSe = totalResponsesIndGrpSe;
	}

	public BigInteger getNoOfClosedAccountsGrpSe() {
		return noOfClosedAccountsGrpSe;
	}

	public void setNoOfClosedAccountsGrpSe(BigInteger noOfClosedAccountsGrpSe) {
		this.noOfClosedAccountsGrpSe = noOfClosedAccountsGrpSe;
	}

	public BigInteger getNoOfActiveAccountsGrpSe() {
		return noOfActiveAccountsGrpSe;
	}

	public void setNoOfActiveAccountsGrpSe(BigInteger noOfActiveAccountsGrpSe) {
		this.noOfActiveAccountsGrpSe = noOfActiveAccountsGrpSe;
	}

	public BigInteger getNoOfOtherMfisGrpSe() {
		return noOfOtherMfisGrpSe;
	}

	public void setNoOfOtherMfisGrpSe(BigInteger noOfOtherMfisGrpSe) {
		this.noOfOtherMfisGrpSe = noOfOtherMfisGrpSe;
	}

	public String getOwnMfiIndecatorGrpSe() {
		return ownMfiIndecatorGrpSe;
	}

	public void setOwnMfiIndecatorGrpSe(String ownMfiIndecatorGrpSe) {
		this.ownMfiIndecatorGrpSe = ownMfiIndecatorGrpSe;
	}

	public BigInteger getTotalOwnDisbursedAmountGrpSe() {
		return totalOwnDisbursedAmountGrpSe;
	}

	public void setTotalOwnDisbursedAmountGrpSe(
			BigInteger totalOwnDisbursedAmountGrpSe) {
		this.totalOwnDisbursedAmountGrpSe = totalOwnDisbursedAmountGrpSe;
	}

	public BigInteger getTotalOtherDisbursedAmountGrpSe() {
		return totalOtherDisbursedAmountGrpSe;
	}

	public void setTotalOtherDisbursedAmountGrpSe(
			BigInteger totalOtherDisbursedAmountGrpSe) {
		this.totalOtherDisbursedAmountGrpSe = totalOtherDisbursedAmountGrpSe;
	}

	public BigInteger getTotalOwnCurrentBalanceGrpSe() {
		return totalOwnCurrentBalanceGrpSe;
	}

	public void setTotalOwnCurrentBalanceGrpSe(
			BigInteger totalOwnCurrentBalanceGrpSe) {
		this.totalOwnCurrentBalanceGrpSe = totalOwnCurrentBalanceGrpSe;
	}

	public BigInteger getTotalOtherCurrentBalanceGrpSe() {
		return totalOtherCurrentBalanceGrpSe;
	}

	public void setTotalOtherCurrentBalanceGrpSe(
			BigInteger totalOtherCurrentBalanceGrpSe) {
		this.totalOtherCurrentBalanceGrpSe = totalOtherCurrentBalanceGrpSe;
	}

	public BigInteger getTotalOwnInstallmentAmountGrpSe() {
		return totalOwnInstallmentAmountGrpSe;
	}

	public void setTotalOwnInstallmentAmountGrpSe(
			BigInteger totalOwnInstallmentAmountGrpSe) {
		this.totalOwnInstallmentAmountGrpSe = totalOwnInstallmentAmountGrpSe;
	}

	public BigInteger getTotalOtherInstallmentAmountGrpSe() {
		return totalOtherInstallmentAmountGrpSe;
	}

	public void setTotalOtherInstallmentAmountGrpSe(
			BigInteger totalOtherInstallmentAmountGrpSe) {
		this.totalOtherInstallmentAmountGrpSe = totalOtherInstallmentAmountGrpSe;
	}

	public BigInteger getMaxWorstDelequencyGrpSe() {
		return maxWorstDelequencyGrpSe;
	}

	public void setMaxWorstDelequencyGrpSe(BigInteger maxWorstDelequencyGrpSe) {
		this.maxWorstDelequencyGrpSe = maxWorstDelequencyGrpSe;
	}

	public String getErrorsGrpSe() {
		return errorsGrpSe;
	}

	public void setErrorsGrpSe(String errorsGrpSe) {
		this.errorsGrpSe = errorsGrpSe;
	}

	public String getStatusGrp() {
		return statusGrp;
	}

	public void setStatusGrp(String statusGrp) {
		this.statusGrp = statusGrp;
	}

	public BigInteger getNoOfDefaultAccountsGrp() {
		return noOfDefaultAccountsGrp;
	}

	public void setNoOfDefaultAccountsGrp(BigInteger noOfDefaultAccountsGrp) {
		this.noOfDefaultAccountsGrp = noOfDefaultAccountsGrp;
	}

	public BigInteger getTotalResponsesGrp() {
		return totalResponsesGrp;
	}

	public void setTotalResponsesGrp(BigInteger totalResponsesGrp) {
		this.totalResponsesGrp = totalResponsesGrp;
	}

	public BigInteger getNoOfClosedAccountsGrp() {
		return noOfClosedAccountsGrp;
	}

	public void setNoOfClosedAccountsGrp(BigInteger noOfClosedAccountsGrp) {
		this.noOfClosedAccountsGrp = noOfClosedAccountsGrp;
	}

	public BigInteger getNoOfActiveAccountsGrp() {
		return noOfActiveAccountsGrp;
	}

	public void setNoOfActiveAccountsGrp(BigInteger noOfActiveAccountsGrp) {
		this.noOfActiveAccountsGrp = noOfActiveAccountsGrp;
	}

	public BigInteger getNoOfOtherMfisGrp() {
		return noOfOtherMfisGrp;
	}

	public void setNoOfOtherMfisGrp(BigInteger noOfOtherMfisGrp) {
		this.noOfOtherMfisGrp = noOfOtherMfisGrp;
	}


	public String getOwnMfiIndecatorGrp() {
		return ownMfiIndecatorGrp;
	}

	public void setOwnMfiIndecatorGrp(String ownMfiIndecatorGrp) {
		this.ownMfiIndecatorGrp = ownMfiIndecatorGrp;
	}

	public BigInteger getTotalOwnDisbursedAmountGrp() {
		return totalOwnDisbursedAmountGrp;
	}

	public void setTotalOwnDisbursedAmountGrp(
			BigInteger totalOwnDisbursedAmountGrp) {
		this.totalOwnDisbursedAmountGrp = totalOwnDisbursedAmountGrp;
	}

	public BigInteger getTotalOtherDisbursedAmountGrp() {
		return totalOtherDisbursedAmountGrp;
	}

	public void setTotalOtherDisbursedAmountGrp(
			BigInteger totalOtherDisbursedAmountGrp) {
		this.totalOtherDisbursedAmountGrp = totalOtherDisbursedAmountGrp;
	}

	public BigInteger getTotalOwnCurrentBalanceGrp() {
		return totalOwnCurrentBalanceGrp;
	}

	public void setTotalOwnCurrentBalanceGrp(
			BigInteger totalOwnCurrentBalanceGrp) {
		this.totalOwnCurrentBalanceGrp = totalOwnCurrentBalanceGrp;
	}

	public BigInteger getTotalOtherCurrentBalanceGrp() {
		return totalOtherCurrentBalanceGrp;
	}

	public void setTotalOtherCurrentBalanceGrp(
			BigInteger totalOtherCurrentBalanceGrp) {
		this.totalOtherCurrentBalanceGrp = totalOtherCurrentBalanceGrp;
	}

	public BigInteger getTotalOwnInstallmentAmountGrp() {
		return totalOwnInstallmentAmountGrp;
	}

	public void setTotalOwnInstallmentAmountGrp(
			BigInteger totalOwnInstallmentAmountGrp) {
		this.totalOwnInstallmentAmountGrp = totalOwnInstallmentAmountGrp;
	}

	public BigInteger getTotalOtherInstallmentAmountGrp() {
		return totalOtherInstallmentAmountGrp;
	}

	public void setTotalOtherInstallmentAmountGrp(
			BigInteger totalOtherInstallmentAmountGrp) {
		this.totalOtherInstallmentAmountGrp = totalOtherInstallmentAmountGrp;
	}

	public BigInteger getMaxWorstDelequencyGrp() {
		return maxWorstDelequencyGrp;
	}

	public void setMaxWorstDelequencyGrp(BigInteger maxWorstDelequencyGrp) {
		this.maxWorstDelequencyGrp = maxWorstDelequencyGrp;
	}

	public String getErrorsGrp() {
		return errorsGrp;
	}

	public void setErrorsGrp(String errorsGrp) {
		this.errorsGrp = errorsGrp;
	}

	public String getMatchedTypeGrp() {
		return matchedTypeGrp;
	}

	public void setMatchedTypeGrp(String matchedTypeGrp) {
		this.matchedTypeGrp = matchedTypeGrp;
	}

	public String getMfiGrp() {
		return mfiGrp;
	}

	public void setMfiGrp(String mfiGrp) {
		this.mfiGrp = mfiGrp;
	}

	public String getMfiIdGrp() {
		return mfiIdGrp;
	}

	public void setMfiIdGrp(String mfiIdGrp) {
		this.mfiIdGrp = mfiIdGrp;
	}

	public String getBranchGrp() {
		return branchGrp;
	}

	public void setBranchGrp(String branchGrp) {
		this.branchGrp = branchGrp;
	}

	public String getKendraGrp() {
		return kendraGrp;
	}

	public void setKendraGrp(String kendraGrp) {
		this.kendraGrp = kendraGrp;
	}

	public String getNameGrp() {
		return nameGrp;
	}

	public void setNameGrp(String nameGrp) {
		this.nameGrp = nameGrp;
	}

	public String getSpouseGrp() {
		return spouseGrp;
	}

	public void setSpouseGrp(String spouseGrp) {
		this.spouseGrp = spouseGrp;
	}

	public String getFatherGrp() {
		return fatherGrp;
	}

	public void setFatherGrp(String fatherGrp) {
		this.fatherGrp = fatherGrp;
	}

	public String getCnsmrMbrIdGrp() {
		return cnsmrMbrIdGrp;
	}

	public void setCnsmrMbrIdGrp(String cnsmrMbrIdGrp) {
		this.cnsmrMbrIdGrp = cnsmrMbrIdGrp;
	}

	public Date getDobGrp() {
		return dobGrp;
	}

	public void setDobGrp(Date dobGrp) {
		this.dobGrp = dobGrp;
	}

	public Integer getAgeGrp() {
		return ageGrp;
	}

	public void setAgeGrp(Integer ageGrp) {
		this.ageGrp = ageGrp;
	}

	public Date getAgeAsOnGrp() {
		return ageAsOnGrp;
	}

	public void setAgeAsOnGrp(Date ageAsOnGrp) {
		this.ageAsOnGrp = ageAsOnGrp;
	}

	public String getPhoneGrp() {
		return phoneGrp;
	}

	public void setPhoneGrp(String phoneGrp) {
		this.phoneGrp = phoneGrp;
	}

	public String getAddress1Grp() {
		return address1Grp;
	}

	public void setAddress1Grp(String address1Grp) {
		this.address1Grp = address1Grp;
	}

	public String getAddress2Grp() {
		return address2Grp;
	}

	public void setAddress2Grp(String address2Grp) {
		this.address2Grp = address2Grp;
	}

	public String getRelTyp1Grp() {
		return relTyp1Grp;
	}

	public void setRelTyp1Grp(String relTyp1Grp) {
		this.relTyp1Grp = relTyp1Grp;
	}

	public String getRelNm1Grp() {
		return relNm1Grp;
	}

	public void setRelNm1Grp(String relNm1Grp) {
		this.relNm1Grp = relNm1Grp;
	}

	public String getRelTyp2Grp() {
		return relTyp2Grp;
	}

	public void setRelTyp2Grp(String relTyp2Grp) {
		this.relTyp2Grp = relTyp2Grp;
	}

	public String getRelNm2Grp() {
		return relNm2Grp;
	}

	public void setRelNm2Grp(String relNm2Grp) {
		this.relNm2Grp = relNm2Grp;
	}

	public String getIdTyp1Grp() {
		return idTyp1Grp;
	}

	public void setIdTyp1Grp(String idTyp1Grp) {
		this.idTyp1Grp = idTyp1Grp;
	}

	public String getIdValue1Grp() {
		return idValue1Grp;
	}

	public void setIdValue1Grp(String idValue1Grp) {
		this.idValue1Grp = idValue1Grp;
	}

	public String getIdTyp2Grp() {
		return idTyp2Grp;
	}

	public void setIdTyp2Grp(String idTyp2Grp) {
		this.idTyp2Grp = idTyp2Grp;
	}

	public String getIdValue2Grp() {
		return idValue2Grp;
	}

	public void setIdValue2Grp(String idValue2Grp) {
		this.idValue2Grp = idValue2Grp;
	}

	public String getGroupIdGrp() {
		return groupIdGrp;
	}

	public void setGroupIdGrp(String groupIdGrp) {
		this.groupIdGrp = groupIdGrp;
	}

	public Date getGroupCreationDateGrp() {
		return groupCreationDateGrp;
	}

	public void setGroupCreationDateGrp(Date groupCreationDateGrp) {
		this.groupCreationDateGrp = groupCreationDateGrp;
	}

	public Date getInsertDateGrp() {
		return insertDateGrp;
	}

	public void setInsertDateGrp(Date insertDateGrp) {
		this.insertDateGrp = insertDateGrp;
	}

	public String getAcctTypeGrp() {
		return acctTypeGrp;
	}

	public void setAcctTypeGrp(String acctTypeGrp) {
		this.acctTypeGrp = acctTypeGrp;
	}

	public String getFreqGrp() {
		return freqGrp;
	}

	public void setFreqGrp(String freqGrp) {
		this.freqGrp = freqGrp;
	}

	public String getStatusAccountGrp() {
		return statusAccountGrp;
	}

	public void setStatusAccountGrp(String statusAccountGrp) {
		this.statusAccountGrp = statusAccountGrp;
	}

	public String getAcctNumberGrp() {
		return acctNumberGrp;
	}

	public void setAcctNumberGrp(String acctNumberGrp) {
		this.acctNumberGrp = acctNumberGrp;
	}

	public String getDisbursedAmtGrp() {
		return disbursedAmtGrp;
	}

	public void setDisbursedAmtGrp(String disbursedAmtGrp) {
		this.disbursedAmtGrp = disbursedAmtGrp;
	}

	public String getCurrentBalGrp() {
		return currentBalGrp;
	}

	public void setCurrentBalGrp(String currentBalGrp) {
		this.currentBalGrp = currentBalGrp;
	}

	public String getInstallmentAmtGrp() {
		return installmentAmtGrp;
	}

	public void setInstallmentAmtGrp(String installmentAmtGrp) {
		this.installmentAmtGrp = installmentAmtGrp;
	}

	public String getOverdueAmtGrp() {
		return overdueAmtGrp;
	}

	public void setOverdueAmtGrp(String overdueAmtGrp) {
		this.overdueAmtGrp = overdueAmtGrp;
	}

	public String getWriteOffAmtGrp() {
		return writeOffAmtGrp;
	}

	public void setWriteOffAmtGrp(String writeOffAmtGrp) {
		this.writeOffAmtGrp = writeOffAmtGrp;
	}

	public Date getDisbursedDtGrp() {
		return disbursedDtGrp;
	}

	public void setDisbursedDtGrp(Date disbursedDtGrp) {
		this.disbursedDtGrp = disbursedDtGrp;
	}

	public Date getClosedDtGrp() {
		return closedDtGrp;
	}

	public void setClosedDtGrp(Date closedDtGrp) {
		this.closedDtGrp = closedDtGrp;
	}

	public Date getRecentDelinqDtGrp() {
		return recentDelinqDtGrp;
	}

	public void setRecentDelinqDtGrp(Date recentDelinqDtGrp) {
		this.recentDelinqDtGrp = recentDelinqDtGrp;
	}

	public BigInteger getDpdGrp() {
		return dpdGrp;
	}

	public void setDpdGrp(BigInteger dpdGrp) {
		this.dpdGrp = dpdGrp;
	}

	public Integer getInqCntGrp() {
		return inqCntGrp;
	}

	public void setInqCntGrp(Integer inqCntGrp) {
		this.inqCntGrp = inqCntGrp;
	}

	public Date getInfoAsOnGrp() {
		return infoAsOnGrp;
	}

	public void setInfoAsOnGrp(Date infoAsOnGrp) {
		this.infoAsOnGrp = infoAsOnGrp;
	}

	public BigInteger getWorstDelequencyAmountGrp() {
		return worstDelequencyAmountGrp;
	}

	public void setWorstDelequencyAmountGrp(BigInteger worstDelequencyAmountGrp) {
		this.worstDelequencyAmountGrp = worstDelequencyAmountGrp;
	}

	public String getPaymentHistoryGrp() {
		return paymentHistoryGrp;
	}

	public void setPaymentHistoryGrp(String paymentHistoryGrp) {
		this.paymentHistoryGrp = paymentHistoryGrp;
	}

	public String getIsActiveBorrowerGrp() {
		return isActiveBorrowerGrp;
	}

	public void setIsActiveBorrowerGrp(String isActiveBorrowerGrp) {
		this.isActiveBorrowerGrp = isActiveBorrowerGrp;
	}

	public Integer getNoOfBorrowersGrp() {
		return noOfBorrowersGrp;
	}

	public void setNoOfBorrowersGrp(Integer noOfBorrowersGrp) {
		this.noOfBorrowersGrp = noOfBorrowersGrp;
	}

	public String getCommentResGrp() {
		return commentResGrp;
	}

	public void setCommentResGrp(String commentResGrp) {
		this.commentResGrp = commentResGrp;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public Date getInquiryDate() {
		return inquiryDate;
	}

	public void setInquiryDate(Date inquiryDate) {
		this.inquiryDate = inquiryDate;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOutputWriteFlag() {
		return outputWriteFlag;
	}

	public void setOutputWriteFlag(String outputWriteFlag) {
		this.outputWriteFlag = outputWriteFlag;
	}

	public String getOutputWriteTime() {
		return outputWriteTime;
	}

	public void setOutputWriteTime(String outputWriteTime) {
		this.outputWriteTime = outputWriteTime;
	}

	public String getOutputReadTime() {
		return outputReadTime;
	}

	public void setOutputReadTime(String outputReadTime) {
		this.outputReadTime = outputReadTime;
	}

	@Override
	public String toString() {
		return "HibHighmarkAdOverlapSropDomain [id=" + id + ", srno=" + srno
				+ ", memberReferenceNumber=" + memberReferenceNumber
				+ ", soaSourceName=" + soaSourceName + ", dateOfRequest="
				+ dateOfRequest + ", preparedFor=" + preparedFor
				+ ", preparedForId=" + preparedForId + ", dateOfIssue="
				+ dateOfIssue + ", reportId=" + reportId + ", nameIq=" + nameIq
				+ ", spouseIq=" + spouseIq + ", fatherIq=" + fatherIq
				+ ", motherIq=" + motherIq + ", dobIq=" + dobIq + ", ageIq="
				+ ageIq + ", ageAsOnIq=" + ageAsOnIq + ", genderIq=" + genderIq
				+ ", phone1Iq=" + phone1Iq + ", phone2Iq=" + phone2Iq
				+ ", phone3Iq=" + phone3Iq + ", address1Iq=" + address1Iq
				+ ", address2Iq=" + address2Iq + ", relTyp1Iq=" + relTyp1Iq
				+ ", relNm1Iq=" + relNm1Iq + ", relTyp2Iq=" + relTyp2Iq
				+ ", relNm2Iq=" + relNm2Iq + ", relTyp3Iq=" + relTyp3Iq
				+ ", relNm3Iq=" + relNm3Iq + ", relTyp4Iq=" + relTyp4Iq
				+ ", relNm4Iq=" + relNm4Iq + ", idType1Iq=" + idType1Iq
				+ ", idValue1Iq=" + idValue1Iq + ", idType2Iq=" + idType2Iq
				+ ", idValue2Iq=" + idValue2Iq + ", rationCardIq="
				+ rationCardIq + ", votersIdIq=" + votersIdIq
				+ ", drivingLicenceNoIq=" + drivingLicenceNoIq + ", panIq="
				+ panIq + ", passportIq=" + passportIq + ", otherIdIq="
				+ otherIdIq + ", branchIq=" + branchIq + ", kendraIq="
				+ kendraIq + ", mbrIdIq=" + mbrIdIq + ", credtIqPurpsTyp="
				+ credtIqPurpsTyp + ", credtInqPurpsTypDesc="
				+ credtInqPurpsTypDesc + ", creditInquiryStage="
				+ creditInquiryStage + ", credtRptId=" + credtRptId
				+ ", credtReqTyp=" + credtReqTyp + ", credtRptTrnDtTm="
				+ credtRptTrnDtTm + ", acOpenDt=" + acOpenDt + ", loanAmount="
				+ loanAmount + ", entityId=" + entityId + ", statusIndPr="
				+ statusIndPr + ", noOfDefaultAccountsIndPr="
				+ noOfDefaultAccountsIndPr + ", totalResponsesIndIndPr="
				+ totalResponsesIndIndPr + ", noOfClosedAccountsIndPr="
				+ noOfClosedAccountsIndPr + ", noOfActiveAccountsIndPr="
				+ noOfActiveAccountsIndPr + ", noOfOtherMfisIndPr="
				+ noOfOtherMfisIndPr + ", ownMfiIndecatorIndPr="
				+ ownMfiIndecatorIndPr + ", totalOwnDisbursedAmountIndPr="
				+ totalOwnDisbursedAmountIndPr
				+ ", totalOtherDisbursedAmountIndPr="
				+ totalOtherDisbursedAmountIndPr
				+ ", totalOwnCurrentBalanceIndPr="
				+ totalOwnCurrentBalanceIndPr
				+ ", totalOtherCurrentBalanceIndPr="
				+ totalOtherCurrentBalanceIndPr
				+ ", totalOwnInstallmentAmountIndPr="
				+ totalOwnInstallmentAmountIndPr
				+ ", totalOtherInstallmentAmountIndPr="
				+ totalOtherInstallmentAmountIndPr
				+ ", maxWorstDelequencyIndPr=" + maxWorstDelequencyIndPr
				+ ", errorsIndPr=" + errorsIndPr + ", statusIndSe="
				+ statusIndSe + ", noOfDefaultAccountsIndSe="
				+ noOfDefaultAccountsIndSe + ", totalResponsesIndIndSe="
				+ totalResponsesIndIndSe + ", noOfClosedAccountsIndSe="
				+ noOfClosedAccountsIndSe + ", noOfActiveAccountsIndSe="
				+ noOfActiveAccountsIndSe + ", noOfOtherMfisIndSe="
				+ noOfOtherMfisIndSe + ", ownMfiIndecatorIndSe="
				+ ownMfiIndecatorIndSe + ", totalOwnDisbursedAmountIndSe="
				+ totalOwnDisbursedAmountIndSe
				+ ", totalOtherDisbursedAmountIndSe="
				+ totalOtherDisbursedAmountIndSe
				+ ", totalOwnCurrentBalanceIndSe="
				+ totalOwnCurrentBalanceIndSe
				+ ", totalOtherCurrentBalanceIndSe="
				+ totalOtherCurrentBalanceIndSe
				+ ", totalOwnInstallmentAmountIndSe="
				+ totalOwnInstallmentAmountIndSe
				+ ", totalOtherInstallmentAmountIndSe="
				+ totalOtherInstallmentAmountIndSe
				+ ", maxWorstDelequencyIndSe=" + maxWorstDelequencyIndSe
				+ ", errorsIndSe=" + errorsIndSe + ", statusInd=" + statusInd
				+ ", noOfDefaultAccountsInd=" + noOfDefaultAccountsInd
				+ ", totalResponsesInd=" + totalResponsesInd
				+ ", noOfClosedAccountsInd=" + noOfClosedAccountsInd
				+ ", noOfActiveAccountsInd=" + noOfActiveAccountsInd
				+ ", noOfOtherMfisInd=" + noOfOtherMfisInd
				+ ", ownMfiIndecatorInd=" + ownMfiIndecatorInd
				+ ", totalOwnDisbursedAmountInd=" + totalOwnDisbursedAmountInd
				+ ", totalOtherDisbursedAmountInd="
				+ totalOtherDisbursedAmountInd + ", totalOwnCurrentBalanceInd="
				+ totalOwnCurrentBalanceInd + ", totalOtherCurrentBalanceInd="
				+ totalOtherCurrentBalanceInd
				+ ", totalOwnInstallmentAmountInd="
				+ totalOwnInstallmentAmountInd
				+ ", totalOtherInstallmentAmountInd="
				+ totalOtherInstallmentAmountInd + ", maxWorstDelequencyInd="
				+ maxWorstDelequencyInd + ", errorsInd=" + errorsInd
				+ ", matchedTypeInd=" + matchedTypeInd + ", mfiInd=" + mfiInd
				+ ", mfiIdInd=" + mfiIdInd + ", branchInd=" + branchInd
				+ ", kendraInd=" + kendraInd + ", nameInd=" + nameInd
				+ ", spouseInd=" + spouseInd + ", fatherInd=" + fatherInd
				+ ", cnsmrMbrIdInd=" + cnsmrMbrIdInd + ", dobInd=" + dobInd
				+ ", ageInd=" + ageInd + ", ageAsOnInd=" + ageAsOnInd
				+ ", phoneInd=" + phoneInd + ", address1Ind=" + address1Ind
				+ ", address2Ind=" + address2Ind + ", relTyp1Ind=" + relTyp1Ind
				+ ", relNm1Ind=" + relNm1Ind + ", relTyp2Ind=" + relTyp2Ind
				+ ", relNm2Ind=" + relNm2Ind + ", idTyp1Ind=" + idTyp1Ind
				+ ", idValue1Ind=" + idValue1Ind + ", idTyp2Ind=" + idTyp2Ind
				+ ", idValue2Ind=" + idValue2Ind + ", groupIdInd=" + groupIdInd
				+ ", groupCreationDateInd=" + groupCreationDateInd
				+ ", insertDateInd=" + insertDateInd + ", acctTypeInd="
				+ acctTypeInd + ", freqInd=" + freqInd + ", statusAccountInd="
				+ statusAccountInd + ", acctNumberInd=" + acctNumberInd
				+ ", disbursedAmtInd=" + disbursedAmtInd + ", currentBalInd="
				+ currentBalInd + ", installmentAmtInd=" + installmentAmtInd
				+ ", overdueAmtInd=" + overdueAmtInd + ", writeOffAmtInd="
				+ writeOffAmtInd + ", disbursedDtInd=" + disbursedDtInd
				+ ", closedDtInd=" + closedDtInd + ", recentDelinqDtInd="
				+ recentDelinqDtInd + ", dpdInd=" + dpdInd + ", inqCntInd="
				+ inqCntInd + ", infoAsOnInd=" + infoAsOnInd
				+ ", worstDelequencyAmountInd=" + worstDelequencyAmountInd
				+ ", paymentHistoryInd=" + paymentHistoryInd
				+ ", isActiveBorrowerInd=" + isActiveBorrowerInd
				+ ", noOfBorrowersInd=" + noOfBorrowersInd + ", commentResInd="
				+ commentResInd + ", statusGrpPr=" + statusGrpPr
				+ ", noOfDefaultAccountsGrpPr=" + noOfDefaultAccountsGrpPr
				+ ", totalResponsesIndGrpPr=" + totalResponsesIndGrpPr
				+ ", noOfClosedAccountsGrpPr=" + noOfClosedAccountsGrpPr
				+ ", noOfActiveAccountsGrpPr=" + noOfActiveAccountsGrpPr
				+ ", noOfOtherMfisGrpPr=" + noOfOtherMfisGrpPr
				+ ", ownMfiIndecatorGrpPr=" + ownMfiIndecatorGrpPr
				+ ", totalOwnDisbursedAmountGrpPr="
				+ totalOwnDisbursedAmountGrpPr
				+ ", totalOtherDisbursedAmountGrpPr="
				+ totalOtherDisbursedAmountGrpPr
				+ ", totalOwnCurrentBalanceGrpPr="
				+ totalOwnCurrentBalanceGrpPr
				+ ", totalOtherCurrentBalanceGrpPr="
				+ totalOtherCurrentBalanceGrpPr
				+ ", totalOwnInstallmentAmountGrpPr="
				+ totalOwnInstallmentAmountGrpPr
				+ ", totalOtherInstallmentAmountGrpPr="
				+ totalOtherInstallmentAmountGrpPr
				+ ", maxWorstDelequencyGrpPr=" + maxWorstDelequencyGrpPr
				+ ", errorsGrpPr=" + errorsGrpPr + ", statusGrpSe="
				+ statusGrpSe + ", noOfDefaultAccountsGrpSe="
				+ noOfDefaultAccountsGrpSe + ", totalResponsesIndGrpSe="
				+ totalResponsesIndGrpSe + ", noOfClosedAccountsGrpSe="
				+ noOfClosedAccountsGrpSe + ", noOfActiveAccountsGrpSe="
				+ noOfActiveAccountsGrpSe + ", noOfOtherMfisGrpSe="
				+ noOfOtherMfisGrpSe + ", ownMfiIndecatorGrpSe="
				+ ownMfiIndecatorGrpSe + ", totalOwnDisbursedAmountGrpSe="
				+ totalOwnDisbursedAmountGrpSe
				+ ", totalOtherDisbursedAmountGrpSe="
				+ totalOtherDisbursedAmountGrpSe
				+ ", totalOwnCurrentBalanceGrpSe="
				+ totalOwnCurrentBalanceGrpSe
				+ ", totalOtherCurrentBalanceGrpSe="
				+ totalOtherCurrentBalanceGrpSe
				+ ", totalOwnInstallmentAmountGrpSe="
				+ totalOwnInstallmentAmountGrpSe
				+ ", totalOtherInstallmentAmountGrpSe="
				+ totalOtherInstallmentAmountGrpSe
				+ ", maxWorstDelequencyGrpSe=" + maxWorstDelequencyGrpSe
				+ ", errorsGrpSe=" + errorsGrpSe + ", statusGrp=" + statusGrp
				+ ", noOfDefaultAccountsGrp=" + noOfDefaultAccountsGrp
				+ ", totalResponsesGrp=" + totalResponsesGrp
				+ ", noOfClosedAccountsGrp=" + noOfClosedAccountsGrp
				+ ", noOfActiveAccountsGrp=" + noOfActiveAccountsGrp
				+ ", noOfOtherMfisGrp=" + noOfOtherMfisGrp
				+ ", ownMfiIndecatorGrp=" + ownMfiIndecatorGrp
				+ ", totalOwnDisbursedAmountGrp=" + totalOwnDisbursedAmountGrp
				+ ", totalOtherDisbursedAmountGrp="
				+ totalOtherDisbursedAmountGrp + ", totalOwnCurrentBalanceGrp="
				+ totalOwnCurrentBalanceGrp + ", totalOtherCurrentBalanceGrp="
				+ totalOtherCurrentBalanceGrp
				+ ", totalOwnInstallmentAmountGrp="
				+ totalOwnInstallmentAmountGrp
				+ ", totalOtherInstallmentAmountGrp="
				+ totalOtherInstallmentAmountGrp + ", maxWorstDelequencyGrp="
				+ maxWorstDelequencyGrp + ", errorsGrp=" + errorsGrp
				+ ", matchedTypeGrp=" + matchedTypeGrp + ", mfiGrp=" + mfiGrp
				+ ", mfiIdGrp=" + mfiIdGrp + ", branchGrp=" + branchGrp
				+ ", kendraGrp=" + kendraGrp + ", nameGrp=" + nameGrp
				+ ", spouseGrp=" + spouseGrp + ", fatherGrp=" + fatherGrp
				+ ", cnsmrMbrIdGrp=" + cnsmrMbrIdGrp + ", dobGrp=" + dobGrp
				+ ", ageGrp=" + ageGrp + ", ageAsOnGrp=" + ageAsOnGrp
				+ ", phoneGrp=" + phoneGrp + ", address1Grp=" + address1Grp
				+ ", address2Grp=" + address2Grp + ", relTyp1Grp=" + relTyp1Grp
				+ ", relNm1Grp=" + relNm1Grp + ", relTyp2Grp=" + relTyp2Grp
				+ ", relNm2Grp=" + relNm2Grp + ", idTyp1Grp=" + idTyp1Grp
				+ ", idValue1Grp=" + idValue1Grp + ", idTyp2Grp=" + idTyp2Grp
				+ ", idValue2Grp=" + idValue2Grp + ", groupIdGrp=" + groupIdGrp
				+ ", groupCreationDateGrp=" + groupCreationDateGrp
				+ ", insertDateGrp=" + insertDateGrp + ", acctTypeGrp="
				+ acctTypeGrp + ", freqGrp=" + freqGrp + ", statusAccountGrp="
				+ statusAccountGrp + ", acctNumberGrp=" + acctNumberGrp
				+ ", disbursedAmtGrp=" + disbursedAmtGrp + ", currentBalGrp="
				+ currentBalGrp + ", installmentAmtGrp=" + installmentAmtGrp
				+ ", overdueAmtGrp=" + overdueAmtGrp + ", writeOffAmtGrp="
				+ writeOffAmtGrp + ", disbursedDtGrp=" + disbursedDtGrp
				+ ", closedDtGrp=" + closedDtGrp + ", recentDelinqDtGrp="
				+ recentDelinqDtGrp + ", dpdGrp=" + dpdGrp + ", inqCntGrp="
				+ inqCntGrp + ", infoAsOnGrp=" + infoAsOnGrp
				+ ", worstDelequencyAmountGrp=" + worstDelequencyAmountGrp
				+ ", paymentHistoryGrp=" + paymentHistoryGrp
				+ ", isActiveBorrowerGrp=" + isActiveBorrowerGrp
				+ ", noOfBorrowersGrp=" + noOfBorrowersGrp + ", commentResGrp="
				+ commentResGrp + ", memberName=" + memberName
				+ ", inquiryDate=" + inquiryDate + ", purpose=" + purpose
				+ ", amount=" + amount + ", remark=" + remark
				+ ", outputWriteFlag=" + outputWriteFlag + ", outputWriteTime="
				+ outputWriteTime + ", outputReadTime=" + outputReadTime + "]";
	}
}
