/**
 * 
 */
package com.softcell.gonogo.model.mbdatapush;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Dipak
 * 
 */
public class ExperianEropDomainObject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Expose
	@SerializedName("EXPERIAN_EROP_DOMAIN_LIST")
	List<ExperianEROPDomain> experianEropDomainList;
	public List<ExperianEROPDomain> getExperianEropDomainList() {
		return experianEropDomainList;
	}
	public void setExperianEropDomainList(
			List<ExperianEROPDomain> experianEropDomainList) {
		this.experianEropDomainList = experianEropDomainList;
	}
	@Override
	public String toString() {
		return "ExperianEropDomainObject [experianEropDomainList="
				+ experianEropDomainList + "]";
	}
	
	

	
	
}
