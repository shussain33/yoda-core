package com.softcell.gonogo.model.mbdatapush.chm;

public class Comments {

	private Comment comment;

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "Comments [comment=" + comment + "]";
	}
}
