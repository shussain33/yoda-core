package com.softcell.gonogo.model.mbdatapush;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExperianOutputStaggingDAO {
	private static  Logger logger_= LoggerFactory.getLogger(ExperianOutputStaggingDAO.class);
	
//	public Boolean insertInExperianSrop(List<ExperianSROPDomain> experianSROPDomainList) throws SystemException, ApplicationResourceNotFound, DataException   {
//		return insertInExperianSrop_(experianSROPDomainList);
//	}

//	private Boolean insertInExperianSrop_(List<ExperianSROPDomain> experianSROPDomainList) throws SystemException, ApplicationResourceNotFound, DataException {
//		
//		Integer update = 0;
//		try{
//			
//			String sql= LookUpService.getDBQuery(CacheConstants.DB_QUERY, "ExperianOutputStaggingDAO_ExperianSrop");
//			
//			for(Integer i = 0; i<experianSROPDomainList.size() ;i++){	
//				
//				ExperianSROPDomain experianSropDomain = experianSROPDomainList.get(i);
//				
//				Object[] param = new Object[]{
//						  experianSropDomain.getSrNo(),
//						  experianSropDomain.getSoaSourceName(),
//						  experianSropDomain.getMemberReferenceNumber(),    
//						  experianSropDomain.getEnquiryDate(),
//					      experianSropDomain.getSystemcodeHeader(),
//					      experianSropDomain.getMessageTextHeader(),
//					      experianSropDomain.getReportdate(),
//					      experianSropDomain.getReportimeHeader(),
//					      experianSropDomain.getUserMessageText(),
//					      experianSropDomain.getEnquiryUsername(),
//						  experianSropDomain.getCreditProfileReportDate(), 
//					      experianSropDomain.getCreditProfileReportTime(), 
//					      experianSropDomain.getVersion(), 
//					      experianSropDomain.getReportNumber(),
//					      experianSropDomain.getSubscriber(), 
//					      experianSropDomain.getSubscriberName(),
//					      experianSropDomain.getEnquiryReason(),
//					      experianSropDomain.getFinancePurpose(),
//					      experianSropDomain.getAmountFinanced(), 
//					      experianSropDomain.getDurationOfAgreement(),
//					      experianSropDomain.getLastname(), 
//					      experianSropDomain.getFirstname(),
//					      experianSropDomain.getMiddleName1(), 
//					      experianSropDomain.getMiddleName2(),
//					      experianSropDomain.getMiddleName3(), 
//					      experianSropDomain.getGenderCode(), 
//					      experianSropDomain.getGender(),
//					      experianSropDomain.getIncomeTaxPanApp(),
//						  experianSropDomain.getPanIssueDateApp(), 
//						  experianSropDomain.getPanExpDateApp(),  
//					      experianSropDomain.getPassportNumberApp(), 
//						  experianSropDomain.getPassportIssueDateApp(),  
//						  experianSropDomain.getPassportExpDateApp(),  
//					      experianSropDomain.getVoterIdentityCardApp(), 
//						  experianSropDomain.getVoterIdIssueDateApp(),  
//						  experianSropDomain.getVoterIdExpDateApp(),  
//					      experianSropDomain.getDriverLicenseNumberApp(), 
//						  experianSropDomain.getDriverLicenseIssueDateApp(),  
//						  experianSropDomain.getDriverLicenseExpDateApp(),  
//					      experianSropDomain.getRationCardNumberApp(), 
//						  experianSropDomain.getRationCardIssueDateApp(),  
//					      experianSropDomain.getRationCardExpDateApp(),  
//					      experianSropDomain.getUniversalIdNumberApp(), 
//						  experianSropDomain.getUniversalIdIssueDateApp(),  
//						  experianSropDomain.getUniversalIdExpDateApp(),  
//						  experianSropDomain.getDateOfBirthApplicant(),  
//					      experianSropDomain.getTelephoneNumberApplicant1st(), 
//					      experianSropDomain.getTeleExtensionApp(), 
//					      experianSropDomain.getTeleTypeApp(), 
//					      experianSropDomain.getMobilePhoneNumber(), 
//					      experianSropDomain.getEmailIdApp(), 
//					      experianSropDomain.getIncome(), 
//					      experianSropDomain.getMaritalStatusCode(), 
//					      experianSropDomain.getMaritalStatus(), 
//					      experianSropDomain.getEmploymentStatus(), 
//					      experianSropDomain.getDateWithEmployer(), 
//					      experianSropDomain.getNumberOfMajorCreditCardHeld(), 
//					      experianSropDomain.getFlatNoPlotNoHouseNo(), 
//					      experianSropDomain.getBldgNoSocietyName(), 
//					      experianSropDomain.getRoadNoNameAreaLocality(), 
//					      experianSropDomain.getCity(), 
//					      experianSropDomain.getLandmark(), 
//					      experianSropDomain.getState(), 
//					      experianSropDomain.getPincode(), 
//					      experianSropDomain.getCountryCode(), 
//					      experianSropDomain.getAddFlatNoPlotNoHouseNo(), 
//					      experianSropDomain.getAddBldgNoSocietyName(), 
//					      experianSropDomain.getAddRoadNoNameAreaLocality(), 
//					      experianSropDomain.getAddCity(), 
//					      experianSropDomain.getAddLandmark(), 
//					      experianSropDomain.getAddState(), 
//					      experianSropDomain.getAddPincode(), 
//					      experianSropDomain.getAddCountrycode(), 
//					      experianSropDomain.getCreditAccountTotal(), 
//					      experianSropDomain.getCreditAccountActive(), 
//					      experianSropDomain.getCreditAccountDefault(), 
//					      experianSropDomain.getCreditAccountClosed(), 
//					      experianSropDomain.getCadsuitfiledCurrentBalance(), 
//					      experianSropDomain.getOutstandingBalanceSecured(), 
//					      experianSropDomain.getOutstandingBalSecuredPer(), 
//					      experianSropDomain.getOutstandingBalanceUnsecured(), 
//					      experianSropDomain.getOutstandingBalUnsecPer(), 
//					      experianSropDomain.getOutstandingBalanceAll(), 
//					      experianSropDomain.getIdentificationNumber(),
//					      experianSropDomain.getCaisSubscriberName(), 
//					      experianSropDomain.getAccountNumber(), 
//					      experianSropDomain.getPortfolioType(), 
//					      experianSropDomain.getAccountTypeCode(), 
//					      experianSropDomain.getAccountType(), 
//						  experianSropDomain.getOpenDate(),  
//					      experianSropDomain.getHighestCreditOrOrgnLoanAmt(), 
//					      experianSropDomain.getTermsDuration(), 
//					      experianSropDomain.getTermsFrequency(), 
//					      experianSropDomain.getScheduledMonthlyPayamt(), 
//					      experianSropDomain.getAccountStatusCode(), 
//					      experianSropDomain.getAccountStatus(), 
//					      experianSropDomain.getPaymentRating(), 
//					      experianSropDomain.getPaymentHistoryProfile(), 
//					      experianSropDomain.getSpecialComment(), 
//					      experianSropDomain.getCurrentBalanceTl(), 
//					      experianSropDomain.getAmountPastdueTl(), 
//					      experianSropDomain.getOriginalChargeOffAmount(), 
//						  experianSropDomain.getDateReported(),  
//						  experianSropDomain.getDateOfFirstDelinquency(),  
//						  experianSropDomain.getDateClosed(),  
//						  experianSropDomain.getDateOfLastPayment(),  
//					      experianSropDomain.getSuitfilledWillFullDefaultWrittenOffStatus(), 
//					      experianSropDomain.getSuitFiledWilfulDef(), 
//					      experianSropDomain.getWrittenOffSettledStatus(), 
//					      experianSropDomain.getValueOfCreditsLastMonth(), 
//					      experianSropDomain.getOccupationCode(), 
//					      experianSropDomain.getSattlementAmount(), 
//					      experianSropDomain.getValueOfColateral(), 
//					      experianSropDomain.getTypeOfColateral(),
//					      experianSropDomain.getWrittenOffAmtTotal(), 
//					      experianSropDomain.getWrittenOffAmtPrincipal(), 
//					      experianSropDomain.getRateOfInterest(), 
//					      experianSropDomain.getRepaymentTenure(), 
//					      experianSropDomain.getPromotionalRateFlag(), 
//					      experianSropDomain.getCaisIncome(), 
//					      experianSropDomain.getIncomeIndicator(), 
//					      experianSropDomain.getIncomeFrequencyIndicator(), 
//					      experianSropDomain.getDefaultStatusDate(),  
//						  experianSropDomain.getLitigationStatusDate(),  
//						  experianSropDomain.getWriteOffStatusDate(),  
//					      experianSropDomain.getCurrencyCode(), 
//					      experianSropDomain.getSubscriberComments(), 
//					      experianSropDomain.getConsumerComments(), 
//					      experianSropDomain.getAccountHolderTypeCode(), 
//					      experianSropDomain.getAccountHolderTypeName(), 
//					      experianSropDomain.getYearHist(), 
//					      experianSropDomain.getMonthHist(), 
//					      experianSropDomain.getDaysPastDue(), 
//					      experianSropDomain.getAssetClassification(),
//					      experianSropDomain.getYearAdvHist(), 
//					      experianSropDomain.getMonthAdvHist(), 
//					      experianSropDomain.getCashLimit(), 
//					      experianSropDomain.getCreditLimitAmt(), 
//					      experianSropDomain.getActualPaymentAmt(), 
//					      experianSropDomain.getEmiAmt(), 
//					      experianSropDomain.getCurrentBalanceAdvHist(), 
//					      experianSropDomain.getAmountPastDueAdvHist(), 
//					      experianSropDomain.getSurNameNonNormalized(), 
//					      experianSropDomain.getFirstNameNonNormalized(), 
//					      experianSropDomain.getMiddleName1NonNormalized(), 
//					      experianSropDomain.getMiddleName2NonNormalized(), 
//					      experianSropDomain.getMiddleName3NonNormalized(),
//					      experianSropDomain.getAlias(), 
//					      experianSropDomain.getCaisGenderCode(), 
//					      experianSropDomain.getCaisGender(), 
//					      experianSropDomain.getCaisIncomeTaxPan(), 
//					      experianSropDomain.getCaisPassportNumber(), 
//					      experianSropDomain.getVoterIdNumber(), 
//						  experianSropDomain.getDateOfBirth(),  
//					      experianSropDomain.getFirstLineOfAddNonNormalized(), 
//					      experianSropDomain.getSecondLineOfAddNonNormalized(), 
//					      experianSropDomain.getThirdLineOfAddNonNormalized(), 
//					      experianSropDomain.getCityNonNormalized(), 
//					      experianSropDomain.getFifthLineOfAddNonNormalized(),         
//					      experianSropDomain.getStateCodeNonNormalized(), 
//					      experianSropDomain.getStateNonNormalized(), 
//					      experianSropDomain.getZipPostalCodeNonNormalized(), 
//					      experianSropDomain.getCountryCodeNonNormalized(), 
//					      experianSropDomain.getAddIndicatorNonNormalized(), 
//					      experianSropDomain.getResidenceCodeNonNormalized(), 
//					      experianSropDomain.getResidenceNonNormalized(),
//					      experianSropDomain.getTelephoneNumber(), 
//					      experianSropDomain.getTeleTypePhone(), 
//					      experianSropDomain.getTeleExtensionPhone(), 
//					      experianSropDomain.getMobileTelephoneNumber(), 
//					      experianSropDomain.getFaxNumber(), 
//					      experianSropDomain.getEmailIdPhone(), 
//					      experianSropDomain.getIncomeTaxPanId(), 
//						  experianSropDomain.getPanIssueDateId(),  
//						  experianSropDomain.getPanExpDateId(),  
//					      experianSropDomain.getPassportNumberId(), 
//						  experianSropDomain.getPassportIssueDateId(),  
//						  experianSropDomain.getPassportExpDateId(),  
//					      experianSropDomain.getVoterIdentityCardId(), 
//						  experianSropDomain.getVoterIdIssueDateId(),   
//						  experianSropDomain.getVoterIdExpDateId(),  
//					      experianSropDomain.getDriverLicenseNumberId(), 
//						  experianSropDomain.getDriverLicenseIssueDateId(),  
//						  experianSropDomain.getDriverLicenseExpDateId(),  
//					      experianSropDomain.getRationCardNumberId(), 
//						  experianSropDomain.getRationCardIssueDateId(),  
//						  experianSropDomain.getRationCardExpDateId(),  
//					      experianSropDomain.getUniversalIdNumberId(), 
//						  experianSropDomain.getUniversalIdIssueDateId(),  
//						  experianSropDomain.getUniversalIdExpDateId(),  
//					      experianSropDomain.getEmailidId(), 
//					      experianSropDomain.getExactMatch(), 
//					      experianSropDomain.getCapsLast7Days(), 
//					      experianSropDomain.getCapsLast30Days(), 
//					      experianSropDomain.getCapsLast90Days(), 
//					      experianSropDomain.getCapsLast180Days(),
//					      experianSropDomain.getCapsSubscriberCode(), 
//					      experianSropDomain.getCapsSubscriberName(), 
//						  experianSropDomain.getCapsdateofRequest(),  
//						  experianSropDomain.getCapsReportDate(),  
//					      experianSropDomain.getCapsReportTime(), 
//					      experianSropDomain.getCapsReportNumber(), 
//					      experianSropDomain.getEnquiryReasonCode(), 
//					      experianSropDomain.getCapsEnquiryReason(), 
//					      experianSropDomain.getFinancePurposeCode(), 
//					      experianSropDomain.getCapsFinancePurpose(), 
//					      experianSropDomain.getCapsAmountFinanced(), 
//					      experianSropDomain.getCapsDurationOfAgreement(), 
//					      experianSropDomain.getCapsApplicantLastName(), 
//					      experianSropDomain.getCapsApplicantFirstName(), 
//					      experianSropDomain.getCapsApplicantMiddleName1(), 
//					      experianSropDomain.getCapsApplicantMiddleName2(), 
//					      experianSropDomain.getCapsApplicantMiddleName3(), 
//					      experianSropDomain.getCapsApplicantgenderCode(), 
//					      experianSropDomain.getCapsApplicantGender(), 
//					      experianSropDomain.getCapsIncomeTaxPan(), 
//						  experianSropDomain.getCapsPanIssueDate(),  
//						  experianSropDomain.getCapsPanExpDate(),  
//					      experianSropDomain.getCapsPassportNumber(), 
//						  experianSropDomain.getCapsPassportIssueDate(),  
//						  experianSropDomain.getCapsPassportExpDate(),  
//					      experianSropDomain.getCapsVoterIdentityCard(), 
//						  experianSropDomain.getCapsVoterIdIssueDate(),  
//						  experianSropDomain.getCapsVoterIdExpDate(),  
//					      experianSropDomain.getCapsDriverLicenseNumber(), 
//						  experianSropDomain.getCapsDriverLicenseIssueDate(),  
//						  experianSropDomain.getCapsDriverLicenseExpDate(),  
//					      experianSropDomain.getCapsRationCardNumber(), 
//						  experianSropDomain.getCapsRationCardIssueDate(),  
//						  experianSropDomain.getCapsRationCardExpDate(),  
//					      experianSropDomain.getCapsUniversalIdNumber(), 
//						  experianSropDomain.getCapsUniversalIdIssueDate(),  
//						  experianSropDomain.getCapsUniversalIdExpDate(),  
//						  experianSropDomain.getCapsApplicantDobApplicant(),  
//					      experianSropDomain.getCapsApplicantTelNoApplicant1st(), 
//					      experianSropDomain.getCapsApplicantTelExt(), 
//					      experianSropDomain.getCapsApplicantTelType(), 
//					      experianSropDomain.getCapsApplicantMobilePhoneNumber(),  
//					      experianSropDomain.getCapsApplicantEmailId(), 
//					      experianSropDomain.getCapsApplicantIncome(), 
//					      experianSropDomain.getApplicantMaritalStatusCode(), 
//					      experianSropDomain.getCapsApplicantMaritalStatus(), 
//					      experianSropDomain.getCapsApplicantEmploymentStatusCode(), 
//					      experianSropDomain.getCapsApplicantEmploymentStatus(), 
//					      experianSropDomain.getCapsApplicantDateWithEmployer(), 
//					      experianSropDomain.getCapsApplicantNoMajorCrditCardHeld(),
//					      experianSropDomain.getCapsApplicantFlatNoPlotNoHouseNo(), 
//					      experianSropDomain.getCapsApplicantBldgNoSocietyName(), 
//					      experianSropDomain.getCapsApplicantRoadNoNameAreaLocality(), 
//					      experianSropDomain.getCapsApplicantCity(), 
//					      experianSropDomain.getCapsApplicantLandmark(), 
//					      experianSropDomain.getCapsApplicantStateCode(), 
//					      experianSropDomain.getCapsApplicantState(), 
//					      experianSropDomain.getCapsApplicantPinCode(), 
//					      experianSropDomain.getCapsApplicantCountryCode(), 
//					      experianSropDomain.getBureauScore(), 
//					      experianSropDomain.getBureauScoreConfidLevel(), 
//					      experianSropDomain.getCreditRating(), 
//					      experianSropDomain.getTnOfBFHLCADExhl(), 
//					      experianSropDomain.getTotValOfBFHLCAD(), 
//					      experianSropDomain.getMNTSMRBFHLCAD(), 
//					      experianSropDomain.getTnOfHLCAD(),
//					      experianSropDomain.getTotValOfHLCAD(),
//					      experianSropDomain.getMntsmrHLCAD(),
//					      experianSropDomain.getTnOfTelcosCAD(),
//					      experianSropDomain.getTotValOfTelcosCad(),
//					      experianSropDomain.getMntsmrTelcosCad(),
//					      experianSropDomain.getTnOfmfCAD(),
//					      experianSropDomain.getTotValOfmfCAD(),
//					      experianSropDomain.getMntsmrmfCAD(),
//					      experianSropDomain.getTnOfRetailCAD(),
//					      experianSropDomain.getTotValOfRetailCAD(),
//					      experianSropDomain.getMntsmrRetailCAD(),
//					      experianSropDomain.getTnOfAllCAD(),
//					      experianSropDomain.getTotValOfAllCAD(),
//					      experianSropDomain.getMntsmrCADAll(),
//					      experianSropDomain.getTnOfBFHLACAExhl(),
//					      experianSropDomain.getBalBFHLACAExhl(),
//					      experianSropDomain.getWcdstBFHLACAExhl(),
//					      experianSropDomain.getWdspr6MntBFHLACAExhl(),
//					      experianSropDomain.getWdspr712MntBFHLACAExhl(),
//					      experianSropDomain.getAgeOfOldestBFHLACAExhl(),
//					      experianSropDomain.getHcbperrevaccBFHLACAExhl(),
//					      experianSropDomain.getTcbperrevaccBFHLACAExhl(),
//					      experianSropDomain.getTnOfHlACA(),
//					      experianSropDomain.getBalHlACA(),
//					      experianSropDomain.getWcdstHlACA(),
//					      experianSropDomain.getWdspr6MnthlACA(),
//					      experianSropDomain.getWdspr712mnthlACA(),
//					      experianSropDomain.getAgeOfOldesthlACA(),
//					      experianSropDomain.getTnOfMfACA(),
//					      experianSropDomain.getTotalBalMfACA(),
//					      experianSropDomain.getWcdstMfACA(),
//					      experianSropDomain.getWdspr6MntMfACA(),
//					      experianSropDomain.getWdspr712mntMfACA(),
//					      experianSropDomain.getAgeOfOldestMfACA(),
//					      experianSropDomain.getTnOfTelcosACA(),
//					      experianSropDomain.getTotalBalTelcosACA(),
//					      experianSropDomain.getWcdstTelcosACA(),
//					      experianSropDomain.getWdspr6mntTelcosACA(),
//					      experianSropDomain.getWdspr712mntTelcosACA(),
//					      experianSropDomain.getAgeOfOldestTelcosACA(),
//					      experianSropDomain.getTnOfRetailACA(),
//					      experianSropDomain.getTotalBalRetailACA(),
//					      experianSropDomain.getWcdstRetailACA(),
//					      experianSropDomain.getWdspr6mntRetailACA(),
//					      experianSropDomain.getWdspr712mntRetailACA(),
//					      experianSropDomain.getAgeOfOldestRetailACA(),
//					      experianSropDomain.getHcblmperrevaccret(),
//					      experianSropDomain.getTotCurBallmperrevaccret(),
//					      experianSropDomain.getTnOfallACA(),
//					      experianSropDomain.getBalAllACAExhl(),
//					      experianSropDomain.getWcdstAllACA(),
//					      experianSropDomain.getWdspr6mntallACA(),
//					      experianSropDomain.getWdspr712mntAllACA(),
//					      experianSropDomain.getAgeOfOldestAllACA(),
//					      experianSropDomain.getTnOfndelBFHLinACAExhl(),
//					      experianSropDomain.getTnOfDelBFHLInACAExhl(),
//					      experianSropDomain.getTnOfnDelHLInACA(),
//					      experianSropDomain.getTnOfDelhlInACA(),
//					      experianSropDomain.getTnOfnDelmfinACA(),
//					      experianSropDomain.getTnOfDelmfinACA(),
//					      experianSropDomain.getTnOfnDelTelcosInACA(),
//					      experianSropDomain.getTnOfdelTelcosInACA(),
//					      experianSropDomain.getTnOfndelRetailInACA(),
//					      experianSropDomain.getTnOfDelRetailInACA(),
//					      experianSropDomain.getBFHLCapsLast90Days(),
//					      experianSropDomain.getMfCapsLast90Days(),
//					      experianSropDomain.getTelcosCapsLast90Days(),
//					      experianSropDomain.getRetailCapsLast90Days(),
//					      experianSropDomain.getTnOfocomcad(),
//					      experianSropDomain.getTotValOfocomCAD(),
//					      experianSropDomain.getMntsmrocomCAD(),
//					      experianSropDomain.getTnOfocomACA(),
//					      experianSropDomain.getBalocomACAExhl(),
//					      experianSropDomain.getBalOcomacahlonly(),
//					      experianSropDomain.getWcdstocomACA(),
//					      experianSropDomain.getHcblmperrevocomACA(),
//					      experianSropDomain.getTnOfnDelocominACA(),
//					      experianSropDomain.getTnOfDelocominACA(),
//					      experianSropDomain.getTnOfocomCapsLast90Days(),
//					      experianSropDomain.getAnyRelcbdatadisyn(),
//					      experianSropDomain.getOthrelcbdfcposmatyn(),
//					      experianSropDomain.getTnOfCADclassedassfwdwo(),
//					      experianSropDomain.getMntsmrcadclassedassfwdwo(),
//					      experianSropDomain.getNumOfCadsfwdwolast24mnt(),
//					      experianSropDomain.getTotCurBalLivesAcc(),
//					      experianSropDomain.getTotCurBalLiveuAcc(),
//					      experianSropDomain.getTotCurBalMaxBalLivesAcc(),
//					      experianSropDomain.getTotCurBalMaxBalLiveuAcc(),
//					      experianSropDomain.getAccountKey(),
//											"Y"	
//
//				};
//				
//				update = DbUtils.getJdbcTemplate().update(sql,param);
//			}
//			if(update > 0){
//				return true;
//			}
//			
//			return false;
//			
//		}catch (DataAccessException e) {
//			e.printStackTrace();
//			throw new DataException("Error While Dumping data in Experian Srop Table"+e.getMessage());
//		} catch (ApplicationResourceNotFound e) {
//			e.printStackTrace();
//			throw new ApplicationResourceNotFound(""+e.getMessage());
//		} catch (DataException e) {
//			e.printStackTrace();
//			throw new DataException(""+e.getMessage());
//		}catch (Exception e) {
//			throw new SystemException("Unknown Exception Occurs while Dumping Data in Experian SROP Table "+e.getMessage());
//		}
//	}

	/*public Boolean insertInExperianErop(List<ExperianEROPDomain> experianEROPDomainList) throws SystemException, ApplicationResourceNotFound, DataException {
		try{
			
			String sql = LookUpService.getDBQuery(CacheConstants.DB_QUERY, "ExperianOutputStaggingDAO_ExperianErop");
			
			for (ExperianEROPDomain experianEROPDomain : experianEROPDomainList) {
				
				Object[] param = new Object[]{
						experianEROPDomain.getSrno(),
						experianEROPDomain.getEnquiryDate(),
						experianEROPDomain.getMemberrefrenceNumber(),
						experianEROPDomain.getMessageText(),
						experianEROPDomain.getSourceName(),
						experianEROPDomain.getSystemCode(),
						experianEROPDomain.getReportDate(),
						experianEROPDomain.getReportTime()
				};
				
				DbUtils.getJdbcTemplate().update(sql, param);
				
			}
			
			
			return true;
			
		}catch (DataAccessException e) {
			e.printStackTrace();
			throw new DataException("Error While Dumping data in Experian EROP Table"+e.getMessage());
		} catch (ApplicationResourceNotFound e) {
			e.printStackTrace();
			throw new ApplicationResourceNotFound(""+e.getMessage());
		} catch (DataException e) {
			e.printStackTrace();
			throw new DataException(""+e.getMessage());
		}catch (Exception e) {
			e.printStackTrace();
			throw new SystemException("Unknown Exception Occurs while Dumping Data in Experian EROP Table "+e.getMessage());
		}
		
	}

	public void insertInExperianSrop(List<HibExperianSropDomain> experianSROPDomainList) throws DataException {
		Session session  = null;
		Transaction tx  = null;
		try {
			session = DbUtils.getSession();
			tx = session.getTransaction();
			tx.begin();
			
			for (Integer i = 0; i < experianSROPDomainList.size(); i++) {
				 HibExperianSropDomain hibExperianSropDomain = experianSROPDomainList.get(i);
				 hibExperianSropDomain.setId(i);
				 hibExperianSropDomain.setEnquiryDate(new Date());
				session.save("json-exp-srop", hibExperianSropDomain);
				if (i % 50 == 0) {
					session.flush();
					session.clear();
				}
			}
			
			tx.commit();
		} catch (Exception e) {
			if (tx != null){
				tx.rollback();
			}
			logger_.error(e.getMessage());
			throw new DataException(e.getMessage());
		} finally{
			if(session!=null && (session.isConnected() || session.isOpen())){
				session.flush();
				session.clear();
				session.close();
			}
		}
	}*/

}
