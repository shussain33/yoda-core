package com.softcell.gonogo.model.mbdatapush.chm;

public class SecondaryMatch {
	private String name;
	private String address;
	private String dob;
	private String phone;
	private String pan;
	private String passport;
	private String drivingLicense;
	private String voterId;
	private String email;
	private String rationCard;
	private LoanDetails loanDetails;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getPassport() {
		return passport;
	}
	public void setPassport(String passport) {
		this.passport = passport;
	}
	public String getDrivingLicense() {
		return drivingLicense;
	}
	public void setDrivingLicense(String drivingLicense) {
		this.drivingLicense = drivingLicense;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRationCard() {
		return rationCard;
	}
	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}
	public LoanDetails getLoanDetails() {
		return loanDetails;
	}
	public void setLoanDetails(LoanDetails loanDetails) {
		this.loanDetails = loanDetails;
	}
	@Override
	public String toString() {
		return "SecondaryMatch [name=" + name + ", address=" + address
				+ ", dob=" + dob + ", phone=" + phone + ", pan=" + pan
				+ ", passport=" + passport + ", drivingLicense="
				+ drivingLicense + ", voterId=" + voterId + ", email=" + email
				+ ", rationCard=" + rationCard + ", loanDetails=" + loanDetails
				+ "]";
	}
}
