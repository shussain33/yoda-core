package com.softcell.gonogo.model.mbdatapush.chm;


public class Inquiry {
	
	private String inquiryUniqueRefrenceNumber;
	private String memberId;
	private String requestDateTime;
	private String reportId;
	private String responseDateTime;
	private String responseType;
	private Errors  errors;
	
	
	public String getInquiryUniqueRefrenceNumber() {
		return inquiryUniqueRefrenceNumber;
	}
	public void setInquiryUniqueRefrenceNumber(String inquiryUniqueRefrenceNumber) {
		this.inquiryUniqueRefrenceNumber = inquiryUniqueRefrenceNumber;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getRequestDateTime() {
		return requestDateTime;
	}
	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getResponseDateTime() {
		return responseDateTime;
	}
	public void setResponseDateTime(String responseDateTime) {
		this.responseDateTime = responseDateTime;
	}
	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	public Errors getErrors() {
		return errors;
	}
	public void setErrors(Errors errors) {
		this.errors = errors;
	}
	@Override
	public String toString() {
		return "Inquiry [inquiryUniqueRefrenceNumber="
				+ inquiryUniqueRefrenceNumber + ", memberId=" + memberId
				+ ", requestDateTime=" + requestDateTime + ", reportId="
				+ reportId + ", responseDateTime=" + responseDateTime
				+ ", responseType=" + responseType + ", errors=" + errors + "]";
	}
 
}
