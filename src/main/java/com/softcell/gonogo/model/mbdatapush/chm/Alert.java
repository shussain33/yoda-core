package com.softcell.gonogo.model.mbdatapush.chm;

public class Alert {
	
	private String alertType;
	private String alertDescription;
	
	
	public String getAlertType() {
		return alertType;
	}
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}
	public String getAlertDescription() {
		return alertDescription;
	}
	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}
	@Override
	public String toString() {
		return "Alerts [alertType=" + alertType + ", alertDescription="
				+ alertDescription + "]";
	}

}
