package com.softcell.gonogo.model.mbdatapush;

import com.softcell.gonogo.model.los.CIBILEROPErrorDomain;
import com.softcell.gonogo.model.los.CIBILEropDomainObject;
import com.softcell.gonogo.model.los.CIBILSropDomainObject;
import com.softcell.gonogo.model.los.HibCIBILSropDomain;
import com.softcell.gonogo.model.mbdatapush.chm.HibHighmarkAOREropDomain;
import com.softcell.soap.mb.hdfcbank.xsd.multibureau.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class SoaSropEropMapper {
	private static Logger logger_ = LoggerFactory.getLogger(SoaSropEropMapper.class);
	static SimpleDateFormat sdf=null;
	static SimpleDateFormat sdf1=null;
	static SimpleDateFormat sdf2=null;
	static SimpleDateFormat sdf3=null;
	static{
		sdf = new SimpleDateFormat("ddMMyyyy");
		sdf1 = new SimpleDateFormat("dd-MM-yyyy");
		sdf2 = new SimpleDateFormat("yyyyMMdd");
		sdf3 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		
	}
		
	
	
	public ResponseJSONType chmAdvEropMapping(Object responseJsonObject,ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> chmAdvEropMapping");
		try {
			CHMAdvEropDomainObject chmAdvEropDomainObject = (CHMAdvEropDomainObject) responseJsonObject;
			if (chmAdvEropDomainObject != null) {
				List<HibHighmarkAOREropDomain> hmAdvEropDomainList = chmAdvEropDomainObject.getHmAdvEropDomainList();
				if (hmAdvEropDomainList != null&& hmAdvEropDomainList.size() > 0) {

					List<ChmAorERespType> chmAorERespTypList = new ArrayList<ChmAorERespType>();
					for (HibHighmarkAOREropDomain hibHighmarkAOREropDomain : hmAdvEropDomainList) {

						ChmAorERespType chmAorERespType = new ChmAorERespType();
						chmAorERespType.setSRNO(hibHighmarkAOREropDomain.getSrno() == null ? "" : Integer.toString(hibHighmarkAOREropDomain.getSrno())); 
						chmAorERespType.setMEMBER_REFERENCE_NUMBER(hibHighmarkAOREropDomain.getMemberReferenceNumber() == null ? "" : hibHighmarkAOREropDomain.getMemberReferenceNumber()); 
						chmAorERespType.setSOA_SOURCE_NAME(hibHighmarkAOREropDomain.getSoaSourceName() == null ? "" : hibHighmarkAOREropDomain.getSoaSourceName()); 
						chmAorERespType.setDATE_OF_REQUEST(hibHighmarkAOREropDomain.getDateOfRequest() == null ? "" : sdf1.format(hibHighmarkAOREropDomain.getDateOfRequest())); 
						chmAorERespType.setPREPARED_FOR(hibHighmarkAOREropDomain.getPreparedFor() == null ? "" : hibHighmarkAOREropDomain.getPreparedFor()); 
						chmAorERespType.setPREPARED_FOR_ID(hibHighmarkAOREropDomain.getPreparedForId() == null ? "" : hibHighmarkAOREropDomain.getPreparedForId()); 
						chmAorERespType.setDATE_OF_ISSUE(hibHighmarkAOREropDomain.getDateOfIssue() == null ? "" : sdf1.format(hibHighmarkAOREropDomain.getDateOfIssue())); 
						chmAorERespType.setREPORT_ID(hibHighmarkAOREropDomain.getReportId() == null ? "" : hibHighmarkAOREropDomain.getReportId()); 
						chmAorERespType.setNAME(hibHighmarkAOREropDomain.getName() == null ? "" : hibHighmarkAOREropDomain.getName()); 
						chmAorERespType.setSPOUSE(hibHighmarkAOREropDomain.getSpouse() == null ? "" : hibHighmarkAOREropDomain.getSpouse()); 
						chmAorERespType.setFATHER(hibHighmarkAOREropDomain.getFather() == null ? "" : hibHighmarkAOREropDomain.getFather()); 
						chmAorERespType.setMOTHER(hibHighmarkAOREropDomain.getMother() == null ? "" : hibHighmarkAOREropDomain.getMother()); 
						chmAorERespType.setDOB(hibHighmarkAOREropDomain.getDob() == null ? "" : sdf1.format(hibHighmarkAOREropDomain.getDob())); 
						chmAorERespType.setAGE(hibHighmarkAOREropDomain.getAge() == null ? "" : Integer.toString(hibHighmarkAOREropDomain.getAge())); 
						chmAorERespType.setAGE_AS_ON(hibHighmarkAOREropDomain.getAgeAsOn() == null ? "" : sdf1.format(hibHighmarkAOREropDomain.getAgeAsOn())); 
						chmAorERespType.setGENDER(hibHighmarkAOREropDomain.getGender() == null ? "" : hibHighmarkAOREropDomain.getGender()); 
						chmAorERespType.setPHONE_1(hibHighmarkAOREropDomain.getPhone1() == null ? "" : hibHighmarkAOREropDomain.getPhone1().toString()); 
						chmAorERespType.setPHONE_2(hibHighmarkAOREropDomain.getPhone2() == null ? "" : hibHighmarkAOREropDomain.getPhone2().toString()); 
						chmAorERespType.setPHONE_3(hibHighmarkAOREropDomain.getPhone3() == null ? "" : hibHighmarkAOREropDomain.getPhone3().toString()); 
						chmAorERespType.setADDRESS_1(hibHighmarkAOREropDomain.getAddress1() == null ? "" : hibHighmarkAOREropDomain.getAddress1()); 
						chmAorERespType.setADDRESS_2(hibHighmarkAOREropDomain.getAddress2() == null ? "" : hibHighmarkAOREropDomain.getAddress2()); 
						chmAorERespType.setREL_TYP_1(hibHighmarkAOREropDomain.getRelTyp1() == null ? "" : hibHighmarkAOREropDomain.getRelTyp1()); 
						chmAorERespType.setREL_NM_1(hibHighmarkAOREropDomain.getRelNm1() == null ? "" : hibHighmarkAOREropDomain.getRelNm1()); 
						chmAorERespType.setREL_TYP_2(hibHighmarkAOREropDomain.getRelTyp2() == null ? "" : hibHighmarkAOREropDomain.getRelTyp2()); 
						chmAorERespType.setREL_NM_2(hibHighmarkAOREropDomain.getRelNm2() == null ? "" : hibHighmarkAOREropDomain.getRelNm2()); 
						chmAorERespType.setREL_TYP_3(hibHighmarkAOREropDomain.getRelTyp3() == null ? "" : hibHighmarkAOREropDomain.getRelTyp3()); 
						chmAorERespType.setREL_NM_3(hibHighmarkAOREropDomain.getRelNm3() == null ? "" : hibHighmarkAOREropDomain.getRelNm3()); 
						chmAorERespType.setREL_TYP_4(hibHighmarkAOREropDomain.getRelTyp4() == null ? "" : hibHighmarkAOREropDomain.getRelTyp4()); 
						chmAorERespType.setREL_NM_4(hibHighmarkAOREropDomain.getRelNm4() == null ? "" : hibHighmarkAOREropDomain.getRelNm4()); 
						chmAorERespType.setID_TYPE_1(hibHighmarkAOREropDomain.getIdType1() == null ? "" : hibHighmarkAOREropDomain.getIdType1()); 
						chmAorERespType.setID_VALUE_1(hibHighmarkAOREropDomain.getIdValue1() == null ? "" : hibHighmarkAOREropDomain.getIdValue1()); 
						chmAorERespType.setID_TYPE_2(hibHighmarkAOREropDomain.getIdType2() == null ? "" : hibHighmarkAOREropDomain.getIdType2()); 
						chmAorERespType.setID_VALUE_2(hibHighmarkAOREropDomain.getIdValue2() == null ? "" : hibHighmarkAOREropDomain.getIdValue2()); 
						chmAorERespType.setRATION_CARD(hibHighmarkAOREropDomain.getRationCard() == null ? "" : hibHighmarkAOREropDomain.getRationCard()); 
						chmAorERespType.setVOTERS_ID(hibHighmarkAOREropDomain.getVotersId() == null ? "" : hibHighmarkAOREropDomain.getVotersId()); 
						chmAorERespType.setDRIVING_LICENCE_NO(hibHighmarkAOREropDomain.getDrivingLicenceNo() == null ? "" : hibHighmarkAOREropDomain.getDrivingLicenceNo()); 
						chmAorERespType.setPAN(hibHighmarkAOREropDomain.getPan() == null ? "" : hibHighmarkAOREropDomain.getPan()); 
						chmAorERespType.setPASSPORT(hibHighmarkAOREropDomain.getPassport() == null ? "" : hibHighmarkAOREropDomain.getPassport()); 
						chmAorERespType.setOTHER_ID(hibHighmarkAOREropDomain.getOtherId() == null ? "" : hibHighmarkAOREropDomain.getOtherId()); 
						chmAorERespType.setBRANCH(hibHighmarkAOREropDomain.getBranch() == null ? "" : hibHighmarkAOREropDomain.getBranch()); 
						chmAorERespType.setKENDRA(hibHighmarkAOREropDomain.getKendra() == null ? "" : hibHighmarkAOREropDomain.getKendra()); 
						chmAorERespType.setMBR_ID(hibHighmarkAOREropDomain.getMbrId() == null ? "" : hibHighmarkAOREropDomain.getMbrId()); 
						chmAorERespType.setCREDT_INQ_PURPS_TYP(hibHighmarkAOREropDomain.getCredtPurpsTyp() == null ? "" : hibHighmarkAOREropDomain.getCredtPurpsTyp()); 
						chmAorERespType.setCREDT_INQ_PURPS_TYP_DESC(hibHighmarkAOREropDomain.getCredtInqPurpsTypDesc() == null ? "" : hibHighmarkAOREropDomain.getCredtInqPurpsTypDesc()); 
						chmAorERespType.setCREDIT_INQUIRY_STAGE(hibHighmarkAOREropDomain.getCreditInquiryStage() == null ? "" : hibHighmarkAOREropDomain.getCreditInquiryStage()); 
						chmAorERespType.setCREDT_RPT_ID(hibHighmarkAOREropDomain.getCredtRptId() == null ? "" : hibHighmarkAOREropDomain.getCredtRptId()); 
						chmAorERespType.setCREDT_REQ_TYP(hibHighmarkAOREropDomain.getCredtReqTyp() == null ? "" : hibHighmarkAOREropDomain.getCredtReqTyp()); 
						chmAorERespType.setCREDT_RPT_TRN_DT_TM(hibHighmarkAOREropDomain.getCredtRptTrnDtTm() == null ? "" : hibHighmarkAOREropDomain.getCredtRptTrnDtTm()); 
						chmAorERespType.setAC_OPEN_DT(hibHighmarkAOREropDomain.getAcOpenDt() == null ? "" : sdf1.format(hibHighmarkAOREropDomain.getAcOpenDt())); 
						chmAorERespType.setLOAN_AMOUNT(hibHighmarkAOREropDomain.getLoanAmount() == null ? "" : sdf1.format(hibHighmarkAOREropDomain.getLoanAmount())); 
						chmAorERespType.setENTITY_ID(hibHighmarkAOREropDomain.getEntityId() == null ? "" : hibHighmarkAOREropDomain.getEntityId()); 
						chmAorERespType.setSTATUS(hibHighmarkAOREropDomain.getStatus() == null ? "" : hibHighmarkAOREropDomain.getStatus()); 
						chmAorERespType.setERRORS(hibHighmarkAOREropDomain.getErrors() == null ? "" : hibHighmarkAOREropDomain.getErrors()); 
						chmAorERespType.setRESPONSE_TYPE(hibHighmarkAOREropDomain.getResponseType() == null ? "" : hibHighmarkAOREropDomain.getResponseType()); 
						chmAorERespType.setACK_CODE(hibHighmarkAOREropDomain.getAckCode() == null ? "" : hibHighmarkAOREropDomain.getAckCode()); 
						chmAorERespType.setACK_DESCRIPTION(hibHighmarkAOREropDomain.getAckDesc() == null ? "" : hibHighmarkAOREropDomain.getAckDesc()); 
						chmAorERespType.setOUTPUT_WRITE_FLAG(hibHighmarkAOREropDomain.getOutputWriteFlag() == null ? "" : hibHighmarkAOREropDomain.getOutputWriteFlag()); 
						chmAorERespType.setOUTPUT_WRITE_TIME(hibHighmarkAOREropDomain.getOutputWriteTime() == null ? "" : hibHighmarkAOREropDomain.getOutputWriteTime()); 
						chmAorERespType.setOUTPUT_READ_TIME(hibHighmarkAOREropDomain.getOutputReadTime() == null ? "" : hibHighmarkAOREropDomain.getOutputReadTime());
						
						chmAorERespTypList.add(chmAorERespType);
					}
					responseJSONType.setCHM_AOR_EROP_DOMAIN_LIST(chmAorERespTypList.toArray(new ChmAorERespType[chmAorERespTypList.size()]));
				}
			}
		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** chmAdvEropMapping >>");
		return responseJSONType;
	}

	public  ResponseJSONType chmAdvSropMapping(Object responseJsonObject,ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> chmAdvSropMapping **");
		try {
			CHMAdvSropDomainObject chmAdvSropDomainObject = (CHMAdvSropDomainObject) responseJsonObject;
			if (chmAdvSropDomainObject != null) {
				List<HibHighmarkAdOverlapSropDomain> hmAdvSropDomainList = chmAdvSropDomainObject.getHmAdvSropDomainList();
				if (hmAdvSropDomainList != null&& hmAdvSropDomainList.size() > 0) {
					List<ChmAorSRespType> chmAorSRespTypeList = new ArrayList<ChmAorSRespType>();
					
					for (HibHighmarkAdOverlapSropDomain hibHighmarkAdOverlapSropDomain : hmAdvSropDomainList) {

						ChmAorSRespType chmAorSRespType = new ChmAorSRespType();
						chmAorSRespType.setSRNO(hibHighmarkAdOverlapSropDomain.getSrno() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getSrno())); 
						chmAorSRespType.setMEMBER_REFERENCE_NUMBER(hibHighmarkAdOverlapSropDomain.getMemberReferenceNumber() == null ? "" : hibHighmarkAdOverlapSropDomain.getMemberReferenceNumber()); 
						chmAorSRespType.setSOA_SOURCE_NAME(hibHighmarkAdOverlapSropDomain.getSoaSourceName() == null ? "" : hibHighmarkAdOverlapSropDomain.getSoaSourceName()); 
						chmAorSRespType.setDATE_OF_REQUEST(hibHighmarkAdOverlapSropDomain.getDateOfRequest() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getDateOfRequest())); 
						chmAorSRespType.setPREPARED_FOR(hibHighmarkAdOverlapSropDomain.getPreparedFor() == null ? "" : hibHighmarkAdOverlapSropDomain.getPreparedFor()); 
						chmAorSRespType.setPREPARED_FOR_ID(hibHighmarkAdOverlapSropDomain.getPreparedForId() == null ? "" : hibHighmarkAdOverlapSropDomain.getPreparedForId()); 
						chmAorSRespType.setDATE_OF_ISSUE(hibHighmarkAdOverlapSropDomain.getDateOfIssue() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getDateOfIssue())); 
						chmAorSRespType.setREPORT_ID(hibHighmarkAdOverlapSropDomain.getReportId() == null ? "" : hibHighmarkAdOverlapSropDomain.getReportId()); 
						chmAorSRespType.setNAME_IQ(hibHighmarkAdOverlapSropDomain.getNameIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getNameIq()); 
						chmAorSRespType.setSPOUSE_IQ(hibHighmarkAdOverlapSropDomain.getSpouseIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getSpouseIq()); 
						chmAorSRespType.setFATHER_IQ(hibHighmarkAdOverlapSropDomain.getFatherIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getFatherIq()); 
						chmAorSRespType.setMOTHER_IQ(hibHighmarkAdOverlapSropDomain.getMotherIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getMotherIq()); 
						chmAorSRespType.setDOB_IQ(hibHighmarkAdOverlapSropDomain.getDobIq() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getDobIq())); 
						chmAorSRespType.setAGE_IQ(hibHighmarkAdOverlapSropDomain.getAgeIq() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getAgeIq())); 
						chmAorSRespType.setAGE_AS_ON_IQ(hibHighmarkAdOverlapSropDomain.getAgeAsOnIq() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getAgeAsOnIq())); 
						chmAorSRespType.setGENDER_IQ(hibHighmarkAdOverlapSropDomain.getGenderIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getGenderIq()); 
						chmAorSRespType.setPHONE_1_IQ(hibHighmarkAdOverlapSropDomain.getPhone1Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getPhone1Iq().toString()); 
						chmAorSRespType.setPHONE_2_IQ(hibHighmarkAdOverlapSropDomain.getPhone2Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getPhone2Iq().toString()); 
						chmAorSRespType.setPHONE_3_IQ(hibHighmarkAdOverlapSropDomain.getPhone3Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getPhone3Iq().toString()); 
						chmAorSRespType.setADDRESS_1_IQ(hibHighmarkAdOverlapSropDomain.getAddress1Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getAddress1Iq()); 
						chmAorSRespType.setADDRESS_2_IQ(hibHighmarkAdOverlapSropDomain.getAddress2Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getAddress2Iq()); 
						chmAorSRespType.setREL_TYP_1_IQ(hibHighmarkAdOverlapSropDomain.getRelTyp1Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp1Iq()); 
						chmAorSRespType.setREL_NM_1_IQ(hibHighmarkAdOverlapSropDomain.getRelNm1Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm1Iq()); 
						chmAorSRespType.setREL_TYP_2_IQ(hibHighmarkAdOverlapSropDomain.getRelTyp2Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp2Iq()); 
						chmAorSRespType.setREL_NM_2_IQ(hibHighmarkAdOverlapSropDomain.getRelNm2Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm2Iq()); 
						chmAorSRespType.setREL_TYP_3_IQ(hibHighmarkAdOverlapSropDomain.getRelTyp3Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp3Iq()); 
						chmAorSRespType.setREL_NM_3_IQ(hibHighmarkAdOverlapSropDomain.getRelNm3Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm3Iq()); 
						chmAorSRespType.setREL_TYP_4_IQ(hibHighmarkAdOverlapSropDomain.getRelTyp4Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp4Iq()); 
						chmAorSRespType.setREL_NM_4_IQ(hibHighmarkAdOverlapSropDomain.getRelNm4Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm4Iq()); 
						chmAorSRespType.setID_TYPE_1_IQ(hibHighmarkAdOverlapSropDomain.getIdType1Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdType1Iq()); 
						chmAorSRespType.setID_VALUE_1_IQ(hibHighmarkAdOverlapSropDomain.getIdValue1Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdValue1Iq()); 
						chmAorSRespType.setID_TYPE_2_IQ(hibHighmarkAdOverlapSropDomain.getIdType2Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdType2Iq()); 
						chmAorSRespType.setID_VALUE_2_IQ(hibHighmarkAdOverlapSropDomain.getIdValue2Iq() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdValue2Iq()); 
						chmAorSRespType.setRATION_CARD_IQ(hibHighmarkAdOverlapSropDomain.getRationCardIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getRationCardIq()); 
						chmAorSRespType.setVOTERS_ID_IQ(hibHighmarkAdOverlapSropDomain.getVotersIdIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getVotersIdIq()); 
						chmAorSRespType.setDRIVING_LICENCE_NO_IQ(hibHighmarkAdOverlapSropDomain.getDrivingLicenceNoIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getDrivingLicenceNoIq()); 
						chmAorSRespType.setPAN_IQ(hibHighmarkAdOverlapSropDomain.getPanIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getPanIq()); 
						chmAorSRespType.setPASSPORT_IQ(hibHighmarkAdOverlapSropDomain.getPassportIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getPassportIq()); 
						chmAorSRespType.setOTHER_ID_IQ(hibHighmarkAdOverlapSropDomain.getOtherIdIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getOtherIdIq()); 
						chmAorSRespType.setBRANCH_IQ(hibHighmarkAdOverlapSropDomain.getBranchIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getBranchIq()); 
						chmAorSRespType.setKENDRA_IQ(hibHighmarkAdOverlapSropDomain.getKendraIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getKendraIq()); 
						chmAorSRespType.setMBR_ID_IQ(hibHighmarkAdOverlapSropDomain.getMbrIdIq() == null ? "" : hibHighmarkAdOverlapSropDomain.getMbrIdIq()); 
						chmAorSRespType.setCREDT_INQ_PURPS_TYP(hibHighmarkAdOverlapSropDomain.getCredtIqPurpsTyp() == null ? "" : hibHighmarkAdOverlapSropDomain.getCredtIqPurpsTyp()); 
						chmAorSRespType.setCREDT_INQ_PURPS_TYP_DESC(hibHighmarkAdOverlapSropDomain.getCredtInqPurpsTypDesc() == null ? "" : hibHighmarkAdOverlapSropDomain.getCredtInqPurpsTypDesc()); 
						chmAorSRespType.setCREDIT_INQUIRY_STAGE(hibHighmarkAdOverlapSropDomain.getCreditInquiryStage() == null ? "" : hibHighmarkAdOverlapSropDomain.getCreditInquiryStage()); 
						chmAorSRespType.setCREDT_RPT_ID(hibHighmarkAdOverlapSropDomain.getCredtRptId() == null ? "" : hibHighmarkAdOverlapSropDomain.getCredtRptId()); 
						chmAorSRespType.setCREDT_REQ_TYP(hibHighmarkAdOverlapSropDomain.getCredtReqTyp() == null ? "" : hibHighmarkAdOverlapSropDomain.getCredtReqTyp()); 
						chmAorSRespType.setCREDT_RPT_TRN_DT_TM(hibHighmarkAdOverlapSropDomain.getCredtRptTrnDtTm() == null ? "" : hibHighmarkAdOverlapSropDomain.getCredtRptTrnDtTm()); 
						chmAorSRespType.setAC_OPEN_DT(hibHighmarkAdOverlapSropDomain.getAcOpenDt() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getAcOpenDt())); 
						chmAorSRespType.setLOAN_AMOUNT(hibHighmarkAdOverlapSropDomain.getLoanAmount() == null ? "" : hibHighmarkAdOverlapSropDomain.getLoanAmount().toString()); 
						chmAorSRespType.setENTITY_ID(hibHighmarkAdOverlapSropDomain.getEntityId() == null ? "" : hibHighmarkAdOverlapSropDomain.getEntityId()); 
						chmAorSRespType.setIND_STATUS_PR(hibHighmarkAdOverlapSropDomain.getStatusIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getStatusIndPr()); 
						chmAorSRespType.setIND_NO_OF_DEFAULT_ACCOUNTS_PR(hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsIndPr().toString()); 
						chmAorSRespType.setIND_TOTAL_RESPONSES_PR(hibHighmarkAdOverlapSropDomain.getTotalResponsesIndIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalResponsesIndIndPr().toString()); 
						chmAorSRespType.setIND_NO_OF_CLOSED_ACCOUNTS_PR(hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsIndPr().toString()); 
						chmAorSRespType.setIND_NO_OF_ACTIVE_ACCOUNTS_PR(hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsIndPr().toString()); 
						chmAorSRespType.setIND_NO_OF_OTHER_MFIS_PR(hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisIndPr().toString()); 
						chmAorSRespType.setIND_OWN_MFI_INDECATOR_PR(hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorIndPr()); 
						chmAorSRespType.setIND_TTL_OWN_DISBURSD_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountIndPr().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_DISBURSD_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountIndPr().toString()); 
						chmAorSRespType.setIND_TTL_OWN_CURRENT_BAL_PR(hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceIndPr().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_CURRENT_BAL_PR(hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceIndPr().toString()); 
						chmAorSRespType.setIND_TTL_OWN_INSTLMNT_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountIndPr().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_INSTLMNT_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountIndPr().toString()); 
						chmAorSRespType.setIND_MAX_WORST_DELEQUENCY_PR(hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyIndPr().toString()); 
						chmAorSRespType.setIND_ERRORS_PR(hibHighmarkAdOverlapSropDomain.getErrorsIndPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getErrorsIndPr()); 
						chmAorSRespType.setIND_STATUS_SE(hibHighmarkAdOverlapSropDomain.getStatusIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getStatusIndSe()); 
						chmAorSRespType.setIND_NO_OF_DEFAULT_ACCOUNTS_SE(hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsIndSe().toString()); 
						chmAorSRespType.setIND_TOTAL_RESPONSES_SE(hibHighmarkAdOverlapSropDomain.getTotalResponsesIndIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalResponsesIndIndSe().toString()); 
						chmAorSRespType.setIND_NO_OF_CLOSED_ACCOUNTS_SE(hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsIndSe().toString()); 
						chmAorSRespType.setIND_NO_OF_ACTIVE_ACCOUNTS_SE(hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsIndSe().toString()); 
						chmAorSRespType.setIND_NO_OF_OTHER_MFIS_SE(hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisIndSe().toString()); 
						chmAorSRespType.setIND_OWN_MFI_INDECATOR_SE(hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorIndSe()); 
						chmAorSRespType.setIND_TTL_OWN_DISBURSD_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountIndSe().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_DISBURSD_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountIndSe().toString()); 
						chmAorSRespType.setIND_TTL_OWN_CURRENT_BAL_SE(hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceIndSe().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_CURRENT_BAL_SE(hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceIndSe().toString()); 
						chmAorSRespType.setIND_TTL_OWN_INSTLMNT_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountIndSe().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_INSTLMNT_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountIndSe().toString()); 
						chmAorSRespType.setIND_MAX_WORST_DELEQUENCY_SE(hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyIndSe().toString()); 
						chmAorSRespType.setIND_ERRORS_SE(hibHighmarkAdOverlapSropDomain.getErrorsIndSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getErrorsIndSe()); 
						chmAorSRespType.setIND_STATUS_SM(hibHighmarkAdOverlapSropDomain.getStatusInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getStatusInd()); 
						chmAorSRespType.setIND_NO_OF_DEFAULT_ACCOUNTS_SM(hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsInd().toString()); 
						chmAorSRespType.setIND_TOTAL_RESPONSES_SM(hibHighmarkAdOverlapSropDomain.getTotalResponsesInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalResponsesInd().toString()); 
						chmAorSRespType.setIND_NO_OF_CLOSED_ACCOUNTS_SM(hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsInd().toString()); 
						chmAorSRespType.setIND_NO_OF_ACTIVE_ACCOUNTS_SM(hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsInd().toString()); 
						chmAorSRespType.setIND_NO_OF_OTHER_MFIS_SM(hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisInd().toString()); 
						chmAorSRespType.setIND_OWN_MFI_INDECATOR_SM(hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorInd()); 
						chmAorSRespType.setIND_TTL_OWN_DISBURSD_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountInd().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_DISBURSD_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountInd().toString()); 
						chmAorSRespType.setIND_TTL_OWN_CURRENT_BAL_SM(hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceInd().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_CURRENT_BAL_SM(hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceInd().toString()); 
						chmAorSRespType.setIND_TTL_OWN_INSTLMNT_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountInd().toString()); 
						chmAorSRespType.setIND_TTL_OTHER_INSTLMNT_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountInd().toString()); 
						chmAorSRespType.setIND_MAX_WORST_DELEQUENCY_SM(hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyInd().toString()); 
						chmAorSRespType.setIND_ERRORS_SM(hibHighmarkAdOverlapSropDomain.getErrorsInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getErrorsInd()); 
						chmAorSRespType.setIND_MATCHED_TYPE(hibHighmarkAdOverlapSropDomain.getMatchedTypeInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getMatchedTypeInd()); 
						chmAorSRespType.setIND_MFI(hibHighmarkAdOverlapSropDomain.getMfiInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getMfiInd()); 
						chmAorSRespType.setIND_MFI_ID(hibHighmarkAdOverlapSropDomain.getMfiIdInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getMfiIdInd()); 
						chmAorSRespType.setIND_BRANCH(hibHighmarkAdOverlapSropDomain.getBranchInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getBranchInd()); 
						chmAorSRespType.setIND_KENDRA(hibHighmarkAdOverlapSropDomain.getKendraInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getKendraInd()); 
						chmAorSRespType.setIND_NAME(hibHighmarkAdOverlapSropDomain.getNameInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getNameInd()); 
						chmAorSRespType.setIND_SPOUSE(hibHighmarkAdOverlapSropDomain.getSpouseInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getSpouseInd()); 
						chmAorSRespType.setIND_FATHER(hibHighmarkAdOverlapSropDomain.getFatherInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getFatherInd()); 
						chmAorSRespType.setIND_CNSMR_MBR_ID(hibHighmarkAdOverlapSropDomain.getCnsmrMbrIdInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getCnsmrMbrIdInd()); 
						chmAorSRespType.setIND_DOB(hibHighmarkAdOverlapSropDomain.getDobInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getDobInd())); 
						chmAorSRespType.setIND_AGE(hibHighmarkAdOverlapSropDomain.getAgeInd() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getAgeInd())); 
						chmAorSRespType.setIND_AGE_AS_ON(hibHighmarkAdOverlapSropDomain.getAgeAsOnInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getAgeAsOnInd())); 
						chmAorSRespType.setIND_PHONE(hibHighmarkAdOverlapSropDomain.getPhoneInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getPhoneInd()); 
						chmAorSRespType.setIND_ADDRESS_1(hibHighmarkAdOverlapSropDomain.getAddress1Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getAddress1Ind()); 
						chmAorSRespType.setIND_ADDRESS_2(hibHighmarkAdOverlapSropDomain.getAddress2Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getAddress2Ind()); 
						chmAorSRespType.setIND_REL_TYP_1(hibHighmarkAdOverlapSropDomain.getRelTyp1Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp1Ind()); 
						chmAorSRespType.setIND_REL_NM_1(hibHighmarkAdOverlapSropDomain.getRelNm1Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm1Ind()); 
						chmAorSRespType.setIND_REL_TYP_2(hibHighmarkAdOverlapSropDomain.getRelTyp2Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp2Ind()); 
						chmAorSRespType.setIND_REL_NM_2(hibHighmarkAdOverlapSropDomain.getRelNm2Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm2Ind()); 
						chmAorSRespType.setIND_ID_TYP_1(hibHighmarkAdOverlapSropDomain.getIdTyp1Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdTyp1Ind()); 
						chmAorSRespType.setIND_ID_VALUE_1(hibHighmarkAdOverlapSropDomain.getIdValue1Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdValue1Ind()); 
						chmAorSRespType.setIND_ID_TYP_2(hibHighmarkAdOverlapSropDomain.getIdTyp2Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdTyp2Ind()); 
						chmAorSRespType.setIND_ID_VALUE_2(hibHighmarkAdOverlapSropDomain.getIdValue2Ind() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdValue2Ind()); 
						chmAorSRespType.setIND_GROUP_ID(hibHighmarkAdOverlapSropDomain.getGroupIdInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getGroupIdInd()); 
						chmAorSRespType.setIND_GROUP_CREATION_DATE(hibHighmarkAdOverlapSropDomain.getGroupCreationDateInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getGroupCreationDateInd())); 
						chmAorSRespType.setIND_INSERT_DATE(hibHighmarkAdOverlapSropDomain.getInsertDateInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getInsertDateInd())); 
						chmAorSRespType.setIND_ACCT_TYPE(hibHighmarkAdOverlapSropDomain.getAcctTypeInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getAcctTypeInd()); 
						chmAorSRespType.setIND_FREQ(hibHighmarkAdOverlapSropDomain.getFreqInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getFreqInd()); 
						chmAorSRespType.setIND_STATUS(hibHighmarkAdOverlapSropDomain.getStatusInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getStatusInd()); 
						chmAorSRespType.setIND_ACCT_NUMBER(hibHighmarkAdOverlapSropDomain.getAcctNumberInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getAcctNumberInd()); 
						chmAorSRespType.setIND_DISBURSED_AMT(hibHighmarkAdOverlapSropDomain.getDisbursedAmtInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getDisbursedAmtInd()); 
						chmAorSRespType.setIND_CURRENT_BAL(hibHighmarkAdOverlapSropDomain.getCurrentBalInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getCurrentBalInd()); 
						chmAorSRespType.setIND_INSTALLMENT_AMT(hibHighmarkAdOverlapSropDomain.getInstallmentAmtInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getInstallmentAmtInd()); 
						chmAorSRespType.setIND_OVERDUE_AMT(hibHighmarkAdOverlapSropDomain.getOverdueAmtInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getOverdueAmtInd()); 
						chmAorSRespType.setIND_WRITE_OFF_AMT(hibHighmarkAdOverlapSropDomain.getWriteOffAmtInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getWriteOffAmtInd()); 
						chmAorSRespType.setIND_DISBURSED_DT(hibHighmarkAdOverlapSropDomain.getDisbursedDtInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getDisbursedDtInd())); 
						chmAorSRespType.setIND_CLOSED_DT(hibHighmarkAdOverlapSropDomain.getClosedDtInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getClosedDtInd())); 
						chmAorSRespType.setIND_RECENT_DELINQ_DT(hibHighmarkAdOverlapSropDomain.getRecentDelinqDtInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getRecentDelinqDtInd())); 
						chmAorSRespType.setIND_DPD(hibHighmarkAdOverlapSropDomain.getDpdInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getDpdInd().toString()); 
						chmAorSRespType.setIND_INQ_CNT(hibHighmarkAdOverlapSropDomain.getInqCntInd() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getInqCntInd())); 
						chmAorSRespType.setIND_INFO_AS_ON(hibHighmarkAdOverlapSropDomain.getInfoAsOnInd() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getInfoAsOnInd())); 
						chmAorSRespType.setIND_WORST_DELEQUENCY_AMOUNT(hibHighmarkAdOverlapSropDomain.getWorstDelequencyAmountInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getWorstDelequencyAmountInd().toString()); 
						chmAorSRespType.setIND_PAYMENT_HISTORY(hibHighmarkAdOverlapSropDomain.getPaymentHistoryInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getPaymentHistoryInd()); 
						chmAorSRespType.setIND_IS_ACTIVE_BORROWER(hibHighmarkAdOverlapSropDomain.getIsActiveBorrowerInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getIsActiveBorrowerInd()); 
						chmAorSRespType.setIND_NO_OF_BORROWERS(hibHighmarkAdOverlapSropDomain.getNoOfBorrowersInd() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getNoOfBorrowersInd())); 
						chmAorSRespType.setIND_COMMENT_RES(hibHighmarkAdOverlapSropDomain.getCommentResInd() == null ? "" : hibHighmarkAdOverlapSropDomain.getCommentResInd()); 
						chmAorSRespType.setGRP_STATUS_PR(hibHighmarkAdOverlapSropDomain.getStatusGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getStatusGrpPr()); 
						chmAorSRespType.setGRP_NO_OF_DEFAULT_ACCOUNTS_PR(hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsGrpPr().toString()); 
						chmAorSRespType.setGRP_TOTAL_RESPONSES_PR(hibHighmarkAdOverlapSropDomain.getTotalResponsesIndGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalResponsesIndGrpPr().toString()); 
						chmAorSRespType.setGRP_NO_OF_CLOSED_ACCOUNTS_PR(hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsGrpPr().toString()); 
						chmAorSRespType.setGRP_NO_OF_ACTIVE_ACCOUNTS_PR(hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsGrpPr().toString()); 
						chmAorSRespType.setGRP_NO_OF_OTHER_MFIS_PR(hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisGrpPr().toString()); 
						chmAorSRespType.setGRP_OWN_MFI_INDECATOR_PR(hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorGrpPr()); 
						chmAorSRespType.setGRP_TTL_OWN_DISBURSD_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountGrpPr().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_DISBURSD_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountGrpPr().toString()); 
						chmAorSRespType.setGRP_TTL_OWN_CURRENT_BAL_PR(hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceGrpPr().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_CURRENT_BAL_PR(hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceGrpPr().toString()); 
						chmAorSRespType.setGRP_TTL_OWN_INSTLMNT_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountGrpPr().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_INSTLMNT_AMT_PR(hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountGrpSe().toString()); 
						chmAorSRespType.setGRP_MAX_WORST_DELEQUENCY_PR(hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyGrpPr().toString()); 
						chmAorSRespType.setGRP_ERRORS_PR(hibHighmarkAdOverlapSropDomain.getErrorsGrpPr() == null ? "" : hibHighmarkAdOverlapSropDomain.getErrorsGrpPr()); 
						chmAorSRespType.setGRP_STATUS_SE(hibHighmarkAdOverlapSropDomain.getStatusGrpSe()==null?"":hibHighmarkAdOverlapSropDomain.getStatusGrpSe()); 
						chmAorSRespType.setGRP_NO_OF_DEFAULT_ACCOUNTS_SE(hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsGrpSe().toString()); 
						chmAorSRespType.setGRP_TOTAL_RESPONSES_SE(hibHighmarkAdOverlapSropDomain.getTotalResponsesIndGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalResponsesIndGrpSe().toString()); 
						chmAorSRespType.setGRP_NO_OF_CLOSED_ACCOUNTS_SE(hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsGrpSe().toString()); 
						chmAorSRespType.setGRP_NO_OF_ACTIVE_ACCOUNTS_SE(hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsGrpSe().toString()); 
						chmAorSRespType.setGRP_NO_OF_OTHER_MFIS_SE(hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisGrpSe().toString()); 
						chmAorSRespType.setGRP_OWN_MFI_INDECATOR_SE(hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorGrpSe()); 
						chmAorSRespType.setGRP_TTL_OWN_DISBURSD_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountGrpSe().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_DISBURSD_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountGrpSe().toString()); 
						chmAorSRespType.setGRP_TTL_OWN_CURRENT_BAL_SE(hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceGrpSe().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_CURRENT_BAL_SE(hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceGrpSe().toString()); 
						chmAorSRespType.setGRP_TTL_OWN_INSTLMNT_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountGrpSe().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_INSTLMNT_AMT_SE(hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountGrpSe().toString()); 
						chmAorSRespType.setGRP_MAX_WORST_DELEQUENCY_SE(hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyGrpSe().toString()); 
						chmAorSRespType.setGRP_ERRORS_SE(hibHighmarkAdOverlapSropDomain.getErrorsGrpSe() == null ? "" : hibHighmarkAdOverlapSropDomain.getErrorsGrpSe()); 
						chmAorSRespType.setGRP_STATUS_SM(hibHighmarkAdOverlapSropDomain.getStatusGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getStatusGrp()); 
						chmAorSRespType.setGRP_NO_OF_DEFAULT_ACCOUNTS_SM(hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfDefaultAccountsGrp().toString()); 
						chmAorSRespType.setGRP_TOTAL_RESPONSES_SM(hibHighmarkAdOverlapSropDomain.getTotalResponsesGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalResponsesGrp().toString()); 
						chmAorSRespType.setGRP_NO_OF_CLOSED_ACCOUNTS_SM(hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfClosedAccountsGrp().toString()); 
						chmAorSRespType.setGRP_NO_OF_ACTIVE_ACCOUNTS_SM(hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfActiveAccountsGrp().toString()); 
						chmAorSRespType.setGRP_NO_OF_OTHER_MFIS_SM(hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getNoOfOtherMfisGrp().toString()); 
						chmAorSRespType.setGRP_OWN_MFI_INDECATOR_SM(hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getOwnMfiIndecatorGrp()); 
						chmAorSRespType.setGRP_TTL_OWN_DISBURSD_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnDisbursedAmountGrp().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_DISBURSD_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherDisbursedAmountGrp().toString()); 
						chmAorSRespType.setGRP_TTL_OWN_CURRENT_BAL_SM(hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnCurrentBalanceGrp().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_CURRENT_BAL_SM(hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherCurrentBalanceGrp().toString()); 
						chmAorSRespType.setGRP_TTL_OWN_INSTLMNT_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOwnInstallmentAmountGrp().toString()); 
						chmAorSRespType.setGRP_TTL_OTHER_INSTLMNT_AMT_SM(hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getTotalOtherInstallmentAmountGrp().toString()); 
						chmAorSRespType.setGRP_MAX_WORST_DELEQUENCY_SM(hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getMaxWorstDelequencyGrp().toString()); 
						chmAorSRespType.setGRP_ERRORS_SM(hibHighmarkAdOverlapSropDomain.getErrorsGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getErrorsGrp()); 
						chmAorSRespType.setGRP_MATCHED_TYPE(hibHighmarkAdOverlapSropDomain.getMatchedTypeGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getMatchedTypeGrp()); 
						chmAorSRespType.setGRP_MFI(hibHighmarkAdOverlapSropDomain.getMfiGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getMfiGrp()); 
						chmAorSRespType.setGRP_MFI_ID(hibHighmarkAdOverlapSropDomain.getMfiIdGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getMfiIdGrp()); 
						chmAorSRespType.setGRP_BRANCH(hibHighmarkAdOverlapSropDomain.getBranchGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getBranchGrp()); 
						chmAorSRespType.setGRP_KENDRA(hibHighmarkAdOverlapSropDomain.getKendraGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getKendraGrp()); 
						chmAorSRespType.setGRP_NAME(hibHighmarkAdOverlapSropDomain.getNameGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getNameGrp()); 
						chmAorSRespType.setGRP_SPOUSE(hibHighmarkAdOverlapSropDomain.getSpouseGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getSpouseGrp()); 
						chmAorSRespType.setGRP_FATHER(hibHighmarkAdOverlapSropDomain.getFatherGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getFatherGrp()); 
						chmAorSRespType.setGRP_CNSMR_MBR_ID(hibHighmarkAdOverlapSropDomain.getCnsmrMbrIdGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getCnsmrMbrIdGrp()); 
						chmAorSRespType.setGRP_DOB(hibHighmarkAdOverlapSropDomain.getDobGrp() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getDobGrp())); 
						chmAorSRespType.setGRP_AGE(hibHighmarkAdOverlapSropDomain.getAgeGrp() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getAgeGrp())); 
						chmAorSRespType.setGRP_AGE_AS_ON(hibHighmarkAdOverlapSropDomain.getAgeAsOnGrp() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getAgeAsOnGrp())); 
						chmAorSRespType.setGRP_PHONE(hibHighmarkAdOverlapSropDomain.getPhoneGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getPhoneGrp()); 
						chmAorSRespType.setGRP_ADDRESS_1(hibHighmarkAdOverlapSropDomain.getAddress1Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getAddress1Grp()); 
						chmAorSRespType.setGRP_ADDRESS_2(hibHighmarkAdOverlapSropDomain.getAddress2Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getAddress2Grp()); 
						chmAorSRespType.setGRP_REL_TYP_1(hibHighmarkAdOverlapSropDomain.getRelTyp1Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp1Grp()); 
						chmAorSRespType.setGRP_REL_NM_1(hibHighmarkAdOverlapSropDomain.getRelNm1Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm1Grp()); 
						chmAorSRespType.setGRP_REL_TYP_2(hibHighmarkAdOverlapSropDomain.getRelTyp2Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelTyp2Grp()); 
						chmAorSRespType.setGRP_REL_NM_2(hibHighmarkAdOverlapSropDomain.getRelNm2Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getRelNm2Grp()); 
						chmAorSRespType.setGRP_ID_TYP_1(hibHighmarkAdOverlapSropDomain.getIdTyp1Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdTyp1Grp()); 
						chmAorSRespType.setGRP_ID_VALUE_1(hibHighmarkAdOverlapSropDomain.getIdValue1Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdValue1Grp()); 
						chmAorSRespType.setGRP_ID_TYP_2(hibHighmarkAdOverlapSropDomain.getIdTyp2Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdTyp2Grp()); 
						chmAorSRespType.setGRP_ID_VALUE_2(hibHighmarkAdOverlapSropDomain.getIdValue2Grp() == null ? "" : hibHighmarkAdOverlapSropDomain.getIdValue2Grp()); 
						chmAorSRespType.setGRP_GROUP_ID(hibHighmarkAdOverlapSropDomain.getGroupIdGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getGroupIdGrp()); 
						chmAorSRespType.setGRP_GROUP_CREATION_DATE(hibHighmarkAdOverlapSropDomain.getGroupCreationDateGrp() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getGroupCreationDateGrp())); 
						chmAorSRespType.setGRP_INSERT_DATE(hibHighmarkAdOverlapSropDomain.getInsertDateGrp() == null ? "" :sdf1.format(hibHighmarkAdOverlapSropDomain.getInsertDateGrp())); 
						chmAorSRespType.setGRP_ACCT_TYPE(hibHighmarkAdOverlapSropDomain.getAcctTypeGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getAcctTypeGrp()); 
						chmAorSRespType.setGRP_FREQ(hibHighmarkAdOverlapSropDomain.getFreqGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getFreqGrp()); 
						chmAorSRespType.setGRP_STATUS(hibHighmarkAdOverlapSropDomain.getStatusAccountGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getStatusAccountGrp()); 
						chmAorSRespType.setGRP_ACCT_NUMBER(hibHighmarkAdOverlapSropDomain.getAcctNumberGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getAcctNumberGrp()); 
						chmAorSRespType.setGRP_DISBURSED_AMT(hibHighmarkAdOverlapSropDomain.getDisbursedAmtGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getDisbursedAmtGrp()); 
						chmAorSRespType.setGRP_CURRENT_BAL(hibHighmarkAdOverlapSropDomain.getCurrentBalGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getCurrentBalGrp()); 
						chmAorSRespType.setGRP_INSTALLMENT_AMT(hibHighmarkAdOverlapSropDomain.getInstallmentAmtGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getInstallmentAmtGrp()); 
						chmAorSRespType.setGRP_OVERDUE_AMT(hibHighmarkAdOverlapSropDomain.getOverdueAmtGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getOverdueAmtGrp()); 
						chmAorSRespType.setGRP_WRITE_OFF_AMT(hibHighmarkAdOverlapSropDomain.getWriteOffAmtGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getWriteOffAmtGrp()); 
						chmAorSRespType.setGRP_DISBURSED_DT(hibHighmarkAdOverlapSropDomain.getDisbursedDtGrp() == null ? "" :sdf1.format(hibHighmarkAdOverlapSropDomain.getDisbursedDtGrp())); 
						chmAorSRespType.setGRP_CLOSED_DT(hibHighmarkAdOverlapSropDomain.getClosedDtGrp() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getClosedDtGrp())); 
						chmAorSRespType.setGRP_RECENT_DELINQ_DT(hibHighmarkAdOverlapSropDomain.getRecentDelinqDtGrp() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getRecentDelinqDtGrp())); 
						chmAorSRespType.setGRP_DPD(hibHighmarkAdOverlapSropDomain.getDpdGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getDpdGrp().toString()); 
						chmAorSRespType.setGRP_INQ_CNT(hibHighmarkAdOverlapSropDomain.getInqCntGrp() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getInqCntGrp())); 
						chmAorSRespType.setGRP_INFO_AS_ON(hibHighmarkAdOverlapSropDomain.getInfoAsOnGrp() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getInfoAsOnGrp())); 
						chmAorSRespType.setGRP_WORST_DELEQUENCY_AMOUNT(hibHighmarkAdOverlapSropDomain.getWorstDelequencyAmountGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getWorstDelequencyAmountGrp().toString()); 
						chmAorSRespType.setGRP_PAYMENT_HISTORY(hibHighmarkAdOverlapSropDomain.getPaymentHistoryGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getPaymentHistoryGrp()); 
						chmAorSRespType.setGRP_IS_ACTIVE_BORROWER(hibHighmarkAdOverlapSropDomain.getIsActiveBorrowerGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getIsActiveBorrowerGrp()); 
						chmAorSRespType.setGRP_NO_OF_BORROWERS(hibHighmarkAdOverlapSropDomain.getNoOfBorrowersGrp() == null ? "" : Integer.toString(hibHighmarkAdOverlapSropDomain.getNoOfBorrowersGrp())); 
						chmAorSRespType.setGRP_COMMENT_RES(hibHighmarkAdOverlapSropDomain.getCommentResGrp() == null ? "" : hibHighmarkAdOverlapSropDomain.getCommentResGrp()); 
						chmAorSRespType.setMEMBER_NAME(hibHighmarkAdOverlapSropDomain.getMemberName() == null ? "" : hibHighmarkAdOverlapSropDomain.getMemberName()); 
						chmAorSRespType.setINQUIRY_DATE(hibHighmarkAdOverlapSropDomain.getInquiryDate() == null ? "" : sdf1.format(hibHighmarkAdOverlapSropDomain.getInquiryDate())); 
						chmAorSRespType.setPURPOSE(hibHighmarkAdOverlapSropDomain.getPurpose() == null ? "" : hibHighmarkAdOverlapSropDomain.getPurpose()); 
						chmAorSRespType.setAMOUNT(hibHighmarkAdOverlapSropDomain.getAmount() == null ? "" : hibHighmarkAdOverlapSropDomain.getAmount().toString()); 
						chmAorSRespType.setIND_REMARK(hibHighmarkAdOverlapSropDomain.getRemark() == null ? "" : hibHighmarkAdOverlapSropDomain.getRemark()); 
						chmAorSRespType.setOUTPUT_WRITE_FLAG(hibHighmarkAdOverlapSropDomain.getOutputWriteFlag() == null ? "" : hibHighmarkAdOverlapSropDomain.getOutputWriteFlag()); 
						chmAorSRespType.setOUTPUT_WRITE_TIME(hibHighmarkAdOverlapSropDomain.getOutputWriteTime() == null ? "" : hibHighmarkAdOverlapSropDomain.getOutputWriteTime()); 
						chmAorSRespType.setOUTPUT_READ_TIME(hibHighmarkAdOverlapSropDomain.getOutputReadTime() == null ? "" : hibHighmarkAdOverlapSropDomain.getOutputReadTime());
						
						chmAorSRespTypeList.add(chmAorSRespType);
					}
					responseJSONType.setCHM_AOR_SROP_DOMAIN_LIST(chmAorSRespTypeList.toArray(new ChmAorSRespType[chmAorSRespTypeList.size()]));

				}
			}
		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** chmAdvSropMapping >>");
		return responseJSONType;
	}

	public ResponseJSONType chmBaseEropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> chmBaseEropMapping ");
		try {
			CHMBaseEropDomainObject chmBaseEropDomainObject = (CHMBaseEropDomainObject) responseJsonObject;
			if (chmBaseEropDomainObject != null) {
				List<HibHighmarkBaseEropDomain> hmBaseEropDomainList = chmBaseEropDomainObject.getHmBaseEropDomainList();
				if (hmBaseEropDomainList != null&& hmBaseEropDomainList.size() > 0) {
					List<ChmbaseERespType> chmbaseERespTypeList = new ArrayList<ChmbaseERespType>();
					
					for (HibHighmarkBaseEropDomain hibHighmarkBaseEropDomain : hmBaseEropDomainList) {

						ChmbaseERespType chmbaseERespType = new ChmbaseERespType();
						chmbaseERespType.setSRNO(hibHighmarkBaseEropDomain.getSrNo() == null ? "" : Integer.toString(hibHighmarkBaseEropDomain.getSrNo())); 
						chmbaseERespType.setMEMBER_REFERENCE_NUMBER(hibHighmarkBaseEropDomain.getMemberReferenceNumber() == null ? "" : hibHighmarkBaseEropDomain.getMemberReferenceNumber()); 
						chmbaseERespType.setSOA_SOURCE_NAME(hibHighmarkBaseEropDomain.getSoaSourceName() == null ? "" : hibHighmarkBaseEropDomain.getSoaSourceName()); 
						chmbaseERespType.setDATE_OF_REQUEST(hibHighmarkBaseEropDomain.getDateOfRequest() == null ? "" : sdf1.format(hibHighmarkBaseEropDomain.getDateOfRequest())); 
						chmbaseERespType.setPREPARED_FOR(hibHighmarkBaseEropDomain.getPreparedFor() == null ? "" : hibHighmarkBaseEropDomain.getPreparedFor()); 
						chmbaseERespType.setPREPARED_FOR_ID(hibHighmarkBaseEropDomain.getPreparedForId() == null ? "" : hibHighmarkBaseEropDomain.getPreparedForId()); 
						chmbaseERespType.setDATE_OF_ISSUE(hibHighmarkBaseEropDomain.getDateOfIssue() == null ? "" : sdf1.format(hibHighmarkBaseEropDomain.getDateOfIssue())); 
						chmbaseERespType.setREPORT_ID(hibHighmarkBaseEropDomain.getReportId() == null ? "" : hibHighmarkBaseEropDomain.getReportId()); 
						chmbaseERespType.setBATCH_ID(hibHighmarkBaseEropDomain.getBatchId() == null ? "" : hibHighmarkBaseEropDomain.getBatchId().toString()); 
						chmbaseERespType.setSTATUS(hibHighmarkBaseEropDomain.getStatus() == null ? "" : hibHighmarkBaseEropDomain.getStatus()); 
						chmbaseERespType.setERROR(hibHighmarkBaseEropDomain.getError() == null ? "" : hibHighmarkBaseEropDomain.getError()); 
						chmbaseERespType.setNAME(hibHighmarkBaseEropDomain.getName() == null ? "" : hibHighmarkBaseEropDomain.getName()); 
						chmbaseERespType.setAKA(hibHighmarkBaseEropDomain.getAka() == null ? "" : hibHighmarkBaseEropDomain.getAka()); 
						chmbaseERespType.setSPOUSE(hibHighmarkBaseEropDomain.getSpouse() == null ? "" : hibHighmarkBaseEropDomain.getSpouse()); 
						chmbaseERespType.setFATHER(hibHighmarkBaseEropDomain.getFather() == null ? "" : hibHighmarkBaseEropDomain.getFather()); 
						chmbaseERespType.setMOTHER(hibHighmarkBaseEropDomain.getMother() == null ? "" : hibHighmarkBaseEropDomain.getMother()); 
						chmbaseERespType.setDOB(hibHighmarkBaseEropDomain.getDobIQ() == null ? "" : sdf1.format(hibHighmarkBaseEropDomain.getDobIQ())); 
						chmbaseERespType.setAGE(hibHighmarkBaseEropDomain.getAge() == null ? "" : Integer.toString(hibHighmarkBaseEropDomain.getAge())); 
						chmbaseERespType.setAGE_AS_ON(hibHighmarkBaseEropDomain.getAgeAsOn() == null ? "" : hibHighmarkBaseEropDomain.getAgeAsOn()); 
						chmbaseERespType.setRATION_CARD(hibHighmarkBaseEropDomain.getRationCard() == null ? "" : hibHighmarkBaseEropDomain.getRationCard()); 
						chmbaseERespType.setPASSPORT(hibHighmarkBaseEropDomain.getPassport() == null ? "" : hibHighmarkBaseEropDomain.getPassport()); 
						chmbaseERespType.setVOTER_ID(hibHighmarkBaseEropDomain.getVotersId() == null ? "" : hibHighmarkBaseEropDomain.getVotersId()); 
						chmbaseERespType.setDRIVING_LICENSE_NO(hibHighmarkBaseEropDomain.getDrivingLicenceNo() == null ? "" : hibHighmarkBaseEropDomain.getDrivingLicenceNo()); 
						chmbaseERespType.setPAN(hibHighmarkBaseEropDomain.getPan() == null ? "" : hibHighmarkBaseEropDomain.getPan()); 
						chmbaseERespType.setGENDER(hibHighmarkBaseEropDomain.getGender() == null ? "" : hibHighmarkBaseEropDomain.getGender()); 
						chmbaseERespType.setOWNERSHIP(hibHighmarkBaseEropDomain.getOwnership() == null ? "" : hibHighmarkBaseEropDomain.getOwnership()); 
						chmbaseERespType.setADDRESS_1(hibHighmarkBaseEropDomain.getAddress1() == null ? "" : hibHighmarkBaseEropDomain.getAddress1()); 
						chmbaseERespType.setADDRESS_2(hibHighmarkBaseEropDomain.getAddress2() == null ? "" : hibHighmarkBaseEropDomain.getAddress2()); 
						chmbaseERespType.setADDRESS_3(hibHighmarkBaseEropDomain.getAddress3() == null ? "" : hibHighmarkBaseEropDomain.getAddress3()); 
						chmbaseERespType.setPHONE_1(hibHighmarkBaseEropDomain.getPhone1() == null ? "" : hibHighmarkBaseEropDomain.getPhone1().toString()); 
						chmbaseERespType.setPHONE_2(hibHighmarkBaseEropDomain.getPhone2() == null ? "" : hibHighmarkBaseEropDomain.getPhone2().toString()); 
						chmbaseERespType.setPHONE_3(hibHighmarkBaseEropDomain.getPhone3() == null ? "" : hibHighmarkBaseEropDomain.getPhone3().toString()); 
						chmbaseERespType.setEMAIL_1(hibHighmarkBaseEropDomain.getEmailId1() == null ? "" : hibHighmarkBaseEropDomain.getEmailId1()); 
						chmbaseERespType.setEMAIL_2(hibHighmarkBaseEropDomain.getEmailId2() == null ? "" : hibHighmarkBaseEropDomain.getEmailId2()); 
						chmbaseERespType.setBRANCH(hibHighmarkBaseEropDomain.getBranch() == null ? "" : hibHighmarkBaseEropDomain.getBranch()); 
						chmbaseERespType.setKENDRA(hibHighmarkBaseEropDomain.getKendra() == null ? "" : hibHighmarkBaseEropDomain.getKendra()); 
						chmbaseERespType.setMBR_ID(hibHighmarkBaseEropDomain.getMbrId() == null ? "" : hibHighmarkBaseEropDomain.getMbrId()); 
						chmbaseERespType.setLOS_APP_ID(hibHighmarkBaseEropDomain.getLosAppId() == null ? "" : hibHighmarkBaseEropDomain.getLosAppId()); 
						chmbaseERespType.setCREDT_INQ_PURPS_TYP(hibHighmarkBaseEropDomain.getCreditInquiryPurposeType() == null ? "" : hibHighmarkBaseEropDomain.getCreditInquiryPurposeType()); 
						chmbaseERespType.setCREDT_INQ_PURPS_TYP_DESC(hibHighmarkBaseEropDomain.getCreditInquiryPurposeTypeDesc() == null ? "" : hibHighmarkBaseEropDomain.getCreditInquiryPurposeTypeDesc()); 
						chmbaseERespType.setCREDIT_INQUIRY_STAGE(hibHighmarkBaseEropDomain.getCreditInquiryStage() == null ? "" : hibHighmarkBaseEropDomain.getCreditInquiryStage()); 
						chmbaseERespType.setCREDT_RPT_ID(hibHighmarkBaseEropDomain.getCreditReportId() == null ? "" : hibHighmarkBaseEropDomain.getCreditReportId()); 
						chmbaseERespType.setCREDT_REQ_TYP(hibHighmarkBaseEropDomain.getCreditRequestType() == null ? "" : hibHighmarkBaseEropDomain.getCreditRequestType()); 
						chmbaseERespType.setCREDT_RPT_TRN_DT_TM(hibHighmarkBaseEropDomain.getCreditReportTransectionDateTime() == null ? "" : hibHighmarkBaseEropDomain.getCreditReportTransectionDateTime()); 
						chmbaseERespType.setAC_OPEN_DT(hibHighmarkBaseEropDomain.getAccountOpenDate() == null ? "" : hibHighmarkBaseEropDomain.getAccountOpenDate()); 
						chmbaseERespType.setLOAN_AMOUNT(hibHighmarkBaseEropDomain.getLoanAmount() == null ? "" : hibHighmarkBaseEropDomain.getLoanAmount().toString()); 
						chmbaseERespType.setENTITY_ID(hibHighmarkBaseEropDomain.getEntityId() == null ? "" : hibHighmarkBaseEropDomain.getEntityId()); 
						chmbaseERespType.setRESPONSE_TYPE(hibHighmarkBaseEropDomain.getResponseType() == null ? "" : hibHighmarkBaseEropDomain.getResponseType()); 
						chmbaseERespType.setACK_CODE(hibHighmarkBaseEropDomain.getAckCode() == null ? "" : hibHighmarkBaseEropDomain.getAckCode()); 
						chmbaseERespType.setACK_DESCRIPTION(hibHighmarkBaseEropDomain.getAckDesc() == null ? "" : hibHighmarkBaseEropDomain.getAckDesc()); 
						chmbaseERespType.setOUTPUT_WRITE_FLAG(hibHighmarkBaseEropDomain.getOutputWriteFlag() == null ? "" : hibHighmarkBaseEropDomain.getOutputWriteFlag()); 
						chmbaseERespType.setOUTPUT_WRITE_TIME(hibHighmarkBaseEropDomain.getOutputWritetime() == null ? "" : hibHighmarkBaseEropDomain.getOutputWritetime()); 
						chmbaseERespType.setOUTPUT_READ_TIME(hibHighmarkBaseEropDomain.getOutputReadTime() == null ? "" : hibHighmarkBaseEropDomain.getOutputReadTime());

						chmbaseERespTypeList.add(chmbaseERespType);
					}
					responseJSONType.setCHM_BASE_EROP_DOMAIN_LIST(chmbaseERespTypeList.toArray(new ChmbaseERespType[chmbaseERespTypeList.size()]));

				}
			}
		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** chmBaseEropMapping >>");
		return responseJSONType;
	}

	public ResponseJSONType chmBaseSropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> chmBaseSropMapping");
		try {
			CHMBaseSropDomainObject chmBaseSropDomainObject = (CHMBaseSropDomainObject) responseJsonObject;
			if (chmBaseSropDomainObject != null) {
				List<HibHighmarkBaseSropDomain> hmBaseSropDomainList = chmBaseSropDomainObject.getHmBaseSropDomainList();
				if (hmBaseSropDomainList != null&& hmBaseSropDomainList.size() > 0) {
					List<ChmbaseSRespType> chmbaseSRespTypeList = new ArrayList<ChmbaseSRespType>();
					for (HibHighmarkBaseSropDomain hibHighmarkBaseSropDomain : hmBaseSropDomainList) {

						ChmbaseSRespType1 chmbaseSRespType1 = new ChmbaseSRespType1();
						ChmbaseSRespType2 chmbaseSRespType2 = new ChmbaseSRespType2();
						chmbaseSRespType1.setSRNO(hibHighmarkBaseSropDomain.getSrNo() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getSrNo())); 
						chmbaseSRespType1.setMEMBER_REFERENCE_NUMBER(hibHighmarkBaseSropDomain.getMemberReferenceNumber() == null ? "" : hibHighmarkBaseSropDomain.getMemberReferenceNumber()); 
						chmbaseSRespType1.setSOA_SOURCE_NAME(hibHighmarkBaseSropDomain.getSoaSourceName() == null ? "" : hibHighmarkBaseSropDomain.getSoaSourceName()); 
						chmbaseSRespType1.setDATE_OF_REQUEST(hibHighmarkBaseSropDomain.getDateOfRequest() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateOfRequest())); 
						chmbaseSRespType1.setPREPARED_FOR(hibHighmarkBaseSropDomain.getPreparedFor() == null ? "" : hibHighmarkBaseSropDomain.getPreparedFor()); 
						chmbaseSRespType1.setPREPARED_FOR_ID(hibHighmarkBaseSropDomain.getPreparedForId() == null ? "" : hibHighmarkBaseSropDomain.getPreparedForId()); 
						chmbaseSRespType1.setDATE_OF_ISSUE(hibHighmarkBaseSropDomain.getDateOfIssue() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateOfIssue())); 
						chmbaseSRespType1.setREPORT_ID(hibHighmarkBaseSropDomain.getReportId() == null ? "" : hibHighmarkBaseSropDomain.getReportId()); 
						chmbaseSRespType1.setBATCH_ID(hibHighmarkBaseSropDomain.getBatchId() == null ? "" : hibHighmarkBaseSropDomain.getBatchId().toString()); 
						chmbaseSRespType1.setSTATUS_IQ(hibHighmarkBaseSropDomain.getStatus() == null ? "" : hibHighmarkBaseSropDomain.getStatus()); 
						chmbaseSRespType1.setNAME_IQ(hibHighmarkBaseSropDomain.getNameIQ() == null ? "" : hibHighmarkBaseSropDomain.getNameIQ()); 
						chmbaseSRespType1.setAKA_IQ(hibHighmarkBaseSropDomain.getAka() == null ? "" : hibHighmarkBaseSropDomain.getAka()); 
						chmbaseSRespType1.setSPOUSE_IQ(hibHighmarkBaseSropDomain.getSpouse() == null ? "" : hibHighmarkBaseSropDomain.getSpouse()); 
						chmbaseSRespType1.setFATHER_IQ(hibHighmarkBaseSropDomain.getFather() == null ? "" : hibHighmarkBaseSropDomain.getFather()); 
						chmbaseSRespType1.setMOTHER_IQ(hibHighmarkBaseSropDomain.getMother() == null ? "" : hibHighmarkBaseSropDomain.getMother()); 
						chmbaseSRespType1.setDOB_IQ(hibHighmarkBaseSropDomain.getDobIQ() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDobIQ())); 
						chmbaseSRespType1.setAGE_IQ(hibHighmarkBaseSropDomain.getAge() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getAge())); 
						chmbaseSRespType1.setAGE_AS_ON_IQ(hibHighmarkBaseSropDomain.getAgeAsOn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getAgeAsOn())); 
						chmbaseSRespType1.setRATION_CARD_IQ(hibHighmarkBaseSropDomain.getRationCard() == null ? "" : hibHighmarkBaseSropDomain.getRationCard()); 
						chmbaseSRespType1.setPASSPORT_IQ(hibHighmarkBaseSropDomain.getPassport() == null ? "" : hibHighmarkBaseSropDomain.getPassport()); 
						chmbaseSRespType1.setVOTERS_ID_IQ(hibHighmarkBaseSropDomain.getVotersId() == null ? "" : hibHighmarkBaseSropDomain.getVotersId()); 
						chmbaseSRespType1.setDRIVING_LICENSE_NO_IQ(hibHighmarkBaseSropDomain.getDrivingLicenceNo() == null ? "" : hibHighmarkBaseSropDomain.getDrivingLicenceNo()); 
						chmbaseSRespType1.setPAN_IQ(hibHighmarkBaseSropDomain.getPan() == null ? "" : hibHighmarkBaseSropDomain.getPan()); 
						chmbaseSRespType1.setGENDER_IQ(hibHighmarkBaseSropDomain.getGender() == null ? "" : hibHighmarkBaseSropDomain.getGender()); 
						chmbaseSRespType1.setOWNERSHIP_IQ(hibHighmarkBaseSropDomain.getOwnership() == null ? "" : hibHighmarkBaseSropDomain.getOwnership()); 
						chmbaseSRespType1.setADDRESS_1_IQ(hibHighmarkBaseSropDomain.getAddress1() == null ? "" : hibHighmarkBaseSropDomain.getAddress1()); 
						chmbaseSRespType1.setADDRESS_2_IQ(hibHighmarkBaseSropDomain.getAddress2() == null ? "" : hibHighmarkBaseSropDomain.getAddress2()); 
						chmbaseSRespType1.setADDRESS_3_IQ(hibHighmarkBaseSropDomain.getAddress3() == null ? "" : hibHighmarkBaseSropDomain.getAddress3()); 
						chmbaseSRespType1.setPHONE_1_IQ(hibHighmarkBaseSropDomain.getPhone1() == null ? "" : hibHighmarkBaseSropDomain.getPhone1().toString()); 
						chmbaseSRespType1.setPHONE_2_IQ(hibHighmarkBaseSropDomain.getPhone2() == null ? "" : hibHighmarkBaseSropDomain.getPhone2().toString()); 
						chmbaseSRespType1.setPHONE_3_IQ(hibHighmarkBaseSropDomain.getPhone3() == null ? "" : hibHighmarkBaseSropDomain.getPhone3().toString()); 
						chmbaseSRespType1.setEMAIL_1_IQ(hibHighmarkBaseSropDomain.getEmailId1() == null ? "" : hibHighmarkBaseSropDomain.getEmailId1()); 
						chmbaseSRespType1.setEMAIL_2_IQ(hibHighmarkBaseSropDomain.getEmailId2() == null ? "" : hibHighmarkBaseSropDomain.getEmailId2()); 
						chmbaseSRespType1.setBRANCH_IQ(hibHighmarkBaseSropDomain.getBranch() == null ? "" : hibHighmarkBaseSropDomain.getBranch()); 
						chmbaseSRespType1.setKENDRA_IQ(hibHighmarkBaseSropDomain.getKendra() == null ? "" : hibHighmarkBaseSropDomain.getKendra()); 
						chmbaseSRespType1.setMBR_ID_IQ(hibHighmarkBaseSropDomain.getMbrId() == null ? "" : hibHighmarkBaseSropDomain.getMbrId()); 
						chmbaseSRespType1.setLOS_APP_ID_IQ(hibHighmarkBaseSropDomain.getLosAppId() == null ? "" : hibHighmarkBaseSropDomain.getLosAppId()); 
						chmbaseSRespType1.setCREDT_INQ_PURPS_TYP_IQ(hibHighmarkBaseSropDomain.getCreditInquiryPurposeType() == null ? "" : hibHighmarkBaseSropDomain.getCreditInquiryPurposeType()); 
						chmbaseSRespType1.setCREDT_INQ_PURPS_TYP_DESC_IQ(hibHighmarkBaseSropDomain.getCreditInquiryPurposeTypeDesc() == null ? "" : hibHighmarkBaseSropDomain.getCreditInquiryPurposeTypeDesc()); 
						chmbaseSRespType1.setCREDIT_INQUIRY_STAGE_IQ(hibHighmarkBaseSropDomain.getCreditInquiryStage() == null ? "" : hibHighmarkBaseSropDomain.getCreditInquiryStage()); 
						chmbaseSRespType1.setCREDT_RPT_ID_IQ(hibHighmarkBaseSropDomain.getCreditReportId() == null ? "" : hibHighmarkBaseSropDomain.getCreditReportId()); 
						chmbaseSRespType1.setCREDT_REQ_TYP_IQ(hibHighmarkBaseSropDomain.getCreditRequestType() == null ? "" : hibHighmarkBaseSropDomain.getCreditRequestType()); 
						chmbaseSRespType1.setCREDT_RPT_TRN_DT_TM_IQ(hibHighmarkBaseSropDomain.getCreditReportTransectionDateTime() == null ? "" : hibHighmarkBaseSropDomain.getCreditReportTransectionDateTime()); 
						chmbaseSRespType1.setAC_OPEN_DT_IQ(hibHighmarkBaseSropDomain.getAccountOpenDate() == null ? "" : hibHighmarkBaseSropDomain.getAccountOpenDate()); 
						chmbaseSRespType1.setLOAN_AMOUNT_IQ(hibHighmarkBaseSropDomain.getLoanAmount() == null ? "" : hibHighmarkBaseSropDomain.getLoanAmount().toString()); 
						chmbaseSRespType1.setENTITY_ID_IQ(hibHighmarkBaseSropDomain.getEntityId() == null ? "" : hibHighmarkBaseSropDomain.getEntityId()); 
						chmbaseSRespType1.setPR_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getPrimaryNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getPrimaryNumberOfAccount())); 
						chmbaseSRespType1.setPR_ACTIVE_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getPrimaryActiveNoOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getPrimaryActiveNoOfAccount())); 
						chmbaseSRespType1.setPR_OVERDUE_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getPrimaryOverdueNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getPrimaryOverdueNumberOfAccount())); 
						chmbaseSRespType1.setPR_CURRENT_BALANCE(hibHighmarkBaseSropDomain.getPrimaryCurrentBalance() == null ? "" : hibHighmarkBaseSropDomain.getPrimaryCurrentBalance()); 
						chmbaseSRespType1.setPR_SANCTIONED_AMOUNT(hibHighmarkBaseSropDomain.getPrimarySanctionedAmount() == null ? "" : hibHighmarkBaseSropDomain.getPrimarySanctionedAmount()); 
						chmbaseSRespType1.setPR_DISBURSED_AMOUNT(hibHighmarkBaseSropDomain.getPrimaryDisbursedAmount() == null ? "" : hibHighmarkBaseSropDomain.getPrimaryDisbursedAmount()); 
						chmbaseSRespType1.setPR_SECURED_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getPrimarySecuredNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getPrimarySecuredNumberOfAccount())); 
						chmbaseSRespType1.setPR_UNSECURED_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getPrimaryUnsecuredNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getPrimaryUnsecuredNumberOfAccount())); 
						chmbaseSRespType1.setPR_UNTAGGED_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getPrimaryUntaggedNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getPrimaryUntaggedNumberOfAccount())); 
						chmbaseSRespType1.setSE_NUMBER_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getSecondaryVNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getSecondaryVNumberOfAccount())); 
						chmbaseSRespType1.setSE_ACTIVE_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getSecondaryActiveNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getSecondaryActiveNumberOfAccount())); 
						chmbaseSRespType1.setSE_OVERDUE_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getSecondaryActiveNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getSecondaryActiveNumberOfAccount())); 
						chmbaseSRespType1.setSE_CURRENT_BALANCE(hibHighmarkBaseSropDomain.getSecondaryCurrentBalance() == null ? "" : hibHighmarkBaseSropDomain.getSecondaryCurrentBalance()); 
						chmbaseSRespType1.setSE_SANCTIONED_AMOUNT(hibHighmarkBaseSropDomain.getSecondarySanctionedAmount() == null ? "" : hibHighmarkBaseSropDomain.getSecondarySanctionedAmount()); 
						chmbaseSRespType1.setSE_DISBURSED_AMOUNT(hibHighmarkBaseSropDomain.getSecondaryDisbursedAmount() == null ? "" : hibHighmarkBaseSropDomain.getSecondaryDisbursedAmount()); 
						chmbaseSRespType1.setSE_SECURED_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getSecondarySecuredNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getSecondarySecuredNumberOfAccount())); 
						chmbaseSRespType1.setSE_UNSECURED_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getSecondaryUnsecuredNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getSecondaryUnsecuredNumberOfAccount())); 
						chmbaseSRespType1.setSE_UNTAGGED_NO_OF_ACCOUNTS(hibHighmarkBaseSropDomain.getSecondaryUntaggedNumberOfAccount() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getSecondaryUntaggedNumberOfAccount())); 
						chmbaseSRespType1.setINQURIES_IN_LAST_SIX_MONTHS(hibHighmarkBaseSropDomain.getInquriesInLastSixMonth() == null ? "" : hibHighmarkBaseSropDomain.getInquriesInLastSixMonth().toString()); 
						chmbaseSRespType1.setLENGTH_OF_CREDIT_HIS_YEAR(hibHighmarkBaseSropDomain.getLengthOfCreditHistoryYear() == null ? "" : hibHighmarkBaseSropDomain.getLengthOfCreditHistoryYear().toString()); 
						chmbaseSRespType1.setLENGTH_OF_CREDIT_HIS_MON(hibHighmarkBaseSropDomain.getLengthOfCreditHistoryMonth() == null ? "" : hibHighmarkBaseSropDomain.getLengthOfCreditHistoryMonth().toString()); 
						chmbaseSRespType1.setAVG_ACC_AGE_YEAR(hibHighmarkBaseSropDomain.getAverageAccountAgeYear() == null ? "" : hibHighmarkBaseSropDomain.getAverageAccountAgeYear().toString()); 
						chmbaseSRespType1.setAVG_ACC_AGE_MON(hibHighmarkBaseSropDomain.getAverageAccountAgeMonth() == null ? "" : hibHighmarkBaseSropDomain.getAverageAccountAgeMonth().toString()); 
						chmbaseSRespType1.setNEW_ACC_IN_LAST_SIX_MON(hibHighmarkBaseSropDomain.getNewAccountInLastSixMonth() == null ? "" : hibHighmarkBaseSropDomain.getNewAccountInLastSixMonth().toString()); 
						chmbaseSRespType1.setNEW_DELINQ_ACC_LAST_SIX_MON(hibHighmarkBaseSropDomain.getNewDelinqAccountInLastSixMonth() == null ? "" : hibHighmarkBaseSropDomain.getNewDelinqAccountInLastSixMonth().toString()); 
						chmbaseSRespType1.setNAME(hibHighmarkBaseSropDomain.getNameVar() == null ? "" : hibHighmarkBaseSropDomain.getNameVar()); 
						chmbaseSRespType1.setNAME_REPORTED_DATE(hibHighmarkBaseSropDomain.getNameVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getNameVarDate())); 
						chmbaseSRespType1.setADDRESS(hibHighmarkBaseSropDomain.getAddressVar() == null ? "" : hibHighmarkBaseSropDomain.getAddressVar()); 
						chmbaseSRespType1.setADDRESS_REPORTED_DATE(hibHighmarkBaseSropDomain.getAddressVarDate()==null?"":sdf1.format(hibHighmarkBaseSropDomain.getAddressVarDate())); 
						chmbaseSRespType1.setPAN_NO(hibHighmarkBaseSropDomain.getPanVar() == null ? "" : hibHighmarkBaseSropDomain.getPanVar()); 
						chmbaseSRespType1.setPAN_NO_REPORTED_DATE(hibHighmarkBaseSropDomain.getPanVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getPanVarDate())); 
						chmbaseSRespType1.setDRIVING_LICENSE(hibHighmarkBaseSropDomain.getDrivingLicenceNo() == null ? "" : hibHighmarkBaseSropDomain.getDrivingLicenceNo()); 
						chmbaseSRespType1.setDRIVING_LICENSE_REPORTED_DATE(hibHighmarkBaseSropDomain.getDlVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDlVarDate())); 
						chmbaseSRespType1.setDOB(hibHighmarkBaseSropDomain.getDobVar() == null ? "" : hibHighmarkBaseSropDomain.getDobVar()); 
						chmbaseSRespType1.setDOB_REPORTED_DATE(hibHighmarkBaseSropDomain.getDobVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDobVarDate())); 
						chmbaseSRespType1.setVOTER_ID(hibHighmarkBaseSropDomain.getVoterVar()== null ? "" : hibHighmarkBaseSropDomain.getVoterVar()); 
						chmbaseSRespType1.setVOTER_ID_REPORTED_DATE(hibHighmarkBaseSropDomain.getVoterVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getVoterVarDate())); 
						chmbaseSRespType1.setPASSPORT_NUMBER(hibHighmarkBaseSropDomain.getPassportVar() == null ? "" : hibHighmarkBaseSropDomain.getPassportVar()); 
						chmbaseSRespType1.setPASSPORT_REPORTED_DATE(hibHighmarkBaseSropDomain.getPassportVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getPassportVarDate())); 
						chmbaseSRespType1.setPHONE_NUMBER(hibHighmarkBaseSropDomain.getPhoneVar() == null ? "" : hibHighmarkBaseSropDomain.getPhoneVar()); 
						chmbaseSRespType1.setPHONE_REPORTED_DATE(hibHighmarkBaseSropDomain.getPhoneVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getPhoneVarDate())); 
						chmbaseSRespType1.setRATION_CARD(hibHighmarkBaseSropDomain.getRationVar() == null ? "" : hibHighmarkBaseSropDomain.getRationVar()); 
						chmbaseSRespType1.setRATION_REPORTED_DATE(hibHighmarkBaseSropDomain.getRationVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getRationVarDate())); 
						chmbaseSRespType1.setEMAIL_ID(hibHighmarkBaseSropDomain.getEmailVar() == null ? "" : hibHighmarkBaseSropDomain.getEmailVar()); 
						chmbaseSRespType1.setEMAIL_ID_REPORTED_DATE(hibHighmarkBaseSropDomain.getEmailVarDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getEmailVarDate())); 
						chmbaseSRespType1.setMATCHED_TYPE(hibHighmarkBaseSropDomain.getMatchedType() == null ? "" : hibHighmarkBaseSropDomain.getMatchedType()); 
						chmbaseSRespType1.setACCT_NUMBER(hibHighmarkBaseSropDomain.getAccountNumber() == null ? "" : hibHighmarkBaseSropDomain.getAccountNumber()); 
						chmbaseSRespType1.setCREDIT_GUARANTOR(hibHighmarkBaseSropDomain.getCreditGuarantor() == null ? "" : hibHighmarkBaseSropDomain.getCreditGuarantor()); 
						chmbaseSRespType1.setACCT_TYPE(hibHighmarkBaseSropDomain.getAcctType() == null ? "" : hibHighmarkBaseSropDomain.getAcctType()); 
						chmbaseSRespType1.setDATE_REPORTED(hibHighmarkBaseSropDomain.getDateReported() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateReported())); 
						chmbaseSRespType1.setOWNERSHIP_IND(hibHighmarkBaseSropDomain.getOwnershipIndicator() == null ? "" : hibHighmarkBaseSropDomain.getOwnershipIndicator()); 
						chmbaseSRespType1.setACCOUNT_STATUS(hibHighmarkBaseSropDomain.getAccountStatus() == null ? "" : hibHighmarkBaseSropDomain.getAccountStatus()); 
						chmbaseSRespType1.setDISBURSED_AMT(hibHighmarkBaseSropDomain.getDisbursedAmount() == null ? "" : hibHighmarkBaseSropDomain.getDisbursedAmount()); 
						chmbaseSRespType1.setDISBURSED_DT(hibHighmarkBaseSropDomain.getDisbursedDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDisbursedDate())); 
						chmbaseSRespType1.setLAST_PAYMENT_DATE(hibHighmarkBaseSropDomain.getLastPaymentDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getLastPaymentDate())); 
						chmbaseSRespType1.setCLOSE_DT(hibHighmarkBaseSropDomain.getClosedDate() == null ? "" :sdf1.format(hibHighmarkBaseSropDomain.getClosedDate())); 
						chmbaseSRespType1.setINSTALLMENT_AMT(hibHighmarkBaseSropDomain.getInstallmentAmount() == null ? "" : hibHighmarkBaseSropDomain.getInstallmentAmount()); 
						chmbaseSRespType1.setOVERDUE_AMT(hibHighmarkBaseSropDomain.getOverdueAmount() == null ? "" : hibHighmarkBaseSropDomain.getOverdueAmount()); 
						chmbaseSRespType1.setWRITE_OFF_AMT(hibHighmarkBaseSropDomain.getWriteOffAmount() == null ? "" : hibHighmarkBaseSropDomain.getWriteOffAmount()); 
						chmbaseSRespType1.setCURRENT_BAL(hibHighmarkBaseSropDomain.getCurrentBalance() == null ? "" : hibHighmarkBaseSropDomain.getCurrentBalance()); 
						chmbaseSRespType1.setCREDIT_LIMIT(hibHighmarkBaseSropDomain.getCreditLimit() == null ? "" : hibHighmarkBaseSropDomain.getCreditLimit()); 
						chmbaseSRespType1.setACCOUNT_REMARKS(hibHighmarkBaseSropDomain.getAccountRemarks() == null ? "" : hibHighmarkBaseSropDomain.getAccountRemarks()); 
						chmbaseSRespType1.setFREQUENCY(hibHighmarkBaseSropDomain.getFrequency() == null ? "" : hibHighmarkBaseSropDomain.getFrequency()); 
						chmbaseSRespType1.setSECURITY_STATUS(hibHighmarkBaseSropDomain.getSecurityStatus() == null ? "" : hibHighmarkBaseSropDomain.getSecurityStatus()); 
						chmbaseSRespType1.setORIGINAL_TERM(hibHighmarkBaseSropDomain.getOriginalTerm() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getOriginalTerm())); 
						chmbaseSRespType1.setTERM_TO_MATURITY(hibHighmarkBaseSropDomain.getTermToMaturity() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getTermToMaturity())); 
						chmbaseSRespType1.setACCT_IN_DISPUTE(hibHighmarkBaseSropDomain.getAccountInDispute() == null ? "" : hibHighmarkBaseSropDomain.getAccountInDispute()); 
						chmbaseSRespType1.setSETTLEMENT_AMT(hibHighmarkBaseSropDomain.getSettlementAmount() == null ? "" : hibHighmarkBaseSropDomain.getSettlementAmount()); 
						chmbaseSRespType1.setPRINCIPAL_WRITE_OFF_AMT(hibHighmarkBaseSropDomain.getPrincipalWriteOffAmount() == null ? "" : hibHighmarkBaseSropDomain.getPrincipalWriteOffAmount()); 
						chmbaseSRespType1.setCOMBINED_PAYMENT_HISTORY(hibHighmarkBaseSropDomain.getCombinedPaymentHistory() == null ? "" : hibHighmarkBaseSropDomain.getCombinedPaymentHistory()); 
						chmbaseSRespType1.setREPAYMENT_TENURE(hibHighmarkBaseSropDomain.getRepaymentTenure() == null ? "" : hibHighmarkBaseSropDomain.getRepaymentTenure()); 
						chmbaseSRespType1.setINTEREST_RATE(hibHighmarkBaseSropDomain.getInterestRate() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getInterestRate())); 
						chmbaseSRespType1.setSUIT_FILED_WILFUL_DEFAULT(hibHighmarkBaseSropDomain.getSuitFiledWilfulDefault() == null ? "" : hibHighmarkBaseSropDomain.getSuitFiledWilfulDefault()); 
						chmbaseSRespType1.setWRITTEN_OFF_SETTLED_STATUS(hibHighmarkBaseSropDomain.getWrittenOffSetteldStatus() == null ? "" : hibHighmarkBaseSropDomain.getWrittenOffSetteldStatus()); 
						chmbaseSRespType1.setCASH_LIMIT(hibHighmarkBaseSropDomain.getCashLimit() == null ? "" : hibHighmarkBaseSropDomain.getCashLimit()); 
						chmbaseSRespType1.setACTUAL_PAYMENT(hibHighmarkBaseSropDomain.getActualPayment() == null ? "" : hibHighmarkBaseSropDomain.getActualPayment()); 
						chmbaseSRespType1.setSECURITY_TYPE_LD(hibHighmarkBaseSropDomain.getSecurityType() == null ? "" : hibHighmarkBaseSropDomain.getSecurityType()); 
						chmbaseSRespType1.setOWNER_NAME_LD(hibHighmarkBaseSropDomain.getOwnerName() == null ? "" : hibHighmarkBaseSropDomain.getOwnerName()); 
						chmbaseSRespType1.setSECURITY_VALUE_LD(hibHighmarkBaseSropDomain.getSecurityValue() == null ? "" : hibHighmarkBaseSropDomain.getSecurityValue()); 
						chmbaseSRespType1.setDATE_OF_VALUE_LD(hibHighmarkBaseSropDomain.getDateOfValue() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateOfValue())); 
						chmbaseSRespType1.setSECURITY_CHARGE_LD(hibHighmarkBaseSropDomain.getSecurityCharge() == null ? "" : hibHighmarkBaseSropDomain.getSecurityCharge()); 
						chmbaseSRespType1.setPROPERTY_ADDRESS_LD(hibHighmarkBaseSropDomain.getPropertyAddress() == null ? "" : hibHighmarkBaseSropDomain.getPropertyAddress()); 
						chmbaseSRespType1.setAUTOMOBILE_TYPE_LD(hibHighmarkBaseSropDomain.getAutomobileType() == null ? "" : hibHighmarkBaseSropDomain.getAutomobileType()); 
						chmbaseSRespType1.setYEAR_OF_MANUFACTURE_LD(hibHighmarkBaseSropDomain.getYearOfManufacture() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getYearOfManufacture())); 
						chmbaseSRespType1.setREGISTRATION_NUMBER_LD(hibHighmarkBaseSropDomain.getRegistrationNumber() == null ? "" : hibHighmarkBaseSropDomain.getRegistrationNumber()); 
						chmbaseSRespType1.setENGINE_NUMBER_LD(hibHighmarkBaseSropDomain.getEngineNumber() == null ? "" : hibHighmarkBaseSropDomain.getEngineNumber()); 
						chmbaseSRespType1.setCHASIS_NUMBER_LD(hibHighmarkBaseSropDomain.getChassisNumber() == null ? "" : hibHighmarkBaseSropDomain.getChassisNumber()); 
						chmbaseSRespType1.setLINKED_ACCT_NUMBER(hibHighmarkBaseSropDomain.getAccountNumberLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountNumberLn()); 
						chmbaseSRespType1.setLINKED_CREDIT_GUARANTOR(hibHighmarkBaseSropDomain.getCreditGuarantorLn() == null ? "" : hibHighmarkBaseSropDomain.getCreditGuarantorLn()); 
						chmbaseSRespType1.setLINKED_ACCT_TYPE(hibHighmarkBaseSropDomain.getAccountTypeLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountTypeLn()); 
						chmbaseSRespType1.setLINKED_DATE_REPORTED(hibHighmarkBaseSropDomain.getDateReportedLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateReportedLn())); 
						chmbaseSRespType1.setLINKED_OWNERSHIP_IND(hibHighmarkBaseSropDomain.getOwnershipIndicatorLn() == null ? "" : hibHighmarkBaseSropDomain.getOwnershipIndicatorLn()); 
						chmbaseSRespType1.setLINKED_ACCOUNT_STATUS(hibHighmarkBaseSropDomain.getAccountStatusLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountStatusLn()); 
						chmbaseSRespType1.setLINKED_DISBURSED_AMT(hibHighmarkBaseSropDomain.getDisbursedAmountLn() == null ? "" : hibHighmarkBaseSropDomain.getDisbursedAmountLn()); 
						chmbaseSRespType1.setLINKED_DISBURSED_DT(hibHighmarkBaseSropDomain.getDisbursedDateLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDisbursedDateLn())); 
						chmbaseSRespType1.setLINKED_LAST_PAYMENT_DATE(hibHighmarkBaseSropDomain.getLastPaymentDateLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getLastPaymentDateLn())); 
						chmbaseSRespType1.setLINKED_CLOSED_DATE(hibHighmarkBaseSropDomain.getClosedDateLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getClosedDateLn())); 
						chmbaseSRespType1.setLINKED_INSTALLMENT_AMT(hibHighmarkBaseSropDomain.getInstallmentAmountLn() == null ? "" : hibHighmarkBaseSropDomain.getInstallmentAmountLn()); 
						chmbaseSRespType1.setLINKED_OVERDUE_AMT(hibHighmarkBaseSropDomain.getOverdueAmountLn() == null ? "" : hibHighmarkBaseSropDomain.getOverdueAmountLn()); 
						chmbaseSRespType1.setLINKED_WRITE_OFF_AMT(hibHighmarkBaseSropDomain.getWriteOffAmountLn() == null ? "" : hibHighmarkBaseSropDomain.getWriteOffAmountLn()); 
						chmbaseSRespType1.setLINKED_CURRENT_BAL(hibHighmarkBaseSropDomain.getCurrentBalanceLn() == null ? "" : hibHighmarkBaseSropDomain.getCurrentBalanceLn()); 
						chmbaseSRespType1.setLINKED_CREDIT_LIMIT(hibHighmarkBaseSropDomain.getCreditLimitLn() == null ? "" : hibHighmarkBaseSropDomain.getCreditLimitLn()); 
						chmbaseSRespType1.setLINKED_ACCOUNT_REMARKS(hibHighmarkBaseSropDomain.getAccountRemarksLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountRemarksLn()); 
						chmbaseSRespType1.setLINKED_FREQUENCY(hibHighmarkBaseSropDomain.getFrequencyLn() == null ? "" : hibHighmarkBaseSropDomain.getFrequencyLn()); 
						chmbaseSRespType1.setLINKED_SECURITY_STATUS(hibHighmarkBaseSropDomain.getSecurityStatusLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityStatusLn()); 
						chmbaseSRespType1.setLINKED_ORIGINAL_TERM(hibHighmarkBaseSropDomain.getOriginalTermLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getOriginalTermLn())); 
						chmbaseSRespType1.setLINKED_TERM_TO_MATURITY(hibHighmarkBaseSropDomain.getTermToMaturityLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getTermToMaturityLn())); 
						chmbaseSRespType1.setLINKED_ACCT_IN_DISPUTE(hibHighmarkBaseSropDomain.getAccountInDisputeLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountInDisputeLn()); 
						chmbaseSRespType1.setLINKED_SETTLEMENT_AMT(hibHighmarkBaseSropDomain.getSettlementAmtLn() == null ? "" : hibHighmarkBaseSropDomain.getSettlementAmtLn()); 
						chmbaseSRespType1.setLINKED_PRNPAL_WRITE_OFF_AMT(hibHighmarkBaseSropDomain.getPrincipalWriteOffAmtLn() == null ? "" : hibHighmarkBaseSropDomain.getPrincipalWriteOffAmtLn()); 
						chmbaseSRespType1.setLINKED_REPAYMENT_TENURE(hibHighmarkBaseSropDomain.getRepaymentTenureLn() == null ? "" : hibHighmarkBaseSropDomain.getRepaymentTenureLn()); 
						chmbaseSRespType1.setLINKED_INTEREST_RATE(hibHighmarkBaseSropDomain.getInterestRateLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getInterestRateLn())); 
						chmbaseSRespType1.setLINKED_SUIT_F_WILFUL_DEFAULT(hibHighmarkBaseSropDomain.getSuitFiledWilfulDefaultLn() == null ? "" : hibHighmarkBaseSropDomain.getSuitFiledWilfulDefaultLn()); 
						chmbaseSRespType1.setLINKED_WRTN_OFF_SETTLD_STATUS(hibHighmarkBaseSropDomain.getWrittenOffSetteldStatusLn() == null ? "" : hibHighmarkBaseSropDomain.getWrittenOffSetteldStatusLn()); 
						chmbaseSRespType1.setLINKED_CASH_LIMIT(hibHighmarkBaseSropDomain.getCashLimitLn() == null ? "" : hibHighmarkBaseSropDomain.getCashLimitLn()); 
						chmbaseSRespType1.setLINKED_ACTUAL_PAYMENT(hibHighmarkBaseSropDomain.getActualPaymentLn() == null ? "" : hibHighmarkBaseSropDomain.getActualPaymentLn()); 
						chmbaseSRespType1.setSECURITY_TYPE_LA(hibHighmarkBaseSropDomain.getSecurityTypeLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityTypeLn()); 
						chmbaseSRespType1.setOWNER_NAME_LA(hibHighmarkBaseSropDomain.getOwnerNameLn() == null ? "" : hibHighmarkBaseSropDomain.getOwnerNameLn()); 
						chmbaseSRespType1.setSECURITY_VALUE_LA(hibHighmarkBaseSropDomain.getSecurityValueLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityValueLn()); 
						chmbaseSRespType1.setDATE_OF_VALUE_LA(hibHighmarkBaseSropDomain.getDateOfValueLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateOfValueLn())); 
						chmbaseSRespType1.setSECURITY_CHARGE_LA(hibHighmarkBaseSropDomain.getSecurityChargeLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityChargeLn()); 
						chmbaseSRespType1.setPROPERTY_ADDRESS_LA(hibHighmarkBaseSropDomain.getPropertyAddressLn() == null ? "" : hibHighmarkBaseSropDomain.getPropertyAddressLn()); 
						chmbaseSRespType1.setAUTOMOBILE_TYPE_LA(hibHighmarkBaseSropDomain.getAutomobileTypeLn() == null ? "" : hibHighmarkBaseSropDomain.getAutomobileTypeLn()); 
						chmbaseSRespType1.setYEAR_OF_MANUFACTURE_LA(hibHighmarkBaseSropDomain.getYearOfManufactureLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getYearOfManufactureLn())); 
						chmbaseSRespType1.setREGISTRATION_NUMBER_LA(hibHighmarkBaseSropDomain.getRegistrationNumberLn() == null ? "" : hibHighmarkBaseSropDomain.getRegistrationNumberLn()); 
						chmbaseSRespType1.setENGINE_NUMBER_LA(hibHighmarkBaseSropDomain.getEngineNumberLn() == null ? "" : hibHighmarkBaseSropDomain.getEngineNumberLn()); 
						chmbaseSRespType1.setCHASIS_NUMBER_LA(hibHighmarkBaseSropDomain.getChassisNumberLn() == null ? "" : hibHighmarkBaseSropDomain.getChassisNumberLn()); 
						chmbaseSRespType1.setSM_NAME(hibHighmarkBaseSropDomain.getNameIndtl() == null ? "" : hibHighmarkBaseSropDomain.getNameIndtl()); 
						chmbaseSRespType1.setSM_ADDRESS(hibHighmarkBaseSropDomain.getAddress() == null ? "" : hibHighmarkBaseSropDomain.getAddress()); 
						chmbaseSRespType1.setSM_DOB(hibHighmarkBaseSropDomain.getDobIndtl() == null ? "" : hibHighmarkBaseSropDomain.getDobIndtl()); 
						chmbaseSRespType1.setSM_PHONE(hibHighmarkBaseSropDomain.getPhoneIndtl() == null ? "" : hibHighmarkBaseSropDomain.getPhoneIndtl()); 
						chmbaseSRespType1.setSM_PAN(hibHighmarkBaseSropDomain.getPanIndtl() == null ? "" : hibHighmarkBaseSropDomain.getPanIndtl()); 
						chmbaseSRespType1.setSM_PASSPORT(hibHighmarkBaseSropDomain.getPassportIndtl() == null ? "" : hibHighmarkBaseSropDomain.getPassportIndtl()); 
						chmbaseSRespType1.setSM_DRIVING_LICENSE(hibHighmarkBaseSropDomain.getDrivingLicenseIndtl() == null ? "" : hibHighmarkBaseSropDomain.getDrivingLicenseIndtl()); 
						chmbaseSRespType1.setSM_VOTER_ID(hibHighmarkBaseSropDomain.getVoterIdIndtl() == null ? "" : hibHighmarkBaseSropDomain.getVoterIdIndtl()); 
						chmbaseSRespType1.setSM_E_MAIL(hibHighmarkBaseSropDomain.getEmailIndtl() == null ? "" : hibHighmarkBaseSropDomain.getEmailIndtl()); 
						chmbaseSRespType1.setSM_RATION_CARD(hibHighmarkBaseSropDomain.getRationCardIndtl() == null ? "" : hibHighmarkBaseSropDomain.getRationCardIndtl()); 
						chmbaseSRespType1.setSM_MATCHED_TYPE(hibHighmarkBaseSropDomain.getMatchedTypeSe() == null ? "" : hibHighmarkBaseSropDomain.getMatchedTypeSe()); 
						chmbaseSRespType1.setSM_ACCT_NUMBER(hibHighmarkBaseSropDomain.getAccountNumberSe() == null ? "" : hibHighmarkBaseSropDomain.getAccountNumberSe()); 
						chmbaseSRespType1.setSM_CREDIT_GUARANTOR(hibHighmarkBaseSropDomain.getCreditGuarantorSe() == null ? "" : hibHighmarkBaseSropDomain.getCreditGuarantorSe()); 
						chmbaseSRespType1.setSM_ACCT_TYPE(hibHighmarkBaseSropDomain.getAcctTypeSe() == null ? "" : hibHighmarkBaseSropDomain.getAcctTypeSe()); 
						chmbaseSRespType1.setSM_DATE_REPORTED(hibHighmarkBaseSropDomain.getDateReportedSe() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateReportedSe())); 
						chmbaseSRespType1.setSM_OWNERSHIP_IND(hibHighmarkBaseSropDomain.getOwnershipIndicatorSe() == null ? "" : hibHighmarkBaseSropDomain.getOwnershipIndicatorSe()); 
						chmbaseSRespType1.setSM_ACCOUNT_STATUS(hibHighmarkBaseSropDomain.getAccountStatusSe() == null ? "" : hibHighmarkBaseSropDomain.getAccountStatusSe()); 
						chmbaseSRespType1.setSM_DISBURSED_AMT(hibHighmarkBaseSropDomain.getDisbursedAmountSe() == null ? "" : hibHighmarkBaseSropDomain.getDisbursedAmountSe()); 
						chmbaseSRespType1.setSM_DISBURSED_DT(hibHighmarkBaseSropDomain.getDisbursedDateSe() == null ? "" :sdf1.format(hibHighmarkBaseSropDomain.getDisbursedDateSe())); 
						chmbaseSRespType1.setSM_LAST_PAYMENT_DATE(hibHighmarkBaseSropDomain.getLastPaymentDateSe() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getLastPaymentDateSe())); 
						chmbaseSRespType1.setSM_CLOSE_DT(hibHighmarkBaseSropDomain.getClosedDateSe() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getClosedDateSe())); 
						chmbaseSRespType1.setSM_INSTALLMENT_AMT(hibHighmarkBaseSropDomain.getInstallmentAmountSe() == null ? "" : hibHighmarkBaseSropDomain.getInstallmentAmountSe()); 
						chmbaseSRespType1.setSM_OVERDUE_AMT(hibHighmarkBaseSropDomain.getOverdueAmountSe() == null ? "" : hibHighmarkBaseSropDomain.getOverdueAmountSe()); 
						chmbaseSRespType1.setSM_WRITE_OFF_AMT(hibHighmarkBaseSropDomain.getWriteOffAmountSe() == null ? "" : hibHighmarkBaseSropDomain.getWriteOffAmountSe()); 
						chmbaseSRespType1.setSM_CURRENT_BAL(hibHighmarkBaseSropDomain.getCurrentBalanceSe() == null ? "" : hibHighmarkBaseSropDomain.getCurrentBalanceSe()); 
						chmbaseSRespType1.setSM_CREDIT_LIMIT(hibHighmarkBaseSropDomain.getCreditLimitSe() == null ? "" : hibHighmarkBaseSropDomain.getCreditLimitSe()); 
						chmbaseSRespType1.setSM_ACCOUNT_REMARKS(hibHighmarkBaseSropDomain.getAccountRemarksSe() == null ? "" : hibHighmarkBaseSropDomain.getAccountRemarksSe()); 
						chmbaseSRespType1.setSM_FREQUENCY(hibHighmarkBaseSropDomain.getFrequencySe() == null ? "" : hibHighmarkBaseSropDomain.getFrequencySe()); 
						chmbaseSRespType2.setSM_SECURITY_STATUS(hibHighmarkBaseSropDomain.getSecurityStatusSe() == null ? "" : hibHighmarkBaseSropDomain.getSecurityStatusSe()); 
						chmbaseSRespType2.setSM_ORIGINAL_TERM(hibHighmarkBaseSropDomain.getOriginalTermSe() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getOriginalTermSe())); 
						chmbaseSRespType2.setSM_TERM_TO_MATURITY(hibHighmarkBaseSropDomain.getTermToMaturitySe() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getTermToMaturitySe())); 
						chmbaseSRespType2.setSM_ACCT_IN_DISPUTE(hibHighmarkBaseSropDomain.getAccountInDisputeSe() == null ? "" : hibHighmarkBaseSropDomain.getAccountInDisputeSe()); 
						chmbaseSRespType2.setSM_SETTLEMENT_AMT(hibHighmarkBaseSropDomain.getSettlementAmountSe() == null ? "" : hibHighmarkBaseSropDomain.getSettlementAmountSe()); 
						chmbaseSRespType2.setSM_PRINCIPAL_WRITE_OFF_AMT(hibHighmarkBaseSropDomain.getPrincipalWriteOffAmountSe() == null ? "" : hibHighmarkBaseSropDomain.getPrincipalWriteOffAmountSe()); 
						chmbaseSRespType2.setSM_COMBINED_PAYMENT_HISTORY(hibHighmarkBaseSropDomain.getCombinedPaymentHistorySe() == null ? "" : hibHighmarkBaseSropDomain.getCombinedPaymentHistorySe()); 
						chmbaseSRespType2.setSM_REPAYMENT_TENURE(hibHighmarkBaseSropDomain.getRepaymentTenureSe() == null ? "" : hibHighmarkBaseSropDomain.getRepaymentTenureSe()); 
						chmbaseSRespType2.setSM_INTEREST_RATE(hibHighmarkBaseSropDomain.getInterestRateSe() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getInterestRateSe())); 
						chmbaseSRespType2.setSM_SUIT_FILED_WILFUL_DEFAULT(hibHighmarkBaseSropDomain.getSuitFiledWilfulDefaultSe() == null ? "" : hibHighmarkBaseSropDomain.getSuitFiledWilfulDefaultSe()); 
						chmbaseSRespType2.setSM_WRITTEN_OFF_SETTLED_STATUS(hibHighmarkBaseSropDomain.getWrittenOffSetteldStatusSe() == null ? "" : hibHighmarkBaseSropDomain.getWrittenOffSetteldStatusSe()); 
						chmbaseSRespType2.setSM_CASH_LIMIT(hibHighmarkBaseSropDomain.getCashLimitSe() == null ? "" : hibHighmarkBaseSropDomain.getCashLimitSe()); 
						chmbaseSRespType2.setSM_ACTUAL_PAYMENT(hibHighmarkBaseSropDomain.getActualPaymentSe() == null ? "" : hibHighmarkBaseSropDomain.getActualPaymentSe()); 
						chmbaseSRespType2.setSM_SECURITY_TYPE_LD(hibHighmarkBaseSropDomain.getSecurityTypeSe() == null ? "" : hibHighmarkBaseSropDomain.getSecurityTypeSe()); 
						chmbaseSRespType2.setSM_OWNER_NAME_LD(hibHighmarkBaseSropDomain.getOwnerNameSe() == null ? "" : hibHighmarkBaseSropDomain.getOwnerNameSe()); 
						chmbaseSRespType2.setSM_SECURITY_VALUE_LD(hibHighmarkBaseSropDomain.getSecurityValueSe() == null ? "" : hibHighmarkBaseSropDomain.getSecurityValueSe()); 
						chmbaseSRespType2.setSM_DATE_OF_VALUE_LD(hibHighmarkBaseSropDomain.getDateOfValueSe() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateOfValueSe())); 
						chmbaseSRespType2.setSM_SECURITY_CHARGE_LD(hibHighmarkBaseSropDomain.getSecurityChargeSe() == null ? "" : hibHighmarkBaseSropDomain.getSecurityChargeSe()); 
						chmbaseSRespType2.setSM_PROPERTY_ADDRESS_LD(hibHighmarkBaseSropDomain.getPropertyAddressSe() == null ? "" : hibHighmarkBaseSropDomain.getPropertyAddressSe()); 
						chmbaseSRespType2.setSM_AUTOMOBILE_TYPE_LD(hibHighmarkBaseSropDomain.getAutomobileTypeSe() == null ? "" : hibHighmarkBaseSropDomain.getAutomobileTypeSe()); 
						chmbaseSRespType2.setSM_YEAR_OF_MANUFACTURE_LD(hibHighmarkBaseSropDomain.getYearOfManufactureSe() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getYearOfManufactureSe())); 
						chmbaseSRespType2.setSM_REGISTRATION_NUMBER_LD(hibHighmarkBaseSropDomain.getRegistrationNumberSe() == null ? "" : hibHighmarkBaseSropDomain.getRegistrationNumberSe()); 
						chmbaseSRespType2.setSM_ENGINE_NUMBER_LD(hibHighmarkBaseSropDomain.getEngineNumberSe() == null ? "" : hibHighmarkBaseSropDomain.getEngineNumberSe()); 
						chmbaseSRespType2.setSM_CHASIS_NUMBER_LD(hibHighmarkBaseSropDomain.getChassisNumberSe() == null ? "" : hibHighmarkBaseSropDomain.getChassisNumberSe()); 
						chmbaseSRespType2.setSM_LINKED_ACCT_NUMBER(hibHighmarkBaseSropDomain.getAccountNumberSeLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountNumberSeLn()); 
						chmbaseSRespType2.setSM_LINKED_CREDIT_GUARANTOR(hibHighmarkBaseSropDomain.getCreditGuarantorSeLn() == null ? "" : hibHighmarkBaseSropDomain.getCreditGuarantorSeLn()); 
						chmbaseSRespType2.setSM_LINKED_ACCT_TYPE(hibHighmarkBaseSropDomain.getAccountTypeSeLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountTypeSeLn()); 
						chmbaseSRespType2.setSM_LINKED_DATE_REPORTED(hibHighmarkBaseSropDomain.getDateReportedSeLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateReportedSeLn())); 
						chmbaseSRespType2.setSM_LINKED_OWNERSHIP_IND(hibHighmarkBaseSropDomain.getOwnershipIndicatorSeLn() == null ? "" : hibHighmarkBaseSropDomain.getOwnershipIndicatorSeLn()); 
						chmbaseSRespType2.setSM_LINKED_ACCOUNT_STATUS(hibHighmarkBaseSropDomain.getAccountStatusSeLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountStatusSeLn()); 
						chmbaseSRespType2.setSM_LINKED_DISBURSED_AMT(hibHighmarkBaseSropDomain.getDisbursedAmountSeLn() == null ? "" : hibHighmarkBaseSropDomain.getDisbursedAmountSeLn()); 
						chmbaseSRespType2.setSM_LINKED_DISBURSED_DT(hibHighmarkBaseSropDomain.getDisbursedDateSeLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDisbursedDateSeLn())); 
						chmbaseSRespType2.setSM_LINKED_LAST_PAYMENT_DATE(hibHighmarkBaseSropDomain.getLastPaymentDateSeLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getLastPaymentDateSeLn())); 
						chmbaseSRespType2.setSM_LINKED_CLOSED_DATE(hibHighmarkBaseSropDomain.getClosedDateSeLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getClosedDateSeLn())); 
						chmbaseSRespType2.setSM_LINKED_INSTALLMENT_AMT(hibHighmarkBaseSropDomain.getInstallmentAmountSeLn() == null ? "" : hibHighmarkBaseSropDomain.getInstallmentAmountSeLn()); 
						chmbaseSRespType2.setSM_LINKED_OVERDUE_AMT(hibHighmarkBaseSropDomain.getOverdueAmountSeLn() == null ? "" : hibHighmarkBaseSropDomain.getOverdueAmountSeLn()); 
						chmbaseSRespType2.setSM_LINKED_WRITE_OFF_AMT(hibHighmarkBaseSropDomain.getWriteOffAmountSeLn() == null ? "" : hibHighmarkBaseSropDomain.getWriteOffAmountSeLn()); 
						chmbaseSRespType2.setSM_LINKED_CURRENT_BAL(hibHighmarkBaseSropDomain.getCurrentBalanceSeLn() == null ? "" : hibHighmarkBaseSropDomain.getCurrentBalanceSeLn()); 
						chmbaseSRespType2.setSM_LINKED_CREDIT_LIMIT(hibHighmarkBaseSropDomain.getCreditLimitSeLn() == null ? "" : hibHighmarkBaseSropDomain.getCreditLimitSeLn()); 
						chmbaseSRespType2.setSM_LINKED_ACCOUNT_REMARKS(hibHighmarkBaseSropDomain.getAccountRemarksSeLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountRemarksSeLn()); 
						chmbaseSRespType2.setSM_LINKED_FREQUENCY(hibHighmarkBaseSropDomain.getFrequencySeLn() == null ? "" : hibHighmarkBaseSropDomain.getFrequencySeLn()); 
						chmbaseSRespType2.setSM_LINKED_SECURITY_STATUS(hibHighmarkBaseSropDomain.getSecurityStatusSeLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityStatusSeLn()); 
						chmbaseSRespType2.setSM_LINKED_ORIGINAL_TERM(hibHighmarkBaseSropDomain.getOriginalTermSeLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getOriginalTermSeLn())); 
						chmbaseSRespType2.setSM_LINKED_TERM_TO_MATURITY(hibHighmarkBaseSropDomain.getTermToMaturitySeLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getTermToMaturitySeLn())); 
						chmbaseSRespType2.setSM_LINKED_ACCT_IN_DISPUTE(hibHighmarkBaseSropDomain.getAccountInDisputeSeLn() == null ? "" : hibHighmarkBaseSropDomain.getAccountInDisputeSeLn()); 
						chmbaseSRespType2.setSM_LINKED_SETTLEMENT_AMT(hibHighmarkBaseSropDomain.getSettlementAmtSeLn() == null ? "" : hibHighmarkBaseSropDomain.getSettlementAmtSeLn()); 
						chmbaseSRespType2.setSM_LINKED_PRNPAL_WRITE_OFF_AMT(hibHighmarkBaseSropDomain.getPrincipalWriteOffAmtSeLn() == null ? "" : hibHighmarkBaseSropDomain.getPrincipalWriteOffAmtSeLn()); 
						chmbaseSRespType2.setSM_LINKED_REPAYMENT_TENURE(hibHighmarkBaseSropDomain.getRepaymentTenureSeLn() == null ? "" : hibHighmarkBaseSropDomain.getRepaymentTenureSeLn()); 
						chmbaseSRespType2.setSM_LINKED_INTEREST_RATE(hibHighmarkBaseSropDomain.getInterestRateSeLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getInterestRateSeLn())); 
						chmbaseSRespType2.setSM_LINKED_SUIT_F_WILFUL_DFLT(hibHighmarkBaseSropDomain.getSuitFiledWilfulDefaultSeLn() == null ? "" : hibHighmarkBaseSropDomain.getSuitFiledWilfulDefaultSeLn()); 
						chmbaseSRespType2.setSM_LINKED_WRTN_OFF_SETTLD_STAT(hibHighmarkBaseSropDomain.getWrittenOffSetteldStatusSeLn() == null ? "" : hibHighmarkBaseSropDomain.getWrittenOffSetteldStatusSeLn()); 
						chmbaseSRespType2.setSM_LINKED_CASH_LIMIT(hibHighmarkBaseSropDomain.getCashLimitSeLn() == null ? "" : hibHighmarkBaseSropDomain.getCashLimitSeLn()); 
						chmbaseSRespType2.setSM_LINKED_ACTUAL_PAYMENT(hibHighmarkBaseSropDomain.getActualPaymentSeLn() == null ? "" : hibHighmarkBaseSropDomain.getActualPaymentSeLn()); 
						chmbaseSRespType2.setSM_SECURITY_TYPE_LA(hibHighmarkBaseSropDomain.getSecurityTypeSeLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityTypeSeLn()); 
						chmbaseSRespType2.setSM_OWNER_NAME_LA(hibHighmarkBaseSropDomain.getOwnerNameSeLn() == null ? "" : hibHighmarkBaseSropDomain.getOwnerNameSeLn()); 
						chmbaseSRespType2.setSM_SECURITY_VALUE_LA(hibHighmarkBaseSropDomain.getSecurityValueSeLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityValueSeLn()); 
						chmbaseSRespType2.setSM_DATE_OF_VALUE_LA(hibHighmarkBaseSropDomain.getDateOfValueSeLn() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getDateOfValueSeLn())); 
						chmbaseSRespType2.setSM_SECURITY_CHARGE_LA(hibHighmarkBaseSropDomain.getSecurityChargeSeLn() == null ? "" : hibHighmarkBaseSropDomain.getSecurityChargeSeLn()); 
						chmbaseSRespType2.setSM_PROPERTY_ADDRESS_LA(hibHighmarkBaseSropDomain.getPropertyAddressSeLn() == null ? "" : hibHighmarkBaseSropDomain.getPropertyAddressSeLn()); 
						chmbaseSRespType2.setSM_AUTOMOBILE_TYPE_LA(hibHighmarkBaseSropDomain.getAutomobileTypeSeLn() == null ? "" : hibHighmarkBaseSropDomain.getAutomobileTypeSeLn()); 
						chmbaseSRespType2.setSM_YEAR_OF_MANUFACTURE_LA(hibHighmarkBaseSropDomain.getYearOfManufactureSeLn() == null ? "" : Integer.toString(hibHighmarkBaseSropDomain.getYearOfManufactureSeLn())); 
						chmbaseSRespType2.setSM_REGISTRATION_NUMBER_LA(hibHighmarkBaseSropDomain.getRegistrationNumberSeLn() == null ? "" : hibHighmarkBaseSropDomain.getRegistrationNumberSeLn()); 
						chmbaseSRespType2.setSM_ENGINE_NUMBER_LA(hibHighmarkBaseSropDomain.getEngineNumberSeLn() == null ? "" : hibHighmarkBaseSropDomain.getEngineNumberSeLn()); 
						chmbaseSRespType2.setSM_CHASIS_NUMBER_LA(hibHighmarkBaseSropDomain.getChassisNumberSeLn() == null ? "" : hibHighmarkBaseSropDomain.getChassisNumberSeLn()); 
						chmbaseSRespType2.setSCORE_TYPE(hibHighmarkBaseSropDomain.getScoreType() == null ? "" : hibHighmarkBaseSropDomain.getScoreType()); 
						chmbaseSRespType2.setSCORE_VERSION(hibHighmarkBaseSropDomain.getScoreVersion() == null ? "" : hibHighmarkBaseSropDomain.getScoreVersion()); 
						chmbaseSRespType2.setSCORE_VALUE(hibHighmarkBaseSropDomain.getScoreValue() == null ? "" : hibHighmarkBaseSropDomain.getScoreValue().toString()); 
						chmbaseSRespType2.setSCORE_FACTORS(hibHighmarkBaseSropDomain.getScoreFactors() == null ? "" : hibHighmarkBaseSropDomain.getScoreFactors()); 
						chmbaseSRespType2.setSCORE_COMMENTS(hibHighmarkBaseSropDomain.getScoreComments() == null ? "" : hibHighmarkBaseSropDomain.getScoreComments()); 
						chmbaseSRespType2.setMEMBER_NAME(hibHighmarkBaseSropDomain.getMemberName() == null ? "" : hibHighmarkBaseSropDomain.getMemberName()); 
						chmbaseSRespType2.setINQUIRY_DATE(hibHighmarkBaseSropDomain.getInquiryDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getInquiryDate())); 
						chmbaseSRespType2.setPURPOSE(hibHighmarkBaseSropDomain.getPurpose() == null ? "" : hibHighmarkBaseSropDomain.getPurpose()); 
						chmbaseSRespType2.setOWNERSHIP_TYPE(hibHighmarkBaseSropDomain.getOwnershipType() == null ? "" : hibHighmarkBaseSropDomain.getOwnershipType()); 
						chmbaseSRespType2.setAMOUNT(hibHighmarkBaseSropDomain.getAmount() == null ? "" : hibHighmarkBaseSropDomain.getAmount()); 
						chmbaseSRespType2.setREMARK(hibHighmarkBaseSropDomain.getRemark() == null ? "" : hibHighmarkBaseSropDomain.getRemark()); 
						chmbaseSRespType2.setCOMMENT_TEXT(hibHighmarkBaseSropDomain.getCommentText() == null ? "" : hibHighmarkBaseSropDomain.getCommentText()); 
						chmbaseSRespType2.setCOMMENT_DATE(hibHighmarkBaseSropDomain.getCommentDate() == null ? "" : sdf1.format(hibHighmarkBaseSropDomain.getCommentDate())); 
						chmbaseSRespType2.setBUREAU_COMMENT(hibHighmarkBaseSropDomain.getBureauComment() == null ? "" : hibHighmarkBaseSropDomain.getBureauComment()); 
						chmbaseSRespType2.setALERT_TYPE(hibHighmarkBaseSropDomain.getAlertType() == null ? "" : hibHighmarkBaseSropDomain.getAlertType()); 
						chmbaseSRespType2.setALERT_DESC(hibHighmarkBaseSropDomain.getAlertDescription() == null ? "" : hibHighmarkBaseSropDomain.getAlertDescription()); 
						chmbaseSRespType2.setOUTPUT_WRITE_FLAG(hibHighmarkBaseSropDomain.getOutputWriteFlag() == null ? "" : hibHighmarkBaseSropDomain.getOutputWriteFlag()); 
						chmbaseSRespType2.setOUTPUT_WRITE_TIME(hibHighmarkBaseSropDomain.getOutputWritetime() == null ? "" : hibHighmarkBaseSropDomain.getOutputWritetime()); 
						chmbaseSRespType2.setOUTPUT_READ_TIME(hibHighmarkBaseSropDomain.getOutputReadTime() == null ? "" : hibHighmarkBaseSropDomain.getOutputReadTime());

						ChmbaseSRespType chmbaseSRespType = new ChmbaseSRespType();
						chmbaseSRespType.setCHM_BASE_SROP_DOMAIN_LIST1(chmbaseSRespType1);
						chmbaseSRespType.setCHM_BASE_SROP_DOMAIN_LIST2(chmbaseSRespType2);
						chmbaseSRespTypeList.add(chmbaseSRespType);
						
					}
					responseJSONType.setCHM_BASE_SROP_DOMAIN_LIST(chmbaseSRespTypeList.toArray(new ChmbaseSRespType[chmbaseSRespTypeList.size()]));
				}

			}

		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper >> chmBaseSropMapping");
		return responseJSONType;
	}

	public ResponseJSONType experianEropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> experianEropMapping ");
		try {
			ExperianEropDomainObject experianEropDomainObject = (ExperianEropDomainObject) responseJsonObject;
			if (experianEropDomainObject != null) {
				List<ExperianEROPDomain> experianEropDomainList = experianEropDomainObject.getExperianEropDomainList();
				if (experianEropDomainList != null&& experianEropDomainList.size() > 0) {
					List<ExperianERespType> experianERespTypeList = new ArrayList<ExperianERespType>();
					
					for (ExperianEROPDomain experianEROPDomain : experianEropDomainList) {
						ExperianERespType experianERespType = new ExperianERespType();
						experianERespType.setSRNO(experianEROPDomain.getSrno() == null ? "" : Long.toString(experianEROPDomain.getSrno())); 
						experianERespType.setSOURCE_NAME(experianEROPDomain.getSourceName() == null ? "" : experianEROPDomain.getSourceName()); 
						experianERespType.setMEMBER_REFERENCE_NUMBER(experianEROPDomain.getMemberrefrenceNumber() == null ? "" : experianEROPDomain.getMemberrefrenceNumber()); 
						experianERespType.setENQUIRY_DATE(experianEROPDomain.getEnquiryDate() == null ? "" :sdf2.format(experianEROPDomain.getEnquiryDate())); 
						experianERespType.setSYSTEMCODE(experianEROPDomain.getSystemCode() == null ? "" : experianEROPDomain.getSystemCode()); 
						experianERespType.setMESSAGETEXT(experianEROPDomain.getMessageText() == null ? "" : experianEROPDomain.getMessageText()); 
						experianERespType.setREPORTDATE(experianEROPDomain.getReportDate() == null ? "" : experianEROPDomain.getReportDate()); 
						experianERespType.setREPORTTIME(experianEROPDomain.getReportTime() == null ? "" : experianEROPDomain.getReportTime()); 
						experianERespType.setOUTPUT_WRITE_FLAG(experianEROPDomain.getOutputWriteFlag_() == null ? "" : experianEROPDomain.getOutputWriteFlag_()); 
						experianERespType.setOUTPUT_WRITE_DATE(experianEROPDomain.getOutputWriteDate_() == null ? "" : experianEROPDomain.getOutputWriteDate_()); 
						experianERespType.setOUTPUT_READ_TIME(experianEROPDomain.getOutputReadTime_() == null ? "" : experianEROPDomain.getOutputReadTime_()); 
						experianERespTypeList.add(experianERespType);
					
					}
					responseJSONType.setEXPERIAN_EROP_DOMAIN_LIST(experianERespTypeList.toArray(new ExperianERespType[experianERespTypeList.size()]));
				}

			}

		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** experianEropMapping >>");
		return responseJSONType;
	}

	public ResponseJSONType experianSropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> experianSropMapping ");
		try {
			ExperianSropDomainObject experianSropDomainObject = (ExperianSropDomainObject) responseJsonObject;
			if (experianSropDomainObject != null) {
				List<HibExperianSropDomain> experianSropDomainList = experianSropDomainObject.getExperianSropDomainList();
				if (experianSropDomainList != null&& experianSropDomainList.size() > 0) {
					List<ExperianSRespType> experianSRespTypeList = new ArrayList<ExperianSRespType>();
					for (HibExperianSropDomain hibExperianSropDomain : experianSropDomainList) {

						
						ExperianSRespType1 experianSRespType1 = new ExperianSRespType1();
						ExperianSRespType2 experianSRespType2 = new ExperianSRespType2();
						experianSRespType1.setSRNO(hibExperianSropDomain.getSrNo() == null ? "" : Integer.toString(hibExperianSropDomain.getSrNo())); 
						experianSRespType1.setSOA_SOURCE_NAME(hibExperianSropDomain.getSoaSourceName() == null ? "" : hibExperianSropDomain.getSoaSourceName()); 
						experianSRespType1.setMEMBER_REFERENCE_NUMBER(hibExperianSropDomain.getMemberReferenceNumber() == null ? "" : hibExperianSropDomain.getMemberReferenceNumber()); 
						experianSRespType1.setENQUIRY_DATE(hibExperianSropDomain.getEnquiryDate() == null ? "" :sdf2.format(hibExperianSropDomain.getEnquiryDate())); 
						experianSRespType1.setSYSTEMCODE_HEADER(hibExperianSropDomain.getSystemcodeHeader() == null ? "" : hibExperianSropDomain.getSystemcodeHeader()); 
						experianSRespType1.setMESSAGETEXT_HEADER(hibExperianSropDomain.getMessageTextHeader() == null ? "" : hibExperianSropDomain.getMessageTextHeader()); 
						experianSRespType1.setREPORTDATE(hibExperianSropDomain.getReportdate() == null ? "" : sdf2.format(hibExperianSropDomain.getReportdate())); 
						experianSRespType1.setREPORTIME_HEADER(hibExperianSropDomain.getReportimeHeader() == null ? "" : hibExperianSropDomain.getReportimeHeader()); 
						experianSRespType1.setUSERMESSAGETEXT(hibExperianSropDomain.getUserMessageText() == null ? "" : hibExperianSropDomain.getUserMessageText()); 
						experianSRespType1.setENQUIRYUSERNAME(hibExperianSropDomain.getEnquiryUsername() == null ? "" : hibExperianSropDomain.getEnquiryUsername()); 
						experianSRespType1.setCREDIT_PROFILE_REPORT_DATE(hibExperianSropDomain.getCreditProfileReportDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCreditProfileReportDate())); 
						experianSRespType1.setCREDIT_PROFILE_REPORT_TIME(hibExperianSropDomain.getCreditProfileReportTime() == null ? "" : hibExperianSropDomain.getCreditProfileReportTime()); 
						experianSRespType1.setVERSION(hibExperianSropDomain.getVersion() == null ? "" : hibExperianSropDomain.getVersion()); 
						experianSRespType1.setREPORTNUMBER(hibExperianSropDomain.getReportNumber() == null ? "" : hibExperianSropDomain.getReportNumber()); 
						experianSRespType1.setSUBSCRIBER(hibExperianSropDomain.getSubscriber() == null ? "" : hibExperianSropDomain.getSubscriber()); 
						experianSRespType1.setSUBSCRIBERNAME(hibExperianSropDomain.getSubscriberName() == null ? "" : hibExperianSropDomain.getSubscriberName()); 
						experianSRespType1.setENQUIRYREASON(hibExperianSropDomain.getEnquiryReason() == null ? "" : hibExperianSropDomain.getEnquiryReason()); 
						experianSRespType1.setFINANCEPURPOSE(hibExperianSropDomain.getFinancePurpose() == null ? "" : hibExperianSropDomain.getFinancePurpose()); 
						experianSRespType1.setAMOUNTFINANCED(hibExperianSropDomain.getAmountFinanced() == null ? "" : Integer.toString(hibExperianSropDomain.getAmountFinanced())); 
						experianSRespType1.setDURATIONOFAGREEMENT(hibExperianSropDomain.getDurationOfAgreement() == null ? "" : Integer.toString(hibExperianSropDomain.getDurationOfAgreement())); 
						experianSRespType1.setLASTNAME(hibExperianSropDomain.getLastname() == null ? "" : hibExperianSropDomain.getLastname()); 
						experianSRespType1.setFIRSTNAME(hibExperianSropDomain.getFirstname() == null ? "" : hibExperianSropDomain.getFirstname()); 
						experianSRespType1.setMIDDLENAME1(hibExperianSropDomain.getMiddleName1() == null ? "" : hibExperianSropDomain.getMiddleName1()); 
						experianSRespType1.setMIDDLENAME2(hibExperianSropDomain.getMiddleName2() == null ? "" : hibExperianSropDomain.getMiddleName2()); 
						experianSRespType1.setMIDDLENAME3(hibExperianSropDomain.getMiddleName3() == null ? "" : hibExperianSropDomain.getMiddleName3()); 
						experianSRespType1.setGENDERCODE(hibExperianSropDomain.getGenderCode() == null ? "" : hibExperianSropDomain.getGenderCode()); 
						experianSRespType1.setGENDER(hibExperianSropDomain.getGender() == null ? "" : hibExperianSropDomain.getGender()); 
						experianSRespType1.setINCOME_TAX_PAN_APP(hibExperianSropDomain.getIncomeTaxPanApp() == null ? "" : hibExperianSropDomain.getIncomeTaxPanApp()); 
						experianSRespType1.setPAN_ISSUE_DATE_APP(hibExperianSropDomain.getPanIssueDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getPanIssueDateApp())); 
						experianSRespType1.setPAN_EXP_DATE_APP(hibExperianSropDomain.getPanExpDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getPanExpDateApp())); 
						experianSRespType1.setPASSPORT_NUMBER_APP(hibExperianSropDomain.getPassportNumberApp() == null ? "" : hibExperianSropDomain.getPassportNumberApp()); 
						experianSRespType1.setPASSPORT_ISSUE_DATE_APP(hibExperianSropDomain.getPassportIssueDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getPassportIssueDateApp())); 
						experianSRespType1.setPASSPORT_EXP_DATE_APP(hibExperianSropDomain.getPassportExpDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getPassportExpDateApp())); 
						experianSRespType1.setVOTER_IDENTITY_CARD_APP(hibExperianSropDomain.getVoterIdentityCardApp() == null ? "" : hibExperianSropDomain.getVoterIdentityCardApp()); 
						experianSRespType1.setVOTER_ID_ISSUE_DATE_APP(hibExperianSropDomain.getVoterIdIssueDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getVoterIdIssueDateApp())); 
						experianSRespType1.setVOTER_ID_EXP_DATE_APP(hibExperianSropDomain.getVoterIdExpDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getVoterIdExpDateApp())); 
						experianSRespType1.setDRIVER_LICENSE_NUMBER_APP(hibExperianSropDomain.getDriverLicenseNumberApp() == null ? "" : hibExperianSropDomain.getDriverLicenseNumberApp()); 
						experianSRespType1.setDRIVER_LICENSE_ISSUE_DATE_APP(hibExperianSropDomain.getDriverLicenseIssueDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getDriverLicenseIssueDateApp())); 
						experianSRespType1.setDRIVER_LICENSE_EXP_DATE_APP(hibExperianSropDomain.getDriverLicenseExpDateApp() == null ? "" :sdf2.format( hibExperianSropDomain.getDriverLicenseExpDateApp())); 
						experianSRespType1.setRATION_CARD_NUMBER_APP(hibExperianSropDomain.getRationCardNumberApp() == null ? "" : hibExperianSropDomain.getRationCardNumberApp()); 
						experianSRespType1.setRATION_CARD_ISSUE_DATE_APP(hibExperianSropDomain.getRationCardIssueDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getRationCardIssueDateApp())); 
						experianSRespType1.setRATION_CARD_EXP_DATE_APP(hibExperianSropDomain.getRationCardExpDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getRationCardExpDateApp())); 
						experianSRespType1.setUNIVERSAL_ID_NUMBER_APP(hibExperianSropDomain.getUniversalIdNumberApp() == null ? "" : hibExperianSropDomain.getUniversalIdNumberApp()); 
						experianSRespType1.setUNIVERSAL_ID_ISSUE_DATE_APP(hibExperianSropDomain.getUniversalIdIssueDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getUniversalIdIssueDateApp())); 
						experianSRespType1.setUNIVERSAL_ID_EXP_DATE_APP(hibExperianSropDomain.getUniversalIdExpDateApp() == null ? "" : sdf2.format(hibExperianSropDomain.getUniversalIdExpDateApp())); 
						experianSRespType1.setDATEOFBIRTHAPPLICANT(hibExperianSropDomain.getDateOfBirthApplicant() == null ? "" : sdf2.format(hibExperianSropDomain.getDateOfBirthApplicant())); 
						experianSRespType1.setTELEPHONENUMBERAPPLICANT1ST(hibExperianSropDomain.getTelephoneNumberApplicant1st() == null ? "" : hibExperianSropDomain.getTelephoneNumberApplicant1st()); 
						experianSRespType1.setTELEEXTENSION_APP(hibExperianSropDomain.getTeleExtensionApp() == null ? "" : hibExperianSropDomain.getTeleExtensionApp()); 
						experianSRespType1.setTELETYPE_APP(hibExperianSropDomain.getTeleTypeApp() == null ? "" : hibExperianSropDomain.getTeleTypeApp()); 
						experianSRespType1.setMOBILEPHONENUMBER(hibExperianSropDomain.getMobilePhoneNumber() == null ? "" : hibExperianSropDomain.getMobilePhoneNumber()); 
						experianSRespType1.setEMAILID_APP(hibExperianSropDomain.getEmailIdApp() == null ? "" : hibExperianSropDomain.getEmailIdApp()); 
						experianSRespType1.setINCOME(hibExperianSropDomain.getIncome() == null ? "" : hibExperianSropDomain.getIncome()); 
						experianSRespType1.setMARITALSTATUSCODE(hibExperianSropDomain.getMaritalStatusCode() == null ? "" : hibExperianSropDomain.getMaritalStatusCode()); 
						experianSRespType1.setMARITALSTATUS(hibExperianSropDomain.getMaritalStatus() == null ? "" : hibExperianSropDomain.getMaritalStatus()); 
						experianSRespType1.setEMPLOYMENTSTATUS(hibExperianSropDomain.getEmploymentStatus() == null ? "" : hibExperianSropDomain.getEmploymentStatus()); 
						experianSRespType1.setDATEWITHEMPLOYER(hibExperianSropDomain.getDateWithEmployer() == null ? "" : sdf2.format(hibExperianSropDomain.getDateWithEmployer())); 
						experianSRespType1.setNUMBEROFMAJORCREDITCARDHELD(hibExperianSropDomain.getNumberOfMajorCreditCardHeld() == null ? "" : hibExperianSropDomain.getNumberOfMajorCreditCardHeld()); 
						experianSRespType1.setFLATNOPLOTNOHOUSENO(hibExperianSropDomain.getFlatNoPlotNoHouseNo() == null ? "" : hibExperianSropDomain.getFlatNoPlotNoHouseNo()); 
						experianSRespType1.setBLDGNOSOCIETYNAME(hibExperianSropDomain.getBldgNoSocietyName() == null ? "" : hibExperianSropDomain.getBldgNoSocietyName()); 
						experianSRespType1.setROADNONAMEAREALOCALITY(hibExperianSropDomain.getRoadNoNameAreaLocality() == null ? "" : hibExperianSropDomain.getRoadNoNameAreaLocality()); 
						experianSRespType1.setCITY(hibExperianSropDomain.getCity() == null ? "" : hibExperianSropDomain.getCity()); 
						experianSRespType1.setLANDMARK(hibExperianSropDomain.getLandmark() == null ? "" : hibExperianSropDomain.getLandmark()); 
						experianSRespType1.setSTATE(hibExperianSropDomain.getState() == null ? "" : hibExperianSropDomain.getState()); 
						experianSRespType1.setPINCODE(hibExperianSropDomain.getPincode() == null ? "" : hibExperianSropDomain.getPincode()); 
						experianSRespType1.setCOUNTRY_CODE(hibExperianSropDomain.getCountryCode() == null ? "" : hibExperianSropDomain.getCountryCode()); 
						experianSRespType1.setADD_FLATNOPLOTNOHOUSENO(hibExperianSropDomain.getAddFlatNoPlotNoHouseNo() == null ? "" : hibExperianSropDomain.getAddFlatNoPlotNoHouseNo()); 
						experianSRespType1.setADD_BLDGNOSOCIETYNAME(hibExperianSropDomain.getAddBldgNoSocietyName() == null ? "" : hibExperianSropDomain.getAddBldgNoSocietyName()); 
						experianSRespType1.setADD_ROADNONAMEAREALOCALITY(hibExperianSropDomain.getAddRoadNoNameAreaLocality() == null ? "" : hibExperianSropDomain.getAddRoadNoNameAreaLocality()); 
						experianSRespType1.setADD_CITY(hibExperianSropDomain.getAddCity() == null ? "" : hibExperianSropDomain.getAddCity()); 
						experianSRespType1.setADD_LANDMARK(hibExperianSropDomain.getAddLandmark() == null ? "" : hibExperianSropDomain.getAddLandmark()); 
						experianSRespType1.setADD_STATE(hibExperianSropDomain.getAddState() == null ? "" : hibExperianSropDomain.getAddState()); 
						experianSRespType1.setADD_PINCODE(hibExperianSropDomain.getAddPincode() == null ? "" : hibExperianSropDomain.getAddPincode()); 
						experianSRespType1.setADD_COUNTRYCODE(hibExperianSropDomain.getAddCountrycode() == null ? "" : hibExperianSropDomain.getAddCountrycode()); 
						experianSRespType1.setCREDITACCOUNTTOTAL(hibExperianSropDomain.getCreditAccountTotal() == null ? "" : hibExperianSropDomain.getCreditAccountTotal()); 
						experianSRespType1.setCREDITACCOUNTACTIVE(hibExperianSropDomain.getCreditAccountActive() == null ? "" : hibExperianSropDomain.getCreditAccountActive()+""); 
						experianSRespType1.setCREDITACCOUNTDEFAULT(hibExperianSropDomain.getCreditAccountDefault() == null ? "" : hibExperianSropDomain.getCreditAccountDefault()+""); 
						experianSRespType1.setCREDITACCOUNTCLOSED(hibExperianSropDomain.getCreditAccountClosed() == null ? "" : hibExperianSropDomain.getCreditAccountClosed()+""); 
						experianSRespType1.setCADSUITFILEDCURRENTBALANCE(hibExperianSropDomain.getCadsuitfiledCurrentBalance() == null ? "" : hibExperianSropDomain.getCadsuitfiledCurrentBalance()+""); 
						experianSRespType1.setOUTSTANDINGBALANCESECURED(hibExperianSropDomain.getOutstandingBalanceSecured() == null ? "" : hibExperianSropDomain.getOutstandingBalanceSecured()); 
						experianSRespType1.setOUTSTANDINGBALSECUREDPER(hibExperianSropDomain.getOutstandingBalSecuredPer() == null ? "" : hibExperianSropDomain.getOutstandingBalSecuredPer()); 
						experianSRespType1.setOUTSTANDINGBALANCEUNSECURED(hibExperianSropDomain.getOutstandingBalanceUnsecured() == null ? "" : hibExperianSropDomain.getOutstandingBalanceUnsecured()); 
						experianSRespType1.setOUTSTANDINGBALUNSECPER(hibExperianSropDomain.getOutstandingBalUnsecPer() == null ? "" : hibExperianSropDomain.getOutstandingBalUnsecPer()); 
						experianSRespType1.setOUTSTANDINGBALANCEALL(hibExperianSropDomain.getOutstandingBalanceAll() == null ? "" : hibExperianSropDomain.getOutstandingBalanceAll()); 
						experianSRespType1.setIDENTIFICATIONNUMBER(hibExperianSropDomain.getIdentificationNumber() == null ? "" : hibExperianSropDomain.getIdentificationNumber()); 
						experianSRespType1.setCAISSUBSCRIBERNAME(hibExperianSropDomain.getCaisSubscriberName() == null ? "" : hibExperianSropDomain.getCaisSubscriberName()); 
						experianSRespType1.setACCOUNTNUMBER(hibExperianSropDomain.getAccountNumber() == null ? "" : hibExperianSropDomain.getAccountNumber()); 
						experianSRespType1.setPORTFOLIOTYPE(hibExperianSropDomain.getPortfolioType() == null ? "" : hibExperianSropDomain.getPortfolioType()); 
						experianSRespType1.setACCOUNTTYPE_CODE(hibExperianSropDomain.getAccountTypeCode() == null ? "" : hibExperianSropDomain.getAccountTypeCode()); 
						experianSRespType1.setACCOUNTTYPE(hibExperianSropDomain.getAccountType() == null ? "" : hibExperianSropDomain.getAccountType()); 
						experianSRespType1.setOPENDATE(hibExperianSropDomain.getOpenDate() == null ? "" : sdf2.format(hibExperianSropDomain.getOpenDate())); 
						experianSRespType1.setHIGHESTCREDITORORGNLOANAMT(hibExperianSropDomain.getHighestCreditOrOrgnLoanAmt() == null ? "" : hibExperianSropDomain.getHighestCreditOrOrgnLoanAmt()); 
						experianSRespType1.setTERMSDURATION(hibExperianSropDomain.getTermsDuration() == null ? "" : hibExperianSropDomain.getTermsDuration()); 
						experianSRespType1.setTERMSFREQUENCY(hibExperianSropDomain.getTermsFrequency() == null ? "" : hibExperianSropDomain.getTermsFrequency()); 
						experianSRespType1.setSCHEDULEDMONTHLYPAYAMT(hibExperianSropDomain.getScheduledMonthlyPayamt() == null ? "" : hibExperianSropDomain.getScheduledMonthlyPayamt()); 
						experianSRespType1.setACCOUNTSTATUS_CODE(hibExperianSropDomain.getAccountStatusCode() == null ? "" : hibExperianSropDomain.getAccountStatusCode()); 
						experianSRespType1.setACCOUNTSTATUS(hibExperianSropDomain.getAccountStatus() == null ? "" : hibExperianSropDomain.getAccountStatus()); 
						experianSRespType1.setPAYMENTRATING(hibExperianSropDomain.getPaymentRating() == null ? "" : hibExperianSropDomain.getPaymentRating()); 
						experianSRespType1.setPAYMENTHISTORYPROFILE(hibExperianSropDomain.getPaymentHistoryProfile() == null ? "" : hibExperianSropDomain.getPaymentHistoryProfile()); 
						experianSRespType1.setSPECIALCOMMENT(hibExperianSropDomain.getSpecialComment() == null ? "" : hibExperianSropDomain.getSpecialComment()); 
						experianSRespType1.setCURRENTBALANCE_TL(hibExperianSropDomain.getCurrentBalanceTl() == null ? "" : Integer.toString(hibExperianSropDomain.getCurrentBalanceTl())); 
						experianSRespType1.setAMOUNTPASTDUE_TL(hibExperianSropDomain.getAmountPastdueTl() == null ? "" : Integer.toString(hibExperianSropDomain.getAmountPastdueTl())); 
						experianSRespType1.setORIGINALCHARGEOFFAMOUNT(hibExperianSropDomain.getOriginalChargeOffAmount() == null ? "" : Integer.toString(hibExperianSropDomain.getOriginalChargeOffAmount())); 
						experianSRespType1.setDATEREPORTED(hibExperianSropDomain.getDateReported() == null ? "" : sdf2.format(hibExperianSropDomain.getDateReported())); 
						experianSRespType1.setDATEOFFIRSTDELINQUENCY(hibExperianSropDomain.getDateOfFirstDelinquency() == null ? "" : sdf2.format(hibExperianSropDomain.getDateOfFirstDelinquency())); 
						experianSRespType1.setDATECLOSED(hibExperianSropDomain.getDateClosed() == null ? "" : sdf2.format(hibExperianSropDomain.getDateClosed())); 
						experianSRespType1.setDATEOFLASTPAYMENT(hibExperianSropDomain.getDateOfLastPayment() == null ? "" : sdf2.format(hibExperianSropDomain.getDateOfLastPayment())); 
						experianSRespType1.setSUITFWILFULDFTWRITTENOFFSTS(hibExperianSropDomain.getSuitfilledWillFullDefaultWrittenOffStatus() == null ? "" : hibExperianSropDomain.getSuitfilledWillFullDefaultWrittenOffStatus()); 
						experianSRespType1.setSUITFILEDWILFULDEF(hibExperianSropDomain.getSuitFiledWilfulDef() == null ? "" : hibExperianSropDomain.getSuitFiledWilfulDef()); 
						experianSRespType1.setWRITTENOFFSETTLEDSTATUS(hibExperianSropDomain.getWrittenOffSettledStatus() == null ? "" : hibExperianSropDomain.getWrittenOffSettledStatus()); 
						experianSRespType1.setVALUEOFCREDITSLASTMONTH(hibExperianSropDomain.getValueOfCreditsLastMonth() == null ? "" : hibExperianSropDomain.getValueOfCreditsLastMonth()); 
						experianSRespType1.setOCCUPATIONCODE(hibExperianSropDomain.getOccupationCode() == null ? "" : hibExperianSropDomain.getOccupationCode()); 
						experianSRespType1.setSATTLEMENTAMOUNT(hibExperianSropDomain.getSattlementAmount() == null ? "" : Integer.toString(hibExperianSropDomain.getSattlementAmount())); 
						experianSRespType1.setVALUEOFCOLATERAL(hibExperianSropDomain.getValueOfColateral() == null ? "" : Integer.toString(hibExperianSropDomain.getValueOfColateral())); 
						experianSRespType1.setTYPEOFCOLATERAL(hibExperianSropDomain.getTypeOfColateral() == null ? "" : hibExperianSropDomain.getTypeOfColateral()); 
						experianSRespType1.setWRITTENOFFAMTTOTAL(hibExperianSropDomain.getWrittenOffAmtTotal() == null ? "" : Integer.toString(hibExperianSropDomain.getWrittenOffAmtTotal())); 
						experianSRespType1.setWRITTENOFFAMTPRINCIPAL(hibExperianSropDomain.getWrittenOffAmtPrincipal() == null ? "" : Integer.toString(hibExperianSropDomain.getWrittenOffAmtPrincipal())); 
						experianSRespType1.setRATEOFINTEREST(hibExperianSropDomain.getRateOfInterest() == null ? "" : Integer.toString(hibExperianSropDomain.getRateOfInterest())); 
						experianSRespType1.setREPAYMENTTENURE(hibExperianSropDomain.getRepaymentTenure() == null ? "" : Integer.toString(hibExperianSropDomain.getRepaymentTenure())); 
						experianSRespType1.setPROMOTIONALRATEFLAG(hibExperianSropDomain.getPromotionalRateFlag() == null ? "" : hibExperianSropDomain.getPromotionalRateFlag()); 
						experianSRespType1.setCAISINCOME(hibExperianSropDomain.getCaisIncome() == null ? "" : Integer.toString(hibExperianSropDomain.getCaisIncome())); 
						experianSRespType1.setINCOMEINDICATOR(hibExperianSropDomain.getIncomeIndicator() == null ? "" : hibExperianSropDomain.getIncomeIndicator()); 
						experianSRespType1.setINCOMEFREQUENCYINDICATOR(hibExperianSropDomain.getIncomeFrequencyIndicator() == null ? "" : hibExperianSropDomain.getIncomeFrequencyIndicator()); 
						experianSRespType1.setDEFAULTSTATUSDATE(hibExperianSropDomain.getDefaultStatusDate() == null ? "" : sdf2.format(hibExperianSropDomain.getDefaultStatusDate())); 
						experianSRespType1.setLITIGATIONSTATUSDATE(hibExperianSropDomain.getLitigationStatusDate() == null ? "" : sdf2.format(hibExperianSropDomain.getLitigationStatusDate())); 
						experianSRespType1.setWRITEOFFSTATUSDATE(hibExperianSropDomain.getWriteOffStatusDate() == null ? "" : sdf2.format(hibExperianSropDomain.getWriteOffStatusDate())); 
						experianSRespType1.setCURRENCYCODE(hibExperianSropDomain.getCurrencyCode() == null ? "" : hibExperianSropDomain.getCurrencyCode()); 
						experianSRespType1.setSUBSCRIBERCOMMENTS(hibExperianSropDomain.getSubscriberComments() == null ? "" : hibExperianSropDomain.getSubscriberComments()); 
						experianSRespType1.setCONSUMERCOMMENTS(hibExperianSropDomain.getConsumerComments() == null ? "" : hibExperianSropDomain.getConsumerComments()); 
						experianSRespType1.setACCOUNTHOLDERTYPECODE(hibExperianSropDomain.getAccountHolderTypeCode() == null ? "" : hibExperianSropDomain.getAccountHolderTypeCode()); 
						experianSRespType1.setACCOUNTHOLDER_TYPENAME(hibExperianSropDomain.getAccountHolderTypeName() == null ? "" : hibExperianSropDomain.getAccountHolderTypeName()); 
						experianSRespType1.setYEAR_HIST(hibExperianSropDomain.getYearHist() == null ? "" : hibExperianSropDomain.getYearHist()); 
						experianSRespType1.setMONTH_HIST(hibExperianSropDomain.getMonthHist() == null ? "" : hibExperianSropDomain.getMonthHist()); 
						experianSRespType1.setDAYSPASTDUE(hibExperianSropDomain.getDaysPastDue() == null ? "" : hibExperianSropDomain.getDaysPastDue()); 
						experianSRespType1.setASSETCLASSIFICATION(hibExperianSropDomain.getAssetClassification() == null ? "" : hibExperianSropDomain.getAssetClassification()); 
						experianSRespType1.setYEAR_ADVHIST(hibExperianSropDomain.getYearAdvHist() == null ? "" : hibExperianSropDomain.getYearAdvHist()); 
						experianSRespType1.setMONTH_ADVHIST(hibExperianSropDomain.getMonthAdvHist() == null ? "" : hibExperianSropDomain.getMonthAdvHist()); 
						experianSRespType1.setCASHLIMIT(hibExperianSropDomain.getCashLimit() == null ? "" : Integer.toString(hibExperianSropDomain.getCashLimit())); 
						experianSRespType1.setCREDITLIMITAMT(hibExperianSropDomain.getCreditLimitAmt() == null ? "" : Integer.toString(hibExperianSropDomain.getCreditLimitAmt())); 
						experianSRespType1.setACTUALPAYMENTAMT(hibExperianSropDomain.getActualPaymentAmt() == null ? "" : Integer.toString(hibExperianSropDomain.getActualPaymentAmt())); 
						experianSRespType1.setEMIAMT(hibExperianSropDomain.getActualPaymentAmt() == null ? "" : Integer.toString(hibExperianSropDomain.getActualPaymentAmt())); 
						experianSRespType1.setCURRENTBALANCE_ADVHIST(hibExperianSropDomain.getCurrentBalanceAdvHist() == null ? "" :Integer.toString( hibExperianSropDomain.getCurrentBalanceAdvHist())); 
						experianSRespType1.setAMOUNTPASTDUE_ADVHIST(hibExperianSropDomain.getAmountPastDueAdvHist() == null ? "" : Integer.toString(hibExperianSropDomain.getAmountPastDueAdvHist())); 
						experianSRespType1.setSURNAMENONNORMALIZED(hibExperianSropDomain.getSurNameNonNormalized() == null ? "" : hibExperianSropDomain.getSurNameNonNormalized()); 
						experianSRespType1.setFIRSTNAMENONNORMALIZED(hibExperianSropDomain.getFirstNameNonNormalized()==null?"":hibExperianSropDomain.getFirstNameNonNormalized()); 
						experianSRespType1.setMIDDLENAME1NONNORMALIZED(hibExperianSropDomain.getMiddleName1NonNormalized() == null ? "" : hibExperianSropDomain.getMiddleName1NonNormalized()); 
						experianSRespType1.setMIDDLENAME2NONNORMALIZED(hibExperianSropDomain.getMiddleName2NonNormalized() == null ? "" : hibExperianSropDomain.getMiddleName2NonNormalized()); 
						experianSRespType1.setMIDDLENAME3NONNORMALIZED(hibExperianSropDomain.getMiddleName3NonNormalized() == null ? "" : hibExperianSropDomain.getMiddleName3NonNormalized()); 
						experianSRespType1.setALIAS(hibExperianSropDomain.getAlias() == null ? "" : hibExperianSropDomain.getAlias()); 
						experianSRespType1.setCAISGENDERCODE(hibExperianSropDomain.getCaisGenderCode() == null ? "" : hibExperianSropDomain.getCaisGenderCode()); 
						experianSRespType1.setCAISGENDER(hibExperianSropDomain.getCaisGender() == null ? "" : hibExperianSropDomain.getCaisGender()); 
						experianSRespType1.setCAISINCOMETAXPAN(hibExperianSropDomain.getCaisIncomeTaxPan() == null ? "" : hibExperianSropDomain.getCaisIncomeTaxPan()); 
						experianSRespType1.setCAISPASSPORTNUMBER(hibExperianSropDomain.getCaisPassportNumber() == null ? "" : hibExperianSropDomain.getCaisPassportNumber()); 
						experianSRespType1.setVOTERIDNUMBER(hibExperianSropDomain.getVoterIdNumber() == null ? "" : hibExperianSropDomain.getVoterIdNumber()); 
						experianSRespType1.setDATEOFBIRTH(hibExperianSropDomain.getDateOfBirth() == null ? "" : sdf2.format(hibExperianSropDomain.getDateOfBirth())); 
						experianSRespType1.setFIRSTLINEOFADDNONNORMALIZED(hibExperianSropDomain.getFirstLineOfAddNonNormalized() == null ? "" : hibExperianSropDomain.getFirstLineOfAddNonNormalized()); 
						experianSRespType1.setSECONDLINEOFADDNONNORMALIZED(hibExperianSropDomain.getSecondLineOfAddNonNormalized() == null ? "" : hibExperianSropDomain.getSecondLineOfAddNonNormalized()); 
						experianSRespType1.setTHIRDLINEOFADDNONNORMALIZED(hibExperianSropDomain.getThirdLineOfAddNonNormalized() == null ? "" : hibExperianSropDomain.getThirdLineOfAddNonNormalized()); 
						experianSRespType1.setCITYNONNORMALIZED(hibExperianSropDomain.getCityNonNormalized() == null ? "" : hibExperianSropDomain.getCityNonNormalized()); 
						experianSRespType1.setFIFTHLINEOFADDNONNORMALIZED(hibExperianSropDomain.getFifthLineOfAddNonNormalized() == null ? "" : hibExperianSropDomain.getFifthLineOfAddNonNormalized()); 
						experianSRespType1.setSTATECODENONNORMALIZED(hibExperianSropDomain.getStateCodeNonNormalized() == null ? "" : hibExperianSropDomain.getStateCodeNonNormalized()); 
						experianSRespType1.setSTATENONNORMALIZED(hibExperianSropDomain.getStateNonNormalized() == null ? "" : hibExperianSropDomain.getStateNonNormalized()); 
						experianSRespType1.setZIPPOSTALCODENONNORMALIZED(hibExperianSropDomain.getZipPostalCodeNonNormalized() == null ? "" : hibExperianSropDomain.getZipPostalCodeNonNormalized()); 
						experianSRespType1.setCOUNTRYCODENONNORMALIZED(hibExperianSropDomain.getCountryCodeNonNormalized() == null ? "" : hibExperianSropDomain.getCountryCodeNonNormalized()); 
						experianSRespType1.setADDINDICATORNONNORMALIZED(hibExperianSropDomain.getAddIndicatorNonNormalized() == null ? "" : hibExperianSropDomain.getAddIndicatorNonNormalized()); 
						experianSRespType1.setRESIDENCECODENONNORMALIZED(hibExperianSropDomain.getResidenceCodeNonNormalized() == null ? "" : hibExperianSropDomain.getResidenceCodeNonNormalized()); 
						experianSRespType1.setRESIDENCENONNORMALIZED(hibExperianSropDomain.getResidenceNonNormalized() == null ? "" : hibExperianSropDomain.getResidenceNonNormalized()); 
						experianSRespType1.setTELEPHONENUMBER(hibExperianSropDomain.getTelephoneNumber() == null ? "" : hibExperianSropDomain.getTelephoneNumber()); 
						experianSRespType1.setTELETYPE_PHONE(hibExperianSropDomain.getTeleTypePhone() == null ? "" : hibExperianSropDomain.getTeleTypePhone()); 
						experianSRespType1.setTELEEXTENSION_PHONE(hibExperianSropDomain.getTeleExtensionPhone() == null ? "" : hibExperianSropDomain.getTeleExtensionPhone()); 
						experianSRespType1.setMOBILETELEPHONENUMBER(hibExperianSropDomain.getMobileTelephoneNumber() == null ? "" : hibExperianSropDomain.getMobileTelephoneNumber()); 
						experianSRespType1.setFAXNUMBER(hibExperianSropDomain.getFaxNumber() == null ? "" : hibExperianSropDomain.getFaxNumber()); 
						experianSRespType1.setEMAILID_PHONE(hibExperianSropDomain.getEmailIdPhone() == null ? "" : hibExperianSropDomain.getEmailIdPhone()); 
						experianSRespType1.setINCOME_TAX_PAN_ID(hibExperianSropDomain.getIncomeTaxPanId() == null ? "" : hibExperianSropDomain.getIncomeTaxPanId()); 
						experianSRespType1.setPAN_ISSUE_DATE_ID(hibExperianSropDomain.getPanIssueDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getPanIssueDateId())); 
						experianSRespType1.setPAN_EXP_DATE_ID(hibExperianSropDomain.getPanExpDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getPanExpDateId())); 
						experianSRespType1.setPASSPORT_NUMBER_ID(hibExperianSropDomain.getPassportNumberId() == null ? "" : hibExperianSropDomain.getPassportNumberId()); 
						experianSRespType1.setPASSPORT_ISSUE_DATE_ID(hibExperianSropDomain.getPassportIssueDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getPassportIssueDateId())); 
						experianSRespType1.setPASSPORT_EXP_DATE_ID(hibExperianSropDomain.getPassportExpDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getPassportExpDateId())); 
						experianSRespType1.setVOTER_IDENTITY_CARD_ID(hibExperianSropDomain.getVoterIdentityCardId() == null ? "" : hibExperianSropDomain.getVoterIdentityCardId()); 
						experianSRespType1.setVOTER_ID_ISSUE_DATE_ID(hibExperianSropDomain.getVoterIdIssueDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getVoterIdIssueDateId())); 
						experianSRespType1.setVOTER_ID_EXP_DATE_ID(hibExperianSropDomain.getVoterIdExpDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getVoterIdExpDateId())); 
						experianSRespType1.setDRIVER_LICENSE_NUMBER_ID(hibExperianSropDomain.getDriverLicenseNumberId() == null ? "" : hibExperianSropDomain.getDriverLicenseNumberId()); 
						experianSRespType1.setDRIVER_LICENSE_ISSUE_DATE_ID(hibExperianSropDomain.getDriverLicenseIssueDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getDriverLicenseIssueDateId())); 
						experianSRespType1.setDRIVER_LICENSE_EXP_DATE_ID(hibExperianSropDomain.getDriverLicenseExpDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getDriverLicenseExpDateId())); 
						experianSRespType1.setRATION_CARD_NUMBER_ID(hibExperianSropDomain.getRationCardNumberId() == null ? "" : hibExperianSropDomain.getRationCardNumberId()); 
						experianSRespType1.setRATION_CARD_ISSUE_DATE_ID(hibExperianSropDomain.getRationCardIssueDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getRationCardIssueDateId())); 
						experianSRespType1.setRATION_CARD_EXP_DATE_ID(hibExperianSropDomain.getRationCardExpDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getRationCardExpDateId())); 
						experianSRespType1.setUNIVERSAL_ID_NUMBER_ID(hibExperianSropDomain.getUniversalIdNumberId() == null ? "" : hibExperianSropDomain.getUniversalIdNumberId()); 
						experianSRespType1.setUNIVERSAL_ID_ISSUE_DATE_ID(hibExperianSropDomain.getUniversalIdIssueDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getUniversalIdIssueDateId())); 
						experianSRespType1.setUNIVERSAL_ID_EXP_DATE_ID(hibExperianSropDomain.getUniversalIdExpDateId() == null ? "" : sdf2.format(hibExperianSropDomain.getUniversalIdExpDateId())); 
						experianSRespType1.setEMAILID_ID(hibExperianSropDomain.getEmailidId() == null ? "" : hibExperianSropDomain.getEmailidId()); 
						experianSRespType1.setEXACTMATCH(hibExperianSropDomain.getExactMatch() == null ? "" : hibExperianSropDomain.getExactMatch()); 
						experianSRespType1.setCAPSLAST7DAYS(hibExperianSropDomain.getCapsLast7Days() == null ? "" :Integer.toString(hibExperianSropDomain.getCapsLast7Days())); 
						experianSRespType1.setCAPSLAST30DAYS(hibExperianSropDomain.getCapsLast30Days() == null ? "" : Integer.toString(hibExperianSropDomain.getCapsLast30Days())); 
						experianSRespType1.setCAPSLAST90DAYS(hibExperianSropDomain.getCapsLast90Days() == null ? "" : Integer.toString(hibExperianSropDomain.getCapsLast90Days())); 
						experianSRespType1.setCAPSLAST180DAYS(hibExperianSropDomain.getCapsLast180Days() == null ? "" : Integer.toString(hibExperianSropDomain.getCapsLast180Days())); 
						experianSRespType1.setCAPSSUBSCRIBERCODE(hibExperianSropDomain.getCapsSubscriberCode() == null ? "" : hibExperianSropDomain.getCapsSubscriberCode()); 
						experianSRespType1.setCAPSSUBSCRIBERNAME(hibExperianSropDomain.getCapsSubscriberName() == null ? "" : hibExperianSropDomain.getCapsSubscriberName()); 
						experianSRespType1.setCAPSDATEOFREQUEST(hibExperianSropDomain.getCapsdateofRequest() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsdateofRequest())); 
						experianSRespType1.setCAPSREPORTDATE(hibExperianSropDomain.getCapsReportDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsReportDate())); 
						experianSRespType2.setCAPSREPORTTIME(hibExperianSropDomain.getCapsReportTime() == null ? "" : hibExperianSropDomain.getCapsReportTime()); 
						experianSRespType2.setCAPSREPORTNUMBER(hibExperianSropDomain.getCapsReportNumber() == null ? "" : hibExperianSropDomain.getCapsReportNumber()); 
						experianSRespType2.setENQUIRY_REASON_CODE(hibExperianSropDomain.getEnquiryReasonCode() == null ? "" : hibExperianSropDomain.getEnquiryReasonCode()); 
						experianSRespType2.setCAPSENQUIRYREASON(hibExperianSropDomain.getCapsEnquiryReason() == null ? "" : hibExperianSropDomain.getCapsEnquiryReason()); 
						experianSRespType2.setFINANCE_PURPOSE_CODE(hibExperianSropDomain.getFinancePurposeCode() == null ? "" : hibExperianSropDomain.getFinancePurposeCode()); 
						experianSRespType2.setCAPSFINANCEPURPOSE(hibExperianSropDomain.getCapsFinancePurpose() == null ? "" : hibExperianSropDomain.getCapsFinancePurpose()); 
						experianSRespType2.setCAPSAMOUNTFINANCED(hibExperianSropDomain.getCapsAmountFinanced() == null ? "" : hibExperianSropDomain.getCapsAmountFinanced()); 
						experianSRespType2.setCAPSDURATIONOFAGREEMENT(hibExperianSropDomain.getCapsDurationOfAgreement() == null ? "" : hibExperianSropDomain.getCapsDurationOfAgreement()); 
						experianSRespType2.setCAPSAPPLICANTLASTNAME(hibExperianSropDomain.getCapsApplicantLastName() == null ? "" : hibExperianSropDomain.getCapsApplicantLastName()); 
						experianSRespType2.setCAPSAPPLICANTFIRSTNAME(hibExperianSropDomain.getCapsApplicantFirstName() == null ? "" : hibExperianSropDomain.getCapsApplicantFirstName()); 
						experianSRespType2.setCAPSAPPLICANTMIDDLENAME1(hibExperianSropDomain.getCapsApplicantMiddleName1() == null ? "" : hibExperianSropDomain.getCapsApplicantMiddleName1()); 
						experianSRespType2.setCAPSAPPLICANTMIDDLENAME2(hibExperianSropDomain.getCapsApplicantMiddleName2() == null ? "" : hibExperianSropDomain.getCapsApplicantMiddleName2()); 
						experianSRespType2.setCAPSAPPLICANTMIDDLENAME3(hibExperianSropDomain.getCapsApplicantMiddleName3() == null ? "" : hibExperianSropDomain.getCapsApplicantMiddleName3()); 
						experianSRespType2.setCAPSAPPLICANTGENDERCODE(hibExperianSropDomain.getCapsApplicantgenderCode() == null ? "" : hibExperianSropDomain.getCapsApplicantgenderCode()); 
						experianSRespType2.setCAPSAPPLICANTGENDER(hibExperianSropDomain.getCapsApplicantGender() == null ? "" : hibExperianSropDomain.getCapsApplicantGender()); 
						experianSRespType2.setCAPSINCOME_TAX_PAN(hibExperianSropDomain.getCapsIncomeTaxPan() == null ? "" : hibExperianSropDomain.getCapsIncomeTaxPan()); 
						experianSRespType2.setCAPSPAN_ISSUE_DATE(hibExperianSropDomain.getCapsPanIssueDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsPanIssueDate())); 
						experianSRespType2.setCAPSPAN_EXP_DATE(hibExperianSropDomain.getCapsPanExpDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsPanExpDate())); 
						experianSRespType2.setCAPSPASSPORT_NUMBER(hibExperianSropDomain.getCapsPassportNumber() == null ? "" : hibExperianSropDomain.getCapsPassportNumber()); 
						experianSRespType2.setCAPSPASSPORT_ISSUE_DATE(hibExperianSropDomain.getCapsPassportIssueDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsPassportIssueDate())); 
						experianSRespType2.setCAPSPASSPORT_EXP_DATE(hibExperianSropDomain.getCapsPassportExpDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsPassportExpDate())); 
						experianSRespType2.setCAPSVOTER_IDENTITY_CARD(hibExperianSropDomain.getCapsVoterIdentityCard() == null ? "" : hibExperianSropDomain.getCapsVoterIdentityCard()); 
						experianSRespType2.setCAPSVOTER_ID_ISSUE_DATE(hibExperianSropDomain.getCapsVoterIdIssueDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsVoterIdIssueDate())); 
						experianSRespType2.setCAPSVOTER_ID_EXP_DATE(hibExperianSropDomain.getCapsVoterIdExpDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsVoterIdExpDate())); 
						experianSRespType2.setCAPSDRIVER_LICENSE_NUMBER(hibExperianSropDomain.getCapsDriverLicenseNumber() == null ? "" : hibExperianSropDomain.getCapsDriverLicenseNumber()); 
						experianSRespType2.setCAPSDRIVER_LICENSE_ISSUE_DATE(hibExperianSropDomain.getCapsDriverLicenseIssueDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsDriverLicenseIssueDate())); 
						experianSRespType2.setCAPSDRIVER_LICENSE_EXP_DATE(hibExperianSropDomain.getCapsDriverLicenseExpDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsDriverLicenseExpDate())); 
						experianSRespType2.setCAPSRATION_CARD_NUMBER(hibExperianSropDomain.getCapsRationCardNumber() == null ? "" : hibExperianSropDomain.getCapsRationCardNumber()); 
						experianSRespType2.setCAPSRATION_CARD_ISSUE_DATE(hibExperianSropDomain.getCapsRationCardIssueDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsRationCardIssueDate())); 
						experianSRespType2.setCAPSRATION_CARD_EXP_DATE(hibExperianSropDomain.getCapsRationCardExpDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsRationCardExpDate())); 
						experianSRespType2.setCAPSUNIVERSAL_ID_NUMBER(hibExperianSropDomain.getCapsUniversalIdNumber() == null ? "" : hibExperianSropDomain.getCapsUniversalIdNumber()); 
						experianSRespType2.setCAPSUNIVERSAL_ID_ISSUE_DATE(hibExperianSropDomain.getCapsUniversalIdIssueDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsUniversalIdIssueDate())); 
						experianSRespType2.setCAPSUNIVERSAL_ID_EXP_DATE(hibExperianSropDomain.getCapsUniversalIdExpDate() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsUniversalIdExpDate())); 
						experianSRespType2.setCAPSAPPLICANTDOBAPPLICANT(hibExperianSropDomain.getCapsApplicantDobApplicant() == null ? "" : sdf2.format(hibExperianSropDomain.getCapsApplicantDobApplicant())); 
						experianSRespType2.setCAPSAPPLICANTTELNOAPPLICANT1ST(hibExperianSropDomain.getCapsApplicantTelNoApplicant1st() == null ? "" : hibExperianSropDomain.getCapsApplicantTelNoApplicant1st()); 
						experianSRespType2.setCAPSAPPLICANTTELEXT(hibExperianSropDomain.getCapsApplicantTelExt() == null ? "" : hibExperianSropDomain.getCapsApplicantTelExt()); 
						experianSRespType2.setCAPSAPPLICANTTELTYPE(hibExperianSropDomain.getCapsApplicantTelType() == null ? "" : hibExperianSropDomain.getCapsApplicantTelType()); 
						experianSRespType2.setCAPSAPPLICANTMOBILEPHONENUMBER(hibExperianSropDomain.getCapsApplicantMobilePhoneNumber() == null ? "" : hibExperianSropDomain.getCapsApplicantMobilePhoneNumber()); 
						experianSRespType2.setCAPSAPPLICANTEMAILID(hibExperianSropDomain.getCapsApplicantEmailId() == null ? "" : hibExperianSropDomain.getCapsApplicantEmailId()); 
						experianSRespType2.setCAPSAPPLICANTINCOME(hibExperianSropDomain.getCapsApplicantIncome() == null ? "" : Integer.toString(hibExperianSropDomain.getCapsApplicantIncome())); 
						experianSRespType2.setAPPLICANT_MARITALSTATUSCODE(hibExperianSropDomain.getApplicantMaritalStatusCode() == null ? "" : hibExperianSropDomain.getApplicantMaritalStatusCode()); 
						experianSRespType2.setCAPSAPPLICANTMARITALSTATUS(hibExperianSropDomain.getCapsApplicantMaritalStatus() == null ? "" : hibExperianSropDomain.getCapsApplicantMaritalStatus()); 
						experianSRespType2.setCAPSAPPLICANTEMPLMTSTATUSCODE(hibExperianSropDomain.getCapsApplicantEmploymentStatusCode() == null ? "" : hibExperianSropDomain.getCapsApplicantEmploymentStatusCode()); 
						experianSRespType2.setCAPSAPPLICANTEMPLOYMENTSTATUS(hibExperianSropDomain.getCapsApplicantEmploymentStatus() == null ? "" : hibExperianSropDomain.getCapsApplicantEmploymentStatus()); 
						experianSRespType2.setCAPSAPPLICANTDATEWITHEMPLOYER(hibExperianSropDomain.getCapsApplicantDateWithEmployer() == null ? "" : hibExperianSropDomain.getCapsApplicantDateWithEmployer()); 
						experianSRespType2.setCAPSAPPLTNOMAJORCRDTCARDHELD(hibExperianSropDomain.getCapsApplicantNoMajorCrditCardHeld() == null ? "" : hibExperianSropDomain.getCapsApplicantNoMajorCrditCardHeld()); 
						experianSRespType2.setCAPSAPPLTFLATNOPLOTNOHOUSENO(hibExperianSropDomain.getCapsApplicantFlatNoPlotNoHouseNo() == null ? "" : hibExperianSropDomain.getCapsApplicantFlatNoPlotNoHouseNo()); 
						experianSRespType2.setCAPSAPPLICANTBLDGNOSOCIETYNAME(hibExperianSropDomain.getCapsApplicantBldgNoSocietyName() == null ? "" : hibExperianSropDomain.getCapsApplicantBldgNoSocietyName()); 
						experianSRespType2.setCAPSAPPLTRDNONAMEAREALOCALITY(hibExperianSropDomain.getCapsApplicantRoadNoNameAreaLocality() == null ? "" : hibExperianSropDomain.getCapsApplicantRoadNoNameAreaLocality()); 
						experianSRespType2.setCAPSAPPLICANTCITY(hibExperianSropDomain.getCapsApplicantCity() == null ? "" : hibExperianSropDomain.getCapsApplicantCity()); 
						experianSRespType2.setCAPSAPPLICANTLANDMARK(hibExperianSropDomain.getCapsApplicantLandmark() == null ? "" : hibExperianSropDomain.getCapsApplicantLandmark()); 
						experianSRespType2.setCAPSAPPLICANTSTATECODE(hibExperianSropDomain.getCapsApplicantStateCode() == null ? "" : hibExperianSropDomain.getCapsApplicantStateCode()); 
						experianSRespType2.setCAPSAPPLICANTSTATE(hibExperianSropDomain.getCapsApplicantState() == null ? "" : hibExperianSropDomain.getCapsApplicantState()); 
						experianSRespType2.setCAPSAPPLICANTPINCODE(hibExperianSropDomain.getCapsApplicantPinCode() == null ? "" : hibExperianSropDomain.getCapsApplicantPinCode()); 
						experianSRespType2.setCAPSAPPLICANTCOUNTRYCODE(hibExperianSropDomain.getCapsApplicantCountryCode() == null ? "" : hibExperianSropDomain.getCapsApplicantCountryCode()); 
						experianSRespType2.setBUREAUSCORE(hibExperianSropDomain.getBureauScore() == null ? "" : Integer.toString(hibExperianSropDomain.getBureauScore())); 
						experianSRespType2.setBUREAUSCORECONFIDLEVEL(hibExperianSropDomain.getBureauScoreConfidLevel() == null ? "" : hibExperianSropDomain.getBureauScoreConfidLevel()); 
						experianSRespType2.setCREDITRATING(hibExperianSropDomain.getCreditRating() == null ? "" : hibExperianSropDomain.getCreditRating()); 
						experianSRespType2.setTNOFBFHLCADEXHL(hibExperianSropDomain.getTnOfBFHLCADExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfBFHLCADExhl())); 
						experianSRespType2.setTOTVALOFBFHLCAD(hibExperianSropDomain.getTotValOfBFHLCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTotValOfBFHLCAD())); 
						experianSRespType2.setMNTSMRBFHLCAD(hibExperianSropDomain.getMNTSMRBFHLCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getMNTSMRBFHLCAD())); 
						experianSRespType2.setTNOFHLCAD(hibExperianSropDomain.getTnOfHLCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfHLCAD())); 
						experianSRespType2.setTOTVALOFHLCAD(hibExperianSropDomain.getTotValOfHLCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTotValOfHLCAD())); 
						experianSRespType2.setMNTSMRHLCAD(hibExperianSropDomain.getMntsmrHLCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getMntsmrHLCAD())); 
						experianSRespType2.setTNOFTELCOSCAD(hibExperianSropDomain.getTnOfTelcosCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfTelcosCAD())); 
						experianSRespType2.setTOTVALOFTELCOSCAD(hibExperianSropDomain.getTotValOfTelcosCad() == null ? "" : Integer.toString(hibExperianSropDomain.getTotValOfTelcosCad())); 
						experianSRespType2.setMNTSMRTELCOSCAD(hibExperianSropDomain.getMntsmrTelcosCad() == null ? "" : Integer.toString(hibExperianSropDomain.getMntsmrTelcosCad())); 
						experianSRespType2.setTNOFMFCAD(hibExperianSropDomain.getTnOfmfCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfmfCAD())); 
						experianSRespType2.setTOTVALOFMFCAD(hibExperianSropDomain.getTotValOfmfCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTotValOfmfCAD())); 
						experianSRespType2.setMNTSMRMFCAD(hibExperianSropDomain.getMntsmrmfCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getMntsmrmfCAD())); 
						experianSRespType2.setTNOFRETAILCAD(hibExperianSropDomain.getTnOfRetailCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfRetailCAD())); 
						experianSRespType2.setTOTVALOFRETAILCAD(hibExperianSropDomain.getTotValOfRetailCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTotValOfRetailCAD())); 
						experianSRespType2.setMNTSMRRETAILCAD(hibExperianSropDomain.getMntsmrRetailCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getMntsmrRetailCAD())); 
						experianSRespType2.setTNOFALLCAD(hibExperianSropDomain.getTnOfAllCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfAllCAD())); 
						experianSRespType2.setTOTVALOFALLCAD(hibExperianSropDomain.getTotValOfAllCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTotValOfAllCAD())); 
						experianSRespType2.setMNTSMRCADALL(hibExperianSropDomain.getMntsmrCADAll() == null ? "" : Integer.toString(hibExperianSropDomain.getMntsmrCADAll())); 
						experianSRespType2.setTNOFBFHLACAEXHL(hibExperianSropDomain.getTnOfBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfBFHLACAExhl())); 
						experianSRespType2.setBALBFHLACAEXHL(hibExperianSropDomain.getBalBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getBalBFHLACAExhl())); 
						experianSRespType2.setWCDSTBFHLACAEXHL(hibExperianSropDomain.getWcdstBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getWcdstBFHLACAExhl())); 
						experianSRespType2.setWDSPR6MNTBFHLACAEXHL(hibExperianSropDomain.getWdspr6MntBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr6MntBFHLACAExhl())); 
						experianSRespType2.setWDSPR712MNTBFHLACAEXHL(hibExperianSropDomain.getWdspr712MntBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr712MntBFHLACAExhl())); 
						experianSRespType2.setAGEOFOLDESTBFHLACAEXHL(hibExperianSropDomain.getAgeOfOldestBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getAgeOfOldestBFHLACAExhl())); 
						experianSRespType2.setHCBPERREVACCBFHLACAEXHL(hibExperianSropDomain.getHcbperrevaccBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getHcbperrevaccBFHLACAExhl())); 
						experianSRespType2.setTCBPERREVACCBFHLACAEXHL(hibExperianSropDomain.getTcbperrevaccBFHLACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getTcbperrevaccBFHLACAExhl())); 
						experianSRespType2.setTNOFHLACA(hibExperianSropDomain.getTnOfHlACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfHlACA())); 
						experianSRespType2.setBALHLACA(hibExperianSropDomain.getBalHlACA() == null ? "" : Integer.toString(hibExperianSropDomain.getBalHlACA())); 
						experianSRespType2.setWCDSTHLACA(hibExperianSropDomain.getWcdstHlACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWcdstHlACA())); 
						experianSRespType2.setWDSPR6MNTHLACA(hibExperianSropDomain.getWdspr6MnthlACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr6MnthlACA())); 
						experianSRespType2.setWDSPR712MNTHLACA(hibExperianSropDomain.getWdspr712mnthlACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr712mnthlACA())); 
						experianSRespType2.setAGEOFOLDESTHLACA(hibExperianSropDomain.getAgeOfOldesthlACA() == null ? "" : Integer.toString(hibExperianSropDomain.getAgeOfOldesthlACA())); 
						experianSRespType2.setTNOFMFACA(hibExperianSropDomain.getTnOfMfACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfMfACA())); 
						experianSRespType2.setTOTALBALMFACA(hibExperianSropDomain.getTotalBalMfACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTotalBalMfACA())); 
						experianSRespType2.setWCDSTMFACA(hibExperianSropDomain.getWcdstMfACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWcdstMfACA())); 
						experianSRespType2.setWDSPR6MNTMFACA(hibExperianSropDomain.getWdspr6MntMfACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr6MntMfACA())); 
						experianSRespType2.setWDSPR712MNTMFACA(hibExperianSropDomain.getWdspr712mntMfACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr712mntMfACA())); 
						experianSRespType2.setAGEOFOLDESTMFACA(hibExperianSropDomain.getAgeOfOldestMfACA() == null ? "" : Integer.toString(hibExperianSropDomain.getAgeOfOldestMfACA())); 
						experianSRespType2.setTNOFTELCOSACA(hibExperianSropDomain.getTnOfTelcosACA() == null ? "" :Integer.toString( hibExperianSropDomain.getTnOfTelcosACA())); 
						experianSRespType2.setTOTALBALTELCOSACA(hibExperianSropDomain.getTotalBalTelcosACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTotalBalTelcosACA())); 
						experianSRespType2.setWCDSTTELCOSACA(hibExperianSropDomain.getWcdstTelcosACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWcdstTelcosACA())); 
						experianSRespType2.setWDSPR6MNTTELCOSACA(hibExperianSropDomain.getWdspr6mntTelcosACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr6mntTelcosACA())); 
						experianSRespType2.setWDSPR712MNTTELCOSACA(hibExperianSropDomain.getWdspr712mntTelcosACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr712mntTelcosACA())); 
						experianSRespType2.setAGEOFOLDESTTELCOSACA(hibExperianSropDomain.getAgeOfOldestTelcosACA() == null ? "" : Integer.toString(hibExperianSropDomain.getAgeOfOldestTelcosACA())); 
						experianSRespType2.setTNOFRETAILACA(hibExperianSropDomain.getTnOfRetailACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfRetailACA())); 
						experianSRespType2.setTOTALBALRETAILACA(hibExperianSropDomain.getTotalBalRetailACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTotalBalRetailACA())); 
						experianSRespType2.setWCDSTRETAILACA(hibExperianSropDomain.getWcdstRetailACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWcdstRetailACA())); 
						experianSRespType2.setWDSPR6MNTRETAILACA(hibExperianSropDomain.getWdspr6mntRetailACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr6mntRetailACA())); 
						experianSRespType2.setWDSPR712MNTRETAILACA(hibExperianSropDomain.getWdspr712mntRetailACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr712mntRetailACA())); 
						experianSRespType2.setAGEOFOLDESTRETAILACA(hibExperianSropDomain.getAgeOfOldestRetailACA() == null ? "" :Integer.toString(hibExperianSropDomain.getAgeOfOldestRetailACA())); 
						experianSRespType2.setHCBLMPERREVACCRET(hibExperianSropDomain.getHcblmperrevaccret() == null ? "" : Integer.toString(hibExperianSropDomain.getHcblmperrevaccret())); 
						experianSRespType2.setTOTCURBALLMPERREVACCRET(hibExperianSropDomain.getTotCurBallmperrevaccret() == null ? "" : Integer.toString(hibExperianSropDomain.getTotCurBallmperrevaccret())); 
						experianSRespType2.setTNOFALLACA(hibExperianSropDomain.getTnOfallACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfallACA())); 
						experianSRespType2.setBALALLACAEXHL(hibExperianSropDomain.getBalAllACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getBalAllACAExhl())); 
						experianSRespType2.setWCDSTALLACA(hibExperianSropDomain.getWcdstAllACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWcdstAllACA())); 
						experianSRespType2.setWDSPR6MNTALLACA(hibExperianSropDomain.getWdspr6mntallACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWdspr6mntallACA())); 
						experianSRespType2.setWDSPR712MNTALLACA(hibExperianSropDomain.getWdspr712mntAllACA() == null ? "" :Integer.toString( hibExperianSropDomain.getWdspr712mntAllACA())); 
						experianSRespType2.setAGEOFOLDESTALLACA(hibExperianSropDomain.getAgeOfOldestAllACA() == null ? "" : Integer.toString(hibExperianSropDomain.getAgeOfOldestAllACA())); 
						experianSRespType2.setTNOFNDELBFHLINACAEXHL(hibExperianSropDomain.getTnOfndelBFHLinACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfndelBFHLinACAExhl())); 
						experianSRespType2.setTNOFDELBFHLINACAEXHL(hibExperianSropDomain.getTnOfDelBFHLInACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfDelBFHLInACAExhl())); 
						experianSRespType2.setTNOFNDELHLINACA(hibExperianSropDomain.getTnOfnDelHLInACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfnDelHLInACA())); 
						experianSRespType2.setTNOFDELHLINACA(hibExperianSropDomain.getTnOfDelhlInACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfDelhlInACA())); 
						experianSRespType2.setTNOFNDELMFINACA(hibExperianSropDomain.getTnOfDelmfinACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfDelmfinACA())); 
						experianSRespType2.setTNOFDELMFINACA(hibExperianSropDomain.getTnOfdelTelcosInACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfdelTelcosInACA())); 
						experianSRespType2.setTNOFNDELTELCOSINACA(hibExperianSropDomain.getTnOfnDelTelcosInACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfnDelTelcosInACA())); 
						experianSRespType2.setTNOFDELTELCOSINACA(hibExperianSropDomain.getTnOfdelTelcosInACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfdelTelcosInACA())); 
						experianSRespType2.setTNOFNDELRETAILINACA(hibExperianSropDomain.getTnOfndelRetailInACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfndelRetailInACA())); 
						experianSRespType2.setTNOFDELRETAILINACA(hibExperianSropDomain.getTnOfDelRetailInACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfDelRetailInACA())); 
						experianSRespType2.setBFHLCAPSLAST90DAYS(hibExperianSropDomain.getBFHLCapsLast90Days() == null ? "" : Integer.toString(hibExperianSropDomain.getBFHLCapsLast90Days())); 
						experianSRespType2.setMFCAPSLAST90DAYS(hibExperianSropDomain.getMfCapsLast90Days() == null ? "" : Integer.toString(hibExperianSropDomain.getMfCapsLast90Days())); 
						experianSRespType2.setTELCOSCAPSLAST90DAYS(hibExperianSropDomain.getTelcosCapsLast90Days() == null ? "" : Integer.toString(hibExperianSropDomain.getTelcosCapsLast90Days())); 
						experianSRespType2.setRETAILCAPSLAST90DAYS(hibExperianSropDomain.getRetailCapsLast90Days() == null ? "" : Integer.toString(hibExperianSropDomain.getRetailCapsLast90Days())); 
						experianSRespType2.setTNOFOCOMCAD(hibExperianSropDomain.getTnOfocomcad() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfocomcad())); 
						experianSRespType2.setTOTVALOFOCOMCAD(hibExperianSropDomain.getTotValOfocomCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getTotValOfocomCAD())); 
						experianSRespType2.setMNTSMROCOMCAD(hibExperianSropDomain.getMntsmrocomCAD() == null ? "" : Integer.toString(hibExperianSropDomain.getMntsmrocomCAD())); 
						experianSRespType2.setTNOFOCOMACA(hibExperianSropDomain.getTnOfocomACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfocomACA())); 
						experianSRespType2.setBALOCOMACAEXHL(hibExperianSropDomain.getBalocomACAExhl() == null ? "" : Integer.toString(hibExperianSropDomain.getBalocomACAExhl())); 
						experianSRespType2.setBALOCOMACAHLONLY(hibExperianSropDomain.getBalOcomacahlonly() == null ? "" : Integer.toString(hibExperianSropDomain.getBalOcomacahlonly())); 
						experianSRespType2.setWCDSTOCOMACA(hibExperianSropDomain.getWcdstocomACA() == null ? "" : Integer.toString(hibExperianSropDomain.getWcdstocomACA())); 
						experianSRespType2.setHCBLMPERREVOCOMACA(hibExperianSropDomain.getHcblmperrevocomACA() == null ? "" : Integer.toString(hibExperianSropDomain.getHcblmperrevocomACA())); 
						experianSRespType2.setTNOFNDELOCOMINACA(hibExperianSropDomain.getTnOfDelocominACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfDelocominACA())); 
						experianSRespType2.setTNOFDELOCOMINACA(hibExperianSropDomain.getTnOfDelocominACA() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfDelocominACA())); 
						experianSRespType2.setTNOFOCOMCAPSLAST90DAYS(hibExperianSropDomain.getTnOfocomCapsLast90Days() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfocomCapsLast90Days())); 
						experianSRespType2.setANYRELCBDATADISYN(hibExperianSropDomain.getAnyRelcbdatadisyn() == null ? "" : Integer.toString(hibExperianSropDomain.getAnyRelcbdatadisyn())); 
						experianSRespType2.setOTHRELCBDFCPOSMATYN(hibExperianSropDomain.getOthrelcbdfcposmatyn() == null ? "" : Integer.toString(hibExperianSropDomain.getOthrelcbdfcposmatyn())); 
						experianSRespType2.setTNOFCADCLASSEDASSFWDWO(hibExperianSropDomain.getTnOfCADclassedassfwdwo() == null ? "" : Integer.toString(hibExperianSropDomain.getTnOfCADclassedassfwdwo())); 
						experianSRespType2.setMNTSMRCADCLASSEDASSFWDWO(hibExperianSropDomain.getMntsmrcadclassedassfwdwo() == null ? "" : Integer.toString(hibExperianSropDomain.getMntsmrcadclassedassfwdwo())); 
						experianSRespType2.setNUMOFCADSFWDWOLAST24MNT(hibExperianSropDomain.getNumOfCadsfwdwolast24mnt() == null ? "" : Integer.toString(hibExperianSropDomain.getNumOfCadsfwdwolast24mnt())); 
						experianSRespType2.setTOTCURBALLIVESACC(hibExperianSropDomain.getTotCurBalLivesAcc() == null ? "" : Integer.toString(hibExperianSropDomain.getTotCurBalLivesAcc())); 
						experianSRespType2.setTOTCURBALLIVEUACC(hibExperianSropDomain.getTotCurBalLiveuAcc() == null ? "" : Integer.toString(hibExperianSropDomain.getTotCurBalLiveuAcc())); 
						experianSRespType2.setTOTCURBALMAXBALLIVESACC(hibExperianSropDomain.getTotCurBalMaxBalLivesAcc() == null ? "" : Integer.toString(hibExperianSropDomain.getTotCurBalMaxBalLivesAcc())); 
						experianSRespType2.setTOTCURBALMAXBALLIVEUACC(hibExperianSropDomain.getTotCurBalMaxBalLiveuAcc() == null ? "" : Integer.toString(hibExperianSropDomain.getTotCurBalMaxBalLiveuAcc())); 
						experianSRespType2.setOUTPUT_WRITE_FLAG(hibExperianSropDomain.getOutputWriteFlag() == null ? "" : hibExperianSropDomain.getOutputWriteFlag()); 
						experianSRespType2.setOUTPUT_WRITE_DATE(hibExperianSropDomain.getOutputWriteDate() == null ? "" : hibExperianSropDomain.getOutputWriteDate()); 
						experianSRespType2.setOUTPUT_READ_DATE(hibExperianSropDomain.getOutputReadDate() == null ? "" : hibExperianSropDomain.getOutputReadDate()); 
						experianSRespType2.setDATEOFADDITION(hibExperianSropDomain.getDateOfAddition() == null ? "" : sdf2.format(hibExperianSropDomain.getDateOfAddition()));
						experianSRespType2.setACCOUNT_KEY(hibExperianSropDomain.getAccountKey()==null?"":Integer.toString(hibExperianSropDomain.getAccountKey()));
						ExperianSRespType experianSRespType = new ExperianSRespType();
						experianSRespType.setEXPERIAN_SROP_DOMAIN_LIST1(experianSRespType1);
						experianSRespType.setEXPERIAN_SROP_DOMAIN_LIST2(experianSRespType2);
						
						experianSRespTypeList.add(experianSRespType);
					}
					responseJSONType.setEXPERIAN_SROP_DOMAIN_LIST(experianSRespTypeList.toArray(new ExperianSRespType[experianSRespTypeList.size()]));
				}
			}

		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** experianSropMapping >>");
		return responseJSONType;
	}

	public ResponseJSONType equifaxEropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> equifaxEropMapping ");
		try {
			EquifaxEropDomainObject equifaxEropDomainObject = (EquifaxEropDomainObject) responseJsonObject;

			if (equifaxEropDomainObject != null) {
				List<EquifaxEropDomain> equifaxEropDomainList = equifaxEropDomainObject.getEquifaxEropDomainList();
				if (equifaxEropDomainList != null&& equifaxEropDomainList.size() > 0) {
					List<EquifaxERespType> equifaxERespTypeList = new ArrayList<EquifaxERespType>();
					for (EquifaxEropDomain equifaxEropDomain : equifaxEropDomainList) {

						EquifaxERespType equifaxERespType = new EquifaxERespType();
						equifaxERespType.setSRNO(equifaxEropDomain.getSrNo_() == null ? "" : equifaxEropDomain.getSrNo_()); 
						equifaxERespType.setSOURCE_NAME(equifaxEropDomain.getSourceName_() == null ? "" : equifaxEropDomain.getSourceName_()); 
						equifaxERespType.setMEMBER_REFERENCE_NUMBER(equifaxEropDomain.getMemberReferenceNo_() == null ? "" : equifaxEropDomain.getMemberReferenceNo_()); 
						equifaxERespType.setENQUIRY_DATE(equifaxEropDomain.getEnquiryDate_() == null ? "" : sdf1.format(equifaxEropDomain.getEnquiryDate_())); 
						equifaxERespType.setERRORCODE(equifaxEropDomain.getErrorCode_() == null ? "" : equifaxEropDomain.getErrorCode_()); 
						equifaxERespType.setERRORMSG(equifaxEropDomain.getErrorMsg_() == null ? "" : equifaxEropDomain.getErrorMsg_()); 
						equifaxERespType.setERRORDETAILS(equifaxEropDomain.getErrorDetails_() == null ? "" : equifaxEropDomain.getErrorDetails_()); 
						equifaxERespType.setOUTPUT_WRITE_FLAG(equifaxEropDomain.getOutputWriteFlag_() == null ? "" : equifaxEropDomain.getOutputWriteFlag_()); 
						equifaxERespType.setOUTPUT_WRITE_DATE(equifaxEropDomain.getOutputWriteDate_() == null ? "" : equifaxEropDomain.getOutputWriteDate_()); 
						equifaxERespType.setOUTPUT_READ_TIME(equifaxEropDomain.getOutputReadTime_() == null ? "" : equifaxEropDomain.getOutputReadTime_());

						equifaxERespTypeList.add(equifaxERespType);
					}
					responseJSONType.setEQUIFAX_EROP_DOMAIN_LIST(equifaxERespTypeList.toArray(new EquifaxERespType[equifaxERespTypeList.size()]));
				}

			}

		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** equifaxEropMapping >>");
		return responseJSONType;
	}

	public ResponseJSONType equifaxSropMapping(Object responseJsonObject, ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> equifaxSropMapping ");
		try {
			EquifaxSropDomainObject equifaxSropDomainObject = (EquifaxSropDomainObject) responseJsonObject;
			if (equifaxSropDomainObject != null) {
				List<HibEquifaxSropDomain> equifaxSropDomainList = equifaxSropDomainObject.getEquifaxSropDomainList();

				if (equifaxSropDomainList != null&& equifaxSropDomainList.size() > 0) {
					List<EquifaxSRespType> equifaxSRespTypeList = new ArrayList<EquifaxSRespType>();
					for (HibEquifaxSropDomain equifaxSropDomain : equifaxSropDomainList) {

						EquifaxSRespType equifaxSRespType = new EquifaxSRespType();

						equifaxSRespType.setSRNO(equifaxSropDomain.getSrNo() == null ? "" :Integer.toString(equifaxSropDomain.getSrNo())); 
						equifaxSRespType.setSOA_SOURCE_NAME(equifaxSropDomain.getSoaSourceName() == null ? "" : equifaxSropDomain.getSoaSourceName()); 
						equifaxSRespType.setMEMBER_REFERENCE_NUMBER(equifaxSropDomain.getMemberReferenceNumber() == null ? "" : equifaxSropDomain.getMemberReferenceNumber()); 
						equifaxSRespType.setENQUIRY_DATE(equifaxSropDomain.getEnquiryDate() == null ? "" : sdf1.format(equifaxSropDomain.getEnquiryDate())); 
						equifaxSRespType.setCUSTOMERCODE(equifaxSropDomain.getCustomerCode() == null ? "" : equifaxSropDomain.getCustomerCode()); 
						equifaxSRespType.setFIRSTNAME_NM_TP(equifaxSropDomain.getFirstnameNmTp() == null ? "" : equifaxSropDomain.getFirstnameNmTp()); 
						equifaxSRespType.setMIDLLENAME_NM_TP(equifaxSropDomain.getMidlleNameNmTp() == null ? "" : equifaxSropDomain.getMidlleNameNmTp()); 
						equifaxSRespType.setLASTNAME_NM_TP(equifaxSropDomain.getLastNameNmTp() == null ? "" : equifaxSropDomain.getLastNameNmTp()); 
						equifaxSRespType.setADDMIDLLENAME_NM_TP(equifaxSropDomain.getAddMidlleNameNmTp() == null ? "" : equifaxSropDomain.getAddMidlleNameNmTp()); 
						equifaxSRespType.setSUFFIX_NM_TP(equifaxSropDomain.getSuffixNmTp() == null ? "" : equifaxSropDomain.getSuffixNmTp()); 
						equifaxSRespType.setREPORTEDDATE_AG_IF(equifaxSropDomain.getReportedDateAgIf() == null ? "" : equifaxSropDomain.getReportedDateAgIf()); 
						equifaxSRespType.setAGE_AG_IF(equifaxSropDomain.getAgeAgIf() == null ? "" : equifaxSropDomain.getAgeAgIf()); 
						equifaxSRespType.setREPORTEDDATE_AL_NM(equifaxSropDomain.getReportedDateAlNm() == null ? "" : equifaxSropDomain.getReportedDateAlNm()); 
						equifaxSRespType.setALIASNAME_AL_NM(equifaxSropDomain.getAliasNameAlNm() == null ? "" : equifaxSropDomain.getAliasNameAlNm()); 
						equifaxSRespType.setADDITIONALNAMEINFO_AD_NM_IF(equifaxSropDomain.getAdditionalNameInfoAdNmIf() == null ? "" : equifaxSropDomain.getAdditionalNameInfoAdNmIf()); 
						equifaxSRespType.setNOOFDEPENDENTS_AD_NM_IF(equifaxSropDomain.getNoOfDependentsAdNmIf() == null ? "" : equifaxSropDomain.getNoOfDependentsAdNmIf()); 
						equifaxSRespType.setREPORTEDDATE_RS_ADD(equifaxSropDomain.getReportedDateRsAdd() == null ? "" : equifaxSropDomain.getReportedDateRsAdd()); 
						equifaxSRespType.setADDRESS_RS_ADD(equifaxSropDomain.getAddressRsAdd() == null ? "" : equifaxSropDomain.getAddressRsAdd()); 
						equifaxSRespType.setSTATE_RS_ADD(equifaxSropDomain.getStateRsAdd() == null ? "" : equifaxSropDomain.getStateRsAdd()); 
						equifaxSRespType.setPOSTAL_RS_ADD(equifaxSropDomain.getPostalRsAdd() == null ? "" : equifaxSropDomain.getPostalRsAdd()); 
						equifaxSRespType.setADDTYPE_RS_ADD(equifaxSropDomain.getAddTypeRsAdd() == null ? "" : equifaxSropDomain.getAddTypeRsAdd()); 
						equifaxSRespType.setIDTYPE_RS_ID(equifaxSropDomain.getIdTypeRsId() == null ? "" : equifaxSropDomain.getIdTypeRsId()); 
						equifaxSRespType.setREPORTEDDATE_RS_ID(equifaxSropDomain.getReportedDateRsId() == null ? "" : equifaxSropDomain.getReportedDateRsId()); 
						equifaxSRespType.setIDNUMBER_RS_ID(equifaxSropDomain.getIdNumberRsId() == null ? "" : equifaxSropDomain.getIdNumberRsId()); 
						equifaxSRespType.setEMAILREPORTEDDATE_RS_ML(equifaxSropDomain.getEmailReportedDateRsMl() == null ? "" : equifaxSropDomain.getEmailReportedDateRsMl()); 
						equifaxSRespType.setEMAILADDRESS_RS_ML(equifaxSropDomain.getEmailAddressRsMl() == null ? "" : equifaxSropDomain.getEmailAddressRsMl()); 
						equifaxSRespType.setEMPNMREPDATE_RS_EMDL(equifaxSropDomain.getEmpNMRepDateRsEMDL() == null ? "" : equifaxSropDomain.getEmpNMRepDateRsEMDL()); 
						equifaxSRespType.setEMPLOYERNAME_RS_EMDL(equifaxSropDomain.getEmployerNameRsEmdl() == null ? "" : equifaxSropDomain.getEmployerNameRsEmdl()); 
						equifaxSRespType.setEMPPOSITION_RS_EMDL(equifaxSropDomain.getEmpPositionRsEmdl() == null ? "" : equifaxSropDomain.getEmpPositionRsEmdl()); 
						equifaxSRespType.setEMPADDRESS_RS_EMDL(equifaxSropDomain.getEmpAddressRsEmdl() == null ? "" : equifaxSropDomain.getEmpAddressRsEmdl()); 
						equifaxSRespType.setDOB_RS_PER_IF(equifaxSropDomain.getDobRsPerIf() == null ? "" : equifaxSropDomain.getDobRsPerIf()); 
						equifaxSRespType.setGENDER_RS_PER_IF(equifaxSropDomain.getGenderRsPerIf() == null ? "" : equifaxSropDomain.getGenderRsPerIf()); 
						equifaxSRespType.setTOTALINCOME_RS_PER_IF(equifaxSropDomain.getTotalIncomeRsPerIf() == null ? "" : equifaxSropDomain.getTotalIncomeRsPerIf()); 
						equifaxSRespType.setOCCUPATION_RS_PER_IF(equifaxSropDomain.getOccupationRsPerIf() == null ? "" : equifaxSropDomain.getOccupationRsPerIf()); 
						equifaxSRespType.setMARITALSTAUS_RS_PER_IF(equifaxSropDomain.getMaritalStausRsPerIf() == null ? "" : equifaxSropDomain.getMaritalStausRsPerIf()); 
						equifaxSRespType.setOCCUPATION_IC_DL(equifaxSropDomain.getOccupationIcDl() == null ? "" : equifaxSropDomain.getOccupationIcDl()); 
						equifaxSRespType.setMONTHLYINCOME_IC_DL(equifaxSropDomain.getMonthlyIncomeIcDl() == null ? "" : equifaxSropDomain.getMonthlyIncomeIcDl()); 
						equifaxSRespType.setMONTHLYEXPENSE_IC_DL(equifaxSropDomain.getMonthlyExpenseIcDl() == null ? "" : equifaxSropDomain.getMonthlyExpenseIcDl()); 
						equifaxSRespType.setPOVERTYINDEX_IC_DL(equifaxSropDomain.getPovertyIndexIcDl() == null ? "" : equifaxSropDomain.getPovertyIndexIcDl()); 
						equifaxSRespType.setASSETOWNERSHIP_IC_DL(equifaxSropDomain.getAssetOwnershipIcDl() == null ? "" : equifaxSropDomain.getAssetOwnershipIcDl()); 
						equifaxSRespType.setREPORTEDDATE_RS_PH(equifaxSropDomain.getReportedDateRsPh() == null ? "" : equifaxSropDomain.getReportedDateRsPh()); 
						equifaxSRespType.setTYPECODE_RS_PH(equifaxSropDomain.getTypeCodeRsPh() == null ? "" : equifaxSropDomain.getTypeCodeRsPh()); 
						equifaxSRespType.setCOUNTRYCODE_RS_PH(equifaxSropDomain.getCountryCodeRsPh() == null ? "" : equifaxSropDomain.getCountryCodeRsPh()); 
						equifaxSRespType.setAREACODE_RS_PH(equifaxSropDomain.getAreaCodeRsPh() == null ? "" : equifaxSropDomain.getAreaCodeRsPh()); 
						equifaxSRespType.setPHNUMBER_RS_PH(equifaxSropDomain.getPhNumberRsPh() == null ? "" : equifaxSropDomain.getPhNumberRsPh()); 
						equifaxSRespType.setPHNOEXTENTION_RS_PH(equifaxSropDomain.getPhNoExtentionRsPh() == null ? "" : equifaxSropDomain.getPhNoExtentionRsPh()); 
						equifaxSRespType.setCUSTOMERCODE_RS_HD(equifaxSropDomain.getCustomercodeRsHd() == null ? "" : equifaxSropDomain.getCustomercodeRsHd()); 
						equifaxSRespType.setCLIENTID_RS_HD(equifaxSropDomain.getClientIdRsHd() == null ? "" : equifaxSropDomain.getClientIdRsHd()); 
						equifaxSRespType.setCUSTREFFIELD_RS_HD(equifaxSropDomain.getCustRefFieldRsHd() == null ? "" : equifaxSropDomain.getCustRefFieldRsHd()); 
						equifaxSRespType.setREPORTORDERNO_RS_HD(equifaxSropDomain.getReportOrderNoRsHd() == null ? "" : equifaxSropDomain.getReportOrderNoRsHd()); 
						equifaxSRespType.setPRODUCTCODE_RS_HD(equifaxSropDomain.getProductCodeRsHd() == null ? "" : equifaxSropDomain.getProductCodeRsHd()); 
						equifaxSRespType.setPRODUCTVERSION_RS_HD(equifaxSropDomain.getProductVersionRsHd() == null ? "" : equifaxSropDomain.getProductVersionRsHd()); 
						equifaxSRespType.setSUCCESSCODE_RS_HD(equifaxSropDomain.getSuccessCodeRsHd() == null ? "" : equifaxSropDomain.getSuccessCodeRsHd()); 
						equifaxSRespType.setMATCHTYPE_RS_HD(equifaxSropDomain.getMatchTypeRsHd() == null ? "" : equifaxSropDomain.getMatchTypeRsHd()); 
						equifaxSRespType.setRESDATE_RS_HD(equifaxSropDomain.getResDateRsHd() == null ? "" : equifaxSropDomain.getResDateRsHd()); 
						equifaxSRespType.setRESTIME_RS_HD(equifaxSropDomain.getResTimeRsHd() == null ? "" : equifaxSropDomain.getResTimeRsHd()); 
						equifaxSRespType.setREPORTEDDATE_RS_AC(equifaxSropDomain.getReportedDateRsAc() == null ? "" : equifaxSropDomain.getReportedDateRsAc()); 
						equifaxSRespType.setCLIENTNAME_RS_AC(equifaxSropDomain.getClientNameRsAc() == null ? "" : equifaxSropDomain.getClientNameRsAc()); 
						equifaxSRespType.setACCOUNTNUMBER_RS_AC(equifaxSropDomain.getAccountNumberRsAc() == null ? "" : equifaxSropDomain.getAccountNumberRsAc()); 
						equifaxSRespType.setCURRENTBALANCE_RS_AC(equifaxSropDomain.getCurrentBalanceRsAc() == null ? "" : equifaxSropDomain.getCurrentBalanceRsAc()); 
						equifaxSRespType.setINSTITUTION_RS_AC(equifaxSropDomain.getInstitutionRsAc() == null ? "" : equifaxSropDomain.getInstitutionRsAc()); 
						equifaxSRespType.setACCOUNTTYPE_RS_AC(equifaxSropDomain.getAccountTypeRsAc() == null ? "" : equifaxSropDomain.getAccountTypeRsAc()); 
						equifaxSRespType.setOWNERSHIPTYPE_RS_AC(equifaxSropDomain.getOwnershipTypeRsAc() == null ? "" : equifaxSropDomain.getOwnershipTypeRsAc()); 
						equifaxSRespType.setBALANCE_RS_AC(equifaxSropDomain.getBalanceRsAc() == null ? "" : Integer.toString(equifaxSropDomain.getBalanceRsAc())); 
						equifaxSRespType.setPASTDUEAMT_RS_AC(equifaxSropDomain.getPastDueAmtRsAc() == null ? "" : Integer.toString(equifaxSropDomain.getPastDueAmtRsAc())); 
						equifaxSRespType.setDISBURSEDAMOUNT_RS_AC(equifaxSropDomain.getDisbursedAmountRsAc() == null ? "" : equifaxSropDomain.getDisbursedAmountRsAc()); 
						equifaxSRespType.setLOANCATEGORY_RS_AC(equifaxSropDomain.getLoanCategoryRsAc() == null ? "" : equifaxSropDomain.getLoanCategoryRsAc()); 
						equifaxSRespType.setLOANPURPOSE_RS_AC(equifaxSropDomain.getLoanPurposeRsAc() == null ? "" : equifaxSropDomain.getLoanPurposeRsAc()); 
						equifaxSRespType.setLASTPAYMENT_RS_AC(equifaxSropDomain.getLastPaymentRsAc() == null ? "" : Integer.toString(equifaxSropDomain.getLastPaymentRsAc())); 
						equifaxSRespType.setWRITEOFFAMT_RS_AC(equifaxSropDomain.getWriteOffAmtRsAc() == null ? "" : Integer.toString(equifaxSropDomain.getWriteOffAmtRsAc())); 
						equifaxSRespType.setACCOPEN_RS_AC(equifaxSropDomain.getAccOpenRsAc() == null ? "" : equifaxSropDomain.getAccOpenRsAc()); 
						equifaxSRespType.setSACTIONAMT_RS_AC(equifaxSropDomain.getSactionAmtRsAc() == null ? "" : Integer.toString(equifaxSropDomain.getSactionAmtRsAc())); 
						equifaxSRespType.setLASTPAYMENTDATE_RS_AC(equifaxSropDomain.getLastPaymentDateRsAc() == null ? "" : equifaxSropDomain.getLastPaymentDateRsAc()); 
						equifaxSRespType.setHIGHCREDIT_RS_AC(equifaxSropDomain.getHighCreditRsAc() == null ? "" : Integer.toString(equifaxSropDomain.getHighCreditRsAc())); 
						equifaxSRespType.setDATEREPORTED_RS_AC(equifaxSropDomain.getDateReportedRsAc() == null ? "" : equifaxSropDomain.getDateReportedRsAc()); 
						equifaxSRespType.setDATEOPENED_RS_AC(equifaxSropDomain.getDateOpenedRsAc() == null ? "" : equifaxSropDomain.getDateOpenedRsAc()); 
						equifaxSRespType.setDATECLOSED_RS_AC(equifaxSropDomain.getDateClosedRsAc() == null ? "" : equifaxSropDomain.getDateClosedRsAc()); 
						equifaxSRespType.setREASON_RS_AC(equifaxSropDomain.getReasonRsAc() == null ? "" : equifaxSropDomain.getReasonRsAc()); 
						equifaxSRespType.setDATEWRITTENOFF_RS_AC(equifaxSropDomain.getDateWrittenOffRsAc() == null ? "" : equifaxSropDomain.getDateWrittenOffRsAc()); 
						equifaxSRespType.setLOANCYCLEID_RS_AC(equifaxSropDomain.getLoanCycleIdRsAc() == null ? "" : equifaxSropDomain.getLoanCycleIdRsAc()); 
						equifaxSRespType.setDATESANCTIONED_RS_AC(equifaxSropDomain.getDateSanctionedRsAc() == null ? "" : equifaxSropDomain.getDateSanctionedRsAc()); 
						equifaxSRespType.setDATEAPPLIED_RS_AC(equifaxSropDomain.getDateAppliedRsAc() == null ? "" : equifaxSropDomain.getDateAppliedRsAc()); 
						equifaxSRespType.setINTRESTRATE_RS_AC(equifaxSropDomain.getIntrestRateRSAC() == null ? "" : equifaxSropDomain.getIntrestRateRSAC()); 
						equifaxSRespType.setAPPLIEDAMOUNT_RS_AC(equifaxSropDomain.getAppliedAmountRsAc() == null ? "" : equifaxSropDomain.getAppliedAmountRsAc()); 
						equifaxSRespType.setNOOFINSTALLMENTS_RS_AC(equifaxSropDomain.getRepaymentTenureRsAc() == null ? "" : equifaxSropDomain.getRepaymentTenureRsAc()); 
						equifaxSRespType.setREPAYMENTTENURE_RS_AC(equifaxSropDomain.getRepaymentTenureRsAc() == null ? "" : equifaxSropDomain.getRepaymentTenureRsAc()); 
						equifaxSRespType.setDISPUTECODE_RS_AC(equifaxSropDomain.getDisputeCodeRsAc() == null ? "" : equifaxSropDomain.getDisputeCodeRsAc()); 
						equifaxSRespType.setINSTALLMENTAMT_RS_AC(equifaxSropDomain.getInstallmentAmtRsAc() == null ? "" : equifaxSropDomain.getInstallmentAmtRsAc()); 
						equifaxSRespType.setKEYPERSON_RS_AC(equifaxSropDomain.getKeyPersonRsAc() == null ? "" : equifaxSropDomain.getKeyPersonRsAc()); 
						equifaxSRespType.setNOMINEE_RS_AC(equifaxSropDomain.getNomineeRsAc() == null ? "" : equifaxSropDomain.getNomineeRsAc()); 
						equifaxSRespType.setTERMFREQUENCY_RS_AC(equifaxSropDomain.getTermFrequencyRsAc() == null ? "" : equifaxSropDomain.getTermFrequencyRsAc()); 
						equifaxSRespType.setCREDITLIMIT_RS_AC(equifaxSropDomain.getCreditLimitRsAc() == null ? "" : Integer.toString(equifaxSropDomain.getCreditLimitRsAc())); 
						equifaxSRespType.setCOLLATERALVALUE_RS_AC(equifaxSropDomain.getCollateralValueRsAc() == null ? "" : equifaxSropDomain.getCollateralValueRsAc()); 
						equifaxSRespType.setCOLLATERALTYPE_RS_AC(equifaxSropDomain.getCollateralTypeRsAc() == null ? "" : equifaxSropDomain.getCollateralTypeRsAc()); 
						equifaxSRespType.setACCOUNTSTATUS_RS_AC(equifaxSropDomain.getAccountStatusRsAc() == null ? "" : equifaxSropDomain.getAccountStatusRsAc()); 
						equifaxSRespType.setASSETCLASSIFICATION_RS_AC(equifaxSropDomain.getAssetClassificationRsAc() == null ? "" : equifaxSropDomain.getAssetClassificationRsAc()); 
						equifaxSRespType.setSUITFILEDSTATUS_RS_AC(equifaxSropDomain.getSuitFiledStatusRsAc() == null ? "" : equifaxSropDomain.getSuitFiledStatusRsAc()); 
						equifaxSRespType.setMONTHKEY_RS_AC_HIS24(equifaxSropDomain.getMonthKeyRsAcHis24() == null ? "" : equifaxSropDomain.getMonthKeyRsAcHis24()); 
						equifaxSRespType.setPAYMENTSTATUS_RS_AC_HIS24(equifaxSropDomain.getPaymentStatusRsAcHis24() == null ? "" : equifaxSropDomain.getPaymentStatusRsAcHis24()); 
						equifaxSRespType.setSUITFILEDSTATUS_RS_AC_HIS24(equifaxSropDomain.getSuitFiledStatusRsAcHis24() == null ? "" : equifaxSropDomain.getSuitFiledStatusRsAcHis24()); 
						equifaxSRespType.setASSETTCLSSTATUS_RS_AC_HIS24(equifaxSropDomain.getAssettClsStatusRsAcHis24() == null ? "" : equifaxSropDomain.getAssettClsStatusRsAcHis24()); 
						equifaxSRespType.setMONTHKEY_RS_AC_HIS48(equifaxSropDomain.getMonthKeyRsAcHis48() == null ? "" : equifaxSropDomain.getMonthKeyRsAcHis48()); 
						equifaxSRespType.setPAYMENTSTATUS_RS_AC_HIS48(equifaxSropDomain.getpAYMENTSTATUSRsAcHis48() == null ? "" : equifaxSropDomain.getpAYMENTSTATUSRsAcHis48()); 
						equifaxSRespType.setSUITFILEDSTATUS_RS_AC_HIS48(equifaxSropDomain.getsUITFILEDSTATUSRsAcHis48() == null ? "" : equifaxSropDomain.getsUITFILEDSTATUSRsAcHis48()); 
						equifaxSRespType.setASSETCLSSTATUS_RS_AC_HIS48(equifaxSropDomain.getaSSETCLSSTATUSRsAcHis48() == null ? "" : equifaxSropDomain.getaSSETCLSSTATUSRsAcHis48()); 
						equifaxSRespType.setNOOFACCOUNTS_RS_AC_SUM(equifaxSropDomain.getNoOfAccountsRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getNoOfAccountsRsAcSum())); 
						equifaxSRespType.setNOOFACTIVEACCOUNTS_RS_AC_SUM(equifaxSropDomain.getNoOfAccountsRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getNoOfAccountsRsAcSum())); 
						equifaxSRespType.setNOOFWRITEOFFS_RS_AC_SUM(equifaxSropDomain.getNoOfWriteOffsRsAcSum() == null ? "" :Integer.toString(equifaxSropDomain.getNoOfWriteOffsRsAcSum())); 
						equifaxSRespType.setTOTALPASTDUE_RS_AC_SUM(equifaxSropDomain.getTotalPastDueRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getTotalPastDueRsAcSum())); 
						equifaxSRespType.setMOSTSRVRSTAWHIN24MON_RS_AC_SUM(equifaxSropDomain.getMostSrvrStaWhin24MonRsAcSum() == null ? "" : equifaxSropDomain.getMostSrvrStaWhin24MonRsAcSum()); 
						equifaxSRespType.setSINGLEHIGHESTCREDIT_RS_AC_SUM(equifaxSropDomain.getSingleHighestCreditRsAcSum() == null ? "" :Integer.toString( equifaxSropDomain.getSingleHighestCreditRsAcSum())); 
						equifaxSRespType.setSINGLEHIGHSANCTAMT_RS_AC_SUM(equifaxSropDomain.getSingleHighSanctAmtRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getSingleHighSanctAmtRsAcSum())); 
						equifaxSRespType.setTOTALHIGHCREDIT_RS_AC_SUM(equifaxSropDomain.getTotalHighCreditRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getTotalHighCreditRsAcSum())); 
						equifaxSRespType.setAVERAGEOPENBALANCE_RS_AC_SUM(equifaxSropDomain.getAverageOpenBalanceRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getAverageOpenBalanceRsAcSum())); 
						equifaxSRespType.setSINGLEHIGHESTBALANCE_RS_AC_SUM(equifaxSropDomain.getSingleHighestBalanceRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getSingleHighestBalanceRsAcSum())); 
						equifaxSRespType.setNOOFPASTDUEACCTS_RS_AC_SUM(equifaxSropDomain.getNoOfPastDueAcctsRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getNoOfPastDueAcctsRsAcSum())); 
						equifaxSRespType.setNOOFZEROBALACCTS_RS_AC_SUM(equifaxSropDomain.getNoOfZeroBalAcctsRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getNoOfZeroBalAcctsRsAcSum())); 
						equifaxSRespType.setRECENTACCOUNT_RS_AC_SUM(equifaxSropDomain.getRecentAccountRsAcSum() == null ? "" : equifaxSropDomain.getRecentAccountRsAcSum()); 
						equifaxSRespType.setOLDESTACCOUNT_RS_AC_SUM(equifaxSropDomain.getOldestAccountRsAcSum() == null ? "" : equifaxSropDomain.getOldestAccountRsAcSum()); 
						equifaxSRespType.setTOTALBALAMT_RS_AC_SUM(equifaxSropDomain.getTotalBalAmtRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getTotalBalAmtRsAcSum())); 
						equifaxSRespType.setTOTALSANCTAMT_RS_AC_SUM(equifaxSropDomain.getTotalSanctAmtRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getTotalSanctAmtRsAcSum())); 
						equifaxSRespType.setTOTALCRDTLIMIT_RS_AC_SUM(equifaxSropDomain.getTotalCrdtLimitRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getTotalCrdtLimitRsAcSum())); 
						equifaxSRespType.setTOTALMONLYPYMNTAMT_RS_AC_SUM(equifaxSropDomain.getTotalMonlyPymntAmtRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getTotalMonlyPymntAmtRsAcSum())); 
						equifaxSRespType.setTOTALWRITTENOFFAMNT_RS_AC_SUM(equifaxSropDomain.getTotalWrittenOffAmntRsAcSum() == null ? "" : Integer.toString(equifaxSropDomain.getTotalWrittenOffAmntRsAcSum())); 
						equifaxSRespType.setAGEOFOLDESTTRADE_RS_OTK(equifaxSropDomain.getAgeOfOldestTradeRsOtk() == null ? "" : equifaxSropDomain.getAgeOfOldestTradeRsOtk()); 
						equifaxSRespType.setNUMBEROFOPENTRADES_RS_OTK(equifaxSropDomain.getNumberOfOpenTradesRsOtk() == null ? "" : equifaxSropDomain.getNumberOfOpenTradesRsOtk()); 
						equifaxSRespType.setALINESEVERWRITTEN_RS_OTK(equifaxSropDomain.getAlineSeverWrittenRsOtk() == null ? "" : equifaxSropDomain.getAlineSeverWrittenRsOtk()); 
						equifaxSRespType.setALINESVWRTNIN9MNTHS_RS_OTK(equifaxSropDomain.getAlineSvWrtnIn9MnthsRsOtk() == null ? "" : equifaxSropDomain.getAlineSvWrtnIn9MnthsRsOtk()); 
						equifaxSRespType.setALINESVRWRTNIN6MNTHS_RS_OTK(equifaxSropDomain.getAlineSvrWrTnIn6MnthsRsOtk() == null ? "" : equifaxSropDomain.getAlineSvrWrTnIn6MnthsRsOtk()); 
						equifaxSRespType.setREPORTEDDATE_RS_EQ_SM(equifaxSropDomain.getReportedDateRsEqSm() == null ? "" : equifaxSropDomain.getReportedDateRsEqSm()); 
						equifaxSRespType.setPURPOSE_RS_EQ_SM(equifaxSropDomain.getPurposeRsEqSm() == null ? "" : equifaxSropDomain.getPurposeRsEqSm()); 
						equifaxSRespType.setTOTAL_RS_EQ_SM(equifaxSropDomain.getTotalRsEqSm() == null ? "" : equifaxSropDomain.getTotalRsEqSm()); 
						equifaxSRespType.setPAST30DAYS_RS_EQ_SM(equifaxSropDomain.getPast30DaysRsEqSm() == null ? "" : equifaxSropDomain.getPast30DaysRsEqSm()); 
						equifaxSRespType.setPAST12MONTHS_RS_EQ_SM(equifaxSropDomain.getPast12MonthsRsEqSm() == null ? "" : equifaxSropDomain.getPast12MonthsRsEqSm()); 
						equifaxSRespType.setPAST24MONTHS_RS_EQ_SM(equifaxSropDomain.getPast24MonthsRsEqSm() == null ? "" : equifaxSropDomain.getPast24MonthsRsEqSm()); 
						equifaxSRespType.setRECENT_RS_EQ_SM(equifaxSropDomain.getRecentRsEqSm() == null ? "" : equifaxSropDomain.getRecentRsEqSm()); 
						equifaxSRespType.setREPORTEDDATE_RS_EQ(equifaxSropDomain.getReportedDateRsEq() == null ? "" : equifaxSropDomain.getReportedDateRsEq()); 
						equifaxSRespType.setENQREFERENCE_RS_EQ(equifaxSropDomain.getEnqReferenceRsEq() == null ? "" : equifaxSropDomain.getEnqReferenceRsEq()); 
						equifaxSRespType.setENQDATE_RS_EQ(equifaxSropDomain.getEnqDateRsEq() == null ? "" : equifaxSropDomain.getEnqDateRsEq()); 
						equifaxSRespType.setENQTIME_RS_EQ(equifaxSropDomain.getEnqTimeRsEq() == null ? "" : equifaxSropDomain.getEnqTimeRsEq()); 
						equifaxSRespType.setREQUESTPURPOSE_RS_EQ(equifaxSropDomain.getRequestPurposeRsEq() == null ? "" : equifaxSropDomain.getRequestPurposeRsEq()); 
						equifaxSRespType.setAMOUNT_RS_EQ(equifaxSropDomain.getAmountRsEq() == null ? "" : Integer.toString(equifaxSropDomain.getAmountRsEq())); 
						equifaxSRespType.setACCOUNTSDELIQUENT_RS_RE_ACT(equifaxSropDomain.getAccountsDeliquentRsReAct() == null ? "" : Integer.toString(equifaxSropDomain.getAccountsDeliquentRsReAct())); 
						equifaxSRespType.setACCOUNTSOPENED_RS_RE_ACT(equifaxSropDomain.getAccountsOpenedRsReAct() == null ? "" : Integer.toString(equifaxSropDomain.getAccountsOpenedRsReAct())); 
						equifaxSRespType.setTOTALINQUIRIES_RS_RE_ACT(equifaxSropDomain.getTotalInquiriesRsReAct() == null ? "" : Integer.toString(equifaxSropDomain.getTotalInquiriesRsReAct())); 
						equifaxSRespType.setACCOUNTSUPDATED_RS_RE_ACT(equifaxSropDomain.getAccountsUpdatedRsReAct() == null ? "" : Integer.toString(equifaxSropDomain.getAccountsUpdatedRsReAct())); 
						equifaxSRespType.setINSTITUTION_GRP_SM(equifaxSropDomain.getInstitutionGrpSm() == null ? "" : equifaxSropDomain.getInstitutionGrpSm()); 
						equifaxSRespType.setCURRENTBALANCE_GRP_SM(equifaxSropDomain.getCurrentBalanceGrpSm() == null ? "" : equifaxSropDomain.getCurrentBalanceGrpSm()); 
						equifaxSRespType.setSTATUS_GRP_SM(equifaxSropDomain.getStatusGrpSm() == null ? "" : equifaxSropDomain.getStatusGrpSm()); 
						equifaxSRespType.setDATEREPORTED_GRP_SM(equifaxSropDomain.getDateReportedGrpSm() == null ? "" : equifaxSropDomain.getDateReportedGrpSm()); 
						equifaxSRespType.setNOOFMEMBERS_GRP_SM(equifaxSropDomain.getNoOfMembersGrpSm() == null ? "" : equifaxSropDomain.getNoOfMembersGrpSm()); 
						equifaxSRespType.setPASTDUEAMOUNT_GRP_SM(equifaxSropDomain.getPastDueAmountGrpSm() == null ? "" : equifaxSropDomain.getPastDueAmountGrpSm()); 
						equifaxSRespType.setSANCTIONAMOUNT_GRP_SM(equifaxSropDomain.getSanctionAmountGrpSm() == null ? "" : equifaxSropDomain.getSanctionAmountGrpSm()); 
						equifaxSRespType.setDATEOPENED_GRP_SM(equifaxSropDomain.getDateOpenedGrpSm() == null ? "" : equifaxSropDomain.getDateOpenedGrpSm()); 
						equifaxSRespType.setACCOUNTNO_GRP_SM(equifaxSropDomain.getAccountNoGrpSm() == null ? "" : equifaxSropDomain.getAccountNoGrpSm()); 
						equifaxSRespType.setMEMBERSPASTDUE_GRP_SM(equifaxSropDomain.getMembersPastDueGrpSm() == null ? "" : equifaxSropDomain.getMembersPastDueGrpSm()); 
						equifaxSRespType.setWRITEOFFAMOUNT_GRP_SM(equifaxSropDomain.getWriteOffAmountGrpSm() == null ? "" : equifaxSropDomain.getWriteOffAmountGrpSm()); 
						equifaxSRespType.setWRITEOFFDATE_GRP_SM(equifaxSropDomain.getWriteOffDateGrpSm() == null ? "" : equifaxSropDomain.getWriteOffDateGrpSm()); 
						equifaxSRespType.setREASONCODE_SC(equifaxSropDomain.getReasonCodeSc() == null ? "" : equifaxSropDomain.getReasonCodeSc()); 
						equifaxSRespType.setSCOREDESCRIPTION_SC(equifaxSropDomain.getScoreDescriptionSc() == null ? "" : equifaxSropDomain.getScoreDescriptionSc()); 
						equifaxSRespType.setSCORENAME_SC(equifaxSropDomain.getScoreNameSc() == null ? "" : equifaxSropDomain.getScoreNameSc()); 
						equifaxSRespType.setSCOREVALUE_SC(equifaxSropDomain.getScoreValueSc() == null ? "" : equifaxSropDomain.getScoreValueSc()); 
						equifaxSRespType.setOUTPUT_WRITE_FLAG(equifaxSropDomain.getOutputWriteFlag() == null ? "" : equifaxSropDomain.getOutputWriteFlag()); 
						equifaxSRespType.setOUTPUT_WRITE_TIME(equifaxSropDomain.getOutputWriteTime() == null ? "" : equifaxSropDomain.getOutputWriteTime()); 
						equifaxSRespType.setOUTPUT_READ_TIME(equifaxSropDomain.getOutputReadTime() == null ? "" : equifaxSropDomain.getOutputReadTime()); 
						equifaxSRespType.setACCNT_NUM(equifaxSropDomain.getAccntNum() == null ? "" : equifaxSropDomain.getAccntNum()); 
						equifaxSRespType.setDISPUTE_COMMENTS(equifaxSropDomain.getDisputeComments() == null ? "" : equifaxSropDomain.getDisputeComments()); 
						equifaxSRespType.setSTATUS(equifaxSropDomain.getStatus() == null ? "" : equifaxSropDomain.getStatus()); 
						equifaxSRespType.setRESOLVED_DATE(equifaxSropDomain.getResolvedDate() == null ? "" : equifaxSropDomain.getResolvedDate()); 
						equifaxSRespType.setCODE(equifaxSropDomain.getCode() == null ? "" : equifaxSropDomain.getCode()); 
						equifaxSRespType.setDESCRIPTION(equifaxSropDomain.getDescription() == null ? "" : equifaxSropDomain.getDescription()); 
						equifaxSRespType.setHITCODE_HD(equifaxSropDomain.getHitCodeHd() == null ? "" : equifaxSropDomain.getHitCodeHd()); 
						equifaxSRespType.setFULLNAME_NM_TP(equifaxSropDomain.getFullnameNmTp() == null ? "" : equifaxSropDomain.getFullnameNmTp()); 
						equifaxSRespType.setEMPPHONE_RS_EMDL(equifaxSropDomain.getEmpPhoneRsEmdl() == null ? "" : equifaxSropDomain.getEmpPhoneRsEmdl());

						equifaxSRespTypeList.add(equifaxSRespType);
					
					}
					responseJSONType.setEQUIFAX_SROP_DOMAIN_LIST(equifaxSRespTypeList.toArray(new EquifaxSRespType[equifaxSRespTypeList.size()]));
				}
			}

		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** equifaxSropMapping >>");
		return responseJSONType;
	}

	public ResponseJSONType cibilEropMapping(Object responseJsonObject,ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> cibilEropMapping ");
		try {
			CIBILEropDomainObject cibilEropDomainObject = (CIBILEropDomainObject) responseJsonObject;

			if (cibilEropDomainObject != null) {
				List<CIBILEROPErrorDomain> cibilEropDomainList = cibilEropDomainObject.getCibilEropDomainList();

				if (cibilEropDomainList != null&& cibilEropDomainList.size() > 0) {
					List<CibilERespType> cibilERespTypeList = new ArrayList<CibilERespType>();
					for (CIBILEROPErrorDomain cibilEropDomain : cibilEropDomainList) {

						CibilERespType cibilERespType = new CibilERespType();
						cibilERespType.setSRNO(cibilEropDomain.getSrNo_() == null ? "" : cibilEropDomain.getSrNo_()); 
						cibilERespType.setSOA_SOURCE_NAME(cibilEropDomain.getSoaSourceName_() == null ? "" : cibilEropDomain.getSoaSourceName_()); 
						cibilERespType.setMEMBER_REFERENCE_NUMBER(cibilEropDomain.getMemberReferenceNo_() == null ? "" : cibilEropDomain.getMemberReferenceNo_()); 
						cibilERespType.setENQUIRY_DATE(cibilEropDomain.getEnquiryDate_() == null ? "" : cibilEropDomain.getEnquiryDate_()); 
						cibilERespType.setSEGMENT_TAG(cibilEropDomain.getSegmentTag_() == null ? "" : cibilEropDomain.getSegmentTag_()); 
						cibilERespType.setTYPE_OF_ERROR(cibilEropDomain.getTypeOfError_() == null ? "" : cibilEropDomain.getTypeOfError_()); 
						cibilERespType.setDATE_PROCESSED(cibilEropDomain.getDateProcessed_() == null ? "" : cibilEropDomain.getDateProcessed_()); 
						cibilERespType.setTIME_PROCESSED(cibilEropDomain.getTimeProcessed_() == null ? "" : cibilEropDomain.getTimeProcessed_()); 
						cibilERespType.setENQUIRY_MEMBER_CODE_ID(cibilEropDomain.getEnquiryMemberCodeId_() == null ? "" : cibilEropDomain.getEnquiryMemberCodeId_()); 
						cibilERespType.setERROR_TYPE_CODE(cibilEropDomain.getErrorTypeCode_() == null ? "" : cibilEropDomain.getErrorTypeCode_()); 
						cibilERespType.setERROR_TYPE(cibilEropDomain.getErrorType_() == null ? "" : cibilEropDomain.getErrorType_()); 
						cibilERespType.setDATA(cibilEropDomain.getData_() == null ? "" : cibilEropDomain.getData_()); 
						cibilERespType.setSEGMENT(cibilEropDomain.getSegment_() == null ? "" : cibilEropDomain.getSegment_()); 
						cibilERespType.setSPECIFIED(cibilEropDomain.getSpecified_() == null ? "" : cibilEropDomain.getSpecified_()); 
						cibilERespType.setEXPECTED(cibilEropDomain.getExpected_() == null ? "" : cibilEropDomain.getExpected_()); 
						cibilERespType.setINVALID_VERSION(cibilEropDomain.getInvalidVersion_() == null ? "" : cibilEropDomain.getInvalidVersion_()); 
						cibilERespType.setINVALID_FIELD_LENGTH(cibilEropDomain.getInvalidFieldLength_() == null ? "" : cibilEropDomain.getInvalidFieldLength_()); 
						cibilERespType.setINVALID_TOTAL_LENGTH(cibilEropDomain.getInvalidTotalLength_() == null ? "" : cibilEropDomain.getInvalidTotalLength_()); 
						cibilERespType.setINVALID_ENQUIRY_PURPOSE(cibilEropDomain.getInvalidEnquiryPurpose_() == null ? "" : cibilEropDomain.getInvalidEnquiryPurpose_()); 
						cibilERespType.setINVALID_ENQUIRY_AMOUNT(cibilEropDomain.getInvalidEnquiryAmount_() == null ? "" : cibilEropDomain.getInvalidEnquiryAmount_()); 
						cibilERespType.setINVALID_ENQUIRY_MEMBER_USER_ID(cibilEropDomain.getInvalidEnquiryMemberUserId_() == null ? "" : cibilEropDomain.getInvalidEnquiryMemberUserId_()); 
						cibilERespType.setREQUIRED_ENQ_SEG_MISSING(cibilEropDomain.getRequiredEnquirySegmentMissing_() == null ? "" : cibilEropDomain.getRequiredEnquirySegmentMissing_()); 
						cibilERespType.setINVALID_ENQUIRY_DATA(cibilEropDomain.getInvalidEnquiryData_() == null ? "" : cibilEropDomain.getInvalidEnquiryData_()); 
						cibilERespType.setCIBIL_SYSTEM_ERROR(cibilEropDomain.getCibilSystemError_() == null ? "" : cibilEropDomain.getCibilSystemError_()); 
						cibilERespType.setINVALID_SEGMENT_TAG(cibilEropDomain.getInvalidSegmentTag_() == null ? "" : cibilEropDomain.getInvalidSegmentTag_()); 
						cibilERespType.setINVALID_SEGMENT_ORDER(cibilEropDomain.getInvalidSegmentOrder_() == null ? "" : cibilEropDomain.getInvalidSegmentOrder_()); 
						cibilERespType.setINVALID_FIELD_TAG_ORDER(cibilEropDomain.getInvalidFieldTagOrder_() == null ? "" : cibilEropDomain.getInvalidFieldTagOrder_()); 
						cibilERespType.setMISSING_REQUIRED_FIELD(cibilEropDomain.getMissingRequiredField_() == null ? "" : cibilEropDomain.getMissingRequiredField_()); 
						cibilERespType.setREQUESTED_RESP_SIZE_EXCEEDED(cibilEropDomain.getRequestedRespSizeExceeded_() == null ? "" : cibilEropDomain.getRequestedRespSizeExceeded_()); 
						cibilERespType.setINVALID_INPUT_OUTPUT_MEDIA(cibilEropDomain.getInvalidInputOutputMedia_() == null ? "" : cibilEropDomain.getInvalidInputOutputMedia_()); 
						cibilERespType.setOUTPUT_WRITE_FLAG(cibilEropDomain.getOutputWriteFlag_() == null ? "" : cibilEropDomain.getOutputWriteFlag_()); 
						cibilERespType.setOUTPUT_WRITE_TIME(cibilEropDomain.getOutputWriteTime_() == null ? "" : cibilEropDomain.getOutputWriteTime_()); 
						cibilERespType.setOUTPUT_READ_TIME(cibilEropDomain.getOutputReadTime_()==null?"":cibilEropDomain.getOutputReadTime_());

						
						cibilERespTypeList.add(cibilERespType);
					
					}
					responseJSONType.setCIBIL_EROP_DOMAIN_LIST(cibilERespTypeList.toArray(new CibilERespType[cibilERespTypeList.size()]));
				}
			}

		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** cibilEropMapping >>");
		return responseJSONType;
	}

	public  ResponseJSONType cibilSropMapping(Object responseJsonObject,ResponseJSONType responseJSONType) {
		logger_.warn("** SoaSropEropMapper >> cibilSropMapping ");
		try {
			CIBILSropDomainObject cibilSropDomainObject = (CIBILSropDomainObject) responseJsonObject;
			if (cibilSropDomainObject != null) {
				List<HibCIBILSropDomain> hibCibilSropDomainList = cibilSropDomainObject.getCibilSropDomainList();

				if (hibCibilSropDomainList != null&& hibCibilSropDomainList.size() > 0) {
					List<CibilSRespType> cibilSRespTypeList = new ArrayList<CibilSRespType>();
					for (HibCIBILSropDomain hibCibilSropDomain : hibCibilSropDomainList) {
						CibilSRespType cibilSRespType = new CibilSRespType();

							cibilSRespType.setSRNO(hibCibilSropDomain.getSrNo() == null? "": Integer.toString(hibCibilSropDomain.getSrNo()));
							cibilSRespType.setSOA_SOURCE_NAME(hibCibilSropDomain.getSoaSourceName() == null? "": hibCibilSropDomain.getSoaSourceName());
							cibilSRespType.setDATE_PROCESSED(hibCibilSropDomain.getDateProcessed() == null? "": sdf.format(hibCibilSropDomain.getDateProcessed()));
							cibilSRespType.setMEMBER_REFERENCE_NUMBER(hibCibilSropDomain.getMemberReferenceNo() == null? "": hibCibilSropDomain.getMemberReferenceNo());
							cibilSRespType.setSUBJECT_RETURN_CODE(hibCibilSropDomain.getSubjectReturnCode() == null? "": hibCibilSropDomain.getSubjectReturnCode());
							cibilSRespType.setENQUIRY_CONTROL_NUMBER(hibCibilSropDomain.getEnquiryControlNumber() == null? "": hibCibilSropDomain.getEnquiryControlNumber());
							cibilSRespType.setCONSUMER_NAME_FIELD1(hibCibilSropDomain.getConsumerNameField1() == null? "": hibCibilSropDomain.getConsumerNameField1());
							cibilSRespType.setCONSUME_NAME_FIELD2(hibCibilSropDomain.getConsumerNameField2() == null? "": hibCibilSropDomain.getConsumerNameField2());
							cibilSRespType.setCONSUMER_NAME_FIELD3(hibCibilSropDomain.getConsumerNameField3() == null? "": hibCibilSropDomain.getConsumerNameField3());
							cibilSRespType.setCONSUMER_NAME_FIELD4(hibCibilSropDomain.getConsumerNameField4() == null? "": hibCibilSropDomain.getConsumerNameField4());
							cibilSRespType.setCONSUMER_NAME_FIELD5(hibCibilSropDomain.getConsumerNameField5() == null? "": hibCibilSropDomain.getConsumerNameField5());
							cibilSRespType.setDATE_OF_BIRTH(hibCibilSropDomain.getDateOfBirth() == null ? "" : sdf.format(hibCibilSropDomain.getDateOfBirth()));
							cibilSRespType.setGENDER(hibCibilSropDomain.getGenderValue() == null ? "" : hibCibilSropDomain.getGenderValue());
							cibilSRespType.setDT_ERRCODE_PN(hibCibilSropDomain.getDateErrorCodePN() == null? "": sdf.format(hibCibilSropDomain.getDateErrorCodePN()));
							cibilSRespType.setERR_SEG_TAG_PN(hibCibilSropDomain.getErrorSegTagPN() == null? "": hibCibilSropDomain.getErrorSegTagPN());
							cibilSRespType.setERR_CODE_PN(hibCibilSropDomain.getErrorCodePN() == null ? "" : hibCibilSropDomain.getErrorCodePN());
							cibilSRespType.setERROR_PN(hibCibilSropDomain.getErrorPN() == null? "": hibCibilSropDomain.getErrorPN());
							cibilSRespType.setDT_ENTCIBILRECODE_PN(hibCibilSropDomain.getDateEntryCIBILRemarkCodePN() == null? "": sdf.format(hibCibilSropDomain.getDateEntryCIBILRemarkCodePN()));
							cibilSRespType.setCIBIL_REMARK_CODE_PN(hibCibilSropDomain.getCibilRemarkCodePN() == null? "": hibCibilSropDomain.getCibilRemarkCodePN());
							cibilSRespType.setDATE_DISP_REMARK_CODE_PN(hibCibilSropDomain.getDateEntryErrorDisputeRemarkCodePN() == null? "": sdf.format(hibCibilSropDomain.getDateEntryErrorDisputeRemarkCodePN()));
							cibilSRespType.setERR_DISP_REM_CODE1_PN(hibCibilSropDomain.getErrorDisputeRemarkCode1PN() == null? "": hibCibilSropDomain.getErrorDisputeRemarkCode1PN());
							cibilSRespType.setERR_DISP_REM_CODE2_PN(hibCibilSropDomain.getErrorDisputeRemarkCode2PN() == null? "": hibCibilSropDomain.getErrorDisputeRemarkCode2PN());
							cibilSRespType.setID_TYPE(hibCibilSropDomain.getIdType() == null? "": hibCibilSropDomain.getIdType());
							cibilSRespType.setID_NUMBER(hibCibilSropDomain.getIdNumber() == null ? "" : hibCibilSropDomain.getIdNumber());
							cibilSRespType.setISSUE_DATE(hibCibilSropDomain.getIssueDate() == null ? "" :sdf.format(hibCibilSropDomain.getIssueDate()));
							cibilSRespType.setEXPIRATION_DATE(hibCibilSropDomain.getExpirationDate() == null? "": sdf.format(hibCibilSropDomain.getExpirationDate()));
							cibilSRespType.setENRICHED_THROUGH_ENQUIRY_ID(hibCibilSropDomain.getEnrichedThroughEnquiryId() == null? "": hibCibilSropDomain.getEnrichedThroughEnquiryId());
							cibilSRespType.setTELEPHONE_TYPE(hibCibilSropDomain.getTelephoneTypeValue() == null? "": hibCibilSropDomain.getTelephoneTypeValue());
							cibilSRespType.setTELEPHONE_EXTENSION(hibCibilSropDomain.getTelephoneExtension() == null? "": hibCibilSropDomain.getTelephoneExtension());
							cibilSRespType.setTELEPHONE_NUMBER(hibCibilSropDomain.getTelephoneNumber() == null? "": hibCibilSropDomain.getTelephoneNumber());
							cibilSRespType.setENRICHED_THROUGH_ENQUIRY_PT(hibCibilSropDomain.getEnrichedThroughEnquiryPT() == null? "": hibCibilSropDomain.getEnrichedThroughEnquiryPT());
							cibilSRespType.setEMAIL_ID(hibCibilSropDomain.getEmailId() == null? "": hibCibilSropDomain.getEmailId());
							cibilSRespType.setACCOUNT_TYPE(hibCibilSropDomain.getAccountTypeValue() == null? "": hibCibilSropDomain.getAccountTypeValue());
							cibilSRespType.setDATE_REPORTED_AND_CERTIFIED_EM(hibCibilSropDomain.getDateReportedAndCertifiedEM() == null? "": sdf.format(hibCibilSropDomain.getDateReportedAndCertifiedEM()));
							cibilSRespType.setOCCUPATION_CODE_EM(hibCibilSropDomain.getOccupationValueEM() == null? "": hibCibilSropDomain.getOccupationValueEM());
							cibilSRespType.setINCOME_EM(hibCibilSropDomain.getIncomeValueEM() == null? "": hibCibilSropDomain.getIncomeValueEM());
							cibilSRespType.setNET_GROSS_INDICATOR_EM(hibCibilSropDomain.getNetGrossIndicatorValueEM() == null? "": hibCibilSropDomain.getNetGrossIndicatorValueEM());
							cibilSRespType.setMNTHLY_ANNUAL_INDICATOR_EM(hibCibilSropDomain.getMonthlyAnnualIndicatorValueEM() == null? "": hibCibilSropDomain.getMonthlyAnnualIndicatorValueEM());
							cibilSRespType.setDATE_ENTRY_ERR_CODE_EM(hibCibilSropDomain.getDateEntryErrorCodeEM() == null? "": sdf.format(hibCibilSropDomain.getDateEntryErrorCodeEM()));
							cibilSRespType.setERR_CODE_EM(hibCibilSropDomain.getErrorCodeEM() == null ? "" : hibCibilSropDomain.getErrorCodeEM());
							cibilSRespType.setDATE_CIBIL_ERR_CODE_EM(hibCibilSropDomain.getDateCIBILErrorCodeEM() == null? "": sdf.format(hibCibilSropDomain.getDateCIBILErrorCodeEM()));
							cibilSRespType.setCIBIL_REMARK_CODE_EM(hibCibilSropDomain.getCibilRemarkCodeEM() == null? "": hibCibilSropDomain.getCibilRemarkCodeEM());
							cibilSRespType.setDT_DIS_REMARK_CODE_EM(hibCibilSropDomain.getDateDisputeRemarkCodeEM() == null? "": sdf.format(hibCibilSropDomain.getDateDisputeRemarkCodeEM()));
							cibilSRespType.setERR_DISP_REMCODE1_EM(hibCibilSropDomain.getErrorDisputeRemarkCode1EM() == null? "": hibCibilSropDomain.getErrorDisputeRemarkCode1EM());
							cibilSRespType.setERR_DISP_REMCODE2_EM(hibCibilSropDomain.getErrorDisputeRemarkCode2EM() == null? "": hibCibilSropDomain.getErrorDisputeRemarkCode2EM());
							cibilSRespType.setACCOUNT_NUMBER_PI(hibCibilSropDomain.getAccountNumberPI() == null? "": hibCibilSropDomain.getAccountNumberPI());
							cibilSRespType.setSCORE_NAME(hibCibilSropDomain.getScoreName() == null ? "" : hibCibilSropDomain.getScoreName());
							cibilSRespType.setSCORE_CARD_NAME(hibCibilSropDomain.getScoreCardName() == null? "": hibCibilSropDomain.getScoreCardName());
							cibilSRespType.setSCORE_CARD_VERSION(hibCibilSropDomain.getScoreCardVersion() == null? "": hibCibilSropDomain.getScoreCardVersion());
							cibilSRespType.setSCORE_DATE(hibCibilSropDomain.getScoreDate() == null? "": sdf.format(hibCibilSropDomain.getScoreDate()));
							cibilSRespType.setSCORE(hibCibilSropDomain.getScore() == null? "": hibCibilSropDomain.getScore());
							cibilSRespType.setEXCLUSION_CODES_1_TO_5(hibCibilSropDomain.getExclusionCodes1_to_5() == null? "": hibCibilSropDomain.getExclusionCodes1_to_5());
							cibilSRespType.setEXCLUSION_CODES_6_TO_10(hibCibilSropDomain.getExclusionCodes6_TO_10() == null? "": hibCibilSropDomain.getExclusionCodes6_TO_10());
							cibilSRespType.setEXCLUSION_CODES_11_TO_15(hibCibilSropDomain.getExclusionCodes11_TO_15() == null? "": hibCibilSropDomain.getExclusionCodes11_TO_15());
							cibilSRespType.setEXCLUSION_CODES_16_TO_20(hibCibilSropDomain.getExclusionCodes16_TO_20() == null? "": hibCibilSropDomain.getExclusionCodes16_TO_20());
							cibilSRespType.setREASON_CODES_1_TO_5(hibCibilSropDomain.getReasonCodes1_TO_5() == null? "": hibCibilSropDomain.getReasonCodes1_TO_5());
							cibilSRespType.setREASON_CODES_6_TO_10(hibCibilSropDomain.getReasonCodes6_TO_10() == null? "": hibCibilSropDomain.getReasonCodes6_TO_10());
							cibilSRespType.setREASON_CODES_11_TO_15(hibCibilSropDomain.getReasonCodes11_TO_15() == null? "": hibCibilSropDomain.getReasonCodes11_TO_15());
							cibilSRespType.setREASON_CODES_16_TO_20(hibCibilSropDomain.getReasonCodes16_TO_20() == null? "": hibCibilSropDomain.getReasonCodes16_TO_20());
							cibilSRespType.setREASON_CODES_21_TO_25(hibCibilSropDomain.getReasonCodes21_TO_25() == null? "": hibCibilSropDomain.getReasonCodes21_TO_25());
							cibilSRespType.setREASON_CODES_26_TO_30(hibCibilSropDomain.getReasonCodes26_TO_30() == null? "": hibCibilSropDomain.getReasonCodes26_TO_30());
							cibilSRespType.setREASON_CODES_31_TO_35(hibCibilSropDomain.getReasonCodes31_TO_35() == null? "": hibCibilSropDomain.getReasonCodes31_TO_35());
							cibilSRespType.setREASON_CODES_36_TO_40(hibCibilSropDomain.getReasonCodes36_TO_40() == null? "": hibCibilSropDomain.getReasonCodes36_TO_40());
							cibilSRespType.setREASON_CODES_41_TO_45(hibCibilSropDomain.getReasonCodes41_TO_45() == null? "": hibCibilSropDomain.getReasonCodes41_TO_45());
							cibilSRespType.setREASON_CODES_46_TO_50(hibCibilSropDomain.getReasonCodes46_TO_50() == null? "": hibCibilSropDomain.getReasonCodes46_TO_50());
							cibilSRespType.setERROR_CODE_SC(hibCibilSropDomain.getErrorCodeSC() == null ? "" : hibCibilSropDomain.getErrorCodeSC());
							cibilSRespType.setADDRESS_LINE_1(hibCibilSropDomain.getAddressLine1() == null? "": hibCibilSropDomain.getAddressLine1());
							cibilSRespType.setADDRESS_LINE_2(hibCibilSropDomain.getAddressLine2() == null? "": hibCibilSropDomain.getAddressLine2());
							cibilSRespType.setADDRESS_LINE_3(hibCibilSropDomain.getAddressLine3() == null? "": hibCibilSropDomain.getAddressLine3());
							cibilSRespType.setADDRESS_LINE_4(hibCibilSropDomain.getAddressLine4() == null? "": hibCibilSropDomain.getAddressLine4());
							cibilSRespType.setADDRESS_LINE_5(hibCibilSropDomain.getAddressLine5() == null? "": hibCibilSropDomain.getAddressLine5());
							cibilSRespType.setSTATE_CODE(hibCibilSropDomain.getStateCode() == null ? "" : Integer.toString(hibCibilSropDomain.getStateCode()));
							cibilSRespType.setSTATE(hibCibilSropDomain.getState() == null? "": hibCibilSropDomain.getState());
							cibilSRespType.setPINCODE(hibCibilSropDomain.getPincode() == null? "": hibCibilSropDomain.getPincode());
							cibilSRespType.setADDRESS_CATERGORY(hibCibilSropDomain.getAddressCatergoryValue() == null? "": hibCibilSropDomain.getAddressCatergoryValue());
							cibilSRespType.setRESIDENCE_CODE(hibCibilSropDomain.getResidenceValue() == null? "": hibCibilSropDomain.getResidenceValue());
							cibilSRespType.setDATE_REPORTED_PA(hibCibilSropDomain.getDateReportedPA() == null? "": hibCibilSropDomain.getDateReportedPA());
							cibilSRespType.setENRICHED_THROUGH_ENQUIRY_PA(hibCibilSropDomain.getEnrichedThroughEnquiryPA() == null? "": hibCibilSropDomain.getEnrichedThroughEnquiryPA());
							cibilSRespType.setREPO_MEMSHORTNAME_TL(hibCibilSropDomain.getReportMemberShortNameTL() == null? "": hibCibilSropDomain.getReportMemberShortNameTL());
							cibilSRespType.setACCOUNT_NUMBER_TL(hibCibilSropDomain.getAccountNumberTL() == null? "": hibCibilSropDomain.getAccountNumberTL());
							cibilSRespType.setACCOUNT_TYPE_TL(hibCibilSropDomain.getAccountTypeValueTL() == null? "": hibCibilSropDomain.getAccountTypeValueTL());
							cibilSRespType.setOWNERSHIP_INDICATOR_TL(hibCibilSropDomain.getOwnershipIndicatorValueTL() == null? "": hibCibilSropDomain.getOwnershipIndicatorValueTL());
							cibilSRespType.setDATE_OPENED_DISBURSED_TL(hibCibilSropDomain.getDateOpenedDisbursedTL() == null? "": sdf.format(hibCibilSropDomain.getDateOpenedDisbursedTL()));
							cibilSRespType.setDATE_OF_LAST_PAYMENT_TL(hibCibilSropDomain.getDateOfLastPaymentTL() == null? "": sdf.format(hibCibilSropDomain.getDateOfLastPaymentTL()));
							cibilSRespType.setDATE_CLOSED_TL(hibCibilSropDomain.getDateClosedTL() == null? "": sdf.format(hibCibilSropDomain.getDateClosedTL()));
							cibilSRespType.setTL_DATE_REPORTED(hibCibilSropDomain.getDateReportedTL() == null? "": sdf.format(hibCibilSropDomain.getDateReportedTL()));
							cibilSRespType.setHIGH_CREDIT_SANCTIONED_AMOUNT(hibCibilSropDomain.getHighCreditSanctionedAmount() == null? "": hibCibilSropDomain.getHighCreditSanctionedAmount());
							cibilSRespType.setCURRENT_BALANCE_TL(hibCibilSropDomain.getCurrentBalanceTL() == null? "": hibCibilSropDomain.getCurrentBalanceTL());
							cibilSRespType.setAMOUNT_OVERDUE_TL(hibCibilSropDomain.getAmountOverdueTL() == null? "": hibCibilSropDomain.getAmountOverdueTL());
							cibilSRespType.setPAYMENT_HISTORY_1(hibCibilSropDomain.getPaymentHistory1() == null? "": hibCibilSropDomain.getPaymentHistory1());
							cibilSRespType.setPAYMENT_HISTORY_2(hibCibilSropDomain.getPaymentHistory2() == null? "": hibCibilSropDomain.getPaymentHistory2());
							cibilSRespType.setPAYMENT_HISTORY_START_DATE(hibCibilSropDomain.getPaymentHistoryStartDate() == null? "": sdf.format(hibCibilSropDomain.getPaymentHistoryStartDate()));
							cibilSRespType.setPAYMENT_HISTORY_END_DATE(hibCibilSropDomain.getPaymentHistoryEndDate() == null? "":sdf.format(hibCibilSropDomain.getPaymentHistoryEndDate()));
							cibilSRespType.setSUIT_FILED_STATUS_TL(hibCibilSropDomain.getSuitFiledStatusValueTL() == null? "": hibCibilSropDomain.getSuitFiledStatusValueTL());
							cibilSRespType.setWOF_SETTLED_STATUS_TL(hibCibilSropDomain.getWofSettledStatusTL() == null? "": hibCibilSropDomain.getWofSettledStatusTL());
							cibilSRespType.setVALOFCOLLATERAL_TL(hibCibilSropDomain.getCollateralValueTL() == null? "": hibCibilSropDomain.getCollateralValueTL());
							cibilSRespType.setTYPEOFCOLLATERAL_TL(hibCibilSropDomain.getTypeOfCollateralTL() == null? "": hibCibilSropDomain.getTypeOfCollateralTL());
							cibilSRespType.setCREDITLIMIT_TL(hibCibilSropDomain.getCreditLimitTL() == null? "": hibCibilSropDomain.getCreditLimitTL());
							cibilSRespType.setCASHLIMIT_TL(hibCibilSropDomain.getCashLimitTL() == null ? "" : hibCibilSropDomain.getCashLimitTL());
							cibilSRespType.setRATEOFINTREST_TL(hibCibilSropDomain.getRateOfIntrestTL() == null? "": hibCibilSropDomain.getRateOfIntrestTL());
							cibilSRespType.setREPAY_TENURE(hibCibilSropDomain.getRepayTenure() == null ? "" : hibCibilSropDomain.getRepayTenure());
							cibilSRespType.setEMI_AMOUNT_TL(hibCibilSropDomain.getEmiAmountTL() == null ? "" : hibCibilSropDomain.getEmiAmountTL());
							cibilSRespType.setWOF_TOT_AMOUNT_TL(hibCibilSropDomain.getWofTotalAmountTL() == null? "": hibCibilSropDomain.getWofTotalAmountTL());
							cibilSRespType.setWOF_PRINCIPAL_TL(hibCibilSropDomain.getWofPrincipalTL() == null? "": hibCibilSropDomain.getWofPrincipalTL());
							cibilSRespType.setSETTLEMENT_AMOUNT_TL(hibCibilSropDomain.getSettlementAmountTL() == null? "": hibCibilSropDomain.getSettlementAmountTL());
							cibilSRespType.setPAYMENT_FREQUENCY_TL(hibCibilSropDomain.getPaymentFrequencyTL() == null? "": hibCibilSropDomain.getPaymentFrequencyTL());
							cibilSRespType.setACTUAL_PAYMENT_AMOUNT_TL(hibCibilSropDomain.getActualPaymentAmountTL() == null? "": hibCibilSropDomain.getActualPaymentAmountTL());
							cibilSRespType.setDT_ENTERRCODE_TL(hibCibilSropDomain.getDateEntErrorCodeTL() == null? "": sdf.format(hibCibilSropDomain.getDateEntErrorCodeTL()));
							cibilSRespType.setERRCODE_TL(hibCibilSropDomain.getErrorCodeTL() == null ? "" : hibCibilSropDomain.getErrorCodeTL());
							cibilSRespType.setDT_ENTCIBIL_REMARK_CODE_TL(hibCibilSropDomain.getDateEntCIBILRemarkCodeTL() == null? "": sdf.format(hibCibilSropDomain.getDateEntCIBILRemarkCodeTL()));
							cibilSRespType.setCIBIL_REMARKS_CODE_TL(hibCibilSropDomain.getCibilRemarksCodeTL() == null? "": hibCibilSropDomain.getCibilRemarksCodeTL());
							cibilSRespType.setDT_DISPUTE_CODE_TL(hibCibilSropDomain.getDateDisputeCodeTL() == null? "": sdf.format(hibCibilSropDomain.getDateDisputeCodeTL()));
							cibilSRespType.setERR_DISP_REMCODE1_TL(hibCibilSropDomain.getErrorDisputeRemarkCode1TL() == null? "": hibCibilSropDomain.getErrorDisputeRemarkCode1TL());
							cibilSRespType.setERR_DISP_REMCODE2_TL(hibCibilSropDomain.getErrorDisputeRemarkCode2TL() == null? "": hibCibilSropDomain.getErrorDisputeRemarkCode2TL());
							cibilSRespType.setDATE_OF_ENQUIRY_IQ(hibCibilSropDomain.getDateOfEnquiryIQ() == null? "": sdf.format(hibCibilSropDomain.getDateOfEnquiryIQ()));
							cibilSRespType.setENQ_MEMBER_SHORT_NAME_IQ(hibCibilSropDomain.getEnquiryMemberShortNameIQ() == null ? "" : hibCibilSropDomain.getEnquiryMemberShortNameIQ());
							cibilSRespType.setENQUIRY_PURPOSE_IQ(hibCibilSropDomain.getEnquiryPurposeIQ() == null ? "" : hibCibilSropDomain.getEnquiryPurposeIQ());
							cibilSRespType.setENQIURY_AMOUNT_IQ(hibCibilSropDomain.getEnqiuryAmountIQ() == null ? "" : hibCibilSropDomain.getEnqiuryAmountIQ());
							cibilSRespType.setDATE_OF_ENTRY_DR(hibCibilSropDomain.getDateOfEntryDR() == null ? "" : sdf.format(hibCibilSropDomain.getDateOfEntryDR()));
							cibilSRespType.setDISP_REM_LINE1(hibCibilSropDomain.getDisputeRemarkLine1() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine1());
							cibilSRespType.setDISP_REM_LINE2(hibCibilSropDomain.getDisputeRemarkLine2() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine2());
							cibilSRespType.setDISP_REM_LINE3(hibCibilSropDomain.getDisputeRemarkLine3() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine3());
							cibilSRespType.setDISP_REM_LINE4(hibCibilSropDomain.getDisputeRemarkLine4() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine4());
							cibilSRespType.setDISP_REM_LINE5(hibCibilSropDomain.getDisputeRemarkLine5() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine5());
							cibilSRespType.setDISP_REM_LINE6(hibCibilSropDomain.getDisputeRemarkLine6() == null ? "" : hibCibilSropDomain.getDisputeRemarkLine6());
							cibilSRespType.setOUTPUT_WRITE_FLAG(hibCibilSropDomain.getOutputWriteFlag() == null ? "" : hibCibilSropDomain.getOutputWriteFlag());
							cibilSRespType.setOUTPUT_WRITE_TIME(hibCibilSropDomain.getOutputWriteTime() == null ? "" : hibCibilSropDomain.getOutputWriteTime());
							cibilSRespType.setOUTPUT_READ_TIME(hibCibilSropDomain.getOutputReadTime() == null ? "" : hibCibilSropDomain.getOutputReadTime());

							cibilSRespTypeList.add(cibilSRespType);
							
					}
					responseJSONType.setCIBIL_SROP_DOMAIN_LIST(cibilSRespTypeList.toArray(new CibilSRespType[cibilSRespTypeList.size()]));
				}
			}
		} catch (Exception e) {
			logger_.error(e.getMessage(),e);
			e.printStackTrace();
		}
		logger_.warn("** SoaSropEropMapper ** cibilSropMapping >>");
		return responseJSONType;
	}
}
