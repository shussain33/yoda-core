package com.softcell.gonogo.model.mbdatapush.chm;

import java.util.List;

public class RationCardVariation {

	private List<Variation> variations;


	public List<Variation> getVariations() {
		return variations;
	}
	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}


	@Override
	public String toString() {
		return "RationCardVariation [variations=" + variations + "]";
	}
	
	

}
