package com.softcell.gonogo.model.reliance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

public class RelianceDigitalRequest {


    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sMopID")
    private String mopID;

    @JsonProperty("sDoNumber")
    private String doNumber;

    @JsonProperty("sCustomerName")
    private String customerName;

    @JsonProperty("sResiAddress1")
    private String resiAddress1;

    @JsonProperty("sResiAddress2")
    private String resiAddress2;

    @JsonProperty("sResiAddress3")
    private String resiAddress3;

    @JsonProperty("sBrand")
    private String brand;

    @JsonProperty("sAssetCategory")
    private String assetCategory;

    @JsonProperty("sAssetQuantity")
    private String assetQuantity;

    @JsonProperty("sProductType")
    private String productType;

    @JsonProperty("sCostOfProduct")
    private String costOfProduct;

    @JsonProperty("sNetLoanAmount")
    private String netLoanAmount;

    @JsonProperty("sProcessingFee")
    private String processingFee;

    @JsonProperty("sDOStatus")
    private String doStatus;

    @JsonProperty("sCreatedDate")
    private String createdDate;

    @JsonProperty("sDealID")
    private String dealID;

    @JsonProperty("sSchemeName")
    private String schemeName;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("sTenure")
    private String tenure;

    @JsonProperty("sEmailId")
    private String emailId;

    @JsonProperty("sMobileNo")
    private String mobileNo;

    @JsonProperty("sDealerCode")
    private String dealerCode;

    @JsonProperty("sCustomerDownPayment")
    private String customerDownPayment;

    @JsonProperty("sSubvention")
    private String subvention;

    @JsonProperty("sMFRSubvention")
    private String mfrSubvention;

    @JsonProperty("sOtherCharges")
    private String otherCharges;

    @JsonProperty("sNetDisbursement")
    private String netDisbursement;

    @JsonProperty("sMarginMoney")
    private String marginMoney;

    @JsonProperty("sGrossLoanAmount")
    private String grossLoanAmount;

    @JsonProperty("sAdvanceEMI")
    private String advanceEMI;

    @JsonProperty("sDbdPercentage")
    private String dbdPercentage;


    @JsonProperty("sNumAdvTenure")
    private String numAdvTenure;

    @JsonProperty("sLandmark")
    private String landmark;

    @JsonProperty("sCity")
    private String city;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sPincode")
    private String pincode;

    @JsonProperty("sHouseNo")
    private String houseNo;

    @JsonProperty("sFloorNo")
    private String floorNo;

    @JsonProperty("sWingNo")
    private String wingNo;

    @JsonProperty("sBuildingname")
    private String buildingName;

    @JsonProperty("sSocietyName")
    private String societyName;

    @JsonProperty("sPlotNo")
    private String plotNo;

    @JsonProperty("sStreet")
    private String street;

    @JsonProperty("sSector")
    private String sector;

    @JsonProperty("sArea")
    private String area;

    @JsonProperty("sGuid")
    private String guid;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getMopID() {
        return mopID;
    }

    public void setMopID(String mopID) {
        this.mopID = mopID;
    }

    public String getDoNumber() {
        return doNumber;
    }

    public void setDoNumber(String doNumber) {
        this.doNumber = doNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getResiAddress1() {
        return resiAddress1;
    }

    public void setResiAddress1(String resiAddress1) {
        this.resiAddress1 = resiAddress1;
    }

    public String getResiAddress2() {
        return resiAddress2;
    }

    public void setResiAddress2(String resiAddress2) {
        this.resiAddress2 = resiAddress2;
    }

    public String getResiAddress3() {
        return resiAddress3;
    }

    public void setResiAddress3(String resiAddress3) {
        this.resiAddress3 = resiAddress3;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getAssetCategory() {
        return assetCategory;
    }

    public void setAssetCategory(String assetCategory) {
        this.assetCategory = assetCategory;
    }

    public String getAssetQuantity() {
        return assetQuantity;
    }

    public void setAssetQuantity(String assetQuantity) {
        this.assetQuantity = assetQuantity;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCostOfProduct() {
        return costOfProduct;
    }

    public void setCostOfProduct(String costOfProduct) {
        this.costOfProduct = costOfProduct;
    }

    public String getNetLoanAmount() {
        return netLoanAmount;
    }

    public void setNetLoanAmount(String netLoanAmount) {
        this.netLoanAmount = netLoanAmount;
    }

    public String getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(String processingFee) {
        this.processingFee = processingFee;
    }

    public String getDoStatus() {
        return doStatus;
    }

    public void setDoStatus(String doStatus) {
        this.doStatus = doStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDealID() {
        return dealID;
    }

    public void setDealID(String dealID) {
        this.dealID = dealID;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getCustomerDownPayment() {
        return customerDownPayment;
    }

    public void setCustomerDownPayment(String customerDownPayment) {
        this.customerDownPayment = customerDownPayment;
    }

    public String getSubvention() {
        return subvention;
    }

    public void setSubvention(String subvention) {
        this.subvention = subvention;
    }

    public String getMfrSubvention() {
        return mfrSubvention;
    }

    public void setMfrSubvention(String mfrSubvention) {
        this.mfrSubvention = mfrSubvention;
    }

    public String getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(String otherCharges) {
        this.otherCharges = otherCharges;
    }

    public String getNetDisbursement() {
        return netDisbursement;
    }

    public void setNetDisbursement(String netDisbursement) {
        this.netDisbursement = netDisbursement;
    }

    public String getMarginMoney() {
        return marginMoney;
    }

    public void setMarginMoney(String marginMoney) {
        this.marginMoney = marginMoney;
    }

    public String getGrossLoanAmount() {
        return grossLoanAmount;
    }

    public void setGrossLoanAmount(String grossLoanAmount) {
        this.grossLoanAmount = grossLoanAmount;
    }

    public String getAdvanceEMI() {
        return advanceEMI;
    }

    public void setAdvanceEMI(String advanceEMI) {
        this.advanceEMI = advanceEMI;
    }

    public String getDbdPercentage() {
        return dbdPercentage;
    }

    public void setDbdPercentage(String dbdPercentage) {
        this.dbdPercentage = dbdPercentage;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(String floorNo) {
        this.floorNo = floorNo;
    }

    public String getWingNo() {
        return wingNo;
    }

    public void setWingNo(String wingNo) {
        this.wingNo = wingNo;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getSocietyName() {
        return societyName;
    }

    public void setSocietyName(String societyName) {
        this.societyName = societyName;
    }

    public String getPlotNo() {
        return plotNo;
    }

    public void setPlotNo(String plotNo) {
        this.plotNo = plotNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * @return the numAdvTenure
     */
    public String getNumAdvTenure() {
        return numAdvTenure;
    }

    /**
     * @param numAdvTenure the numAdvTenure to set
     */
    public void setNumAdvTenure(String numAdvTenure) {
        this.numAdvTenure = numAdvTenure;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RelianceDigitalRequest [header=");
        builder.append(header);
        builder.append(", refID=");
        builder.append(refID);
        builder.append(", mopID=");
        builder.append(mopID);
        builder.append(", doNumber=");
        builder.append(doNumber);
        builder.append(", customerName=");
        builder.append(customerName);
        builder.append(", resiAddress1=");
        builder.append(resiAddress1);
        builder.append(", resiAddress2=");
        builder.append(resiAddress2);
        builder.append(", resiAddress3=");
        builder.append(resiAddress3);
        builder.append(", brand=");
        builder.append(brand);
        builder.append(", assetCategory=");
        builder.append(assetCategory);
        builder.append(", assetQuantity=");
        builder.append(assetQuantity);
        builder.append(", productType=");
        builder.append(productType);
        builder.append(", costOfProduct=");
        builder.append(costOfProduct);
        builder.append(", netLoanAmount=");
        builder.append(netLoanAmount);
        builder.append(", processingFee=");
        builder.append(processingFee);
        builder.append(", doStatus=");
        builder.append(doStatus);
        builder.append(", createdDate=");
        builder.append(createdDate);
        builder.append(", dealID=");
        builder.append(dealID);
        builder.append(", schemeName=");
        builder.append(schemeName);
        builder.append(", product=");
        builder.append(product);
        builder.append(", tenure=");
        builder.append(tenure);
        builder.append(", emailId=");
        builder.append(emailId);
        builder.append(", mobileNo=");
        builder.append(mobileNo);
        builder.append(", dealerCode=");
        builder.append(dealerCode);
        builder.append(", customerDownPayment=");
        builder.append(customerDownPayment);
        builder.append(", subvention=");
        builder.append(subvention);
        builder.append(", mfrSubvention=");
        builder.append(mfrSubvention);
        builder.append(", otherCharges=");
        builder.append(otherCharges);
        builder.append(", netDisbursement=");
        builder.append(netDisbursement);
        builder.append(", marginMoney=");
        builder.append(marginMoney);
        builder.append(", grossLoanAmount=");
        builder.append(grossLoanAmount);
        builder.append(", advanceEMI=");
        builder.append(advanceEMI);
        builder.append(", dbdPercentage=");
        builder.append(dbdPercentage);
        builder.append(", landmark=");
        builder.append(landmark);
        builder.append(", city=");
        builder.append(city);
        builder.append(", state=");
        builder.append(state);
        builder.append(", pincode=");
        builder.append(pincode);
        builder.append(", houseNo=");
        builder.append(houseNo);
        builder.append(", floorNo=");
        builder.append(floorNo);
        builder.append(", wingNo=");
        builder.append(wingNo);
        builder.append(", buildingName=");
        builder.append(buildingName);
        builder.append(", societyName=");
        builder.append(societyName);
        builder.append(", plotNo=");
        builder.append(plotNo);
        builder.append(", street=");
        builder.append(street);
        builder.append(", sector=");
        builder.append(sector);
        builder.append(", area=");
        builder.append(area);
        builder.append(", guid=");
        builder.append(guid);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((advanceEMI == null) ? 0 : advanceEMI.hashCode());
        result = prime * result + ((area == null) ? 0 : area.hashCode());
        result = prime * result
                + ((assetCategory == null) ? 0 : assetCategory.hashCode());
        result = prime * result
                + ((assetQuantity == null) ? 0 : assetQuantity.hashCode());
        result = prime * result + ((brand == null) ? 0 : brand.hashCode());
        result = prime * result
                + ((buildingName == null) ? 0 : buildingName.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result
                + ((costOfProduct == null) ? 0 : costOfProduct.hashCode());
        result = prime * result
                + ((createdDate == null) ? 0 : createdDate.hashCode());
        result = prime
                * result
                + ((customerDownPayment == null) ? 0 : customerDownPayment
                .hashCode());
        result = prime * result
                + ((customerName == null) ? 0 : customerName.hashCode());
        result = prime * result
                + ((dbdPercentage == null) ? 0 : dbdPercentage.hashCode());
        result = prime * result + ((dealID == null) ? 0 : dealID.hashCode());
        result = prime * result
                + ((dealerCode == null) ? 0 : dealerCode.hashCode());
        result = prime * result
                + ((doNumber == null) ? 0 : doNumber.hashCode());
        result = prime * result
                + ((doStatus == null) ? 0 : doStatus.hashCode());
        result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
        result = prime * result + ((floorNo == null) ? 0 : floorNo.hashCode());
        result = prime * result
                + ((grossLoanAmount == null) ? 0 : grossLoanAmount.hashCode());
        result = prime * result + ((guid == null) ? 0 : guid.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result + ((houseNo == null) ? 0 : houseNo.hashCode());
        result = prime * result
                + ((landmark == null) ? 0 : landmark.hashCode());
        result = prime * result
                + ((marginMoney == null) ? 0 : marginMoney.hashCode());
        result = prime * result
                + ((mfrSubvention == null) ? 0 : mfrSubvention.hashCode());
        result = prime * result
                + ((mobileNo == null) ? 0 : mobileNo.hashCode());
        result = prime * result + ((mopID == null) ? 0 : mopID.hashCode());
        result = prime * result
                + ((netDisbursement == null) ? 0 : netDisbursement.hashCode());
        result = prime * result
                + ((netLoanAmount == null) ? 0 : netLoanAmount.hashCode());
        result = prime * result
                + ((otherCharges == null) ? 0 : otherCharges.hashCode());
        result = prime * result + ((pincode == null) ? 0 : pincode.hashCode());
        result = prime * result + ((plotNo == null) ? 0 : plotNo.hashCode());
        result = prime * result
                + ((processingFee == null) ? 0 : processingFee.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        result = prime * result
                + ((productType == null) ? 0 : productType.hashCode());
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        result = prime * result
                + ((resiAddress1 == null) ? 0 : resiAddress1.hashCode());
        result = prime * result
                + ((resiAddress2 == null) ? 0 : resiAddress2.hashCode());
        result = prime * result
                + ((resiAddress3 == null) ? 0 : resiAddress3.hashCode());
        result = prime * result
                + ((schemeName == null) ? 0 : schemeName.hashCode());
        result = prime * result + ((sector == null) ? 0 : sector.hashCode());
        result = prime * result
                + ((societyName == null) ? 0 : societyName.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((street == null) ? 0 : street.hashCode());
        result = prime * result
                + ((subvention == null) ? 0 : subvention.hashCode());
        result = prime * result + ((tenure == null) ? 0 : tenure.hashCode());
        result = prime * result + ((wingNo == null) ? 0 : wingNo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof RelianceDigitalRequest))
            return false;
        RelianceDigitalRequest other = (RelianceDigitalRequest) obj;
        if (advanceEMI == null) {
            if (other.advanceEMI != null)
                return false;
        } else if (!advanceEMI.equals(other.advanceEMI))
            return false;
        if (area == null) {
            if (other.area != null)
                return false;
        } else if (!area.equals(other.area))
            return false;
        if (assetCategory == null) {
            if (other.assetCategory != null)
                return false;
        } else if (!assetCategory.equals(other.assetCategory))
            return false;
        if (assetQuantity == null) {
            if (other.assetQuantity != null)
                return false;
        } else if (!assetQuantity.equals(other.assetQuantity))
            return false;
        if (brand == null) {
            if (other.brand != null)
                return false;
        } else if (!brand.equals(other.brand))
            return false;
        if (buildingName == null) {
            if (other.buildingName != null)
                return false;
        } else if (!buildingName.equals(other.buildingName))
            return false;
        if (city == null) {
            if (other.city != null)
                return false;
        } else if (!city.equals(other.city))
            return false;
        if (costOfProduct == null) {
            if (other.costOfProduct != null)
                return false;
        } else if (!costOfProduct.equals(other.costOfProduct))
            return false;
        if (createdDate == null) {
            if (other.createdDate != null)
                return false;
        } else if (!createdDate.equals(other.createdDate))
            return false;
        if (customerDownPayment == null) {
            if (other.customerDownPayment != null)
                return false;
        } else if (!customerDownPayment.equals(other.customerDownPayment))
            return false;
        if (customerName == null) {
            if (other.customerName != null)
                return false;
        } else if (!customerName.equals(other.customerName))
            return false;
        if (dbdPercentage == null) {
            if (other.dbdPercentage != null)
                return false;
        } else if (!dbdPercentage.equals(other.dbdPercentage))
            return false;
        if (dealID == null) {
            if (other.dealID != null)
                return false;
        } else if (!dealID.equals(other.dealID))
            return false;
        if (dealerCode == null) {
            if (other.dealerCode != null)
                return false;
        } else if (!dealerCode.equals(other.dealerCode))
            return false;
        if (doNumber == null) {
            if (other.doNumber != null)
                return false;
        } else if (!doNumber.equals(other.doNumber))
            return false;
        if (doStatus == null) {
            if (other.doStatus != null)
                return false;
        } else if (!doStatus.equals(other.doStatus))
            return false;
        if (emailId == null) {
            if (other.emailId != null)
                return false;
        } else if (!emailId.equals(other.emailId))
            return false;
        if (floorNo == null) {
            if (other.floorNo != null)
                return false;
        } else if (!floorNo.equals(other.floorNo))
            return false;
        if (grossLoanAmount == null) {
            if (other.grossLoanAmount != null)
                return false;
        } else if (!grossLoanAmount.equals(other.grossLoanAmount))
            return false;
        if (guid == null) {
            if (other.guid != null)
                return false;
        } else if (!guid.equals(other.guid))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (houseNo == null) {
            if (other.houseNo != null)
                return false;
        } else if (!houseNo.equals(other.houseNo))
            return false;
        if (landmark == null) {
            if (other.landmark != null)
                return false;
        } else if (!landmark.equals(other.landmark))
            return false;
        if (marginMoney == null) {
            if (other.marginMoney != null)
                return false;
        } else if (!marginMoney.equals(other.marginMoney))
            return false;
        if (mfrSubvention == null) {
            if (other.mfrSubvention != null)
                return false;
        } else if (!mfrSubvention.equals(other.mfrSubvention))
            return false;
        if (mobileNo == null) {
            if (other.mobileNo != null)
                return false;
        } else if (!mobileNo.equals(other.mobileNo))
            return false;
        if (mopID == null) {
            if (other.mopID != null)
                return false;
        } else if (!mopID.equals(other.mopID))
            return false;
        if (netDisbursement == null) {
            if (other.netDisbursement != null)
                return false;
        } else if (!netDisbursement.equals(other.netDisbursement))
            return false;
        if (netLoanAmount == null) {
            if (other.netLoanAmount != null)
                return false;
        } else if (!netLoanAmount.equals(other.netLoanAmount))
            return false;
        if (otherCharges == null) {
            if (other.otherCharges != null)
                return false;
        } else if (!otherCharges.equals(other.otherCharges))
            return false;
        if (pincode == null) {
            if (other.pincode != null)
                return false;
        } else if (!pincode.equals(other.pincode))
            return false;
        if (plotNo == null) {
            if (other.plotNo != null)
                return false;
        } else if (!plotNo.equals(other.plotNo))
            return false;
        if (processingFee == null) {
            if (other.processingFee != null)
                return false;
        } else if (!processingFee.equals(other.processingFee))
            return false;
        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;
        if (productType == null) {
            if (other.productType != null)
                return false;
        } else if (!productType.equals(other.productType))
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        if (resiAddress1 == null) {
            if (other.resiAddress1 != null)
                return false;
        } else if (!resiAddress1.equals(other.resiAddress1))
            return false;
        if (resiAddress2 == null) {
            if (other.resiAddress2 != null)
                return false;
        } else if (!resiAddress2.equals(other.resiAddress2))
            return false;
        if (resiAddress3 == null) {
            if (other.resiAddress3 != null)
                return false;
        } else if (!resiAddress3.equals(other.resiAddress3))
            return false;
        if (schemeName == null) {
            if (other.schemeName != null)
                return false;
        } else if (!schemeName.equals(other.schemeName))
            return false;
        if (sector == null) {
            if (other.sector != null)
                return false;
        } else if (!sector.equals(other.sector))
            return false;
        if (societyName == null) {
            if (other.societyName != null)
                return false;
        } else if (!societyName.equals(other.societyName))
            return false;
        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;
        if (street == null) {
            if (other.street != null)
                return false;
        } else if (!street.equals(other.street))
            return false;
        if (subvention == null) {
            if (other.subvention != null)
                return false;
        } else if (!subvention.equals(other.subvention))
            return false;
        if (tenure == null) {
            if (other.tenure != null)
                return false;
        } else if (!tenure.equals(other.tenure))
            return false;
        if (wingNo == null) {
            if (other.wingNo != null)
                return false;
        } else if (!wingNo.equals(other.wingNo))
            return false;
        return true;
    }

}
