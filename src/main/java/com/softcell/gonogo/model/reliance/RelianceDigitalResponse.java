package com.softcell.gonogo.model.reliance;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RelianceDigitalResponse {

    @JsonProperty("sErrorCode")
    private String errorCode;

    @JsonProperty("sErrorMsg")
    private String errorMsg;

    @JsonProperty("sDONumber")
    private String doNumber;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getDoNumber() {
        return doNumber;
    }

    public void setDoNumber(String doNumber) {
        this.doNumber = doNumber;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RelianceDigitalResponse [errorCode=");
        builder.append(errorCode);
        builder.append(", errorMsg=");
        builder.append(errorMsg);
        builder.append(", doNumber=");
        builder.append(doNumber);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((doNumber == null) ? 0 : doNumber.hashCode());
        result = prime * result
                + ((errorCode == null) ? 0 : errorCode.hashCode());
        result = prime * result
                + ((errorMsg == null) ? 0 : errorMsg.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof RelianceDigitalResponse))
            return false;
        RelianceDigitalResponse other = (RelianceDigitalResponse) obj;
        if (doNumber == null) {
            if (other.doNumber != null)
                return false;
        } else if (!doNumber.equals(other.doNumber))
            return false;
        if (errorCode == null) {
            if (other.errorCode != null)
                return false;
        } else if (!errorCode.equals(other.errorCode))
            return false;
        if (errorMsg == null) {
            if (other.errorMsg != null)
                return false;
        } else if (!errorMsg.equals(other.errorMsg))
            return false;
        return true;
    }


}
