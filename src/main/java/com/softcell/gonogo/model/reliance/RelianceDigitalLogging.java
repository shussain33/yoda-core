package com.softcell.gonogo.model.reliance;

import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.dmz.DMZResponse;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Document(collection = "RelianceDigitalLogging")
public class RelianceDigitalLogging extends AuditEntity {

    private String institutionId;
    private Date insertDate;
    private RelianceDigitalRequest request;
    private DMZResponse response;
    private RelianceDigitalException exception;
    private String rawResponse;

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }


    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }


    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }


    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }


    /**
     * @return the request
     */
    public RelianceDigitalRequest getRequest() {
        return request;
    }


    /**
     * @param request the request to set
     */
    public void setRequest(RelianceDigitalRequest request) {
        this.request = request;
    }


    /**
     * @return the response
     */
    public DMZResponse getResponse() {
        return response;
    }


    /**
     * @param response the response to set
     */
    public void setResponse(DMZResponse response) {
        this.response = response;
    }


    /**
     * @return the exception
     */
    public RelianceDigitalException getException() {
        return exception;
    }


    /**
     * @param exception the exception to set
     */
    public void setException(RelianceDigitalException exception) {
        this.exception = exception;
    }

    public String getRawResponse() {
        return rawResponse;
    }

    public void setRawResponse(String rawResponse) {
        this.rawResponse = rawResponse;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RelianceDigitalLogging [institutionId=");
        builder.append(institutionId);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append(", request=");
        builder.append(request);
        builder.append(", response=");
        builder.append(response);
        builder.append("]");
        return builder.toString();
    }

    public class RelianceDigitalException {
        private String devMessage;
        private String cause;

        /**
         * @return the devMessage
         */
        public String getDevMessage() {
            return devMessage;
        }

        /**
         * @param devMessage the devMessage to set
         */
        public void setDevMessage(String devMessage) {
            this.devMessage = devMessage;
        }

        /**
         * @return the cause
         */
        public String getCause() {
            return cause;
        }

        /**
         * @param cause the cause to set
         */
        public void setCause(String cause) {
            this.cause = cause;
        }


    }


}
