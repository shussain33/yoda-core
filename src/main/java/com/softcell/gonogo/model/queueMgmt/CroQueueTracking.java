package com.softcell.gonogo.model.queueMgmt;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by archana on 9/11/17.
 */
@Document(collection = "CroQueueTracking")
public class CroQueueTracking {

    private String userId;
    private String userName;
    private String institutionId;
    private String role;
    private String zone; //Zone(additional hierarchies will be added in the subsequent iterations)
    private List<String> products;

    /*List of outstanding and ordered applications.
     Iteration 2 will have the feature of priority applications landing at the head of the queue/list
    */
    private List<String> assignedCaseIds = new ArrayList<>();

    /* Time stamp of the last heartbeat received from the portal. */
    private Date lastSignalReceived;

    /* Last case assigned Time Stamp in nano seconds. */
    // TODO : nano seconds
    private Date lastCaseAssignment;

    private boolean availableFlag;

    /* Additional attributes to be added in this collection during the implementation phase
        including the housekeeping ones.   */

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }

    public List<String> getAssignedCaseIds() {
        return assignedCaseIds;
    }

    public void setAssignedCaseIds(List<String> assignedCaseIds) {
        this.assignedCaseIds = assignedCaseIds;
    }

    public Date getLastSignalReceived() {
        return lastSignalReceived;
    }

    public void setLastSignalReceived(Date lastSignalReceived) {
        this.lastSignalReceived = lastSignalReceived;
    }

    public Date getLastCaseAssignment() {
        return lastCaseAssignment;
    }

    public void setLastCaseAssignment(Date lastCaseAssignment) {
        this.lastCaseAssignment = lastCaseAssignment;
    }

    public boolean isAvailableFlag() {
        return availableFlag;
    }

    public void setAvailableFlag(boolean availableFlag) {
        this.availableFlag = availableFlag;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CroQueueTracking{");
        sb.append("userId='").append(userId).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", role='").append(role).append('\'');
        sb.append(", zone='").append(zone).append('\'');
        sb.append(", products=").append(products);
        sb.append(", availibility=").append(availableFlag);
        sb.append(", assignedCaseIds=").append(assignedCaseIds);
        sb.append(", lastSignalReceived=").append(lastSignalReceived);
        sb.append(", lastCaseAssignment=").append(lastCaseAssignment);
        sb.append('}');
        return sb.toString();
    }
}
