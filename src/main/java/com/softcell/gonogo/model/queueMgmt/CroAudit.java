package com.softcell.gonogo.model.queueMgmt;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * POJO storing information of a CRO worked on an application.
 * Created by archana on 31/8/17.
 */
@Document(collection = "CroAudit")
public class CroAudit implements Serializable {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sUserId")
    private String userId;

    @JsonProperty("sRole")
    private String role;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sDecision")
    private String decision;

    @JsonProperty("dDecisionDate")
    private Date decisionDate = new Date();


    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getDecisionDate() {
        return decisionDate;
    }

    public void setDecisionDate(Date decisionDate) {
        this.decisionDate = decisionDate;
    }


    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CroAudit{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", role='").append(role).append('\'');
        sb.append(", refID='").append(refID).append('\'');
        sb.append(", decision='").append(decision).append('\'');
        sb.append(", decisionDate=").append(decisionDate);
        sb.append('}');
        return sb.toString();
    }
}
