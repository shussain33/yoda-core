package com.softcell.gonogo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
/**
 * Created by ssg237 on 28/5/21.
 */
@Document
@Data
public class MasterSchedulerActivityLogs {

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sCustMsg")
    private String reason;

    @JsonProperty("oMasterSchedulerConfiguration")
    private MasterSchedulerConfiguration masterSchedulerConfiguration;

    @JsonProperty("dtDate")
    private Date date;
}
