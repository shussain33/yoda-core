package com.softcell.gonogo.model.ops;

import java.util.*;

import com.softcell.gonogo.model.Remark;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerCreditDetail{

	@JsonProperty("sDesc")
	private String desc;

	@JsonProperty("sCreditAssoStatus")
	private String creditAssoStatus;

	@JsonProperty("dtCreditAssoReceiptDt")
	private Date creditAssoReceiptDt;

	@JsonProperty("oCreditRemarks")
	private Remark creditRemarks;

	@JsonProperty("sOpsStatus")
	private String opsStatus;

	@JsonProperty("dtOpsReceiptDt")
	private Date opsReceiptDt;

	@JsonProperty("oOpsRemarks")
	private Remark opsRemarks;

	@JsonProperty("sOtherDocDesc")
	private String otherDocDesc;

	@JsonProperty("sSection")
	private String section;

}
