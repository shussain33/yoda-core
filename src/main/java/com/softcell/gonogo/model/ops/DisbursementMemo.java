package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.cam.CamSummary;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by suhasini on 23/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "dmDetails")
public class DisbursementMemo {

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstitutionId")
    public String institutionId;

    @JsonProperty("oRepayment")
    public Repayment repayment;

    @JsonProperty("oLoanCharges")
    public LoanCharges loanCharges;

    @JsonProperty("oCamSummary")
    public CamSummary camSummary;

    @JsonProperty("aDMInput")
    public List<DMInput> dmInputList;

    @JsonProperty("sBranchName")
    private String branchName;

    @JsonProperty("sBranchCode")
    private String branchcode;

    @JsonProperty("sSpoke")
    private String spokeName;

    @JsonProperty("sSpokeCode")
    private String spokeCode;

    @JsonProperty("sLoanStation")
    private String loanStationName;

    @JsonProperty("sLoanStationCode")
    private String loanStationCode;


    @JsonProperty("sDMName")
    private String dmName;

    @JsonProperty("oPropertyAddress")
    private CustomerAddress propertyAddress;

    @JsonProperty("sValuer1")
    private String valuer1;

    @JsonProperty("sValuer2")
    private String valuer2;

    @JsonProperty("sLinkLoan")
    private String linkLoan;

    @JsonProperty("sCustomerId")
    private String customerId;

    @JsonProperty("sLocationCode")
    private String locationCode;

    @JsonProperty("sFinalConsiderValueType")
    private String finalConsiderValueType;

    @JsonProperty("aCoApplicant")
    private List<CoApplicant> coApplicant;

    @JsonProperty("aRemarks")
    private List<Remark> remarkList;


    /*****MIS refer field***/
    @JsonProperty("sMisHubName")
    private String misHubName;

    @JsonProperty("sMisHubCode")
    private String misHubCode;
    /**************************/

}


