package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by suhasini on 28/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "sanctionConditions")
public class SanctionConditions extends ApplicationVerification {

    @JsonProperty("aSCDetails")
    private List<SanctionConditionDetails> scDetailsList;

}
