package com.softcell.gonogo.model.ops;

import java.util.*;

import com.softcell.gonogo.model.Remark;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DocDetail{

	@JsonProperty("sDesc")
	private String desc;

	@JsonProperty("dtDocDate")
	private Date docDate;

	/* Type of Docs (Original / Photocopy/Certified Copy/Online Print) */
	@JsonProperty("sDocType")
	private String docType;

	@JsonProperty("iPageCnt")
	private int pageCnt;

	/* Rcvd/OTC/PDD */
	@JsonProperty("sCrAssoDocStatus")
	private String crAssoDocStatus;

	@JsonProperty("dtCrAssoReceiptDt")
	private Date crAssoReceiptDt;

	/* Rcvd/OTC/PDD */
	@JsonProperty("sOpsDocStatus")
	private String opsDocStatus;

	@JsonProperty("dtOpsReceiptDt")
	private Date opsReceiptDt;

	@JsonProperty("aRemarks")
	private List<Remark> remarks;

}
