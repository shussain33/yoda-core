package com.softcell.gonogo.model.ops;

import java.util.*;

import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "customerCreditDocs")
public class CustomerCreditDocs extends ApplicationVerification{

	@JsonProperty("aCustomerCreditDtls")
	private List<CustomerCreditDetail> customerCreditDetails;

}
