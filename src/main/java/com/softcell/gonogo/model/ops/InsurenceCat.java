package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

  /**
  * Created by ssg408 on 24/7/18.
  */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsurenceCat {

    @JsonProperty("sInsuranceCategory")
    public String insuranceCategory;

    @JsonProperty("sCompanyName")
    public String companyName;

    @JsonProperty("dAmount")
    public Double amount;
}
