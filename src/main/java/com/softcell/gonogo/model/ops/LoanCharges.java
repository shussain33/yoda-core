package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "loanCharges")
public class LoanCharges {

	@Id
	@JsonProperty("sRefId")
	private String refId;

	@JsonProperty("sInstitutionId")
	public String institutionId;

	@JsonProperty("oLoanDetls")
	private LoanAmtDetails loanDetails;

	@JsonProperty("oTenureDetls")
	private TenureDetails tenureDetails;

	@JsonProperty("oInterestDetls")
	private InterestDetails interestDetails;

	/* FIXED/FLOATING */
	@JsonProperty("sTypeOfROI")
	private String typeOfROI;

	@JsonProperty("dEMIStartDt")
	private Date emiStartDate;

    @JsonProperty("dEMIEndDt")
    private Date emiEndDate;

	@JsonProperty("dIMDAmt")
	private double imdCollectedAmt;

	@JsonProperty("dIMD2Amt")
	private double imd2Amount;

	@JsonProperty("dIMD2GstAmt")
	private double imd2gstAmt;

	/* Percentage */
	@JsonProperty("dProFee")
	private double proFees;

	@JsonProperty("dProFeeAmt")
	private double proFeesAmt;

	/* Percentage(Master Field) */
	@JsonProperty("dGST")
	private double gst;

	@JsonProperty("dGSTAmt")
	private double gstAmt;

	/* TOTAL PROCESSING FEES */
	@JsonProperty("dTotalFees")
	private double totalFees;

	/* PROCESSING FEES - TO BE DEDUCT FROM DISBURSEMENT */
	@JsonProperty("dProDeductAmt")
	private double proDeductAmt;

	/* CERSAI FEES */
	@JsonProperty("dCersaiFee")
	private double cersaiFees;

	/* PRE EMI INTEREST AMT - TO BE DEDUCT FROM DISBURSEMENT */
	@JsonProperty("dEMIDeductAmt")
	private double preEMIDeductAmt;

	/* NAME OF INSURANCE COMPANY */
	@JsonProperty("sInsuranceCo")
	private String insuranceCoName;

	/* INSURANCE AMT */
	@JsonProperty("dInsuranceAmt")
	private double insuranceAmt;

	/* NET DISBURSAL AMT */
	@JsonProperty("dNetDisbAmt")
	private double netDisbAmt;

	@JsonProperty("dFinalEMIAmt")
	private double finalEMIAmt;

	@JsonProperty("dAdvEMI")
	private double advanceEMI;

	@JsonProperty("dtDisbursedDate")
	private Date disbursedDate;

	@JsonProperty("oRemark")
	private Remark remark;

	//adding extra fields

	@JsonProperty("aInsurenceCat")
	private List<InsurenceCat> insurenceCatList;

	@JsonProperty("sCompanyName")
	public String companyName;

	@JsonProperty("sAmount")
	public String amount;

	@JsonProperty("sCreditVidyaCharge")
	public String creditVidyaCharge;


    @JsonProperty("aLoanChargesDetails")
    private List<LoanDetails> loanChargesDetails = new ArrayList<LoanDetails>();


	@JsonProperty("dBOPRcvDt")
	public Date bopsReceivedDt;

	@JsonProperty("FileRecieveDt")
	public Date fileReceivedDt;

	@JsonProperty("dHOPRcvDt")
	private Date hOpsFileRecieveDt;

	//to allow calaculate PF amount on Cashout File in case of topUp
	@JsonProperty("bonCashOut")
	private boolean isCashOut;

	@JsonProperty("iDueDate")
	private int dueDate;

	@JsonProperty("dCersaiFeeWithGST")
	private double cersaiFeeWithGST;

	@JsonProperty("sInsuranceMsg")
	private String insuranceMsg;

	@JsonProperty("dAdvEMIWithGST")
	private double advEMIWithGST;

	//for additional charges key in  wavier
	@JsonProperty("bAdditionalCharges")
	private boolean additionalCharges;

	//DigiPL Fields Started
	@JsonProperty("sLoanPercentage")
	private String loanPercentage;

	@JsonProperty("aLoanCharges")
	private List<LoanCharges> loanChargesList;

	@JsonProperty("iBrokenPeriodDays")
	private int brokenPeriodDays;

	@JsonProperty("binsuranceRequired")
	private boolean insuranceRequired;

	@JsonProperty("dtInterestEmiStartDt")
	private Date interestEmiStartDt;

	@JsonProperty("sQuarter")
	private String quarter;

    public interface InsertGrp {

	}
}
