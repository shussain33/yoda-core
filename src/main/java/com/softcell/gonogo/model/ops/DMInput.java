package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by suhasini on 23/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DMInput {

    @JsonProperty("sDMBankName")
    public String dmBankName;

    @JsonProperty("sMifinBankID")
    private String mifinBankCode;

    @JsonProperty("sDMAmt")
    public String dmAmt;

    @JsonProperty("dtDMDate")
    public Date dmDate;

    @JsonProperty("sDMPaymentMode")
    public String dmPaymentMode;

    @JsonProperty("sDMFavourting")
    public String dmFavourting;

    @JsonProperty("sDMApprovedBy")
    public String dmApprovedBy;

    @JsonProperty("sDMRemarks")
    public String dmRemarks;

    @JsonProperty("sDMAccNo")
    public String dmAccNo;

    @JsonProperty("sInstrumentNo")
    public String instrumentNo;

    //DigiPl Fields Started
    @JsonProperty("sLoanPercentage")
    private String loanPercentage;
}
