package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanAmtDetails{

	/* Auto Populate */
	@JsonProperty("dFinalLoanApprovedAmt")
	private double finalLoanApprovedAmt;

	/* Editiable */
	@JsonProperty("dFinalDisb")
	private double finalDisbAmt;

	@JsonProperty("dBTLoanAmt")
	private double btLoanAmt;

	@JsonProperty("dTUParallelAmt")
	private double topUpParallelAmt;

	@JsonProperty("dTUGrossAmt")
	private double topUpGrossAmt;

	@JsonProperty("bChngApprvl")
	private boolean changeApproval;

	/* Mandatory field' if there is a change in saction and disb field */
	@JsonProperty("oRemark")
	private Remark remarks;

	@JsonProperty("dExistingLoanAmt")
	private double existingLoanAmt;

	@JsonProperty("dApprovedPf")
	private double approvedPf;

	//adding field for pl

	@JsonProperty("dApprovedLoan")
	private double approvedLoanAmt;


	/*TopUp Releated field*/
	@JsonProperty("dPrincipalOsAMt")
	private double principalOsAmt;

	@JsonProperty("dInterestOsAmount")
	private double interestOsAmt;

	@JsonProperty("dPDCAmount")
	private double pdcAmount;

	@JsonProperty("dPDCGSTAmount")
	private double pdcGstAmount;

	@JsonProperty("dDocumentCharges")
	private double documentCharges;

	@JsonProperty("dDocumentChargesGST")
	private double documentChargesGST;

}
