package com.softcell.gonogo.model.ops;

import java.util.*;

import com.softcell.gonogo.model.Remark;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InterestDetails{

	/* Rate Of Interest */
	@JsonProperty("dROI")
	private double interestRate;

	/* Disbursed Rate Of Interest */
	@JsonProperty("dDisdROI")
	private double disbInterestRate;

	@JsonProperty("bChngApprvl")
	private boolean changeApproval;

	/* Mandatory field' if there is a change in saction and disb field */
	@JsonProperty("oRemark")
	private Remark remarks;

	@JsonProperty("dPFPercentage")
	private double pfPercentage;

	@JsonProperty("dFOIR")
	private double foir;

	@JsonProperty("dFinalFOIR")
	private double finalFoir;

}
