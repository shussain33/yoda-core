package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by suhasini on 28/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SanctionConditionDetails {

    @JsonProperty("sCondition")
    private String condition;

    @JsonProperty("dtRaisedDate")
    private Date raisedDate;

    @JsonProperty("sRaisedBy")
    private String raisedBy;

    @JsonProperty("sOpsRemark")
    private String opsRemark;

    @JsonProperty("sResolvedBy")
    private String resolvedBy;

    @JsonProperty("dtResolvedDate")
    private Date resolvedDate;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sDescription")
    private String description;

}
