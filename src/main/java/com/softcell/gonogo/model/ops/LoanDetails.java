package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 13/10/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoanDetails {
    @JsonProperty("sChargeName")
    private String chargeName;

    @JsonProperty("sDefaultType")
    private String defaultType;

    @JsonProperty("dDefLoanAmt")
    private Double defaultLoanAmount;

    @JsonProperty("dChargeAmt")
    private Double chargeAmount;

    @JsonProperty("dGstAmount")
    private Double gstAmount;

    @JsonProperty("dGrossAmt")
    private Double grossAmount;

    @JsonProperty("bServiceChrge")
    private boolean serviceCharge;//GST

    @JsonProperty("bChargeInclusive")
    private boolean chargeInclusive;

    @JsonProperty("dNetChargeAmt")
    private Double netChargeAmount;

    @JsonProperty("dGSTPercent")
    private Double gstPercent;

    @JsonProperty("bGstApplicable")
    private boolean gstApplicable;

    @JsonProperty("bConsiderForDeduction")
    private boolean considerForDeduction;

    @JsonProperty("sMifinChargeId")
    private String mifinChargeId;

}
