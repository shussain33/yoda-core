package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.LegalVerification;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 27/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerCreditRequest {
    @JsonProperty("oHeader")
    @NotNull
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotNull(groups = {CustomerCreditRequest.InsertGrp.class,CustomerCreditRequest.FetchGrp.class})
    public String refId;

    @JsonProperty("oCustomerCreditDocs")
    @NotNull(groups = {CustomerCreditRequest.InsertGrp.class})
    private CustomerCreditDocs customerCreditDocs;

    public interface InsertGrp {
    }
    public interface FetchGrp{
    }
}
