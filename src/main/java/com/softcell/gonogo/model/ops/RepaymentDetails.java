package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.address.Address;
import com.softcell.gonogo.model.core.BankingDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;



/**

 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RepaymentDetails extends BankingDetails{


	@JsonProperty("oBranchAddr")
	private Address branchaddress;

	@JsonProperty("dtEmiStartDt")
	private Date emistartdate;

	@JsonProperty("sChqType")
	private String chqtype;

	@JsonProperty("iChqCount")
	private int chqcount;

	@JsonProperty("sUserId")
	private String verifiedby;

	@JsonProperty("sUserRole")
	private String role;

	@JsonProperty("aRemarks")
	private List<Remark> remarkList;

	@JsonProperty("sChqNumber")
	private String chqNumber;

}
