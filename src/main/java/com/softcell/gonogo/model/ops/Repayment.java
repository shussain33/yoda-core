package com.softcell.gonogo.model.ops;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by yogesh on 22/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "repaymentDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Repayment {
    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstitutionId")
    public String institutionId;

    @JsonProperty("aRepaymentDetails")
    public List<RepaymentDetails> repaymentDetailsList;

}
