package com.softcell.gonogo.model.ops;

import java.util.*;

import com.softcell.gonogo.model.Remark;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TenureDetails{

	/* Tenure in Months */
	@JsonProperty("iTenureMnth")
	private int tenureMonths;

	/* For increase in tenure approval to be sought and attached */
	@JsonProperty("iDisbTenureMnth")
	private int disbTenureMonths;

	@JsonProperty("dBTEMIAmt")
	private double btEMIAmt;

	@JsonProperty("dTUEMIAmt")
	private double topUpEMIAmt;

	@JsonProperty("bChngApprvl")
	private boolean changeApproval;

	/* Mandatory field' if there is a change in saction and disb field */
	@JsonProperty("oRemark")
	private Remark remarks;

}
