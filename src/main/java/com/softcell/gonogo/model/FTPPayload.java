package com.softcell.gonogo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 26/11/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FTPPayload {
    private String institutionId;
    private String file;
    private String fileName;
    private String path;
}
