package com.softcell.gonogo.model.finfort;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.softcell.gonogo.model.ThirdPartyCallLog;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by amit on 15/11/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "finfortCallLog")
public class FinfortCallLog extends ThirdPartyCallLog {
    private Date callDate = new Date();

    private Object request;

    private Object response;

    private String strResponse;
}
