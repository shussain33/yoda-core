package com.softcell.gonogo.model.finfort;

import com.softcell.ssl2.finfort.model.FileUploadRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 28/11/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileUploadInfo {
    private FileUploadRequest uploadRequest;
    private String applicantId;

}
