package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdsgOccupationInfo {

    @JsonProperty("occupationType")
    private String occupationType;

    @JsonProperty("occupationTypeCode")
    private String occupationTypeCode;

    @JsonProperty("occupationTypeOther")
    private String occupationTypeOther;

    @JsonProperty("industry")
    private String industry;

    @JsonProperty("AGEOFRETIREMENT")
    private String ageOfRetirement;

    @JsonProperty("CompanyType")
    private String companyType;

    @JsonProperty("CompanyName")
    private String companyName;

    @JsonProperty("CompanyNature")
    private String companyNature;

    @JsonProperty("Designation")
    private String designation;

    @JsonProperty("IncomeConsidered")
    private String incomeConsidered;

    @JsonProperty("grossAnnualIncome")
    private String grossAnnualIncome;

    @JsonProperty("monthsInJob")
    private String monthsInJob;

    @JsonProperty("netMonthlyIncome")
    private String netMonthlyIncome;

    @JsonProperty("Profession")
    private String profession;

    @JsonProperty("yearsInJob")
    private String yearsInJob;

    @JsonProperty("creditcardflag")
    private String creditCardflag;


}
