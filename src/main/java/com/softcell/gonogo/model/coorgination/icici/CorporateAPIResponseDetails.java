package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 3/12/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CorporateAPIResponseDetails {

    @JsonProperty("QDE_FN")
    private String QDE_FN;

    @JsonProperty("idisburse_Error_Message")
    private String idisburse_Error_Message;

    @JsonProperty("applicationNumber")
    private String applicationNumber;

    @JsonProperty("REQSTATUS")
    private String REQSTATUS;

    @JsonProperty("QDE_LN")
    private String QDE_LN;

    @JsonProperty("QDE_APSID")
    private String QDE_APSID;

    @JsonProperty("ERRM")
    private String ERRM;

    @JsonProperty("QDE_MN")
    private String QDE_MN;

    @JsonProperty("idisburseId")
    private String idisburseId;

    @JsonProperty("QDE_CCT_HOME")
    private String QDE_CCT_HOME;

}
