package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdsgNidvApplicantDetails {

    @JsonProperty("NIDV_CORPORATEFLAG")
    private String nidvCorporateFlag;

    @JsonProperty("APPLICANTTYPE")
    private String applicantType;

    @JsonProperty("ReasonForCoApplicant")
    private String reasonForCoApplicant;

    @JsonProperty("ExistingCustomer")
    private String existingCustomer;

    @JsonProperty("NIDV_CompanyName")
    private  String nidvCompanyName;

    @JsonProperty("Region")
    private String region;

    @JsonProperty("Constitution")
    private String constitution;

    @JsonProperty("Groupp")
    private String groupp;

    @JsonProperty("NIDV_Category")
    private String nidvCategory;

    @JsonProperty("NIDV_Industry")
    private String nidvIndustry;

    @JsonProperty("IncorporationDate")
    private String incorporationDate;

    @JsonProperty("ContactPerson")
    private String contactPerson;

    @JsonProperty("NIDV_Designation")
    private String nidvDesignation;

    @JsonProperty("Entity")
    private String entity;

    @JsonProperty("NIDV_PANNumber")
    private String nidvPanNumber;

    @JsonProperty("FormNumber")
    private String formNumber;

    @JsonProperty("GSTRegistered")
    private String gstRegistered;

    @JsonProperty("GSTRegistratnNo")
    private String gstRegistratnNo;

    @JsonProperty("TurnOver")
    private String turnOver;

    @JsonProperty("RiskMonitoring")
    private String riskMonitoring;

    @JsonProperty("NRI")
    private String nri;

    @JsonProperty("EmploymentType")
    private String employmentType;

    @JsonProperty("ProvidentFundNumber")
    private String providentFundNumber;

    @JsonProperty("NoOfEmployeesInCompany")
    private String noOfEmployeesInCompany;

    @JsonProperty("SalaryType")
    private String salaryType;

    @JsonProperty("PaymentMode")
    private String paymentMode;

    //new field addition
    @JsonProperty("GrossBlockValueInRupees")
    private String grossBlockValueInRuppes;

    @JsonProperty("Land_OtherItemValuesInRupees")
    private String landOtherItemValuesInRuppes;

    @JsonProperty("PreviousCompanyName")
    private String previousCompanyName;

    @JsonProperty("OfficeOwnership")
    private String officeOwnerShip;

    @JsonProperty("ShopRegistrationNo")
    private String shopRegistrationNo;

    @JsonProperty("WeakerSectnDescript")
    private String weakerSectnDescript;

    @JsonProperty("addressType")
    private String addressType;

    @JsonProperty("addressLine1")
    private String addressLine1;

    @JsonProperty("addressLine2")
    private String addressLine2;

    @JsonProperty("country")
    private String country;

    @JsonProperty("countryCode")
    private String countryCode;

    @JsonProperty("state")
    private String state;

    @JsonProperty("stateCode")
    private String stateCode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("cityCode")
    private String cityCode;

    @JsonProperty("districtId")
    private String districtId;

    @JsonProperty("zipcode")
    private String zipcode;

    @JsonProperty("phoneNo")
    private String phoneNo;

    @JsonProperty("stdCode")
    private String stdCode;

    @JsonProperty("mailingAddress")
    private String mailingAddress;

    @JsonProperty("corporate_flag_co_applicant")
    private String corporateFlagcoApplicant;



}
