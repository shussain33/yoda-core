package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdsgApplicantDetails {

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("middleName")
    private String middleName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("fatherName")
    private String fatherName;

    @JsonProperty("dateOfBirth")
    private String dateOfBirth;

    @JsonProperty("maritalStatus")
    private String maritalStatus;

    @JsonProperty("maritalStatusCode")
    private String maritalStatusCode;

    @JsonProperty("nationality")
    private String nationality;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("genderCode")
    private String genderCode;

    @JsonProperty("salutation")
    private String salutation;

    @JsonProperty("salutationCode")
    private String salutationCode;

    @JsonProperty("mobile_number")
    private String mobile_number;

    @JsonProperty("email")
    private String email;

    @JsonProperty("noOfDependent")
    private String noOfDependent;

    @JsonProperty("qualification")
    private String qualification;

    @JsonProperty("category")
    private  String category;

    @JsonProperty("categoryCode")
    private String categoryCode;

    @JsonProperty("residentType")
    private String residentType;

    @JsonProperty("residenceStatus")
    private String residenceStatus;

    @JsonProperty("panNo")
    private String panNo;

    @JsonProperty("specialCatagorycustomer")
    private String specialCatagorycustomer;

    @JsonProperty("spouseWorking")
    private String spouseWorking;

    @JsonProperty("monthAtcurrentresident")
    private String monthAtcurrentresident;

    @JsonProperty("yearAtCurrentResident")
    private String yearAtCurrentResident;

    @JsonProperty("REASONOFCOAPPLICANT")
    private String reasonoOfCoApplicant;

    @JsonProperty("RELATIONSHIPWITHAPPLICANT")
    private String relationshipWithApplicant;

    @JsonProperty("coApplicantType")
    private String coApplicantType;

    @JsonProperty("religion")
    private String religion;

    @JsonProperty("Address_details")
    private List<AddressDetails> addressDetailsList;

    @JsonProperty("IDSG_OCCUPATIONINFO")
    private IdsgOccupationInfo idsgOccupationInfo;

    @JsonProperty("IDSG_NIDVAPPLICANTDETAILS")
    private IdsgNidvApplicantDetails idsgNidvApplicantDetails;


}
