package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdsgApplicationSource {

    @JsonProperty("sourceApplicationNumber")
    private String sourceApplicationNumber;

    @JsonProperty("ChannelCode")
    private String channelCode;

    @JsonProperty("DSA")
    private String dsa;

    @JsonProperty("Product")
    private String product;

    @JsonProperty("productCode")
    private String productCode;

    @JsonProperty("promoCode")
    private String promoCode;

    @JsonProperty("promotion")
    private String promotion;

    @JsonProperty("processShopName")
    private String processShopName;

    @JsonProperty("relationshipManager")
    private String relationshipManager;

    @JsonProperty("Scheme")
    private String scheme;

    @JsonProperty("solId")
    private String solId;

    @JsonProperty("spreadid")
    private String spreadid;

    @JsonProperty("source")
    private String source;

    @JsonProperty("SOURCE_TYPE")
    private String sourceType;

    @JsonProperty("SurrogateCode")
    private String surrogateCode;

    @JsonProperty("lopReferenceCode")
    private String lopReferenceCode;

    @JsonProperty("dstCode")
    private String dstCode;

    //added new fields
    @JsonProperty("SOURCINGSYSTEM")
    private String sourcingSystem;

    @JsonProperty("HLSOURCENUMBER")
    private String hlSourceNumber;

    @JsonProperty("LOAN_CROEMPID")
    private String loanCroEmpId;

    @JsonProperty("LOAN_CENTRALCPC")
    private String loanCentralCpc;

    @JsonProperty("LOAN_BSM_EMPID")
    private String loanBsmEmpId;

    @JsonProperty("LOAN_PROCESSSHOPNAMEDESC")
    private String loanProcessHopNameDesc;

    @JsonProperty("LOAN_BROKERID")
    private String loanBrokerId;

    @JsonProperty("LOAN_Location")
    private String loanLocation;

    @JsonProperty("LOAN_MAILTO")
    private String loanMailTo;

    @JsonProperty("LOAN_CREDITMANAGERID")
    private String loanCreditManagerId;

    @JsonProperty("LOAN_VENDORNOTIFICATION")
    private String loanVendorNotification;
}
