package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdsgInvestmentDetails {

    @JsonProperty("investmentType")
    private String investmentType;

    @JsonProperty("InvestmentName")
    private String investmentName;

    @JsonProperty("Amount")
    private String amount;

}
