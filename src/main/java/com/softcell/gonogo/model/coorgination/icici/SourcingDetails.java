package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ssg0302 on 17/2/20.
 */
@Document(collection = "sourcingDetailsMaster")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SourcingDetails {
    private String product;

    private String institutionId;

    private String branchId;

    private String branchName;

    private String location;

    private String region;

    private String zone;

    private String country;

    private String apsLocation;

    private String iciciLocation;

    private String rmName;

    private String rmEmpId;

    private String iDisburseProcessShop;

    private String iDisburseSpokeLocation;

    private String rcmName;

    private String rcmEmpid;

}
