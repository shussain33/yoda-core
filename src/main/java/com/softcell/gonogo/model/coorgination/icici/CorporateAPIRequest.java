package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CorporateAPIRequest {

    @JsonProperty("IDSG_APPLICATION_SOURCE")
    private IdsgApplicationSource idsgApplicationSource;

    @JsonProperty("IDSG_APPLICANTDETAILS")
    private IdsgApplicantDetails idsgApplicantDetails;

    @JsonProperty("IDSG_loanDetails")
    private IdsgLoanDetails idsgLoanDetails;

    @JsonProperty("IDSG_CO_APPLICANTDETAILS")
    private List<IdsgApplicantDetails> idsgCoApplicantDetails;

    @JsonProperty("IDSG_INVESTMENTDETAIL")
    private List<IdsgInvestmentDetails> idsgInvestmentdetail;

    @JsonProperty("IDSG_LIABILITYDETAIL")
    private List<IdsgLiabilityDetails> liabilityDetails;

}
