package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressDetails {

    @JsonProperty("addressType")
    private String addressType;

    @JsonProperty("addressLine1")
    private String addressLine1;

    @JsonProperty("addressLine2")
    private String addressLine2;

    @JsonProperty("addressLine3")
    private String addressLine3;

    @JsonProperty("state")
    private String state;

    @JsonProperty("stateCode")
    private String stateCode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("cityCode")
    private String cityCode;

    @JsonProperty("zipcode")
    private String zipcode;

    @JsonProperty("country")
    private String country;

    @JsonProperty("MailingAddress")
    private String mailingAddress;

    @JsonProperty("countryCode")
    private String countryCode;

    @JsonProperty("phoneNumber")
    private List<PhoneNumber> phoneNumber;

}
