package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PhoneNumber {

    @JsonProperty("phoneType")
    private String phoneType;

    @JsonProperty("phoneNo")
    private String phoneNo;

    @JsonProperty("stdCode")
    private String stdCode;

}
