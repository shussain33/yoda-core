package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CoorginationAPIRequest {

    @JsonProperty("oHeader")
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("oCorporateAPIRequest")
    private CorporateAPIRequest corporateAPIRequest;

    @JsonProperty("sType")
    private String type;
}
