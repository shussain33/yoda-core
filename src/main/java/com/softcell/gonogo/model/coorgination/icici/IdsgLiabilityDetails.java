package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdsgLiabilityDetails {

    @JsonProperty("sourceDescription")
    private String sourceDescription;

    @JsonProperty("balanceAmount")
    private String balanceAmount;

    @JsonProperty("emiAmount")
    private String emiAmount;

    @JsonProperty("liabilityConsidered")
    private String liabilityConsidered;

}
