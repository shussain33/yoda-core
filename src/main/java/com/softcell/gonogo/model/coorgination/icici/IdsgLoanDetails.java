package com.softcell.gonogo.model.coorgination.icici;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 28/11/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdsgLoanDetails {

    @JsonProperty("loanAmountRequested")
    private String loanAmountRequested;

    @JsonProperty("DisburseTo")
    private String disburseTo;

    @JsonProperty("effectiveRate")
    private String effectiveRate;

    @JsonProperty("flatRate")
    private String flatRate;

    @JsonProperty("instalmentMode")
    private String instalmentMode;

    @JsonProperty("InstalmentPlan")
    private String instalmentPlan;

    @JsonProperty("LOANEMI")
    private String loanEmi;

    @JsonProperty("LTV")
    private String ltv;

    @JsonProperty("NoOfIntalments")
    private String noOfIntalments;

    @JsonProperty("processingFee")
    private String processingFee;

    @JsonProperty("purposeOfLoan")
    private String purposeOfLoan;

    @JsonProperty("rateEmiFlag")
    private String rateEmiFlag;

    @JsonProperty("Tenure")
    private String tenure;
}
