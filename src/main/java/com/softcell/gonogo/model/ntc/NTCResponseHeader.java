package com.softcell.gonogo.model.ntc;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author bhuvneshk
 */
public class NTCResponseHeader {

    @JsonProperty("APPLICATION-ID")
    private String applicationId;

    @JsonProperty("CUST-ID")
    private String custID;

    @JsonProperty("RESPONSE-TYPE")
    private String responseType;

    @JsonProperty("REQUEST-RECEIVED-TIME")
    private String requestReceivedTime;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getRequestReceivedTime() {
        return requestReceivedTime;
    }

    public void setRequestReceivedTime(String requestReceivedTime) {
        this.requestReceivedTime = requestReceivedTime;
    }

    /**
     * @return the custID
     */
    public String getCustID() {
        return custID;
    }

    /**
     * @param custID the custID to set
     */
    public void setCustID(String custID) {
        this.custID = custID;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "NTCResponseHeader [applicationId=" + applicationId
                + ", custID=" + custID + ", responseType=" + responseType
                + ", requestReceivedTime=" + requestReceivedTime + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((applicationId == null) ? 0 : applicationId.hashCode());
        result = prime * result + ((custID == null) ? 0 : custID.hashCode());
        result = prime
                * result
                + ((requestReceivedTime == null) ? 0 : requestReceivedTime
                .hashCode());
        result = prime * result
                + ((responseType == null) ? 0 : responseType.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NTCResponseHeader other = (NTCResponseHeader) obj;
        if (applicationId == null) {
            if (other.applicationId != null)
                return false;
        } else if (!applicationId.equals(other.applicationId))
            return false;
        if (custID == null) {
            if (other.custID != null)
                return false;
        } else if (!custID.equals(other.custID))
            return false;
        if (requestReceivedTime == null) {
            if (other.requestReceivedTime != null)
                return false;
        } else if (!requestReceivedTime.equals(other.requestReceivedTime))
            return false;
        if (responseType == null) {
            if (other.responseType != null)
                return false;
        } else if (!responseType.equals(other.responseType))
            return false;
        return true;
    }


}
