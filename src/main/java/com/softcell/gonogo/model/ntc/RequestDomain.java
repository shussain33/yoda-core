package com.softcell.gonogo.model.ntc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDomain {

    @JsonProperty("HEADER")
    private NTCHeader header;

    @JsonProperty("REQUEST")
    private NTCRequest request;


    /**
     * @return the header
     */
    public NTCHeader getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(NTCHeader header) {
        this.header = header;
    }

    /**
     * @return the request
     */
    public NTCRequest getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(NTCRequest request) {
        this.request = request;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RequestDomain [header=" + header + ", request=" + request + "]";
    }


}
