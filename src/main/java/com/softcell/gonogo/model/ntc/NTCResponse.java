package com.softcell.gonogo.model.ntc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;

import java.util.List;

public class NTCResponse {


    @JsonProperty("HEADER")
    private NTCResponseHeader header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private String acknowledgementId;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("NTC-SCORE")
    private String ntcScore;


    @JsonProperty("NTC-SCORE-BAND")
    private String ntcScoreBand;


    @JsonProperty("ERRORS")
    private List<Errors> errors;

    /**
     * @return the header
     */
    public NTCResponseHeader getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(NTCResponseHeader header) {
        this.header = header;
    }

    /**
     * @return the acknowledgementId
     */
    public String getAcknowledgementId() {
        return acknowledgementId;
    }

    /**
     * @param acknowledgementId the acknowledgementId to set
     */
    public void setAcknowledgementId(String acknowledgementId) {
        this.acknowledgementId = acknowledgementId;
    }


    /**
     * @return the errors
     */
    public List<Errors> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }


    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the ntcScore
     */
    public String getNtcScore() {
        return ntcScore;
    }

    /**
     * @param ntcScore the ntcScore to set
     */
    public void setNtcScore(String ntcScore) {
        this.ntcScore = ntcScore;
    }

    /**
     * @return the ntcScoreBand
     */
    public String getNtcScoreBand() {
        return ntcScoreBand;
    }

    /**
     * @param ntcScoreBand the ntcScoreBand to set
     */
    public void setNtcScoreBand(String ntcScoreBand) {
        this.ntcScoreBand = ntcScoreBand;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("NTCResponse [header=");
        builder.append(header);
        builder.append(", acknowledgementId=");
        builder.append(acknowledgementId);
        builder.append(", status=");
        builder.append(status);
        builder.append(", ntcScore=");
        builder.append(ntcScore);
        builder.append(", ntcScoreBand=");
        builder.append(ntcScoreBand);
        builder.append(", errors=");
        builder.append(errors);
        builder.append("]");
        return builder.toString();
    }

}
