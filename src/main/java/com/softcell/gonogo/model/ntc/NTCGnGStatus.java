package com.softcell.gonogo.model.ntc;

/**
 * @author kishorp
 *         It Use to update Status of NTC reppService if called
 */
public class NTCGnGStatus {
    private String status;
    private String message;
    private String score;
    private String band;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the score
     */
    public String getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     * @return the band
     */
    public String getBand() {
        return band;
    }

    /**
     * @param band the band to set
     */
    public void setBand(String band) {
        this.band = band;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("NTCGnGStatus [status=");
        builder.append(status);
        builder.append(", message=");
        builder.append(message);
        builder.append(", score=");
        builder.append(score);
        builder.append(", band=");
        builder.append(band);
        builder.append("]");
        return builder.toString();
    }


}
