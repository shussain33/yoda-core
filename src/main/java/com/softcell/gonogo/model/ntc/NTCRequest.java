package com.softcell.gonogo.model.ntc;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */

public class NTCRequest {

    @JsonProperty("03")
    private String loanType;

    @JsonProperty("07")
    private String sourceSystemName;


    @JsonProperty("111")
    private String filler8;

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }


    public String getSourceSystemName() {
        return sourceSystemName;
    }

    public void setSourceSystemName(String sourceSystemName) {
        this.sourceSystemName = sourceSystemName;
    }

    /**
     * @return the filler8
     */
    public String getFiller8() {
        return filler8;
    }

    /**
     * @param filler8 the filler8 to set
     */
    public void setFiller8(String filler8) {
        this.filler8 = filler8;
    }

    @Override
    public String toString() {
        return "Request [loanType=" + loanType + ", sourceSystemName="
                + sourceSystemName + ", filler8=" + filler8 + "]";
    }

}
