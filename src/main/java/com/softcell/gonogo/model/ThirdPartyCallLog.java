package com.softcell.gonogo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 15/10/18.
 */

public abstract class ThirdPartyCallLog {
    protected String refId;

    protected String institutionId;

    protected String applicantId;

    protected String collateralId;

    protected String acknowledgementId;

    protected String requestType;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getApplicantId() {        return applicantId; }

    public void setApplicantId(String applicantId) {        this.applicantId = applicantId;    }

    public String getCollateralId() {
        return collateralId;
    }

    public void setCollateralId(String collateralId) {
        this.collateralId = collateralId;
    }

    public String getAcknowledgementId() {
        return acknowledgementId;
    }

    public void setAcknowledgementId(String acknowledgementId) {
        this.acknowledgementId = acknowledgementId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
