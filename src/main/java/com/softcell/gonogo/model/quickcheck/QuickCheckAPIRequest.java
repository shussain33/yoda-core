package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

/**
 * Created by ssg0302 on 17/8/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuickCheckAPIRequest {

    @JsonProperty("oHeader")
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sRequest")
    private EMandateRequest request;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sTransactionId")
    private String transactionId;

    @JsonProperty("sRequestType")
    private String requestType;

    @JsonProperty("sCallbackUrl")
    private String callbackUrl;
}
