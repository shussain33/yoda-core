package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by ssg0302 on 28/8/19.
 */
@Document(collection = "EMandateDetailsCallLog")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EMandateDetailsCallLog {

    @JsonProperty("refId")
    private String refId;

    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRequestType")
    private String requestType;

    @JsonProperty("oEmandateRequest")
    private EMandateRequest emandateRequest;

    @JsonProperty("oEmandateResponse")
    private EMandateResponse emandateResponse;

    @JsonProperty("oEmandateCallBackRequest")
    private EMandateCallBackRequest emandateCallBackRequest;

    @JsonProperty("oCallDate")
    private Date callDate = new Date();

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;

    @JsonProperty("sEMandateURL")
    private String emandateURL;

    @JsonProperty("sEMandateResult")
    private String emandateResult;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sTransactionId")
    private String transactionId;

    @JsonProperty("sCallbackUrl")
    private String callbackUrl;
}
