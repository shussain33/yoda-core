package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.Date;

/**
 * Created by ssg0302 on 28/8/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessedRequests {

    @JsonProperty("sRequestType")
    private String requestType;

    @JsonProperty("oEmandateRequest")
    private EMandateRequest emandateRequest;

    @JsonProperty("oEmandateResponse")
    private EMandateResponse emandateResponse;

    @JsonProperty("oEmandateCallBackRequest")
    private EMandateCallBackRequest emandateCallBackRequest;

    @JsonProperty("oCallDate")
    private Date callDate = new Date();

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sInstitutionName")
    private String institutionName;

    @JsonProperty("sTransactionId")
    private String transactionId;
}
