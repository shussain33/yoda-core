package com.softcell.gonogo.model.quickcheck;

/**
 * Created by ssg0302 on 28/8/19.
 */
public class EMandateConstants {

    public static final String CREATE_MANDATE = "save-mandate";
    public static final String PROCESS_MANDATE = "e-mandate";
    public static final String CALLBACK_MANDATE = "callback-mandate";
    public static final String SAVE_ESIGN_ENACH_DATA = "save-esign-enach-data";
    public static final String GET_MANDATE_STATUS = "get-mandate-status";
    public static final String RESEND_MANDATE_DETAILS = "resend-mandate-details";
}
