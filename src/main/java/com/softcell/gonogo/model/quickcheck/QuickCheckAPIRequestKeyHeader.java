package com.softcell.gonogo.model.quickcheck;

/**
 * Created by yogesh Khandare on 7/10/18.
 */
public class QuickCheckAPIRequestKeyHeader {

    public static final String INSTITUTION_NAME = "sInstituteName";
    public static final String LOGIN_ID = "sLoginId";
    public static final String PASSWORD = "sPassword";
    public static final String APPLICATION_ID = "sApplicationId";
    public static final String PRODUCT = "sProduct";
    public static final String SOURCE_SYSTEM = "sSourceSystem";

}
