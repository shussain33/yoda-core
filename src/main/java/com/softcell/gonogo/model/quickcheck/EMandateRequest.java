package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 17/8/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EMandateRequest {

    @JsonProperty("sAppID")
    private String appId;

    @JsonProperty("sMdtID")
    private String mdtId;

    @JsonProperty("sMType")
    private String mtype;

    @JsonProperty("sUMRN")
    private String umrn;

    @JsonProperty("sMDate")
    private String mdate;

    @JsonProperty("sSpBankCode")
    private String spBankCode;

    @JsonProperty("sUTLSCode")
    private String utlsCode;

    @JsonProperty("sTDebit")
    private String tdebit;

    @JsonProperty("sBankAc")
    private String bankAc;

    @JsonProperty("sIFSC")
    private String ifsc;

    @JsonProperty("sAmt")
    private String amt;

    @JsonProperty("sMICR")
    private String micr;

    @JsonProperty("sFrequency")
    private String frequency;

    @JsonProperty("sDType")
    private String dtype;

    @JsonProperty("sRef1")
    private String ref1;

    @JsonProperty("sRef2")
    private String ref2;

    @JsonProperty("sPhone")
    private String phone;

    @JsonProperty("sEmail")
    private String email;

    @JsonProperty("sPFrom")
    private String pfrom;

    @JsonProperty("sPTo")
    private String pto;

    @JsonProperty("sUntlCancel")
    private String untlCancel;

    @JsonProperty("sCust1")
    private String cust1;

    @JsonProperty("sCust2")
    private String cust2;

    @JsonProperty("sCust3")
    private String cust3;

    @JsonProperty("sIsAggregator")
    private String isAggregator;

    @JsonProperty("sSubMerchantId")
    private String subMerchantId;

    @JsonProperty("sCategoryCode")
    private String categoryCode;

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;

    //N - NetBanking, D - DebitCard
    @JsonProperty("sEmandateType")
    private String emandateType;
}
