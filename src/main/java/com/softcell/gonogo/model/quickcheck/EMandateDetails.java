package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssg0302 on 28/8/19.
 */

@Document(collection = "EMandateDetails")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EMandateDetails {

    @Id
    @JsonProperty("refId")
    private String refId;

    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("oProcessedRequests")
    private List<ProcessedRequests> processedRequests = new ArrayList();

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;

    @JsonProperty("sEMandateURL")
    private String emandateURL; //URL received from processMandate API

    @JsonProperty("sEMandateCallBackResult")
    private String emandateCallBackResult; //Result received from CallBackAPI

    @JsonProperty("sEMandateStatus")
    private String emandateStatus;//getMandateStatus API response status.

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("iResendCount")
    private Integer resendCount = 0;

}
