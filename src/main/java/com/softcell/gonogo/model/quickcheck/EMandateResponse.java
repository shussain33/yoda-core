package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by ssg0302 on 17/8/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EMandateResponse {

    @JsonProperty("statusCode")
    private int statusCode;

    @JsonProperty("payload")
    private EMandateResponseDetails payload;

    @JsonProperty("timeStamp")
    private String timeStamp;

    @JsonProperty("transactionId")
    private String transactionId;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("path")
    private String path;

    @JsonProperty("errorMessage")
    private String errorMessage;

}
