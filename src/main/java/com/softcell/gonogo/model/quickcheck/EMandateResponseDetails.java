package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


/**
 * Created by ssg0302 on 27/8/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EMandateResponseDetails {

    @JsonProperty("mandateData")
    private String mandateData;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResCode")
    private String resCode;

    @JsonProperty("sMdtID")
    private String mdtID;

    @JsonProperty("sAccountHolderName")
    private String accountHolderName;

    @JsonProperty("oMandateData")
    private Object omandateData;

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("sMandateId")
    private String mandateId;

    @JsonProperty("sMandateStatus")
    private String mandateStatus;

    @JsonProperty("sEMandateURL")
    private String emandateURL;
}
