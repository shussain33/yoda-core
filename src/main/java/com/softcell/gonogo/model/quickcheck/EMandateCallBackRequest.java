package com.softcell.gonogo.model.quickcheck;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 27/8/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EMandateCallBackRequest {

    @JsonProperty("sMdtID")
    private String mdtId;

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResCode")
    private String resCode;

    @JsonProperty("sAcceptNo")
    private String acceptNo;

    @JsonProperty("sMndtReqId")
    private String mndtReqId;

    @JsonProperty("sNPCIRefMsgId")
    private String NPCIRefMsgId;

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;
}
