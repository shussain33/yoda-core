package com.softcell.gonogo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 16/11/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ThirdPartyCall {

    @JsonProperty("bFinfortConsent")
    private boolean finfortConsent;

    @JsonProperty("bFinfortInitiated")
    private boolean finfortInitiated;
}
