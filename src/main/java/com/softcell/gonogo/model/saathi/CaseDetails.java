package com.softcell.gonogo.model.saathi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CaseDetails {

    @JsonProperty("prospectno")
    private String prospectno;

    @JsonProperty("status")
    private String status;

    @JsonProperty("branch")
    private String branch;

    // Format "22/09/2017 12:28 PM"
    @JsonProperty("createddate")
    private String createdDate;

    @JsonProperty("updateddate")
    private String updatedDate;

    @JsonProperty("dealername")
    private String dealerName;

    @JsonProperty("customername")
    private String customerName;

    @JsonProperty("mobileno")
    private String mobileNo;

    @JsonProperty("emi")
    private String emi;

    @JsonProperty("currentStatus")
    private String currentStatus;

    @JsonProperty("tenur")
    private String tenor;

    @JsonProperty("customerIRR")
    private String customerIRR;

    @JsonProperty("advancedEmiCount")
    private String advancedEmiCount;

    @JsonProperty("areaCode")
    private String areaCode;

    @JsonProperty("portfolio")
    private String portfolio;

    @JsonProperty("modelDesc")
    private String modelDesc;

}
