package com.softcell.gonogo.model.saathi;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by archana on 27/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "saathiCallLog")
public class SaathiCallLog {

    @JsonProperty("api")
    private String api;

    @JsonProperty("refId")
    private String refId;

    @JsonProperty("callDate")
    private Date callDate = new Date();

    @JsonProperty("mobileNumber")
    private String mobileNumber;

    @JsonProperty("response")
    private SaathiResponse response;


}
