package com.softcell.gonogo.model.casehistory;

import com.softcell.constants.DateUnit;

/**
 * @author kishorp
 */
public class DataUnit {

    private String value;

    private DateUnit.UNIT unit = DateUnit.UNIT.ms;

    public DateUnit.UNIT getUnit() {
        return unit;
    }

    public void setUnit(DateUnit.UNIT unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DataUnit [value=");
        builder.append(value);
        builder.append(", unit=");
        builder.append(unit);
        builder.append("]");
        return builder.toString();
    }

}
