package com.softcell.gonogo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "otpVerificationData")
/*db.otpVerificationData.createIndex( { "created": 1}, { "expireAfterSeconds": 600, "name": "otpVerificationData_created_idx"} );*/
public class OtpVerificationData extends AuditEntity {

    @Id
    private String id;

    private String institutionId;

    private String otp;

    private String purpose;

    private String status;

    private int retry;
}
