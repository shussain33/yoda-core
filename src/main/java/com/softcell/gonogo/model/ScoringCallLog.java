package com.softcell.gonogo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by archana on 20/12/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "scoringCallLog")
public class ScoringCallLog {

    private String refId;

    private String institutionId;

    private String applicantId;

    private String collateralId;

    private String acknowledgementId;

    private String requestType;

    private String step;

    private Date callDate = new Date();

    private Object request;

    private Object response;

    private String requestString;

    private String responseString;

    private String thirdPartyName;

    private String headers;

    private String url;

    private String error;

    @Deprecated
    private String strResponse;

}
