package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author yogeshb
 */
@Document(collection = "PlPincodeEmailMaster")
public class PlPincodeEmailMaster extends AuditEntity {

    private String srNo;
    private String pincodeMaster;
    private String product;
    private String applicantType;
    private String constitution;
    private String pincode;
    private String branchId;
    private String branchName;
    private String fprName;
    private String fprMailId;
    private String fprMobileNumber;
    private String secondFprName;
    private String secondFprMailId;
    private String secondFprMobileNumber;
    private String userId;
    private String city;

    @JsonIgnore
    private String institutionID;
    @JsonIgnore
    private boolean active;
    @JsonIgnore
    private Date date;

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getPincodeMaster() {
        return pincodeMaster;
    }

    public void setPincodeMaster(String pincodeMaster) {
        this.pincodeMaster = pincodeMaster;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public String getConstitution() {
        return constitution;
    }

    public void setConstitution(String constitution) {
        this.constitution = constitution;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getFprName() {
        return fprName;
    }

    public void setFprName(String fprName) {
        this.fprName = fprName;
    }

    public String getFprMailId() {
        return fprMailId;
    }

    public void setFprMailId(String fprMailId) {
        this.fprMailId = fprMailId;
    }

    public String getFprMobileNumber() {
        return fprMobileNumber;
    }

    public void setFprMobileNumber(String fprMobileNumber) {
        this.fprMobileNumber = fprMobileNumber;
    }

    public String getSecondFprName() {
        return secondFprName;
    }

    public void setSecondFprName(String secondFprName) {
        this.secondFprName = secondFprName;
    }

    public String getSecondFprMailId() {
        return secondFprMailId;
    }

    public void setSecondFprMailId(String secondFprMailId) {
        this.secondFprMailId = secondFprMailId;
    }

    public String getSecondFprMobileNumber() {
        return secondFprMobileNumber;
    }

    public void setSecondFprMobileNumber(String secondFprMobileNumber) {
        this.secondFprMobileNumber = secondFprMobileNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PlPincodeEmailMaster [srNo=");
        builder.append(srNo);
        builder.append(", pincodeMaster=");
        builder.append(pincodeMaster);
        builder.append(", product=");
        builder.append(product);
        builder.append(", applicantType=");
        builder.append(applicantType);
        builder.append(", constitution=");
        builder.append(constitution);
        builder.append(", pincode=");
        builder.append(pincode);
        builder.append(", branchId=");
        builder.append(branchId);
        builder.append(", branchName=");
        builder.append(branchName);
        builder.append(", fprName=");
        builder.append(fprName);
        builder.append(", fprMailId=");
        builder.append(fprMailId);
        builder.append(", fprMobileNumber=");
        builder.append(fprMobileNumber);
        builder.append(", secondFprName=");
        builder.append(secondFprName);
        builder.append(", secondFprMailId=");
        builder.append(secondFprMailId);
        builder.append(", secondFprMobileNumber=");
        builder.append(secondFprMobileNumber);
        builder.append(", userId=");
        builder.append(userId);
        builder.append(", institutionID=");
        builder.append(institutionID);
        builder.append(", active=");
        builder.append(active);
        builder.append(", date=");
        builder.append(date);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result
                + ((applicantType == null) ? 0 : applicantType.hashCode());
        result = prime * result
                + ((branchId == null) ? 0 : branchId.hashCode());
        result = prime * result
                + ((branchName == null) ? 0 : branchName.hashCode());
        result = prime * result
                + ((constitution == null) ? 0 : constitution.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result
                + ((fprMailId == null) ? 0 : fprMailId.hashCode());
        result = prime * result
                + ((fprMobileNumber == null) ? 0 : fprMobileNumber.hashCode());
        result = prime * result + ((fprName == null) ? 0 : fprName.hashCode());
        result = prime * result
                + ((institutionID == null) ? 0 : institutionID.hashCode());
        result = prime * result + ((pincode == null) ? 0 : pincode.hashCode());
        result = prime * result
                + ((pincodeMaster == null) ? 0 : pincodeMaster.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        result = prime * result
                + ((secondFprMailId == null) ? 0 : secondFprMailId.hashCode());
        result = prime
                * result
                + ((secondFprMobileNumber == null) ? 0 : secondFprMobileNumber
                .hashCode());
        result = prime * result
                + ((secondFprName == null) ? 0 : secondFprName.hashCode());
        result = prime * result + ((srNo == null) ? 0 : srNo.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PlPincodeEmailMaster))
            return false;
        PlPincodeEmailMaster other = (PlPincodeEmailMaster) obj;
        if (active != other.active)
            return false;
        if (applicantType == null) {
            if (other.applicantType != null)
                return false;
        } else if (!applicantType.equals(other.applicantType))
            return false;
        if (branchId == null) {
            if (other.branchId != null)
                return false;
        } else if (!branchId.equals(other.branchId))
            return false;
        if (branchName == null) {
            if (other.branchName != null)
                return false;
        } else if (!branchName.equals(other.branchName))
            return false;
        if (constitution == null) {
            if (other.constitution != null)
                return false;
        } else if (!constitution.equals(other.constitution))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (fprMailId == null) {
            if (other.fprMailId != null)
                return false;
        } else if (!fprMailId.equals(other.fprMailId))
            return false;
        if (fprMobileNumber == null) {
            if (other.fprMobileNumber != null)
                return false;
        } else if (!fprMobileNumber.equals(other.fprMobileNumber))
            return false;
        if (fprName == null) {
            if (other.fprName != null)
                return false;
        } else if (!fprName.equals(other.fprName))
            return false;
        if (institutionID == null) {
            if (other.institutionID != null)
                return false;
        } else if (!institutionID.equals(other.institutionID))
            return false;
        if (pincode == null) {
            if (other.pincode != null)
                return false;
        } else if (!pincode.equals(other.pincode))
            return false;
        if (pincodeMaster == null) {
            if (other.pincodeMaster != null)
                return false;
        } else if (!pincodeMaster.equals(other.pincodeMaster))
            return false;
        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;
        if (secondFprMailId == null) {
            if (other.secondFprMailId != null)
                return false;
        } else if (!secondFprMailId.equals(other.secondFprMailId))
            return false;
        if (secondFprMobileNumber == null) {
            if (other.secondFprMobileNumber != null)
                return false;
        } else if (!secondFprMobileNumber.equals(other.secondFprMobileNumber))
            return false;
        if (secondFprName == null) {
            if (other.secondFprName != null)
                return false;
        } else if (!secondFprName.equals(other.secondFprName))
            return false;
        if (srNo == null) {
            if (other.srNo != null)
                return false;
        } else if (!srNo.equals(other.srNo))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }
}
