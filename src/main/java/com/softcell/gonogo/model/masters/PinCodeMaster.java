/**
 * kishorp11:18:08 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author kishorp
 * <pre>
 * 		<em>PinCodeMaster</em>
 * </pre>
 * <p>
 * 		This is used for mongo collection and it is use to store Pin codes. 
 * </p>
 */
@Document(collection = "pincodeMaster")
public class PinCodeMaster extends AuditEntity {


    @JsonProperty("sZipCode")
    private String zipCode;
    @JsonProperty("sCity")
    private String city;
    @JsonProperty("sState")
    private String state;
    @JsonProperty("sZipDscr")
    private String zipCodeDescr;
    @JsonProperty("sInstID")
    private String institutionId;
    @JsonIgnore
    private boolean active = true;

    @JsonIgnore
    @JsonProperty("sBrnchCd")
    private String branchCode;

    @JsonIgnore
    @JsonProperty("sBrnchNm")
    private String branchName;

    @JsonIgnore
    @JsonProperty("slctn")
    private String location;

    @JsonIgnore
    @JsonProperty("sClstr")
    private String cluster;

    @JsonIgnore
    @JsonProperty("sRgn")
    private String region;

    @JsonIgnore
    @JsonProperty("sZone")
    private String zone;

    @JsonIgnore
    @JsonProperty("sCntry")
    private String country;

    @JsonIgnore
    @JsonProperty("sPinElgblty")
    private String pinEligibility;

    @JsonProperty("sCityCode")
    private String cityCode;

    @JsonProperty("sStateCode")
    private String stateCode;

    @JsonProperty("sCountryCode")
    private String countryCode;

    @JsonIgnore
    private Date insertDate = new Date();


    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCodeDescr() {
        return zipCodeDescr;
    }

    public void setZipCodeDescr(String zipCodeDescr) {
        this.zipCodeDescr = zipCodeDescr;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * @param branchCode the branchCode to set
     */
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the cluster
     */
    public String getCluster() {
        return cluster;
    }

    /**
     * @param cluster the cluster to set
     */
    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return the zone
     */
    public String getZone() {
        return zone;
    }

    /**
     * @param zone the zone to set
     */
    public void setZone(String zone) {
        this.zone = zone;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the pinEligibility
     */
    public String getPinEligibility() {
        return pinEligibility;
    }

    /**
     * @param pinEligibility the pinEligibility to set
     */
    public void setPinEligibility(String pinEligibility) {
        this.pinEligibility = pinEligibility;
    }


    public String getCityCode() { return cityCode; }

    public void setCityCode(String cityCode) { this.cityCode = cityCode; }

    public String getStateCode() { return stateCode; }

    public void setStateCode(String stateCode) { this.stateCode = stateCode; }

    public String getCountryCode() { return countryCode; }

    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PinCodeMaster [zipCode=");
        builder.append(zipCode);
        builder.append(", city=");
        builder.append(city);
        builder.append(", state=");
        builder.append(state);
        builder.append(", zipCodeDescr=");
        builder.append(zipCodeDescr);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", active=");
        builder.append(active);
        builder.append(", branchCode=");
        builder.append(branchCode);
        builder.append(", branchName=");
        builder.append(branchName);
        builder.append(", location=");
        builder.append(location);
        builder.append(", cluster=");
        builder.append(cluster);
        builder.append(", region=");
        builder.append(region);
        builder.append(", zone=");
        builder.append(zone);
        builder.append(", country=");
        builder.append(country);
        builder.append(", pinEligibility=");
        builder.append(pinEligibility);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append("]");
        return builder.toString();
    }


}
