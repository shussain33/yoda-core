package com.softcell.gonogo.model.masters;

/**
 * Created by bhuvneshk on 28/9/17.
 */
public enum CityStateMasterFields {

    CITYID("cityId"),
    CITYNAME("cityName"),
    STATEID("stateId"),
    STATEDESC("stateDesc");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    CityStateMasterFields(String value) {
        this.value = value;
    }
}
