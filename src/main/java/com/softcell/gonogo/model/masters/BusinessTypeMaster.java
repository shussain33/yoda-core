/**
 * vinodk6:14:22 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "BusinessTypeMaster")
public class BusinessTypeMaster extends AuditEntity {

    @JsonProperty("sBuisnessTypeDesc")
    private String businessTypeDescription;

    /**
     * @return the businessTypeDescription
     */
    public String getBusinessTypeDescription() {
        return businessTypeDescription;
    }

    /**
     * @param businessTypeDescription
     *            the businessTypeDescription to set
     */
    public void setBusinessTypeDescription(String businessTypeDescription) {
        this.businessTypeDescription = businessTypeDescription;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "BusinessTypeMaster [businessTypeDescription="
                + businessTypeDescription + "]";
    }
}
