package com.softcell.gonogo.model.masters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "logsMaster")
public class LogsMaster {

    public String institutionId;

    public String type;

    public Map<String, Map<String, Map<String, String>>> masterValues;
}
