/**
 * kishorp11:42:46 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "educationDescr"
})
/**
 *
 * @author kishorp
 *
 */
@Document(collection = "EducationMaster")
public class EducationMaster {

    @JsonProperty("educationDescr")
    private String educationDescr;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The educationDescr
     */
    @JsonProperty("educationDescr")
    public String getEducationDescr() {
        return educationDescr;
    }

    /**
     *
     * @param educationDescr
     * The educationDescr
     */
    @JsonProperty("educationDescr")
    public void setEducationDescr(String educationDescr) {
        this.educationDescr = educationDescr;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}