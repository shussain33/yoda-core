package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 26/10/17.
 */
@Document(collection = "netDisbursalDetailsMaster")
public class NetDisbursalDetailsMaster extends AuditEntity {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sPersonalMobile")
    private String personalMobile;

    @JsonProperty("sLosID")
    private String losID;

    @JsonProperty("sUtrNumber")
    private String utrNumber;

    @JsonProperty("sLosStatus")
    private String losStatus;

    @JsonProperty("dNewNetDisbursalAmt")
    private double newNetDisbursalAmt;

    @JsonProperty("dtDtTime")
    private Date dateTime = new Date();

    @JsonProperty("sInstId")
    private String institutionID;

    @JsonProperty("sProduct")
    private String product;

    @Transient
    @JsonProperty("sStatus")
    private String status;

    @Transient
    @JsonProperty("sResponseMsg")
    private String responseMessage;

    @JsonProperty("dOldNetDisbAmt")
    private double oldNetDisbursalAmt;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public String getLosID() {
        return losID;
    }

    public void setLosID(String losID) {
        this.losID = losID;
    }

    public String getLosStatus() {
        return losStatus;
    }

    public void setLosStatus(String losStatus) {
        this.losStatus = losStatus;
    }

    public double getNewNetDisbursalAmt() {
        return newNetDisbursalAmt;
    }

    public void setNewNetDisbursalAmt(double newNetDisbursalAmt) {
        this.newNetDisbursalAmt = newNetDisbursalAmt;
    }

    public String getPersonalMobile() {
        return personalMobile;
    }

    public void setPersonalMobile(String personalMobile) {
        this.personalMobile = personalMobile;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUtrNumber() {
        return utrNumber;
    }

    public void setUtrNumber(String utrNumber) {
        this.utrNumber = utrNumber;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public double getOldNetDisbursalAmt() {
        return oldNetDisbursalAmt;
    }

    public void setOldNetDisbursalAmt(double oldNetDisbursalAmt) {
        this.oldNetDisbursalAmt = oldNetDisbursalAmt;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NetDisbursalDetailsMaster{");
        sb.append("refID='").append(refID).append('\'');
        sb.append(", personalMobile='").append(personalMobile).append('\'');
        sb.append(", losID='").append(losID).append('\'');
        sb.append(", utrNumber='").append(utrNumber).append('\'');
        sb.append(", losStatus='").append(losStatus).append('\'');
        sb.append(", newNetDisbursalAmt=").append(newNetDisbursalAmt);
        sb.append(", dateTime=").append(dateTime);
        sb.append(", institutionID='").append(institutionID).append('\'');
        sb.append(", product='").append(product).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", responseMessage='").append(responseMessage).append('\'');
        sb.append(", oldNetDisbursalAmt=").append(oldNetDisbursalAmt);
        sb.append('}');
        return sb.toString();
    }
}
