package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Set;

/**
 * Created by amit on 30/5/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyUser {
    @JsonProperty("sUserId")
    String userId;

    @JsonProperty("sUserName")
    String userName;

    @JsonProperty("sRole")
    String role;

    @JsonProperty("sEmailId")
    String emailId;

    @JsonProperty("sContactNo")
    String contactNo;

    @JsonProperty("sDealerId")
    String dealerId;

    @JsonProperty("aDealerIds")
    Set<String> dealerIds;

    @JsonProperty("bActive")
    boolean active = true;

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        HierarchyUser that = (HierarchyUser)o;
        return StringUtils.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "HierarchyUser{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", role='" + role + '\'' +
                ", emailId='" + emailId + '\'' +
                ", contactNo='" + contactNo + '\'' +
                ", active=" + active +
                '}';
    }
}
