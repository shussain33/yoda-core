/**
 * yogeshb3:16:47 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author yogeshb
 *         <pre>
 *         		<em>SchemeMasterData</em>
 *         </pre>
 *         <p>
 *         This is used for mongo collection and it store the SchemeMasterData.
 *         </p>
 */

@Document(collection = "schemeMaster")
public class SchemeMasterData extends AuditEntity{
    @JsonProperty("sSchID")
    private String schemeID;//in use
    @JsonProperty("sSchDes")
    private String schemeDesc;//in use
    @JsonProperty("sCurrId")
    private String currencyID;
    @JsonProperty("sEnFlg")
    private String enabledFlag;
    @JsonProperty("sMaxAmt")
    private String maxAmtFin;
    @JsonProperty("sMinAmt")
    private String minAmtFin;//in use
    @JsonProperty("sIntR")
    private String intRate;
    @JsonProperty("sMinIntR")
    private String minIntRate;
    @JsonProperty("sIntTyp")
    private String intType;
    @JsonProperty("sTenu")
    private String tenure;
    @JsonProperty("sFreq")
    private String frequency;
    @JsonProperty("sInsTyp")
    private String instlType;
    @JsonProperty("sNumInst")
    private String numInstl;
    @JsonProperty("sDRte")
    private String sdRate;
    @JsonProperty("sDAmt")
    private String sdAmt;
    @JsonProperty("sDint")
    private String sdInt;//in use
    @JsonProperty("sDinTyp")
    private String sdIntType;
    @JsonProperty("sInstMod")
    private String instlMode;
    @JsonProperty("sFloRatFlg")
    private String floatingRateFlag;
    @JsonProperty("sLonTyp")
    private String loanType;
    @JsonProperty("sMxIntRt")
    private String maxIntRate;
    @JsonProperty("sMinTenu")
    private String minTenure;//in use
    @JsonProperty("sMxTenu")
    private String maxTenure;//in use
    @JsonProperty("sDpErYr")
    private String DpErYr;
    @JsonProperty("sPrePyPen")
    private String prePayPenalty;
    @JsonProperty("sFrEclosLocLn")
    private String forEcloseLockLn;
    @JsonProperty("sProdFlg")
    private String productFlag;
    @JsonProperty("sPdcFlg")
    private String pdcFlag;
    @JsonProperty("sDeci")
    private String decimals;
    @JsonProperty("sStat")
    private String status;
    @JsonProperty("sCid")
    private String ccid;
    @JsonProperty("sPMNTmod")
    private String pmnTmode;
    @JsonProperty("sAdjExitComp")
    private String adjExintComp;
    @JsonProperty("sLpiCrite")
    private String lpiCriteria;
    @JsonProperty("sLpiDateTUse")
    private String lpiDateToBeused;
    @JsonProperty("sLpiDon")
    private String lpiDoneOn;
    @JsonProperty("sMCStat")
    private String mcStatus;
    @JsonProperty("sMakeRID")
    private String makeRID;
    @JsonProperty("sMakeDat")
    private String makeDate;
    @JsonProperty("sAutID")
    private String authId;
    @JsonProperty("sAutDat")
    private String authDate;
    @JsonProperty("sLpiChrgID")
    private String lpiChargeID;
    @JsonProperty("sLOC")
    private String loc;
    @JsonProperty("sMinRR")
    private String minIRR;
    @JsonProperty("sProceFlg")
    private String processedFlag;
    @JsonProperty("sMultLonFlg")
    private String multiLoanFlag;
    @JsonProperty("sFlotFreq")
    private String floatingFrequency;
    @JsonProperty("sIntRounOfPara")
    private String intRoundOffPara;
    @JsonProperty("sIntRouTil")
    private String intRoundTill;
    @JsonProperty("sInsAmtRoundofPar")
    private String instlAmtRoundOffPara;
    @JsonProperty("sInstAmtTil")
    private String instlAmtRoundTill;
    @JsonProperty("sPriBasUni")
    private String prinBaseUnit;
    @JsonProperty("sDepoLink")
    private String depositLink;
    @JsonProperty("sFixedTer")
    private String fixedTerm;
    @JsonProperty("sPrimTyp")
    private String primeType;
    @JsonProperty("sLtMulti")
    private String ltvMultiplier;
    @JsonProperty("sInsuProdID")
    private String insurProductid;
    @JsonProperty("sBonuFre")
    private String bonusFreq;
    @JsonProperty("sLpiChargDon")
    private String lpiChargeDon;
    @JsonProperty("sPrePayNoCharge")
    private String prePayNotChargable;
    @JsonProperty("sParPrePrin")
    private String PartPrepaymentPrin;
    @JsonProperty("sPrePayAmt")
    private String prePayAmount;
    @JsonProperty("sFixedMonth")
    private String fixedForMonths;
    @JsonProperty("sMinIr")
    private String minIrr;
    @JsonProperty("sMaxIr")
    private String maxIrr;
    @JsonProperty("sScemSDate")
    private String schemeStartDate;
    @JsonProperty("sScemEDate")
    private String schemeEndDate;
    @JsonProperty("sBuckCod")
    private String bucketCode;
    @JsonProperty("sMxIns")
    private String maxInsr;
    @JsonProperty("sLSMVeriCodC")
    private String lsmLvmVericodeC;
    @JsonProperty("sLSMScorMerCritC")
    private String lsmScoreMergeCriteriaC;
    @JsonProperty("sSCHID")
    private String schID;
    @JsonProperty("sLSMAsseTMinN")
    private String lsmAssetMinageN;
    @JsonProperty("sLSMAsseTMxN")
    private String lsmAssetMaxageN;
    @JsonProperty("sLsmLwwWorkflowIDC")
    private String lsmLwwWorkflowIDC;
    @JsonProperty("sLSMFFMForIDC")
    private String lsmLffmFormIdC;
    @JsonProperty("sLSMPMPliSetC")
    private String lsmLpmPolicySetC;
    @JsonProperty("sLSMScoreTabC")
    private String lsmLsmScoreTableC;
    @JsonProperty("sLSMmode")
    private String lsmLmmModel;
    @JsonProperty("sLSMMake")
    private String lsmLmmMake;
    @JsonProperty("sLSMCollRatioN")
    private String lsmCollRatioN;
    @JsonProperty("sLSMForEclosDayN")
    private String lsmForEclosureIntDaysN;
    @JsonProperty("sLSMTv")
    private String lsmLtv;
    @JsonProperty("sEquCod")
    private String equivalentCode;
    @JsonProperty("sUsedFlg")
    private String usedFlag;
    @JsonProperty("sBouChrgID")
    private String bounceChargeId;
    @JsonProperty("sExInterRefun")
    private String exInterestRefund;
    @JsonProperty("sGapInterFlg")
    private String gapInterestFlag;
    @JsonProperty("sCommDate")
    private String commenceDate;
    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonIgnore
    private boolean active = true;

    @JsonIgnore
    private Date insertDate= new Date();
    @JsonProperty("sApplyFlg")
    private String applyFlag;
    @JsonProperty("dLoanAmtMax")
    private String loanAmtMax;
    @JsonProperty("dLoanAmtMin")
    private String loanAmtMin;
    @JsonProperty("dLTVMax")
    private String ltvMax;
    @JsonProperty("dLtvMin")
    private String ltvMin;
    @JsonProperty("dDownpaymentMax")
    private String downPaymentMax;
    @JsonProperty("dDownpaymentMin")
    private String downPaymentMin;
    @JsonProperty("dEcsChargeMax")
    private String ecsChargeMax;
    @JsonProperty("dEcsChargeMin")
    private String ecsChargeMin;
    @JsonProperty("dPFPerMax")
    private String pFPerMax;
    @JsonProperty("dPFPerMin")
    private String pFPerMin;
    @JsonProperty("dPFFixedMax")
    private String pFFixedMax;
    @JsonProperty("dPFFixedMin")
    private String pFFixedMin;
    @JsonProperty("dManuSubMax")
    private String manuSubMax;
    @JsonProperty("sMFRSubType")
    private String mFRSubType;
    @JsonProperty("sDealerSubType")
    private String dealerSubType;
    @JsonProperty("dDealerSubMax")
    private String dealerSubMax;
    @JsonProperty("sPFType")
    private String pfType;
    @JsonProperty("sPFValue")
    private String pfValue;
    @JsonProperty("sStampsChargesMax")
    private String stampsChargesMax;
    @JsonProperty("sStampsChargesMin")
    private String stampsChargesMin;
    @JsonProperty("sSubventionType")
    private String subventionType;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }

    public String getEnabledFlag() {
        return enabledFlag;
    }

    public void setEnabledFlag(String enabledFlag) {
        this.enabledFlag = enabledFlag;
    }

    public String getMaxAmtFin() {
        return maxAmtFin;
    }

    public void setMaxAmtFin(String maxAmtFin) {
        this.maxAmtFin = maxAmtFin;
    }

    public String getMinAmtFin() {
        return minAmtFin;
    }

    public void setMinAmtFin(String minAmtFin) {
        this.minAmtFin = minAmtFin;
    }

    public String getIntRate() {
        return intRate;
    }

    public void setIntRate(String intRate) {
        this.intRate = intRate;
    }

    public String getMinIntRate() {
        return minIntRate;
    }

    public void setMinIntRate(String minIntRate) {
        this.minIntRate = minIntRate;
    }

    public String getIntType() {
        return intType;
    }

    public void setIntType(String intType) {
        this.intType = intType;
    }

    public String getTenure() {
        return tenure;
    }

    public void setTenure(String tenure) {
        this.tenure = tenure;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getInstlType() {
        return instlType;
    }

    public void setInstlType(String instlType) {
        this.instlType = instlType;
    }

    public String getNumInstl() {
        return numInstl;
    }

    public void setNumInstl(String numInstl) {
        this.numInstl = numInstl;
    }

    public String getSdRate() {
        return sdRate;
    }

    public void setSdRate(String sdRate) {
        this.sdRate = sdRate;
    }

    public String getSdAmt() {
        return sdAmt;
    }

    public void setSdAmt(String sdAmt) {
        this.sdAmt = sdAmt;
    }

    public String getSdInt() {
        return sdInt;
    }

    public void setSdInt(String sdInt) {
        this.sdInt = sdInt;
    }

    public String getSdIntType() {
        return sdIntType;
    }

    public void setSdIntType(String sdIntType) {
        this.sdIntType = sdIntType;
    }

    public String getInstlMode() {
        return instlMode;
    }

    public void setInstlMode(String instlMode) {
        this.instlMode = instlMode;
    }

    public String getFloatingRateFlag() {
        return floatingRateFlag;
    }

    public void setFloatingRateFlag(String floatingRateFlag) {
        this.floatingRateFlag = floatingRateFlag;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getMaxIntRate() {
        return maxIntRate;
    }

    public void setMaxIntRate(String maxIntRate) {
        this.maxIntRate = maxIntRate;
    }

    public String getMinTenure() {
        return minTenure;
    }

    public void setMinTenure(String minTenure) {
        this.minTenure = minTenure;
    }

    public String getMaxTenure() {
        return maxTenure;
    }

    public void setMaxTenure(String maxTenure) {
        this.maxTenure = maxTenure;
    }

    public String getDpErYr() {
        return DpErYr;
    }

    public void setDpErYr(String dpErYr) {
        DpErYr = dpErYr;
    }

    public String getPrePayPenalty() {
        return prePayPenalty;
    }

    public void setPrePayPenalty(String prePayPenalty) {
        this.prePayPenalty = prePayPenalty;
    }

    public String getForEcloseLockLn() {
        return forEcloseLockLn;
    }

    public void setForEcloseLockLn(String forEcloseLockLn) {
        this.forEcloseLockLn = forEcloseLockLn;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public String getPdcFlag() {
        return pdcFlag;
    }

    public void setPdcFlag(String pdcFlag) {
        this.pdcFlag = pdcFlag;
    }

    public String getDecimals() {
        return decimals;
    }

    public void setDecimals(String decimals) {
        this.decimals = decimals;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCcid() {
        return ccid;
    }

    public void setCcid(String ccid) {
        this.ccid = ccid;
    }

    public String getPmnTmode() {
        return pmnTmode;
    }

    public void setPmnTmode(String pmnTmode) {
        this.pmnTmode = pmnTmode;
    }

    public String getAdjExintComp() {
        return adjExintComp;
    }

    public void setAdjExintComp(String adjExintComp) {
        this.adjExintComp = adjExintComp;
    }

    public String getLpiCriteria() {
        return lpiCriteria;
    }

    public void setLpiCriteria(String lpiCriteria) {
        this.lpiCriteria = lpiCriteria;
    }

    public String getLpiDateToBeused() {
        return lpiDateToBeused;
    }

    public void setLpiDateToBeused(String lpiDateToBeused) {
        this.lpiDateToBeused = lpiDateToBeused;
    }

    public String getLpiDoneOn() {
        return lpiDoneOn;
    }

    public void setLpiDoneOn(String lpiDoneOn) {
        this.lpiDoneOn = lpiDoneOn;
    }

    public String getMcStatus() {
        return mcStatus;
    }

    public void setMcStatus(String mcStatus) {
        this.mcStatus = mcStatus;
    }

    public String getMakeRID() {
        return makeRID;
    }

    public void setMakeRID(String makeRID) {
        this.makeRID = makeRID;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getLpiChargeID() {
        return lpiChargeID;
    }

    public void setLpiChargeID(String lpiChargeID) {
        this.lpiChargeID = lpiChargeID;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getMinIRR() {
        return minIRR;
    }

    public void setMinIRR(String minIRR) {
        this.minIRR = minIRR;
    }

    public String getProcessedFlag() {
        return processedFlag;
    }

    public void setProcessedFlag(String processedFlag) {
        this.processedFlag = processedFlag;
    }

    public String getMultiLoanFlag() {
        return multiLoanFlag;
    }

    public void setMultiLoanFlag(String multiLoanFlag) {
        this.multiLoanFlag = multiLoanFlag;
    }

    public String getFloatingFrequency() {
        return floatingFrequency;
    }

    public void setFloatingFrequency(String floatingFrequency) {
        this.floatingFrequency = floatingFrequency;
    }

    public String getIntRoundOffPara() {
        return intRoundOffPara;
    }

    public void setIntRoundOffPara(String intRoundOffPara) {
        this.intRoundOffPara = intRoundOffPara;
    }

    public String getIntRoundTill() {
        return intRoundTill;
    }

    public void setIntRoundTill(String intRoundTill) {
        this.intRoundTill = intRoundTill;
    }

    public String getInstlAmtRoundOffPara() {
        return instlAmtRoundOffPara;
    }

    public void setInstlAmtRoundOffPara(String instlAmtRoundOffPara) {
        this.instlAmtRoundOffPara = instlAmtRoundOffPara;
    }

    public String getInstlAmtRoundTill() {
        return instlAmtRoundTill;
    }

    public void setInstlAmtRoundTill(String instlAmtRoundTill) {
        this.instlAmtRoundTill = instlAmtRoundTill;
    }

    public String getPrinBaseUnit() {
        return prinBaseUnit;
    }

    public void setPrinBaseUnit(String prinBaseUnit) {
        this.prinBaseUnit = prinBaseUnit;
    }

    public String getDepositLink() {
        return depositLink;
    }

    public void setDepositLink(String depositLink) {
        this.depositLink = depositLink;
    }

    public String getFixedTerm() {
        return fixedTerm;
    }

    public void setFixedTerm(String fixedTerm) {
        this.fixedTerm = fixedTerm;
    }

    public String getPrimeType() {
        return primeType;
    }

    public void setPrimeType(String primeType) {
        this.primeType = primeType;
    }

    public String getLtvMultiplier() {
        return ltvMultiplier;
    }

    public void setLtvMultiplier(String ltvMultiplier) {
        this.ltvMultiplier = ltvMultiplier;
    }

    public String getInsurProductid() {
        return insurProductid;
    }

    public void setInsurProductid(String insurProductid) {
        this.insurProductid = insurProductid;
    }

    public String getBonusFreq() {
        return bonusFreq;
    }

    public void setBonusFreq(String bonusFreq) {
        this.bonusFreq = bonusFreq;
    }

    public String getLpiChargeDon() {
        return lpiChargeDon;
    }

    public void setLpiChargeDon(String lpiChargeDon) {
        this.lpiChargeDon = lpiChargeDon;
    }

    public String getPrePayNotChargable() {
        return prePayNotChargable;
    }

    public void setPrePayNotChargable(String prePayNotChargable) {
        this.prePayNotChargable = prePayNotChargable;
    }

    public String getPartPrepaymentPrin() {
        return PartPrepaymentPrin;
    }

    public void setPartPrepaymentPrin(String partPrepaymentPrin) {
        PartPrepaymentPrin = partPrepaymentPrin;
    }

    public String getPrePayAmount() {
        return prePayAmount;
    }

    public void setPrePayAmount(String prePayAmount) {
        this.prePayAmount = prePayAmount;
    }

    public String getFixedForMonths() {
        return fixedForMonths;
    }

    public void setFixedForMonths(String fixedForMonths) {
        this.fixedForMonths = fixedForMonths;
    }

    public String getMinIrr() {
        return minIrr;
    }

    public void setMinIrr(String minIrr) {
        this.minIrr = minIrr;
    }

    public String getMaxIrr() {
        return maxIrr;
    }

    public void setMaxIrr(String maxIrr) {
        this.maxIrr = maxIrr;
    }

    public String getSchemeStartDate() {
        return schemeStartDate;
    }

    public void setSchemeStartDate(String schemeStartDate) {
        this.schemeStartDate = schemeStartDate;
    }

    public String getSchemeEndDate() {
        return schemeEndDate;
    }

    public void setSchemeEndDate(String schemeEndDate) {
        this.schemeEndDate = schemeEndDate;
    }

    public String getBucketCode() {
        return bucketCode;
    }

    public void setBucketCode(String bucketCode) {
        this.bucketCode = bucketCode;
    }

    public String getMaxInsr() {
        return maxInsr;
    }

    public void setMaxInsr(String maxInsr) {
        this.maxInsr = maxInsr;
    }

    public String getLsmLvmVericodeC() {
        return lsmLvmVericodeC;
    }

    public void setLsmLvmVericodeC(String lsmLvmVericodeC) {
        this.lsmLvmVericodeC = lsmLvmVericodeC;
    }

    public String getLsmScoreMergeCriteriaC() {
        return lsmScoreMergeCriteriaC;
    }

    public void setLsmScoreMergeCriteriaC(String lsmScoreMergeCriteriaC) {
        this.lsmScoreMergeCriteriaC = lsmScoreMergeCriteriaC;
    }

    public String getSchID() {
        return schID;
    }

    public void setSchID(String schID) {
        this.schID = schID;
    }

    public String getLsmAssetMinageN() {
        return lsmAssetMinageN;
    }

    public void setLsmAssetMinageN(String lsmAssetMinageN) {
        this.lsmAssetMinageN = lsmAssetMinageN;
    }

    public String getLsmAssetMaxageN() {
        return lsmAssetMaxageN;
    }

    public void setLsmAssetMaxageN(String lsmAssetMaxageN) {
        this.lsmAssetMaxageN = lsmAssetMaxageN;
    }

    public String getLsmLwwWorkflowIDC() {
        return lsmLwwWorkflowIDC;
    }

    public void setLsmLwwWorkflowIDC(String lsmLwwWorkflowIDC) {
        this.lsmLwwWorkflowIDC = lsmLwwWorkflowIDC;
    }

    public String getLsmLffmFormIdC() {
        return lsmLffmFormIdC;
    }

    public void setLsmLffmFormIdC(String lsmLffmFormIdC) {
        this.lsmLffmFormIdC = lsmLffmFormIdC;
    }

    public String getLsmLpmPolicySetC() {
        return lsmLpmPolicySetC;
    }

    public void setLsmLpmPolicySetC(String lsmLpmPolicySetC) {
        this.lsmLpmPolicySetC = lsmLpmPolicySetC;
    }

    public String getLsmLsmScoreTableC() {
        return lsmLsmScoreTableC;
    }

    public void setLsmLsmScoreTableC(String lsmLsmScoreTableC) {
        this.lsmLsmScoreTableC = lsmLsmScoreTableC;
    }

    public String getLsmLmmModel() {
        return lsmLmmModel;
    }

    public void setLsmLmmModel(String lsmLmmModel) {
        this.lsmLmmModel = lsmLmmModel;
    }

    public String getLsmLmmMake() {
        return lsmLmmMake;
    }

    public void setLsmLmmMake(String lsmLmmMake) {
        this.lsmLmmMake = lsmLmmMake;
    }

    public String getLsmCollRatioN() {
        return lsmCollRatioN;
    }

    public void setLsmCollRatioN(String lsmCollRatioN) {
        this.lsmCollRatioN = lsmCollRatioN;
    }

    public String getLsmForEclosureIntDaysN() {
        return lsmForEclosureIntDaysN;
    }

    public void setLsmForEclosureIntDaysN(String lsmForEclosureIntDaysN) {
        this.lsmForEclosureIntDaysN = lsmForEclosureIntDaysN;
    }

    public String getLsmLtv() {
        return lsmLtv;
    }

    public void setLsmLtv(String lsmLtv) {
        this.lsmLtv = lsmLtv;
    }

    public String getEquivalentCode() {
        return equivalentCode;
    }

    public void setEquivalentCode(String equivalentCode) {
        this.equivalentCode = equivalentCode;
    }

    public String getUsedFlag() {
        return usedFlag;
    }

    public void setUsedFlag(String usedFlag) {
        this.usedFlag = usedFlag;
    }

    public String getBounceChargeId() {
        return bounceChargeId;
    }

    public void setBounceChargeId(String bounceChargeId) {
        this.bounceChargeId = bounceChargeId;
    }

    public String getExInterestRefund() {
        return exInterestRefund;
    }

    public void setExInterestRefund(String exInterestRefund) {
        this.exInterestRefund = exInterestRefund;
    }

    public String getGapInterestFlag() {
        return gapInterestFlag;
    }

    public void setGapInterestFlag(String gapInterestFlag) {
        this.gapInterestFlag = gapInterestFlag;
    }

    public String getCommenceDate() {
        return commenceDate;
    }

    public void setCommenceDate(String commenceDate) {
        this.commenceDate = commenceDate;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getLtvMax() {return ltvMax;}

    public void setLtvMax(String ltvMax) {this.ltvMax = ltvMax;}

    public String getPfType() {
        return pfType;
    }

    public void setPfType(String pfType) {
        this.pfType = pfType;
    }

    public String getpFPerMax() { return pFPerMax; }

    public void setpFPerMax(String pFPerMax) { this.pFPerMax = pFPerMax; }

    public String getpFFixedMax() { return pFFixedMax; }

    public void setpFFixedMax(String pFFixedMax) { this.pFFixedMax = pFFixedMax; }

    public String getDealerSubMax() { return dealerSubMax; }

    public void setDealerSubMax(String dealerSubMax) { this.dealerSubMax = dealerSubMax; }

    public String getManuSubMax() { return manuSubMax; }

    public void setManuSubMax(String manuSubMax) { this.manuSubMax = manuSubMax; }

    public String getmFRSubType() { return mFRSubType; }

    public void setmFRSubType(String mFRSubType) { this.mFRSubType = mFRSubType; }

    public String getDealerSubType() { return dealerSubType; }

    public void setDealerSubType(String dealerSubType) { this.dealerSubType = dealerSubType;}

    public String getApplyFlag() {
        return applyFlag;
    }

    public void setApplyFlag(String applyFlag) {
        this.applyFlag = applyFlag;
    }

    public String getDownPaymentMax() {
        return downPaymentMax;
    }

    public void setDownPaymentMax(String downPaymentMax) {
        this.downPaymentMax = downPaymentMax;
    }

    public String getDownPaymentMin() {
        return downPaymentMin;
    }

    public void setDownPaymentMin(String downPaymentMin) {
        this.downPaymentMin = downPaymentMin;
    }

    public String getpFFixedMin() {
        return pFFixedMin;
    }

    public void setpFFixedMin(String pFFixedMin) {
        this.pFFixedMin = pFFixedMin;
    }

    public String getpFPerMin() {
        return pFPerMin;
    }

    public void setpFPerMin(String pFPerMin) {
        this.pFPerMin = pFPerMin;
    }

    public String getEcsChargeMax() {
        return ecsChargeMax;
    }

    public void setEcsChargeMax(String ecsChargeMax) {
        this.ecsChargeMax = ecsChargeMax;
    }

    public String getEcsChargeMin() {
        return ecsChargeMin;
    }

    public void setEcsChargeMin(String ecsChargeMin) {
        this.ecsChargeMin = ecsChargeMin;
    }

    public String getLoanAmtMax() {
        return loanAmtMax;
    }

    public void setLoanAmtMax(String loanAmtMax) {
        this.loanAmtMax = loanAmtMax;
    }

    public String getLoanAmtMin() {
        return loanAmtMin;
    }

    public void setLoanAmtMin(String loanAmtMin) {
        this.loanAmtMin = loanAmtMin;
    }

    public String getLtvMin() {
        return ltvMin;
    }

    public void setLtvMin(String ltvMin) {
        this.ltvMin = ltvMin;
    }

    public String getPfValue() {
        return pfValue;
    }

    public void setPfValue(String pfValue) {
        this.pfValue = pfValue;
    }

    public String getStampsChargesMax() {
        return stampsChargesMax;
    }

    public void setStampsChargesMax(String stampsChargesMax) {
        this.stampsChargesMax = stampsChargesMax;
    }

    public String getStampsChargesMin() {
        return stampsChargesMin;
    }

    public void setStampsChargesMin(String stampsChargesMin) {
        this.stampsChargesMin = stampsChargesMin;
    }

    public String getSubventionType() {
        return subventionType;
    }

    public void setSubventionType(String subventionType) {
        this.subventionType = subventionType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SchemeMasterData{");
        sb.append("schemeID='").append(schemeID).append('\'');
        sb.append(", schemeDesc='").append(schemeDesc).append('\'');
        sb.append(", currencyID='").append(currencyID).append('\'');
        sb.append(", enabledFlag='").append(enabledFlag).append('\'');
        sb.append(", maxAmtFin='").append(maxAmtFin).append('\'');
        sb.append(", minAmtFin='").append(minAmtFin).append('\'');
        sb.append(", intRate='").append(intRate).append('\'');
        sb.append(", minIntRate='").append(minIntRate).append('\'');
        sb.append(", intType='").append(intType).append('\'');
        sb.append(", tenure='").append(tenure).append('\'');
        sb.append(", frequency='").append(frequency).append('\'');
        sb.append(", instlType='").append(instlType).append('\'');
        sb.append(", numInstl='").append(numInstl).append('\'');
        sb.append(", sdRate='").append(sdRate).append('\'');
        sb.append(", sdAmt='").append(sdAmt).append('\'');
        sb.append(", sdInt='").append(sdInt).append('\'');
        sb.append(", sdIntType='").append(sdIntType).append('\'');
        sb.append(", instlMode='").append(instlMode).append('\'');
        sb.append(", floatingRateFlag='").append(floatingRateFlag).append('\'');
        sb.append(", loanType='").append(loanType).append('\'');
        sb.append(", maxIntRate='").append(maxIntRate).append('\'');
        sb.append(", minTenure='").append(minTenure).append('\'');
        sb.append(", maxTenure='").append(maxTenure).append('\'');
        sb.append(", DpErYr='").append(DpErYr).append('\'');
        sb.append(", prePayPenalty='").append(prePayPenalty).append('\'');
        sb.append(", forEcloseLockLn='").append(forEcloseLockLn).append('\'');
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append(", pdcFlag='").append(pdcFlag).append('\'');
        sb.append(", decimals='").append(decimals).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", ccid='").append(ccid).append('\'');
        sb.append(", pmnTmode='").append(pmnTmode).append('\'');
        sb.append(", adjExintComp='").append(adjExintComp).append('\'');
        sb.append(", lpiCriteria='").append(lpiCriteria).append('\'');
        sb.append(", lpiDateToBeused='").append(lpiDateToBeused).append('\'');
        sb.append(", lpiDoneOn='").append(lpiDoneOn).append('\'');
        sb.append(", mcStatus='").append(mcStatus).append('\'');
        sb.append(", makeRID='").append(makeRID).append('\'');
        sb.append(", makeDate='").append(makeDate).append('\'');
        sb.append(", authId='").append(authId).append('\'');
        sb.append(", authDate='").append(authDate).append('\'');
        sb.append(", lpiChargeID='").append(lpiChargeID).append('\'');
        sb.append(", loc='").append(loc).append('\'');
        sb.append(", minIRR='").append(minIRR).append('\'');
        sb.append(", processedFlag='").append(processedFlag).append('\'');
        sb.append(", multiLoanFlag='").append(multiLoanFlag).append('\'');
        sb.append(", floatingFrequency='").append(floatingFrequency).append('\'');
        sb.append(", intRoundOffPara='").append(intRoundOffPara).append('\'');
        sb.append(", intRoundTill='").append(intRoundTill).append('\'');
        sb.append(", instlAmtRoundOffPara='").append(instlAmtRoundOffPara).append('\'');
        sb.append(", instlAmtRoundTill='").append(instlAmtRoundTill).append('\'');
        sb.append(", prinBaseUnit='").append(prinBaseUnit).append('\'');
        sb.append(", depositLink='").append(depositLink).append('\'');
        sb.append(", fixedTerm='").append(fixedTerm).append('\'');
        sb.append(", primeType='").append(primeType).append('\'');
        sb.append(", ltvMultiplier='").append(ltvMultiplier).append('\'');
        sb.append(", insurProductid='").append(insurProductid).append('\'');
        sb.append(", bonusFreq='").append(bonusFreq).append('\'');
        sb.append(", lpiChargeDon='").append(lpiChargeDon).append('\'');
        sb.append(", prePayNotChargable='").append(prePayNotChargable).append('\'');
        sb.append(", PartPrepaymentPrin='").append(PartPrepaymentPrin).append('\'');
        sb.append(", prePayAmount='").append(prePayAmount).append('\'');
        sb.append(", fixedForMonths='").append(fixedForMonths).append('\'');
        sb.append(", minIrr='").append(minIrr).append('\'');
        sb.append(", maxIrr='").append(maxIrr).append('\'');
        sb.append(", schemeStartDate='").append(schemeStartDate).append('\'');
        sb.append(", schemeEndDate='").append(schemeEndDate).append('\'');
        sb.append(", bucketCode='").append(bucketCode).append('\'');
        sb.append(", maxInsr='").append(maxInsr).append('\'');
        sb.append(", lsmLvmVericodeC='").append(lsmLvmVericodeC).append('\'');
        sb.append(", lsmScoreMergeCriteriaC='").append(lsmScoreMergeCriteriaC).append('\'');
        sb.append(", schID='").append(schID).append('\'');
        sb.append(", lsmAssetMinageN='").append(lsmAssetMinageN).append('\'');
        sb.append(", lsmAssetMaxageN='").append(lsmAssetMaxageN).append('\'');
        sb.append(", lsmLwwWorkflowIDC='").append(lsmLwwWorkflowIDC).append('\'');
        sb.append(", lsmLffmFormIdC='").append(lsmLffmFormIdC).append('\'');
        sb.append(", lsmLpmPolicySetC='").append(lsmLpmPolicySetC).append('\'');
        sb.append(", lsmLsmScoreTableC='").append(lsmLsmScoreTableC).append('\'');
        sb.append(", lsmLmmModel='").append(lsmLmmModel).append('\'');
        sb.append(", lsmLmmMake='").append(lsmLmmMake).append('\'');
        sb.append(", lsmCollRatioN='").append(lsmCollRatioN).append('\'');
        sb.append(", lsmForEclosureIntDaysN='").append(lsmForEclosureIntDaysN).append('\'');
        sb.append(", lsmLtv='").append(lsmLtv).append('\'');
        sb.append(", equivalentCode='").append(equivalentCode).append('\'');
        sb.append(", usedFlag='").append(usedFlag).append('\'');
        sb.append(", bounceChargeId='").append(bounceChargeId).append('\'');
        sb.append(", exInterestRefund='").append(exInterestRefund).append('\'');
        sb.append(", gapInterestFlag='").append(gapInterestFlag).append('\'');
        sb.append(", commenceDate='").append(commenceDate).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", active=").append(active);
        sb.append(", insertDate=").append(insertDate);
        sb.append(", applyFlag='").append(applyFlag).append('\'');
        sb.append(", loanAmtMax=").append(loanAmtMax);
        sb.append(", loanAmtMin=").append(loanAmtMin);
        sb.append(", ltvMax=").append(ltvMax);
        sb.append(", ltvMin=").append(ltvMin);
        sb.append(", downPaymentMax=").append(downPaymentMax);
        sb.append(", downPaymentMin=").append(downPaymentMin);
        sb.append(", ecsChargeMax=").append(ecsChargeMax);
        sb.append(", ecsChargeMin=").append(ecsChargeMin);
        sb.append(", pFPerMax=").append(pFPerMax);
        sb.append(", pFPerMin=").append(pFPerMin);
        sb.append(", pFFixedMax=").append(pFFixedMax);
        sb.append(", pFFixedMin=").append(pFFixedMin);
        sb.append(", manuSubMax=").append(manuSubMax);
        sb.append(", mFRSubType='").append(mFRSubType).append('\'');
        sb.append(", dealerSubType='").append(dealerSubType).append('\'');
        sb.append(", dealerSubMax=").append(dealerSubMax);
        sb.append(", pfType='").append(pfType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
