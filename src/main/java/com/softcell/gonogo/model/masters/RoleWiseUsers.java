package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 10/7/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleWiseUsers {
    @JsonProperty("sRole")
    private String role;

    @JsonProperty("aUsers")
    private List<HierarchyUser> users;
}
