package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Softcell on 26/09/17.
 */
@Document(collection = "tCLosGngMappingMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TCLosGngMappingMaster extends AuditEntity {

    //SR_NO
    @JsonProperty("iSrNo")
    private int srNo;

    @JsonProperty("sGngField")
    private String gngField;

    @JsonProperty("sAccEntrytype")
    private String accEntrytype;

    @JsonProperty("sAccType")
    private String accType;

    @JsonProperty("sAccEntryNumber")
    private String accEntryNumber;

    @JsonProperty("sAccFieldType")
    private String accFieldType;

    @JsonProperty("sSapField")
    private String sapField;

    @JsonProperty("sFieldType")
    private String fieldType;

    @JsonIgnore
    private boolean active = true;

    @JsonProperty("sInstID")
    private String institutionId;

    public int getSrNo() { return srNo; }

    public void setSrNo(int srNo) { this.srNo = srNo; }

    public String getGngField() {
        return gngField;
    }

    public void setGngField(String gngField) {
        this.gngField = gngField;
    }

    public String getAccEntrytype() {
        return accEntrytype;
    }

    public void setAccEntrytype(String accEntrytype) {
        this.accEntrytype = accEntrytype;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccEntryNumber() {
        return accEntryNumber;
    }

    public void setAccEntryNumber(String accEntryNumber) {
        this.accEntryNumber = accEntryNumber;
    }

    public String getAccFieldType() {
        return accFieldType;
    }

    public void setAccFieldType(String accFieldType) {
        this.accFieldType = accFieldType;
    }

    public String getSapField() {
        return sapField;
    }

    public void setSapField(String sapField) {
        this.sapField = sapField;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getFieldType() { return fieldType; }

    public void setFieldType(String fieldType) { this.fieldType = fieldType; }
}
