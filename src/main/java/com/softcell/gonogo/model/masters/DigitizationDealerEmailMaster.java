package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Set;

/**
 * Created by mahesh on 3/1/18.
 */
@Document(collection = "digitizationDealerEmailMaster")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigitizationDealerEmailMaster  {

    @JsonProperty("sSupplierDesc")
    private String supplierDesc;

    @JsonProperty("sSupplierId")
    private String supplierID;

    @JsonProperty("aDelvryOrderCcEmailIds")
    private Set<String> deliveryOrderCcEmailIds;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonIgnore
    private boolean active = true;

    @JsonProperty("dtInsertDt")
    private Date insertDate = new Date();


    @JsonProperty("dtUpdateDt")
    private String updateDate;


}
