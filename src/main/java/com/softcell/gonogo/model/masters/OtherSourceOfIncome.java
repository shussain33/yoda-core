/**
 * vinodk6:23:42 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "OtherSourceOfIncome")
public class OtherSourceOfIncome extends AuditEntity {

    @JsonProperty("sOthSourceOfIncome")
    private String otherSourceOfIncome;

    /**
     * @return the otherSourceOfIncome
     */
    public String getOtherSourceOfIncome() {
        return otherSourceOfIncome;
    }

    /**
     * @param otherSourceOfIncome
     *            the otherSourceOfIncome to set
     */
    public void setOtherSourceOfIncome(String otherSourceOfIncome) {
        this.otherSourceOfIncome = otherSourceOfIncome;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "OtherSourceOfIncome [otherSourceOfIncome="
                + otherSourceOfIncome + "]";
    }
}
