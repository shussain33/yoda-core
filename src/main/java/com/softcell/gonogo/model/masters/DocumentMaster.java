/**
 * vinodk6:22:10 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "DocumentMaster")
public class DocumentMaster {

    @JsonProperty("sTypeOfDoc")
    private String typeOfDocument;

    /**
     * @return the typeOfDocument
     */
    public String getTypeOfDocument() {
        return typeOfDocument;
    }

    /**
     * @param typeOfDocument
     *            the typeOfDocument to set
     */
    public void setTypeOfDocument(String typeOfDocument) {
        this.typeOfDocument = typeOfDocument;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DocumentMaster [typeOfDocument=" + typeOfDocument + "]";
    }
}
