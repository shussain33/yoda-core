package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.master.CustomerName;
import com.softcell.constants.master.State;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.utils.GngDateUtil;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by abhishek on 8/9/17.
 */

@Document(collection = "taxCodeMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaxCodeMaster extends AuditEntity {
    @JsonProperty("sStateCode")
    private String stateCode;

    @JsonProperty("sOldTaxCode")
    private String oldTaxCode;

    @JsonProperty("sNewTaxCode")
    private String newTaxCode;

    @JsonProperty("sSacCode")
    private String sacCode;

    @JsonProperty("iSgstValue")
    private int sgstValue;

    @JsonProperty("iGstValue")
    private int gstValue;

    @JsonProperty("effectiveFrom")
    private Date effectiveFrom;

    @JsonProperty("effectiveTo")
    private Date effectiveTo;

    @JsonProperty("eCustomerName")
    private CustomerName customerName;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active = true;

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getOldTaxCode() {
        return oldTaxCode;
    }

    public void setOldTaxCode(String oldTaxCode) {
        this.oldTaxCode = oldTaxCode;
    }

    public String getNewTaxCode() {
        return newTaxCode;
    }

    public void setNewTaxCode(String newTaxCode) {
        this.newTaxCode = newTaxCode;
    }

    public String getSacCode() {
        return sacCode;
    }

    public void setSacCode(String sacCode) {
        this.sacCode = sacCode;
    }

    public int getSgstValue() {
        return sgstValue;
    }

    public void setSgstValue(int sgstValue) {
        this.sgstValue = sgstValue;
    }

    public int getGstValue() {
        return gstValue;
    }

    public void setGstValue(int gstValue) {
        this.gstValue = gstValue;
    }

    public String getEffectiveFrom() {
        return GngDateUtil.getFormattedDate(effectiveFrom, "dd/MM/yyyy");
    }

    public void setEffectiveFrom(String effectiveFrom) {
        Date effectiveFromDt = GngDateUtil.getDateFromSpecificFormat("dd/MM/yyyy", effectiveFrom);
        this.effectiveFrom = effectiveFromDt;
    }

    public String getEffectiveTo() {
        return GngDateUtil.getFormattedDate(effectiveTo, "dd/MM/yyyy");
    }

    public void setEffectiveTo(String effectiveTo) {
        Date effectiveToDt = GngDateUtil.getDateFromSpecificFormat("dd/MM/yyyy", effectiveTo);
        this.effectiveTo = effectiveToDt;
    }

    public String getCustomerName() {return customerName.name();}

    public void setCustomerName(String customerName) {
        this.customerName = CustomerName.valueOf(customerName);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getInstitutionId() { return institutionId; }

    public void setInstitutionId(String institutionId) { this.institutionId = institutionId; }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TaxCodeMaster [stateCode=");
        builder.append(stateCode);
        builder.append(", oldTaxCode=");
        builder.append(oldTaxCode);
        builder.append(", newTaxCode=");
        builder.append(newTaxCode);
        builder.append(", sacCode=");
        builder.append(sacCode);
        builder.append(", sgstValue=");
        builder.append(sgstValue);
        builder.append(", gstValue=");
        builder.append(gstValue);
        builder.append(", effectiveFrom=");
        builder.append(effectiveFrom);
        builder.append(", effectiveTo=");
        builder.append(effectiveTo);
        builder.append(", customerName=");
        builder.append(customerName);
        builder.append(", state=");
        builder.append(state);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", active=");
        builder.append(active);
        builder.append("]");
        return builder.toString();
    }


}
