package com.softcell.gonogo.model.masters;

/**
 * Created by bhuvneshk on 27/9/17.
 */
public enum InsurancePremiumFields {

    MINASSETVALUE("minAssetValue"),
    MAXASSETVALUE("maxAssetValue"),
    PREMIUM("insurancePremium"),
    PRODUCT_FLAG("productFlag"),
    INSURANCE_CODE("insuranceType"),
    INSURANCE_DESC("insuranceTypeDesc");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    InsurancePremiumFields(String value) {
        this.value = value;
    }
}
