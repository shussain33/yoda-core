/**
 * vinodk6:15:54 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "ProfessionsMaster")
public class ProfessionsMaster extends AuditEntity {

    @JsonProperty("sProfessionCodeDesc")
    private String professionCodeDesc;


    public String getProfessionCodeDesc() {
        return professionCodeDesc;
    }


    public void setProfessionCodeDesc(String professionCodeDesc) {
        this.professionCodeDesc = professionCodeDesc;
    }


    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ProfessionsMaster [professionCodeDesc=" + professionCodeDesc
                + "]";
    }
}
