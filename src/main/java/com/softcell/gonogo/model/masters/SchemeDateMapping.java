/**
 * yogeshb8:16:07 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb
 *         <pre>
 *         		<em>SchemeDateMapping</em>
 *         </pre>
 *         <p>
 *         This is used for mongo collection and it store the SchemeDateMapping Linkages.
 *         </p>
 */
@Document(collection = "SchemeDateMapping")
public class SchemeDateMapping extends AuditEntity {

    @JsonProperty("sSchemeID")
    private String schemeID;

    @JsonProperty("sValidFrDt")
    private String validFromDate;

    @JsonProperty("sValidToDt")
    private String validToDate;

    @JsonProperty("sExcldDt")
    private String excludedDate;

    @JsonProperty("sIncldDt")
    private String includedDate;

    @JsonProperty("sInsertDt")
    private String insertDate;

    @JsonProperty("sMkrID")
    private String makerID;

    @JsonProperty("sMakeDt")
    private String makeDate;

    @JsonProperty("sAuthID")
    private String authID;

    @JsonProperty("sAuthDt")
    private String authDate;

    @JsonProperty("sInstId")
    private String institutionID;

    @JsonProperty("bActive")
    private boolean active = true;


    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    public String getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    public String getExcludedDate() {
        return excludedDate;
    }

    public void setExcludedDate(String excludedDate) {
        this.excludedDate = excludedDate;
    }

    public String getIncludedDate() {
        return includedDate;
    }

    public void setIncludedDate(String includedDate) {
        this.includedDate = includedDate;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public String getMakerID() {
        return makerID;
    }

    public void setMakerID(String makerID) {
        this.makerID = makerID;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getAuthID() {
        return authID;
    }

    public void setAuthID(String authID) {
        this.authID = authID;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchemeDateMapping [schemeID=");
        builder.append(schemeID);
        builder.append(", validFromDate=");
        builder.append(validFromDate);
        builder.append(", validToDate=");
        builder.append(validToDate);
        builder.append(", excludedDate=");
        builder.append(excludedDate);
        builder.append(", includedDate=");
        builder.append(includedDate);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append(", makerID=");
        builder.append(makerID);
        builder.append(", makeDate=");
        builder.append(makeDate);
        builder.append(", authID=");
        builder.append(authID);
        builder.append(", authDate=");
        builder.append(authDate);
        builder.append(", institutionID=");
        builder.append(institutionID);
        builder.append(", active=");
        builder.append(active);
        builder.append("]");
        return builder.toString();
    }


}
