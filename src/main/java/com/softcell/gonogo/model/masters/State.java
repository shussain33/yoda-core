/**
 * kishorp8:05:13 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import java.util.Map;

/**
 * @author kishorp
 *
 */
public class State {

    private String institutionId;
    private BUREAU_TYPE bureauType;
    private Map<String, String> stateMAp;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public BUREAU_TYPE getBureauType() {
        return bureauType;
    }

    public void setBureauType(BUREAU_TYPE bureauType) {
        this.bureauType = bureauType;
    }

    public Map<String, String> getStateMAp() {
        return stateMAp;
    }

    public void setStateMAp(Map<String, String> stateMAp) {
        this.stateMAp = stateMAp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((bureauType == null) ? 0 : bureauType.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((stateMAp == null) ? 0 : stateMAp.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        State other = (State) obj;
        if (bureauType != other.bureauType)
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (stateMAp == null) {
            if (other.stateMAp != null)
                return false;
        } else if (!stateMAp.equals(other.stateMAp))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "State [institutionId=" + institutionId + ", bureauType="
                + bureauType + ", stateMAp=" + stateMAp + "]";
    }

    public enum BUREAU_TYPE {
        CIBIL, EQUIFAX, EXPERIAN, CRIF_HIGHMARK;
    }

}
