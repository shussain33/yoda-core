package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by bhuvneshk on 19/9/17.
 */
@Document(collection = "promotionalSchemeMaster")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PromotionalSchemeMaster {

    @JsonProperty("sPromotionId")
    private String promotionId;

    @JsonProperty("sProdCode")
    private String productCode;

    @JsonProperty("sMcStatus")
    private String mcStatus;

    @JsonProperty("sPrmDesc")
    private String promotionDesc;

    @JsonProperty("sPrmCode")
    private String promotionCode;

    @JsonProperty("dtInsertDate")
    private Date insertDate = new Date();

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonIgnore
    private boolean active = true;

}
