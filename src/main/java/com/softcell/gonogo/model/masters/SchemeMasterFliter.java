package com.softcell.gonogo.model.masters;

import java.util.List;

;

/**
 * @author kishor
 */
public class SchemeMasterFliter {

    private List<SchemeMasterData> withFilterSchemeData;
    private List<SchemeMasterData> withoutFilterSchemeData;

    public List<SchemeMasterData> getWithFilterSchemeData() {
        return withFilterSchemeData;
    }

    public void setWithFilterSchemeData(
            List<SchemeMasterData> withFilterSchemeData) {
        this.withFilterSchemeData = withFilterSchemeData;
    }

    public List<SchemeMasterData> getWithoutFilterSchemeData() {
        return withoutFilterSchemeData;
    }

    public void setWithoutFilterSchemeData(
            List<SchemeMasterData> withoutFilterSchemeData) {
        this.withoutFilterSchemeData = withoutFilterSchemeData;
    }

}
