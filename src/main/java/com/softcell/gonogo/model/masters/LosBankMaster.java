package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by yogeshb on 19/5/17.
 */
@Document(collection = "losBankMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LosBankMaster {
    @JsonIgnore
    private String bankId;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonIgnore
    private String bankCode;

    @JsonIgnore
    private String cityId;

    @JsonIgnore
    private String city;

    @JsonIgnore
    private String bankBranchId;

    @JsonIgnore
    private String bankBranchName;

    @JsonIgnore
    private String micrFlag;

    @JsonIgnore
    private String micrCode;

    @JsonIgnore
    private String institutionId;

    @JsonIgnore
    private Date dateTime;

    @JsonIgnore
    private boolean status;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBankBranchId() {
        return bankBranchId;
    }

    public void setBankBranchId(String bankBranchId) {
        this.bankBranchId = bankBranchId;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getMicrFlag() {
        return micrFlag;
    }

    public void setMicrFlag(String micrFlag) {
        this.micrFlag = micrFlag;
    }

    public String getMicrCode() {
        return micrCode;
    }

    public void setMicrCode(String micrCode) {
        this.micrCode = micrCode;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LosBankMaster{");
        sb.append("bankId='").append(bankId).append('\'');
        sb.append(", bankName='").append(bankName).append('\'');
        sb.append(", bankCode='").append(bankCode).append('\'');
        sb.append(", cityId='").append(cityId).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", bankBranchId='").append(bankBranchId).append('\'');
        sb.append(", bankBranchName='").append(bankBranchName).append('\'');
        sb.append(", micrFlag='").append(micrFlag).append('\'');
        sb.append(", micrCode='").append(micrCode).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", status=").append(status);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LosBankMaster that = (LosBankMaster) o;

        if (status != that.status) return false;
        if (bankId != null ? !bankId.equals(that.bankId) : that.bankId != null) return false;
        if (bankName != null ? !bankName.equals(that.bankName) : that.bankName != null) return false;
        if (bankCode != null ? !bankCode.equals(that.bankCode) : that.bankCode != null) return false;
        if (cityId != null ? !cityId.equals(that.cityId) : that.cityId != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (bankBranchId != null ? !bankBranchId.equals(that.bankBranchId) : that.bankBranchId != null) return false;
        if (bankBranchName != null ? !bankBranchName.equals(that.bankBranchName) : that.bankBranchName != null)
            return false;
        if (micrFlag != null ? !micrFlag.equals(that.micrFlag) : that.micrFlag != null) return false;
        if (micrCode != null ? !micrCode.equals(that.micrCode) : that.micrCode != null) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        return dateTime != null ? dateTime.equals(that.dateTime) : that.dateTime == null;
    }

    @Override
    public int hashCode() {
        int result = bankId != null ? bankId.hashCode() : 0;
        result = 31 * result + (bankName != null ? bankName.hashCode() : 0);
        result = 31 * result + (bankCode != null ? bankCode.hashCode() : 0);
        result = 31 * result + (cityId != null ? cityId.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (bankBranchId != null ? bankBranchId.hashCode() : 0);
        result = 31 * result + (bankBranchName != null ? bankBranchName.hashCode() : 0);
        result = 31 * result + (micrFlag != null ? micrFlag.hashCode() : 0);
        result = 31 * result + (micrCode != null ? micrCode.hashCode() : 0);
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (status ? 1 : 0);
        return result;
    }
}
