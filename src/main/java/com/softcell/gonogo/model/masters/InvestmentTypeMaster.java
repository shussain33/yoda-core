/**
 * vinodk6:20:52 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "InvestmentTypeMaster")
public class InvestmentTypeMaster extends AuditEntity {

    @JsonProperty("sInvestmentDesc")
    private String investmentTypeDescription;

    /**
     * @return the investmentTypeDescription
     */
    public String getInvestmentTypeDescription() {
        return investmentTypeDescription;
    }

    /**
     * @param investmentTypeDescription
     *            the investmentTypeDescription to set
     */
    public void setInvestmentTypeDescription(String investmentTypeDescription) {
        this.investmentTypeDescription = investmentTypeDescription;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "InvestmentTypeMaster [investmentTypeDescription="
                + investmentTypeDescription + "]";
    }
}
