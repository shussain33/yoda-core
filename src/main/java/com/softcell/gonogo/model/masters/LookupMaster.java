package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by ssg0302 on 3/9/19.
 */
@Document(collection = "LookupMaster")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LookupMaster {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sProductName")
    private String productName;

    @JsonProperty("sProductId")
    private String productId;

    @JsonProperty("sLookupIdentifier")
    private String lookupIdentifier;

    @JsonProperty("sLookupValue")
    private String lookupValue;

    @JsonProperty("dtInsertDate")
    private Date insertDate = new Date();

    @JsonProperty("bActive")
    private boolean active = true;
}
