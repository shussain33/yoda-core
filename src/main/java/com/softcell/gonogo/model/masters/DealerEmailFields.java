package com.softcell.gonogo.model.masters;

/**
 * Created by bhuvneshk on 27/9/17.
 */
public enum DealerEmailFields {

    SUPPLIERID("supplierID"),
    SUPPLIERDESC("supplierDesc"),
    City("cityId"),
    State("stateId"),
    Email("email"),
    DEALERSAPCODE("dealerSapCode");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    DealerEmailFields(String value) {
        this.value = value;
    }
}
