package com.softcell.gonogo.model.masters;

/**
 * Created by priyanka on 14/9/17.
 */
public enum SchemeMasterFields {
    SCHEMEID("schemeID"),
    SCHEMEDESC("schemeDesc"),
    CURRENCYID("currencyID"),
    ENABLED_FLAG("enabledFlag"),
    MAXAMTFIN("maxAmtFin"),
    MINAMTFIN("minAmtFin"),
    INTRATE("intRate"),
    MININTRATE("minIntRate"),
    INTTYPE("intType"),

    TENURE("tenure"),
    FREQUENCY("frequency"),
    INSTLTYPE("instlType"),
    NUMINSTL("numInstl"),
    SDRATE("sdRate"),
    SDAMT("sdAmt"),
    SDINT("sdInt"),
    SDINTTYPE("sdIntType"),
    INSTLMODE("instlMode"),
    FLOATING_RATE_FLAG("floatingRateFlag"),
    LOANTYPE("loanType"),
    MAXINTRATE("maxIntRate"),
    MINTENURE("minTenure"),

    MAXTENURE("maxTenure"),
    DPERYR("DpErYr"),
    PREPAYPENALTY("prePayPenalty"),
    FORECLOSELOCKLN("forEcloseLockLn"),
    PRODUCTFLAG("productFlag"),
    PDCFLAG("pdcFlag"),
    DECIMALS("decimals"),
    STATUS("status"),
    CCID("ccid"),
    PMNTMODE("pmnTmode"),
    ADJEXINTCOMP("adjExintComp"),
    LPICRITERIA("lpiCriteria"),
    LPIDATETOBEUSED("lpiDateToBeused"),
    LPIDONEON("lpiDoneOn"),

    MC_STATUS("mcStatus"),
    MAKERID("makeRID"),
    MAKEDATE("makeDate"),
    AUTHID("authId"),
    AUTHDATE("authDate"),
    LPI_CHARGEID("lpiChargeID"),
    LOC("loc"),
    MINIRR("minIRR"),
    PROCESSEDFLAG("processedFlag"),
    MULTILOANFLAG("multiLoanFlag"),
    FLOATING_FREQUENCY("floatingFrequency"),
    INT_ROUNDOFF_PARA("intRoundOffPara"),
    INT_ROUNDTILL("intRoundTill"),
    INSTLAMT_ROUNDOFF_PARA("instlAmtRoundOffPara"),
    INSTLAMT_ROUNDTILL("instlAmtRoundTill"),
    PRIN_BASE_UNIT("prinBaseUnit"),
    DEPOSIT_LINK("depositLink"),
    FIXED_TERM("fixedTerm"),
    PRIME_TYPE("primeType"),
    LTV_MULTIPLIER("ltvMultiplier"),
    INSUR_PRODUCTID("insurProductid"),
    BONUS_FREQ("bonusFreq"),
    LPI_CHARGEDON("lpiChargeDon"),
    PREPAYNOTCHARGABLE("prePayNotChargable"),
    PARTPREPAYMENTPRIN("PartPrepaymentPrin"),
    PREPAYAMOUNT("prePayAmount"),

    FIXED_FOR_MONTHS("fixedForMonths"),
    MIN_IRR("minIrr"),
    MAX_IRR("maxIrr"),
    SCHEME_START_DATE("schemeStartDate"),
    SCHEME_END_DATE("schemeEndDate"),
    BUCKETCODE("bucketCode"),
    MAX_INSR("maxInsr"),
    LSM_LVM_VERICODE_C("lsmLvmVericodeC"),
    LSM_SCORE_MERGE_CRITERIA_C("lsmScoreMergeCriteriaC"),
    SCHID("schID"),
    LSM_ASSET_MINAGE_N("lsmAssetMinageN"),
    LSM_ASSET_MAXAGE_N("lsmAssetMaxageN"),
    LSM_LWW_WORKFLOW_ID_C("lsmLwwWorkflowIDC"),
    LSM_LFFM_FORM_ID_C("lsmLffmFormIdC"),
    LSM_LPM_POLICY_SET_C("lsmLpmPolicySetC"),
    LSM_LSM_SCORE_TABLE_C("lsmLsmScoreTableC"),
    LSM_LMM_MODEL("lsmLmmModel"),
    LSM_LMM_MAKE("lsmLmmMake"),
    LSM_COLLRATIO_N("lsmCollRatioN"),
    LSM_FORECLOSURE_INT_DAYS_N("lsmForEclosureIntDaysN"),
    LSM_LTV("lsmLtv"),
    EQUIVALENT_CODE("equivalentCode"),
    USED_FLAG("usedFlag"),
    BOUNCE_CHARGEID("bounceChargeId"),
    EX_INTEREST_REFUND("exInterestRefund"),
    GAP_INTEREST_FLAG("gapInterestFlag"),
    COMMENCEDATE("commenceDate"),

    M_APPLY_FLAG("applyFlag"),
    LOANAMOUNT_MAX("loanAmtMax"),
    LOANAMOUNT_MIN("loanAmtMin"),
    LTV_MAX("ltvMax"),
    LTV_MIN("ltvMin"),
    DOWNPAYMENT_MAX("downPaymentMax"),
    DOWNPAYMEN_MIN("downPaymentMin"),
    ECSCHARGES_MAX("ecsChargeMax"),
    ECSCHARGES_MIN("ecsChargeMin"),
    PF_PERCENT_MAX("pFPerMax"),
    PF_PERCENT_MIN("pFPerMin"),
    PF_FIXED_MAX("pFFixedMax"),
    PF_FIXED_MIN("pFFixedMin"),
    MANUFACTURE_SUB_MAX("manuSubMax"),
    MFR_SUBVENTION_TYPE("mFRSubType"),
    DEALER_SUBVENTION_TYPE("dealerSubType"),
    PF_TYPE("pfType"),
    DEALER_SUBVENTION_MAX("dealerSubMax"),
    PF_VALUE("pfValue"),
    STAMPSCHARGESMAX("stampsChargesMax"),
    STAMPSCHARGESMIN("stampsChargesMin"),
    SUBVENTIONTYPE("subventionType");


    private final String stringValue;

    public String getStringValue() {
        return stringValue;
    }

    SchemeMasterFields(String stringValue) {
        this.stringValue = stringValue;

    }


    /*SchemeMasterFields(String stringValue, String stringValueName) {
        this.stringValue = stringValue;
        this.stringValueName = stringValueName;
    }*/


}
