/**
 * vinodk12:47:00 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author vinodk
 *
 */
public class Master {

    @JsonProperty("oAccountType")
    private List<AccountType> accountType;
    @JsonProperty("oApplicantTypeMaster")
    private List<ApplicantTypeMaster> applicantTypeMaster;
    @JsonProperty("oAssetsOwned")
    private List<AssetsOwned> assetsOwned;
    @JsonProperty("oBusinessTypeMaster")
    private List<BusinessTypeMaster> businessTypeMaster;
    @JsonProperty("oCompanyTypeMaster")
    private List<CompanyTypeMaster> companyTypeMaster;
    @JsonProperty("oDocumentMaster")
    private List<DocumentMaster> documentMaster;
    @JsonProperty("oEducationMaster")
    private List<EducationMaster> educationMaster;
    @JsonProperty("ogender")
    private List<Gender> gender;
    @JsonProperty("oIdentitiesMaster")
    private List<IdentitiesMaster> identitiesMaster;
    @JsonProperty("oIndustryCodeMaster")
    private List<IndustryCodeMaster> industryCodeMaster;
    @JsonProperty("oInvestmentTypeMaster")
    private List<InvestmentTypeMaster> investmentTypeMaster;
    @JsonProperty("oLoanPurposeMaster")
    private List<LoanPurposeMaster> loanPurposeMaster;
    @JsonProperty("oOtherSourceOfIncome")
    private List<OtherSourceOfIncome> otherSourceOfIncome;
    @JsonProperty("oProductsMaster")
    private List<ProductsMaster> productsMaster;
    @JsonProperty("oProfessionsMaster")
    private List<ProfessionsMaster> professionsMaster;
    @JsonProperty("oPropertyTypeMaster")
    private List<PropertyTypeMaster> propertyTypeMaster;
    @JsonProperty("oPropertyClassificationMaster")
    private List<PropertyClassificationMaster> propertyClassificationMaster;
    @JsonProperty("oResidenceTypeMaster")
    private List<ResidenceTypeMaster> residenceTypeMaster;
    @JsonProperty("oStatesMaster")
    private List<StatesMaster> statesMaster;

    public List<AccountType> getAccountType() {
        return accountType;
    }

    public void setAccountType(List<AccountType> accountType) {
        this.accountType = accountType;
    }

    public List<ApplicantTypeMaster> getApplicantTypeMaster() {
        return applicantTypeMaster;
    }

    public void setApplicantTypeMaster(List<ApplicantTypeMaster> applicantTypeMaster) {
        this.applicantTypeMaster = applicantTypeMaster;
    }

    public List<AssetsOwned> getAssetsOwned() {
        return assetsOwned;
    }

    public void setAssetsOwned(List<AssetsOwned> assetsOwned) {
        this.assetsOwned = assetsOwned;
    }

    public List<BusinessTypeMaster> getBusinessTypeMaster() {
        return businessTypeMaster;
    }

    public void setBusinessTypeMaster(List<BusinessTypeMaster> businessTypeMaster) {
        this.businessTypeMaster = businessTypeMaster;
    }

    public List<CompanyTypeMaster> getCompanyTypeMaster() {
        return companyTypeMaster;
    }

    public void setCompanyTypeMaster(List<CompanyTypeMaster> companyTypeMaster) {
        this.companyTypeMaster = companyTypeMaster;
    }

    public List<DocumentMaster> getDocumentMaster() {
        return documentMaster;
    }

    public void setDocumentMaster(List<DocumentMaster> documentMaster) {
        this.documentMaster = documentMaster;
    }

    public List<EducationMaster> getEducationMaster() {
        return educationMaster;
    }

    public void setEducationMaster(List<EducationMaster> educationMaster) {
        this.educationMaster = educationMaster;
    }

    public List<Gender> getGender() {
        return gender;
    }

    public void setGender(List<Gender> gender) {
        this.gender = gender;
    }

    public List<IdentitiesMaster> getIdentitiesMaster() {
        return identitiesMaster;
    }

    public void setIdentitiesMaster(List<IdentitiesMaster> identitiesMaster) {
        this.identitiesMaster = identitiesMaster;
    }

    public List<IndustryCodeMaster> getIndustryCodeMaster() {
        return industryCodeMaster;
    }

    public void setIndustryCodeMaster(List<IndustryCodeMaster> industryCodeMaster) {
        this.industryCodeMaster = industryCodeMaster;
    }

    public List<InvestmentTypeMaster> getInvestmentTypeMaster() {
        return investmentTypeMaster;
    }

    public void setInvestmentTypeMaster(
            List<InvestmentTypeMaster> investmentTypeMaster) {
        this.investmentTypeMaster = investmentTypeMaster;
    }

    public List<LoanPurposeMaster> getLoanPurposeMaster() {
        return loanPurposeMaster;
    }

    public void setLoanPurposeMaster(List<LoanPurposeMaster> loanPurposeMaster) {
        this.loanPurposeMaster = loanPurposeMaster;
    }

    public List<OtherSourceOfIncome> getOtherSourceOfIncome() {
        return otherSourceOfIncome;
    }

    public void setOtherSourceOfIncome(List<OtherSourceOfIncome> otherSourceOfIncome) {
        this.otherSourceOfIncome = otherSourceOfIncome;
    }

    public List<ProductsMaster> getProductsMaster() {
        return productsMaster;
    }

    public void setProductsMaster(List<ProductsMaster> productsMaster) {
        this.productsMaster = productsMaster;
    }

    public List<ProfessionsMaster> getProfessionsMaster() {
        return professionsMaster;
    }

    public void setProfessionsMaster(List<ProfessionsMaster> professionsMaster) {
        this.professionsMaster = professionsMaster;
    }

    public List<PropertyTypeMaster> getPropertyTypeMaster() {
        return propertyTypeMaster;
    }

    public void setPropertyTypeMaster(List<PropertyTypeMaster> propertyTypeMaster) {
        this.propertyTypeMaster = propertyTypeMaster;
    }

    public List<ResidenceTypeMaster> getResidenceTypeMaster() {
        return residenceTypeMaster;
    }

    public void setResidenceTypeMaster(List<ResidenceTypeMaster> residenceTypeMaster) {
        this.residenceTypeMaster = residenceTypeMaster;
    }

    public List<StatesMaster> getStatesMaster() {
        return statesMaster;
    }

    public void setStatesMaster(List<StatesMaster> statesMaster) {
        this.statesMaster = statesMaster;
    }

    /**
     * @return the propertyClassificationMaster
     */
    public List<PropertyClassificationMaster> getPropertyClassificationMaster() {
        return propertyClassificationMaster;
    }

    /**
     * @param propertyClassificationMaster the propertyClassificationMaster to set
     */
    public void setPropertyClassificationMaster(
            List<PropertyClassificationMaster> propertyClassificationMaster) {
        this.propertyClassificationMaster = propertyClassificationMaster;
    }

}
