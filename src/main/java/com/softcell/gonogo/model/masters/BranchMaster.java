package com.softcell.gonogo.model.masters;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by bhuvneshk on 2/8/17.
 */
@Document(collection = "branchMaster")
public class BranchMaster extends AuditEntity {

    private String branchId;

    private String branchName;

    private String stateId;

    private String stateName;

    private String institutionId;

    private boolean active;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BranchMaster{");
        sb.append("branchId='").append(branchId).append('\'');
        sb.append(", branchName='").append(branchName).append('\'');
        sb.append(", stateId='").append(stateId).append('\'');
        sb.append(", stateName='").append(stateName).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }
}
