package com.softcell.gonogo.model.masters;

import java.util.Date;

/**
 * @author Amit Surve
 */
public class CDModelSpoolMaster {
    private String blockId;
    private String blockDesc;
    private String catgDesc;
    private String manufacturerDesc;
    private String modelId;
    private String modelNo;
    private String classOfVehicle;
    private String typeOfBody;
    private String typeOfVehicle;
    private String make;
    private String cylinderNo;
    private String horsePower;
    private String cubicCapacity;
    private String wheelBase;
    private String seatingCapacity;
    private String fuelUsed;
    private String weightUnloaded;
    private String tyresFrontAxle;
    private String tyresRearAxle;
    private String tyresAnyOtherAxle;
    private String tyresTandenAxle;
    private String vehicleWeightCertified;
    private String vehicleWeightRegd;
    private String vehicleWeightFrontAxle;
    private String vehicleWeightRearAxle;
    private String vehicleWeightAnyOtherAxle;
    private String vehicleWeightTandenAxle;
    private String overallLength;
    private String overallWidth;
    private String overallHeight;
    private String overallHaug;
    private String mcStatus;
    private String makerId;
    private String makeDate;
    private String authId;
    private String authDate;
    private String manufacturerId;
    private String ltv;
    private String varience;
    private String bodyFunding;
    private String bodyFundingPer;
    private String chassisFunding;
    private String chassisFundingPer;
    private String maxFunding;
    private String nltv;
    private String makeModelFlag;
    private String lmmLcmCatgid;
    private String modelAsset;
    private String lmStartDate;
    private String lmEndDate;
    private String modelCategory;
    private String institutionId;
    private Date insertDate = new Date();
    private boolean active;


    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getBlockDesc() {
        return blockDesc;
    }

    public void setBlockDesc(String blockDesc) {
        this.blockDesc = blockDesc;
    }

    public String getCatgDesc() {
        return catgDesc;
    }

    public void setCatgDesc(String catgDesc) {
        this.catgDesc = catgDesc;
    }

    public String getManufacturerDesc() {
        return manufacturerDesc;
    }

    public void setManufacturerDesc(String manufacturerDesc) {
        this.manufacturerDesc = manufacturerDesc;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getClassOfVehicle() {
        return classOfVehicle;
    }

    public void setClassOfVehicle(String classOfVehicle) {
        this.classOfVehicle = classOfVehicle;
    }

    public String getTypeOfBody() {
        return typeOfBody;
    }

    public void setTypeOfBody(String typeOfBody) {
        this.typeOfBody = typeOfBody;
    }

    public String getTypeOfVehicle() {
        return typeOfVehicle;
    }

    public void setTypeOfVehicle(String typeOfVehicle) {
        this.typeOfVehicle = typeOfVehicle;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getCylinderNo() {
        return cylinderNo;
    }

    public void setCylinderNo(String cylinderNo) {
        this.cylinderNo = cylinderNo;
    }

    public String getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(String horsePower) {
        this.horsePower = horsePower;
    }

    public String getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(String cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getWheelBase() {
        return wheelBase;
    }

    public void setWheelBase(String wheelBase) {
        this.wheelBase = wheelBase;
    }

    public String getSeatingCapacity() {
        return seatingCapacity;
    }

    public void setSeatingCapacity(String seatingCapacity) {
        this.seatingCapacity = seatingCapacity;
    }

    public String getFuelUsed() {
        return fuelUsed;
    }

    public void setFuelUsed(String fuelUsed) {
        this.fuelUsed = fuelUsed;
    }

    public String getWeightUnloaded() {
        return weightUnloaded;
    }

    public void setWeightUnloaded(String weightUnloaded) {
        this.weightUnloaded = weightUnloaded;
    }

    public String getTyresFrontAxle() {
        return tyresFrontAxle;
    }

    public void setTyresFrontAxle(String tyresFrontAxle) {
        this.tyresFrontAxle = tyresFrontAxle;
    }

    public String getTyresRearAxle() {
        return tyresRearAxle;
    }

    public void setTyresRearAxle(String tyresRearAxle) {
        this.tyresRearAxle = tyresRearAxle;
    }

    public String getTyresAnyOtherAxle() {
        return tyresAnyOtherAxle;
    }

    public void setTyresAnyOtherAxle(String tyresAnyOtherAxle) {
        this.tyresAnyOtherAxle = tyresAnyOtherAxle;
    }

    public String getTyresTandenAxle() {
        return tyresTandenAxle;
    }

    public void setTyresTandenAxle(String tyresTandenAxle) {
        this.tyresTandenAxle = tyresTandenAxle;
    }

    public String getVehicleWeightCertified() {
        return vehicleWeightCertified;
    }

    public void setVehicleWeightCertified(String vehicleWeightCertified) {
        this.vehicleWeightCertified = vehicleWeightCertified;
    }

    public String getVehicleWeightRegd() {
        return vehicleWeightRegd;
    }

    public void setVehicleWeightRegd(String vehicleWeightRegd) {
        this.vehicleWeightRegd = vehicleWeightRegd;
    }

    public String getVehicleWeightFrontAxle() {
        return vehicleWeightFrontAxle;
    }

    public void setVehicleWeightFrontAxle(String vehicleWeightFrontAxle) {
        this.vehicleWeightFrontAxle = vehicleWeightFrontAxle;
    }

    public String getVehicleWeightRearAxle() {
        return vehicleWeightRearAxle;
    }

    public void setVehicleWeightRearAxle(String vehicleWeightRearAxle) {
        this.vehicleWeightRearAxle = vehicleWeightRearAxle;
    }

    public String getVehicleWeightAnyOtherAxle() {
        return vehicleWeightAnyOtherAxle;
    }

    public void setVehicleWeightAnyOtherAxle(String vehicleWeightAnyOtherAxle) {
        this.vehicleWeightAnyOtherAxle = vehicleWeightAnyOtherAxle;
    }

    public String getVehicleWeightTandenAxle() {
        return vehicleWeightTandenAxle;
    }

    public void setVehicleWeightTandenAxle(String vehicleWeightTandenAxle) {
        this.vehicleWeightTandenAxle = vehicleWeightTandenAxle;
    }

    public String getOverallLength() {
        return overallLength;
    }

    public void setOverallLength(String overallLength) {
        this.overallLength = overallLength;
    }

    public String getOverallWidth() {
        return overallWidth;
    }

    public void setOverallWidth(String overallWidth) {
        this.overallWidth = overallWidth;
    }

    public String getOverallHeight() {
        return overallHeight;
    }

    public void setOverallHeight(String overallHeight) {
        this.overallHeight = overallHeight;
    }

    public String getOverallHaug() {
        return overallHaug;
    }

    public void setOverallHaug(String overallHaug) {
        this.overallHaug = overallHaug;
    }

    public String getMcStatus() {
        return mcStatus;
    }

    public void setMcStatus(String mcStatus) {
        this.mcStatus = mcStatus;
    }

    public String getMakerId() {
        return makerId;
    }

    public void setMakerId(String makerId) {
        this.makerId = makerId;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getLtv() {
        return ltv;
    }

    public void setLtv(String ltv) {
        this.ltv = ltv;
    }

    public String getVarience() {
        return varience;
    }

    public void setVarience(String varience) {
        this.varience = varience;
    }

    public String getBodyFunding() {
        return bodyFunding;
    }

    public void setBodyFunding(String bodyFunding) {
        this.bodyFunding = bodyFunding;
    }

    public String getBodyFundingPer() {
        return bodyFundingPer;
    }

    public void setBodyFundingPer(String bodyFundingPer) {
        this.bodyFundingPer = bodyFundingPer;
    }

    public String getChassisFunding() {
        return chassisFunding;
    }

    public void setChassisFunding(String chassisFunding) {
        this.chassisFunding = chassisFunding;
    }

    public String getChassisFundingPer() {
        return chassisFundingPer;
    }

    public void setChassisFundingPer(String chassisFundingPer) {
        this.chassisFundingPer = chassisFundingPer;
    }

    public String getMaxFunding() {
        return maxFunding;
    }

    public void setMaxFunding(String maxFunding) {
        this.maxFunding = maxFunding;
    }

    public String getNltv() {
        return nltv;
    }

    public void setNltv(String nltv) {
        this.nltv = nltv;
    }

    public String getMakeModelFlag() {
        return makeModelFlag;
    }

    public void setMakeModelFlag(String makeModelFlag) {
        this.makeModelFlag = makeModelFlag;
    }

    public String getLmmLcmCatgid() {
        return lmmLcmCatgid;
    }

    public void setLmmLcmCatgid(String lmmLcmCatgid) {
        this.lmmLcmCatgid = lmmLcmCatgid;
    }

    public String getModelAsset() {
        return modelAsset;
    }

    public void setModelAsset(String modelAsset) {
        this.modelAsset = modelAsset;
    }

    public String getLmStartDate() {
        return lmStartDate;
    }

    public void setLmStartDate(String lmStartDate) {
        this.lmStartDate = lmStartDate;
    }

    public String getLmEndDate() {
        return lmEndDate;
    }

    public void setLmEndDate(String lmEndDate) {
        this.lmEndDate = lmEndDate;
    }

    public String getModelCategory() {
        return modelCategory;
    }

    public void setModelCategory(String modelCategory) {
        this.modelCategory = modelCategory;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "CDModelSpoolMaster [blockId=" + blockId + ", blockDesc="
                + blockDesc + ", catgDesc=" + catgDesc + ", manufacturerDesc="
                + manufacturerDesc + ", modelId=" + modelId + ", modelNo="
                + modelNo + ", classOfVehicle=" + classOfVehicle
                + ", typeOfBody=" + typeOfBody + ", typeOfVehicle="
                + typeOfVehicle + ", make=" + make + ", cylinderNo="
                + cylinderNo + ", horsePower=" + horsePower
                + ", cubicCapacity=" + cubicCapacity + ", wheelBase="
                + wheelBase + ", seatingCapacity=" + seatingCapacity
                + ", fuelUsed=" + fuelUsed + ", weightUnloaded="
                + weightUnloaded + ", tyresFrontAxle=" + tyresFrontAxle
                + ", tyresRearAxle=" + tyresRearAxle + ", tyresAnyOtherAxle="
                + tyresAnyOtherAxle + ", tyresTandenAxle=" + tyresTandenAxle
                + ", vehicleWeightCertified=" + vehicleWeightCertified
                + ", vehicleWeightRegd=" + vehicleWeightRegd
                + ", vehicleWeightFrontAxle=" + vehicleWeightFrontAxle
                + ", vehicleWeightRearAxle=" + vehicleWeightRearAxle
                + ", vehicleWeightAnyOtherAxle=" + vehicleWeightAnyOtherAxle
                + ", vehicleWeightTandenAxle=" + vehicleWeightTandenAxle
                + ", overallLength=" + overallLength + ", overallWidth="
                + overallWidth + ", overallHeight=" + overallHeight
                + ", overallHaug=" + overallHaug + ", mcStatus=" + mcStatus
                + ", makerId=" + makerId + ", makeDate=" + makeDate
                + ", authId=" + authId + ", authDate=" + authDate
                + ", manufacturerId=" + manufacturerId + ", ltv=" + ltv
                + ", varience=" + varience + ", bodyFunding=" + bodyFunding
                + ", bodyFundingPer=" + bodyFundingPer + ", chassisFunding="
                + chassisFunding + ", chassisFundingPer=" + chassisFundingPer
                + ", maxFunding=" + maxFunding + ", nltv=" + nltv
                + ", makeModelFlag=" + makeModelFlag + ", lmmLcmCatgid="
                + lmmLcmCatgid + ", modelAsset=" + modelAsset
                + ", lmStartDate=" + lmStartDate + ", lmEndDate=" + lmEndDate
                + ", modelCategory=" + modelCategory + ", institutionId="
                + institutionId + ", insertDate=" + insertDate + ", active="
                + active + "]";
    }

}