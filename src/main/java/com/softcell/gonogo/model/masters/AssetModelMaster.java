/**
 * kishorp4:24:41 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author kishorp</br>
 *
 *         <pre>
 * 		<em>AssetModelMaster</em>
 * </pre>
 *         <p>
 *         Use for store metadata of AssetModel Which is used DO generation.
 *         </p>
 */
@Document(collection = "assetModelMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssetModelMaster extends AuditEntity {

    @JsonProperty("sBlockID")
    private String blockId;

    @JsonProperty("sBlkDsc")
    private String blockDesc;

    @JsonProperty("sMdlID")
    private String modelId;

    @JsonProperty("sMdlNo")
    private String modelNo;

    @JsonProperty("sMake")
    private String make;

    @JsonProperty("sMfrID")
    private String manufacturerId;

    @JsonProperty("sMfrDscr")
    private String manufacturerDesc;

    @JsonProperty("sLmmLcmCatID")
    private String lmmLcmCatgId;

    @JsonProperty("sCatDsc")
    private String catgDesc;

    @JsonProperty("sMkMdlFl")
    private String makeModelFlag;

    @JsonProperty("dWghtUnld")
    private double weightUnloaded;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sProdFlg")
    private String productFlag;

    @JsonIgnore
    private Date insertDate = new Date();
    @JsonIgnore
    private boolean active = true;


    public double getWeightUnloaded() {
        return weightUnloaded;
    }

    public void setWeightUnloaded(double weightUnloaded) {
        this.weightUnloaded = weightUnloaded;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getBlockDesc() {
        return blockDesc;
    }

    public void setBlockDesc(String blockDesc) {
        this.blockDesc = blockDesc;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(String manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerDesc() {
        return manufacturerDesc;
    }

    public void setManufacturerDesc(String manufacturerDesc) {
        this.manufacturerDesc = manufacturerDesc;
    }

    public String getLmmLcmCatgId() {
        return lmmLcmCatgId;
    }

    public void setLmmLcmCatgId(String lmmLcmCatgId) {
        this.lmmLcmCatgId = lmmLcmCatgId;
    }

    public String getCatgDesc() {
        return catgDesc;
    }

    public void setCatgDesc(String catgDesc) {
        this.catgDesc = catgDesc;
    }

    public String getMakeModelFlag() {
        return makeModelFlag;
    }

    public void setMakeModelFlag(String makeModelFlag) {
        this.makeModelFlag = makeModelFlag;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AssetModelMaster{");
        sb.append("active=").append(active);
        sb.append(", blockId='").append(blockId).append('\'');
        sb.append(", blockDesc='").append(blockDesc).append('\'');
        sb.append(", modelId='").append(modelId).append('\'');
        sb.append(", modelNo='").append(modelNo).append('\'');
        sb.append(", make='").append(make).append('\'');
        sb.append(", manufacturerId='").append(manufacturerId).append('\'');
        sb.append(", manufacturerDesc='").append(manufacturerDesc).append('\'');
        sb.append(", lmmLcmCatgId='").append(lmmLcmCatgId).append('\'');
        sb.append(", catgDesc='").append(catgDesc).append('\'');
        sb.append(", makeModelFlag='").append(makeModelFlag).append('\'');
        sb.append(", weightUnloaded=").append(weightUnloaded);
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append(", insertDate=").append(insertDate);
        sb.append('}');
        return sb.toString();
    }
}
