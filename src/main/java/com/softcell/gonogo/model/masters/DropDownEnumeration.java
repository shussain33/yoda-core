/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 4, 2016 9:25:52 PM
 */
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * @author kishorp
 *
 */
public class DropDownEnumeration {
    /**
     * Name of drop down
     */
    @JsonProperty("sEnumName")
    private String enumName;
    /**
     * Possible unique values
     */
    @JsonProperty("aEnumVal")
    private Set<String> enumValues;

    /**
     * @return the enumName
     */
    public String getEnumName() {
        return enumName;
    }

    /**
     * @param enumName the enumName to set
     */
    public void setEnumName(String enumName) {
        this.enumName = enumName;
    }

    /**
     * @return the enumValues
     */
    public Set<String> getEnumValues() {
        return enumValues;
    }

    /**
     * @param enumValues the enumValues to set
     */
    public void setEnumValues(Set<String> enumValues) {
        this.enumValues = enumValues;
    }


}
