package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by anupamad on 11/10/17.
 */
@Document(collection = "manufacturerMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ManufacturerMaster extends AuditEntity {
    //MFR_CODE
    @JsonProperty("sMfrCode")
    private String mfrCode;

    //MFR_DESC
    @JsonProperty("sMfrDesc")
    private String mfrDesc;

    //MFR_NAME
    @JsonProperty("sMfrName")
    private String mfrName;

    //ADDRESS
    @JsonProperty("sAddress")
    private String address;

    //GST_NO
    @JsonProperty("sGstNo")
    private String gstNo;

    //SAP_CODE
    @JsonProperty("sSapCode")
    private String sapCode;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("bActive")
    private boolean active = true;

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getMfrDesc() { return mfrDesc; }

    public void setMfrDesc(String mfrDesc) { this.mfrDesc = mfrDesc; }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getSapCode() {
        return sapCode;
    }

    public void setSapCode(String sapCode) {
        this.sapCode = sapCode;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }
}
