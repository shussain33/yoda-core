package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.security.v2.Branch;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by amit on 30/5/18.
 */
@Document(collection = "organizationalHierarchyMasterV2")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationalHierarchyMasterV2 {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sProdName")
    private String product;

    @JsonProperty("sChannel")
    private String channel;

    @JsonProperty("oBranch")
    private Branch branch;

    @JsonProperty("oRoleUserMap")
    public List<RoleWiseUsers> roleWiseUsersList;

    @JsonProperty("bActive")
    private boolean active = true;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

