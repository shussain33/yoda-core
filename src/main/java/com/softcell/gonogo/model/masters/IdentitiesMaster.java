/**
 * vinodk5:01:58 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "IdentitiesMaster")
public class IdentitiesMaster extends AuditEntity {

    @JsonProperty("sIdTypeCode")
    private String idTypeCode;

    @JsonProperty("sIdTypeDesc")
    private String idTypeDescription;

    @JsonProperty("sChkExp")
    private String checkExpiry;

    @JsonProperty("sIssuingAuthority")
    private String issuingAuthority;

    @JsonProperty("sPurpose")
    private String purpose;

    /**
     * @return the idTypeCode
     */
    public String getIdTypeCode() {
        return idTypeCode;
    }

    /**
     * @param idTypeCode
     *            the idTypeCode to set
     */
    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    /**
     * @return the idTypeDescription
     */
    public String getIdTypeDescription() {
        return idTypeDescription;
    }

    /**
     * @param idTypeDescription
     *            the idTypeDescription to set
     */
    public void setIdTypeDescription(String idTypeDescription) {
        this.idTypeDescription = idTypeDescription;
    }

    /**
     * @return the checkExpiry
     */
    public String getCheckExpiry() {
        return checkExpiry;
    }

    /**
     * @param checkExpiry
     *            the checkExpiry to set
     */
    public void setCheckExpiry(String checkExpiry) {
        this.checkExpiry = checkExpiry;
    }

    /**
     * @return the issuingAuthority
     */
    public String getIssuingAuthority() {
        return issuingAuthority;
    }

    /**
     * @param issuingAuthority
     *            the issuingAuthority to set
     */
    public void setIssuingAuthority(String issuingAuthority) {
        this.issuingAuthority = issuingAuthority;
    }

    /**
     * @return the purpose
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * @param purpose
     *            the purpose to set
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IdentitiesMaster [idTypeCode=" + idTypeCode
                + ", idTypeDescription=" + idTypeDescription + ", checkExpiry="
                + checkExpiry + ", issuingAuthority=" + issuingAuthority
                + ", purpose=" + purpose + "]";
    }
}
