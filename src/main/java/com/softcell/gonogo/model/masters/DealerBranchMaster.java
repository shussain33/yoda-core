package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

/**
 * It is LOS related master.
 * It is use to get branch id based on dealer and product id return broker id else null.
 *
 * @author bhuvneshk
 */


@Document(collection = "dealerBranchMaster")
public class DealerBranchMaster extends AuditEntity {

    @JsonProperty("sDlrId")
    private String dealerId;

    @JsonProperty("sProdId")
    private String productId;

    @JsonProperty("sBranchId")
    private String branchId;

    @JsonProperty("sInstID")
    private String institutionId;

    /**
     * This flag is to push data to los based on value,
     * true to push data else no operation.
     */
    @JsonProperty("sDealerFlag")
    private Boolean dealerFlag;

    /**
     * This field added for sales report purpose.
     */
    @JsonProperty("sBranchName")
    private String branchName;

    @JsonProperty("bIsSkipSerialOrImeiValidation")
    private boolean skipSerialOrImeiValidation;

    @JsonProperty("aLoyaltyCards")
    private Set<String> loyaltyCards;

    @JsonIgnore
    private boolean active;

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Boolean getDealerFlag() {
        return dealerFlag;
    }

    public void setDealerFlag(Boolean dealerFlag) {
        this.dealerFlag = dealerFlag;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public boolean isSkipSerialOrImeiValidation() {
        return skipSerialOrImeiValidation;
    }

    public void setSkipSerialOrImeiValidation(boolean skipSerialOrImeiValidation) {
        this.skipSerialOrImeiValidation = skipSerialOrImeiValidation;
    }

    public Set<String> getLoyaltyCards() {
        return loyaltyCards;
    }

    public void setLoyaltyCards(Set<String> loyaltyCards) {
        this.loyaltyCards = loyaltyCards;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DealerBranchMaster{");
        sb.append("dealerId='").append(dealerId).append('\'');
        sb.append(", productId='").append(productId).append('\'');
        sb.append(", branchId='").append(branchId).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", dealerFlag=").append(dealerFlag);
        sb.append(", branchName='").append(branchName).append('\'');
        sb.append(", skipSerialOrImeiValidation=").append(skipSerialOrImeiValidation);
        sb.append(", loyaltyCards=").append(loyaltyCards);
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DealerBranchMaster that = (DealerBranchMaster) o;

        if (skipSerialOrImeiValidation != that.skipSerialOrImeiValidation) return false;
        if (active != that.active) return false;
        if (dealerId != null ? !dealerId.equals(that.dealerId) : that.dealerId != null) return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (branchId != null ? !branchId.equals(that.branchId) : that.branchId != null) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        if (dealerFlag != null ? !dealerFlag.equals(that.dealerFlag) : that.dealerFlag != null) return false;
        if (branchName != null ? !branchName.equals(that.branchName) : that.branchName != null) return false;
        return !(loyaltyCards != null ? !loyaltyCards.equals(that.loyaltyCards) : that.loyaltyCards != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (dealerId != null ? dealerId.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (branchId != null ? branchId.hashCode() : 0);
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (dealerFlag != null ? dealerFlag.hashCode() : 0);
        result = 31 * result + (branchName != null ? branchName.hashCode() : 0);
        result = 31 * result + (skipSerialOrImeiValidation ? 1 : 0);
        result = 31 * result + (loyaltyCards != null ? loyaltyCards.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }
}
