package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 24/5/17.
 */
@Document(collection = "eWPremiumMaster")
public class EWPremiumMaster {

    @JsonProperty("sEWAssetCategory")
    private String ewAssetCategory;

    @JsonProperty("dMinAssetValue")
    private double minAssetValue;

    @JsonProperty("dMaxAssetValue")
    private double  maxAssetValue;

    @JsonProperty("dOneYearPremium")
    private double oneYearPremium;

    @JsonProperty("dTwoYearPremium")
    private double twoYearPremium;

    @JsonProperty("dThreeYearPremium")
    private double threeYearPremium;

    @JsonProperty("sProductFlag")
    private String productFlag;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonIgnore
    private boolean active=true;

    @JsonProperty("dtInsertDate")
    private Date insertDate =new Date();

    public String getEwAssetCategory() {
        return ewAssetCategory;
    }

    public void setEwAssetCategory(String ewAssetCategory) {
        this.ewAssetCategory = ewAssetCategory;
    }

    public double getMinAssetValue() {
        return minAssetValue;
    }

    public void setMinAssetValue(double minAssetValue) {
        this.minAssetValue = minAssetValue;
    }

    public double getMaxAssetValue() {
        return maxAssetValue;
    }

    public void setMaxAssetValue(double maxAssetValue) {
        this.maxAssetValue = maxAssetValue;
    }

    public double getOneYearPremium() {
        return oneYearPremium;
    }

    public void setOneYearPremium(double oneYearPremium) {
        this.oneYearPremium = oneYearPremium;
    }

    public double getTwoYearPremium() {
        return twoYearPremium;
    }

    public void setTwoYearPremium(double twoYearPremium) {
        this.twoYearPremium = twoYearPremium;
    }

    public double getThreeYearPremium() {
        return threeYearPremium;
    }

    public void setThreeYearPremium(double threeYearPremium) {
        this.threeYearPremium = threeYearPremium;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EWPremiumMaster{");
        sb.append("ewAssetCategory='").append(ewAssetCategory).append('\'');
        sb.append(", minAssetValue=").append(minAssetValue);
        sb.append(", maxAssetValue=").append(maxAssetValue);
        sb.append(", oneYearPremium=").append(oneYearPremium);
        sb.append(", twoYearPremium=").append(twoYearPremium);
        sb.append(", threeYearPremium=").append(threeYearPremium);
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", active=").append(active);
        sb.append(", insertDate=").append(insertDate);
        sb.append('}');
        return sb.toString();
    }
}
