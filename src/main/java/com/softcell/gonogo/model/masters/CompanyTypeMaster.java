/**
 * vinodk6:11:12 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "CompanyTypeMaster")
public class CompanyTypeMaster extends AuditEntity {

    @JsonProperty("sCompTypeDesc")
    private String companyTypeDescription;

    /**
     * @return the companyTypeDescription
     */
    public String getCompanyTypeDescription() {
        return companyTypeDescription;
    }

    /**
     * @param companyTypeDescription
     *            the companyTypeDescription to set
     */
    public void setCompanyTypeDescription(String companyTypeDescription) {
        this.companyTypeDescription = companyTypeDescription;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CompanyTypeMaster [companyTypeDescription="
                + companyTypeDescription + "]";
    }
}
