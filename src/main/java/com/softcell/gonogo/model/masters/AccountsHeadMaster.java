package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.master.*;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by abhishek on 7/9/17.
 */

@Document(collection = "accountsHeadMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountsHeadMaster extends AuditEntity {

    @JsonProperty("sTranAccountHead")
    private String tranAccountHead;

    @JsonProperty("sTranAccDesc")
    private String tranAccDesc;

    @JsonProperty("sTvsmAccountCode")
    private String tvsmAccountCode;

    @JsonProperty("eGlType")
    private GlType glType;

    @JsonProperty("sPostingLink")
    private String postingLink;

    @JsonProperty("sPostingLinkCode")
    private String postingLinkCode;

    @JsonProperty("sSpecialGlIndicator")
    private String specialGlIndicator;

    @JsonProperty("sPostingKeyDb")
    private String postingKeyDb;

    @JsonProperty("sPostingKeyCr")
    private String postingKeyCr;

    @JsonProperty("eTaxFlag")
    private TaxFlag taxFlag;

    @JsonProperty("sTaxAccCode")
    private String taxAccCode;

    @JsonProperty("sTaxtype")
    private String taxType;

    @JsonProperty("ePandlFlag")
    private PandlFlag pandlFlag;

    @JsonProperty("eBusinessArea")
    private BusinessArea businessArea;

    @JsonProperty("eCostCenter")
    private CostCenter costCenter;

    @JsonProperty("sCustomer")
    private String customer;

    @JsonProperty("eProduct")
    private Product product;

    @JsonProperty("eBranchCode")
    private BranchCode branchCode;

    @JsonProperty("sPaymentMode")
    private String paymentMode;

    @JsonProperty("eCactive")
    private Cactive cactive;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active = true;

    public String getTranAccountHead() {
        return tranAccountHead;
    }

    public void setTranAccountHead(String tranAccountHead) {
        this.tranAccountHead = tranAccountHead;
    }

    public String getTranAccDesc() {
        return tranAccDesc;
    }

    public void setTranAccDesc(String tranAccDesc) {
        this.tranAccDesc = tranAccDesc;
    }

    public String getTvsmAccountCode() {
        return tvsmAccountCode;
    }

    public void setTvsmAccountCode(String tvsmAccountCode) {
        this.tvsmAccountCode = tvsmAccountCode;
    }

    public String getGlType() {
        return glType.name();
    }

    public void setGlType(String glType) {this.glType = GlType.valueOf(glType); }

    public String getPostingLink() {
        return postingLink;
    }

    public void setPostingLink(String postingLink) {
        this.postingLink = postingLink;
    }

    public String getPostingLinkCode() {
        return postingLinkCode;
    }

    public void setPostingLinkCode(String postingLinkCode) {
        this.postingLinkCode = postingLinkCode;
    }

    public String getSpecialGlIndicator() {
        return specialGlIndicator;
    }

    public void setSpecialGlIndicator(String specialGlIndicator) {
        this.specialGlIndicator = specialGlIndicator;
    }

    public String getPostingKeyDb() {
        return postingKeyDb;
    }

    public void setPostingKeyDb(String postingKeyDb) {
        this.postingKeyDb = postingKeyDb;
    }

    public String getPostingKeyCr() {
        return postingKeyCr;
    }

    public void setPostingKeyCr(String postingKeyCr) {
        this.postingKeyCr = postingKeyCr;
    }

    public String getTaxFlag() {
        return taxFlag.name();
    }

    public void setTaxFlag(String taxFlag) {
        this.taxFlag = TaxFlag.valueOf(taxFlag);
    }

    public String getTaxAccCode() {
        return taxAccCode;
    }

    public void setTaxAccCode(String taxAccCode) {
        this.taxAccCode = taxAccCode;
    }

    public String getTaxType() {
        return taxType;
    }

    public void setTaxType(String taxType) {
        this.taxType = taxType;
    }

    public String getPandlFlag() {
        return pandlFlag.name();
    }

    public void setPandlFlag(String pandlFlag) { this.pandlFlag = PandlFlag.valueOf(pandlFlag);}

    public String getBusinessArea() {
        return businessArea.name();
    }

    public void setBusinessArea(String businessArea) { this.businessArea = BusinessArea.valueOf(businessArea);}

    public String getCostCenter() {
        return costCenter.name();
    }

    public void setCostCenter(String costCenter) { this.costCenter = CostCenter.valueOf(costCenter); }

    public String getCustomer() { return customer; }

    public void setCustomer(String customer) { this.customer = customer; }

    public String getProduct() { return product.name(); }

    public void setProduct(String product) { this.product = Product.valueOf(product); }

    public String getBranchCode() {
        return branchCode.name();
    }

    public void setBranchCode(String branchCode) { this.branchCode = BranchCode.valueOf(branchCode); }

    public String getPaymentMode() { return paymentMode; }

    public void setPaymentMode(String paymentMode) {
        if(paymentMode != null && !paymentMode.isEmpty()) {
            this.paymentMode = PaymentMode.valueOf(paymentMode).name();
        }
    }

    public String getCactive() {
        return cactive.name();
    }

    public void setCactive(String cactive) { this.cactive = Cactive.valueOf(cactive); }

    public String getInstitutionId() { return institutionId; }

    public void setInstitutionId(String institutionId) { this.institutionId = institutionId; }

    public boolean isActive() { return active; }

    public void setActive(boolean active) { this.active = active; }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AccountHeadMaster [tranAccountHead=");
        builder.append(tranAccountHead);
        builder.append(", tranAccDesc=");
        builder.append(tranAccDesc);
        builder.append(", tvsmAccountCode=");
        builder.append(tvsmAccountCode);
        builder.append(", glType=");
        builder.append(glType);
        builder.append(", postingLink=");
        builder.append(postingLink);
        builder.append(", postingLinkCode=");
        builder.append(postingLinkCode);
        builder.append(", specialGlIndicator=");
        builder.append(specialGlIndicator);
        builder.append(", postingKeyDb=");
        builder.append(postingKeyDb);
        builder.append(", postingKeyCr=");
        builder.append(postingKeyCr);
        builder.append(", taxFlag=");
        builder.append(taxFlag);
        builder.append(", taxAccCode=");
        builder.append(taxAccCode);
        builder.append(", taxType=");
        builder.append(taxType);
        builder.append(", pandlFlag=");
        builder.append(pandlFlag);
        builder.append(", businessArea=");
        builder.append(businessArea);
        builder.append(", costCenter=");
        builder.append(costCenter);
        builder.append(", customer=");
        builder.append(customer);
        builder.append(", product=");
        builder.append(product);
        builder.append(", branchCode=");
        builder.append(branchCode);
        builder.append(", cactive=");
        builder.append(cactive);
        builder.append(", paymentMode=");
        builder.append(paymentMode);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(",active=");
        builder.append(active);
        builder.append("]");

        return builder.toString();
    }
}
