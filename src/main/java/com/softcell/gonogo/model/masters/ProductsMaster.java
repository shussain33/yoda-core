/**
 * kishorp11:32:46 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;


import com.fasterxml.jackson.annotation.*;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "productCode",
        "productDescr",
        "mortgageRequired"
})
/**
 *
 * @author kishorp
 *
 */
@Document(collection = "ProductsMaster")
public class ProductsMaster extends AuditEntity {

    @JsonProperty("productCode")
    private String productCode;
    @JsonProperty("productDescr")
    private String productDescr;
    @JsonProperty("mortgageRequired")
    private String mortgageRequired;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The productCode
     */
    @JsonProperty("productCode")
    public String getProductCode() {
        return productCode;
    }

    /**
     *
     * @param productCode
     * The productCode
     */
    @JsonProperty("productCode")
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     *
     * @return
     * The productDescr
     */
    @JsonProperty("productDescr")
    public String getProductDescr() {
        return productDescr;
    }

    /**
     *
     * @param productDescr
     * The productDescr
     */
    @JsonProperty("productDescr")
    public void setProductDescr(String productDescr) {
        this.productDescr = productDescr;
    }

    /**
     *
     * @return
     * The mortgageRequired
     */
    @JsonProperty("mortgageRequired")
    public String getMortgageRequired() {
        return mortgageRequired;
    }

    /**
     *
     * @param mortgageRequired
     * The mortgageRequired
     */
    @JsonProperty("mortgageRequired")
    public void setMortgageRequired(String mortgageRequired) {
        this.mortgageRequired = mortgageRequired;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
