package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by amit on 22/3/18.
 */
@Document(collection = "deviationMaster")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviationMaster {

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("oProd")
    private String product;

    @JsonProperty("sDevID")
    private String deviationId;

    @JsonProperty("sDev")
    private String deviation;

    @JsonProperty("sCategory")
    private String category;

    @JsonProperty("aAuthority")
    private List<String> authorities;

    @JsonProperty("sLevel")
    private String level;

    @JsonProperty("sMitigation")
    private String mitigation;

    @JsonProperty("sSeverity")
    private String severity;

    @JsonIgnore
    private boolean active = true;
}
