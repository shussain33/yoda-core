package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 28/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HierarchyNode {

    @JsonProperty("iLevel")
    private int level;
    @JsonProperty("sRole")
    private String role;
    @JsonProperty("sUserId")
    private String userId;
    @JsonProperty("sUserName")
    private String userName;

    public HierarchyNode(HierarchyNode hNode) {
        this.level = hNode.level;
        this.role = hNode.role;
        this.userId = hNode.userId;
        this.userName = hNode.userName;
    }
}
