package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * 
 * @author mahesh
 *
 */
@Document(collection = "negativeAreaGeoLimitMaster")
public class NegativeAreaGeoLimitMaster extends AuditEntity {

	@JsonProperty("sBranchCode")
	private String branchCode;

	@JsonProperty("sBranchName")
	private String branchName;

	@JsonProperty("sPinCode")
	private String pinCode;

	@JsonProperty("sCity")
	private String city;

	@JsonProperty("sArea")
	private String area;

	@JsonProperty("sAreaType")
	private String areaType;

	@JsonProperty("sReason")
	private String reason;

	@JsonProperty("sIsNegative")
	private String isNegative;

	@JsonProperty("sIsGeoAreaActive")
	private String isGeoAreaActive;

	@JsonProperty("sInstId")
	private String institutionId;

	@JsonProperty("sProductName")
	private String productName;

	@JsonProperty("dtInsrtDt")
	private Date insertDate = new Date();

	@JsonProperty("bActive")
	private boolean active;

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAreaType() {
		return areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getIsNegative() {
		return isNegative;
	}

	public void setIsNegative(String isNegative) {
		this.isNegative = isNegative;
	}

	public String getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(String institutionId) {
		this.institutionId = institutionId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIsGeoAreaActive() {
		return isGeoAreaActive;
	}

	public void setIsGeoAreaActive(String isGeoAreaActive) {
		this.isGeoAreaActive = isGeoAreaActive;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("NegativeAreaGeoLimitMaster{");
		sb.append("branchCode='").append(branchCode).append('\'');
		sb.append(", branchName='").append(branchName).append('\'');
		sb.append(", pinCode='").append(pinCode).append('\'');
		sb.append(", city='").append(city).append('\'');
		sb.append(", area='").append(area).append('\'');
		sb.append(", areaType='").append(areaType).append('\'');
		sb.append(", reason='").append(reason).append('\'');
		sb.append(", isNegative='").append(isNegative).append('\'');
		sb.append(", isGeoAreaActive='").append(isGeoAreaActive).append('\'');
		sb.append(", institutionId='").append(institutionId).append('\'');
		sb.append(", productName='").append(productName).append('\'');
		sb.append(", insertDate=").append(insertDate);
		sb.append(", active=").append(active);
		sb.append('}');
		return sb.toString();
	}

}
