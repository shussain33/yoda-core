package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * It is LOS related master.
 * It is use to get city id based on city desc master.
 *
 * @author bhuvneshk
 */


@Document(collection = "creditPromotionMaster")
public class CreditPromotionMaster extends AuditEntity {

    @JsonProperty("sSchemeId")
    private String schemeId;

    @JsonProperty("sPromotionId")
    private String promotionId;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active;

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CreditPromotionMaster [schemeId=");
        builder.append(schemeId);
        builder.append(", promotionId=");
        builder.append(promotionId);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }

}
