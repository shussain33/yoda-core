package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by bhuvneshk on 25/9/17.
 */
@Document(collection = "bankMaster")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BankMaster {

    @Id
    @JsonProperty("sId")
    private  String _id;

    @JsonProperty("sBankCode")
    private String bankCode;

    @JsonProperty("sMifinID")
    private String mifinBankcode;

    @JsonProperty("sMifinBankName")
    private String mifinBankName;

    @JsonProperty("sBankDesc")
    private String bankDescription;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("dtInsertDate")
    private Date insertDate = new Date();
    @JsonIgnore
    private boolean active = true;
}
