package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * It is LOS related master.
 * It is use to get state id based on state desc master.
 *
 * @author bhuvneshk
 */

@Document(collection = "stateMaster")
public class StateMaster extends AuditEntity {

    @JsonProperty("sStateId")
    private String stateId;

    @JsonProperty("sStateCode")
    private String stateCode;

    @JsonProperty("sStateDesc")
    private String stateDesc;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active = true;

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("StateMaster [stateId=");
        builder.append(stateId);
        builder.append(", stateDesc=");
        builder.append(stateDesc);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }
}
