/**
 * vinodk6:12:49 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "IndustryCodeMaster")
public class IndustryCodeMaster extends AuditEntity {

    @JsonProperty("sIndustryTypeDesc")
    private String industryTypeDescription;

    /**
     * @return the industryTypeDescription
     */
    public String getIndustryTypeDescription() {
        return industryTypeDescription;
    }

    /**
     * @param industryTypeDescription
     *            the industryTypeDescription to set
     */
    public void setIndustryTypeDescription(String industryTypeDescription) {
        this.industryTypeDescription = industryTypeDescription;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IndustryCodeMaster [industryTypeDescription="
                + industryTypeDescription + "]";
    }
}
