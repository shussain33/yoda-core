package com.softcell.gonogo.model.masters;

/**
 * Created by bhuvneshk on 28/9/17.
 */
public enum AssetModelMasterfields {

    Block_Desc("blockDesc"),
    Product_Flag("productFlag"),
    Model_ID("modelId"),
    Make("make"),
    Manufacturer_ID("manufacturerId"),
    Manufacturer_DESC("manufacturerDesc"),
    CATDESC("catgDesc"),
    Weight_Unloaded("weightUnloaded"),
    Model_No("modelNo");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    AssetModelMasterfields(String value) {
        this.value = value;
    }
}
