package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by anupamad on 15/9/17.
 */
@Document(collection = "stateBranchMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StateBranchMaster extends AuditEntity{
    @JsonProperty("sStateCode")
    private String stateCode;
    @JsonProperty("sSapStateCode")
    private String sapStateCode;
    @JsonProperty("sBranchCode")
    private String branchCode;
    @JsonIgnore
    private boolean active = true;
    @JsonProperty("sInstID")
    private String institutionId;

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getSapStateCode() {
        return sapStateCode;
    }

    public void setSapStateCode(String sapStateCode) {
        this.sapStateCode = sapStateCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }
}
