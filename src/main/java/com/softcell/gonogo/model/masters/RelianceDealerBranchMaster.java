package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by bhuvneshk on 28/2/17.
 */

@Document(collection="relianceDealerBranchMaster")
public class RelianceDealerBranchMaster extends AuditEntity {


    @JsonProperty("sDlrId")
    private String dealerId;

    @JsonProperty("sBrnchNm")
    private String branchName;

    @JsonProperty("sDlrNm")
    private String dealerName;;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active;

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RelianceDealerBranchMaster [dealerId=");
        builder.append(dealerId);
        builder.append(", branchName=");
        builder.append(branchName);
        builder.append(", dealerName=");
        builder.append(dealerName);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }

}

