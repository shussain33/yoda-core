/**
 * vinodk5:08:22 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "residenceTypeMaster")
public class ResidenceTypeMaster extends AuditEntity {

    @JsonProperty("sResTypeDesc")
    private String residenceTypeDescription;

    @JsonProperty("sMonthlyRentReq")
    private String monthlyRentRequired;

    /**
     * @return the residenceTypeDescription
     */
    public String getResidenceTypeDescription() {
        return residenceTypeDescription;
    }

    /**
     * @param residenceTypeDescription
     *            the residenceTypeDescription to set
     */
    public void setResidenceTypeDescription(String residenceTypeDescription) {
        this.residenceTypeDescription = residenceTypeDescription;
    }

    /**
     * @return the monthlyRentRequired
     */
    public String getMonthlyRentRequired() {
        return monthlyRentRequired;
    }

    /**
     * @param monthlyRentRequired
     *            the monthlyRentRequired to set
     */
    public void setMonthlyRentRequired(String monthlyRentRequired) {
        this.monthlyRentRequired = monthlyRentRequired;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ResidenceTypeMaster [residenceTypeDescription="
                + residenceTypeDescription + ", monthlyRentRequired="
                + monthlyRentRequired + "]";
    }
}
