package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author mahesh
 */
@Document(collection = "cityStateMappingMaster")
public class CityStateMappingMaster extends AuditEntity {

    @JsonProperty("sSrNo")
    private String srNo;

    @JsonProperty("sCityName")
    private String cityName;

    @JsonProperty("sStateID")
    private String stateId;

    @JsonProperty("sStateDesc")
    private String stateDesc;

    @JsonProperty("sCityId")
    private String cityId;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sProductName")
    private String productName;

    @JsonProperty("dtInsrtDt")
    private Date insertDate = new Date();

    @JsonProperty("bActive")
    private boolean active;

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CityStateMappingMaster [srNo=");
        builder.append(srNo);
        builder.append(", cityName=");
        builder.append(cityName);
        builder.append(", stateId=");
        builder.append(stateId);
        builder.append(", stateDesc=");
        builder.append(stateDesc);
        builder.append(", cityId=");
        builder.append(cityId);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", productName=");
        builder.append(productName);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append(", active=");
        builder.append(active);
        builder.append("]");
        return builder.toString();
    }

}
