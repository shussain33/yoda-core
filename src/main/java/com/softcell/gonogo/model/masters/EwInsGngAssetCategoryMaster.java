package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 24/5/17.
 *
 * This master are used to store the information regarding to Extended Warranty ,Insurance and gonogo Asset category and
 * the manufacturer warranty.
 */
@Document(collection = "ewInsGngAssetCategoryMaster")
public class EwInsGngAssetCategoryMaster {

    @JsonProperty("sInsAssetCat")
    private String insuranceAssetCat;

    @JsonProperty("sEWAssetCat")
    private String extendedWarrantyAssetCat;

    @JsonProperty("sGngAssetCat")
    private String gngAssetCat;

    @JsonProperty("dMnfcrWarranty")
    private double manufacturerWarranty;

    @JsonProperty("sProductFlag")
    private String productFlag;

    @JsonIgnore
    private boolean active = true;

    @JsonProperty("dtInsertDate")
    private Date insertDate =new Date();

    @JsonProperty("sInstitutionId")
    private String institutionId;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getInsuranceAssetCat() {
        return insuranceAssetCat;
    }

    public void setInsuranceAssetCat(String insuranceAssetCat) {
        this.insuranceAssetCat = insuranceAssetCat;
    }

    public String getExtendedWarrantyAssetCat() {
        return extendedWarrantyAssetCat;
    }

    public void setExtendedWarrantyAssetCat(String extendedWarrantyAssetCat) {
        this.extendedWarrantyAssetCat = extendedWarrantyAssetCat;
    }

    public String getGngAssetCat() {
        return gngAssetCat;
    }

    public void setGngAssetCat(String gngAssetCat) {
        this.gngAssetCat = gngAssetCat;
    }

    public double getManufacturerWarranty() {
        return manufacturerWarranty;
    }

    public void setManufacturerWarranty(double manufacturerWarranty) {
        this.manufacturerWarranty = manufacturerWarranty;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EwInsGngAssetCategoryMaster{");
        sb.append("insuranceAssetCat='").append(insuranceAssetCat).append('\'');
        sb.append(", extendedWarrantyAssetCat='").append(extendedWarrantyAssetCat).append('\'');
        sb.append(", gngAssetCat='").append(gngAssetCat).append('\'');
        sb.append(", manufacturerWarranty=").append(manufacturerWarranty);
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append(", active=").append(active);
        sb.append(", insertDate=").append(insertDate);
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
