/**
 * vinodk6:25:14 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "AssetsOwned")
public class AssetsOwned extends AuditEntity {

    @JsonProperty("sAssetsOwned")
    private String assetsOwned;

    /**
     * @return the assetsOwned
     */
    public String getAssetsOwned() {
        return assetsOwned;
    }

    /**
     * @param assetsOwned
     *            the assetsOwned to set
     */
    public void setAssetsOwned(String assetsOwned) {
        this.assetsOwned = assetsOwned;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AssetsOwned [assetsOwned=" + assetsOwned + "]";
    }
}
