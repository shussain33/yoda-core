package com.softcell.gonogo.model.masters;

/**
 * Created by priyanka on 16/9/17.
 */
public enum StateMasterfields {

    _id("stateId"),
    STATE_CODE("stateCode"),
    stateDesc("stateDesc");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    StateMasterfields(String value) {
        this.value = value;
    }
}
