/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 4, 2016 5:17:00 PM
 */
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author kishorp
 *
 */
public class ApplicationMetaData {
    @JsonProperty("aEnum")
    private List<DropDownEnumeration> enumeration;

    /**
     * @return the enumeration
     */
    public List<DropDownEnumeration> getEnumeration() {
        return enumeration;
    }

    /**
     * @param enumeration the enumeration to set
     */
    public void setEnumeration(List<DropDownEnumeration> enumeration) {
        this.enumeration = enumeration;
    }

}
