package com.softcell.gonogo.model.masters;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author bhuvneshk
 *         <pre>
 *         		<em>HierarchyMaster</em>
 *         </pre>
 *         <p>
 *         This is used for mongo collection which Stores the Hierarchy Master details for user based on institution and products .
 *         </p>
 */
@Document(collection = "HierarchyMaster")
public class HierarchyMaster extends AuditEntity {

    private List<String> type;
    private Map<String, List<String>> level;
    private Map<String, String> collection;

    private int institutionId;
    private Date insertDt;
    private boolean active;

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public Map<String, List<String>> getLevel() {
        return level;
    }

    public void setLevel(Map<String, List<String>> level) {
        this.level = level;
    }

    public Map<String, String> getCollection() {
        return collection;
    }

    public void setCollection(Map<String, String> collection) {
        this.collection = collection;
    }

    /**
     * @return the institutionId
     */
    public int getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(int institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the insertDt
     */
    public Date getInsertDt() {
        return insertDt;
    }

    /**
     * @param insertDt the insertDt to set
     */
    public void setInsertDt(Date insertDt) {
        this.insertDt = insertDt;
    }


    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "HierarchyMaster [type=" + type + ", level=" + level
                + ", collection=" + collection + ", institutionId="
                + institutionId + ", insertDt=" + insertDt + ", active="
                + active + "]";
    }


}
