package com.softcell.gonogo.model.masters;

/**
 * Created by bhuvneshk on 27/9/17.
 */
public enum BranchMasterFields {

    LSO_OFFICE_CODE_C("branchId"),
    LSO_OFFICE_NAME_C("branchName");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    BranchMasterFields(String value) {
        this.value = value;
    }
}
