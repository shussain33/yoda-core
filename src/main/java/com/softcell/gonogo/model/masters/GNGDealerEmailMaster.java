package com.softcell.gonogo.model.masters;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * This master keeps the data for cc and bcc emails.
 *
 * @author bhuvneshk
 */

@Document(collection = "GNGDealerEmailMaster")
public class GNGDealerEmailMaster extends AuditEntity {

    private String productId;
    private String groupId;
    private String institutionID;
    private String email;
    private String recipientType;
    private String region;
    private Date insertDate;
    private boolean active;

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return the groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the institutionID
     */
    public String getInstitutionID() {
        return institutionID;
    }

    /**
     * @param institutionID the institutionID to set
     */
    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the recipientType
     */
    public String getRecipientType() {
        return recipientType;
    }


    /**
     * @param recipientType the recipientType to set
     */
    public void setRecipientType(String recipientType) {
        this.recipientType = recipientType;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDt the insertDate to set
     */
    public void setInsertDate(Date insertDt) {
        this.insertDate = insertDt;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "GNGEmailMaster [productId=" + productId + ", groupId="
                + groupId + ", institutionID=" + institutionID + ", email="
                + email + ", active=" + active + ", recipientType="
                + recipientType + ", region=" + region + ", insertDt="
                + insertDate + "]";
    }

}
