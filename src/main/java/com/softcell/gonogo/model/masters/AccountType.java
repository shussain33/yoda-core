/**
 * vinodk5:14:38 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "accountType")
public class AccountType extends AuditEntity {

    @JsonProperty("sConst")
    private String constitution;

    @JsonProperty("sAccntType")
    private String accountType;

    /**
     * @return the constitution
     */
    public String getConstitution() {
        return constitution;
    }

    /**
     * @param constitution
     *            the constitution to set
     */
    public void setConstitution(String constitution) {
        this.constitution = constitution;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * @param accountType
     *            the accountType to set
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AccountType [constitution=" + constitution + ", accountType="
                + accountType + "]";
    }
}
