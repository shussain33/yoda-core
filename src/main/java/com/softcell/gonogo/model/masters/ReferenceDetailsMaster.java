package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by yogeshb on 24/4/17.
 */

@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReferenceDetailsMaster extends AuditEntity {

    @JsonIgnore
    private String institutionId;

    @JsonIgnore
    private boolean active = true;

    @JsonIgnore
    private String product;

    private Date dateTime;

    private String branchId;

    private String makerId;

    private String makerDate;

    private String authId;

    private String authDate;

    private String moduleId;

    private String key1;

    private String key2;

    private String value;

    @JsonProperty("sRelation")
    private String description;

    private String status;

    private String moduleFlag;

    private String appFlag;

    private String disableFlag;

    private String cgpStartDate;

    private String cgpEndDate;

    private String mApplyFlag;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getMakerId() {
        return makerId;
    }

    public void setMakerId(String makerId) {
        this.makerId = makerId;
    }

    public String getMakerDate() {
        return makerDate;
    }

    public void setMakerDate(String makerDate) {
        this.makerDate = makerDate;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModuleFlag() {
        return moduleFlag;
    }

    public void setModuleFlag(String moduleFlag) {
        this.moduleFlag = moduleFlag;
    }

    public String getAppFlag() {
        return appFlag;
    }

    public void setAppFlag(String appFlag) {
        this.appFlag = appFlag;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public String getCgpStartDate() {
        return cgpStartDate;
    }

    public void setCgpStartDate(String cgpStartDate) {
        this.cgpStartDate = cgpStartDate;
    }

    public String getCgpEndDate() {
        return cgpEndDate;
    }

    public void setCgpEndDate(String cgpEndDate) {
        this.cgpEndDate = cgpEndDate;
    }

    public String getmApplyFlag() {
        return mApplyFlag;
    }

    public void setmApplyFlag(String mApplyFlag) {
        this.mApplyFlag = mApplyFlag;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
