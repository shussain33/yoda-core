/**
 * vinodk6:09:35 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "LoanPurposeMaster")
public class LoanPurposeMaster extends AuditEntity {

    @JsonProperty("sLoanPurposeDesc")
    private String loanPurposeDescription;

    /**
     * @return the loanPurposeDescription
     */
    public String getLoanPurposeDescription() {
        return loanPurposeDescription;
    }

    /**
     * @param loanPurposeDescription
     *            the loanPurposeDescription to set
     */
    public void setLoanPurposeDescription(String loanPurposeDescription) {
        this.loanPurposeDescription = loanPurposeDescription;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LoanPurposeMaster [loanPurposeDescription="
                + loanPurposeDescription + "]";
    }
}
