package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by sampat on 22/12/17.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "paynimoBankMaster")
public class PaynimoBankMaster {

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sPhoneToAccount")
    private String phoneToAccount;

    @JsonProperty("sBankType")
    private String bankType;

    @JsonProperty("sApiHit")
    private String apiHit;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("dtInsertDate")
    private Date insertDate = new Date();

    @JsonIgnore
    private boolean active = true;
}
