package com.softcell.gonogo.model.masters;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


/**
 * @author bhuvneshk
 *         <pre>
 *         		<em>HierarchyMaster</em>
 *         </pre>
 *         <p>
 *         This is used for mongo collection which Stores the CDL Hierarchy Master details for hierarchy based applications .
 *         </p>
 */
@Document(collection = "CDLHierarchyMaster")
public class CDLHierarchyMaster extends AuditEntity {

    private String dealerId;
    private String dealerDesc;
    private String lsomBranchName;
    private String branchCode;
    private String branchName;
    private String area;
    private String region;
    private String zone;
    private String country;
    private String institutionId;
    private Date insertDt;
    private boolean active;

    /**
     * @return the dealerId
     */
    public String getDealerId() {
        return dealerId;
    }

    /**
     * @param dealerId the dealerId to set
     */
    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    /**
     * @return the dealerDesc
     */
    public String getDealerDesc() {
        return dealerDesc;
    }

    /**
     * @param dealerDesc the dealerDesc to set
     */
    public void setDealerDesc(String dealerDesc) {
        this.dealerDesc = dealerDesc;
    }

    /**
     * @return the lsomBranchName
     */
    public String getLsomBranchName() {
        return lsomBranchName;
    }

    /**
     * @param lsomBranchName the lsomBranchName to set
     */
    public void setLsomBranchName(String lsomBranchName) {
        this.lsomBranchName = lsomBranchName;
    }

    /**
     * @return the branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * @param branchCode the branchCode to set
     */
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return the area
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return the zone
     */
    public String getZone() {
        return zone;
    }

    /**
     * @param zone the zone to set
     */
    public void setZone(String zone) {
        this.zone = zone;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the insertDt
     */
    public Date getInsertDt() {
        return insertDt;
    }

    /**
     * @param insertDt the insertDt to set
     */
    public void setInsertDt(Date insertDt) {
        this.insertDt = insertDt;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CDLHierarchyMaster [dealerId=" + dealerId + ", dealerDesc="
                + dealerDesc + ", lsomBranchName=" + lsomBranchName
                + ", branchCode=" + branchCode + ", branchName=" + branchName
                + ", area=" + area + ", region=" + region + ", zone=" + zone
                + ", country=" + country + ", institutionId=" + institutionId
                + ", insertDt=" + insertDt + ", active=" + active + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result + ((area == null) ? 0 : area.hashCode());
        result = prime * result
                + ((branchCode == null) ? 0 : branchCode.hashCode());
        result = prime * result
                + ((branchName == null) ? 0 : branchName.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result
                + ((dealerDesc == null) ? 0 : dealerDesc.hashCode());
        result = prime * result
                + ((dealerId == null) ? 0 : dealerId.hashCode());
        result = prime * result
                + ((insertDt == null) ? 0 : insertDt.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((lsomBranchName == null) ? 0 : lsomBranchName.hashCode());
        result = prime * result + ((region == null) ? 0 : region.hashCode());
        result = prime * result + ((zone == null) ? 0 : zone.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CDLHierarchyMaster other = (CDLHierarchyMaster) obj;
        if (active != other.active)
            return false;
        if (area == null) {
            if (other.area != null)
                return false;
        } else if (!area.equals(other.area))
            return false;
        if (branchCode == null) {
            if (other.branchCode != null)
                return false;
        } else if (!branchCode.equals(other.branchCode))
            return false;
        if (branchName == null) {
            if (other.branchName != null)
                return false;
        } else if (!branchName.equals(other.branchName))
            return false;
        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;
        if (dealerDesc == null) {
            if (other.dealerDesc != null)
                return false;
        } else if (!dealerDesc.equals(other.dealerDesc))
            return false;
        if (dealerId == null) {
            if (other.dealerId != null)
                return false;
        } else if (!dealerId.equals(other.dealerId))
            return false;
        if (insertDt == null) {
            if (other.insertDt != null)
                return false;
        } else if (!insertDt.equals(other.insertDt))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (lsomBranchName == null) {
            if (other.lsomBranchName != null)
                return false;
        } else if (!lsomBranchName.equals(other.lsomBranchName))
            return false;
        if (region == null) {
            if (other.region != null)
                return false;
        } else if (!region.equals(other.region))
            return false;
        if (zone == null) {
            if (other.zone != null)
                return false;
        } else if (!zone.equals(other.zone))
            return false;
        return true;
    }


}
