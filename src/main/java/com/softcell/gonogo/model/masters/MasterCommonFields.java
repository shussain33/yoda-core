package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

public abstract class MasterCommonFields implements Serializable {

    @JsonProperty("sInsertedBy")
    private String insertedBy;

    @JsonProperty("sInsertedOn")
    private Date insertedOn = new Date();

    public String getInsertedBy() {
        return insertedBy;
    }

    public void setInsertedBy(String insertedBy) {
        this.insertedBy = insertedBy;
    }

    public Date getInsertedOn() {
        return insertedOn;
    }

    public void setInsertedOn(Date insertedOn) {
        this.insertedOn = insertedOn;
    }
}
