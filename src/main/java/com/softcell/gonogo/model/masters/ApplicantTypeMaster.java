/**
 * kishorp11:25:01 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.*;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "applicantTypeCode",
        "applicantTypeDescr"
})

@Document(collection = "ApplicantTypeMaster")
public class ApplicantTypeMaster extends AuditEntity {

    @JsonProperty("applicantTypeCode")
    private String applicantTypeCode;
    @JsonProperty("applicantTypeDescr")
    private String applicantTypeDescr;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The applicantTypeCode
     */
    @JsonProperty("applicantTypeCode")
    public String getApplicantTypeCode() {
        return applicantTypeCode;
    }

    /**
     *
     * @param applicantTypeCode
     * The applicantTypeCode
     */
    @JsonProperty("applicantTypeCode")
    public void setApplicantTypeCode(String applicantTypeCode) {
        this.applicantTypeCode = applicantTypeCode;
    }

    /**
     *
     * @return
     * The applicantTypeDescr
     */
    @JsonProperty("applicantTypeDescr")
    public String getApplicantTypeDescr() {
        return applicantTypeDescr;
    }

    /**
     *
     * @param applicantTypeDescr
     * The applicantTypeDescr
     */
    @JsonProperty("applicantTypeDescr")
    public void setApplicantTypeDescr(String applicantTypeDescr) {
        this.applicantTypeDescr = applicantTypeDescr;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
