/**
 * kishorp8:07:58 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import java.util.Map;

/**
 * @author kishorp
 *
 */
public class BureauStates {

    private Map<String, State> cibilState;
    private Map<String, State> highmarkState;
    private Map<String, State> equifaxState;
    private Map<String, State> experianState;
    private boolean active;


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Map<String, State> getCibilState() {
        return cibilState;
    }

    public void setCibilState(Map<String, State> cibilState) {
        this.cibilState = cibilState;
    }

    public Map<String, State> getHighmarkState() {
        return highmarkState;
    }

    public void setHighmarkState(Map<String, State> highmarkState) {
        this.highmarkState = highmarkState;
    }

    public Map<String, State> getEquifaxState() {
        return equifaxState;
    }

    public void setEquifaxState(Map<String, State> equifaxState) {
        this.equifaxState = equifaxState;
    }

    public Map<String, State> getExperianState() {
        return experianState;
    }

    public void setExperianState(Map<String, State> experianState) {
        this.experianState = experianState;
    }

    @Override
    public String toString() {
        return "BureauStates [cibilState=" + cibilState + ", highmarkState="
                + highmarkState + ", equifaxState=" + equifaxState
                + ", experianState=" + experianState + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((cibilState == null) ? 0 : cibilState.hashCode());
        result = prime * result
                + ((equifaxState == null) ? 0 : equifaxState.hashCode());
        result = prime * result
                + ((experianState == null) ? 0 : experianState.hashCode());
        result = prime * result
                + ((highmarkState == null) ? 0 : highmarkState.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BureauStates other = (BureauStates) obj;
        if (cibilState == null) {
            if (other.cibilState != null)
                return false;
        } else if (!cibilState.equals(other.cibilState))
            return false;
        if (equifaxState == null) {
            if (other.equifaxState != null)
                return false;
        } else if (!equifaxState.equals(other.equifaxState))
            return false;
        if (experianState == null) {
            if (other.experianState != null)
                return false;
        } else if (!experianState.equals(other.experianState))
            return false;
        if (highmarkState == null) {
            if (other.highmarkState != null)
                return false;
        } else if (!highmarkState.equals(other.highmarkState))
            return false;
        return true;
    }

}
