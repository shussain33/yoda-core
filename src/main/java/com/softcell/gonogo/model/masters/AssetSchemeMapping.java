/**
 * yogeshb11:43:17 am  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb
 * <pre>
 * 		<em>AssetSchemeMapping</em>
 * </pre>
 * <p>This class is used for Asset and Scheme Mapping.</p>
 */
@Document(collection = "AssetSchemeMapping")
public class AssetSchemeMapping extends AuditEntity {
    private String schemeID;
    private String modelId;
    private String enabledFlag;
    private String inclusionFlag;
    private String makerID;
    private String makerDate;
    private String authID;
    private String authDate;
    private String institutionID;

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getEnabledFlag() {
        return enabledFlag;
    }

    public void setEnabledFlag(String enabledFlag) {
        this.enabledFlag = enabledFlag;
    }

    public String getInclusionFlag() {
        return inclusionFlag;
    }

    public void setInclusionFlag(String inclusionFlag) {
        this.inclusionFlag = inclusionFlag;
    }

    public String getMakerID() {
        return makerID;
    }

    public void setMakerID(String makerID) {
        this.makerID = makerID;
    }

    public String getMakerDate() {
        return makerDate;
    }

    public void setMakerDate(String makerDate) {
        this.makerDate = makerDate;
    }

    public String getAuthID() {
        return authID;
    }

    public void setAuthID(String authID) {
        this.authID = authID;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    @Override
    public String toString() {
        return "AssetSchemeMapping [schemeID=" + schemeID + ", modelId="
                + modelId + ", enabledFlag=" + enabledFlag + ", inclusionFlag="
                + inclusionFlag + ", makerID=" + makerID + ", makerDate="
                + makerDate + ", authID=" + authID + ", authDate=" + authDate
                + ", institutionID=" + institutionID + "]";
    }
}
