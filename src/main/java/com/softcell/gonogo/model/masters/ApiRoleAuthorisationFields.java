package com.softcell.gonogo.model.masters;

public enum ApiRoleAuthorisationFields {

    API_NAME("apiName"),
    METHOD("methodType"),
    ROLES("roles");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    ApiRoleAuthorisationFields(String value) {
        this.value = value;
    }
}
