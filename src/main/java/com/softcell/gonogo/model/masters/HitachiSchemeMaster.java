/**
 * yogeshb12:24:23 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb
 * <pre>
 * 		<em>HitachiSchemeMaster</em>
 * </pre>
 * <p>
 * 		This is used for mongo collection and it store the Hitachi Schemes. 
 * </p>
 */
@Document(collection = "HitachiSchemeMaster")
public class HitachiSchemeMaster extends AuditEntity {
    @JsonIgnore
    private String supplierID;

    @JsonProperty("sSchID")
    private String schemeID;

    @JsonProperty("sSchDes")
    private String schemeDesc;

    @JsonIgnore
    private String currencyID;

    @JsonProperty("sMxTenu")
    private String maxTenure;

    @JsonProperty("sMinTenu")
    private String minTenure;

    @JsonIgnore
    private String sdAmt;

    @JsonProperty("sDint")
    private String sdInt;

    @JsonProperty("sMinAmt")
    private String minAmtFin;

    @JsonIgnore
    private String custMinAmtFin;

    @JsonIgnore
    private String totalBd;

    @JsonIgnore
    private String insertDate;

    @JsonIgnore
    private String schemeStartDate;

    @JsonIgnore
    private String schemeEndDate;

    @JsonIgnore
    private String makerID;

    @JsonIgnore
    private String makeDate;

    @JsonIgnore
    private String authID;

    @JsonIgnore
    private String authDate;

    @JsonIgnore
    private String institutionID;

    @JsonIgnore
    private boolean active;


    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public String getCurrencyID() {
        return currencyID;
    }

    public void setCurrencyID(String currencyID) {
        this.currencyID = currencyID;
    }

    public String getMaxTenure() {
        return maxTenure;
    }

    public void setMaxTenure(String maxTenure) {
        this.maxTenure = maxTenure;
    }

    public String getMinTenure() {
        return minTenure;
    }

    public void setMinTenure(String minTenure) {
        this.minTenure = minTenure;
    }

    public String getSdAmt() {
        return sdAmt;
    }

    public void setSdAmt(String sdAmt) {
        this.sdAmt = sdAmt;
    }

    public String getSdInt() {
        return sdInt;
    }

    public void setSdInt(String sdInt) {
        this.sdInt = sdInt;
    }

    public String getMinAmtFin() {
        return minAmtFin;
    }

    public void setMinAmtFin(String minAmtFin) {
        this.minAmtFin = minAmtFin;
    }

    public String getCustMinAmtFin() {
        return custMinAmtFin;
    }

    public void setCustMinAmtFin(String custMinAmtFin) {
        this.custMinAmtFin = custMinAmtFin;
    }

    public String getTotalBd() {
        return totalBd;
    }

    public void setTotalBd(String totalBd) {
        this.totalBd = totalBd;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public String getSchemeStartDate() {
        return schemeStartDate;
    }

    public void setSchemeStartDate(String schemeStartDate) {
        this.schemeStartDate = schemeStartDate;
    }

    public String getSchemeEndDate() {
        return schemeEndDate;
    }

    public void setSchemeEndDate(String schemeEndDate) {
        this.schemeEndDate = schemeEndDate;
    }

    public String getMakerID() {
        return makerID;
    }

    public void setMakerID(String makerID) {
        this.makerID = makerID;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getAuthID() {
        return authID;
    }

    public void setAuthID(String authID) {
        this.authID = authID;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "HitachiSchemeMaster [supplierID=" + supplierID + ", schemeID="
                + schemeID + ", schemeDesc=" + schemeDesc + ", currencyID="
                + currencyID + ", maxTenure=" + maxTenure + ", minTenure="
                + minTenure + ", sdAmt=" + sdAmt + ", sdInt=" + sdInt
                + ", minAmtFin=" + minAmtFin + ", custMinAmtFin="
                + custMinAmtFin + ", totalBd=" + totalBd + ", insertDate="
                + insertDate + ", schemeStartDate=" + schemeStartDate
                + ", schemeEndDate=" + schemeEndDate + ", makerID=" + makerID
                + ", makeDate=" + makeDate + ", authID=" + authID
                + ", authDate=" + authDate + ", institutionID=" + institutionID
                + ", active=" + active + "]";
    }


}
