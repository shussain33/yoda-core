package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * It is LOS related master.
 * It is use to get asset category id based on asset category desc master.
 *
 * @author bhuvneshk
 */
@Document(collection = "assetCategory")
public class AssetCategoryMaster extends AuditEntity {

    @JsonProperty("sAssetCatgId")
    private String assetCatgId;

    @JsonProperty("sAssetCatgDesc")
    private String assetCatgDesc;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active;

    public String getAssetCatgId() {
        return assetCatgId;
    }

    public void setAssetCatgId(String assetCatgId) {
        this.assetCatgId = assetCatgId;
    }

    public String getAssetCatgDesc() {
        return assetCatgDesc;
    }

    public void setAssetCatgDesc(String assetCatgDesc) {
        this.assetCatgDesc = assetCatgDesc;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AssetCategoryMaster [assetCatgId=");
        builder.append(assetCatgId);
        builder.append(", assetCatgDesc=");
        builder.append(assetCatgDesc);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }


}
