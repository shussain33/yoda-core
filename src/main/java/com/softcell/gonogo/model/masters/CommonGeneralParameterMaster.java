package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.master.commongeneralmaster.InsertCommonGeneralMasterRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 15/5/17.
 */
@Document(collection = "commonGeneralParameterMaster")
public class CommonGeneralParameterMaster {

    @Id
    @JsonProperty("sUniqueId")
    private String uniqueId;

    @JsonProperty("sBranchId")
    private String branchId;

    @JsonProperty("sMakerId")
    private String makerId;

    @JsonProperty("dtMakeDate")
    private Date makeDate;

    @JsonProperty("sAuthId")
    private String authId;

    @JsonProperty("dtAuthDate")
    private Date authDate;

    @JsonProperty("sModuleId")
    private String moduleId;

    @JsonProperty("sKey1")
    private String key1;

    @JsonProperty("sKey2")
    private String key2;

    @JsonProperty("sValue")
    private String value;

    @JsonProperty("sDescription")
    private String description;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sModuleFlag")
    private String moduleFlag;

    @JsonProperty("sAppFlag")
    private String appFlag;

    @JsonProperty("sDisableFlag")
    private String disableFlag;

    @JsonProperty("dtCgpStartDate")
    private Date cgpStartDate;

    @JsonProperty("dtCgpEndDate")
    private Date cgpEndDate;

    @JsonProperty("sMApplyFlag")
    private String mApplyFlag;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("dtInsertDate")
    private Date insertDate = new Date();

    @JsonProperty("dtUpdateDate")
    private Date updateDate;

    @JsonIgnore
    private boolean active = true;


    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getMakerId() {
        return makerId;
    }

    public void setMakerId(String makerId) {
        this.makerId = makerId;
    }

    public Date getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(Date makeDate) {
        this.makeDate = makeDate;
    }

    public void setAuthDate(Date authDate) {
        this.authDate = authDate;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public Date getAuthDate() {
        return authDate;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModuleFlag() {
        return moduleFlag;
    }

    public void setModuleFlag(String moduleFlag) {
        this.moduleFlag = moduleFlag;
    }

    public String getAppFlag() {
        return appFlag;
    }

    public void setAppFlag(String appFlag) {
        this.appFlag = appFlag;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public Date getCgpStartDate() {
        return cgpStartDate;
    }

    public void setCgpStartDate(Date cgpStartDate) {
        this.cgpStartDate = cgpStartDate;
    }

    public Date getCgpEndDate() {
        return cgpEndDate;
    }

    public void setCgpEndDate(Date cgpEndDate) {
        this.cgpEndDate = cgpEndDate;
    }

    public String getmApplyFlag() {
        return mApplyFlag;
    }

    public void setmApplyFlag(String mApplyFlag) {
        this.mApplyFlag = mApplyFlag;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CommonGeneralParameterMaster{");
        sb.append("uniqueId='").append(uniqueId).append('\'');
        sb.append(", branchId='").append(branchId).append('\'');
        sb.append(", makerId='").append(makerId).append('\'');
        sb.append(", makeDate=").append(makeDate);
        sb.append(", authId='").append(authId).append('\'');
        sb.append(", authDate=").append(authDate);
        sb.append(", moduleId='").append(moduleId).append('\'');
        sb.append(", key1='").append(key1).append('\'');
        sb.append(", key2='").append(key2).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", moduleFlag='").append(moduleFlag).append('\'');
        sb.append(", appFlag='").append(appFlag).append('\'');
        sb.append(", disableFlag='").append(disableFlag).append('\'');
        sb.append(", cgpStartDate=").append(cgpStartDate);
        sb.append(", cgpEndDate=").append(cgpEndDate);
        sb.append(", mApplyFlag='").append(mApplyFlag).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", insertDate=").append(insertDate);
        sb.append(", updateDate=").append(updateDate);
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {

    }
}