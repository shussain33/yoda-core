package com.softcell.gonogo.model.masters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by bhuvneshk on 31/10/17.
 *
 * It is a los master. It will be used to fetch dealer/supplier location
 * based on dealer/supplier id.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "supplierLocationMaster")
public class SupplierLocationMaster {

    private String supplierId;
    private String branchId;
    private String supplierLocationCode;
    private String city;
    private String address;
    private String inactive;
    private String institutionId;
    private boolean active;
}
