package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 29/5/17.
 */
@Document(collection = "insurancePremiumMaster")
public class InsurancePremiumMaster {

    @JsonProperty("sInsAssetCat")
    private String insuranceAssetCat;

    @JsonProperty("dMinAssetValue")
    private double minAssetValue;

    @JsonProperty("dMaxAssetValue")
    private double maxAssetValue;

    @JsonProperty("dInsurancePremium")
    private double insurancePremium;

    @JsonProperty("sProductFlag")
    private String productFlag;

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("dtInsertDate")
    private Date insertDate = new Date();

    @JsonIgnore
    private boolean active = true;

    @JsonProperty("sInsuranceType")
    private String insuranceType;



    @JsonProperty("sInsuranceTypeDesc")
    private String insuranceTypeDesc;

    public String getInsuranceType() {return insuranceType;}

    public void setInsuranceType(String insuranceType) {this.insuranceType = insuranceType;}

    public String getInsuranceTypeDesc() {return insuranceTypeDesc;}

    public void setInsuranceTypeDesc(String insuranceTypeDesc) {this.insuranceTypeDesc = insuranceTypeDesc;}

    public String getInsuranceAssetCat() {
        return insuranceAssetCat;
    }

    public void setInsuranceAssetCat(String insuranceAssetCat) {
        this.insuranceAssetCat = insuranceAssetCat;
    }

    public double getMinAssetValue() {
        return minAssetValue;
    }

    public void setMinAssetValue(double minAssetValue) {
        this.minAssetValue = minAssetValue;
    }

    public double getMaxAssetValue() {
        return maxAssetValue;
    }

    public void setMaxAssetValue(double maxAssetValue) {
        this.maxAssetValue = maxAssetValue;
    }

    public double getInsurancePremium() {
        return insurancePremium;
    }

    public void setInsurancePremium(double insurancePremium) {
        this.insurancePremium = insurancePremium;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InsurancePremiumMaster{");
        sb.append("insuranceAssetCat='").append(insuranceAssetCat).append('\'');
        sb.append(", minAssetValue=").append(minAssetValue);
        sb.append(", maxAssetValue=").append(maxAssetValue);
        sb.append(", insurancePremium=").append(insurancePremium);
        sb.append(", insuranceType=").append(insuranceType);
        sb.append(", insuranceTypeDesc=").append(insuranceTypeDesc);
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", insertDate=").append(insertDate);
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }
}
