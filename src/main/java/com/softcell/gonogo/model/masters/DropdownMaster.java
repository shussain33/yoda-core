package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ssg228 on 25/1/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "DropDownMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DropdownMaster {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @NotNull
    @JsonProperty("sDropDownType")
    private String dropDownType;

    @JsonProperty("aProductList")
    private List<Product> productList;

    @JsonProperty("aRoles")
    private List<Roles.Role> rolesList;

    @NotNull
    @JsonProperty("aDropDownValueDetails")
    public List<HashMap<String, String>> dropDownValueDetailsList;

    @JsonProperty("dtCreatedDate")
    private Date createdDate;

    @JsonProperty("sCreatedBy")
    private String createdBy;

    @JsonProperty("dtUpdatedDate")
    private Date updatedDate;

    @JsonProperty("sUpdateBy")
    private String updateBy;

    @JsonProperty("dVersion")
    private Long version;

    @JsonProperty("aCriteria")
    private List<Object> criteria;
}