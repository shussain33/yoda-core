/**
 * kishorp11:54:52 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

/**
 * @author kishorp
 *
 */
public class DistrictMaster {
    private String districtId;
    private String districtDesc;
    private String districtAbbrv;
    private String bsrCode;

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictDesc() {
        return districtDesc;
    }

    public void setDistrictDesc(String districtDesc) {
        this.districtDesc = districtDesc;
    }

    public String getDistrictAbbrv() {
        return districtAbbrv;
    }

    public void setDistrictAbbrv(String districtAbbrv) {
        this.districtAbbrv = districtAbbrv;
    }

    public String getBsrCode() {
        return bsrCode;
    }

    public void setBsrCode(String bsrCode) {
        this.bsrCode = bsrCode;
    }


}
