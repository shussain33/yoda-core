package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * It is LOS related master.
 * It is use to get broker id based on branch id from  master.
 *
 * @author bhuvneshk
 */
@Document(collection = "dsaBranchMaster")
public class DSABranchMaster extends AuditEntity {
    @JsonProperty("sBranchId")
    private String branchId;

    @JsonProperty("sBrokerId")
    private String brokerId;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active;

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(String brokerId) {
        this.brokerId = brokerId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DSABranchMaster [branchId=");
        builder.append(branchId);
        builder.append(", brokerId=");
        builder.append(brokerId);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }


}
