/**
 * yogeshb10:44:48 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb
 *         <p/>
 *         <pre>
 *         		<em>DealerEmailMaster</em>
 *         </pre>
 *         <p>
 *         This is used for mongo collection which Stores the dealer email IDs
 *         and use to get email while email sending of DO Report.
 *         </p>
 */
@Document(collection = "DealerEmailMaster")
public class DealerEmailMaster extends AuditEntity {

    private String dealerID;
    private String email;
    private String supplierDesc;
    private String supplierID;

    private String recipientsType;
    private String institutionID;
    private String cityId;
    private String stateId;
    private String itNum;
    private String contactPerson;
    private String zipCode;
    private String phoneNumber;
    private String address1;
    private String address2;
    private String address3;

    @JsonIgnore
    private boolean active=true;
    // is the dealer ranking
    private double dealerComm;

    //TVS SAP Dealer Code Mapping
    private String dealerSapCode;

    public String getDealerSapCode() {
        return dealerSapCode;
    }

    public void setDealerSapCode(String dealerSapCode) {
        this.dealerSapCode = dealerSapCode;
    }

    public String getDealerID() {
        return dealerID;
    }

    public void setDealerID(String dealerID) {
        this.dealerID = dealerID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSupplierDesc() {
        return supplierDesc;
    }

    public void setSupplierDesc(String supplierDesc) {
        this.supplierDesc = supplierDesc;
    }

    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }

    public String getRecipientsType() {
        return recipientsType;
    }

    public void setRecipientsType(String recipientsType) {
        this.recipientsType = recipientsType;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getItNum() {
        return itNum;
    }

    public void setItNum(String itNum) {
        this.itNum = itNum;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getDealerComm() {
        return dealerComm;
    }

    public void setDealerComm(double dealerComm) {
        this.dealerComm = dealerComm;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DealerEmailMaster{");
        sb.append("dealerID='").append(dealerID).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", supplierDesc='").append(supplierDesc).append('\'');
        sb.append(", supplierID='").append(supplierID).append('\'');
        sb.append(", recipientsType='").append(recipientsType).append('\'');
        sb.append(", institutionID='").append(institutionID).append('\'');
        sb.append(", cityId='").append(cityId).append('\'');
        sb.append(", stateId='").append(stateId).append('\'');
        sb.append(", itNum='").append(itNum).append('\'');
        sb.append(", contactPerson='").append(contactPerson).append('\'');
        sb.append(", zipCode='").append(zipCode).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", address1='").append(address1).append('\'');
        sb.append(", address2='").append(address2).append('\'');
        sb.append(", address3='").append(address3).append('\'');
        sb.append(", active=").append(active);
        sb.append(", dealerComm=").append(dealerComm);
        sb.append(", dealerSapCode='").append(dealerSapCode).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DealerEmailMaster that = (DealerEmailMaster) o;

        if (active != that.active) return false;
        if (Double.compare(that.dealerComm, dealerComm) != 0) return false;
        if (dealerID != null ? !dealerID.equals(that.dealerID) : that.dealerID != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (supplierDesc != null ? !supplierDesc.equals(that.supplierDesc) : that.supplierDesc != null) return false;
        if (supplierID != null ? !supplierID.equals(that.supplierID) : that.supplierID != null) return false;
        if (recipientsType != null ? !recipientsType.equals(that.recipientsType) : that.recipientsType != null)
            return false;
        if (institutionID != null ? !institutionID.equals(that.institutionID) : that.institutionID != null)
            return false;
        if (cityId != null ? !cityId.equals(that.cityId) : that.cityId != null) return false;
        if (stateId != null ? !stateId.equals(that.stateId) : that.stateId != null) return false;
        if (itNum != null ? !itNum.equals(that.itNum) : that.itNum != null) return false;
        if (contactPerson != null ? !contactPerson.equals(that.contactPerson) : that.contactPerson != null)
            return false;
        if (zipCode != null ? !zipCode.equals(that.zipCode) : that.zipCode != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        if (address1 != null ? !address1.equals(that.address1) : that.address1 != null) return false;
        if (address2 != null ? !address2.equals(that.address2) : that.address2 != null) return false;
        if (address3 != null ? !address3.equals(that.address3) : that.address3 != null) return false;
        return dealerSapCode != null ? dealerSapCode.equals(that.dealerSapCode) : that.dealerSapCode == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + (dealerID != null ? dealerID.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (supplierDesc != null ? supplierDesc.hashCode() : 0);
        result = 31 * result + (supplierID != null ? supplierID.hashCode() : 0);
        result = 31 * result + (recipientsType != null ? recipientsType.hashCode() : 0);
        result = 31 * result + (institutionID != null ? institutionID.hashCode() : 0);
        result = 31 * result + (cityId != null ? cityId.hashCode() : 0);
        result = 31 * result + (stateId != null ? stateId.hashCode() : 0);
        result = 31 * result + (itNum != null ? itNum.hashCode() : 0);
        result = 31 * result + (contactPerson != null ? contactPerson.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (address1 != null ? address1.hashCode() : 0);
        result = 31 * result + (address2 != null ? address2.hashCode() : 0);
        result = 31 * result + (address3 != null ? address3.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        temp = Double.doubleToLongBits(dealerComm);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (dealerSapCode != null ? dealerSapCode.hashCode() : 0);
        return result;
    }
}