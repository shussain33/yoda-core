package com.softcell.gonogo.model.masters;

/**
 * Created by bhuvneshk on 27/9/17.
 */
public enum PincodeStateMasterFields {

    LMC_CITYNAME_C("city"),
    ZIPCODE("zipCode"),
    ZIPDESC("zipCodeDescr"),
    STATEDESC("state");

    private String value;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    PincodeStateMasterFields(String value) {
        this.value = value;
    }
}

