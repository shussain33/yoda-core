/**
 * vinodk6:17:42 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "PropertyTypeMaster")
public class PropertyTypeMaster extends AuditEntity {

    @JsonProperty("sPropCodeDesc")
    private String propertyCodeDescription;

    /**
     * @return the propertyCodeDescription
     */
    public String getPropertyCodeDescription() {
        return propertyCodeDescription;
    }

    /**
     * @param propertyCodeDescription
     *            the propertyCodeDescription to set
     */
    public void setPropertyCodeDescription(String propertyCodeDescription) {
        this.propertyCodeDescription = propertyCodeDescription;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PropertyTypeMaster [propertyCodeDescription="
                + propertyCodeDescription + "]";
    }
}
