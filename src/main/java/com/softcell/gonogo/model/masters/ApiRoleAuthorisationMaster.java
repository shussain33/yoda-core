package com.softcell.gonogo.model.masters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document(collection = "apiRoleAuthorisationMaster")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiRoleAuthorisationMaster extends MasterCommonFields implements Serializable {

    private boolean active;

    private String apiName;

    private String methodType;

    private String institutionId;

    private String sourceId;

    private String roles;

    private List<String> rolesList;
}
