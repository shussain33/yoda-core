package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by amit on 27/12/18.
 */
@Document(collection = "roiSchemeMaster")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoiSchemeMaster {
    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("sCompanyName")
    private String companyName;

    @JsonProperty("sScheme")
    private String schemeName;

    @JsonProperty("dExternalPer")
    private Double externalPercentage;

    @JsonProperty("dInternalPer")
    private Double internalPercentage;

    @JsonProperty("sRoi")
    private Double roi;
}
