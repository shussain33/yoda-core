package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 */
@Document(collection = "ModelVariantMaster")
public class ModelVariantMaster extends AuditEntity {

    @JsonProperty("sManufacturer")
    private String manufacturer;

    @JsonProperty("sModel")
    private String model;

    @JsonProperty("sVariant")
    private String variant;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active;

    /**
     * @return the manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer the manufacturer to set
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the variant
     */
    public String getVariant() {
        return variant;
    }

    /**
     * @param variant the variant to set
     */
    public void setVariant(String variant) {
        this.variant = variant;
    }

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ModelVariantMaster [manufacturer=");
        builder.append(manufacturer);
        builder.append(", model=");
        builder.append(model);
        builder.append(", variant=");
        builder.append(variant);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }
}
