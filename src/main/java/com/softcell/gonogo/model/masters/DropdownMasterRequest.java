package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by ssg228 on 25/1/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DropdownMasterRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sDropDownType")
    private String dropDownType;

    @NotNull
    @JsonProperty("sQueryType")
    private String queryType;

    @JsonProperty("bProductWise")
    private  boolean productWise;
}