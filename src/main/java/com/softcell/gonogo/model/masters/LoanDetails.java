/**
 * vinodk1:19:12 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

/**
 * @author vinodk
 *
 */

import com.fasterxml.jackson.annotation.*;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"loanAmount", "loanPurpose", "interestRate", "duration",
        "emi"})
@Document(collection = "LoanDetails")
public class LoanDetails extends AuditEntity {

    @JsonProperty("dLoanAmount")
    private double loanAmount;
    @JsonProperty("sLoanPurpose")
    private String loanPurpose;
    @JsonProperty("dInterestRate")
    private double interestRate;
    @JsonProperty("iDuration")
    private Integer duration;
    @JsonProperty("dEmi")
    private double emi;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The loanAmount
     */
    @JsonProperty("dLoanAmount")
    public Object getLoanAmount() {
        return loanAmount;
    }

    /**
     *
     * @param loanAmount
     *            The loanAmount
     */
    @JsonProperty("dLoanAmount")
    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    /**
     *
     * @return The loanPurpose
     */
    @JsonProperty("sLoanPurpose")
    public String getLoanPurpose() {
        return loanPurpose;
    }

    /**
     *
     * @param loanPurpose
     *            The loanPurpose
     */
    @JsonProperty("sLoanPurpose")
    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    /**
     *
     * @return The interestRate
     */
    @JsonProperty("dInterestRate")
    public Object getInterestRate() {
        return interestRate;
    }

    /**
     *
     * @param interestRate
     *            The interestRate
     */
    @JsonProperty("dInterestRate")
    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    /**
     *
     * @return The duration
     */
    @JsonProperty("iDuration")
    public Integer getDuration() {
        return duration;
    }

    /**
     *
     * @param duration
     *            The duration
     */
    @JsonProperty("iDuration")
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     *
     * @return The emi
     */
    @JsonProperty("dEmi")
    public Object getEmi() {
        return emi;
    }

    /**
     *
     * @param emi
     *            The emi
     */
    @JsonProperty("dEmi")
    public void setEmi(double emi) {
        this.emi = emi;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LoanDetails [loanAmount=" + loanAmount + ", loanPurpose="
                + loanPurpose + ", interestRate=" + interestRate
                + ", duration=" + duration + ", emi=" + emi + "]";
    }
}