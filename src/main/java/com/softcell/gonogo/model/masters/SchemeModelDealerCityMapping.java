/**
 * yogeshb6:02:45 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

/**
 * @author yogeshb
 * <pre>
 * 		<em>SchemeModelDealerCityMapping</em>
 * </pre>
 * <p>
 * 		This is used for mongo collection and it is linkage between Scheme model and dealer. 
 * </p>
 */

@Document(collection = "SchemeModelDealerCityMapping")
public class SchemeModelDealerCityMapping extends AuditEntity {

    private String schemeID;
    private String modelID;
    private String validity;
    private String stateID;
    private String includedStateID;
    private String excludedStateID;
    private String cityID;
    private String includedCityID;
    private String excludedCityID;
    private String includedDealerID;
    private String excludedDealerID;
    private String insertDate;
    private String makerID;
    private String makeDate;
    private String authID;
    private String authDate;
    private String dealerId;
    private Set<String> dealerInclusion;
    private String institutionID;
    private boolean active = true;


    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public Set<String> getDealerInclusion() {
        return dealerInclusion;
    }

    public void setDealerInclusion(Set<String> dealerInclusion) {
        this.dealerInclusion = dealerInclusion;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public String getMakerID() {
        return makerID;
    }

    public void setMakerID(String makerID) {
        this.makerID = makerID;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getAuthID() {
        return authID;
    }

    public void setAuthID(String authID) {
        this.authID = authID;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getIncludedDealerID() {
        return includedDealerID;
    }

    public void setIncludedDealerID(String includedDealerID) {
        this.includedDealerID = includedDealerID;
    }

    public String getExcludedDealerID() {
        return excludedDealerID;
    }

    public void setExcludedDealerID(String excludedDealerID) {
        this.excludedDealerID = excludedDealerID;
    }

    public String getIncludedCityID() {
        return includedCityID;
    }

    public void setIncludedCityID(String includedCityID) {
        this.includedCityID = includedCityID;
    }

    public String getExcludedCityID() {
        return excludedCityID;
    }

    public void setExcludedCityID(String excludedCityID) {
        this.excludedCityID = excludedCityID;
    }

    public String getStateID() {
        return stateID;
    }

    public void setStateID(String stateID) {
        this.stateID = stateID;
    }

    public String getIncludedStateID() {
        return includedStateID;
    }

    public void setIncludedStateID(String includedStateID) {
        this.includedStateID = includedStateID;
    }

    public String getExcludedStateID() {
        return excludedStateID;
    }

    public void setExcludedStateID(String excludedStateID) {
        this.excludedStateID = excludedStateID;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SchemeModelDealerCityMapping [schemeID=" + schemeID
                + ", modelID=" + modelID + ", validity=" + validity
                + ", stateID=" + stateID + ", includedStateID="
                + includedStateID + ", excludedStateID=" + excludedStateID
                + ", cityID=" + cityID + ", includedCityID=" + includedCityID
                + ", excludedCityID=" + excludedCityID + ", includedDealerID="
                + includedDealerID + ", excludedDealerID=" + excludedDealerID
                + ", insertDate=" + insertDate + ", makerID=" + makerID
                + ", makeDate=" + makeDate + ", authID=" + authID
                + ", authDate=" + authDate + ", dealerId=" + dealerId
                + ", dealerInclusion=" + dealerInclusion + ", institutionID="
                + institutionID + ", active=" + active + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result
                + ((authDate == null) ? 0 : authDate.hashCode());
        result = prime * result + ((authID == null) ? 0 : authID.hashCode());
        result = prime * result + ((cityID == null) ? 0 : cityID.hashCode());
        result = prime * result
                + ((dealerId == null) ? 0 : dealerId.hashCode());
        result = prime * result
                + ((dealerInclusion == null) ? 0 : dealerInclusion.hashCode());
        result = prime * result
                + ((excludedCityID == null) ? 0 : excludedCityID.hashCode());
        result = prime
                * result
                + ((excludedDealerID == null) ? 0 : excludedDealerID.hashCode());
        result = prime * result
                + ((excludedStateID == null) ? 0 : excludedStateID.hashCode());
        result = prime * result
                + ((includedCityID == null) ? 0 : includedCityID.hashCode());
        result = prime
                * result
                + ((includedDealerID == null) ? 0 : includedDealerID.hashCode());
        result = prime * result
                + ((includedStateID == null) ? 0 : includedStateID.hashCode());
        result = prime * result
                + ((insertDate == null) ? 0 : insertDate.hashCode());
        result = prime * result
                + ((institutionID == null) ? 0 : institutionID.hashCode());
        result = prime * result
                + ((makeDate == null) ? 0 : makeDate.hashCode());
        result = prime * result + ((makerID == null) ? 0 : makerID.hashCode());
        result = prime * result + ((modelID == null) ? 0 : modelID.hashCode());
        result = prime * result
                + ((schemeID == null) ? 0 : schemeID.hashCode());
        result = prime * result + ((stateID == null) ? 0 : stateID.hashCode());
        result = prime * result
                + ((validity == null) ? 0 : validity.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof SchemeModelDealerCityMapping))
            return false;
        SchemeModelDealerCityMapping other = (SchemeModelDealerCityMapping) obj;
        if (active != other.active)
            return false;
        if (authDate == null) {
            if (other.authDate != null)
                return false;
        } else if (!authDate.equals(other.authDate))
            return false;
        if (authID == null) {
            if (other.authID != null)
                return false;
        } else if (!authID.equals(other.authID))
            return false;
        if (cityID == null) {
            if (other.cityID != null)
                return false;
        } else if (!cityID.equals(other.cityID))
            return false;
        if (dealerId == null) {
            if (other.dealerId != null)
                return false;
        } else if (!dealerId.equals(other.dealerId))
            return false;
        if (dealerInclusion == null) {
            if (other.dealerInclusion != null)
                return false;
        } else if (!dealerInclusion.equals(other.dealerInclusion))
            return false;
        if (excludedCityID == null) {
            if (other.excludedCityID != null)
                return false;
        } else if (!excludedCityID.equals(other.excludedCityID))
            return false;
        if (excludedDealerID == null) {
            if (other.excludedDealerID != null)
                return false;
        } else if (!excludedDealerID.equals(other.excludedDealerID))
            return false;
        if (excludedStateID == null) {
            if (other.excludedStateID != null)
                return false;
        } else if (!excludedStateID.equals(other.excludedStateID))
            return false;
        if (includedCityID == null) {
            if (other.includedCityID != null)
                return false;
        } else if (!includedCityID.equals(other.includedCityID))
            return false;
        if (includedDealerID == null) {
            if (other.includedDealerID != null)
                return false;
        } else if (!includedDealerID.equals(other.includedDealerID))
            return false;
        if (includedStateID == null) {
            if (other.includedStateID != null)
                return false;
        } else if (!includedStateID.equals(other.includedStateID))
            return false;
        if (insertDate == null) {
            if (other.insertDate != null)
                return false;
        } else if (!insertDate.equals(other.insertDate))
            return false;
        if (institutionID == null) {
            if (other.institutionID != null)
                return false;
        } else if (!institutionID.equals(other.institutionID))
            return false;
        if (makeDate == null) {
            if (other.makeDate != null)
                return false;
        } else if (!makeDate.equals(other.makeDate))
            return false;
        if (makerID == null) {
            if (other.makerID != null)
                return false;
        } else if (!makerID.equals(other.makerID))
            return false;
        if (modelID == null) {
            if (other.modelID != null)
                return false;
        } else if (!modelID.equals(other.modelID))
            return false;
        if (schemeID == null) {
            if (other.schemeID != null)
                return false;
        } else if (!schemeID.equals(other.schemeID))
            return false;
        if (stateID == null) {
            if (other.stateID != null)
                return false;
        } else if (!stateID.equals(other.stateID))
            return false;
        if (validity == null) {
            if (other.validity != null)
                return false;
        } else if (!validity.equals(other.validity))
            return false;
        return true;
    }


}
