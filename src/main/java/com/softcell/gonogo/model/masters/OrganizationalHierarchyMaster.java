package com.softcell.gonogo.model.masters;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by amit on 28/3/18.
 */
@Document(collection = "organizationalHierarchyMaster")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationalHierarchyMaster{

    private String institutionId;

    private String branchCode;

    private String product;

    private String channel;

    private List<HierarchyNode> hierarchyNodes;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
