/**
 * yogeshb8:16:07 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.response.master.schemedatemapping.SchemeInformationResponse;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * @author yogeshb
 *         <p>
 *         <pre>
 *                                 		<em>SchemeDateMapping</em>
 *                                 </pre>
 *         <p>
 *         This is used for mongo collection and it store the SchemeDateMapping
 *         Linkages.
 *         </p>
 */
@Document(collection = "schemeDetailsDateMapping")
public class SchemeDetailsDateMapping extends AuditEntity {

    @JsonProperty("sSchemeID")
    private String schemeID;

    @JsonProperty("sSchDes")
    private String schemeDesc;


    @Transient
    @JsonProperty("asSchemeIdSet")
    private Set<String> schemeIdSet;

    @Transient
    @NotEmpty(groups = SchemeDetailsDateMapping.FetchGrp.class)
    @JsonProperty("aoSchemeIdWithDesc")
    private List<SchemeInformationResponse> schemeInformationResponseList;


    @JsonProperty("dtValidFrDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date validFromDate;

    @JsonProperty("dtValidToDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date validToDate;

    @JsonProperty("aExcldDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Set<Date> excludedDate;

    @JsonProperty("aIncldDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Set<Date> includedDate;

    @JsonProperty("sMkrID")
    private String makerID;

    @JsonProperty("dtMakeDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date makeDate;

    @JsonProperty("sCheckerId")
    private String checkerId;

    @JsonProperty("dtCheckDt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date checkDate;

    @JsonProperty("sInstId")
    private String institutionID;

    @JsonProperty("bIsUpdated")
    private boolean updateStatus;

    @JsonProperty("sSchemeStatus")
    private String schemeStatus;

    @Transient
    @JsonProperty("sUserRole")
    @NotEmpty(groups = SchemeDetailsDateMapping.FetchGrp.class)
    private String userRole;

    @JsonProperty("bApprvSchmUpdteStatus")
    private boolean approveSchemeUpdateStatus;

    /**
     * newly added field for multiple product purpose. ie .cdl ,dpl
     */
    @JsonProperty("sProductFlag")
    private String productFlag;

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public Set<String> getSchemeIdSet() {
        return schemeIdSet;
    }

    public void setSchemeIdSet(Set<String> schemeIdSet) {
        this.schemeIdSet = schemeIdSet;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public List<SchemeInformationResponse> getSchemeInformationResponseList() {
        return schemeInformationResponseList;
    }

    public void setSchemeInformationResponseList(
            List<SchemeInformationResponse> schemeInformationResponseList) {
        this.schemeInformationResponseList = schemeInformationResponseList;
    }

    public Date getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(Date validFromDate) {
        this.validFromDate = validFromDate;
    }

    public Date getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(Date validToDate) {
        this.validToDate = validToDate;
    }

    public Set<Date> getExcludedDate() {
        return excludedDate;
    }

    public void setExcludedDate(Set<Date> excludedDate) {
        this.excludedDate = excludedDate;
    }

    public Set<Date> getIncludedDate() {
        return includedDate;
    }

    public void setIncludedDate(Set<Date> includedDate) {
        this.includedDate = includedDate;
    }

    public String getMakerID() {
        return makerID;
    }

    public void setMakerID(String makerID) {
        this.makerID = makerID;
    }

    public Date getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(Date makeDate) {
        this.makeDate = makeDate;
    }

    public String getCheckerId() {
        return checkerId;
    }

    public void setCheckerId(String checkerId) {
        this.checkerId = checkerId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public boolean isUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(boolean updateStatus) {
        this.updateStatus = updateStatus;
    }

    public String getSchemeStatus() {
        return schemeStatus;
    }

    public void setSchemeStatus(String schemeStatus) {
        this.schemeStatus = schemeStatus;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public boolean isApproveSchemeUpdateStatus() {
        return approveSchemeUpdateStatus;
    }

    public void setApproveSchemeUpdateStatus(boolean approveSchemeUpdateStatus) {
        this.approveSchemeUpdateStatus = approveSchemeUpdateStatus;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SchemeDetailsDateMapping{");
        sb.append("approveSchemeUpdateStatus=").append(approveSchemeUpdateStatus);
        sb.append(", schemeID='").append(schemeID).append('\'');
        sb.append(", schemeDesc='").append(schemeDesc).append('\'');
        sb.append(", schemeIdSet=").append(schemeIdSet);
        sb.append(", schemeInformationResponseList=").append(schemeInformationResponseList);
        sb.append(", validFromDate=").append(validFromDate);
        sb.append(", validToDate=").append(validToDate);
        sb.append(", excludedDate=").append(excludedDate);
        sb.append(", includedDate=").append(includedDate);
        sb.append(", makerID='").append(makerID).append('\'');
        sb.append(", makeDate=").append(makeDate);
        sb.append(", checkerId='").append(checkerId).append('\'');
        sb.append(", checkDate=").append(checkDate);
        sb.append(", institutionID='").append(institutionID).append('\'');
        sb.append(", updateStatus=").append(updateStatus);
        sb.append(", schemeStatus='").append(schemeStatus).append('\'');
        sb.append(", userRole='").append(userRole).append('\'');
        sb.append(", productFlag='").append(productFlag).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {

    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private SchemeDetailsDateMapping schemeDetailsDateMapping = new SchemeDetailsDateMapping();

        public SchemeDetailsDateMapping build() {
            return schemeDetailsDateMapping;
        }

        public Builder schemeID(String schemeID) {
            this.schemeDetailsDateMapping.schemeID = schemeID;
            return this;
        }

        public Builder schemeDesc(String schemeDesc) {
            this.schemeDetailsDateMapping.schemeDesc = schemeDesc;
            return this;
        }


        public Builder schemeIdSet(Set<String> schemeIdSet) {
            this.schemeDetailsDateMapping.schemeIdSet = schemeIdSet;
            return this;
        }

        public Builder schemeInformationResponseList(List<SchemeInformationResponse> schemeInformationResponseList) {
            this.schemeDetailsDateMapping.schemeInformationResponseList = schemeInformationResponseList;
            return this;
        }

        public Builder validFromDate(Date validFromDate) {
            this.schemeDetailsDateMapping.validFromDate = validFromDate;
            return this;
        }

        public Builder validToDate(Date validToDate) {
            this.schemeDetailsDateMapping.validToDate = validToDate;
            return this;
        }

        public Builder excludedDate(Set<Date> excludedDate) {
            this.schemeDetailsDateMapping.excludedDate = excludedDate;
            return this;
        }

        public Builder includedDate(Set<Date> includedDate) {
            this.schemeDetailsDateMapping.includedDate = includedDate;
            return this;
        }

        public Builder makerID(String makerID) {
            this.schemeDetailsDateMapping.makerID = makerID;
            return this;
        }

        public Builder makeDate(Date makeDate) {
            this.schemeDetailsDateMapping.makeDate = makeDate;
            return this;
        }

        public Builder checkerId(String checkerId) {
            this.schemeDetailsDateMapping.checkerId = checkerId;
            return this;
        }

        public Builder checkDate(Date checkDate) {
            this.schemeDetailsDateMapping.checkDate = checkDate;
            return this;
        }

        public Builder institutionID(String institutionID) {
            this.schemeDetailsDateMapping.institutionID = institutionID;
            return this;
        }

        public Builder updateStatus(boolean updateStatus) {
            this.schemeDetailsDateMapping.updateStatus = updateStatus;
            return this;
        }

        public Builder schemeStatus(String schemeStatus) {
            this.schemeDetailsDateMapping.schemeStatus = schemeStatus;
            return this;
        }

        public Builder userRole(String userRole) {
            this.schemeDetailsDateMapping.userRole = userRole;
            return this;
        }

        public Builder approveSchemeUpdateStatus(boolean approveSchemeUpdateStatus) {
            this.schemeDetailsDateMapping.approveSchemeUpdateStatus = approveSchemeUpdateStatus;
            return this;
        }

        public Builder productFlag(String productFlag) {
            this.schemeDetailsDateMapping.productFlag = productFlag;
            return this;
        }
    }

}
