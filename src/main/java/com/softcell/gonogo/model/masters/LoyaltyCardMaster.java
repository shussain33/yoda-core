package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 2/5/17.
 */
@Document(collection = "loyaltyCardMaster")
public class LoyaltyCardMaster {

    @JsonProperty("sLoyaltyCardNo")
    private String loyaltyCardNo;

    @JsonProperty("sCardStatus")
    private String cardStatus;

    @JsonProperty("dtDateTime")
    private Date dateTime;

    @JsonIgnore
    private String institutionId;

    @JsonIgnore
    private boolean active = true;


    public String getLoyaltyCardNo() {
        return loyaltyCardNo;
    }

    public void setLoyaltyCardNo(String loyaltyCardNo) {
        this.loyaltyCardNo = loyaltyCardNo;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LoyaltyCardMaster that = (LoyaltyCardMaster) o;

        if (active != that.active) return false;
        if (loyaltyCardNo != null ? !loyaltyCardNo.equals(that.loyaltyCardNo) : that.loyaltyCardNo != null)
            return false;
        if (cardStatus != null ? !cardStatus.equals(that.cardStatus) : that.cardStatus != null) return false;
        if (dateTime != null ? !dateTime.equals(that.dateTime) : that.dateTime != null) return false;
        return !(institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null);

    }

    @Override
    public int hashCode() {
        int result = loyaltyCardNo != null ? loyaltyCardNo.hashCode() : 0;
        result = 31 * result + (cardStatus != null ? cardStatus.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);

        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LoyaltyCardMaster{");
        sb.append("loyaltyCardNo='").append(loyaltyCardNo).append('\'');
        sb.append(", cardStatus='").append(cardStatus).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }
}
