/**
 * yogeshb1:01:37 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author yogeshb
 *
 */
@Document(collection = "CarSurrogateMaster")
public class CarSurrogateMaster extends AuditEntity {

    @JsonProperty("sSeg")
    private String segment;

    @JsonProperty("sClas")
    private String classification;

    @JsonIgnore
    private String institutionID;

    @JsonIgnore
    private boolean active = true;

    @JsonIgnore
    private Date date;

    public CarSurrogateMaster() {
        super();
    }

    public CarSurrogateMaster(String segment, String classification,
                              String institutionID, boolean active, Date date) {
        super();
        this.segment = segment;
        this.classification = classification;
        this.institutionID = institutionID;
        this.active = active;
        this.date = date;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CarSurrogateMaster [segment=" + segment + ", classification="
                + classification + ", institutionID=" + institutionID
                + ", active=" + active + ", date=" + date + "]";
    }

}
