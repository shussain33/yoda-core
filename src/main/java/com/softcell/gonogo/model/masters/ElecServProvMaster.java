package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by anupamad on 19/7/17.
 */
@Data
@Document(collection = "elecServProvMaster")
public class ElecServProvMaster extends AuditEntity {

    @JsonProperty("sCode")
    private String code;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sName")
    private String name;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active = true;
}
