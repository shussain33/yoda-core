/**
 * kishorp9:53:40 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author kishorp
 */
@Document(collection = "employerMaster")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployerMaster extends AuditEntity {

    @JsonProperty("sEmpID")
    private String employerId;

    @JsonProperty("sEmpName")
    private String employerName;

    @JsonProperty("sDsaEmpFl")
    private String dasEmpFlag;

    @JsonProperty("sWrkEmpFl")
    private String worksideEmpFlag;

    @JsonProperty("sEmpCat")
    private String employerCatg;

    @JsonProperty("sLemEndDt")
    private String lemEndDate;

    @JsonIgnore
    private boolean deteted;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private Date insertDate = new Date();

    @JsonIgnore
    private boolean active = true;

    @JsonProperty("sIndstryType")
    private String industryType;

    @JsonProperty("sBsnesTyp")
    private String businessType;

    @JsonProperty("sPrfsnTyp")
    private String professionType;

    @JsonProperty("sIciciEmpCode")
    private String iciciEmpCode;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result
                + ((businessType == null) ? 0 : businessType.hashCode());
        result = prime * result
                + ((dasEmpFlag == null) ? 0 : dasEmpFlag.hashCode());
        result = prime * result + (deteted ? 1231 : 1237);
        result = prime * result
                + ((employerCatg == null) ? 0 : employerCatg.hashCode());
        result = prime * result
                + ((employerId == null) ? 0 : employerId.hashCode());
        result = prime * result
                + ((employerName == null) ? 0 : employerName.hashCode());
        result = prime * result
                + ((industryType == null) ? 0 : industryType.hashCode());
        result = prime * result
                + ((insertDate == null) ? 0 : insertDate.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((lemEndDate == null) ? 0 : lemEndDate.hashCode());
        result = prime * result
                + ((professionType == null) ? 0 : professionType.hashCode());
        result = prime * result
                + ((worksideEmpFlag == null) ? 0 : worksideEmpFlag.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EmployerMaster other = (EmployerMaster) obj;
        if (active != other.active)
            return false;
        if (businessType == null) {
            if (other.businessType != null)
                return false;
        } else if (!businessType.equals(other.businessType))
            return false;
        if (dasEmpFlag == null) {
            if (other.dasEmpFlag != null)
                return false;
        } else if (!dasEmpFlag.equals(other.dasEmpFlag))
            return false;
        if (deteted != other.deteted)
            return false;
        if (employerCatg == null) {
            if (other.employerCatg != null)
                return false;
        } else if (!employerCatg.equals(other.employerCatg))
            return false;
        if (employerId == null) {
            if (other.employerId != null)
                return false;
        } else if (!employerId.equals(other.employerId))
            return false;
        if (employerName == null) {
            if (other.employerName != null)
                return false;
        } else if (!employerName.equals(other.employerName))
            return false;
        if (industryType == null) {
            if (other.industryType != null)
                return false;
        } else if (!industryType.equals(other.industryType))
            return false;
        if (insertDate == null) {
            if (other.insertDate != null)
                return false;
        } else if (!insertDate.equals(other.insertDate))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (lemEndDate == null) {
            if (other.lemEndDate != null)
                return false;
        } else if (!lemEndDate.equals(other.lemEndDate))
            return false;
        if (professionType == null) {
            if (other.professionType != null)
                return false;
        } else if (!professionType.equals(other.professionType))
            return false;
        if (worksideEmpFlag == null) {
            if (other.worksideEmpFlag != null)
                return false;
        } else if (!worksideEmpFlag.equals(other.worksideEmpFlag))
            return false;
        return true;
    }

    public boolean isDeteted() {
        return deteted;
    }

    public void setDeteted(boolean deteted) {
        this.deteted = deteted;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getEmployerId() {
        return employerId;
    }

    public void setEmployerId(String employerId) {
        this.employerId = employerId;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getDasEmpFlag() {
        return dasEmpFlag;
    }

    public void setDasEmpFlag(String dasEmpFlag) {
        this.dasEmpFlag = dasEmpFlag;
    }

    public String getWorksideEmpFlag() {
        return worksideEmpFlag;
    }

    public void setWorksideEmpFlag(String worksideEmpFlag) {
        this.worksideEmpFlag = worksideEmpFlag;
    }

    public String getEmployerCatg() {
        return employerCatg;
    }

    public void setEmployerCatg(String employerCatg) {
        this.employerCatg = employerCatg;
    }

    public String getLemEndDate() {
        return lemEndDate;
    }

    public void setLemEndDate(String lemEndDate) {
        this.lemEndDate = lemEndDate;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the industryType
     */
    public String getIndustryType() {
        return industryType;
    }

    /**
     * @param industryType the industryType to set
     */
    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    /**
     * @return the businessType
     */
    public String getBusinessType() {
        return businessType;
    }

    /**
     * @param businessType the businessType to set
     */
    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    /**
     * @return the professionType
     */
    public String getProfessionType() {
        return professionType;
    }

    /**
     * @param professionType the professionType to set
     */
    public void setProfessionType(String professionType) {
        this.professionType = professionType;
    }

    public String getIciciEmpCode() { return iciciEmpCode; }

    public void setIciciEmpCode(String iciciEmpCode) { this.iciciEmpCode = iciciEmpCode; }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("EmployerMaster [employerId=");
        builder.append(employerId);
        builder.append(", employerName=");
        builder.append(employerName);
        builder.append(", dasEmpFlag=");
        builder.append(dasEmpFlag);
        builder.append(", worksideEmpFlag=");
        builder.append(worksideEmpFlag);
        builder.append(", employerCatg=");
        builder.append(employerCatg);
        builder.append(", lemEndDate=");
        builder.append(lemEndDate);
        builder.append(", deteted=");
        builder.append(deteted);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", insertDate=");
        builder.append(insertDate);
        builder.append(", active=");
        builder.append(active);
        builder.append(", industryType=");
        builder.append(industryType);
        builder.append(", businessType=");
        builder.append(businessType);
        builder.append(", professionType=");
        builder.append(professionType);
        builder.append("]");
        return builder.toString();
    }


}
