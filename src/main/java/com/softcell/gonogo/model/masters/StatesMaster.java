/**
 * vinodk6:08:06 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author vinodk
 *
 */
@Document(collection = "StatesMaster")
public class StatesMaster {
    @JsonProperty("sStateDesc")
    private String stateDescription;

    /**
     * @return the stateDescription
     */
    public String getStateDescription() {
        return stateDescription;
    }

    /**
     * @param stateDescription
     *            the stateDescription to set
     */
    public void setStateDescription(String stateDescription) {
        this.stateDescription = stateDescription;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StatesMaster [stateDescription=" + stateDescription + "]";
    }
}
