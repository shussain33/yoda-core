package com.softcell.gonogo.model.masters;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * It is LOS related master.
 * It is use to get broker id and product id return broker id else null.
 *
 * @author bhuvneshk
 */

@Document(collection = "dsaProductMaster")
public class DSAProductMaster extends AuditEntity {

    @JsonProperty("sBrokerId")
    private String brokerId;

    @JsonProperty("sProductId")
    private String productId;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonIgnore
    private boolean active;

    public String getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(String brokerId) {
        this.brokerId = brokerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DSAProductMaster [brokerId=");
        builder.append(brokerId);
        builder.append(", productId=");
        builder.append(productId);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append("]");
        return builder.toString();
    }

}
