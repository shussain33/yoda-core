/**
 * yogeshb11:57:18 am  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.masters;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb
 *         <pre>
 *                         		<em>DealerSchemeMapping</em>
 *                         </pre>
 *         <p>
 *         This is used for mongo collection and it is linkage between dealer and Scheme.
 *         </p>
 */

@Document(collection = "dealerSchemeMapping")
public class DealerSchemeMapping {

    private String dealerID;
    private String schemeID;
    private String counterCode;
    private String enabledFlag;
    private String inclusionFlag;
    private String makerID;
    private String makerDate;
    private String authID;
    private String authDate;
    private String institutionID;


    public String getDealerID() {
        return dealerID;
    }

    public void setDealerID(String dealerID) {
        this.dealerID = dealerID;
    }

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getCounterCode() {
        return counterCode;
    }

    public void setCounterCode(String counterCode) {
        this.counterCode = counterCode;
    }

    public String getEnabledFlag() {
        return enabledFlag;
    }

    public void setEnabledFlag(String enabledFlag) {
        this.enabledFlag = enabledFlag;
    }

    public String getInclusionFlag() {
        return inclusionFlag;
    }

    public void setInclusionFlag(String inclusionFlag) {
        this.inclusionFlag = inclusionFlag;
    }

    public String getMakerID() {
        return makerID;
    }

    public void setMakerID(String makerID) {
        this.makerID = makerID;
    }

    public String getMakerDate() {
        return makerDate;
    }

    public void setMakerDate(String makerDate) {
        this.makerDate = makerDate;
    }

    public String getAuthID() {
        return authID;
    }

    public void setAuthID(String authID) {
        this.authID = authID;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    @Override
    public String toString() {
        return "DealerSchemeMapping [dealerID=" + dealerID + ", schemeID="
                + schemeID + ", counterCode=" + counterCode + ", enabledFlag="
                + enabledFlag + ", inclusionFlag=" + inclusionFlag
                + ", makerID=" + makerID + ", makerDate=" + makerDate
                + ", authID=" + authID + ", authDate=" + authDate
                + ", institutionID=" + institutionID + "]";
    }


}
