package com.softcell.gonogo.model.mifin.IMD;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IMDDetails {

    @JsonProperty("APPLICANTION_REF_NO")
    private String appRefNo;

    @JsonProperty("Mode_OF_PAYMENT")
    private String modeOfPayment;

    @JsonProperty("UTR")
    private String utr;

    @JsonProperty("IMD_AMOUNT")
    private String imdAmt;

    @JsonProperty("TRANSACTION_DATE")
    private String transactionDt;

    @JsonProperty("CLEARANCE_DATE")
    private String clearanceDt;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("BRANCH")
    private String branch;

    @JsonProperty("CUSTOMER_ADDRESS")
    private String custAddress;
}
