package com.softcell.gonogo.model.mifin.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0302 on 1/10/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CrmResponseDetails {

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("MESSAGE")
    private String message;

    @JsonProperty("UNIQUE_LEAD_ID")
    private String uniqueLeadId;

    @JsonProperty("CUSTOMER_DETAIL")
    private List<CustomerDetail> customerDetail;

    @JsonProperty("CUSTOMER_DETAILS")
    private List<CustomerDetail> customerDetails;
}
