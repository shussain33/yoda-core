package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Valuation {

    @JsonProperty("REPLACEMENT_COST")
    private String replacementCost;

    @JsonProperty("PROPERTY_STATUS")
    private String propertyStatus;

    @JsonProperty("VALUATORNAME")
    private String valuatorName;

    @JsonProperty("PROPERTY_AGE")
    private String propertyAge = "0";


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("EVALUATION_DATE")
    private String evaluationDate;

    @JsonProperty("PRO_RESIDUALAGE")
    private String proResidualAge;

    @JsonProperty("PROPERTY_USAGE")
    private String propertyUsage;

    @JsonProperty("MARKET_VALUE")
    private String marketValue;
}
