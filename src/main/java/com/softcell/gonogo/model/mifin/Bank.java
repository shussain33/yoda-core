package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bank {

    @JsonProperty("BANKDETAIL")
    private List<BankDetails> bankDetailsList;

}
