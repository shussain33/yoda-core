package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDetails {
    @JsonProperty("ADDRESS_TYPE")
    private String addressType;

    @JsonProperty("MAILING_ADDRESS")
    private String mailingAddress;

    @JsonProperty("NAMEOFCOMPANY")
    private String nameOfCompany;

    @JsonProperty("ADD_1")
    private String add1;

    @JsonProperty("ADD_2")
    private String add2;

    @JsonProperty("ADD_3")
    private String add3;

    @JsonProperty("ADD_CITY")
    private String city;

    @JsonProperty("ADD_DIST")
    private String dist;

    @JsonProperty("ADD_LANDMARK")
    private String landmark;

    @JsonProperty("ADD_STATE")
    private String state;

    @JsonProperty("ADD_PINCODE")
    private String pincode;

    @JsonProperty("ADD_YEAR_OF_STAY")
    private String yearOfStay;

    @JsonProperty("ADD_PHONE_STD_CODE")
    private String phoneStdCode;

    @JsonProperty("ADD_PHONE")
    private String phone;

    @JsonProperty("ADD_MOBILE")
    private String mobile;

    @JsonProperty("ADD_EMAIL")
    private String email;

    @JsonProperty("OCCUPANCY_STATUS")
    private String occupancyStatus;

    @JsonProperty("ADD_MONTH_OF_STAY")
    private String monthsOfStay;
}
