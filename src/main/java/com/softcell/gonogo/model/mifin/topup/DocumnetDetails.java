package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumnetDetails {
    @JsonProperty("DOCUMENTTYPE")
    private String documentType;

    @JsonProperty("DOCUMENTNAME")
    private String documentName;

    @JsonProperty("DOCUMENTDESCRIPTION")
    private String documentDescription;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("RECIEVEDDATE")
    private String receivedDate;

}
