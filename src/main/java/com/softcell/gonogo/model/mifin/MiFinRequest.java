package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.HeaderKey;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by suhasini on 13/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MiFinRequest {

    @JsonProperty("AUTHENTICATION")
    private Authentication authentication;

    @JsonProperty("BASICINFO")
    private BasicInfo basicInfo;


    @JsonProperty("CREDITSANCTION")
    private CreditSanction creditSanction;

    @JsonProperty("CHARGES")
    private Charges charges;

    @JsonProperty("VERIFICATION")
    private Verification verification;

    @JsonProperty("DOCUMENTS")
    private Documents documents;

    @JsonProperty("ASSETDETAIL")
    private Asset asset;

    @JsonProperty("BANKDETAILS")
    private Bank bank;

    @JsonProperty("DISBURSALDETAIL")
    private Disbursal disbusal;

    @JsonProperty("CUSTOMER")
    private Customer customer;
}
