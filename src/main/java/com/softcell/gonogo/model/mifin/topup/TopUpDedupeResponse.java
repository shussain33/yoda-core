package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TopUpDedupeResponse extends MiFinCallLog {
    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("MESSAGE")
    private String message;

    //for Applicant data API
    @JsonProperty("APPLICANTLIST")
    private List<Applicant> applicantlist;

    @JsonProperty("DEDUPEINFO")
    private DedupeInfo dedupeInfo ;

    @JsonProperty("APPLICANTDETAILS")
    private ApplicantDetails applicantDetailsList;


    @JsonProperty("MATCHEDAPPLICANTS")
    private List<MatchApplicantDetails> matchApplicantList;

    //for dedupe APi call response
    @JsonProperty("MATCH_COUNT")
    private String matchCount;

    //for MIFIN LOAN DETAILS
    private List<LoanSummary> loanSummaryList;
    private LoanSummary loanSummary;

    @Transient
    @JsonProperty("LMSDETAIL")
    Object object ;

    @Transient
    @JsonProperty("LOANSUMMARY")
    Object LoanSummaryObject ;

    @JsonProperty("TOTALEXPOSURE")
    @NotEmpty
    public double totalExposure;

    @JsonProperty("sTotalValueToBeConsider")
    @NotEmpty
    public double totalValueToBeConsider;

    private LoanSummaryDetail loanSummaryDeatil;
    private List<LoanSummaryDetail> loanSummaryDeatilList;
    private List<LmsDetail> lmsDetailsList;
    private LmsDetail lmsDetail;
    private String topUpType;

}






