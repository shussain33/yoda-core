package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Constant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "miFinAmbitCallLog")
public class MiFinAmbitCallLog {

    @JsonProperty(value = "sRequestType" ,defaultValue = Constant.BLANK)
    private String requestType;

    @JsonProperty("api")
    private String api;

    @JsonProperty("refId")
    private String refId;

    @JsonProperty("callDate")
    private Date callDate = new Date();

    @JsonProperty("request")
    private Object miFinRequest;

    @JsonProperty("response")
    private Object miFinResponse;

    @JsonProperty("sAppId")
    private String appId;

    //To store request and response in String format
    @JsonProperty("sStringRequestFormat")
    private String request;

    @JsonProperty("sStringResponseFormat")
    private String response;

    @JsonProperty("bLoanDetailApi")
    public String apiHit;
}