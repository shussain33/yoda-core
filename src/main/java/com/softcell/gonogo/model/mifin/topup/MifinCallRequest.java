package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
public class MifinCallRequest
{
    @JsonProperty("sRequest")
    String request;

    @JsonProperty("sReqType")
    private String reqType;
}
