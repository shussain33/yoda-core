package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChargeDetails {

    @JsonProperty("CHARGEID")
    private String chargeId;

    @JsonProperty("CHARGE_AMOUNT")
    private String chargeAmt;

    @JsonProperty("GST_ON_CHARGE")
    private String gstOnCharge;

    @JsonProperty("TOTAL_CHARGE_AMOUNT")
    private String totalChargeAmt;

    //originationField
    @JsonProperty("CHARGE_APPLIED_ON")
    private String chargeAppliedOn;
}
