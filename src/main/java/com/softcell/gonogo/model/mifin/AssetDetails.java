package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssetDetails {

    @JsonProperty("PROPERTYTYPE")
    private String propertyType = "";

    @JsonProperty("PROPERTYADDRESS1")
    private String propertyAdd1 = "";

    @JsonProperty("PROPERTYADDRESS2")
    private String propertyAdd2 = "";

    @JsonProperty("PROPERTYADDRESS3")
    private String propertyAdd3 = "";

    @JsonProperty("LANDMARK")
    private String landmark = "";

    @JsonProperty("LOCALITY")
    private String locality = "";

    @JsonProperty("CITY")
    private String city = "";

    @JsonProperty("STATE")
    private String state = "";

    @JsonProperty("ZIPCODE")
    private String zipcode = "";

    @JsonProperty("OWNER_FNAME")
    private String ownerFName = "";

    @JsonProperty("OWNER_MNAME")
    private String ownerMName = "";

    @JsonProperty("OWNER_LNAME")
    private String ownerLName = "";

    @JsonProperty("VALUATION_DETAIL")
    private List<Valuation> valuationDetailList;
}
