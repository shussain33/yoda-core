package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreditSanction
{
    @JsonProperty("SANCTIONAPPROVEDBY")
    private String sanctionApprovedBy;

    @JsonProperty("PRODUCT")
    private String productId;

    @JsonProperty("SCHEMEID")
    private String schemeId;

    @JsonProperty("APPLIED_LOAN_AMOUNT")
    private String appliedLoanAmount;

    @JsonProperty("SANCTIONED_AMOUNT")
    private String sanctionedAmount;

    @JsonProperty("TENOR")
    private String Tenor;

    @JsonProperty("ROI")
    private String roi;

    @JsonProperty("INSTALLMENTTYPE")
    private String InstallmentType;

    @JsonProperty("FREQUENCY")
    private String frequency;

    @JsonProperty("INTERESTTYPE")
    private String interestType;

    @JsonProperty("INSTTYPE")
    private String instType;

    @JsonProperty("EMISTARTDATE")
    private String emiStartDate;

    @JsonProperty("DISBURSALDATE")
    private String disbursalDate;

    @JsonProperty("ADVANCE_EMI_FLAG")
    private String advanceEmiFlag;

    @JsonProperty("NO_ADVANCE_INSTALMENT")
    private String noAdvannstallment;

    @JsonProperty("LTV")
    private String ltv;

    @JsonProperty("DBR")
    private String dbr;

    @JsonProperty("REPAYMENTMODE")
    private String repayment;

    @JsonProperty("NO_ADVANCED_INSTALLMENT")
    private String noAdvancedInstallment;

}
