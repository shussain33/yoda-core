package com.softcell.gonogo.model.mifin.topup;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BasicInfo  {

    //1 st api MIFIN applicant details
    @JsonProperty( "PROSPECT_ID")
    private String prospectCode;

    @JsonProperty("APPLICANT_ID")
    private String applicantId;

    @JsonProperty("LOAN_NO")
    private String lan;

    @JsonProperty("PAN_NO")
    private String pan;


    //MIFIN dedupe call

    @JsonProperty("APPLICANTCODE")
    private String applicantCode;

    @JsonProperty("CUSTOMER_ID")
    private String custId;

    @JsonProperty("MOBILE_NO")
    private String mobNo;

    @JsonProperty("BATCHID")
    private String batchID;

    @JsonProperty("FNAME")
    private String firstName;

    @JsonProperty("MNAME")
    private String middleName;

    @JsonProperty("LNAME")
    private String lastName;


    @JsonProperty("FATHERNAME")
    private String fatherName;

    @JsonProperty("MOTHERNAME")
    private String motherName;


    @JsonProperty("EMAIL")
    private String email;


    @JsonProperty("DL")
    private String dl;


    @JsonProperty("RCARDNUMBER")
    private String rationCardNumber;

    @JsonProperty("VOTERIDCARDNO")
    private String voterCardNo;

    @JsonProperty( "PASSPORTNO")
    private String passPortNo;

    @JsonProperty("DOB")
    private String dob;


    @JsonProperty( "GENDER")
    private String gender;


    @JsonProperty("CURRENT_CITY")
    private String currentCity;


    @JsonProperty("CUST_TYPE")
    private String custType;

    @JsonProperty("COMPANY")
    private String compnay;

    @JsonProperty("ALL_COMPANY")
    private String allCompany;


    @JsonProperty( "COMPANY_NAME_CORPORATE")
    private String companyNameCorporate;

    @JsonProperty("COMPANY_ADDRESS")
    private String companyAddress;


    @JsonProperty("COMPANY_CITY")
    private String companyCity;


    @JsonProperty("COMPANY_ZIP")
    private String companyZip;

    @JsonProperty("COMPANY_PHONE1")
    private String companyPhone1;


    @JsonProperty("COMPANY_FAX")
    private String companyFAx;

    @JsonProperty( "COMPANY_EMAIL")
    private String companyEmail;

    @JsonProperty( "DATE_OF_INCORPORATION")
    private String doi;

    @JsonProperty("ADDRESSTYPE")
    private String addressType;

    @JsonProperty("BUILDINGNAME")
    private String buildingName;

    @JsonProperty( "FLAT")
    private String flat;

    @JsonProperty( "FLOOR")
    private String floor;

    @JsonProperty("LANDMARK")
    private String landMark;

    @JsonProperty( "LOCALITY")
    private String locality;

    @JsonProperty( "CITY")
    private String city;


    @JsonProperty("STATE")
    private String state;

    @JsonProperty( "COUNTRY")
    private String country;

    @JsonProperty("ZIP")
    private String zip;

    @JsonProperty("LANDLINE1")
    private String landLine1;

    @JsonProperty("LANDLINE2")
    private String landLine2;

    @JsonProperty("CELL")
    private String cell;

    @JsonProperty("CELL2")
    private String cell2;

    @JsonProperty("CELL3")
    private String cell3;

    @JsonProperty("PROSPECTCODE")
    private List<String> prospectCodeList;

    @JsonProperty("LOANDETAILTYPE")
    private  String loanDetailType;

    @JsonProperty("COMPANY_STATE")
    private  String companyState;

    //4 th MiFinLoan Detail
    @JsonProperty("OLD_PR_NO")
    private String oldPrNo;

    @JsonProperty( "TOPUPTYPE")
    private String topUpType;

    //Required for response
    @JsonProperty("BRANCH")
    private String branch;

    @JsonProperty("PURPOSE_OF_LOAN")
    private String purposeOfLoan;

    @JsonProperty("RELATIONSHIP_MANAGER")
    private String relationshipManager;

    @JsonProperty("SOURCING_CHANNEL_TYPE")
    private String sourcingChannelType;

    @JsonProperty("SOURCING_CHANNEL_NAME")
    private String sourcingChannelName;

    @JsonProperty("APPFORMNO")
    private String appFormNo;

    @JsonProperty("FULLFILLMENT_CHANNEL_TYPE")
    private String fullfillmentChannelType;

    @JsonProperty("FULLFILLMENT_CHANNEL_NAME")
    private String fullfillmentChannelName;

}

