package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suhasini on 17/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RepaymentScheduleDetails {

    @JsonProperty("LOAN_AMT")
    private String loanAmt;

    @JsonProperty("INTEREST_RATE")
    private String interestRate;

    @JsonProperty("TENURE")
    private String tenure;

    @JsonProperty("INSTALMENT_NO")
    private String instalmentNo;

    @JsonProperty("EMI_AMT")
    private String emiAmt;

    @JsonProperty("INTEREST")
    private String interest;

    @JsonProperty("PRINCIPAL")
    private String principal;

    @JsonProperty("BAL_PRIN")
    private String balPrin;

    @JsonProperty("DUE_DATE")
    private String dueDate;
}
