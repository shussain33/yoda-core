package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by suhasini on 13/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasicInfo {

    @JsonProperty("BRANCH")
    private String branch;

    @JsonProperty("RELATIONSHIP_MANAGER")
    private String relationshipManager;

    @JsonProperty("PURPOSE_OF_LOAN")
    private String purposeOfLoan;

    @JsonProperty("SOURCING_CHANNEL_TYPE")
    private String sourcingChannelType;

    @JsonProperty("SOURCING_CHANNEL_NAME")
    private String sourcingChannelName;

    @JsonProperty("APPFORMNO")
    private String appFormNo;

    @JsonProperty("FULLFILLMENT_CHANNEL_TYPE")
    private String fullfillmentChannelType;

    @JsonProperty("FULFILLMENT_CHANNEL_NAME")
    private String fullfillmentChannelName;

    // Fields for topup
    @JsonProperty("OLD_PROSPECTCODE")
    private List<String> oldProspectCode;

    //'G' For GROSS TOPUP and 'P' for parallel TOPUP
    @JsonProperty("LOANTOPUPTYPE")
    private String loanTopupType;

    //Origination tag
    @JsonProperty("BRANCHCODE")
    private String branchCode;

    @JsonProperty("COMPANY_NAME")
    private String companyName;

    @JsonProperty("CONSTID")
    private String constId;

    @JsonProperty("COORGIN_FLAG")
    private String cooriginFlag ;

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

    @JsonProperty("PROSPECTCODE")
    private String prospectCodee;

    @JsonProperty("PREVIOUS_TRANCHE_AMOUNT")
    private String previousTranchAmount;

    @JsonProperty("FINAL_TRANCHE_FLAG")
    private String finalTranchFlag;

    @JsonProperty("FINAL_TRANCHE_AMOUNT")
    private String finalTranchAmount;

    @JsonProperty("TRANCHE_DATE")
    private String tranchDate;
}
