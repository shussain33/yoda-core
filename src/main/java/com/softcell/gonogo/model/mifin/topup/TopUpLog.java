package com.softcell.gonogo.model.mifin.topup;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "topupLog")
public class TopUpLog
{
    @JsonProperty("api")
    private String api;

    @JsonProperty("refId")
    private String refId;

    @JsonProperty("TopUp")
    private TopUpMifinDedupeRequest topUpMifinDedupeRequest;

    @JsonProperty("respponse")
    private TopUpDedupeResponse topUpDedupeResponse;
}
