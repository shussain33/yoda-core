package com.softcell.gonogo.model.mifin.topup;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanSummaryDetail implements  Comparable<LoanSummaryDetail> {
    @JsonProperty("PROSPECTCODE")
    private String PROSPECTCODE;

    @JsonProperty("APPLICANTCODE")
    private String applicantCode;

    @JsonProperty("BASICINFO")
    private BasicInfo basicInfo ;

    @JsonProperty("CREDITSANCTION")
    private CreditSanction creditSanction;

    @JsonProperty("CHARGES")
    private Charges charges;

    @JsonProperty("VERIFCATION")
    private Verification verification;


    @JsonProperty("DOCUMENTS")
    private Documnet documnet;

    @JsonProperty("ASSETDETAIL")
    private AssetDetail assetDetails;

    @JsonProperty("LMSDETAIL")
    private LmsDetail lmsDetail;

    @JsonProperty("BANKDETAILS")
    private BankDetail bankDetail;

    @JsonProperty("DISBURSALDETAIL")
    private DisbursalDetail disbursalDetail;

    @JsonProperty("CUSTOMER")
    private Customer customer;

    @Override
    public int compareTo(LoanSummaryDetail o) {

        return this.PROSPECTCODE.compareTo(o.PROSPECTCODE);
    }
}
