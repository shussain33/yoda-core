package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 19/4/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Verification {

    @JsonProperty("VARIFICATION_DETAIL")
    private List<VerificationDetailMiFin> verificationDetailsList;
}
