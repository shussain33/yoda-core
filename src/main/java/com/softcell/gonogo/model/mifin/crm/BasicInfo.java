package com.softcell.gonogo.model.mifin.crm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 23/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasicInfo {

    @JsonProperty("CUSTOMER_FULL_NAME")
    private String customerFullName;

    @JsonProperty("MOBILE_NUMBER")
    private String mobileNumber;
}
