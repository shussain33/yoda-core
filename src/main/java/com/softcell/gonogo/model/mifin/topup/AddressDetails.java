package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDetails {
    @JsonProperty("ADDRESSTYPE")
    private String addressType;

    @JsonProperty("MAILING_ADDRESS")
    private String mailingAddress;

    @JsonProperty("NAMEOFCOMPANY")
    private String nameOfCompany;

    @JsonProperty("ADD_1")
    private String add1;

    @JsonProperty("ADD_2")
    private String add2;


    @JsonProperty("ADD_3")
    private String add3;

    @JsonProperty("ADD_CITY")
    private String addCity;

    @JsonProperty("ADD_DIST")
    private String addDist;

    @JsonProperty("ADD_LANDMARK")
    private String addLandmark;

    @JsonProperty("ADD_STATE")
    private String addState;

    @JsonProperty("ADD_PINCODE")
    private String addPincode;


    @JsonProperty("ADD_YEAR_OF_STAY")
    private String addYearOfStay;

    @JsonProperty("ADD_PHONE_STD_CODE")
    private String addPhoneStdCode;

    @JsonProperty("ADD_PHONE")
    private String addPhone;

    @JsonProperty("ADD_MOBILE")
    private String addMobile;

    @JsonProperty("ADD_EMAIL")
    private String addEmail;

    @JsonProperty("OCCUPANCY_STATUS")
    private String occupancyStatus;

    @JsonProperty("ADD_MONTH_OF_STAY")
    private String addMonthOfStay;

    @JsonProperty("QUALIFICATION")
    private String qualification;


    @JsonProperty("ADD_YEARS_OF_STAY")
    private String addYearsOfStay;
}
