package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankDetails {

    @JsonProperty("MICR")
    private String micr;

    @JsonProperty("IFSC_CODE")
    private String ifscCode;

    @JsonProperty("BANK_ID")
    private String bankId;

    @JsonProperty("BANK_BRANCH_ID")
    private String branchId;

    @JsonProperty("BANK_BRANCH_CITY_ID")
    private String branchCityId;

    @JsonProperty("CUSTOMER_AC_NUMBER")
    private String acNo;

    @JsonProperty("BENEFICERY_NAME")
    private String beneficeryName;

    @JsonProperty("BANKTYPE")
    private String bankType;

    @JsonProperty("ACCOUNT_TYPE")
    private String accountType;

    //TODO
    @JsonProperty("NOOFYEAR")
    private int noOfYear;

    //TODO @JsonProperty("NOOFYEAR")
}
