package com.softcell.gonogo.model.mifin.topup;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDetail
{
    @JsonProperty("ADDRESSTYPE")
    @NotEmpty
    public String addressType;

    @JsonProperty("ADDRESS")
    @NotEmpty
    public String address;

    @JsonProperty("CITY")
    @NotEmpty
    public String city;

    @JsonProperty("STATE")
    @NotEmpty
    public String state;

    @JsonProperty("COUNTRY")
    @NotEmpty
    public String country;

    @JsonProperty("PINCODE")
    @NotEmpty
    public String pinCode;

}
