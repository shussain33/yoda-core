package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetDetails {

    @JsonProperty("PROPERTYTYPE")
    private String propertyType;

    @JsonProperty("PROPERTYADDRESS1")
    private String propertyAddress1;

    @JsonProperty("PROPERTYADDRESS2")
    private String propertyAddress2;

    @JsonProperty("PROPERTYADDRESS3")
    private String propertyAddress3;

    @JsonProperty("LANDMARK")
    private String landmark;

    @JsonProperty("LOCALITY")
    private String locality;

    @JsonProperty("CITY")
    private String city;

    @JsonProperty("STATE")
    private String state;

    @JsonProperty("ZIPCODE")
    private String zipCode;

    @JsonProperty("OWNER_FNAME")
    private String ownerName;

    @JsonProperty("OWNER_MNAME")
    private String ownerMname;

    @JsonProperty("OWNER_LNAME")
    private String ownerLname;

    @Deprecated
    @JsonProperty("VALUATORNAME")
    private String valuatorName;

    @Deprecated
    @JsonProperty("PROPERTY_USAGE")
    private String propertyUsage;

    @Deprecated
    @JsonProperty("PROPERTY_STATUS")
    private String propertyStatus;

    @Deprecated
    @JsonProperty("PROPERTY_AGE")
    private String propertyAge;

    @Deprecated
    @JsonProperty("PRO_RESIDUALAGE")
    private String proResidualAge;

    @Deprecated
    @JsonProperty("MARKET_VALUE")
    private String marketValue;

    @Deprecated
    @JsonProperty("REPLACEMENT_COST")
    private String replacementCost;

    @Deprecated
    @JsonProperty("EVALUATION_DATE")
    private String evaluationDate;

    @JsonProperty("VALUATION_DETAIL")
    private Object valuationStructure;

    private List<ValuationDetail> valuationDetailList;




}
