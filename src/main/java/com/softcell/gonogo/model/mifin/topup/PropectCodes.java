package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PropectCodes {

    @JsonProperty("SCHEME")
    @NotEmpty
    public String scheme;

    @JsonProperty("PROSPECTCODE")
    @NotEmpty
    public String prospectCode;

    @JsonProperty("LOAN_STATUS")
    @NotEmpty
    public String loanStatus;

    @JsonProperty("SANCTIONED_AMOUNT")
    @NotEmpty
    public String sanctionedAmount;
}
