package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditSanction {

    @JsonProperty("SANCTIONAPPROVEDBY")
    private String sanctionAprvBy;

    @JsonProperty(value = "PRODUCTID", defaultValue = "SBL")
    private String productId;

    @JsonProperty("SCHEMEID")
    private String schemeId;

    @JsonProperty("APPLIED_LOAN_AMOUNT")
    private String appliedLoanAmt;

    @JsonProperty("SANCTIONED_AMOUNT")
    private String sanctionedAmt;

    @JsonProperty("TENOR")
    private String tenor;

    @JsonProperty("ROI")
    private String roi;

    @JsonProperty("INSTALLMENTTYPE")
    private String installmentType;

    @JsonProperty("FREQUENCY")
    private String frequency;

    @JsonProperty("INTERESTTYPE")
    private String interestType;

    @JsonProperty("EMI_START_DATE")
    private String emiStartDate;

    @JsonProperty("DISBURSAL_DATE")
    private String disbDate;

    @JsonProperty("ADVANCE_EMI_FLAG")
    private String advEmiFlag;

    @JsonProperty("NO_ADVANCE_INSTALMENT")
    private String noOfAdvInstallments;

    @JsonProperty("LTV")
    private String ltv;

    @JsonProperty("DBR")
    private String dbr;

    @JsonProperty("REPAYMENTMODE")
    private String repaymentMode;

    //origination tag
    @JsonProperty("PARTNER_BANK_RATIO")
    private String partnerBankRatio;

    @JsonProperty("PROMOTION_DESC")
    private String promationDesc;

    @JsonProperty("PSL_CODE")
    private String pslCode;

    @JsonProperty("PSL_FLAG")
    private String pslFlag;

    @JsonProperty("ROI_OF_PARTNER_BANK")
    private String roiOfPartnerBank;

    @JsonProperty("SANCTIONED_AMOUNT_PARTNER_BANK")
    private String sanctionedAmountPartnerBank;

    @JsonProperty("SANCTIONED_AMOUNT_SBFC")
    private String sanctionedAmountSbfc;

    @JsonProperty("SBFC_RATIO")
    private String sbfcRatio;

    @JsonProperty("SCHEMEID_OF_PARTNER_BANK")
    private String schemeIdOfPaertnerBank;

    @JsonProperty("TRANCHEAMOUNT")
    private String tranchAmount;

    @JsonProperty("TRANCHEAPPLICABLE")
    private String trancheApplicable;
}
