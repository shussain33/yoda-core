package com.softcell.gonogo.model.mifin.topup;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class verificationDetail {
    @JsonProperty("VERIFICATIONRESULT")
    private String verificationResult;

    @JsonProperty("TARGET")
    private String target;

    @JsonProperty("NOOFATTEMPTS")
    private String noOfAttempts;

    @JsonProperty("VERIFICATIONTYPE")
    private String verificationType;

    @JsonProperty("VERIFICATIONSTATUS")
    private String verificationStatus;

    @JsonProperty("VERIFICATIONPOINT")
    private String verificationPoint;

}
