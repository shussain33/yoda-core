package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicantDetails
{
    @JsonProperty("CUSTID")
    @NotEmpty
    public String CustomerId;

    @JsonProperty("DOB")
    @NotEmpty
    public String dob;

    @JsonProperty("FNAME")
    @NotEmpty
    public String fName;

    @JsonProperty("MNAME")
    @NotEmpty
    public String mName;

    @JsonProperty("LNAME")
    @NotEmpty
    public String lName;

    @JsonProperty("PAN")
    @NotEmpty
    public String pan;

    @JsonProperty("FATHERNAME")
    @NotEmpty
    public String  fatherName;

    @JsonProperty("MOTHERNAME")
    @NotEmpty
    public String motherName;

    @JsonProperty("CUSTOMERNAME")
    @NotEmpty
    public String CustomerName;

    @JsonProperty("VOTERIDCARDNO")
    @NotEmpty
    public String voterCardNo;

    @JsonProperty("PASSPORTNO")
    @NotEmpty
    public String passportNo;

    @JsonProperty("DL")
    @NotEmpty
    public String dl;

    @JsonProperty("SCORE")
    @NotEmpty
    public String score;

    @JsonProperty("POTENTIAL_MATCH")
    @NotEmpty
    public String potentialMatch;

    @JsonProperty("STRONG_MATCH")
    @NotEmpty
    public String strongMatch;

    @JsonProperty("CUSTTYPE")
    @NotEmpty
    public String custType;

    @JsonProperty("EXACTMATCH_MSGSTATUS")
    @NotEmpty
    public String ExactMatchMsgsStatus;

    @JsonProperty("POTENTIALMATCH_MSGSTATUS")
    @NotEmpty
    public String potentialMatchMsgsStatus;


    @JsonProperty("MATCHED_APPLICANTCODE")
    @NotEmpty
    public String matchedApplicantCode;

    @JsonProperty("POTENTIAL_APPLICANTCODE")
    @NotEmpty
    public String potentialApplicantCode;

    @JsonProperty("ADDRESSDETAIL")
    @NotEmpty
    public  List <AddressDetail> AddressDetailsList;

    @JsonProperty("MOBILE")
    public String mobile;

    //From over side
    @JsonProperty("bDedupeFlag")
    public boolean flag;
}
