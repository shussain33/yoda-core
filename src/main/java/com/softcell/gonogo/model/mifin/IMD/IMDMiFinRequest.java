package com.softcell.gonogo.model.mifin.IMD;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.Authentication;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IMDMiFinRequest {

    @JsonProperty("AUTHENTICATION")
    private Authentication authentication;

    @JsonProperty("BASICINFO")
    private BasicInfo basicInfo;
}
