package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 20/6/19.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValuationDetail {

    @JsonProperty("REPLACEMENT_COST")
    private String replacementCost;

    @JsonProperty("PROPERTY_STATUS")
    private String propertyStatus;

    @JsonProperty("VALUATORNAME")
    private String valuatorName;

    @JsonProperty("PROPERTY_AGE")
    private String propertyAge = "0";

    @JsonProperty("EVALUATION_DATE")
    private String evaluationDate;

    @JsonProperty("PRO_RESIDUALAGE")
    private String proResidualAge;

    @JsonProperty("PROPERTY_USAGE")
    private String propertyUsage;

    @JsonProperty("MARKET_VALUE")
    private String marketValue;
}

