package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DedupeInfo {

    @JsonProperty("MATCH_COUNT")
    private String matchCount;

    @JsonProperty("APPLICANTDETAILS")
    private ApplicantDetails applicantDetailsList;

    @JsonProperty("MATCHEDAPPLICANTS")
    private List<MatchApplicantDetails> matchApplicantList;
}
