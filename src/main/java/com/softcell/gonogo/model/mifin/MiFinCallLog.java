package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Constant;
import com.softcell.gonogo.model.mifin.topup.TopUpDedupeResponse;
import com.softcell.gonogo.model.mifin.topup.TopUpMifinDedupeRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "mifinCallLog")
public class MiFinCallLog {

    //ExistingLoan,MIFIN_DEDUPE_DETAIL,MIFINLOANDETAIL
    @JsonProperty(value = "sRequestType" ,defaultValue = Constant.BLANK)
    private String requestType;

    // TODO Make it transient
    @JsonProperty("api")
    private String api;

    @JsonProperty("refId")
    private String refId;

    @JsonProperty("callDate")
    private Date callDate = new Date();

    @JsonProperty("request")
    private Object miFinRequest;

    @JsonProperty("response")
    private MiFinResponse miFinResponse;

    @JsonProperty("dedupeResponse")
    private List<TopUpDedupeResponse> dedupeResponseList;

    //Like A or B mifin loanDetails
    @JsonProperty("sLoanDetailType")
    private String loanDetailType;

    @JsonProperty("sAppId")
    private String appId;


    //To store request and response in String format
    @JsonProperty("sStringRequestFormat")
    private String request;

    @JsonProperty("sStringResponseFormat")
    private String response;

    @JsonProperty("bLoanDetailApi")
    public boolean apiHit;

}
