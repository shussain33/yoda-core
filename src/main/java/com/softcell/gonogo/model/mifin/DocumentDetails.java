package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentDetails {

    @JsonProperty(value = "DOCUMENTTYPE", defaultValue = "")
    private String documentType;

    @JsonProperty(value = "DOCUMENTNAME", defaultValue = "")
    private String documentName;

    @JsonProperty(value = "DOCUMENTDESCRIPTION", defaultValue = "")
    private String documentDes;

    @JsonProperty(value = "STATUS", defaultValue = "")
    private String status = "Received";

    @JsonProperty(value = "RECEIVEDDATE", defaultValue = "")
    private String receivedDate;

}
