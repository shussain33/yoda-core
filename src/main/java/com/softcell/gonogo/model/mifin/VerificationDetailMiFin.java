package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 19/4/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VerificationDetailMiFin {

   @JsonProperty(value = "VERIFICATIONTYPE", defaultValue = "")
   private String verificationType;

   @JsonProperty(value = "VERIFICATIONPOINT", defaultValue = "")
   private String verificationPoint;

    @JsonProperty(value = "TARGET", defaultValue = "")
    private String target;

    @JsonProperty(value = "VERIFICATIONSTATUS" , defaultValue = "")
    private String verificationStatus;

    @JsonProperty(value = "VERIFICATIONRESULT", defaultValue = "")
    private String verificationResult;

    @JsonProperty(value = "NOOFATTEMPTS", defaultValue = "")
    private String noOfAttempts;

}
