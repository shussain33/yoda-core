package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LmsDetail
{

    @JsonProperty("PROSPECTCODE")
    private String prospectCode;

    @JsonProperty("FORECLOSURE_INTEREST")
    private String foreclosureInterest;

    @JsonProperty("EMI")
    private String emi;

    @JsonProperty("PRINCIPAL_OUTSTANDING")
    private String principalOutstanding;


    @JsonProperty("INTEREST_DUE_OUTSTANDING")
    private String interestdueOutstanding;


    @JsonProperty("INTEREST_NOT_DUE_FORECLOSURE_INTEREST")
    private String interestNotDueForeClosureInterest;


    @JsonProperty("OTHER_CHARGES")
    private String otherCharges;

    @JsonProperty("NO_OF_BOUNCE")
    private String noOfBounce;



    @JsonProperty("bflag")
    @JsonIgnore
    private boolean flag = false;


}

