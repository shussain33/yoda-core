package com.softcell.gonogo.model.mifin.topup;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProspectDetails
{
    @JsonProperty("PROSPECTCODE")
    @NotEmpty
    public String prospectCode;
    @JsonProperty("PRODUCTID")
    @NotEmpty
    public String productId;
    @JsonProperty("SCHEMEID")
    @NotEmpty
    public String schemeId;
    @JsonProperty("LOANAMOUNT")
    @NotEmpty
    public String loanAmount;
    @JsonProperty("DISBURSALDATE")
    @NotEmpty
    public String disbursalDate;
    @JsonProperty("PRINCIPAL_OUTSTANDING")
    @NotEmpty
    public String proncipalOutstanding;
    @JsonProperty("EMI_OUTSTANDING")
    @NotEmpty
    public String emiOutstanding;
    @JsonProperty("FORECLOSURE_INTEREST")
    @NotEmpty
    public String forceClosureInterest;

    @JsonProperty("SCHEME")
    @NotEmpty
    public String scheme;

    @JsonProperty("Status")
    @NotEmpty
    public String loanStatus;

    @JsonProperty("TENOR")
    @NotEmpty
    public String tenor;

    @JsonProperty("ROI")
    @NotEmpty
    public String roi;

    //from LEVIOSA side
    @JsonProperty("bDedupeFlag")
    @NotEmpty
    public boolean dedupeFlag = false;

    @JsonProperty("bValueToBeConsider")
    public boolean valueToBeConsider ;

}
