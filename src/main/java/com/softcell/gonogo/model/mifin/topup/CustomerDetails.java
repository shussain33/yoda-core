package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerDetails {

    @JsonProperty("CUSTOMERCODE")
    private String customerCode;

    @JsonProperty("CUSTOMER_ENTITY_TYPE")
    private String customerEntityType;

    @JsonProperty("CUSTOMER_TYPE")
    private String customerType;

    @JsonProperty("FNAME")
    private String firstName;

    @JsonProperty("MNAME")
    private String middleName;

    @JsonProperty("LNAME")
    private String lastName;

    @JsonProperty("FATHER_FNAME")
    private String fatherFname;

    @JsonProperty("FATHER_MNAME")
    private String fatherMname;

    @JsonProperty("FATHER_LNAME")
    private String fatherLname;

    @JsonProperty("SPOUSE_FNAME")
    private String spouseFirstname;

    @JsonProperty("SPOUSE_LNAME")
    private String spouseLastname;

    @JsonProperty("SPOUSE_MNAME")
    private String spouseMiddlename;

    @JsonProperty("MOTHER_MAIDEN_NAME")
    private String motherMaidenName;

    @JsonProperty("DOB")
    private String dob;

    @JsonProperty("GENDER")
    private String gender;

    @JsonProperty("MARTIAL_STATUS")
    private String maritalStatus;

    @JsonProperty("CATEGORY")
    private String category;

    @JsonProperty("QUALIFICATION")
    private String qualification;

    @JsonProperty("NO_OF_DEPENDENTS")
    private String noOfDependents;

    @JsonProperty("OCCUPATION_TYPE")
    private String occupationType;

    @JsonProperty("TANNO")
    private String tanNo;

    @JsonProperty("GROSS_INCOME")
    private String grosIncome;

    @JsonProperty("NET_INCOME")
    private String netIncome;

    @JsonProperty("WORK_EXPERIENCE")
    private String workExperience;

    @JsonProperty("PASSPORT_NO")
    private String passportNo;

    @JsonProperty("PASSPORT_EXP_DATE")
    private String passportExpDate;

    @JsonProperty("AADHAR_NO")
    private String addharNo;

    @JsonProperty("PAN_NO")
    private String panNo;

    @JsonProperty("VOTER_ID")
    private String VoterId;

    @JsonProperty("DRIVING_LICENSE")
    private String DrivingLicense;

    @JsonProperty("DRIVING_LICENSE_EXP_DATE")
    private String DrivingLicenseexpDate;

    @JsonProperty("RATION_CARD")
    private String rationCard;

    @JsonProperty("BANK_PASSBOOK")
    private String bankPassBook;

    @JsonProperty("INDUSTRYTYPE")
    private String industryType;

    @JsonProperty("PROFESSION")
    private String profession;

    @JsonProperty("GSTIN")
    private String gstIn;

    @JsonProperty("EMPLOYMENTTYPE")
    private String employmentType;

    @JsonProperty("CIBIL_SCORE")
    private String cibilScore;


    @JsonProperty("CURRENT_EMPOLYER")
    private String currentEmployer;

    @JsonProperty("ENTITY_OTHER")
    private String entityOther;

    @JsonProperty("Industry_Code")
    private String industryCode;

    @JsonProperty("Organization_Type")
    private String organizationType;

    @JsonProperty("PROFESSION_Code")
    private String professionCode;

    @JsonProperty("RELATION")
    private String relation;

    @JsonProperty("ADDRESS_DETAIL")
    private List<AddressDetails> addressDetailList;
}
