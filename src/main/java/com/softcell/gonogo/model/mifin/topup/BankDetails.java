package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BankDetails {
    @JsonProperty("MICR")
    private String micr;

    @JsonProperty("IFSC_CODE")
    private String ifscCode;

    @JsonProperty("BANK_ID")
    private String bankId;

    @JsonProperty("BANK_BRANCH_ID")
    private String bankBranchId;

    @JsonProperty("BANK_BRANCH_CITY_ID")
    private String bankBranchCityId;

    @JsonProperty("CUSTOMER_AC_NUMBER")
    private String customerAcNumber;

    @JsonProperty("BENEFICERY_NAME")
    private String beneficeryName;

    @JsonProperty("BANKTYPE")
    private String bankType;

    @JsonProperty("ACCOUNT_TYPE")
    private String accountType;

    @JsonProperty("NOOFYEAR")
    private String noOfYear;
}
