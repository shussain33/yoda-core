package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by archana on 8/5/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Disbursal {
    @JsonProperty("DISBURSALLIST")
    private List<DisbursalDetails> disbusalDetails;
}
