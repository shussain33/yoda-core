package com.softcell.gonogo.model.mifin.topup;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchApplicantDetails implements Serializable {
    @JsonProperty("CUSTID")
    @NotEmpty
    public String CustomerId;

    @JsonProperty("DOB")
    @NotEmpty
    public String dob;

    @JsonProperty("FNAME")
    @NotEmpty
    public String fName;

    @JsonProperty("MNAME")
    @NotEmpty
    public String mName;

    @JsonProperty("LNAME")
    @NotEmpty
    public String lName;
    @JsonProperty("FATHERNAME")
    @NotEmpty
    public String  fatherName;
    @JsonProperty("MOTHERNAME")
    @NotEmpty
    public String motherName;
    @JsonProperty("PAN")
    @NotEmpty
    public String pan;
    @JsonProperty("VOTERIDCARDNO")
    @NotEmpty
    public String voterCardNo;
    @JsonProperty("PASSPORTNO")
    @NotEmpty
    public String passportNo;
    @JsonProperty("DL")
    @NotEmpty
    public String dl;
    @JsonProperty("SCORE")
    @NotEmpty
    public String score;
    @JsonProperty("POTENTIAL_MATCH")
    @NotEmpty
    public String potentialMatch;

    @JsonProperty("STRONG_MATCH")
    @NotEmpty
    public String strongMatch;

    @JsonProperty("CUSTTYPE")
    @NotEmpty
    public String custType;
    @JsonProperty("EXACTMATCH_MSGSTATUS")
    @NotEmpty
    public String ExactMatchMsgsStatus;
    @JsonProperty("POTENTIALMATCH_MSGSTATUS")
    @NotEmpty
    public String potentialMatchMsgsStatus;

    @JsonProperty("MATCHED_APPLICANTCODE")
    @NotEmpty
    public String matchedApplicantCode;
    @JsonProperty("POTENTIAL_APPLICANTCODE")
    @NotEmpty
    public String potentialApplicantCode;
    @JsonProperty("ADDRESSDETAIL")
    @NotEmpty
    public List<AddressDetail> AddressDetailsList;
    @JsonProperty("MATCH_COUNT")
    @NotEmpty
    public String matchCount;

    @JsonProperty("MOBILE")
    public String mobile;

    @JsonProperty("APPLICANTLOANINFO")
    @NotEmpty
    public List<ApplicantLoanInfo> applicantLoanInfoList;

    //From over side
    @JsonProperty("bDedupeFlag")
    public boolean selected;

    @JsonProperty("PROSPECTLIST")
    public ProspectList propectList ;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchApplicantDetails that = (MatchApplicantDetails) o;
        return selected == that.selected &&
                Objects.equals(CustomerId, that.CustomerId) &&
                Objects.equals(dob, that.dob) &&
                Objects.equals(fName, that.fName) &&
                Objects.equals(mName, that.mName) &&
                Objects.equals(lName, that.lName) &&
                Objects.equals(fatherName, that.fatherName) &&
                Objects.equals(motherName, that.motherName) &&
                Objects.equals(pan, that.pan) &&
                Objects.equals(voterCardNo, that.voterCardNo) &&
                Objects.equals(passportNo, that.passportNo) &&
                Objects.equals(dl, that.dl) &&
                Objects.equals(score, that.score) &&
                Objects.equals(potentialMatch, that.potentialMatch) &&
                Objects.equals(strongMatch, that.strongMatch) &&
                Objects.equals(custType, that.custType) &&
                Objects.equals(ExactMatchMsgsStatus, that.ExactMatchMsgsStatus) &&
                Objects.equals(potentialMatchMsgsStatus, that.potentialMatchMsgsStatus) &&
                Objects.equals(matchedApplicantCode, that.matchedApplicantCode) &&
                Objects.equals(potentialApplicantCode, that.potentialApplicantCode) &&
                Objects.equals(AddressDetailsList, that.AddressDetailsList) &&
                Objects.equals(matchCount, that.matchCount) &&
                Objects.equals(mobile, that.mobile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(CustomerId, dob, fName, mName, lName, fatherName, motherName, pan, voterCardNo, passportNo, dl, score, potentialMatch, strongMatch, custType, ExactMatchMsgsStatus, potentialMatchMsgsStatus, matchedApplicantCode, potentialApplicantCode, AddressDetailsList, matchCount, mobile, selected);
    }
}
