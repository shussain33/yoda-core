package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

//not Required

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TopUpDedupeResponseField
{


    @JsonProperty("sLaonDisbursementDate")
    @NotEmpty
    public String loanDisbursementDate;

    @JsonProperty("sProduct")
    @NotEmpty
    public String product;

    @JsonProperty("sDisbursedAmount")
    @NotEmpty
    public String disbursedAmount;


    @JsonProperty("sOutstandingAmount")
    @NotEmpty
    public String  outstandingAmount;



    @JsonProperty("sCustomerId")
    @NotEmpty
    public String CustomerId;


    @JsonProperty("sLoanNo")
    @NotEmpty
    public String loanNo;


    @JsonProperty("sCustomerName")
    @NotEmpty
    public String CustomerName;


    @JsonProperty(value = "sCompanyEmail", defaultValue = "false")
    @NotEmpty
    public Boolean flag;;

    //for Leviosa purpose




}
