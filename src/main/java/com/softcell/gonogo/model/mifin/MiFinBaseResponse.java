package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yogesh on 19/4/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("other")
public class MiFinBaseResponse {

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("MESSAGE")
    private String message;

    @JsonProperty("PROSPECTCODE")
    private String prospectCode;

    @JsonProperty("CUSTOMERCODE")
    private String customerCode;

    @JsonProperty("DISBURSAL_AMOUNT")
    private String disbursalAmt;

    @JsonProperty("REPAYMENTSCHEDULE")
    private RepaymentSchedule repaymentSchedule;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }


}
