package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisbursalDetailLs {
    @JsonProperty("BANK_ID")
    private  String bankId;

    @JsonProperty("BANK_BRANCH_ID")
    private  String bankBranchId;

    @JsonProperty("INSTRUMENTTYPE")
    private  String instrumentType;

    @JsonProperty("INSTRUMENTNO")
    private  String instrumentNo;

    @JsonProperty("INSTRUMENTDATE")
    private  String instrumentDate;

    @JsonProperty("BOPS")
    private  String bops;

    @JsonProperty("HOPS")
    private  String hops;

    @JsonProperty("DISBURSAL_AMOUNT")
    private  String disbursalAmount;

    @JsonProperty("BANKNAME")
    private  String bankName;

    @JsonProperty("DISBURSAL_DATE")
    private  String disbursalDate;

}
