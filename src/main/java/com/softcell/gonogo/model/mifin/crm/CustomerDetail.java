package com.softcell.gonogo.model.mifin.crm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 23/9/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerDetail {

    @JsonProperty("Customer_Full_Name")
    private String customerFullName;

    @JsonProperty("Mobile_number")
    private String mobileNumber;

    @JsonProperty("DOB")
    public String dob;

    @JsonProperty("PAN")
    public String pan;

    @JsonProperty("Gender")
    private String gender;

    @JsonProperty("Marital_status")
    private String maritalStatus;

    @JsonProperty("Personal_Email_ID")
    private String personalEmaiId;

    @JsonProperty("Requested_Loan_Amount")
    private String requestedLoanAmount;

    @JsonProperty("Net_Income")
    private String netIncome;

    @JsonProperty("Current_Obligations")
    private String currentObligations;

    @JsonProperty("Credit_Card_Outstanding")
    private String creditCardOutstanding;

    @JsonProperty("Tenure")
    private String tenure;

    @JsonProperty("HL_LAP")
    private String HL_LAP;

    @JsonProperty("Ownership_Type")
    private String ownershipType;

    @JsonProperty("Time_at_address")
    private String timeAtAddress;

    @JsonProperty("Address_line_1")
    private String addressLine1;

    @JsonProperty("Address_line_2")
    private String addressLine2;

    @JsonProperty("Landmark")
    private String landMark;

    @JsonProperty("Pincode")
    private String pinCode;

    @JsonProperty("City")
    private String city;

    @JsonProperty("State")
    private String State;

    @JsonProperty("Company_Name")
    private String companyName;

    @JsonProperty("Category")
    private String category;

    @JsonProperty("Total_Work_Experience_IN_YEAR")
    private String totalWorkExperienceInYear;

    @JsonProperty("Total_Work_Experience_IN_MONTH")
    private String totalWorkExperienceInMonth;

    @JsonProperty("Current_Work_Experience_IN_YEAR")
    private String currentWorkExperienceInYear;

    @JsonProperty("Current_Work_Experience_IN_MONTH")
    private String currentWorkExperienceInMonth;

    @JsonProperty("Company_Email_ID")
    private String companyEmailId;

    @JsonProperty("Mode_of_Salary")
    private String modeOfSalary;

    @JsonProperty("Branch_Name")
    private String branchName;

    @JsonProperty("Office_Address_1")
    private String officeAddress_1;

    @JsonProperty("Office_Address_2")
    private String officeAddress_2;

    @JsonProperty("Office_Pincode")
    private String officePinCode;

    @JsonProperty("Office_City")
    private String officeCity;

    @JsonProperty("Office_State")
    private String officeState;

    @JsonProperty("Loan_purpose")
    private String loanPurpose;
}
