package com.softcell.gonogo.model.mifin.crm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.Authentication;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 23/9/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CrmRequest {

    @JsonProperty("AUTHENTICATION")
    private Authentication authentication;

    @JsonProperty("BASICINFO")
    private BasicInfo basicInfo;

}
