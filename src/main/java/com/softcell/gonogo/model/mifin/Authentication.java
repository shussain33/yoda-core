package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.softcell.constants.Constant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suhasini on 13/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Authentication {

    @JsonProperty(value = "APP_NAME", defaultValue = Constant.BLANK)
    private String appName;

    @JsonProperty(value = "APP_PASS", defaultValue = Constant.BLANK)
    private String appPass;

    @JsonProperty(value = "IPADDRESS", defaultValue = "123.0.0.1")
    private String ipAddress;

    @JsonProperty(value = "DEVICE_ID", defaultValue = "GONOGO")
    private String deviceId;

    @JsonProperty(value = "LONGITUDE", defaultValue = "41.0")
    private String longitude;

    @JsonProperty(value = "LATITUDE", defaultValue = "23.0")
    private String latitude;

}
