package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 8/5/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DisbursalDetails {

    @JsonProperty("BANK_ID")
    private String bankId;

    @JsonProperty("BANK_BRANCH_ID")
    private String bankBranchId;

    @JsonProperty("INSTRUMENTTYPE")
    private String instrumentType;

    @JsonProperty("INSTRUMENTNO")
    private String instrumentNo;

    // "2018-01-04"
    @JsonProperty("INSTRUMENTDATE")
    private String instrumentDate;

    @JsonProperty("BOPS")
    private String bopsId;

    @JsonProperty("HOPS")
    private String hopsId;

    @JsonProperty("DISBURSAL_AMOUNT")
    private String disbursalAmt;

    @JsonProperty("DISBURSAL_AMOUNT_PARTNER_BANK")
    private String disbursalAmtPartnerBank;

    @JsonProperty("DISBURSAL_AMOUNT_SBFC")
    private String disbursalAmtSBFC;

    @JsonProperty("Issue_In_Favour_Of")
    private String issueInFavourOf;

    @JsonProperty("TOWARDS")
    private String towards;
}
