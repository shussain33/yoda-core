package com.softcell.gonogo.model.mifin.topup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicantLoanInfo {
    @JsonProperty("SCHEME")
    @NotEmpty
    public String scheme;

    @JsonProperty("OS_OTHERCHARGES")
    @NotEmpty
    public String osOtherCharges;

    @JsonProperty("PROSPECTCODE")
    @NotEmpty
    public String prospectCode;

    @JsonProperty("OS_INTERST")
    @NotEmpty
    public String osInterst;

    @JsonProperty("OS_PRINCIPAL")
    @NotEmpty
    public String osPrincipal;

}
