package com.softcell.gonogo.model.mifin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by suhasini on 14/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDetails {
    @JsonProperty("CUSTOMERCODE")
    private String customerCode;

    @JsonProperty("CUSTOMER_ENTITY_TYPE")
    private String entityType;

    @JsonProperty("CUSTOMER_TYPE")
    private String customerType;

    @JsonProperty("FNAME")
    private String fname;

    @JsonProperty("MNAME")
    private String mname;

    @JsonProperty("LNAME")
    private String lname;

    @JsonProperty("FATHER_FNAME")
    private String fatherFName;

    @JsonProperty("FATHER_MNAME")
    private String fatherMName;

    @JsonProperty("FATHER_LNAME")
    private String fatherLName;

    @JsonProperty("SPOUSE_FNAME")
    private String spouseFName;

    @JsonProperty("SPOUSE_MNAME")
    private String spouseMName;

    @JsonProperty("SPOUSE_LNAME")
    private String spouseLName;

    @JsonProperty("MOTHER_MAIDEN_NAME")
    private String motherMaidenName;

    @JsonProperty("DOB")
    private String dob;

    @JsonProperty("GENDER")
    private String gender;

    @JsonProperty("MARITAL_STATUS")
    private String maritalStatus;

    @JsonProperty("CATEGORY")
    private String category;

    @JsonProperty("QUALIFICATION")
    private String qualification;

    @JsonProperty("NO_OF_DEPENDENTS")
    private String noOfDependents;

    @JsonProperty("OCCUPATION_TYPE")
    private String occupationType;

    @JsonProperty("TANNO")
    private String tanNo;

    @JsonProperty("GROSS_INCOME")
    private String grossIncome;

    @JsonProperty("NET_INCOME")
    private String netIncome;

    @JsonProperty("WORK_EXPERIENCE")
    private String workExperience;

    @JsonProperty("PASSPORT_NO")
    private String passportNo;

    @JsonProperty("PASSPORT_EXP_DATE")
    private String passportExpDate;

    @JsonProperty("AADHAR_NO")
    private String aadharNo;

    @JsonProperty("PAN_NO")
    private String panNo;

    @JsonProperty("VOTER_ID")
    private String voterId;

    @JsonProperty("DRIVING_LICENSE")
    private String drivingLicense;

    @JsonProperty("DRIVING_LICENSE_EXP_DATE")
    private String drivingLicenseExpDate;

    @JsonProperty("RATION_CARD")
    private String rationCard;

    @JsonProperty("BANK_PASSBOOK")
    private String bankPassbook;

/*
    @JsonProperty("ADHAR_ENROLMENT_NO")
    private String adharEnrolmentNo;
*/

    @JsonProperty("INDUSTRYTYPE")
    private String industryType;

    @JsonProperty("PROFESSION")
    private String profession;

    @JsonProperty("GSTIN")
    private String gstin;

    @JsonProperty("EMPLOYMENTTYPE")
    private String employmentType;

    @JsonProperty("ADDRESS_DETAIL")
    private List<AddressDetails> addressDetailsList;

    @JsonProperty("CIBIL_SCORE")
    private String cibilScore;

    @JsonProperty("CURRENT_EMPLOYER")
    private String currentEemployer;

    @JsonProperty("ENTITY_OTHER")
    private String otherEntity;

    @JsonProperty("Industry_Code")
    private String industryCode;

    @JsonProperty("Organization_Type")
    private String organizationType;

    @JsonProperty("PROFESSION_Code")
    private String professtionCode;

    @JsonProperty("RELATION")
    private String relation;

}
