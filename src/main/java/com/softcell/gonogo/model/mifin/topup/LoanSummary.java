package com.softcell.gonogo.model.mifin.topup;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanSummary
{

    @JsonProperty("PROSPECTCODE")
    private String prospectCode;

    @JsonProperty("DOCUMENTTYPE")
    private String documentType;

    @JsonProperty("DOCUMENTNAME")
    private String documentName;

    @JsonProperty("DOCUMENT_REMARKS")
    private String document_Remarks;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("RECEIVEDDATE")
    private String receivedDate;

    @JsonProperty("VERIFICATIONTYPE")
    private String verificationType;


    @JsonProperty("VERIFICATIONPOINT")
    private String verificationPoint;

    @JsonProperty("TARGET")
    private String target;

    @JsonProperty("VERIFICATIONSTATUS")
    private String verificationStatus;


    @JsonProperty("VERIFICATIONRESULT")
    private String verificationResult;


    @JsonProperty("NOOFATTEMPTS")
    private String noOfAttempts;


    @JsonProperty("CUSTOMERCODE")
    private String customerCode;


    @JsonProperty("CUSTOMER_ENTITY_TYPE")
    private String customerEntityType;


    @JsonProperty("CUSTOMER_TYPE")
    private String customerType;


    @JsonProperty("FNAME")
    private String fName;

    @JsonProperty("MNAME")
    private String mName;


    @JsonProperty("LNAME")
    private String lName;

    @JsonProperty("FATHER_FNAME")
    private String fatherName;


    @JsonProperty("FATHER_MNAME")
    private String fatherMname;


    @JsonProperty("FATHER_LNAME")
    private String fatherLname;


    @JsonProperty("SPOUSE_FNAME")
    private String spouseFname;


    @JsonProperty("SPOUSE_MNAME")
    private String spouseMname;

    @JsonProperty("SPOUSE_LNAME")
    private String spouseLname;


    @JsonProperty("MOTHER_MAIDEN_NAME")
    private String motherMaidenName;


    @JsonProperty("DOB")
    private String dob;


    @JsonProperty("GENDER")
    private String gender;

    @JsonProperty("MARITAL_STATUS")
    private String maritalStaus;


    @JsonProperty("CATEGORY")
    private String category;


    @JsonProperty("QUALIFICATION")
    private String qualification;


    @JsonProperty("NO_OF_DEPENDENTS")
    private String noOfDependents;

    @JsonProperty("OCCUPATION_TYPE")
    private String occupationType;


    @JsonProperty("TANNO")
    private String tanNo;


    @JsonProperty("GROSS_INCOME")
    private String grossIncome;


    @JsonProperty("NET_INCOME")
    private String netIncome;


    @JsonProperty("PASSPORT_NO")
    private String passportNo;

    @JsonProperty("PASSPORT_EXP_DATE")
    private String passportExpDate;

    @JsonProperty("AADHAR_NO")
    private String aadharNo;


    @JsonProperty("PAN_NO")
    private String panNo;


    @JsonProperty("VOTER_ID")
    private String VoterId;

    @JsonProperty("DRIVING_LICENSE")
    private String drivingLicense;

    @JsonProperty("DRIVING_LICENSE_EXP_DATE")
    private String drivingLicenseExpDate;

    @JsonProperty("RATION_CARD")
    private String rationCard;

    @JsonProperty("BANK_PASSBOOK")
    private String bankPassBook;

    @JsonProperty("WORK_EXPERIENCE")
    private String workExperience;

    @JsonProperty("ADDRESS_TYPE")
    private String addressType;

    @JsonProperty("MAILING_ADDRESS")
    private String mailingAddress;


    @JsonProperty("NAMEOFCOMPANY")
    private String nameOfCompany;


    @JsonProperty("ADD_1")
    private String add1;


    @JsonProperty("ADD_2")
    private String add2;

    @JsonProperty("ADD_3")
    private String add3;

    @JsonProperty("ADD_CITY")
    private String addCity;

    @JsonProperty("ADD_DIST")
    private String addDist;

    @JsonProperty("ADD_LANDMARK")
    private String addLandMark;


    @JsonProperty("ADD_STATE")
    private String addState;

    @JsonProperty("ADD_PINCODE")
    private String addPinCode;

    @JsonProperty("ADD_YEAR_OF_STAY")
    private String addYearOfStay;

    @JsonProperty("ADD_PHONE_STD_CODE")
    private String addPhoneStdCode;

    @JsonProperty("ADD_PHONE")
    private String addPhone;

    @JsonProperty("ADD_MOBILE")
    private String addMobile;

    @JsonProperty("ADD_EMAIL")
    private String addEmail;

    @JsonProperty("OCCUPANCY_STATUS ")
    private String occupancyStatus;


    @JsonProperty("BRANCH")
    private String branch;

    @JsonProperty("RELATIONSHIP_MANAGER")
    private String  relationShipManager;


    @JsonProperty("PURPOSE_OF_LOAN")
    private String  purposeOfLoan;

    @JsonProperty("APPFORMNO")
    private String  appFormNo;

    @JsonProperty("SOURCING_CHANNEL_TYPE")
    private String  sourcingChannelType;

    @JsonProperty("SOURCING_CHANNEL_NAME")
    private String  sourcingChannelName;


    @JsonProperty("FULLFILLMENT_CHANNEL_TYPE")
    private String  fullfillmentChannelType;

    @JsonProperty("FULFILLMENT_CHANNEL_NAME")
    private String fulfillmentChannelName;

    @JsonProperty("PROSPECTID")
    private String prospectId;

    @JsonProperty("APPLICANTID")
    private String applicantId;


    @JsonProperty("SANCTIONAPPROVEDBY")
    private String sanctionApprovedBy;


    @JsonProperty("PRODUCTID")
    private String productId;

    @JsonProperty("SCHEMEID")
    private String schemeId;


    @JsonProperty("APPLIED_LOAN_AMOUNT")
    private String appliedLoanAmount;

    @JsonProperty("SANCTIONED_AMOUNT")
    private String sanctionedAmont;


    @JsonProperty("TENOR")
    private String tenor;


    @JsonProperty("ROI")
    private String roi;

    @JsonProperty("INTERESTTYPE")
    private String interestType;


    @JsonProperty("INSTALLMENTTYPE")
    private String installmentType;

    @JsonProperty("FREQUENCY")
    private String frequency;


    @JsonProperty("LTV")
    private String ltv;

    @JsonProperty("DBR")
    private String dbr;

    @JsonProperty("EMI_START_DATE")
    private String emiStartDate;

    @JsonProperty("DISBURSAL_DATE")
    private String disbursalDate;

    @JsonProperty("ADVANCE_EMI_FLAG")
    private String advanceEmiFlag;


    @JsonProperty("NO_ADVANCE_INSTALMENT")
    private String noAdvanceInstalment;

    @JsonProperty("CHARGEID")
    private String chargeId;

    @JsonProperty("CHARGE_AMOUNT")
    private String chargeAmount;

    @JsonProperty("GST_ON_CHARGE")
    private String gstOnCharge;


    @JsonProperty("TOTAL_CHARGE_AMOUNT")
    private String totalChargeAmount;

    @JsonProperty("MICR")
    private String micr;


    @JsonProperty("IFSC_CODE")
    private String ifscCode;

    @JsonProperty("BANK_ID")
    private String bankId;

    @JsonProperty("BANK_BRANCH_ID")
    private String bankBranchId;

    @JsonProperty("BANK_BRANCH_CITY_ID")
    private String bankBranchCityId;


    @JsonProperty("CUSTOMER_AC_NUMBER")
    private String customerAcNumber;

    @JsonProperty("BENEFICERY_NAME")
    private String benficeryName;

    @JsonProperty("BANKTYPE")
    private String bankType;

    @JsonProperty("ACCOUNT_TYPE")
    private String accountType;

    @JsonProperty("PROPERTYTYPE")
    private String prpertyType;

    @JsonProperty("PROPERTYADDRESS1")
    private String prpertyTypeAddress1;

    @JsonProperty("PROPERTYADDRESS2")
    private String prpertyTypeAddress2;

    @JsonProperty("PROPERTYADDRESS3")
    private String prpertyTypeAddress3;


    @JsonProperty("LANDMARK")
    private String landMark;

    @JsonProperty("LOCALITY")
    private String locality;

    @JsonProperty("CITY")
    private String city;

    @JsonProperty("STATE")
    private String state;

    @JsonProperty("ZIPCODE")
    private String zipCode;


    @JsonProperty("OWNER_FNAME")
    private String ownerName;

    @JsonProperty("OWNER_MNAME")
    private String ownerMName;

    @JsonProperty("OWNER_LNAME")
    private String oWNERLNAME;

    @JsonProperty("VALUATORNAME")
    private String vALUATORNAME;

    @JsonProperty("PROPERTY_USAGE")
    private String pROPERTYUSAGE;

    @JsonProperty("PROPERTY_STATUS")
    private String pROPERTYSTATUS;

    @JsonProperty("PROPERTY_AGE")
    private String pROPERTYAGE;

    @JsonProperty("PRO_RESIDUALAGE")
    private String pRORESIDUALAGE;

    @JsonProperty("MARKET_VALUE")
    private String mARKETVALUE;

    @JsonProperty("REPLACEMENT_COST")
    private String rEPLACEMENTCOST;

    @JsonProperty("EVALUATION_DATE")
    private String eVALUATIONDATE;

    @JsonProperty("INSTRUMENTTYPE")
    private String iNSTRUMENTTYPE;

    @JsonProperty("INSTRUMENTNO")
    private String iNSTRUMENTNO;

    @JsonProperty("INSTRUMENTDATE")
    private String iNSTRUMENTDATE;

    @JsonProperty("MAKER")
    private String mAKER;

    @JsonProperty("CHECKER")
    private String cHECKER;

    @JsonProperty("DISBURSAL_AMOUNT")
    private String dISBURSALAMOUNT;

    @JsonProperty("EMI")
    private String eMI;

    @JsonProperty("PRINCIPAL_OUTSTANDING")
    private String pRINCIPALOUTSTANDING;

    @JsonProperty("INTEREST_DUE_OUTSTANDING")
    private String iNTERESTDUEOUTSTANDING;

    @JsonProperty("INTEREST_NOT_DUE_FORECLOSURE_INTEREST")
    private String iNTERESTNOTDUEFORECLOSUREINTEREST;

    @JsonProperty("OTHER_CHARGES")
    private String oTHERCHARGES;

    //from leviosw side
    @JsonProperty("bflag")
    @JsonIgnore
    private boolean flag = false;
}







