package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import java.util.List;

/**
 * @author kishorp
 */
@JsonInclude(Include.NON_NULL)
public class Stage {
    private String name;
    private List<String> stepHeader;
    private List<List<String>> data;
    private Statastics statastics;
    private boolean isPending;
    private String date;
    private String timeStamp;
    private int bucketId;
    @JsonIgnore
    private int stageId;


    public int getStageId() {
        return stageId;
    }

    public void setStageId(int stageId) {
        this.stageId = stageId;
    }

    public int getBucketId() {
        return bucketId;
    }

    public void setBucketId(int bucketId) {
        this.bucketId = bucketId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public boolean isPending() {
        return isPending;
    }

    public void setPending(boolean isPending) {
        this.isPending = isPending;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<List<String>> getData() {
        return data;
    }

    public void setData(List<List<String>> data) {
        this.data = data;
    }

    public List<String> getStepHeader() {
        return stepHeader;
    }

    public void setStepHeader(List<String> stepHeader) {
        this.stepHeader = stepHeader;
    }

    public Statastics getStatastics() {
        return statastics;
    }

    public void setStatastics(Statastics statastics) {
        this.statastics = statastics;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Stage [name=");
        builder.append(name);
        builder.append(", stepHeader=");
        builder.append(stepHeader);
        builder.append(", data=");
        builder.append(data);
        builder.append(", statastics=");
        builder.append(statastics);
        builder.append(", isPending=");
        builder.append(isPending);
        builder.append(", date=");
        builder.append(date);
        builder.append(", timeStamp=");
        builder.append(timeStamp);
        builder.append(", bucketId=");
        builder.append(bucketId);
        builder.append(", stageId=");
        builder.append(stageId);
        builder.append("]");
        return builder.toString();
    }

}
