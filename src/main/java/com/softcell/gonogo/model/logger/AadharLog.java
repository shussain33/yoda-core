/**
 * yogeshb5:56:01 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequestNew;
import com.softcell.gonogo.model.core.kyc.request.aadhar.ClientAadhaarRequest;
import com.softcell.gonogo.model.core.kyc.response.aadhar.AadharMainResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


/**
 * @author yogeshb
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "AadharLog")
public class AadharLog extends AuditEntity {

    @JsonProperty("oClientRequest")
    @Field("AadharClientRequest")
    private ClientAadhaarRequest aadharClientRequest;

    @JsonProperty("oAadhaarRequest")
    private AadhaarRequest aadhaarRequest;

    @JsonProperty("oAadhaarRequestNew")
    private AadhaarRequestNew aadhaarRequestNew;

    @JsonProperty("oAadhaarResponse")
    private AadharMainResponse aadharMainResponse;

    @JsonProperty("sGNGStatus")   // gonogo status
    private String gNGstatus;

    @JsonProperty("sGNGStatusDtl")   // service failure reason.
    private String gNGStatusDtl;

    @JsonProperty("sEKycStatus")   // EKyc status
    private String eKycStatus;

    @JsonProperty("sEKycStatusDtl")   // service failure reason.
    private String eKycStatusDtl;

    @JsonProperty("sUIDAIStatus")   // UIDAI status
    private String uIDAIStatus;

    @JsonProperty("sUIDAIStatusDtl")
    private String uIDAIStatusDtl;

}
