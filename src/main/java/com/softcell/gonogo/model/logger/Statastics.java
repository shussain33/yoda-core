package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.softcell.constants.DateUnit;

/**
 * @author kishorp
 */
public class Statastics {
    @JsonIgnore
    private double minima;
    @JsonIgnore
    private double maxima;
    @JsonIgnore
    private double average;
    @JsonIgnore
    private double expected;
    @JsonIgnore
    private double empirical;
    private long duration;
    private long cummulativeDuration;

    private String text;
    private DateUnit.UNIT unit = DateUnit.UNIT.ms;

    public double getMinima() {
        return minima;
    }

    public void setMinima(double minima) {
        this.minima = minima;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getCummulativeDuration() {
        return cummulativeDuration;
    }

    public void setCummulativeDuration(long cummulativeDuration) {
        this.cummulativeDuration = cummulativeDuration;
    }

    public double getMaxima() {
        return maxima;
    }

    public void setMaxima(double maxima) {
        this.maxima = maxima;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getExpected() {
        return expected;
    }

    public void setExpected(double expected) {
        this.expected = expected;
    }

    public double getEmpirical() {
        return empirical;
    }

    public void setEmpirical(double empirical) {
        this.empirical = empirical;
    }

    public DateUnit.UNIT getUnit() {
        return unit;
    }

    public void setUnit(DateUnit.UNIT unit) {
        this.unit = unit;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Statastics [minima=");
        builder.append(minima);
        builder.append(", maxima=");
        builder.append(maxima);
        builder.append(", average=");
        builder.append(average);
        builder.append(", expected=");
        builder.append(expected);
        builder.append(", empirical=");
        builder.append(empirical);
        builder.append(", duration=");
        builder.append(duration);
        builder.append(", cummulativeDuration=");
        builder.append(cummulativeDuration);
        builder.append(", text=");
        builder.append(text);
        builder.append(", unit=");
        builder.append(unit);
        builder.append("]");
        return builder.toString();
    }


}
