package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author kishorp
 */
@JsonInclude(Include.NON_NULL)
public class ApplicationCaseHistory {


    @JsonIgnore
    private String refId;
    @JsonProperty()
    private List<Stage> stages;

    @JsonIgnore
    private Statastics statastics;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    public Statastics getStatastics() {
        return statastics;
    }

    public void setStatastics(Statastics statastics) {
        this.statastics = statastics;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ApplicationCaseHistory [refId=");
        builder.append(refId);
        builder.append(", stages=");
        builder.append(stages);
        builder.append(", statastics=");
        builder.append(statastics);
        builder.append("]");
        return builder.toString();
    }

}
