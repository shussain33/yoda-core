/**
 * kishorp10:45:34 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author kishorp
 *
 */
@Aspect
public class WorkFlowLogger {

    public static final Logger logger = LoggerFactory.getLogger(WorkFlowLogger.class);

    @Around("execution(* com.softcell.workflow.component.manager.execute())")
    public void componentLogger(ProceedingJoinPoint proceedingJoinPoint) {

        try {
            proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            logger.error("{}",e.getStackTrace());
        }
    }

}
