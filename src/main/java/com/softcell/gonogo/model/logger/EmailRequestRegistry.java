/**
 * yogeshb9:39:47 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.logger;

import org.joda.time.DateTime;

/**
 * @author yogeshb
 *
 */
public class EmailRequestRegistry {
    private DateTime requestTime = new DateTime();
    private String status = "ip";
    private String dedupeKey;

    public DateTime getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(DateTime requestTime) {
        this.requestTime = requestTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDedupeKey() {
        return dedupeKey;
    }

    public void setDedupeKey(String dedupeKey) {
        this.dedupeKey = dedupeKey;
    }


}
