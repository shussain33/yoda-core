/**
 * yogeshb5:35:04 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author yogeshb
 */
@Document(collection = "loggerDoc")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtpLog extends AuditEntity {
    @Id
    private String refID;
    @JsonIgnore
    private Date startTime;
    @JsonIgnore
    private Date endTime;
    private String startTimeToString;
    private String endTimeToString;
    private String institutionID;
    private String dsaID;
    private String mobileNumber;
    private String otp;
    private String emailID;
    private long turnAroundTime;

    public static Builder builder() {
        return new Builder();
    }


    public long getTurnAroundTime() {
        return turnAroundTime;
    }

    public void setTurnAroundTime(long turnAroundTime) {
        this.turnAroundTime = turnAroundTime;
    }

    public String getStartTimeToString() {
        return startTimeToString;
    }

    public void setStartTimeToString(String startTimeToString) {
        this.startTimeToString = startTimeToString;
    }

    public String getEndTimeToString() {
        return endTimeToString;
    }

    public void setEndTimeToString(String endTimeToString) {
        this.endTimeToString = endTimeToString;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getInstitutionID() {
        return institutionID;
    }

    public void setInstitutionID(String institutionID) {
        this.institutionID = institutionID;
    }

    public String getDsaID() {
        return dsaID;
    }

    public void setDsaID(String dsaID) {
        this.dsaID = dsaID;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    @Override
    public String toString() {
        return "WorkFlowLog [refID=" + refID + ", startTime=" + startTime
                + ", endTime=" + endTime + ", startTimeToString="
                + startTimeToString + ", endTimeToString=" + endTimeToString
                + ", institutionID=" + institutionID + ", dsaID=" + dsaID
                + ", mobileNumber=" + mobileNumber + ", otp=" + otp
                + ", emailID=" + emailID + "]";
    }


    public static final class Builder {

        OtpLog otpLog = new OtpLog();

        private Builder() {
        }

        public OtpLog build() {
            return this.otpLog;
        }

        public Builder refID(String refID) {
            this.otpLog.refID = refID;
            return this;
        }

        public Builder startTime(Date startTime) {
            this.otpLog.startTime = startTime;
            return this;
        }

        public Builder endTime(Date endTime) {
            this.otpLog.endTime = endTime;
            return this;
        }

        public Builder startTimeToString(String startTimeToString) {
            this.otpLog.startTimeToString = startTimeToString;
            return this;
        }

        public Builder endTimeToString(String endTimeToString) {
            this.otpLog.endTimeToString = endTimeToString;
            return this;
        }

        public Builder institutionID(String institutionID) {
            this.otpLog.institutionID = institutionID;
            return this;
        }

        public Builder dsaID(String dsaID) {
            this.otpLog.dsaID = dsaID;
            return this;
        }

        public Builder mobileNumber(String mobileNumber) {
            this.otpLog.mobileNumber = mobileNumber;
            return this;
        }

        public Builder otp(String otp) {
            this.otpLog.otp = otp;
            return this;
        }

        public Builder emailID(String emailID) {
            this.otpLog.emailID = emailID;
            return this;
        }

        public Builder turnAroundTime(long turnAroundTime) {
            this.otpLog.turnAroundTime = turnAroundTime;
            return this;
        }
    }
}