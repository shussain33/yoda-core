package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAuthenticationRequest;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaClientRequest;
import com.softcell.gonogo.model.core.kyc.response.karza.KarzaClientResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by yogesh Khandare on 7/10/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "KarzaLog")
public class KarzaLog {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sDocumentId")
    private String documentId;

    @JsonProperty("sDocumentType")
    private String documentType;

    @JsonProperty("dAuthDate")
    private Date authDate=new Date();

    @JsonProperty("oKarzaClientRequest")
    private KarzaClientRequest karzaClientRequest;

    @JsonProperty("oKarzaAuthRequest")
    private KarzaAuthenticationRequest karzaAuthRequest;

    @JsonProperty("oKarzaClientResponse")
    private KarzaClientResponse karzaClientResponse;

    @JsonProperty("sKarzaStatus")
    private String karzaStatus;

    @JsonProperty("sSslApplicationId")
    private String sslApplicationId;

}
