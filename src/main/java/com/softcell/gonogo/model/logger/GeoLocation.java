/**
 * yogeshb5:10:53 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author yogeshb
 *
 */
public class GeoLocation implements Serializable {

    @JsonProperty("dLtd")
    private double latitude;

    @JsonProperty("dLnge")
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "GeoLocation [latitude=" + latitude + ", longitude=" + longitude
                + "]";
    }
}
