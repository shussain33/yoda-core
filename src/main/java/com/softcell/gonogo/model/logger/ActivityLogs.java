/**
 * kishorp1:22:52 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author kishorp
 *
 */
@Document(collection = "activityLogs")
public class ActivityLogs extends AuditEntity{

    @JsonProperty("dtAction")
    private Date actionDate = new Date();

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("sApplicationID")
    private String applicationId;

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("sUserRole")
    private String userRole;

    @JsonProperty("sAction")
    private String action;

    @JsonProperty("sCustMsg")
    private String customMsg;

    @JsonProperty("sIpAddress")
    private String ipAddress;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sStep")
    private String step;

    @JsonProperty("sStage")
    private String stage;

    @JsonProperty("sChangedStage")
    private String changedStage;

    @JsonProperty("sOldAppStatus")
    private String oldAppStatus;

    @JsonProperty("sOldAppStage")
    private String oldAppStage;

    @JsonProperty("sCurrentAppStatus")
    private String currentAppStatus;

    @JsonProperty("sCurrentAppStage")
    private String currentAppStage;

    @JsonProperty("lDuration")
    private long duration;

    @JsonProperty("bReadFlag")
    private boolean readFlag = false;

    @JsonProperty("SPhase")
    private String phase;

    @JsonProperty("sIMEI")
    private String imeiNumber;

    @JsonProperty("sSource")
    private String source;

    @JsonProperty("sRemarks")
    private String remarks;

    @JsonProperty("sUserId")
    private String userId;

    @JsonProperty("sCurrentUserId")
    private String currentUserId;

    @JsonProperty("sCurrentUserName")
    private String currentUserName;

    @JsonProperty("sChangedUserId")
    private String changedUserId;

    @JsonProperty("sChangedUserName")
    private String changedUserName;

    public String getRemarks() { return remarks;  }

    public void setRemarks(String remarks) { this.remarks = remarks; }

    public String getUserId() { return userId; }

    public void setUserId(String userId) { this.userId = userId; }

    public String getCurrentUserId() { return currentUserId; }

    public void setCurrentUserId(String currentUserId) { this.currentUserId = currentUserId; }

    public String getCurrentUserName() { return currentUserName; }

    public void setCurrentUserName(String currentUserName) { this.currentUserName = currentUserName; }

    public String getChangedUserId() { return changedUserId; }

    public void setChangedUserId(String changedUserId) { this.changedUserId = changedUserId; }

    public String getChangedUserName() { return changedUserName; }

    public void setChangedUserName(String changedUserName) { this.changedUserName = changedUserName; }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public Date getActionDate() {
        return actionDate;
    }

    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCustomMsg() {
        return customMsg;
    }

    public void setCustomMsg(String customMsg) {
        this.customMsg = customMsg;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getChangedStage() {
        return changedStage;
    }

    public void setChangedStage(String changedStage) {
        this.changedStage = changedStage;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isReadFlag() { return readFlag; }

    public void setReadFlag(boolean readFlag) { this.readFlag = readFlag; }

    public String getOldAppStatus() {
        return oldAppStatus;
    }

    public void setOldAppStatus(String oldAppStatus) {
        this.oldAppStatus = oldAppStatus;
    }

    public String getOldAppStage() {
        return oldAppStage;
    }

    public void setOldAppStage(String oldAppStage) {
        this.oldAppStage = oldAppStage;
    }

    public String getCurrentAppStatus() {
        return currentAppStatus;
    }

    public void setCurrentAppStatus(String currentAppStatus) {
        this.currentAppStatus = currentAppStatus;
    }

    public String getCurrentAppStage() {
        return currentAppStage;
    }

    public void setCurrentAppStage(String currentAppStage) {
        this.currentAppStage = currentAppStage;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getImeiNumber() {  return imeiNumber; }

    public void setImeiNumber(String imeiNumber) { this.imeiNumber = imeiNumber; }

    public String getSource() { return source;  }

    public void setSource(String source) { this.source = source; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ActivityLogs that = (ActivityLogs) o;

        if (duration != that.duration) return false;
        if (readFlag != that.readFlag) return false;
        if (actionDate != null ? !actionDate.equals(that.actionDate) : that.actionDate != null) return false;
        if (refId != null ? !refId.equals(that.refId) : that.refId != null) return false;
        if (applicationId != null ? !applicationId.equals(that.applicationId) : that.applicationId != null)
            return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        if (customMsg != null ? !customMsg.equals(that.customMsg) : that.customMsg != null) return false;
        if (ipAddress != null ? !ipAddress.equals(that.ipAddress) : that.ipAddress != null) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (step != null ? !step.equals(that.step) : that.step != null) return false;
        if (stage != null ? !stage.equals(that.stage) : that.stage != null) return false;
        if (oldAppStatus != null ? !oldAppStatus.equals(that.oldAppStatus) : that.oldAppStatus != null) return false;
        if (oldAppStage != null ? !oldAppStage.equals(that.oldAppStage) : that.oldAppStage != null) return false;
        if (currentAppStatus != null ? !currentAppStatus.equals(that.currentAppStatus) : that.currentAppStatus != null)
            return false;
        return !(currentAppStage != null ? !currentAppStage.equals(that.currentAppStage) : that.currentAppStage != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (actionDate != null ? actionDate.hashCode() : 0);
        result = 31 * result + (refId != null ? refId.hashCode() : 0);
        result = 31 * result + (applicationId != null ? applicationId.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (customMsg != null ? customMsg.hashCode() : 0);
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        result = 31 * result + (institutionId != null ? institutionId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (step != null ? step.hashCode() : 0);
        result = 31 * result + (stage != null ? stage.hashCode() : 0);
        result = 31 * result + (oldAppStatus != null ? oldAppStatus.hashCode() : 0);
        result = 31 * result + (oldAppStage != null ? oldAppStage.hashCode() : 0);
        result = 31 * result + (currentAppStatus != null ? currentAppStatus.hashCode() : 0);
        result = 31 * result + (currentAppStage != null ? currentAppStage.hashCode() : 0);
        result = 31 * result + (int) (duration ^ (duration >>> 32));
        result = 31 * result + (readFlag ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ActivityLogs{");
        sb.append("actionDate=").append(actionDate);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", applicationId='").append(applicationId).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", action='").append(action).append('\'');
        sb.append(", customMsg='").append(customMsg).append('\'');
        sb.append(", ipAddress='").append(ipAddress).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", step='").append(step).append('\'');
        sb.append(", stage='").append(stage).append('\'');
        sb.append(", oldAppStatus='").append(oldAppStatus).append('\'');
        sb.append(", oldAppStage='").append(oldAppStage).append('\'');
        sb.append(", currentAppStatus='").append(currentAppStatus).append('\'');
        sb.append(", currentAppStage='").append(currentAppStage).append('\'');
        sb.append(", duration=").append(duration);
        sb.append(", readFlag=").append(readFlag);
        sb.append('}');
        return sb.toString();
    }

}
