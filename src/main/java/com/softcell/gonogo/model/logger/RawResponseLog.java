package com.softcell.gonogo.model.logger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.ActionName;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * It will keep log of all raw response coming from services like
 * MultiBureau , Scoring , NTC.
 * We can differentiate between two response based on responseType.
 * responseType enum can be referred form
 *
 * Note : Currently only for MB.
 * Created by bhuvneshk on 16/9/17.
 */

@Data
@Builder
@Document(collection = "rawResponseLog")
public class RawResponseLog {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sResponseType")
    private ActionName responseType;

    @JsonProperty("sRawResponse")
    private String rawResponse;
}
