/**
 * yogeshb3:30:35 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.logger;

import java.util.Date;

/**
 * @author yogeshb
 *
 */
public class AuthenticationLogger {
    private String userName;
    private String institutionId;
    private Date startTime;
    private Date endTime;
    private String loginStatus;
    private String ipAddress;
    private String agent;
    private String source;
    private String contentType;
    private String statusCode;
    private String productType;
    private GeoLocation geoLocation;
    private String protocal;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public String getProtocal() {
        return protocal;
    }

    public void setProtocal(String protocal) {
        this.protocal = protocal;
    }

    @Override
    public String toString() {
        return "AuthenticationLogger [userName=" + userName
                + ", institutionId=" + institutionId + ", startTime="
                + startTime + ", endTime=" + endTime + ", loginStatus="
                + loginStatus + ", ipAddress=" + ipAddress + ", agent=" + agent
                + ", source=" + source + ", contentType=" + contentType
                + ", statusCode=" + statusCode + ", productType=" + productType
                + ", geoLocation=" + geoLocation + ", protocal=" + protocal
                + "]";
    }

}
