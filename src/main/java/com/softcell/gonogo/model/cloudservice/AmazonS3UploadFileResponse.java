package com.softcell.gonogo.model.cloudservice;

/**
 * This class represents the response model for amazon S3 file upload respoonse.
 *
 * @author vinodk
 */
public class AmazonS3UploadFileResponse {

    private Status status;
    private String responseMessage;
    private String errorMessage;
    private String s3Url;

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
        setResponseMessage(status.getMsg());
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    private void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the s3Url
     */
    public String getS3Url() {
        return s3Url;
    }

    /**
     * @param s3Url the s3Url to set
     */
    public void setS3Url(String s3Url) {
        this.s3Url = s3Url;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AmazonS3UploadFileResponse [status=");
        builder.append(status);
        builder.append(", responseMessage=");
        builder.append(responseMessage);
        builder.append(", errorMessage=");
        builder.append(errorMessage);
        builder.append(", s3Url=");
        builder.append(s3Url);
        builder.append("]");
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((errorMessage == null) ? 0 : errorMessage.hashCode());
        result = prime * result
                + ((responseMessage == null) ? 0 : responseMessage.hashCode());
        result = prime * result + ((s3Url == null) ? 0 : s3Url.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AmazonS3UploadFileResponse other = (AmazonS3UploadFileResponse) obj;
        if (errorMessage == null) {
            if (other.errorMessage != null)
                return false;
        } else if (!errorMessage.equals(other.errorMessage))
            return false;
        if (responseMessage == null) {
            if (other.responseMessage != null)
                return false;
        } else if (!responseMessage.equals(other.responseMessage))
            return false;
        if (s3Url == null) {
            if (other.s3Url != null)
                return false;
        } else if (!s3Url.equals(other.s3Url))
            return false;
        if (status != other.status)
            return false;
        return true;
    }

    /**
     * Enum for setting the status of the upload request.
     */
    public enum Status {

        FAILED("Failed to upload"), SUCCESS("File Uploaded Successfully"), ERROR(
                "Error uploading the file to amazon S3"), IN_PROCESS(
                "File upload is in process"), INVALID_FILE_PATH(
                "Empty or null file path"), INVALID_BUCKET_NAME(
                "Empty or null bucket name"), INVALID_TARGET_FILE_NAME(
                "Empty or null target file name");

        String msg;

        /**
         * @param message set message for corresponding status.
         */
        Status(String message) {
            this.msg = message;
        }

        public String getMsg() {
            return this.msg;
        }
    }
}
