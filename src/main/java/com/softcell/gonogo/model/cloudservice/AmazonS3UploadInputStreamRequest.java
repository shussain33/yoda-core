package com.softcell.gonogo.model.cloudservice;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.mongodb.gridfs.GridFSDBFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class represents the request to upload inputstream to amazon s3.
 *
 * @author vinodk
 */
public class AmazonS3UploadInputStreamRequest {

    private InputStream inputStream;
    private ObjectMetadata objectMetaData;

    private String bucketName;
    private String targetFileName;

    /**
     * @return the inputStream
     */
    public InputStream getInputStream() {
        return inputStream;
    }

    /**
     * @param inputStream the inputStream to set
     */
    private void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * @return the objectMetaData
     */
    public ObjectMetadata getObjectMetaData() {
        return objectMetaData;
    }

    /**
     * @param objectMetaData the objectMetaData to set
     */
    private void setObjectMetaData(ObjectMetadata objectMetaData) {
        this.objectMetaData = objectMetaData;
    }

    /**
     * @return the bucketName
     */
    public String getBucketName() {
        return bucketName;
    }

    /**
     * @param bucketName the bucketName to set
     */
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    /**
     * @return the targetFileName
     */
    public String getTargetFileName() {
        return targetFileName;
    }

    /**
     * @param targetFileName the targetFileName to set
     */
    public void setTargetFileName(String targetFileName) {
        this.targetFileName = targetFileName;
    }

    /**
     * This methods maps the inputStream and objectMetadata from gridFsDbFile.
     *
     * @param gridFsDbFile File from the mongo grid database.
     */
    public void mapObjectFromMongoGridFsFile(GridFSDBFile gridFsDbFile,
                                             String bucketName, String targetFileName) {
        ObjectMetadata metaData = new ObjectMetadata();
        metaData.setContentLength(gridFsDbFile.getLength());
        metaData.setContentType(gridFsDbFile.getContentType());
        this.setObjectMetaData(metaData);
        this.setInputStream(gridFsDbFile.getInputStream());
        this.setBucketName(bucketName);
        this.setTargetFileName(targetFileName);
    }

    /**
     * @param inputStream
     * @param bucketName
     * @param targetFileName
     * @param fileContentType
     * @throws IOException
     */
    public void mapObjectFromInputStream(InputStream inputStream,
                                         String bucketName, String targetFileName, String fileContentType)
            throws IOException {
        ObjectMetadata metaData = new ObjectMetadata();
        metaData.setContentLength(inputStream.available());
        metaData.setContentType(fileContentType);
        this.setInputStream(inputStream);
        this.setBucketName(bucketName);
        this.setObjectMetaData(metaData);
        this.setTargetFileName(targetFileName);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AmazonS3UploadInputStreamRequest [inputStream=");
        builder.append(inputStream);
        builder.append(", objectMetaData=");
        builder.append(objectMetaData);
        builder.append(", bucketName=");
        builder.append(bucketName);
        builder.append(", targetFileName=");
        builder.append(targetFileName);
        builder.append("]");
        return builder.toString();
    }
}
