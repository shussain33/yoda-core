package com.softcell.gonogo.model;

/**
 * Created by yogesh on 8/12/17.
 */
public class EKycServiceException extends Exception
{
    public EKycServiceException(){super();}

    public EKycServiceException(String msg)
    {
        super(msg);
    }


}
