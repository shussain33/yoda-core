package com.softcell.gonogo.model.multibureau.commercialCibil.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderSearchItem {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@JsonProperty("SRCH-LIST-ID")
	private String bureauId;
	
	@JsonProperty("SRCH-LIST-ITEM-ACK-ID")
	private Long searchTrackingID;

	public String getBureauId() {
		return bureauId;
	}

	public void setBureauId(String bureauId) {
		this.bureauId = bureauId;
	}

	public Long getSearchTrackingID() {
		return searchTrackingID;
	}

	public void setSearchTrackingID(Long searchTrackingID) {
		this.searchTrackingID = searchTrackingID;
	}

	@Override
	public String toString() {
		return "OrderSearchItem [bureauId=" + bureauId + ", searchTrackingID="
				+ searchTrackingID + "]";
	}
}
