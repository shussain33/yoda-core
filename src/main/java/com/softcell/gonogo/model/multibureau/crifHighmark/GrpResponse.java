package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="GRP-RESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
public class GrpResponse {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="MFI")
	private String mfi;
	@XmlElement(name="MFI-ID")
	private String mfiId;
	@XmlElement(name="BRANCH")
	private String branch;
	@XmlElement(name="KENDRA")
	private String kendra;
	@XmlElement(name="NAME")
	private String name;
	@XmlElement(name="AGE")
	private String age;
	@XmlElement(name="AGE-AS-ON")
	private String ageAsOn;
	@XmlElement(name="IDS")
	private Ids ids;
	@XmlElement(name="ADDRESSES")
	private Addresses addresses;
	@XmlElement(name="RELATIONS")
	private Relations relations;
	@XmlElement(name="CNSMRMBRID")
    private String cnsmrmbrId;
	@XmlElement(name="MATCHED-TYPE")
    private String matchedType;
	@XmlElement(name="INSERT-DATE")
	private String insertDate;
	@XmlElement(name="DOB")
	private String dob;
	@XmlElement(name="PHONES")
	private Phones phones;
	@XmlElement(name="GROUP-CREATION-DATE")
	private String groupCreatedDate;
	@XmlElement(name="LOAN-DETAIL")
	private LoanDetail loanDetail;
	@XmlElement(name="GROUP-DETAILS")
	private GroupDetails groupDetails;
	@XmlElement(name="ENTITY-ID")
	private String entityId;
	@XmlElement(name="REPORT_DT")
	private String reportedDate;
	@XmlElement(name="EMAILS")
	private Emails emails;
	
	public Emails getEmails() {
		return emails;
	}

	public void setEmails(Emails emails) {
		this.emails = emails;
	}

	public String getGroupCreatedDate() {
		return groupCreatedDate;
	}

	public void setGroupCreatedDate(String groupCreatedDate) {
		this.groupCreatedDate = groupCreatedDate;
	}

	public String getReportedDate() {
		return reportedDate;
	}

	public void setReportedDate(String reportedDate) {
		this.reportedDate = reportedDate;
	}

	/***
	 * 
	 *  EMAIL DETAILS,ENTITY-ID MISSED
	 */
	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	/*public String getReportDate() {
		return reportDate;
	}
	
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}*/

	public Phones getPhones() {
		return phones;
	}
	

	public void setPhones(Phones phones) {
		this.phones = phones;
	}
	




	public String getDob() {
		return dob;
	}
	



	public void setDob(String dob) {
		this.dob = dob;
	}
	



	
	private String loanDetails;
	
	public String getLoanDetails() {
		return loanDetails;
	}
	


	public void setLoanDetails(String loanDetails) {
		this.loanDetails = loanDetails;
	}
	


	
	
	public LoanDetail getLoanDetail() {
		return loanDetail;
	}
	

	public void setLoanDetail(LoanDetail loanDetail) {
		this.loanDetail = loanDetail;
	}
	

	public String getMfi() {
		return mfi;
	}
	
	public void setMfi(String mfi) {
		this.mfi = mfi;
	}
	
	public String getMfiId() {
		return mfiId;
	}
	
	public void setMfiId(String mfiId) {
		this.mfiId = mfiId;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getKendra() {
		return kendra;
	}
	
	public void setKendra(String kendra) {
		this.kendra = kendra;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAge() {
		return age;
	}
	
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getAgeAsOn() {
		return ageAsOn;
	}
	
	public void setAgeAsOn(String ageAsOn) {
		this.ageAsOn = ageAsOn;
	}
	
	public Ids getIds() {
		return ids;
	}
	
	public void setIds(Ids ids) {
		this.ids = ids;
	}
	
	public Addresses getAddresses() {
		return addresses;
	}
	
	public void setAddresses(Addresses addresses) {
		this.addresses = addresses;
	}
	
	public Relations getRelations() {
		return relations;
	}
	
	public void setRelations(Relations relations) {
		this.relations = relations;
	}
	
	public String getCnsmrmbrId() {
		return cnsmrmbrId;
	}
	
	public void setCnsmrmbrId(String cnsmrmbrId) {
		this.cnsmrmbrId = cnsmrmbrId;
	}
	
	public String getMatchedType() {
		return matchedType;
	}
	
	public void setMatchedType(String matchedType) {
		this.matchedType = matchedType;
	}
	
	public String getInsertDate() {
		return insertDate;
	}
	
	public void setInsertDate(String insertDate) {
		this.insertDate = insertDate;
	}
	
	public GroupDetails getGroupDetails() {
		return groupDetails;
	}
	
	public void setGroupDetails(GroupDetails groupDetails) {
		this.groupDetails = groupDetails;
	}
	
	
	@Override
	public String toString() {
		return "GrpResponse [mfi=" + mfi + ", mfiId=" + mfiId + ", branch="
				+ branch + ", kendra=" + kendra + ", name=" + name + ", age="
				+ age + ", ageAsOn=" + ageAsOn + ", ids=" + ids
				+ ", addresses=" + addresses + ", relations=" + relations
				+ ", cnsmrmbrId=" + cnsmrmbrId + ", matchedType=" + matchedType
				+ ", insertDate=" + insertDate + ", dob=" + dob + ", phones="
				+ phones + ", groupCreatedDate=" + groupCreatedDate
				+ ", loanDetail=" + loanDetail + ", groupDetails="
				+ groupDetails + ", entityId=" + entityId + ", reportedDate="
				+ reportedDate + ", emails=" + emails + ", loanDetails="
				+ loanDetails + "]";
	}
}
