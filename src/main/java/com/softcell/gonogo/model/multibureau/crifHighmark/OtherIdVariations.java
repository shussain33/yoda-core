package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class OtherIdVariations {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="VARIATION")
	private List<Variation> variations;

	public List<Variation> getVariations() {
		return variations;
	}
	

	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}
}
