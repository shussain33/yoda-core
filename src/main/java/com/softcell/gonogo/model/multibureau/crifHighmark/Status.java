package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="STATUS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Status {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="OPTION")
	private String option;
	@XmlElement(name="OPTION-STATUS")
	private String optionStatus;
	@XmlElement(name="ERRORS")
	//private String errors;
	private Errors errorObj;
	
	public String getOption() {
		return option;
	}
	
	public void setOption(String option) {
		this.option = option;
	}
	
	public String getOptionStatus() {
		return optionStatus;
	}
	
	public void setOptionStatus(String optionStatus) {
		this.optionStatus = optionStatus;
	}

	public Errors getErrorObj() {
		return errorObj;
	}

	public void setErrorObj(Errors errorObj) {
		this.errorObj = errorObj;
	}

	@Override
	public String toString() {
		return "Status [option=" + option + ", optionStatus=" + optionStatus
				+ ", errorObj=" + errorObj + "]";
	}
}
