package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

;

public class Request {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@JsonProperty("01")
	private String priority;
	
	@JsonProperty("02")
	private String productType;
	
	@JsonProperty("03")
	private String loanType;				  
	
	@JsonProperty("04")
	private String loanAmount;
	
	@JsonProperty("05")
	private String jointInd;
	
	@JsonProperty("06")
	private String inquirySubmittedBy;
	
	@JsonProperty("07")
	private String sourceSystemName;
	
	@JsonProperty("08")
	private String sourceSystemVersion;
	
	@JsonProperty("09")
	private String sourceSystemVender;
	
	@JsonProperty("10")
	private String sourceSystemInstanceId;
	
	@JsonProperty("11")
	private String bureauRegion;			  
	
	@JsonProperty("12")
	private String loanPurposeDesc;		  
	
	@JsonProperty("13")
	private String branchIfscCode;
	
	@JsonProperty("14")
	private String kendra;
	
	@JsonProperty("15")
	private String inquiryStage;	
	
	@JsonProperty("16")
	private String authrizationFlag;
	
	@JsonProperty("17")
	private String authroizedBy;
	
	@JsonProperty("19")
	private String individualCorporateFlag;		  
	
	@JsonProperty("20")
	private String constitution;
	
	@JsonProperty("21")
	private String name;
	
	@JsonProperty("22")
	private String shortName;
	
	@JsonProperty("23")
	private String incorpDt;
	
	@JsonProperty("24")
	private List<AddressDomain> address;
	
	@JsonProperty("25")
	private IDDomain id;						  
	
	@JsonProperty("26")
	private PhoneDomain phone;
	
	@JsonProperty("27")
	private String actOpeningDt;
	
	@JsonProperty("28")
	private String accountNumber1;
	
	@JsonProperty("29")
	private String accountNumber2;
	
	@JsonProperty("30")
	private String accountNumber3;
	
	@JsonProperty("31")
	private String accountNumber4;
	
	@JsonProperty("32")
	private String tenure;
	
	@JsonProperty("33")
	private String groupId;
	
	@JsonProperty("34")
	private String companyCategory;
	
	@JsonProperty("35")
	private String natureOfBusiness;
	
	//added generac
	@JsonProperty("36")
    private IndividualEntitiesSegment individualEntities;
	
	@JsonProperty("37")
    private OrganisationEntitiesSegment organisationEntities;

	
	//Changed the tag name as this is specific to HM comm
	//START ACE	
	@JsonProperty("46")
    private IndividualEntitiesSegment individualEntitiesSegment;
	
	@JsonProperty("47")
    private OrganisationEntitiesSegment organisationEntitiesSegment;
	
	@JsonProperty("38")
	private String legalConstitution;
	
	/*@JsonProperty("37")
    private String serviceTaxNo;*/
    
	@JsonProperty("39")
	private String classOfActivity1;
	
	@JsonProperty("40")
	private String classOfActivity2;
	
	@JsonProperty("41")
	private String classOfActivity3;
	
	@JsonProperty("42")
	private String fax1;
	
	@JsonProperty("43")
	private String fax2;
	
	@JsonProperty("44")
	private String fax3;
	
	@JsonProperty("45")
	private String creditRptId;
	
	@JsonProperty("48")
	private String bureauProductType;
	
	@JsonProperty("49")
	private String registration_dt;

	@JsonProperty("50")
	private String cmrFlag;
	
    
	
//	private String accountNumber;
//	private String age;
//	private String ageOnDate;
//	private String appId;
//	private String appScore;
//	private String assetCost;
//	private String behaviourScore;
//	private String branch;
//	private String center;

//	private String collateral;
//	private String collateralValuation1;
//	private String collateralValuation2;
	
	
//	private String custId;
//	private String customerSegment;
//	private String ddeScore;
//	private String debitScore;

//	private String email;

	
//	private String fileName;
	

//	private String monthlyIncome;


	
	
//	private String sourceSystem;

	
//	private Long insertedBy; 
//	private Long institutionId;
//	private String bureau;
	
	
    //END ACE
    
	
	public String getPriority() {
		return priority;
	}
	public String getRegistration_dt() {
		return registration_dt;
	}
	public void setRegistration_dt(String registration_dt) {
		this.registration_dt = registration_dt;
	}
	
	public String getCmrFlag() {
		return cmrFlag;
	}
	public void setCmrFlag(String cmrFlag) {
		this.cmrFlag = cmrFlag;
	}
	public IndividualEntitiesSegment getIndividualEntities() {
		return individualEntities;
	}
	public void setIndividualEntities(
			IndividualEntitiesSegment individualEntities) {
		this.individualEntities = individualEntities;
	}
	public OrganisationEntitiesSegment getOrganisationEntities() {
		return organisationEntities;
	}
	public void setOrganisationEntities(
			OrganisationEntitiesSegment organisationEntities) {
		this.organisationEntities = organisationEntities;
	}
	public IndividualEntitiesSegment getIndividualEntitiesSegment() {
		return individualEntitiesSegment;
	}
	public void setIndividualEntitiesSegment(
			IndividualEntitiesSegment individualEntitiesSegment) {
		this.individualEntitiesSegment = individualEntitiesSegment;
	}
	public OrganisationEntitiesSegment getOrganisationEntitiesSegment() {
		return organisationEntitiesSegment;
	}
	public void setOrganisationEntitiesSegment(
			OrganisationEntitiesSegment organisationEntitiesSegment) {
		this.organisationEntitiesSegment = organisationEntitiesSegment;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public String getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getJointInd() {
		return jointInd;
	}
	public void setJointInd(String jointInd) {
		this.jointInd = jointInd;
	}
	public String getInquirySubmittedBy() {
		return inquirySubmittedBy;
	}
	public void setInquirySubmittedBy(String inquirySubmittedBy) {
		this.inquirySubmittedBy = inquirySubmittedBy;
	}
	public String getSourceSystemName() {
		return sourceSystemName;
	}
	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}
	public String getSourceSystemVersion() {
		return sourceSystemVersion;
	}
	public void setSourceSystemVersion(String sourceSystemVersion) {
		this.sourceSystemVersion = sourceSystemVersion;
	}
	public String getSourceSystemVender() {
		return sourceSystemVender;
	}
	public void setSourceSystemVender(String sourceSystemVender) {
		this.sourceSystemVender = sourceSystemVender;
	}
	public String getSourceSystemInstanceId() {
		return sourceSystemInstanceId;
	}
	public void setSourceSystemInstanceId(String sourceSystemInstanceId) {
		this.sourceSystemInstanceId = sourceSystemInstanceId;
	}
	public String getBureauRegion() {
		return bureauRegion;
	}
	public void setBureauRegion(String bureauRegion) {
		this.bureauRegion = bureauRegion;
	}
	public String getLoanPurposeDesc() {
		return loanPurposeDesc;
	}
	public void setLoanPurposeDesc(String loanPurposeDesc) {
		this.loanPurposeDesc = loanPurposeDesc;
	}
	public String getBranchIfscCode() {
		return branchIfscCode;
	}
	public void setBranchIfscCode(String branchIfscCode) {
		this.branchIfscCode = branchIfscCode;
	}
	public String getKendra() {
		return kendra;
	}
	public void setKendra(String kendra) {
		this.kendra = kendra;
	}
	public String getInquiryStage() {
		return inquiryStage;
	}
	public void setInquiryStage(String inquiryStage) {
		this.inquiryStage = inquiryStage;
	}
	public String getAuthrizationFlag() {
		return authrizationFlag;
	}
	public void setAuthrizationFlag(String authrizationFlag) {
		this.authrizationFlag = authrizationFlag;
	}
	public String getIndividualCorporateFlag() {
		return individualCorporateFlag;
	}
	public void setIndividualCorporateFlag(String individualCorporateFlag) {
		this.individualCorporateFlag = individualCorporateFlag;
	}
	public String getAuthroizedBy() {
		return authroizedBy;
	}
	public void setAuthroizedBy(String authroizedBy) {
		this.authroizedBy = authroizedBy;
	}
	public String getConstitution() {
		return constitution;
	}
	public void setConstitution(String constitution) {
		this.constitution = constitution;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIncorpDt() {
		return incorpDt;
	}
	public void setIncorpDt(String incorpDt) {
		this.incorpDt = incorpDt;
	}
    public List<AddressDomain> getAddress() {
		return address;
	}

	public void setAddress(List<AddressDomain> address) {
		this.address = address;
	}

	public IDDomain getId() {
		return id;
	}
	public void setId(IDDomain id) {
		this.id = id;
	}
	public PhoneDomain getPhone() {
		return phone;
	}
	public void setPhone(PhoneDomain phone) {
		this.phone = phone;
	}
	public String getActOpeningDt() {
		return actOpeningDt;
	}
	public void setActOpeningDt(String actOpeningDt) {
		this.actOpeningDt = actOpeningDt;
	}
	public String getAccountNumber1() {
		return accountNumber1;
	}
	public void setAccountNumber1(String accountNumber1) {
		this.accountNumber1 = accountNumber1;
	}
	public String getAccountNumber2() {
		return accountNumber2;
	}
	public void setAccountNumber2(String accountNumber2) {
		this.accountNumber2 = accountNumber2;
	}
	public String getAccountNumber3() {
		return accountNumber3;
	}
	public void setAccountNumber3(String accountNumber3) {
		this.accountNumber3 = accountNumber3;
	}
	public String getAccountNumber4() {
		return accountNumber4;
	}
	public void setAccountNumber4(String accountNumber4) {
		this.accountNumber4 = accountNumber4;
	}
	public String getTenure() {
		return tenure;
	}
	public void setTenure(String tenure) {
		this.tenure = tenure;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getCompanyCategory() {
		return companyCategory;
	}
	public void setCompanyCategory(String companyCategory) {
		this.companyCategory = companyCategory;
	}
	public String getNatureOfBusiness() {
		return natureOfBusiness;
	}
	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getLegalConstitution() {
		return legalConstitution;
	}
	public void setLegalConstitution(String legalConstitution) {
		this.legalConstitution = legalConstitution;
	}
	public String getClassOfActivity1() {
		return classOfActivity1;
	}
	public void setClassOfActivity1(String classOfActivity1) {
		this.classOfActivity1 = classOfActivity1;
	}
	public String getClassOfActivity2() {
		return classOfActivity2;
	}
	public void setClassOfActivity2(String classOfActivity2) {
		this.classOfActivity2 = classOfActivity2;
	}
	public String getClassOfActivity3() {
		return classOfActivity3;
	}
	public void setClassOfActivity3(String classOfActivity3) {
		this.classOfActivity3 = classOfActivity3;
	}
	public String getFax1() {
		return fax1;
	}
	public void setFax1(String fax1) {
		this.fax1 = fax1;
	}
	public String getFax2() {
		return fax2;
	}
	public void setFax2(String fax2) {
		this.fax2 = fax2;
	}
	public String getFax3() {
		return fax3;
	}
	public void setFax3(String fax3) {
		this.fax3 = fax3;
	}
	public String getCreditRptId() {
		return creditRptId;
	}
	public void setCreditRptId(String creditRptId) {
		this.creditRptId = creditRptId;
	}
	public String getBureauProductType() {
		return bureauProductType;
	}
	public void setBureauProductType(String bureauProductType) {
		this.bureauProductType = bureauProductType;
	}
	@Override
	public String toString() {
		return "Request [priority=" + priority + ", productType=" + productType
				+ ", loanType=" + loanType + ", loanAmount=" + loanAmount
				+ ", jointInd=" + jointInd + ", inquirySubmittedBy="
				+ inquirySubmittedBy + ", sourceSystemName=" + sourceSystemName
				+ ", sourceSystemVersion=" + sourceSystemVersion
				+ ", sourceSystemVender=" + sourceSystemVender
				+ ", sourceSystemInstanceId=" + sourceSystemInstanceId
				+ ", bureauRegion=" + bureauRegion + ", loanPurposeDesc="
				+ loanPurposeDesc + ", branchIfscCode=" + branchIfscCode
				+ ", kendra=" + kendra + ", inquiryStage=" + inquiryStage
				+ ", authrizationFlag=" + authrizationFlag + ", authroizedBy="
				+ authroizedBy + ", individualCorporateFlag="
				+ individualCorporateFlag + ", constitution=" + constitution
				+ ", name=" + name + ", shortName=" + shortName + ", incorpDt="
				+ incorpDt + ", address=" + address + ", id=" + id + ", phone="
				+ phone + ", actOpeningDt=" + actOpeningDt
				+ ", accountNumber1=" + accountNumber1 + ", accountNumber2="
				+ accountNumber2 + ", accountNumber3=" + accountNumber3
				+ ", accountNumber4=" + accountNumber4 + ", tenure=" + tenure
				+ ", groupId=" + groupId + ", companyCategory="
				+ companyCategory + ", natureOfBusiness=" + natureOfBusiness
				+ ", individualEntities=" + individualEntities
				+ ", organisationEntities=" + organisationEntities
				+ ", individualEntitiesSegment=" + individualEntitiesSegment
				+ ", organisationEntitiesSegment="
				+ organisationEntitiesSegment + ", legalConstitution="
				+ legalConstitution + ", classOfActivity1=" + classOfActivity1
				+ ", classOfActivity2=" + classOfActivity2
				+ ", classOfActivity3=" + classOfActivity3 + ", fax1=" + fax1
				+ ", fax2=" + fax2 + ", fax3=" + fax3 + ", creditRptId="
				+ creditRptId + ", bureauProductType=" + bureauProductType
				+ "]";
	}
}
