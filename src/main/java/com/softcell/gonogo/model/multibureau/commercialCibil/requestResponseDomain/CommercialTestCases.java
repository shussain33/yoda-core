package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CommercialTestCases {
	/**
	 * @author Dipak
	 *
	 *
	 */
	
	
	public static void main(String[] args) {
		CommercialRequestResponseDomain domain = new CommercialRequestResponseDomain();
		Header header = new Header();
		header.setApplicationId("applicatoinId");
		header.setCustomerId("customerId");
		header.setRequestTime(new Date().toString());
		header.setRequestType("REQUEST");
		
		domain.setHeader(header);
		Request request = new Request();
		request.setName("SOFTCELL TECHNOLOGIES");
		request.setLoanAmount("250000");
		request.setLoanType("PERSONAL LOAN");
		domain.setRequest(request );
		
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.create();
	    String jsonString = gson.toJson(domain);
	    
	    System.out.println(jsonString);
	    
	    CommercialRequestResponseDomain fromJson = gson.fromJson(jsonString, CommercialRequestResponseDomain.class);
	    System.out.println("Domain   : -"+fromJson);
		
	}
}
