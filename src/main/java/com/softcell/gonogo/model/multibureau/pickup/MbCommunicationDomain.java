package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author prateek
 */
@Document(collection = "MB_COMMUNICATION_DOMAIN")
public class MbCommunicationDomain extends AuditEntity {

    @JsonProperty("MULTIBUREAU_ID")
    private String mbId;

    @JsonProperty("INSTITUTION_ID")
    private String institutionId;

    @JsonProperty("AGGREGATOR_ID")
    private String aggregatorId;

    @JsonProperty("MEMBER_ID")
    private String memberId;

    @JsonProperty("PASSWORD")
    private String password;

    @JsonProperty("MULTIBUREAU_URL")
    private String mbUrl;

    @JsonProperty("BASE_URL")
    private String baseUrl;

    @JsonProperty("SCORE_APP_URL")
    private String scoreAppUrl;

    @JsonProperty("SOURCE_SYSTEM")
    private String sourceSystem;

    @JsonIgnore
    private int numberOfReTry = 10;

    public int getNumberOfReTry() {
        return numberOfReTry;
    }

    public void setNumberOfReTry(int numberOfReTry) {
        this.numberOfReTry = numberOfReTry;
    }

    public String getMbId() {
        return mbId;
    }

    public void setMbId(String mbId) {
        this.mbId = mbId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getAggregatorId() {
        return aggregatorId;
    }

    public void setAggregatorId(String aggregatorId) {
        this.aggregatorId = aggregatorId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMbUrl() {
        return mbUrl;
    }

    public void setMbUrl(String mbUrl) {
        this.mbUrl = mbUrl;
    }

    public String getScoreAppUrl() {
        return scoreAppUrl;
    }

    public void setScoreAppUrl(String scoreAppUrl) {
        this.scoreAppUrl = scoreAppUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public static Builder builder(){
        return new Builder();
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MbCommunicationDomain [mbId=");
        builder.append(mbId);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", aggregatorId=");
        builder.append(aggregatorId);
        builder.append(", memberId=");
        builder.append(memberId);
        builder.append(", password=");
        builder.append(password);
        builder.append(", mbUrl=");
        builder.append(mbUrl);
        builder.append(", baseUrl=");
        builder.append(baseUrl);
        builder.append(", scoreAppUrl=");
        builder.append(scoreAppUrl);
        builder.append(", numberOfReTry=");
        builder.append(numberOfReTry);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((aggregatorId == null) ? 0 : aggregatorId.hashCode());
        result = prime * result + ((baseUrl == null) ? 0 : baseUrl.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result + ((mbId == null) ? 0 : mbId.hashCode());
        result = prime * result + ((mbUrl == null) ? 0 : mbUrl.hashCode());
        result = prime * result
                + ((memberId == null) ? 0 : memberId.hashCode());
        result = prime * result + numberOfReTry;
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        result = prime * result
                + ((scoreAppUrl == null) ? 0 : scoreAppUrl.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof MbCommunicationDomain))
            return false;
        MbCommunicationDomain other = (MbCommunicationDomain) obj;
        if (aggregatorId == null) {
            if (other.aggregatorId != null)
                return false;
        } else if (!aggregatorId.equals(other.aggregatorId))
            return false;
        if (baseUrl == null) {
            if (other.baseUrl != null)
                return false;
        } else if (!baseUrl.equals(other.baseUrl))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (mbId == null) {
            if (other.mbId != null)
                return false;
        } else if (!mbId.equals(other.mbId))
            return false;
        if (mbUrl == null) {
            if (other.mbUrl != null)
                return false;
        } else if (!mbUrl.equals(other.mbUrl))
            return false;
        if (memberId == null) {
            if (other.memberId != null)
                return false;
        } else if (!memberId.equals(other.memberId))
            return false;
        if (numberOfReTry != other.numberOfReTry)
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (scoreAppUrl == null) {
            if (other.scoreAppUrl != null)
                return false;
        } else if (!scoreAppUrl.equals(other.scoreAppUrl))
            return false;
        return true;
    }

    public static class Builder {
        private MbCommunicationDomain mbCommunicationDomain = new MbCommunicationDomain();

        public MbCommunicationDomain build(){
            return this.mbCommunicationDomain;
        }

        public Builder mbId(String mbId){
            this.mbCommunicationDomain.mbId =  mbId;
            return this;
        }

        public Builder institutionId(String institutionId){
            this.mbCommunicationDomain.institutionId = institutionId;
            return this;
        }

        public Builder aggregatorId(String aggregrator){
            this.mbCommunicationDomain.aggregatorId = aggregrator;
            return this;
        }

        public Builder memberId(String memberId){
            this.mbCommunicationDomain.memberId = memberId;
            return this;
        }

        public Builder password(String password){
            this.mbCommunicationDomain.password = password;
            return this;
        }

        public Builder mbUrl(String mbUrl){
            this.mbCommunicationDomain.mbUrl = mbUrl;
            return this;
        }

        public Builder baseUrl(String baseUrl){
            this.mbCommunicationDomain.baseUrl = baseUrl;
            return this;
        }

        public Builder scoreAppUrl(String scoreAppUrl){
            this.mbCommunicationDomain.scoreAppUrl = scoreAppUrl;
            return this;
        }

        public Builder numberOfReTry(int numberOfReTry){
            this.mbCommunicationDomain.numberOfReTry = numberOfReTry;
            return this;
        }


    }

}