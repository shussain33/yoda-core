package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {

    @JsonProperty("BUREAU")
    private String bureau;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("JSON-RESPONSE")
    private Object jsonResponse;

    @JsonProperty("BUREAU-RRP-OBJECT")
    private Object bureauRrpObject;

    public Object getBureauRrpObject() {
        return bureauRrpObject;
    }

    public void setBureauRrpObject(Object bureauRrpObject) {
        this.bureauRrpObject = bureauRrpObject;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public Object getJsonResponse() {
        return jsonResponse;
    }

    public void setJsonResponse(Object jsonResponse) {
        this.jsonResponse = jsonResponse;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Response [bureau=" + bureau + ", product=" + product
                + ", status=" + status + ", jsonResponse=" + jsonResponse
                + ", bureauRrpObject=" + bureauRrpObject + "]";
    }


}
