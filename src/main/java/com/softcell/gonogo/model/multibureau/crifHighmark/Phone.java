package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="PHONES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Phone {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="PHONE")
	private String phone;

	public String getPhone() {
		return phone;
	}
	

	public void setPhone(String phone) {
		this.phone = phone;
	}


	@Override
	public String toString() {
		return "Phone [phone=" + phone + "]";
	}
	
}
