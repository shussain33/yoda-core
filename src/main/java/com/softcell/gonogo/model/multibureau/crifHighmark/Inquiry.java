package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INQUIRY")
@XmlAccessorType(XmlAccessType.FIELD)
public class Inquiry {

	@XmlElement(name="INQUIRY-UNIQUE-REF-NO")
	private String inquiryUniqueRefrenceNUmber;
	@XmlElement(name="REQUEST-DT-TM")
	private String requestDateTime;
	@XmlElement(name="RESPONSE-DT-TM")
	private String responseDateTime;
	@XmlElement(name="RESPONSE-TYPE")
	private String responseType;
	@XmlElement(name="DESCRIPTION")
	private String description;
	//@XmlElement(name="VARIATION")
	private String reportId;
	//@XmlElement(name="VARIATION")
	private Errors errorObj;
	//@XmlElement(name="VARIATION")
	private String mbrId;
	
	public String getInquiryUniqueRefrenceNUmber() {
		return inquiryUniqueRefrenceNUmber;
	}
	public void setInquiryUniqueRefrenceNUmber(String inquiryUniqueRefrenceNUmber) {
		this.inquiryUniqueRefrenceNUmber = inquiryUniqueRefrenceNUmber;
	}
	public String getRequestDateTime() {
		return requestDateTime;
	}
	public void setRequestDateTime(String requestDateTime) {
		this.requestDateTime = requestDateTime;
	}
	public String getResponseDateTime() {
		return responseDateTime;
	}
	public void setResponseDateTime(String responseDateTime) {
		this.responseDateTime = responseDateTime;
	}
	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public Errors getErrorObj() {
		return errorObj;
	}
	public void setErrorObj(Errors errorObj) {
		this.errorObj = errorObj;
	}
	public String getMbrId() {
		return mbrId;
	}
	public void setMbrId(String mbrId) {
		this.mbrId = mbrId;
	}
	@Override
	public String toString() {
		return "Inquiry [inquiryUniqueRefrenceNUmber="
				+ inquiryUniqueRefrenceNUmber + ", requestDateTime="
				+ requestDateTime + ", responseDateTime=" + responseDateTime
				+ ", responseType=" + responseType + ", description="
				+ description + ", reportId=" + reportId + "]";
	}
	

	
}
