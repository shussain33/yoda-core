package com.softcell.gonogo.model.multibureau.experian;

public class CurrentApplicantdetails {
	
	private String lastName;
	private String firstName;
	private String middleName1;
	private String middleName2;
	private String middleName3;
	private String genderCode;
	private String incometaxPan;
	private String panIssueDate;
	private String panExpirationDate;
	private String passportNumber;
	private String passportIssueDate;
	private String passportExpirationDate;
	private String votersIdentityCard;
	private String voterIdIssueDate;
	private String voterIdExpirationdate;
	private String driverLicenceNumber;
	private String driverLicenceIssueDate;
	private String driverLicenceExpirationdate;
	private String rationCardNumber;
	private String rationCardIssueDate;
	private String rationCardExpirationDate;
	private String universalIdNumber;
	private String universalIdIssueDate;
	private String universalIdExpirationDate;
	private String dateofBirthApplicant;
	private String telephoneNumberApplicant1st;
	private String telephoneType;
	private String telephoneExtension;
	private String mobilePhoneNumber;
	private String emailid;
	
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName1() {
		return middleName1;
	}
	public void setMiddleName1(String middleName1) {
		this.middleName1 = middleName1;
	}
	public String getMiddleName2() {
		return middleName2;
	}
	public void setMiddleName2(String middleName2) {
		this.middleName2 = middleName2;
	}
	public String getMiddleName3() {
		return middleName3;
	}
	public void setMiddleName3(String middleName3) {
		this.middleName3 = middleName3;
	}
	public String getGenderCode() {
		return genderCode;
	}
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}
	public String getIncometaxPan() {
		return incometaxPan;
	}
	public void setIncometaxPan(String incometaxPan) {
		this.incometaxPan = incometaxPan;
	}
	public String getPanIssueDate() {
		return panIssueDate;
	}
	public void setPanIssueDate(String panIssueDate) {
		this.panIssueDate = panIssueDate;
	}
	public String getPanExpirationDate() {
		return panExpirationDate;
	}
	public void setPanExpirationDate(String panExpirationDate) {
		this.panExpirationDate = panExpirationDate;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getPassportIssueDate() {
		return passportIssueDate;
	}
	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}
	public String getPassportExpirationDate() {
		return passportExpirationDate;
	}
	public void setPassportExpirationDate(String passportExpirationDate) {
		this.passportExpirationDate = passportExpirationDate;
	}
	public String getVotersIdentityCard() {
		return votersIdentityCard;
	}
	public void setVotersIdentityCard(String votersIdentityCard) {
		this.votersIdentityCard = votersIdentityCard;
	}
	public String getVoterIdIssueDate() {
		return voterIdIssueDate;
	}
	public void setVoterIdIssueDate(String voterIdIssueDate) {
		this.voterIdIssueDate = voterIdIssueDate;
	}
	public String getVoterIdExpirationdate() {
		return voterIdExpirationdate;
	}
	public void setVoterIdExpirationdate(String voterIdExpirationdate) {
		this.voterIdExpirationdate = voterIdExpirationdate;
	}
	public String getDriverLicenceNumber() {
		return driverLicenceNumber;
	}
	public void setDriverLicenceNumber(String driverLicenceNumber) {
		this.driverLicenceNumber = driverLicenceNumber;
	}
	public String getDriverLicenceIssueDate() {
		return driverLicenceIssueDate;
	}
	public void setDriverLicenceIssueDate(String driverLicenceIssueDate) {
		this.driverLicenceIssueDate = driverLicenceIssueDate;
	}
	public String getDriverLicenceExpirationdate() {
		return driverLicenceExpirationdate;
	}
	public void setDriverLicenceExpirationdate(String driverLicenceExpirationdate) {
		this.driverLicenceExpirationdate = driverLicenceExpirationdate;
	}
	public String getRationCardNumber() {
		return rationCardNumber;
	}
	public void setRationCardNumber(String rationCardNumber) {
		this.rationCardNumber = rationCardNumber;
	}
	public String getRationCardIssueDate() {
		return rationCardIssueDate;
	}
	public void setRationCardIssueDate(String rationCardIssueDate) {
		this.rationCardIssueDate = rationCardIssueDate;
	}
	public String getRationCardExpirationDate() {
		return rationCardExpirationDate;
	}
	public void setRationCardExpirationDate(String rationCardExpirationDate) {
		this.rationCardExpirationDate = rationCardExpirationDate;
	}
	public String getUniversalIdNumber() {
		return universalIdNumber;
	}
	public void setUniversalIdNumber(String universalIdNumber) {
		this.universalIdNumber = universalIdNumber;
	}
	public String getUniversalIdIssueDate() {
		return universalIdIssueDate;
	}
	public void setUniversalIdIssueDate(String universalIdIssueDate) {
		this.universalIdIssueDate = universalIdIssueDate;
	}
	public String getUniversalIdExpirationDate() {
		return universalIdExpirationDate;
	}
	public void setUniversalIdExpirationDate(String universalIdExpirationDate) {
		this.universalIdExpirationDate = universalIdExpirationDate;
	}
	public String getDateofBirthApplicant() {
		return dateofBirthApplicant;
	}
	public void setDateofBirthApplicant(String dateofBirthApplicant) {
		this.dateofBirthApplicant = dateofBirthApplicant;
	}
	public String getTelephoneNumberApplicant1st() {
		return telephoneNumberApplicant1st;
	}
	public void setTelephoneNumberApplicant1st(String telephoneNumberApplicant1st) {
		this.telephoneNumberApplicant1st = telephoneNumberApplicant1st;
	}
	public String getTelephoneType() {
		return telephoneType;
	}
	public void setTelephoneType(String telephoneType) {
		this.telephoneType = telephoneType;
	}
	
	public String getTelephoneExtension() {
		return telephoneExtension;
	}
	public void setTelephoneExtension(String telephoneExtension) {
		this.telephoneExtension = telephoneExtension;
	}
	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}
	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}
	public String getEmailid() {
		return emailid;
	}
	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CurrentApplicantdetails{");
		sb.append("lastName='").append(lastName).append('\'');
		sb.append(", firstName='").append(firstName).append('\'');
		sb.append(", middleName1='").append(middleName1).append('\'');
		sb.append(", middleName2='").append(middleName2).append('\'');
		sb.append(", middleName3='").append(middleName3).append('\'');
		sb.append(", genderCode='").append(genderCode).append('\'');
		sb.append(", incometaxPan='").append(incometaxPan).append('\'');
		sb.append(", panIssueDate='").append(panIssueDate).append('\'');
		sb.append(", panExpirationDate='").append(panExpirationDate).append('\'');
		sb.append(", passportNumber='").append(passportNumber).append('\'');
		sb.append(", passportIssueDate='").append(passportIssueDate).append('\'');
		sb.append(", passportExpirationDate='").append(passportExpirationDate).append('\'');
		sb.append(", votersIdentityCard='").append(votersIdentityCard).append('\'');
		sb.append(", voterIdIssueDate='").append(voterIdIssueDate).append('\'');
		sb.append(", voterIdExpirationdate='").append(voterIdExpirationdate).append('\'');
		sb.append(", driverLicenceNumber='").append(driverLicenceNumber).append('\'');
		sb.append(", driverLicenceIssueDate='").append(driverLicenceIssueDate).append('\'');
		sb.append(", driverLicenceExpirationdate='").append(driverLicenceExpirationdate).append('\'');
		sb.append(", rationCardNumber='").append(rationCardNumber).append('\'');
		sb.append(", rationCardIssueDate='").append(rationCardIssueDate).append('\'');
		sb.append(", rationCardExpirationDate='").append(rationCardExpirationDate).append('\'');
		sb.append(", universalIdNumber='").append(universalIdNumber).append('\'');
		sb.append(", universalIdIssueDate='").append(universalIdIssueDate).append('\'');
		sb.append(", universalIdExpirationDate='").append(universalIdExpirationDate).append('\'');
		sb.append(", dateofBirthApplicant='").append(dateofBirthApplicant).append('\'');
		sb.append(", telephoneNumberApplicant1st='").append(telephoneNumberApplicant1st).append('\'');
		sb.append(", telephoneType='").append(telephoneType).append('\'');
		sb.append(", telephoneExtension='").append(telephoneExtension).append('\'');
		sb.append(", mobilePhoneNumber='").append(mobilePhoneNumber).append('\'');
		sb.append(", emailid='").append(emailid).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CurrentApplicantdetails that = (CurrentApplicantdetails) o;

		if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
		if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
		if (middleName1 != null ? !middleName1.equals(that.middleName1) : that.middleName1 != null) return false;
		if (middleName2 != null ? !middleName2.equals(that.middleName2) : that.middleName2 != null) return false;
		if (middleName3 != null ? !middleName3.equals(that.middleName3) : that.middleName3 != null) return false;
		if (genderCode != null ? !genderCode.equals(that.genderCode) : that.genderCode != null) return false;
		if (incometaxPan != null ? !incometaxPan.equals(that.incometaxPan) : that.incometaxPan != null) return false;
		if (panIssueDate != null ? !panIssueDate.equals(that.panIssueDate) : that.panIssueDate != null) return false;
		if (panExpirationDate != null ? !panExpirationDate.equals(that.panExpirationDate) : that.panExpirationDate != null)
			return false;
		if (passportNumber != null ? !passportNumber.equals(that.passportNumber) : that.passportNumber != null)
			return false;
		if (passportIssueDate != null ? !passportIssueDate.equals(that.passportIssueDate) : that.passportIssueDate != null)
			return false;
		if (passportExpirationDate != null ? !passportExpirationDate.equals(that.passportExpirationDate) : that.passportExpirationDate != null)
			return false;
		if (votersIdentityCard != null ? !votersIdentityCard.equals(that.votersIdentityCard) : that.votersIdentityCard != null)
			return false;
		if (voterIdIssueDate != null ? !voterIdIssueDate.equals(that.voterIdIssueDate) : that.voterIdIssueDate != null)
			return false;
		if (voterIdExpirationdate != null ? !voterIdExpirationdate.equals(that.voterIdExpirationdate) : that.voterIdExpirationdate != null)
			return false;
		if (driverLicenceNumber != null ? !driverLicenceNumber.equals(that.driverLicenceNumber) : that.driverLicenceNumber != null)
			return false;
		if (driverLicenceIssueDate != null ? !driverLicenceIssueDate.equals(that.driverLicenceIssueDate) : that.driverLicenceIssueDate != null)
			return false;
		if (driverLicenceExpirationdate != null ? !driverLicenceExpirationdate.equals(that.driverLicenceExpirationdate) : that.driverLicenceExpirationdate != null)
			return false;
		if (rationCardNumber != null ? !rationCardNumber.equals(that.rationCardNumber) : that.rationCardNumber != null)
			return false;
		if (rationCardIssueDate != null ? !rationCardIssueDate.equals(that.rationCardIssueDate) : that.rationCardIssueDate != null)
			return false;
		if (rationCardExpirationDate != null ? !rationCardExpirationDate.equals(that.rationCardExpirationDate) : that.rationCardExpirationDate != null)
			return false;
		if (universalIdNumber != null ? !universalIdNumber.equals(that.universalIdNumber) : that.universalIdNumber != null)
			return false;
		if (universalIdIssueDate != null ? !universalIdIssueDate.equals(that.universalIdIssueDate) : that.universalIdIssueDate != null)
			return false;
		if (universalIdExpirationDate != null ? !universalIdExpirationDate.equals(that.universalIdExpirationDate) : that.universalIdExpirationDate != null)
			return false;
		if (dateofBirthApplicant != null ? !dateofBirthApplicant.equals(that.dateofBirthApplicant) : that.dateofBirthApplicant != null)
			return false;
		if (telephoneNumberApplicant1st != null ? !telephoneNumberApplicant1st.equals(that.telephoneNumberApplicant1st) : that.telephoneNumberApplicant1st != null)
			return false;
		if (telephoneType != null ? !telephoneType.equals(that.telephoneType) : that.telephoneType != null)
			return false;
		if (telephoneExtension != null ? !telephoneExtension.equals(that.telephoneExtension) : that.telephoneExtension != null)
			return false;
		if (mobilePhoneNumber != null ? !mobilePhoneNumber.equals(that.mobilePhoneNumber) : that.mobilePhoneNumber != null)
			return false;
		return emailid != null ? emailid.equals(that.emailid) : that.emailid == null;
	}

	@Override
	public int hashCode() {
		int result = lastName != null ? lastName.hashCode() : 0;
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (middleName1 != null ? middleName1.hashCode() : 0);
		result = 31 * result + (middleName2 != null ? middleName2.hashCode() : 0);
		result = 31 * result + (middleName3 != null ? middleName3.hashCode() : 0);
		result = 31 * result + (genderCode != null ? genderCode.hashCode() : 0);
		result = 31 * result + (incometaxPan != null ? incometaxPan.hashCode() : 0);
		result = 31 * result + (panIssueDate != null ? panIssueDate.hashCode() : 0);
		result = 31 * result + (panExpirationDate != null ? panExpirationDate.hashCode() : 0);
		result = 31 * result + (passportNumber != null ? passportNumber.hashCode() : 0);
		result = 31 * result + (passportIssueDate != null ? passportIssueDate.hashCode() : 0);
		result = 31 * result + (passportExpirationDate != null ? passportExpirationDate.hashCode() : 0);
		result = 31 * result + (votersIdentityCard != null ? votersIdentityCard.hashCode() : 0);
		result = 31 * result + (voterIdIssueDate != null ? voterIdIssueDate.hashCode() : 0);
		result = 31 * result + (voterIdExpirationdate != null ? voterIdExpirationdate.hashCode() : 0);
		result = 31 * result + (driverLicenceNumber != null ? driverLicenceNumber.hashCode() : 0);
		result = 31 * result + (driverLicenceIssueDate != null ? driverLicenceIssueDate.hashCode() : 0);
		result = 31 * result + (driverLicenceExpirationdate != null ? driverLicenceExpirationdate.hashCode() : 0);
		result = 31 * result + (rationCardNumber != null ? rationCardNumber.hashCode() : 0);
		result = 31 * result + (rationCardIssueDate != null ? rationCardIssueDate.hashCode() : 0);
		result = 31 * result + (rationCardExpirationDate != null ? rationCardExpirationDate.hashCode() : 0);
		result = 31 * result + (universalIdNumber != null ? universalIdNumber.hashCode() : 0);
		result = 31 * result + (universalIdIssueDate != null ? universalIdIssueDate.hashCode() : 0);
		result = 31 * result + (universalIdExpirationDate != null ? universalIdExpirationDate.hashCode() : 0);
		result = 31 * result + (dateofBirthApplicant != null ? dateofBirthApplicant.hashCode() : 0);
		result = 31 * result + (telephoneNumberApplicant1st != null ? telephoneNumberApplicant1st.hashCode() : 0);
		result = 31 * result + (telephoneType != null ? telephoneType.hashCode() : 0);
		result = 31 * result + (telephoneExtension != null ? telephoneExtension.hashCode() : 0);
		result = 31 * result + (mobilePhoneNumber != null ? mobilePhoneNumber.hashCode() : 0);
		result = 31 * result + (emailid != null ? emailid.hashCode() : 0);
		return result;
	}
}
