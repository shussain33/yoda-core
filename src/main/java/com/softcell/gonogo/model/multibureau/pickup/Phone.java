/**
 *
 */
package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author Dipak
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class Phone {

    @JsonProperty("01")
    private String phoneType;

    @JsonProperty("02")
    private String phoneNumber;

    @JsonProperty("03")
    private String phoneExtn;

    @JsonProperty("04")
    private String stdCode;

    @JsonProperty("ALTERNATE-PHONE 1")
    private String alternatePhoneNumber1;

    @JsonProperty("ALTERNATE-PHONE 2")
    private String alternatePhoneNumber2;


    public static Builder builder() {
        return new Builder();
    }


    public String getAlternatePhoneNumber1() {
        return alternatePhoneNumber1;
    }

    public void setAlternatePhoneNumber1(String alternatePhoneNumber1) {
        this.alternatePhoneNumber1 = alternatePhoneNumber1;
    }

    public String getAlternatePhoneNumber2() {
        return alternatePhoneNumber2;
    }

    public void setAlternatePhoneNumber2(String alternatePhoneNumber2) {
        this.alternatePhoneNumber2 = alternatePhoneNumber2;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneExtn() {
        return phoneExtn;
    }

    public void setPhoneExtn(String phoneExtn) {
        this.phoneExtn = phoneExtn;
    }

    public String getStdCode() {
        return stdCode;
    }

    public void setStdCode(String stdCode) {
        this.stdCode = stdCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Phone{");
        sb.append("phoneType='").append(phoneType).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", phoneExtn='").append(phoneExtn).append('\'');
        sb.append(", stdCode='").append(stdCode).append('\'');
        sb.append(", alternatePhoneNumber1='").append(alternatePhoneNumber1).append('\'');
        sb.append(", alternatePhoneNumber2='").append(alternatePhoneNumber2).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Phone)) return false;
        Phone phone = (Phone) o;
        return Objects.equal(getPhoneType(), phone.getPhoneType()) &&
                Objects.equal(getPhoneNumber(), phone.getPhoneNumber()) &&
                Objects.equal(getPhoneExtn(), phone.getPhoneExtn()) &&
                Objects.equal(getStdCode(), phone.getStdCode()) &&
                Objects.equal(getAlternatePhoneNumber1(), phone.getAlternatePhoneNumber1()) &&
                Objects.equal(getAlternatePhoneNumber2(), phone.getAlternatePhoneNumber2());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPhoneType(), getPhoneNumber(), getPhoneExtn(), getStdCode(), getAlternatePhoneNumber1(), getAlternatePhoneNumber2());
    }

    public static class Builder {
        private Phone phone = new Phone();

        public Phone build() {
            return this.phone;
        }


        public Builder phoneType(String phoneType) {
            this.phone.phoneType = phoneType;
            return this;
        }

        public Builder phoneNumber(String phoneNumber) {
            this.phone.phoneNumber = phoneNumber;
            return this;
        }

        public Builder phoneExtn(String phoneExtn) {
            this.phone.phoneExtn = phoneExtn;
            return this;
        }

        public Builder stdCode(String stdCode) {
            this.phone.stdCode = stdCode;
            return this;
        }

        public Builder alternatePhoneNumber1(String alternatePhoneNumber1) {
            this.phone.alternatePhoneNumber1 = alternatePhoneNumber1;
            return this;
        }

        public Builder alternatePhoneNumber2(String alternatePhoneNumber2) {
            this.phone.alternatePhoneNumber2 = alternatePhoneNumber2;
            return this;
        }

    }
}
