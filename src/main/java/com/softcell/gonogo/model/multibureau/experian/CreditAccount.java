package com.softcell.gonogo.model.multibureau.experian;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreditAccount {

	private String creditAccountTotal;
	private String creditAccountActive;
	private String creditAccountDefault;
	private String creditAccountClosed;
	@JsonProperty("CADSuitFiledCurrentBalance")
	private String CADSuitFiledCurrentBalance;
	
	/**
	 * @return the creditAccountTotal
	 */
	public String getCreditAccountTotal() {
		return creditAccountTotal;
	}
	/**
	 * @param creditAccountTotal the creditAccountTotal to set
	 */
	public void setCreditAccountTotal(String creditAccountTotal) {
		this.creditAccountTotal = creditAccountTotal;
	}
	/**
	 * @return the creditAccountActive
	 */
	public String getCreditAccountActive() {
		return creditAccountActive;
	}
	/**
	 * @param creditAccountActive the creditAccountActive to set
	 */
	public void setCreditAccountActive(String creditAccountActive) {
		this.creditAccountActive = creditAccountActive;
	}
	/**
	 * @return the creditAccountDefault
	 */
	public String getCreditAccountDefault() {
		return creditAccountDefault;
	}
	/**
	 * @param creditAccountDefault the creditAccountDefault to set
	 */
	public void setCreditAccountDefault(String creditAccountDefault) {
		this.creditAccountDefault = creditAccountDefault;
	}
	/**
	 * @return the creditAccountClosed
	 */
	public String getCreditAccountClosed() {
		return creditAccountClosed;
	}
	/**
	 * @param creditAccountClosed the creditAccountClosed to set
	 */
	public void setCreditAccountClosed(String creditAccountClosed) {
		this.creditAccountClosed = creditAccountClosed;
	}
	/**
	 * @return the cADSuitFiledCurrentBalance
	 */
	public String getCADSuitFiledCurrentBalance() {
		return CADSuitFiledCurrentBalance;
	}
	/**
	 * @param cADSuitFiledCurrentBalance the cADSuitFiledCurrentBalance to set
	 */
	public void setCADSuitFiledCurrentBalance(String cADSuitFiledCurrentBalance) {
		CADSuitFiledCurrentBalance = cADSuitFiledCurrentBalance;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CreditAccount{");
		sb.append("creditAccountTotal='").append(creditAccountTotal).append('\'');
		sb.append(", creditAccountActive='").append(creditAccountActive).append('\'');
		sb.append(", creditAccountDefault='").append(creditAccountDefault).append('\'');
		sb.append(", creditAccountClosed='").append(creditAccountClosed).append('\'');
		sb.append(", CADSuitFiledCurrentBalance='").append(CADSuitFiledCurrentBalance).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CreditAccount that = (CreditAccount) o;

		if (creditAccountTotal != null ? !creditAccountTotal.equals(that.creditAccountTotal) : that.creditAccountTotal != null)
			return false;
		if (creditAccountActive != null ? !creditAccountActive.equals(that.creditAccountActive) : that.creditAccountActive != null)
			return false;
		if (creditAccountDefault != null ? !creditAccountDefault.equals(that.creditAccountDefault) : that.creditAccountDefault != null)
			return false;
		if (creditAccountClosed != null ? !creditAccountClosed.equals(that.creditAccountClosed) : that.creditAccountClosed != null)
			return false;
		return CADSuitFiledCurrentBalance != null ? CADSuitFiledCurrentBalance.equals(that.CADSuitFiledCurrentBalance) : that.CADSuitFiledCurrentBalance == null;
	}

	@Override
	public int hashCode() {
		int result = creditAccountTotal != null ? creditAccountTotal.hashCode() : 0;
		result = 31 * result + (creditAccountActive != null ? creditAccountActive.hashCode() : 0);
		result = 31 * result + (creditAccountDefault != null ? creditAccountDefault.hashCode() : 0);
		result = 31 * result + (creditAccountClosed != null ? creditAccountClosed.hashCode() : 0);
		result = 31 * result + (CADSuitFiledCurrentBalance != null ? CADSuitFiledCurrentBalance.hashCode() : 0);
		return result;
	}
}
