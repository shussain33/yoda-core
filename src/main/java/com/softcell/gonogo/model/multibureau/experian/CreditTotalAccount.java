package com.softcell.gonogo.model.multibureau.experian;

public class CreditTotalAccount {
	
	private String creditAccountTotal;
	private String creditAccountActive;
	private String creditAccountDefault;
	private String creditAccountClosed;
	private String CADSuitFiledCurrentBalance;

	
	/**
	 * @return the creditAccountTotal
	 */
	public String getCreditAccountTotal() {
		return creditAccountTotal;
	}
	/**
	 * @param creditAccountTotal the creditAccountTotal to set
	 */
	public void setCreditAccountTotal(String creditAccountTotal) {
		this.creditAccountTotal = creditAccountTotal;
	}
	public String getCreditAccountActive() {
		return creditAccountActive;
	}
	public void setCreditAccountActive(String creditAccountActive) {
		this.creditAccountActive = creditAccountActive;
	}
	public String getCreditAccountDefault() {
		return creditAccountDefault;
	}
	public void setCreditAccountDefault(String creditAccountDefault) {
		this.creditAccountDefault = creditAccountDefault;
	}
	public String getCreditAccountClosed() {
		return creditAccountClosed;
	}
	public void setCreditAccountClosed(String creditAccountClosed) {
		this.creditAccountClosed = creditAccountClosed;
	}
	public String getCADSuitFiledCurrentBalance() {
		return CADSuitFiledCurrentBalance;
	}
	public void setCADSuitFiledCurrentBalance(String cADSuitFiledCurrentBalance) {
		CADSuitFiledCurrentBalance = cADSuitFiledCurrentBalance;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CreditTotalAccount{");
		sb.append("creditAccountTotal='").append(creditAccountTotal).append('\'');
		sb.append(", creditAccountActive='").append(creditAccountActive).append('\'');
		sb.append(", creditAccountDefault='").append(creditAccountDefault).append('\'');
		sb.append(", creditAccountClosed='").append(creditAccountClosed).append('\'');
		sb.append(", CADSuitFiledCurrentBalance='").append(CADSuitFiledCurrentBalance).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CreditTotalAccount that = (CreditTotalAccount) o;

		if (creditAccountTotal != null ? !creditAccountTotal.equals(that.creditAccountTotal) : that.creditAccountTotal != null)
			return false;
		if (creditAccountActive != null ? !creditAccountActive.equals(that.creditAccountActive) : that.creditAccountActive != null)
			return false;
		if (creditAccountDefault != null ? !creditAccountDefault.equals(that.creditAccountDefault) : that.creditAccountDefault != null)
			return false;
		if (creditAccountClosed != null ? !creditAccountClosed.equals(that.creditAccountClosed) : that.creditAccountClosed != null)
			return false;
		return CADSuitFiledCurrentBalance != null ? CADSuitFiledCurrentBalance.equals(that.CADSuitFiledCurrentBalance) : that.CADSuitFiledCurrentBalance == null;
	}

	@Override
	public int hashCode() {
		int result = creditAccountTotal != null ? creditAccountTotal.hashCode() : 0;
		result = 31 * result + (creditAccountActive != null ? creditAccountActive.hashCode() : 0);
		result = 31 * result + (creditAccountDefault != null ? creditAccountDefault.hashCode() : 0);
		result = 31 * result + (creditAccountClosed != null ? creditAccountClosed.hashCode() : 0);
		result = 31 * result + (CADSuitFiledCurrentBalance != null ? CADSuitFiledCurrentBalance.hashCode() : 0);
		return result;
	}
}
