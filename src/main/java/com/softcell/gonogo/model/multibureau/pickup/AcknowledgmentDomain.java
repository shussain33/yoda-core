/**
 *
 */
package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AcknowledgmentDomain {

    @JsonProperty("HEADER")
    private Header header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private Long acknowledgmentId;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("WARNINGS")
    private List<WarningAndError> warning;

    @JsonProperty("ERRORS")
    private List<WarningAndError> errors;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Long getAcknowledgmentId() {
        return acknowledgmentId;
    }

    public void setAcknowledgmentId(Long acknowledgmentId) {
        this.acknowledgmentId = acknowledgmentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<WarningAndError> getWarning() {
        return warning;
    }

    public void setWarning(List<WarningAndError> warning) {
        this.warning = warning;
    }

    public List<WarningAndError> getErrors() {
        return errors;
    }

    public void setErrors(List<WarningAndError> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "AcknowledgmentDomain [header=" + header
                + ", acknowledgmentId=" + acknowledgmentId + ", status="
                + status + ", warning=" + warning + ", errors=" + errors + "]";
    }
}
