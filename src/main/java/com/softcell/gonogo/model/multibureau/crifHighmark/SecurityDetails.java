package com.softcell.gonogo.model.multibureau.crifHighmark;

public class SecurityDetails {

	private String securityType ;
	private String ownerName ;
	private String securityValue ;
	private String dateOfValue ;
	private String securityCharge; 
	private String propertyAddress; 
	private String automobileType ;
	private String yearOfManufacture; 
	private String registrationNumber; 
	private String engineNumber ;
	private String chassisNumber ;
	
	public String getSecurityType() {
		return securityType;
	}
	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getSecurityValue() {
		return securityValue;
	}
	public void setSecurityValue(String securityValue) {
		this.securityValue = securityValue;
	}
	public String getDateOfValue() {
		return dateOfValue;
	}
	public void setDateOfValue(String dateOfValue) {
		this.dateOfValue = dateOfValue;
	}
	public String getSecurityCharge() {
		return securityCharge;
	}
	public void setSecurityCharge(String securityCharge) {
		this.securityCharge = securityCharge;
	}
	public String getPropertyAddress() {
		return propertyAddress;
	}
	public void setPropertyAddress(String propertyAddress) {
		this.propertyAddress = propertyAddress;
	}
	public String getAutomobileType() {
		return automobileType;
	}
	public void setAutomobileType(String automobileType) {
		this.automobileType = automobileType;
	}
	public String getYearOfManufacture() {
		return yearOfManufacture;
	}
	public void setYearOfManufacture(String yearOfManufacture) {
		this.yearOfManufacture = yearOfManufacture;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public String getEngineNumber() {
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}
	public String getChassisNumber() {
		return chassisNumber;
	}
	public void setChassisNumber(String chassisNumber) {
		this.chassisNumber = chassisNumber;
	}
	@Override
	public String toString() {
		return "SecurityDetails [securityType=" + securityType + ", ownerName="
				+ ownerName + ", securityValue=" + securityValue
				+ ", dateOfValue=" + dateOfValue + ", securityCharge="
				+ securityCharge + ", propertyAddress=" + propertyAddress
				+ ", automobileType=" + automobileType + ", yearOfManufacture="
				+ yearOfManufacture + ", registrationNumber="
				+ registrationNumber + ", engineNumber=" + engineNumber
				+ ", chassisNumber=" + chassisNumber + "]";
	}
	
	


}
