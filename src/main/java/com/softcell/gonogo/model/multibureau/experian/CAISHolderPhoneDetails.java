package com.softcell.gonogo.model.multibureau.experian;

public class CAISHolderPhoneDetails {

	private String telephoneNumber;
	private String telephoneType;
	private String telephoneExtension;
	private String mobileTelephoneNumber;
	private String faxNumber;
	private String emailId;

	
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getTelephoneType() {
		return telephoneType;
	}
	public void setTelephoneType(String telephoneType) {
		this.telephoneType = telephoneType;
	}
	public String getTelephoneExtension() {
		return telephoneExtension;
	}
	public void setTelephoneExtension(String telephoneExtension) {
		this.telephoneExtension = telephoneExtension;
	}
	public String getMobileTelephoneNumber() {
		return mobileTelephoneNumber;
	}
	public void setMobileTelephoneNumber(String mobileTelephoneNumber) {
		this.mobileTelephoneNumber = mobileTelephoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CAISHolderPhoneDetails{");
		sb.append("telephoneNumber='").append(telephoneNumber).append('\'');
		sb.append(", telephoneType='").append(telephoneType).append('\'');
		sb.append(", telephoneExtension='").append(telephoneExtension).append('\'');
		sb.append(", mobileTelephoneNumber='").append(mobileTelephoneNumber).append('\'');
		sb.append(", faxNumber='").append(faxNumber).append('\'');
		sb.append(", emailId='").append(emailId).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CAISHolderPhoneDetails that = (CAISHolderPhoneDetails) o;

		if (telephoneNumber != null ? !telephoneNumber.equals(that.telephoneNumber) : that.telephoneNumber != null)
			return false;
		if (telephoneType != null ? !telephoneType.equals(that.telephoneType) : that.telephoneType != null)
			return false;
		if (telephoneExtension != null ? !telephoneExtension.equals(that.telephoneExtension) : that.telephoneExtension != null)
			return false;
		if (mobileTelephoneNumber != null ? !mobileTelephoneNumber.equals(that.mobileTelephoneNumber) : that.mobileTelephoneNumber != null)
			return false;
		if (faxNumber != null ? !faxNumber.equals(that.faxNumber) : that.faxNumber != null) return false;
		return emailId != null ? emailId.equals(that.emailId) : that.emailId == null;
	}

	@Override
	public int hashCode() {
		int result = telephoneNumber != null ? telephoneNumber.hashCode() : 0;
		result = 31 * result + (telephoneType != null ? telephoneType.hashCode() : 0);
		result = 31 * result + (telephoneExtension != null ? telephoneExtension.hashCode() : 0);
		result = 31 * result + (mobileTelephoneNumber != null ? mobileTelephoneNumber.hashCode() : 0);
		result = 31 * result + (faxNumber != null ? faxNumber.hashCode() : 0);
		result = 31 * result + (emailId != null ? emailId.hashCode() : 0);
		return result;
	}
}
