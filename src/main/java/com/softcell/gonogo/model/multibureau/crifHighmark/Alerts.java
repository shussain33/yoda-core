package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ALERTS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Alerts {
	
	@XmlElement(name="ALERT")
	private Alert alert;

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	@Override
	public String toString() {
		return "Alerts [alert=" + alert + "]";
	}
}
