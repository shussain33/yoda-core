package com.softcell.gonogo.model.multibureau.commercialCibil.cibilCommresponse;

import javax.xml.bind.annotation.XmlRegistry;


 
@XmlRegistry
public class ObjectFactory {


    
    public ObjectFactory() {
    }
 
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Settled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecYourInstitutionSettled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Settled();
    }
 
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD61To90();
    }

   
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.Total createBaseResponseReportProductSecCreditProfileSummarySecTotal() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.Total();
    }

   
    public Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary createBaseResponseReportProductSecCreditRatingSummaryVecCreditRatingSummary() {
        return new Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary();
    }

    
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.GreaterThan180DPD();
    }

   
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD1To30();
    }

    
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionNBFCOthers() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers();
    }

    
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPrivateForeignBanksTotalOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalOutstanding();
    }

    
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.WrittenOff createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecYourInstitutionWrittenOff() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.WrittenOff();
    }

     
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorAddressContactDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityGuarantorDetailsVecCreditFacilityGuarantorDetailsGuarantorAddressContactDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorAddressContactDetails();
    }

     
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD0();
    }

    
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecOutsideInstitution() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution();
    }

     
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Dbt();
    }

     
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFunded() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec.CreditFacilitySecurityDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec.CreditFacilitySecurityDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilitySecurityDetailsVecCreditFacilitySecurityDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec.CreditFacilitySecurityDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal.NoOfEnquiries }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal.NoOfEnquiries createBaseResponseReportProductSecEnquirySummarySecEnquiryTotalNoOfEnquiries() {
        return new Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal.NoOfEnquiries();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Amount }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Amount createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityCurrentDetailsVecCreditFacilityCurrentDetailsAmount() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Amount();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPrivateForeignBanksDelinquentCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.SuitFiledVec }
     * 
     */
    public Base.ResponseReport.ProductSec.SuitFiledVec createBaseResponseReportProductSecSuitFiledVec() {
        return new Base.ResponseReport.ProductSec.SuitFiledVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitution() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Invoked }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Invoked createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecYourInstitutionInvoked() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Invoked();
    }

    /**
     * Create an instance of {@link Base }
     * 
     */
    public Base createBase() {
        return new Base();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForex() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapital() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentOutstanding createBaseResponseReportProductSecCreditProfileSummarySecYourInstitutionDelinquentOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPublicSectorBanksDelinquentCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorBorrwerInfoBorrwerDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalCF createBaseResponseReportProductSecCreditProfileSummarySecYourInstitutionTotalCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOutsideTotal() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPrivateForeignBanks() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.YourInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.YourInstitution createBaseResponseReportProductSecBorrowerProfileSecBorrowerDelinquencyReportedOnBorrowerYourInstitution() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.YourInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditGrantors }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditGrantors createBaseResponseReportProductSecCreditFacilitiesSummaryCountOfCreditFacilitiesNoOfCreditGrantors() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditGrantors();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec createBaseResponseReportProductSecRelationshipDetailsVecRelationshipDetailsBorrwerIDDetailsVec() {
        return new Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityCurrentDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorCreditFacilityCurrentDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails createBaseResponseReportProductSecRelationshipDetailsVecRelationshipDetails() {
        return new Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCFHistoryforACOrDPDupto24MonthsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.EnquiryInformationRec }
     * 
     */
    public Base.ResponseReport.EnquiryInformationRec createBaseResponseReportEnquiryInformationRec() {
        return new Base.ResponseReport.EnquiryInformationRec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.OverdueCF }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.OverdueCF createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerTotalOverdueCF() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.OverdueCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrower() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPublicSectorBanks() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport }
     * 
     */
    public Base.ResponseReport createBaseResponseReport() {
        return new Base.ResponseReport();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec.CreditFacilitiesDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec.CreditFacilitiesDetails createBaseResponseReportProductSecCreditFacilitiesDetailsVecCreditFacilitiesDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec.CreditFacilitiesDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails.ClassOfActivityVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails.ClassOfActivityVec createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorBorrwerInfoBorrwerDetailsClassOfActivityVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails.ClassOfActivityVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityGuarantorDetailsVecCreditFacilityGuarantorDetailsGuarantorDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec }
     * 
     */
    public Base.ResponseReport.ProductSec createBaseResponseReportProductSec() {
        return new Base.ResponseReport.ProductSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.SuitFilled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.SuitFilled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerTotalSuitFilled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.SuitFilled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Dates }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Dates createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorCreditFacilityCurrentDetailsDates() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Dates();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower createBaseResponseReportProductSecBorrowerProfileSecBorrowerDelinquencyReportedOnBorrower() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails.ClassOfActivityVec }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails.ClassOfActivityVec createBaseResponseReportProductSecBorrowerProfileSecBorrwerDetailsClassOfActivityVec() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails.ClassOfActivityVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec createBaseResponseReportProductSecBorrowerProfileSecBorrwerIDDetailsVec() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal createBaseResponseReportProductSecEnquirySummarySecEnquiryTotal() {
        return new Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.WrittenOff }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.WrittenOff createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecOutsideInstitutionWrittenOff() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.WrittenOff();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.OtherDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.OtherDetails createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorCreditFacilityCurrentDetailsOtherDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.OtherDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPrivateForeignBanksDelinquentOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditRatingSummaryVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditRatingSummaryVec createBaseResponseReportProductSecCreditRatingSummaryVec() {
        return new Base.ResponseReport.ProductSec.CreditRatingSummaryVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.WrittenOff }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.WrittenOff createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerYourInstitutionWrittenOff() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.WrittenOff();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RankSec }
     * 
     */
    public Base.ResponseReport.ProductSec.RankSec createBaseResponseReportProductSecRankSec() {
        return new Base.ResponseReport.ProductSec.RankSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution createBaseResponseReportProductSecCreditProfileSummarySecYourInstitution() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RelationshipDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.RelationshipDetailsVec createBaseResponseReportProductSecRelationshipDetailsVec() {
        return new Base.ResponseReport.ProductSec.RelationshipDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.LocationDetailsSec }
     * 
     */
    public Base.ResponseReport.ProductSec.LocationDetailsSec createBaseResponseReportProductSecLocationDetailsSec() {
        return new Base.ResponseReport.ProductSec.LocationDetailsSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFunded() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.SuitFilled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.SuitFilled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecOutsideInstitutionSuitFilled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.SuitFilled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityGuarantorDetailsVecCreditFacilityGuarantorDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquirySummarySec }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquirySummarySec createBaseResponseReportProductSecEnquirySummarySec() {
        return new Base.ResponseReport.ProductSec.EnquirySummarySec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution.NoOfEnquiries }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution.NoOfEnquiries createBaseResponseReportProductSecEnquirySummarySecEnquiryYourInstitutionNoOfEnquiries() {
        return new Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution.NoOfEnquiries();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Invoked }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Invoked createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerTotalInvoked() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Invoked();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution createBaseResponseReportProductSecEnquirySummarySecEnquiryOutsideInstitution() {
        return new Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityOverdueDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPublicSectorBanksTotalCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec }
     * 
     */
    public Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec createBaseResponseReportProductSecLocationDetailsSecLocationInformationVec() {
        return new Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec createBaseResponseReportProductSecCreditFacilitiesSummarySummaryOfCreditFacilitiesVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.SuitFilled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.SuitFilled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerYourInstitutionSuitFilled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.SuitFilled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.OverdueCF }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.OverdueCF createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecYourInstitutionOverdueCF() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.OverdueCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalOutstanding createBaseResponseReportProductSecCreditProfileSummarySecYourInstitutionTotalOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoan() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerTotal() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecTotal() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec createBaseResponseReportProductSecCreditProfileSummarySec() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.OverdueCF }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.OverdueCF createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecOutsideInstitutionOverdueCF() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.OverdueCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Settled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Settled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerOutsideInstitutionSettled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Settled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.ChequeDishounouredDuetoInsufficientFunds }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.ChequeDishounouredDuetoInsufficientFunds createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecChequeDishounouredDuetoInsufficientFunds() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.ChequeDishounouredDuetoInsufficientFunds();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Dates }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Dates createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityCurrentDetailsVecCreditFacilityCurrentDetailsDates() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Dates();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec.LocationInformation }
     * 
     */
    public Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec.LocationInformation createBaseResponseReportProductSecLocationDetailsSecLocationInformationVecLocationInformation() {
        return new Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec.LocationInformation();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Invoked }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Invoked createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerYourInstitutionInvoked() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Invoked();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.EnquiryInformationRec.AddressVec }
     * 
     */
    public Base.ResponseReport.EnquiryInformationRec.AddressVec createBaseResponseReportEnquiryInformationRecAddressVec() {
        return new Base.ResponseReport.EnquiryInformationRec.AddressVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Amount }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Amount createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorCreditFacilityCurrentDetailsAmount() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Amount();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec.EnquiryDetailsInLast24Month }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec.EnquiryDetailsInLast24Month createBaseResponseReportProductSecEnquiryDetailsInLast24MonthVecEnquiryDetailsInLast24Month() {
        return new Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec.EnquiryDetailsInLast24Month();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPublicSectorBanksTotalOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.OverdueCF }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.OverdueCF createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecTotalOverdueCF() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.OverdueCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails createBaseResponseReportProductSecBorrowerProfileSecBorrwerDetails() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.RelationshipInformation }
     * 
     */
    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.RelationshipInformation createBaseResponseReportProductSecRelationshipDetailsVecRelationshipDetailsRelationshipInformation() {
        return new Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.RelationshipInformation();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionNBFCOthersTotalOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorBorrwerInfo() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution createBaseResponseReportProductSecEnquirySummarySecEnquiryYourInstitution() {
        return new Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorBorrwerInfoBorrwerAddressContactDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOutsideTotalDelinquentCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec createBaseResponseReportProductSecBorrowerProfileSec() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecYourInstitution() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityGuarantorDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.SuitFiledVec.SuitFilled }
     * 
     */
    public Base.ResponseReport.ProductSec.SuitFiledVec.SuitFilled createBaseResponseReportProductSecSuitFiledVecSuitFilled() {
        return new Base.ResponseReport.ProductSec.SuitFiledVec.SuitFilled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Settled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Settled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerTotalSettled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Settled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec.SummaryOfCreditFacilitiesRec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec.SummaryOfCreditFacilitiesRec createBaseResponseReportProductSecCreditFacilitiesSummarySummaryOfCreditFacilitiesVecSummaryOfCreditFacilitiesRec() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec.SummaryOfCreditFacilitiesRec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.DPD91To180 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.DPD91To180 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexNONSTDVecDPD91To180() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.DPD91To180();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months createBaseResponseReportProductSecBorrowerProfileSecBorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVecBorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Settled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Settled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecOutsideInstitutionSettled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Settled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.OutsideInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.OutsideInstitution createBaseResponseReportProductSecBorrowerProfileSecBorrowerDelinquencyReportedOnBorrowerOutsideInstitution() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.OutsideInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.SuitFilled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.SuitFilled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecYourInstitutionSuitFilled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.SuitFilled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOutsideTotalTotalCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOutsideTotalDelinquentOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.OtherDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.OtherDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityCurrentDetailsVecCreditFacilityCurrentDetailsOtherDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.OtherDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.OverdueCF }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.OverdueCF createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerOutsideInstitutionOverdueCF() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.OverdueCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionWorkingCapitalSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec.BorrwerIDDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec.BorrwerIDDetails createBaseResponseReportProductSecBorrowerProfileSecBorrwerIDDetailsVecBorrwerIDDetails() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec.BorrwerIDDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Invoked }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Invoked createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecOutsideInstitutionInvoked() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Invoked();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec.BorrwerIDDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec.BorrwerIDDetails createBaseResponseReportProductSecRelationshipDetailsVecRelationshipDetailsBorrwerIDDetailsVecBorrwerIDDetails() {
        return new Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec.BorrwerIDDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerOutsideInstitution() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionNBFCOthersDelinquentCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitution() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityGuarantorDetailsVecCreditFacilityGuarantorDetailsGuarantorDetailsBorrwerIDDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec.GuarantorIDDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec.GuarantorIDDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityGuarantorDetailsVecCreditFacilityGuarantorDetailsGuarantorDetailsBorrwerIDDetailsVecGuarantorIDDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec.GuarantorIDDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.InsideTotal createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.InsideTotal();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForex() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerAddressContactDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerAddressContactDetails createBaseResponseReportProductSecBorrowerProfileSecBorrwerAddressContactDetails() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerAddressContactDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ReportHeaderRec }
     * 
     */
    public Base.ResponseReport.ReportHeaderRec createBaseResponseReportReportHeaderRec() {
        return new Base.ResponseReport.ReportHeaderRec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditFacilities }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditFacilities createBaseResponseReportProductSecCreditFacilitiesSummaryCountOfCreditFacilitiesNoOfCreditFacilities() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditFacilities();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD1To30();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.WrittenOff }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.WrittenOff createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerTotalWrittenOff() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.WrittenOff();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionNBFCOthersTotalCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Settled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Settled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecTotalSettled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Settled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Invoked }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Invoked createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecTotalInvoked() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Invoked();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec createBaseResponseReportProductSecEnquiryDetailsInLast24MonthVec() {
        return new Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTotalSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities createBaseResponseReportProductSecCreditFacilitiesSummaryCountOfCreditFacilities() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ReportIssuesVec }
     * 
     */
    public Base.ResponseReport.ReportIssuesVec createBaseResponseReportReportIssuesVec() {
        return new Base.ResponseReport.ReportIssuesVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentCF createBaseResponseReportProductSecCreditProfileSummarySecYourInstitutionDelinquentCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec.BorrwerAddressContactDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec.BorrwerAddressContactDetails createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorBorrwerInfoBorrwerAddressContactDetailsVecBorrwerAddressContactDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec.BorrwerAddressContactDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCFHistoryforACOrDPDupto24MonthsVecCFHistoryforACOrDPDupto24Months() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.SuitFilled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.SuitFilled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecTotalSuitFilled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.SuitFilled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionForexSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionNBFCOthersDelinquentOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPublicSectorBanksDelinquentOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityCurrentDetailsVecCreditFacilityCurrentDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerAddressContactDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerAddressContactDetails createBaseResponseReportProductSecRelationshipDetailsVecRelationshipDetailsBorrwerAddressContactDetails() {
        return new Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerAddressContactDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.OverdueCF }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.OverdueCF createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerYourInstitutionOverdueCF() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.OverdueCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.SuitFilled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.SuitFilled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerOutsideInstitutionSuitFilled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.SuitFilled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilitySecurityDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec.CreditFacilityOverdueDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec.CreditFacilityOverdueDetails createBaseResponseReportProductSecCreditFacilityDetailsasBorrowerSecVecCreditFacilityDetailsasBorrowerSecCreditFacilityOverdueDetailsVecCreditFacilityOverdueDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec.CreditFacilityOverdueDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD61To90 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD61To90 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanSTDVecDPD61To90() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD61To90();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesSummary }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary createBaseResponseReportProductSecCreditFacilitiesSummary() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesSummary();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary.CreditRatingSummaryDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary.CreditRatingSummaryDetailsVec createBaseResponseReportProductSecCreditRatingSummaryVecCreditRatingSummaryCreditRatingSummaryDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary.CreditRatingSummaryDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec createBaseResponseReportProductSecBorrowerProfileSecBorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec() {
        return new Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.EnquiryInformationRec.AddressVec.Address }
     * 
     */
    public Base.ResponseReport.EnquiryInformationRec.AddressVec.Address createBaseResponseReportEnquiryInformationRecAddressVecAddress() {
        return new Base.ResponseReport.EnquiryInformationRec.AddressVec.Address();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec createBaseResponseReportProductSecDerogatoryInformationSec() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Loss }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Loss createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalNONSTDVecLoss() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Loss();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionNonFundedNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Dbt }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Dbt createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanNONSTDVecDbt() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Dbt();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorBorrwerInfoBorrwerIDDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Sub }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Sub createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedNONSTDVecSub() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Sub();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationReportedOnGuarantedPartiesVec }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationReportedOnGuarantedPartiesVec createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationReportedOnGuarantedPartiesVec() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationReportedOnGuarantedPartiesVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec createBaseResponseReportProductSecCreditFacilitiesDetailsVec() {
        return new Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Invoked }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Invoked createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerOutsideInstitutionInvoked() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Invoked();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalOutstanding }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalOutstanding createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOutsideTotalTotalOutstanding() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalOutstanding();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionTermLoanTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitution() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec.BorrwerIDDetails }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec.BorrwerIDDetails createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantorBorrwerInfoBorrwerIDDetailsVecBorrwerIDDetails() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec.BorrwerIDDetails();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerYourInstitution() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Settled }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Settled createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerYourInstitutionSettled() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Settled();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD0 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD0 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoanSTDVecDPD0() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD0();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.GreaterThan180DPD }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.GreaterThan180DPD createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalNONSTDVecGreaterThan180DPD() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.GreaterThan180DPD();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution.NoOfEnquiries }
     * 
     */
    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution.NoOfEnquiries createBaseResponseReportProductSecEnquirySummarySecEnquiryOutsideInstitutionNoOfEnquiries() {
        return new Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution.NoOfEnquiries();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapital() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTermLoan() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.WrittenOff }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.WrittenOff createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSecTotalWrittenOff() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.WrittenOff();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalNONSTDVec() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.InsideTotal createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecYourInstitutionTotalTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.InsideTotal();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalCF }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalCF createBaseResponseReportProductSecCreditProfileSummarySecOutsideInstitutionOtherPrivateForeignBanksTotalCF() {
        return new Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalCF();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD31To60 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD31To60 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionWorkingCapitalSTDVecDPD31To60() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD31To60();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.RankSec.RankVec }
     * 
     */
    public Base.ResponseReport.ProductSec.RankSec.RankVec createBaseResponseReportProductSecRankSecRankVec() {
        return new Base.ResponseReport.ProductSec.RankSec.RankVec();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.WrittenOff }
     * 
     */
    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.WrittenOff createBaseResponseReportProductSecDerogatoryInformationSecDerogatoryInformationBorrowerOutsideInstitutionWrittenOff() {
        return new Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.WrittenOff();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor }
     * 
     */
    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor createBaseResponseReportProductSecCreditFacilityDetailsasGuarantorVecCreditFacilityDetailsasGuarantor() {
        return new Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.Total }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.Total createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionForexTotal() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.Total();
    }

    /**
     * Create an instance of {@link Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD1To30 }
     * 
     */
    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD1To30 createBaseResponseReportProductSecOustandingBalanceByCFAndAssetClasificationSecOutsideInstitutionNonFundedSTDVecDPD1To30() {
        return new Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD1To30();
    }

}
