package com.softcell.gonogo.model.multibureau.crifHighmark;

public class HeaderSegment {

	private String productType;
	private String productVersion ;
	private String requestMbr ;
	private String subMbrId ;
	private String inquiryDateTime ;
	private String requestVolumeType;
	private String requestActionType;
	private String testFlag;
	private String userId;
	private String password;
	private String authFlag;
	private String authTitle;
	private String responseFormat;
	private String memberPreOverride;
	private String responseFormatOverride;
	private String losName;
	private String losVendor;
	private String losVersion;

	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductVersion() {
		return productVersion;
	}
	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}
	public String getRequestMbr() {
		return requestMbr;
	}
	public void setRequestMbr(String requestMbr) {
		this.requestMbr = requestMbr;
	}
	public String getSubMbrId() {
		return subMbrId;
	}
	public void setSubMbrId(String subMbrId) {
		this.subMbrId = subMbrId;
	}
	public String getInquiryDateTime() {
		return inquiryDateTime;
	}
	public void setInquiryDateTime(String inquiryDateTime) {
		this.inquiryDateTime = inquiryDateTime;
	}
	public String getRequestVolumeType() {
		return requestVolumeType;
	}
	public void setRequestVolumeType(String requestVolumeType) {
		this.requestVolumeType = requestVolumeType;
	}
	public String getRequestActionType() {
		return requestActionType;
	}
	public void setRequestActionType(String requestActionType) {
		this.requestActionType = requestActionType;
	}
	public String getTestFlag() {
		return testFlag;
	}
	public void setTestFlag(String testFlag) {
		this.testFlag = testFlag;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAuthFlag() {
		return authFlag;
	}
	public void setAuthFlag(String authFlag) {
		this.authFlag = authFlag;
	}
	public String getAuthTitle() {
		return authTitle;
	}
	public void setAuthTitle(String authTitle) {
		this.authTitle = authTitle;
	}
	public String getResponseFormat() {
		return responseFormat;
	}
	public void setResponseFormat(String responseFormat) {
		this.responseFormat = responseFormat;
	}
	public String getMemberPreOverride() {
		return memberPreOverride;
	}
	public void setMemberPreOverride(String memberPreOverride) {
		this.memberPreOverride = memberPreOverride;
	}
	public String getResponseFormatOverride() {
		return responseFormatOverride;
	}
	public void setResponseFormatOverride(String responseFormatOverride) {
		this.responseFormatOverride = responseFormatOverride;
	}
	public String getLosName() {
		return losName;
	}
	public void setLosName(String losName) {
		this.losName = losName;
	}
	public String getLosVendor() {
		return losVendor;
	}
	public void setLosVendor(String losVendor) {
		this.losVendor = losVendor;
	}
	public String getLosVersion() {
		return losVersion;
	}
	public void setLosVersion(String losVersion) {
		this.losVersion = losVersion;
	}
	@Override
	public String toString() {
		return "HeaderSegment [productType=" + productType
				+ ", productVersion=" + productVersion + ", requestMbr="
				+ requestMbr + ", subMbrId=" + subMbrId + ", inquiryDateTime="
				+ inquiryDateTime + ", requestVolumeType=" + requestVolumeType
				+ ", requestActionType=" + requestActionType + ", testFlag="
				+ testFlag + ", userId=" + userId + ", password=" + password
				+ ", authFlag=" + authFlag + ", authTitle=" + authTitle
				+ ", responseFormat=" + responseFormat + ", memberPreOverride="
				+ memberPreOverride + ", responseFormatOverride="
				+ responseFormatOverride + ", losName=" + losName
				+ ", losVendor=" + losVendor + ", losVersion=" + losVersion
				+ "]";
	}
	
	
}
