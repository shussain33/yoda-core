package com.softcell.gonogo.model.multibureau.crifHighmark;

public class HM_INDV_RESPONSE_CONST {

	public  interface ROOT{
		public static final  String REPORT_FILE = "REPORT-FILE";
		public static final  String INDV_REPORT_FILE = "INDV-REPORT-FILE";
	}
		
	public  interface INDV_REPORT_FILE{
		public static final String INQUIRY_STATUS  = "INQUIRY-STATUS";
		public static final String INDV_REPORTS   = "INDV-REPORTS";
	}
	
	
	public interface INQUIRY_STATUS{
		public static final String INQUIRY = "INQUIRY"; 
	}
	
	public interface INQUIRY{
		public static final String INQUIRY_UNIQUE_REF_NO  = "INQUIRY-UNIQUE-REF-NO";
		public static final String REQUEST_DT_TM      = "REQUEST-DT-TM";  
		public static final String RESPONSE_DT_TM     = "RESPONSE-DT-TM";
		public static final String RESPONSE_TYPE       = "RESPONSE-TYPE";
		public static final String DESCRIPTION        = "DESCRIPTION";
		public static final String REPORT_ID        = "REPORT-ID";
		public static final String MBR_ID        = "MBR-ID";
		public static final String ERRORS        = "ERRORS";
	}
	
	public interface ERRORS{
		public static final String ERROR        = "ERROR";
	}
	
	public interface ERROR{
		public static final String CODE        = "CODE";
		public static final String DESCRIPTION = "DESCRIPTION";
		public static final String ERROR_DESCRIPTION="ERROR-DESCRIPTION";
	}
	
	public interface BASE_FILE_HEADER{
		public static final String REPORT_TYPE        = "REPORT-TYPE";   
		public static final String REPORT_DATE        = "REPORT-DATE";
		public static final String FILE_NAME          = "FILE-NAME";
		public static final String INQ_CNT_FILE       = "INQ-CNT-FILE";
		public static final String RESPONSE_CNT_FILE  = "RESPONSE-CNT-FILE";
	}
	
	
	
	public interface INDV_REPORTS{
		public static final String  INDV_REPORT = "INDV-REPORT";
	}
	
	public interface INDV_REPORT{
		public static final String  HEADER                    = "HEADER";
		public static final String  REQUEST                   = "REQUEST";
		public static final String  ACCOUNTS_SUMMARY          = "ACCOUNTS-SUMMARY";
		public static final String  PERSONAL_INFO_VARIATION   = "PERSONAL-INFO-VARIATION";
		public static final String  RESPONSES                 = "RESPONSES";
		public static final String  SECONDARY_MATCHES         = "SECONDARY-MATCHES";
		public static final String  SCORES                     = "SCORES";
		public static final String  INQUIRY_HISTORY           = "INQUIRY-HISTORY";
		public static final String  COMMENTS                  = "COMMENTS";
		public static final String  ALERTS                    = "ALERTS";
		public static final String  PRINTABLE_REPORT          = "PRINTABLE-REPORT";
		public static final String INDV_RESPONSES ="INDV-RESPONSES";
		public static final String PHONES="PHONES";
		public static final String STATUS_DETAILS="STATUS-DETAILS";
		public static final String ADDRESSES="ADDRESSES";
		public static final String LOAN_DETAILS="LOAN-DETAILS";
		public static final String RELATIONS="RELATIONS";
		public static final String LINKED_ACCOUNTS="LINKED-ACCOUNTS";
		public static final String SECURITY_DETAILS="SECURITY-DETAILS";
		public static final String IDS="IDS";
		public static final String EMAILS="EMAILS";
		//public static final String GROUP_RESPONSES = "GRP-RESPONSES";
		public static final String GRP_RESPONSES = "GRP-RESPONSES";
	}
	public interface GROUP_RESPONSES{
		public static final String SUMMARY="SUMMARY";
		public static final String PRIMARY_SUMMARY="PRIMARY-SUMMARY";
		public static final String SECONDARY_SUMMARY="SECONDARY-SUMMARY";
		public static final String GRP_RESPONSE_LIST="GRP-RESPONSE-LIST";
	}
	public interface GRP_RESPONSE_LIST{
		public static final String REPORT_DT="reportDt";
		public static final String RECENT_DELINQ_DT="RECENT-DELINQ-DT";
		public static final String LOAN_CYCLE_ID="LOAN-CYCLE-ID";
		public static final String GRP_RESPONSE="GRP-RESPONSE";
		public static final String MFI="MFI";
		public static final String MFI_ID="MFI-ID";
		public static final String BRANCH="BRANCH";
		public static final String KENDRA="KENDRA";
		public static final String NAME="NAME";
		public static final String DOB="DOB";
		public static final String TYPE="TYPE";
		public static final String VALUE="VALUE";
		public static final String CNSMRMBRID="CNSMRMBRID";
		public static final String MATCHED_TYPE="MATCHED-TYPE";
		public static final String INSERT_DATE="INSERT-DATE";
		public static final String GROUP_DETAILS="GROUP-DETAILS";
		public static final String GROUP_ID="GROUP-ID";
		public static final String TOT_DISBURSED_AMT="TOT-DISBURSED-AMT";
		public static final String TOT_CURRENT_BAL="TOT-CURRENT-BAL";
		public static final String TOT_ACCOUNTS="TOT-ACCOUNTS";
		public static final String TOT_DPD_30="TOT-DPD-30";
		public static final String TOT_DPD_60="TOT-DPD-60";
		public static final String TOT_DPD_90="TOT-DPD-90";
		public static final String LOAN_DETAILS="LOAN-DETAILS";
		public static final String LOAN_DETAIL="LOAN-DETAIL";
		public static final String ACCT_TYPE="ACCT-TYPE";
		public static final String FREQ="FREQ";
		public static final String STATUS="STATUS";
		public static final String ACCT_NUMBER="ACCT-NUMBER";
		public static final String DISBURSED_AMT="DISBURSED-AMT";
		public static final String CURRENT_BAL="CURRENT-BAL";
		public static final String INSTALLMENT_AMT="INSTALLMENT-AMT";
		public static final String OVERDUE_AMT="OVERDUE-AMT";
		public static final String WRITE_OFF_AMT="WRITE-OFF-AMT";
		public static final String DISBURSED_DT="DISBURSED-DT";
		public static final String CLOSED_DT="CLOSED-DT";
		public static final String INQ_CNT="INQ-CNT";
		public static final String DPD="DPD";
		public static final String INFO_AS_ON="INFO-AS-ON";
		public static final String COMBINED_PAYMENT_HISTORY="COMBINED-PAYMENT-HISTORY";
		public static final String AGE = "AGE";
		public static final String AGE_AS_ON = "AGE-AS-ON";
		public static final String RELATIONS = "RELATIONS";
		public static final String PHONES = "PHONES";
		public static final String ADDRESSES = "ADDRESSES";
		public static final String IDS = "IDS";
		public static final String EMAILS = "EMAILS";
		public static final String ENTITY_ID = "ENTITY-ID";
		public static final String GROUP_CREATION_DATE = "GROUP-CREATION-DATE";
		public static final String TOT_INSTALLMENT_AMT = "TOT-INSTALLMENT-AMT";
		public static final String TOT_OVERDUE_AMT = "TOT-OVERDUE-AMT";
		public static final String TOT_MEMBER = "TOT-MEMBER";
		public static final String TOT_DELINQ_MBR = "TOT-DELINQ-MBR";
		public static final String TOT_WRITE_OFFS = "TOT-WRITE-OFFS";
		public static final String WRITE_OFF_DT = "WRITE-OFF-DT";
		public static final String WORST_DELEQUENCY_AMOUNT = "WORST-DELEQUENCY-AMOUNT";
		public static final String ACTIVE_BORROWERS = "ACTIVE-BORROWERS";
		public static final String NO_OF_BORROWERS = "NO-OF-BORROWERS";
		public static final String COMMENT = "COMMENT";
		public static final String PAYMENT_HISTORY = "PAYMENT-HISTORY";
	}
	public interface STATUS_DETAILS{
		public static final String STATUS="STATUS";
	}
	public interface STATUS{
		public static final String OPTION="OPTION";
		public static final String OPTION_STATUS="OPTION-STATUS";
		public static final String ERRORS="ERRORS";
	}
	public interface EMAILS{
		public static final String EMAIL="EMAIL";
	}
	public interface RELATIONS{
		public static final String RELATION="RELATION";
	}
	public interface RELATION{
		public static final String NAME="NAME";
		public static final String TYPE="TYPE";
	}
	public interface ADDRESSES{
		public static final String ADDRESS="ADDRESS";
	}
	public interface PHONES{
		public static final String PHONE="PHONE";
	}
	public interface IDS{
		public static final String ID="ID";
	}
	
	public interface HEADER{
		public static final String DATE_OF_REQUEST     = "DATE-OF-REQUEST";
		public static final String PREPARED_FOR        = "PREPARED-FOR";
		public static final String PREPARED_FOR_ID     = "PREPARED-FOR-ID";
		public static final String DATE_OF_ISSUE       = "DATE-OF-ISSUE";
		public static final String REPORT_ID           = "REPORT-ID";
		public static final String BATCH_ID            = "BATCH-ID";
		public static final String STATUS              = "STATUS";
		public static final String ERROR ="ERROR";
	}
	
	public interface REQUEST{
		public static final String NAME                     = "NAME";
		public static final String AKA                      = "AKA";
		public static final String SPOUSE                   = "SPOUSE";
		public static final String FATHER                   = "FATHER";
		public static final String MOTHER                   = "MOTHER";
		public static final String DOB                      = "DOB";
		public static final String AGE                      = "AGE";
		public static final String AGE_AS_ON                = "AGE-AS-ON";
		public static final String RATION_CARD              = "RATION-CARD";
		public static final String PASSPORT 	            = "PASSPORT";
		public static final String VOTERS_ID                = "VOTERS-ID";
		public static final String DRIVING_LICENCE_NO       = "DRIVING-LICENCE-NO";
		public static final String PAN                      = "PAN";
		public static final String GENDER                   = "GENDER";
		public static final String OWNERSHIP                = "OWNERSHIP";
		public static final String ADDRESSES                = "ADDRESSES";
		public static final String ADDRESS_1                = "ADDRESS-1";
		public static final String ADDRESS_2                = "ADDRESS-2";
		public static final String ADDRESS_3                = "ADDRESS-3";
		public static final String PHONE_1                  = "PHONE";    //"PHONE-1";
		public static final String PHONE_2                  = "PHONE-2";
		public static final String PHONE_3                  = "PHONE-3";
		public static final String EMAIL_1                  = "EMAILS";	//"EMAIL-1";
		public static final String EMAIL_2                  = "EMAIL-2";
		public static final String BRANCH                   = "BRANCH";
		public static final String KENDRA                   = "KENDRA";
		public static final String MBR_ID                   = "MBR-ID";
		public static final String LOS_APP_ID               = "LOS-APP-ID";
		public static final String CREDT_INQ_PURPS_TYP      = "CREDIT-INQ-PURPS-TYP";
		public static final String CREDT_INQ_PURPS_TYP_DESC = "CREDIT-INQ-PURPS-TYP-DESC";
		public static final String CREDIT_INQUIRY_STAGE     = "CREDIT-INQUIRY-STAGE";
		public static final String CREDT_RPT_ID             = "CREDIT-RPT-ID";
		public static final String CREDT_REQ_TYP            = "CREDIT-REQ-TYP";
		public static final String CREDT_RPT_TRN_DT_TM      = "CREDIT-RPT-TRN-DT-TM";
		public static final String AC_OPEN_DT               = "AC-OPEN-DT";
		public static final String LOAN_AMOUNT              = "LOAN-AMOUNT";
		public static final String MFI_IND = "MFI-IND";
		public static final String MFI_SCORE="MFI-SCORE";
		public static final String MFI_GROUP="MFI-GROUP";
		public static final String CNS_IND="CNS-IND";
		public static final String CNS_SCORE="CNS-SCORE";
		public static final String IOI="IOI";
		public static final String IDS="IDS";
		public static final String ENTITY_ID = "ENTITY-ID";
		public static final String PHONES = "PHONES";
	}
	
	public interface ACCOUNTS_SUMMARY{
		public static final String PRIMARY_ACCOUNTS_SUMMARY   = "PRIMARY-ACCOUNTS-SUMMARY";
		public static final String SECONDARY_ACCOUNTS_SUMMARY = "SECONDARY-ACCOUNTS-SUMMARY";
		public static final String DERIVED_ATTRIBUTES         = "DERIVED-ATTRIBUTES";
		
	}
	
	
	public interface PRIMARY_ACCOUNTS_SUMMARY{
		public static final String PRIMARY_NUMBER_OF_ACCOUNTS             = "PRIMARY-NUMBER-OF-ACCOUNTS"; 
		public static final String PRIMARY_ACTIVE_NUMBER_OF_ACCOUNTS      = "PRIMARY-ACTIVE-NUMBER-OF-ACCOUNTS";
		public static final String PRIMARY_OVERDUE_NUMBER_OF_ACCOUNTS     = "PRIMARY-OVERDUE-NUMBER-OF-ACCOUNTS";
		public static final String PRIMARY_CURRENT_BALANCE                = "PRIMARY-CURRENT-BALANCE";
		public static final String PRIMARY_SANCTIONED_AMOUNT              = "PRIMARY-SANCTIONED-AMOUNT";
		public static final String PRIMARY_DISBURSED_AMOUNT               = "PRIMARY-DISBURSED-AMOUNT";
		public static final String PRIMARY_SECURED_NUMBER_OF_ACCOUNTS     = "PRIMARY-SECURED-NUMBER-OF-ACCOUNTS";
		public static final String PRIMARY_UNSECURED_NUMBER_OF_ACCOUNTS   = "PRIMARY-UNSECURED-NUMBER-OF-ACCOUNTS";
		public static final String PRIMARY_UNTAGGED_NUMBER_OF_ACCOUNTS    = "PRIMARY-UNTAGGED-NUMBER-OF-ACCOUNTS";
		public static final String PRIMARYCURRBAL="primaryCurrBal";
		public static final String PRIMARYSANCAMT="primarySancAmt";
		public static final String PRIMARYDISBAMT="primaryDisbAmt";
		
	}
	
	public interface SECONDARY_ACCOUNTS_SUMMARY{
		public static final String SECONDARY_NUMBER_OF_ACCOUNTS           = "SECONDARY-NUMBER-OF-ACCOUNTS";  
		public static final String SECONDARY_ACTIVE_NUMBER_OF_ACCOUNTS    = "SECONDARY-ACTIVE-NUMBER-OF-ACCOUNTS";
		public static final String SECONDARY_OVERDUE_NUMBER_OF_ACCOUNTS   = "SECONDARY-OVERDUE-NUMBER-OF-ACCOUNTS";
		public static final String SECONDARY_CURRENT_BALANCE              = "SECONDARY-CURRENT-BALANCE";
		public static final String SECONDARY_SANCTIONED_AMOUNT            = "SECONDARY-SANCTIONED-AMOUNT";
		public static final String SECONDARY_SECURED_NUMBER_OF_ACCOUNTS   = "SECONDARY-SECURED-NUMBER-OF-ACCOUNTS";
		public static final String SECONDARY_UNSECURED_NUMBER_OF_ACCOUNTS = "SECONDARY-UNSECURED-NUMBER-OF-ACCOUNTS";
		public static final String SECONDARY_UNTAGGED_NUMBER_OF_ACCOUNTS  = "SECONDARY-UNTAGGED-NUMBER-OF-ACCOUNTS";
		public static final String SECONDARY_DISBURSED_AMOUNT             = "SECONDARY-DISBURSED-AMOUNT";
		public static final String SECCURRBAL="secCurrBal";
		public static final String SECSANCAMT="secSancAmt";
		public static final String SECDISBAMT="secDisbAmt";
	}
	
	public interface DERIVED_ATTRIBUTES{
		public static final String INQURIES_IN_LAST_SIX_MONTHS            = "INQUIRIES-IN-LAST-SIX-MONTHS";
		public static final String LENGTH_OF_CREDIT_HISTORY_YEAR          = "LENGTH-OF-CREDIT-HISTORY-YEAR";
		public static final String LENGTH_OF_CREDIT_HISTORY_MONTH         = "LENGTH-OF-CREDIT-HISTORY-MONTH";
		public static final String AVERAGE_ACCOUNT_AGE_YEAR               = "AVERAGE-ACCOUNT-AGE-YEAR";
		public static final String AVERAGE_ACCOUNT_AGE_MONTH              = "AVERAGE-ACCOUNT-AGE-MONTH";
		public static final String NEW_ACCOUNTS_IN_LAST_SIX_MONTHS        = "NEW-ACCOUNTS-IN-LAST-SIX-MONTHS";
		public static final String NEW_DELINQ_ACCOUNT_IN_LAST_SIX_MONTHS  = "NEW-DELINQ-ACCOUNT-IN-LAST-SIX-MONTHS";
		
	}
	
	public interface PERSONAL_INFO_VARIATION{
		public static final String NAME_VARIATIONS              = "NAME-VARIATIONS";
		public static final String ADDRESS_VARIATIONS           = "ADDRESS-VARIATIONS";
		public static final String PAN_VARIATIONS               = "PAN-VARIATIONS";
		public static final String DRIVING_LICENSE_VARIATIONS   = "DRIVING-LICENSE-VARIATIONS";
		public static final String DATE_OF_BIRTH_VARIATIONS 	= "DATE-OF-BIRTH-VARIATIONS";
		public static final String VOTER_ID_VARIATIONS          = "VOTER-ID-VARIATIONS";
		public static final String PASSPORT_VARIATIONS          = "PASSPORT-VARIATIONS";
		public static final String PHONE_NUMBER_VARIATIONS      = "PHONE-NUMBER-VARIATIONS";
		public static final String RATION_CARD_VARIATIONS       = "RATION-CARD-VARIATIONS";  
		public static final String EMAIL_VARIATIONS             = "EMAIL-VARIATIONS";
		public static final String UID_VARIATIONS               = "UID-VARIATIONS";
		public static final String OTHER_ID_VARIATIONS          = "OTHER-ID-VARIATIONS";
		public static final String VARIATION                    = "VARIATION";
		
	}
	
	
	
	public interface NAME_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	} 
	public interface OTHER_ID_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	} 
	public interface UID_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	} 
	
	public interface ADDRESS_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface PAN_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	
	public interface DRIVING_LICENSE_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface DATE_OF_BIRTH_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface VOTER_ID_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface PASSPORT_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface PHONE_NUMBER_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface RATION_CARD_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface EMAIL_VARIATIONS{
		public static final String VARIATION = "VARIATION";
	}
	
	public interface VARIATION{
		public static final String VALUE = "VALUE";
		public static final String REPORTED_DATE = "REPORTED-DATE";
	}
	
	public interface RESPONSES {
		public static final String RESPONSE = "RESPONSE";
	}
	
	
	public interface RESPONSE{
		public static final String LOAN_DETAILS  = "LOAN-DETAILS";
	}
	
	public interface LOAN_DETAILS{
		public static final String MATCHED_TYPE             = "MATCHED-TYPE"; 
		public static final String ACCT_NUMBER              = "ACCT-NUMBER";
		public static final String CREDIT_GUARANTOR         = "CREDIT-GUARANTOR";
		public static final String ACCT_TYPE                = "ACCT-TYPE";
		public static final String DATE_REPORTED            = "DATE-REPORTED";
		public static final String OWNERSHIP_IND            = "OWNERSHIP-IND";
		public static final String ACCOUNT_STATUS           = "ACCOUNT-STATUS";
		public static final String DISBURSED_AMT            = "DISBURSED-AMT";
		public static final String DISBURSED_DATE           =  "DISBURSED-DATE";    //"DISBURSED-DT";
		public static final String LAST_PAYMENT_DATE        = "LAST-PAYMENT-DATE";
		public static final String CLOSED_DATE              =  "CLOSED-DATE"; 
		public static final String INSTALLMENT_AMT          = "INSTALLMENT-AMT";
		public static final String OVERDUE_AMT              = "OVERDUE-AMT";
		public static final String WRITE_OFF_AMT            = "WRITE-OFF-AMT";
		public static final String CURRENT_BAL              = "CURRENT-BAL";
		public static final String CREDIT_LIMIT             = "CREDIT-LIMIT";
		public static final String ACCOUNT_REMARKS          = "ACCOUNT-REMARKS";
		public static final String FREQUENCY                = "FREQUENCY";
		public static final String SECURITY_STATUS          = "SECURITY-STATUS";
		public static final String ORIGINAL_TERM            = "ORIGINAL-TERM";
		public static final String TERM_TO_MATURITY         = "TERM-TO-MATURITY";
		public static final String ACCT_IN_DISPUTE          = "ACCT-IN-DISPUTE";
		public static final String SETTLEMENT_AMT           = "SETTLEMENT-AMT";
		public static final String PRINCIPAL_WRITE_OFF_AMT  = "PRINCIPAL-WRITE-OFF-AMT";
		public static final String PRINCIPAL_WRITE_OFF_AMT_  = "writeOffPrincipalAmt_";
		public static final String COMBINED_PAYMENT_HISTORY = "COMBINED-PAYMENT-HISTORY";
		public static final String LINKED_ACCOUNTS          = "LINKED-ACCOUNTS";
		public static final String SECURITY_DETAILS         = "SECURITY-DETAILS";
		public static final String SECURITY_DETAILS_         ="securityDetails__";
		public static final String REPAYMENT_TENURE 		="REPAYMENT-TENURE";
		public static final String CASH_LIMIT				="CASH-LIMIT";
		public static final String ACTUAL_PAYMENT			="ACTUAL-PAYMENT";
		public static final String SUIT_FILED_WILFUL_DEFAULT ="SUIT-FILED_WILFUL-DEFAULT";
		public static final String WRITTEN_OFF_SETTLED_STATUS="WRITTEN-OFF_SETTLED-STATUS";
		public static final String interestRate = "interestRate";
		public static final String CLOSE_DATE    = "CLOSE-DATE";	//added on 03-07-2017
	}
	
	public interface LINKED_ACCOUNTS{
		public static final String ACCOUNT_DETAILS             = "ACCOUNT-DETAILS"; 
		public static final String MATCHED_TYPE             = "MATCHED-TYPE"; 
		public static final String ACCT_NUMBER              = "ACCT-NUMBER";
		public static final String CREDIT_GUARANTOR         = "CREDIT-GUARANTOR";
		public static final String ACCT_TYPE                = "ACCT-TYPE";
		public static final String DATE_REPORTED            = "DATE-REPORTED";
		public static final String OWNERSHIP_IND            = "OWNERSHIP-IND";
		public static final String ACCOUNT_STATUS           = "ACCOUNT-STATUS";
		public static final String DISBURSED_AMT            = "DISBURSED-AMT";
		//public static final String DISBURSED_DT             = "DISBURSED-DT";
		public static final String LAST_PAYMENT_DATE        = "LAST-PAYMENT-DATE";
		//public static final String CLOSE_DT                 = "CLOSE-DT";
		public static final String INSTALLMENT_AMT          = "INSTALLMENT-AMT";
		public static final String OVERDUE_AMT              = "OVERDUE-AMT";
		public static final String WRITE_OFF_AMT            = "WRITE-OFF-AMT";
		public static final String CURRENT_BAL              = "CURRENT-BAL";
		public static final String CREDIT_LIMIT             = "CREDIT-LIMIT";
		public static final String ACCOUNT_REMARKS          = "ACCOUNT-REMARKS";
		public static final String FREQUENCY                = "FREQUENCY";
		public static final String SECURITY_STATUS          = "SECURITY-STATUS";
		public static final String ORIGINAL_TERM            = "ORIGINAL-TERM";
		public static final String TERM_TO_MATURITY         = "TERM-TO-MATURITY";
		public static final String ACCT_IN_DISPUTE          = "ACCT-IN-DISPUTE";
		public static final String SETTLEMENT_AMT           = "SETTLEMENT-AMT";
		public static final String PRINCIPAL_WRITE_OFF_AMT  = "PRINCIPAL-WRITE-OFF-AMT";
		public static final String COMBINED_PAYMENT_HISTORY = "COMBINED-PAYMENT-HISTORY";
		public static final String LINKED_ACCOUNTS          = "LINKED-ACCOUNTS";
		public static final String SECURITY_DETAILS         = "SECURITY-DETAILS";
		public static final String DISBURSED_DT             = "DISBURSED-DATE";
		public static final String CLOSE_DT                 = "CLOSE-DATE";
	}
	
	public interface SECURITY_DETAILS{
		public static final String SECURITY_DETAIL       = "SECURITY-DETAIL";
		public static final String SECURITY_TYPE       = "SECURITY-TYPE";
		public static final String OWNER_NAME          = "OWNER-NAME";
		public static final String SECURITY_VALUE      = "SECURITY-VALUE";
		public static final String DATE_OF_VALUE       = "DATE-OF-VALUE";
		public static final String SECURITY_CHARGE     = "SECURITY-CHARGE";
		public static final String PROPERTY_ADDRESS    = "PROPERTY-ADDRESS";
		public static final String AUTOMOBILE_TYPE     = "AUTOMOBILE-TYPE";
		public static final String YEAR_OF_MANUFACTURE = "YEAR-OF-MANUFACTURE";
		public static final String REGISTRATION_NUMBER = "REGISTRATION-NUMBER";
		public static final String ENGINE_NUMBER	   = "ENGINE-NUMBER";
		public static final String CHASSIS_NUMBER      = "CHASSIS-NUMBER";
	}
	
	public interface SECONDARY_MATCHES{
		public static final String SECONDARY_MATCH  = "SECONDARY-MATCH";
	}
	
	public interface SECONDARY_MATCH{
		public static final String NAME             = "NAME";         
		public static final String ADDRESS          = "ADDRESSES";		//"ADDRESS";
		public static final String DOB              = "DOB";
		public static final String PHONE            = "PHONE";
		public static final String PAN              = "PAN";
		public static final String PASSPORT         = "PASSPORT";
		public static final String DRIVING_LICENSE  = "DRIVING-LICENSE";
		public static final String VOTER_ID         = "VOTER-ID";
		public static final String E_MAIL           = "EMAILS";		//"E-MAIL";
		public static final String RATION_CARD      = "RATION-CARD";
		public static final String LOAN_DETAILS     = "LOAN-DETAILS";
		public static final String IDS="IDS";
		public static final String PHONES = "PHONES";
		public static final String RELATIONS="RELATIONS";
		
	}
	
	public interface SCORES{
		public static final String SCORE       = "SCORE";
	}
	
	public interface SCORE{
		public static final String SCORE_TYPE       = "SCORE-TYPE";
		public static final String SCORE_VERSION    = "SCORE-VERSION";
		public static final String SCORE_VALUE      = "SCORE-VALUE";
		public static final String SCORE_FACTORS    = "SCORE-FACTORS";
		public static final String SCORE_COMMENTS   = "SCORE-COMMENTS";
	}
	
	public interface INQUIRY_HISTORY{
		public static final String HISTORY = "HISTORY";
	}
	
	public interface HISTORY{
		public static final String MEMBER_NAME     = "MEMBER-NAME"; 
		public static final String INQUIRY_DATE    = "INQUIRY-DATE";
		public static final String PURPOSE         = "PURPOSE";
		public static final String OWNERSHIP_TYPE  = "OWNERSHIP-TYPE";
		public static final String AMOUNT          = "AMOUNT";
		public static final String REMARK          = "REMARK";
	}
	
	public interface COMMENTS {
		public static final String COMMENT = "COMMENT";
	}
	
	public interface COMMENT{
		public static final String  COMMENT_TEXT = "COMMENT-TEXT";
		public static final String  COMMENT_DATE  ="COMMENT-DATE";
		public static final String  BUREAU_COMMENT = "BUREAU-COMMENT"; 
	}
	
	public interface ALERTS {
		public static final String  ALERT = "ALERT";
	}
	
	
	public interface INDV_RESPONSES{

		public static final String PRIMARY_SUMMARY = "PRIMARY-SUMMARY";
		public static final  String SUMMARY = "SUMMARY";
		public static final String SECONDARY_SUMMARY="SECONDARY-SUMMARY";
		public static final String INDV_RESPONSE_LIST="INDV-RESPONSE-LIST";
		
	}
	public interface INDV_RESPONSE_LIST{
		public static final String REPORT_DT="reportDt";
		public static final String RECENT_DELINQ_DT="RECENT-DELINQ-DT";
		public static final String LOAN_CYCLE_ID="LOAN-CYCLE-ID";
		public static final String INDV_RESPONSE="INDV-RESPONSE";
		public static final String MFI="MFI";
		public static final String MFI_ID="MFI-ID";
		public static final String BRANCH="BRANCH";
		public static final String KENDRA="KENDRA";
		public static final String NAME="NAME";
		public static final String DOB="DOB";
		
		public static final String TYPE="TYPE";
		public static final String VALUE="VALUE";
		
		public static final String CNSMRMBRID="CNSMRMBRID";
		public static final String MATCHED_TYPE="MATCHED-TYPE";
		public static final String INSERT_DATE="INSERT-DATE";
		public static final String GROUP_DETAILS="GROUP-DETAILS";
		public static final String GROUP_ID="GROUP-ID";
		public static final String TOT_DISBURSED_AMT="TOT-DISBURSED-AMT";
		public static final String TOT_CURRENT_BAL="TOT-CURRENT-BAL";
		public static final String TOT_ACCOUNTS="TOT-ACCOUNTS";
		public static final String TOT_DPD_30="TOT-DPD-30";
		public static final String TOT_DPD_60="TOT-DPD-60";
		public static final String TOT_DPD_90="TOT-DPD-90";
		public static final String LOAN_DETAILS="LOAN-DETAILS";
		public static final String LOAN_DETAIL="LOAN-DETAIL";
		public static final String ACCT_TYPE="ACCT-TYPE";
		public static final String FREQ="FREQ";
		public static final String STATUS="STATUS";
		public static final String ACCT_NUMBER="ACCT-NUMBER";
		public static final String DISBURSED_AMT="DISBURSED-AMT";
		public static final String CURRENT_BAL="CURRENT-BAL";
		public static final String INSTALLMENT_AMT="INSTALLMENT-AMT";
		public static final String OVERDUE_AMT="OVERDUE-AMT";
		public static final String WRITE_OFF_AMT="WRITE-OFF-AMT";
		public static final String DISBURSED_DT="DISBURSED-DT";
		public static final String CLOSED_DT="CLOSED-DT";
		public static final String INQ_CNT="INQ-CNT";
		public static final String DPD="DPD";
		public static final String INFO_AS_ON="INFO-AS-ON";
		public static final String COMBINED_PAYMENT_HISTORY="COMBINED-PAYMENT-HISTORY";
		public static final String AGE = "AGE";
		public static final String AGE_AS_ON = "AGE-AS-ON";
		public static final String IDS="IDS";
		public static final String ADDRESSES="ADDRESSES";
		public static final String RELATIONS="RELATIONS";
		public static final String PHONES="PHONES";
		public static final String TOT_INSTALLMENT_AMT="TOT-INSTALLMENT-AMT";
		public static final String TOT_WRITE_OFFS="TOT-WRITE-OFFS";
		public static final String ENTITY_ID="ENTITY-ID";
		public static final String GROUP_CREATION_DATE="GROUP-CREATION-DATE";
		public static final String PAYMENT_HISTORY="PAYMENT-HISTORY";
		public static final String WORST_DELEQUENCY_AMOUNT="WORST-DELEQUENCY-AMOUNT";
		public static final String NO_OF_BORROWERS="NO-OF-BORROWERS";
		public static final String ACTIVE_BORROWERS="ACTIVE-BORROWERS";
		public static final String COMMENT = "COMMENT";
		public static final String EMAILS="EMAILS";
		
	}
	
		
		

	public interface SUMMARY{
		public static final String NO_OF_DEFAULT_ACCOUNTS="NO-OF-DEFAULT-ACCOUNTS";
		public static final String TOTAL_OTHER_DISBURSED_AMOUNT="TOTAL-OTHER-DISBURSED-AMOUNT";
		public static final String TOTAL_OTHER_CURRENT_BALANCE = "TOTAL-OTHER-CURRENT-BALANCE";
		public static final String TOTAL_OTHER_INSTALLMENT_AMOUNT = "TOTAL-OTHER-INSTALLMENT-AMOUNT";
		public static final String TOTAL_OWN_DISBURSED_AMOUNT = "TOTAL-OWN-DISBURSED-AMOUNT";
		public static final String TOTAL_OWN_CURRENT_BALANCE="TOTAL-OWN-CURRENT-BALANCE";
		public static final String STATUS = "STATUS";
		public static final String TOTAL_RESPONSES="TOTAL-RESPONSES";
		public static final String NO_OF_CLOSED_ACCOUNTS = "NO-OF-CLOSED-ACCOUNTS";
		public static final String NO_OF_ACTIVE_ACCOUNTS ="NO-OF-ACTIVE-ACCOUNTS";
		public static final String NO_OF_OTHER_MFIS= "NO-OF-OTHER-MFIS";
		public static final String NO_OF_OWN_MFIS= "NO-OF-OWN-MFIS";
		public static final String OWN_MFI_INDECATOR= "OWN-MFI-INDECATOR";
		public static final String MAX_WORST_DELEQUENCY= "MAX-WORST-DELEQUENCY";
		public static final String ERRORS = "ERRORS";
		public static final String TOTAL_OWN_INSTALLMENT_AMOUNT="TOTAL-OWN-INSTALLMENT-AMOUNT";
	}
	
	
	public interface ALERT{
		public static final String ALERT_TYPE = "ALERT-TYPE";
		public static final String ALERT_DESC = "ALERT-DESC";
	}
	
	public interface PRINTABLE_REPORT {
		public static final String TYPE      = "TYPE"; 
		public static final String FILE_NAME = "FILE-NAME";
		public static final String CONTENT   = "CONTENT";
	}

	
	
	
}
