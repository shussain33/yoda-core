package com.softcell.gonogo.model.multibureau.experian;

public class OthCBInformation {

	private String 	AnyRelCBDataDisYN;
	private String OthRelCBDFCPosMatYN;
	
	
	
	public String getAnyRelCBDataDisYN() {
		return AnyRelCBDataDisYN;
	}
	public void setAnyRelCBDataDisYN(String anyRelCBDataDisYN) {
		AnyRelCBDataDisYN = anyRelCBDataDisYN;
	}
	public String getOthRelCBDFCPosMatYN() {
		return OthRelCBDFCPosMatYN;
	}
	public void setOthRelCBDFCPosMatYN(String othRelCBDFCPosMatYN) {
		OthRelCBDFCPosMatYN = othRelCBDFCPosMatYN;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		OthCBInformation that = (OthCBInformation) o;

		if (AnyRelCBDataDisYN != null ? !AnyRelCBDataDisYN.equals(that.AnyRelCBDataDisYN) : that.AnyRelCBDataDisYN != null)
			return false;
		return OthRelCBDFCPosMatYN != null ? OthRelCBDFCPosMatYN.equals(that.OthRelCBDFCPosMatYN) : that.OthRelCBDFCPosMatYN == null;
	}

	@Override
	public int hashCode() {
		int result = AnyRelCBDataDisYN != null ? AnyRelCBDataDisYN.hashCode() : 0;
		result = 31 * result + (OthRelCBDFCPosMatYN != null ? OthRelCBDFCPosMatYN.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("OthCBInformation{");
		sb.append("AnyRelCBDataDisYN='").append(AnyRelCBDataDisYN).append('\'');
		sb.append(", OthRelCBDFCPosMatYN='").append(OthRelCBDFCPosMatYN).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
