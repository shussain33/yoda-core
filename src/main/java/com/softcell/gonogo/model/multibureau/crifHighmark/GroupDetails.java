package com.softcell.gonogo.model.multibureau.crifHighmark;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="GROUP-DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupDetails {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="GROUP-ID")
	private String grpId;
	@XmlElement(name="TOT-DISBURSED-AMT")
	private String totDisbAmt;
	@XmlElement(name="TOT-CURRENT-BAL")
	private String totCurrBal;
	@XmlElement(name="TOT-ACCOUNTS")
	private String totAcc;
	@XmlElement(name="TOT-DPD-30")
	private String totDpd30;
	@XmlElement(name="TOT-DPD-60")
	private String totDpd60;
	@XmlElement(name="TOT-DPD-90")
	private String totDpd90;
	@XmlElement(name="TOT-WRITE-OFFS")
	private String totWriteOffs;
	@XmlElement(name="TOT-DELINQ-MBR")
	private String totDelinqMbr;
	@XmlElement(name="TOT-OVERDUE-AMT")
	private String totOverdueAmt;
	@XmlElement(name="TOT-INSTALLMENT-AMT")
	private String totInstallmentAmt;
	@XmlElement(name="TOT-MEMBER")
	private String totMember;
	
	public String getTotMember() {
		return totMember;
	}

	public void setTotMember(String totMember) {
		this.totMember = totMember;
	}

	public String getTotWriteOffs() {
		return totWriteOffs;
	}

	public void setTotWriteOffs(String totWriteOffs) {
		this.totWriteOffs = totWriteOffs;
	}

	public String getTotDelinqMbr() {
		return totDelinqMbr;
	}

	public void setTotDelinqMbr(String totDelinqMbr) {
		this.totDelinqMbr = totDelinqMbr;
	}

	public String getTotOverdueAmt() {
		return totOverdueAmt;
	}

	public void setTotOverdueAmt(String totOverdueAmt) {
		this.totOverdueAmt = totOverdueAmt;
	}

	public String getTotInstallmentAmt() {
		return totInstallmentAmt;
	}

	public void setTotInstallmentAmt(String totInstallmentAmt) {
		this.totInstallmentAmt = totInstallmentAmt;
	}

	public String getGrpId() {
		return grpId;
	}
	
	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}
	
	public String getTotDisbAmt() {
		return totDisbAmt;
	}
	
	public void setTotDisbAmt(String totDisbAmt) {
		this.totDisbAmt = totDisbAmt;
	}
	
	public String getTotCurrBal() {
		return totCurrBal;
	}
	
	public void setTotCurrBal(String totCurrBal) {
		this.totCurrBal = totCurrBal;
	}
	
	public String getTotAcc() {
		return totAcc;
	}
	
	public void setTotAcc(String totAcc) {
		this.totAcc = totAcc;
	}
	
	public String getTotDpd30() {
		return totDpd30;
	}
	
	public void setTotDpd30(String totDpd30) {
		this.totDpd30 = totDpd30;
	}
	
	public String getTotDpd60() {
		return totDpd60;
	}
	
	public void setTotDpd60(String totDpd60) {
		this.totDpd60 = totDpd60;
	}
	
	public String getTotDpd90() {
		return totDpd90;
	}
	
	public void setTotDpd90(String totDpd90) {
		this.totDpd90 = totDpd90;
	}

	@Override
	public String toString() {
		return "GroupDetails [grpId=" + grpId + ", totDisbAmt=" + totDisbAmt
				+ ", totCurrBal=" + totCurrBal + ", totAcc=" + totAcc
				+ ", totDpd30=" + totDpd30 + ", totDpd60=" + totDpd60
				+ ", totDpd90=" + totDpd90 + ", totWriteOffs=" + totWriteOffs
				+ ", totDelinqMbr=" + totDelinqMbr + ", totOverdueAmt="
				+ totOverdueAmt + ", totInstallmentAmt=" + totInstallmentAmt
				+ ", totMember=" + totMember + "]";
	}
	
}
