package com.softcell.gonogo.model.multibureau.experian;

public class ProveIdSummary {

    private String noOfDistinctActiveLenders;

    public String getNoOfDistinctActiveLenders() {
        return noOfDistinctActiveLenders;
    }

    public void setNoOfDistinctActiveLenders(String noOfDistinctActiveLenders) {
        this.noOfDistinctActiveLenders = noOfDistinctActiveLenders;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProveIdSummary that = (ProveIdSummary) o;

        return noOfDistinctActiveLenders != null ? noOfDistinctActiveLenders.equals(that.noOfDistinctActiveLenders) : that.noOfDistinctActiveLenders == null;
    }

    @Override
    public int hashCode() {
        return noOfDistinctActiveLenders != null ? noOfDistinctActiveLenders.hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProveIdSummary{");
        sb.append("noOfDistinctActiveLenders='").append(noOfDistinctActiveLenders).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
