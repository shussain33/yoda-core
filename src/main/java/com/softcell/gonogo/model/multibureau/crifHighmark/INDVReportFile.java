package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INDV-REPORT-FILE")
@XmlAccessorType(XmlAccessType.FIELD)
public class INDVReportFile {
	
	@XmlElement(name="INDV-REPORTS")
	private INDVReports INDVReports =null;
	@XmlElement(name="INQUIRY-STATUS")
	private InquiryStatus inquiryStatus= null;
	
	public INDVReports getINDVReports() {
		return INDVReports;
	}
	public void setINDVReports(INDVReports INDVReports) {
		this.INDVReports = INDVReports;
	}
	public InquiryStatus getInquiryStatus() {
		return inquiryStatus;
	}
	public void setInquiryStatus(InquiryStatus inquiryStatus) {
		this.inquiryStatus = inquiryStatus;
	}
	@Override
	public String toString() {
		return "INDVReportFile [INDVReports=" + INDVReports
				+ ", inquiryStatus=" + inquiryStatus + "]";
	}
	
	
	
}
