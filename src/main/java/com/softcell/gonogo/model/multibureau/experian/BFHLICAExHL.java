package com.softcell.gonogo.model.multibureau.experian;

public class BFHLICAExHL {

    private String TNOfNDelBFHLInACAExHL;
    private String TNOfDelBFHLInACAExHL;

    public String getTNOfNDelBFHLInACAExHL() {
        return TNOfNDelBFHLInACAExHL;
    }

    public void setTNOfNDelBFHLInACAExHL(String tNOfNDelBFHLInACAExHL) {
        TNOfNDelBFHLInACAExHL = tNOfNDelBFHLInACAExHL;
    }

    public String getTNOfDelBFHLInACAExHL() {
        return TNOfDelBFHLInACAExHL;
    }

    public void setTNOfDelBFHLInACAExHL(String tNOfDelBFHLInACAExHL) {
        TNOfDelBFHLInACAExHL = tNOfDelBFHLInACAExHL;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BFHLICAExHL{");
        sb.append("TNOfNDelBFHLInACAExHL='").append(TNOfNDelBFHLInACAExHL).append('\'');
        sb.append(", TNOfDelBFHLInACAExHL='").append(TNOfDelBFHLInACAExHL).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BFHLICAExHL that = (BFHLICAExHL) o;

        if (TNOfNDelBFHLInACAExHL != null ? !TNOfNDelBFHLInACAExHL.equals(that.TNOfNDelBFHLInACAExHL) : that.TNOfNDelBFHLInACAExHL != null)
            return false;
        return TNOfDelBFHLInACAExHL != null ? TNOfDelBFHLInACAExHL.equals(that.TNOfDelBFHLInACAExHL) : that.TNOfDelBFHLInACAExHL == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfNDelBFHLInACAExHL != null ? TNOfNDelBFHLInACAExHL.hashCode() : 0;
        result = 31 * result + (TNOfDelBFHLInACAExHL != null ? TNOfDelBFHLInACAExHL.hashCode() : 0);
        return result;
    }
}
