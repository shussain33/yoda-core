package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrganisationEntity {

	/**
	 * @author namratat
	 *
	 *
	 */
	
	@JsonProperty("01")
	private String organisationEntityType;
	
	@JsonProperty("02")
	private String organisationName;
	
	@JsonProperty("03")
	private String organisationShortName;

	@JsonProperty("04")
	private String incorpDt;
	
	@JsonProperty("05")
	private List<AddressDomain> addressDomainList;

	@JsonProperty("06")
	private IDDomain domain;

	@JsonProperty("07")
	private List<PhoneDomain> phoneDomainList;

	@JsonProperty("08")
	private String classOfActivity;
	
	@JsonProperty("09")
	private String registrationDt;
	
	@JsonProperty("10")
	private String cmrFlag;
	
	@JsonProperty("11")
	private String gstDestState;

	public String getOrganisationEntityType() {
		return organisationEntityType;
	}

	public void setOrganisationEntityType(String organisationEntityType) {
		this.organisationEntityType = organisationEntityType;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getOrganisationShortName() {
		return organisationShortName;
	}

	public void setOrganisationShortName(String organisationShortName) {
		this.organisationShortName = organisationShortName;
	}

	public String getIncorpDt() {
		return incorpDt;
	}

	public void setIncorpDt(String incorpDt) {
		this.incorpDt = incorpDt;
	}

	public List<AddressDomain> getAddressDomainList() {
		return addressDomainList;
	}

	public void setAddressDomainList(List<AddressDomain> addressDomainList) {
		this.addressDomainList = addressDomainList;
	}

	public IDDomain getDomain() {
		return domain;
	}

	public void setDomain(IDDomain domain) {
		this.domain = domain;
	}

	public List<PhoneDomain> getPhoneDomainList() {
		return phoneDomainList;
	}

	public void setPhoneDomainList(List<PhoneDomain> phoneDomainList) {
		this.phoneDomainList = phoneDomainList;
	}

	public String getClassOfActivity() {
		return classOfActivity;
	}

	public void setClassOfActivity(String classOfActivity) {
		this.classOfActivity = classOfActivity;
	}

	public String getRegistrationDt() {
		return registrationDt;
	}

	public void setRegistrationDt(String registrationDt) {
		this.registrationDt = registrationDt;
	}

	public String getCmrFlag() {
		return cmrFlag;
	}

	public void setCmrFlag(String cmrFlag) {
		this.cmrFlag = cmrFlag;
	}

	public String getGstDestState() {
		return gstDestState;
	}

	public void setGstDestState(String gstDestState) {
		this.gstDestState = gstDestState;
	}

	@Override
	public String toString() {
		return "OrganisationEntity [organisationEntityType="
				+ organisationEntityType + ", organisationName="
				+ organisationName + ", organisationShortName="
				+ organisationShortName + ", incorpDt=" + incorpDt
				+ ", addressDomainList=" + addressDomainList + ", domain="
				+ domain + ", phoneDomainList=" + phoneDomainList
				+ ", classOfActivity=" + classOfActivity + ", registrationDt="
				+ registrationDt + ", cmrFlag=" + cmrFlag + ", gstDestState="
				+ gstDestState + "]";
	}
	
}
