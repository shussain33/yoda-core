package com.softcell.gonogo.model.multibureau.experian;

public class IndianMarketSpecificVar {

    private String TNOfCADClassedAsSFWDWO;
    private String MNTSMRCADClassedAsSFWDWO;
    private String NumOfCADSFWDWOLast24MNT;
    private String TotCurBalLiveSAcc;
    private String TotCurBalLiveUAcc;
    private String TotCurBalMaxBalLiveSAcc;
    private String TotCurBalMaxBalLiveUAcc;

    public String getTNOfCADClassedAsSFWDWO() {
        return TNOfCADClassedAsSFWDWO;
    }

    public void setTNOfCADClassedAsSFWDWO(String tNOfCADClassedAsSFWDWO) {
        TNOfCADClassedAsSFWDWO = tNOfCADClassedAsSFWDWO;
    }

    public String getMNTSMRCADClassedAsSFWDWO() {
        return MNTSMRCADClassedAsSFWDWO;
    }

    public void setMNTSMRCADClassedAsSFWDWO(String mNTSMRCADClassedAsSFWDWO) {
        MNTSMRCADClassedAsSFWDWO = mNTSMRCADClassedAsSFWDWO;
    }

    public String getNumOfCADSFWDWOLast24MNT() {
        return NumOfCADSFWDWOLast24MNT;
    }

    public void setNumOfCADSFWDWOLast24MNT(String numOfCADSFWDWOLast24MNT) {
        NumOfCADSFWDWOLast24MNT = numOfCADSFWDWOLast24MNT;
    }

    public String getTotCurBalLiveSAcc() {
        return TotCurBalLiveSAcc;
    }

    public void setTotCurBalLiveSAcc(String totCurBalLiveSAcc) {
        TotCurBalLiveSAcc = totCurBalLiveSAcc;
    }

    public String getTotCurBalLiveUAcc() {
        return TotCurBalLiveUAcc;
    }

    public void setTotCurBalLiveUAcc(String totCurBalLiveUAcc) {
        TotCurBalLiveUAcc = totCurBalLiveUAcc;
    }

    public String getTotCurBalMaxBalLiveSAcc() {
        return TotCurBalMaxBalLiveSAcc;
    }

    public void setTotCurBalMaxBalLiveSAcc(String totCurBalMaxBalLiveSAcc) {
        TotCurBalMaxBalLiveSAcc = totCurBalMaxBalLiveSAcc;
    }

    public String getTotCurBalMaxBalLiveUAcc() {
        return TotCurBalMaxBalLiveUAcc;
    }

    public void setTotCurBalMaxBalLiveUAcc(String totCurBalMaxBalLiveUAcc) {
        TotCurBalMaxBalLiveUAcc = totCurBalMaxBalLiveUAcc;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IndianMarketSpecificVar{");
        sb.append("TNOfCADClassedAsSFWDWO='").append(TNOfCADClassedAsSFWDWO).append('\'');
        sb.append(", MNTSMRCADClassedAsSFWDWO='").append(MNTSMRCADClassedAsSFWDWO).append('\'');
        sb.append(", NumOfCADSFWDWOLast24MNT='").append(NumOfCADSFWDWOLast24MNT).append('\'');
        sb.append(", TotCurBalLiveSAcc='").append(TotCurBalLiveSAcc).append('\'');
        sb.append(", TotCurBalLiveUAcc='").append(TotCurBalLiveUAcc).append('\'');
        sb.append(", TotCurBalMaxBalLiveSAcc='").append(TotCurBalMaxBalLiveSAcc).append('\'');
        sb.append(", TotCurBalMaxBalLiveUAcc='").append(TotCurBalMaxBalLiveUAcc).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IndianMarketSpecificVar that = (IndianMarketSpecificVar) o;

        if (TNOfCADClassedAsSFWDWO != null ? !TNOfCADClassedAsSFWDWO.equals(that.TNOfCADClassedAsSFWDWO) : that.TNOfCADClassedAsSFWDWO != null)
            return false;
        if (MNTSMRCADClassedAsSFWDWO != null ? !MNTSMRCADClassedAsSFWDWO.equals(that.MNTSMRCADClassedAsSFWDWO) : that.MNTSMRCADClassedAsSFWDWO != null)
            return false;
        if (NumOfCADSFWDWOLast24MNT != null ? !NumOfCADSFWDWOLast24MNT.equals(that.NumOfCADSFWDWOLast24MNT) : that.NumOfCADSFWDWOLast24MNT != null)
            return false;
        if (TotCurBalLiveSAcc != null ? !TotCurBalLiveSAcc.equals(that.TotCurBalLiveSAcc) : that.TotCurBalLiveSAcc != null)
            return false;
        if (TotCurBalLiveUAcc != null ? !TotCurBalLiveUAcc.equals(that.TotCurBalLiveUAcc) : that.TotCurBalLiveUAcc != null)
            return false;
        if (TotCurBalMaxBalLiveSAcc != null ? !TotCurBalMaxBalLiveSAcc.equals(that.TotCurBalMaxBalLiveSAcc) : that.TotCurBalMaxBalLiveSAcc != null)
            return false;
        return TotCurBalMaxBalLiveUAcc != null ? TotCurBalMaxBalLiveUAcc.equals(that.TotCurBalMaxBalLiveUAcc) : that.TotCurBalMaxBalLiveUAcc == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfCADClassedAsSFWDWO != null ? TNOfCADClassedAsSFWDWO.hashCode() : 0;
        result = 31 * result + (MNTSMRCADClassedAsSFWDWO != null ? MNTSMRCADClassedAsSFWDWO.hashCode() : 0);
        result = 31 * result + (NumOfCADSFWDWOLast24MNT != null ? NumOfCADSFWDWOLast24MNT.hashCode() : 0);
        result = 31 * result + (TotCurBalLiveSAcc != null ? TotCurBalLiveSAcc.hashCode() : 0);
        result = 31 * result + (TotCurBalLiveUAcc != null ? TotCurBalLiveUAcc.hashCode() : 0);
        result = 31 * result + (TotCurBalMaxBalLiveSAcc != null ? TotCurBalMaxBalLiveSAcc.hashCode() : 0);
        result = 31 * result + (TotCurBalMaxBalLiveUAcc != null ? TotCurBalMaxBalLiveUAcc.hashCode() : 0);
        return result;
    }
}
