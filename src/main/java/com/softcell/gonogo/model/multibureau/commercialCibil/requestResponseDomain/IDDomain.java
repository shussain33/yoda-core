package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IDDomain {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@JsonProperty("01")
	private String pan;					
	
	@JsonProperty("02")
	private String duns;
	
	@JsonProperty("03")
	private String cin;
	
	@JsonProperty("04")
	private String tin;
	
	@JsonProperty("05")
	private String idType1;
	
	@JsonProperty("06")
	private String idType1Value;
	
	@JsonProperty("07")
	private String idType2;
	
	@JsonProperty("08")
	private String idType2Value;
	
	@JsonProperty("09")
	private String serviceTaxNo; // ACE

	@JsonProperty("10")
	private String crn;
	
	@JsonProperty("11")
	private String passportId;
	
	@JsonProperty("12")
	private String voterId;
	
	@JsonProperty("13")
	private String drivingLicenseNo;
	
	@JsonProperty("14")
	private String uidNo;
	
	@JsonProperty("15")
	private String rationCard;

	public String getPassportId() {
		return passportId;
	}

	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}

	public String getVoterId() {
		return voterId;
	}

	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}

	public String getDrivingLicenseNo() {
		return drivingLicenseNo;
	}

	public void setDrivingLicenseNo(String drivingLicenseNo) {
		this.drivingLicenseNo = drivingLicenseNo;
	}

	public String getUidNo() {
		return uidNo;
	}

	public void setUidNo(String uidNo) {
		this.uidNo = uidNo;
	}

	public String getRationCard() {
		return rationCard;
	}

	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getDuns() {
		return duns;
	}

	public void setDuns(String duns) {
		this.duns = duns;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getTin() {
		return tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public String getIdType1() {
		return idType1;
	}

	public void setIdType1(String idType1) {
		this.idType1 = idType1;
	}

	public String getIdType1Value() {
		return idType1Value;
	}

	public void setIdType1Value(String idType1Value) {
		this.idType1Value = idType1Value;
	}

	public String getIdType2() {
		return idType2;
	}

	public void setIdType2(String idType2) {
		this.idType2 = idType2;
	}

	public String getIdType2Value() {
		return idType2Value;
	}

	public void setIdType2Value(String idType2Value) {
		this.idType2Value = idType2Value;
	}

	
	public String getServiceTaxNo() {
		return serviceTaxNo;
	}

	
	public void setServiceTaxNo(String serviceTaxNo) {
		this.serviceTaxNo = serviceTaxNo;
	}

	@Override
	public String toString() {
		return "IDDomain [pan=" + pan + ", duns=" + duns + ", cin=" + cin
				+ ", tin=" + tin + ", idType1=" + idType1 + ", idType1Value="
				+ idType1Value + ", idType2=" + idType2 + ", idType2Value="
				+ idType2Value + ", serviceTaxNo=" + serviceTaxNo + ", crn="
				+ crn + ", passportId=" + passportId + ", voterId=" + voterId
				+ ", drivingLicenseNo=" + drivingLicenseNo + ", uidNo=" + uidNo
				+ ", rationCard=" + rationCard + "]";
	}

}
