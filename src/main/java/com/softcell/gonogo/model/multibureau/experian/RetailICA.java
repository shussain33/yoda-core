package com.softcell.gonogo.model.multibureau.experian;

public class RetailICA {

	private String TNOfNDelRetailInACA;
	private String TNOfDelRetailInACA;
	public String getTNOfNDelRetailInACA() {
		return TNOfNDelRetailInACA;
	}
	public void setTNOfNDelRetailInACA(String tNOfNDelRetailInACA) {
		TNOfNDelRetailInACA = tNOfNDelRetailInACA;
	}
	public String getTNOfDelRetailInACA() {
		return TNOfDelRetailInACA;
	}
	public void setTNOfDelRetailInACA(String tNOfDelRetailInACA) {
		TNOfDelRetailInACA = tNOfDelRetailInACA;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("RetailICA{");
		sb.append("TNOfNDelRetailInACA='").append(TNOfNDelRetailInACA).append('\'');
		sb.append(", TNOfDelRetailInACA='").append(TNOfDelRetailInACA).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		RetailICA retailICA = (RetailICA) o;

		if (TNOfNDelRetailInACA != null ? !TNOfNDelRetailInACA.equals(retailICA.TNOfNDelRetailInACA) : retailICA.TNOfNDelRetailInACA != null)
			return false;
		return TNOfDelRetailInACA != null ? TNOfDelRetailInACA.equals(retailICA.TNOfDelRetailInACA) : retailICA.TNOfDelRetailInACA == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfNDelRetailInACA != null ? TNOfNDelRetailInACA.hashCode() : 0;
		result = 31 * result + (TNOfDelRetailInACA != null ? TNOfDelRetailInACA.hashCode() : 0);
		return result;
	}
}
