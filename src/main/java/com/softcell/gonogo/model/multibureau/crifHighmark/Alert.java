package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ALERT")
@XmlAccessorType(XmlAccessType.FIELD)
public class Alert {
	
	@XmlElement(name="ALERT-TYPE")
	private String alertType;
	@XmlElement(name="ALERT-DESC")
	private String alertDescription;
	
	
	public String getAlertType() {
		return alertType;
	}
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}
	public String getAlertDescription() {
		return alertDescription;
	}
	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}
	@Override
	public String toString() {
		return "Alerts [alertType=" + alertType + ", alertDescription="
				+ alertDescription + "]";
	}

}
