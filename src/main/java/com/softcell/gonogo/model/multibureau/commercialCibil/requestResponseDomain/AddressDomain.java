package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddressDomain {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@JsonProperty("01")
	private String addressType;
	
	@JsonProperty("02")
	private String addressResidenceCode;
	
	@JsonProperty("03")
	private String address;
	
	@JsonProperty("04")
	private String addressCity;
	
	@JsonProperty("05")
	private String addressPin;
	
	@JsonProperty("06")
	private String addressState;
	
	@JsonProperty("07")
	private String addressTan;
	
	//START ACE
	@JsonProperty("08")
	private String addressDuns; 
	
	@JsonProperty("09")
	private String address2Type;
	
	@JsonProperty("10")
	private String address2ResidenceCode;
	
	@JsonProperty("11")
	private String address2;
	
	@JsonProperty("12")
	private String address2City;
	
	@JsonProperty("13")
	private String address2Pin;
	
	@JsonProperty("14")
	private String address2State;
	
	@JsonProperty("15")
	private String address2Tan;
	
	@JsonProperty("16")
	private String address2Duns;
	
	@JsonProperty("17")
	private String address3Type;
	
	@JsonProperty("18")
	private String address3ResidenceCode;
	
	@JsonProperty("19")
	private String address3;
	
	@JsonProperty("20")
	private String address3City;
	
	@JsonProperty("21")
	private String address3Pin;
	
	@JsonProperty("22")
	private String address3State;
	
	@JsonProperty("23")
	private String address3Tan; 
	
	@JsonProperty("24")
	private String address3Duns;
	//END ACE
	
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddressResidenceCode() {
		return addressResidenceCode;
	}
	public void setAddressResidenceCode(String addressResidenceCode) {
		this.addressResidenceCode = addressResidenceCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressCity() {
		return addressCity;
	}
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}
	public String getAddressPin() {
		return addressPin;
	}
	public void setAddressPin(String addressPin) {
		this.addressPin = addressPin;
	}
	public String getAddressState() {
		return addressState;
	}
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}
	public String getAddressTan() {
		return addressTan;
	}
	public void setAddressTan(String addressTan) {
		this.addressTan = addressTan;
	}
	public String getAddressDuns() {
		return addressDuns;
	}
	public void setAddressDuns(String addressDuns) {
		this.addressDuns = addressDuns;
	}
	public String getAddress2Duns() {
		return address2Duns;
	}
	public void setAddress2Duns(String address2Duns) {
		this.address2Duns = address2Duns;
	}
	public String getAddress3Duns() {
		return address3Duns;
	}
	public void setAddress3Duns(String address3Duns) {
		this.address3Duns = address3Duns;
	}
	public String getAddress2Type() {
		return address2Type;
	}
	public void setAddress2Type(String address2Type) {
		this.address2Type = address2Type;
	}
	public String getAddress2ResidenceCode() {
		return address2ResidenceCode;
	}
	public void setAddress2ResidenceCode(String address2ResidenceCode) {
		this.address2ResidenceCode = address2ResidenceCode;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress2City() {
		return address2City;
	}
	public void setAddress2City(String address2City) {
		this.address2City = address2City;
	}
	public String getAddress2Pin() {
		return address2Pin;
	}
	public void setAddress2Pin(String address2Pin) {
		this.address2Pin = address2Pin;
	}
	public String getAddress2State() {
		return address2State;
	}
	public void setAddress2State(String address2State) {
		this.address2State = address2State;
	}
	public String getAddress2Tan() {
		return address2Tan;
	}
	public void setAddress2Tan(String address2Tan) {
		this.address2Tan = address2Tan;
	}
	public String getAddress3Type() {
		return address3Type;
	}
	public void setAddress3Type(String address3Type) {
		this.address3Type = address3Type;
	}
	public String getAddress3ResidenceCode() {
		return address3ResidenceCode;
	}
	public void setAddress3ResidenceCode(String address3ResidenceCode) {
		this.address3ResidenceCode = address3ResidenceCode;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getAddress3City() {
		return address3City;
	}
	public void setAddress3City(String address3City) {
		this.address3City = address3City;
	}
	public String getAddress3Pin() {
		return address3Pin;
	}
	public void setAddress3Pin(String address3Pin) {
		this.address3Pin = address3Pin;
	}
	public String getAddress3State() {
		return address3State;
	}
	public void setAddress3State(String address3State) {
		this.address3State = address3State;
	}
	public String getAddress3Tan() {
		return address3Tan;
	}
	public void setAddress3Tan(String address3Tan) {
		this.address3Tan = address3Tan;
	}
	
	@Override
	public String toString() {
		return "AddressDomain [addressType=" + addressType
				+ ", addressResidenceCode=" + addressResidenceCode
				+ ", address=" + address + ", addressCity=" + addressCity
				+ ", addressPin=" + addressPin + ", addressState="
				+ addressState + ", addressTan=" + addressTan
				+ ", addressDuns=" + addressDuns + ", address2Type="
				+ address2Type + ", address2ResidenceCode="
				+ address2ResidenceCode + ", address2=" + address2
				+ ", address2City=" + address2City + ", address2Pin="
				+ address2Pin + ", address2State=" + address2State
				+ ", address2Tan=" + address2Tan + ", address2Duns="
				+ address2Duns + ", address3Type=" + address3Type
				+ ", address3ResidenceCode=" + address3ResidenceCode
				+ ", address3=" + address3 + ", address3City=" + address3City
				+ ", address3Pin=" + address3Pin + ", address3State="
				+ address3State + ", address3Tan=" + address3Tan
				+ ", address3Duns=" + address3Duns + "]";
	}

	
		

	
	
	
}
