package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;



public class IndividualEntity {

	/**
	 * @author namratat
	 *
	 *
	 */
	
	@JsonProperty("01")
	private String individualEntityType;
	
	@JsonProperty("02")
	private Names names;
	
	@JsonProperty("03")
	private String din;
	
	@JsonProperty("04")
	private String gender;
	
	@JsonProperty("05")
	private String maritalStatus;
	
	@JsonProperty("06")
	private Relation relation;
	
	@JsonProperty("07")
	private String age;
	
	@JsonProperty("08")
	private String ageAsOn;
	
	@JsonProperty("09")
	private String dob;

	@JsonProperty("10")
	private int numberOfDependents;

	@JsonProperty("11")
	private List<AddressDomain> addressList;
	
	@JsonProperty("12")
	private IDDomain ids;
	
	@JsonProperty("13")
	private List<PhoneDomain> phoneList;
	
	@JsonProperty("14")
	private String emailId1;
	
	@JsonProperty("15")
	private String emailId2;

	public String getIndividualEntityType() {
		return individualEntityType;
	}

	public void setIndividualEntityType(String individualEntityType) {
		this.individualEntityType = individualEntityType;
	}

	public Names getNames() {
		return names;
	}

	public void setNames(Names names) {
		this.names = names;
	}

	public String getDin() {
		return din;
	}

	public void setDin(String din) {
		this.din = din;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Relation getRelation() {
		return relation;
	}

	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getAgeAsOn() {
		return ageAsOn;
	}

	public void setAgeAsOn(String ageAsOn) {
		this.ageAsOn = ageAsOn;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public List<AddressDomain> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<AddressDomain> addressList) {
		this.addressList = addressList;
	}

	public IDDomain getIds() {
		return ids;
	}

	public void setIds(IDDomain ids) {
		this.ids = ids;
	}

	public List<PhoneDomain> getPhoneList() {
		return phoneList;
	}

	public void setPhoneList(List<PhoneDomain> phoneList) {
		this.phoneList = phoneList;
	}

	public String getEmailId1() {
		return emailId1;
	}

	public void setEmailId1(String emailId1) {
		this.emailId1 = emailId1;
	}

	public String getEmailId2() {
		return emailId2;
	}

	public void setEmailId2(String emailId2) {
		this.emailId2 = emailId2;
	}

	public int getNumberOfDependents() {
		return numberOfDependents;
	}

	public void setNumberOfDependents(int numberOfDependents) {
		this.numberOfDependents = numberOfDependents;
	}

	@Override
	public String toString() {
		return "IndividualEntity [individualEntityType=" + individualEntityType
				+ ", names=" + names + ", din=" + din + ", gender=" + gender
				+ ", maritalStatus=" + maritalStatus + ", relation=" + relation
				+ ", age=" + age + ", ageAsOn=" + ageAsOn + ", dob=" + dob
				+ ", numberOfDependents=" + numberOfDependents
				+ ", addressList=" + addressList + ", ids=" + ids
				+ ", phoneList=" + phoneList + ", emailId1=" + emailId1
				+ ", emailId2=" + emailId2 + "]";
	}

}
