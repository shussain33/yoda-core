package com.softcell.gonogo.model.multibureau.experian;

public class CAPSApplicantDetails {

    private String lastName;
    private String firstname;
    private String middleName1;
    private String middleName2;
    private String middleName3;
    private String genderCode;
    private String incomeTaxPan;
    private String panIssuedate;
    private String panExpirationDate;
    private String passportNumber;
    private String passportissueDate;
    private String passportExpirationDate;
    private String votersidentityCard;
    private String voterIdIssueDate;
    private String voterIdExpirationDate;
    private String driverLincenceNumber;
    private String driverLincenceIssueDate;
    private String driverLincenceExpirationDate;
    private String rationCardNumber;
    private String rationCardIssueDate;
    private String rationCardExpirationDate;
    private String universalIdNumber;
    private String universalIdIssueDate;
    private String universalIdExpirationDate;
    private String dateOfBirthapplicant;
    private String telephoneNumberApplicant1st;
    private String telephonetype;
    private String telephoneExtension;
    private String mobilePhoneNumber;
    private String emailId;


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddleName1() {
        return middleName1;
    }

    public void setMiddleName1(String middleName1) {
        this.middleName1 = middleName1;
    }

    public String getMiddleName2() {
        return middleName2;
    }

    public void setMiddleName2(String middleName2) {
        this.middleName2 = middleName2;
    }

    public String getMiddleName3() {
        return middleName3;
    }

    public void setMiddleName3(String middleName3) {
        this.middleName3 = middleName3;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public String getIncomeTaxPan() {
        return incomeTaxPan;
    }

    public void setIncomeTaxPan(String incomeTaxPan) {
        this.incomeTaxPan = incomeTaxPan;
    }

    public String getPanIssuedate() {
        return panIssuedate;
    }

    public void setPanIssuedate(String panIssuedate) {
        this.panIssuedate = panIssuedate;
    }

    public String getPanExpirationDate() {
        return panExpirationDate;
    }

    public void setPanExpirationDate(String panExpirationDate) {
        this.panExpirationDate = panExpirationDate;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getPassportissueDate() {
        return passportissueDate;
    }

    public void setPassportissueDate(String passportissueDate) {
        this.passportissueDate = passportissueDate;
    }

    public String getPassportExpirationDate() {
        return passportExpirationDate;
    }

    public void setPassportExpirationDate(String passportExpirationDate) {
        this.passportExpirationDate = passportExpirationDate;
    }

    public String getVotersidentityCard() {
        return votersidentityCard;
    }

    public void setVotersidentityCard(String votersidentityCard) {
        this.votersidentityCard = votersidentityCard;
    }

    public String getVoterIdIssueDate() {
        return voterIdIssueDate;
    }

    public void setVoterIdIssueDate(String voterIdIssueDate) {
        this.voterIdIssueDate = voterIdIssueDate;
    }

    public String getVoterIdExpirationDate() {
        return voterIdExpirationDate;
    }

    public void setVoterIdExpirationDate(String voterIdExpirationDate) {
        this.voterIdExpirationDate = voterIdExpirationDate;
    }

    public String getDriverLincenceNumber() {
        return driverLincenceNumber;
    }

    public void setDriverLincenceNumber(String driverLincenceNumber) {
        this.driverLincenceNumber = driverLincenceNumber;
    }

    public String getDriverLincenceIssueDate() {
        return driverLincenceIssueDate;
    }

    public void setDriverLincenceIssueDate(String driverLincenceIssueDate) {
        this.driverLincenceIssueDate = driverLincenceIssueDate;
    }

    public String getDriverLincenceExpirationDate() {
        return driverLincenceExpirationDate;
    }

    public void setDriverLincenceExpirationDate(
            String driverLincenceExpirationDate) {
        this.driverLincenceExpirationDate = driverLincenceExpirationDate;
    }

    public String getRationCardNumber() {
        return rationCardNumber;
    }

    public void setRationCardNumber(String rationCardNumber) {
        this.rationCardNumber = rationCardNumber;
    }

    public String getRationCardIssueDate() {
        return rationCardIssueDate;
    }

    public void setRationCardIssueDate(String rationCardIssueDate) {
        this.rationCardIssueDate = rationCardIssueDate;
    }

    public String getRationCardExpirationDate() {
        return rationCardExpirationDate;
    }

    public void setRationCardExpirationDate(String rationCardExpirationDate) {
        this.rationCardExpirationDate = rationCardExpirationDate;
    }

    public String getUniversalIdNumber() {
        return universalIdNumber;
    }

    public void setUniversalIdNumber(String universalIdNumber) {
        this.universalIdNumber = universalIdNumber;
    }

    public String getUniversalIdIssueDate() {
        return universalIdIssueDate;
    }

    public void setUniversalIdIssueDate(String universalIdIssueDate) {
        this.universalIdIssueDate = universalIdIssueDate;
    }

    public String getUniversalIdExpirationDate() {
        return universalIdExpirationDate;
    }

    public void setUniversalIdExpirationDate(String universalIdExpirationDate) {
        this.universalIdExpirationDate = universalIdExpirationDate;
    }

    public String getDateOfBirthapplicant() {
        return dateOfBirthapplicant;
    }

    public void setDateOfBirthapplicant(String dateOfBirthapplicant) {
        this.dateOfBirthapplicant = dateOfBirthapplicant;
    }

    public String getTelephoneNumberApplicant1st() {
        return telephoneNumberApplicant1st;
    }

    public void setTelephoneNumberApplicant1st(String telephoneNumberApplicant1st) {
        this.telephoneNumberApplicant1st = telephoneNumberApplicant1st;
    }

    public String getTelephonetype() {
        return telephonetype;
    }

    public void setTelephonetype(String telephonetype) {
        this.telephonetype = telephonetype;
    }

    public String getTelephoneExtension() {
        return telephoneExtension;
    }

    public void setTelephoneExtension(String telephoneExtension) {
        this.telephoneExtension = telephoneExtension;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAPSApplicantDetails{");
        sb.append("lastName='").append(lastName).append('\'');
        sb.append(", firstname='").append(firstname).append('\'');
        sb.append(", middleName1='").append(middleName1).append('\'');
        sb.append(", middleName2='").append(middleName2).append('\'');
        sb.append(", middleName3='").append(middleName3).append('\'');
        sb.append(", genderCode='").append(genderCode).append('\'');
        sb.append(", incomeTaxPan='").append(incomeTaxPan).append('\'');
        sb.append(", panIssuedate='").append(panIssuedate).append('\'');
        sb.append(", panExpirationDate='").append(panExpirationDate).append('\'');
        sb.append(", passportNumber='").append(passportNumber).append('\'');
        sb.append(", passportissueDate='").append(passportissueDate).append('\'');
        sb.append(", passportExpirationDate='").append(passportExpirationDate).append('\'');
        sb.append(", votersidentityCard='").append(votersidentityCard).append('\'');
        sb.append(", voterIdIssueDate='").append(voterIdIssueDate).append('\'');
        sb.append(", voterIdExpirationDate='").append(voterIdExpirationDate).append('\'');
        sb.append(", driverLincenceNumber='").append(driverLincenceNumber).append('\'');
        sb.append(", driverLincenceIssueDate='").append(driverLincenceIssueDate).append('\'');
        sb.append(", driverLincenceExpirationDate='").append(driverLincenceExpirationDate).append('\'');
        sb.append(", rationCardNumber='").append(rationCardNumber).append('\'');
        sb.append(", rationCardIssueDate='").append(rationCardIssueDate).append('\'');
        sb.append(", rationCardExpirationDate='").append(rationCardExpirationDate).append('\'');
        sb.append(", universalIdNumber='").append(universalIdNumber).append('\'');
        sb.append(", universalIdIssueDate='").append(universalIdIssueDate).append('\'');
        sb.append(", universalIdExpirationDate='").append(universalIdExpirationDate).append('\'');
        sb.append(", dateOfBirthapplicant='").append(dateOfBirthapplicant).append('\'');
        sb.append(", telephoneNumberApplicant1st='").append(telephoneNumberApplicant1st).append('\'');
        sb.append(", telephonetype='").append(telephonetype).append('\'');
        sb.append(", telephoneExtension='").append(telephoneExtension).append('\'');
        sb.append(", mobilePhoneNumber='").append(mobilePhoneNumber).append('\'');
        sb.append(", emailId='").append(emailId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAPSApplicantDetails that = (CAPSApplicantDetails) o;

        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (firstname != null ? !firstname.equals(that.firstname) : that.firstname != null) return false;
        if (middleName1 != null ? !middleName1.equals(that.middleName1) : that.middleName1 != null) return false;
        if (middleName2 != null ? !middleName2.equals(that.middleName2) : that.middleName2 != null) return false;
        if (middleName3 != null ? !middleName3.equals(that.middleName3) : that.middleName3 != null) return false;
        if (genderCode != null ? !genderCode.equals(that.genderCode) : that.genderCode != null) return false;
        if (incomeTaxPan != null ? !incomeTaxPan.equals(that.incomeTaxPan) : that.incomeTaxPan != null) return false;
        if (panIssuedate != null ? !panIssuedate.equals(that.panIssuedate) : that.panIssuedate != null) return false;
        if (panExpirationDate != null ? !panExpirationDate.equals(that.panExpirationDate) : that.panExpirationDate != null)
            return false;
        if (passportNumber != null ? !passportNumber.equals(that.passportNumber) : that.passportNumber != null)
            return false;
        if (passportissueDate != null ? !passportissueDate.equals(that.passportissueDate) : that.passportissueDate != null)
            return false;
        if (passportExpirationDate != null ? !passportExpirationDate.equals(that.passportExpirationDate) : that.passportExpirationDate != null)
            return false;
        if (votersidentityCard != null ? !votersidentityCard.equals(that.votersidentityCard) : that.votersidentityCard != null)
            return false;
        if (voterIdIssueDate != null ? !voterIdIssueDate.equals(that.voterIdIssueDate) : that.voterIdIssueDate != null)
            return false;
        if (voterIdExpirationDate != null ? !voterIdExpirationDate.equals(that.voterIdExpirationDate) : that.voterIdExpirationDate != null)
            return false;
        if (driverLincenceNumber != null ? !driverLincenceNumber.equals(that.driverLincenceNumber) : that.driverLincenceNumber != null)
            return false;
        if (driverLincenceIssueDate != null ? !driverLincenceIssueDate.equals(that.driverLincenceIssueDate) : that.driverLincenceIssueDate != null)
            return false;
        if (driverLincenceExpirationDate != null ? !driverLincenceExpirationDate.equals(that.driverLincenceExpirationDate) : that.driverLincenceExpirationDate != null)
            return false;
        if (rationCardNumber != null ? !rationCardNumber.equals(that.rationCardNumber) : that.rationCardNumber != null)
            return false;
        if (rationCardIssueDate != null ? !rationCardIssueDate.equals(that.rationCardIssueDate) : that.rationCardIssueDate != null)
            return false;
        if (rationCardExpirationDate != null ? !rationCardExpirationDate.equals(that.rationCardExpirationDate) : that.rationCardExpirationDate != null)
            return false;
        if (universalIdNumber != null ? !universalIdNumber.equals(that.universalIdNumber) : that.universalIdNumber != null)
            return false;
        if (universalIdIssueDate != null ? !universalIdIssueDate.equals(that.universalIdIssueDate) : that.universalIdIssueDate != null)
            return false;
        if (universalIdExpirationDate != null ? !universalIdExpirationDate.equals(that.universalIdExpirationDate) : that.universalIdExpirationDate != null)
            return false;
        if (dateOfBirthapplicant != null ? !dateOfBirthapplicant.equals(that.dateOfBirthapplicant) : that.dateOfBirthapplicant != null)
            return false;
        if (telephoneNumberApplicant1st != null ? !telephoneNumberApplicant1st.equals(that.telephoneNumberApplicant1st) : that.telephoneNumberApplicant1st != null)
            return false;
        if (telephonetype != null ? !telephonetype.equals(that.telephonetype) : that.telephonetype != null)
            return false;
        if (telephoneExtension != null ? !telephoneExtension.equals(that.telephoneExtension) : that.telephoneExtension != null)
            return false;
        if (mobilePhoneNumber != null ? !mobilePhoneNumber.equals(that.mobilePhoneNumber) : that.mobilePhoneNumber != null)
            return false;
        return emailId != null ? emailId.equals(that.emailId) : that.emailId == null;
    }

    @Override
    public int hashCode() {
        int result = lastName != null ? lastName.hashCode() : 0;
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (middleName1 != null ? middleName1.hashCode() : 0);
        result = 31 * result + (middleName2 != null ? middleName2.hashCode() : 0);
        result = 31 * result + (middleName3 != null ? middleName3.hashCode() : 0);
        result = 31 * result + (genderCode != null ? genderCode.hashCode() : 0);
        result = 31 * result + (incomeTaxPan != null ? incomeTaxPan.hashCode() : 0);
        result = 31 * result + (panIssuedate != null ? panIssuedate.hashCode() : 0);
        result = 31 * result + (panExpirationDate != null ? panExpirationDate.hashCode() : 0);
        result = 31 * result + (passportNumber != null ? passportNumber.hashCode() : 0);
        result = 31 * result + (passportissueDate != null ? passportissueDate.hashCode() : 0);
        result = 31 * result + (passportExpirationDate != null ? passportExpirationDate.hashCode() : 0);
        result = 31 * result + (votersidentityCard != null ? votersidentityCard.hashCode() : 0);
        result = 31 * result + (voterIdIssueDate != null ? voterIdIssueDate.hashCode() : 0);
        result = 31 * result + (voterIdExpirationDate != null ? voterIdExpirationDate.hashCode() : 0);
        result = 31 * result + (driverLincenceNumber != null ? driverLincenceNumber.hashCode() : 0);
        result = 31 * result + (driverLincenceIssueDate != null ? driverLincenceIssueDate.hashCode() : 0);
        result = 31 * result + (driverLincenceExpirationDate != null ? driverLincenceExpirationDate.hashCode() : 0);
        result = 31 * result + (rationCardNumber != null ? rationCardNumber.hashCode() : 0);
        result = 31 * result + (rationCardIssueDate != null ? rationCardIssueDate.hashCode() : 0);
        result = 31 * result + (rationCardExpirationDate != null ? rationCardExpirationDate.hashCode() : 0);
        result = 31 * result + (universalIdNumber != null ? universalIdNumber.hashCode() : 0);
        result = 31 * result + (universalIdIssueDate != null ? universalIdIssueDate.hashCode() : 0);
        result = 31 * result + (universalIdExpirationDate != null ? universalIdExpirationDate.hashCode() : 0);
        result = 31 * result + (dateOfBirthapplicant != null ? dateOfBirthapplicant.hashCode() : 0);
        result = 31 * result + (telephoneNumberApplicant1st != null ? telephoneNumberApplicant1st.hashCode() : 0);
        result = 31 * result + (telephonetype != null ? telephonetype.hashCode() : 0);
        result = 31 * result + (telephoneExtension != null ? telephoneExtension.hashCode() : 0);
        result = 31 * result + (mobilePhoneNumber != null ? mobilePhoneNumber.hashCode() : 0);
        result = 31 * result + (emailId != null ? emailId.hashCode() : 0);
        return result;
    }
}
