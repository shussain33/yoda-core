package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Summary {

	/**
	 * @author Akshata
	 *
	 *
	 */
	
	@XmlElement(name="STATUS")
	private String status;
	@XmlElement(name="NO-OF-DEFAULT-ACCOUNTS")
	private String noOfDefaultAccounts;
	@XmlElement(name="TOTAL-RESPONSES")
	private String totalResponses;
	@XmlElement(name="NO-OF-CLOSED-ACCOUNTS")
	private String noOfClosedAccounts;
	@XmlElement(name="NO-OF-ACTIVE-ACCOUNTS")
	private String noOfActiveAccounts;
	@XmlElement(name="NO-OF-OTHER-MFIS")
	private String noOfOtherMfis;
	@XmlElement(name="NO-OF-OWN-MFIS")
	private String noOfOwnMfi;
	@XmlElement(name="OWN-MFI-INDECATOR")
	private String ownMfiIndecator;
	@XmlElement(name="TOTAL-OWN-DISBURSED-AMOUNT")
	private String totalOwnDisbursedAmount;
	@XmlElement(name="TOTAL-OTHER-DISBURSED-AMOUNT")
	private String totalOtherDisbursedAmount;
	@XmlElement(name="TOTAL-OWN-CURRENT-BALANCE")
	private String totalOwnCurrentBalance;
	@XmlElement(name="TOTAL-OTHER-CURRENT-BALANCE")
	private String totalOtherCurrentBalance;
	@XmlElement(name="TOTAL-OWN-INSTALLMENT-AMOUNT")
	private String totalOwnInstallmentAmount;
	@XmlElement(name="TOTAL-OTHER-INSTALLMENT-AMOUNT")
	private String totalOtherInstallmentAmount;
	@XmlElement(name="MAX-WORST-DELEQUENCY")
	private String maxWorstDelequency;
	@XmlElement(name="ERRORS")
	private String errors;
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getNoOfDefaultAccounts() {
		return noOfDefaultAccounts;
	}
	
	public void setNoOfDefaultAccounts(String noOfDefaultAccounts) {
		this.noOfDefaultAccounts = noOfDefaultAccounts;
	}
	
	public String getTotalResponses() {
		return totalResponses;
	}
	
	public void setTotalResponses(String totalResponses) {
		this.totalResponses = totalResponses;
	}
	
	public String getNoOfClosedAccounts() {
		return noOfClosedAccounts;
	}
	
	public void setNoOfClosedAccounts(String noOfClosedAccounts) {
		this.noOfClosedAccounts = noOfClosedAccounts;
	}
	
	public String getNoOfActiveAccounts() {
		return noOfActiveAccounts;
	}
	
	public void setNoOfActiveAccounts(String noOfActiveAccounts) {
		this.noOfActiveAccounts = noOfActiveAccounts;
	}
	
	public String getNoOfOtherMfis() {
		return noOfOtherMfis;
	}
	
	public void setNoOfOtherMfis(String noOfOtherMfis) {
		this.noOfOtherMfis = noOfOtherMfis;
	}
	
	public String getNoOfOwnMfi() {
		return noOfOwnMfi;
	}
	
	public void setNoOfOwnMfi(String noOfOwnMfi) {
		this.noOfOwnMfi = noOfOwnMfi;
	}
	
	public String getOwnMfiIndecator() {
		return ownMfiIndecator;
	}
	
	public void setOwnMfiIndecator(String ownMfiIndicator) {
		this.ownMfiIndecator = ownMfiIndecator;
	}
	
	public String getTotalOwnDisbursedAmount() {
		return totalOwnDisbursedAmount;
	}
	
	public void setTotalOwnDisbursedAmount(String totalOwnDisbursedAmount) {
		this.totalOwnDisbursedAmount = totalOwnDisbursedAmount;
	}
	
	public String getTotalOtherDisbursedAmount() {
		return totalOtherDisbursedAmount;
	}
	
	public void setTotalOtherDisbursedAmount(String totalOtherDisbursedAmount) {
		this.totalOtherDisbursedAmount = totalOtherDisbursedAmount;
	}
	
	public String getTotalOwnCurrentBalance() {
		return totalOwnCurrentBalance;
	}
	
	public void setTotalOwnCurrentBalance(String totalOwnCurrentBalance) {
		this.totalOwnCurrentBalance = totalOwnCurrentBalance;
	}
	
	public String getTotalOtherCurrentBalance() {
		return totalOtherCurrentBalance;
	}
	
	public void setTotalOtherCurrentBalance(String totalOtherCurrentBalance) {
		this.totalOtherCurrentBalance = totalOtherCurrentBalance;
	}
	
	public String getTotalOwnInstallmentAmount() {
		return totalOwnInstallmentAmount;
	}
	
	public void setTotalOwnInstallmentAmount(String totalOwnInstallmentAmount) {
		this.totalOwnInstallmentAmount = totalOwnInstallmentAmount;
	}
	
	public String getTotalOtherInstallmentAmount() {
		return totalOtherInstallmentAmount;
	}
	
	public void setTotalOtherInstallmentAmount(String totalOtherInstallmentAmount) {
		this.totalOtherInstallmentAmount = totalOtherInstallmentAmount;
	}
	
	public String getMaxWorstDelequency() {
		return maxWorstDelequency;
	}
	
	public void setMaxWorstDelequency(String maxWorstDelequency) {
		this.maxWorstDelequency = maxWorstDelequency;
	}
	
	public String getErrors() {
		return errors;
	}
	
	public void setErrors(String errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "Summary [status=" + status + ", noOfDefaultAccounts="
				+ noOfDefaultAccounts + ", totalResponses=" + totalResponses
				+ ", noOfClosedAccounts=" + noOfClosedAccounts
				+ ", noOfActiveAccounts=" + noOfActiveAccounts
				+ ", noOfOtherMfis=" + noOfOtherMfis + ", noOfOwnMfi="
				+ noOfOwnMfi + ", ownMfiIndecator=" + ownMfiIndecator
				+ ", totalOwnDisbursedAmount=" + totalOwnDisbursedAmount
				+ ", totalOtherDisbursedAmount=" + totalOtherDisbursedAmount
				+ ", totalOwnCurrentBalance=" + totalOwnCurrentBalance
				+ ", totalOtherCurrentBalance=" + totalOtherCurrentBalance
				+ ", totalOwnInstallmentAmount=" + totalOwnInstallmentAmount
				+ ", totalOtherInstallmentAmount=" + totalOtherInstallmentAmount
				+ ", maxWorstDelequency=" + maxWorstDelequency + ", errors="
				+ errors + "]";
	}
	
}
