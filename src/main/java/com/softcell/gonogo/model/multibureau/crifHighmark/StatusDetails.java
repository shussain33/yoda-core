package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="STATUS-DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusDetails {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="STATUS")
	private List<Status> status;

	public List<Status> getStatus() {
		return status;
	}
	

	public void setStatus(List<Status> status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return "StatusDetails [status=" + status + "]";
	}
	
	
	
	
}
