package com.softcell.gonogo.model.multibureau.commercialCibil.pickup;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OutputAckDomain {

	@JsonProperty("HEADER")
	OutputHeader outputHeader;
	
	@JsonProperty("ACKNOWLEDGEMENT-ID")
	private Long acknowledgementId;
	
	@JsonProperty("STATUS")
	private String status;
	
	@JsonProperty("WARNINGS")
	List<ErrorWarning> warnings;
	
	@JsonProperty("ERRORS")
	List<ErrorWarning> errors;
	
	@JsonProperty("SEARCH-LIST")
	List<CommercialSearch> searchList;
	
	@JsonProperty("SRCH-LIST-ITEMS")
	OrderSearchItem  orderSearchItem;

	@JsonProperty("MERGED_RESPONSE_OBJECT")
	private Object mergedResponse;

	@JsonProperty("FINISHED")
	private List<FinishedInprocessObject> finishedList;
	
	@JsonProperty("IN-PROCESS")
	private List<FinishedInprocessObject> inprocessList;
	
	@JsonProperty("REJECT")
	private List<FinishedInprocessObject> rejectList;
	
	public List<FinishedInprocessObject> getRejectList() {
		return rejectList;
	}
	public void setRejectList(List<FinishedInprocessObject> rejectList) {
		this.rejectList = rejectList;
	}
	public List<FinishedInprocessObject> getFinishedList() {
		return finishedList;
	}
	public void setFinishedList(List<FinishedInprocessObject> finishedList) {
		this.finishedList = finishedList;
	}
	public List<FinishedInprocessObject> getInprocessList() {
		return inprocessList;
	}
	public void setInprocessList(List<FinishedInprocessObject> inprocessList) {
		this.inprocessList = inprocessList;
	}
	public OrderSearchItem getOrderSearchItem() {
		return orderSearchItem;
	}
	public void setOrderSearchItem(OrderSearchItem orderSearchItem) {
		this.orderSearchItem = orderSearchItem;
	}
	public List<CommercialSearch> getSearchList() {
		return searchList;
	}
	public void setSearchList(List<CommercialSearch> searchList) {
		this.searchList = searchList;
	}
	public OutputHeader getOutputHeader() {
		return outputHeader;
	}
	public void setOutputHeader(OutputHeader outputHeader) {
		this.outputHeader = outputHeader;
	}
	public Long getAcknowledgementId() {
		return acknowledgementId;
	}
	public void setAcknowledgementId(Long acknowledgementId) {
		this.acknowledgementId = acknowledgementId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<ErrorWarning> getWarnings() {
		return warnings;
	}
	public void setWarnings(List<ErrorWarning> warnings) {
		this.warnings = warnings;
	}
	public List<ErrorWarning> getErrors() {
		return errors;
	}
	public void setErrors(List<ErrorWarning> errors) {
		this.errors = errors;
	}

	public Object getMergedResponse() {
		return mergedResponse;
	}

	public void setMergedResponse(Object mergedResponse) {
		this.mergedResponse = mergedResponse;
	}

	@Override
	public String toString() {
		return "OutputAckDomain [outputHeader=" + outputHeader
				+ ", acknowledgementId=" + acknowledgementId + ", status="
				+ status + ", warnings=" + warnings + ", errors=" + errors
				+ ", searchList=" + searchList + ", orderSearchItem="
				+ orderSearchItem + ", finishedList=" + finishedList
				+ ", inprocessList=" + inprocessList + ", mergedResponse=" + mergedResponse+"]";
	}
}
