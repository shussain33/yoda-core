package com.softcell.gonogo.model.multibureau.experian;

public class MFCAD {

	private String TNOfMFCAD;
	private String TotValOfMFCAD;
	private String MNTSMRMFCAD;
	public String getTNOfMFCAD() {
		return TNOfMFCAD;
	}
	public void setTNOfMFCAD(String tNOfMFCAD) {
		TNOfMFCAD = tNOfMFCAD;
	}
	public String getTotValOfMFCAD() {
		return TotValOfMFCAD;
	}
	public void setTotValOfMFCAD(String totValOfMFCAD) {
		TotValOfMFCAD = totValOfMFCAD;
	}
	public String getMNTSMRMFCAD() {
		return MNTSMRMFCAD;
	}
	public void setMNTSMRMFCAD(String mNTSMRMFCAD) {
		MNTSMRMFCAD = mNTSMRMFCAD;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MFCAD{");
		sb.append("TNOfMFCAD='").append(TNOfMFCAD).append('\'');
		sb.append(", TotValOfMFCAD='").append(TotValOfMFCAD).append('\'');
		sb.append(", MNTSMRMFCAD='").append(MNTSMRMFCAD).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MFCAD mfcad = (MFCAD) o;

		if (TNOfMFCAD != null ? !TNOfMFCAD.equals(mfcad.TNOfMFCAD) : mfcad.TNOfMFCAD != null) return false;
		if (TotValOfMFCAD != null ? !TotValOfMFCAD.equals(mfcad.TotValOfMFCAD) : mfcad.TotValOfMFCAD != null)
			return false;
		return MNTSMRMFCAD != null ? MNTSMRMFCAD.equals(mfcad.MNTSMRMFCAD) : mfcad.MNTSMRMFCAD == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfMFCAD != null ? TNOfMFCAD.hashCode() : 0;
		result = 31 * result + (TotValOfMFCAD != null ? TotValOfMFCAD.hashCode() : 0);
		result = 31 * result + (MNTSMRMFCAD != null ? MNTSMRMFCAD.hashCode() : 0);
		return result;
	}
}
