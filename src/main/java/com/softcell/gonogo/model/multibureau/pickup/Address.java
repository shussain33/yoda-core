package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Address {

    @JsonProperty("01")
    private String addressType1;

    @JsonProperty("02")
    private String residenceCode;

    @JsonProperty("03")
    private String address;

    @JsonProperty("04")
    private String city;

    @JsonProperty("05")
    private String addressPin;

    @JsonProperty("06")
    private String addressState;

    public String getAddressType1() {
        return addressType1;
    }

    public void setAddressType1(String addressType1) {
        this.addressType1 = addressType1;
    }

    public String getResidenceCode() {
        return residenceCode;
    }

    public void setResidenceCode(String residenceCode) {
        this.residenceCode = residenceCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressPin() {
        return addressPin;
    }

    public void setAddressPin(String addressPin) {
        this.addressPin = addressPin;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Address{");
        sb.append("addressType1='").append(addressType1).append('\'');
        sb.append(", residenceCode='").append(residenceCode).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", addressPin='").append(addressPin).append('\'');
        sb.append(", addressState='").append(addressState).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address1 = (Address) o;
        return Objects.equal(getAddressType1(), address1.getAddressType1()) &&
                Objects.equal(getResidenceCode(), address1.getResidenceCode()) &&
                Objects.equal(getAddress(), address1.getAddress()) &&
                Objects.equal(getCity(), address1.getCity()) &&
                Objects.equal(getAddressPin(), address1.getAddressPin()) &&
                Objects.equal(getAddressState(), address1.getAddressState());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getAddressType1(), getResidenceCode(), getAddress(), getCity(), getAddressPin(), getAddressState());
    }

    public static  Builder builder(){
        return new Builder();
    }
    public static class Builder{
        private Address addressObj = new Address();

        public Address build(){
            return this.addressObj ;
        }
        public Builder addressType1(String addressType1){
            this.addressObj.addressType1=addressType1;
            return this;
        }

        public Builder residenceCode(String residenceCode){
            this.addressObj.residenceCode=residenceCode;
            return this;
        }

        public Builder address(String address){
            this.addressObj.address= address;
            return this;
        }

        public Builder city(String city){
            this.addressObj.city =city;
            return this;
        }

        public Builder addressPin(String addressPin){
            this.addressObj.addressPin =addressPin;
            return this;
        }

        public Builder addressState(String addressState){
            this.addressObj.addressState=addressState;
            return this;
        }
    }
}
