package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="IDS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Ids {

	/**
	 * @author Akshata
	 *
	 *
	 */
	
	@XmlElement(name="ID")
	private List<Id> id;

	public List<Id> getId() {
		return id;
	}
	
	public void setId(List<Id> id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Ids [id=" + id + "]";
	}
	
}
