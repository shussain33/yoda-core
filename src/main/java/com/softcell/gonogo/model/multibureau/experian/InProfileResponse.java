package com.softcell.gonogo.model.multibureau.experian;

public class InProfileResponse {
	private Header header;
	private UserMessage userMessage;
	private CreditProfileHeader creditProfileHeader;
	private CurrentApplication currentApplication;
	private CAISAccount caisAccount;
	private MatchResult matchResult;
	private CAPS caps;
	private NonCreditCAPS nonCreditCAPS;
	private Score score;
	private PSV psv;
	private TotalCAPSSummary totalCAPSSummary;

	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public UserMessage getUserMessage() {
		return userMessage;
	}
	public void setUserMessage(UserMessage userMessage) {
		this.userMessage = userMessage;
	}
	public CreditProfileHeader getCreditProfileHeader() {
		return creditProfileHeader;
	}
	public void setCreditProfileHeader(CreditProfileHeader creditProfileHeader) {
		this.creditProfileHeader = creditProfileHeader;
	}
	public CurrentApplication getCurrentApplication() {
		return currentApplication;
	}
	public void setCurrentApplication(CurrentApplication currentApplication) {
		this.currentApplication = currentApplication;
	}
	public CAISAccount getCaisAccount() {
		return caisAccount;
	}
	public void setCaisAccount(CAISAccount caisAccount) {
		this.caisAccount = caisAccount;
	}
	public MatchResult getMatchResult() {
		return matchResult;
	}
	public void setMatchResult(MatchResult matchResult) {
		this.matchResult = matchResult;
	}
	public CAPS getCaps() {
		return caps;
	}
	public void setCaps(CAPS caps) {
		this.caps = caps;
	}
	public Score getScore() {
		return score;
	}
	public void setScore(Score score) {
		this.score = score;
	}
	public PSV getPsv() {
		return psv;
	}
	public void setPsv(PSV psv) {
		this.psv = psv;
	}
	public TotalCAPSSummary getTotalCAPSSummary() {
		return totalCAPSSummary;
	}
	public void setTotalCAPSSummary(TotalCAPSSummary totalCAPSSummary) {
		this.totalCAPSSummary = totalCAPSSummary;
	}
	public NonCreditCAPS getNonCreditCAPS() {
		return nonCreditCAPS;
	}
	public void setNonCreditCAPS(NonCreditCAPS nonCreditCAPS) {
		this.nonCreditCAPS = nonCreditCAPS;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("InProfileResponse{");
		sb.append("header=").append(header);
		sb.append(", userMessage=").append(userMessage);
		sb.append(", creditProfileHeader=").append(creditProfileHeader);
		sb.append(", currentApplication=").append(currentApplication);
		sb.append(", caisAccount=").append(caisAccount);
		sb.append(", matchResult=").append(matchResult);
		sb.append(", caps=").append(caps);
		sb.append(", nonCreditCAPS=").append(nonCreditCAPS);
		sb.append(", score=").append(score);
		sb.append(", psv=").append(psv);
		sb.append(", totalCAPSSummary=").append(totalCAPSSummary);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		InProfileResponse that = (InProfileResponse) o;

		if (header != null ? !header.equals(that.header) : that.header != null) return false;
		if (userMessage != null ? !userMessage.equals(that.userMessage) : that.userMessage != null) return false;
		if (creditProfileHeader != null ? !creditProfileHeader.equals(that.creditProfileHeader) : that.creditProfileHeader != null)
			return false;
		if (currentApplication != null ? !currentApplication.equals(that.currentApplication) : that.currentApplication != null)
			return false;
		if (caisAccount != null ? !caisAccount.equals(that.caisAccount) : that.caisAccount != null) return false;
		if (matchResult != null ? !matchResult.equals(that.matchResult) : that.matchResult != null) return false;
		if (caps != null ? !caps.equals(that.caps) : that.caps != null) return false;
		if (nonCreditCAPS != null ? !nonCreditCAPS.equals(that.nonCreditCAPS) : that.nonCreditCAPS != null)
			return false;
		if (score != null ? !score.equals(that.score) : that.score != null) return false;
		if (psv != null ? !psv.equals(that.psv) : that.psv != null) return false;
		return totalCAPSSummary != null ? totalCAPSSummary.equals(that.totalCAPSSummary) : that.totalCAPSSummary == null;
	}

	@Override
	public int hashCode() {
		int result = header != null ? header.hashCode() : 0;
		result = 31 * result + (userMessage != null ? userMessage.hashCode() : 0);
		result = 31 * result + (creditProfileHeader != null ? creditProfileHeader.hashCode() : 0);
		result = 31 * result + (currentApplication != null ? currentApplication.hashCode() : 0);
		result = 31 * result + (caisAccount != null ? caisAccount.hashCode() : 0);
		result = 31 * result + (matchResult != null ? matchResult.hashCode() : 0);
		result = 31 * result + (caps != null ? caps.hashCode() : 0);
		result = 31 * result + (nonCreditCAPS != null ? nonCreditCAPS.hashCode() : 0);
		result = 31 * result + (score != null ? score.hashCode() : 0);
		result = 31 * result + (psv != null ? psv.hashCode() : 0);
		result = 31 * result + (totalCAPSSummary != null ? totalCAPSSummary.hashCode() : 0);
		return result;
	}
}
