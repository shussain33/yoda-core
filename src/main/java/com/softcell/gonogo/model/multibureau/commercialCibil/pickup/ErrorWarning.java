package com.softcell.gonogo.model.multibureau.commercialCibil.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorWarning {

	@JsonProperty("code")
	private String code;
	
	@JsonProperty("description")
	private String description;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "ErrorWarning [code=" + code + ", description=" + description
				+ "]";
	}
}
