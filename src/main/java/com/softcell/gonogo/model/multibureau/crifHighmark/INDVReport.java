package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INDVReport")
@XmlAccessorType(XmlAccessType.FIELD)
public class INDVReport {

	@XmlElement(name="NameVariations")
	private NameVariations nameVariations;
	@XmlElement(name="HEADER")
	private Header header;
	@XmlElement(name="REQUEST")
	private Request request;
	@XmlElement(name="PERSONAL-INFO-VARIATION")
	private PersonalInfoVariation personalInfoVariation;
	@XmlElement(name="ACCOUNTS-SUMMARY")
	private AccountSummary accountSummary;
	@XmlElement(name="RESPONSES")
	private Responses responses;
	@XmlElement(name="SCORES")
	private Scores scores;
	@XmlElement(name="INQUIRY-HISTORY")
	private InquiryHistory inquiryHistory;
	@XmlElement(name="COMMENTS")
	private Comments comments;
	@XmlElement(name="ALERTS")
	private Alerts alerts;
	@XmlElement(name="SECONDARY-MATCHES")
	private SecondaryMatches secondaryMatches;
	@XmlElement(name="PRINTABLE-REPORT")
	private PrintableReport printableReport;
	@XmlElement(name="INDV-RESPONSES")
	private IndvResponses indvResponses;
	@XmlElement(name="GRP-RESPONSES")
	private GrpResponses grpResponses;
	@XmlElement(name="IDS")
	private Ids ids;
	@XmlElement(name="PHONES")
	private Phones phones;
	@XmlElement(name="ADDRESS")
	private Addresses addresses;
//	private Relations relations;
//	private Emails emails;
	@XmlElement(name="STATUS-DETAILS")
	private StatusDetails statusDetails;
	@XmlElement(name="DERIVED-ATTRIBUTES")
	private List<DerivedAtttibute> derivedAtttibuteList;
	
	
	public List<DerivedAtttibute> getDerivedAtttibuteList() {
		return derivedAtttibuteList;
	}

	public void setDerivedAtttibuteList(List<DerivedAtttibute> derivedAtttibuteList) {
		this.derivedAtttibuteList = derivedAtttibuteList;
	}

	public GrpResponses getGrpResponses() {
		return grpResponses;
	}

	public void setGrpResponses(GrpResponses grpResponses) {
		this.grpResponses = grpResponses;
	}
	
	public StatusDetails getStatusDetails() {
		return statusDetails;
	}

	public void setStatusDetails(StatusDetails statusDetails) {
		this.statusDetails = statusDetails;
	}
/*
	public Emails getEmails() {
		return emails;
	}

	public void setEmails(Emails emails) {
		this.emails = emails;
	}
	
	public Relations getRelations() {
		return relations;
	}
	
	public void setRelations(Relations relations) {
		this.relations = relations;
	}
*/	
	public Phones getPhones() {
		return phones;
	}
	
	public void setPhones(Phones phones) {
		this.phones = phones;
	}
	public Ids getIds() {
		return ids;
	}

	public void setIds(Ids ids) {
		this.ids = ids;
	}

	public Addresses getAddresses() {
		return addresses;
	}
	
	public void setAddresses(Addresses addresses) {
		this.addresses = addresses;
	}
	public NameVariations getNameVariations() {
		return nameVariations;
	}
	
	public void setNameVariations(NameVariations nameVariations) {
		this.nameVariations = nameVariations;
	}
	
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
	public PersonalInfoVariation getPersonalInfoVariation() {
		return personalInfoVariation;
	}
	public void setPersonalInfoVariation(PersonalInfoVariation personalInfoVariation) {
		this.personalInfoVariation = personalInfoVariation;
	}
	public AccountSummary getAccountSummary() {
		return accountSummary;
	}
	public void setAccountSummary(AccountSummary accountSummary) {
		this.accountSummary = accountSummary;
	}
	public Responses getResponses() {
		return responses;
	}
	public void setResponses(Responses responses) {
		this.responses = responses;
	}
	public Scores getScores() {
		return scores;
	}
	public void setScores(Scores scores) {
		this.scores = scores;
	}
	public InquiryHistory getInquiryHistory() {
		return inquiryHistory;
	}
	public void setInquiryHistory(InquiryHistory inquiryHistory) {
		this.inquiryHistory = inquiryHistory;
	}
	public Comments getComments() {
		return comments;
	}
	public void setComments(Comments comments) {
		this.comments = comments;
	}
	public Alerts getAlerts() {
		return alerts;
	}
	public void setAlerts(Alerts alerts) {
		this.alerts = alerts;
	}
	public SecondaryMatches getSecondaryMatches() {
		return secondaryMatches;
	}
	public void setSecondaryMatches(SecondaryMatches secondaryMatches) {
		this.secondaryMatches = secondaryMatches;
	}
	public PrintableReport getPrintableReport() {
		return printableReport;
	}
	public void setPrintableReport(PrintableReport printableReport) {
		this.printableReport = printableReport;
	}
	
	public InquiryStatus getInquiryStatus() {
		// TODO Auto-generated method stub
		return null;
	}
	public INDVReports getINDVReports() {
		// TODO Auto-generated method stub
		return null;
	}
	public IndvResponses getIndvResponses() {
		return indvResponses;
	}
	
	public void setIndvResponses(IndvResponses indvResponses) {
		this.indvResponses = indvResponses;
	}
	@Override
	public String toString() {
		return "INDVReport [nameVariations=" + nameVariations + ", header="
				+ header + ", request=" + request + ", personalInfoVariation="
				+ personalInfoVariation + ", accountSummary=" + accountSummary
				+ ", responses=" + responses + ", scores=" + scores
				+ ", inquiryHistory=" + inquiryHistory + ", comments="
				+ comments + ", alerts=" + alerts + ", secondaryMatches="
				+ secondaryMatches + ", printableReport=" + printableReport
				+ ", indvResponses=" + indvResponses + ", grpResponses="
				+ grpResponses + ", ids=" + ids + ", phones=" + phones
				+ ", addresses=" + addresses + ", statusDetails="
				+ statusDetails + ", derivedAtttibuteList="
				+ derivedAtttibuteList + "]";
	}
	
}
