package com.softcell.gonogo.model.multibureau.experian;

public class TelcosCAD {

    private String TNOfTelcosCAD;
    private String TotValOfTelcosCAD;
    private String MNTSMRTelcosCAD;

    public String getTNOfTelcosCAD() {
        return TNOfTelcosCAD;
    }

    public void setTNOfTelcosCAD(String tNOfTelcosCAD) {
        TNOfTelcosCAD = tNOfTelcosCAD;
    }

    public String getTotValOfTelcosCAD() {
        return TotValOfTelcosCAD;
    }

    public void setTotValOfTelcosCAD(String totValOfTelcosCAD) {
        TotValOfTelcosCAD = totValOfTelcosCAD;
    }

    public String getMNTSMRTelcosCAD() {
        return MNTSMRTelcosCAD;
    }

    public void setMNTSMRTelcosCAD(String mNTSMRTelcosCAD) {
        MNTSMRTelcosCAD = mNTSMRTelcosCAD;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TelcosCAD{");
        sb.append("TNOfTelcosCAD='").append(TNOfTelcosCAD).append('\'');
        sb.append(", TotValOfTelcosCAD='").append(TotValOfTelcosCAD).append('\'');
        sb.append(", MNTSMRTelcosCAD='").append(MNTSMRTelcosCAD).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TelcosCAD telcosCAD = (TelcosCAD) o;

        if (TNOfTelcosCAD != null ? !TNOfTelcosCAD.equals(telcosCAD.TNOfTelcosCAD) : telcosCAD.TNOfTelcosCAD != null)
            return false;
        if (TotValOfTelcosCAD != null ? !TotValOfTelcosCAD.equals(telcosCAD.TotValOfTelcosCAD) : telcosCAD.TotValOfTelcosCAD != null)
            return false;
        return MNTSMRTelcosCAD != null ? MNTSMRTelcosCAD.equals(telcosCAD.MNTSMRTelcosCAD) : telcosCAD.MNTSMRTelcosCAD == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfTelcosCAD != null ? TNOfTelcosCAD.hashCode() : 0;
        result = 31 * result + (TotValOfTelcosCAD != null ? TotValOfTelcosCAD.hashCode() : 0);
        result = 31 * result + (MNTSMRTelcosCAD != null ? MNTSMRTelcosCAD.hashCode() : 0);
        return result;
    }
}
