package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IndividualEntitiesSegment {

	/**
	 * @author namratat
	 *
	 *
	 */
	@JsonProperty("01")
	List<IndividualEntity> individualEntityList;

	public List<IndividualEntity> getIndividualEntityList() {
		return individualEntityList;
	}

	public void setIndividualEntityList(List<IndividualEntity> individualEntityList) {
		this.individualEntityList = individualEntityList;
	}

	@Override
	public String toString() {
		return "IndividualEntitiesSegment [individualEntityList="
				+ individualEntityList + "]";
	}
	
}
