package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="EMAIL")
@XmlAccessorType(XmlAccessType.FIELD)
public class Email {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="EMAIL")
	private String email;

	public String getEmail() {
		return email;
	}
	

	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return "Email [email=" + email + "]";
	}
	
}
