package com.softcell.gonogo.model.multibureau.experian;

public class TotalCAD {

    private String TNOfAllCAD;
    private String TotValOfAllCAD;
    private String MNTSMRCADAll;

    public String getTNOfAllCAD() {
        return TNOfAllCAD;
    }

    public void setTNOfAllCAD(String tNOfAllCAD) {
        TNOfAllCAD = tNOfAllCAD;
    }

    public String getTotValOfAllCAD() {
        return TotValOfAllCAD;
    }

    public void setTotValOfAllCAD(String totValOfAllCAD) {
        TotValOfAllCAD = totValOfAllCAD;
    }

    public String getMNTSMRCADAll() {
        return MNTSMRCADAll;
    }

    public void setMNTSMRCADAll(String mNTSMRCADAll) {
        MNTSMRCADAll = mNTSMRCADAll;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TotalCAD{");
        sb.append("TNOfAllCAD='").append(TNOfAllCAD).append('\'');
        sb.append(", TotValOfAllCAD='").append(TotValOfAllCAD).append('\'');
        sb.append(", MNTSMRCADAll='").append(MNTSMRCADAll).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TotalCAD totalCAD = (TotalCAD) o;

        if (TNOfAllCAD != null ? !TNOfAllCAD.equals(totalCAD.TNOfAllCAD) : totalCAD.TNOfAllCAD != null) return false;
        if (TotValOfAllCAD != null ? !TotValOfAllCAD.equals(totalCAD.TotValOfAllCAD) : totalCAD.TotValOfAllCAD != null)
            return false;
        return MNTSMRCADAll != null ? MNTSMRCADAll.equals(totalCAD.MNTSMRCADAll) : totalCAD.MNTSMRCADAll == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfAllCAD != null ? TNOfAllCAD.hashCode() : 0;
        result = 31 * result + (TotValOfAllCAD != null ? TotValOfAllCAD.hashCode() : 0);
        result = 31 * result + (MNTSMRCADAll != null ? MNTSMRCADAll.hashCode() : 0);
        return result;
    }
}
