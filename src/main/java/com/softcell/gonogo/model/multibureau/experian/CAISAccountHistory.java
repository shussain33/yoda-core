package com.softcell.gonogo.model.multibureau.experian;

public class CAISAccountHistory {
    private String year;
    private String month;
    private String daysPastDue;
    private String assetClassification;


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDaysPastDue() {
        return daysPastDue;
    }

    public void setDaysPastDue(String daysPastDue) {
        this.daysPastDue = daysPastDue;
    }

    public String getAssetClassification() {
        return assetClassification;
    }

    public void setAssetClassification(String assetClassification) {
        this.assetClassification = assetClassification;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAISAccountHistory{");
        sb.append("year='").append(year).append('\'');
        sb.append(", month='").append(month).append('\'');
        sb.append(", daysPastDue='").append(daysPastDue).append('\'');
        sb.append(", assetClassification='").append(assetClassification).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAISAccountHistory that = (CAISAccountHistory) o;

        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (month != null ? !month.equals(that.month) : that.month != null) return false;
        if (daysPastDue != null ? !daysPastDue.equals(that.daysPastDue) : that.daysPastDue != null) return false;
        return assetClassification != null ? assetClassification.equals(that.assetClassification) : that.assetClassification == null;
    }

    @Override
    public int hashCode() {
        int result = year != null ? year.hashCode() : 0;
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (daysPastDue != null ? daysPastDue.hashCode() : 0);
        result = 31 * result + (assetClassification != null ? assetClassification.hashCode() : 0);
        return result;
    }
}
