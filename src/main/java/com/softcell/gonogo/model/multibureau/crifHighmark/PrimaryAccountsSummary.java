package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="PRIMARY-ACCOUNTS-SUMMARY")
@XmlAccessorType(XmlAccessType.FIELD)
public class PrimaryAccountsSummary {
	
	@XmlElement(name="PRIMARY-NUMBER-OF-ACCOUNTS")
	private String primaryNumberOfAccounts;
	@XmlElement(name="PRIMARY-ACTIVE-NUMBER-OF-ACCOUNTS")
	private String primaryActiveNumberOfAccounts;
	@XmlElement(name="PRIMARY-OVERDUE-NUMBER-OF-ACCOUNTS")
	private String primaryOverdueNumberOfAccounts;
	@XmlElement(name="PRIMARY-SECURED-NUMBER-OF-ACCOUNTS")
	private String primarySecuredNumberOfAccounts;
	@XmlElement(name="PRIMARY-UNSECURED-NUMBER-OF-ACCOUNTS")
	private String primaryUnsecuredNumberOfAccounts;
	@XmlElement(name="PRIMARY-UNTAGGED-NUMBER-OF-ACCOUNTS")
	private String primaryUntaggedNumberOfAccounts;
	@XmlElement(name="PRIMARY-CURRENT-BALANCE")
	private String primaryCurrentBalance;
	@XmlElement(name="PRIMARY-SANCTIONED-AMOUNT")
	private String primarySanctionedAmount;
	@XmlElement(name="PRIMARY-DISBURSED-AMOUNT")
	private String primaryDisbursedAmount;
	@XmlElement(name="PRIMARY-DISBURSED-AMOUNT")
	private String PrimaryDisbAmt;
	@XmlElement(name="PRIMARY-CURRENT-BALANCE")
	private String PrimaryCurrBal;
	@XmlElement(name="PRIMARY-SANCTIONED-AMOUNT")
	private String PrimarySancAmt;
	
	public String getPrimaryDisbAmt() {
		return PrimaryDisbAmt;
	}
	
	public void setPrimaryDisbAmt(String primaryDisbAmt) {
		PrimaryDisbAmt = primaryDisbAmt;
	}
	
	public String getPrimaryCurrBal() {
		return PrimaryCurrBal;
	}
	
	public void setPrimaryCurrBal(String primaryCurrBal) {
		PrimaryCurrBal = primaryCurrBal;
	}
	
	public String getPrimarySancAmt() {
		return PrimarySancAmt;
	}
	
	public void setPrimarySancAmt(String primarySancAmt) {
		PrimarySancAmt = primarySancAmt;
	}
	
	public String getPrimaryNumberOfAccounts() {
		return primaryNumberOfAccounts;
	}
	public void setPrimaryNumberOfAccounts(String primaryNumberOfAccounts) {
		this.primaryNumberOfAccounts = primaryNumberOfAccounts;
	}
	public String getPrimaryActiveNumberOfAccounts() {
		return primaryActiveNumberOfAccounts;
	}
	public void setPrimaryActiveNumberOfAccounts(
			String primaryActiveNumberOfAccounts) {
		this.primaryActiveNumberOfAccounts = primaryActiveNumberOfAccounts;
	}
	public String getPrimaryOverdueNumberOfAccounts() {
		return primaryOverdueNumberOfAccounts;
	}
	public void setPrimaryOverdueNumberOfAccounts(
			String primaryOverdueNumberOfAccounts) {
		this.primaryOverdueNumberOfAccounts = primaryOverdueNumberOfAccounts;
	}
	public String getPrimarySecuredNumberOfAccounts() {
		return primarySecuredNumberOfAccounts;
	}
	public void setPrimarySecuredNumberOfAccounts(
			String primarySecuredNumberOfAccounts) {
		this.primarySecuredNumberOfAccounts = primarySecuredNumberOfAccounts;
	}
	public String getPrimaryUnsecuredNumberOfAccounts() {
		return primaryUnsecuredNumberOfAccounts;
	}
	public void setPrimaryUnsecuredNumberOfAccounts(
			String primaryUnsecuredNumberOfAccounts) {
		this.primaryUnsecuredNumberOfAccounts = primaryUnsecuredNumberOfAccounts;
	}
	public String getPrimaryUntaggedNumberOfAccounts() {
		return primaryUntaggedNumberOfAccounts;
	}
	public void setPrimaryUntaggedNumberOfAccounts(
			String primaryUntaggedNumberOfAccounts) {
		this.primaryUntaggedNumberOfAccounts = primaryUntaggedNumberOfAccounts;
	}
	public String getPrimaryCurrentBalance() {
		return primaryCurrentBalance;
	}
	public void setPrimaryCurrentBalance(String primaryCurrentBalance) {
		this.primaryCurrentBalance = primaryCurrentBalance;
	}
	public String getPrimarySanctionedAmount() {
		return primarySanctionedAmount;
	}
	public void setPrimarySanctionedAmount(String primarySanctionedAmount) {
		this.primarySanctionedAmount = primarySanctionedAmount;
	}
	public String getPrimaryDisbursedAmount() {
		return primaryDisbursedAmount;
	}
	public void setPrimaryDisbursedAmount(String primaryDisbursedAmount) {
		this.primaryDisbursedAmount = primaryDisbursedAmount;
	}
	
	@Override
	public String toString() {
		return "PrimaryAccountsSummary [primaryNumberOfAccounts="
				+ primaryNumberOfAccounts + ", primaryActiveNumberOfAccounts="
				+ primaryActiveNumberOfAccounts
				+ ", primaryOverdueNumberOfAccounts="
				+ primaryOverdueNumberOfAccounts
				+ ", primarySecuredNumberOfAccounts="
				+ primarySecuredNumberOfAccounts
				+ ", primaryUnsecuredNumberOfAccounts="
				+ primaryUnsecuredNumberOfAccounts
				+ ", primaryUntaggedNumberOfAccounts="
				+ primaryUntaggedNumberOfAccounts + ", primaryCurrentBalance="
				+ primaryCurrentBalance + ", primarySanctionedAmount="
				+ primarySanctionedAmount + ", primaryDisbursedAmount="
				+ primaryDisbursedAmount + ", PrimaryDisbAmt=" + PrimaryDisbAmt
				+ ", PrimaryCurrBal=" + PrimaryCurrBal + ", PrimarySancAmt="
				+ PrimarySancAmt + "]";
	}

}
