package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDomain {

    /**
     * Set Hard Coded Value
     */
    @JsonProperty("01")
    private String priority;

    /**
     * Not Available
     */
    @JsonProperty("02")
    private String productType;

    /**
     * Set but need review
     */
    @JsonProperty("03")
    private String loanType;

    /**
     * Set value of gonogo request is loanAmount
     */
    @JsonProperty("04")
    private long enqAmt;

    /**
     * Set Hard Coded Value
     */
    @JsonProperty("05")
    private String jointInd;

    /**
     * Set Hard Coded Value (not available in  gonogo request )
     */
    @JsonProperty("06")
    private String enquirySubmittedBy;

    /**
     * Set Hard Coded Value (not available in  gonogo request )
     */
    @JsonProperty("07")
    private String sourceSystemName;

    /**
     * Unknown
     */
    @JsonProperty("08")
    private String sourceSystemVersion;

    /**
     * Unknown
     */
    @JsonProperty("09")
    private String sourceSystemVendor;

    /**
     * Unknown
     */
    @JsonProperty("10")
    private String sourceSystemInstanceId;

    /**
     * Set Hard Coded Value (not available in  gonogo request )
     */
    @JsonProperty("11")
    private String bereauRegion;

    /**
     * Set value of gonogo request is Appliedfor of Application Object
     */
    @JsonProperty("12")
    private String loanPurposeDesc;

    /**
     * Unknown
     */
    @JsonProperty("13")
    private Long originBranch;

    /**
     * Unknown
     */
    @JsonProperty("14")
    private String kendra;

    /**
     * Unknown
     */
    @JsonProperty("15")
    private String inquiryStage;

    /**
     * Unknown
     */
    @JsonProperty("16")
    private String authorizationFlag;

    /**
     * Unknown
     */
    @JsonProperty("17")
    private String authorizedBy;

    /**
     * Unknown
     */
    @JsonProperty("19")
    private String indvCorporateFlag;

    /**
     * Unknown
     */
    @JsonProperty("20")
    private String constitution;

    /**
     * Set
     */
    @JsonProperty("21")
    private Name name;

    /**
     * Set
     */
    @JsonProperty("22")
    private String gender;

    /**
     * Set
     */
    @JsonProperty("23")
    private String maritalStatus;

    /**
     * Need review (but Optional field)
     */
    @JsonProperty("24")
    private Relation relation;

    /**
     * Set
     */
    @JsonProperty("25")
    private int age;

    /**
     * Review
     */
    @JsonProperty("26")
    private String ageAsOnDate;

    /**
     * Set
     */
    @JsonProperty("27")
    private String dob;

    /**
     * Set
     */
    @JsonProperty("28")
    private String noOfDependents;

    /**
     * Set
     */
    @JsonProperty("29")
    private List<Address> addressList;

    /**
     * Set
     */
    @JsonProperty("30")
    private ID id;

    /**
     * Set
     */
    @JsonProperty("31")
    private List<Phone> phoneList;

    /**
     * Set
     */
    @JsonProperty("32")
    private String emailId1;

    /**
     * Set
     */
    @JsonProperty("33")
    private String emailId2;

    /**
     * Unknown
     */
    @JsonProperty("34")
    private String alias;

    /**
     * Unknown
     */
    @JsonProperty("35")
    private String actOpeningDate;

    /**
     * Unknown
     */
    @JsonProperty("36")
    private String accountNo1;

    /**
     * Unknown
     */
    @JsonProperty("37")
    private String accountNo2;

    /**
     * Unknown
     */
    @JsonProperty("38")
    private String accountNo3;

    /**
     * Unknown
     */
    @JsonProperty("39")
    private String accountNo4;

    /**
     * Set
     */
    @JsonProperty("40")
    private Long tenure;

    /**
     * Unknown
     */
    @JsonProperty("41")
    private String groupId;

    /**
     * Unknown
     */
    @JsonProperty("42")
    private String numberCreditCards;

    /**
     * Unknown
     */
    @JsonProperty("43")
    private Long creditCardNo;

    /**
     * Set but needs review(Confuse between monthlyIncome and monthlySalary) ,monthlyIncome is available in Array
     */
    @JsonProperty("44")
    private String monthlyIncome;

    /**
     * Unknown
     */
    @JsonProperty("45")
    private String soaEmployerNameC;
    /**
     * Set
     */
    @JsonProperty("46")
    private Long timeWithEmploy;

    /**
     * Unknown
     */
    @JsonProperty("47")
    private String companyCategory;

    /**
     * Unknown
     */
    @JsonProperty("48")
    private String natureOfBusiness;

    /**
     * Unknown
     */
    @JsonProperty("49")
    private String assetCost;

    /**
     * Unknown
     */
    @JsonProperty("50")
    private String collateral1;

    /**
     * Unknown
     */
    @JsonProperty("51")
    private String collateral1Valuation1;

    /**
     * Unknown
     */
    @JsonProperty("52")
    private String collateral1Valuation2;

    /**
     * Unknown
     */
    @JsonProperty("53")
    private String collateral2;

    /**
     * Unknown
     */
    @JsonProperty("54")
    private String collateral2Valuation1;

    /**
     * Unknown
     */
    @JsonProperty("55")
    private String collateral2Valuation2;


    //ADDITIONAL FILLERS
    /**
     * Unknown
     */
    @JsonProperty("61")
    private String agentName;

    /**
     * Unknown
     */
    @JsonProperty("62")
    private String grRatio;

    /**
     * Unknown
     */
    @JsonProperty("63")
    private String totalCaseProject;

    /**
     * Unknown
     */
    @JsonProperty("64")
    private String totalCase3month;

    /**
     * Unknown
     */
    @JsonProperty("65")
    private String dsaIdAgent;

    /**
     * Set
     */
    @JsonProperty("66")
    private String applicationId;

    /**
     * Unknown
     */
    @JsonProperty("67")
    private String dsaIdApplication;

    /**
     * Unknown
     */
    @JsonProperty("68")
    private String croId;

    /**
     * Unknown
     */
    @JsonProperty("70")
    private String time;

    /**
     * Unknown
     */
    @JsonProperty("71")
    private String aDate;

    /**
     * Unknown
     */
    @JsonProperty("74")
    private Date createdDate;

    /**
     * Needs Review Multiple value Attribute in Gonogo request object
     */
    @JsonProperty("76")
    private String timeAddress;

    /**
     * Set
     */
    @JsonProperty("77")
    private String employer;

    /**
     * Unknown
     */
    @JsonProperty("79")
    private String grossAnnual;

    /**
     * Unknown
     */
    @JsonProperty("80")
    private String currentEmi;

    /**
     * Unknown
     */
    @JsonProperty("81")
    private String lmth;

    /**
     * Unknown
     */
    @JsonProperty("82")
    private String llmth;

    /**
     * Unknown
     */
    @JsonProperty("83")
    private String panStatus;

    /**
     * Unknown
     */
    @JsonProperty("84")
    private String adharStatus;

    /**
     * Set
     */
    @JsonProperty("85")
    private String propertyName;

    /**
     * Set
     */
    @JsonProperty("86")
    private String location;

    /**
     * Unknown
     */
    @JsonProperty("87")
    private String appType;

    /**
     * Set
     */
    @JsonProperty("88")
    private String institutionId;

    /**
     * Set
     */
    @JsonProperty("90")
    private String propertyStatus;

    /**
     * Set
     */
    @JsonProperty("91")
    private String propertyType;

    /**
     * Set
     */
    @JsonProperty("92")
    private String propertyValue;

    /**
     * Set
     */
    @JsonProperty("93")
    private String propertyAddress;

    /**
     * Unknown
     */
    @JsonProperty("94")
    private String propertyAge;

    /**
     * Unknown
     */
    @JsonProperty("95")
    private String loanFor;

    /**
     * Set
     */
    @JsonProperty("96")
    private String employmentType;

    /**
     * Available in Sepate object
     */
    @JsonProperty("97")
    private String coBorowerName;

    /**
     * Set
     */
    @JsonProperty("98")
    private String noEarningMember;

    /**
     * Set
     */
    @JsonProperty("99")
    private String noFamilyMember;

    /**
     * Set
     */
    @JsonProperty("100")
    private String numberOfEmi;

    /**
     * Unknown
     */
    @JsonProperty("101")
    private String propertyCode;

    @JsonProperty("102")
    private String applicantType;

    public static Builder builder() {
        return new Builder();
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public long getEnqAmt() {
        return enqAmt;
    }

    public void setEnqAmt(long enqAmt) {
        this.enqAmt = enqAmt;
    }

    public String getJointInd() {
        return jointInd;
    }

    public void setJointInd(String jointInd) {
        this.jointInd = jointInd;
    }

    public String getEnquirySubmittedBy() {
        return enquirySubmittedBy;
    }

    public void setEnquirySubmittedBy(String enquirySubmittedBy) {
        this.enquirySubmittedBy = enquirySubmittedBy;
    }

    public String getSourceSystemName() {
        return sourceSystemName;
    }

    public void setSourceSystemName(String sourceSystemName) {
        this.sourceSystemName = sourceSystemName;
    }

    public String getSourceSystemVersion() {
        return sourceSystemVersion;
    }

    public void setSourceSystemVersion(String sourceSystemVersion) {
        this.sourceSystemVersion = sourceSystemVersion;
    }

    public String getSourceSystemVendor() {
        return sourceSystemVendor;
    }

    public void setSourceSystemVendor(String sourceSystemVendor) {
        this.sourceSystemVendor = sourceSystemVendor;
    }

    public String getSourceSystemInstanceId() {
        return sourceSystemInstanceId;
    }

    public void setSourceSystemInstanceId(String sourceSystemInstanceId) {
        this.sourceSystemInstanceId = sourceSystemInstanceId;
    }

    public String getBereauRegion() {
        return bereauRegion;
    }

    public void setBereauRegion(String bereauRegion) {
        this.bereauRegion = bereauRegion;
    }

    public String getLoanPurposeDesc() {
        return loanPurposeDesc;
    }

    public void setLoanPurposeDesc(String loanPurposeDesc) {
        this.loanPurposeDesc = loanPurposeDesc;
    }

    public Long getOriginBranch() {
        return originBranch;
    }

    public void setOriginBranch(Long  originBranch) {
        this.originBranch = originBranch;
    }

    public String getKendra() {
        return kendra;
    }

    public void setKendra(String kendra) {
        this.kendra = kendra;
    }

    public String getInquiryStage() {
        return inquiryStage;
    }

    public void setInquiryStage(String inquiryStage) {
        this.inquiryStage = inquiryStage;
    }

    public String getAuthorizationFlag() {
        return authorizationFlag;
    }

    public void setAuthorizationFlag(String authorizationFlag) {
        this.authorizationFlag = authorizationFlag;
    }

    public String getAuthorizedBy() {
        return authorizedBy;
    }

    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    public String getIndvCorporateFlag() {
        return indvCorporateFlag;
    }

    public void setIndvCorporateFlag(String indvCorporateFlag) {
        this.indvCorporateFlag = indvCorporateFlag;
    }

    public String getConstitution() {
        return constitution;
    }

    public void setConstitution(String constitution) {
        this.constitution = constitution;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAgeAsOnDate() {
        return ageAsOnDate;
    }

    public void setAgeAsOnDate(String ageAsOnDate) {
        this.ageAsOnDate = ageAsOnDate;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNoOfDependents() {
        return noOfDependents;
    }

    public void setNoOfDependents(String noOfDependents) {
        this.noOfDependents = noOfDependents;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public List<Phone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<Phone> phoneList) {
        this.phoneList = phoneList;
    }

    public String getEmailId1() {
        return emailId1;
    }

    public void setEmailId1(String emailId1) {
        this.emailId1 = emailId1;
    }

    public String getEmailId2() {
        return emailId2;
    }

    public void setEmailId2(String emailId2) {
        this.emailId2 = emailId2;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getActOpeningDate() {
        return actOpeningDate;
    }

    public void setActOpeningDate(String actOpeningDate) {
        this.actOpeningDate = actOpeningDate;
    }

    public String getAccountNo1() {
        return accountNo1;
    }

    public void setAccountNo1(String accountNo1) {
        this.accountNo1 = accountNo1;
    }

    public String getAccountNo2() {
        return accountNo2;
    }

    public void setAccountNo2(String accountNo2) {
        this.accountNo2 = accountNo2;
    }

    public String getAccountNo3() {
        return accountNo3;
    }

    public void setAccountNo3(String accountNo3) {
        this.accountNo3 = accountNo3;
    }

    public String getAccountNo4() {
        return accountNo4;
    }

    public void setAccountNo4(String accountNo4) {
        this.accountNo4 = accountNo4;
    }

    public Long getTenure() {
        return tenure;
    }

    public void setTenure(Long tenure) {
        this.tenure = tenure;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getNumberCreditCards() {
        return numberCreditCards;
    }

    public void setNumberCreditCards(String numberCreditCards) {
        this.numberCreditCards = numberCreditCards;
    }

    public Long getCreditCardNo() {
        return creditCardNo;
    }

    public void setCreditCardNo(Long creditCardNo) {
        this.creditCardNo = creditCardNo;
    }

    public String getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(String monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getSoaEmployerNameC() {
        return soaEmployerNameC;
    }

    public void setSoaEmployerNameC(String soaEmployerNameC) {
        this.soaEmployerNameC = soaEmployerNameC;
    }

    public Long getTimeWithEmploy() {
        return timeWithEmploy;
    }

    public void setTimeWithEmploy(Long timeWithEmploy) {
        this.timeWithEmploy = timeWithEmploy;
    }

    public String getCompanyCategory() {
        return companyCategory;
    }

    public void setCompanyCategory(String companyCategory) {
        this.companyCategory = companyCategory;
    }

    public String getNatureOfBusiness() {
        return natureOfBusiness;
    }

    public void setNatureOfBusiness(String natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    public String getAssetCost() {
        return assetCost;
    }

    public void setAssetCost(String assetCost) {
        this.assetCost = assetCost;
    }

    public String getCollateral1() {
        return collateral1;
    }

    public void setCollateral1(String collateral1) {
        this.collateral1 = collateral1;
    }

    public String getCollateral1Valuation1() {
        return collateral1Valuation1;
    }

    public void setCollateral1Valuation1(String collateral1Valuation1) {
        this.collateral1Valuation1 = collateral1Valuation1;
    }

    public String getCollateral1Valuation2() {
        return collateral1Valuation2;
    }

    public void setCollateral1Valuation2(String collateral1Valuation2) {
        this.collateral1Valuation2 = collateral1Valuation2;
    }

    public String getCollateral2() {
        return collateral2;
    }

    public void setCollateral2(String collateral2) {
        this.collateral2 = collateral2;
    }

    public String getCollateral2Valuation1() {
        return collateral2Valuation1;
    }

    public void setCollateral2Valuation1(String collateral2Valuation1) {
        this.collateral2Valuation1 = collateral2Valuation1;
    }

    public String getCollateral2Valuation2() {
        return collateral2Valuation2;
    }

    public void setCollateral2Valuation2(String collateral2Valuation2) {
        this.collateral2Valuation2 = collateral2Valuation2;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getGrRatio() {
        return grRatio;
    }

    public void setGrRatio(String grRatio) {
        this.grRatio = grRatio;
    }

    public String getTotalCaseProject() {
        return totalCaseProject;
    }

    public void setTotalCaseProject(String totalCaseProject) {
        this.totalCaseProject = totalCaseProject;
    }

    public String getTotalCase3month() {
        return totalCase3month;
    }

    public void setTotalCase3month(String totalCase3month) {
        this.totalCase3month = totalCase3month;
    }

    public String getDsaIdAgent() {
        return dsaIdAgent;
    }

    public void setDsaIdAgent(String dsaIdAgent) {
        this.dsaIdAgent = dsaIdAgent;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getDsaIdApplication() {
        return dsaIdApplication;
    }

    public void setDsaIdApplication(String dsaIdApplication) {
        this.dsaIdApplication = dsaIdApplication;
    }

    public String getCroId() {
        return croId;
    }

    public void setCroId(String croId) {
        this.croId = croId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getaDate() {
        return aDate;
    }

    public void setaDate(String aDate) {
        this.aDate = aDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTimeAddress() {
        return timeAddress;
    }

    public void setTimeAddress(String timeAddress) {
        this.timeAddress = timeAddress;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getGrossAnnual() {
        return grossAnnual;
    }

    public void setGrossAnnual(String grossAnnual) {
        this.grossAnnual = grossAnnual;
    }

    public String getCurrentEmi() {
        return currentEmi;
    }

    public void setCurrentEmi(String currentEmi) {
        this.currentEmi = currentEmi;
    }

    public String getLmth() {
        return lmth;
    }

    public void setLmth(String lmth) {
        this.lmth = lmth;
    }

    public String getLlmth() {
        return llmth;
    }

    public void setLlmth(String llmth) {
        this.llmth = llmth;
    }

    public String getPanStatus() {
        return panStatus;
    }

    public void setPanStatus(String panStatus) {
        this.panStatus = panStatus;
    }

    public String getAdharStatus() {
        return adharStatus;
    }

    public void setAdharStatus(String adharStatus) {
        this.adharStatus = adharStatus;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getPropertyStatus() {
        return propertyStatus;
    }

    public void setPropertyStatus(String propertyStatus) {
        this.propertyStatus = propertyStatus;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public String getPropertyAddress() {
        return propertyAddress;
    }

    public void setPropertyAddress(String propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    public String getPropertyAge() {
        return propertyAge;
    }

    public void setPropertyAge(String propertyAge) {
        this.propertyAge = propertyAge;
    }

    public String getLoanFor() {
        return loanFor;
    }

    public void setLoanFor(String loanFor) {
        this.loanFor = loanFor;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getCoBorowerName() {
        return coBorowerName;
    }

    public void setCoBorowerName(String coBorowerName) {
        this.coBorowerName = coBorowerName;
    }

    public String getNoEarningMember() {
        return noEarningMember;
    }

    public void setNoEarningMember(String noEarningMember) {
        this.noEarningMember = noEarningMember;
    }

    public String getNoFamilyMember() {
        return noFamilyMember;
    }

    public void setNoFamilyMember(String noFamilyMember) {
        this.noFamilyMember = noFamilyMember;
    }

    public String getNumberOfEmi() {
        return numberOfEmi;
    }

    public void setNumberOfEmi(String numberOfEmi) {
        this.numberOfEmi = numberOfEmi;
    }

    public String getPropertyCode() {
        return propertyCode;
    }

    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("priority", priority)
                .append("productType", productType)
                .append("loanType", loanType)
                .append("enqAmt", enqAmt)
                .append("jointInd", jointInd)
                .append("enquirySubmittedBy", enquirySubmittedBy)
                .append("sourceSystemName", sourceSystemName)
                .append("sourceSystemVersion", sourceSystemVersion)
                .append("sourceSystemVendor", sourceSystemVendor)
                .append("sourceSystemInstanceId", sourceSystemInstanceId)
                .append("bereauRegion", bereauRegion)
                .append("loanPurposeDesc", loanPurposeDesc)
                .append("originBranch", originBranch)
                .append("kendra", kendra)
                .append("inquiryStage", inquiryStage)
                .append("authorizationFlag", authorizationFlag)
                .append("authorizedBy", authorizedBy)
                .append("indvCorporateFlag", indvCorporateFlag)
                .append("constitution", constitution)
                .append("name", name)
                .append("gender", gender)
                .append("maritalStatus", maritalStatus)
                .append("relation", relation)
                .append("age", age)
                .append("ageAsOnDate", ageAsOnDate)
                .append("dob", dob)
                .append("noOfDependents", noOfDependents)
                .append("addressList", addressList)
                .append("id", id)
                .append("phoneList", phoneList)
                .append("emailId1", emailId1)
                .append("emailId2", emailId2)
                .append("alias", alias)
                .append("actOpeningDate", actOpeningDate)
                .append("accountNo1", accountNo1)
                .append("accountNo2", accountNo2)
                .append("accountNo3", accountNo3)
                .append("accountNo4", accountNo4)
                .append("tenure", tenure)
                .append("groupId", groupId)
                .append("numberCreditCards", numberCreditCards)
                .append("creditCardNo", creditCardNo)
                .append("monthlyIncome", monthlyIncome)
                .append("soaEmployerNameC", soaEmployerNameC)
                .append("timeWithEmploy", timeWithEmploy)
                .append("companyCategory", companyCategory)
                .append("natureOfBusiness", natureOfBusiness)
                .append("assetCost", assetCost)
                .append("collateral1", collateral1)
                .append("collateral1Valuation1", collateral1Valuation1)
                .append("collateral1Valuation2", collateral1Valuation2)
                .append("collateral2", collateral2)
                .append("collateral2Valuation1", collateral2Valuation1)
                .append("collateral2Valuation2", collateral2Valuation2)
                .append("agentName", agentName)
                .append("grRatio", grRatio)
                .append("totalCaseProject", totalCaseProject)
                .append("totalCase3month", totalCase3month)
                .append("dsaIdAgent", dsaIdAgent)
                .append("applicationId", applicationId)
                .append("dsaIdApplication", dsaIdApplication)
                .append("croId", croId)
                .append("time", time)
                .append("aDate", aDate)
                .append("createdDate", createdDate)
                .append("timeAddress", timeAddress)
                .append("employer", employer)
                .append("grossAnnual", grossAnnual)
                .append("currentEmi", currentEmi)
                .append("lmth", lmth)
                .append("llmth", llmth)
                .append("panStatus", panStatus)
                .append("adharStatus", adharStatus)
                .append("propertyName", propertyName)
                .append("location", location)
                .append("appType", appType)
                .append("institutionId", institutionId)
                .append("propertyStatus", propertyStatus)
                .append("propertyType", propertyType)
                .append("propertyValue", propertyValue)
                .append("propertyAddress", propertyAddress)
                .append("propertyAge", propertyAge)
                .append("loanFor", loanFor)
                .append("employmentType", employmentType)
                .append("coBorowerName", coBorowerName)
                .append("noEarningMember", noEarningMember)
                .append("noFamilyMember", noFamilyMember)
                .append("numberOfEmi", numberOfEmi)
                .append("propertyCode", propertyCode)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequestDomain)) return false;
        RequestDomain that = (RequestDomain) o;
        return Objects.equal(getEnqAmt(), that.getEnqAmt()) &&
                Objects.equal(getAge(), that.getAge()) &&
                Objects.equal(getPriority(), that.getPriority()) &&
                Objects.equal(getProductType(), that.getProductType()) &&
                Objects.equal(getLoanType(), that.getLoanType()) &&
                Objects.equal(getJointInd(), that.getJointInd()) &&
                Objects.equal(getEnquirySubmittedBy(), that.getEnquirySubmittedBy()) &&
                Objects.equal(getSourceSystemName(), that.getSourceSystemName()) &&
                Objects.equal(getSourceSystemVersion(), that.getSourceSystemVersion()) &&
                Objects.equal(getSourceSystemVendor(), that.getSourceSystemVendor()) &&
                Objects.equal(getSourceSystemInstanceId(), that.getSourceSystemInstanceId()) &&
                Objects.equal(getBereauRegion(), that.getBereauRegion()) &&
                Objects.equal(getLoanPurposeDesc(), that.getLoanPurposeDesc()) &&
                Objects.equal(getOriginBranch(), that.getOriginBranch()) &&
                Objects.equal(getKendra(), that.getKendra()) &&
                Objects.equal(getInquiryStage(), that.getInquiryStage()) &&
                Objects.equal(getAuthorizationFlag(), that.getAuthorizationFlag()) &&
                Objects.equal(getAuthorizedBy(), that.getAuthorizedBy()) &&
                Objects.equal(getIndvCorporateFlag(), that.getIndvCorporateFlag()) &&
                Objects.equal(getConstitution(), that.getConstitution()) &&
                Objects.equal(getName(), that.getName()) &&
                Objects.equal(getGender(), that.getGender()) &&
                Objects.equal(getMaritalStatus(), that.getMaritalStatus()) &&
                Objects.equal(getRelation(), that.getRelation()) &&
                Objects.equal(getAgeAsOnDate(), that.getAgeAsOnDate()) &&
                Objects.equal(getDob(), that.getDob()) &&
                Objects.equal(getNoOfDependents(), that.getNoOfDependents()) &&
                Objects.equal(getAddressList(), that.getAddressList()) &&
                Objects.equal(getId(), that.getId()) &&
                Objects.equal(getPhoneList(), that.getPhoneList()) &&
                Objects.equal(getEmailId1(), that.getEmailId1()) &&
                Objects.equal(getEmailId2(), that.getEmailId2()) &&
                Objects.equal(getAlias(), that.getAlias()) &&
                Objects.equal(getActOpeningDate(), that.getActOpeningDate()) &&
                Objects.equal(getAccountNo1(), that.getAccountNo1()) &&
                Objects.equal(getAccountNo2(), that.getAccountNo2()) &&
                Objects.equal(getAccountNo3(), that.getAccountNo3()) &&
                Objects.equal(getAccountNo4(), that.getAccountNo4()) &&
                Objects.equal(getTenure(), that.getTenure()) &&
                Objects.equal(getGroupId(), that.getGroupId()) &&
                Objects.equal(getNumberCreditCards(), that.getNumberCreditCards()) &&
                Objects.equal(getCreditCardNo(), that.getCreditCardNo()) &&
                Objects.equal(getMonthlyIncome(), that.getMonthlyIncome()) &&
                Objects.equal(getSoaEmployerNameC(), that.getSoaEmployerNameC()) &&
                Objects.equal(getTimeWithEmploy(), that.getTimeWithEmploy()) &&
                Objects.equal(getCompanyCategory(), that.getCompanyCategory()) &&
                Objects.equal(getNatureOfBusiness(), that.getNatureOfBusiness()) &&
                Objects.equal(getAssetCost(), that.getAssetCost()) &&
                Objects.equal(getCollateral1(), that.getCollateral1()) &&
                Objects.equal(getCollateral1Valuation1(), that.getCollateral1Valuation1()) &&
                Objects.equal(getCollateral1Valuation2(), that.getCollateral1Valuation2()) &&
                Objects.equal(getCollateral2(), that.getCollateral2()) &&
                Objects.equal(getCollateral2Valuation1(), that.getCollateral2Valuation1()) &&
                Objects.equal(getCollateral2Valuation2(), that.getCollateral2Valuation2()) &&
                Objects.equal(getAgentName(), that.getAgentName()) &&
                Objects.equal(getGrRatio(), that.getGrRatio()) &&
                Objects.equal(getTotalCaseProject(), that.getTotalCaseProject()) &&
                Objects.equal(getTotalCase3month(), that.getTotalCase3month()) &&
                Objects.equal(getDsaIdAgent(), that.getDsaIdAgent()) &&
                Objects.equal(getApplicationId(), that.getApplicationId()) &&
                Objects.equal(getDsaIdApplication(), that.getDsaIdApplication()) &&
                Objects.equal(getCroId(), that.getCroId()) &&
                Objects.equal(getTime(), that.getTime()) &&
                Objects.equal(getaDate(), that.getaDate()) &&
                Objects.equal(getCreatedDate(), that.getCreatedDate()) &&
                Objects.equal(getTimeAddress(), that.getTimeAddress()) &&
                Objects.equal(getEmployer(), that.getEmployer()) &&
                Objects.equal(getGrossAnnual(), that.getGrossAnnual()) &&
                Objects.equal(getCurrentEmi(), that.getCurrentEmi()) &&
                Objects.equal(getLmth(), that.getLmth()) &&
                Objects.equal(getLlmth(), that.getLlmth()) &&
                Objects.equal(getPanStatus(), that.getPanStatus()) &&
                Objects.equal(getAdharStatus(), that.getAdharStatus()) &&
                Objects.equal(getPropertyName(), that.getPropertyName()) &&
                Objects.equal(getLocation(), that.getLocation()) &&
                Objects.equal(getAppType(), that.getAppType()) &&
                Objects.equal(getInstitutionId(), that.getInstitutionId()) &&
                Objects.equal(getPropertyStatus(), that.getPropertyStatus()) &&
                Objects.equal(getPropertyType(), that.getPropertyType()) &&
                Objects.equal(getPropertyValue(), that.getPropertyValue()) &&
                Objects.equal(getPropertyAddress(), that.getPropertyAddress()) &&
                Objects.equal(getPropertyAge(), that.getPropertyAge()) &&
                Objects.equal(getLoanFor(), that.getLoanFor()) &&
                Objects.equal(getEmploymentType(), that.getEmploymentType()) &&
                Objects.equal(getCoBorowerName(), that.getCoBorowerName()) &&
                Objects.equal(getNoEarningMember(), that.getNoEarningMember()) &&
                Objects.equal(getNoFamilyMember(), that.getNoFamilyMember()) &&
                Objects.equal(getNumberOfEmi(), that.getNumberOfEmi()) &&
                Objects.equal(getPropertyCode(), that.getPropertyCode());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPriority(), getProductType(), getLoanType(), getEnqAmt(),
                getJointInd(), getEnquirySubmittedBy(), getSourceSystemName(), getSourceSystemVersion(),
                getSourceSystemVendor(), getSourceSystemInstanceId(), getBereauRegion(), getLoanPurposeDesc(),
                getOriginBranch(), getKendra(), getInquiryStage(), getAuthorizationFlag(), getAuthorizedBy(),
                getIndvCorporateFlag(), getConstitution(), getName(), getGender(), getMaritalStatus(),
                getRelation(), getAge(), getAgeAsOnDate(), getDob(), getNoOfDependents(), getAddressList(),
                getId(), getPhoneList(), getEmailId1(), getEmailId2(), getAlias(), getActOpeningDate(),
                getAccountNo1(), getAccountNo2(), getAccountNo3(), getAccountNo4(), getTenure(), getGroupId(),
                getNumberCreditCards(), getCreditCardNo(), getMonthlyIncome(), getSoaEmployerNameC(),
                getTimeWithEmploy(), getCompanyCategory(), getNatureOfBusiness(), getAssetCost(),
                getCollateral1(), getCollateral1Valuation1(), getCollateral1Valuation2(), getCollateral2(),
                getCollateral2Valuation1(), getCollateral2Valuation2(), getAgentName(), getGrRatio(),
                getTotalCaseProject(), getTotalCase3month(), getDsaIdAgent(), getApplicationId(),
                getDsaIdApplication(), getCroId(), getTime(), getaDate(), getCreatedDate(), getTimeAddress(),
                getEmployer(), getGrossAnnual(), getCurrentEmi(), getLmth(), getLlmth(), getPanStatus(),
                getAdharStatus(), getPropertyName(), getLocation(), getAppType(), getInstitutionId(),
                getPropertyStatus(), getPropertyType(), getPropertyValue(), getPropertyAddress(),
                getPropertyAge(), getLoanFor(), getEmploymentType(), getCoBorowerName(), getNoEarningMember(),
                getNoFamilyMember(), getNumberOfEmi(), getPropertyCode());
    }

    public static class Builder {
        private RequestDomain requestDomain = new RequestDomain();

        public RequestDomain build() {
            return this.requestDomain;
        }

        public Builder priority(String priority) {
            this.requestDomain.setPriority(priority);
            return this;
        }

        public Builder productType(String productType) {
            this.requestDomain.setProductType(productType);
            return this;
        }

        public Builder loanType(String loanType) {
            this.requestDomain.setLoanType(loanType);
            return this;
        }

        public Builder enqAmt(long enqAmt) {
            this.requestDomain.setEnqAmt(enqAmt);
            return this;
        }

        public Builder jointInd(String jointInd) {
            this.requestDomain.setJointInd(jointInd);
            return this;
        }

        public Builder enquirySubmittedBy(String enquirySubmittedBy) {
            this.requestDomain.setEnquirySubmittedBy(enquirySubmittedBy);
            return this;
        }

        public Builder sourceSystemName(String sourceSystemName) {
            this.requestDomain.setSourceSystemName(sourceSystemName);
            return this;
        }

        public Builder sourceSystemVersion(String sourceSystemVersion) {
            this.requestDomain.setSourceSystemVersion(sourceSystemVersion);
            return this;
        }

        public Builder sourceSystemVendor(String sourceSystemVendor) {
            this.requestDomain.setSourceSystemVendor(sourceSystemVendor);
            return this;
        }

        public Builder sourceSystemInstanceId(String sourceSystemInstanceId) {
            this.requestDomain.setSourceSystemInstanceId(sourceSystemInstanceId);
            return this;
        }

        public Builder bereauRegion(String bereauRegion) {
            this.requestDomain.setBereauRegion(bereauRegion);
            return this;
        }

        public Builder loanPurposeDesc(String loanPurposeDesc) {
            this.requestDomain.setLoanPurposeDesc(loanPurposeDesc);
            return this;
        }

        public Builder originBranch(Long originBranch) {
            this.requestDomain.setOriginBranch(originBranch);
            return this;
        }

        public Builder kendra(String kendra) {
            this.requestDomain.setKendra(kendra);
            return this;
        }

        public Builder inquiryStage(String inquiryStage) {
            this.requestDomain.setInquiryStage(inquiryStage);
            return this;
        }

        public Builder authorizationFlag(String authorizationFlag) {
            this.requestDomain.setAuthorizationFlag(authorizationFlag);
            return this;
        }

        public Builder authorizedBy(String authorizedBy) {
            this.requestDomain.setAuthorizedBy(authorizedBy);
            return this;
        }

        public Builder indvCorporateFlag(String indvCorporateFlag) {
            this.requestDomain.setIndvCorporateFlag(indvCorporateFlag);
            return this;
        }

        public Builder constitution(String constitution) {
            this.requestDomain.setConstitution(constitution);
            return this;
        }

        public Builder name(Name name) {
            this.requestDomain.setName(name);
            return this;
        }

        public Builder gender(String gender) {
            this.requestDomain.setGender(gender);
            return this;
        }

        public Builder maritalStatus(String maritalStatus) {
            this.requestDomain.setMaritalStatus(maritalStatus);
            return this;
        }

        public Builder relation(Relation relation) {
            this.requestDomain.setRelation(relation);
            return this;
        }

        public Builder age(int age) {
            this.requestDomain.setAge(age);
            return this;
        }

        public Builder ageAsOnDate(String ageAsOnDate) {
            this.requestDomain.setAgeAsOnDate(ageAsOnDate);
            return this;
        }

        public Builder dob(String dob) {
            this.requestDomain.setDob(dob);
            return this;
        }

        public Builder noOfDependents(String noOfDependents) {
            this.requestDomain.setNoOfDependents(noOfDependents);
            return this;
        }

        public Builder addressList(List<Address> addressList) {
            this.requestDomain.setAddressList(addressList);
            return this;
        }

        public Builder id(ID id) {
            this.requestDomain.setId(id);
            return this;
        }

        public Builder phoneList(List<Phone> phoneList) {
            this.requestDomain.setPhoneList(phoneList);
            return this;
        }

        public Builder emailId1(String emailId1) {
            this.requestDomain.setEmailId1(emailId1);
            return this;
        }

        public Builder emailId2(String emailId2) {
            this.requestDomain.setEmailId2(emailId2);
            return this;
        }

        public Builder alias(String alias) {
            this.requestDomain.setAlias(alias);
            return this;

        }

        public Builder actOpeningDate(String actOpeningDate) {
            this.requestDomain.setActOpeningDate(actOpeningDate);
            return this;
        }

        public Builder accountNo1(String accountNo1) {
            this.requestDomain.setAccountNo1(accountNo1);
            return this;
        }

        public Builder accountNo2(String accountNo2) {
            this.requestDomain.setAccountNo2(accountNo2);
            return this;
        }

        public Builder accountNo3(String accountNo3) {
            this.requestDomain.setAccountNo3(accountNo3);
            return this;
        }

        public Builder accountNo4(String accountNo4) {
            this.requestDomain.setAccountNo4(accountNo4);
            return this;
        }

        public Builder tenure(Long tenure) {
            this.requestDomain.setTenure(tenure);
            return this;
        }

        public Builder groupId(String groupId) {
            this.requestDomain.setGroupId(groupId);
            return this;
        }

        public Builder numberCreditCards(String numberCreditCards) {
            this.requestDomain.setNumberCreditCards(numberCreditCards);
            return this;
        }

        public Builder creditCardNo(Long creditCardNo) {
            this.requestDomain.setCreditCardNo(creditCardNo);
            return this;
        }

        public Builder monthlyIncome(String monthlyIncome) {
            this.requestDomain.setMonthlyIncome(monthlyIncome);
            return this;
        }

        public Builder soaEmployerNameC(String soaEmployerNameC) {
            this.requestDomain.setSoaEmployerNameC(soaEmployerNameC);
            return this;
        }

        public Builder timeWithEmploy(Long timeWithEmploy) {
            this.requestDomain.setTimeWithEmploy(timeWithEmploy);
            return this;
        }

        public Builder companyCategory(String companyCategory) {
            this.requestDomain.setCompanyCategory(companyCategory);
            return this;
        }

        public Builder natureOfBusiness(String natureOfBusiness) {
            this.requestDomain.setNatureOfBusiness(natureOfBusiness);
            return this;
        }

        public Builder assetCost(String assetCost) {
            this.requestDomain.setAssetCost(assetCost);
            return this;
        }

        public Builder collateral1(String collateral1) {
            this.requestDomain.setCollateral1(collateral1);
            return this;
        }

        public Builder collateral1Valuation1(String collateral1Valuation1) {
            this.requestDomain.setCollateral1Valuation1(collateral1Valuation1);
            return this;
        }

        public Builder collateral1Valuation2(String collateral1Valuation2) {
            this.requestDomain.setCollateral1Valuation2(collateral1Valuation2);
            return this;
        }

        public Builder collateral2(String collateral2) {
            this.requestDomain.setCollateral2(collateral2);
            return this;
        }

        public Builder collateral2Valuation1(String collateral2Valuation1) {
            this.requestDomain.setCollateral2Valuation1(collateral2Valuation1);
            return this;
        }

        public Builder collateral2Valuation2(String collateral2Valuation2) {
            this.requestDomain.setCollateral2Valuation2(collateral2Valuation2);
            return this;
        }

        public Builder agentName(String agentName) {
            this.requestDomain.setAgentName(agentName);
            return this;
        }

        public Builder grRatio(String grRatio) {
            this.requestDomain.setGrRatio(grRatio);
            return this;
        }

        public Builder totalCaseProject(String totalCaseProject) {
            this.requestDomain.setTotalCaseProject(totalCaseProject);
            return this;

        }

        public Builder totalCase3month(String totalCase3month) {
            this.requestDomain.setTotalCase3month(totalCase3month);
            return this;
        }

        public Builder dsaIdAgent(String dsaIdAgent) {
            this.requestDomain.setDsaIdAgent(dsaIdAgent);
            return this;
        }

        public Builder applicationId(String applicationId) {
            this.requestDomain.setApplicationId(applicationId);
            return this;
        }

        public Builder dsaIdApplication(String dsaIdApplication) {
            this.requestDomain.setDsaIdApplication(dsaIdApplication);
            return this;
        }

        public Builder croId(String croId) {
            this.requestDomain.setCroId(croId);
            return this;
        }

        public Builder time(String time) {
            this.requestDomain.setTime(time);
            return this;
        }

        public Builder aDate(String aDate) {
            this.requestDomain.setaDate(aDate);
            return this;
        }

        public Builder createdDate(Date createdDate) {
            this.requestDomain.setCreatedDate(createdDate);
            return this;
        }

        public Builder timeAddress(String timeAddress) {
            this.requestDomain.setTimeAddress(timeAddress);
            return this;
        }

        public Builder employer(String employer) {
            this.requestDomain.setEmployer(employer);
            return this;
        }

        public Builder grossAnnual(String grossAnnual) {
            this.requestDomain.setGrossAnnual(grossAnnual);
            return this;
        }

        public Builder currentEmi(String currentEmi) {
            this.requestDomain.setCurrentEmi(currentEmi);
            return this;
        }

        public Builder lmth(String lmth) {
            this.requestDomain.setLmth(lmth);
            return this;
        }

        public Builder llmth(String llmth) {
            this.requestDomain.setLlmth(llmth);
            return this;
        }

        public Builder panStatus(String panStatus) {
            this.requestDomain.setPanStatus(panStatus);
            return this;
        }

        public Builder adharStatus(String adharStatus) {
            this.requestDomain.setAdharStatus(adharStatus);
            return this;
        }

        public Builder propertyName(String propertyName) {
            this.requestDomain.setPropertyName(propertyName);
            return this;
        }

        public Builder location(String location) {
            this.requestDomain.setLocation(location);
            return this;
        }

        public Builder appType(String appType) {
            this.requestDomain.setAppType(appType);
            return this;
        }

        public Builder institutionId(String institutionId) {
            this.requestDomain.setInstitutionId(institutionId);
            return this;
        }

        public Builder propertyStatus(String propertyStatus) {
            this.requestDomain.setPropertyStatus(propertyStatus);
            return this;
        }

        public Builder propertyType(String propertyType) {
            this.requestDomain.setPropertyType(propertyType);
            return this;
        }

        public Builder propertyValue(String propertyValue) {
            this.requestDomain.setPropertyValue(propertyValue);
            return this;
        }

        public Builder propertyAddress(String propertyAddress) {
            this.requestDomain.setPropertyAddress(propertyAddress);
            return this;
        }

        public Builder propertyAge(String propertyAge) {
            this.requestDomain.setPropertyAge(propertyAge);
            return this;
        }

        public Builder loanFor(String loanFor) {
            this.requestDomain.setLoanFor(loanFor);
            return this;
        }

        public Builder employmentType(String employmentType) {
            this.requestDomain.setEmploymentType(employmentType);
            return this;
        }

        public Builder coBorowerName(String coBorowerName) {
            this.requestDomain.setCoBorowerName(coBorowerName);
            return this;

        }

        public Builder noEarningMember(String noEarningMember) {
            this.requestDomain.setNoEarningMember(noEarningMember);
            return this;
        }

        public Builder noFamilyMember(String noFamilyMember) {
            this.requestDomain.setNoFamilyMember(noFamilyMember);
            return this;
        }

        public Builder numberOfEmi(String numberOfEmi) {
            this.requestDomain.setNumberOfEmi(numberOfEmi);
            return this;
        }

        public Builder propertyCode(String propertyCode) {
            this.requestDomain.setPropertyCode(propertyCode);
            return this;
        }

        public Builder applicantType(String applicantType) {
            this.requestDomain.setApplicantType(applicantType);
            return this;
        }

    }
}
