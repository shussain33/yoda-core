package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ADDRESS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Address {

	/**
	 * @author Akshata
	 *
	 *
	 */
	
	@XmlElement(name="ADDRESS")
	private String address;

	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}


	@Override
	public String toString() {
		return "Address [address=" + address + "]";
	}
	
}
