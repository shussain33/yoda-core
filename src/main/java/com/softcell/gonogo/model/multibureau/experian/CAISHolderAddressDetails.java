package com.softcell.gonogo.model.multibureau.experian;

public class CAISHolderAddressDetails {

    private String firstLineOfAddressNonNormalized;
    private String secondLineOfAddressNonNormalized;
    private String thirdLineOfAddressNonNormalized;
    private String cityNonNormalized;
    private String fifthLineOfAddressNonNormalized;
    private String stateNonNormalized;
    private String zipPostalCodeNonNormalized;
    private String countryCodenonNormalized;
    private String addressIndicatorNonNormalized;
    private String residenceCodeNonNormalized;


    public String getFirstLineOfAddressNonNormalized() {
        return firstLineOfAddressNonNormalized;
    }


    public void setFirstLineOfAddressNonNormalized(
            String firstLineOfAddressNonNormalized) {
        this.firstLineOfAddressNonNormalized = firstLineOfAddressNonNormalized;
    }


    public String getSecondLineOfAddressNonNormalized() {
        return secondLineOfAddressNonNormalized;
    }


    public void setSecondLineOfAddressNonNormalized(
            String secondLineOfAddressNonNormalized) {
        this.secondLineOfAddressNonNormalized = secondLineOfAddressNonNormalized;
    }


    public String getThirdLineOfAddressNonNormalized() {
        return thirdLineOfAddressNonNormalized;
    }


    public void setThirdLineOfAddressNonNormalized(
            String thirdLineOfAddressNonNormalized) {
        this.thirdLineOfAddressNonNormalized = thirdLineOfAddressNonNormalized;
    }


    public String getCityNonNormalized() {
        return cityNonNormalized;
    }


    public void setCityNonNormalized(String cityNonNormalized) {
        this.cityNonNormalized = cityNonNormalized;
    }


    public String getFifthLineOfAddressNonNormalized() {
        return fifthLineOfAddressNonNormalized;
    }


    public void setFifthLineOfAddressNonNormalized(
            String fifthLineOfAddressNonNormalized) {
        this.fifthLineOfAddressNonNormalized = fifthLineOfAddressNonNormalized;
    }


    public String getStateNonNormalized() {
        return stateNonNormalized;
    }


    public void setStateNonNormalized(String stateNonNormalized) {
        this.stateNonNormalized = stateNonNormalized;
    }


    public String getZipPostalCodeNonNormalized() {
        return zipPostalCodeNonNormalized;
    }


    public void setZipPostalCodeNonNormalized(String zipPostalCodeNonNormalized) {
        this.zipPostalCodeNonNormalized = zipPostalCodeNonNormalized;
    }


    public String getCountryCodenonNormalized() {
        return countryCodenonNormalized;
    }


    public void setCountryCodenonNormalized(String countryCodenonNormalized) {
        this.countryCodenonNormalized = countryCodenonNormalized;
    }


    public String getAddressIndicatorNonNormalized() {
        return addressIndicatorNonNormalized;
    }


    public void setAddressIndicatorNonNormalized(
            String addressIndicatorNonNormalized) {
        this.addressIndicatorNonNormalized = addressIndicatorNonNormalized;
    }


    public String getResidenceCodeNonNormalized() {
        return residenceCodeNonNormalized;
    }


    public void setResidenceCodeNonNormalized(String residenceCodeNonNormalized) {
        this.residenceCodeNonNormalized = residenceCodeNonNormalized;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAISHolderAddressDetails{");
        sb.append("firstLineOfAddressNonNormalized='").append(firstLineOfAddressNonNormalized).append('\'');
        sb.append(", secondLineOfAddressNonNormalized='").append(secondLineOfAddressNonNormalized).append('\'');
        sb.append(", thirdLineOfAddressNonNormalized='").append(thirdLineOfAddressNonNormalized).append('\'');
        sb.append(", cityNonNormalized='").append(cityNonNormalized).append('\'');
        sb.append(", fifthLineOfAddressNonNormalized='").append(fifthLineOfAddressNonNormalized).append('\'');
        sb.append(", stateNonNormalized='").append(stateNonNormalized).append('\'');
        sb.append(", zipPostalCodeNonNormalized='").append(zipPostalCodeNonNormalized).append('\'');
        sb.append(", countryCodenonNormalized='").append(countryCodenonNormalized).append('\'');
        sb.append(", addressIndicatorNonNormalized='").append(addressIndicatorNonNormalized).append('\'');
        sb.append(", residenceCodeNonNormalized='").append(residenceCodeNonNormalized).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAISHolderAddressDetails that = (CAISHolderAddressDetails) o;

        if (firstLineOfAddressNonNormalized != null ? !firstLineOfAddressNonNormalized.equals(that.firstLineOfAddressNonNormalized) : that.firstLineOfAddressNonNormalized != null)
            return false;
        if (secondLineOfAddressNonNormalized != null ? !secondLineOfAddressNonNormalized.equals(that.secondLineOfAddressNonNormalized) : that.secondLineOfAddressNonNormalized != null)
            return false;
        if (thirdLineOfAddressNonNormalized != null ? !thirdLineOfAddressNonNormalized.equals(that.thirdLineOfAddressNonNormalized) : that.thirdLineOfAddressNonNormalized != null)
            return false;
        if (cityNonNormalized != null ? !cityNonNormalized.equals(that.cityNonNormalized) : that.cityNonNormalized != null)
            return false;
        if (fifthLineOfAddressNonNormalized != null ? !fifthLineOfAddressNonNormalized.equals(that.fifthLineOfAddressNonNormalized) : that.fifthLineOfAddressNonNormalized != null)
            return false;
        if (stateNonNormalized != null ? !stateNonNormalized.equals(that.stateNonNormalized) : that.stateNonNormalized != null)
            return false;
        if (zipPostalCodeNonNormalized != null ? !zipPostalCodeNonNormalized.equals(that.zipPostalCodeNonNormalized) : that.zipPostalCodeNonNormalized != null)
            return false;
        if (countryCodenonNormalized != null ? !countryCodenonNormalized.equals(that.countryCodenonNormalized) : that.countryCodenonNormalized != null)
            return false;
        if (addressIndicatorNonNormalized != null ? !addressIndicatorNonNormalized.equals(that.addressIndicatorNonNormalized) : that.addressIndicatorNonNormalized != null)
            return false;
        return residenceCodeNonNormalized != null ? residenceCodeNonNormalized.equals(that.residenceCodeNonNormalized) : that.residenceCodeNonNormalized == null;
    }

    @Override
    public int hashCode() {
        int result = firstLineOfAddressNonNormalized != null ? firstLineOfAddressNonNormalized.hashCode() : 0;
        result = 31 * result + (secondLineOfAddressNonNormalized != null ? secondLineOfAddressNonNormalized.hashCode() : 0);
        result = 31 * result + (thirdLineOfAddressNonNormalized != null ? thirdLineOfAddressNonNormalized.hashCode() : 0);
        result = 31 * result + (cityNonNormalized != null ? cityNonNormalized.hashCode() : 0);
        result = 31 * result + (fifthLineOfAddressNonNormalized != null ? fifthLineOfAddressNonNormalized.hashCode() : 0);
        result = 31 * result + (stateNonNormalized != null ? stateNonNormalized.hashCode() : 0);
        result = 31 * result + (zipPostalCodeNonNormalized != null ? zipPostalCodeNonNormalized.hashCode() : 0);
        result = 31 * result + (countryCodenonNormalized != null ? countryCodenonNormalized.hashCode() : 0);
        result = 31 * result + (addressIndicatorNonNormalized != null ? addressIndicatorNonNormalized.hashCode() : 0);
        result = 31 * result + (residenceCodeNonNormalized != null ? residenceCodeNonNormalized.hashCode() : 0);
        return result;
    }
}
