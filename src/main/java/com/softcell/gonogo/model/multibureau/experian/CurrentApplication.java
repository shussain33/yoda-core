package com.softcell.gonogo.model.multibureau.experian;

public class CurrentApplication {
	
	private CurrentApplicationDetails currentApplicationDetails;

	public CurrentApplicationDetails getCurrentApplicationDetails() {
		return currentApplicationDetails;
	}

	public void setCurrentApplicationDetails(
			CurrentApplicationDetails currentApplicationDetails) {
		this.currentApplicationDetails = currentApplicationDetails;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CurrentApplication that = (CurrentApplication) o;

		return currentApplicationDetails != null ? currentApplicationDetails.equals(that.currentApplicationDetails) : that.currentApplicationDetails == null;
	}

	@Override
	public int hashCode() {
		return currentApplicationDetails != null ? currentApplicationDetails.hashCode() : 0;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CurrentApplication{");
		sb.append("currentApplicationDetails=").append(currentApplicationDetails);
		sb.append('}');
		return sb.toString();
	}

}
