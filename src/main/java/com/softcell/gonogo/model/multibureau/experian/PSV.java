package com.softcell.gonogo.model.multibureau.experian;

public class PSV {
	
	private BFHLExHl bfhlExHl;
	private HLCAD hlcad;
	private TelcosCAD telcosCAD;
	private MFCAD mfcad;
	private RetailCAD retailCAD;
	private TotalCAD totalCAD;
	private BFHLACAExHL bfhlacaExHL;
	private HlACA hlACA;
	private HLInACA hlInACA;
	private  MFACA mfaca;
	private TelcosACA telcosACA;
	private RetailACA  retailACA;
	private TotalACA totalACA;
	private BFHLICAExHL bfhlicaExHL;
	private MFICA mfica;
	private TelcosICA telcosICA;
	private RetailICA retailICA;
	private PSVCAPS psvcaps;
	private OwnCompanyData ownCompanyData;
	private OthCBInformation othCBInformation;
	private IndianMarketSpecificVar indianMarketSpecificVar;
	public BFHLExHl getBfhlExHl() {
		return bfhlExHl;
	}
	public void setBfhlExHl(BFHLExHl bfhlExHl) {
		this.bfhlExHl = bfhlExHl;
	}
	public HLCAD getHlcad() {
		return hlcad;
	}
	public void setHlcad(HLCAD hlcad) {
		this.hlcad = hlcad;
	}
	public TelcosCAD getTelcosCAD() {
		return telcosCAD;
	}
	public void setTelcosCAD(TelcosCAD telcosCAD) {
		this.telcosCAD = telcosCAD;
	}
	public MFCAD getMfcad() {
		return mfcad;
	}
	public void setMfcad(MFCAD mfcad) {
		this.mfcad = mfcad;
	}
	public RetailCAD getRetailCAD() {
		return retailCAD;
	}
	public void setRetailCAD(RetailCAD retailCAD) {
		this.retailCAD = retailCAD;
	}
	public TotalCAD getTotalCAD() {
		return totalCAD;
	}
	public void setTotalCAD(TotalCAD totalCAD) {
		this.totalCAD = totalCAD;
	}
	public BFHLACAExHL getBfhlacaExHL() {
		return bfhlacaExHL;
	}
	public void setBfhlacaExHL(BFHLACAExHL bfhlacaExHL) {
		this.bfhlacaExHL = bfhlacaExHL;
	}
	public HlACA getHlACA() {
		return hlACA;
	}
	public void setHlACA(HlACA hlACA) {
		this.hlACA = hlACA;
	}
	public HLInACA getHlInACA() {
		return hlInACA;
	}
	public void setHlInACA(HLInACA hlInACA) {
		this.hlInACA = hlInACA;
	}
	public MFACA getMfaca() {
		return mfaca;
	}
	public void setMfaca(MFACA mfaca) {
		this.mfaca = mfaca;
	}
	public TelcosACA getTelcosACA() {
		return telcosACA;
	}
	public void setTelcosACA(TelcosACA telcosACA) {
		this.telcosACA = telcosACA;
	}
	public RetailACA getRetailACA() {
		return retailACA;
	}
	public void setRetailACA(RetailACA retailACA) {
		this.retailACA = retailACA;
	}
	public TotalACA getTotalACA() {
		return totalACA;
	}
	public void setTotalACA(TotalACA totalACA) {
		this.totalACA = totalACA;
	}
	public BFHLICAExHL getBfhlicaExHL() {
		return bfhlicaExHL;
	}
	public void setBfhlicaExHL(BFHLICAExHL bfhlicaExHL) {
		this.bfhlicaExHL = bfhlicaExHL;
	}
	public MFICA getMfica() {
		return mfica;
	}
	public void setMfica(MFICA mfica) {
		this.mfica = mfica;
	}
	public TelcosICA getTelcosICA() {
		return telcosICA;
	}
	public void setTelcosICA(TelcosICA telcosICA) {
		this.telcosICA = telcosICA;
	}
	public RetailICA getRetailICA() {
		return retailICA;
	}
	public void setRetailICA(RetailICA retailICA) {
		this.retailICA = retailICA;
	}
	public PSVCAPS getPsvcaps() {
		return psvcaps;
	}
	public void setPsvcaps(PSVCAPS psvcaps) {
		this.psvcaps = psvcaps;
	}
	public OwnCompanyData getOwnCompanyData() {
		return ownCompanyData;
	}
	public void setOwnCompanyData(OwnCompanyData ownCompanyData) {
		this.ownCompanyData = ownCompanyData;
	}
	public OthCBInformation getOthCBInformation() {
		return othCBInformation;
	}
	public void setOthCBInformation(OthCBInformation othCBInformation) {
		this.othCBInformation = othCBInformation;
	}
	public IndianMarketSpecificVar getIndianMarketSpecificVar() {
		return indianMarketSpecificVar;
	}
	public void setIndianMarketSpecificVar(
			IndianMarketSpecificVar indianMarketSpecificVar) {
		this.indianMarketSpecificVar = indianMarketSpecificVar;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PSV{");
		sb.append("bfhlExHl=").append(bfhlExHl);
		sb.append(", hlcad=").append(hlcad);
		sb.append(", telcosCAD=").append(telcosCAD);
		sb.append(", mfcad=").append(mfcad);
		sb.append(", retailCAD=").append(retailCAD);
		sb.append(", totalCAD=").append(totalCAD);
		sb.append(", bfhlacaExHL=").append(bfhlacaExHL);
		sb.append(", hlACA=").append(hlACA);
		sb.append(", hlInACA=").append(hlInACA);
		sb.append(", mfaca=").append(mfaca);
		sb.append(", telcosACA=").append(telcosACA);
		sb.append(", retailACA=").append(retailACA);
		sb.append(", totalACA=").append(totalACA);
		sb.append(", bfhlicaExHL=").append(bfhlicaExHL);
		sb.append(", mfica=").append(mfica);
		sb.append(", telcosICA=").append(telcosICA);
		sb.append(", retailICA=").append(retailICA);
		sb.append(", psvcaps=").append(psvcaps);
		sb.append(", ownCompanyData=").append(ownCompanyData);
		sb.append(", othCBInformation=").append(othCBInformation);
		sb.append(", indianMarketSpecificVar=").append(indianMarketSpecificVar);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PSV psv = (PSV) o;

		if (bfhlExHl != null ? !bfhlExHl.equals(psv.bfhlExHl) : psv.bfhlExHl != null) return false;
		if (hlcad != null ? !hlcad.equals(psv.hlcad) : psv.hlcad != null) return false;
		if (telcosCAD != null ? !telcosCAD.equals(psv.telcosCAD) : psv.telcosCAD != null) return false;
		if (mfcad != null ? !mfcad.equals(psv.mfcad) : psv.mfcad != null) return false;
		if (retailCAD != null ? !retailCAD.equals(psv.retailCAD) : psv.retailCAD != null) return false;
		if (totalCAD != null ? !totalCAD.equals(psv.totalCAD) : psv.totalCAD != null) return false;
		if (bfhlacaExHL != null ? !bfhlacaExHL.equals(psv.bfhlacaExHL) : psv.bfhlacaExHL != null) return false;
		if (hlACA != null ? !hlACA.equals(psv.hlACA) : psv.hlACA != null) return false;
		if (hlInACA != null ? !hlInACA.equals(psv.hlInACA) : psv.hlInACA != null) return false;
		if (mfaca != null ? !mfaca.equals(psv.mfaca) : psv.mfaca != null) return false;
		if (telcosACA != null ? !telcosACA.equals(psv.telcosACA) : psv.telcosACA != null) return false;
		if (retailACA != null ? !retailACA.equals(psv.retailACA) : psv.retailACA != null) return false;
		if (totalACA != null ? !totalACA.equals(psv.totalACA) : psv.totalACA != null) return false;
		if (bfhlicaExHL != null ? !bfhlicaExHL.equals(psv.bfhlicaExHL) : psv.bfhlicaExHL != null) return false;
		if (mfica != null ? !mfica.equals(psv.mfica) : psv.mfica != null) return false;
		if (telcosICA != null ? !telcosICA.equals(psv.telcosICA) : psv.telcosICA != null) return false;
		if (retailICA != null ? !retailICA.equals(psv.retailICA) : psv.retailICA != null) return false;
		if (psvcaps != null ? !psvcaps.equals(psv.psvcaps) : psv.psvcaps != null) return false;
		if (ownCompanyData != null ? !ownCompanyData.equals(psv.ownCompanyData) : psv.ownCompanyData != null)
			return false;
		if (othCBInformation != null ? !othCBInformation.equals(psv.othCBInformation) : psv.othCBInformation != null)
			return false;
		return indianMarketSpecificVar != null ? indianMarketSpecificVar.equals(psv.indianMarketSpecificVar) : psv.indianMarketSpecificVar == null;
	}

	@Override
	public int hashCode() {
		int result = bfhlExHl != null ? bfhlExHl.hashCode() : 0;
		result = 31 * result + (hlcad != null ? hlcad.hashCode() : 0);
		result = 31 * result + (telcosCAD != null ? telcosCAD.hashCode() : 0);
		result = 31 * result + (mfcad != null ? mfcad.hashCode() : 0);
		result = 31 * result + (retailCAD != null ? retailCAD.hashCode() : 0);
		result = 31 * result + (totalCAD != null ? totalCAD.hashCode() : 0);
		result = 31 * result + (bfhlacaExHL != null ? bfhlacaExHL.hashCode() : 0);
		result = 31 * result + (hlACA != null ? hlACA.hashCode() : 0);
		result = 31 * result + (hlInACA != null ? hlInACA.hashCode() : 0);
		result = 31 * result + (mfaca != null ? mfaca.hashCode() : 0);
		result = 31 * result + (telcosACA != null ? telcosACA.hashCode() : 0);
		result = 31 * result + (retailACA != null ? retailACA.hashCode() : 0);
		result = 31 * result + (totalACA != null ? totalACA.hashCode() : 0);
		result = 31 * result + (bfhlicaExHL != null ? bfhlicaExHL.hashCode() : 0);
		result = 31 * result + (mfica != null ? mfica.hashCode() : 0);
		result = 31 * result + (telcosICA != null ? telcosICA.hashCode() : 0);
		result = 31 * result + (retailICA != null ? retailICA.hashCode() : 0);
		result = 31 * result + (psvcaps != null ? psvcaps.hashCode() : 0);
		result = 31 * result + (ownCompanyData != null ? ownCompanyData.hashCode() : 0);
		result = 31 * result + (othCBInformation != null ? othCBInformation.hashCode() : 0);
		result = 31 * result + (indianMarketSpecificVar != null ? indianMarketSpecificVar.hashCode() : 0);
		return result;
	}
}
