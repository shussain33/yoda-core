package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RATION-CARD-VARIATIONS")
@XmlAccessorType(XmlAccessType.FIELD)
public class RationCardVariation {

	@XmlElement(name="VARIATION")
	private List<Variation> variations;

	public List<Variation> getVariations() {
		return variations;
	}

	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}

	@Override
	public String toString() {
		return "RationCardVariation [variations=" + variations + "]";
	}

	
	
	

}
