package com.softcell.gonogo.model.multibureau.experian;

public class MFACA {

	private String TNOfMFACA;
	private String TotalBalMFACA;
	private String WCDStMFACA;
	private String WDSPr6MNTMFACA;
	private String WDSPr712MNTMFACA;
	private String AgeOfOldestMFACA;
	public String getTNOfMFACA() {
		return TNOfMFACA;
	}
	public void setTNOfMFACA(String tNOfMFACA) {
		TNOfMFACA = tNOfMFACA;
	}
	public String getTotalBalMFACA() {
		return TotalBalMFACA;
	}
	public void setTotalBalMFACA(String totalBalMFACA) {
		TotalBalMFACA = totalBalMFACA;
	}
	public String getWCDStMFACA() {
		return WCDStMFACA;
	}
	public void setWCDStMFACA(String wCDStMFACA) {
		WCDStMFACA = wCDStMFACA;
	}
	public String getWDSPr6MNTMFACA() {
		return WDSPr6MNTMFACA;
	}
	public void setWDSPr6MNTMFACA(String wDSPr6MNTMFACA) {
		WDSPr6MNTMFACA = wDSPr6MNTMFACA;
	}
	public String getWDSPr712MNTMFACA() {
		return WDSPr712MNTMFACA;
	}
	public void setWDSPr712MNTMFACA(String wDSPr712MNTMFACA) {
		WDSPr712MNTMFACA = wDSPr712MNTMFACA;
	}
	public String getAgeOfOldestMFACA() {
		return AgeOfOldestMFACA;
	}
	public void setAgeOfOldestMFACA(String ageOfOldestMFACA) {
		AgeOfOldestMFACA = ageOfOldestMFACA;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MFACA mfaca = (MFACA) o;

		if (TNOfMFACA != null ? !TNOfMFACA.equals(mfaca.TNOfMFACA) : mfaca.TNOfMFACA != null) return false;
		if (TotalBalMFACA != null ? !TotalBalMFACA.equals(mfaca.TotalBalMFACA) : mfaca.TotalBalMFACA != null)
			return false;
		if (WCDStMFACA != null ? !WCDStMFACA.equals(mfaca.WCDStMFACA) : mfaca.WCDStMFACA != null) return false;
		if (WDSPr6MNTMFACA != null ? !WDSPr6MNTMFACA.equals(mfaca.WDSPr6MNTMFACA) : mfaca.WDSPr6MNTMFACA != null)
			return false;
		if (WDSPr712MNTMFACA != null ? !WDSPr712MNTMFACA.equals(mfaca.WDSPr712MNTMFACA) : mfaca.WDSPr712MNTMFACA != null)
			return false;
		return AgeOfOldestMFACA != null ? AgeOfOldestMFACA.equals(mfaca.AgeOfOldestMFACA) : mfaca.AgeOfOldestMFACA == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfMFACA != null ? TNOfMFACA.hashCode() : 0;
		result = 31 * result + (TotalBalMFACA != null ? TotalBalMFACA.hashCode() : 0);
		result = 31 * result + (WCDStMFACA != null ? WCDStMFACA.hashCode() : 0);
		result = 31 * result + (WDSPr6MNTMFACA != null ? WDSPr6MNTMFACA.hashCode() : 0);
		result = 31 * result + (WDSPr712MNTMFACA != null ? WDSPr712MNTMFACA.hashCode() : 0);
		result = 31 * result + (AgeOfOldestMFACA != null ? AgeOfOldestMFACA.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MFACA{");
		sb.append("TNOfMFACA='").append(TNOfMFACA).append('\'');
		sb.append(", TotalBalMFACA='").append(TotalBalMFACA).append('\'');
		sb.append(", WCDStMFACA='").append(WCDStMFACA).append('\'');
		sb.append(", WDSPr6MNTMFACA='").append(WDSPr6MNTMFACA).append('\'');
		sb.append(", WDSPr712MNTMFACA='").append(WDSPr712MNTMFACA).append('\'');
		sb.append(", AgeOfOldestMFACA='").append(AgeOfOldestMFACA).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
