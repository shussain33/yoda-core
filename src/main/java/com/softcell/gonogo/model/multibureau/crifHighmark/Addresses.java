package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ADDRESSES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Addresses {

	/**
	 * @author Akshata
	 *
	 */
	@XmlElement(name="ADDRESS")
	private List<Address> Address;

	public List<Address> getAddress() {
		return Address;
	}

	public void setAddress(List<Address> address) {
		Address = address;
	}
	
	@Override
	public String toString() {
		return "Addresses [Address=" + Address + "]";
	}
}
