package com.softcell.gonogo.model.multibureau.experian;

public class NonCreditCAPSSummary {
	
	private String nonCreditCAPSLast7Days;
	private String nonCreditCAPSLast30Days; 
	private String nonCreditCAPSLast90Days;
    private String nonCreditCAPSLast180Days;
    
	public String getNonCreditCAPSLast7Days() {
		return nonCreditCAPSLast7Days;
	}
	public void setNonCreditCAPSLast7Days(String nonCreditCAPSLast7Days) {
		this.nonCreditCAPSLast7Days = nonCreditCAPSLast7Days;
	}
	public String getNonCreditCAPSLast30Days() {
		return nonCreditCAPSLast30Days;
	}
	public void setNonCreditCAPSLast30Days(String nonCreditCAPSLast30Days) {
		this.nonCreditCAPSLast30Days = nonCreditCAPSLast30Days;
	}
	public String getNonCreditCAPSLast90Days() {
		return nonCreditCAPSLast90Days;
	}
	public void setNonCreditCAPSLast90Days(String nonCreditCAPSLast90Days) {
		this.nonCreditCAPSLast90Days = nonCreditCAPSLast90Days;
	}
	public String getNonCreditCAPSLast180Days() {
		return nonCreditCAPSLast180Days;
	}
	public void setNonCreditCAPSLast180Days(String nonCreditCAPSLast180Days) {
		this.nonCreditCAPSLast180Days = nonCreditCAPSLast180Days;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NonCreditCAPSSummary that = (NonCreditCAPSSummary) o;

		if (nonCreditCAPSLast7Days != null ? !nonCreditCAPSLast7Days.equals(that.nonCreditCAPSLast7Days) : that.nonCreditCAPSLast7Days != null)
			return false;
		if (nonCreditCAPSLast30Days != null ? !nonCreditCAPSLast30Days.equals(that.nonCreditCAPSLast30Days) : that.nonCreditCAPSLast30Days != null)
			return false;
		if (nonCreditCAPSLast90Days != null ? !nonCreditCAPSLast90Days.equals(that.nonCreditCAPSLast90Days) : that.nonCreditCAPSLast90Days != null)
			return false;
		return nonCreditCAPSLast180Days != null ? nonCreditCAPSLast180Days.equals(that.nonCreditCAPSLast180Days) : that.nonCreditCAPSLast180Days == null;
	}

	@Override
	public int hashCode() {
		int result = nonCreditCAPSLast7Days != null ? nonCreditCAPSLast7Days.hashCode() : 0;
		result = 31 * result + (nonCreditCAPSLast30Days != null ? nonCreditCAPSLast30Days.hashCode() : 0);
		result = 31 * result + (nonCreditCAPSLast90Days != null ? nonCreditCAPSLast90Days.hashCode() : 0);
		result = 31 * result + (nonCreditCAPSLast180Days != null ? nonCreditCAPSLast180Days.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("NonCreditCAPSSummary{");
		sb.append("nonCreditCAPSLast7Days='").append(nonCreditCAPSLast7Days).append('\'');
		sb.append(", nonCreditCAPSLast30Days='").append(nonCreditCAPSLast30Days).append('\'');
		sb.append(", nonCreditCAPSLast90Days='").append(nonCreditCAPSLast90Days).append('\'');
		sb.append(", nonCreditCAPSLast180Days='").append(nonCreditCAPSLast180Days).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
