package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PhoneDomain {

	/**
	 * @author Dipak
	 *
	 *
	 */
	@JsonProperty("01")
	private String phoneType;
	
	@JsonProperty("02")
	private String phoneNumber;
	
	@JsonProperty("03")
	private String phoneExtn;
	
	@JsonProperty("04")
	private String stdCode;
	
	//START ACE
	@JsonProperty("05")
	private String phoneType2;
	
	@JsonProperty("06")
	private String phoneNumber2;

	@JsonProperty("07")
	private String phoneType3;
	
	@JsonProperty("08")
	private String phoneNumber3;
	//END ACE
	
	@JsonProperty("09")
	private String contactPrifix;
	
	
	public String getContactPrifix() {
		return contactPrifix;
	}

	public void setContactPrifix(String contactPrifix) {
		this.contactPrifix = contactPrifix;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneExtn() {
		return phoneExtn;
	}

	public void setPhoneExtn(String phoneExtn) {
		this.phoneExtn = phoneExtn;
	}

	public String getStdCode() {
		return stdCode;
	}

	public void setStdCode(String stdCode) {
		this.stdCode = stdCode;
	}

	public String getPhoneType2() {
		return phoneType2;
	}

	public void setPhoneType2(String phoneType2) {
		this.phoneType2 = phoneType2;
	}

	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	public String getPhoneType3() {
		return phoneType3;
	}

	public void setPhoneType3(String phoneType3) {
		this.phoneType3 = phoneType3;
	}

	public String getPhoneNumber3() {
		return phoneNumber3;
	}

	public void setPhoneNumber3(String phoneNumber3) {
		this.phoneNumber3 = phoneNumber3;
	}

	@Override
	public String toString() {
		return "PhoneDomain [phoneType=" + phoneType + ", phoneNumber="
				+ phoneNumber + ", phoneExtn=" + phoneExtn + ", stdCode="
				+ stdCode + ", phoneType2=" + phoneType2 + ", phoneNumber2="
				+ phoneNumber2 + ", phoneType3=" + phoneType3
				+ ", phoneNumber3=" + phoneNumber3 + ", contactPrifix="
				+ contactPrifix + "]";
	}

	
}
