package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LOAN-DETAILS")
@XmlAccessorType(XmlAccessType.FIELD)
public class LoanDetails {

	@XmlElement(name="ACCT-NUMBER")
	private String accountNumber;
	@XmlElement(name="CREDIT-GUARANTOR")
	private String creditGuarantor;
	@XmlElement(name="ACCT-TYPE")
	private String accountType;
	@XmlElement(name="DATE-REPORTED")
	private String dateReported;
	@XmlElement(name="OWNERSHIP-IND")
	private String ownershipIndicator;
	@XmlElement(name="ACCOUNT-STATUS")
	private String accountStatus;
	@XmlElement(name="DISBURSED-AMT")
	private String disbursedAmount;
	@XmlElement(name="DISBURSED-DATE")
	private String disbursedDate;
	@XmlElement(name="LAST-PAYMENT-DATE")
	private String lastPaymentDate;
	@XmlElement(name="CLOSED-DATE")
	private String closedDate;
	@XmlElement(name="INSTALLMENT-AMT")
	private String installmentAmount;
	@XmlElement(name="WRITE-OFF-AMT")
	private String writeOffAmount;
	@XmlElement(name="OVERDUE-AMT")
	private String overdueAmount;
	@XmlElement(name="CURRENT-BAL")
	private String currentBalance;
	@XmlElement(name="CREDIT-LIMIT")
	private String creditLimit;
	@XmlElement(name="ACCOUNT-REMARKS")
	private String accountRemarks; 
	@XmlElement(name="FREQUENCY")
	private String frequency ;
	@XmlElement(name="SECURITY-STATUS")
	private String securityStatus ;
	@XmlElement(name="ORIGINAL-TERM")
	private String originalTerm ;
	@XmlElement(name="TERM-TO-MATURITY")
	private String termToMaturity;
	@XmlElement(name="ACCT-IN-DISPUTE")
	private String accountInDispute;
	@XmlElement(name="SETTLEMENT-AMT")
	private String settlementAmt;
	@XmlElement(name="PRINCIPAL-WRITE-OFF-AMT")
	private String principalWriteOffAmt;
	@XmlElement(name="COMBINED-PAYMENT-HISTORY")
	private String combinedPaymentHistory;
	@XmlElement(name="MATCHED-TYPE")
	private String matchedType;
	//@XmlElement(name="LOAN-DETAILS")
	private List<LoanDetails> accountVariations_ = new ArrayList<LoanDetails>();
	//@XmlElement(name="LOAN-DETAILS")
	private List<SecurityDetails> securityDetailList = new ArrayList<SecurityDetails>();
	@XmlElement(name="REPAYMENT-TENURE")
	private String repaymentTenure;
	@XmlElement(name="CASH-LIMIT")
	private String cashLimit;
	@XmlElement(name="ACTUAL-PAYMENT")
	private String actualPayment;
	@XmlElement(name="SUIT-FILED_WILFUL-DEFAULT")
	private String suitFiledWilfulDefault;
	@XmlElement(name="WRITTEN-OFF_SETTLED-STATUS")
	private String writtenOffSetteldStatus;
	//@XmlElement(name="LOAN-DETAILS")
	private String status;
	//@XmlElement(name="LOAN-DETAILS")
	private String inquiryCnt;
	//@XmlElement(name="LOAN-DETAILS")
	private String dpd;
//	@XmlElement(name="LOAN-DETAILS")
	private String infoAsOn;
	@XmlElement(name="SECURITY-DETAILS")
	private String securityDetails;
	@XmlElement(name="LINKED-ACCOUNTS")
	private String linkedAccounts;
	@XmlElement(name="interestRate")
	private String interestRate;
	
	
	public String getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}

	public String getSecurityDetails() {
		return securityDetails;
	}

	public void setSecurityDetails(String securityDetails) {
		this.securityDetails = securityDetails;
	}

	public String getLinkedAccounts() {
		return linkedAccounts;
	}

	public void setLinkedAccounts(String linkedAccounts) {
		this.linkedAccounts = linkedAccounts;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getInquiryCnt() {
		return inquiryCnt;
	}
	
	public void setInquiryCnt(String inquiryCnt) {
		this.inquiryCnt = inquiryCnt;
	}
	
	public String getDpd() {
		return dpd;
	}
	
	public void setDpd(String dpd) {
		this.dpd = dpd;
	}
	
	public String getInfoAsOn() {
		return infoAsOn;
	}
	
	public void setInfoAsOn(String infoAsOn) {
		this.infoAsOn = infoAsOn;
	}
	
	public String getRepaymentTenure() {
		return repaymentTenure;
	}
	public void setRepaymentTenure(String repaymentTenure) {
		this.repaymentTenure = repaymentTenure;
	}
	public String getCashLimit() {
		return cashLimit;
	}
	public void setCashLimit(String cashLimit) {
		this.cashLimit = cashLimit;
	}
	public String getActualPayment() {
		return actualPayment;
	}
	public void setActualPayment(String actualPayment) {
		this.actualPayment = actualPayment;
	}
	public String getSuitFiledWilfulDefault() {
		return suitFiledWilfulDefault;
	}
	public void setSuitFiledWilfulDefault(String suitFiledWilfulDefault) {
		this.suitFiledWilfulDefault = suitFiledWilfulDefault;
	}
	public String getWrittenOffSetteldStatus() {
		return writtenOffSetteldStatus;
	}
	public void setWrittenOffSetteldStatus(String writtenOffSetteldStatus) {
		this.writtenOffSetteldStatus = writtenOffSetteldStatus;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCreditGuarantor() {
		return creditGuarantor;
	}
	public void setCreditGuarantor(String creditGuarantor) {
		this.creditGuarantor = creditGuarantor;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getDateReported() {
		return dateReported;
	}
	public void setDateReported(String dateReported) {
		this.dateReported = dateReported;
	}
	public String getOwnershipIndicator() {
		return ownershipIndicator;
	}
	public void setOwnershipIndicator(String ownershipIndicator) {
		this.ownershipIndicator = ownershipIndicator;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getDisbursedAmount() {
		return disbursedAmount;
	}
	public void setDisbursedAmount(String disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}
	public String getDisbursedDate() {
		return disbursedDate;
	}
	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}
	public String getLastPaymentDate() {
		return lastPaymentDate;
	}
	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}
	public String getClosedDate() {
		return closedDate;
	}
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}
	public String getOverdueAmount() {
		return overdueAmount;
	}
	public void setOverdueAmount(String overdueAmount) {
		this.overdueAmount = overdueAmount;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}
	public String getAccountRemarks() {
		return accountRemarks;
	}
	public void setAccountRemarks(String accountRemarks) {
		this.accountRemarks = accountRemarks;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getSecurityStatus() {
		return securityStatus;
	}
	public void setSecurityStatus(String securityStatus) {
		this.securityStatus = securityStatus;
	}
	public String getOriginalTerm() {
		return originalTerm;
	}
	public void setOriginalTerm(String originalTerm) {
		this.originalTerm = originalTerm;
	}
	public String getTermToMaturity() {
		return termToMaturity;
	}
	public void setTermToMaturity(String termToMaturity) {
		this.termToMaturity = termToMaturity;
	}
	public String getAccounttInDispute() {
		return accountInDispute;
	}
	public void setAccountInDispute(String accountInDispute) {
		this.accountInDispute = accountInDispute;
	}
	public String getSettlementAmt() {
		return settlementAmt;
	}
	public void setSettlementAmt(String settlementAmt) {
		this.settlementAmt = settlementAmt;
	}
	public String getPrincipalWriteOffAmt() {
		return principalWriteOffAmt;
	}
	public void setPrincipalWriteOffAmt(String principalWriteOffAmt) {
		this.principalWriteOffAmt = principalWriteOffAmt;
	}
	public String getCombinedPaymentHistory() {
		return combinedPaymentHistory;
	}
	public void setCombinedPaymentHistory(String combinedPaymentHistory) {
		this.combinedPaymentHistory = combinedPaymentHistory;
	}
	public String getMatchedType() {
		return matchedType;
	}
	public void setMatchedType(String matchedType) {
		this.matchedType = matchedType;
	}
	
	
	public String getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	
	public String getWriteOffAmount() {
		return writeOffAmount;
	}
	public void setWriteOffAmount(String writeOffAmount) {
		this.writeOffAmount = writeOffAmount;
	}
	public String getAccountInDispute() {
		return accountInDispute;
	}
	public List<LoanDetails> getAccountVariations_() {
		return accountVariations_;
	}
	public void setAccountVariations_(List<LoanDetails> accountVariations_) {
		this.accountVariations_ = accountVariations_;
	}
	public List<SecurityDetails> getSecurityDetailList() {
		return securityDetailList;
	}
	public void setSecurityDetailList(List<SecurityDetails> securityDetailList) {
		this.securityDetailList = securityDetailList;
	}
	@Override
	public String toString() {
		return "LoanDetails [accountNumber=" + accountNumber
				+ ", creditGuarantor=" + creditGuarantor + ", accountType="
				+ accountType + ", dateReported=" + dateReported
				+ ", ownershipIndicator=" + ownershipIndicator
				+ ", accountStatus=" + accountStatus + ", disbursedAmount="
				+ disbursedAmount + ", disbursedDate=" + disbursedDate
				+ ", lastPaymentDate=" + lastPaymentDate + ", closedDate="
				+ closedDate + ", installmentAmount=" + installmentAmount
				+ ", writeOffAmount=" + writeOffAmount + ", overdueAmount="
				+ overdueAmount + ", currentBalance=" + currentBalance
				+ ", creditLimit=" + creditLimit + ", accountRemarks="
				+ accountRemarks + ", frequency=" + frequency
				+ ", securityStatus=" + securityStatus + ", originalTerm="
				+ originalTerm + ", termToMaturity=" + termToMaturity
				+ ", accountInDispute=" + accountInDispute + ", settlementAmt="
				+ settlementAmt + ", principalWriteOffAmt="
				+ principalWriteOffAmt + ", combinedPaymentHistory="
				+ combinedPaymentHistory + ", matchedType=" + matchedType
				+ ", accountVariations_=" + accountVariations_
				+ ", securityDetailList=" + securityDetailList
				+ ", repaymentTenure=" + repaymentTenure + ", cashLimit="
				+ cashLimit + ", actualPayment=" + actualPayment
				+ ", suitFiledWilfulDefault=" + suitFiledWilfulDefault
				+ ", writtenOffSetteldStatus=" + writtenOffSetteldStatus
				+ ", status=" + status + ", inquiryCnt=" + inquiryCnt
				+ ", dpd=" + dpd + ", infoAsOn=" + infoAsOn
				+ ", securityDetails=" + securityDetails + ", linkedAccounts="
				+ linkedAccounts + ", interestRate=" + interestRate + "]";
	}

}
