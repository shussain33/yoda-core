package com.softcell.gonogo.model.multibureau.experian;

public class CAISHolderIdDetails {

	private String incomeTaxPan;
	private String panIssueDate;
	private String panExpirationDate;
	private String passportNumber;
	private String passportissueDate;
	private String passportExpirationDate;
	private String voterIdNumber;
	private String voteridIssueDate;
	private String voterIdExpirationDate;
	private String driverLicenceNumber;
	private String driverLicenceIssueDate;
	private String 	driverLicenceExpirationDate;
	private String rationCardNumber;
	private String rationCardissueDate;
	private String rationCardExpirationDate;
	private String universalIdNumber;
	private String universalIdIssueDate;
	private String universalIdExpirationdate;
	private String emailId;
	
	public String getIncomeTaxPan() {
		return incomeTaxPan;
	}
	public void setIncomeTaxPan(String incomeTaxPan) {
		this.incomeTaxPan = incomeTaxPan;
	}
	public String getPanIssueDate() {
		return panIssueDate;
	}
	public void setPanIssueDate(String panIssueDate) {
		this.panIssueDate = panIssueDate;
	}
	public String getPanExpirationDate() {
		return panExpirationDate;
	}
	public void setPanExpirationDate(String panExpirationDate) {
		this.panExpirationDate = panExpirationDate;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getPassportissueDate() {
		return passportissueDate;
	}
	public void setPassportissueDate(String passportissueDate) {
		this.passportissueDate = passportissueDate;
	}
	public String getPassportExpirationDate() {
		return passportExpirationDate;
	}
	public void setPassportExpirationDate(String passportExpirationDate) {
		this.passportExpirationDate = passportExpirationDate;
	}
	public String getVoterIdNumber() {
		return voterIdNumber;
	}
	public void setVoterIdNumber(String voterIdNumber) {
		this.voterIdNumber = voterIdNumber;
	}
	public String getVoteridIssueDate() {
		return voteridIssueDate;
	}
	public void setVoteridIssueDate(String voteridIssueDate) {
		this.voteridIssueDate = voteridIssueDate;
	}
	public String getVoterIdExpirationDate() {
		return voterIdExpirationDate;
	}
	public void setVoterIdExpirationDate(String voterIdExpirationDate) {
		this.voterIdExpirationDate = voterIdExpirationDate;
	}
	public String getDriverLicenceNumber() {
		return driverLicenceNumber;
	}
	public void setDriverLicenceNumber(String driverLicenceNumber) {
		this.driverLicenceNumber = driverLicenceNumber;
	}
	public String getDriverLicenceIssueDate() {
		return driverLicenceIssueDate;
	}
	public void setDriverLicenceIssueDate(String driverLicenceIssueDate) {
		this.driverLicenceIssueDate = driverLicenceIssueDate;
	}
	public String getDriverLicenceExpirationDate() {
		return driverLicenceExpirationDate;
	}
	public void setDriverLicenceExpirationDate(String driverLicenceExpirationDate) {
		this.driverLicenceExpirationDate = driverLicenceExpirationDate;
	}
	public String getRationCardNumber() {
		return rationCardNumber;
	}
	public void setRationCardNumber(String rationCardNumber) {
		this.rationCardNumber = rationCardNumber;
	}
	public String getRationCardissueDate() {
		return rationCardissueDate;
	}
	public void setRationCardissueDate(String rationCardissueDate) {
		this.rationCardissueDate = rationCardissueDate;
	}
	public String getRationCardExpirationDate() {
		return rationCardExpirationDate;
	}
	public void setRationCardExpirationDate(String rationCardExpirationDate) {
		this.rationCardExpirationDate = rationCardExpirationDate;
	}
	public String getUniversalIdNumber() {
		return universalIdNumber;
	}
	public void setUniversalIdNumber(String universalIdNumber) {
		this.universalIdNumber = universalIdNumber;
	}
	public String getUniversalIdIssueDate() {
		return universalIdIssueDate;
	}
	public void setUniversalIdIssueDate(String universalIdIssueDate) {
		this.universalIdIssueDate = universalIdIssueDate;
	}
	public String getUniversalIdExpirationdate() {
		return universalIdExpirationdate;
	}
	public void setUniversalIdExpirationdate(String universalIdExpirationdate) {
		this.universalIdExpirationdate = universalIdExpirationdate;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CAISHolderIdDetails{");
		sb.append("incomeTaxPan='").append(incomeTaxPan).append('\'');
		sb.append(", panIssueDate='").append(panIssueDate).append('\'');
		sb.append(", panExpirationDate='").append(panExpirationDate).append('\'');
		sb.append(", passportNumber='").append(passportNumber).append('\'');
		sb.append(", passportissueDate='").append(passportissueDate).append('\'');
		sb.append(", passportExpirationDate='").append(passportExpirationDate).append('\'');
		sb.append(", voterIdNumber='").append(voterIdNumber).append('\'');
		sb.append(", voteridIssueDate='").append(voteridIssueDate).append('\'');
		sb.append(", voterIdExpirationDate='").append(voterIdExpirationDate).append('\'');
		sb.append(", driverLicenceNumber='").append(driverLicenceNumber).append('\'');
		sb.append(", driverLicenceIssueDate='").append(driverLicenceIssueDate).append('\'');
		sb.append(", driverLicenceExpirationDate='").append(driverLicenceExpirationDate).append('\'');
		sb.append(", rationCardNumber='").append(rationCardNumber).append('\'');
		sb.append(", rationCardissueDate='").append(rationCardissueDate).append('\'');
		sb.append(", rationCardExpirationDate='").append(rationCardExpirationDate).append('\'');
		sb.append(", universalIdNumber='").append(universalIdNumber).append('\'');
		sb.append(", universalIdIssueDate='").append(universalIdIssueDate).append('\'');
		sb.append(", universalIdExpirationdate='").append(universalIdExpirationdate).append('\'');
		sb.append(", emailId='").append(emailId).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CAISHolderIdDetails that = (CAISHolderIdDetails) o;

		if (incomeTaxPan != null ? !incomeTaxPan.equals(that.incomeTaxPan) : that.incomeTaxPan != null) return false;
		if (panIssueDate != null ? !panIssueDate.equals(that.panIssueDate) : that.panIssueDate != null) return false;
		if (panExpirationDate != null ? !panExpirationDate.equals(that.panExpirationDate) : that.panExpirationDate != null)
			return false;
		if (passportNumber != null ? !passportNumber.equals(that.passportNumber) : that.passportNumber != null)
			return false;
		if (passportissueDate != null ? !passportissueDate.equals(that.passportissueDate) : that.passportissueDate != null)
			return false;
		if (passportExpirationDate != null ? !passportExpirationDate.equals(that.passportExpirationDate) : that.passportExpirationDate != null)
			return false;
		if (voterIdNumber != null ? !voterIdNumber.equals(that.voterIdNumber) : that.voterIdNumber != null)
			return false;
		if (voteridIssueDate != null ? !voteridIssueDate.equals(that.voteridIssueDate) : that.voteridIssueDate != null)
			return false;
		if (voterIdExpirationDate != null ? !voterIdExpirationDate.equals(that.voterIdExpirationDate) : that.voterIdExpirationDate != null)
			return false;
		if (driverLicenceNumber != null ? !driverLicenceNumber.equals(that.driverLicenceNumber) : that.driverLicenceNumber != null)
			return false;
		if (driverLicenceIssueDate != null ? !driverLicenceIssueDate.equals(that.driverLicenceIssueDate) : that.driverLicenceIssueDate != null)
			return false;
		if (driverLicenceExpirationDate != null ? !driverLicenceExpirationDate.equals(that.driverLicenceExpirationDate) : that.driverLicenceExpirationDate != null)
			return false;
		if (rationCardNumber != null ? !rationCardNumber.equals(that.rationCardNumber) : that.rationCardNumber != null)
			return false;
		if (rationCardissueDate != null ? !rationCardissueDate.equals(that.rationCardissueDate) : that.rationCardissueDate != null)
			return false;
		if (rationCardExpirationDate != null ? !rationCardExpirationDate.equals(that.rationCardExpirationDate) : that.rationCardExpirationDate != null)
			return false;
		if (universalIdNumber != null ? !universalIdNumber.equals(that.universalIdNumber) : that.universalIdNumber != null)
			return false;
		if (universalIdIssueDate != null ? !universalIdIssueDate.equals(that.universalIdIssueDate) : that.universalIdIssueDate != null)
			return false;
		if (universalIdExpirationdate != null ? !universalIdExpirationdate.equals(that.universalIdExpirationdate) : that.universalIdExpirationdate != null)
			return false;
		return emailId != null ? emailId.equals(that.emailId) : that.emailId == null;
	}

	@Override
	public int hashCode() {
		int result = incomeTaxPan != null ? incomeTaxPan.hashCode() : 0;
		result = 31 * result + (panIssueDate != null ? panIssueDate.hashCode() : 0);
		result = 31 * result + (panExpirationDate != null ? panExpirationDate.hashCode() : 0);
		result = 31 * result + (passportNumber != null ? passportNumber.hashCode() : 0);
		result = 31 * result + (passportissueDate != null ? passportissueDate.hashCode() : 0);
		result = 31 * result + (passportExpirationDate != null ? passportExpirationDate.hashCode() : 0);
		result = 31 * result + (voterIdNumber != null ? voterIdNumber.hashCode() : 0);
		result = 31 * result + (voteridIssueDate != null ? voteridIssueDate.hashCode() : 0);
		result = 31 * result + (voterIdExpirationDate != null ? voterIdExpirationDate.hashCode() : 0);
		result = 31 * result + (driverLicenceNumber != null ? driverLicenceNumber.hashCode() : 0);
		result = 31 * result + (driverLicenceIssueDate != null ? driverLicenceIssueDate.hashCode() : 0);
		result = 31 * result + (driverLicenceExpirationDate != null ? driverLicenceExpirationDate.hashCode() : 0);
		result = 31 * result + (rationCardNumber != null ? rationCardNumber.hashCode() : 0);
		result = 31 * result + (rationCardissueDate != null ? rationCardissueDate.hashCode() : 0);
		result = 31 * result + (rationCardExpirationDate != null ? rationCardExpirationDate.hashCode() : 0);
		result = 31 * result + (universalIdNumber != null ? universalIdNumber.hashCode() : 0);
		result = 31 * result + (universalIdIssueDate != null ? universalIdIssueDate.hashCode() : 0);
		result = 31 * result + (universalIdExpirationdate != null ? universalIdExpirationdate.hashCode() : 0);
		result = 31 * result + (emailId != null ? emailId.hashCode() : 0);
		return result;
	}
}
