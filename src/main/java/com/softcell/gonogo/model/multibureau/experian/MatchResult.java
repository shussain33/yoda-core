package com.softcell.gonogo.model.multibureau.experian;

public class MatchResult {

	private String exactMatch;

	public String getExactMatch() {
		return exactMatch;
	}

	public void setExactMatch(String exactMatch) {
		this.exactMatch = exactMatch;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MatchResult{");
		sb.append("exactMatch='").append(exactMatch).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MatchResult that = (MatchResult) o;

		return exactMatch != null ? exactMatch.equals(that.exactMatch) : that.exactMatch == null;
	}

	@Override
	public int hashCode() {
		return exactMatch != null ? exactMatch.hashCode() : 0;
	}
}
