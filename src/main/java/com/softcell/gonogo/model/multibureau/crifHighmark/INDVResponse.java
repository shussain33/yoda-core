package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INDV-RESPONSE-LIST")
@XmlAccessorType(XmlAccessType.FIELD)
public class INDVResponse {

	
	@XmlElement(name="MFI")
	private String mfi;
	@XmlElement(name="MFI-ID")
	private String mfiId;
	@XmlElement(name="BRANCH")
	private String branch;
	@XmlElement(name="KENDRA")
	private String kendra;
	@XmlElement(name="NAME")
	private String name;
	@XmlElement(name="DOB")
	private String dob;
	@XmlElement(name="AGE")
	private String age;
	@XmlElement(name="AGE-AS-ON")
	private String ageAsOn;
	@XmlElement(name="CNSMRMBRID")
	private String cnsmrmbrId; 
	@XmlElement(name="IDS")
	private Ids ids;
	@XmlElement(name="MATCHED-TYPE")
	private String matchedType;
	@XmlElement(name="GROUP-CREATION-DATE")
	private String reportDate;
	@XmlElement(name="INSERT-DATE")
	private String InsertDate;
	@XmlElement(name="PHONES")
    private Phones phones;
	@XmlElement(name="RELATIONS")
    private Relations relations;
	@XmlElement(name="ADDRESSES")
    private Addresses addresses;
	@XmlElement(name="GROUP-CREATION")
    private GroupDetails groupDetails;
	@XmlElement(name="LOAN-DETAIL")
    private LoanDetail loanDetail;
	@XmlElement(name="ENTITY-ID")
	private String entityId; 

	/**
	 * 
	 * Missed .... Emails DEtails 
	 */
	
	
	public Ids getIds() {
		return ids;
	}
	

	public String getEntityId() {
		return entityId;
	}


	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}


	public void setIds(Ids ids2) {
		this.ids = ids2;
	}
	

	
	public String getMfi() {
		return mfi;
	}
	
	public void setMfi(String mfi) {
		this.mfi = mfi;
	}
	
	public String getMfiId() {
		return mfiId;
	}
	
	public void setMfiId(String mfiId) {
		this.mfiId = mfiId;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getKendra() {
		return kendra;
	}
	
	public void setKendra(String kendra) {
		this.kendra = kendra;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDob() {
		return dob;
	}
	
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	public String getAge() {
		return age;
	}
	
	public void setAge(String age) {
		this.age = age;
	}
	
	public String getAgeAsOn() {
		return ageAsOn;
	}
	
	public void setAgeAsOn(String ageAsOn) {
		this.ageAsOn = ageAsOn;
	}
	
	public String getCnsmrmbrId() {
		return cnsmrmbrId;
	}
	
	public void setCnsmrmbrId(String cnsmrmbrId) {
		this.cnsmrmbrId = cnsmrmbrId;
	}
	
	public String getMatchedType() {
		return matchedType;
	}
	
	public void setMatchedType(String matchedType) {
		this.matchedType = matchedType;
	}
	
	public String getReportDate() {
		return reportDate;
	}
	
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	
	public String getInsertDate() {
		return InsertDate;
	}
	
	public void setInsertDate(String insertDate) {
		InsertDate = insertDate;
	}
	
	
	public LoanDetail getLoanDetail() {
		return loanDetail;
	}
	


	public void setLoanDetail(LoanDetail loanDetail) {
		this.loanDetail = loanDetail;
	}
	


	public GroupDetails getGroupDetails() {
		return groupDetails;
	}
	


	public void setGroupDetails(GroupDetails groupDetails) {
		this.groupDetails = groupDetails;
	}
	


	public Relations getRelations() {
		return relations;
	}
	


	public void setRelations(Relations relations) {
		this.relations = relations;
	}
	


	public Phones getPhones() {
		return phones;
	}
	


	public void setPhones(Phones phones) {
		this.phones = phones;
	}


	public Addresses getAddresses() {
		return addresses;
	}
	


	public void setAddresses(Addresses addressesOut) {
		this.addresses = addressesOut;
	}
	


	@Override
	public String toString() {
		return "INDVResponse [mfi=" + mfi + ", mfiId=" + mfiId + ", branch="
				+ branch + ", kendra=" + kendra + ", name=" + name + ", dob="
				+ dob + ", age=" + age + ", ageAsOn=" + ageAsOn
				+ ", cnsmrmbrId=" + cnsmrmbrId + ", ids=" + ids
				+ ", matchedType=" + matchedType + ", reportDate=" + reportDate
				+ ", InsertDate=" + InsertDate + ", phones=" + phones
				+ ", relations=" + relations + ", addresses=" + addresses
				+ ", groupDetails=" + groupDetails + ", loanDetail="
				+ loanDetail + ", entityId=" + entityId + "]";
	}

}
