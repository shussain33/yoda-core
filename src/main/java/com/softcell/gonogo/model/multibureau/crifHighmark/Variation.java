package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="VARIATION")
@XmlAccessorType(XmlAccessType.FIELD)
public class Variation {
	
	@XmlElement(name="VALUE")
	private String value;
	@XmlElement(name="REPORTED-DATE")
	private String reportedDate;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getReportedDate() {
		return reportedDate;
	}
	public void setReportedDate(String reportedDate) {
		this.reportedDate = reportedDate;
	}
	@Override
	public String toString() {
		return "Variation [value=" + value + ", reportedDate=" + reportedDate
				+ "]";
	}
	
}
