/**
 *
 */
package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author Dipak
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ID {

    @JsonProperty("01")
    private String panNo;
    @JsonProperty("04")
    private String passportNo;
    @JsonProperty("07")
    private String voterId;
    @JsonProperty("10")
    private String OtherId;
    @JsonProperty("13")
    private String uidNo;


    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getOtherId() {
        return OtherId;
    }

    public void setOtherId(String otherId) {
        OtherId = otherId;
    }

    public String getUidNo() {
        return uidNo;
    }

    public void setUidNo(String uidNo) {
        this.uidNo = uidNo;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ID{");
        sb.append("panNo='").append(panNo).append('\'');
        sb.append(", passportNo='").append(passportNo).append('\'');
        sb.append(", voterId='").append(voterId).append('\'');
        sb.append(", OtherId='").append(OtherId).append('\'');
        sb.append(", uidNo='").append(uidNo).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ID)) return false;
        ID id = (ID) o;
        return Objects.equal(getPanNo(), id.getPanNo()) &&
                Objects.equal(getPassportNo(), id.getPassportNo()) &&
                Objects.equal(getVoterId(), id.getVoterId()) &&
                Objects.equal(getOtherId(), id.getOtherId()) &&
                Objects.equal(getUidNo(), id.getUidNo());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPanNo(), getPassportNo(), getVoterId(), getOtherId(), getUidNo());
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder{
        private ID id = new ID();

        public ID build(){
            return this.id;
        }

        public Builder panNo(String panNo){
            this.id.panNo = panNo;
            return this;
        }

        public Builder passportNo(String passportNo){
            this.id.passportNo = passportNo;
            return this;
        }

        public Builder voterId(String voterId){
            this.id.voterId = voterId;
            return this;
        }

        public Builder OtherId(String OtherId){
            this.id.OtherId = OtherId;
            return this;
        }

        public Builder uidNo(String uidNo){
            this.id.uidNo = uidNo;
            return this;
        }
    }
}
