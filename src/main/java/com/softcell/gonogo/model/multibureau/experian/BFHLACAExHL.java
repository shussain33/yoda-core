package com.softcell.gonogo.model.multibureau.experian;

public class BFHLACAExHL {

    private String TNOfBFHLACAExHL;
    private String BalBFHLACAExHL;
    private String WCDStBFHLACAExHL;
    private String WDSPr6MNTBFHLACAExHL;
    private String WDSPr712MNTBFHLACAExHL;
    private String AgeOfOldestBFHLACAExHL;
    private String HCBPerRevAccBFHLACAExHL;
    private String TCBPerRevAccBFHLACAExHL;


    public String getTNOfBFHLACAExHL() {
        return TNOfBFHLACAExHL;
    }

    public void setTNOfBFHLACAExHL(String tNOfBFHLACAExHL) {
        TNOfBFHLACAExHL = tNOfBFHLACAExHL;
    }

    public String getBalBFHLACAExHL() {
        return BalBFHLACAExHL;
    }

    public void setBalBFHLACAExHL(String balBFHLACAExHL) {
        BalBFHLACAExHL = balBFHLACAExHL;
    }

    public String getWCDStBFHLACAExHL() {
        return WCDStBFHLACAExHL;
    }

    public void setWCDStBFHLACAExHL(String wCDStBFHLACAExHL) {
        WCDStBFHLACAExHL = wCDStBFHLACAExHL;
    }

    public String getWDSPr6MNTBFHLACAExHL() {
        return WDSPr6MNTBFHLACAExHL;
    }

    public void setWDSPr6MNTBFHLACAExHL(String wDSPr6MNTBFHLACAExHL) {
        WDSPr6MNTBFHLACAExHL = wDSPr6MNTBFHLACAExHL;
    }

    public String getWDSPr712MNTBFHLACAExHL() {
        return WDSPr712MNTBFHLACAExHL;
    }

    public void setWDSPr712MNTBFHLACAExHL(String wDSPr712MNTBFHLACAExHL) {
        WDSPr712MNTBFHLACAExHL = wDSPr712MNTBFHLACAExHL;
    }

    public String getAgeOfOldestBFHLACAExHL() {
        return AgeOfOldestBFHLACAExHL;
    }

    public void setAgeOfOldestBFHLACAExHL(String ageOfOldestBFHLACAExHL) {
        AgeOfOldestBFHLACAExHL = ageOfOldestBFHLACAExHL;
    }

    public String getHCBPerRevAccBFHLACAExHL() {
        return HCBPerRevAccBFHLACAExHL;
    }

    public void setHCBPerRevAccBFHLACAExHL(String hCBPerRevAccBFHLACAExHL) {
        HCBPerRevAccBFHLACAExHL = hCBPerRevAccBFHLACAExHL;
    }

    public String getTCBPerRevAccBFHLACAExHL() {
        return TCBPerRevAccBFHLACAExHL;
    }

    public void setTCBPerRevAccBFHLACAExHL(String tCBPerRevAccBFHLACAExHL) {
        TCBPerRevAccBFHLACAExHL = tCBPerRevAccBFHLACAExHL;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BFHLACAExHL{");
        sb.append("TNOfBFHLACAExHL='").append(TNOfBFHLACAExHL).append('\'');
        sb.append(", BalBFHLACAExHL='").append(BalBFHLACAExHL).append('\'');
        sb.append(", WCDStBFHLACAExHL='").append(WCDStBFHLACAExHL).append('\'');
        sb.append(", WDSPr6MNTBFHLACAExHL='").append(WDSPr6MNTBFHLACAExHL).append('\'');
        sb.append(", WDSPr712MNTBFHLACAExHL='").append(WDSPr712MNTBFHLACAExHL).append('\'');
        sb.append(", AgeOfOldestBFHLACAExHL='").append(AgeOfOldestBFHLACAExHL).append('\'');
        sb.append(", HCBPerRevAccBFHLACAExHL='").append(HCBPerRevAccBFHLACAExHL).append('\'');
        sb.append(", TCBPerRevAccBFHLACAExHL='").append(TCBPerRevAccBFHLACAExHL).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BFHLACAExHL that = (BFHLACAExHL) o;

        if (TNOfBFHLACAExHL != null ? !TNOfBFHLACAExHL.equals(that.TNOfBFHLACAExHL) : that.TNOfBFHLACAExHL != null)
            return false;
        if (BalBFHLACAExHL != null ? !BalBFHLACAExHL.equals(that.BalBFHLACAExHL) : that.BalBFHLACAExHL != null)
            return false;
        if (WCDStBFHLACAExHL != null ? !WCDStBFHLACAExHL.equals(that.WCDStBFHLACAExHL) : that.WCDStBFHLACAExHL != null)
            return false;
        if (WDSPr6MNTBFHLACAExHL != null ? !WDSPr6MNTBFHLACAExHL.equals(that.WDSPr6MNTBFHLACAExHL) : that.WDSPr6MNTBFHLACAExHL != null)
            return false;
        if (WDSPr712MNTBFHLACAExHL != null ? !WDSPr712MNTBFHLACAExHL.equals(that.WDSPr712MNTBFHLACAExHL) : that.WDSPr712MNTBFHLACAExHL != null)
            return false;
        if (AgeOfOldestBFHLACAExHL != null ? !AgeOfOldestBFHLACAExHL.equals(that.AgeOfOldestBFHLACAExHL) : that.AgeOfOldestBFHLACAExHL != null)
            return false;
        if (HCBPerRevAccBFHLACAExHL != null ? !HCBPerRevAccBFHLACAExHL.equals(that.HCBPerRevAccBFHLACAExHL) : that.HCBPerRevAccBFHLACAExHL != null)
            return false;
        return TCBPerRevAccBFHLACAExHL != null ? TCBPerRevAccBFHLACAExHL.equals(that.TCBPerRevAccBFHLACAExHL) : that.TCBPerRevAccBFHLACAExHL == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfBFHLACAExHL != null ? TNOfBFHLACAExHL.hashCode() : 0;
        result = 31 * result + (BalBFHLACAExHL != null ? BalBFHLACAExHL.hashCode() : 0);
        result = 31 * result + (WCDStBFHLACAExHL != null ? WCDStBFHLACAExHL.hashCode() : 0);
        result = 31 * result + (WDSPr6MNTBFHLACAExHL != null ? WDSPr6MNTBFHLACAExHL.hashCode() : 0);
        result = 31 * result + (WDSPr712MNTBFHLACAExHL != null ? WDSPr712MNTBFHLACAExHL.hashCode() : 0);
        result = 31 * result + (AgeOfOldestBFHLACAExHL != null ? AgeOfOldestBFHLACAExHL.hashCode() : 0);
        result = 31 * result + (HCBPerRevAccBFHLACAExHL != null ? HCBPerRevAccBFHLACAExHL.hashCode() : 0);
        result = 31 * result + (TCBPerRevAccBFHLACAExHL != null ? TCBPerRevAccBFHLACAExHL.hashCode() : 0);
        return result;
    }
}
