/**
 *
 */
package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author Dipak
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Name {

    @JsonProperty("01")
    private String custName1;

    @JsonProperty("02")
    private String custName2;

    @JsonProperty("03")
    private String custName3;

    @JsonProperty("04")
    private String custName4;

    @JsonProperty("05")
    private String custName5;

    public String getCustName1() {
        return custName1;
    }

    public void setCustName1(String custName1) {
        this.custName1 = custName1;
    }

    public String getCustName2() {
        return custName2;
    }

    public void setCustName2(String custName2) {
        this.custName2 = custName2;
    }

    public String getCustName3() {
        return custName3;
    }

    public void setCustName3(String custName3) {
        this.custName3 = custName3;
    }

    public String getCustName4() {
        return custName4;
    }

    public void setCustName4(String custName4) {
        this.custName4 = custName4;
    }

    public String getCustName5() {
        return custName5;
    }

    public void setCustName5(String custName5) {
        this.custName5 = custName5;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Name{");
        sb.append("custName1='").append(custName1).append('\'');
        sb.append(", custName2='").append(custName2).append('\'');
        sb.append(", custName3='").append(custName3).append('\'');
        sb.append(", custName4='").append(custName4).append('\'');
        sb.append(", custName5='").append(custName5).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Name)) return false;
        Name name = (Name) o;
        return Objects.equal(getCustName1(), name.getCustName1()) &&
                Objects.equal(getCustName2(), name.getCustName2()) &&
                Objects.equal(getCustName3(), name.getCustName3()) &&
                Objects.equal(getCustName4(), name.getCustName4()) &&
                Objects.equal(getCustName5(), name.getCustName5());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getCustName1(), getCustName2(), getCustName3(), getCustName4(), getCustName5());
    }

    public static Builder builder(){
        return new Builder();
    }
    public static class Builder{
        private Name name = new Name();

        public Name build(){
            return this.name;
        }

        public Builder custName1(String custName1){
            this.name.custName1 = custName1;
            return this;
        }

        public Builder custName2(String custName2){
            this.name.custName2 = custName2;
            return this;
        }

        public Builder custName3(String custName3){
            this.name.custName3 = custName3;
            return this;
        }
        public Builder custName4(String custName4){
            this.name.custName4 = custName4;
            return this;
        }
        public Builder custName5(String custName5){
            this.name.custName5 = custName5;
            return this;
        }

    }
}
