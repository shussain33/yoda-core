package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="PERSONAL-INFO-VARIATION")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonalInfoVariation {
	
	@XmlElement(name="NAME-VARIATIONS")
	private NameVariations nameVariations;
	@XmlElement(name="ADDRESS-VARIATIONS")
	private AddressVariations addressVariations;
	@XmlElement(name="PAN-VARIATIONS")
	private PanVariations panVariations;
	@XmlElement(name="DATE-OF-BIRTH-VARIATIONS")
	private DateOfBirthVariations dateOfBirthVariations;
	@XmlElement(name="VOTER-ID-VARIATIONS")
	private VoterIdVariations voterIdVariations;
	@XmlElement(name="PHONE-NUMBER-VARIATIONS")
	private PhoneNumberVariations phoneNumberVariations;
	@XmlElement(name="RATION-CARD-VARIATIONSS")
	private RationCardVariation rationCardVariation;
	@XmlElement(name="PASSPORT-VARIATIONS")
	private PassportVariation passportVariation;
	@XmlElement(name="EMAIL-VARIATIONS")
	private EmailVariation emailVariation;
	@XmlElement(name="DRIVING-LICENSE-VARIATIONS")
	private DrivingLicenseVariations drivingLicenseVariations;
	//@XmlElement(name="PHONE-NUMBER-VARIATIONS")
	//private UidVariations uidVariations;
	//@XmlElement(name="PHONE-NUMBER-VARIATIONS")
	//private OtherIdVariations otherIdVariations;
	/**
	 * Missed - passport, Ration card Variations
	 * @return
	 */
	
	/*
	 public OtherIdVariations getOtherIdVariations() {
		return otherIdVariations;
	}

	public void setOtherIdVariations(OtherIdVariations otherIdVariations) {
		this.otherIdVariations = otherIdVariations;
	}
	
	public UidVariations getUidVariations() {
		return uidVariations;
	}
	
	public void setUidVariations(UidVariations uidVariations) {
		this.uidVariations = uidVariations;
	}
	*/
	
	public NameVariations getNameVariations() {
		return nameVariations;
	}
	public void setNameVariations(NameVariations nameVariations) {
		this.nameVariations = nameVariations;
	}
	public AddressVariations getAddressVariations() {
		return addressVariations;
	}
	public void setAddressVariations(AddressVariations addressVariations) {
		this.addressVariations = addressVariations;
	}
	public PanVariations getPanVariations() {
		return panVariations;
	}
	public void setPanVariations(PanVariations panVariations) {
		this.panVariations = panVariations;
	}
	public DateOfBirthVariations getDateOfBirthVariations() {
		return dateOfBirthVariations;
	}
	public void setDateOfBirthVariations(DateOfBirthVariations dateOfBirthVariations) {
		this.dateOfBirthVariations = dateOfBirthVariations;
	}
	public VoterIdVariations getVoterIdVariations() {
		return voterIdVariations;
	}
	public void setVoterIdVariations(VoterIdVariations voterIdVariations) {
		this.voterIdVariations = voterIdVariations;
	}
	public PhoneNumberVariations getPhoneNumberVariations() {
		return phoneNumberVariations;
	}
	public void setPhoneNumberVariations(PhoneNumberVariations phoneNumberVariations) {
		this.phoneNumberVariations = phoneNumberVariations;
	}
	public RationCardVariation getRationCardVariation() {
		return rationCardVariation;
	}
	public void setRationCardVariation(RationCardVariation rationCardVariation) {
		this.rationCardVariation = rationCardVariation;
	}
	public PassportVariation getPassportVariation() {
		return passportVariation;
	}
	public void setPassportVariation(PassportVariation passportVariation) {
		this.passportVariation = passportVariation;
	}
	public EmailVariation getEmailVariation() {
		return emailVariation;
	}
	public void setEmailVariation(EmailVariation emailVariation) {
		this.emailVariation = emailVariation;
	}
	public DrivingLicenseVariations getDrivingLicenseVariations() {
		return drivingLicenseVariations;
	}
	public void setDrivingLicenseVariations(
			DrivingLicenseVariations drivingLicenseVariations) {
		this.drivingLicenseVariations = drivingLicenseVariations;
	}
	@Override
	public String toString() {
		return "PersonalInfoVariation [nameVariations=" + nameVariations
				+ ", addressVariations=" + addressVariations
				+ ", panVariations=" + panVariations
				+ ", dateOfBirthVariations=" + dateOfBirthVariations
				+ ", voterIdVariations=" + voterIdVariations
				+ ", phoneNumberVariations=" + phoneNumberVariations
				+ ", rationCardVariation=" + rationCardVariation
				+ ", passportVariation=" + passportVariation
				+ ", emailVariation=" + emailVariation
				+ ", drivingLicenseVariations=" + drivingLicenseVariations + "]";
	}
}
