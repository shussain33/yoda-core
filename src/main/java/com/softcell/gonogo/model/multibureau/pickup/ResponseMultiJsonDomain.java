package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseMultiJsonDomain {

    @JsonProperty("HEADER")
    private Header header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private Long acknowledgmentId;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("FINISHED")
    private List<Finished> finishedList;

    @JsonProperty("IN-PROCESS")
    private List<Finished> inprocessList;

    @JsonProperty("REJECT")
    private List<Finished> reject;

    @JsonProperty("MERGED_RESPONSE_OBJECT")
    private Object mergedResponse;

    @JsonProperty("ERRORS")
    private List<WarningAndError> errors;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Long getAcknowledgmentId() {
        return acknowledgmentId;
    }

    public void setAcknowledgmentId(Long acknowledgmentId) {
        this.acknowledgmentId = acknowledgmentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Finished> getFinishedList() {
        return finishedList;
    }

    public void setFinishedList(List<Finished> finishedList) {
        this.finishedList = finishedList;
    }

    public List<Finished> getInprocessList() {
        return inprocessList;
    }

    public void setInprocessList(List<Finished> inprocessList) {
        this.inprocessList = inprocessList;
    }

    public List<Finished> getReject() {
        return reject;
    }

    public Object getMergedResponse() {
        return mergedResponse;
    }

    public void setMergedResponse(Object mergedResponse) {
        this.mergedResponse = mergedResponse;
    }

    public void setReject(List<Finished> reject) {
        this.reject = reject;
    }

    public List<WarningAndError> getErrors() {
        return errors;
    }

    public void setErrors(List<WarningAndError> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ResponseMultiJsonDomain [header=");
        builder.append(header);
        builder.append(", acknowledgmentId=");
        builder.append(acknowledgmentId);
        builder.append(", status=");
        builder.append(status);
        builder.append(", finishedList=");
        builder.append(finishedList);
        builder.append(", inprocessList=");
        builder.append(inprocessList);
        builder.append(", mergedResponse=");
        builder.append(mergedResponse);
        builder.append(", reject=");
        builder.append(reject);
        builder.append(", errors=");
        builder.append(errors);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((acknowledgmentId == null) ? 0 : acknowledgmentId.hashCode());
        result = prime * result + ((errors == null) ? 0 : errors.hashCode());
        result = prime * result
                + ((finishedList == null) ? 0 : finishedList.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result
                + ((inprocessList == null) ? 0 : inprocessList.hashCode());
        result = prime * result + ((reject == null) ? 0 : reject.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ResponseMultiJsonDomain other = (ResponseMultiJsonDomain) obj;
        if (acknowledgmentId == null) {
            if (other.acknowledgmentId != null)
                return false;
        } else if (!acknowledgmentId.equals(other.acknowledgmentId))
            return false;
        if (errors == null) {
            if (other.errors != null)
                return false;
        } else if (!errors.equals(other.errors))
            return false;
        if (finishedList == null) {
            if (other.finishedList != null)
                return false;
        } else if (!finishedList.equals(other.finishedList))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (inprocessList == null) {
            if (other.inprocessList != null)
                return false;
        } else if (!inprocessList.equals(other.inprocessList))
            return false;
        if (reject == null) {
            if (other.reject != null)
                return false;
        } else if (!reject.equals(other.reject))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }

}
