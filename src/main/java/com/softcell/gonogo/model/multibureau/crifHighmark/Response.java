package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
public class Response {

	@XmlElement(name="LOAN-DETAILS")
	private LoanDetails loanDetail;

	public LoanDetails getLoanDetail() {
		return loanDetail;
	}

	public void setLoanDetail(LoanDetails loanDetail) {
		this.loanDetail = loanDetail;
	}

	@Override
	public String toString() {
		return "Response [loanDetail=" + loanDetail + "]";
	}

}
