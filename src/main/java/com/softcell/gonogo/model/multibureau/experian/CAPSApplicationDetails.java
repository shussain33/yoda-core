package com.softcell.gonogo.model.multibureau.experian;


public class CAPSApplicationDetails {

    private String subscriberCode;
    private String subscriberName;
    private String dateOfRequest;
    private String reportTime;
    private String reportNumber;
    private String enquiryReason;
    private String financePurpose;
    private String amountFinanced;
    private String durationOfAgreement;
    private CAPSOtherDetails capsOtherDetails;
    private CAPSApplicantAddressDetails capsApplicantAddressDetails;
    private CAPSApplicantAdditionalAddressDetails capsApplicantAdditionalAddressDetails;
    private CAPSApplicantDetails capsApplicantDetails;

    public String getSubscriberCode() {
        return subscriberCode;
    }

    public void setSubscriberCode(String subscriberCode) {
        this.subscriberCode = subscriberCode;
    }

    public String getSubscriberName() {
        return subscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }

    public String getDateOfRequest() {
        return dateOfRequest;
    }

    public void setDateOfRequest(String dateOfRequest) {
        this.dateOfRequest = dateOfRequest;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getEnquiryReason() {
        return enquiryReason;
    }

    public void setEnquiryReason(String enquiryReason) {
        this.enquiryReason = enquiryReason;
    }

    public String getFinancePurpose() {
        return financePurpose;
    }

    public void setFinancePurpose(String financePurpose) {
        this.financePurpose = financePurpose;
    }

    public String getAmountFinanced() {
        return amountFinanced;
    }

    public void setAmountFinanced(String amountFinanced) {
        this.amountFinanced = amountFinanced;
    }

    public String getDurationOfAgreement() {
        return durationOfAgreement;
    }

    public void setDurationOfAgreement(String durationOfAgreement) {
        this.durationOfAgreement = durationOfAgreement;
    }

    public CAPSOtherDetails getCapsOtherDetails() {
        return capsOtherDetails;
    }

    public void setCapsOtherDetails(CAPSOtherDetails capsOtherDetails) {
        this.capsOtherDetails = capsOtherDetails;
    }

    public CAPSApplicantAddressDetails getCapsApplicantAddressDetails() {
        return capsApplicantAddressDetails;
    }

    public void setCapsApplicantAddressDetails(
            CAPSApplicantAddressDetails capsApplicantAddressDetails) {
        this.capsApplicantAddressDetails = capsApplicantAddressDetails;
    }

    public CAPSApplicantAdditionalAddressDetails getCapsApplicantAdditionalAddressDetails() {
        return capsApplicantAdditionalAddressDetails;
    }

    public void setCapsApplicantAdditionalAddressDetails(
            CAPSApplicantAdditionalAddressDetails capsApplicantAdditionalAddressDetails) {
        this.capsApplicantAdditionalAddressDetails = capsApplicantAdditionalAddressDetails;
    }

    public CAPSApplicantDetails getCapsApplicantDetails() {
        return capsApplicantDetails;
    }

    public void setCapsApplicantDetails(CAPSApplicantDetails capsApplicantDetails) {
        this.capsApplicantDetails = capsApplicantDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAPSApplicationDetails{");
        sb.append("subscriberCode='").append(subscriberCode).append('\'');
        sb.append(", subscriberName='").append(subscriberName).append('\'');
        sb.append(", dateOfRequest='").append(dateOfRequest).append('\'');
        sb.append(", reportTime='").append(reportTime).append('\'');
        sb.append(", reportNumber='").append(reportNumber).append('\'');
        sb.append(", enquiryReason='").append(enquiryReason).append('\'');
        sb.append(", financePurpose='").append(financePurpose).append('\'');
        sb.append(", amountFinanced='").append(amountFinanced).append('\'');
        sb.append(", durationOfAgreement='").append(durationOfAgreement).append('\'');
        sb.append(", capsOtherDetails=").append(capsOtherDetails);
        sb.append(", capsApplicantAddressDetails=").append(capsApplicantAddressDetails);
        sb.append(", capsApplicantAdditionalAddressDetails=").append(capsApplicantAdditionalAddressDetails);
        sb.append(", capsApplicantDetails=").append(capsApplicantDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAPSApplicationDetails that = (CAPSApplicationDetails) o;

        if (subscriberCode != null ? !subscriberCode.equals(that.subscriberCode) : that.subscriberCode != null)
            return false;
        if (subscriberName != null ? !subscriberName.equals(that.subscriberName) : that.subscriberName != null)
            return false;
        if (dateOfRequest != null ? !dateOfRequest.equals(that.dateOfRequest) : that.dateOfRequest != null)
            return false;
        if (reportTime != null ? !reportTime.equals(that.reportTime) : that.reportTime != null) return false;
        if (reportNumber != null ? !reportNumber.equals(that.reportNumber) : that.reportNumber != null) return false;
        if (enquiryReason != null ? !enquiryReason.equals(that.enquiryReason) : that.enquiryReason != null)
            return false;
        if (financePurpose != null ? !financePurpose.equals(that.financePurpose) : that.financePurpose != null)
            return false;
        if (amountFinanced != null ? !amountFinanced.equals(that.amountFinanced) : that.amountFinanced != null)
            return false;
        if (durationOfAgreement != null ? !durationOfAgreement.equals(that.durationOfAgreement) : that.durationOfAgreement != null)
            return false;
        if (capsOtherDetails != null ? !capsOtherDetails.equals(that.capsOtherDetails) : that.capsOtherDetails != null)
            return false;
        if (capsApplicantAddressDetails != null ? !capsApplicantAddressDetails.equals(that.capsApplicantAddressDetails) : that.capsApplicantAddressDetails != null)
            return false;
        if (capsApplicantAdditionalAddressDetails != null ? !capsApplicantAdditionalAddressDetails.equals(that.capsApplicantAdditionalAddressDetails) : that.capsApplicantAdditionalAddressDetails != null)
            return false;
        return capsApplicantDetails != null ? capsApplicantDetails.equals(that.capsApplicantDetails) : that.capsApplicantDetails == null;
    }

    @Override
    public int hashCode() {
        int result = subscriberCode != null ? subscriberCode.hashCode() : 0;
        result = 31 * result + (subscriberName != null ? subscriberName.hashCode() : 0);
        result = 31 * result + (dateOfRequest != null ? dateOfRequest.hashCode() : 0);
        result = 31 * result + (reportTime != null ? reportTime.hashCode() : 0);
        result = 31 * result + (reportNumber != null ? reportNumber.hashCode() : 0);
        result = 31 * result + (enquiryReason != null ? enquiryReason.hashCode() : 0);
        result = 31 * result + (financePurpose != null ? financePurpose.hashCode() : 0);
        result = 31 * result + (amountFinanced != null ? amountFinanced.hashCode() : 0);
        result = 31 * result + (durationOfAgreement != null ? durationOfAgreement.hashCode() : 0);
        result = 31 * result + (capsOtherDetails != null ? capsOtherDetails.hashCode() : 0);
        result = 31 * result + (capsApplicantAddressDetails != null ? capsApplicantAddressDetails.hashCode() : 0);
        result = 31 * result + (capsApplicantAdditionalAddressDetails != null ? capsApplicantAdditionalAddressDetails.hashCode() : 0);
        result = 31 * result + (capsApplicantDetails != null ? capsApplicantDetails.hashCode() : 0);
        return result;
    }
}
