package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="PHONES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Phones {

@XmlElement(name="PHONE")
private List<Phone> phone;

public List<Phone> getPhone() {
	return phone;
}


public void setPhone(List<Phone> phone) {
	this.phone = phone;
}


@Override
public String toString() {
	return "Phones [phone=" + phone + "]";
}


	
	
}
