package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class FinishedInprocessObject {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@SerializedName("TRACKING-ID")
	private Long trackindId;
	
	@SerializedName("BUREAU")
	private String bureauName;
	
	@SerializedName("PRODUCT")
	private String productCode;
	
	@SerializedName("HTML-REPORT")
	private String htmlReport;
	
	@SerializedName("BUREAU-STRING")
	private String bureauOutput;
	
	@SerializedName("PDF-REPORT")
	private byte[] pdfReport;
	
	@SerializedName("RESPONSE_JSON_OBJECT")
	private Object jsonObject;
	
	@SerializedName("STATUS")
	private String status;
	
	
	public Long getTrackindId() {
		return trackindId;
	}
	public void setTrackindId(Long trackindId) {
		this.trackindId = trackindId;
	}
	public String getBureauName() {
		return bureauName;
	}
	public void setBureauName(String bureauName) {
		this.bureauName = bureauName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getHtmlReport() {
		return htmlReport;
	}
	public void setHtmlReport(String htmlReport) {
		this.htmlReport = htmlReport;
	}
	public String getBureauOutput() {
		return bureauOutput;
	}
	public void setBureauOutput(String bureauOutput) {
		this.bureauOutput = bureauOutput;
	}
	public byte[] getPdfReport() {
		return pdfReport;
	}
	public void setPdfReport(byte[] pdfReport) {
		this.pdfReport = pdfReport;
	}
	public Object getJsonObject() {
		return jsonObject;
	}
	public void setJsonObject(Object jsonObject) {
		this.jsonObject = jsonObject;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "FinishObject [trackindId=" + trackindId + ", bureauName="
				+ bureauName + ", productCode=" + productCode + ", htmlReport="
				+ htmlReport + ", bureauOutput=" + bureauOutput
				+ ", pdfReport=" + Arrays.toString(pdfReport) + ", jsonObject="
				+ jsonObject + ", status=" + status + "]";
	}
}
