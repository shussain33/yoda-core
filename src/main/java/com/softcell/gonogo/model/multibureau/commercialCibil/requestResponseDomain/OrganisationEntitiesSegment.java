package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrganisationEntitiesSegment {

	/**
	 * @author namratat
	 *
	 *
	 */
	@JsonProperty("01")
	private List<OrganisationEntity> organisationEntityList;

	public List<OrganisationEntity> getOrganisationEntityList() {
		return organisationEntityList;
	}

	public void setOrganisationEntityList(
			List<OrganisationEntity> organisationEntityList) {
		this.organisationEntityList = organisationEntityList;
	}

	@Override
	public String toString() {
		return "OrganisationEntitiesSegment [organisationEntityList="
				+ organisationEntityList + "]";
	}
	
}
