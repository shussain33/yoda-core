package com.softcell.gonogo.model.multibureau.experian;

public class CreditProfileHeader {

    private String enquiryUserName;
    private String reportDate;
    private String reporttime;
    private String version;
    private String reportNumber;
    private String subscriber;
    private String subscriberName;

    public String getEnquiryUserName() {
        return enquiryUserName;
    }

    public void setEnquiryUserName(String enquiryUserName) {
        this.enquiryUserName = enquiryUserName;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getReporttime() {
        return reporttime;
    }

    public void setReporttime(String reporttime) {
        this.reporttime = reporttime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getReportNumber() {
        return reportNumber;
    }

    public void setReportNumber(String reportNumber) {
        this.reportNumber = reportNumber;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(String subscriber) {
        this.subscriber = subscriber;
    }

    public String getSubscriberName() {
        return subscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CreditProfileHeader{");
        sb.append("enquiryUserName='").append(enquiryUserName).append('\'');
        sb.append(", reportDate='").append(reportDate).append('\'');
        sb.append(", reporttime='").append(reporttime).append('\'');
        sb.append(", version='").append(version).append('\'');
        sb.append(", reportNumber='").append(reportNumber).append('\'');
        sb.append(", subscriber='").append(subscriber).append('\'');
        sb.append(", subscriberName='").append(subscriberName).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreditProfileHeader that = (CreditProfileHeader) o;

        if (enquiryUserName != null ? !enquiryUserName.equals(that.enquiryUserName) : that.enquiryUserName != null)
            return false;
        if (reportDate != null ? !reportDate.equals(that.reportDate) : that.reportDate != null) return false;
        if (reporttime != null ? !reporttime.equals(that.reporttime) : that.reporttime != null) return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;
        if (reportNumber != null ? !reportNumber.equals(that.reportNumber) : that.reportNumber != null) return false;
        if (subscriber != null ? !subscriber.equals(that.subscriber) : that.subscriber != null) return false;
        return subscriberName != null ? subscriberName.equals(that.subscriberName) : that.subscriberName == null;
    }

    @Override
    public int hashCode() {
        int result = enquiryUserName != null ? enquiryUserName.hashCode() : 0;
        result = 31 * result + (reportDate != null ? reportDate.hashCode() : 0);
        result = 31 * result + (reporttime != null ? reporttime.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (reportNumber != null ? reportNumber.hashCode() : 0);
        result = 31 * result + (subscriber != null ? subscriber.hashCode() : 0);
        result = 31 * result + (subscriberName != null ? subscriberName.hashCode() : 0);
        return result;
    }
}
