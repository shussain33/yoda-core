package com.softcell.gonogo.model.multibureau;

import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author bhuvneshk
 */

/**
 * @author bhuvneshk
 *
 */

@Document(collection = "CoApplicantMultiBureauResponse")
public class MultiBureauCoApplicantResponse extends AuditEntity {

    private String applicantRefId;

    private String coApplicantRefId;

    private ResponseMultiJsonDomain multiBureauJsonRespose;
    /**
     * MBCorp Response
     */
    private OutputAckDomain mbCorpJsonResponse;
    /**
     * @return the applicantRefId
     */
    public String getApplicantRefId() {
        return applicantRefId;
    }

    /**
     * @param applicantRefId the applicantRefId to set
     */
    public void setApplicantRefId(String applicantRefId) {
        this.applicantRefId = applicantRefId;
    }

    /**
     * @return the coApplicantRefId
     */
    public String getCoApplicantRefId() {
        return coApplicantRefId;
    }

    /**
     * @param coApplicantRefId the coApplicantRefId to set
     */
    public void setCoApplicantRefId(String coApplicantRefId) {
        this.coApplicantRefId = coApplicantRefId;
    }

    /**
     * @return the multiBureauJsonRespose
     */
    public ResponseMultiJsonDomain getMultiBureauJsonRespose() {
        return multiBureauJsonRespose;
    }

    /**
     * @param multiBureauJsonRespose the multiBureauJsonRespose to set
     */
    public void setMultiBureauJsonRespose(ResponseMultiJsonDomain multiBureauJsonRespose) {
        this.multiBureauJsonRespose = multiBureauJsonRespose;
    }

    public OutputAckDomain getMbCorpJsonResponse() {
        return mbCorpJsonResponse;
    }

    public void setMbCorpJsonResponse(OutputAckDomain mbCorpJsonResponse) {
        this.mbCorpJsonResponse = mbCorpJsonResponse;
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MultiBureauCoApplicantResponse [applicantRefId=");
        builder.append(applicantRefId);
        builder.append(", coApplicantRefId=");
        builder.append(coApplicantRefId);
        builder.append(", multiBureauJsonRespose=");
        builder.append(multiBureauJsonRespose);
        builder.append(", mbCorpJsonResponse=");
        builder.append(mbCorpJsonResponse);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((applicantRefId == null) ? 0 : applicantRefId.hashCode());
        result = prime * result + ((coApplicantRefId == null) ? 0 : coApplicantRefId.hashCode());
        result = prime * result + ((multiBureauJsonRespose == null) ? 0 : multiBureauJsonRespose.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MultiBureauCoApplicantResponse other = (MultiBureauCoApplicantResponse) obj;
        if (applicantRefId == null) {
            if (other.applicantRefId != null)
                return false;
        } else if (!applicantRefId.equals(other.applicantRefId))
            return false;
        if (coApplicantRefId == null) {
            if (other.coApplicantRefId != null)
                return false;
        } else if (!coApplicantRefId.equals(other.coApplicantRefId))
            return false;
        if (multiBureauJsonRespose == null) {
            if (other.multiBureauJsonRespose != null)
                return false;
        } else if (!multiBureauJsonRespose.equals(other.multiBureauJsonRespose))
            return false;
        return true;
    }


}
