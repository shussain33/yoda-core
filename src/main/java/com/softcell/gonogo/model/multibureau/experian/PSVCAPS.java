package com.softcell.gonogo.model.multibureau.experian;

public class PSVCAPS {

	private String BFHLCAPSLast90Days;
	private String MFCAPSLast90Days;
	private String TelcosCAPSLast90Days;
	private String RetailCAPSLast90Days;
	public String getBFHLCAPSLast90Days() {
		return BFHLCAPSLast90Days;
	}
	public void setBFHLCAPSLast90Days(String bFHLCAPSLast90Days) {
		BFHLCAPSLast90Days = bFHLCAPSLast90Days;
	}
	public String getMFCAPSLast90Days() {
		return MFCAPSLast90Days;
	}
	public void setMFCAPSLast90Days(String mFCAPSLast90Days) {
		MFCAPSLast90Days = mFCAPSLast90Days;
	}
	public String getTelcosCAPSLast90Days() {
		return TelcosCAPSLast90Days;
	}
	public void setTelcosCAPSLast90Days(String telcosCAPSLast90Days) {
		TelcosCAPSLast90Days = telcosCAPSLast90Days;
	}
	public String getRetailCAPSLast90Days() {
		return RetailCAPSLast90Days;
	}
	public void setRetailCAPSLast90Days(String retailCAPSLast90Days) {
		RetailCAPSLast90Days = retailCAPSLast90Days;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PSVCAPS{");
		sb.append("BFHLCAPSLast90Days='").append(BFHLCAPSLast90Days).append('\'');
		sb.append(", MFCAPSLast90Days='").append(MFCAPSLast90Days).append('\'');
		sb.append(", TelcosCAPSLast90Days='").append(TelcosCAPSLast90Days).append('\'');
		sb.append(", RetailCAPSLast90Days='").append(RetailCAPSLast90Days).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		PSVCAPS psvcaps = (PSVCAPS) o;

		if (BFHLCAPSLast90Days != null ? !BFHLCAPSLast90Days.equals(psvcaps.BFHLCAPSLast90Days) : psvcaps.BFHLCAPSLast90Days != null)
			return false;
		if (MFCAPSLast90Days != null ? !MFCAPSLast90Days.equals(psvcaps.MFCAPSLast90Days) : psvcaps.MFCAPSLast90Days != null)
			return false;
		if (TelcosCAPSLast90Days != null ? !TelcosCAPSLast90Days.equals(psvcaps.TelcosCAPSLast90Days) : psvcaps.TelcosCAPSLast90Days != null)
			return false;
		return RetailCAPSLast90Days != null ? RetailCAPSLast90Days.equals(psvcaps.RetailCAPSLast90Days) : psvcaps.RetailCAPSLast90Days == null;
	}

	@Override
	public int hashCode() {
		int result = BFHLCAPSLast90Days != null ? BFHLCAPSLast90Days.hashCode() : 0;
		result = 31 * result + (MFCAPSLast90Days != null ? MFCAPSLast90Days.hashCode() : 0);
		result = 31 * result + (TelcosCAPSLast90Days != null ? TelcosCAPSLast90Days.hashCode() : 0);
		result = 31 * result + (RetailCAPSLast90Days != null ? RetailCAPSLast90Days.hashCode() : 0);
		return result;
	}
}
