package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DERIVED-ATTRIBUTES")
@XmlAccessorType(XmlAccessType.FIELD)
public class DerivedAttributes {
	
	@XmlElement(name="INQUIRIES-IN-LAST-SIX-MONTHS")
	private String inquiriesInLastSixMonth;
	@XmlElement(name="LENGTH-OF-CREDIT-HISTORY-YEAR")
	private String lengthOfCreditHistoryYear;
	@XmlElement(name="LENGTH-OF-CREDIT-HISTORY-MONTH")
	private String lengthOfCreditHistoryMonth;
	@XmlElement(name="AVERAGE-ACCOUNT-AGE-YEAR")
	private String averageAccountAgeYear;
	@XmlElement(name="AVERAGE-ACCOUNT-AGE-MONTH")
	private String averageAccountAgeMonth;
	@XmlElement(name="NEW-ACCOUNTS-IN-LAST-SIX-MONTHS")
	private String newAccountInLastSixMonths;
	@XmlElement(name="NEW-DELINQ-ACCOUNT-IN-LAST-SIX-MONTHS")
	private String newDlinqAccountInLastSixMonths;
	@XmlElement(name="INQUIRIES-IN-LAST-SIX-MONTHS")
	private String InquriesInLastSixMonths;
	
	
	public String getInquriesInLastSixMonths() {
		return InquriesInLastSixMonths;
	}
	
	public void setInquriesInLastSixMonths(String inquriesInLastSixMonths) {
		InquriesInLastSixMonths = inquriesInLastSixMonths;
	}
	
	public String getInquiriesInLastSixMonth() {
		return inquiriesInLastSixMonth;
	}
	public void setInquiriesInLastSixMonth(String inquiriesInLastSixMonth) {
		this.inquiriesInLastSixMonth = inquiriesInLastSixMonth;
	}
	public String getLengthOfCreditHistoryYear() {
		return lengthOfCreditHistoryYear;
	}
	public void setLengthOfCreditHistoryYear(String lengthOfCreditHistoryYear) {
		this.lengthOfCreditHistoryYear = lengthOfCreditHistoryYear;
	}
	public String getLengthOfCreditHistoryMonth() {
		return lengthOfCreditHistoryMonth;
	}
	public void setLengthOfCreditHistoryMonth(String lengthOfCreditHistoryMonth) {
		this.lengthOfCreditHistoryMonth = lengthOfCreditHistoryMonth;
	}
	public String getAverageAccountAgeYear() {
		return averageAccountAgeYear;
	}
	public void setAverageAccountAgeYear(String averageAccountAgeYear) {
		this.averageAccountAgeYear = averageAccountAgeYear;
	}
	public String getAverageAccountAgeMonth() {
		return averageAccountAgeMonth;
	}
	public void setAverageAccountAgeMonth(String averageAccountAgeMonth) {
		this.averageAccountAgeMonth = averageAccountAgeMonth;
	}
	public String getNewAccountInLastSixMonths() {
		return newAccountInLastSixMonths;
	}
	public void setNewAccountInLastSixMonths(String newAccountInLastSixMonths) {
		this.newAccountInLastSixMonths = newAccountInLastSixMonths;
	}
	public String getNewDlinqAccountInLastSixMonths() {
		return newDlinqAccountInLastSixMonths;
	}
	public void setNewDlinqAccountInLastSixMonths(
			String newDlinqAccountInLastSixMonths) {
		this.newDlinqAccountInLastSixMonths = newDlinqAccountInLastSixMonths;
	}
	
	@Override
	public String toString() {
		return "DerivedAttributes [inquiriesInLastSixMonth="
				+ inquiriesInLastSixMonth + ", lengthOfCreditHistoryYear="
				+ lengthOfCreditHistoryYear + ", lengthOfCreditHistoryMonth="
				+ lengthOfCreditHistoryMonth + ", averageAccountAgeYear="
				+ averageAccountAgeYear + ", averageAccountAgeMonth="
				+ averageAccountAgeMonth + ", newAccountInLastSixMonths="
				+ newAccountInLastSixMonths
				+ ", newDlinqAccountInLastSixMonths="
				+ newDlinqAccountInLastSixMonths + ", InquriesInLastSixMonths="
				+ InquriesInLastSixMonths + "]";
	}

}
