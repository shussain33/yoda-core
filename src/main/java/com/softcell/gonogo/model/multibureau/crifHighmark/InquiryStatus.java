package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INQUIRY-STATUS")
@XmlAccessorType(XmlAccessType.FIELD)
public class InquiryStatus {

	@XmlElement(name="INQUIRY")
	private Inquiry inquiry;

	public Inquiry getInquiry() {
		return inquiry;
	}

	public void setInquiry(Inquiry inquiry) {
		this.inquiry = inquiry;
	}

	@Override
	public String toString() {
		return "InquiryStatus [inquiry=" + inquiry + "]";
	}

}
