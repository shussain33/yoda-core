package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Header")
@XmlAccessorType(XmlAccessType.FIELD)
public class Header {
	@XmlElement(name="DATE-OF-REQUEST")
	private String dateOfRequest;
	@XmlElement(name="PREPARED-FOR")
	private String preparedFor;
	@XmlElement(name="PREPARED-FOR-ID")
	private String preparedForId;
	@XmlElement(name="DATE-OF-ISSUE")
	private String dateOfIssue;
	@XmlElement(name="REPORT-ID")
	private String reportId;
	@XmlElement(name="BATCH-ID")
	private String batchId;
	@XmlElement(name="STATUS")
	private String status;
	@XmlElement(name="ERROR")
	private String Error;
	
	public String getError() {
		return Error;
	}
	public void setError(String error) {
		Error = error;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateOfRequest() {
		return dateOfRequest;
	}
	public void setDateOfRequest(String dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}
	public String getPreparedFor() {
		return preparedFor;
	}
	public void setPreparedFor(String preparedFor) {
		this.preparedFor = preparedFor;
	}
	public String getDateOfIssue() {
		return dateOfIssue;
	}
	public void setDateOfIssue(String dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getPreparedForId() {
		return preparedForId;
	}
	public void setPreparedForId(String preparedForId) {
		this.preparedForId = preparedForId;
	}
	
	@Override
	public String toString() {
		return "Header [dateOfRequest=" + dateOfRequest + ", preparedFor="
				+ preparedFor + ", preparedForId=" + preparedForId
				+ ", dateOfIssue=" + dateOfIssue + ", reportId=" + reportId
				+ ", batchId=" + batchId + ", status=" + status + ", Error="
				+ Error + "]";
	}
}
