package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Main REquest object for MB
 *
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "MbRequest")
public class RequestJsonDomain extends AuditEntity {

    @JsonProperty("HEADER")
    private Header header;

    @JsonProperty("REQUEST")
    private RequestDomain request;

    @JsonIgnore
    private String gngRefId;

    @JsonIgnore
    private String coApplicantId;

    @JsonIgnore
    private Date dateTime = new Date();

    public static Builder builder() {
        return new Builder();
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public RequestDomain getRequest() {
        return request;
    }

    public void setRequest(RequestDomain request) {
        this.request = request;
    }

    public String getGngRefId() {
        return gngRefId;
    }

    public void setGngRefId(String gngRefId) {
        this.gngRefId = gngRefId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the coApplicantId
     */
    public String getCoApplicantId() {
        return coApplicantId;
    }

    /**
     * @param coApplicantId the coApplicantId to set
     */
    public void setCoApplicantId(String coApplicantId) {
        this.coApplicantId = coApplicantId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestJsonDomain{");
        sb.append("header=").append(header);
        sb.append(", request=").append(request);
        sb.append(", gngRefId='").append(gngRefId).append('\'');
        sb.append(", coApplicantId='").append(coApplicantId).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append('}');
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequestJsonDomain)) return false;
        RequestJsonDomain that = (RequestJsonDomain) o;
        return Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getRequest(), that.getRequest()) &&
                Objects.equal(getGngRefId(), that.getGngRefId()) &&
                Objects.equal(getCoApplicantId(), that.getCoApplicantId()) &&
                Objects.equal(getDateTime(), that.getDateTime());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHeader(), getRequest(), getGngRefId(), getCoApplicantId(), getDateTime());
    }

    public static class Builder {

        private RequestJsonDomain requestJsonDomain = new RequestJsonDomain();

        public RequestJsonDomain build() {
            return this.requestJsonDomain;
        }

        public Builder header(Header header) {
            this.requestJsonDomain.header = header;
            return this;
        }

        public Builder request(RequestDomain request){
            this.requestJsonDomain.request = request;
            return this;
        }

        public Builder gngRefId(String gngRefId){
            this.requestJsonDomain.gngRefId=gngRefId;
            return this;
        }

        public Builder coApplicantId(String coApplicantId){
            this.requestJsonDomain.coApplicantId=coApplicantId;
            return this;
        }

    }



}
