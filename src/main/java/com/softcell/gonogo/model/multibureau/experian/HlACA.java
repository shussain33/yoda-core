package com.softcell.gonogo.model.multibureau.experian;

public class HlACA {

	private String TNOfHLACA;
	private String BalHLACA;
	private String WCDStHLACA;
	private String WDSPr6MNTHLACA;
	private String WDSPr712MNTHLACA;
	private String AgeOfOldestHLACA;
	private String TNOfNDelHLInACA;
	private String TNOfDelHLInACA;
	
	public String getTNOfHLACA() {
		return TNOfHLACA;
	}
	public void setTNOfHLACA(String tNOfHLACA) {
		TNOfHLACA = tNOfHLACA;
	}
	public String getBalHLACA() {
		return BalHLACA;
	}
	public void setBalHLACA(String balHLACA) {
		BalHLACA = balHLACA;
	}
	public String getWCDStHLACA() {
		return WCDStHLACA;
	}
	public void setWCDStHLACA(String wCDStHLACA) {
		WCDStHLACA = wCDStHLACA;
	}
	public String getWDSPr6MNTHLACA() {
		return WDSPr6MNTHLACA;
	}
	public void setWDSPr6MNTHLACA(String wDSPr6MNTHLACA) {
		WDSPr6MNTHLACA = wDSPr6MNTHLACA;
	}
	public String getWDSPr712MNTHLACA() {
		return WDSPr712MNTHLACA;
	}
	public void setWDSPr712MNTHLACA(String wDSPr712MNTHLACA) {
		WDSPr712MNTHLACA = wDSPr712MNTHLACA;
	}
	public String getAgeOfOldestHLACA() {
		return AgeOfOldestHLACA;
	}
	public void setAgeOfOldestHLACA(String ageOfOldestHLACA) {
		AgeOfOldestHLACA = ageOfOldestHLACA;
	}
	public String getTNOfNDelHLInACA() {
		return TNOfNDelHLInACA;
	}
	public void setTNOfNDelHLInACA(String tNOfNDelHLInACA) {
		TNOfNDelHLInACA = tNOfNDelHLInACA;
	}
	public String getTNOfDelHLInACA() {
		return TNOfDelHLInACA;
	}
	public void setTNOfDelHLInACA(String tNOfDelHLInACA) {
		TNOfDelHLInACA = tNOfDelHLInACA;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("HlACA{");
		sb.append("TNOfHLACA='").append(TNOfHLACA).append('\'');
		sb.append(", BalHLACA='").append(BalHLACA).append('\'');
		sb.append(", WCDStHLACA='").append(WCDStHLACA).append('\'');
		sb.append(", WDSPr6MNTHLACA='").append(WDSPr6MNTHLACA).append('\'');
		sb.append(", WDSPr712MNTHLACA='").append(WDSPr712MNTHLACA).append('\'');
		sb.append(", AgeOfOldestHLACA='").append(AgeOfOldestHLACA).append('\'');
		sb.append(", TNOfNDelHLInACA='").append(TNOfNDelHLInACA).append('\'');
		sb.append(", TNOfDelHLInACA='").append(TNOfDelHLInACA).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		HlACA hlACA = (HlACA) o;

		if (TNOfHLACA != null ? !TNOfHLACA.equals(hlACA.TNOfHLACA) : hlACA.TNOfHLACA != null) return false;
		if (BalHLACA != null ? !BalHLACA.equals(hlACA.BalHLACA) : hlACA.BalHLACA != null) return false;
		if (WCDStHLACA != null ? !WCDStHLACA.equals(hlACA.WCDStHLACA) : hlACA.WCDStHLACA != null) return false;
		if (WDSPr6MNTHLACA != null ? !WDSPr6MNTHLACA.equals(hlACA.WDSPr6MNTHLACA) : hlACA.WDSPr6MNTHLACA != null)
			return false;
		if (WDSPr712MNTHLACA != null ? !WDSPr712MNTHLACA.equals(hlACA.WDSPr712MNTHLACA) : hlACA.WDSPr712MNTHLACA != null)
			return false;
		if (AgeOfOldestHLACA != null ? !AgeOfOldestHLACA.equals(hlACA.AgeOfOldestHLACA) : hlACA.AgeOfOldestHLACA != null)
			return false;
		if (TNOfNDelHLInACA != null ? !TNOfNDelHLInACA.equals(hlACA.TNOfNDelHLInACA) : hlACA.TNOfNDelHLInACA != null)
			return false;
		return TNOfDelHLInACA != null ? TNOfDelHLInACA.equals(hlACA.TNOfDelHLInACA) : hlACA.TNOfDelHLInACA == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfHLACA != null ? TNOfHLACA.hashCode() : 0;
		result = 31 * result + (BalHLACA != null ? BalHLACA.hashCode() : 0);
		result = 31 * result + (WCDStHLACA != null ? WCDStHLACA.hashCode() : 0);
		result = 31 * result + (WDSPr6MNTHLACA != null ? WDSPr6MNTHLACA.hashCode() : 0);
		result = 31 * result + (WDSPr712MNTHLACA != null ? WDSPr712MNTHLACA.hashCode() : 0);
		result = 31 * result + (AgeOfOldestHLACA != null ? AgeOfOldestHLACA.hashCode() : 0);
		result = 31 * result + (TNOfNDelHLInACA != null ? TNOfNDelHLInACA.hashCode() : 0);
		result = 31 * result + (TNOfDelHLInACA != null ? TNOfDelHLInACA.hashCode() : 0);
		return result;
	}
}
