package com.softcell.gonogo.model.multibureau.experian;

public class TelcosICA {

    private String TNOfNDelTelcosInACA;
    private String TNOfDelTelcosInACA;

    public String getTNOfNDelTelcosInACA() {
        return TNOfNDelTelcosInACA;
    }

    public void setTNOfNDelTelcosInACA(String tNOfNDelTelcosInACA) {
        TNOfNDelTelcosInACA = tNOfNDelTelcosInACA;
    }

    public String getTNOfDelTelcosInACA() {
        return TNOfDelTelcosInACA;
    }

    public void setTNOfDelTelcosInACA(String tNOfDelTelcosInACA) {
        TNOfDelTelcosInACA = tNOfDelTelcosInACA;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TelcosICA{");
        sb.append("TNOfNDelTelcosInACA='").append(TNOfNDelTelcosInACA).append('\'');
        sb.append(", TNOfDelTelcosInACA='").append(TNOfDelTelcosInACA).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TelcosICA telcosICA = (TelcosICA) o;

        if (TNOfNDelTelcosInACA != null ? !TNOfNDelTelcosInACA.equals(telcosICA.TNOfNDelTelcosInACA) : telcosICA.TNOfNDelTelcosInACA != null)
            return false;
        return TNOfDelTelcosInACA != null ? TNOfDelTelcosInACA.equals(telcosICA.TNOfDelTelcosInACA) : telcosICA.TNOfDelTelcosInACA == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfNDelTelcosInACA != null ? TNOfNDelTelcosInACA.hashCode() : 0;
        result = 31 * result + (TNOfDelTelcosInACA != null ? TNOfDelTelcosInACA.hashCode() : 0);
        return result;
    }
}
