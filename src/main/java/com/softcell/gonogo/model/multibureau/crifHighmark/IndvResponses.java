package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INDV-RESPONSES")
@XmlAccessorType(XmlAccessType.FIELD)
public class IndvResponses {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="PRIMARY-SUMMARY")
	private Summary primarySummary;
	@XmlElement(name="SUMMARY")
	private Summary summary;
	@XmlElement(name="SECONDARY-SUMMARY")
	private Summary secSummary;
	@XmlElement(name="INDV-RESPONSE-LIST")
	private List <INDVResponse> INDVResponseList;
	
	public List<INDVResponse> getINDVResponseList() {
		return INDVResponseList;
	}
	


	public void setINDVResponseList(List<INDVResponse> INDVresponseList) {
		INDVResponseList = INDVresponseList;
	}
	


	public Summary getSecSummary() {
		return secSummary;
	}
	

	public void setSecSummary(Summary secSummary) {
		this.secSummary = secSummary;
	}
	

	public Summary getPrimarySummary() {
		return primarySummary;
	}
	
	public void setPrimarySummary(Summary primarySummary) {
		this.primarySummary = primarySummary;
	}
	
	public Summary getSummary() {
		return summary;
	}
	
	public void setSummary(Summary summary) {
		this.summary = summary;
	}


	@Override
	public String toString() {
		return "IndvResponses [primarySummary=" + primarySummary + ", summary="
				+ summary + ", secSummary=" + secSummary + ", INDVResponseList="
				+ INDVResponseList + "]";
	}
	
}
