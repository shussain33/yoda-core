package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "CommercialMbRequest")
public class CommercialRequestResponseDomain {

	/**
	 * @author Dipak 
	 * 
	 *
	 *
	 */
	@Id
	@JsonIgnore
	private String gngRefId;
	
	@JsonIgnore
	private Date dateTime = new Date();
	
	
	@JsonProperty("HEADER")
	private Header header;
	
	@JsonProperty("REQUEST")
	private Request request;
	
	@JsonProperty("ACKNOWLEDGEMENT-ID")
	private Long acknowledgmentId;
	
	@JsonProperty("SRCH-LIST-ID")
	private String searchListId;
	
	@JsonProperty("REQUEST-ID")
	private String requestID;
	
	@JsonProperty("REFERENCE-NO")
	private String referenceNo;

	@JsonProperty("SRCH-LIST-ITEM-ACK-ID")
	private Long searchListItemAckId;

	@JsonProperty("RESPONSE-FORMAT")
	private List<String> responseFormat;
	
	
	public Header getHeader() {
		return header;
	}

	public void setHeader(Header header) {
		this.header = header;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public Long getAcknowledgmentId() {
		return acknowledgmentId;
	}

	public void setAcknowledgmentId(Long acknowledgmentId) {
		this.acknowledgmentId = acknowledgmentId;
	}

	public String getSearchListId() {
		return searchListId;
	}

	public void setSearchListId(String searchListId) {
		this.searchListId = searchListId;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public Long getSearchListItemAckId() {
		return searchListItemAckId;
	}

	public void setSearchListItemAckId(Long searchListItemAckId) {
		this.searchListItemAckId = searchListItemAckId;
	}

	public List<String> getResponseFormat() {
		return responseFormat;
	}

	public void setResponseFormat(List<String> responseFormat) {
		this.responseFormat = responseFormat;
	}

	@Override
	public String toString() {
		return "CommercialRequestResponseDomain [header=" + header
				+ ", request=" + request + ", acknowledgmentId="
				+ acknowledgmentId + ", searchListId=" + searchListId
				+ ", requestID=" + requestID + ", referenceNo=" + referenceNo
				+ ", searchListItemAckId=" + searchListItemAckId
				+ ", responseFormat=" + responseFormat + "]";
	}

	public String getGngRefId() {
		return gngRefId;
	}

	public void setGngRefId(String gngRefId) {
		this.gngRefId = gngRefId;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
}

