package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ID")
@XmlAccessorType(XmlAccessType.FIELD)
public class Id {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="TYPE")
	private String type;
	@XmlElement(name="VALUE")
	private String value;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Id [type=" + type + ", value=" + value + "]";
	}
	
}
