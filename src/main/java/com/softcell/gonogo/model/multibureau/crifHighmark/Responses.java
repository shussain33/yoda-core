package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RESPONSES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Responses {
	
	@XmlElement(name="RESPONSE")
	List<Response> response;

	public List<Response> getResponse() {
		return response;
	}

	public void setResponse(List<Response> response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "Responses [response=" + response + "]";
	}
}
