package com.softcell.gonogo.model.multibureau.crifHighmark;

public class INDVRespReportFile {

	InquiryStatus inquiryStatus;

	public InquiryStatus getInquiryStatus() {
		return inquiryStatus;
	}

	public void setInquiryStatus(InquiryStatus inquiryStatus) {
		this.inquiryStatus = inquiryStatus;
	}

	@Override
	public String toString() {
		return "ReportFile [inquiryStatus=" + inquiryStatus + "]";
	}
	
}
