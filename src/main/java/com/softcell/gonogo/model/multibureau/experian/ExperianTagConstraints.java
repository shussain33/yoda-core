package com.softcell.gonogo.model.multibureau.experian;

public class ExperianTagConstraints {

	public static class INProfileResponse{
		public static String HEADER="Header";
		public static String USER_MESSAGE="UserMessage";
		public static String CREDIT_PROFILE_HEADER="CreditProfileHeader";
		public static String CURRENT_APPLICATION="Current_Application";
		public static String CAIS_ACCOUNT="CAIS_Account";
		public static String MATCH_RESULT="Match_result";
		public static String CAPS="CAPS";
		public static String SCORE="SCORE";
		public static String PSV="PSV";
		public static String TOTAL_CAPS_SUMMARY="TotalCAPS_Summary";
		public static String NON_CREDIT_CAPS ="NonCreditCAPS";
	}
	
	public static class TotalCAPSSummary{
		public static String  TOTAL_CAPS_LAST_7_DAYS="TotalCAPSLast7Days";
		public static String  TOTAL_CAPS_LAST_30_DAYS="TotalCAPSLast30Days";
		public static String  TOTAL_CAPS_LAST_90_DAYS="TotalCAPSLast90Days";
		public static String  TOTAL_CAPS_LAST_180_DAYS="TotalCAPSLast180Days";
	}
	
	
	public  static class NonCreditCAPS{
		public static String  NON_CREDIT_CAPS_SUMMARY = "NonCreditCAPS_Summary";
		public static String CAPS_APPLICATION_DETAILS="CAPS_Application_Details";
		public static String CAPS_APPLICANT_DETAILS="CAPS_Applicant_Details";
		public static String CAPS_OTHER_DETAILS="CAPS_Other_Details";
		public static String CAPS_APPLICANT_ADDITIONAL_ADDRESS_DETAILS="CAPS_Applicant_Additional_Address_Details";
		
	}
	
	public static class NonCreditCAPS_Summary{
		public static String NON_CREDIT_CAPS_LAST_7_DAYS = "NonCreditCAPSLast7Days";
		public static String NON_CREDIT_CAPS_LAST_30_DAYS = "NonCreditCAPSLast30Days";
		public static String NON_CREDIT_CAPS_LAST_90_DAYS = "NonCreditCAPSLast90Days";
		public static String NON_CREDIT_CAPS_LAST_180_DAYS = "NonCreditCAPSLast180Days";
	}
	
	
	public static class Header{
		public static String SYSTEM_CODE="SystemCode";
		public static String MESSAGE_TEXT="MessageText";
		public static String REPORT_DATE="ReportDate";
		public static String REPORT_TIME="ReportTime";
	}

	public static class UserMessage{
		public static String USER_MESSAGE_TEXT="UserMessageText";
	}
	
	public static class CreditProfileHeader{
		public static String ENQUIRY_USERNAME="Enquiry_Username";
		public static String REPORT_DATE="ReportDate";
		public static String REPORT_TIME="ReportTime";
		public static String VERSION="Version";
		public static String REPORT_NUMBER="ReportNumber";
		public static String SUBSCRIBER="Subscriber";
		public static String SUBSCRIBER_NAME="Subscriber_Name";
	}

	public static class CurrentApplication{
		public static String CURRENT_APPLICATION_DETAILS="Current_Application_Details";
	}

	public static class CurrentApplicationDetails{
		public static String ENQUIRY_REASON="Enquiry_Reason";
		public static String FINANCE_PURPOSE="Finance_Purpose";
		public static String AMOUNT_FINANCED="Amount_Financed";
		public static String DURATION_OF_AGREEMENT="Duration_Of_Agreement";
		public static String CURRENT_APPLICANT_DETAILS="Current_Applicant_Details";
		public static String CURRENT_OTHER_DETAILS="Current_Other_Details";
		public static String CURRENT_APPLICANT_ADDRESS_DETAILS="Current_Applicant_Address_Details";
		public static String CURRENT_APPLICANT_ADDITIONAL_ADDRESS_DETAILS="Current_Applicant_Additional_Address_Details";
	}

	public static class CurrentApplicantDetails{
		public static String LAST_NAME = "Last_Name";
		public static String FIRST_NAME = "First_Name";
		public static String MIDDLE_NAME1 = "Middle_Name1";
		public static String MIDDLE_NAME2 = "Middle_Name2";
		public static String MIDDLE_NAME3 = "Middle_Name3";
		public static String GENDER_CODE = "Gender_Code";
		public static String INCOME_TAX_PAN = "IncomeTaxPan";
		public static String PAN_ISSUE_DATE = "PAN_Issue_Date";
		public static String PAN_EXPIRATION_DATE = "PAN_Expiration_Date";
		public static String PASSPORT_NUMBER = "Passport_Number";
		public static String PASSPORT_NUMBER_1 = "Passport_number";
		public static String PASSPORT_ISSUE_DATE = "Passport_Issue_Date";
		public static String PASSPORT_EXPIRATION_DATE = "Passport_Expiration_Date";
		public static String VOTER_S_IDENTITY_CARD = "Voter_s_Identity_Card";
		public static String VOTER_ID_ISSUE_DATE = "Voter_ID_Issue_Date";
		public static String VOTER_ID_EXPIRATION_DATE = "Voter_ID_Expiration_Date";
		public static String DRIVER_LICENSE_NUMBER = "Driver_License_Number";
		public static String DRIVER_LICENSE_ISSUE_DATE = "Driver_License_Issue_Date";
		public static String DRIVER_LICENSE_EXPIRATION_DATE = "Driver_License_Expiration_Date";
		public static String RATION_CARD_NUMBER = "Ration_Card_Number";
		public static String RATION_CARD_ISSUE_DATE = "Ration_Card_Issue_Date";
		public static String RATION_CARD_EXPIRATION_DATE = "Ration_Card_Expiration_Date";
		public static String UNIVERSAL_ID_NUMBER = "Universal_ID_Number";
		public static String UNIVERSAL_ID_ISSUE_DATE = "Universal_ID_Issue_Date";
		public static String UNIVERSAL_ID_EXPIRATION_DATE = "Universal_ID_Expiration_Date";
		public static String DATE_OF_BIRTH_APPLICANT = "Date_Of_Birth_Applicant";
		public static String TELEPHONE_NUMBER_APPLICANT_1ST = "Telephone_Number_Applicant_1st";
		public static String TELEPHONE_EXTENSION = "Telephone_Extension";
		public static String TELEPHONE_TYPE = "Telephone_Type";
		public static String MOBILE_PHONE_NUMBER = "MobilePhoneNumber";
		public static String EMAIL_ID = "EMailId";
		public static String EMAIL_ID_1 = "EMAILID";
	}

	public static class CurrentOtherDetails{
		public static String INCOME="Income";
		public static String MARITAL_STATUS="Marital_Status";
		public static String EMPLOYMENT_STATUS="Employment_Status";
		public static String TIME_WITH_EMPLOYER="Time_with_Employer";
		public static String NUMBER_OF_MAJOR_CREDIT_CARD_HELD="Number_of_Major_Credit_Card_Held"; 
	}

	public static class CurrentApplicantAddressDetails{
		public static String FLAT_NO_PLOT_NO_HOUSE_NO="FlatNoPlotNoHouseNo";
		public static String BLDG_NO_SOCIETY_NAME="BldgNoSocietyName";
		public static String ROAD_NO_NAME_AREA_LOCALITY="RoadNoNameAreaLocality";
		public static String CITY="City";
		public static String LANDMARK="Landmark";
		public static String STATE="State";
		public static String PIN_CODE="PINCode";
		public static String COUNTRY_CODE="Country_Code";
	}

	public static class CurrentApplicantAdditionalAddressDetails{
		public static String FLAT_NO_PLOT_NO_HOUSE_NO="FlatNoPlotNoHouseNo";
		public static String BLDG_NO_SOCIETY_NAME="BldgNoSocietyName";
		public static String ROAD_NO_NAME_AREA_LOCALITY="RoadNoNameAreaLocality";
		public static String CITY="City";
		public static String LANDMARK="Landmark";
		public static String STATE="State";
		public static String PIN_CODE="PINCode";
		public static String COUNTRY_CODE="Country_Code";
	}

	public static class CAISAccount{
		public static String CAIS_SUMMARY="CAIS_Summary";
		public static String CAIS_ACCOUNT_DETAILS="CAIS_Account_DETAILS";
	}
	
	public static class CAISSummary{
		public static String CREDIT_ACCOUNT="Credit_Account";
		public static String TOTAL_OUTSTANDING_BALANCE="Total_Outstanding_Balance";
		public static String PROVE_ID_SUMMARY="ProveID_Summary";
	}
	public static class ProveIdSummary{
		public static String NO_OF_DISTINCT_ACTIVE_LENDERS="Number_of_Distinct_Active_Lenders";
	}
	public static class CreditAccount{
		public static String CREDIT_ACCOUNT_TOTAL="CreditAccountTotal";
		public static String CREDIT_ACCOUNT_ACTIVE="CreditAccountActive";
		public static String CREDIT_ACCOUNT_DEFAULT="CreditAccountDefault";
		public static String CREDIT_ACCOUNT_CLOSED="CreditAccountClosed";
		public static String CAD_SUIT_FILED_CURRENT_BALANCE="CADSuitFiledCurrentBalance";
	}

	public static class TotalOutstandingBalance{
		public static String OUTSTANDING_BALANCE_SECURED="Outstanding_Balance_Secured";
		public static String OUTSTANDING_BALANCE_SECURED_PERCENTAGE="Outstanding_Balance_Secured_Percentage";
		public static String OUTSTANDING_BALANCE_UNSECURED="Outstanding_Balance_UnSecured";
		public static String OUTSTANDING_BALANCE_UNSECURED_PERCENTAGE="Outstanding_Balance_UnSecured_Percentage";
		public static String OUTSTANDING_BALANCE_ALL="Outstanding_Balance_All";
	}

	public static class CAISAccountDetails{
		public static String IDENTIFICATION_NUMBER = "Identification_Number";
		public static String SUBSCRIBER_NAME = "Subscriber_Name";
		public static String ACCOUNT_NUMBER = "Account_Number";
		public static String PORTFOLIO_TYPE = "Portfolio_Type";
		public static String ACCOUNT_TYPE = "Account_Type";
		public static String OPEN_DATE = "Open_Date";
		public static String HIGHEST_CREDIT_OR_ORIGINAL_LOAN_AMOUNT = "Highest_Credit_or_Original_Loan_Amount";
		public static String TERMS_DURATION = "Terms_Duration";
		public static String TERMS_FREQUENCY = "Terms_Frequency";
		public static String SCHEDULED_MONTHLY_PAYMENT_AMOUNT = "Scheduled_Monthly_Payment_Amount";
		public static String ACCOUNT_STATUS = "Account_Status";
		public static String PAYMENT_RATING = "Payment_Rating";
		public static String PAYMENT_HISTORY_PROFILE = "Payment_History_Profile";
		public static String SPECIAL_COMMENT = "Special_Comment";
		public static String CURRENT_BALANCE = "Current_Balance";
		public static String AMOUNT_PAST_DUE = "Amount_Past_Due";
		public static String ORIGINAL_CHARGE_OFF_AMOUNT = "Original_Charge_Off_Amount";
		public static String DATE_REPORTED = "Date_Reported";
		public static String DATE_OF_FIRST_DELINQUENCY = "Date_of_First_Delinquency";
		public static String DATE_CLOSED = "Date_Closed";
		public static String DATE_OF_LAST_PAYMENT = "Date_of_Last_Payment";
		public static String SUIT_FILED_WILLFUL_DEFAULT_WRITTEN_OFF_STATUS = "SuitFiledWillfulDefaultWrittenOffStatus";
		public static String SUIT_FILED__WILFUL_DEFAULT = "SuitFiled_WilfulDefault";
		public static String WRITTEN_OFF_SETTLED_STATUS = "Written_off_Settled_Status";
		public static String VALUE_OF_CREDITS_LAST_MONTH = "Value_of_Credits_Last_Month";
		public static String OCCUPATION_CODE = "Occupation_Code";
		public static String SETTLEMENT_AMOUNT = "Settlement_Amount";
		public static String VALUE_OF_COLLATERAL = "Value_of_Collateral";
		public static String TYPE_OF_COLLATERAL = "Type_of_Collateral";
		public static String WRITTEN_OFF_AMT_TOTAL = "Written_Off_Amt_Total";
		public static String WRITTEN_OFF_AMT_PRINCIPAL = "Written_Off_Amt_Principal";
		public static String RATE_OF_INTEREST = "Rate_of_Interest";
		public static String REPAYMENT_TENURE = "Repayment_Tenure";
		public static String PROMOTIONAL_RATE_FLAG = "Promotional_Rate_Flag";
		public static String INCOME = "Income";
		public static String INCOME_INDICATOR = "Income_Indicator";
		public static String INCOME_FREQUENCY_INDICATOR = "Income_Frequency_Indicator";
		public static String DEFAULT_STATUS_DATE = "DefaultStatusDate";
		public static String LITIGATION_STATUS_DATE = "LitigationStatusDate";
		public static String WRITE_OFF_STATUS_DATE = "WriteOffStatusDate";
		public static String DATE_OF_ADDITION = "DateOfAddition";
		public static String CURRENCY_CODE = "CurrencyCode";
		public static String SUBSCRIBER_COMMENTS = "Subscriber_comments";
		public static String CONSUMER_COMMENTS = "Consumer_comments";
		public static String ACCOUNT_HOLDERTYPE_CODE = "AccountHoldertypeCode";
		public static String CAIS_ACCOUNT_HISTORY = "CAIS_Account_History";
		public static String ADVANCED_ACCOUNT_HISTORY = "Advanced_Account_History";
		public static String CAIS_HOLDER_DETAILS = "CAIS_Holder_Details";
		public static String CAIS_HOLDER_ADDRESS_DETAILS = "CAIS_Holder_Address_Details";
		public static String CAIS_HOLDER_PHONE_DETAILS = "CAIS_Holder_Phone_Details";
		public static String CAIS_HOLDER_ID_DETAILS = "CAIS_Holder_ID_Details";
	}

	public static class CAISAccountHistory{
		public static String YEAR="Year";
		public static String MONTH="Month";
		public static String DAYS_PAST_DUE="Days_Past_Due";
		public static String ASSET_CLASSIFICATION="Asset_Classification";
	}

	public static class AdvancedAccountHistory{
		public static String YEAR="Year";
		public static String MONTH="Month";
		public static String CASH_LIMIT="Cash_Limit";
		public static String CREDIT_LIMIT_AMOUNT="Credit_Limit_Amount";
		public static String ACTUAL_PAYMENT_AMOUNT="Actual_Payment_Amount";
		public static String EMI_AMOUNT="EMI_Amount";
		public static String CURRENT_BALANCE="Current_Balance";
		public static String AMOUNT_PAST_DUE="Amount_Past_Due";
		public static String HIGHEST_CREDIT_OR_ORIGINAL_LOAN_AMOUNT="Highest_Credit_or_Original_Loan_Amount";
	}

	public static class CAISHolderDetails{
		public static String SURNAME_NON_NORMALIZED = "Surname_Non_Normalized";
		public static String FIRST_NAME_NON_NORMALIZED = "First_Name_Non_Normalized";
		public static String MIDDLE_NAME_1_NON_NORMALIZED = "Middle_Name_1_Non_Normalized";
		public static String MIDDLE_NAME_2_NON_NORMALIZED = "Middle_Name_2_Non_Normalized";
		public static String MIDDLE_NAME_3_NON_NORMALIZED = "Middle_Name_3_Non_Normalized";
		public static String ALIAS = "Alias";
		public static String GENDER_CODE = "Gender_Code";
		public static String INCOME_TAX_PAN = "Income_TAX_PAN";
		public static String PASSPORT_NUMBER = "Passport_Number";
		public static String PASSPORT_NUMBER_1 = "Passport_number";
		public static String VOTER_ID_NUMBER = "Voter_ID_Number";
		public static String DATE_OF_BIRTH = "Date_of_birth";
	}

	public static class CAISHolderAddressDetails{
		public static String FIRST_LINE_OF_ADDRESS_NON_NORMALIZED = "First_Line_Of_Address_non_normalized";
		public static String SECOND_LINE_OF_ADDRESS_NON_NORMALIZED = "Second_Line_Of_Address_non_normalized";
		public static String THIRD_LINE_OF_ADDRESS_NON_NORMALIZED = "Third_Line_Of_Address_non_normalized";
		public static String CITY_NON_NORMALIZED = "City_non_normalized";
		public static String FIFTH_LINE_OF_ADDRESS_NON_NORMALIZED = "Fifth_Line_Of_Address_non_normalized";
		public static String STATE_NON_NORMALIZED = "State_non_normalized";
		public static String ZIP_POSTAL_CODE_NON_NORMALIZED = "ZIP_Postal_Code_non_normalized";
		public static String COUNTRYCODE_NON_NORMALIZED = "CountryCode_non_normalized";
		public static String ADDRESS_INDICATOR_NON_NORMALIZED = "Address_indicator_non_normalized";
		public static String RESIDENCE_CODE_NON_NORMALIZED = "Residence_code_non_normalized";
		public static String RESIDENCE_CODE_NON_NORMALIZED_2 = "Residence_CODE_non_normalized";
	}

	public static class CAISHolderPhoneDetails{
		public static String TELEPHONE_NUMBER = "Telephone_Number";
		public static String TELEPHONE_TYPE = "Telephone_Type";
		public static String TELEPHONE_EXTENSION = "Telephone_Extension";
		public static String MOBILE_TELEPHONE_NUMBER = "Mobile_Telephone_Number";
		public static String FAX_NUMBER = "FaxNumber";
		public static String EMAIL_ID = "EMailId";
		public static String EMAIL_ID_1 = "EMAILID";
	}

	public static class CAISHolderIDDetails{
		public static String INCOME_TAX_PAN = "Income_TAX_PAN";
		public static String PAN_ISSUE_DATE = "PAN_Issue_Date";
		public static String PAN_EXPIRATION_DATE = "PAN_Expiration_Date";
		public static String PASSPORT_NUMBER = "Passport_Number";
		public static String PASSPORT_NUMBER_1 = "Passport_number";
		public static String PASSPORT_ISSUE_DATE = "Passport_Issue_Date";
		public static String PASSPORT_EXPIRATION_DATE = "Passport_Expiration_Date";
		public static String VOTER_ID_NUMBER = "Voter_ID_Number";
		public static String VOTER_ID_ISSUE_DATE = "Voter_ID_Issue_Date";
		public static String VOTER_ID_EXPIRATION_DATE = "Voter_ID_Expiration_Date";
		public static String DRIVER_LICENSE_NUMBER = "Driver_License_Number";
		public static String DRIVER_LICENSE_ISSUE_DATE = "Driver_License_Issue_Date";
		public static String DRIVER_LICENSE_EXPIRATION_DATE = "Driver_License_Expiration_Date";
		public static String RATION_CARD_NUMBER = "Ration_Card_Number";
		public static String RATION_CARD_ISSUE_DATE = "Ration_Card_Issue_Date";
		public static String RATION_CARD_EXPIRATION_DATE = "Ration_Card_Expiration_Date";
		public static String UNIVERSAL_ID_NUMBER = "Universal_ID_Number";
		public static String UNIVERSAL_ID_ISSUE_DATE = "Universal_ID_Issue_Date";
		public static String UNIVERSAL_ID_EXPIRATION_DATE = "Universal_ID_Expiration_Date";
		public static String EMAIL_ID = "EMailId";
		public static String EMAIL_ID_1 = "EMAILID";

	}

	public static class MatchResult{
		public static String EXACT_MATCH="Exact_match";
	}

	public static class CAPS{
		public static String CAPS_SUMMARY="CAPS_Summary";
		public static String CAPS_APPLICATION_DETAILS="CAPS_Application_Details";
	}

	public static class CAPSSummary{
		public static String CAPS_LAST7_DAYS="CAPSLast7Days";
		public static String CAPS_LAST30_DAYS="CAPSLast30Days";
		public static String CAPS_LAST90_DAYS="CAPSLast90Days";
		public static String CAPS_LAST180_DAYS="CAPSLast180Days";
	}

	public static class CAPSApplicationDetails{
		public static String SUBSCRIBER_CODE="Subscriber_Code";
		public static String SUBSCRIBER_CODE_1="Subscriber_code";
		public static String SUBSCRIBER_NAME="Subscriber_Name";
		public static String DATE_OF_REQUEST="Date_of_Request";
		public static String REPORT_TIME="ReportTime";
		public static String REPORT_NUMBER="ReportNumber";
		public static String ENQUIRY_REASON="Enquiry_Reason";
		public static String FINANCE_PURPOSE="Finance_Purpose";
		public static String AMOUNT_FINANCED="Amount_Financed";
		public static String DURATION_OF_AGREEMENT="Duration_Of_Agreement";
		public static String CAPS_APPLICANT_DETAILS="CAPS_Applicant_Details";
		public static String CAPS_OTHER_DETAILS="CAPS_Other_Details";
		public static String CAPS_APPLICANT_ADDRESS_DETAILS="CAPS_Applicant_Address_Details";
		public static String CAPS_APPLICANT_ADDITIONAL_ADDRESS_DETAILS="CAPS_Applicant_Additional_Address_Details";
	}

	public static class CAPSApplicantDetails{
		public static String LAST_NAME = "Last_Name";
		public static String FIRST_NAME = "First_Name";
		public static String MIDDLE_NAME1 = "Middle_Name1";
		public static String MIDDLE_NAME2 = "Middle_Name2";
		public static String MIDDLE_NAME3 = "Middle_Name3";
		public static String GENDER_CODE = "Gender_Code";
		public static String INCOME_TAX_PAN = "IncomeTaxPan";
		public static String PAN_ISSUE_DATE = "PAN_Issue_Date";
		public static String PAN_EXPIRATION_DATE = "PAN_Expiration_Date";
		public static String PASSPORT_NUMBER = "Passport_Number";
		public static String PASSPORT_NUMBER_1 = "Passport_number";
		public static String PASSPORT_ISSUE_DATE = "Passport_Issue_Date";
		public static String PASSPORT_EXPIRATION_DATE = "Passport_Expiration_Date";
		public static String VOTER_S_IDENTITY_CARD = "Voter_s_Identity_Card";
		public static String VOTER_ID_ISSUE_DATE = "Voter_ID_Issue_Date";
		public static String VOTER_ID_EXPIRATION_DATE = "Voter_ID_Expiration_Date";
		public static String DRIVER_LICENSE_NUMBER = "Driver_License_Number";
		public static String DRIVER_LICENSE_ISSUE_DATE = "Driver_License_Issue_Date";
		public static String DRIVER_LICENSE_EXPIRATION_DATE = "Driver_License_Expiration_Date";
		public static String RATION_CARD_NUMBER = "Ration_Card_Number";
		public static String RATION_CARD_ISSUE_DATE = "Ration_Card_Issue_Date";
		public static String RATION_CARD_EXPIRATION_DATE = "Ration_Card_Expiration_Date";
		public static String UNIVERSAL_ID_NUMBER = "Universal_ID_Number";
		public static String UNIVERSAL_ID_ISSUE_DATE = "Universal_ID_Issue_Date";
		public static String UNIVERSAL_ID_EXPIRATION_DATE = "Universal_ID_Expiration_Date";
		public static String DATE_OF_BIRTH_APPLICANT = "Date_Of_Birth_Applicant";
		public static String TELEPHONE_NUMBER_APPLICANT_1ST = "Telephone_Number_Applicant_1st";
		public static String TELEPHONE_TYPE = "Telephone_Type";
		public static String TELEPHONE_EXTENSION = "Telephone_Extension";
		public static String MOBILE_PHONE_NUMBER = "MobilePhoneNumber";
		public static String EMAIL_ID = "EMailId";
		public static String EMAIL_ID_1 = "EMAILID";
	}

	public static class CAPSOtherDetails{
		public static String INCOME="Income";
		public static String MARITAL_STATUS="Marital_Status";
		public static String EMPLOYMENT_STATUS="Employment_Status";
		public static String TIME_WITH_EMPLOYER="Time_with_Employer";
		public static String NUMBER_OF_MAJOR_CREDIT_CARD_HELD="Number_of_Major_Credit_Card_Held";
	}

	public static class CAPSApplicantAddressDetails{
		public static String FLAT_NO_PLOT_NO_HOUSE_NO="FlatNoPlotNoHouseNo";
		public static String BLDG_NO_SOCIETY_NAME="BldgNoSocietyName";
		public static String ROAD_NO_NAME_AREA_LOCALITY="RoadNoNameAreaLocality";
		public static String CITY="City";
		public static String LANDMARK="Landmark";
		public static String STATE="State";
		public static String PIN_CODE="PINCode";
		public static String COUNTRY_CODE="Country_Code";
	}

	public static class CAPSApplicantAdditionalAddressDetails{
		public static String FLAT_NO_PLOT_NO_HOUSE_NO = "FlatNoPlotNoHouseNo";
		public static String BLDG_NO_SOCIETY_NAME = "BldgNoSocietyName";
		public static String ROAD_NO_NAME_AREA_LOCALITY = "RoadNoNameAreaLocality";
		public static String CITY = "City";
		public static String LANDMARK = "Landmark";
		public static String STATE = "State";
		public static String PIN_CODE = "PINCode";
		public static String COUNTRY_CODE = "Country_Code";
	}

	public static class SCORE{
		public static String BUREAU_SCORE="BureauScore";
		public static String BUREAU_SCORE_CONFID_LEVEL="BureauScoreConfidLevel";
		public static String CREDIT_RATING="CreditRating";
	}

	public static class PSV{
		public static String BFHL_EX_HL = "BFHL_Ex_HL";
		public static String HL_CAD = "HL_CAD";
		public static String TELCOS_CAD = "Telcos_CAD";
		public static String MF_CAD = "MF_CAD";
		public static String RETAIL_CAD = "Retail_CAD";
		public static String TOTAL_CAD = "Total_CAD";
		public static String BFHL_ACA_EXHL = "BFHL_ACA_ExHL";
		public static String HL_ACA = "HL_ACA";
		public static String HL_ICA = "HL_ICA";
		public static String MF_ACA = "MF_ACA";
		public static String TELCOS_ACA = "Telcos_ACA";
		public static String RETAIL_ACA = "Retail_ACA";
		public static String TOTAL_ACA = "Total_ACA";
		public static String BFHL_ICA_EX_HL = "BFHL_ICA_Ex_HL";
		public static String MF_ICA = "MF_ICA";
		public static String TELCOS_ICA = "Telcos_ICA";
		public static String RETAIL_ICA = "Retail_ICA";
		public static String PSV_CAPS = "PSV_CAPS";
		public static String OWN_COMPANY_DATA = "Own_Company_Data";
		public static String OTH_CB_INFORMATION = "Oth_CB_Information";
		public static String INDIAN_MARKET_SPECIFIC_VAR = "Indian_Market_Specific_Var";
	}

	public static class BFHLExHL{
		public static String TN_OF_BFHL_CAD_EX_HL="TN_of_BFHL_CAD_Ex_HL";
		public static String TOT_VAL_OF_BFHL_CAD="Tot_Val_of_BFHL_CAD";
		public static String MNT_SMR_BFHL_CAD="MNT_SMR_BFHL_CAD";
	}

	public static class HLCAD{
		public static String TN_of_HL_CAD="TN_of_HL_CAD";
		public static String Tot_Val_of_HL_CAD="Tot_Val_of_HL_CAD";
		public static String MNT_SMR_HL_CAD="MNT_SMR_HL_CAD";
	}

	public static class TelcosCAD{
		public static String TN_OF_TELCOS_CAD="TN_of_Telcos_CAD";
		public static String TOT_VAL_OF_TELCOS_CAD="Tot_Val_of_Telcos_CAD";
		public static String MNT_SMR_TELCOS_CAD="MNT_SMR_Telcos_CAD";
	}

	public static class MFCAD{
		public static String TN_OF_MF_CAD="TN_of_MF_CAD";
		public static String TOT_VAL_OF_MF_CAD="Tot_Val_of_MF_CAD";
		public static String MNT_SMR_MF_CAD="MNT_SMR_MF_CAD";		
	}

	public static class RetailCAD{
		public static String TN_OF_RETAIL_CAD="TN_of_Retail_CAD";
		public static String TOT_VAL_OF_RETAIL_CAD="Tot_Val_of_Retail_CAD";
		public static String MNT_SMR_RETAIL_CAD="MNT_SMR_Retail_CAD";
	}

	public static class TotalCAD{
		public static String TN_OF_ALL_CAD="TN_of_All_CAD";
		public static String TOT_VAL_OF_ALL_CAD="Tot_Val_of_All_CAD";
		public static String MNT_SMR_CAD_ALL="MNT_SMR_CAD_All";
	}

	public static class BFHLACAExHL{
		public static String TN_OF_BFHL_ACA_EX_HL = "TN_of_BFHL_ACA_Ex_HL";
		public static String BAL_BFHL_ACA_EX_HL = "Bal_BFHL_ACA_Ex_HL";
		public static String WCD_ST_BFHL_ACA_EX_HL = "WCD_St_BFHL_ACA_Ex_HL";
		public static String WDS_PR_6_MNT_BFHL_ACA_EX_HL = "WDS_Pr_6_MNT_BFHL_ACA_Ex_HL";
		public static String WDS_PR_7_12_MNT_BFHL_ACA_EX_HL = "WDS_Pr_7_12_MNT_BFHL_ACA_Ex_HL";
		public static String AGE_OF_OLDEST_BFHL_ACA_EX_HL = "Age_of_Oldest_BFHL_ACA_Ex_HL";
		public static String HCB_PER_REV_ACC_BFHL_ACA_EX_HL = "HCB_Per_Rev_Acc_BFHL_ACA_Ex_HL";
		public static String TCB_PER_REV_ACC_BFHL_ACA_EX_HL = "TCB_Per_Rev_Acc_BFHL_ACA_Ex_HL";
	}

	public static class HLACA{
		public static String TN_OF_HL_ACA = "TN_of_HL_ACA";
		public static String BAL_HL_ACA = "Bal_HL_ACA";
		public static String WCD_St_HL_ACA = "WCD_St_HL_ACA";
		public static String WDS_PR_6_MNT_HL_ACA = "WDS_Pr_6_MNT_HL_ACA";
		public static String WDS_PR_7_12_MNT_HL_ACA = "WDS_Pr_7_12_MNT_HL_ACA";
		public static String AGE_OF_OLDEST_HL_ACA = "Age_of_Oldest_HL_ACA";
		public static String TN_OF_NDEL_HL_INACA="TN_of_NDel_HL_InACA";
		public static String TN_OF_DEL_HL_INACA="TN_of_Del_HL_InACA";

	}

	public static class MFACA{
		public static String TN_OF_MF_ACA = "TN_of_MF_ACA";
		public static String TOTAL_BAL_MF_ACA = "Total_Bal_MF_ACA";
		public static String WCD_ST_MF_ACA = "WCD_St_MF_ACA";
		public static String WDS_PR_6_MNT_MF_ACA = "WDS_Pr_6_MNT_MF_ACA";
		public static String WDS_PR_7_12_MNT_MF_ACA = "WDS_Pr_7_12_MNT_MF_ACA";
		public static String AGE_OF_OLDEST_MF_ACA = "Age_of_Oldest_MF_ACA";

	}

	public static class TelcosACA{
		public static String TN_OF_TELCOS_ACA = "TN_of_Telcos_ACA";
		public static String TOTAL_BAL_TELCOS_ACA = "Total_Bal_Telcos_ACA";
		public static String WCD_ST_TELCOS_ACA = "WCD_St_Telcos_ACA";
		public static String WDS_PR_6_MNT_TELCOS_ACA = "WDS_Pr_6_MNT_Telcos_ACA";
		public static String WDS_PR_7_12_MNT_TELCOS_ACA = "WDS_Pr_7_12_MNT_Telcos_ACA";
		public static String AGE_OF_OLDEST_TELCOS_ACA = "Age_of_Oldest_Telcos_ACA";

	}

	public static class RetailACA{
		public static String TN_OF_RETAIL_ACA = "TN_of_Retail_ACA";
		public static String TOTAL_BAL_RETAIL_ACA = "Total_Bal_Retail_ACA";
		public static String WCD_ST_RETAIL_ACA = "WCD_St_Retail_ACA";
		public static String WDS_PR_6_MNT_RETAIL_ACA = "WDS_Pr_6_MNT_Retail_ACA";
		public static String WDS_PR_7_12_MNT_RETAIL_ACA = "WDS_Pr_7_12_MNT_Retail_ACA";
		public static String AGE_OF_OLDEST_RETAIL_ACA = "Age_of_Oldest_Retail_ACA";
		public static String HCB_LM_PER_REV_ACC_RET = "HCB_Lm_Per_Rev_Acc_Ret";
		public static String TOT_CUR_BAL_LM_PER_REV_ACC_RET = "Tot_Cur_Bal_Lm_Per_Rev_Acc_Ret";

	}

	public static class TotalACA{
		public static String TN_OF_ALL_ACA = "TN_of_All_ACA";
		public static String BAL_ALL_ACA_EX_HL = "Bal_All_ACA_Ex_HL";
		public static String WCD_ST_ALL_ACA = "WCD_St_All_ACA";
		public static String WDS_PR_6_MNT_ALL_ACA = "WDS_Pr_6_MNT_All_ACA";
		public static String WDS_PR_7_12_MNT_ALL_ACA = "WDS_Pr_7_12_MNT_All_ACA";
		public static String AGE_OF_OLDEST_ALL_ACA = "Age_of_Oldest_All_ACA";

	}

	public static class BFHLICAExHL{
		public static String TN_OF_NDEL_BFHL_INACA_EX_HL="TN_of_NDel_BFHL_InACA_Ex_HL";
		public static String TN_OF_DEL_BFHL_INACA_EX_HL="TN_of_Del_BFHL_InACA_Ex_HL";
		
	}

	public static class HLICA{
		public static String TN_OF_NDEL_HL_INACA="TN_of_NDel_HL_InACA";
		public static String TN_OF_DEL_HL_INACA="TN_of_Del_HL_InACA";
		
	}

	public static class MFICA{
		public static String TN_OF_NDEL_MF_INACA="TN_of_NDel_MF_InACA";
		public static String TN_OF_DEL_MF_INACA="TN_of_Del_MF_InACA";
		
	}

	public static class TelcosICA{
		public static String TN_OF_NDEL_TELCOS_INACA="TN_of_NDel_Telcos_InACA";
		public static String TN_OF_DEL_TELCOS_INACA="TN_of_Del_Telcos_InACA";
	}

	public static class RetailICA{
		public static String TN_OF_NDEL_RETAIL_INACA="TN_of_NDel_Retail_InACA";
		public static String TN_OF_DEL_RETAIL_INACA="TN_of_Del_Retail_InACA";
	}

	public static class PSVCAPS{
		/*public static String BFHL_CAPS_LAST_90_DAYS="BFHL_CAPS_Last_90_Days";
		public static String MF_CAPS_LAST_90_DAYS="MF_CAPS_Last_90_Days";
		public static String TELCOS_CAPS_LAST_90_DAYS="Telcos_CAPS_Last_90_Days";
		public static String RETAIL_CAPS_LAST_90_DAYS="Retail_CAPS_Last_90_Days";*/
		//CHANGE 12 OCT 2015
		public static String BFHL_CAPS_LAST_90_DAYS="Bfhl_CAPS_Last_90_Days";
		public static String MF_CAPS_LAST_90_DAYS="mf_CAPS_Last_90_Days";
		public static String TELCOS_CAPS_LAST_90_DAYS="telcos_CAPS_Last_90_Days";
		public static String RETAIL_CAPS_LAST_90_DAYS="retail_CAPS_Last_90_Days";
	}

	public static class OwnCompanyData{
		public static String TN_OF_OCOM_CAD = "TN_of_OCom_CAD";
		public static String TOT_VAL_OF_OCOM_CAD = "Tot_Val_of_OCom_CAD";
		public static String MNT_SMR_OCOM_CAD = "MNT_SMR_OCom_CAD";
		public static String TN_OF_OCOM_ACA = "TN_of_OCom_ACA";
		public static String BAL_OCOM_ACA_EX_HL = "Bal_OCom_ACA_Ex_HL";
		public static String BAL_OCOM_ACA_HL_ONLY = "Bal_OCom_ACA_HL_Only";
		public static String WCD_ST_OCOM_ACA = "WCD_St_OCom_ACA";
		public static String HCB_LM_PER_REV_OCOM_ACA = "HCB_Lm_Per_Rev_OCom_ACA";
		public static String TN_OF_NDEL_OCOM_INACA = "TN_of_NDel_OCom_InACA";
		public static String TN_OF_DEL_OCOM_INACA = "TN_of_Del_OCom_InACA";
		public static String TN_OF_OCOM_CAPS_LAST_90_DAYS = "TN_of_OCom_CAPS_Last_90_days";
	}
	
	public static class OthCBInformation{
		public static String ANY_REL_CB_DATA_DIS_Y_N="Any_Rel_CB_Data_Dis_Y_N";
		public static String OTH_REL_CB_DFC_POS_MAT_Y_N="Oth_Rel_CB_DFC_Pos_Mat_Y_N";		
	}
	
	public static class IndianMarketSpecificVar{
		public static String TN_OF_CAD_CLASSED_AS_SFWDWO = "TN_of_CAD_classed_as_SFWDWO";
		public static String MNT_SMR_CAD_CLASSED_AS_SFWDWO = "MNT_SMR_CAD_classed_as_SFWDWO";
		public static String NUM_OF_CAD_SFWDWO_LAST_24_MNT = "Num_of_CAD_SFWDWO_Last_24_MNT";
		public static String TOT_CUR_BAL_LIVE_SACC = "Tot_Cur_Bal_Live_SAcc";
		public static String TOT_CUR_BAL_LIVE_UACC = "Tot_Cur_Bal_Live_UAcc";
		public static String TOT_CUR_BAL_MAX_BAL_LIVE_SACC = "Tot_Cur_Bal_Max_Bal_Live_SAcc";
		public static String TOT_CUR_BAL_MAX_BAL_LIVE_UACC = "Tot_Cur_Bal_Max_Bal_Live_UAcc";
	}
	
}
