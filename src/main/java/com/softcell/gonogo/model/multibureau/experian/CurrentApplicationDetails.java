package com.softcell.gonogo.model.multibureau.experian;

public class CurrentApplicationDetails {
	
	private String enquiryReason;
	private String financePurpose;
	private String amountFinanced;
	private String durationOfAgreement;
	private CurrentApplicantdetails currentApplicantdetails=null;
	private CurrentOtherDetails currentOtherDetails=null;
	private CurrentApplicantAddressDetails currentApplicantAddressDetails=null;
	private CurrentApplicantAditionalAddressDetails currentApplicantAditionalAddressDetails=null;
	public String getEnquiryReason() {
		return enquiryReason;
	}
	public void setEnquiryReason(String enquiryReason) {
		this.enquiryReason = enquiryReason;
	}
	public String getFinancePurpose() {
		return financePurpose;
	}
	public void setFinancePurpose(String financePurpose) {
		this.financePurpose = financePurpose;
	}
	public String getAmountFinanced() {
		return amountFinanced;
	}
	public void setAmountFinanced(String amountFinanced) {
		this.amountFinanced = amountFinanced;
	}
	public String getDurationOfAgreement() {
		return durationOfAgreement;
	}
	public void setDurationOfAgreement(String durationOfAgreement) {
		this.durationOfAgreement = durationOfAgreement;
	}
	public CurrentApplicantdetails getCurrentApplicantdetails() {
		return currentApplicantdetails;
	}
	public void setCurrentApplicantdetails(
			CurrentApplicantdetails currentApplicantdetails) {
		this.currentApplicantdetails = currentApplicantdetails;
	}
	public CurrentOtherDetails getCurrentOtherDetails() {
		return currentOtherDetails;
	}
	public void setCurrentOtherDetails(CurrentOtherDetails currentOtherDetails) {
		this.currentOtherDetails = currentOtherDetails;
	}
	public CurrentApplicantAddressDetails getCurrentApplicantAddressDetails() {
		return currentApplicantAddressDetails;
	}
	public void setCurrentApplicantAddressDetails(
			CurrentApplicantAddressDetails currentApplicantAddressDetails) {
		this.currentApplicantAddressDetails = currentApplicantAddressDetails;
	}
	public CurrentApplicantAditionalAddressDetails getCurrentApplicantAditionalAddressDetails() {
		return currentApplicantAditionalAddressDetails;
	}
	public void setCurrentApplicantAditionalAddressDetails(
			CurrentApplicantAditionalAddressDetails currentApplicantAditionalAddressDetails) {
		this.currentApplicantAditionalAddressDetails = currentApplicantAditionalAddressDetails;
	}


	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CurrentApplicationDetails{");
		sb.append("enquiryReason='").append(enquiryReason).append('\'');
		sb.append(", financePurpose='").append(financePurpose).append('\'');
		sb.append(", amountFinanced='").append(amountFinanced).append('\'');
		sb.append(", durationOfAgreement='").append(durationOfAgreement).append('\'');
		sb.append(", currentApplicantdetails=").append(currentApplicantdetails);
		sb.append(", currentOtherDetails=").append(currentOtherDetails);
		sb.append(", currentApplicantAddressDetails=").append(currentApplicantAddressDetails);
		sb.append(", currentApplicantAditionalAddressDetails=").append(currentApplicantAditionalAddressDetails);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CurrentApplicationDetails that = (CurrentApplicationDetails) o;

		if (enquiryReason != null ? !enquiryReason.equals(that.enquiryReason) : that.enquiryReason != null)
			return false;
		if (financePurpose != null ? !financePurpose.equals(that.financePurpose) : that.financePurpose != null)
			return false;
		if (amountFinanced != null ? !amountFinanced.equals(that.amountFinanced) : that.amountFinanced != null)
			return false;
		if (durationOfAgreement != null ? !durationOfAgreement.equals(that.durationOfAgreement) : that.durationOfAgreement != null)
			return false;
		if (currentApplicantdetails != null ? !currentApplicantdetails.equals(that.currentApplicantdetails) : that.currentApplicantdetails != null)
			return false;
		if (currentOtherDetails != null ? !currentOtherDetails.equals(that.currentOtherDetails) : that.currentOtherDetails != null)
			return false;
		if (currentApplicantAddressDetails != null ? !currentApplicantAddressDetails.equals(that.currentApplicantAddressDetails) : that.currentApplicantAddressDetails != null)
			return false;
		return currentApplicantAditionalAddressDetails != null ? currentApplicantAditionalAddressDetails.equals(that.currentApplicantAditionalAddressDetails) : that.currentApplicantAditionalAddressDetails == null;
	}

	@Override
	public int hashCode() {
		int result = enquiryReason != null ? enquiryReason.hashCode() : 0;
		result = 31 * result + (financePurpose != null ? financePurpose.hashCode() : 0);
		result = 31 * result + (amountFinanced != null ? amountFinanced.hashCode() : 0);
		result = 31 * result + (durationOfAgreement != null ? durationOfAgreement.hashCode() : 0);
		result = 31 * result + (currentApplicantdetails != null ? currentApplicantdetails.hashCode() : 0);
		result = 31 * result + (currentOtherDetails != null ? currentOtherDetails.hashCode() : 0);
		result = 31 * result + (currentApplicantAddressDetails != null ? currentApplicantAddressDetails.hashCode() : 0);
		result = 31 * result + (currentApplicantAditionalAddressDetails != null ? currentApplicantAditionalAddressDetails.hashCode() : 0);
		return result;
	}
}
