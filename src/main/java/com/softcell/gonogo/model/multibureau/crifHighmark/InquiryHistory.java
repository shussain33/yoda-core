package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INQUIRY-HISTORY")
@XmlAccessorType(XmlAccessType.FIELD)
public class InquiryHistory {


	@XmlElement(name="HISTORY")
	private List<History> history;

	public List<History> getHistory() {
		return history;
	}

	public void setHistory(List<History> history) {
		this.history = history;
	}

	@Override
	public String toString() {
		return "InquiryHistory [history=" + history + "]";
	}
	
}
