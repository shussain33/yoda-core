package com.softcell.gonogo.model.multibureau.experian;

public class TotalOutStandingBalance {

    private String outstandingBalanceSecured;
    private String outstandingBalanceSecuredPercentage;
    private String outstandingBalanceUnsecured;
    private String outstandingBalanceUnsecuredPercentage;
    private String outstandingBalanceAll;


    public String getOutstandingBalanceSecured() {
        return outstandingBalanceSecured;
    }

    public void setOutstandingBalanceSecured(String outstandingBalanceSecured) {
        this.outstandingBalanceSecured = outstandingBalanceSecured;
    }

    public String getOutstandingBalanceSecuredPercentage() {
        return outstandingBalanceSecuredPercentage;
    }

    public void setOutstandingBalanceSecuredPercentage(
            String outstandingBalanceSecuredPercentage) {
        this.outstandingBalanceSecuredPercentage = outstandingBalanceSecuredPercentage;
    }

    public String getOutstandingBalanceUnsecured() {
        return outstandingBalanceUnsecured;
    }

    public void setOutstandingBalanceUnsecured(String outstandingBalanceUnsecured) {
        this.outstandingBalanceUnsecured = outstandingBalanceUnsecured;
    }

    public String getOutstandingBalanceUnsecuredPercentage() {
        return outstandingBalanceUnsecuredPercentage;
    }

    public void setOutstandingBalanceUnsecuredPercentage(
            String outstandingBalanceUnsecuredPercentage) {
        this.outstandingBalanceUnsecuredPercentage = outstandingBalanceUnsecuredPercentage;
    }

    public String getOutstandingBalanceAll() {
        return outstandingBalanceAll;
    }

    public void setOutstandingBalanceAll(String outstandingBalanceAll) {
        this.outstandingBalanceAll = outstandingBalanceAll;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TotalOutStandingBalance{");
        sb.append("outstandingBalanceSecured='").append(outstandingBalanceSecured).append('\'');
        sb.append(", outstandingBalanceSecuredPercentage='").append(outstandingBalanceSecuredPercentage).append('\'');
        sb.append(", outstandingBalanceUnsecured='").append(outstandingBalanceUnsecured).append('\'');
        sb.append(", outstandingBalanceUnsecuredPercentage='").append(outstandingBalanceUnsecuredPercentage).append('\'');
        sb.append(", outstandingBalanceAll='").append(outstandingBalanceAll).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TotalOutStandingBalance that = (TotalOutStandingBalance) o;

        if (outstandingBalanceSecured != null ? !outstandingBalanceSecured.equals(that.outstandingBalanceSecured) : that.outstandingBalanceSecured != null)
            return false;
        if (outstandingBalanceSecuredPercentage != null ? !outstandingBalanceSecuredPercentage.equals(that.outstandingBalanceSecuredPercentage) : that.outstandingBalanceSecuredPercentage != null)
            return false;
        if (outstandingBalanceUnsecured != null ? !outstandingBalanceUnsecured.equals(that.outstandingBalanceUnsecured) : that.outstandingBalanceUnsecured != null)
            return false;
        if (outstandingBalanceUnsecuredPercentage != null ? !outstandingBalanceUnsecuredPercentage.equals(that.outstandingBalanceUnsecuredPercentage) : that.outstandingBalanceUnsecuredPercentage != null)
            return false;
        return outstandingBalanceAll != null ? outstandingBalanceAll.equals(that.outstandingBalanceAll) : that.outstandingBalanceAll == null;
    }

    @Override
    public int hashCode() {
        int result = outstandingBalanceSecured != null ? outstandingBalanceSecured.hashCode() : 0;
        result = 31 * result + (outstandingBalanceSecuredPercentage != null ? outstandingBalanceSecuredPercentage.hashCode() : 0);
        result = 31 * result + (outstandingBalanceUnsecured != null ? outstandingBalanceUnsecured.hashCode() : 0);
        result = 31 * result + (outstandingBalanceUnsecuredPercentage != null ? outstandingBalanceUnsecuredPercentage.hashCode() : 0);
        result = 31 * result + (outstandingBalanceAll != null ? outstandingBalanceAll.hashCode() : 0);
        return result;
    }
}
