package com.softcell.gonogo.model.multibureau.experian;

public class HLInACA {
	private String TNOfNDelHLInACA;
	private String TNOfDelHLInACA;
	
	public String getTNOfNDelHLInACA() {
		return TNOfNDelHLInACA;
	}
	public void setTNOfNDelHLInACA(String tNOfNDelHLInACA) {
		TNOfNDelHLInACA = tNOfNDelHLInACA;
	}
	public String getTNOfDelHLInACA() {
		return TNOfDelHLInACA;
	}
	public void setTNOfDelHLInACA(String tNOfDelHLInACA) {
		TNOfDelHLInACA = tNOfDelHLInACA;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("HLInACA{");
		sb.append("TNOfNDelHLInACA='").append(TNOfNDelHLInACA).append('\'');
		sb.append(", TNOfDelHLInACA='").append(TNOfDelHLInACA).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		HLInACA hlInACA = (HLInACA) o;

		if (TNOfNDelHLInACA != null ? !TNOfNDelHLInACA.equals(hlInACA.TNOfNDelHLInACA) : hlInACA.TNOfNDelHLInACA != null)
			return false;
		return TNOfDelHLInACA != null ? TNOfDelHLInACA.equals(hlInACA.TNOfDelHLInACA) : hlInACA.TNOfDelHLInACA == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfNDelHLInACA != null ? TNOfNDelHLInACA.hashCode() : 0;
		result = 31 * result + (TNOfDelHLInACA != null ? TNOfDelHLInACA.hashCode() : 0);
		return result;
	}
}