package com.softcell.gonogo.model.multibureau.crifHighmark;

public class GrpResponseList {

	/**
	 * @author Akshata
	 *
	 *
	 */
	private GrpResponse grpResponse;

	public GrpResponse getGrpResponse() {
		return grpResponse;
	}
	

	public void setGrpResponse(GrpResponse grpResponse) {
		this.grpResponse = grpResponse;
	}


	@Override
	public String toString() {
		return "GrpResponseList [grpResponse=" + grpResponse + "]";
	}
	
}
