package com.softcell.gonogo.model.multibureau.experian;

public class CAPSSummary {

	private String capsLast7Days;
	private String capsLast30Days;
	private String capsLast90Days;
	private String capsLast180Days;

	
	public String getCapsLast7Days() {
		return capsLast7Days;
	}
	public void setCapsLast7Days(String capsLast7Days) {
		this.capsLast7Days = capsLast7Days;
	}
	public String getCapsLast30Days() {
		return capsLast30Days;
	}
	public void setCapsLast30Days(String capsLast30Days) {
		this.capsLast30Days = capsLast30Days;
	}
	public String getCapsLast90Days() {
		return capsLast90Days;
	}
	public void setCapsLast90Days(String capsLast90Days) {
		this.capsLast90Days = capsLast90Days;
	}
	public String getCapsLast180Days() {
		return capsLast180Days;
	}
	public void setCapsLast180Days(String capsLast180Days) {
		this.capsLast180Days = capsLast180Days;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CAPSSummary{");
		sb.append("capsLast7Days='").append(capsLast7Days).append('\'');
		sb.append(", capsLast30Days='").append(capsLast30Days).append('\'');
		sb.append(", capsLast90Days='").append(capsLast90Days).append('\'');
		sb.append(", capsLast180Days='").append(capsLast180Days).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CAPSSummary that = (CAPSSummary) o;

		if (capsLast7Days != null ? !capsLast7Days.equals(that.capsLast7Days) : that.capsLast7Days != null)
			return false;
		if (capsLast30Days != null ? !capsLast30Days.equals(that.capsLast30Days) : that.capsLast30Days != null)
			return false;
		if (capsLast90Days != null ? !capsLast90Days.equals(that.capsLast90Days) : that.capsLast90Days != null)
			return false;
		return capsLast180Days != null ? capsLast180Days.equals(that.capsLast180Days) : that.capsLast180Days == null;
	}

	@Override
	public int hashCode() {
		int result = capsLast7Days != null ? capsLast7Days.hashCode() : 0;
		result = 31 * result + (capsLast30Days != null ? capsLast30Days.hashCode() : 0);
		result = 31 * result + (capsLast90Days != null ? capsLast90Days.hashCode() : 0);
		result = 31 * result + (capsLast180Days != null ? capsLast180Days.hashCode() : 0);
		return result;
	}
}
