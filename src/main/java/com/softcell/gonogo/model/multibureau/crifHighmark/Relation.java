package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RELATION")
@XmlAccessorType(XmlAccessType.FIELD)
public class Relation {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="TYPE")
	private String type;
	@XmlElement(name="NAME")
	private String name;
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Relation [type=" + type + ", name=" + name + "]";
	}
	
}
