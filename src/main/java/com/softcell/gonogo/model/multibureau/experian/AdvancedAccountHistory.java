package com.softcell.gonogo.model.multibureau.experian;


public class AdvancedAccountHistory {

	private String year;
	private String month;
	private String cashLimit;
	private String creditLimitAmount;
	private String actualPaymentAmount;
	private String emiAmount;
	private String currentBalance;
	private String amountPastDue;
	private String highestCreditOrOriginalLoanAmount;
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getCashLimit() {
		return cashLimit;
	}
	public void setCashLimit(String cashLimit) {
		this.cashLimit = cashLimit;
	}
	public String getCreditLimitAmount() {
		return creditLimitAmount;
	}
	public void setCreditLimitAmount(String creditLimitAmount) {
		this.creditLimitAmount = creditLimitAmount;
	}
	public String getActualPaymentAmount() {
		return actualPaymentAmount;
	}
	public void setActualPaymentAmount(String actualPaymentAmount) {
		this.actualPaymentAmount = actualPaymentAmount;
	}
	public String getEmiAmount() {
		return emiAmount;
	}
	public void setEmiAmount(String emiAmount) {
		this.emiAmount = emiAmount;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getAmountPastDue() {
		return amountPastDue;
	}
	public void setAmountPastDue(String amountPastDue) {
		this.amountPastDue = amountPastDue;
	}
	public String getHighestCreditOrOriginalLoanAmount() {
		return highestCreditOrOriginalLoanAmount;
	}
	public void setHighestCreditOrOriginalLoanAmount(
			String highestCreditOrOriginalLoanAmount) {
		this.highestCreditOrOriginalLoanAmount = highestCreditOrOriginalLoanAmount;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("AdvancedAccountHistory{");
		sb.append("year='").append(year).append('\'');
		sb.append(", month='").append(month).append('\'');
		sb.append(", cashLimit='").append(cashLimit).append('\'');
		sb.append(", creditLimitAmount='").append(creditLimitAmount).append('\'');
		sb.append(", actualPaymentAmount='").append(actualPaymentAmount).append('\'');
		sb.append(", emiAmount='").append(emiAmount).append('\'');
		sb.append(", currentBalance='").append(currentBalance).append('\'');
		sb.append(", amountPastDue='").append(amountPastDue).append('\'');
		sb.append(", highestCreditOrOriginalLoanAmount='").append(highestCreditOrOriginalLoanAmount).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		AdvancedAccountHistory that = (AdvancedAccountHistory) o;

		if (year != null ? !year.equals(that.year) : that.year != null) return false;
		if (month != null ? !month.equals(that.month) : that.month != null) return false;
		if (cashLimit != null ? !cashLimit.equals(that.cashLimit) : that.cashLimit != null) return false;
		if (creditLimitAmount != null ? !creditLimitAmount.equals(that.creditLimitAmount) : that.creditLimitAmount != null)
			return false;
		if (actualPaymentAmount != null ? !actualPaymentAmount.equals(that.actualPaymentAmount) : that.actualPaymentAmount != null)
			return false;
		if (emiAmount != null ? !emiAmount.equals(that.emiAmount) : that.emiAmount != null) return false;
		if (currentBalance != null ? !currentBalance.equals(that.currentBalance) : that.currentBalance != null)
			return false;
		if (amountPastDue != null ? !amountPastDue.equals(that.amountPastDue) : that.amountPastDue != null)
			return false;
		return highestCreditOrOriginalLoanAmount != null ? highestCreditOrOriginalLoanAmount.equals(that.highestCreditOrOriginalLoanAmount) : that.highestCreditOrOriginalLoanAmount == null;
	}

	@Override
	public int hashCode() {
		int result = year != null ? year.hashCode() : 0;
		result = 31 * result + (month != null ? month.hashCode() : 0);
		result = 31 * result + (cashLimit != null ? cashLimit.hashCode() : 0);
		result = 31 * result + (creditLimitAmount != null ? creditLimitAmount.hashCode() : 0);
		result = 31 * result + (actualPaymentAmount != null ? actualPaymentAmount.hashCode() : 0);
		result = 31 * result + (emiAmount != null ? emiAmount.hashCode() : 0);
		result = 31 * result + (currentBalance != null ? currentBalance.hashCode() : 0);
		result = 31 * result + (amountPastDue != null ? amountPastDue.hashCode() : 0);
		result = 31 * result + (highestCreditOrOriginalLoanAmount != null ? highestCreditOrOriginalLoanAmount.hashCode() : 0);
		return result;
	}
}
