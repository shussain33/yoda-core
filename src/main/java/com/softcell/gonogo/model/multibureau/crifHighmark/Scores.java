package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SCORES")
@XmlAccessorType(XmlAccessType.FIELD)
public class Scores {
	
	@XmlElement(name="SCORE")
	private List<Score> scoreList ;

	public List<Score> getScoreList() {
		return scoreList;
	}

	public void setScoreList(List<Score> scoreList) {
		this.scoreList = scoreList;
	}

	@Override
	public String toString() {
		return "Scores [scoreList=" + scoreList + "]";
	}
}