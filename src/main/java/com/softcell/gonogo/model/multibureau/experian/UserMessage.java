package com.softcell.gonogo.model.multibureau.experian;

public class UserMessage {

    private String userMessageText;

    public String getUserMessageText() {
        return userMessageText;
    }

    public void setUserMessageText(String userMessageText) {
        this.userMessageText = userMessageText;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserMessage{");
        sb.append("userMessageText='").append(userMessageText).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserMessage that = (UserMessage) o;

        return userMessageText != null ? userMessageText.equals(that.userMessageText) : that.userMessageText == null;
    }

    @Override
    public int hashCode() {
        return userMessageText != null ? userMessageText.hashCode() : 0;
    }
}
