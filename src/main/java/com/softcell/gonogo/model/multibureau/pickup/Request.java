package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */

public class Request {

    @JsonProperty("01")
    private String ipSrNo;

    @JsonProperty("02")
    private String sourceSystem;

    @JsonProperty("03")
    private String fileName;

    @JsonProperty("04")
    private String loanId;

    @JsonProperty("05")
    private String productCode;

    @JsonProperty("06")
    private String financePurpose;

    @JsonProperty("07")
    private Long tenure;

    @JsonProperty("08")
    private String internalExternalFlag;

    @JsonProperty("09")
    private String soaInstaLoadFlagc;

    @JsonProperty("10")
    private String firstSource;

    @JsonProperty("11")
    private String soaSourceBrokeridN;

    @JsonProperty("12")
    private String policySegment;

    @JsonProperty("13")
    private String soaPromotionC;

    @JsonProperty("14")
    private Long loanAmount;

    @JsonProperty("15")
    private String fName;

    @JsonProperty("16")
    private String mName;

    @JsonProperty("17")
    private String lName;

    @JsonProperty("18")
    private String name4;

    @JsonProperty("19")
    private String name5;

    @JsonProperty("20")
    private String dob;

    @JsonProperty("21")
    private String sex;

    @JsonProperty("22")
    private String maritalStatus;

    @JsonProperty("23")
    private String customerSegment;

    @JsonProperty("24")
    private String borrowerFlag;

    @JsonProperty("25")
    private String individualCorporateFlag;

    @JsonProperty("26")
    private String constitution;

    @JsonProperty("27")
    private String branchCode;

    @JsonProperty("28")
    private String groupId;

    @JsonProperty("29")
    private String numberCreditCards;

    @JsonProperty("30")
    private String addressType1;

    @JsonProperty("31")
    private String address;

    @JsonProperty("32")
    private String city;

    @JsonProperty("33")
    private String state;

    @JsonProperty("34")
    private Long zipCode;

    @JsonProperty("35")
    private String residenceCode;

    @JsonProperty("36")
    private String phoneType1;

    @JsonProperty("37")
    private String residencePhoneNumber;

    @JsonProperty("38")
    private Long stdCode;

    @JsonProperty("39")
    private String telephoneExtension;

    @JsonProperty("40")
    private String phoneType2;

    @JsonProperty("41")
    private String mobileNo;

    @JsonProperty("42")
    private String emailId;

    @JsonProperty("43")
    private String employerName;

    @JsonProperty("44")
    private String timeWithEmploy;

    @JsonProperty("45")
    private String companyCategory;

    @JsonProperty("46")
    private String natureOfBusiness;

    @JsonProperty("47")
    private String assetCost;

    @JsonProperty("48")
    private String logo;

    @JsonProperty("49")
    private String collateral1;

    @JsonProperty("50")
    private String collateral1Valuation1;

    @JsonProperty("51")
    private String collateral1Valuation2;

    @JsonProperty("52")
    private String collateral2;

    @JsonProperty("53")
    private String collateral2Valuation1;

    @JsonProperty("54")
    private String collateral2Valuation2;

    @JsonProperty("55")
    private String applicationScore;

    @JsonProperty("56")
    private String ddeScore;

    @JsonProperty("57")
    private String debitScore;

    @JsonProperty("58")
    private String behaviorScore;

    @JsonProperty("59")
    private String addressType2;

    @JsonProperty("60")
    private String offAddress;

    @JsonProperty("61")
    private String soaOffCity;

    @JsonProperty("62")
    private String soaOffState;

    @JsonProperty("63")
    private String soaOffZip;

    @JsonProperty("64")
    private String phoneType3;

    @JsonProperty("65")
    private String offPhoneNumber;

    @JsonProperty("66")
    private String offExtension;

    @JsonProperty("67")
    private String phoneType4;

    @JsonProperty("68")
    private String alternatePhoneNumber1;

    @JsonProperty("69")
    private String lsiLosNo;

    @JsonProperty("70")
    private String lsiFinnnoneLanNo;

    @JsonProperty("71")
    private String customerIdN;

    @JsonProperty("72")
    private Long income;

    @JsonProperty("73")
    private String incomeTaxPan;

    @JsonProperty("74")
    private String panIssueDate;

    @JsonProperty("75")
    private String panExpirationDate;

    @JsonProperty("76")
    private String passportNo;

    @JsonProperty("77")
    private String passportIssueDate;

    @JsonProperty("78")
    private String passportExpirationDate;

    @JsonProperty("79")
    private String voterId;

    @JsonProperty("80")
    private String voterIdIssueDate;

    @JsonProperty("81")
    private String voterIdExpirationDate;

    @JsonProperty("82")
    private String driverLicense;

    @JsonProperty("83")
    private String driverLicenseIssueDate;

    @JsonProperty("84")
    private String driverLicenceExpirationDate;

    @JsonProperty("85")
    private String rationCard;

    @JsonProperty("86")
    private String rationCardIssueDate;

    @JsonProperty("87")
    private String rationCardExpirationDate;

    @JsonProperty("88")
    private String uidNumber;

    @JsonProperty("89")
    private String uidIssueDate;

    @JsonProperty("90")
    private String uidExpirationDate;

    @JsonProperty("91")
    private String creditCard;

    @JsonProperty("92")
    private Long accountNo;

    @JsonProperty("93")
    private String accountNo2;

    @JsonProperty("94")
    private String accountNo3;

    @JsonProperty("95")
    private String tokenNumber1;

    @JsonProperty("96")
    private String tokenNumber2;

    @JsonProperty("97")
    private String tokenNumber3;

    @JsonProperty("98")
    private String inquiryUniqueRefNo;

    @JsonProperty("99")
    private String credtRptId;

    @JsonProperty("100")
    private String creditReqType;

    @JsonProperty("101")
    private String creditInqPurposeType;

    @JsonProperty("102")
    private String creditInquiryStage;

    @JsonProperty("103")
    private String kendraId;

    @JsonProperty("104")
    private String filler1;

    @JsonProperty("105")
    private String filler2;

    @JsonProperty("106")
    private String filler3;

    @JsonProperty("107")
    private String filler4;

    @JsonProperty("108")
    private String filler5;

    @JsonProperty("109")
    private String filler6;

    @JsonProperty("110")
    private String filler7;

    @JsonProperty("111")
    private String filler8;

    @JsonProperty("112")
    private String filler9;

    @JsonProperty("113")
    private String filler10;

    @JsonProperty("114")
    private String filler11;

    @JsonProperty("115")
    private String filler12;

    @JsonProperty("116")
    private String filler13;

    @JsonProperty("117")
    private String filler14;

    @JsonProperty("118")
    private String filler15;

    @JsonProperty("119")
    private String filler16;

    @JsonProperty("120")
    private String filler17;

    @JsonProperty("121")
    private String filler18;

    @JsonProperty("122")
    private String filler19;

    @JsonProperty("123")
    private String filler20;

    @JsonProperty("124")
    private String filler21;

    @JsonProperty("125")
    private String filler22;

    @JsonProperty("126")
    private String filler23;

    @JsonProperty("127")
    private String filler24;

    @JsonProperty("128")
    private String filler25;

    @JsonProperty("129")
    private String relationType;

    @JsonProperty("130")
    private String keyPersonName;

    @JsonProperty("131")
    private String loanAccountNumber1;

    @JsonProperty("132")
    private String loanAccountNumber2;

    @JsonProperty("133")
    private String loanAccountNumber3;

    @JsonProperty("134")
    private String regionCode;

    @JsonProperty("135")
    private Long age;

    @JsonProperty("136")
    private String multibureauFlag;

    @JsonProperty("137")
    private String sasFlag;

    @JsonProperty("138")
    private String soaMatchCodeSet;

    @JsonProperty("139")
    private String soaSetTargetDb;

    @JsonProperty("140")
    private String authenticationMethod;

    @JsonProperty("141")
    private Long ltv;

    @JsonProperty("142")
    private String soaFwCustomeridC;

    @JsonProperty("143")
    private String alternatePhoneNumber2;

    @JsonProperty("144")
    private String inquirySubmittedBy;

    @JsonProperty("145")
    private String sourceSystemVersion;

    @JsonProperty("146")
    private String sourceSystemVender;

    @JsonProperty("147")
    private String sourceSystemInstanceId;

    public String getIpSrNo() {
        return ipSrNo;
    }

    public void setIpSrNo(String ipSrNo) {
        this.ipSrNo = ipSrNo;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLoanId() {
        return loanId;
    }

    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getFinancePurpose() {
        return financePurpose;
    }

    public void setFinancePurpose(String financePurpose) {
        this.financePurpose = financePurpose;
    }

    public Long getTenure() {
        return tenure;
    }

    public void setTenure(Long tenure) {
        this.tenure = tenure;
    }

    public String getInternalExternalFlag() {
        return internalExternalFlag;
    }

    public void setInternalExternalFlag(String internalExternalFlag) {
        this.internalExternalFlag = internalExternalFlag;
    }

    public String getSoaInstaLoadFlagc() {
        return soaInstaLoadFlagc;
    }

    public void setSoaInstaLoadFlagc(String soaInstaLoadFlagc) {
        this.soaInstaLoadFlagc = soaInstaLoadFlagc;
    }

    public String getFirstSource() {
        return firstSource;
    }

    public void setFirstSource(String firstSource) {
        this.firstSource = firstSource;
    }

    public String getSoaSourceBrokeridN() {
        return soaSourceBrokeridN;
    }

    public void setSoaSourceBrokeridN(String soaSourceBrokeridN) {
        this.soaSourceBrokeridN = soaSourceBrokeridN;
    }

    public String getPolicySegment() {
        return policySegment;
    }

    public void setPolicySegment(String policySegment) {
        this.policySegment = policySegment;
    }

    public String getSoaPromotionC() {
        return soaPromotionC;
    }

    public void setSoaPromotionC(String soaPromotionC) {
        this.soaPromotionC = soaPromotionC;
    }

    public Long getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Long loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public String getName5() {
        return name5;
    }

    public void setName5(String name5) {
        this.name5 = name5;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getCustomerSegment() {
        return customerSegment;
    }

    public void setCustomerSegment(String customerSegment) {
        this.customerSegment = customerSegment;
    }

    public String getBorrowerFlag() {
        return borrowerFlag;
    }

    public void setBorrowerFlag(String borrowerFlag) {
        this.borrowerFlag = borrowerFlag;
    }

    public String getIndividualCorporateFlag() {
        return individualCorporateFlag;
    }

    public void setIndividualCorporateFlag(String individualCorporateFlag) {
        this.individualCorporateFlag = individualCorporateFlag;
    }

    public String getConstitution() {
        return constitution;
    }

    public void setConstitution(String constitution) {
        this.constitution = constitution;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getNumberCreditCards() {
        return numberCreditCards;
    }

    public void setNumberCreditCards(String numberCreditCards) {
        this.numberCreditCards = numberCreditCards;
    }

    public String getAddressType1() {
        return addressType1;
    }

    public void setAddressType1(String addressType1) {
        this.addressType1 = addressType1;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getZipCode() {
        return zipCode;
    }

    public void setZipCode(Long zipCode) {
        this.zipCode = zipCode;
    }

    public String getResidenceCode() {
        return residenceCode;
    }

    public void setResidenceCode(String residenceCode) {
        this.residenceCode = residenceCode;
    }

    public String getPhoneType1() {
        return phoneType1;
    }

    public void setPhoneType1(String phoneType1) {
        this.phoneType1 = phoneType1;
    }

    public String getResidencePhoneNumber() {
        return residencePhoneNumber;
    }

    public void setResidencePhoneNumber(String residencePhoneNumber) {
        this.residencePhoneNumber = residencePhoneNumber;
    }

    public Long getStdCode() {
        return stdCode;
    }

    public void setStdCode(Long stdCode) {
        this.stdCode = stdCode;
    }

    public String getTelephoneExtension() {
        return telephoneExtension;
    }

    public void setTelephoneExtension(String telephoneExtension) {
        this.telephoneExtension = telephoneExtension;
    }

    public String getPhoneType2() {
        return phoneType2;
    }

    public void setPhoneType2(String phoneType2) {
        this.phoneType2 = phoneType2;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getTimeWithEmploy() {
        return timeWithEmploy;
    }

    public void setTimeWithEmploy(String timeWithEmploy) {
        this.timeWithEmploy = timeWithEmploy;
    }

    public String getCompanyCategory() {
        return companyCategory;
    }

    public void setCompanyCategory(String companyCategory) {
        this.companyCategory = companyCategory;
    }

    public String getNatureOfBusiness() {
        return natureOfBusiness;
    }

    public void setNatureOfBusiness(String natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    public String getAssetCost() {
        return assetCost;
    }

    public void setAssetCost(String assetCost) {
        this.assetCost = assetCost;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCollateral1() {
        return collateral1;
    }

    public void setCollateral1(String collateral1) {
        this.collateral1 = collateral1;
    }

    public String getCollateral1Valuation1() {
        return collateral1Valuation1;
    }

    public void setCollateral1Valuation1(String collateral1Valuation1) {
        this.collateral1Valuation1 = collateral1Valuation1;
    }

    public String getCollateral1Valuation2() {
        return collateral1Valuation2;
    }

    public void setCollateral1Valuation2(String collateral1Valuation2) {
        this.collateral1Valuation2 = collateral1Valuation2;
    }

    public String getCollateral2() {
        return collateral2;
    }

    public void setCollateral2(String collateral2) {
        this.collateral2 = collateral2;
    }

    public String getCollateral2Valuation1() {
        return collateral2Valuation1;
    }

    public void setCollateral2Valuation1(String collateral2Valuation1) {
        this.collateral2Valuation1 = collateral2Valuation1;
    }

    public String getCollateral2Valuation2() {
        return collateral2Valuation2;
    }

    public void setCollateral2Valuation2(String collateral2Valuation2) {
        this.collateral2Valuation2 = collateral2Valuation2;
    }

    public String getApplicationScore() {
        return applicationScore;
    }

    public void setApplicationScore(String applicationScore) {
        this.applicationScore = applicationScore;
    }

    public String getDdeScore() {
        return ddeScore;
    }

    public void setDdeScore(String ddeScore) {
        this.ddeScore = ddeScore;
    }

    public String getDebitScore() {
        return debitScore;
    }

    public void setDebitScore(String debitScore) {
        this.debitScore = debitScore;
    }

    public String getBehaviorScore() {
        return behaviorScore;
    }

    public void setBehaviorScore(String behaviorScore) {
        this.behaviorScore = behaviorScore;
    }

    public String getAddressType2() {
        return addressType2;
    }

    public void setAddressType2(String addressType2) {
        this.addressType2 = addressType2;
    }

    public String getOffAddress() {
        return offAddress;
    }

    public void setOffAddress(String offAddress) {
        this.offAddress = offAddress;
    }

    public String getSoaOffCity() {
        return soaOffCity;
    }

    public void setSoaOffCity(String soaOffCity) {
        this.soaOffCity = soaOffCity;
    }

    public String getSoaOffState() {
        return soaOffState;
    }

    public void setSoaOffState(String soaOffState) {
        this.soaOffState = soaOffState;
    }

    public String getSoaOffZip() {
        return soaOffZip;
    }

    public void setSoaOffZip(String soaOffZip) {
        this.soaOffZip = soaOffZip;
    }

    public String getPhoneType3() {
        return phoneType3;
    }

    public void setPhoneType3(String phoneType3) {
        this.phoneType3 = phoneType3;
    }

    public String getOffPhoneNumber() {
        return offPhoneNumber;
    }

    public void setOffPhoneNumber(String offPhoneNumber) {
        this.offPhoneNumber = offPhoneNumber;
    }

    public String getOffExtension() {
        return offExtension;
    }

    public void setOffExtension(String offExtension) {
        this.offExtension = offExtension;
    }

    public String getPhoneType4() {
        return phoneType4;
    }

    public void setPhoneType4(String phoneType4) {
        this.phoneType4 = phoneType4;
    }

    public String getAlternatePhoneNumber1() {
        return alternatePhoneNumber1;
    }

    public void setAlternatePhoneNumber1(String alternatePhoneNumber1) {
        this.alternatePhoneNumber1 = alternatePhoneNumber1;
    }

    public String getLsiLosNo() {
        return lsiLosNo;
    }

    public void setLsiLosNo(String lsiLosNo) {
        this.lsiLosNo = lsiLosNo;
    }

    public String getLsiFinnnoneLanNo() {
        return lsiFinnnoneLanNo;
    }

    public void setLsiFinnnoneLanNo(String lsiFinnnoneLanNo) {
        this.lsiFinnnoneLanNo = lsiFinnnoneLanNo;
    }

    public String getCustomerIdN() {
        return customerIdN;
    }

    public void setCustomerIdN(String customerIdN) {
        this.customerIdN = customerIdN;
    }

    public Long getIncome() {
        return income;
    }

    public void setIncome(Long income) {
        this.income = income;
    }

    public String getIncomeTaxPan() {
        return incomeTaxPan;
    }

    public void setIncomeTaxPan(String incomeTaxPan) {
        this.incomeTaxPan = incomeTaxPan;
    }

    public String getPanIssueDate() {
        return panIssueDate;
    }

    public void setPanIssueDate(String panIssueDate) {
        this.panIssueDate = panIssueDate;
    }

    public String getPanExpirationDate() {
        return panExpirationDate;
    }

    public void setPanExpirationDate(String panExpirationDate) {
        this.panExpirationDate = panExpirationDate;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPassportIssueDate() {
        return passportIssueDate;
    }

    public void setPassportIssueDate(String passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }

    public String getPassportExpirationDate() {
        return passportExpirationDate;
    }

    public void setPassportExpirationDate(String passportExpirationDate) {
        this.passportExpirationDate = passportExpirationDate;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getVoterIdIssueDate() {
        return voterIdIssueDate;
    }

    public void setVoterIdIssueDate(String voterIdIssueDate) {
        this.voterIdIssueDate = voterIdIssueDate;
    }

    public String getVoterIdExpirationDate() {
        return voterIdExpirationDate;
    }

    public void setVoterIdExpirationDate(String voterIdExpirationDate) {
        this.voterIdExpirationDate = voterIdExpirationDate;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getDriverLicenseIssueDate() {
        return driverLicenseIssueDate;
    }

    public void setDriverLicenseIssueDate(String driverLicenseIssueDate) {
        this.driverLicenseIssueDate = driverLicenseIssueDate;
    }

    public String getDriverLicenceExpirationDate() {
        return driverLicenceExpirationDate;
    }

    public void setDriverLicenceExpirationDate(String driverLicenceExpirationDate) {
        this.driverLicenceExpirationDate = driverLicenceExpirationDate;
    }

    public String getRationCard() {
        return rationCard;
    }

    public void setRationCard(String rationCard) {
        this.rationCard = rationCard;
    }

    public String getRationCardIssueDate() {
        return rationCardIssueDate;
    }

    public void setRationCardIssueDate(String rationCardIssueDate) {
        this.rationCardIssueDate = rationCardIssueDate;
    }

    public String getRationCardExpirationDate() {
        return rationCardExpirationDate;
    }

    public void setRationCardExpirationDate(String rationCardExpirationDate) {
        this.rationCardExpirationDate = rationCardExpirationDate;
    }

    public String getUidNumber() {
        return uidNumber;
    }

    public void setUidNumber(String uidNumber) {
        this.uidNumber = uidNumber;
    }

    public String getUidIssueDate() {
        return uidIssueDate;
    }

    public void setUidIssueDate(String uidIssueDate) {
        this.uidIssueDate = uidIssueDate;
    }

    public String getUidExpirationDate() {
        return uidExpirationDate;
    }

    public void setUidExpirationDate(String uidExpirationDate) {
        this.uidExpirationDate = uidExpirationDate;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public Long getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Long accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountNo2() {
        return accountNo2;
    }

    public void setAccountNo2(String accountNo2) {
        this.accountNo2 = accountNo2;
    }

    public String getAccountNo3() {
        return accountNo3;
    }

    public void setAccountNo3(String accountNo3) {
        this.accountNo3 = accountNo3;
    }

    public String getTokenNumber1() {
        return tokenNumber1;
    }

    public void setTokenNumber1(String tokenNumber1) {
        this.tokenNumber1 = tokenNumber1;
    }

    public String getTokenNumber2() {
        return tokenNumber2;
    }

    public void setTokenNumber2(String tokenNumber2) {
        this.tokenNumber2 = tokenNumber2;
    }

    public String getTokenNumber3() {
        return tokenNumber3;
    }

    public void setTokenNumber3(String tokenNumber3) {
        this.tokenNumber3 = tokenNumber3;
    }

    public String getInquiryUniqueRefNo() {
        return inquiryUniqueRefNo;
    }

    public void setInquiryUniqueRefNo(String inquiryUniqueRefNo) {
        this.inquiryUniqueRefNo = inquiryUniqueRefNo;
    }

    public String getCredtRptId() {
        return credtRptId;
    }

    public void setCredtRptId(String credtRptId) {
        this.credtRptId = credtRptId;
    }

    public String getCreditReqType() {
        return creditReqType;
    }

    public void setCreditReqType(String creditReqType) {
        this.creditReqType = creditReqType;
    }

    public String getCreditInqPurposeType() {
        return creditInqPurposeType;
    }

    public void setCreditInqPurposeType(String creditInqPurposeType) {
        this.creditInqPurposeType = creditInqPurposeType;
    }

    public String getCreditInquiryStage() {
        return creditInquiryStage;
    }

    public void setCreditInquiryStage(String creditInquiryStage) {
        this.creditInquiryStage = creditInquiryStage;
    }

    public String getKendraId() {
        return kendraId;
    }

    public void setKendraId(String kendraId) {
        this.kendraId = kendraId;
    }

    public String getFiller1() {
        return filler1;
    }

    public void setFiller1(String filler1) {
        this.filler1 = filler1;
    }

    public String getFiller2() {
        return filler2;
    }

    public void setFiller2(String filler2) {
        this.filler2 = filler2;
    }

    public String getFiller3() {
        return filler3;
    }

    public void setFiller3(String filler3) {
        this.filler3 = filler3;
    }

    public String getFiller4() {
        return filler4;
    }

    public void setFiller4(String filler4) {
        this.filler4 = filler4;
    }

    public String getFiller5() {
        return filler5;
    }

    public void setFiller5(String filler5) {
        this.filler5 = filler5;
    }

    public String getFiller6() {
        return filler6;
    }

    public void setFiller6(String filler6) {
        this.filler6 = filler6;
    }

    public String getFiller7() {
        return filler7;
    }

    public void setFiller7(String filler7) {
        this.filler7 = filler7;
    }

    public String getFiller8() {
        return filler8;
    }

    public void setFiller8(String filler8) {
        this.filler8 = filler8;
    }

    public String getFiller9() {
        return filler9;
    }

    public void setFiller9(String filler9) {
        this.filler9 = filler9;
    }

    public String getFiller10() {
        return filler10;
    }

    public void setFiller10(String filler10) {
        this.filler10 = filler10;
    }

    public String getFiller11() {
        return filler11;
    }

    public void setFiller11(String filler11) {
        this.filler11 = filler11;
    }

    public String getFiller12() {
        return filler12;
    }

    public void setFiller12(String filler12) {
        this.filler12 = filler12;
    }

    public String getFiller13() {
        return filler13;
    }

    public void setFiller13(String filler13) {
        this.filler13 = filler13;
    }

    public String getFiller14() {
        return filler14;
    }

    public void setFiller14(String filler14) {
        this.filler14 = filler14;
    }

    public String getFiller15() {
        return filler15;
    }

    public void setFiller15(String filler15) {
        this.filler15 = filler15;
    }

    public String getFiller16() {
        return filler16;
    }

    public void setFiller16(String filler16) {
        this.filler16 = filler16;
    }

    public String getFiller17() {
        return filler17;
    }

    public void setFiller17(String filler17) {
        this.filler17 = filler17;
    }

    public String getFiller18() {
        return filler18;
    }

    public void setFiller18(String filler18) {
        this.filler18 = filler18;
    }

    public String getFiller19() {
        return filler19;
    }

    public void setFiller19(String filler19) {
        this.filler19 = filler19;
    }

    public String getFiller20() {
        return filler20;
    }

    public void setFiller20(String filler20) {
        this.filler20 = filler20;
    }

    public String getFiller21() {
        return filler21;
    }

    public void setFiller21(String filler21) {
        this.filler21 = filler21;
    }

    public String getFiller22() {
        return filler22;
    }

    public void setFiller22(String filler22) {
        this.filler22 = filler22;
    }

    public String getFiller23() {
        return filler23;
    }

    public void setFiller23(String filler23) {
        this.filler23 = filler23;
    }

    public String getFiller24() {
        return filler24;
    }

    public void setFiller24(String filler24) {
        this.filler24 = filler24;
    }

    public String getFiller25() {
        return filler25;
    }

    public void setFiller25(String filler25) {
        this.filler25 = filler25;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public String getKeyPersonName() {
        return keyPersonName;
    }

    public void setKeyPersonName(String keyPersonName) {
        this.keyPersonName = keyPersonName;
    }

    public String getLoanAccountNumber1() {
        return loanAccountNumber1;
    }

    public void setLoanAccountNumber1(String loanAccountNumber1) {
        this.loanAccountNumber1 = loanAccountNumber1;
    }

    public String getLoanAccountNumber2() {
        return loanAccountNumber2;
    }

    public void setLoanAccountNumber2(String loanAccountNumber2) {
        this.loanAccountNumber2 = loanAccountNumber2;
    }

    public String getLoanAccountNumber3() {
        return loanAccountNumber3;
    }

    public void setLoanAccountNumber3(String loanAccountNumber3) {
        this.loanAccountNumber3 = loanAccountNumber3;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getMultibureauFlag() {
        return multibureauFlag;
    }

    public void setMultibureauFlag(String multibureauFlag) {
        this.multibureauFlag = multibureauFlag;
    }

    public String getSasFlag() {
        return sasFlag;
    }

    public void setSasFlag(String sasFlag) {
        this.sasFlag = sasFlag;
    }

    public String getSoaMatchCodeSet() {
        return soaMatchCodeSet;
    }

    public void setSoaMatchCodeSet(String soaMatchCodeSet) {
        this.soaMatchCodeSet = soaMatchCodeSet;
    }

    public String getSoaSetTargetDb() {
        return soaSetTargetDb;
    }

    public void setSoaSetTargetDb(String soaSetTargetDb) {
        this.soaSetTargetDb = soaSetTargetDb;
    }

    public String getAuthenticationMethod() {
        return authenticationMethod;
    }

    public void setAuthenticationMethod(String authenticationMethod) {
        this.authenticationMethod = authenticationMethod;
    }

    public Long getLtv() {
        return ltv;
    }

    public void setLtv(Long ltv) {
        this.ltv = ltv;
    }

    public String getSoaFwCustomeridC() {
        return soaFwCustomeridC;
    }

    public void setSoaFwCustomeridC(String soaFwCustomeridC) {
        this.soaFwCustomeridC = soaFwCustomeridC;
    }

    public String getAlternatePhoneNumber2() {
        return alternatePhoneNumber2;
    }

    public void setAlternatePhoneNumber2(String alternatePhoneNumber2) {
        this.alternatePhoneNumber2 = alternatePhoneNumber2;
    }

    public String getInquirySubmittedBy() {
        return inquirySubmittedBy;
    }

    public void setInquirySubmittedBy(String inquirySubmittedBy) {
        this.inquirySubmittedBy = inquirySubmittedBy;
    }

    public String getSourceSystemVersion() {
        return sourceSystemVersion;
    }

    public void setSourceSystemVersion(String sourceSystemVersion) {
        this.sourceSystemVersion = sourceSystemVersion;
    }

    public String getSourceSystemVender() {
        return sourceSystemVender;
    }

    public void setSourceSystemVender(String sourceSystemVender) {
        this.sourceSystemVender = sourceSystemVender;
    }

    public String getSourceSystemInstanceId() {
        return sourceSystemInstanceId;
    }

    public void setSourceSystemInstanceId(String sourceSystemInstanceId) {
        this.sourceSystemInstanceId = sourceSystemInstanceId;
    }

    @Override
    public String toString() {
        return "Request [ipSrNo=" + ipSrNo + ", sourceSystem=" + sourceSystem + ", fileName=" + fileName + ", loanId="
                + loanId + ", productCode=" + productCode + ", financePurpose=" + financePurpose + ", tenure=" + tenure
                + ", internalExternalFlag=" + internalExternalFlag + ", soaInstaLoadFlagc=" + soaInstaLoadFlagc
                + ", firstSource=" + firstSource + ", soaSourceBrokeridN=" + soaSourceBrokeridN + ", policySegment="
                + policySegment + ", soaPromotionC=" + soaPromotionC + ", loanAmount=" + loanAmount + ", fName="
                + fName + ", mName=" + mName + ", lName=" + lName + ", name4=" + name4 + ", name5=" + name5 + ", dob="
                + dob + ", sex=" + sex + ", maritalStatus=" + maritalStatus + ", customerSegment=" + customerSegment
                + ", borrowerFlag=" + borrowerFlag + ", individualCorporateFlag=" + individualCorporateFlag
                + ", constitution=" + constitution + ", branchCode=" + branchCode + ", groupId=" + groupId
                + ", numberCreditCards=" + numberCreditCards + ", addressType1=" + addressType1 + ", address="
                + address + ", city=" + city + ", state=" + state + ", zipCode=" + zipCode + ", residenceCode="
                + residenceCode + ", phoneType1=" + phoneType1 + ", residencePhoneNumber=" + residencePhoneNumber
                + ", stdCode=" + stdCode + ", telephoneExtension=" + telephoneExtension + ", phoneType2=" + phoneType2
                + ", mobileNo=" + mobileNo + ", emailId=" + emailId + ", employerName=" + employerName
                + ", timeWithEmploy=" + timeWithEmploy + ", companyCategory=" + companyCategory + ", natureOfBusiness="
                + natureOfBusiness + ", assetCost=" + assetCost + ", logo=" + logo + ", collateral1=" + collateral1
                + ", collateral1Valuation1=" + collateral1Valuation1 + ", collateral1Valuation2="
                + collateral1Valuation2 + ", collateral2=" + collateral2 + ", collateral2Valuation1="
                + collateral2Valuation1 + ", collateral2Valuation2=" + collateral2Valuation2 + ", applicationScore="
                + applicationScore + ", ddeScore=" + ddeScore + ", debitScore=" + debitScore + ", behaviorScore="
                + behaviorScore + ", addressType2=" + addressType2 + ", offAddress=" + offAddress + ", soaOffCity="
                + soaOffCity + ", soaOffState=" + soaOffState + ", soaOffZip=" + soaOffZip + ", phoneType3="
                + phoneType3 + ", offPhoneNumber=" + offPhoneNumber + ", offExtension=" + offExtension
                + ", phoneType4=" + phoneType4 + ", alternatePhoneNumber1=" + alternatePhoneNumber1 + ", lsiLosNo="
                + lsiLosNo + ", lsiFinnnoneLanNo=" + lsiFinnnoneLanNo + ", customerIdN=" + customerIdN + ", income="
                + income + ", incomeTaxPan=" + incomeTaxPan + ", panIssueDate=" + panIssueDate + ", panExpirationDate="
                + panExpirationDate + ", passportNo=" + passportNo + ", passportIssueDate=" + passportIssueDate
                + ", passportExpirationDate=" + passportExpirationDate + ", voterId=" + voterId + ", voterIdIssueDate="
                + voterIdIssueDate + ", voterIdExpirationDate=" + voterIdExpirationDate + ", driverLicense="
                + driverLicense + ", driverLicenseIssueDate=" + driverLicenseIssueDate
                + ", driverLicenceExpirationDate=" + driverLicenceExpirationDate + ", rationCard=" + rationCard
                + ", rationCardIssueDate=" + rationCardIssueDate + ", rationCardExpirationDate="
                + rationCardExpirationDate + ", uidNumber=" + uidNumber + ", uidIssueDate=" + uidIssueDate
                + ", uidExpirationDate=" + uidExpirationDate + ", creditCard=" + creditCard + ", accountNo="
                + accountNo + ", accountNo2=" + accountNo2 + ", accountNo3=" + accountNo3 + ", tokenNumber1="
                + tokenNumber1 + ", tokenNumber2=" + tokenNumber2 + ", tokenNumber3=" + tokenNumber3
                + ", inquiryUniqueRefNo=" + inquiryUniqueRefNo + ", credtRptId=" + credtRptId + ", creditReqType="
                + creditReqType + ", creditInqPurposeType=" + creditInqPurposeType + ", creditInquiryStage="
                + creditInquiryStage + ", kendraId=" + kendraId + ", filler1=" + filler1 + ", filler2=" + filler2
                + ", filler3=" + filler3 + ", filler4=" + filler4 + ", filler5=" + filler5 + ", filler6=" + filler6
                + ", filler7=" + filler7 + ", filler8=" + filler8 + ", filler9=" + filler9 + ", filler10=" + filler10
                + ", filler11=" + filler11 + ", filler12=" + filler12 + ", filler13=" + filler13 + ", filler14="
                + filler14 + ", filler15=" + filler15 + ", filler16=" + filler16 + ", filler17=" + filler17
                + ", filler18=" + filler18 + ", filler19=" + filler19 + ", filler20=" + filler20 + ", filler21="
                + filler21 + ", filler22=" + filler22 + ", filler23=" + filler23 + ", filler24=" + filler24
                + ", filler25=" + filler25 + ", relationType=" + relationType + ", keyPersonName=" + keyPersonName
                + ", loanAccountNumber1=" + loanAccountNumber1 + ", loanAccountNumber2=" + loanAccountNumber2
                + ", loanAccountNumber3=" + loanAccountNumber3 + ", regionCode=" + regionCode + ", age=" + age
                + ", multibureauFlag=" + multibureauFlag + ", sasFlag=" + sasFlag + ", soaMatchCodeSet="
                + soaMatchCodeSet + ", soaSetTargetDb=" + soaSetTargetDb + ", authenticationMethod="
                + authenticationMethod + ", ltv=" + ltv + ", soaFwCustomeridC=" + soaFwCustomeridC
                + ", alternatePhoneNumber2=" + alternatePhoneNumber2 + ", inquirySubmittedBy=" + inquirySubmittedBy
                + ", sourceSystemVersion=" + sourceSystemVersion + ", sourceSystemVender=" + sourceSystemVender
                + ", sourceSystemInstanceId=" + sourceSystemInstanceId + "]";
    }


}
