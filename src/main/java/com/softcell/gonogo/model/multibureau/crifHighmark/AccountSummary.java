package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ACCOUNTS-SUMMARY")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountSummary {

	@XmlElement(name="DERIVED-ATTRIBUTES")
	private DerivedAttributes drivedAttributes=null;
	@XmlElement(name="PRIMARY-ACCOUNTS-SUMMARY")
	private PrimaryAccountsSummary primaryAccountsSummary=null;
	@XmlElement(name="SECONDARY-ACCOUNTS-SUMMARY")
	private SecondaryAccountSummary secondaryAccountSummary=null;
	
	public DerivedAttributes getDrivedAttributes() {
		return drivedAttributes;
	}
	public void setDrivedAttributes(DerivedAttributes drivedAttributes) {
		this.drivedAttributes = drivedAttributes;
	}
	public PrimaryAccountsSummary getPrimaryAccountsSummary() {
		return primaryAccountsSummary;
	}
	public void setPrimaryAccountsSummary(PrimaryAccountsSummary primaryAccountSummary) {
		this.primaryAccountsSummary = primaryAccountSummary;
	}
	public SecondaryAccountSummary getSecondaryAccountSummary() {
		return secondaryAccountSummary;
	}
	public void setSecondaryAccountSummary(
			SecondaryAccountSummary secondaryAccountSummary) {
		this.secondaryAccountSummary = secondaryAccountSummary;
	}

	@Override
	public String toString() {
		return "AccountSummary [drivedAttributes=" + drivedAttributes
				+ ", primaryAccountsSummary=" + primaryAccountsSummary
				+ ", secondaryAccountSummary=" + secondaryAccountSummary + "]";
	}
}
