package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="EMAILS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Emails {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="EMAIL")
	private List<Email> email;

	public List<Email> getEmail() {
		return email;
	}
	

	public void setEmail(List<Email> email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return "Emails [email=" + email + "]";
	}
	
}
