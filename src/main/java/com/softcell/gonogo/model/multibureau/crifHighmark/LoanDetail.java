package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LOAN-DETAIL")
@XmlAccessorType(XmlAccessType.FIELD)
public class LoanDetail {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="RECENT-DELINQ-DT")
	private String recentDelinqDate;
	@XmlElement(name="ACCT-NUMBER")
	private String accountNumber;
	@XmlElement(name="ACCT-TYPE")
	private String accountType;
	@XmlElement(name="DISBURSED-AMT")
	private String disbursedAmount;
	@XmlElement(name="DISBURSED-DT")
	private String disbursedDate;
	@XmlElement(name="CLOSED-DT")
	private String closedDate;
	@XmlElement(name="INSTALLMENT-AMT")
	private String installmentAmount;
	@XmlElement(name="WRITE-OFF-AMT")
	private String writeOffAmount;
	@XmlElement(name="OVERDUE-AMT")
	private String overdueAmount;
	@XmlElement(name="CURRENT-BAL")
	private String currentBalance;
	@XmlElement(name="FREQ")
	private String frequency ;
	@XmlElement(name="COMBINED-PAYMENT-HISTORY")
	private String combinedPaymentHistory;
	@XmlElement(name="STATUS")
	private String status;
	@XmlElement(name="INQ-CNT")
	private String inquiryCnt;
	@XmlElement(name="DPD")
	private String dpd;
	@XmlElement(name="INFO-AS-ON")
	private String infoAsOn;
	@XmlElement(name="LOAN-CYCLE-ID")
	private String loanCycleId;
	@XmlElement(name="WRITE-OFF-DT")
	private String writeoffDate;
	@XmlElement(name="WORST-DELEQUENCY-AMOUNT")
	private String worstDelequencyAmount;
	@XmlElement(name="ACTIVE-BORROWERS")
	private String activeBorrowers;
	@XmlElement(name="NO-OF-BORROWERS")
	private String noOfBorrowers;
	@XmlElement(name="COMMENT")
	private String comments;
	@XmlElement(name="PAYMENT-HISTORY")
	private String paymentHistory;
	
	public String getPaymentHistory() {
		return paymentHistory;
	}

	public void setPaymentHistory(String paymentHistory) {
		this.paymentHistory = paymentHistory;
	}

	public String getWriteoffDate() {
		return writeoffDate;
	}

	public void setWriteoffDate(String writeoffDate) {
		this.writeoffDate = writeoffDate;
	}

	public String getWorstDelequencyAmount() {
		return worstDelequencyAmount;
	}

	public void setWorstDelequencyAmount(String worstDelequencyAmount) {
		this.worstDelequencyAmount = worstDelequencyAmount;
	}

	public String getActiveBorrowers() {
		return activeBorrowers;
	}

	public void setActiveBorrowers(String activeBorrowers) {
		this.activeBorrowers = activeBorrowers;
	}

	public String getNoOfBorrowers() {
		return noOfBorrowers;
	}

	public void setNoOfBorrowers(String noOfBorrowers) {
		this.noOfBorrowers = noOfBorrowers;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getRecentDelinqDate() {
		return recentDelinqDate;
	}
	
	public void setRecentDelinqDate(String recentDelinqDate) {
		this.recentDelinqDate = recentDelinqDate;
	}
	
	public String getLoanCycleId() {
		return loanCycleId;
	}

	public void setLoanCycleId(String loanCycleId) {
		this.loanCycleId = loanCycleId;
	}
	

	public String getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getAccountType() {
		return accountType;
	}
	
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public String getDisbursedAmount() {
		return disbursedAmount;
	}
	
	public void setDisbursedAmount(String disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}
	
	public String getDisbursedDate() {
		return disbursedDate;
	}
	
	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}
	
	public String getClosedDate() {
		return closedDate;
	}
	
	public void setClosedDate(String closedDate) {
		this.closedDate = closedDate;
	}
	
	public String getInstallmentAmount() {
		return installmentAmount;
	}
	
	public void setInstallmentAmount(String installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	
	public String getWriteOffAmount() {
		return writeOffAmount;
	}
	
	public void setWriteOffAmount(String writeOffAmount) {
		this.writeOffAmount = writeOffAmount;
	}
	
	public String getOverdueAmount() {
		return overdueAmount;
	}
	
	public void setOverdueAmount(String overdueAmount) {
		this.overdueAmount = overdueAmount;
	}
	
	public String getCurrentBalance() {
		return currentBalance;
	}
	
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	public String getFrequency() {
		return frequency;
	}
	
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	public String getCombinedPaymentHistory() {
		return combinedPaymentHistory;
	}
	
	public void setCombinedPaymentHistory(String combinedPaymentHistory) {
		this.combinedPaymentHistory = combinedPaymentHistory;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getInquiryCnt() {
		return inquiryCnt;
	}
	
	public void setInquiryCnt(String inquiryCnt) {
		this.inquiryCnt = inquiryCnt;
	}
	
	public String getDpd() {
		return dpd;
	}
	
	public void setDpd(String dpd) {
		this.dpd = dpd;
	}
	
	public String getInfoAsOn() {
		return infoAsOn;
	}
	
	public void setInfoAsOn(String infoAsOn) {
		this.infoAsOn = infoAsOn;
	}

	@Override
	public String toString() {
		return "LoanDetail [recentDelinqDate=" + recentDelinqDate
				+ ", accountNumber=" + accountNumber + ", accountType="
				+ accountType + ", disbursedAmount=" + disbursedAmount
				+ ", disbursedDate=" + disbursedDate + ", closedDate="
				+ closedDate + ", installmentAmount=" + installmentAmount
				+ ", writeOffAmount=" + writeOffAmount + ", overdueAmount="
				+ overdueAmount + ", currentBalance=" + currentBalance
				+ ", frequency=" + frequency + ", combinedPaymentHistory="
				+ combinedPaymentHistory + ", status=" + status
				+ ", inquiryCnt=" + inquiryCnt + ", dpd=" + dpd + ", infoAsOn="
				+ infoAsOn + ", loanCycleId=" + loanCycleId + ", writeoffDate="
				+ writeoffDate + ", worstDelequencyAmount="
				+ worstDelequencyAmount + ", activeBorrowers="
				+ activeBorrowers + ", noOfBorrowers=" + noOfBorrowers
				+ ", comments=" + comments + ", paymentHistory="
				+ paymentHistory + "]";
	}
	
}
