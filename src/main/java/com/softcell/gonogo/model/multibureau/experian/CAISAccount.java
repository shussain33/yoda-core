package com.softcell.gonogo.model.multibureau.experian;

import java.util.List;

public class CAISAccount {

    private CAISSummary caisSummary;
    private List<CAISAccountDetails> caisAccountDetails;

    public CAISSummary getCaisSummary() {
        return caisSummary;
    }

    public void setCaisSummary(CAISSummary caisSummary) {
        this.caisSummary = caisSummary;
    }

    public List<CAISAccountDetails> getCaisAccountDetails() {
        return caisAccountDetails;
    }

    public void setCaisAccountDetails(List<CAISAccountDetails> caisAccountDetails) {
        this.caisAccountDetails = caisAccountDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAISAccount{");
        sb.append("caisSummary=").append(caisSummary);
        sb.append(", caisAccountDetails=").append(caisAccountDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAISAccount that = (CAISAccount) o;

        if (caisSummary != null ? !caisSummary.equals(that.caisSummary) : that.caisSummary != null) return false;
        return caisAccountDetails != null ? caisAccountDetails.equals(that.caisAccountDetails) : that.caisAccountDetails == null;
    }

    @Override
    public int hashCode() {
        int result = caisSummary != null ? caisSummary.hashCode() : 0;
        result = 31 * result + (caisAccountDetails != null ? caisAccountDetails.hashCode() : 0);
        return result;
    }
}
