package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SECONDARY-MATCHES")
@XmlAccessorType(XmlAccessType.FIELD)
public class SecondaryMatches {
	
	@XmlElement(name="SECONDARY-MATCH")
	private List<SecondaryMatch> secondaryMatchList ;

	public List<SecondaryMatch> getSecondaryMatchList() {
		return secondaryMatchList;
	}

	public void setSecondaryMatchList(List<SecondaryMatch> secondaryMatchList) {
		this.secondaryMatchList = secondaryMatchList;
	}

	@Override
	public String toString() {
		return "SecondaryMatches [secondaryMatchList=" + secondaryMatchList
				+ "]";
	}
}
