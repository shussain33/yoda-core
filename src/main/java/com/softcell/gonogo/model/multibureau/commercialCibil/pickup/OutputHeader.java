package com.softcell.gonogo.model.multibureau.commercialCibil.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OutputHeader {

	@JsonProperty("APPLICATION-ID")
	private String applicationId;
	
	@JsonProperty("CUST-ID")
	private String custId;
	
	@JsonProperty("RESPONSE-TYPE")
	private String responseType;
	
	@JsonProperty("REQUEST-RECEIVED-TIME")
	private String timestamp;
	
	
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getResponseType() {
		return responseType;
	}
	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	@Override
	public String toString() {
		return "OutputHeader [applicationId=" + applicationId + ", custId="
				+ custId + ", responseType=" + responseType + ", timestamp="
				+ timestamp + "]";
	}
}
