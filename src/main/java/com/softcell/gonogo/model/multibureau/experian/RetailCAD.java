package com.softcell.gonogo.model.multibureau.experian;

public class RetailCAD {

    private String TNOfRetailCAD;
    private String TotValOfRetailCAD;
    private String MNTSMRRetailCAD;

    public String getTNOfRetailCAD() {
        return TNOfRetailCAD;
    }

    public void setTNOfRetailCAD(String tNOfRetailCAD) {
        TNOfRetailCAD = tNOfRetailCAD;
    }

    public String getTotValOfRetailCAD() {
        return TotValOfRetailCAD;
    }

    public void setTotValOfRetailCAD(String totValOfRetailCAD) {
        TotValOfRetailCAD = totValOfRetailCAD;
    }

    public String getMNTSMRRetailCAD() {
        return MNTSMRRetailCAD;
    }

    public void setMNTSMRRetailCAD(String mNTSMRRetailCAD) {
        MNTSMRRetailCAD = mNTSMRRetailCAD;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RetailCAD retailCAD = (RetailCAD) o;

        if (TNOfRetailCAD != null ? !TNOfRetailCAD.equals(retailCAD.TNOfRetailCAD) : retailCAD.TNOfRetailCAD != null)
            return false;
        if (TotValOfRetailCAD != null ? !TotValOfRetailCAD.equals(retailCAD.TotValOfRetailCAD) : retailCAD.TotValOfRetailCAD != null)
            return false;
        return MNTSMRRetailCAD != null ? MNTSMRRetailCAD.equals(retailCAD.MNTSMRRetailCAD) : retailCAD.MNTSMRRetailCAD == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfRetailCAD != null ? TNOfRetailCAD.hashCode() : 0;
        result = 31 * result + (TotValOfRetailCAD != null ? TotValOfRetailCAD.hashCode() : 0);
        result = 31 * result + (MNTSMRRetailCAD != null ? MNTSMRRetailCAD.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RetailCAD{");
        sb.append("TNOfRetailCAD='").append(TNOfRetailCAD).append('\'');
        sb.append(", TotValOfRetailCAD='").append(TotValOfRetailCAD).append('\'');
        sb.append(", MNTSMRRetailCAD='").append(MNTSMRRetailCAD).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
