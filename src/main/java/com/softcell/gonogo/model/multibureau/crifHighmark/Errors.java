package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ERRORS")
public class Errors {

	@XmlElement(name="ERROR")
	private List<java.lang.Error> errorLists;

	public List<java.lang.Error> getErrorList() {
		return errorLists;
	}

	public void setErrorList(List<java.lang.Error> errorList) {
		this.errorLists = errorList;
	}

	@Override
	public String toString() {
		return "Errors [errorList=" + errorLists + "]";
	}

}
