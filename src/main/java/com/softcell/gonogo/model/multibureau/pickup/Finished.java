package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Finished {

    @JsonProperty("TRACKING-ID")
    private Long trackingId;

    @JsonProperty("BUREAU")
    private String bureau;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("BUREAU-STRING")
    private String bureauString;

    @JsonProperty("HTML REPORT")
    private String htmlReport;

    @JsonProperty("PDF REPORT")
    private byte[] pdfReport;

    @JsonProperty("JSON-RESPONSE-OBJECT")
    private Object responseJsonObject;

    @JsonProperty("BUREAU-RRP-OBJECT")
    private Object bureauRrpObject;

    @JsonProperty("ERRORS")
    private List<WarningAndError> errorList;

    @JsonProperty("WARNINGS")
    private List<WarningAndError> warningList;

    public Long getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(Long trackingId) {
        this.trackingId = trackingId;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBureauString() {
        return bureauString;
    }

    public void setBureauString(String bureauString) {
        this.bureauString = bureauString;
    }

    public String getHtmlReport() {
        return htmlReport;
    }

    public void setHtmlReport(String htmlReport) {
        this.htmlReport = htmlReport;
    }

    public byte[] getPdfReport() {
        return pdfReport;
    }

    public void setPdfReport(byte[] pdfReport) {
        this.pdfReport = pdfReport;
    }

    public Object getResponseJsonObject() {
        return responseJsonObject;
    }

    public void setResponseJsonObject(Object responseJsonObject) {
        this.responseJsonObject = responseJsonObject;
    }

    public Object getBureauRrpObject() {
        return bureauRrpObject;
    }

    public void setBureauRrpObject(Object bureauRrpObject) {
        this.bureauRrpObject = bureauRrpObject;
    }

    public List<WarningAndError> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<WarningAndError> errorList) {
        this.errorList = errorList;
    }

    public List<WarningAndError> getWarningList() {
        return warningList;
    }

    public void setWarningList(List<WarningAndError> warningList) {
        this.warningList = warningList;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Finished [trackingId=");
        builder.append(trackingId);
        builder.append(", bureau=");
        builder.append(bureau);
        builder.append(", product=");
        builder.append(product);
        builder.append(", status=");
        builder.append(status);
        builder.append(", bureauString=");
        builder.append(bureauString);
        builder.append(", htmlReport=");
        builder.append(htmlReport);
        builder.append(", pdfReport=");
        builder.append(Arrays.toString(pdfReport));
        builder.append(", responseJsonObject=");
        builder.append(responseJsonObject);
        builder.append(", bureauRrpObject=");
        builder.append(bureauRrpObject);
        builder.append(", errorList=");
        builder.append(errorList);
        builder.append(", warningList=");
        builder.append(warningList);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bureau == null) ? 0 : bureau.hashCode());
        result = prime * result
                + ((bureauRrpObject == null) ? 0 : bureauRrpObject.hashCode());
        result = prime * result
                + ((bureauString == null) ? 0 : bureauString.hashCode());
        result = prime * result
                + ((errorList == null) ? 0 : errorList.hashCode());
        result = prime * result
                + ((htmlReport == null) ? 0 : htmlReport.hashCode());
        result = prime * result + Arrays.hashCode(pdfReport);
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        result = prime
                * result
                + ((responseJsonObject == null) ? 0 : responseJsonObject
                .hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result
                + ((trackingId == null) ? 0 : trackingId.hashCode());
        result = prime * result
                + ((warningList == null) ? 0 : warningList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Finished other = (Finished) obj;
        if (bureau == null) {
            if (other.bureau != null)
                return false;
        } else if (!bureau.equals(other.bureau))
            return false;
        if (bureauRrpObject == null) {
            if (other.bureauRrpObject != null)
                return false;
        } else if (!bureauRrpObject.equals(other.bureauRrpObject))
            return false;
        if (bureauString == null) {
            if (other.bureauString != null)
                return false;
        } else if (!bureauString.equals(other.bureauString))
            return false;
        if (errorList == null) {
            if (other.errorList != null)
                return false;
        } else if (!errorList.equals(other.errorList))
            return false;
        if (htmlReport == null) {
            if (other.htmlReport != null)
                return false;
        } else if (!htmlReport.equals(other.htmlReport))
            return false;
        if (!Arrays.equals(pdfReport, other.pdfReport))
            return false;
        if (product == null) {
            if (other.product != null)
                return false;
        } else if (!product.equals(other.product))
            return false;
        if (responseJsonObject == null) {
            if (other.responseJsonObject != null)
                return false;
        } else if (!responseJsonObject.equals(other.responseJsonObject))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (trackingId == null) {
            if (other.trackingId != null)
                return false;
        } else if (!trackingId.equals(other.trackingId))
            return false;
        if (warningList == null) {
            if (other.warningList != null)
                return false;
        } else if (!warningList.equals(other.warningList))
            return false;
        return true;
    }

}
