package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="INDV-REPORTS")
@XmlAccessorType(XmlAccessType.FIELD)
public class INDVReports {

	@XmlElement(name="INDV-REPORT")
	private INDVReport INDVReport;

	public INDVReport getINDVReport() {
		return INDVReport;
	}

	public void setINDVReport(INDVReport INDVReport) {
		this.INDVReport = INDVReport;
	}

	@Override
	public String toString() {
		return "INDVReports [INDVReport=" + INDVReport + "]";
	}
}
