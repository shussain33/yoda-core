package com.softcell.gonogo.model.multibureau.experian;

public class BFHLExHl {

    private String TNOfBFHLCADExHl;
    private String TotValOfBFHLCAD;
    private String MNTSMRBFHLCAD;

    public String getTNOfBFHLCADExHl() {
        return TNOfBFHLCADExHl;
    }

    public void setTNOfBFHLCADExHl(String tNOfBFHLCADExHl) {
        TNOfBFHLCADExHl = tNOfBFHLCADExHl;
    }

    public String getTotValOfBFHLCAD() {
        return TotValOfBFHLCAD;
    }

    public void setTotValOfBFHLCAD(String totValOfBFHLCAD) {
        TotValOfBFHLCAD = totValOfBFHLCAD;
    }

    public String getMNTSMRBFHLCAD() {
        return MNTSMRBFHLCAD;
    }

    public void setMNTSMRBFHLCAD(String mNTSMRBFHLCAD) {
        MNTSMRBFHLCAD = mNTSMRBFHLCAD;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BFHLExHl{");
        sb.append("TNOfBFHLCADExHl='").append(TNOfBFHLCADExHl).append('\'');
        sb.append(", TotValOfBFHLCAD='").append(TotValOfBFHLCAD).append('\'');
        sb.append(", MNTSMRBFHLCAD='").append(MNTSMRBFHLCAD).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BFHLExHl bfhlExHl = (BFHLExHl) o;

        if (TNOfBFHLCADExHl != null ? !TNOfBFHLCADExHl.equals(bfhlExHl.TNOfBFHLCADExHl) : bfhlExHl.TNOfBFHLCADExHl != null)
            return false;
        if (TotValOfBFHLCAD != null ? !TotValOfBFHLCAD.equals(bfhlExHl.TotValOfBFHLCAD) : bfhlExHl.TotValOfBFHLCAD != null)
            return false;
        return MNTSMRBFHLCAD != null ? MNTSMRBFHLCAD.equals(bfhlExHl.MNTSMRBFHLCAD) : bfhlExHl.MNTSMRBFHLCAD == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfBFHLCADExHl != null ? TNOfBFHLCADExHl.hashCode() : 0;
        result = 31 * result + (TotValOfBFHLCAD != null ? TotValOfBFHLCAD.hashCode() : 0);
        result = 31 * result + (MNTSMRBFHLCAD != null ? MNTSMRBFHLCAD.hashCode() : 0);
        return result;
    }
}