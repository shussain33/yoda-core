package com.softcell.gonogo.model.multibureau.experian;

public class OwnCompanyData {

    private String TNOfOComCAD;
    private String TotValOfOComCAD;
    private String MNTSMROComCAD;
    private String TNOfOComACA;
    private String BalOComACAExHL;
    private String BalOComACAHLOnly;
    private String WCDStOComACA;
    private String HCBLmPerRevOComACA;
    private String TNOfNDelOComInACA;
    private String TNOfDelOComInACA;
    private String TNOfOComCAPSLast90Days;

    public String getTNOfOComCAD() {
        return TNOfOComCAD;
    }

    public void setTNOfOComCAD(String tNOfOComCAD) {
        TNOfOComCAD = tNOfOComCAD;
    }

    public String getTotValOfOComCAD() {
        return TotValOfOComCAD;
    }

    public void setTotValOfOComCAD(String totValOfOComCAD) {
        TotValOfOComCAD = totValOfOComCAD;
    }

    public String getMNTSMROComCAD() {
        return MNTSMROComCAD;
    }

    public void setMNTSMROComCAD(String mNTSMROComCAD) {
        MNTSMROComCAD = mNTSMROComCAD;
    }

    public String getTNOfOComACA() {
        return TNOfOComACA;
    }

    public void setTNOfOComACA(String tNOfOComACA) {
        TNOfOComACA = tNOfOComACA;
    }

    public String getBalOComACAExHL() {
        return BalOComACAExHL;
    }

    public void setBalOComACAExHL(String balOComACAExHL) {
        BalOComACAExHL = balOComACAExHL;
    }

    public String getBalOComACAHLOnly() {
        return BalOComACAHLOnly;
    }

    public void setBalOComACAHLOnly(String balOComACAHLOnly) {
        BalOComACAHLOnly = balOComACAHLOnly;
    }

    public String getWCDStOComACA() {
        return WCDStOComACA;
    }

    public void setWCDStOComACA(String wCDStOComACA) {
        WCDStOComACA = wCDStOComACA;
    }

    public String getHCBLmPerRevOComACA() {
        return HCBLmPerRevOComACA;
    }

    public void setHCBLmPerRevOComACA(String hCBLmPerRevOComACA) {
        HCBLmPerRevOComACA = hCBLmPerRevOComACA;
    }

    public String getTNOfNDelOComInACA() {
        return TNOfNDelOComInACA;
    }

    public void setTNOfNDelOComInACA(String tNOfNDelOComInACA) {
        TNOfNDelOComInACA = tNOfNDelOComInACA;
    }

    public String getTNOfDelOComInACA() {
        return TNOfDelOComInACA;
    }

    public void setTNOfDelOComInACA(String tNOfDelOComInACA) {
        TNOfDelOComInACA = tNOfDelOComInACA;
    }

    public String getTNOfOComCAPSLast90Days() {
        return TNOfOComCAPSLast90Days;
    }

    public void setTNOfOComCAPSLast90Days(String tNOfOComCAPSLast90Days) {
        TNOfOComCAPSLast90Days = tNOfOComCAPSLast90Days;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OwnCompanyData{");
        sb.append("TNOfOComCAD='").append(TNOfOComCAD).append('\'');
        sb.append(", TotValOfOComCAD='").append(TotValOfOComCAD).append('\'');
        sb.append(", MNTSMROComCAD='").append(MNTSMROComCAD).append('\'');
        sb.append(", TNOfOComACA='").append(TNOfOComACA).append('\'');
        sb.append(", BalOComACAExHL='").append(BalOComACAExHL).append('\'');
        sb.append(", BalOComACAHLOnly='").append(BalOComACAHLOnly).append('\'');
        sb.append(", WCDStOComACA='").append(WCDStOComACA).append('\'');
        sb.append(", HCBLmPerRevOComACA='").append(HCBLmPerRevOComACA).append('\'');
        sb.append(", TNOfNDelOComInACA='").append(TNOfNDelOComInACA).append('\'');
        sb.append(", TNOfDelOComInACA='").append(TNOfDelOComInACA).append('\'');
        sb.append(", TNOfOComCAPSLast90Days='").append(TNOfOComCAPSLast90Days).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OwnCompanyData that = (OwnCompanyData) o;

        if (TNOfOComCAD != null ? !TNOfOComCAD.equals(that.TNOfOComCAD) : that.TNOfOComCAD != null) return false;
        if (TotValOfOComCAD != null ? !TotValOfOComCAD.equals(that.TotValOfOComCAD) : that.TotValOfOComCAD != null)
            return false;
        if (MNTSMROComCAD != null ? !MNTSMROComCAD.equals(that.MNTSMROComCAD) : that.MNTSMROComCAD != null)
            return false;
        if (TNOfOComACA != null ? !TNOfOComACA.equals(that.TNOfOComACA) : that.TNOfOComACA != null) return false;
        if (BalOComACAExHL != null ? !BalOComACAExHL.equals(that.BalOComACAExHL) : that.BalOComACAExHL != null)
            return false;
        if (BalOComACAHLOnly != null ? !BalOComACAHLOnly.equals(that.BalOComACAHLOnly) : that.BalOComACAHLOnly != null)
            return false;
        if (WCDStOComACA != null ? !WCDStOComACA.equals(that.WCDStOComACA) : that.WCDStOComACA != null) return false;
        if (HCBLmPerRevOComACA != null ? !HCBLmPerRevOComACA.equals(that.HCBLmPerRevOComACA) : that.HCBLmPerRevOComACA != null)
            return false;
        if (TNOfNDelOComInACA != null ? !TNOfNDelOComInACA.equals(that.TNOfNDelOComInACA) : that.TNOfNDelOComInACA != null)
            return false;
        if (TNOfDelOComInACA != null ? !TNOfDelOComInACA.equals(that.TNOfDelOComInACA) : that.TNOfDelOComInACA != null)
            return false;
        return TNOfOComCAPSLast90Days != null ? TNOfOComCAPSLast90Days.equals(that.TNOfOComCAPSLast90Days) : that.TNOfOComCAPSLast90Days == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfOComCAD != null ? TNOfOComCAD.hashCode() : 0;
        result = 31 * result + (TotValOfOComCAD != null ? TotValOfOComCAD.hashCode() : 0);
        result = 31 * result + (MNTSMROComCAD != null ? MNTSMROComCAD.hashCode() : 0);
        result = 31 * result + (TNOfOComACA != null ? TNOfOComACA.hashCode() : 0);
        result = 31 * result + (BalOComACAExHL != null ? BalOComACAExHL.hashCode() : 0);
        result = 31 * result + (BalOComACAHLOnly != null ? BalOComACAHLOnly.hashCode() : 0);
        result = 31 * result + (WCDStOComACA != null ? WCDStOComACA.hashCode() : 0);
        result = 31 * result + (HCBLmPerRevOComACA != null ? HCBLmPerRevOComACA.hashCode() : 0);
        result = 31 * result + (TNOfNDelOComInACA != null ? TNOfNDelOComInACA.hashCode() : 0);
        result = 31 * result + (TNOfDelOComInACA != null ? TNOfDelOComInACA.hashCode() : 0);
        result = 31 * result + (TNOfOComCAPSLast90Days != null ? TNOfOComCAPSLast90Days.hashCode() : 0);
        return result;
    }
}
