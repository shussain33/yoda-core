package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="REQUEST")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {
	
	@XmlElement(name="NAME")
	private String name;
	@XmlElement(name="AKA")
	private String aka;
	@XmlElement(name="SPOUSE")
	private String spouse;
	@XmlElement(name="FATHER")
	private String father;
	@XmlElement(name="MOTHER")
	private String mother;
	@XmlElement(name="DOB")
	private String dob;
	@XmlElement(name="AGE")
	private String age;
	@XmlElement(name="AGE-AS-ON")
	private String ageAsOn;
	//@XmlElement(name="NAME")
	private String rationCard;
	//@XmlElement(name="NAME")
	private String passport;
//	@XmlElement(name="NAME")
	private String voterId;
	//@XmlElement(name="NAME")
	private String drivingLicenseNo;
	//@XmlElement(name="NAME")
	private String pan;
	@XmlElement(name="GENDER")
	private String gender;
	@XmlElement(name="OWNERSHIP")
	private String ownership;
	@XmlElement(name="ADDRESSES")
	private Addresses addresses;
	//@XmlElement(name="NAME")
	private String address1;
	//@XmlElement(name="NAME")
	private String address2;
	//@XmlElement(name="NAME")
	private String address3;
	//@XmlElement(name="PHONES")
	private String phone1;
	//@XmlElement(name="PHONES")
	private String phone2;
	//@XmlElement(name="PHONES")
	private String phone3;
	@XmlElement(name="EMAILS")
	private String email1;
	@XmlElement(name="EMAILS")
	private String email2;
	@XmlElement(name="KENDRA")
	private String kendra;
	@XmlElement(name="BRANCH")
	private String branch;
	@XmlElement(name="MBR-ID")
	private String memberId;
	@XmlElement(name="CREDIT-INQ-PURPS-TYP")
	private String creditInquiryPurposeType;
	@XmlElement(name="CREDIT-INQ-PURPS-TYP-DESC")
	private String creditInquiryPurposeTypeDescription;
	@XmlElement(name="CREDIT-INQUIRY-STAGE")
	private String creditInquiryStage;
	@XmlElement(name="CREDIT-RPT-ID")
	private String creditReportId;
	@XmlElement(name="CREDIT-REQ-TYP")
	private String creditRequestType;
	@XmlElement(name="CREDIT-RPT-TRN-DT-TM")
	private String creditReportTransectionDatetime;
	@XmlElement(name="LOS-APP-ID")
	private String losApplicationId;
	@XmlElement(name="AC-OPEN-DT")
	private String accountOpenDate;
	@XmlElement(name="LOAN-AMOUNT")
	private String loanAmount;
	@XmlElement(name="MFI-IND")
	private String mfiInd;
	@XmlElement(name="MFI-SCORE")
	private String Mfiscore;
	@XmlElement(name="MFI-GROUP")
	private String Mfigroup;
	@XmlElement(name="CNS-IND")
	private String Cnsind;
	@XmlElement(name="CNS-SCORE")
	private String Cnsscore;
	//@XmlElement(name="NAME")
	private String Ioi;
	@XmlElement(name="IDS")
	private Ids ids;
	@XmlElement(name="ENTITY-ID")
	private String entityId;
	@XmlElement(name="PHONES")
	private Phones phones;
	
	public Phones getPhones() {
		return phones;
	}







	public void setPhones(Phones phones) {
		this.phones = phones;
	}







	public String getEntityId() {
		return entityId;
	}







	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}







	public Addresses getAddresses() {
		return addresses;
	}
	






	public void setAddresses(Addresses addresses) {
		this.addresses = addresses;
	}
	






	public String getCnsind() {
		return Cnsind;
	}
	





	public void setCnsind(String cnsind) {
		Cnsind = cnsind;
	}
	





	public String getCnsscore() {
		return Cnsscore;
	}
	





	public void setCnsscore(String cnsscore) {
		Cnsscore = cnsscore;
	}
	





	public String getIoi() {
		return Ioi;
	}
	





	public void setIoi(String ioi) {
		Ioi = ioi;
	}
	


	
	public String getMfigroup() {
		return Mfigroup;
	}
	




	public void setMfigroup(String mfigroup) {
		Mfigroup = mfigroup;
	}
	




	public String getMfiscore() {
		return Mfiscore;
	}
	



	public void setMfiscore(String mfiscore) {
		Mfiscore = mfiscore;
	}
	



	public String getVoterId() {
		return voterId;
	}
	


	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	


	public String getMfiInd() {
		return mfiInd;
	}
	


	public void setMfiInd(String mfiInd) {
		this.mfiInd = mfiInd;
	}
	


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAka() {
		return aka;
	}


	public void setAka(String aka) {
		this.aka = aka;
	}


	public String getSpouse() {
		return spouse;
	}


	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}


	public String getFather() {
		return father;
	}


	public void setFather(String father) {
		this.father = father;
	}


	public String getMother() {
		return mother;
	}


	public void setMother(String mother) {
		this.mother = mother;
	}


	public String getDob() {
		return dob;
	}


	public void setDob(String dob) {
		this.dob = dob;
	}


	public String getAge() {
		return age;
	}


	public void setAge(String age) {
		this.age = age;
	}


	public String getAgeAsOn() {
		return ageAsOn;
	}


	public void setAgeAsOn(String ageAsOn) {
		this.ageAsOn = ageAsOn;
	}


	public String getRationCard() {
		return rationCard;
	}


	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}


	public String getPassport() {
		return passport;
	}


	public void setPassport(String passport) {
		this.passport = passport;
	}


	public String getVotersId() {
		return voterId;
	}


	public void setVotersId(String votersId) {
		this.voterId = votersId;
	}


	public String getDrivingLicenseNo() {
		return drivingLicenseNo;
	}


	public void setDrivingLicenseNo(String drivingLicenseNo) {
		this.drivingLicenseNo = drivingLicenseNo;
	}


	public String getPan() {
		return pan;
	}


	public void setPan(String pan) {
		this.pan = pan;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getOwnership() {
		return ownership;
	}


	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getAddress3() {
		return address3;
	}


	public void setAddress3(String address3) {
		this.address3 = address3;
	}


	public String getPhone1() {
		return phone1;
	}


	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}


	public String getPhone2() {
		return phone2;
	}


	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}


	public String getPhone3() {
		return phone3;
	}


	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}


	public String getEmail1() {
		return email1;
	}


	public void setEmail1(String email1) {
		this.email1 = email1;
	}


	public String getEmail2() {
		return email2;
	}


	public void setEmail2(String email2) {
		this.email2 = email2;
	}


	public String getKendra() {
		return kendra;
	}


	public void setKendra(String kendra) {
		this.kendra = kendra;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getMemberId() {
		return memberId;
	}


	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}


	public String getCreditInquiryPurposeType() {
		return creditInquiryPurposeType;
	}


	public void setCreditInquiryPurposeType(String creditInquiryPurposeType) {
		this.creditInquiryPurposeType = creditInquiryPurposeType;
	}


	public String getCreditInquiryPurposeTypeDescription() {
		return creditInquiryPurposeTypeDescription;
	}


	public void setCreditInquiryPurposeTypeDescription(
			String creditInquiryPurposeTypeDescription) {
		this.creditInquiryPurposeTypeDescription = creditInquiryPurposeTypeDescription;
	}


	public String getCreditInquiryStage() {
		return creditInquiryStage;
	}


	public void setCreditInquiryStage(String creditInquiryStage) {
		this.creditInquiryStage = creditInquiryStage;
	}


	public String getCreditReportId() {
		return creditReportId;
	}


	public void setCreditReportId(String creditReportId) {
		this.creditReportId = creditReportId;
	}


	public String getCreditRequestType() {
		return creditRequestType;
	}


	public void setCreditRequestType(String creditRequestType) {
		this.creditRequestType = creditRequestType;
	}


	public String getCreditReportTransectionDatetime() {
		return creditReportTransectionDatetime;
	}


	public void setCreditReportTransectionDatetime(
			String creditReportTransectionDatetime) {
		this.creditReportTransectionDatetime = creditReportTransectionDatetime;
	}


	public String getLosApplicationId() {
		return losApplicationId;
	}


	public void setLosApplicationId(String losApplicationId) {
		this.losApplicationId = losApplicationId;
	}


	public String getAccountOpenDate() {
		return accountOpenDate;
	}


	public void setAccountOpenDate(String accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}


	public String getLoanAmount() {
		return loanAmount;
	}


	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}
	public Ids getIds() {
		return ids;
	}
	






	public void setIds(Ids ids) {
		this.ids = ids;
	}
	

	@Override
	public String toString() {
		return "Request [name=" + name + ", aka=" + aka + ", spouse=" + spouse
				+ ", father=" + father + ", mother=" + mother + ", dob=" + dob
				+ ", age=" + age + ", ageAsOn=" + ageAsOn + ", rationCard="
				+ rationCard + ", passport=" + passport + ", voterId="
				+ voterId + ", drivingLicenseNo=" + drivingLicenseNo + ", pan="
				+ pan + ", gender=" + gender + ", ownership=" + ownership
				+ ", addresses=" + addresses + ", address1=" + address1
				+ ", address2=" + address2 + ", address3=" + address3
				+ ", phone1=" + phone1 + ", phone2=" + phone2 + ", phone3="
				+ phone3 + ", email1=" + email1 + ", email2=" + email2
				+ ", kendra=" + kendra + ", branch=" + branch + ", memberId="
				+ memberId + ", creditInquiryPurposeType="
				+ creditInquiryPurposeType
				+ ", creditInquiryPurposeTypeDescription="
				+ creditInquiryPurposeTypeDescription + ", creditInquiryStage="
				+ creditInquiryStage + ", creditReportId=" + creditReportId
				+ ", creditRequestType=" + creditRequestType
				+ ", creditReportTransectionDatetime="
				+ creditReportTransectionDatetime + ", losApplicationId="
				+ losApplicationId + ", accountOpenDate=" + accountOpenDate
				+ ", loanAmount=" + loanAmount + ", mfiInd=" + mfiInd
				+ ", Mfiscore=" + Mfiscore + ", Mfigroup=" + Mfigroup
				+ ", Cnsind=" + Cnsind + ", Cnsscore=" + Cnsscore + ", Ioi="
				+ Ioi + ", ids=" + ids + ", entityId=" + entityId + ", phones="
				+ phones + "]";
	}
}