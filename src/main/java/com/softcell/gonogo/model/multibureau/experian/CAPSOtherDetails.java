package com.softcell.gonogo.model.multibureau.experian;

public class CAPSOtherDetails {

	private String income;
	private String maritialStatus;
	private String employmentStatus;
	private String timeWithEmployer;
	private String numberOfMajorCreditCardHeld;

	
	public String getIncome() {
		return income;
	}
	public void setIncome(String income) {
		this.income = income;
	}
	public String getMaritialStatus() {
		return maritialStatus;
	}
	public void setMaritialStatus(String maritialStatus) {
		this.maritialStatus = maritialStatus;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public String getTimeWithEmployer() {
		return timeWithEmployer;
	}
	public void setTimeWithEmployer(String timeWithEmployer) {
		this.timeWithEmployer = timeWithEmployer;
	}
	public String getNumberOfMajorCreditCardHeld() {
		return numberOfMajorCreditCardHeld;
	}
	public void setNumberOfMajorCreditCardHeld(String numberOfMajorCreditCardHeld) {
		this.numberOfMajorCreditCardHeld = numberOfMajorCreditCardHeld;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CAPSOtherDetails{");
		sb.append("income='").append(income).append('\'');
		sb.append(", maritialStatus='").append(maritialStatus).append('\'');
		sb.append(", employmentStatus='").append(employmentStatus).append('\'');
		sb.append(", timeWithEmployer='").append(timeWithEmployer).append('\'');
		sb.append(", numberOfMajorCreditCardHeld='").append(numberOfMajorCreditCardHeld).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CAPSOtherDetails that = (CAPSOtherDetails) o;

		if (income != null ? !income.equals(that.income) : that.income != null) return false;
		if (maritialStatus != null ? !maritialStatus.equals(that.maritialStatus) : that.maritialStatus != null)
			return false;
		if (employmentStatus != null ? !employmentStatus.equals(that.employmentStatus) : that.employmentStatus != null)
			return false;
		if (timeWithEmployer != null ? !timeWithEmployer.equals(that.timeWithEmployer) : that.timeWithEmployer != null)
			return false;
		return numberOfMajorCreditCardHeld != null ? numberOfMajorCreditCardHeld.equals(that.numberOfMajorCreditCardHeld) : that.numberOfMajorCreditCardHeld == null;
	}

	@Override
	public int hashCode() {
		int result = income != null ? income.hashCode() : 0;
		result = 31 * result + (maritialStatus != null ? maritialStatus.hashCode() : 0);
		result = 31 * result + (employmentStatus != null ? employmentStatus.hashCode() : 0);
		result = 31 * result + (timeWithEmployer != null ? timeWithEmployer.hashCode() : 0);
		result = 31 * result + (numberOfMajorCreditCardHeld != null ? numberOfMajorCreditCardHeld.hashCode() : 0);
		return result;
	}
}
