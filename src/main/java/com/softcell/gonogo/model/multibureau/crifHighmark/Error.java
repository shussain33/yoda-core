package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ERROR")
@XmlAccessorType(XmlAccessType.FIELD)
public class Error {

	@XmlElement(name="ERROR-CODE")
	
	private String errorCode;
	@XmlElement(name="ERROR-DESCRIPTION")
	private String description;
	
	public String getCode() {
		return errorCode;
	}
	
	public void setCode(String code) {
		this.errorCode = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Error [code=" + errorCode + ", description=" + description + "]";
	}
	
	
}
