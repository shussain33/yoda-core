package com.softcell.gonogo.model.multibureau.commercialCibil.cibilCommresponse;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "responseReport"
})
@XmlRootElement(name = "base")
public class Base {

    @XmlElement(required = true)
    protected Base.ResponseReport responseReport;


    public Base.ResponseReport getResponseReport() {
        return responseReport;
    }


    public void setResponseReport(Base.ResponseReport value) {
        this.responseReport = value;
    }


   
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reportIssuesVec",
        "reportHeaderRec",
        "enquiryInformationRec",
        "productSec"
    })
    public static class ResponseReport {

        protected List<Base.ResponseReport.ReportIssuesVec> reportIssuesVec;
        @XmlElement(required = true)
        protected Base.ResponseReport.ReportHeaderRec reportHeaderRec;
        @XmlElement(required = true)
        protected Base.ResponseReport.EnquiryInformationRec enquiryInformationRec;
        @XmlElement(required = true)
        protected Base.ResponseReport.ProductSec productSec;

       
        public List<Base.ResponseReport.ReportIssuesVec> getReportIssuesVec() {
            if (reportIssuesVec == null) {
                reportIssuesVec = new ArrayList<Base.ResponseReport.ReportIssuesVec>();
            }
            return this.reportIssuesVec;
        }

       
        public Base.ResponseReport.ReportHeaderRec getReportHeaderRec() {
            return reportHeaderRec;
        }

       
        public void setReportHeaderRec(Base.ResponseReport.ReportHeaderRec value) {
            this.reportHeaderRec = value;
        }

       
        public Base.ResponseReport.EnquiryInformationRec getEnquiryInformationRec() {
            return enquiryInformationRec;
        }

        
        public void setEnquiryInformationRec(Base.ResponseReport.EnquiryInformationRec value) {
            this.enquiryInformationRec = value;
        }

       
        public Base.ResponseReport.ProductSec getProductSec() {
            return productSec;
        }

       
        public void setProductSec(Base.ResponseReport.ProductSec value) {
            this.productSec = value;
        }


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "borrowerName",
            "dateOfRegistration",
            "pan",
            "cin",
            "tin",
            "crn",
            "addressCount",
            "addressVec"
        })
        public static class EnquiryInformationRec {

            @XmlElement(required = true)
            protected String borrowerName;
            @XmlElement(required = true)
            protected String dateOfRegistration;
            @XmlElement(required = true)
            protected String pan;
            @XmlElement(required = true)
            protected String cin;
            @XmlElement(required = true)
            protected String tin;
            @XmlElement(required = true)
            protected String crn;
            @XmlElement(required = true)
            protected String addressCount;
            @XmlElement(required = true)
            protected Base.ResponseReport.EnquiryInformationRec.AddressVec addressVec;

           
            public String getBorrowerName() {
                return borrowerName;
            }

            
            public void setBorrowerName(String value) {
                this.borrowerName = value;
            }

            
            public String getDateOfRegistration() {
                return dateOfRegistration;
            }

           
            public void setDateOfRegistration(String value) {
                this.dateOfRegistration = value;
            }

          
            public String getPan() {
                return pan;
            }

           
            public void setPan(String value) {
                this.pan = value;
            }

            
            public String getCin() {
                return cin;
            }

           
            public void setCin(String value) {
                this.cin = value;
            }

            
            public String getTin() {
                return tin;
            }

           
            public void setTin(String value) {
                this.tin = value;
            }

           
            public String getCrn() {
                return crn;
            }

           
            public void setCrn(String value) {
                this.crn = value;
            }

           
            public String getAddressCount() {
                return addressCount;
            }

           
            public void setAddressCount(String value) {
                this.addressCount = value;
            }

           
            public Base.ResponseReport.EnquiryInformationRec.AddressVec getAddressVec() {
                return addressVec;
            }

            
            public void setAddressVec(Base.ResponseReport.EnquiryInformationRec.AddressVec value) {
                this.addressVec = value;
            }


            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "address"
            })
            public static class AddressVec {

                protected List<Base.ResponseReport.EnquiryInformationRec.AddressVec.Address> address;

                
                public List<Base.ResponseReport.EnquiryInformationRec.AddressVec.Address> getAddress() {
                    if (address == null) {
                        address = new ArrayList<Base.ResponseReport.EnquiryInformationRec.AddressVec.Address>();
                    }
                    return this.address;
                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "addressLine",
                    "city",
                    "state",
                    "pinCode"
                })
                public static class Address {

                    @XmlElement(required = true)
                    protected String addressLine;
                    @XmlElement(required = true)
                    protected String city;
                    @XmlElement(required = true)
                    protected String state;
                    @XmlElement(required = true)
                    protected String pinCode;

                  
                    public String getAddressLine() {
                        return addressLine;
                    }

                   
                    public void setAddressLine(String value) {
                        this.addressLine = value;
                    }

                   
                    public String getCity() {
                        return city;
                    }

                   
                    public void setCity(String value) {
                        this.city = value;
                    }

                    
                    public String getState() {
                        return state;
                    }

                   
                    public void setState(String value) {
                        this.state = value;
                    }

                   
                    public String getPinCode() {
                        return pinCode;
                    }

                    
                    public void setPinCode(String value) {
                        this.pinCode = value;
                    }

                }

            }

        }


       
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "borrowerProfileSec",
            "rankSec",
            "creditProfileSummarySec",
            "enquirySummarySec",
            "derogatoryInformationSec",
            "oustandingBalanceByCFAndAssetClasificationSec",
            "locationDetailsSec",
            "relationshipDetailsVec",
            "creditFacilityDetailsasBorrowerSecVec",
            "creditFacilityDetailsasGuarantorVec",
            "suitFiledVec",
            "creditRatingSummaryVec",
            "enquiryDetailsInLast24MonthVec",
            "creditFacilitiesDetailsVec",
            "creditFacilitiesSummary"
        })
        public static class ProductSec {

            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.BorrowerProfileSec borrowerProfileSec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.RankSec rankSec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.CreditProfileSummarySec creditProfileSummarySec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.EnquirySummarySec enquirySummarySec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.DerogatoryInformationSec derogatoryInformationSec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec oustandingBalanceByCFAndAssetClasificationSec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.LocationDetailsSec locationDetailsSec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.RelationshipDetailsVec relationshipDetailsVec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec creditFacilityDetailsasBorrowerSecVec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec creditFacilityDetailsasGuarantorVec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.SuitFiledVec suitFiledVec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.CreditRatingSummaryVec creditRatingSummaryVec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec enquiryDetailsInLast24MonthVec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec creditFacilitiesDetailsVec;
            @XmlElement(required = true)
            protected Base.ResponseReport.ProductSec.CreditFacilitiesSummary creditFacilitiesSummary;

          
            public Base.ResponseReport.ProductSec.BorrowerProfileSec getBorrowerProfileSec() {
                return borrowerProfileSec;
            }

           
            public void setBorrowerProfileSec(Base.ResponseReport.ProductSec.BorrowerProfileSec value) {
                this.borrowerProfileSec = value;
            }

           
            public Base.ResponseReport.ProductSec.RankSec getRankSec() {
                return rankSec;
            }

           
            public void setRankSec(Base.ResponseReport.ProductSec.RankSec value) {
                this.rankSec = value;
            }

           
            public Base.ResponseReport.ProductSec.CreditProfileSummarySec getCreditProfileSummarySec() {
                return creditProfileSummarySec;
            }

            
            public void setCreditProfileSummarySec(Base.ResponseReport.ProductSec.CreditProfileSummarySec value) {
                this.creditProfileSummarySec = value;
            }

           
            public Base.ResponseReport.ProductSec.EnquirySummarySec getEnquirySummarySec() {
                return enquirySummarySec;
            }

           
            public void setEnquirySummarySec(Base.ResponseReport.ProductSec.EnquirySummarySec value) {
                this.enquirySummarySec = value;
            }

           
            public Base.ResponseReport.ProductSec.DerogatoryInformationSec getDerogatoryInformationSec() {
                return derogatoryInformationSec;
            }

           
            public void setDerogatoryInformationSec(Base.ResponseReport.ProductSec.DerogatoryInformationSec value) {
                this.derogatoryInformationSec = value;
            }

           
            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec getOustandingBalanceByCFAndAssetClasificationSec() {
                return oustandingBalanceByCFAndAssetClasificationSec;
            }

           
            public void setOustandingBalanceByCFAndAssetClasificationSec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec value) {
                this.oustandingBalanceByCFAndAssetClasificationSec = value;
            }

           
            public Base.ResponseReport.ProductSec.LocationDetailsSec getLocationDetailsSec() {
                return locationDetailsSec;
            }

            
            public void setLocationDetailsSec(Base.ResponseReport.ProductSec.LocationDetailsSec value) {
                this.locationDetailsSec = value;
            }

            
            public Base.ResponseReport.ProductSec.RelationshipDetailsVec getRelationshipDetailsVec() {
                return relationshipDetailsVec;
            }

            
            public void setRelationshipDetailsVec(Base.ResponseReport.ProductSec.RelationshipDetailsVec value) {
                this.relationshipDetailsVec = value;
            }

            
            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec getCreditFacilityDetailsasBorrowerSecVec() {
                return creditFacilityDetailsasBorrowerSecVec;
            }

           
            public void setCreditFacilityDetailsasBorrowerSecVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec value) {
                this.creditFacilityDetailsasBorrowerSecVec = value;
            }

           
            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec getCreditFacilityDetailsasGuarantorVec() {
                return creditFacilityDetailsasGuarantorVec;
            }

            
            public void setCreditFacilityDetailsasGuarantorVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec value) {
                this.creditFacilityDetailsasGuarantorVec = value;
            }

            
            public Base.ResponseReport.ProductSec.SuitFiledVec getSuitFiledVec() {
                return suitFiledVec;
            }

            
            public void setSuitFiledVec(Base.ResponseReport.ProductSec.SuitFiledVec value) {
                this.suitFiledVec = value;
            }

           
            public Base.ResponseReport.ProductSec.CreditRatingSummaryVec getCreditRatingSummaryVec() {
                return creditRatingSummaryVec;
            }

            
            public void setCreditRatingSummaryVec(Base.ResponseReport.ProductSec.CreditRatingSummaryVec value) {
                this.creditRatingSummaryVec = value;
            }

           
            public Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec getEnquiryDetailsInLast24MonthVec() {
                return enquiryDetailsInLast24MonthVec;
            }

            
            public void setEnquiryDetailsInLast24MonthVec(Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec value) {
                this.enquiryDetailsInLast24MonthVec = value;
            }

            
            public Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec getCreditFacilitiesDetailsVec() {
                return creditFacilitiesDetailsVec;
            }

           
            public void setCreditFacilitiesDetailsVec(Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec value) {
                this.creditFacilitiesDetailsVec = value;
            }

           
            public Base.ResponseReport.ProductSec.CreditFacilitiesSummary getCreditFacilitiesSummary() {
                return creditFacilitiesSummary;
            }

           
            public void setCreditFacilitiesSummary(Base.ResponseReport.ProductSec.CreditFacilitiesSummary value) {
                this.creditFacilitiesSummary = value;
            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "borrwerDetails",
                "borrwerAddressContactDetails",
                "borrwerIDDetailsVec",
                "borrowerDelinquencyReportedOnBorrower",
                "borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec"
            })
            public static class BorrowerProfileSec {

                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails borrwerDetails;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerAddressContactDetails borrwerAddressContactDetails;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec borrwerIDDetailsVec;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower borrowerDelinquencyReportedOnBorrower;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec;

                
                public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails getBorrwerDetails() {
                    return borrwerDetails;
                }

               
                public void setBorrwerDetails(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails value) {
                    this.borrwerDetails = value;
                }

               
                public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerAddressContactDetails getBorrwerAddressContactDetails() {
                    return borrwerAddressContactDetails;
                }

                
                public void setBorrwerAddressContactDetails(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerAddressContactDetails value) {
                    this.borrwerAddressContactDetails = value;
                }

                
                public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec getBorrwerIDDetailsVec() {
                    return borrwerIDDetailsVec;
                }

               
                public void setBorrwerIDDetailsVec(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec value) {
                    this.borrwerIDDetailsVec = value;
                }

               
                public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower getBorrowerDelinquencyReportedOnBorrower() {
                    return borrowerDelinquencyReportedOnBorrower;
                }

               
                public void setBorrowerDelinquencyReportedOnBorrower(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower value) {
                    this.borrowerDelinquencyReportedOnBorrower = value;
                }

               
                public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec getBorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec() {
                    return borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec;
                }

               
                public void setBorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec value) {
                    this.borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec = value;
                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "yourInstitution",
                    "outsideInstitution"
                })
                public static class BorrowerDelinquencyReportedOnBorrower {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.YourInstitution yourInstitution;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.OutsideInstitution outsideInstitution;

                  
                    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.YourInstitution getYourInstitution() {
                        return yourInstitution;
                    }

                   
                    public void setYourInstitution(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.YourInstitution value) {
                        this.yourInstitution = value;
                    }

                   
                    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.OutsideInstitution getOutsideInstitution() {
                        return outsideInstitution;
                    }

                   
                    public void setOutsideInstitution(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnBorrower.OutsideInstitution value) {
                        this.outsideInstitution = value;
                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "current",
                        "last24Months"
                    })
                    public static class OutsideInstitution {

                        @XmlElement(required = true)
                        protected String current;
                        @XmlElement(required = true)
                        protected String last24Months;

                       
                        public String getCurrent() {
                            return current;
                        }

                        
                        public void setCurrent(String value) {
                            this.current = value;
                        }

                      
                        public String getLast24Months() {
                            return last24Months;
                        }

                       
                        public void setLast24Months(String value) {
                            this.last24Months = value;
                        }

                    }


                  
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "current",
                        "last24Months"
                    })
                    public static class YourInstitution {

                        @XmlElement(required = true)
                        protected String current;
                        @XmlElement(required = true)
                        protected String last24Months;

                       
                        public String getCurrent() {
                            return current;
                        }

                       
                        public void setCurrent(String value) {
                            this.current = value;
                        }

                       
                        public String getLast24Months() {
                            return last24Months;
                        }

                       
                        public void setLast24Months(String value) {
                            this.last24Months = value;
                        }

                    }

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "message",
                    "borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months"
                })
                public static class BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec {

                    @XmlElement(required = true)
                    protected String message;
                    protected List<Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months> borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months;

                   
                    public String getMessage() {
                        return message;
                    }

                   
                    public void setMessage(String value) {
                        this.message = value;
                    }

                    
                    public List<Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months> getBorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months() {
                        if (borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months == null) {
                            borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months = new ArrayList<Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24MonthsVec.BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months>();
                        }
                        return this.borrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months;
                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "detail",
                        "relationship",
                        "tlCount",
                        "tlOutstanding",
                        "wcCount",
                        "wcOutstanding",
                        "nfCount",
                        "nfOutstanding",
                        "fxCount",
                        "fxOutstanding"
                    })
                    public static class BorrowerDelinquencyReportedOnRSOrGSOftheBorrowerIn24Months {

                        @XmlElement(required = true)
                        protected String detail;
                        @XmlElement(required = true)
                        protected String relationship;
                        @XmlElement(name = "tl_count", required = true)
                        protected String tlCount;
                        @XmlElement(name = "tl_outstanding", required = true)
                        protected String tlOutstanding;
                        @XmlElement(name = "wc_count", required = true)
                        protected String wcCount;
                        @XmlElement(name = "wc_outstanding", required = true)
                        protected String wcOutstanding;
                        @XmlElement(name = "nf_count", required = true)
                        protected String nfCount;
                        @XmlElement(name = "nf_outstanding", required = true)
                        protected String nfOutstanding;
                        @XmlElement(name = "fx_count", required = true)
                        protected String fxCount;
                        @XmlElement(name = "fx_outstanding", required = true)
                        protected String fxOutstanding;

                      
                        public String getDetail() {
                            return detail;
                        }

                       
                        public void setDetail(String value) {
                            this.detail = value;
                        }

                        
                        public String getRelationship() {
                            return relationship;
                        }

                       
                        public void setRelationship(String value) {
                            this.relationship = value;
                        }

                       
                        public String getTlCount() {
                            return tlCount;
                        }

                       
                        public void setTlCount(String value) {
                            this.tlCount = value;
                        }

                        
                        public String getTlOutstanding() {
                            return tlOutstanding;
                        }

                       
                        public void setTlOutstanding(String value) {
                            this.tlOutstanding = value;
                        }

                        
                        public String getWcCount() {
                            return wcCount;
                        }

                      
                        public void setWcCount(String value) {
                            this.wcCount = value;
                        }

                       
                        public String getWcOutstanding() {
                            return wcOutstanding;
                        }

                        
                        public void setWcOutstanding(String value) {
                            this.wcOutstanding = value;
                        }

                       
                        public String getNfCount() {
                            return nfCount;
                        }

                        
                        public void setNfCount(String value) {
                            this.nfCount = value;
                        }

                       
                        public String getNfOutstanding() {
                            return nfOutstanding;
                        }

                        
                        public void setNfOutstanding(String value) {
                            this.nfOutstanding = value;
                        }

                        
                        public String getFxCount() {
                            return fxCount;
                        }

                        
                        public void setFxCount(String value) {
                            this.fxCount = value;
                        }

                       
                        public String getFxOutstanding() {
                            return fxOutstanding;
                        }

                       
                        public void setFxOutstanding(String value) {
                            this.fxOutstanding = value;
                        }

                    }

                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "address",
                    "telephoneNumber",
                    "faxNumber",
                    "mobileNumber"
                })
                public static class BorrwerAddressContactDetails {

                    @XmlElement(required = true)
                    protected String address;
                    @XmlElement(required = true)
                    protected String telephoneNumber;
                    @XmlElement(required = true)
                    protected String faxNumber;
                    @XmlElement(required = true)
                    protected String mobileNumber;

                    
                    public String getAddress() {
                        return address;
                    }

                   
                    public void setAddress(String value) {
                        this.address = value;
                    }

                   
                    public String getTelephoneNumber() {
                        return telephoneNumber;
                    }

                   
                    public void setTelephoneNumber(String value) {
                        this.telephoneNumber = value;
                    }

                    
                    public String getFaxNumber() {
                        return faxNumber;
                    }

                   
                    public void setFaxNumber(String value) {
                        this.faxNumber = value;
                    }

                    
                    public String getMobileNumber() {
                        return mobileNumber;
                    }

                   
                    public void setMobileNumber(String value) {
                        this.mobileNumber = value;
                    }

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "name",
                    "borrowersLegalConstitution",
                    "classOfActivityVec",
                    "businessCategory",
                    "businessIndustryType",
                    "salesFigure",
                    "numberOfEmployees",
                    "dateOfIncorporation",
                    "year"
                })
                public static class BorrwerDetails {

                    @XmlElement(required = true)
                    protected String name;
                    @XmlElement(required = true)
                    protected String borrowersLegalConstitution;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails.ClassOfActivityVec classOfActivityVec;
                    @XmlElement(required = true)
                    protected String businessCategory;
                    @XmlElement(required = true)
                    protected String businessIndustryType;
                    @XmlElement(required = true)
                    protected String salesFigure;
                    @XmlElement(required = true)
                    protected String numberOfEmployees;
                    @XmlElement(required = true)
                    protected String dateOfIncorporation;
                    @XmlElement(required = true)
                    protected String year;

                   
                    public String getName() {
                        return name;
                    }

                   
                    public void setName(String value) {
                        this.name = value;
                    }

                    
                    public String getBorrowersLegalConstitution() {
                        return borrowersLegalConstitution;
                    }

                   
                    public void setBorrowersLegalConstitution(String value) {
                        this.borrowersLegalConstitution = value;
                    }

                   
                    public Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails.ClassOfActivityVec getClassOfActivityVec() {
                        return classOfActivityVec;
                    }

                    
                    public void setClassOfActivityVec(Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerDetails.ClassOfActivityVec value) {
                        this.classOfActivityVec = value;
                    }

                    
                    public String getBusinessCategory() {
                        return businessCategory;
                    }

                   
                    public void setBusinessCategory(String value) {
                        this.businessCategory = value;
                    }

                   
                    public String getBusinessIndustryType() {
                        return businessIndustryType;
                    }

                   
                    public void setBusinessIndustryType(String value) {
                        this.businessIndustryType = value;
                    }

                   
                    public String getSalesFigure() {
                        return salesFigure;
                    }

                   
                    public void setSalesFigure(String value) {
                        this.salesFigure = value;
                    }

                    
                    public String getNumberOfEmployees() {
                        return numberOfEmployees;
                    }

                    
                    public void setNumberOfEmployees(String value) {
                        this.numberOfEmployees = value;
                    }

                    
                    public String getDateOfIncorporation() {
                        return dateOfIncorporation;
                    }

                   
                    public void setDateOfIncorporation(String value) {
                        this.dateOfIncorporation = value;
                    }

                    
                    public String getYear() {
                        return year;
                    }

                   
                    public void setYear(String value) {
                        this.year = value;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "classOfActivity"
                    })
                    public static class ClassOfActivityVec {

                        protected List<String> classOfActivity;

                       
                        public List<String> getClassOfActivity() {
                            if (classOfActivity == null) {
                                classOfActivity = new ArrayList<String>();
                            }
                            return this.classOfActivity;
                        }

                    }

                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "lastReportedDate",
                    "borrwerIDDetails"
                })
                public static class BorrwerIDDetailsVec {

                    @XmlElement(required = true)
                    protected String lastReportedDate;
                    protected List<Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec.BorrwerIDDetails> borrwerIDDetails;

                    
                    public String getLastReportedDate() {
                        return lastReportedDate;
                    }

                    
                    public void setLastReportedDate(String value) {
                        this.lastReportedDate = value;
                    }

                   
                    public List<Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec.BorrwerIDDetails> getBorrwerIDDetails() {
                        if (borrwerIDDetails == null) {
                            borrwerIDDetails = new ArrayList<Base.ResponseReport.ProductSec.BorrowerProfileSec.BorrwerIDDetailsVec.BorrwerIDDetails>();
                        }
                        return this.borrwerIDDetails;
                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "cin",
                        "pan",
                        "tin",
                        "registrationNumber",
                        "serviceTaxNumber"
                    })
                    public static class BorrwerIDDetails {

                        @XmlElement(required = true)
                        protected String cin;
                        @XmlElement(required = true)
                        protected String pan;
                        @XmlElement(required = true)
                        protected String tin;
                        @XmlElement(required = true)
                        protected String registrationNumber;
                        @XmlElement(required = true)
                        protected String serviceTaxNumber;

                       
                        public String getCin() {
                            return cin;
                        }

                        
                        public void setCin(String value) {
                            this.cin = value;
                        }

                       
                        public String getPan() {
                            return pan;
                        }

                        
                        public void setPan(String value) {
                            this.pan = value;
                        }

                       
                        public String getTin() {
                            return tin;
                        }

                        
                        public void setTin(String value) {
                            this.tin = value;
                        }

                      
                        public String getRegistrationNumber() {
                            return registrationNumber;
                        }

                        
                        public void setRegistrationNumber(String value) {
                            this.registrationNumber = value;
                        }

                       
                        public String getServiceTaxNumber() {
                            return serviceTaxNumber;
                        }

                        
                        public void setServiceTaxNumber(String value) {
                            this.serviceTaxNumber = value;
                        }

                    }

                }

            }


           
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "creditFacilitiesDetails",
                "total"
            })
            public static class CreditFacilitiesDetailsVec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec.CreditFacilitiesDetails> creditFacilitiesDetails;
                @XmlElement(required = true)
                protected String total;

               
                public String getMessage() {
                    return message;
                }

               
                public void setMessage(String value) {
                    this.message = value;
                }

                
                public List<Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec.CreditFacilitiesDetails> getCreditFacilitiesDetails() {
                    if (creditFacilitiesDetails == null) {
                        creditFacilitiesDetails = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilitiesDetailsVec.CreditFacilitiesDetails>();
                    }
                    return this.creditFacilitiesDetails;
                }

               
                public String getTotal() {
                    return total;
                }

                
                public void setTotal(String value) {
                    this.total = value;
                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "creditType",
                    "group",
                    "ownership",
                    "accountNo",
                    "reportedBy",
                    "currentBalance",
                    "closedDate",
                    "lastReported"
                })
                public static class CreditFacilitiesDetails {

                    @XmlElement(required = true)
                    protected String creditType;
                    @XmlElement(required = true)
                    protected String group;
                    @XmlElement(required = true)
                    protected String ownership;
                    @XmlElement(required = true)
                    protected String accountNo;
                    @XmlElement(required = true)
                    protected String reportedBy;
                    @XmlElement(required = true)
                    protected String currentBalance;
                    @XmlElement(required = true)
                    protected String closedDate;
                    @XmlElement(required = true)
                    protected String lastReported;

                   
                    public String getCreditType() {
                        return creditType;
                    }

                    
                    public void setCreditType(String value) {
                        this.creditType = value;
                    }

                   
                    public String getGroup() {
                        return group;
                    }

                   
                    public void setGroup(String value) {
                        this.group = value;
                    }

                   
                    public String getOwnership() {
                        return ownership;
                    }

                    
                    public void setOwnership(String value) {
                        this.ownership = value;
                    }

                    
                    public String getAccountNo() {
                        return accountNo;
                    }

                    
                    public void setAccountNo(String value) {
                        this.accountNo = value;
                    }

                   
                    public String getReportedBy() {
                        return reportedBy;
                    }

                    
                    public void setReportedBy(String value) {
                        this.reportedBy = value;
                    }

                  
                    public String getCurrentBalance() {
                        return currentBalance;
                    }

                   
                    public void setCurrentBalance(String value) {
                        this.currentBalance = value;
                    }

                   
                    public String getClosedDate() {
                        return closedDate;
                    }

                   
                    public void setClosedDate(String value) {
                        this.closedDate = value;
                    }

                   
                    public String getLastReported() {
                        return lastReported;
                    }

                   
                    public void setLastReported(String value) {
                        this.lastReported = value;
                    }

                }

            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "countOfCreditFacilities",
                "summaryOfCreditFacilitiesVec"
            })
            public static class CreditFacilitiesSummary {

                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities countOfCreditFacilities;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec summaryOfCreditFacilitiesVec;

               
                public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities getCountOfCreditFacilities() {
                    return countOfCreditFacilities;
                }

               
                public void setCountOfCreditFacilities(Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities value) {
                    this.countOfCreditFacilities = value;
                }

               
                public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec getSummaryOfCreditFacilitiesVec() {
                    return summaryOfCreditFacilitiesVec;
                }

               
                public void setSummaryOfCreditFacilitiesVec(Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec value) {
                    this.summaryOfCreditFacilitiesVec = value;
                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "noOfCreditFacilities",
                    "noOfCreditGrantors"
                })
                public static class CountOfCreditFacilities {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditFacilities noOfCreditFacilities;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditGrantors noOfCreditGrantors;

                    
                    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditFacilities getNoOfCreditFacilities() {
                        return noOfCreditFacilities;
                    }

                   
                    public void setNoOfCreditFacilities(Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditFacilities value) {
                        this.noOfCreditFacilities = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditGrantors getNoOfCreditGrantors() {
                        return noOfCreditGrantors;
                    }

                    
                    public void setNoOfCreditGrantors(Base.ResponseReport.ProductSec.CreditFacilitiesSummary.CountOfCreditFacilities.NoOfCreditGrantors value) {
                        this.noOfCreditGrantors = value;
                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "yourBank",
                        "others",
                        "total"
                    })
                    public static class NoOfCreditFacilities {

                        @XmlElement(required = true)
                        protected String yourBank;
                        @XmlElement(required = true)
                        protected String others;
                        @XmlElement(required = true)
                        protected String total;

                       
                        public String getYourBank() {
                            return yourBank;
                        }

                       
                        public void setYourBank(String value) {
                            this.yourBank = value;
                        }

                        
                        public String getOthers() {
                            return others;
                        }

                        
                        public void setOthers(String value) {
                            this.others = value;
                        }

                        
                        public String getTotal() {
                            return total;
                        }

                        
                        public void setTotal(String value) {
                            this.total = value;
                        }

                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "yourBank",
                        "others",
                        "total"
                    })
                    public static class NoOfCreditGrantors {

                        @XmlElement(required = true)
                        protected String yourBank;
                        @XmlElement(required = true)
                        protected String others;
                        @XmlElement(required = true)
                        protected String total;

                       
                        public String getYourBank() {
                            return yourBank;
                        }

                        
                        public void setYourBank(String value) {
                            this.yourBank = value;
                        }

                        
                        public String getOthers() {
                            return others;
                        }

                       
                        public void setOthers(String value) {
                            this.others = value;
                        }

                        
                        public String getTotal() {
                            return total;
                        }

                       
                        public void setTotal(String value) {
                            this.total = value;
                        }

                    }

                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "summaryOfCreditFacilitiesRec"
                })
                public static class SummaryOfCreditFacilitiesVec {

                    protected List<Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec.SummaryOfCreditFacilitiesRec> summaryOfCreditFacilitiesRec;

                    
                    public List<Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec.SummaryOfCreditFacilitiesRec> getSummaryOfCreditFacilitiesRec() {
                        if (summaryOfCreditFacilitiesRec == null) {
                            summaryOfCreditFacilitiesRec = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilitiesSummary.SummaryOfCreditFacilitiesVec.SummaryOfCreditFacilitiesRec>();
                        }
                        return this.summaryOfCreditFacilitiesRec;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "creditType",
                        "yourBank",
                        "othersBank"
                    })
                    public static class SummaryOfCreditFacilitiesRec {

                        @XmlElement(required = true)
                        protected String creditType;
                        @XmlElement(required = true)
                        protected String yourBank;
                        @XmlElement(required = true)
                        protected String othersBank;

                       
                        public String getCreditType() {
                            return creditType;
                        }

                       
                        public void setCreditType(String value) {
                            this.creditType = value;
                        }

                        
                        public String getYourBank() {
                            return yourBank;
                        }

                       
                        public void setYourBank(String value) {
                            this.yourBank = value;
                        }

                        
                        public String getOthersBank() {
                            return othersBank;
                        }

                       
                        public void setOthersBank(String value) {
                            this.othersBank = value;
                        }

                    }

                }

            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "creditFacilityDetailsasBorrowerSec"
            })
            public static class CreditFacilityDetailsasBorrowerSecVec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec> creditFacilityDetailsasBorrowerSec;

               
                public String getMessage() {
                    return message;
                }

               
                public void setMessage(String value) {
                    this.message = value;
                }

                
                public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec> getCreditFacilityDetailsasBorrowerSec() {
                    if (creditFacilityDetailsasBorrowerSec == null) {
                        creditFacilityDetailsasBorrowerSec = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec>();
                    }
                    return this.creditFacilityDetailsasBorrowerSec;
                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "message",
                    "creditFacilityCurrentDetailsVec",
                    "cfHistoryforACOrDPDupto24MonthsVec",
                    "creditFacilityOverdueDetailsVec",
                    "chequeDishounouredDuetoInsufficientFunds",
                    "creditFacilitySecurityDetailsVec",
                    "creditFacilityGuarantorDetailsVec"
                })
                public static class CreditFacilityDetailsasBorrowerSec {

                    @XmlElement(required = true)
                    protected String message;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec creditFacilityCurrentDetailsVec;
                    @XmlElement(name = "CFHistoryforACOrDPDupto24MonthsVec", required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec cfHistoryforACOrDPDupto24MonthsVec;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec creditFacilityOverdueDetailsVec;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.ChequeDishounouredDuetoInsufficientFunds chequeDishounouredDuetoInsufficientFunds;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec creditFacilitySecurityDetailsVec;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec creditFacilityGuarantorDetailsVec;

                   
                    public String getMessage() {
                        return message;
                    }

                   
                    public void setMessage(String value) {
                        this.message = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec getCreditFacilityCurrentDetailsVec() {
                        return creditFacilityCurrentDetailsVec;
                    }

                   
                    public void setCreditFacilityCurrentDetailsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec value) {
                        this.creditFacilityCurrentDetailsVec = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec getCFHistoryforACOrDPDupto24MonthsVec() {
                        return cfHistoryforACOrDPDupto24MonthsVec;
                    }

                    
                    public void setCFHistoryforACOrDPDupto24MonthsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec value) {
                        this.cfHistoryforACOrDPDupto24MonthsVec = value;
                    }

                   
                    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec getCreditFacilityOverdueDetailsVec() {
                        return creditFacilityOverdueDetailsVec;
                    }

                    
                    public void setCreditFacilityOverdueDetailsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec value) {
                        this.creditFacilityOverdueDetailsVec = value;
                    }

                   
                    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.ChequeDishounouredDuetoInsufficientFunds getChequeDishounouredDuetoInsufficientFunds() {
                        return chequeDishounouredDuetoInsufficientFunds;
                    }

                   
                    public void setChequeDishounouredDuetoInsufficientFunds(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.ChequeDishounouredDuetoInsufficientFunds value) {
                        this.chequeDishounouredDuetoInsufficientFunds = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec getCreditFacilitySecurityDetailsVec() {
                        return creditFacilitySecurityDetailsVec;
                    }

                    
                    public void setCreditFacilitySecurityDetailsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec value) {
                        this.creditFacilitySecurityDetailsVec = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec getCreditFacilityGuarantorDetailsVec() {
                        return creditFacilityGuarantorDetailsVec;
                    }

                    
                    public void setCreditFacilityGuarantorDetailsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec value) {
                        this.creditFacilityGuarantorDetailsVec = value;
                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "cfHistoryforACOrDPDupto24Months"
                    })
                    public static class CFHistoryforACOrDPDupto24MonthsVec {

                        @XmlElement(name = "CFHistoryforACOrDPDupto24Months")
                        protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months> cfHistoryforACOrDPDupto24Months;

                         
                        public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months> getCFHistoryforACOrDPDupto24Months() {
                            if (cfHistoryforACOrDPDupto24Months == null) {
                                cfHistoryforACOrDPDupto24Months = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CFHistoryforACOrDPDupto24MonthsVec.CFHistoryforACOrDPDupto24Months>();
                            }
                            return this.cfHistoryforACOrDPDupto24Months;
                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "month",
                            "aCorDPD",
                            "osAmount"
                        })
                        public static class CFHistoryforACOrDPDupto24Months {

                            @XmlElement(required = true)
                            protected String month;
                            @XmlElement(name = "ACorDPD", required = true)
                            protected String aCorDPD;
                            @XmlElement(name = "OSAmount", required = true)
                            protected String osAmount;

                             
                            public String getMonth() {
                                return month;
                            }

                             
                            public void setMonth(String value) {
                                this.month = value;
                            }

                             
                            public String getACorDPD() {
                                return aCorDPD;
                            }

                            
                            public void setACorDPD(String value) {
                                this.aCorDPD = value;
                            }

                            
                            public String getOSAmount() {
                                return osAmount;
                            }

                            
                            public void setOSAmount(String value) {
                                this.osAmount = value;
                            }

                        }

                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "cd3Monthcount",
                        "cd4To6Monthcount",
                        "cd7To9Monthcount",
                        "cd10To12Monthcount"
                    })
                    public static class ChequeDishounouredDuetoInsufficientFunds {

                        @XmlElement(required = true)
                        protected String message;
                        @XmlElement(name = "CD3monthcount", required = true)
                        protected String cd3Monthcount;
                        @XmlElement(name = "CD4to6monthcount", required = true)
                        protected String cd4To6Monthcount;
                        @XmlElement(name = "CD7to9monthcount", required = true)
                        protected String cd7To9Monthcount;
                        @XmlElement(name = "CD10to12monthcount", required = true)
                        protected String cd10To12Monthcount;

                        
                        public String getMessage() {
                            return message;
                        }

                        
                        public void setMessage(String value) {
                            this.message = value;
                        }

                         
                        public String getCD3Monthcount() {
                            return cd3Monthcount;
                        }

                       
                        public void setCD3Monthcount(String value) {
                            this.cd3Monthcount = value;
                        }

                         
                        public String getCD4To6Monthcount() {
                            return cd4To6Monthcount;
                        }

                        
                        public void setCD4To6Monthcount(String value) {
                            this.cd4To6Monthcount = value;
                        }

                        
                        public String getCD7To9Monthcount() {
                            return cd7To9Monthcount;
                        }

                         
                        public void setCD7To9Monthcount(String value) {
                            this.cd7To9Monthcount = value;
                        }

                        
                        public String getCD10To12Monthcount() {
                            return cd10To12Monthcount;
                        }

                        
                        public void setCD10To12Monthcount(String value) {
                            this.cd10To12Monthcount = value;
                        }

                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "creditFacilityCurrentDetails"
                    })
                    public static class CreditFacilityCurrentDetailsVec {

                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails creditFacilityCurrentDetails;

                         
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails getCreditFacilityCurrentDetails() {
                            return creditFacilityCurrentDetails;
                        }

                         
                        public void setCreditFacilityCurrentDetails(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails value) {
                            this.creditFacilityCurrentDetails = value;
                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "derivative",
                            "accountNumber",
                            "cfSerialNumber",
                            "cfType",
                            "cfMember",
                            "assetClassificationDaysPastDueDpd",
                            "status",
                            "statusDate",
                            "lastReportedDate",
                            "amount",
                            "dates",
                            "otherDetails"
                        })
                        public static class CreditFacilityCurrentDetails {

                            @XmlElement(required = true)
                            protected String derivative;
                            @XmlElement(required = true)
                            protected String accountNumber;
                            @XmlElement(required = true)
                            protected String cfSerialNumber;
                            @XmlElement(required = true)
                            protected String cfType;
                            @XmlElement(required = true)
                            protected String cfMember;
                            @XmlElement(required = true)
                            protected String assetClassificationDaysPastDueDpd;
                            @XmlElement(required = true)
                            protected String status;
                            @XmlElement(required = true)
                            protected String statusDate;
                            @XmlElement(required = true)
                            protected String lastReportedDate;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Amount amount;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Dates dates;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.OtherDetails otherDetails;

                            
                            public String getDerivative() {
                                return derivative;
                            }

                             
                            public void setDerivative(String value) {
                                this.derivative = value;
                            }

                             
                            public String getAccountNumber() {
                                return accountNumber;
                            }

                             
                            public void setAccountNumber(String value) {
                                this.accountNumber = value;
                            }

                            
                            public String getCfSerialNumber() {
                                return cfSerialNumber;
                            }

                             
                            public void setCfSerialNumber(String value) {
                                this.cfSerialNumber = value;
                            }

                             
                            public String getCfType() {
                                return cfType;
                            }

                             
                            public void setCfType(String value) {
                                this.cfType = value;
                            }

                            
                            public String getCfMember() {
                                return cfMember;
                            }

                            
                            public void setCfMember(String value) {
                                this.cfMember = value;
                            }

                             
                            public String getAssetClassificationDaysPastDueDpd() {
                                return assetClassificationDaysPastDueDpd;
                            }

                             
                            public void setAssetClassificationDaysPastDueDpd(String value) {
                                this.assetClassificationDaysPastDueDpd = value;
                            }

                            
                            public String getStatus() {
                                return status;
                            }

                             
                            public void setStatus(String value) {
                                this.status = value;
                            }

                            
                            public String getStatusDate() {
                                return statusDate;
                            }

                            
                            public void setStatusDate(String value) {
                                this.statusDate = value;
                            }

                            
                            public String getLastReportedDate() {
                                return lastReportedDate;
                            }

                             
                            public void setLastReportedDate(String value) {
                                this.lastReportedDate = value;
                            }

                            
                            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Amount getAmount() {
                                return amount;
                            }

                            
                            public void setAmount(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Amount value) {
                                this.amount = value;
                            }

                            
                            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Dates getDates() {
                                return dates;
                            }

                            
                            public void setDates(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.Dates value) {
                                this.dates = value;
                            }

                             
                            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.OtherDetails getOtherDetails() {
                                return otherDetails;
                            }

                            
                            public void setOtherDetails(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityCurrentDetailsVec.CreditFacilityCurrentDetails.OtherDetails value) {
                                this.otherDetails = value;
                            }


                             
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "currency",
                                "sanctionedAmt",
                                "drawingPower",
                                "outstandingBalance",
                                "markToMarket",
                                "overdue",
                                "highCredit",
                                "installmentAmt",
                                "suitFiledAmt",
                                "lastRepaid",
                                "writtenOFF",
                                "settled",
                                "naorc",
                                "contractsClassifiedAsNPA",
                                "notionalAmountOfContracts"
                            })
                            public static class Amount {

                                @XmlElement(required = true)
                                protected String currency;
                                @XmlElement(required = true)
                                protected String sanctionedAmt;
                                @XmlElement(required = true)
                                protected String drawingPower;
                                @XmlElement(required = true)
                                protected String outstandingBalance;
                                @XmlElement(required = true)
                                protected String markToMarket;
                                @XmlElement(required = true)
                                protected String overdue;
                                @XmlElement(required = true)
                                protected String highCredit;
                                @XmlElement(required = true)
                                protected String installmentAmt;
                                @XmlElement(required = true)
                                protected String suitFiledAmt;
                                @XmlElement(required = true)
                                protected String lastRepaid;
                                @XmlElement(required = true)
                                protected String writtenOFF;
                                @XmlElement(required = true)
                                protected String settled;
                                @XmlElement(required = true)
                                protected String naorc;
                                @XmlElement(required = true)
                                protected String contractsClassifiedAsNPA;
                                @XmlElement(required = true)
                                protected String notionalAmountOfContracts;

                                
                                public String getCurrency() {
                                    return currency;
                                }

                                
                                public void setCurrency(String value) {
                                    this.currency = value;
                                }

                                 
                                public String getSanctionedAmt() {
                                    return sanctionedAmt;
                                }

                                 
                                public void setSanctionedAmt(String value) {
                                    this.sanctionedAmt = value;
                                }

                                 
                                public String getDrawingPower() {
                                    return drawingPower;
                                }

                                 
                                public void setDrawingPower(String value) {
                                    this.drawingPower = value;
                                }

                                
                                public String getOutstandingBalance() {
                                    return outstandingBalance;
                                }

                                
                                public void setOutstandingBalance(String value) {
                                    this.outstandingBalance = value;
                                }

                                 
                                public String getMarkToMarket() {
                                    return markToMarket;
                                }

                                 
                                public void setMarkToMarket(String value) {
                                    this.markToMarket = value;
                                }

                                
                                public String getOverdue() {
                                    return overdue;
                                }

                                
                                public void setOverdue(String value) {
                                    this.overdue = value;
                                }

                                
                                public String getHighCredit() {
                                    return highCredit;
                                }

                                
                                public void setHighCredit(String value) {
                                    this.highCredit = value;
                                }

                               
                                public String getInstallmentAmt() {
                                    return installmentAmt;
                                }

                                
                                public void setInstallmentAmt(String value) {
                                    this.installmentAmt = value;
                                }

                               
                                public String getSuitFiledAmt() {
                                    return suitFiledAmt;
                                }

                                
                                public void setSuitFiledAmt(String value) {
                                    this.suitFiledAmt = value;
                                }

                                
                                public String getLastRepaid() {
                                    return lastRepaid;
                                }

                                 
                                public void setLastRepaid(String value) {
                                    this.lastRepaid = value;
                                }

                                 
                                public String getWrittenOFF() {
                                    return writtenOFF;
                                }

                                 
                                public void setWrittenOFF(String value) {
                                    this.writtenOFF = value;
                                }

                                
                                public String getSettled() {
                                    return settled;
                                }

                                 
                                public void setSettled(String value) {
                                    this.settled = value;
                                }

                                 
                                public String getNaorc() {
                                    return naorc;
                                }

                                 
                                public void setNaorc(String value) {
                                    this.naorc = value;
                                }

                                
                                public String getContractsClassifiedAsNPA() {
                                    return contractsClassifiedAsNPA;
                                }

                                 
                                public void setContractsClassifiedAsNPA(String value) {
                                    this.contractsClassifiedAsNPA = value;
                                }

                                
                                public String getNotionalAmountOfContracts() {
                                    return notionalAmountOfContracts;
                                }

                                
                                public void setNotionalAmountOfContracts(String value) {
                                    this.notionalAmountOfContracts = value;
                                }

                            }


                             
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "sanctionedDt",
                                "loanExpiryDt",
                                "loanRenewalDt",
                                "suitFiledDt",
                                "wilfulDefault"
                            })
                            public static class Dates {

                                @XmlElement(required = true)
                                protected String sanctionedDt;
                                @XmlElement(required = true)
                                protected String loanExpiryDt;
                                @XmlElement(required = true)
                                protected String loanRenewalDt;
                                @XmlElement(required = true)
                                protected String suitFiledDt;
                                @XmlElement(required = true)
                                protected String wilfulDefault;

                                
                                public String getSanctionedDt() {
                                    return sanctionedDt;
                                }

                                 
                                public void setSanctionedDt(String value) {
                                    this.sanctionedDt = value;
                                }

                                 
                                public String getLoanExpiryDt() {
                                    return loanExpiryDt;
                                }

                                 
                                public void setLoanExpiryDt(String value) {
                                    this.loanExpiryDt = value;
                                }

                                
                                public String getLoanRenewalDt() {
                                    return loanRenewalDt;
                                }

                                 
                                public void setLoanRenewalDt(String value) {
                                    this.loanRenewalDt = value;
                                }

                                 
                                public String getSuitFiledDt() {
                                    return suitFiledDt;
                                }

                                
                                public void setSuitFiledDt(String value) {
                                    this.suitFiledDt = value;
                                }

                                 
                                public String getWilfulDefault() {
                                    return wilfulDefault;
                                }

                                 
                                public void setWilfulDefault(String value) {
                                    this.wilfulDefault = value;
                                }

                            }


                             
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "repaymentFrequency",
                                "tenure",
                                "weightedAverageMaturityPeriodOfContracts",
                                "restructingReason",
                                "assetBasedSecurityCoverage",
                                "guaranteeCoverage"
                            })
                            public static class OtherDetails {

                                @XmlElement(required = true)
                                protected String repaymentFrequency;
                                @XmlElement(required = true)
                                protected String tenure;
                                @XmlElement(required = true)
                                protected String weightedAverageMaturityPeriodOfContracts;
                                @XmlElement(required = true)
                                protected String restructingReason;
                                @XmlElement(required = true)
                                protected String assetBasedSecurityCoverage;
                                @XmlElement(name = "GuaranteeCoverage", required = true)
                                protected String guaranteeCoverage;

                               
                                public String getRepaymentFrequency() {
                                    return repaymentFrequency;
                                }

                                
                                public void setRepaymentFrequency(String value) {
                                    this.repaymentFrequency = value;
                                }

                                
                                public String getTenure() {
                                    return tenure;
                                }

                                 
                                public void setTenure(String value) {
                                    this.tenure = value;
                                }

                                 
                                public String getWeightedAverageMaturityPeriodOfContracts() {
                                    return weightedAverageMaturityPeriodOfContracts;
                                }

                                 
                                public void setWeightedAverageMaturityPeriodOfContracts(String value) {
                                    this.weightedAverageMaturityPeriodOfContracts = value;
                                }

                               
                                public String getRestructingReason() {
                                    return restructingReason;
                                }

                                 
                                public void setRestructingReason(String value) {
                                    this.restructingReason = value;
                                }

                                 
                                public String getAssetBasedSecurityCoverage() {
                                    return assetBasedSecurityCoverage;
                                }

                                
                                public void setAssetBasedSecurityCoverage(String value) {
                                    this.assetBasedSecurityCoverage = value;
                                }

                                 
                                public String getGuaranteeCoverage() {
                                    return guaranteeCoverage;
                                }

                                
                                public void setGuaranteeCoverage(String value) {
                                    this.guaranteeCoverage = value;
                                }

                            }

                        }

                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "creditFacilityGuarantorDetails"
                    })
                    public static class CreditFacilityGuarantorDetailsVec {

                        @XmlElement(required = true)
                        protected String message;
                        protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails> creditFacilityGuarantorDetails;

                       
                        public String getMessage() {
                            return message;
                        }

                         
                        public void setMessage(String value) {
                            this.message = value;
                        }

                        
                        public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails> getCreditFacilityGuarantorDetails() {
                            if (creditFacilityGuarantorDetails == null) {
                                creditFacilityGuarantorDetails = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails>();
                            }
                            return this.creditFacilityGuarantorDetails;
                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "guarantorDetails",
                            "guarantorAddressContactDetails",
                            "guarantorDetailsBorrwerIDDetailsVec"
                        })
                        public static class CreditFacilityGuarantorDetails {

                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetails guarantorDetails;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorAddressContactDetails guarantorAddressContactDetails;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec guarantorDetailsBorrwerIDDetailsVec;

                            
                            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetails getGuarantorDetails() {
                                return guarantorDetails;
                            }

                            
                            public void setGuarantorDetails(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetails value) {
                                this.guarantorDetails = value;
                            }

                             
                            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorAddressContactDetails getGuarantorAddressContactDetails() {
                                return guarantorAddressContactDetails;
                            }

                             
                            public void setGuarantorAddressContactDetails(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorAddressContactDetails value) {
                                this.guarantorAddressContactDetails = value;
                            }

                             
                            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec getGuarantorDetailsBorrwerIDDetailsVec() {
                                return guarantorDetailsBorrwerIDDetailsVec;
                            }

                            
                            public void setGuarantorDetailsBorrwerIDDetailsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec value) {
                                this.guarantorDetailsBorrwerIDDetailsVec = value;
                            }


                             
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "address",
                                "telephoneNumber",
                                "faxNumber",
                                "mobileNumber"
                            })
                            public static class GuarantorAddressContactDetails {

                                @XmlElement(required = true)
                                protected String address;
                                @XmlElement(required = true)
                                protected String telephoneNumber;
                                @XmlElement(required = true)
                                protected String faxNumber;
                                @XmlElement(required = true)
                                protected String mobileNumber;

                                
                                public String getAddress() {
                                    return address;
                                }

                                 
                                public void setAddress(String value) {
                                    this.address = value;
                                }

                                
                                public String getTelephoneNumber() {
                                    return telephoneNumber;
                                }

                                 
                                public void setTelephoneNumber(String value) {
                                    this.telephoneNumber = value;
                                }

                                 
                                public String getFaxNumber() {
                                    return faxNumber;
                                }

                                
                                public void setFaxNumber(String value) {
                                    this.faxNumber = value;
                                }

                                
                                public String getMobileNumber() {
                                    return mobileNumber;
                                }

                                
                                public void setMobileNumber(String value) {
                                    this.mobileNumber = value;
                                }

                            }


                             
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "name",
                                "relatedType",
                                "dateOfBirth",
                                "gender",
                                "dateOfIncorporation",
                                "businessCategory",
                                "businessIndustryType",
                                "message"
                            })
                            public static class GuarantorDetails {

                                @XmlElement(required = true)
                                protected String name;
                                @XmlElement(required = true)
                                protected String relatedType;
                                @XmlElement(required = true)
                                protected String dateOfBirth;
                                @XmlElement(required = true)
                                protected String gender;
                                @XmlElement(required = true)
                                protected String dateOfIncorporation;
                                @XmlElement(required = true)
                                protected String businessCategory;
                                @XmlElement(required = true)
                                protected String businessIndustryType;
                                @XmlElement(required = true)
                                protected String message;

                                
                                public String getName() {
                                    return name;
                                }

                                 
                                public void setName(String value) {
                                    this.name = value;
                                }

                                
                                public String getRelatedType() {
                                    return relatedType;
                                }

                                
                                public void setRelatedType(String value) {
                                    this.relatedType = value;
                                }

                                
                                public String getDateOfBirth() {
                                    return dateOfBirth;
                                }

                                
                                public void setDateOfBirth(String value) {
                                    this.dateOfBirth = value;
                                }

                                
                                public String getGender() {
                                    return gender;
                                }

                                 
                                public void setGender(String value) {
                                    this.gender = value;
                                }

                                
                                public String getDateOfIncorporation() {
                                    return dateOfIncorporation;
                                }

                                
                                public void setDateOfIncorporation(String value) {
                                    this.dateOfIncorporation = value;
                                }

                                
                                public String getBusinessCategory() {
                                    return businessCategory;
                                }

                                 
                                public void setBusinessCategory(String value) {
                                    this.businessCategory = value;
                                }

                                 
                                public String getBusinessIndustryType() {
                                    return businessIndustryType;
                                }

                                 
                                public void setBusinessIndustryType(String value) {
                                    this.businessIndustryType = value;
                                }

                                
                                public String getMessage() {
                                    return message;
                                }

                                 
                                public void setMessage(String value) {
                                    this.message = value;
                                }

                            }


                             
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "lastReportedDate",
                                "guarantorIDDetails"
                            })
                            public static class GuarantorDetailsBorrwerIDDetailsVec {

                                @XmlElement(required = true)
                                protected String lastReportedDate;
                                protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec.GuarantorIDDetails> guarantorIDDetails;

                                
                                public String getLastReportedDate() {
                                    return lastReportedDate;
                                }

                                
                                public void setLastReportedDate(String value) {
                                    this.lastReportedDate = value;
                                }

                                
                                public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec.GuarantorIDDetails> getGuarantorIDDetails() {
                                    if (guarantorIDDetails == null) {
                                        guarantorIDDetails = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityGuarantorDetailsVec.CreditFacilityGuarantorDetails.GuarantorDetailsBorrwerIDDetailsVec.GuarantorIDDetails>();
                                    }
                                    return this.guarantorIDDetails;
                                }


                                 
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "", propOrder = {
                                    "pan",
                                    "registrationNumber",
                                    "cin",
                                    "tin",
                                    "serviceTaxNumber",
                                    "din",
                                    "voterID",
                                    "passportNumber",
                                    "uid",
                                    "drivingLicenseNumber",
                                    "rationCard",
                                    "otherID"
                                })
                                public static class GuarantorIDDetails {

                                    @XmlElement(required = true)
                                    protected String pan;
                                    @XmlElement(required = true)
                                    protected String registrationNumber;
                                    @XmlElement(required = true)
                                    protected String cin;
                                    @XmlElement(required = true)
                                    protected String tin;
                                    @XmlElement(required = true)
                                    protected String serviceTaxNumber;
                                    @XmlElement(required = true)
                                    protected String din;
                                    @XmlElement(required = true)
                                    protected String voterID;
                                    @XmlElement(required = true)
                                    protected String passportNumber;
                                    @XmlElement(required = true)
                                    protected String uid;
                                    @XmlElement(required = true)
                                    protected String drivingLicenseNumber;
                                    @XmlElement(required = true)
                                    protected String rationCard;
                                    @XmlElement(required = true)
                                    protected String otherID;

                                    
                                    public String getPan() {
                                        return pan;
                                    }

                                     
                                    public void setPan(String value) {
                                        this.pan = value;
                                    }

                                    
                                    public String getRegistrationNumber() {
                                        return registrationNumber;
                                    }

                                    
                                    public void setRegistrationNumber(String value) {
                                        this.registrationNumber = value;
                                    }

                                     
                                    public String getCin() {
                                        return cin;
                                    }

                                     
                                    public void setCin(String value) {
                                        this.cin = value;
                                    }

                                     
                                    public String getTin() {
                                        return tin;
                                    }

                                    
                                    public void setTin(String value) {
                                        this.tin = value;
                                    }

                                    
                                    public String getServiceTaxNumber() {
                                        return serviceTaxNumber;
                                    }

                                     
                                    public void setServiceTaxNumber(String value) {
                                        this.serviceTaxNumber = value;
                                    }

                                   
                                    public String getDin() {
                                        return din;
                                    }

                                     
                                    public void setDin(String value) {
                                        this.din = value;
                                    }

                                    
                                    public String getVoterID() {
                                        return voterID;
                                    }

                                    
                                    public void setVoterID(String value) {
                                        this.voterID = value;
                                    }

                                    
                                    public String getPassportNumber() {
                                        return passportNumber;
                                    }

                                     
                                    public void setPassportNumber(String value) {
                                        this.passportNumber = value;
                                    }

                                     
                                    public String getUid() {
                                        return uid;
                                    }

                                     
                                    public void setUid(String value) {
                                        this.uid = value;
                                    }

                                    
                                    public String getDrivingLicenseNumber() {
                                        return drivingLicenseNumber;
                                    }

                                     
                                    public void setDrivingLicenseNumber(String value) {
                                        this.drivingLicenseNumber = value;
                                    }

                                     
                                    public String getRationCard() {
                                        return rationCard;
                                    }

                                     
                                    public void setRationCard(String value) {
                                        this.rationCard = value;
                                    }

                                    
                                    public String getOtherID() {
                                        return otherID;
                                    }

                                     
                                    public void setOtherID(String value) {
                                        this.otherID = value;
                                    }

                                }

                            }

                        }

                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "creditFacilityOverdueDetails"
                    })
                    public static class CreditFacilityOverdueDetailsVec {

                        @XmlElement(required = true)
                        protected String message;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec.CreditFacilityOverdueDetails creditFacilityOverdueDetails;

                       
                        public String getMessage() {
                            return message;
                        }

                       
                        public void setMessage(String value) {
                            this.message = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec.CreditFacilityOverdueDetails getCreditFacilityOverdueDetails() {
                            return creditFacilityOverdueDetails;
                        }

                       
                        public void setCreditFacilityOverdueDetails(Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilityOverdueDetailsVec.CreditFacilityOverdueDetails value) {
                            this.creditFacilityOverdueDetails = value;
                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd1To30Amt",
                            "dpd31To60Amt",
                            "dpd61T090Amt",
                            "dpd91To180Amt",
                            "dpDabove180Amt"
                        })
                        public static class CreditFacilityOverdueDetails {

                            @XmlElement(name = "DPD1to30amt", required = true)
                            protected String dpd1To30Amt;
                            @XmlElement(name = "DPD31to60amt", required = true)
                            protected String dpd31To60Amt;
                            @XmlElement(name = "DPD61t090amt", required = true)
                            protected String dpd61T090Amt;
                            @XmlElement(name = "DPD91to180amt", required = true)
                            protected String dpd91To180Amt;
                            @XmlElement(name = "DPDabove180amt", required = true)
                            protected String dpDabove180Amt;

                           
                            public String getDPD1To30Amt() {
                                return dpd1To30Amt;
                            }

                            
                            public void setDPD1To30Amt(String value) {
                                this.dpd1To30Amt = value;
                            }

                            
                            public String getDPD31To60Amt() {
                                return dpd31To60Amt;
                            }

                            
                            public void setDPD31To60Amt(String value) {
                                this.dpd31To60Amt = value;
                            }

                           
                            public String getDPD61T090Amt() {
                                return dpd61T090Amt;
                            }

                            
                            public void setDPD61T090Amt(String value) {
                                this.dpd61T090Amt = value;
                            }

                           
                            public String getDPD91To180Amt() {
                                return dpd91To180Amt;
                            }

                           
                            public void setDPD91To180Amt(String value) {
                                this.dpd91To180Amt = value;
                            }

                            
                            public String getDPDabove180Amt() {
                                return dpDabove180Amt;
                            }

                           
                            public void setDPDabove180Amt(String value) {
                                this.dpDabove180Amt = value;
                            }

                        }

                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "creditFacilitySecurityDetails"
                    })
                    public static class CreditFacilitySecurityDetailsVec {

                        @XmlElement(required = true)
                        protected String message;
                        protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec.CreditFacilitySecurityDetails> creditFacilitySecurityDetails;

                       
                        public String getMessage() {
                            return message;
                        }

                       
                        public void setMessage(String value) {
                            this.message = value;
                        }

                        
                        public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec.CreditFacilitySecurityDetails> getCreditFacilitySecurityDetails() {
                            if (creditFacilitySecurityDetails == null) {
                                creditFacilitySecurityDetails = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasBorrowerSecVec.CreditFacilityDetailsasBorrowerSec.CreditFacilitySecurityDetailsVec.CreditFacilitySecurityDetails>();
                            }
                            return this.creditFacilitySecurityDetails;
                        }


                      
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "relatedType",
                            "classification",
                            "value",
                            "currency",
                            "validationDt",
                            "lastReportedDt"
                        })
                        public static class CreditFacilitySecurityDetails {

                            @XmlElement(required = true)
                            protected String relatedType;
                            @XmlElement(required = true)
                            protected String classification;
                            @XmlElement(required = true)
                            protected String value;
                            @XmlElement(required = true)
                            protected String currency;
                            @XmlElement(required = true)
                            protected String validationDt;
                            @XmlElement(required = true)
                            protected String lastReportedDt;

                            
                            public String getRelatedType() {
                                return relatedType;
                            }

                           
                            public void setRelatedType(String value) {
                                this.relatedType = value;
                            }

                           
                            public String getClassification() {
                                return classification;
                            }

                           
                            public void setClassification(String value) {
                                this.classification = value;
                            }

                            
                            public String getValue() {
                                return value;
                            }

                           
                            public void setValue(String value) {
                                this.value = value;
                            }

                            
                            public String getCurrency() {
                                return currency;
                            }

                            
                            public void setCurrency(String value) {
                                this.currency = value;
                            }

                            
                            public String getValidationDt() {
                                return validationDt;
                            }

                           
                            public void setValidationDt(String value) {
                                this.validationDt = value;
                            }

                           
                            public String getLastReportedDt() {
                                return lastReportedDt;
                            }

                            
                            public void setLastReportedDt(String value) {
                                this.lastReportedDt = value;
                            }

                        }

                    }

                }

            }


            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "creditFacilityDetailsasGuarantor"
            })
            public static class CreditFacilityDetailsasGuarantorVec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor> creditFacilityDetailsasGuarantor;

               
                public String getMessage() {
                    return message;
                }

               
                public void setMessage(String value) {
                    this.message = value;
                }

                
                public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor> getCreditFacilityDetailsasGuarantor() {
                    if (creditFacilityDetailsasGuarantor == null) {
                        creditFacilityDetailsasGuarantor = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor>();
                    }
                    return this.creditFacilityDetailsasGuarantor;
                }


               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "creditFacilityCurrentDetails",
                    "borrwerInfo"
                })
                public static class CreditFacilityDetailsasGuarantor {

                    protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails> creditFacilityCurrentDetails;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo borrwerInfo;

                  
                    public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails> getCreditFacilityCurrentDetails() {
                        if (creditFacilityCurrentDetails == null) {
                            creditFacilityCurrentDetails = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails>();
                        }
                        return this.creditFacilityCurrentDetails;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo getBorrwerInfo() {
                        return borrwerInfo;
                    }

                    
                    public void setBorrwerInfo(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo value) {
                        this.borrwerInfo = value;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "borrwerDetails",
                        "borrwerAddressContactDetailsVec",
                        "borrwerIDDetailsVec"
                    })
                    public static class BorrwerInfo {

                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails borrwerDetails;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec borrwerAddressContactDetailsVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec borrwerIDDetailsVec;

                        
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails getBorrwerDetails() {
                            return borrwerDetails;
                        }

                       
                        public void setBorrwerDetails(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails value) {
                            this.borrwerDetails = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec getBorrwerAddressContactDetailsVec() {
                            return borrwerAddressContactDetailsVec;
                        }

                        
                        public void setBorrwerAddressContactDetailsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec value) {
                            this.borrwerAddressContactDetailsVec = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec getBorrwerIDDetailsVec() {
                            return borrwerIDDetailsVec;
                        }

                       
                        public void setBorrwerIDDetailsVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec value) {
                            this.borrwerIDDetailsVec = value;
                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrwerAddressContactDetails"
                        })
                        public static class BorrwerAddressContactDetailsVec {

                            protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec.BorrwerAddressContactDetails> borrwerAddressContactDetails;

                            
                            public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec.BorrwerAddressContactDetails> getBorrwerAddressContactDetails() {
                                if (borrwerAddressContactDetails == null) {
                                    borrwerAddressContactDetails = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerAddressContactDetailsVec.BorrwerAddressContactDetails>();
                                }
                                return this.borrwerAddressContactDetails;
                            }


                            
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "address",
                                "telephoneNumber",
                                "faxNumber",
                                "mobileNumber"
                            })
                            public static class BorrwerAddressContactDetails {

                                @XmlElement(required = true)
                                protected String address;
                                @XmlElement(required = true)
                                protected String telephoneNumber;
                                @XmlElement(required = true)
                                protected String faxNumber;
                                @XmlElement(required = true)
                                protected String mobileNumber;

                               
                                public String getAddress() {
                                    return address;
                                }

                                
                                public void setAddress(String value) {
                                    this.address = value;
                                }

                               
                                public String getTelephoneNumber() {
                                    return telephoneNumber;
                                }

                               
                                public void setTelephoneNumber(String value) {
                                    this.telephoneNumber = value;
                                }

                                
                                public String getFaxNumber() {
                                    return faxNumber;
                                }

                                
                                public void setFaxNumber(String value) {
                                    this.faxNumber = value;
                                }

                               
                                public String getMobileNumber() {
                                    return mobileNumber;
                                }

                                
                                public void setMobileNumber(String value) {
                                    this.mobileNumber = value;
                                }

                            }

                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "name",
                            "borrowersLegalConstitution",
                            "classOfActivityVec",
                            "businessCategory",
                            "businessIndustryType",
                            "salesFigure",
                            "numberOfEmployees",
                            "dateOfIncorporation",
                            "year"
                        })
                        public static class BorrwerDetails {

                            @XmlElement(required = true)
                            protected String name;
                            @XmlElement(required = true)
                            protected String borrowersLegalConstitution;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails.ClassOfActivityVec classOfActivityVec;
                            @XmlElement(required = true)
                            protected String businessCategory;
                            @XmlElement(required = true)
                            protected String businessIndustryType;
                            @XmlElement(required = true)
                            protected String salesFigure;
                            @XmlElement(required = true)
                            protected String numberOfEmployees;
                            @XmlElement(required = true)
                            protected String dateOfIncorporation;
                            @XmlElement(required = true)
                            protected String year;

                           
                            public String getName() {
                                return name;
                            }

                           
                            public void setName(String value) {
                                this.name = value;
                            }

                           
                            public String getBorrowersLegalConstitution() {
                                return borrowersLegalConstitution;
                            }

                            
                            public void setBorrowersLegalConstitution(String value) {
                                this.borrowersLegalConstitution = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails.ClassOfActivityVec getClassOfActivityVec() {
                                return classOfActivityVec;
                            }

                           
                            public void setClassOfActivityVec(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerDetails.ClassOfActivityVec value) {
                                this.classOfActivityVec = value;
                            }

                            
                            public String getBusinessCategory() {
                                return businessCategory;
                            }

                            
                            public void setBusinessCategory(String value) {
                                this.businessCategory = value;
                            }

                            
                            public String getBusinessIndustryType() {
                                return businessIndustryType;
                            }

                           
                            public void setBusinessIndustryType(String value) {
                                this.businessIndustryType = value;
                            }

                            
                            public String getSalesFigure() {
                                return salesFigure;
                            }

                            
                            public void setSalesFigure(String value) {
                                this.salesFigure = value;
                            }

                            
                            public String getNumberOfEmployees() {
                                return numberOfEmployees;
                            }

                            
                            public void setNumberOfEmployees(String value) {
                                this.numberOfEmployees = value;
                            }

                            
                            public String getDateOfIncorporation() {
                                return dateOfIncorporation;
                            }

                           
                            public void setDateOfIncorporation(String value) {
                                this.dateOfIncorporation = value;
                            }

                           
                            public String getYear() {
                                return year;
                            }

                            
                            public void setYear(String value) {
                                this.year = value;
                            }


                            
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "classOfActivity"
                            })
                            public static class ClassOfActivityVec {

                                protected List<String> classOfActivity;

                                
                                public List<String> getClassOfActivity() {
                                    if (classOfActivity == null) {
                                        classOfActivity = new ArrayList<String>();
                                    }
                                    return this.classOfActivity;
                                }

                            }

                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "lastReportedDate",
                            "borrwerIDDetails"
                        })
                        public static class BorrwerIDDetailsVec {

                            @XmlElement(required = true)
                            protected String lastReportedDate;
                            protected List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec.BorrwerIDDetails> borrwerIDDetails;

                           
                            public String getLastReportedDate() {
                                return lastReportedDate;
                            }

                            
                            public void setLastReportedDate(String value) {
                                this.lastReportedDate = value;
                            }

                            
                            public List<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec.BorrwerIDDetails> getBorrwerIDDetails() {
                                if (borrwerIDDetails == null) {
                                    borrwerIDDetails = new ArrayList<Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.BorrwerInfo.BorrwerIDDetailsVec.BorrwerIDDetails>();
                                }
                                return this.borrwerIDDetails;
                            }


                            
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "cin",
                                "pan",
                                "tin",
                                "registrationNumber",
                                "serviceTaxNumber"
                            })
                            public static class BorrwerIDDetails {

                                @XmlElement(required = true)
                                protected String cin;
                                @XmlElement(required = true)
                                protected String pan;
                                @XmlElement(required = true)
                                protected String tin;
                                @XmlElement(required = true)
                                protected String registrationNumber;
                                @XmlElement(required = true)
                                protected String serviceTaxNumber;

                               
                                public String getCin() {
                                    return cin;
                                }

                                
                                public void setCin(String value) {
                                    this.cin = value;
                                }

                               
                                public String getPan() {
                                    return pan;
                                }

                               
                                public void setPan(String value) {
                                    this.pan = value;
                                }

                                
                                public String getTin() {
                                    return tin;
                                }

                               
                                public void setTin(String value) {
                                    this.tin = value;
                                }

                               
                                public String getRegistrationNumber() {
                                    return registrationNumber;
                                }

                                
                                public void setRegistrationNumber(String value) {
                                    this.registrationNumber = value;
                                }

                                
                                public String getServiceTaxNumber() {
                                    return serviceTaxNumber;
                                }

                               
                                public void setServiceTaxNumber(String value) {
                                    this.serviceTaxNumber = value;
                                }

                            }

                        }

                    }


                   
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "derivative",
                        "accountNumber",
                        "cfSerialNumber",
                        "cfType",
                        "cfMember",
                        "assetClassificationDaysPastDueDpd",
                        "status",
                        "statusDate",
                        "lastReportedDate",
                        "amount",
                        "dates",
                        "otherDetails"
                    })
                    public static class CreditFacilityCurrentDetails {

                        @XmlElement(required = true)
                        protected String derivative;
                        @XmlElement(required = true)
                        protected String accountNumber;
                        @XmlElement(required = true)
                        protected String cfSerialNumber;
                        @XmlElement(required = true)
                        protected String cfType;
                        @XmlElement(required = true)
                        protected String cfMember;
                        @XmlElement(required = true)
                        protected String assetClassificationDaysPastDueDpd;
                        @XmlElement(required = true)
                        protected String status;
                        @XmlElement(required = true)
                        protected String statusDate;
                        @XmlElement(required = true)
                        protected String lastReportedDate;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Amount amount;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Dates dates;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.OtherDetails otherDetails;

                        
                        public String getDerivative() {
                            return derivative;
                        }

                        
                        public void setDerivative(String value) {
                            this.derivative = value;
                        }

                        
                        public String getAccountNumber() {
                            return accountNumber;
                        }

                        
                        public void setAccountNumber(String value) {
                            this.accountNumber = value;
                        }

                        
                        public String getCfSerialNumber() {
                            return cfSerialNumber;
                        }

                         
                        public void setCfSerialNumber(String value) {
                            this.cfSerialNumber = value;
                        }

                        
                        public String getCfType() {
                            return cfType;
                        }

                        
                        public void setCfType(String value) {
                            this.cfType = value;
                        }

                        
                        public String getCfMember() {
                            return cfMember;
                        }

                        
                        public void setCfMember(String value) {
                            this.cfMember = value;
                        }

                        
                        public String getAssetClassificationDaysPastDueDpd() {
                            return assetClassificationDaysPastDueDpd;
                        }

                         
                        public void setAssetClassificationDaysPastDueDpd(String value) {
                            this.assetClassificationDaysPastDueDpd = value;
                        }

                        
                        public String getStatus() {
                            return status;
                        }

                        
                        public void setStatus(String value) {
                            this.status = value;
                        }

                         
                        public String getStatusDate() {
                            return statusDate;
                        }

                         
                        public void setStatusDate(String value) {
                            this.statusDate = value;
                        }

                        
                        public String getLastReportedDate() {
                            return lastReportedDate;
                        }

                         
                        public void setLastReportedDate(String value) {
                            this.lastReportedDate = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Amount getAmount() {
                            return amount;
                        }

                        
                        public void setAmount(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Amount value) {
                            this.amount = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Dates getDates() {
                            return dates;
                        }

                       
                        public void setDates(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.Dates value) {
                            this.dates = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.OtherDetails getOtherDetails() {
                            return otherDetails;
                        }

                        
                        public void setOtherDetails(Base.ResponseReport.ProductSec.CreditFacilityDetailsasGuarantorVec.CreditFacilityDetailsasGuarantor.CreditFacilityCurrentDetails.OtherDetails value) {
                            this.otherDetails = value;
                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "currency",
                            "sanctionedAmt",
                            "drawingPower",
                            "outstandingBalance",
                            "markToMarket",
                            "overdue",
                            "highCredit",
                            "installmentAmt",
                            "suitFiledAmt",
                            "lastRepaid",
                            "writtenOFF",
                            "settled",
                            "naorc",
                            "contractsClassifiedAsNPA",
                            "notionalAmountOfContracts"
                        })
                        public static class Amount {

                            @XmlElement(required = true)
                            protected String currency;
                            @XmlElement(required = true)
                            protected String sanctionedAmt;
                            @XmlElement(required = true)
                            protected String drawingPower;
                            @XmlElement(required = true)
                            protected String outstandingBalance;
                            @XmlElement(required = true)
                            protected String markToMarket;
                            @XmlElement(required = true)
                            protected String overdue;
                            @XmlElement(required = true)
                            protected String highCredit;
                            @XmlElement(required = true)
                            protected String installmentAmt;
                            @XmlElement(required = true)
                            protected String suitFiledAmt;
                            @XmlElement(required = true)
                            protected String lastRepaid;
                            @XmlElement(required = true)
                            protected String writtenOFF;
                            @XmlElement(required = true)
                            protected String settled;
                            @XmlElement(required = true)
                            protected String naorc;
                            @XmlElement(required = true)
                            protected String contractsClassifiedAsNPA;
                            @XmlElement(required = true)
                            protected String notionalAmountOfContracts;

                            
                            public String getCurrency() {
                                return currency;
                            }

                            
                            public void setCurrency(String value) {
                                this.currency = value;
                            }

                            
                            public String getSanctionedAmt() {
                                return sanctionedAmt;
                            }

                            
                            public void setSanctionedAmt(String value) {
                                this.sanctionedAmt = value;
                            }

                             
                            public String getDrawingPower() {
                                return drawingPower;
                            }

                            
                            public void setDrawingPower(String value) {
                                this.drawingPower = value;
                            }

                            
                            public String getOutstandingBalance() {
                                return outstandingBalance;
                            }

                             
                            public void setOutstandingBalance(String value) {
                                this.outstandingBalance = value;
                            }

                            
                            public String getMarkToMarket() {
                                return markToMarket;
                            }

                             
                            public void setMarkToMarket(String value) {
                                this.markToMarket = value;
                            }

                             
                            public String getOverdue() {
                                return overdue;
                            }

                            
                            public void setOverdue(String value) {
                                this.overdue = value;
                            }

                            
                            public String getHighCredit() {
                                return highCredit;
                            }

                             
                            public void setHighCredit(String value) {
                                this.highCredit = value;
                            }

                             
                            public String getInstallmentAmt() {
                                return installmentAmt;
                            }

                             
                            public void setInstallmentAmt(String value) {
                                this.installmentAmt = value;
                            }

                            
                            public String getSuitFiledAmt() {
                                return suitFiledAmt;
                            }

                            
                            public void setSuitFiledAmt(String value) {
                                this.suitFiledAmt = value;
                            }

                            
                            public String getLastRepaid() {
                                return lastRepaid;
                            }

                             
                            public void setLastRepaid(String value) {
                                this.lastRepaid = value;
                            }

                            
                            public String getWrittenOFF() {
                                return writtenOFF;
                            }

                             
                            public void setWrittenOFF(String value) {
                                this.writtenOFF = value;
                            }

                             
                            public String getSettled() {
                                return settled;
                            }

                             
                            public void setSettled(String value) {
                                this.settled = value;
                            }

                            
                            public String getNaorc() {
                                return naorc;
                            }

                            
                            public void setNaorc(String value) {
                                this.naorc = value;
                            }

                            
                            public String getContractsClassifiedAsNPA() {
                                return contractsClassifiedAsNPA;
                            }

                            
                            public void setContractsClassifiedAsNPA(String value) {
                                this.contractsClassifiedAsNPA = value;
                            }

                           
                            public String getNotionalAmountOfContracts() {
                                return notionalAmountOfContracts;
                            }

                            
                            public void setNotionalAmountOfContracts(String value) {
                                this.notionalAmountOfContracts = value;
                            }

                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "sanctionedDt",
                            "loanExpiryDt",
                            "loanRenewalDt",
                            "suitFiledDt",
                            "wilfulDefault"
                        })
                        public static class Dates {

                            @XmlElement(required = true)
                            protected String sanctionedDt;
                            @XmlElement(required = true)
                            protected String loanExpiryDt;
                            @XmlElement(required = true)
                            protected String loanRenewalDt;
                            @XmlElement(required = true)
                            protected String suitFiledDt;
                            @XmlElement(required = true)
                            protected String wilfulDefault;

                            
                            public String getSanctionedDt() {
                                return sanctionedDt;
                            }

                             
                            public void setSanctionedDt(String value) {
                                this.sanctionedDt = value;
                            }

                            
                            public String getLoanExpiryDt() {
                                return loanExpiryDt;
                            }

                            
                            public void setLoanExpiryDt(String value) {
                                this.loanExpiryDt = value;
                            }

                             
                            public String getLoanRenewalDt() {
                                return loanRenewalDt;
                            }

                            
                            public void setLoanRenewalDt(String value) {
                                this.loanRenewalDt = value;
                            }

                            
                            public String getSuitFiledDt() {
                                return suitFiledDt;
                            }

                            
                            public void setSuitFiledDt(String value) {
                                this.suitFiledDt = value;
                            }

                             
                            public String getWilfulDefault() {
                                return wilfulDefault;
                            }

                             
                            public void setWilfulDefault(String value) {
                                this.wilfulDefault = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "repaymentFrequency",
                            "tenure",
                            "weightedAverageMaturityPeriodOfContracts",
                            "restructingReason",
                            "assetBasedSecurityCoverage",
                            "guaranteeCoverage"
                        })
                        public static class OtherDetails {

                            @XmlElement(required = true)
                            protected String repaymentFrequency;
                            @XmlElement(required = true)
                            protected String tenure;
                            @XmlElement(required = true)
                            protected String weightedAverageMaturityPeriodOfContracts;
                            @XmlElement(required = true)
                            protected String restructingReason;
                            @XmlElement(required = true)
                            protected String assetBasedSecurityCoverage;
                            @XmlElement(name = "GuaranteeCoverage", required = true)
                            protected String guaranteeCoverage;

                             
                            public String getRepaymentFrequency() {
                                return repaymentFrequency;
                            }

                            
                            public void setRepaymentFrequency(String value) {
                                this.repaymentFrequency = value;
                            }

                            
                            public String getTenure() {
                                return tenure;
                            }

                             
                            public void setTenure(String value) {
                                this.tenure = value;
                            }

                            
                            public String getWeightedAverageMaturityPeriodOfContracts() {
                                return weightedAverageMaturityPeriodOfContracts;
                            }

                            
                            public void setWeightedAverageMaturityPeriodOfContracts(String value) {
                                this.weightedAverageMaturityPeriodOfContracts = value;
                            }

                            
                            public String getRestructingReason() {
                                return restructingReason;
                            }

                            
                            public void setRestructingReason(String value) {
                                this.restructingReason = value;
                            }

                            
                            public String getAssetBasedSecurityCoverage() {
                                return assetBasedSecurityCoverage;
                            }

                             
                            public void setAssetBasedSecurityCoverage(String value) {
                                this.assetBasedSecurityCoverage = value;
                            }

                            
                            public String getGuaranteeCoverage() {
                                return guaranteeCoverage;
                            }

                            
                            public void setGuaranteeCoverage(String value) {
                                this.guaranteeCoverage = value;
                            }

                        }

                    }

                }

            }


            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "yourInstitution",
                "outsideInstitution",
                "total"
            })
            public static class CreditProfileSummarySec {

                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution yourInstitution;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution outsideInstitution;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.Total total;

                
                public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution getYourInstitution() {
                    return yourInstitution;
                }

                 
                public void setYourInstitution(Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution value) {
                    this.yourInstitution = value;
                }

                 
                public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution getOutsideInstitution() {
                    return outsideInstitution;
                }

                 
                public void setOutsideInstitution(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution value) {
                    this.outsideInstitution = value;
                }

                 
                public Base.ResponseReport.ProductSec.CreditProfileSummarySec.Total getTotal() {
                    return total;
                }

                
                public void setTotal(Base.ResponseReport.ProductSec.CreditProfileSummarySec.Total value) {
                    this.total = value;
                }


                 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "message",
                    "otherPublicSectorBanks",
                    "otherPrivateForeignBanks",
                    "nbfcOthers",
                    "outsideTotal"
                })
                public static class OutsideInstitution {

                    @XmlElement(required = true)
                    protected String message;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks otherPublicSectorBanks;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks otherPrivateForeignBanks;
                    @XmlElement(name = "NBFC_Others", required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers nbfcOthers;
                    @XmlElement(name = "OutsideTotal", required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal outsideTotal;

                    
                    public String getMessage() {
                        return message;
                    }

                     
                    public void setMessage(String value) {
                        this.message = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks getOtherPublicSectorBanks() {
                        return otherPublicSectorBanks;
                    }

                    
                    public void setOtherPublicSectorBanks(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks value) {
                        this.otherPublicSectorBanks = value;
                    }

                     
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks getOtherPrivateForeignBanks() {
                        return otherPrivateForeignBanks;
                    }

                    
                    public void setOtherPrivateForeignBanks(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks value) {
                        this.otherPrivateForeignBanks = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers getNBFCOthers() {
                        return nbfcOthers;
                    }

                    
                    public void setNBFCOthers(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers value) {
                        this.nbfcOthers = value;
                    }

                     
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal getOutsideTotal() {
                        return outsideTotal;
                    }

                    
                    public void setOutsideTotal(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal value) {
                        this.outsideTotal = value;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "totalLenders",
                        "totalCF",
                        "openCF",
                        "totalOutstanding",
                        "latestCFOpenedDate",
                        "delinquentCF",
                        "delinquentOutstanding"
                    })
                    public static class NBFCOthers {

                        @XmlElement(required = true)
                        protected String message;
                        @XmlElement(required = true)
                        protected String totalLenders;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalCF totalCF;
                        @XmlElement(required = true)
                        protected String openCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalOutstanding totalOutstanding;
                        @XmlElement(required = true)
                        protected String latestCFOpenedDate;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentCF delinquentCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentOutstanding delinquentOutstanding;

                         
                        public String getMessage() {
                            return message;
                        }

                         
                        public void setMessage(String value) {
                            this.message = value;
                        }

                         
                        public String getTotalLenders() {
                            return totalLenders;
                        }

                        
                        public void setTotalLenders(String value) {
                            this.totalLenders = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalCF getTotalCF() {
                            return totalCF;
                        }

                        
                        public void setTotalCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalCF value) {
                            this.totalCF = value;
                        }

                        
                        public String getOpenCF() {
                            return openCF;
                        }

                         
                        public void setOpenCF(String value) {
                            this.openCF = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalOutstanding getTotalOutstanding() {
                            return totalOutstanding;
                        }

                        
                        public void setTotalOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.TotalOutstanding value) {
                            this.totalOutstanding = value;
                        }

                         
                        public String getLatestCFOpenedDate() {
                            return latestCFOpenedDate;
                        }

                         
                        public void setLatestCFOpenedDate(String value) {
                            this.latestCFOpenedDate = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentCF getDelinquentCF() {
                            return delinquentCF;
                        }

                        
                        public void setDelinquentCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentCF value) {
                            this.delinquentCF = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentOutstanding getDelinquentOutstanding() {
                            return delinquentOutstanding;
                        }

                        
                        public void setDelinquentOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.NBFCOthers.DelinquentOutstanding value) {
                            this.delinquentOutstanding = value;
                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                            
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                            
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                            
                            public String getGuarantor() {
                                return guarantor;
                            }

                            
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                            
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                            
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                            
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                            
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                            
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                            
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }

                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "totalLenders",
                        "totalCF",
                        "openCF",
                        "totalOutstanding",
                        "latestCFOpenedDate",
                        "delinquentCF",
                        "delinquentOutstanding"
                    })
                    public static class OtherPrivateForeignBanks {

                        @XmlElement(required = true)
                        protected String message;
                        @XmlElement(required = true)
                        protected String totalLenders;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalCF totalCF;
                        @XmlElement(required = true)
                        protected String openCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalOutstanding totalOutstanding;
                        @XmlElement(required = true)
                        protected String latestCFOpenedDate;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentCF delinquentCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentOutstanding delinquentOutstanding;

                        
                        public String getMessage() {
                            return message;
                        }

                         
                        public void setMessage(String value) {
                            this.message = value;
                        }

                        
                        public String getTotalLenders() {
                            return totalLenders;
                        }

                         
                        public void setTotalLenders(String value) {
                            this.totalLenders = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalCF getTotalCF() {
                            return totalCF;
                        }

                        
                        public void setTotalCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalCF value) {
                            this.totalCF = value;
                        }

                         
                        public String getOpenCF() {
                            return openCF;
                        }

                        
                        public void setOpenCF(String value) {
                            this.openCF = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalOutstanding getTotalOutstanding() {
                            return totalOutstanding;
                        }

                        
                        public void setTotalOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.TotalOutstanding value) {
                            this.totalOutstanding = value;
                        }

                        
                        public String getLatestCFOpenedDate() {
                            return latestCFOpenedDate;
                        }

                        
                        public void setLatestCFOpenedDate(String value) {
                            this.latestCFOpenedDate = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentCF getDelinquentCF() {
                            return delinquentCF;
                        }

                         
                        public void setDelinquentCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentCF value) {
                            this.delinquentCF = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentOutstanding getDelinquentOutstanding() {
                            return delinquentOutstanding;
                        }

                        
                        public void setDelinquentOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPrivateForeignBanks.DelinquentOutstanding value) {
                            this.delinquentOutstanding = value;
                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                             
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                            
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                            
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                            
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                            
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                            
                            public String getGuarantor() {
                                return guarantor;
                            }

                            
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                            
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                            
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }

                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "totalLenders",
                        "totalCF",
                        "openCF",
                        "totalOutstanding",
                        "latestCFOpenedDate",
                        "delinquentCF",
                        "delinquentOutstanding"
                    })
                    public static class OtherPublicSectorBanks {

                        @XmlElement(required = true)
                        protected String message;
                        @XmlElement(required = true)
                        protected String totalLenders;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalCF totalCF;
                        @XmlElement(required = true)
                        protected String openCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalOutstanding totalOutstanding;
                        @XmlElement(required = true)
                        protected String latestCFOpenedDate;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentCF delinquentCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentOutstanding delinquentOutstanding;

                         
                        public String getMessage() {
                            return message;
                        }

                        
                        public void setMessage(String value) {
                            this.message = value;
                        }

                        
                        public String getTotalLenders() {
                            return totalLenders;
                        }

                         
                        public void setTotalLenders(String value) {
                            this.totalLenders = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalCF getTotalCF() {
                            return totalCF;
                        }

                         
                        public void setTotalCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalCF value) {
                            this.totalCF = value;
                        }

                         
                        public String getOpenCF() {
                            return openCF;
                        }

                         
                        public void setOpenCF(String value) {
                            this.openCF = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalOutstanding getTotalOutstanding() {
                            return totalOutstanding;
                        }

                         
                        public void setTotalOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.TotalOutstanding value) {
                            this.totalOutstanding = value;
                        }

                        
                        public String getLatestCFOpenedDate() {
                            return latestCFOpenedDate;
                        }

                        
                        public void setLatestCFOpenedDate(String value) {
                            this.latestCFOpenedDate = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentCF getDelinquentCF() {
                            return delinquentCF;
                        }

                         
                        public void setDelinquentCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentCF value) {
                            this.delinquentCF = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentOutstanding getDelinquentOutstanding() {
                            return delinquentOutstanding;
                        }

                        
                        public void setDelinquentOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OtherPublicSectorBanks.DelinquentOutstanding value) {
                            this.delinquentOutstanding = value;
                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                            
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                             
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                            
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                            
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                             
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                            
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                            
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                            
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }

                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "message",
                        "totalLenders",
                        "totalCF",
                        "openCF",
                        "totalOutstanding",
                        "latestCFOpenedDate",
                        "delinquentCF",
                        "delinquentOutstanding"
                    })
                    public static class OutsideTotal {

                        @XmlElement(required = true)
                        protected String message;
                        @XmlElement(required = true)
                        protected String totalLenders;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalCF totalCF;
                        @XmlElement(required = true)
                        protected String openCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalOutstanding totalOutstanding;
                        @XmlElement(required = true)
                        protected String latestCFOpenedDate;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentCF delinquentCF;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentOutstanding delinquentOutstanding;

                         
                        public String getMessage() {
                            return message;
                        }

                        
                        public void setMessage(String value) {
                            this.message = value;
                        }

                        
                        public String getTotalLenders() {
                            return totalLenders;
                        }

                         
                        public void setTotalLenders(String value) {
                            this.totalLenders = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalCF getTotalCF() {
                            return totalCF;
                        }

                        
                        public void setTotalCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalCF value) {
                            this.totalCF = value;
                        }

                         
                        public String getOpenCF() {
                            return openCF;
                        }

                         
                        public void setOpenCF(String value) {
                            this.openCF = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalOutstanding getTotalOutstanding() {
                            return totalOutstanding;
                        }

                         
                        public void setTotalOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.TotalOutstanding value) {
                            this.totalOutstanding = value;
                        }

                         
                        public String getLatestCFOpenedDate() {
                            return latestCFOpenedDate;
                        }

                        
                        public void setLatestCFOpenedDate(String value) {
                            this.latestCFOpenedDate = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentCF getDelinquentCF() {
                            return delinquentCF;
                        }

                         
                        public void setDelinquentCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentCF value) {
                            this.delinquentCF = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentOutstanding getDelinquentOutstanding() {
                            return delinquentOutstanding;
                        }

                        
                        public void setDelinquentOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.OutsideInstitution.OutsideTotal.DelinquentOutstanding value) {
                            this.delinquentOutstanding = value;
                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                             
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                            
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                            
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class DelinquentOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                            
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                            
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                            
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalCF {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                             
                            public String getBorrower() {
                                return borrower;
                            }

                            
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                            
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                             
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                            
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }


                         
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "borrower",
                            "borrowerPercentage",
                            "guarantor",
                            "guarantorPercentage"
                        })
                        public static class TotalOutstanding {

                            @XmlElement(required = true)
                            protected String borrower;
                            @XmlElement(required = true)
                            protected String borrowerPercentage;
                            @XmlElement(required = true)
                            protected String guarantor;
                            @XmlElement(required = true)
                            protected String guarantorPercentage;

                             
                            public String getBorrower() {
                                return borrower;
                            }

                             
                            public void setBorrower(String value) {
                                this.borrower = value;
                            }

                             
                            public String getBorrowerPercentage() {
                                return borrowerPercentage;
                            }

                             
                            public void setBorrowerPercentage(String value) {
                                this.borrowerPercentage = value;
                            }

                             
                            public String getGuarantor() {
                                return guarantor;
                            }

                             
                            public void setGuarantor(String value) {
                                this.guarantor = value;
                            }

                            
                            public String getGuarantorPercentage() {
                                return guarantorPercentage;
                            }

                             
                            public void setGuarantorPercentage(String value) {
                                this.guarantorPercentage = value;
                            }

                        }

                    }

                }


                 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "totalLenders",
                    "totalCF",
                    "openCF",
                    "totalOutstanding",
                    "latestCFOpenedDate",
                    "delinquentCF",
                    "delinquentOutstanding"
                })
                public static class Total {

                    @XmlElement(required = true)
                    protected String totalLenders;
                    @XmlElement(required = true)
                    protected String totalCF;
                    @XmlElement(required = true)
                    protected String openCF;
                    @XmlElement(required = true)
                    protected String totalOutstanding;
                    @XmlElement(required = true)
                    protected String latestCFOpenedDate;
                    @XmlElement(required = true)
                    protected String delinquentCF;
                    @XmlElement(required = true)
                    protected String delinquentOutstanding;

                     
                    public String getTotalLenders() {
                        return totalLenders;
                    }

                     
                    public void setTotalLenders(String value) {
                        this.totalLenders = value;
                    }

                    
                    public String getTotalCF() {
                        return totalCF;
                    }

                     
                    public void setTotalCF(String value) {
                        this.totalCF = value;
                    }

                    
                    public String getOpenCF() {
                        return openCF;
                    }

                     
                    public void setOpenCF(String value) {
                        this.openCF = value;
                    }

                     
                    public String getTotalOutstanding() {
                        return totalOutstanding;
                    }

                     
                    public void setTotalOutstanding(String value) {
                        this.totalOutstanding = value;
                    }

                     
                    public String getLatestCFOpenedDate() {
                        return latestCFOpenedDate;
                    }

                    
                    public void setLatestCFOpenedDate(String value) {
                        this.latestCFOpenedDate = value;
                    }

                    
                    public String getDelinquentCF() {
                        return delinquentCF;
                    }

                     
                    public void setDelinquentCF(String value) {
                        this.delinquentCF = value;
                    }

                     
                    public String getDelinquentOutstanding() {
                        return delinquentOutstanding;
                    }

                     
                    public void setDelinquentOutstanding(String value) {
                        this.delinquentOutstanding = value;
                    }

                }


                 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "message",
                    "totalLenders",
                    "totalCF",
                    "openCF",
                    "totalOutstanding",
                    "latestCFOpenedDate",
                    "delinquentCF",
                    "delinquentOutstanding"
                })
                public static class YourInstitution {

                    @XmlElement(required = true)
                    protected String message;
                    @XmlElement(required = true)
                    protected String totalLenders;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalCF totalCF;
                    @XmlElement(required = true)
                    protected String openCF;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalOutstanding totalOutstanding;
                    @XmlElement(required = true)
                    protected String latestCFOpenedDate;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentCF delinquentCF;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentOutstanding delinquentOutstanding;

                     
                    public String getMessage() {
                        return message;
                    }

                     
                    public void setMessage(String value) {
                        this.message = value;
                    }

                    
                    public String getTotalLenders() {
                        return totalLenders;
                    }

                     
                    public void setTotalLenders(String value) {
                        this.totalLenders = value;
                    }

                     
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalCF getTotalCF() {
                        return totalCF;
                    }

                     
                    public void setTotalCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalCF value) {
                        this.totalCF = value;
                    }

                     
                    public String getOpenCF() {
                        return openCF;
                    }

                    
                    public void setOpenCF(String value) {
                        this.openCF = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalOutstanding getTotalOutstanding() {
                        return totalOutstanding;
                    }

                    
                    public void setTotalOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.TotalOutstanding value) {
                        this.totalOutstanding = value;
                    }

                     
                    public String getLatestCFOpenedDate() {
                        return latestCFOpenedDate;
                    }

                     
                    public void setLatestCFOpenedDate(String value) {
                        this.latestCFOpenedDate = value;
                    }

                     
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentCF getDelinquentCF() {
                        return delinquentCF;
                    }

                    
                    public void setDelinquentCF(Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentCF value) {
                        this.delinquentCF = value;
                    }

                     
                    public Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentOutstanding getDelinquentOutstanding() {
                        return delinquentOutstanding;
                    }

                     
                    public void setDelinquentOutstanding(Base.ResponseReport.ProductSec.CreditProfileSummarySec.YourInstitution.DelinquentOutstanding value) {
                        this.delinquentOutstanding = value;
                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "borrower",
                        "borrowerPercentage",
                        "guarantor",
                        "guarantorPercentage"
                    })
                    public static class DelinquentCF {

                        @XmlElement(required = true)
                        protected String borrower;
                        @XmlElement(required = true)
                        protected String borrowerPercentage;
                        @XmlElement(required = true)
                        protected String guarantor;
                        @XmlElement(required = true)
                        protected String guarantorPercentage;

                         
                        public String getBorrower() {
                            return borrower;
                        }

                         
                        public void setBorrower(String value) {
                            this.borrower = value;
                        }

                        
                        public String getBorrowerPercentage() {
                            return borrowerPercentage;
                        }

                         
                        public void setBorrowerPercentage(String value) {
                            this.borrowerPercentage = value;
                        }

                        
                        public String getGuarantor() {
                            return guarantor;
                        }

                        
                        public void setGuarantor(String value) {
                            this.guarantor = value;
                        }

                         
                        public String getGuarantorPercentage() {
                            return guarantorPercentage;
                        }

                         
                        public void setGuarantorPercentage(String value) {
                            this.guarantorPercentage = value;
                        }

                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "borrower",
                        "borrowerPercentage",
                        "guarantor",
                        "guarantorPercentage"
                    })
                    public static class DelinquentOutstanding {

                        @XmlElement(required = true)
                        protected String borrower;
                        @XmlElement(required = true)
                        protected String borrowerPercentage;
                        @XmlElement(required = true)
                        protected String guarantor;
                        @XmlElement(required = true)
                        protected String guarantorPercentage;

                        
                        public String getBorrower() {
                            return borrower;
                        }

                         
                        public void setBorrower(String value) {
                            this.borrower = value;
                        }

                        
                        public String getBorrowerPercentage() {
                            return borrowerPercentage;
                        }

                        
                        public void setBorrowerPercentage(String value) {
                            this.borrowerPercentage = value;
                        }

                        
                        public String getGuarantor() {
                            return guarantor;
                        }

                        
                        public void setGuarantor(String value) {
                            this.guarantor = value;
                        }

                         
                        public String getGuarantorPercentage() {
                            return guarantorPercentage;
                        }

                        
                        public void setGuarantorPercentage(String value) {
                            this.guarantorPercentage = value;
                        }

                    }


                     
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "borrower",
                        "borrowerPercentage",
                        "guarantor",
                        "guarantorPercentage"
                    })
                    public static class TotalCF {

                        @XmlElement(required = true)
                        protected String borrower;
                        @XmlElement(required = true)
                        protected String borrowerPercentage;
                        @XmlElement(required = true)
                        protected String guarantor;
                        @XmlElement(required = true)
                        protected String guarantorPercentage;

                         
                        public String getBorrower() {
                            return borrower;
                        }

                        
                        public void setBorrower(String value) {
                            this.borrower = value;
                        }

                        
                        public String getBorrowerPercentage() {
                            return borrowerPercentage;
                        }

                         
                        public void setBorrowerPercentage(String value) {
                            this.borrowerPercentage = value;
                        }

                         
                        public String getGuarantor() {
                            return guarantor;
                        }

                        
                        public void setGuarantor(String value) {
                            this.guarantor = value;
                        }

                        
                        public String getGuarantorPercentage() {
                            return guarantorPercentage;
                        }

                         
                        public void setGuarantorPercentage(String value) {
                            this.guarantorPercentage = value;
                        }

                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "borrower",
                        "borrowerPercentage",
                        "guarantor",
                        "guarantorPercentage"
                    })
                    public static class TotalOutstanding {

                        @XmlElement(required = true)
                        protected String borrower;
                        @XmlElement(required = true)
                        protected String borrowerPercentage;
                        @XmlElement(required = true)
                        protected String guarantor;
                        @XmlElement(required = true)
                        protected String guarantorPercentage;

                         
                        public String getBorrower() {
                            return borrower;
                        }

                         
                        public void setBorrower(String value) {
                            this.borrower = value;
                        }

                         
                        public String getBorrowerPercentage() {
                            return borrowerPercentage;
                        }

                         
                        public void setBorrowerPercentage(String value) {
                            this.borrowerPercentage = value;
                        }

                        
                        public String getGuarantor() {
                            return guarantor;
                        }

                         
                        public void setGuarantor(String value) {
                            this.guarantor = value;
                        }

                         
                        public String getGuarantorPercentage() {
                            return guarantorPercentage;
                        }

                         
                        public void setGuarantorPercentage(String value) {
                            this.guarantorPercentage = value;
                        }

                    }

                }

            }


             
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "creditRatingSummary"
            })
            public static class CreditRatingSummaryVec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary> creditRatingSummary;

                 
                public String getMessage() {
                    return message;
                }

                 
                public void setMessage(String value) {
                    this.message = value;
                }

                
                public List<Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary> getCreditRatingSummary() {
                    if (creditRatingSummary == null) {
                        creditRatingSummary = new ArrayList<Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary>();
                    }
                    return this.creditRatingSummary;
                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "creditRatingAgency",
                    "creditRatingSummaryDetailsVec"
                })
                public static class CreditRatingSummary {

                    @XmlElement(required = true)
                    protected String creditRatingAgency;
                    protected List<Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary.CreditRatingSummaryDetailsVec> creditRatingSummaryDetailsVec;

                     
                    public String getCreditRatingAgency() {
                        return creditRatingAgency;
                    }

                     
                    public void setCreditRatingAgency(String value) {
                        this.creditRatingAgency = value;
                    }

                    
                    public List<Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary.CreditRatingSummaryDetailsVec> getCreditRatingSummaryDetailsVec() {
                        if (creditRatingSummaryDetailsVec == null) {
                            creditRatingSummaryDetailsVec = new ArrayList<Base.ResponseReport.ProductSec.CreditRatingSummaryVec.CreditRatingSummary.CreditRatingSummaryDetailsVec>();
                        }
                        return this.creditRatingSummaryDetailsVec;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "creditRating",
                        "ratingAsOn",
                        "ratingExpiryDt",
                        "lastReportedDt"
                    })
                    public static class CreditRatingSummaryDetailsVec {

                        @XmlElement(required = true)
                        protected String creditRating;
                        @XmlElement(required = true)
                        protected String ratingAsOn;
                        @XmlElement(required = true)
                        protected String ratingExpiryDt;
                        @XmlElement(required = true)
                        protected String lastReportedDt;

                         
                        public String getCreditRating() {
                            return creditRating;
                        }

                         
                        public void setCreditRating(String value) {
                            this.creditRating = value;
                        }

                        
                        public String getRatingAsOn() {
                            return ratingAsOn;
                        }

                        
                        public void setRatingAsOn(String value) {
                            this.ratingAsOn = value;
                        }

                         
                        public String getRatingExpiryDt() {
                            return ratingExpiryDt;
                        }

                        
                        public void setRatingExpiryDt(String value) {
                            this.ratingExpiryDt = value;
                        }

                        
                        public String getLastReportedDt() {
                            return lastReportedDt;
                        }

                        
                        public void setLastReportedDt(String value) {
                            this.lastReportedDt = value;
                        }

                    }

                }

            }
 
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "messageOfBorrower",
                "messageOfBorrowerYourInstitution",
                "messageOfBorrowerOutsideInstitution",
                "messageOfRelatedParties",
                "messageOfRelatedPartiesYourInstitution",
                "messageOfRelatedPartiesOutsideInstitution",
                "messageOfGuarantedParties",
                "derogatoryInformationBorrower",
                "derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec",
                "derogatoryInformationReportedOnGuarantedPartiesVec"
            })
            public static class DerogatoryInformationSec {

                @XmlElement(required = true)
                protected String message;
                @XmlElement(required = true)
                protected String messageOfBorrower;
                @XmlElement(required = true)
                protected String messageOfBorrowerYourInstitution;
                @XmlElement(required = true)
                protected String messageOfBorrowerOutsideInstitution;
                @XmlElement(required = true)
                protected String messageOfRelatedParties;
                @XmlElement(required = true)
                protected String messageOfRelatedPartiesYourInstitution;
                @XmlElement(required = true)
                protected String messageOfRelatedPartiesOutsideInstitution;
                @XmlElement(required = true)
                protected String messageOfGuarantedParties;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower derogatoryInformationBorrower;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationReportedOnGuarantedPartiesVec derogatoryInformationReportedOnGuarantedPartiesVec;

                
                public String getMessage() {
                    return message;
                }

                
                public void setMessage(String value) {
                    this.message = value;
                }

                
                public String getMessageOfBorrower() {
                    return messageOfBorrower;
                }

                
                public void setMessageOfBorrower(String value) {
                    this.messageOfBorrower = value;
                }

                 
                public String getMessageOfBorrowerYourInstitution() {
                    return messageOfBorrowerYourInstitution;
                }

                
                public void setMessageOfBorrowerYourInstitution(String value) {
                    this.messageOfBorrowerYourInstitution = value;
                }

                 
                public String getMessageOfBorrowerOutsideInstitution() {
                    return messageOfBorrowerOutsideInstitution;
                }

                 
                public void setMessageOfBorrowerOutsideInstitution(String value) {
                    this.messageOfBorrowerOutsideInstitution = value;
                }

                 
                public String getMessageOfRelatedParties() {
                    return messageOfRelatedParties;
                }

                
                public void setMessageOfRelatedParties(String value) {
                    this.messageOfRelatedParties = value;
                }

                 
                public String getMessageOfRelatedPartiesYourInstitution() {
                    return messageOfRelatedPartiesYourInstitution;
                }

                 
                public void setMessageOfRelatedPartiesYourInstitution(String value) {
                    this.messageOfRelatedPartiesYourInstitution = value;
                }

                 
                public String getMessageOfRelatedPartiesOutsideInstitution() {
                    return messageOfRelatedPartiesOutsideInstitution;
                }

                 
                public void setMessageOfRelatedPartiesOutsideInstitution(String value) {
                    this.messageOfRelatedPartiesOutsideInstitution = value;
                }

                 
                public String getMessageOfGuarantedParties() {
                    return messageOfGuarantedParties;
                }

                 
                public void setMessageOfGuarantedParties(String value) {
                    this.messageOfGuarantedParties = value;
                }

                 
                public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower getDerogatoryInformationBorrower() {
                    return derogatoryInformationBorrower;
                }

                 
                public void setDerogatoryInformationBorrower(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower value) {
                    this.derogatoryInformationBorrower = value;
                }

                 
                public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec getDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec() {
                    return derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec;
                }

                 
                public void setDerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec value) {
                    this.derogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec = value;
                }

                 
                public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationReportedOnGuarantedPartiesVec getDerogatoryInformationReportedOnGuarantedPartiesVec() {
                    return derogatoryInformationReportedOnGuarantedPartiesVec;
                }

                 
                public void setDerogatoryInformationReportedOnGuarantedPartiesVec(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationReportedOnGuarantedPartiesVec value) {
                    this.derogatoryInformationReportedOnGuarantedPartiesVec = value;
                }


                 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "yourInstitution",
                    "outsideInstitution",
                    "total"
                })
                public static class DerogatoryInformationBorrower {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution yourInstitution;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution outsideInstitution;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total total;

                     
                    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution getYourInstitution() {
                        return yourInstitution;
                    }

                     
                    public void setYourInstitution(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution value) {
                        this.yourInstitution = value;
                    }

                     
                    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution getOutsideInstitution() {
                        return outsideInstitution;
                    }

                     
                    public void setOutsideInstitution(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution value) {
                        this.outsideInstitution = value;
                    }

                     
                    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total getTotal() {
                        return total;
                    }

                     
                    public void setTotal(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total value) {
                        this.total = value;
                    }


                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "wilfulDefault",
                        "suitFilled",
                        "writtenOff",
                        "settled",
                        "invoked",
                        "overdueCF",
                        "dishonoredCheque"
                    })
                    public static class OutsideInstitution {

                        @XmlElement(required = true)
                        protected String wilfulDefault;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.SuitFilled suitFilled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.WrittenOff writtenOff;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Settled settled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Invoked invoked;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.OverdueCF overdueCF;
                        @XmlElement(required = true)
                        protected String dishonoredCheque;

                         
                        public String getWilfulDefault() {
                            return wilfulDefault;
                        }

                        
                        public void setWilfulDefault(String value) {
                            this.wilfulDefault = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.SuitFilled getSuitFilled() {
                            return suitFilled;
                        }

                        
                        public void setSuitFilled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.SuitFilled value) {
                            this.suitFilled = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.WrittenOff getWrittenOff() {
                            return writtenOff;
                        }

                         
                        public void setWrittenOff(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.WrittenOff value) {
                            this.writtenOff = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Settled getSettled() {
                            return settled;
                        }

                         
                        public void setSettled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Settled value) {
                            this.settled = value;
                        }

                         
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Invoked getInvoked() {
                            return invoked;
                        }

                         
                        public void setInvoked(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.Invoked value) {
                            this.invoked = value;
                        }
 
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.OverdueCF getOverdueCF() {
                            return overdueCF;
                        }

                     
                        public void setOverdueCF(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.OutsideInstitution.OverdueCF value) {
                            this.overdueCF = value;
                        }

                      
                        public String getDishonoredCheque() {
                            return dishonoredCheque;
                        }

                        
                        public void setDishonoredCheque(String value) {
                            this.dishonoredCheque = value;
                        }


                    
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Invoked {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                          
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                           
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                           
                            public String getAmt() {
                                return amt;
                            }

                           
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


               
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class OverdueCF {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                           
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                         
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                       
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Settled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;
 
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                          
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                        
                            public String getAmt() {
                                return amt;
                            }

                         
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class SuitFilled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                             
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                         
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                           
                            public String getAmt() {
                                return amt;
                            }

                          
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class WrittenOff {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                          
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                            
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                           
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "wilfulDefault",
                        "suitFilled",
                        "writtenOff",
                        "settled",
                        "invoked",
                        "overdueCF",
                        "dishonoredCheque"
                    })
                    public static class Total {

                        @XmlElement(required = true)
                        protected String wilfulDefault;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.SuitFilled suitFilled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.WrittenOff writtenOff;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Settled settled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Invoked invoked;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.OverdueCF overdueCF;
                        @XmlElement(required = true)
                        protected String dishonoredCheque;

                        
                        public String getWilfulDefault() {
                            return wilfulDefault;
                        }

                       
                        public void setWilfulDefault(String value) {
                            this.wilfulDefault = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.SuitFilled getSuitFilled() {
                            return suitFilled;
                        }

                        
                        public void setSuitFilled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.SuitFilled value) {
                            this.suitFilled = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.WrittenOff getWrittenOff() {
                            return writtenOff;
                        }
 
                        public void setWrittenOff(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.WrittenOff value) {
                            this.writtenOff = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Settled getSettled() {
                            return settled;
                        }

                       
                        public void setSettled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Settled value) {
                            this.settled = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Invoked getInvoked() {
                            return invoked;
                        }
 
                        public void setInvoked(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.Invoked value) {
                            this.invoked = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.OverdueCF getOverdueCF() {
                            return overdueCF;
                        }

                      
                        public void setOverdueCF(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.Total.OverdueCF value) {
                            this.overdueCF = value;
                        }

                        
                        public String getDishonoredCheque() {
                            return dishonoredCheque;
                        }

                        
                        public void setDishonoredCheque(String value) {
                            this.dishonoredCheque = value;
                        }


                     
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Invoked {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                         
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                            
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                             
                            public String getAmt() {
                                return amt;
                            }

                            
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class OverdueCF {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                            
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                            
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                           
                            public String getAmt() {
                                return amt;
                            }

                         
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Settled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                          
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                           
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }
 
                            public String getAmt() {
                                return amt;
                            }

                            
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class SuitFilled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;
 
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                         
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }
 
                            public String getAmt() {
                                return amt;
                            }

                             
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class WrittenOff {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                          
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                          
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                           
                            public String getAmt() {
                                return amt;
                            }

                         
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "wilfulDefault",
                        "suitFilled",
                        "writtenOff",
                        "settled",
                        "invoked",
                        "overdueCF",
                        "dishonoredCheque"
                    })
                    public static class YourInstitution {

                        @XmlElement(required = true)
                        protected String wilfulDefault;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.SuitFilled suitFilled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.WrittenOff writtenOff;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Settled settled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Invoked invoked;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.OverdueCF overdueCF;
                        @XmlElement(required = true)
                        protected String dishonoredCheque;

                        
                        public String getWilfulDefault() {
                            return wilfulDefault;
                        }

                        
                        public void setWilfulDefault(String value) {
                            this.wilfulDefault = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.SuitFilled getSuitFilled() {
                            return suitFilled;
                        }

                         
                        public void setSuitFilled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.SuitFilled value) {
                            this.suitFilled = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.WrittenOff getWrittenOff() {
                            return writtenOff;
                        }

                       
                        public void setWrittenOff(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.WrittenOff value) {
                            this.writtenOff = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Settled getSettled() {
                            return settled;
                        }

                       
                        public void setSettled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Settled value) {
                            this.settled = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Invoked getInvoked() {
                            return invoked;
                        }

                        
                        public void setInvoked(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.Invoked value) {
                            this.invoked = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.OverdueCF getOverdueCF() {
                            return overdueCF;
                        }

                        
                        public void setOverdueCF(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationBorrower.YourInstitution.OverdueCF value) {
                            this.overdueCF = value;
                        }

                        
                        public String getDishonoredCheque() {
                            return dishonoredCheque;
                        }
 
                        public void setDishonoredCheque(String value) {
                            this.dishonoredCheque = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Invoked {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                            
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                           
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                            
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class OverdueCF {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                     
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }
 
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                         
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Settled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                            
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                         
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }
 
                            public String getAmt() {
                                return amt;
                            }

                       
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class SuitFilled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                         
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                           
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                          
                            public String getAmt() {
                                return amt;
                            }

                         
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


            
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class WrittenOff {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

           
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                        
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                        
                            public String getAmt() {
                                return amt;
                            }

                           
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                    }

                }


  
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "yourInstitution",
                    "outsideInstitution",
                    "total"
                })
                public static class DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution yourInstitution;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution outsideInstitution;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total total;

                   
                    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution getYourInstitution() {
                        return yourInstitution;
                    }

                 
                    public void setYourInstitution(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution value) {
                        this.yourInstitution = value;
                    }

                 
                    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution getOutsideInstitution() {
                        return outsideInstitution;
                    }

                   
                    public void setOutsideInstitution(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution value) {
                        this.outsideInstitution = value;
                    }

                  
                    public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total getTotal() {
                        return total;
                    }

                  
                    public void setTotal(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total value) {
                        this.total = value;
                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "wilfulDefault",
                        "suitFilled",
                        "writtenOff",
                        "settled",
                        "invoked",
                        "overdueCF",
                        "dishonoredCheque"
                    })
                    public static class OutsideInstitution {

                        @XmlElement(required = true)
                        protected String wilfulDefault;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.SuitFilled suitFilled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.WrittenOff writtenOff;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Settled settled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Invoked invoked;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.OverdueCF overdueCF;
                        @XmlElement(required = true)
                        protected String dishonoredCheque;

        
                        public String getWilfulDefault() {
                            return wilfulDefault;
                        }

                      
                        public void setWilfulDefault(String value) {
                            this.wilfulDefault = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.SuitFilled getSuitFilled() {
                            return suitFilled;
                        }

                       
                        public void setSuitFilled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.SuitFilled value) {
                            this.suitFilled = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.WrittenOff getWrittenOff() {
                            return writtenOff;
                        }

                      
                        public void setWrittenOff(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.WrittenOff value) {
                            this.writtenOff = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Settled getSettled() {
                            return settled;
                        }

                       
                        public void setSettled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Settled value) {
                            this.settled = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Invoked getInvoked() {
                            return invoked;
                        }

                      
                        public void setInvoked(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.Invoked value) {
                            this.invoked = value;
                        }
 
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.OverdueCF getOverdueCF() {
                            return overdueCF;
                        }

                     
                        public void setOverdueCF(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.OutsideInstitution.OverdueCF value) {
                            this.overdueCF = value;
                        }

                      
                        public String getDishonoredCheque() {
                            return dishonoredCheque;
                        }

                       
                        public void setDishonoredCheque(String value) {
                            this.dishonoredCheque = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Invoked {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                         
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                          
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                           
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class OverdueCF {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;
 
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                           
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                          
                            public String getAmt() {
                                return amt;
                            }

                             
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


  
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Settled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                           
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                            
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                           
                            public String getAmt() {
                                return amt;
                            }

                           
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class SuitFilled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                             
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                            
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                          
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class WrittenOff {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                           
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }
 
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                             
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "wilfulDefault",
                        "suitFilled",
                        "writtenOff",
                        "settled",
                        "invoked",
                        "overdueCF",
                        "dishonoredCheque"
                    })
                    public static class Total {

                        @XmlElement(required = true)
                        protected String wilfulDefault;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.SuitFilled suitFilled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.WrittenOff writtenOff;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Settled settled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Invoked invoked;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.OverdueCF overdueCF;
                        @XmlElement(required = true)
                        protected String dishonoredCheque;

                        
                        public String getWilfulDefault() {
                            return wilfulDefault;
                        }

                       
                        public void setWilfulDefault(String value) {
                            this.wilfulDefault = value;
                        }
 
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.SuitFilled getSuitFilled() {
                            return suitFilled;
                        }

                       
                        public void setSuitFilled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.SuitFilled value) {
                            this.suitFilled = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.WrittenOff getWrittenOff() {
                            return writtenOff;
                        }
 
                        public void setWrittenOff(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.WrittenOff value) {
                            this.writtenOff = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Settled getSettled() {
                            return settled;
                        }

                         
                        public void setSettled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Settled value) {
                            this.settled = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Invoked getInvoked() {
                            return invoked;
                        }

                       
                        public void setInvoked(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.Invoked value) {
                            this.invoked = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.OverdueCF getOverdueCF() {
                            return overdueCF;
                        }

                       
                        public void setOverdueCF(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.Total.OverdueCF value) {
                            this.overdueCF = value;
                        }

                        
                        public String getDishonoredCheque() {
                            return dishonoredCheque;
                        }

                       
                        public void setDishonoredCheque(String value) {
                            this.dishonoredCheque = value;
                        }


                        
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Invoked {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;
 
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                            
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                            
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                   
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class OverdueCF {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;
 
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                          
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                          
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                  
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Settled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;
 
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                          
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }
 
                            public String getAmt() {
                                return amt;
                            }

                        
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class SuitFilled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                            
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                           
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }
 
                            public String getAmt() {
                                return amt;
                            }
 
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                  
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class WrittenOff {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                          
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                          
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                          
                            public String getAmt() {
                                return amt;
                            }

                           
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "wilfulDefault",
                        "suitFilled",
                        "writtenOff",
                        "settled",
                        "invoked",
                        "overdueCF",
                        "dishonoredCheque"
                    })
                    public static class YourInstitution {

                        @XmlElement(required = true)
                        protected String wilfulDefault;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.SuitFilled suitFilled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.WrittenOff writtenOff;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Settled settled;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Invoked invoked;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.OverdueCF overdueCF;
                        @XmlElement(required = true)
                        protected String dishonoredCheque;

                       
                        public String getWilfulDefault() {
                            return wilfulDefault;
                        }

                   
                        public void setWilfulDefault(String value) {
                            this.wilfulDefault = value;
                        }

                        
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.SuitFilled getSuitFilled() {
                            return suitFilled;
                        }

                       
                        public void setSuitFilled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.SuitFilled value) {
                            this.suitFilled = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.WrittenOff getWrittenOff() {
                            return writtenOff;
                        }

                       
                        public void setWrittenOff(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.WrittenOff value) {
                            this.writtenOff = value;
                        }

                     
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Settled getSettled() {
                            return settled;
                        }

                       
                        public void setSettled(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Settled value) {
                            this.settled = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Invoked getInvoked() {
                            return invoked;
                        }
 
                        public void setInvoked(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.Invoked value) {
                            this.invoked = value;
                        }
 
                        public Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.OverdueCF getOverdueCF() {
                            return overdueCF;
                        }

                      
                        public void setOverdueCF(Base.ResponseReport.ProductSec.DerogatoryInformationSec.DerogatoryInformationOnRelatedPartiesOrGuarantorsOfBorrowerSec.YourInstitution.OverdueCF value) {
                            this.overdueCF = value;
                        }
 
                        public String getDishonoredCheque() {
                            return dishonoredCheque;
                        }

                        
                        public void setDishonoredCheque(String value) {
                            this.dishonoredCheque = value;
                        }


                 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Invoked {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                           
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                         
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                            
                            public String getAmt() {
                                return amt;
                            }

                       
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                  
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class OverdueCF {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                          
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                            
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }
 
                            public String getAmt() {
                                return amt;
                            }

                         
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                   
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class Settled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                            
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }
 
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                        
                            public String getAmt() {
                                return amt;
                            }

                          
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


              
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class SuitFilled {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                        
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                           
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }
 
                            public String getAmt() {
                                return amt;
                            }

                        
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }


                 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "numberOfSuitFiled",
                            "amt"
                        })
                        public static class WrittenOff {

                            @XmlElement(required = true)
                            protected String numberOfSuitFiled;
                            @XmlElement(required = true)
                            protected String amt;

                          
                            public String getNumberOfSuitFiled() {
                                return numberOfSuitFiled;
                            }

                      
                            public void setNumberOfSuitFiled(String value) {
                                this.numberOfSuitFiled = value;
                            }

                      
                            public String getAmt() {
                                return amt;
                            }
 
                            public void setAmt(String value) {
                                this.amt = value;
                            }

                        }

                    }

                }

 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "derogatoryInformationReportedOnGuarantedParties"
                })
                public static class DerogatoryInformationReportedOnGuarantedPartiesVec {

                    protected List<String> derogatoryInformationReportedOnGuarantedParties;

   
                    public List<String> getDerogatoryInformationReportedOnGuarantedParties() {
                        if (derogatoryInformationReportedOnGuarantedParties == null) {
                            derogatoryInformationReportedOnGuarantedParties = new ArrayList<String>();
                        }
                        return this.derogatoryInformationReportedOnGuarantedParties;
                    }

                }

            }


 
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "enquiryDetailsInLast24Month"
            })
            public static class EnquiryDetailsInLast24MonthVec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec.EnquiryDetailsInLast24Month> enquiryDetailsInLast24Month;

             
                public String getMessage() {
                    return message;
                }

               
                public void setMessage(String value) {
                    this.message = value;
                }

     
                public List<Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec.EnquiryDetailsInLast24Month> getEnquiryDetailsInLast24Month() {
                    if (enquiryDetailsInLast24Month == null) {
                        enquiryDetailsInLast24Month = new ArrayList<Base.ResponseReport.ProductSec.EnquiryDetailsInLast24MonthVec.EnquiryDetailsInLast24Month>();
                    }
                    return this.enquiryDetailsInLast24Month;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "creditLender",
                    "enquiryDt",
                    "enquiryPurpose",
                    "enquiryAmt"
                })
                public static class EnquiryDetailsInLast24Month {

                    @XmlElement(required = true)
                    protected String creditLender;
                    @XmlElement(required = true)
                    protected String enquiryDt;
                    @XmlElement(required = true)
                    protected String enquiryPurpose;
                    @XmlElement(required = true)
                    protected String enquiryAmt;
 
                    public String getCreditLender() {
                        return creditLender;
                    }

              
                    public void setCreditLender(String value) {
                        this.creditLender = value;
                    }

                   
                    public String getEnquiryDt() {
                        return enquiryDt;
                    }
 
                    public void setEnquiryDt(String value) {
                        this.enquiryDt = value;
                    }

                   
                    public String getEnquiryPurpose() {
                        return enquiryPurpose;
                    }

               
                    public void setEnquiryPurpose(String value) {
                        this.enquiryPurpose = value;
                    }

                   
                    public String getEnquiryAmt() {
                        return enquiryAmt;
                    }

                  
                    public void setEnquiryAmt(String value) {
                        this.enquiryAmt = value;
                    }

                }

            }

 
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "enquiryYourInstitution",
                "enquiryOutsideInstitution",
                "enquiryTotal"
            })
            public static class EnquirySummarySec {

                @XmlElement(required = true)
                protected String message;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution enquiryYourInstitution;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution enquiryOutsideInstitution;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal enquiryTotal;

            
                public String getMessage() {
                    return message;
                }

              
                public void setMessage(String value) {
                    this.message = value;
                }

               
                public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution getEnquiryYourInstitution() {
                    return enquiryYourInstitution;
                }

              
                public void setEnquiryYourInstitution(Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution value) {
                    this.enquiryYourInstitution = value;
                }

               
                public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution getEnquiryOutsideInstitution() {
                    return enquiryOutsideInstitution;
                }

             
                public void setEnquiryOutsideInstitution(Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution value) {
                    this.enquiryOutsideInstitution = value;
                }

             
                public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal getEnquiryTotal() {
                    return enquiryTotal;
                }

            
                public void setEnquiryTotal(Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal value) {
                    this.enquiryTotal = value;
                }


                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "noOfEnquiries"
                })
                public static class EnquiryOutsideInstitution {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution.NoOfEnquiries noOfEnquiries;
 
                    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution.NoOfEnquiries getNoOfEnquiries() {
                        return noOfEnquiries;
                    }

                  
                    public void setNoOfEnquiries(Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryOutsideInstitution.NoOfEnquiries value) {
                        this.noOfEnquiries = value;
                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "month1",
                        "month2To3",
                        "month4To6",
                        "month7To12",
                        "month12To24",
                        "greaterthan24Month",
                        "total",
                        "mostRecentDate"
                    })
                    public static class NoOfEnquiries {

                        @XmlElement(required = true)
                        protected String month1;
                        @XmlElement(name = "month2to3", required = true)
                        protected String month2To3;
                        @XmlElement(name = "month4to6", required = true)
                        protected String month4To6;
                        @XmlElement(name = "month7to12", required = true)
                        protected String month7To12;
                        @XmlElement(name = "month12to24", required = true)
                        protected String month12To24;
                        @XmlElement(required = true)
                        protected String greaterthan24Month;
                        @XmlElement(required = true)
                        protected String total;
                        @XmlElement(required = true)
                        protected String mostRecentDate;

                  
                        public String getMonth1() {
                            return month1;
                        }

                     
                        public void setMonth1(String value) {
                            this.month1 = value;
                        }

                      
                        public String getMonth2To3() {
                            return month2To3;
                        }

                      
                        public void setMonth2To3(String value) {
                            this.month2To3 = value;
                        }
 
                        public String getMonth4To6() {
                            return month4To6;
                        }

                      
                        public void setMonth4To6(String value) {
                            this.month4To6 = value;
                        }

                      
                        public String getMonth7To12() {
                            return month7To12;
                        }

                      
                        public void setMonth7To12(String value) {
                            this.month7To12 = value;
                        }

                      
                        public String getMonth12To24() {
                            return month12To24;
                        }

                      
                        public void setMonth12To24(String value) {
                            this.month12To24 = value;
                        }

                        
                        public String getGreaterthan24Month() {
                            return greaterthan24Month;
                        }

                      
                        public void setGreaterthan24Month(String value) {
                            this.greaterthan24Month = value;
                        }

                        
                        public String getTotal() {
                            return total;
                        }

                       
                        public void setTotal(String value) {
                            this.total = value;
                        }

                      
                        public String getMostRecentDate() {
                            return mostRecentDate;
                        }

                       
                        public void setMostRecentDate(String value) {
                            this.mostRecentDate = value;
                        }

                    }

                }

 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "noOfEnquiries"
                })
                public static class EnquiryTotal {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal.NoOfEnquiries noOfEnquiries;

              
                    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal.NoOfEnquiries getNoOfEnquiries() {
                        return noOfEnquiries;
                    }

           
                    public void setNoOfEnquiries(Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryTotal.NoOfEnquiries value) {
                        this.noOfEnquiries = value;
                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "month1",
                        "month2To3",
                        "month4To6",
                        "month7To12",
                        "month12To24",
                        "greaterthan24Month",
                        "total",
                        "mostRecentDate"
                    })
                    public static class NoOfEnquiries {

                        @XmlElement(required = true)
                        protected String month1;
                        @XmlElement(name = "month2to3", required = true)
                        protected String month2To3;
                        @XmlElement(name = "month4to6", required = true)
                        protected String month4To6;
                        @XmlElement(name = "month7to12", required = true)
                        protected String month7To12;
                        @XmlElement(name = "month12to24", required = true)
                        protected String month12To24;
                        @XmlElement(required = true)
                        protected String greaterthan24Month;
                        @XmlElement(required = true)
                        protected String total;
                        @XmlElement(required = true)
                        protected String mostRecentDate;

                      
                        public String getMonth1() {
                            return month1;
                        }

                      
                        public void setMonth1(String value) {
                            this.month1 = value;
                        }

                      
                        public String getMonth2To3() {
                            return month2To3;
                        }

                      
                        public void setMonth2To3(String value) {
                            this.month2To3 = value;
                        }
 
                        public String getMonth4To6() {
                            return month4To6;
                        }

                      
                        public void setMonth4To6(String value) {
                            this.month4To6 = value;
                        }

                      
                        public String getMonth7To12() {
                            return month7To12;
                        }

                        
                        public void setMonth7To12(String value) {
                            this.month7To12 = value;
                        }
 
                        public String getMonth12To24() {
                            return month12To24;
                        }
 
                        public void setMonth12To24(String value) {
                            this.month12To24 = value;
                        }

                       
                        public String getGreaterthan24Month() {
                            return greaterthan24Month;
                        }

                       
                        public void setGreaterthan24Month(String value) {
                            this.greaterthan24Month = value;
                        }

                        
                        public String getTotal() {
                            return total;
                        }

                      
                        public void setTotal(String value) {
                            this.total = value;
                        }

                       
                        public String getMostRecentDate() {
                            return mostRecentDate;
                        }

                       
                        public void setMostRecentDate(String value) {
                            this.mostRecentDate = value;
                        }

                    }

                }

 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "noOfEnquiries"
                })
                public static class EnquiryYourInstitution {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution.NoOfEnquiries noOfEnquiries;

                  
                    public Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution.NoOfEnquiries getNoOfEnquiries() {
                        return noOfEnquiries;
                    }

                  
                    public void setNoOfEnquiries(Base.ResponseReport.ProductSec.EnquirySummarySec.EnquiryYourInstitution.NoOfEnquiries value) {
                        this.noOfEnquiries = value;
                    }


               
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "month1",
                        "month2To3",
                        "month4To6",
                        "month7To12",
                        "month12To24",
                        "greaterthan24Month",
                        "total",
                        "mostRecentDate"
                    })
                    public static class NoOfEnquiries {

                        @XmlElement(required = true)
                        protected String month1;
                        @XmlElement(name = "month2to3", required = true)
                        protected String month2To3;
                        @XmlElement(name = "month4to6", required = true)
                        protected String month4To6;
                        @XmlElement(name = "month7to12", required = true)
                        protected String month7To12;
                        @XmlElement(name = "month12to24", required = true)
                        protected String month12To24;
                        @XmlElement(required = true)
                        protected String greaterthan24Month;
                        @XmlElement(required = true)
                        protected String total;
                        @XmlElement(required = true)
                        protected String mostRecentDate;
 
                        public String getMonth1() {
                            return month1;
                        }

                         
                        public void setMonth1(String value) {
                            this.month1 = value;
                        }
 
                        public String getMonth2To3() {
                            return month2To3;
                        }

                         
                        public void setMonth2To3(String value) {
                            this.month2To3 = value;
                        }
 
                        public String getMonth4To6() {
                            return month4To6;
                        }

                       
                        public void setMonth4To6(String value) {
                            this.month4To6 = value;
                        }

                       
                        public String getMonth7To12() {
                            return month7To12;
                        }

                     
                        public void setMonth7To12(String value) {
                            this.month7To12 = value;
                        }

                       
                        public String getMonth12To24() {
                            return month12To24;
                        }
 
                        public void setMonth12To24(String value) {
                            this.month12To24 = value;
                        }

                       
                        public String getGreaterthan24Month() {
                            return greaterthan24Month;
                        }

                       
                        public void setGreaterthan24Month(String value) {
                            this.greaterthan24Month = value;
                        }

                      
                        public String getTotal() {
                            return total;
                        }

                         
                        public void setTotal(String value) {
                            this.total = value;
                        }
 
                        public String getMostRecentDate() {
                            return mostRecentDate;
                        }

                     
                        public void setMostRecentDate(String value) {
                            this.mostRecentDate = value;
                        }

                    }

                }

            }

 
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "locationInformationVec",
                "contactNumber",
                "faxNumber"
            })
            public static class LocationDetailsSec {

                @XmlElement(required = true)
                protected String message;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec locationInformationVec;
                @XmlElement(required = true)
                protected String contactNumber;
                @XmlElement(required = true)
                protected String faxNumber;
 
                public String getMessage() {
                    return message;
                }

               
                public void setMessage(String value) {
                    this.message = value;
                }

             
                public Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec getLocationInformationVec() {
                    return locationInformationVec;
                }

               
                public void setLocationInformationVec(Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec value) {
                    this.locationInformationVec = value;
                }

             
                public String getContactNumber() {
                    return contactNumber;
                }

            
                public void setContactNumber(String value) {
                    this.contactNumber = value;
                }

               
                public String getFaxNumber() {
                    return faxNumber;
                }

                
                public void setFaxNumber(String value) {
                    this.faxNumber = value;
                }

 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "locationInformation"
                })
                public static class LocationInformationVec {

                    protected List<Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec.LocationInformation> locationInformation;

             
                    public List<Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec.LocationInformation> getLocationInformation() {
                        if (locationInformation == null) {
                            locationInformation = new ArrayList<Base.ResponseReport.ProductSec.LocationDetailsSec.LocationInformationVec.LocationInformation>();
                        }
                        return this.locationInformation;
                    }


          
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "borrowerOfficeLocationType",
                        "address",
                        "firstReportedDate",
                        "lastReportedDate",
                        "numberOfInstitutions"
                    })
                    public static class LocationInformation {

                        @XmlElement(required = true)
                        protected String borrowerOfficeLocationType;
                        @XmlElement(required = true)
                        protected String address;
                        @XmlElement(required = true)
                        protected String firstReportedDate;
                        @XmlElement(required = true)
                        protected String lastReportedDate;
                        @XmlElement(required = true)
                        protected String numberOfInstitutions;

                       
                        public String getBorrowerOfficeLocationType() {
                            return borrowerOfficeLocationType;
                        }

                        
                        public void setBorrowerOfficeLocationType(String value) {
                            this.borrowerOfficeLocationType = value;
                        }

                      
                        public String getAddress() {
                            return address;
                        }

                         
                        public void setAddress(String value) {
                            this.address = value;
                        }

                      
                        public String getFirstReportedDate() {
                            return firstReportedDate;
                        }

                      
                        public void setFirstReportedDate(String value) {
                            this.firstReportedDate = value;
                        }

                   
                        public String getLastReportedDate() {
                            return lastReportedDate;
                        }

                   
                        public void setLastReportedDate(String value) {
                            this.lastReportedDate = value;
                        }

                       
                        public String getNumberOfInstitutions() {
                            return numberOfInstitutions;
                        }

                     
                        public void setNumberOfInstitutions(String value) {
                            this.numberOfInstitutions = value;
                        }

                    }

                }

            }

 
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "yourInstitution",
                "outsideInstitution"
            })
            public static class OustandingBalanceByCFAndAssetClasificationSec {

                @XmlElement(required = true)
                protected String message;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution yourInstitution;
                @XmlElement(required = true)
                protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution outsideInstitution;

            
                public String getMessage() {
                    return message;
                }

            
                public void setMessage(String value) {
                    this.message = value;
                }

             
                public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution getYourInstitution() {
                    return yourInstitution;
                }

             
                public void setYourInstitution(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution value) {
                    this.yourInstitution = value;
                }

             
                public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution getOutsideInstitution() {
                    return outsideInstitution;
                }

             
                public void setOutsideInstitution(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution value) {
                    this.outsideInstitution = value;
                }

 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "nonFunded",
                    "workingCapital",
                    "termLoan",
                    "forex",
                    "total"
                })
                public static class OutsideInstitution {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded nonFunded;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital workingCapital;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan termLoan;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex forex;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total total;

                   
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded getNonFunded() {
                        return nonFunded;
                    }
 
                    public void setNonFunded(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded value) {
                        this.nonFunded = value;
                    }
 
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital getWorkingCapital() {
                        return workingCapital;
                    }

                   
                    public void setWorkingCapital(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital value) {
                        this.workingCapital = value;
                    }
 
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan getTermLoan() {
                        return termLoan;
                    }
 
                    public void setTermLoan(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan value) {
                        this.termLoan = value;
                    }
 
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex getForex() {
                        return forex;
                    }

               
                    public void setForex(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex value) {
                        this.forex = value;
                    }

            
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total getTotal() {
                        return total;
                    }
 
                    public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total value) {
                        this.total = value;
                    }
 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class Forex {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.Total total;
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec getSTDVec() {
                            return stdVec;
                        }

                 
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec value) {
                            this.stdVec = value;
                        }
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                    
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec value) {
                            this.nonstdVec = value;
                        }
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.Total getTotal() {
                            return total;
                        }

                      
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.Total value) {
                            this.total = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Loss loss;

                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                    
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

              
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

             
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

                
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Sub getSub() {
                                return sub;
                            }

              
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

            
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.NONSTDVec.Loss value) {
                                this.loss = value;
                            }


              
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }
 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


   
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }

                           
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                      
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                
                                public String getCount() {
                                    return count;
                                }

                                
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD61To90 dpd61To90;

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }

                         
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                           
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }
 
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                    
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

                    
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Forex.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }


 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


     
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                    
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


              
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                        
                            public String getCount() {
                                return count;
                            }

                           
                            public void setCount(String value) {
                                this.count = value;
                            }

                           
                            public String getValue() {
                                return value;
                            }

                       
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class NonFunded {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.Total total;
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec getSTDVec() {
                            return stdVec;
                        }

                      
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec value) {
                            this.stdVec = value;
                        }
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                    
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.Total getTotal() {
                            return total;
                        }

                      
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.Total value) {
                            this.total = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Loss loss;

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                           
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

                        
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Sub getSub() {
                                return sub;
                            }

                           
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

                         
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                          
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.NONSTDVec.Loss value) {
                                this.loss = value;
                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                            
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


     
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                      
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                 
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                                
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                                 
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD61To90 dpd61To90;

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }
 
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                        
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

                           
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

                        
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.NonFunded.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                
                                public String getCount() {
                                    return count;
                                }

                                 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                           
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                           
                                public String getCount() {
                                    return count;
                                }

                          
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                       
                            public String getCount() {
                                return count;
                            }

                      
                            public void setCount(String value) {
                                this.count = value;
                            }

                      
                            public String getValue() {
                                return value;
                            }

                       
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class TermLoan {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.Total total;

                      
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec getSTDVec() {
                            return stdVec;
                        }

                        
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec value) {
                            this.stdVec = value;
                        }

                
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                  
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

                 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.Total getTotal() {
                            return total;
                        }

             
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.Total value) {
                            this.total = value;
                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Loss loss;

            
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }
 
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }
 
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Sub getSub() {
                                return sub;
                            }
 
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

                   
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

                     
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                      
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.NONSTDVec.Loss value) {
                                this.loss = value;
                            }
 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                        
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                  
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                            
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                           
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

  
                                public void setCount(String value) {
                                    this.count = value;
                                }

 
                                public String getValue() {
                                    return value;
                                }

   
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                      
                                public String getCount() {
                                    return count;
                                }

                           
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }
 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                            
                                public String getCount() {
                                    return count;
                                }

                        
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                           
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD61To90 dpd61To90;
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }

                            
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

                  
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                        
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

                   
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                       
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

                      
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.TermLoan.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }
 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                           
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }

 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                        
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                           
                                public String getValue() {
                                    return value;
                                }

                    
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                          
                                public String getCount() {
                                    return count;
                                }

                           
                                public void setCount(String value) {
                                    this.count = value;
                                }

                           
                                public String getValue() {
                                    return value;
                                }

                          
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;
 
                            public String getCount() {
                                return count;
                            }

                      
                            public void setCount(String value) {
                                this.count = value;
                            }

                           
                            public String getValue() {
                                return value;
                            }

                   
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class Total {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec nonstdVec;
                        @XmlElement(name="Total",required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.InsideTotal total;
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec getSTDVec() {
                            return stdVec;
                        }
 
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec value) {
                            this.stdVec = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                    
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

                   
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.InsideTotal getTotal() {
                            return total;
                        }

                      
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.InsideTotal value) {
                            this.total = value;
                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "Total", propOrder = {
                            "count",
                            "value"
                        })
                        public static class InsideTotal {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                       
                            public String getCount() {
                                return count;
                            }

                          
                            public void setCount(String value) {
                                this.count = value;
                            }

                      
                            public String getValue() {
                                return value;
                            }

                          
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }
                        //rajesh
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Loss loss;

                      
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                        
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

                        
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Sub getSub() {
                                return sub;
                            }

                     
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

                      
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

                       
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                         
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.NONSTDVec.Loss value) {
                                this.loss = value;
                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                           
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                            
                                public String getCount() {
                                    return count;
                                }

                           
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }
 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                    
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                           
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                           
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                         
                                public String getCount() {
                                    return count;
                                }

                          
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD61To90 dpd61To90;

                       
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }
 
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

                      
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                    
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

               
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                     
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }
 
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.Total.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                   
                                public String getCount() {
                                    return count;
                                }

                
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                   
                                public String getCount() {
                                    return count;
                                }

     
                                public void setCount(String value) {
                                    this.count = value;
                                }

  
                                public String getValue() {
                                    return value;
                                }

           
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


       
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                
                                public void setCount(String value) {
                                    this.count = value;
                                }

              
                                public String getValue() {
                                    return value;
                                }

             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


  
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                         
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class WorkingCapital {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.Total total;

                     
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec getSTDVec() {
                            return stdVec;
                        }

                  
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec value) {
                            this.stdVec = value;
                        }

            
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

             
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

                   
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.Total getTotal() {
                            return total;
                        }

                   
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.Total value) {
                            this.total = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Loss loss;

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                    
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

                       
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

                       
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Sub getSub() {
                                return sub;
                            }

                   
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

                 
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }

                      
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                         
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.NONSTDVec.Loss value) {
                                this.loss = value;
                            }


                
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                          
                                public String getCount() {
                                    return count;
                                }

                     
                                public void setCount(String value) {
                                    this.count = value;
                                }

                         
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                    
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                         
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                     
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                        
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD61To90 dpd61To90;

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }

                         
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                           
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                             
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

                          
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                             
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

                             
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.OutsideInstitution.WorkingCapital.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }


                            
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                                 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                            
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                       
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


                       
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                          
                            public String getCount() {
                                return count;
                            }

                          
                            public void setCount(String value) {
                                this.count = value;
                            }

                       
                            public String getValue() {
                                return value;
                            }

                           
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

                }


            
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "nonFunded",
                    "workingCapital",
                    "termLoan",
                    "forex",
                    "total"
                })
                public static class YourInstitution {

                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded nonFunded;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital workingCapital;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan termLoan;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex forex;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total total;

                  
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded getNonFunded() {
                        return nonFunded;
                    }

               
                    public void setNonFunded(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded value) {
                        this.nonFunded = value;
                    }

                
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital getWorkingCapital() {
                        return workingCapital;
                    }

              
                    public void setWorkingCapital(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital value) {
                        this.workingCapital = value;
                    }

                
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan getTermLoan() {
                        return termLoan;
                    }

                  
                    public void setTermLoan(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan value) {
                        this.termLoan = value;
                    }
 
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex getForex() {
                        return forex;
                    }

                  
                    public void setForex(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex value) {
                        this.forex = value;
                    }

                    
                    public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total getTotal() {
                        return total;
                    }

                  
                    public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total value) {
                        this.total = value;
                    }


 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class Forex {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.Total total;

                    
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec getSTDVec() {
                            return stdVec;
                        }

                     
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec value) {
                            this.stdVec = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                  
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

                   
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.Total getTotal() {
                            return total;
                        }

                    
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.Total value) {
                            this.total = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Loss loss;

                      
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                        
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

                            
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

                
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

               
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Sub getSub() {
                                return sub;
                            }

          
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

         
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

   
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.NONSTDVec.Loss value) {
                                this.loss = value;
                            }


 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

 
                                public String getCount() {
                                    return count;
                                }

 
                                public void setCount(String value) {
                                    this.count = value;
                                }

 
                                public String getValue() {
                                    return value;
                                }
 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

 
                                public String getCount() {
                                    return count;
                                }

 
                                public void setCount(String value) {
                                    this.count = value;
                                }

 
                                public String getValue() {
                                    return value;
                                }
 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                           
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD61To90 dpd61To90;

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }
 
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                     
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

                        
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                     
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

                      
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Forex.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


           
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                      
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                        
                            public String getCount() {
                                return count;
                            }

                          
                            public void setCount(String value) {
                                this.count = value;
                            }

                           
                            public String getValue() {
                                return value;
                            }

                          
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class NonFunded {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.Total total;

                      
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec getSTDVec() {
                            return stdVec;
                        }

                       
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec value) {
                            this.stdVec = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                      
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.Total getTotal() {
                            return total;
                        }

                        
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.Total value) {
                            this.total = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Loss loss;

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                          
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

                      
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

                            
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Sub getSub() {
                                return sub;
                            }

                           
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

                            
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

                          
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }

                        
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                          
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.NONSTDVec.Loss value) {
                                this.loss = value;
                            }


                        
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                        
                                public String getCount() {
                                    return count;
                                }

            
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


  
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


        
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                
                                public String getCount() {
                                    return count;
                                }

                            
                                public void setCount(String value) {
                                    this.count = value;
                                }

                                 
                                public String getValue() {
                                    return value;
                                }
 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                          
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD61To90 dpd61To90;

                             
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }

                             
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                        
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

                            
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }
 
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.NonFunded.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }


                           
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                                
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                          
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                                
                                public void setCount(String value) {
                                    this.count = value;
                                }

                                
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                         
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                                
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


                       
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                         
                            public String getCount() {
                                return count;
                            }

                         
                            public void setCount(String value) {
                                this.count = value;
                            }

                           
                            public String getValue() {
                                return value;
                            }

                           
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class TermLoan {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.Total total;
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec getSTDVec() {
                            return stdVec;
                        }

 
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec value) {
                            this.stdVec = value;
                        }

                  
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                   
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec value) {
                            this.nonstdVec = value;
                        }
 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.Total getTotal() {
                            return total;
                        }

                 
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.Total value) {
                            this.total = value;
                        }


 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Loss loss;

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                          
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

                       
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }
 
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Sub getSub() {
                                return sub;
                            }

                          
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

                        
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }
 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                          
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.NONSTDVec.Loss value) {
                                this.loss = value;
                            }


                      
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                            
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                         
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }

                            
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                    
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }
 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                      
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                            
                                public String getCount() {
                                    return count;
                                }

                            
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                       
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


  
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD61To90 dpd61To90;

                       
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }

                         
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                        
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

                          
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

                         
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.TermLoan.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }


                        
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                         
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                    
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                            
                                public String getCount() {
                                    return count;
                                }

                           
                                public void setCount(String value) {
                                    this.count = value;
                                }

                          
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                         
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                 
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                          
                            public String getCount() {
                                return count;
                            }

                          
                            public void setCount(String value) {
                                this.count = value;
                            }

                         
                            public String getValue() {
                                return value;
                            }

                           
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class Total {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec nonstdVec;
                        @XmlElement(name="Total",required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.InsideTotal total;

                   
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec getSTDVec() {
                            return stdVec;
                        }

                       
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec value) {
                            this.stdVec = value;
                        }

                      
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

                      
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

                       
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.InsideTotal getTotal() {
                            return total;
                        }

                       
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.InsideTotal value) {
                            this.total = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Loss loss;

                            
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

                            
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

                           
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Sub getSub() {
                                return sub;
                            }

                         
                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Sub value) {
                                this.sub = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }

                           
                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }

                           
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Loss getLoss() {
                                return loss;
                            }

                          
                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.NONSTDVec.Loss value) {
                                this.loss = value;
                            }


                          
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                 
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                         
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                            
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                           
                                public String getCount() {
                                    return count;
                                }

                           
                                public void setCount(String value) {
                                    this.count = value;
                                }

                         
                                public String getValue() {
                                    return value;
                                }

                        
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

                        
                                public void setCount(String value) {
                                    this.count = value;
                                }
 
                                public String getValue() {
                                    return value;
                                }

                        
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                           
                                public String getCount() {
                                    return count;
                                }

                  
                                public void setCount(String value) {
                                    this.count = value;
                                }

            
                                public String getValue() {
                                    return value;
                                }

 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD61To90 dpd61To90;

 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }
 
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

 
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

 
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

 
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.Total.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

 
                                public String getCount() {
                                    return count;
                                }

 
                                public void setCount(String value) {
                                    this.count = value;
                                }

 
                                public String getValue() {
                                    return value;
                                }

 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

 
                                public String getCount() {
                                    return count;
                                }
 
                                public void setCount(String value) {
                                    this.count = value;
                                }

 
                                public String getValue() {
                                    return value;
                                }

 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

         
                                public void setCount(String value) {
                                    this.count = value;
                                }

 
                                public String getValue() {
                                    return value;
                                }

 
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

 
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;
 
                                public String getCount() {
                                    return count;
                                }

  
                                public void setCount(String value) {
                                    this.count = value;
                                }

 
                                public String getValue() {
                                    return value;
                                }

        
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class InsideTotal {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

 
                            public String getCount() {
                                return count;
                            }

 
                            public void setCount(String value) {
                                this.count = value;
                            }

 
                            public String getValue() {
                                return value;
                            }

 
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "stdVec",
                        "nonstdVec",
                        "total"
                    })
                    public static class WorkingCapital {

                        @XmlElement(name = "STDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec stdVec;
                        @XmlElement(name = "NONSTDVec", required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec nonstdVec;
                        @XmlElement(required = true)
                        protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.Total total;

 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec getSTDVec() {
                            return stdVec;
                        }

 
                        public void setSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec value) {
                            this.stdVec = value;
                        }

 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec getNONSTDVec() {
                            return nonstdVec;
                        }

 
                        public void setNONSTDVec(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec value) {
                            this.nonstdVec = value;
                        }

 
                        public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.Total getTotal() {
                            return total;
                        }

 
                        public void setTotal(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.Total value) {
                            this.total = value;
                        }

 
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd91To180",
                            "greaterThan180DPD",
                            "sub",
                            "dbt",
                            "loss"
                        })
                        public static class NONSTDVec {

                            @XmlElement(name = "DPD91to180", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.DPD91To180 dpd91To180;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD greaterThan180DPD;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Sub sub;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Dbt dbt;
                            @XmlElement(required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Loss loss;

   
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.DPD91To180 getDPD91To180() {
                                return dpd91To180;
                            }

 
                            public void setDPD91To180(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.DPD91To180 value) {
                                this.dpd91To180 = value;
                            }

 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD getGreaterThan180DPD() {
                                return greaterThan180DPD;
                            }

    
                            public void setGreaterThan180DPD(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.GreaterThan180DPD value) {
                                this.greaterThan180DPD = value;
                            }

 
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Sub getSub() {
                                return sub;
                            }


                            public void setSub(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Sub value) {
                                this.sub = value;
                            }


                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Dbt getDbt() {
                                return dbt;
                            }


                            public void setDbt(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Dbt value) {
                                this.dbt = value;
                            }


                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Loss getLoss() {
                                return loss;
                            }


                            public void setLoss(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.NONSTDVec.Loss value) {
                                this.loss = value;
                            }



                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD91To180 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                         
                                public void setCount(String value) {
                                    this.count = value;
                                }

                           
                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Dbt {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                             
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                               
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                          
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class GreaterThan180DPD {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                             
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                    
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Loss {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                            
                                public void setCount(String value) {
                                    this.count = value;
                                }

                                public String getValue() {
                                    return value;
                                }

                             
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                      
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class Sub {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                                
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "dpd0",
                            "dpd1To30",
                            "dpd31To60",
                            "dpd61To90"
                        })
                        public static class STDVec {

                            @XmlElement(name = "DPD0", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD0 dpd0;
                            @XmlElement(name = "DPD1to30", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD1To30 dpd1To30;
                            @XmlElement(name = "DPD31to60", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD31To60 dpd31To60;
                            @XmlElement(name = "DPD61to90", required = true)
                            protected Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD61To90 dpd61To90;

                       
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD0 getDPD0() {
                                return dpd0;
                            }

                          
                            public void setDPD0(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD0 value) {
                                this.dpd0 = value;
                            }

                         
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD1To30 getDPD1To30() {
                                return dpd1To30;
                            }

                         
                            public void setDPD1To30(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD1To30 value) {
                                this.dpd1To30 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD31To60 getDPD31To60() {
                                return dpd31To60;
                            }

                           
                            public void setDPD31To60(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD31To60 value) {
                                this.dpd31To60 = value;
                            }

                          
                            public Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD61To90 getDPD61To90() {
                                return dpd61To90;
                            }

                           
                            public void setDPD61To90(Base.ResponseReport.ProductSec.OustandingBalanceByCFAndAssetClasificationSec.YourInstitution.WorkingCapital.STDVec.DPD61To90 value) {
                                this.dpd61To90 = value;
                            }



                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD0 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                                
                                public String getCount() {
                                    return count;
                                }

                                
                                public void setCount(String value) {
                                    this.count = value;
                                }

                                
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                      
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD1To30 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                              
                                public void setCount(String value) {
                                    this.count = value;
                                }

                             
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                        
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD31To60 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                              
                                public String getCount() {
                                    return count;
                                }

                               
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                              
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }


                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "count",
                                "value"
                            })
                            public static class DPD61To90 {

                                @XmlElement(required = true)
                                protected String count;
                                @XmlElement(required = true)
                                protected String value;

                               
                                public String getCount() {
                                    return count;
                                }

                                
                                public void setCount(String value) {
                                    this.count = value;
                                }

                              
                                public String getValue() {
                                    return value;
                                }

                               
                                public void setValue(String value) {
                                    this.value = value;
                                }

                            }

                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "count",
                            "value"
                        })
                        public static class Total {

                            @XmlElement(required = true)
                            protected String count;
                            @XmlElement(required = true)
                            protected String value;

                          
                            public String getCount() {
                                return count;
                            }

                          
                            public void setCount(String value) {
                                this.count = value;
                            }

                           
                            public String getValue() {
                                return value;
                            }

                           
                            public void setValue(String value) {
                                this.value = value;
                            }

                        }

                    }

                }

            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "rankVec"
            })
            public static class RankSec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.RankSec.RankVec> rankVec;

               
                public String getMessage() {
                    return message;
                }

               
                public void setMessage(String value) {
                    this.message = value;
                }

              
                public List<Base.ResponseReport.ProductSec.RankSec.RankVec> getRankVec() {
                    if (rankVec == null) {
                        rankVec = new ArrayList<Base.ResponseReport.ProductSec.RankSec.RankVec>();
                    }
                    return this.rankVec;
                }


            
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "rankName",
                    "rankValue",
                    "exclusionReason"
                })
                public static class RankVec {

                    @XmlElement(required = true)
                    protected String rankName;
                    @XmlElement(required = true)
                    protected String rankValue;
                    @XmlElement(required = true)
                    protected String exclusionReason;

                  
                    public String getRankName() {
                        return rankName;
                    }

                 
                    public void setRankName(String value) {
                        this.rankName = value;
                    }

                    public String getRankValue() {
                        return rankValue;
                    }

                 
                    public void setRankValue(String value) {
                        this.rankValue = value;
                    }

                 
                    public String getExclusionReason() {
                        return exclusionReason;
                    }

              
                    public void setExclusionReason(String value) {
                        this.exclusionReason = value;
                    }

                }

            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "relationshipDetails"
            })
            public static class RelationshipDetailsVec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails> relationshipDetails;


                public String getMessage() {
                    return message;
                }

           
                public void setMessage(String value) {
                    this.message = value;
                }

               
                public List<Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails> getRelationshipDetails() {
                    if (relationshipDetails == null) {
                        relationshipDetails = new ArrayList<Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails>();
                    }
                    return this.relationshipDetails;
                }


                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "relationshipHeader",
                    "relationshipInformation",
                    "borrwerAddressContactDetails",
                    "borrwerIDDetailsVec"
                })
                public static class RelationshipDetails {

                    @XmlElement(required = true)
                    protected String relationshipHeader;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.RelationshipInformation relationshipInformation;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerAddressContactDetails borrwerAddressContactDetails;
                    @XmlElement(required = true)
                    protected Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec borrwerIDDetailsVec;

                 
                    public String getRelationshipHeader() {
                        return relationshipHeader;
                    }

              
                    public void setRelationshipHeader(String value) {
                        this.relationshipHeader = value;
                    }

                  
                    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.RelationshipInformation getRelationshipInformation() {
                        return relationshipInformation;
                    }

                    public void setRelationshipInformation(Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.RelationshipInformation value) {
                        this.relationshipInformation = value;
                    }

           
                    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerAddressContactDetails getBorrwerAddressContactDetails() {
                        return borrwerAddressContactDetails;
                    }

  
                    public void setBorrwerAddressContactDetails(Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerAddressContactDetails value) {
                        this.borrwerAddressContactDetails = value;
                    }


                    public Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec getBorrwerIDDetailsVec() {
                        return borrwerIDDetailsVec;
                    }

                    public void setBorrwerIDDetailsVec(Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec value) {
                        this.borrwerIDDetailsVec = value;
                    }

 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "address",
                        "telephoneNumber",
                        "faxNumber",
                        "mobileNumber"
                    })
                    public static class BorrwerAddressContactDetails {

                        @XmlElement(required = true)
                        protected String address;
                        @XmlElement(required = true)
                        protected String telephoneNumber;
                        @XmlElement(required = true)
                        protected String faxNumber;
                        @XmlElement(required = true)
                        protected String mobileNumber;

                        public String getAddress() {
                            return address;
                        }


                        public void setAddress(String value) {
                            this.address = value;
                        }


                        public String getTelephoneNumber() {
                            return telephoneNumber;
                        }


                        public void setTelephoneNumber(String value) {
                            this.telephoneNumber = value;
                        }


                        public String getFaxNumber() {
                            return faxNumber;
                        }


                        public void setFaxNumber(String value) {
                            this.faxNumber = value;
                        }


                        public String getMobileNumber() {
                            return mobileNumber;
                        }


                        public void setMobileNumber(String value) {
                            this.mobileNumber = value;
                        }

                    }
 
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "lastReportedDate",
                        "borrwerIDDetails"
                    })
                    public static class BorrwerIDDetailsVec {

                        @XmlElement(required = true)
                        protected String lastReportedDate;
                        protected List<Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec.BorrwerIDDetails> borrwerIDDetails;


                        public String getLastReportedDate() {
                            return lastReportedDate;
                        }


                        public void setLastReportedDate(String value) {
                            this.lastReportedDate = value;
                        }


                        public List<Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec.BorrwerIDDetails> getBorrwerIDDetails() {
                            if (borrwerIDDetails == null) {
                                borrwerIDDetails = new ArrayList<Base.ResponseReport.ProductSec.RelationshipDetailsVec.RelationshipDetails.BorrwerIDDetailsVec.BorrwerIDDetails>();
                            }
                            return this.borrwerIDDetails;
                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "pan",
                            "din",
                            "voterID",
                            "passportNumber",
                            "drivingLicenseNo",
                            "uid",
                            "registrationNumber",
                            "cin",
                            "tin",
                            "serviceTaxNumber",
                            "rationCard"
                        })
                        public static class BorrwerIDDetails {

                            @XmlElement(required = true)
                            protected String pan;
                            @XmlElement(required = true)
                            protected String din;
                            @XmlElement(required = true)
                            protected String voterID;
                            @XmlElement(required = true)
                            protected String passportNumber;
                            @XmlElement(required = true)
                            protected String drivingLicenseNo;
                            @XmlElement(required = true)
                            protected String uid;
                            @XmlElement(required = true)
                            protected String registrationNumber;
                            @XmlElement(required = true)
                            protected String cin;
                            @XmlElement(required = true)
                            protected String tin;
                            @XmlElement(required = true)
                            protected String serviceTaxNumber;
                            @XmlElement(required = true)
                            protected String rationCard;


                            public String getPan() {
                                return pan;
                            }


                            public void setPan(String value) {
                                this.pan = value;
                            }

 
                            public String getDin() {
                                return din;
                            }


                            public void setDin(String value) {
                                this.din = value;
                            }


                            public String getVoterID() {
                                return voterID;
                            }


                            public void setVoterID(String value) {
                                this.voterID = value;
                            }


                            public String getPassportNumber() {
                                return passportNumber;
                            }


                            public void setPassportNumber(String value) {
                                this.passportNumber = value;
                            }

 
                            public String getDrivingLicenseNo() {
                                return drivingLicenseNo;
                            }


                            public void setDrivingLicenseNo(String value) {
                                this.drivingLicenseNo = value;
                            }


                            public String getUid() {
                                return uid;
                            }


                            public void setUid(String value) {
                                this.uid = value;
                            }


                            public String getRegistrationNumber() {
                                return registrationNumber;
                            }


                            public void setRegistrationNumber(String value) {
                                this.registrationNumber = value;
                            }


                            public String getCin() {
                                return cin;
                            }


                            public void setCin(String value) {
                                this.cin = value;
                            }


                            public String getTin() {
                                return tin;
                            }


                            public void setTin(String value) {
                                this.tin = value;
                            }

  
                            public String getServiceTaxNumber() {
                                return serviceTaxNumber;
                            }


                            public void setServiceTaxNumber(String value) {
                                this.serviceTaxNumber = value;
                            }

   
                            public String getRationCard() {
                                return rationCard;
                            }

                        
                            public void setRationCard(String value) {
                                this.rationCard = value;
                            }

                        }

                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "name",
                        "relatedType",
                        "relationship",
                        "percentageOfControl",
                        "dateOfIncorporation",
                        "dateOfBirth",
                        "gender",
                        "businessCategory",
                        "businessIndustryType",
                        "classOfActivity1"
                    })
                    public static class RelationshipInformation {

                        @XmlElement(required = true)
                        protected String name;
                        @XmlElement(required = true)
                        protected String relatedType;
                        @XmlElement(required = true)
                        protected String relationship;
                        @XmlElement(required = true)
                        protected String percentageOfControl;
                        @XmlElement(required = true)
                        protected String dateOfIncorporation;
                        @XmlElement(required = true)
                        protected String dateOfBirth;
                        @XmlElement(required = true)
                        protected String gender;
                        @XmlElement(required = true)
                        protected String businessCategory;
                        @XmlElement(required = true)
                        protected String businessIndustryType;
                        @XmlElement(required = true)
                        protected String classOfActivity1;

                        
                        public String getName() {
                            return name;
                        }

                        
                        public void setName(String value) {
                            this.name = value;
                        }

                        
                        public String getRelatedType() {
                            return relatedType;
                        }

                       
                        public void setRelatedType(String value) {
                            this.relatedType = value;
                        }

                       
                        public String getRelationship() {
                            return relationship;
                        }

                        
                        public void setRelationship(String value) {
                            this.relationship = value;
                        }

                       
                        public String getPercentageOfControl() {
                            return percentageOfControl;
                        }

                        
                        public void setPercentageOfControl(String value) {
                            this.percentageOfControl = value;
                        }

                        
                        public String getDateOfIncorporation() {
                            return dateOfIncorporation;
                        }

                       
                        public void setDateOfIncorporation(String value) {
                            this.dateOfIncorporation = value;
                        }

                        
                        public String getDateOfBirth() {
                            return dateOfBirth;
                        }

                       
                        public void setDateOfBirth(String value) {
                            this.dateOfBirth = value;
                        }

                        
                        public String getGender() {
                            return gender;
                        }

                       
                        public void setGender(String value) {
                            this.gender = value;
                        }

                       
                        public String getBusinessCategory() {
                            return businessCategory;
                        }

                       
                        public void setBusinessCategory(String value) {
                            this.businessCategory = value;
                        }

                        
                        public String getBusinessIndustryType() {
                            return businessIndustryType;
                        }

                        
                        public void setBusinessIndustryType(String value) {
                            this.businessIndustryType = value;
                        }

                        
                        public String getClassOfActivity1() {
                            return classOfActivity1;
                        }

                       
                        public void setClassOfActivity1(String value) {
                            this.classOfActivity1 = value;
                        }

                    }

                }

            }

            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "message",
                "suitFilled"
            })
            public static class SuitFiledVec {

                @XmlElement(required = true)
                protected String message;
                protected List<Base.ResponseReport.ProductSec.SuitFiledVec.SuitFilled> suitFilled;

                
                public String getMessage() {
                    return message;
                }

               
                public void setMessage(String value) {
                    this.message = value;
                }

                
                public List<Base.ResponseReport.ProductSec.SuitFiledVec.SuitFilled> getSuitFilled() {
                    if (suitFilled == null) {
                        suitFilled = new ArrayList<Base.ResponseReport.ProductSec.SuitFiledVec.SuitFilled>();
                    }
                    return this.suitFilled;
                }

 
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "suitFilledBy",
                    "suitStatus",
                    "suitRefNumber",
                    "suitAmt",
                    "dateSuit"
                })
                public static class SuitFilled {

                    @XmlElement(required = true)
                    protected String suitFilledBy;
                    @XmlElement(required = true)
                    protected String suitStatus;
                    @XmlElement(required = true)
                    protected String suitRefNumber;
                    @XmlElement(required = true)
                    protected String suitAmt;
                    @XmlElement(required = true)
                    protected String dateSuit;


                    public String getSuitFilledBy() {
                        return suitFilledBy;
                    }


                    public void setSuitFilledBy(String value) {
                        this.suitFilledBy = value;
                    }

 
                    public String getSuitStatus() {
                        return suitStatus;
                    }


                    public void setSuitStatus(String value) {
                        this.suitStatus = value;
                    }

  
                    public String getSuitRefNumber() {
                        return suitRefNumber;
                    }


                    public void setSuitRefNumber(String value) {
                        this.suitRefNumber = value;
                    }


                    public String getSuitAmt() {
                        return suitAmt;
                    }

      
                    public void setSuitAmt(String value) {
                        this.suitAmt = value;
                    }


                    public String getDateSuit() {
                        return dateSuit;
                    }

              
                    public void setDateSuit(String value) {
                        this.dateSuit = value;
                    }

                }

            }

        }
 
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "daysPasswordToExpire",
            "reportOrderNumber",
            "reportOrderDate",
            "reportOrderedBy",
            "memberDetails",
            "applicationReferenceNumber",
            "memberReferenceNumber",
            "inquiryPurpose"
        })
        public static class ReportHeaderRec {

            @XmlElement(required = true)
            protected String daysPasswordToExpire;
            @XmlElement(required = true)
            protected String reportOrderNumber;
            @XmlElement(required = true)
            protected String reportOrderDate;
            @XmlElement(required = true)
            protected String reportOrderedBy;
            @XmlElement(required = true)
            protected String memberDetails;
            @XmlElement(required = true)
            protected String applicationReferenceNumber;
            @XmlElement(required = true)
            protected String memberReferenceNumber;
            @XmlElement(required = true)
            protected String inquiryPurpose;

           
            public String getDaysPasswordToExpire() {
                return daysPasswordToExpire;
            }

            
            public void setDaysPasswordToExpire(String value) {
                this.daysPasswordToExpire = value;
            }

            
            public String getReportOrderNumber() {
                return reportOrderNumber;
            }

            
            public void setReportOrderNumber(String value) {
                this.reportOrderNumber = value;
            }

           
            public String getReportOrderDate() {
                return reportOrderDate;
            }

            
            public void setReportOrderDate(String value) {
                this.reportOrderDate = value;
            }

            
            public String getReportOrderedBy() {
                return reportOrderedBy;
            }

            
            public void setReportOrderedBy(String value) {
                this.reportOrderedBy = value;
            }

            
            public String getMemberDetails() {
                return memberDetails;
            }

            
            public void setMemberDetails(String value) {
                this.memberDetails = value;
            }

            
            public String getApplicationReferenceNumber() {
                return applicationReferenceNumber;
            }

           
            public void setApplicationReferenceNumber(String value) {
                this.applicationReferenceNumber = value;
            }

            
            public String getMemberReferenceNumber() {
                return memberReferenceNumber;
            }

           
            public void setMemberReferenceNumber(String value) {
                this.memberReferenceNumber = value;
            }

            
            public String getInquiryPurpose() {
                return inquiryPurpose;
            }

           
            public void setInquiryPurpose(String value) {
                this.inquiryPurpose = value;
            }

        }
 
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "code",
            "message",
            "description"
        })
        public static class ReportIssuesVec {

            @XmlElement(required = true)
            protected String code;
            @XmlElement(required = true)
            protected String message;
            @XmlElement(required = true)
            protected String description;

            
            public String getCode() {
                return code;
            }

            
            public void setCode(String value) {
                this.code = value;
            }

            
            public String getMessage() {
                return message;
            }

           
            public void setMessage(String value) {
                this.message = value;
            }

            
            public String getDescription() {
                return description;
            }

            
            public void setDescription(String value) {
                this.description = value;
            }

        }

    }

}
