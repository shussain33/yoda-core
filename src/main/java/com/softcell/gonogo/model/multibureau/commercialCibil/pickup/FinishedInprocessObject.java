package com.softcell.gonogo.model.multibureau.commercialCibil.pickup;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FinishedInprocessObject {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@JsonProperty("TRACKING-ID")
	private Long trackindId;
	
	@JsonProperty("BUREAU")
	private String bureauName;
	
	@JsonProperty("PRODUCT")
	private String productCode;
	
	@JsonProperty("HTML-REPORT")
	private String htmlReport;
	
	@JsonProperty("BUREAU-STRING")
	private String bureauOutput;
	
	@JsonProperty("PDF REPORT")
	private byte[] pdfReport;
	
	@JsonProperty("JSON-RESPONSE-OBJECT")
	private Object jsonObject;
	
	@JsonProperty("STATUS")
	private String status;

	@JsonProperty("ERRORS")
	private List<ErrorWarning> errors;
	
	
	public Long getTrackindId() {
		return trackindId;
	}
	public void setTrackindId(Long trackindId) {
		this.trackindId = trackindId;
	}
	public String getBureauName() {
		return bureauName;
	}
	public void setBureauName(String bureauName) {
		this.bureauName = bureauName;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getHtmlReport() {
		return htmlReport;
	}
	public void setHtmlReport(String htmlReport) {
		this.htmlReport = htmlReport;
	}
	public String getBureauOutput() {
		return bureauOutput;
	}
	public void setBureauOutput(String bureauOutput) {
		this.bureauOutput = bureauOutput;
	}
	public byte[] getPdfReport() {
		return pdfReport;
	}
	public void setPdfReport(byte[] pdfReport) {
		this.pdfReport = pdfReport;
	}
	public Object getJsonObject() {
		return jsonObject;
	}
	public void setJsonObject(Object jsonObject) {
		this.jsonObject = jsonObject;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public List<ErrorWarning> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorWarning> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "FinishObject [trackindId=" + trackindId + ", bureauName="
				+ bureauName + ", productCode=" + productCode + ", htmlReport="
				+ htmlReport + ", bureauOutput=" + bureauOutput
				+ ", pdfReport=" + Arrays.toString(pdfReport) + ", jsonObject="
				+ jsonObject + ", status=" + status + ", errors=" + errors +"]";
	}
}
