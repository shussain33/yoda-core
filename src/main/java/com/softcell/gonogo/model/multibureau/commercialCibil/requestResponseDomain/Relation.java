package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import com.google.gson.annotations.SerializedName;

public class Relation {

	/**
	 * @author namratat
	 *
	 *
	 */
	
	@SerializedName("01")
	private String fatherName;

	@SerializedName("02")
	private String spouseName;

	@SerializedName("03")
	private String motherName;

	@SerializedName("04")
	private String relationType1;

	@SerializedName("05")
	private String relationType1Value;

	@SerializedName("06")
	private String relationType2;

	@SerializedName("07")
	private String relationType2Value;

	@SerializedName("08")
	private String keyPersonName;

	@SerializedName("09")
	private String keyPersonRelation;

	@SerializedName("10")
	private String nomineeName;

	@SerializedName("11")
	private String nomineeRelationType;

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getRelationType1() {
		return relationType1;
	}

	public void setRelationType1(String relationType1) {
		this.relationType1 = relationType1;
	}

	public String getRelationType1Value() {
		return relationType1Value;
	}

	public void setRelationType1Value(String relationType1Value) {
		this.relationType1Value = relationType1Value;
	}

	public String getRelationType2() {
		return relationType2;
	}

	public void setRelationType2(String relationType2) {
		this.relationType2 = relationType2;
	}

	public String getRelationType2Value() {
		return relationType2Value;
	}

	public void setRelationType2Value(String relationType2Value) {
		this.relationType2Value = relationType2Value;
	}

	public String getKeyPersonName() {
		return keyPersonName;
	}

	public void setKeyPersonName(String keyPersonName) {
		this.keyPersonName = keyPersonName;
	}

	public String getKeyPersonRelation() {
		return keyPersonRelation;
	}

	public void setKeyPersonRelation(String keyPersonRelation) {
		this.keyPersonRelation = keyPersonRelation;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getNomineeRelationType() {
		return nomineeRelationType;
	}

	public void setNomineeRelationType(String nomineeRelationType) {
		this.nomineeRelationType = nomineeRelationType;
	}

	@Override
	public String toString() {
		return "Relation [fatherName=" + fatherName + ", spouseName="
				+ spouseName + ", motherName=" + motherName
				+ ", relationType1=" + relationType1 + ", relationType1Value="
				+ relationType1Value + ", relationType2=" + relationType2
				+ ", relationType2Value=" + relationType2Value
				+ ", keyPersonName=" + keyPersonName + ", keyPersonRelation="
				+ keyPersonRelation + ", nomineeName=" + nomineeName
				+ ", nomineeRelationType=" + nomineeRelationType + "]";
	}
	
}
