package com.softcell.gonogo.model.multibureau.experian;

import java.util.List;

public class CAPS {

    private CAPSSummary capsSummary;
    private List<CAPSApplicationDetails> capsApplicationDetailList;

    public CAPSSummary getCapsSummary() {
        return capsSummary;
    }

    public void setCapsSummary(CAPSSummary capsSummary) {
        this.capsSummary = capsSummary;
    }

    public List<CAPSApplicationDetails> getCapsApplicationDetailList() {
        return capsApplicationDetailList;
    }

    public void setCapsApplicationDetailList(
            List<CAPSApplicationDetails> capsApplicationDetailList) {
        this.capsApplicationDetailList = capsApplicationDetailList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAPS{");
        sb.append("capsSummary=").append(capsSummary);
        sb.append(", capsApplicationDetailList=").append(capsApplicationDetailList);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAPS caps = (CAPS) o;

        if (capsSummary != null ? !capsSummary.equals(caps.capsSummary) : caps.capsSummary != null) return false;
        return capsApplicationDetailList != null ? capsApplicationDetailList.equals(caps.capsApplicationDetailList) : caps.capsApplicationDetailList == null;
    }

    @Override
    public int hashCode() {
        int result = capsSummary != null ? capsSummary.hashCode() : 0;
        result = 31 * result + (capsApplicationDetailList != null ? capsApplicationDetailList.hashCode() : 0);
        return result;
    }
}