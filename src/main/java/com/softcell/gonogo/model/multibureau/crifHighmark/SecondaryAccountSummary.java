package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SECONDARY-ACCOUNTS-SUMMARY")
@XmlAccessorType(XmlAccessType.FIELD)
public class SecondaryAccountSummary {

	@XmlElement(name="SECONDARY-NUMBER-OF-ACCOUNTS")
	private String secondaryNumberOfAccounts;
	@XmlElement(name="SECONDARY-ACTIVE-NUMBER-OF-ACCOUNTS")
	private String secondaryActiveNumberOfAccounts;
	@XmlElement(name="SECONDARY-OVERDUE-NUMBER-OF-ACCOUNTS")
	private String secondaryOverdueNumberOfAccounts;
	@XmlElement(name="SECONDARY-SECURED-NUMBER-OF-ACCOUNTS")
	private String secondarySecuredNumberOfAccounts;
	@XmlElement(name="SECONDARY-UNSECURED-NUMBER-OF-ACCOUNTS")
	private String secondaryUnsecuredNumberOfAccounts;
	@XmlElement(name="SECONDARY-UNTAGGED-NUMBER-OF-ACCOUNTS")
	private String secondaryUntaggedNumberOfAccounts;
	@XmlElement(name="SECONDARY-CURRENT-BALANCE")
	private String secondaryCurrentBalance;
	@XmlElement(name="SECONDARY-SANCTIONED-AMOUNT")
	private String secondarySanctionedAmount;
	@XmlElement(name="SECONDARY-DISBURSED-AMOUNT")
	private String secondaryDisbursedAmount;
	@XmlElement(name="SECONDARY-CURRENT-BALANCE")
	private String SecCurrBal;
	@XmlElement(name="SECONDARY-SANCTIONED-AMOUNT")
	private String SecSancAmt;
	@XmlElement(name="SECONDARY-DISBURSED-AMOUNT")
	private String SecDisbAmt;
	
	public String getSecCurrBal() {
		return SecCurrBal;
	}
	
	public void setSecCurrBal(String secCurrBal) {
		SecCurrBal = secCurrBal;
	}
	
	public String getSecSancAmt() {
		return SecSancAmt;
	}
	
	public void setSecSancAmt(String secSancAmt) {
		SecSancAmt = secSancAmt;
	}
	
	public String getSecDisbAmt() {
		return SecDisbAmt;
	}
	
	public void setSecDisbAmt(String secDisbAmt) {
		SecDisbAmt = secDisbAmt;
	}
	
	public String getSecondaryNumberOfAccounts() {
		return secondaryNumberOfAccounts;
	}
	public void setSecondaryNumberOfAccounts(String secondaryNumberOfAccounts) {
		this.secondaryNumberOfAccounts = secondaryNumberOfAccounts;
	}
	public String getSecondaryActiveNumberOfAccounts() {
		return secondaryActiveNumberOfAccounts;
	}
	public void setSecondaryActiveNumberOfAccounts(
			String secondaryActiveNumberOfAccounts) {
		this.secondaryActiveNumberOfAccounts = secondaryActiveNumberOfAccounts;
	}
	public String getSecondaryOverdueNumberOfAccounts() {
		return secondaryOverdueNumberOfAccounts;
	}
	public void setSecondaryOverdueNumberOfAccounts(
			String secondaryOverdueNumberOfAccounts) {
		this.secondaryOverdueNumberOfAccounts = secondaryOverdueNumberOfAccounts;
	}
	public String getSecondarySecuredNumberOfAccounts() {
		return secondarySecuredNumberOfAccounts;
	}
	public void setSecondarySecuredNumberOfAccounts(
			String secondarySecuredNumberOfAccounts) {
		this.secondarySecuredNumberOfAccounts = secondarySecuredNumberOfAccounts;
	}
	public String getSecondaryUnsecuredNumberOfAccounts() {
		return secondaryUnsecuredNumberOfAccounts;
	}
	public void setSecondaryUnsecuredNumberOfAccounts(
			String secondaryUnsecuredNumberOfAccounts) {
		this.secondaryUnsecuredNumberOfAccounts = secondaryUnsecuredNumberOfAccounts;
	}
	public String getSecondaryUntaggedNumberOfAccounts() {
		return secondaryUntaggedNumberOfAccounts;
	}
	public void setSecondaryUntaggedNumberOfAccounts(
			String secondaryUntaggedNumberOfAccounts) {
		this.secondaryUntaggedNumberOfAccounts = secondaryUntaggedNumberOfAccounts;
	}
	public String getSecondaryCurrentBalance() {
		return secondaryCurrentBalance;
	}
	public void setSecondaryCurrentBalance(String secondaryCurrentBalance) {
		this.secondaryCurrentBalance = secondaryCurrentBalance;
	}
	public String getSecondarySanctionedAmount() {
		return secondarySanctionedAmount;
	}
	public void setSecondarySanctionedAmount(String secondarySanctionedAmount) {
		this.secondarySanctionedAmount = secondarySanctionedAmount;
	}
	public String getSecondaryDisbursedAmount() {
		return secondaryDisbursedAmount;
	}
	public void setSecondaryDisbursedAmount(String secondaryDisbursedAmount) {
		this.secondaryDisbursedAmount = secondaryDisbursedAmount;
	}
	@Override
	public String toString() {
		return "SecondaryAccountSummary [secondaryNumberOfAccounts="
				+ secondaryNumberOfAccounts
				+ ", secondaryActiveNumberOfAccounts="
				+ secondaryActiveNumberOfAccounts
				+ ", secondaryOverdueNumberOfAccounts="
				+ secondaryOverdueNumberOfAccounts
				+ ", secondarySecuredNumberOfAccounts="
				+ secondarySecuredNumberOfAccounts
				+ ", secondaryUnsecuredNumberOfAccounts="
				+ secondaryUnsecuredNumberOfAccounts
				+ ", secondaryUntaggedNumberOfAccounts="
				+ secondaryUntaggedNumberOfAccounts
				+ ", secondaryCurrentBalance=" + secondaryCurrentBalance
				+ ", secondarySanctionedAmount=" + secondarySanctionedAmount
				+ ", secondaryDisbursedAmount=" + secondaryDisbursedAmount
				+ ", SecCurrBal=" + SecCurrBal + ", SecSancAmt=" + SecSancAmt
				+ ", SecDisbAmt=" + SecDisbAmt + "]";
	}
	
}
