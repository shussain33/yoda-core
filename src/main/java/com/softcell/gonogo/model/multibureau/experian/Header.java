package com.softcell.gonogo.model.multibureau.experian;

public class Header {
	
	private String systemCode;
	private String messageText;
	private String reportDate;
	private String reportTime;
	
	public String getSystemCode() {
		return systemCode;
	}
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getReportTime() {
		return reportTime;
	}
	public void setReportTime(String reportTime) {
		this.reportTime = reportTime;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Header{");
		sb.append("systemCode='").append(systemCode).append('\'');
		sb.append(", messageText='").append(messageText).append('\'');
		sb.append(", reportDate='").append(reportDate).append('\'');
		sb.append(", reportTime='").append(reportTime).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Header header = (Header) o;

		if (systemCode != null ? !systemCode.equals(header.systemCode) : header.systemCode != null) return false;
		if (messageText != null ? !messageText.equals(header.messageText) : header.messageText != null) return false;
		if (reportDate != null ? !reportDate.equals(header.reportDate) : header.reportDate != null) return false;
		return reportTime != null ? reportTime.equals(header.reportTime) : header.reportTime == null;
	}

	@Override
	public int hashCode() {
		int result = systemCode != null ? systemCode.hashCode() : 0;
		result = 31 * result + (messageText != null ? messageText.hashCode() : 0);
		result = 31 * result + (reportDate != null ? reportDate.hashCode() : 0);
		result = 31 * result + (reportTime != null ? reportTime.hashCode() : 0);
		return result;
	}
}
