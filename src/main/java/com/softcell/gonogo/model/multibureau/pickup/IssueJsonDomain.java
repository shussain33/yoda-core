/**
 *
 */
package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import java.util.Arrays;


public class IssueJsonDomain {
    @JsonProperty("HEADER")
    private Header header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private Long acknowledgmentId;

    @JsonProperty("RESPONSE-FORMAT")
    private String[] responseType;


    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Long getAcknowledgmentId() {
        return acknowledgmentId;
    }

    public void setAcknowledgmentId(Long acknowledgmentId) {
        this.acknowledgmentId = acknowledgmentId;
    }

    public String[] getResponseType() {
        return responseType;
    }

    public void setResponseType(String[] responseType) {
        this.responseType = responseType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IssueJsonDomain{");
        sb.append("header=").append(header);
        sb.append(", acknowledgmentId=").append(acknowledgmentId);
        sb.append(", responseType=").append(Arrays.toString(responseType));
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IssueJsonDomain)) return false;
        IssueJsonDomain that = (IssueJsonDomain) o;
        return Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getAcknowledgmentId(), that.getAcknowledgmentId()) &&
                Objects.equal(getResponseType(), that.getResponseType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHeader(), getAcknowledgmentId(), getResponseType());
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private IssueJsonDomain issueJsonDomain =new IssueJsonDomain();

        public IssueJsonDomain build(){
            return this.issueJsonDomain;
        }

        public Builder header(Header header){
            this.issueJsonDomain.header= header;
            return this;
        }

        public Builder acknowledgmentId(Long acknowledgmentId){
            this.issueJsonDomain.acknowledgmentId=acknowledgmentId;
            return this;
        }

        public Builder responseType(String[] responseType){
            this.issueJsonDomain.responseType=responseType;
            return this;
        }
    }
}
