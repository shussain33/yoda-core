package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SECONDARY-MATCH")
@XmlAccessorType(XmlAccessType.FIELD)
public class SecondaryMatch {
	
	@XmlElement(name="NAME")
	private String name;
	@XmlElement(name="ADDRESSES")
	//private String address;
	private Addresses addresses;
	@XmlElement(name="DOB")
	private String dob;
	@XmlElement(name="PHONES")
	//private String phone;
	private Phones phones;
	//@XmlElement(name="NAME")
	private String pan;
	//@XmlElement(name="NAME")
	private String passport;
	//@XmlElement(name="NAME")
	private String drivingLicense;
//	@XmlElement(name="NAME")
	private String voterId;
//	@XmlElement(name="NAME")
	private String rationCard;
	@XmlElement(name="EMAILS")
	private String email;
	@XmlElement(name="LOAN-DETAILS")
	private LoanDetails loanDetails;
	@XmlElement(name="RELATIONS")
	private Relations relations;
	@XmlElement(name="IDS")
    private Ids ids;
	@XmlElement(name="EMAILS")
	private Emails emails;
	
	
	public Emails getEmails() {
		return emails;
	}
	public void setEmails(Emails emails) {
		this.emails = emails;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getPassport() {
		return passport;
	}
	public void setPassport(String passport) {
		this.passport = passport;
	}
	public String getDrivingLicense() {
		return drivingLicense;
	}
	public void setDrivingLicense(String drivingLicense) {
		this.drivingLicense = drivingLicense;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRationCard() {
		return rationCard;
	}
	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}
	public LoanDetails getLoanDetails() {
		return loanDetails;
	}
	public void setLoanDetails(LoanDetails loanDetails) {
		this.loanDetails = loanDetails;
	}
	public Addresses getAddresses() {
		return addresses;
	}
	public void setAddresses(Addresses addresses) {
		this.addresses = addresses;
	}
	public Phones getPhones() {
		return phones;
	}
	public void setPhones(Phones phones) {
		this.phones = phones;
	}
	public Relations getRelations() {
		return relations;
	}
	public void setRelations(Relations relations) {
		this.relations = relations;
	}
	public Ids getIds() {
		return ids;
	}
	public void setIds(Ids ids) {
		this.ids = ids;
	}
	@Override
	public String toString() {
		return "SecondaryMatch [name=" + name + ", addresses=" + addresses
				+ ", dob=" + dob + ", phones=" + phones + ", pan=" + pan
				+ ", passport=" + passport + ", drivingLicense="
				+ drivingLicense + ", voterId=" + voterId + ", rationCard="
				+ rationCard + ", email=" + email + ", loanDetails="
				+ loanDetails + ", relations=" + relations + ", ids=" + ids
				+ ", emails=" + emails + "]";
	}
}
