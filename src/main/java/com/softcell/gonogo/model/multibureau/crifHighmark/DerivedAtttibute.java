package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DERIVED-ATTRIBUTES")
@XmlAccessorType(XmlAccessType.FIELD)
public class DerivedAtttibute {

	/**
	 * @author Pruthvi Pendhota
	 *
	 *
	 */
	@XmlElement(name="DERIVED-ATTRIBUTE")
	private DerivedAtttibuteDetails derivedAtttibute;

	public DerivedAtttibuteDetails getDerivedAtttibute() {
		return derivedAtttibute;
	}

	public void setDerivedAtttibute(DerivedAtttibuteDetails derivedAtttibute) {
		this.derivedAtttibute = derivedAtttibute;
	}

	@Override
	public String toString() {
		return "DerivedAtttibute [derivedAtttibute=" + derivedAtttibute + "]";
	}
	
}
