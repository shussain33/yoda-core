package com.softcell.gonogo.model.multibureau.experian;

public class TotalCAPSSummary {
    private String totalCAPSLast7Days;
    private String totalCAPSLast30Days;
    private String totalCAPSLast90Days;
    private String totalCAPSLast180Days;

    public String getTotalCAPSLast7Days() {
        return totalCAPSLast7Days;
    }

    public void setTotalCAPSLast7Days(String totalCAPSLast7Days) {
        this.totalCAPSLast7Days = totalCAPSLast7Days;
    }

    public String getTotalCAPSLast30Days() {
        return totalCAPSLast30Days;
    }

    public void setTotalCAPSLast30Days(String totalCAPSLast30Days) {
        this.totalCAPSLast30Days = totalCAPSLast30Days;
    }

    public String getTotalCAPSLast90Days() {
        return totalCAPSLast90Days;
    }

    public void setTotalCAPSLast90Days(String totalCAPSLast90Days) {
        this.totalCAPSLast90Days = totalCAPSLast90Days;
    }

    public String getTotalCAPSLast180Days() {
        return totalCAPSLast180Days;
    }

    public void setTotalCAPSLast180Days(String totalCAPSLast180Days) {
        this.totalCAPSLast180Days = totalCAPSLast180Days;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TotalCAPSSummary{");
        sb.append("totalCAPSLast7Days='").append(totalCAPSLast7Days).append('\'');
        sb.append(", totalCAPSLast30Days='").append(totalCAPSLast30Days).append('\'');
        sb.append(", totalCAPSLast90Days='").append(totalCAPSLast90Days).append('\'');
        sb.append(", totalCAPSLast180Days='").append(totalCAPSLast180Days).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TotalCAPSSummary that = (TotalCAPSSummary) o;

        if (totalCAPSLast7Days != null ? !totalCAPSLast7Days.equals(that.totalCAPSLast7Days) : that.totalCAPSLast7Days != null)
            return false;
        if (totalCAPSLast30Days != null ? !totalCAPSLast30Days.equals(that.totalCAPSLast30Days) : that.totalCAPSLast30Days != null)
            return false;
        if (totalCAPSLast90Days != null ? !totalCAPSLast90Days.equals(that.totalCAPSLast90Days) : that.totalCAPSLast90Days != null)
            return false;
        return totalCAPSLast180Days != null ? totalCAPSLast180Days.equals(that.totalCAPSLast180Days) : that.totalCAPSLast180Days == null;
    }

    @Override
    public int hashCode() {
        int result = totalCAPSLast7Days != null ? totalCAPSLast7Days.hashCode() : 0;
        result = 31 * result + (totalCAPSLast30Days != null ? totalCAPSLast30Days.hashCode() : 0);
        result = 31 * result + (totalCAPSLast90Days != null ? totalCAPSLast90Days.hashCode() : 0);
        result = 31 * result + (totalCAPSLast180Days != null ? totalCAPSLast180Days.hashCode() : 0);
        return result;
    }
}
