package com.softcell.gonogo.model.multibureau.experian;

import java.util.List;

public class NonCreditCAPS {

    private NonCreditCAPSSummary nonCreditCAPSSummary;
    private CAPSApplicantDetails capsApplicantDetails;
    private List<CAPSApplicationDetails> capsApplicationDetails;
    private CAPSOtherDetails capsOtherDetails;
    private CAPSApplicantAddressDetails capsApplicantAddressDetails;
    private CAPSApplicantAdditionalAddressDetails capsApplicantAdditionalAddressDetails;


    public NonCreditCAPSSummary getNonCreditCAPSSummary() {
        return nonCreditCAPSSummary;
    }

    public void setNonCreditCAPSSummary(NonCreditCAPSSummary nonCreditCAPSSummary) {
        this.nonCreditCAPSSummary = nonCreditCAPSSummary;
    }

    public CAPSApplicantDetails getCapsApplicantDetails() {
        return capsApplicantDetails;
    }

    public void setCapsApplicantDetails(CAPSApplicantDetails capsApplicantDetails) {
        this.capsApplicantDetails = capsApplicantDetails;
    }

    public List<CAPSApplicationDetails> getCapsApplicationDetails() {
        return capsApplicationDetails;
    }

    public void setCapsApplicationDetails(
            List<CAPSApplicationDetails> capsApplicationDetails) {
        this.capsApplicationDetails = capsApplicationDetails;
    }

    public CAPSOtherDetails getCapsOtherDetails() {
        return capsOtherDetails;
    }

    public void setCapsOtherDetails(CAPSOtherDetails capsOtherDetails) {
        this.capsOtherDetails = capsOtherDetails;
    }

    public CAPSApplicantAddressDetails getCapsApplicantAddressDetails() {
        return capsApplicantAddressDetails;
    }

    public void setCapsApplicantAddressDetails(
            CAPSApplicantAddressDetails capsApplicantAddressDetails) {
        this.capsApplicantAddressDetails = capsApplicantAddressDetails;
    }

    public CAPSApplicantAdditionalAddressDetails getCapsApplicantAdditionalAddressDetails() {
        return capsApplicantAdditionalAddressDetails;
    }

    public void setCapsApplicantAdditionalAddressDetails(
            CAPSApplicantAdditionalAddressDetails capsApplicantAdditionalAddressDetails) {
        this.capsApplicantAdditionalAddressDetails = capsApplicantAdditionalAddressDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NonCreditCAPS{");
        sb.append("nonCreditCAPSSummary=").append(nonCreditCAPSSummary);
        sb.append(", capsApplicantDetails=").append(capsApplicantDetails);
        sb.append(", capsApplicationDetails=").append(capsApplicationDetails);
        sb.append(", capsOtherDetails=").append(capsOtherDetails);
        sb.append(", capsApplicantAddressDetails=").append(capsApplicantAddressDetails);
        sb.append(", capsApplicantAdditionalAddressDetails=").append(capsApplicantAdditionalAddressDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NonCreditCAPS that = (NonCreditCAPS) o;

        if (nonCreditCAPSSummary != null ? !nonCreditCAPSSummary.equals(that.nonCreditCAPSSummary) : that.nonCreditCAPSSummary != null)
            return false;
        if (capsApplicantDetails != null ? !capsApplicantDetails.equals(that.capsApplicantDetails) : that.capsApplicantDetails != null)
            return false;
        if (capsApplicationDetails != null ? !capsApplicationDetails.equals(that.capsApplicationDetails) : that.capsApplicationDetails != null)
            return false;
        if (capsOtherDetails != null ? !capsOtherDetails.equals(that.capsOtherDetails) : that.capsOtherDetails != null)
            return false;
        if (capsApplicantAddressDetails != null ? !capsApplicantAddressDetails.equals(that.capsApplicantAddressDetails) : that.capsApplicantAddressDetails != null)
            return false;
        return capsApplicantAdditionalAddressDetails != null ? capsApplicantAdditionalAddressDetails.equals(that.capsApplicantAdditionalAddressDetails) : that.capsApplicantAdditionalAddressDetails == null;
    }

    @Override
    public int hashCode() {
        int result = nonCreditCAPSSummary != null ? nonCreditCAPSSummary.hashCode() : 0;
        result = 31 * result + (capsApplicantDetails != null ? capsApplicantDetails.hashCode() : 0);
        result = 31 * result + (capsApplicationDetails != null ? capsApplicationDetails.hashCode() : 0);
        result = 31 * result + (capsOtherDetails != null ? capsOtherDetails.hashCode() : 0);
        result = 31 * result + (capsApplicantAddressDetails != null ? capsApplicantAddressDetails.hashCode() : 0);
        result = 31 * result + (capsApplicantAdditionalAddressDetails != null ? capsApplicantAdditionalAddressDetails.hashCode() : 0);
        return result;
    }
}
