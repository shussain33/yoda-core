package com.softcell.gonogo.model.multibureau.commercialCibil.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Office {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@JsonProperty("DUNS")
	private String duns;
	
	@JsonProperty("ADDRESS")
	private String address;
	

	public String getDuns() {
		return duns;
	}

	public void setDuns(String duns) {
		this.duns = duns;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Office [duns=" + duns + ", address=" + address + "]";
	}
}
