package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class Header {

    @JsonProperty("APPLICATION-ID")
    private String applicationID;

    @JsonProperty("CUST-ID")
    private String consumerID;

    @JsonProperty("REQUEST-TIME")
    private String requestDate;

    @JsonProperty("REQUEST-RECEIVED-TIME")
    private String dateOfRequest;

    @JsonProperty("REQUEST-TYPE")
    private String requestType;

    @JsonProperty("RESPONSE-TYPE")
    private String responseType;

    public static Builder builder() {
        return new Builder();
    }

    public String getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(String applicationID) {
        this.applicationID = applicationID;
    }

    public String getConsumerID() {
        return consumerID;
    }

    public void setConsumerID(String consumerID) {
        this.consumerID = consumerID;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getDateOfRequest() {
        return dateOfRequest;
    }

    public void setDateOfRequest(String dateOfRequest) {
        this.dateOfRequest = dateOfRequest;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Header [applicationID=");
        builder.append(applicationID);
        builder.append(", consumerID=");
        builder.append(consumerID);
        builder.append(", requestDate=");
        builder.append(requestDate);
        builder.append(", dateOfRequest=");
        builder.append(dateOfRequest);
        builder.append(", requestType=");
        builder.append(requestType);
        builder.append(", responseType=");
        builder.append(responseType);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((applicationID == null) ? 0 : applicationID.hashCode());
        result = prime * result
                + ((consumerID == null) ? 0 : consumerID.hashCode());
        result = prime * result
                + ((dateOfRequest == null) ? 0 : dateOfRequest.hashCode());
        result = prime * result
                + ((requestDate == null) ? 0 : requestDate.hashCode());
        result = prime * result
                + ((requestType == null) ? 0 : requestType.hashCode());
        result = prime * result
                + ((responseType == null) ? 0 : responseType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Header other = (Header) obj;
        if (applicationID == null) {
            if (other.applicationID != null)
                return false;
        } else if (!applicationID.equals(other.applicationID))
            return false;
        if (consumerID == null) {
            if (other.consumerID != null)
                return false;
        } else if (!consumerID.equals(other.consumerID))
            return false;
        if (dateOfRequest == null) {
            if (other.dateOfRequest != null)
                return false;
        } else if (!dateOfRequest.equals(other.dateOfRequest))
            return false;
        if (requestDate == null) {
            if (other.requestDate != null)
                return false;
        } else if (!requestDate.equals(other.requestDate))
            return false;
        if (requestType == null) {
            if (other.requestType != null)
                return false;
        } else if (!requestType.equals(other.requestType))
            return false;
        if (responseType == null) {
            if (other.responseType != null)
                return false;
        } else if (!responseType.equals(other.responseType))
            return false;
        return true;
    }

    public static class Builder {
        private Header header = new Header();

        public Header build() {
            return this.header;
        }

        public Builder applicationID(String applicationId) {
            this.header.applicationID = applicationId;
            return this;
        }

        public Builder consumerID(String consumerId) {
            this.header.consumerID = consumerId;
            return this;
        }

        public Builder requestDate(String requestDate) {
            this.header.requestDate = requestDate;
            return this;
        }

        public Builder dateOfRequest(String dateOfRequest) {
            this.header.dateOfRequest = dateOfRequest;
            return this;
        }

        public Builder requestType(String requestType) {
            this.header.requestType = requestType;
            return this;
        }

        public Builder responseType(String responseType) {
            this.header.responseType = responseType;
            return this;
        }

    }

}
