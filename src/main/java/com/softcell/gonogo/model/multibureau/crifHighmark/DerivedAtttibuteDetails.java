package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DERIVED-ATTRIBUTE")
@XmlAccessorType(XmlAccessType.FIELD)
public class DerivedAtttibuteDetails {

	/**
	 * @author Pruthvi Pendhota
	 *
	 *
	 */
	@XmlElement(name="Name")
	private String name;
	@XmlElement(name="Value")
	private String value;
	@XmlElement(name="Remark")
	private String remark;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "DerivedAtttibuteDetails [name=" + name + ", value=" + value
				+ ", remark=" + remark + "]";
	}
	
}
