package com.softcell.gonogo.model.multibureau.experian;

public class CAISHolderDetails {

    private String surnameNonNormalized;
    private String firstNameNonNormalized;
    private String middleName1NonNormalized;
    private String middleName2NonNormalized;
    private String middleName3NonNormalized;
    private String alias;
    private String genderCode;
    private String incomeTaxPan;
    private String passportNumber;
    private String voterIdNumber;
    private String dateOfBirth;

    public String getSurnameNonNormalized() {
        return surnameNonNormalized;
    }

    public void setSurnameNonNormalized(String surnameNonNormalized) {
        this.surnameNonNormalized = surnameNonNormalized;
    }

    public String getFirstNameNonNormalized() {
        return firstNameNonNormalized;
    }

    public void setFirstNameNonNormalized(String firstNameNonNormalized) {
        this.firstNameNonNormalized = firstNameNonNormalized;
    }

    public String getMiddleName1NonNormalized() {
        return middleName1NonNormalized;
    }

    public void setMiddleName1NonNormalized(String middleName1NonNormalized) {
        this.middleName1NonNormalized = middleName1NonNormalized;
    }

    public String getMiddleName2NonNormalized() {
        return middleName2NonNormalized;
    }

    public void setMiddleName2NonNormalized(String middleName2NonNormalized) {
        this.middleName2NonNormalized = middleName2NonNormalized;
    }

    public String getMiddleName3NonNormalized() {
        return middleName3NonNormalized;
    }

    public void setMiddleName3NonNormalized(String middleName3NonNormalized) {
        this.middleName3NonNormalized = middleName3NonNormalized;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public String getIncomeTaxPan() {
        return incomeTaxPan;
    }

    public void setIncomeTaxPan(String incomeTaxPan) {
        this.incomeTaxPan = incomeTaxPan;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getVoterIdNumber() {
        return voterIdNumber;
    }

    public void setVoterIdNumber(String voterIdNumber) {
        this.voterIdNumber = voterIdNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAISHolderDetails{");
        sb.append("surnameNonNormalized='").append(surnameNonNormalized).append('\'');
        sb.append(", firstNameNonNormalized='").append(firstNameNonNormalized).append('\'');
        sb.append(", middleName1NonNormalized='").append(middleName1NonNormalized).append('\'');
        sb.append(", middleName2NonNormalized='").append(middleName2NonNormalized).append('\'');
        sb.append(", middleName3NonNormalized='").append(middleName3NonNormalized).append('\'');
        sb.append(", alias='").append(alias).append('\'');
        sb.append(", genderCode='").append(genderCode).append('\'');
        sb.append(", incomeTaxPan='").append(incomeTaxPan).append('\'');
        sb.append(", passportNumber='").append(passportNumber).append('\'');
        sb.append(", voterIdNumber='").append(voterIdNumber).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAISHolderDetails that = (CAISHolderDetails) o;

        if (surnameNonNormalized != null ? !surnameNonNormalized.equals(that.surnameNonNormalized) : that.surnameNonNormalized != null)
            return false;
        if (firstNameNonNormalized != null ? !firstNameNonNormalized.equals(that.firstNameNonNormalized) : that.firstNameNonNormalized != null)
            return false;
        if (middleName1NonNormalized != null ? !middleName1NonNormalized.equals(that.middleName1NonNormalized) : that.middleName1NonNormalized != null)
            return false;
        if (middleName2NonNormalized != null ? !middleName2NonNormalized.equals(that.middleName2NonNormalized) : that.middleName2NonNormalized != null)
            return false;
        if (middleName3NonNormalized != null ? !middleName3NonNormalized.equals(that.middleName3NonNormalized) : that.middleName3NonNormalized != null)
            return false;
        if (alias != null ? !alias.equals(that.alias) : that.alias != null) return false;
        if (genderCode != null ? !genderCode.equals(that.genderCode) : that.genderCode != null) return false;
        if (incomeTaxPan != null ? !incomeTaxPan.equals(that.incomeTaxPan) : that.incomeTaxPan != null) return false;
        if (passportNumber != null ? !passportNumber.equals(that.passportNumber) : that.passportNumber != null)
            return false;
        if (voterIdNumber != null ? !voterIdNumber.equals(that.voterIdNumber) : that.voterIdNumber != null)
            return false;
        return dateOfBirth != null ? dateOfBirth.equals(that.dateOfBirth) : that.dateOfBirth == null;
    }

    @Override
    public int hashCode() {
        int result = surnameNonNormalized != null ? surnameNonNormalized.hashCode() : 0;
        result = 31 * result + (firstNameNonNormalized != null ? firstNameNonNormalized.hashCode() : 0);
        result = 31 * result + (middleName1NonNormalized != null ? middleName1NonNormalized.hashCode() : 0);
        result = 31 * result + (middleName2NonNormalized != null ? middleName2NonNormalized.hashCode() : 0);
        result = 31 * result + (middleName3NonNormalized != null ? middleName3NonNormalized.hashCode() : 0);
        result = 31 * result + (alias != null ? alias.hashCode() : 0);
        result = 31 * result + (genderCode != null ? genderCode.hashCode() : 0);
        result = 31 * result + (incomeTaxPan != null ? incomeTaxPan.hashCode() : 0);
        result = 31 * result + (passportNumber != null ? passportNumber.hashCode() : 0);
        result = 31 * result + (voterIdNumber != null ? voterIdNumber.hashCode() : 0);
        result = 31 * result + (dateOfBirth != null ? dateOfBirth.hashCode() : 0);
        return result;
    }
}
