package com.softcell.gonogo.model.multibureau.experian;

public class MFICA {
	
	private String TNOfNDelMFInACA;
	private String TNOfDelMFInACA;
	public String getTNOfNDelMFInACA() {
		return TNOfNDelMFInACA;
	}
	public void setTNOfNDelMFInACA(String tNOfNDelMFInACA) {
		TNOfNDelMFInACA = tNOfNDelMFInACA;
	}
	public String getTNOfDelMFInACA() {
		return TNOfDelMFInACA;
	}
	public void setTNOfDelMFInACA(String tNOfDelMFInACA) {
		TNOfDelMFInACA = tNOfDelMFInACA;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("MFICA{");
		sb.append("TNOfNDelMFInACA='").append(TNOfNDelMFInACA).append('\'');
		sb.append(", TNOfDelMFInACA='").append(TNOfDelMFInACA).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MFICA mfica = (MFICA) o;

		if (TNOfNDelMFInACA != null ? !TNOfNDelMFInACA.equals(mfica.TNOfNDelMFInACA) : mfica.TNOfNDelMFInACA != null)
			return false;
		return TNOfDelMFInACA != null ? TNOfDelMFInACA.equals(mfica.TNOfDelMFInACA) : mfica.TNOfDelMFInACA == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfNDelMFInACA != null ? TNOfNDelMFInACA.hashCode() : 0;
		result = 31 * result + (TNOfDelMFInACA != null ? TNOfDelMFInACA.hashCode() : 0);
		return result;
	}
}
