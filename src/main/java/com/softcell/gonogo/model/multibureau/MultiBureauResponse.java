package com.softcell.gonogo.model.multibureau;

public class MultiBureauResponse {

    private String bureauName;//type issue
    private String bureauStatus;
    private String bureauErrors;
    private String bureauScore;
    private String bureauRawResponse;
    private String bureauPdfUrl;

    public String getBureauName() {
        return bureauName;
    }

    public void setBureauName(String bureauName) {
        this.bureauName = bureauName;
    }

    public String getBureauStatus() {
        return bureauStatus;
    }

    public void setBureauStatus(String bureauStatus) {
        this.bureauStatus = bureauStatus;
    }

    public String getBureauErrors() {
        return bureauErrors;
    }

    public void setBureauErrors(String bureauErrors) {
        this.bureauErrors = bureauErrors;
    }

    public String getBureauScore() {
        return bureauScore;
    }

    public void setBureauScore(String bureauScore) {
        this.bureauScore = bureauScore;
    }

    public String getBureauRawResponse() {
        return bureauRawResponse;
    }

    public void setBureauRawResponse(String bureauRawResponse) {
        this.bureauRawResponse = bureauRawResponse;
    }

    public String getBureauPdfUrl() {
        return bureauPdfUrl;
    }

    public void setBureauPdfUrl(String bureauPdfUrl) {
        this.bureauPdfUrl = bureauPdfUrl;
    }

    @Override
    public String toString() {
        return "BureauResponse [bureauName=" + bureauName + ", bureauStatus="
                + bureauStatus + ", bureauErrors=" + bureauErrors
                + ", bureauScore=" + bureauScore + ", bureauRawResponse="
                + bureauRawResponse + ", bureauPdfUrl=" + bureauPdfUrl + "]";
    }
}
