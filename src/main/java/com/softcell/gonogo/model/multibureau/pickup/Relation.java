/**
 *
 */
package com.softcell.gonogo.model.multibureau.pickup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Relation {

    @JsonProperty("01")
    private String fatherName;

    @JsonProperty("02")
    private String spouseName;

    @JsonProperty("03")
    private String motherName;

    @JsonProperty("04")
    private String relationType1;

    @JsonProperty("05")
    private String relationType1Value;

    @JsonProperty("06")
    private String relationType2;

    @JsonProperty("07")
    private String relationType2Value;

    @JsonProperty("08")
    private String keyPersonName;

    @JsonProperty("09")
    private String keyPersonRelation;

    @JsonProperty("10")
    private String nomineeName;

    @JsonProperty("11")
    private String nomineeRelationType;

    public static Builder builder() {
        return new Builder();
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getRelationType1() {
        return relationType1;
    }

    public void setRelationType1(String relationType1) {
        this.relationType1 = relationType1;
    }

    public String getRelationType1Value() {
        return relationType1Value;
    }

    public void setRelationType1Value(String relationType1Value) {
        this.relationType1Value = relationType1Value;
    }

    public String getRelationType2() {
        return relationType2;
    }

    public void setRelationType2(String relationType2) {
        this.relationType2 = relationType2;
    }

    public String getRelationType2Value() {
        return relationType2Value;
    }

    public void setRelationType2Value(String relationType2Value) {
        this.relationType2Value = relationType2Value;
    }

    public String getKeyPersonName() {
        return keyPersonName;
    }

    public void setKeyPersonName(String keyPersonName) {
        this.keyPersonName = keyPersonName;
    }

    public String getKeyPersonRelation() {
        return keyPersonRelation;
    }

    public void setKeyPersonRelation(String keyPersonRelation) {
        this.keyPersonRelation = keyPersonRelation;
    }

    public String getNomineeName() {
        return nomineeName;
    }

    public void setNomineeName(String nomineeName) {
        this.nomineeName = nomineeName;
    }

    public String getNomineeRelationType() {
        return nomineeRelationType;
    }

    public void setNomineeRelationType(String nomineeRelationType) {
        this.nomineeRelationType = nomineeRelationType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Relation{");
        sb.append("fatherName='").append(fatherName).append('\'');
        sb.append(", spouseName='").append(spouseName).append('\'');
        sb.append(", motherName='").append(motherName).append('\'');
        sb.append(", relationType1='").append(relationType1).append('\'');
        sb.append(", relationType1Value='").append(relationType1Value).append('\'');
        sb.append(", relationType2='").append(relationType2).append('\'');
        sb.append(", relationType2Value='").append(relationType2Value).append('\'');
        sb.append(", keyPersonName='").append(keyPersonName).append('\'');
        sb.append(", keyPersonRelation='").append(keyPersonRelation).append('\'');
        sb.append(", nomineeName='").append(nomineeName).append('\'');
        sb.append(", nomineeRelationType='").append(nomineeRelationType).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Relation)) return false;
        Relation relation = (Relation) o;
        return Objects.equal(getFatherName(), relation.getFatherName()) &&
                Objects.equal(getSpouseName(), relation.getSpouseName()) &&
                Objects.equal(getMotherName(), relation.getMotherName()) &&
                Objects.equal(getRelationType1(), relation.getRelationType1()) &&
                Objects.equal(getRelationType1Value(), relation.getRelationType1Value()) &&
                Objects.equal(getRelationType2(), relation.getRelationType2()) &&
                Objects.equal(getRelationType2Value(), relation.getRelationType2Value()) &&
                Objects.equal(getKeyPersonName(), relation.getKeyPersonName()) &&
                Objects.equal(getKeyPersonRelation(), relation.getKeyPersonRelation()) &&
                Objects.equal(getNomineeName(), relation.getNomineeName()) &&
                Objects.equal(getNomineeRelationType(), relation.getNomineeRelationType());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getFatherName(), getSpouseName(), getMotherName(), getRelationType1(), getRelationType1Value(), getRelationType2(), getRelationType2Value(), getKeyPersonName(), getKeyPersonRelation(), getNomineeName(), getNomineeRelationType());
    }


    public static class Builder {

        private Relation relation = new Relation();

        public Relation build() {
            return this.relation;
        }

        public Builder fatherName(String fatherName) {
            this.relation.fatherName = fatherName;
            return this;
        }

        public Builder spouseName(String spouseName) {
            this.relation.spouseName = spouseName;
            return this;
        }

        public Builder motherName(String motherName) {
            this.relation.motherName = motherName;
            return this;
        }

        public Builder relationType1(String relationType1) {
            this.relation.relationType1 = relationType1;
            return this;
        }

        public Builder relationType1Value(String relationType1Value) {
            this.relation.relationType1Value = relationType1Value;
            return this;
        }

        public Builder relationType2(String relationType2) {
            this.relation.relationType2 = relationType2;
            return this;
        }

        public Builder relationType2Value(String relationType2Value) {
            this.relation.relationType2Value = relationType2Value;
            return this;
        }

        public Builder keyPersonName(String keyPersonName) {
            this.relation.keyPersonName = keyPersonName;
            return this;
        }

        public Builder keyPersonRelation(String keyPersonRelation) {
            this.relation.keyPersonRelation = keyPersonRelation;
            return this;
        }

        public Builder nomineeName(String nomineeName) {
            this.relation.nomineeName = nomineeName;
            return this;
        }

        private Builder nomineeRelationType(String nomineeRelationType) {
            this.relation.nomineeRelationType = nomineeRelationType;
            return this;
        }


    }


}
