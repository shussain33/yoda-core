package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="PRINTABLE-REPORT")
@XmlAccessorType(XmlAccessType.FIELD)
public class PrintableReport {

	@XmlElement(name="TYPE")
	private String type;
	@XmlElement(name="FILE-NAME")
	private String fileName;
	@XmlElement(name="CONTENT")
	private String content;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "PrintableReport [type=" + type + ", fileName=" + fileName
				+ ", content=" + content + "]";
	}
}
