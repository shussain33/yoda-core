package com.softcell.gonogo.model.multibureau.experian;

/**
 * @author stalwar
 */
public class CAISSummary {

    private ProveIdSummary proveIdSummary;
    private CreditAccount creditAccount;
    private TotalOutStandingBalance totalOutStandingBalance;


    public ProveIdSummary getProveIdSummary() {
        return proveIdSummary;
    }

    public void setProveIdSummary(ProveIdSummary proveIdSummary) {
        this.proveIdSummary = proveIdSummary;
    }

    public CreditAccount getCreditAccount() {
        return creditAccount;
    }

    public void setCreditAccount(CreditAccount creditAccount) {
        this.creditAccount = creditAccount;
    }

    public TotalOutStandingBalance getTotalOutStandingBalance() {
        return totalOutStandingBalance;
    }

    public void setTotalOutStandingBalance(
            TotalOutStandingBalance totalOutStandingBalance) {
        this.totalOutStandingBalance = totalOutStandingBalance;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAISSummary{");
        sb.append("proveIdSummary=").append(proveIdSummary);
        sb.append(", creditAccount=").append(creditAccount);
        sb.append(", totalOutStandingBalance=").append(totalOutStandingBalance);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAISSummary that = (CAISSummary) o;

        if (proveIdSummary != null ? !proveIdSummary.equals(that.proveIdSummary) : that.proveIdSummary != null)
            return false;
        if (creditAccount != null ? !creditAccount.equals(that.creditAccount) : that.creditAccount != null)
            return false;
        return totalOutStandingBalance != null ? totalOutStandingBalance.equals(that.totalOutStandingBalance) : that.totalOutStandingBalance == null;
    }

    @Override
    public int hashCode() {
        int result = proveIdSummary != null ? proveIdSummary.hashCode() : 0;
        result = 31 * result + (creditAccount != null ? creditAccount.hashCode() : 0);
        result = 31 * result + (totalOutStandingBalance != null ? totalOutStandingBalance.hashCode() : 0);
        return result;
    }
}
