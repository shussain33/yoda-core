package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DATE-OF-BIRTH-VARIATIONS")
@XmlAccessorType(XmlAccessType.FIELD)
public class DateOfBirthVariations {
	
	@XmlElement(name="VARIATION")
	private List<Variation> variations;

	public List<Variation> getVariations() {
		return variations;
	}

	public void setVariations(List<Variation> variations) {
		this.variations = variations;
	}

	@Override
	public String toString() {
		return "DateOfBirthVariations [variations=" + variations + "]";
	}

	
		

}
