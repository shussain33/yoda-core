package com.softcell.gonogo.model.multibureau.experian;

public class CurrentApplicantAddressDetails {

	private String flatNoPlotNoHouseNo;
	private String bldgNumberSocietyName;
	private String roadNumbernameAreaLocality;
	private String city;
	private String landmark;
	private String state;
	private String pinCode;
	private String countryCode;
	public String getFlatNoPlotNoHouseNo() {
		return flatNoPlotNoHouseNo;
	}
	public void setFlatNoPlotNoHouseNo(String flatNoPlotNoHouseNo) {
		this.flatNoPlotNoHouseNo = flatNoPlotNoHouseNo;
	}
	public String getBldgNumberSocietyName() {
		return bldgNumberSocietyName;
	}
	public void setBldgNumberSocietyName(String bldgNumberSocietyName) {
		this.bldgNumberSocietyName = bldgNumberSocietyName;
	}
	public String getRoadNumbernameAreaLocality() {
		return roadNumbernameAreaLocality;
	}
	public void setRoadNumbernameAreaLocality(String roadNumbernameAreaLocality) {
		this.roadNumbernameAreaLocality = roadNumbernameAreaLocality;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CurrentApplicantAddressDetails{");
		sb.append("flatNoPlotNoHouseNo='").append(flatNoPlotNoHouseNo).append('\'');
		sb.append(", bldgNumberSocietyName='").append(bldgNumberSocietyName).append('\'');
		sb.append(", roadNumbernameAreaLocality='").append(roadNumbernameAreaLocality).append('\'');
		sb.append(", city='").append(city).append('\'');
		sb.append(", landmark='").append(landmark).append('\'');
		sb.append(", state='").append(state).append('\'');
		sb.append(", pinCode='").append(pinCode).append('\'');
		sb.append(", countryCode='").append(countryCode).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CurrentApplicantAddressDetails that = (CurrentApplicantAddressDetails) o;

		if (flatNoPlotNoHouseNo != null ? !flatNoPlotNoHouseNo.equals(that.flatNoPlotNoHouseNo) : that.flatNoPlotNoHouseNo != null)
			return false;
		if (bldgNumberSocietyName != null ? !bldgNumberSocietyName.equals(that.bldgNumberSocietyName) : that.bldgNumberSocietyName != null)
			return false;
		if (roadNumbernameAreaLocality != null ? !roadNumbernameAreaLocality.equals(that.roadNumbernameAreaLocality) : that.roadNumbernameAreaLocality != null)
			return false;
		if (city != null ? !city.equals(that.city) : that.city != null) return false;
		if (landmark != null ? !landmark.equals(that.landmark) : that.landmark != null) return false;
		if (state != null ? !state.equals(that.state) : that.state != null) return false;
		if (pinCode != null ? !pinCode.equals(that.pinCode) : that.pinCode != null) return false;
		return countryCode != null ? countryCode.equals(that.countryCode) : that.countryCode == null;
	}

	@Override
	public int hashCode() {
		int result = flatNoPlotNoHouseNo != null ? flatNoPlotNoHouseNo.hashCode() : 0;
		result = 31 * result + (bldgNumberSocietyName != null ? bldgNumberSocietyName.hashCode() : 0);
		result = 31 * result + (roadNumbernameAreaLocality != null ? roadNumbernameAreaLocality.hashCode() : 0);
		result = 31 * result + (city != null ? city.hashCode() : 0);
		result = 31 * result + (landmark != null ? landmark.hashCode() : 0);
		result = 31 * result + (state != null ? state.hashCode() : 0);
		result = 31 * result + (pinCode != null ? pinCode.hashCode() : 0);
		result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
		return result;
	}
}
