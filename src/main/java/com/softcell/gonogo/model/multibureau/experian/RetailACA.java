package com.softcell.gonogo.model.multibureau.experian;

public class RetailACA {

	private String TNOfRetailACA;
	private String TotalBalRetailACA;
	private String WCDStRetailACA;
	private String WDSPr6MNTRetailACA;
	private String WDSPr712MNTRetailACA;
	private String AgeOfOldestRetail;
	private String HCBLmPerRevAccRet;
	private String TotCurBalLmPerRevAccRet;
	public String getTNOfRetailACA() {
		return TNOfRetailACA;
	}
	public void setTNOfRetailACA(String tNOfRetailACA) {
		TNOfRetailACA = tNOfRetailACA;
	}
	public String getTotalBalRetailACA() {
		return TotalBalRetailACA;
	}
	public void setTotalBalRetailACA(String totalBalRetailACA) {
		TotalBalRetailACA = totalBalRetailACA;
	}
	public String getWCDStRetailACA() {
		return WCDStRetailACA;
	}
	public void setWCDStRetailACA(String wCDStRetailACA) {
		WCDStRetailACA = wCDStRetailACA;
	}
	public String getWDSPr6MNTRetailACA() {
		return WDSPr6MNTRetailACA;
	}
	public void setWDSPr6MNTRetailACA(String wDSPr6MNTRetailACA) {
		WDSPr6MNTRetailACA = wDSPr6MNTRetailACA;
	}
	public String getWDSPr712MNTRetailACA() {
		return WDSPr712MNTRetailACA;
	}
	public void setWDSPr712MNTRetailACA(String wDSPr712MNTRetailACA) {
		WDSPr712MNTRetailACA = wDSPr712MNTRetailACA;
	}
	public String getAgeOfOldestRetail() {
		return AgeOfOldestRetail;
	}
	public void setAgeOfOldestRetail(String ageOfOldestRetail) {
		AgeOfOldestRetail = ageOfOldestRetail;
	}
	public String getHCBLmPerRevAccRet() {
		return HCBLmPerRevAccRet;
	}
	public void setHCBLmPerRevAccRet(String hCBLmPerRevAccRet) {
		HCBLmPerRevAccRet = hCBLmPerRevAccRet;
	}
	public String getTotCurBalLmPerRevAccRet() {
		return TotCurBalLmPerRevAccRet;
	}
	public void setTotCurBalLmPerRevAccRet(String totCurBalLmPerRevAccRet) {
		TotCurBalLmPerRevAccRet = totCurBalLmPerRevAccRet;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("RetailACA{");
		sb.append("TNOfRetailACA='").append(TNOfRetailACA).append('\'');
		sb.append(", TotalBalRetailACA='").append(TotalBalRetailACA).append('\'');
		sb.append(", WCDStRetailACA='").append(WCDStRetailACA).append('\'');
		sb.append(", WDSPr6MNTRetailACA='").append(WDSPr6MNTRetailACA).append('\'');
		sb.append(", WDSPr712MNTRetailACA='").append(WDSPr712MNTRetailACA).append('\'');
		sb.append(", AgeOfOldestRetail='").append(AgeOfOldestRetail).append('\'');
		sb.append(", HCBLmPerRevAccRet='").append(HCBLmPerRevAccRet).append('\'');
		sb.append(", TotCurBalLmPerRevAccRet='").append(TotCurBalLmPerRevAccRet).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		RetailACA retailACA = (RetailACA) o;

		if (TNOfRetailACA != null ? !TNOfRetailACA.equals(retailACA.TNOfRetailACA) : retailACA.TNOfRetailACA != null)
			return false;
		if (TotalBalRetailACA != null ? !TotalBalRetailACA.equals(retailACA.TotalBalRetailACA) : retailACA.TotalBalRetailACA != null)
			return false;
		if (WCDStRetailACA != null ? !WCDStRetailACA.equals(retailACA.WCDStRetailACA) : retailACA.WCDStRetailACA != null)
			return false;
		if (WDSPr6MNTRetailACA != null ? !WDSPr6MNTRetailACA.equals(retailACA.WDSPr6MNTRetailACA) : retailACA.WDSPr6MNTRetailACA != null)
			return false;
		if (WDSPr712MNTRetailACA != null ? !WDSPr712MNTRetailACA.equals(retailACA.WDSPr712MNTRetailACA) : retailACA.WDSPr712MNTRetailACA != null)
			return false;
		if (AgeOfOldestRetail != null ? !AgeOfOldestRetail.equals(retailACA.AgeOfOldestRetail) : retailACA.AgeOfOldestRetail != null)
			return false;
		if (HCBLmPerRevAccRet != null ? !HCBLmPerRevAccRet.equals(retailACA.HCBLmPerRevAccRet) : retailACA.HCBLmPerRevAccRet != null)
			return false;
		return TotCurBalLmPerRevAccRet != null ? TotCurBalLmPerRevAccRet.equals(retailACA.TotCurBalLmPerRevAccRet) : retailACA.TotCurBalLmPerRevAccRet == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfRetailACA != null ? TNOfRetailACA.hashCode() : 0;
		result = 31 * result + (TotalBalRetailACA != null ? TotalBalRetailACA.hashCode() : 0);
		result = 31 * result + (WCDStRetailACA != null ? WCDStRetailACA.hashCode() : 0);
		result = 31 * result + (WDSPr6MNTRetailACA != null ? WDSPr6MNTRetailACA.hashCode() : 0);
		result = 31 * result + (WDSPr712MNTRetailACA != null ? WDSPr712MNTRetailACA.hashCode() : 0);
		result = 31 * result + (AgeOfOldestRetail != null ? AgeOfOldestRetail.hashCode() : 0);
		result = 31 * result + (HCBLmPerRevAccRet != null ? HCBLmPerRevAccRet.hashCode() : 0);
		result = 31 * result + (TotCurBalLmPerRevAccRet != null ? TotCurBalLmPerRevAccRet.hashCode() : 0);
		return result;
	}
}
