package com.softcell.gonogo.model.multibureau.experian;

public class TotalACA {

    private String TNOfAllACA;
    private String BalAllACAExHL;
    private String WCDStAllACA;
    private String WDSPr6MNTAllACA;
    private String WDSPr712MNTAllACA;
    private String AgeOfOldestAllACA;

    public String getTNOfAllACA() {
        return TNOfAllACA;
    }

    public void setTNOfAllACA(String tNOfAllACA) {
        TNOfAllACA = tNOfAllACA;
    }

    public String getBalAllACAExHL() {
        return BalAllACAExHL;
    }

    public void setBalAllACAExHL(String balAllACAExHL) {
        BalAllACAExHL = balAllACAExHL;
    }

    public String getWCDStAllACA() {
        return WCDStAllACA;
    }

    public void setWCDStAllACA(String wCDStAllACA) {
        WCDStAllACA = wCDStAllACA;
    }

    public String getWDSPr6MNTAllACA() {
        return WDSPr6MNTAllACA;
    }

    public void setWDSPr6MNTAllACA(String wDSPr6MNTAllACA) {
        WDSPr6MNTAllACA = wDSPr6MNTAllACA;
    }

    public String getWDSPr712MNTAllACA() {
        return WDSPr712MNTAllACA;
    }

    public void setWDSPr712MNTAllACA(String wDSPr712MNTAllACA) {
        WDSPr712MNTAllACA = wDSPr712MNTAllACA;
    }

    public String getAgeOfOldestAllACA() {
        return AgeOfOldestAllACA;
    }

    public void setAgeOfOldestAllACA(String ageOfOldestAllACA) {
        AgeOfOldestAllACA = ageOfOldestAllACA;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TotalACA{");
        sb.append("TNOfAllACA='").append(TNOfAllACA).append('\'');
        sb.append(", BalAllACAExHL='").append(BalAllACAExHL).append('\'');
        sb.append(", WCDStAllACA='").append(WCDStAllACA).append('\'');
        sb.append(", WDSPr6MNTAllACA='").append(WDSPr6MNTAllACA).append('\'');
        sb.append(", WDSPr712MNTAllACA='").append(WDSPr712MNTAllACA).append('\'');
        sb.append(", AgeOfOldestAllACA='").append(AgeOfOldestAllACA).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TotalACA totalACA = (TotalACA) o;

        if (TNOfAllACA != null ? !TNOfAllACA.equals(totalACA.TNOfAllACA) : totalACA.TNOfAllACA != null) return false;
        if (BalAllACAExHL != null ? !BalAllACAExHL.equals(totalACA.BalAllACAExHL) : totalACA.BalAllACAExHL != null)
            return false;
        if (WCDStAllACA != null ? !WCDStAllACA.equals(totalACA.WCDStAllACA) : totalACA.WCDStAllACA != null)
            return false;
        if (WDSPr6MNTAllACA != null ? !WDSPr6MNTAllACA.equals(totalACA.WDSPr6MNTAllACA) : totalACA.WDSPr6MNTAllACA != null)
            return false;
        if (WDSPr712MNTAllACA != null ? !WDSPr712MNTAllACA.equals(totalACA.WDSPr712MNTAllACA) : totalACA.WDSPr712MNTAllACA != null)
            return false;
        return AgeOfOldestAllACA != null ? AgeOfOldestAllACA.equals(totalACA.AgeOfOldestAllACA) : totalACA.AgeOfOldestAllACA == null;
    }

    @Override
    public int hashCode() {
        int result = TNOfAllACA != null ? TNOfAllACA.hashCode() : 0;
        result = 31 * result + (BalAllACAExHL != null ? BalAllACAExHL.hashCode() : 0);
        result = 31 * result + (WCDStAllACA != null ? WCDStAllACA.hashCode() : 0);
        result = 31 * result + (WDSPr6MNTAllACA != null ? WDSPr6MNTAllACA.hashCode() : 0);
        result = 31 * result + (WDSPr712MNTAllACA != null ? WDSPr712MNTAllACA.hashCode() : 0);
        result = 31 * result + (AgeOfOldestAllACA != null ? AgeOfOldestAllACA.hashCode() : 0);
        return result;
    }
}
