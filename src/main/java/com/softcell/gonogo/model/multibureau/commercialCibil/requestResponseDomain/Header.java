package com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Header {

	/**
	 * @author Dipak
	 *
	 *
	 */
	@JsonProperty("APPLICATION-ID")
	private String applicationId;
	
	@JsonProperty("CUST-ID")
	private String customerId;
	
	@JsonProperty("REQUEST-TYPE")
	private String requestType;
	
	@JsonProperty("REQUEST-TIME")
	private String requestTime;
	
	@JsonProperty("REQUEST-RECEIVED-TIME")
	private String requestReceivedTime;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}

	public String getRequestReceivedTime() {
		return requestReceivedTime;
	}

	public void setRequestReceivedTime(String requestReceivedTime) {
		this.requestReceivedTime = requestReceivedTime;
	}
	
	
	/*//START ACE
	@JsonProperty("PRODUCT-TYPE")
	private String productType;
	
	@JsonProperty("PRODUCT-VERSION")
	private String productVersion ;
	
	@JsonProperty("REQUEST-MBR")
	private String requestMbr ;
	
	@JsonProperty("SUB-MBR-ID")
	private String subMbrId ;
	
	@JsonProperty("INQUIRY-DATE-TIME")
	private String inquiryDateTime ;
	
	@JsonProperty("REQUEST-VOLUME-TYPE")
	private String requestVolumeType;
	
	@JsonProperty("REQUEST-ACTION-TYPE")
	private String requestActionType;
	
	@JsonProperty("TEST-FLAG")
	private String testFlag;
	
	@JsonProperty("USERID")
	private String userId;
	
	@JsonProperty("PASSWORD")
	private String password;
	
	@JsonProperty("AUTH-FLAG")
	private String authFlag;
	
	@JsonProperty("AUTH-TITLE")
	private String authTitle;
	
	@JsonProperty("RESPONSE-FORMAT")
	private String responseFormat;
	
	@JsonProperty("MEMBER-PRE-OVERRIDE")
	private String memberPreOverride;
	
	@JsonProperty("RESPONSE-FORMAT-OVERRIDE")
	private String responseFormatOverride;
	
	@JsonProperty("RES-FRMT-EMBD")
	private String resFrmtEmbd;
	
	@JsonProperty("LOS-NAME")
	private String losName;
	
	@JsonProperty("LOS-VENDOR")
	private String losVendor;
	
	@JsonProperty("LOS-VERSION")
	private String losVersion;
	
	
	//END ACE

	@JsonProperty("SOURCE-SYSTEM")
	private String sourceSystem;
	
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}
	public String getRequestReceivedTime() {
		return requestReceivedTime;
	}
	public void setRequestReceivedTime(String requestReceivedTime) {
		this.requestReceivedTime = requestReceivedTime;
	}
	
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductVersion() {
		return productVersion;
	}
	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}
	public String getRequestMbr() {
		return requestMbr;
	}
	public void setRequestMbr(String requestMbr) {
		this.requestMbr = requestMbr;
	}
	public String getSubMbrId() {
		return subMbrId;
	}
	public void setSubMbrId(String subMbrId) {
		this.subMbrId = subMbrId;
	}
	public String getInquiryDateTime() {
		return inquiryDateTime;
	}
	public void setInquiryDateTime(String inquiryDateTime) {
		this.inquiryDateTime = inquiryDateTime;
	}
	public String getRequestVolumeType() {
		return requestVolumeType;
	}
	public void setRequestVolumeType(String requestVolumeType) {
		this.requestVolumeType = requestVolumeType;
	}
	public String getRequestActionType() {
		return requestActionType;
	}
	public void setRequestActionType(String requestActionType) {
		this.requestActionType = requestActionType;
	}
	public String getTestFlag() {
		return testFlag;
	}
	public void setTestFlag(String testFlag) {
		this.testFlag = testFlag;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAuthFlag() {
		return authFlag;
	}
	public void setAuthFlag(String authFlag) {
		this.authFlag = authFlag;
	}
	public String getAuthTitle() {
		return authTitle;
	}
	public void setAuthTitle(String authTitle) {
		this.authTitle = authTitle;
	}
	public String getResponseFormat() {
		return responseFormat;
	}
	public void setResponseFormat(String responseFormat) {
		this.responseFormat = responseFormat;
	}
	public String getMemberPreOverride() {
		return memberPreOverride;
	}
	public void setMemberPreOverride(String memberPreOverride) {
		this.memberPreOverride = memberPreOverride;
	}
	public String getResponseFormatOverride() {
		return responseFormatOverride;
	}
	public void setResponseFormatOverride(String responseFormatOverride) {
		this.responseFormatOverride = responseFormatOverride;
	}
	public String getResFrmtEmbd() {
		return resFrmtEmbd;
	}
	public void setResFrmtEmbd(String resFrmtEmbd) {
		this.resFrmtEmbd = resFrmtEmbd;
	}
	public String getLosName() {
		return losName;
	}
	public void setLosName(String losName) {
		this.losName = losName;
	}
	public String getLosVendor() {
		return losVendor;
	}
	public void setLosVendor(String losVendor) {
		this.losVendor = losVendor;
	}
	public String getLosVersion() {
		return losVersion;
	}
	public void setLosVersion(String losVersion) {
		this.losVersion = losVersion;
	}
	@Override
	public String toString() {
		return "Header [applicationId=" + applicationId + ", customerId="
				+ customerId + ", requestType=" + requestType
				+ ", requestTime=" + requestTime + ", requestReceivedTime="
				+ requestReceivedTime + ", productType=" + productType
				+ ", productVersion=" + productVersion + ", requestMbr="
				+ requestMbr + ", subMbrId=" + subMbrId + ", inquiryDateTime="
				+ inquiryDateTime + ", requestVolumeType=" + requestVolumeType
				+ ", requestActionType=" + requestActionType + ", testFlag="
				+ testFlag + ", userId=" + userId + ", password=" + password
				+ ", authFlag=" + authFlag + ", authTitle=" + authTitle
				+ ", responseFormat=" + responseFormat + ", memberPreOverride="
				+ memberPreOverride + ", responseFormatOverride="
				+ responseFormatOverride + ", resFrmtEmbd=" + resFrmtEmbd
				+ ", losName=" + losName + ", losVendor=" + losVendor
				+ ", losVersion=" + losVersion + ", sourceSystem="
				+ sourceSystem + "]";
	}*/
}
