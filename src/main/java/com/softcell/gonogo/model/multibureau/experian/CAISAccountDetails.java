package com.softcell.gonogo.model.multibureau.experian;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;


public class CAISAccountDetails {


    private String identificationNumber;
    private String subscriberName;
    private String accountNumber;
    private String portfolioType;
    private String accountType;
    private String openDate;
    private String highestCreditOrOrignalLoanAmount;
    private String termsDuration;
    private String termsFrequency;
    private String scheduledMonthlyPaymentAmount;
    private String accountStatus;
    private String paymentRating;
    private String paymentHistoryProfile;
    private String specialComment;
    private String currentBalance;
    private String amountPastDue;
    private String originalChargeOffAmount;
    private String dateReported;
    private String dateOfFirstDeleinquency;
    private String dateClosed;
    private String dateOfLastPayment;
    private String suitFiledWillfulDefaultWrittenOffStatus;
    private String suitFiledWilfulDefault;
    private String writtenOffSettledStatus;
    private String valueOfCreditsLastMonth;
    private String occupationCode;
    private String settelmentAmount;
    private String valueOfCollateral;
    private String typeOfCollateral;
    private String writtenOffAmountTotal;
    private String writtenOffAmountPrincipal;
    private String rateOfInterest;
    private String repaymentTenure;
    private String pramotionalRateFlag;
    private String income;
    private String incomeIndicator;
    private String incomeFrequencyIndicator;
    private String defaultStatusDate;
    private String litigationStatusDate;
    private String writeOffStatusDate;
    private String dateOfAddition;
    private String currencyCode;
    private String subscriberComments;
    private String consumerComments;
    @JsonProperty("accountHoldertypeCODE")
    private String accountHoldertypecode;
    private List<CAISAccountHistory> caisAccountHistoryList;
    private Map<String, Map<String, Map<String, String>>> historyMatrix;
    private List<AdvancedAccountHistory> advancedAccountHistoryList;
    private List<CAISHolderDetails> caisHolderDetailsList;
    private List<CAISHolderAddressDetails> caisHolderAddressDetailsList;
    private List<CAISHolderPhoneDetails> caisHolderPhoneDetailsList;
    private CAISHolderIdDetails caisHolderIdDetails;

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getSubscriberName() {
        return subscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPortfolioType() {
        return portfolioType;
    }

    public void setPortfolioType(String portfolioType) {
        this.portfolioType = portfolioType;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getHighestCreditOrOrignalLoanAmount() {
        return highestCreditOrOrignalLoanAmount;
    }

    public void setHighestCreditOrOrignalLoanAmount(
            String highestCreditOrOrignalLoanAmount) {
        this.highestCreditOrOrignalLoanAmount = highestCreditOrOrignalLoanAmount;
    }

    public String getTermsDuration() {
        return termsDuration;
    }

    public void setTermsDuration(String termsDuration) {
        this.termsDuration = termsDuration;
    }

    public String getTermsFrequency() {
        return termsFrequency;
    }

    public void setTermsFrequency(String termsFrequency) {
        this.termsFrequency = termsFrequency;
    }

    public String getScheduledMonthlyPaymentAmount() {
        return scheduledMonthlyPaymentAmount;
    }

    public void setScheduledMonthlyPaymentAmount(
            String scheduledMonthlyPaymentAmount) {
        this.scheduledMonthlyPaymentAmount = scheduledMonthlyPaymentAmount;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getPaymentRating() {
        return paymentRating;
    }

    public void setPaymentRating(String paymentRating) {
        this.paymentRating = paymentRating;
    }

    public String getPaymentHistoryProfile() {
        return paymentHistoryProfile;
    }

    public void setPaymentHistoryProfile(String paymentHistoryProfile) {
        this.paymentHistoryProfile = paymentHistoryProfile;
    }

    public String getSpecialComment() {
        return specialComment;
    }

    public void setSpecialComment(String specialComment) {
        this.specialComment = specialComment;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getAmountPastDue() {
        return amountPastDue;
    }

    public void setAmountPastDue(String amountPastDue) {
        this.amountPastDue = amountPastDue;
    }

    public String getOriginalChargeOffAmount() {
        return originalChargeOffAmount;
    }

    public void setOriginalChargeOffAmount(String originalChargeOffAmount) {
        this.originalChargeOffAmount = originalChargeOffAmount;
    }

    public String getDateReported() {
        return dateReported;
    }

    public void setDateReported(String dateReported) {
        this.dateReported = dateReported;
    }

    public String getDateOfFirstDeleinquency() {
        return dateOfFirstDeleinquency;
    }

    public void setDateOfFirstDeleinquency(String dateOfFirstDeleinquency) {
        this.dateOfFirstDeleinquency = dateOfFirstDeleinquency;
    }

    public String getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(String dateClosed) {
        this.dateClosed = dateClosed;
    }

    public String getDateOfLastPayment() {
        return dateOfLastPayment;
    }

    public void setDateOfLastPayment(String dateOfLastPayment) {
        this.dateOfLastPayment = dateOfLastPayment;
    }

    public String getSuitFiledWillfulDefaultWrittenOffStatus() {
        return suitFiledWillfulDefaultWrittenOffStatus;
    }

    public void setSuitFiledWillfulDefaultWrittenOffStatus(
            String suitFiledWillfulDefaultWrittenOffStatus) {
        this.suitFiledWillfulDefaultWrittenOffStatus = suitFiledWillfulDefaultWrittenOffStatus;
    }

    public String getSuitFiledWilfulDefault() {
        return suitFiledWilfulDefault;
    }

    public void setSuitFiledWilfulDefault(String suitFiledWilfulDefault) {
        this.suitFiledWilfulDefault = suitFiledWilfulDefault;
    }

    public String getWrittenOffSettledStatus() {
        return writtenOffSettledStatus;
    }

    public void setWrittenOffSettledStatus(String writtenOffSettledStatus) {
        this.writtenOffSettledStatus = writtenOffSettledStatus;
    }

    public String getValueOfCreditsLastMonth() {
        return valueOfCreditsLastMonth;
    }

    public void setValueOfCreditsLastMonth(String valueOfCreditsLastMonth) {
        this.valueOfCreditsLastMonth = valueOfCreditsLastMonth;
    }

    public String getOccupationCode() {
        return occupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        this.occupationCode = occupationCode;
    }

    public String getSettelmentAmount() {
        return settelmentAmount;
    }

    public void setSettelmentAmount(String settelmentAmount) {
        this.settelmentAmount = settelmentAmount;
    }

    public String getValueOfCollateral() {
        return valueOfCollateral;
    }

    public void setValueOfCollateral(String valueOfCollateral) {
        this.valueOfCollateral = valueOfCollateral;
    }

    public String getTypeOfCollateral() {
        return typeOfCollateral;
    }

    public void setTypeOfCollateral(String typeOfCollateral) {
        this.typeOfCollateral = typeOfCollateral;
    }

    public String getWrittenOffAmountTotal() {
        return writtenOffAmountTotal;
    }

    public void setWrittenOffAmountTotal(String writtenOffAmountTotal) {
        this.writtenOffAmountTotal = writtenOffAmountTotal;
    }

    public String getWrittenOffAmountPrincipal() {
        return writtenOffAmountPrincipal;
    }

    public void setWrittenOffAmountPrincipal(String writtenOffAmountPrincipal) {
        this.writtenOffAmountPrincipal = writtenOffAmountPrincipal;
    }

    public String getRateOfInterest() {
        return rateOfInterest;
    }

    public void setRateOfInterest(String rateOfInterest) {
        this.rateOfInterest = rateOfInterest;
    }

    public String getRepaymentTenure() {
        return repaymentTenure;
    }

    public void setRepaymentTenure(String repaymentTenure) {
        this.repaymentTenure = repaymentTenure;
    }

    public String getPramotionalRateFlag() {
        return pramotionalRateFlag;
    }

    public void setPramotionalRateFlag(String pramotionalRateFlag) {
        this.pramotionalRateFlag = pramotionalRateFlag;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getIncomeIndicator() {
        return incomeIndicator;
    }

    public void setIncomeIndicator(String incomeIndicator) {
        this.incomeIndicator = incomeIndicator;
    }

    public String getIncomeFrequencyIndicator() {
        return incomeFrequencyIndicator;
    }

    public void setIncomeFrequencyIndicator(String incomeFrequencyIndicator) {
        this.incomeFrequencyIndicator = incomeFrequencyIndicator;
    }

    public String getDefaultStatusDate() {
        return defaultStatusDate;
    }

    public void setDefaultStatusDate(String defaultStatusDate) {
        this.defaultStatusDate = defaultStatusDate;
    }

    public String getLitigationStatusDate() {
        return litigationStatusDate;
    }

    public void setLitigationStatusDate(String litigationStatusDate) {
        this.litigationStatusDate = litigationStatusDate;
    }

    public String getWriteOffStatusDate() {
        return writeOffStatusDate;
    }

    public void setWriteOffStatusDate(String writeOffStatusDate) {
        this.writeOffStatusDate = writeOffStatusDate;
    }

    public String getDateOfAddition() {
        return dateOfAddition;
    }

    public void setDateOfAddition(String dateOfAddition) {
        this.dateOfAddition = dateOfAddition;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getSubscriberComments() {
        return subscriberComments;
    }

    public void setSubscriberComments(String subscriberComments) {
        this.subscriberComments = subscriberComments;
    }

    public String getConsumerComments() {
        return consumerComments;
    }

    public void setConsumerComments(String consumerComments) {
        this.consumerComments = consumerComments;
    }

    public String getAccountHoldertypecode() {
        return accountHoldertypecode;
    }

    public void setAccountHoldertypecode(String accountHoldertypecode) {
        this.accountHoldertypecode = accountHoldertypecode;
    }

    public List<CAISAccountHistory> getCaisAccountHistoryList() {
        return caisAccountHistoryList;
    }

    public void setCaisAccountHistoryList(
            List<CAISAccountHistory> caisAccountHistoryList) {
        this.caisAccountHistoryList = caisAccountHistoryList;
    }

    public List<AdvancedAccountHistory> getAdvancedAccountHistoryList() {
        return advancedAccountHistoryList;
    }

    public void setAdvancedAccountHistoryList(
            List<AdvancedAccountHistory> advancedAccountHistoryList) {
        this.advancedAccountHistoryList = advancedAccountHistoryList;
    }

    public List<CAISHolderDetails> getCaisHolderDetailsList() {
        return caisHolderDetailsList;
    }

    public void setCaisHolderDetailsList(
            List<CAISHolderDetails> caisHolderDetailsList) {
        this.caisHolderDetailsList = caisHolderDetailsList;
    }

    public List<CAISHolderAddressDetails> getCaisHolderAddressDetailsList() {
        return caisHolderAddressDetailsList;
    }

    public void setCaisHolderAddressDetailsList(
            List<CAISHolderAddressDetails> caisHolderAddressDetailsList) {
        this.caisHolderAddressDetailsList = caisHolderAddressDetailsList;
    }

    public List<CAISHolderPhoneDetails> getCaisHolderPhoneDetailsList() {
        return caisHolderPhoneDetailsList;
    }

    public void setCaisHolderPhoneDetailsList(
            List<CAISHolderPhoneDetails> caisHolderPhoneDetailsList) {
        this.caisHolderPhoneDetailsList = caisHolderPhoneDetailsList;
    }

    public CAISHolderIdDetails getCaisHolderIdDetails() {
        return caisHolderIdDetails;
    }

    public void setCaisHolderIdDetails(CAISHolderIdDetails caisHolderIdDetails) {
        this.caisHolderIdDetails = caisHolderIdDetails;
    }

    public Map<String, Map<String, Map<String, String>>> getHistoryMatrix() {
        return historyMatrix;
    }

    public void setHistoryMatrix(
            Map<String, Map<String, Map<String, String>>> historyMatrix) {
        this.historyMatrix = historyMatrix;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CAISAccountDetails{");
        sb.append("identificationNumber='").append(identificationNumber).append('\'');
        sb.append(", subscriberName='").append(subscriberName).append('\'');
        sb.append(", accountNumber='").append(accountNumber).append('\'');
        sb.append(", portfolioType='").append(portfolioType).append('\'');
        sb.append(", accountType='").append(accountType).append('\'');
        sb.append(", openDate='").append(openDate).append('\'');
        sb.append(", highestCreditOrOrignalLoanAmount='").append(highestCreditOrOrignalLoanAmount).append('\'');
        sb.append(", termsDuration='").append(termsDuration).append('\'');
        sb.append(", termsFrequency='").append(termsFrequency).append('\'');
        sb.append(", scheduledMonthlyPaymentAmount='").append(scheduledMonthlyPaymentAmount).append('\'');
        sb.append(", accountStatus='").append(accountStatus).append('\'');
        sb.append(", paymentRating='").append(paymentRating).append('\'');
        sb.append(", paymentHistoryProfile='").append(paymentHistoryProfile).append('\'');
        sb.append(", specialComment='").append(specialComment).append('\'');
        sb.append(", currentBalance='").append(currentBalance).append('\'');
        sb.append(", amountPastDue='").append(amountPastDue).append('\'');
        sb.append(", originalChargeOffAmount='").append(originalChargeOffAmount).append('\'');
        sb.append(", dateReported='").append(dateReported).append('\'');
        sb.append(", dateOfFirstDeleinquency='").append(dateOfFirstDeleinquency).append('\'');
        sb.append(", dateClosed='").append(dateClosed).append('\'');
        sb.append(", dateOfLastPayment='").append(dateOfLastPayment).append('\'');
        sb.append(", suitFiledWillfulDefaultWrittenOffStatus='").append(suitFiledWillfulDefaultWrittenOffStatus).append('\'');
        sb.append(", suitFiledWilfulDefault='").append(suitFiledWilfulDefault).append('\'');
        sb.append(", writtenOffSettledStatus='").append(writtenOffSettledStatus).append('\'');
        sb.append(", valueOfCreditsLastMonth='").append(valueOfCreditsLastMonth).append('\'');
        sb.append(", occupationCode='").append(occupationCode).append('\'');
        sb.append(", settelmentAmount='").append(settelmentAmount).append('\'');
        sb.append(", valueOfCollateral='").append(valueOfCollateral).append('\'');
        sb.append(", typeOfCollateral='").append(typeOfCollateral).append('\'');
        sb.append(", writtenOffAmountTotal='").append(writtenOffAmountTotal).append('\'');
        sb.append(", writtenOffAmountPrincipal='").append(writtenOffAmountPrincipal).append('\'');
        sb.append(", rateOfInterest='").append(rateOfInterest).append('\'');
        sb.append(", repaymentTenure='").append(repaymentTenure).append('\'');
        sb.append(", pramotionalRateFlag='").append(pramotionalRateFlag).append('\'');
        sb.append(", income='").append(income).append('\'');
        sb.append(", incomeIndicator='").append(incomeIndicator).append('\'');
        sb.append(", incomeFrequencyIndicator='").append(incomeFrequencyIndicator).append('\'');
        sb.append(", defaultStatusDate='").append(defaultStatusDate).append('\'');
        sb.append(", litigationStatusDate='").append(litigationStatusDate).append('\'');
        sb.append(", writeOffStatusDate='").append(writeOffStatusDate).append('\'');
        sb.append(", dateOfAddition='").append(dateOfAddition).append('\'');
        sb.append(", currencyCode='").append(currencyCode).append('\'');
        sb.append(", subscriberComments='").append(subscriberComments).append('\'');
        sb.append(", consumerComments='").append(consumerComments).append('\'');
        sb.append(", accountHoldertypecode='").append(accountHoldertypecode).append('\'');
        sb.append(", caisAccountHistoryList=").append(caisAccountHistoryList);
        sb.append(", historyMatrix=").append(historyMatrix);
        sb.append(", advancedAccountHistoryList=").append(advancedAccountHistoryList);
        sb.append(", caisHolderDetailsList=").append(caisHolderDetailsList);
        sb.append(", caisHolderAddressDetailsList=").append(caisHolderAddressDetailsList);
        sb.append(", caisHolderPhoneDetailsList=").append(caisHolderPhoneDetailsList);
        sb.append(", caisHolderIdDetails=").append(caisHolderIdDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CAISAccountDetails that = (CAISAccountDetails) o;

        if (identificationNumber != null ? !identificationNumber.equals(that.identificationNumber) : that.identificationNumber != null)
            return false;
        if (subscriberName != null ? !subscriberName.equals(that.subscriberName) : that.subscriberName != null)
            return false;
        if (accountNumber != null ? !accountNumber.equals(that.accountNumber) : that.accountNumber != null)
            return false;
        if (portfolioType != null ? !portfolioType.equals(that.portfolioType) : that.portfolioType != null)
            return false;
        if (accountType != null ? !accountType.equals(that.accountType) : that.accountType != null) return false;
        if (openDate != null ? !openDate.equals(that.openDate) : that.openDate != null) return false;
        if (highestCreditOrOrignalLoanAmount != null ? !highestCreditOrOrignalLoanAmount.equals(that.highestCreditOrOrignalLoanAmount) : that.highestCreditOrOrignalLoanAmount != null)
            return false;
        if (termsDuration != null ? !termsDuration.equals(that.termsDuration) : that.termsDuration != null)
            return false;
        if (termsFrequency != null ? !termsFrequency.equals(that.termsFrequency) : that.termsFrequency != null)
            return false;
        if (scheduledMonthlyPaymentAmount != null ? !scheduledMonthlyPaymentAmount.equals(that.scheduledMonthlyPaymentAmount) : that.scheduledMonthlyPaymentAmount != null)
            return false;
        if (accountStatus != null ? !accountStatus.equals(that.accountStatus) : that.accountStatus != null)
            return false;
        if (paymentRating != null ? !paymentRating.equals(that.paymentRating) : that.paymentRating != null)
            return false;
        if (paymentHistoryProfile != null ? !paymentHistoryProfile.equals(that.paymentHistoryProfile) : that.paymentHistoryProfile != null)
            return false;
        if (specialComment != null ? !specialComment.equals(that.specialComment) : that.specialComment != null)
            return false;
        if (currentBalance != null ? !currentBalance.equals(that.currentBalance) : that.currentBalance != null)
            return false;
        if (amountPastDue != null ? !amountPastDue.equals(that.amountPastDue) : that.amountPastDue != null)
            return false;
        if (originalChargeOffAmount != null ? !originalChargeOffAmount.equals(that.originalChargeOffAmount) : that.originalChargeOffAmount != null)
            return false;
        if (dateReported != null ? !dateReported.equals(that.dateReported) : that.dateReported != null) return false;
        if (dateOfFirstDeleinquency != null ? !dateOfFirstDeleinquency.equals(that.dateOfFirstDeleinquency) : that.dateOfFirstDeleinquency != null)
            return false;
        if (dateClosed != null ? !dateClosed.equals(that.dateClosed) : that.dateClosed != null) return false;
        if (dateOfLastPayment != null ? !dateOfLastPayment.equals(that.dateOfLastPayment) : that.dateOfLastPayment != null)
            return false;
        if (suitFiledWillfulDefaultWrittenOffStatus != null ? !suitFiledWillfulDefaultWrittenOffStatus.equals(that.suitFiledWillfulDefaultWrittenOffStatus) : that.suitFiledWillfulDefaultWrittenOffStatus != null)
            return false;
        if (suitFiledWilfulDefault != null ? !suitFiledWilfulDefault.equals(that.suitFiledWilfulDefault) : that.suitFiledWilfulDefault != null)
            return false;
        if (writtenOffSettledStatus != null ? !writtenOffSettledStatus.equals(that.writtenOffSettledStatus) : that.writtenOffSettledStatus != null)
            return false;
        if (valueOfCreditsLastMonth != null ? !valueOfCreditsLastMonth.equals(that.valueOfCreditsLastMonth) : that.valueOfCreditsLastMonth != null)
            return false;
        if (occupationCode != null ? !occupationCode.equals(that.occupationCode) : that.occupationCode != null)
            return false;
        if (settelmentAmount != null ? !settelmentAmount.equals(that.settelmentAmount) : that.settelmentAmount != null)
            return false;
        if (valueOfCollateral != null ? !valueOfCollateral.equals(that.valueOfCollateral) : that.valueOfCollateral != null)
            return false;
        if (typeOfCollateral != null ? !typeOfCollateral.equals(that.typeOfCollateral) : that.typeOfCollateral != null)
            return false;
        if (writtenOffAmountTotal != null ? !writtenOffAmountTotal.equals(that.writtenOffAmountTotal) : that.writtenOffAmountTotal != null)
            return false;
        if (writtenOffAmountPrincipal != null ? !writtenOffAmountPrincipal.equals(that.writtenOffAmountPrincipal) : that.writtenOffAmountPrincipal != null)
            return false;
        if (rateOfInterest != null ? !rateOfInterest.equals(that.rateOfInterest) : that.rateOfInterest != null)
            return false;
        if (repaymentTenure != null ? !repaymentTenure.equals(that.repaymentTenure) : that.repaymentTenure != null)
            return false;
        if (pramotionalRateFlag != null ? !pramotionalRateFlag.equals(that.pramotionalRateFlag) : that.pramotionalRateFlag != null)
            return false;
        if (income != null ? !income.equals(that.income) : that.income != null) return false;
        if (incomeIndicator != null ? !incomeIndicator.equals(that.incomeIndicator) : that.incomeIndicator != null)
            return false;
        if (incomeFrequencyIndicator != null ? !incomeFrequencyIndicator.equals(that.incomeFrequencyIndicator) : that.incomeFrequencyIndicator != null)
            return false;
        if (defaultStatusDate != null ? !defaultStatusDate.equals(that.defaultStatusDate) : that.defaultStatusDate != null)
            return false;
        if (litigationStatusDate != null ? !litigationStatusDate.equals(that.litigationStatusDate) : that.litigationStatusDate != null)
            return false;
        if (writeOffStatusDate != null ? !writeOffStatusDate.equals(that.writeOffStatusDate) : that.writeOffStatusDate != null)
            return false;
        if (dateOfAddition != null ? !dateOfAddition.equals(that.dateOfAddition) : that.dateOfAddition != null)
            return false;
        if (currencyCode != null ? !currencyCode.equals(that.currencyCode) : that.currencyCode != null) return false;
        if (subscriberComments != null ? !subscriberComments.equals(that.subscriberComments) : that.subscriberComments != null)
            return false;
        if (consumerComments != null ? !consumerComments.equals(that.consumerComments) : that.consumerComments != null)
            return false;
        if (accountHoldertypecode != null ? !accountHoldertypecode.equals(that.accountHoldertypecode) : that.accountHoldertypecode != null)
            return false;
        if (caisAccountHistoryList != null ? !caisAccountHistoryList.equals(that.caisAccountHistoryList) : that.caisAccountHistoryList != null)
            return false;
        if (historyMatrix != null ? !historyMatrix.equals(that.historyMatrix) : that.historyMatrix != null)
            return false;
        if (advancedAccountHistoryList != null ? !advancedAccountHistoryList.equals(that.advancedAccountHistoryList) : that.advancedAccountHistoryList != null)
            return false;
        if (caisHolderDetailsList != null ? !caisHolderDetailsList.equals(that.caisHolderDetailsList) : that.caisHolderDetailsList != null)
            return false;
        if (caisHolderAddressDetailsList != null ? !caisHolderAddressDetailsList.equals(that.caisHolderAddressDetailsList) : that.caisHolderAddressDetailsList != null)
            return false;
        if (caisHolderPhoneDetailsList != null ? !caisHolderPhoneDetailsList.equals(that.caisHolderPhoneDetailsList) : that.caisHolderPhoneDetailsList != null)
            return false;
        return caisHolderIdDetails != null ? caisHolderIdDetails.equals(that.caisHolderIdDetails) : that.caisHolderIdDetails == null;
    }

    @Override
    public int hashCode() {
        int result = identificationNumber != null ? identificationNumber.hashCode() : 0;
        result = 31 * result + (subscriberName != null ? subscriberName.hashCode() : 0);
        result = 31 * result + (accountNumber != null ? accountNumber.hashCode() : 0);
        result = 31 * result + (portfolioType != null ? portfolioType.hashCode() : 0);
        result = 31 * result + (accountType != null ? accountType.hashCode() : 0);
        result = 31 * result + (openDate != null ? openDate.hashCode() : 0);
        result = 31 * result + (highestCreditOrOrignalLoanAmount != null ? highestCreditOrOrignalLoanAmount.hashCode() : 0);
        result = 31 * result + (termsDuration != null ? termsDuration.hashCode() : 0);
        result = 31 * result + (termsFrequency != null ? termsFrequency.hashCode() : 0);
        result = 31 * result + (scheduledMonthlyPaymentAmount != null ? scheduledMonthlyPaymentAmount.hashCode() : 0);
        result = 31 * result + (accountStatus != null ? accountStatus.hashCode() : 0);
        result = 31 * result + (paymentRating != null ? paymentRating.hashCode() : 0);
        result = 31 * result + (paymentHistoryProfile != null ? paymentHistoryProfile.hashCode() : 0);
        result = 31 * result + (specialComment != null ? specialComment.hashCode() : 0);
        result = 31 * result + (currentBalance != null ? currentBalance.hashCode() : 0);
        result = 31 * result + (amountPastDue != null ? amountPastDue.hashCode() : 0);
        result = 31 * result + (originalChargeOffAmount != null ? originalChargeOffAmount.hashCode() : 0);
        result = 31 * result + (dateReported != null ? dateReported.hashCode() : 0);
        result = 31 * result + (dateOfFirstDeleinquency != null ? dateOfFirstDeleinquency.hashCode() : 0);
        result = 31 * result + (dateClosed != null ? dateClosed.hashCode() : 0);
        result = 31 * result + (dateOfLastPayment != null ? dateOfLastPayment.hashCode() : 0);
        result = 31 * result + (suitFiledWillfulDefaultWrittenOffStatus != null ? suitFiledWillfulDefaultWrittenOffStatus.hashCode() : 0);
        result = 31 * result + (suitFiledWilfulDefault != null ? suitFiledWilfulDefault.hashCode() : 0);
        result = 31 * result + (writtenOffSettledStatus != null ? writtenOffSettledStatus.hashCode() : 0);
        result = 31 * result + (valueOfCreditsLastMonth != null ? valueOfCreditsLastMonth.hashCode() : 0);
        result = 31 * result + (occupationCode != null ? occupationCode.hashCode() : 0);
        result = 31 * result + (settelmentAmount != null ? settelmentAmount.hashCode() : 0);
        result = 31 * result + (valueOfCollateral != null ? valueOfCollateral.hashCode() : 0);
        result = 31 * result + (typeOfCollateral != null ? typeOfCollateral.hashCode() : 0);
        result = 31 * result + (writtenOffAmountTotal != null ? writtenOffAmountTotal.hashCode() : 0);
        result = 31 * result + (writtenOffAmountPrincipal != null ? writtenOffAmountPrincipal.hashCode() : 0);
        result = 31 * result + (rateOfInterest != null ? rateOfInterest.hashCode() : 0);
        result = 31 * result + (repaymentTenure != null ? repaymentTenure.hashCode() : 0);
        result = 31 * result + (pramotionalRateFlag != null ? pramotionalRateFlag.hashCode() : 0);
        result = 31 * result + (income != null ? income.hashCode() : 0);
        result = 31 * result + (incomeIndicator != null ? incomeIndicator.hashCode() : 0);
        result = 31 * result + (incomeFrequencyIndicator != null ? incomeFrequencyIndicator.hashCode() : 0);
        result = 31 * result + (defaultStatusDate != null ? defaultStatusDate.hashCode() : 0);
        result = 31 * result + (litigationStatusDate != null ? litigationStatusDate.hashCode() : 0);
        result = 31 * result + (writeOffStatusDate != null ? writeOffStatusDate.hashCode() : 0);
        result = 31 * result + (dateOfAddition != null ? dateOfAddition.hashCode() : 0);
        result = 31 * result + (currencyCode != null ? currencyCode.hashCode() : 0);
        result = 31 * result + (subscriberComments != null ? subscriberComments.hashCode() : 0);
        result = 31 * result + (consumerComments != null ? consumerComments.hashCode() : 0);
        result = 31 * result + (accountHoldertypecode != null ? accountHoldertypecode.hashCode() : 0);
        result = 31 * result + (caisAccountHistoryList != null ? caisAccountHistoryList.hashCode() : 0);
        result = 31 * result + (historyMatrix != null ? historyMatrix.hashCode() : 0);
        result = 31 * result + (advancedAccountHistoryList != null ? advancedAccountHistoryList.hashCode() : 0);
        result = 31 * result + (caisHolderDetailsList != null ? caisHolderDetailsList.hashCode() : 0);
        result = 31 * result + (caisHolderAddressDetailsList != null ? caisHolderAddressDetailsList.hashCode() : 0);
        result = 31 * result + (caisHolderPhoneDetailsList != null ? caisHolderPhoneDetailsList.hashCode() : 0);
        result = 31 * result + (caisHolderIdDetails != null ? caisHolderIdDetails.hashCode() : 0);
        return result;
    }
}
