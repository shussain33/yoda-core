package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="HISTORY")
@XmlAccessorType(XmlAccessType.FIELD)
public class History {
	
	@XmlElement(name="MEMBER-NAME")
	private String  memberName;
	@XmlElement(name="INQUIRY-DATE")
	private String 	inquiryDate;
	@XmlElement(name="PURPOSE")
	private String 	purpose;
	@XmlElement(name="OWNERSHIP-TYPE")
	private String 	ownershipType;
	@XmlElement(name="AMOUNT")
	private String 	amount;
	@XmlElement(name="REMARK")
	private String 	remark;
	
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getInquiryDate() {
		return inquiryDate;
	}
	public void setInquiryDate(String inquiryDate) {
		this.inquiryDate = inquiryDate;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getOwnershipType() {
		return ownershipType;
	}
	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "History [memberName=" + memberName + ", inquiryDate="
				+ inquiryDate + ", purpose=" + purpose + ", ownershipType="
				+ ownershipType + ", amount=" + amount + ", remark=" + remark
				+ "]";
	}
}
