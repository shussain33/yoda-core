package com.softcell.gonogo.model.multibureau.commercialCibil.pickup;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CommercialSearch {

	/**
	 * @author Dipak
	 *
	 *
	 */
	
	@JsonProperty("BORROWER-NAME")
	private String borrowerName;
	
	@JsonProperty("BUREAU-ID")
	private String bureauId;
	
	@JsonProperty("REFERENCE-ID")
	private String bureauRefId;
	
	@JsonProperty("REGISTERED-OFFICE")
	private Office registeredOffice;
	
	@JsonProperty("BRANCH-OFFICE")
	private List<Office> branchOffice;
	
	@JsonProperty("NO-HIT")
	private String noHit;
	
	@JsonProperty("ERROR-LIST")
	private List<String> errorList;

	
	
	public String getBorrowerName() {
		return borrowerName;
	}

	public void setBorrowerName(String borrowerName) {
		this.borrowerName = borrowerName;
	}

	public String getBureauId() {
		return bureauId;
	}

	public void setBureauId(String bureauId) {
		this.bureauId = bureauId;
	}

	public String getBureauRefId() {
		return bureauRefId;
	}

	public void setBureauRefId(String bureauRefId) {
		this.bureauRefId = bureauRefId;
	}

	public Office getRegisteredOffice() {
		return registeredOffice;
	}

	public void setRegisteredOffice(Office registeredOffice) {
		this.registeredOffice = registeredOffice;
	}

	public List<Office> getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(List<Office> branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getNoHit() {
		return noHit;
	}

	public void setNoHit(String noHit) {
		this.noHit = noHit;
	}

	public List<String> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}

	@Override
	public String toString() {
		return "CommercialSearchList [borrowerName=" + borrowerName
				+ ", bureauId=" + bureauId + ", bureauRefId=" + bureauRefId
				+ ", registeredOffice=" + registeredOffice + ", branchOffice="
				+ branchOffice + ", noHit=" + noHit + ", errorList="
				+ errorList + "]";
	}
}
