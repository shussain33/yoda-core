package com.softcell.gonogo.model.multibureau.experian;

public class InProfileErrorResponse {
    private Header header;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InProfileErrorResponse{");
        sb.append("header=").append(header);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InProfileErrorResponse that = (InProfileErrorResponse) o;

        return header != null ? header.equals(that.header) : that.header == null;
    }

    @Override
    public int hashCode() {
        return header != null ? header.hashCode() : 0;
    }
}
