package com.softcell.gonogo.model.multibureau.crifHighmark;

public class INDVResponseList {

	private INDVResponse INDVResponse;

	public INDVResponse getINDVResponse() {
		return INDVResponse;
	}
	

	public void setINDVResponse(INDVResponse iNDVResponse) {
		INDVResponse = iNDVResponse;
	}


	@Override
	public String toString() {
		return "INDVResponseList [INDVResponse=" + INDVResponse + "]";
	}
	

	
	
}
