package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="GRP-RESPONSES")
@XmlAccessorType(XmlAccessType.FIELD)
public class GrpResponses {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="PRIMARY-SUMMARY")
	private Summary primarySummary;
	@XmlElement(name="SUMMARY")
	private Summary summary;
	@XmlElement(name="SECONDARY-SUMMARY")
	private Summary secSummary;
	@XmlElement(name="GRP-RESPONSE-LIST")
	private List <GrpResponse> GrpResponseList;
	
	
	public Summary getPrimarySummary() {
		return primarySummary;
	}
	
	public void setPrimarySummary(Summary primarySummary) {
		this.primarySummary = primarySummary;
	}
	
	public Summary getSummary() {
		return summary;
	}
	
	public void setSummary(Summary summary) {
		this.summary = summary;
	}
	
	public Summary getSecSummary() {
		return secSummary;
	}
	
	public void setSecSummary(Summary secSummary) {
		this.secSummary = secSummary;
	}

	public List<GrpResponse> getGrpResponseList() {
		return GrpResponseList;
	}
	

	public void setGrpResponseList(List<GrpResponse> grpResponseList) {
		GrpResponseList = grpResponseList;
	}

	@Override
	public String toString() {
		return "GrpResponses [primarySummary=" + primarySummary + ", summary="
				+ summary + ", secSummary=" + secSummary + ", GrpResponseList="
				+ GrpResponseList + "]";
	}
	
	
	

	
	
	
}
