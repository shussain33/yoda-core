package com.softcell.gonogo.model.multibureau.experian;

public class Score {

    private String bureauScore;
    private String bureauScoreConfidLevel;
    private String creditRating;


    public String getBureauScore() {
        return bureauScore;
    }

    public void setBureauScore(String bureauScore) {
        this.bureauScore = bureauScore;
    }

    public String getBureauScoreConfidLevel() {
        return bureauScoreConfidLevel;
    }

    public void setBureauScoreConfidLevel(String bureauScoreConfidLevel) {
        this.bureauScoreConfidLevel = bureauScoreConfidLevel;
    }

    public String getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(String creditRating) {
        this.creditRating = creditRating;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Score{");
        sb.append("bureauScore='").append(bureauScore).append('\'');
        sb.append(", bureauScoreConfidLevel='").append(bureauScoreConfidLevel).append('\'');
        sb.append(", creditRating='").append(creditRating).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Score score = (Score) o;

        if (bureauScore != null ? !bureauScore.equals(score.bureauScore) : score.bureauScore != null) return false;
        if (bureauScoreConfidLevel != null ? !bureauScoreConfidLevel.equals(score.bureauScoreConfidLevel) : score.bureauScoreConfidLevel != null)
            return false;
        return creditRating != null ? creditRating.equals(score.creditRating) : score.creditRating == null;
    }

    @Override
    public int hashCode() {
        int result = bureauScore != null ? bureauScore.hashCode() : 0;
        result = 31 * result + (bureauScoreConfidLevel != null ? bureauScoreConfidLevel.hashCode() : 0);
        result = 31 * result + (creditRating != null ? creditRating.hashCode() : 0);
        return result;
    }
}
