package com.softcell.gonogo.model.multibureau.crifHighmark;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="RELATIONS")
@XmlAccessorType(XmlAccessType.FIELD)
public class Relations {

	/**
	 * @author Akshata
	 *
	 *
	 */
	@XmlElement(name="RELATION")
	private List<Relation> relation;

	


	public List<Relation> getRelation() {
		return relation;
	}
	




	public void setRelation(List<Relation> relation) {
		this.relation = relation;
	}
	




	@Override
	public String toString() {
		return "Relations [relation=" + relation + "]";
	}
	
	
}
