package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="COMMENT")
@XmlAccessorType(XmlAccessType.FIELD)
public class Comment {
	
	@XmlElement(name="COMMENT-TEXT")
	private String commentText;
	@XmlElement(name="COMMENT-DATE")
	private String commentDate ;
	@XmlElement(name="BUREAU-COMMENT")
	private String bureauComment;
	
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public String getCommentDate() {
		return commentDate;
	}
	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}
	public String getBureauComment() {
		return bureauComment;
	}
	public void setBureauComment(String bureauComment) {
		this.bureauComment = bureauComment;
	}
	@Override
	public String toString() {
		return "Comments [commentText=" + commentText + ", commentDate="
				+ commentDate + ", bureauComment=" + bureauComment + "]";
	}

}
