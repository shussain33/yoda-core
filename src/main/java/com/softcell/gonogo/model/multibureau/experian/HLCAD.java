package com.softcell.gonogo.model.multibureau.experian;

public class HLCAD {

	private String TNOfHLCAD;
	private String TotValOfHLCAD;
	private String MNTSMRHLCAD;
	public String getTNOfHLCAD() {
		return TNOfHLCAD;
	}
	public void setTNOfHLCAD(String tNOfHLCAD) {
		TNOfHLCAD = tNOfHLCAD;
	}
	public String getTotValOfHLCAD() {
		return TotValOfHLCAD;
	}
	public void setTotValOfHLCAD(String totValOfHLCAD) {
		TotValOfHLCAD = totValOfHLCAD;
	}
	public String getMNTSMRHLCAD() {
		return MNTSMRHLCAD;
	}
	public void setMNTSMRHLCAD(String mNTSMRHLCAD) {
		MNTSMRHLCAD = mNTSMRHLCAD;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("HLCAD{");
		sb.append("TNOfHLCAD='").append(TNOfHLCAD).append('\'');
		sb.append(", TotValOfHLCAD='").append(TotValOfHLCAD).append('\'');
		sb.append(", MNTSMRHLCAD='").append(MNTSMRHLCAD).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		HLCAD hlcad = (HLCAD) o;

		if (TNOfHLCAD != null ? !TNOfHLCAD.equals(hlcad.TNOfHLCAD) : hlcad.TNOfHLCAD != null) return false;
		if (TotValOfHLCAD != null ? !TotValOfHLCAD.equals(hlcad.TotValOfHLCAD) : hlcad.TotValOfHLCAD != null)
			return false;
		return MNTSMRHLCAD != null ? MNTSMRHLCAD.equals(hlcad.MNTSMRHLCAD) : hlcad.MNTSMRHLCAD == null;
	}

	@Override
	public int hashCode() {
		int result = TNOfHLCAD != null ? TNOfHLCAD.hashCode() : 0;
		result = 31 * result + (TotValOfHLCAD != null ? TotValOfHLCAD.hashCode() : 0);
		result = 31 * result + (MNTSMRHLCAD != null ? MNTSMRHLCAD.hashCode() : 0);
		return result;
	}
}
