package com.softcell.gonogo.model.multibureau.crifHighmark;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="SCORE")
@XmlAccessorType(XmlAccessType.FIELD)
public class Score {
	
	@XmlElement(name="SCORE-TYPE")
	private String scoreType;	
	@XmlElement(name="SCORE-VERSION")
	private String scoreVersion;
	@XmlElement(name="SCORE-VALUE")
	private String scoreValue;
	@XmlElement(name="SCORE-FACTORS")
	private String scoreFactors;
	@XmlElement(name="SCORE-COMMENTS")
	private String scoreComments;
	
	public String getScoreType() {
		return scoreType;
	}
	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}
	public String getScoreVersion() {
		return scoreVersion;
	}
	public void setScoreVersion(String scoreVersion) {
		this.scoreVersion = scoreVersion;
	}
	public String getScoreValue() {
		return scoreValue;
	}
	public void setScoreValue(String scoreValue) {
		this.scoreValue = scoreValue;
	}
	public String getScoreFactors() {
		return scoreFactors;
	}
	public void setScoreFactors(String scoreFactors) {
		this.scoreFactors = scoreFactors;
	}
	public String getScoreComments() {
		return scoreComments;
	}
	public void setScoreComments(String scoreComments) {
		this.scoreComments = scoreComments;
	}
	@Override
	public String toString() {
		return "Score [scoreType=" + scoreType + ", scoreVersion="
				+ scoreVersion + ", scoreValue=" + scoreValue
				+ ", scoreFactors=" + scoreFactors + ", scoreComments="
				+ scoreComments + "]";
	}
}
