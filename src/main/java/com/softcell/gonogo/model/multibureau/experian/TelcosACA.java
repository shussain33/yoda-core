package com.softcell.gonogo.model.multibureau.experian;

public class TelcosACA {

	private String TNofTelcosACA;
	private String TotalBalTelcosACA;
	private String WCDStTelcosACA;
	private String WDSPr6MNTTelcosACA;
	private String WDSPr712MNTTelcosACA;
	private String AgeOfOldestTelcosACA;
	public String getTNofTelcosACA() {
		return TNofTelcosACA;
	}
	public void setTNofTelcosACA(String tNofTelcosACA) {
		TNofTelcosACA = tNofTelcosACA;
	}
	public String getTotalBalTelcosACA() {
		return TotalBalTelcosACA;
	}
	public void setTotalBalTelcosACA(String totalBalTelcosACA) {
		TotalBalTelcosACA = totalBalTelcosACA;
	}
	public String getWCDStTelcosACA() {
		return WCDStTelcosACA;
	}
	public void setWCDStTelcosACA(String wCDStTelcosACA) {
		WCDStTelcosACA = wCDStTelcosACA;
	}
	public String getWDSPr6MNTTelcosACA() {
		return WDSPr6MNTTelcosACA;
	}
	public void setWDSPr6MNTTelcosACA(String wDSPr6MNTTelcosACA) {
		WDSPr6MNTTelcosACA = wDSPr6MNTTelcosACA;
	}
	public String getWDSPr712MNTTelcosACA() {
		return WDSPr712MNTTelcosACA;
	}
	public void setWDSPr712MNTTelcosACA(String wDSPr712MNTTelcosACA) {
		WDSPr712MNTTelcosACA = wDSPr712MNTTelcosACA;
	}
	public String getAgeOfOldestTelcosACA() {
		return AgeOfOldestTelcosACA;
	}
	public void setAgeOfOldestTelcosACA(String ageOfOldestTelcosACA) {
		AgeOfOldestTelcosACA = ageOfOldestTelcosACA;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("TelcosACA{");
		sb.append("TNofTelcosACA='").append(TNofTelcosACA).append('\'');
		sb.append(", TotalBalTelcosACA='").append(TotalBalTelcosACA).append('\'');
		sb.append(", WCDStTelcosACA='").append(WCDStTelcosACA).append('\'');
		sb.append(", WDSPr6MNTTelcosACA='").append(WDSPr6MNTTelcosACA).append('\'');
		sb.append(", WDSPr712MNTTelcosACA='").append(WDSPr712MNTTelcosACA).append('\'');
		sb.append(", AgeOfOldestTelcosACA='").append(AgeOfOldestTelcosACA).append('\'');
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TelcosACA telcosACA = (TelcosACA) o;

		if (TNofTelcosACA != null ? !TNofTelcosACA.equals(telcosACA.TNofTelcosACA) : telcosACA.TNofTelcosACA != null)
			return false;
		if (TotalBalTelcosACA != null ? !TotalBalTelcosACA.equals(telcosACA.TotalBalTelcosACA) : telcosACA.TotalBalTelcosACA != null)
			return false;
		if (WCDStTelcosACA != null ? !WCDStTelcosACA.equals(telcosACA.WCDStTelcosACA) : telcosACA.WCDStTelcosACA != null)
			return false;
		if (WDSPr6MNTTelcosACA != null ? !WDSPr6MNTTelcosACA.equals(telcosACA.WDSPr6MNTTelcosACA) : telcosACA.WDSPr6MNTTelcosACA != null)
			return false;
		if (WDSPr712MNTTelcosACA != null ? !WDSPr712MNTTelcosACA.equals(telcosACA.WDSPr712MNTTelcosACA) : telcosACA.WDSPr712MNTTelcosACA != null)
			return false;
		return AgeOfOldestTelcosACA != null ? AgeOfOldestTelcosACA.equals(telcosACA.AgeOfOldestTelcosACA) : telcosACA.AgeOfOldestTelcosACA == null;
	}

	@Override
	public int hashCode() {
		int result = TNofTelcosACA != null ? TNofTelcosACA.hashCode() : 0;
		result = 31 * result + (TotalBalTelcosACA != null ? TotalBalTelcosACA.hashCode() : 0);
		result = 31 * result + (WCDStTelcosACA != null ? WCDStTelcosACA.hashCode() : 0);
		result = 31 * result + (WDSPr6MNTTelcosACA != null ? WDSPr6MNTTelcosACA.hashCode() : 0);
		result = 31 * result + (WDSPr712MNTTelcosACA != null ? WDSPr712MNTTelcosACA.hashCode() : 0);
		result = 31 * result + (AgeOfOldestTelcosACA != null ? AgeOfOldestTelcosACA.hashCode() : 0);
		return result;
	}
}
