/**
 * kishorp11:46:56 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.GNGWorkflowConstant;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author kishorp
 *
 *         <pre>
 * A <em>CarSurrogate</em> is used to store credit details under Car surrogate program.
 * </pre>
 *
 *         <pre>
 * <em>modelName</em> : model  number of customer car, which is String data type.
 * <br>
 * <em>dtManufactureDate</em> : is manufactured date customer of car.
 * <br>
 * <em>sRegNumber</em> : Registration number of car.
 * </pre>
 *
 *         <
 */
public class CarSurrogate implements Serializable {

    @JsonProperty("sModelNo")
    private String modelName;

    @JsonProperty("dtManufactureDate")
    private Date manufactureYear;

    @JsonProperty("sRegNumber")
    private String registrationNumber;

    @JsonProperty("sCat")
    private String category;

    @JsonProperty("sType")
    private GNGWorkflowConstant type = GNGWorkflowConstant.CAR_SURROGATE;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return the modelName
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * @param modelName
     *            the modelName to set
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     * @return the manufactureYear
     */
    public Date getManufactureYear() {
        return manufactureYear;
    }

    /**
     * @param manufactureYear
     *            the manufactureYear to set
     */
    public void setManufactureYear(Date manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    /**
     * @return the registrationNumber
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * @param registrationNumber
     *            the registrationNumber to set
     */
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public GNGWorkflowConstant getType() {
        return type;
    }

    public void setType(GNGWorkflowConstant type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CarSurrogate [modelName=");
        builder.append(modelName);
        builder.append(", manufactureYear=");
        builder.append(manufactureYear);
        builder.append(", registrationNumber=");
        builder.append(registrationNumber);
        builder.append(", category=");
        builder.append(category);
        builder.append(", type=");
        builder.append(type);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result
                + ((category == null) ? 0 : category.hashCode());
        result = prime * result
                + ((manufactureYear == null) ? 0 : manufactureYear.hashCode());
        result = prime * result
                + ((modelName == null) ? 0 : modelName.hashCode());
        result = prime
                * result
                + ((registrationNumber == null) ? 0 : registrationNumber
                .hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CarSurrogate))
            return false;
        CarSurrogate other = (CarSurrogate) obj;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (category == null) {
            if (other.category != null)
                return false;
        } else if (!category.equals(other.category))
            return false;
        if (manufactureYear == null) {
            if (other.manufactureYear != null)
                return false;
        } else if (!manufactureYear.equals(other.manufactureYear))
            return false;
        if (modelName == null) {
            if (other.modelName != null)
                return false;
        } else if (!modelName.equals(other.modelName))
            return false;
        if (registrationNumber == null) {
            if (other.registrationNumber != null)
                return false;
        } else if (!registrationNumber.equals(other.registrationNumber))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

}
