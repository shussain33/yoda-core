/**
 * kishorp11:48:34 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.GNGWorkflowConstant;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author kishorp
 *
 *         <pre>
 * A <em>TraderSurrogate </em> is used to  store Trader surrogate info under  Trader Surrogate Surrogate program
 * </pre>
 */
public class TraderSurrogate implements Serializable {
    @JsonProperty("iYrsInBussines")
    private int yearInBussines;
    @JsonProperty("sBussinesProof")
    private String bussinesProof;
    @JsonProperty("sType")
    private GNGWorkflowConstant type = GNGWorkflowConstant.TRADER_SURROGATE;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return the yearInBussines
     */
    public int getYearInBussines() {
        return yearInBussines;
    }

    /**
     * @param yearInBussines
     *            the yearInBussines to set
     */
    public void setYearInBussines(int yearInBussines) {
        this.yearInBussines = yearInBussines;
    }

    /**
     * @return the bussinesProof
     */
    public String getBussinesProof() {
        return bussinesProof;
    }

    /**
     * @param bussinesProof
     *            the bussinesProof to set
     */
    public void setBussinesProof(String bussinesProof) {
        this.bussinesProof = bussinesProof;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("TraderSurrogate [yearInBussines=");
        builder.append(yearInBussines);
        builder.append(", bussinesProof=");
        builder.append(bussinesProof);
        builder.append(", type=");
        builder.append(type);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result
                + ((bussinesProof == null) ? 0 : bussinesProof.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + yearInBussines;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TraderSurrogate other = (TraderSurrogate) obj;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (bussinesProof == null) {
            if (other.bussinesProof != null)
                return false;
        } else if (!bussinesProof.equals(other.bussinesProof))
            return false;
        if (type != other.type)
            return false;
        if (yearInBussines != other.yearInBussines)
            return false;
        return true;
    }

}
