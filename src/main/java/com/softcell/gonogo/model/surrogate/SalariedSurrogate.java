/**
 * kishorp11:47:16 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.GNGWorkflowConstant;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author kishorp
 *
 *         <pre>
 * A <em>SalariedSurrogate </em> is used to  store salary info under  Salaried Surrogate program
 * </pre>
 */
public class SalariedSurrogate implements Serializable {

    @JsonProperty("dNetTakeHome")
    private double netTakeHome;
    @JsonProperty("sType")
    private GNGWorkflowConstant type = GNGWorkflowConstant.SALARIED_SURROGATE;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    /**
     * @return the netTakeHome
     */
    public double getNetTakeHome() {
        return netTakeHome;
    }

    /**
     * @param netTakeHome
     *            the netTakeHome to set
     */
    public void setNetTakeHome(double netTakeHome) {
        this.netTakeHome = netTakeHome;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SalariedSurrogate [netTakeHome=");
        builder.append(netTakeHome);
        builder.append(", type=");
        builder.append(type);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        long temp;
        temp = Double.doubleToLongBits(netTakeHome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SalariedSurrogate other = (SalariedSurrogate) obj;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (Double.doubleToLongBits(netTakeHome) != Double
                .doubleToLongBits(other.netTakeHome))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

}
