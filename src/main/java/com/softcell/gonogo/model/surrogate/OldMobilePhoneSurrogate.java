package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by yogeshb on 6/7/17.
 */
public class OldMobilePhoneSurrogate implements Serializable{

    @JsonProperty("dInvoiceAmt")
    private double invoiceAmount;

    @JsonProperty("sIMEI")
    private String imeiNumber;

    public double getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(double invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OldMobilePhoneSurrogate{");
        sb.append("invoiceAmount=").append(invoiceAmount);
        sb.append(", imeiNumber='").append(imeiNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OldMobilePhoneSurrogate that = (OldMobilePhoneSurrogate) o;

        if (Double.compare(that.invoiceAmount, invoiceAmount) != 0) return false;
        return imeiNumber != null ? imeiNumber.equals(that.imeiNumber) : that.imeiNumber == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(invoiceAmount);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (imeiNumber != null ? imeiNumber.hashCode() : 0);
        return result;
    }
}
