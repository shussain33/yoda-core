package com.softcell.gonogo.model.surrogate.creditcard;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CreditCardSurrogateRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {CreditCardSurrogateRequest.FetchGrp.class})
    private String refID;

    @JsonProperty("sCreditCardNumber")
    @NotEmpty(groups = {CreditCardSurrogateRequest.FetchGrp.class})
    private String creditCardNumber;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CreditCardSurrogateRequest [header=");
        builder.append(header);
        builder.append(", refID=");
        builder.append(refID);
        builder.append(", creditCardNumber=");
        builder.append(creditCardNumber);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((creditCardNumber == null) ? 0 : creditCardNumber.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CreditCardSurrogateRequest))
            return false;
        CreditCardSurrogateRequest other = (CreditCardSurrogateRequest) obj;
        if (creditCardNumber == null) {
            if (other.creditCardNumber != null)
                return false;
        } else if (!creditCardNumber.equals(other.creditCardNumber))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        return true;
    }

    public interface FetchGrp{}
}
