package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.utils.GngDateUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yogeshb on 6/7/17.
 */
public class PostPaidBillSurrogate implements Serializable{
    @JsonProperty("dtBill")
    private Date billDate;
    @JsonProperty("oAddress")
    private CustomerAddress address;

    @JsonProperty("sBillDate")
    private String billDt;

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public CustomerAddress getAddress() {
        return address;
    }

    public void setAddress(CustomerAddress address) {
        this.address = address;
    }

    public String getBillDt() {
        if(this.billDate != null)
            return GngDateUtil.changeDateFormat(this.getBillDate(), GngDateUtil.ddMMyyyy);
        return "";
    }

    public void setBillDt(String billDt) {
        if(this.billDate != null)
        this.billDt = GngDateUtil.changeDateFormat(this.getBillDate(), GngDateUtil.ddMMyyyy);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PostPaidBillSurrogate{");
        sb.append("billDate=").append(billDate);
        sb.append(", address=").append(address);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostPaidBillSurrogate that = (PostPaidBillSurrogate) o;

        if (billDate != null ? !billDate.equals(that.billDate) : that.billDate != null) return false;
        return address != null ? address.equals(that.address) : that.address == null;
    }

    @Override
    public int hashCode() {
        int result = billDate != null ? billDate.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }
}
