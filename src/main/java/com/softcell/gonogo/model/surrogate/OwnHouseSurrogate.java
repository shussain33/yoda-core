/**
 * kishorp11:46:37 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.GNGWorkflowConstant;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author kishorp
 *
 *         <pre>
 * A <em> OwnHouseSurrogate</em> is used to  store house details under surrogate program
 * </pre>
 */
public class OwnHouseSurrogate implements Serializable {
    @JsonProperty("sHouseType")
    private String houseType;

    @JsonProperty("sDocumentType")
    private String documentType;

    @JsonProperty("sOwnerName")
    private String ownerName;

    @JsonProperty("sOwnerMobNumber")
    private String ownerMobNumber;

    @JsonProperty("sServiceProvider")
    private String serviceProvider;

    @JsonProperty("sConmrIdOrAccNo")
    private String consumerIdOrAccNo;

    @JsonProperty("sType")
    private GNGWorkflowConstant type = GNGWorkflowConstant.HOUSE_SURROGATE;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    /**
     * @return the houseType
     */
    public String getHouseType() {
        return houseType;
    }

    /**
     * @param houseType
     *            the houseType to set
     */
    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    /**
     * @return the informationType
     */
    public String getDocumentType() {
        return documentType;
    }

    /**
     * @param documentType
     *            the informationType to set
     */
    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }


    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerMobNumber() {
        return ownerMobNumber;
    }

    public void setOwnerMobNumber(String ownerMobNumber) {
        this.ownerMobNumber = ownerMobNumber;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getConsumerIdOrAccNo() {
        return consumerIdOrAccNo;
    }

    public void setConsumerIdOrAccNo(String consumerIdOrAccNo) {
        this.consumerIdOrAccNo = consumerIdOrAccNo;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OwnHouseSurrogate{");
        sb.append("houseType='").append(houseType).append('\'');
        sb.append(", documentType='").append(documentType).append('\'');
        sb.append(", ownerName='").append(ownerName).append('\'');
        sb.append(", ownerMobNumber='").append(ownerMobNumber).append('\'');
        sb.append(", serviceProvider='").append(serviceProvider).append('\'');
        sb.append(", consumerIdOrAccNo='").append(consumerIdOrAccNo).append('\'');
        sb.append(", type=").append(type);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OwnHouseSurrogate that = (OwnHouseSurrogate) o;

        if (houseType != null ? !houseType.equals(that.houseType) : that.houseType != null) return false;
        if (documentType != null ? !documentType.equals(that.documentType) : that.documentType != null) return false;
        if (ownerName != null ? !ownerName.equals(that.ownerName) : that.ownerName != null) return false;
        if (ownerMobNumber != null ? !ownerMobNumber.equals(that.ownerMobNumber) : that.ownerMobNumber != null)
            return false;
        if (serviceProvider != null ? !serviceProvider.equals(that.serviceProvider) : that.serviceProvider != null)
            return false;
        if (consumerIdOrAccNo != null ? !consumerIdOrAccNo.equals(that.consumerIdOrAccNo) : that.consumerIdOrAccNo != null)
            return false;
        if (type != that.type) return false;
        return !(additionalProperties != null ? !additionalProperties.equals(that.additionalProperties) : that.additionalProperties != null);

    }

    @Override
    public int hashCode() {
        int result = houseType != null ? houseType.hashCode() : 0;
        result = 31 * result + (documentType != null ? documentType.hashCode() : 0);
        result = 31 * result + (ownerName != null ? ownerName.hashCode() : 0);
        result = 31 * result + (ownerMobNumber != null ? ownerMobNumber.hashCode() : 0);
        result = 31 * result + (serviceProvider != null ? serviceProvider.hashCode() : 0);
        result = 31 * result + (consumerIdOrAccNo != null ? consumerIdOrAccNo.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}
