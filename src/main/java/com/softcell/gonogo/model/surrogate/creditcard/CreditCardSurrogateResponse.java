package com.softcell.gonogo.model.surrogate.creditcard;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import java.util.Arrays;

public class CreditCardSurrogateResponse {

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("success")
    private String success;

    @JsonProperty("BIN")
    private String bin;

    @JsonProperty("CC_type")
    private String ccType;

    @JsonProperty("Data")
    private String data;

    @JsonProperty("data")
    private String[] dataArray;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String[] getDataArray() {
        return dataArray;
    }

    public void setDataArray(String[] dataArray) {
        this.dataArray = dataArray;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreditCardSurrogateResponse)) return false;
        CreditCardSurrogateResponse that = (CreditCardSurrogateResponse) o;
        return Objects.equal(getStatus(), that.getStatus()) &&
                Objects.equal(getMessage(), that.getMessage()) &&
                Objects.equal(getSuccess(), that.getSuccess()) &&
                Objects.equal(getBin(), that.getBin()) &&
                Objects.equal(getCcType(), that.getCcType()) &&
                Objects.equal(getData(), that.getData()) &&
                Objects.equal(getDataArray(), that.getDataArray());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getStatus(), getMessage(), getSuccess(), getBin(), getCcType(), getData(), getDataArray());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CreditCardSurrogateResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", success='").append(success).append('\'');
        sb.append(", bin='").append(bin).append('\'');
        sb.append(", ccType='").append(ccType).append('\'');
        sb.append(", data='").append(data).append('\'');
        sb.append(", dataArray=").append(Arrays.toString(dataArray));
        sb.append('}');
        return sb.toString();
    }
}
