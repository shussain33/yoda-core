/**
 * kishorp11:48:12 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.GNGWorkflowConstant;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author kishorp
 *
 *         <pre>
 * A <em>CreditCardSurrogate </em> is used to  store CreditCard info under  CreditCard Surrogate program
 * </pre>
 */
public class CreditCardSurrogate implements Serializable {
    @JsonProperty("sCardNumber")
    private String cardNumber;
    @JsonProperty("sCardCategory")
    private String cardCategory;
    @JsonProperty("dLimit")
    private double limit;
    @JsonProperty("iVintage")
    private int cardVintage;
    @JsonProperty("dOutstandingAmt")
    private double outStandingAmt;
    @JsonProperty("dLastPaymentAmt")
    private double lastPaymentAmt;
    @JsonProperty("sType")
    private GNGWorkflowConstant type = GNGWorkflowConstant.CREDIT_SURROGATE;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public double getLastPaymentAmt() {
        return lastPaymentAmt;
    }

    public void setLastPaymentAmt(double lastPaymentAmt) {
        this.lastPaymentAmt = lastPaymentAmt;
    }

    public GNGWorkflowConstant getType() {
        return type;
    }

    public void setType(GNGWorkflowConstant type) {
        this.type = type;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public int getCardVintage() {
        return cardVintage;
    }

    public void setCardVintage(int cardVintage) {
        this.cardVintage = cardVintage;
    }

    public double getOutStandingAmt() {
        return outStandingAmt;
    }

    public void setOutStandingAmt(double outStandingAmt) {
        this.outStandingAmt = outStandingAmt;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardCategory
     */
    public String getCardCategory() {
        return cardCategory;
    }

    /**
     * @param cardCategory
     *            the cardCategory to set
     */
    public void setCardCategory(String cardCategory) {
        this.cardCategory = cardCategory;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CreditCardSurrogate [cardNumber=");
        builder.append(cardNumber);
        builder.append(", cardCategory=");
        builder.append(cardCategory);
        builder.append(", limit=");
        builder.append(limit);
        builder.append(", cardVintage=");
        builder.append(cardVintage);
        builder.append(", outStandingAmt=");
        builder.append(outStandingAmt);
        builder.append(", lastPaymentAmt=");
        builder.append(lastPaymentAmt);
        builder.append(", type=");
        builder.append(type);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result
                + ((cardCategory == null) ? 0 : cardCategory.hashCode());
        result = prime * result
                + ((cardNumber == null) ? 0 : cardNumber.hashCode());
        result = prime * result + cardVintage;
        long temp;
        temp = Double.doubleToLongBits(lastPaymentAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(limit);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(outStandingAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CreditCardSurrogate other = (CreditCardSurrogate) obj;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (cardCategory == null) {
            if (other.cardCategory != null)
                return false;
        } else if (!cardCategory.equals(other.cardCategory))
            return false;
        if (cardNumber == null) {
            if (other.cardNumber != null)
                return false;
        } else if (!cardNumber.equals(other.cardNumber))
            return false;
        if (cardVintage != other.cardVintage)
            return false;
        if (Double.doubleToLongBits(lastPaymentAmt) != Double
                .doubleToLongBits(other.lastPaymentAmt))
            return false;
        if (Double.doubleToLongBits(limit) != Double
                .doubleToLongBits(other.limit))
            return false;
        if (Double.doubleToLongBits(outStandingAmt) != Double
                .doubleToLongBits(other.outStandingAmt))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

}
