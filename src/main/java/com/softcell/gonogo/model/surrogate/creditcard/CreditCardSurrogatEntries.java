package com.softcell.gonogo.model.surrogate.creditcard;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "creditCardSurrogateEntries")
public class CreditCardSurrogatEntries extends AuditEntity {
    private Date dateTime = new Date();
    private CreditCardSurrogateRequest creditCardSurrogateRequest;
    private CreditCardSurrogateResponse creditCardSurrogateResponse;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public CreditCardSurrogateRequest getCreditCardSurrogateRequest() {
        return creditCardSurrogateRequest;
    }

    public void setCreditCardSurrogateRequest(
            CreditCardSurrogateRequest creditCardSurrogateRequest) {
        this.creditCardSurrogateRequest = creditCardSurrogateRequest;
    }

    public CreditCardSurrogateResponse getCreditCardSurrogateResponse() {
        return creditCardSurrogateResponse;
    }

    public void setCreditCardSurrogateResponse(
            CreditCardSurrogateResponse creditCardSurrogateResponse) {
        this.creditCardSurrogateResponse = creditCardSurrogateResponse;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CreditCardSurrogatStore [dateTime=");
        builder.append(dateTime);
        builder.append(", creditCardSurrogateRequest=");
        builder.append(creditCardSurrogateRequest);
        builder.append(", creditCardSurrogateResponse=");
        builder.append(creditCardSurrogateResponse);
        builder.append("]");
        return builder.toString();
    }

}
