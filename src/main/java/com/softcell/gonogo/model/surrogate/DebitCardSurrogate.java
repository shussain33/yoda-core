package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.DebitCardType;
import com.softcell.constants.GNGWorkflowConstant;

import java.io.Serializable;

/**
 * Created by piyushgupta on 3/5/17.
 */
public class DebitCardSurrogate implements Serializable {
    @JsonProperty("sCardNumber")
    private String cardNumber;

    /**
     * this field is type of surrogate
     */
    @JsonProperty("sType")
    private GNGWorkflowConstant type = GNGWorkflowConstant.DEBIT_CARD_SURROGATE;

    /**
     * this field is debit card type
     */
    @JsonProperty("sDebitCardType")
    private DebitCardType debitCardType;

    public DebitCardType getDebitCardType() {
        return debitCardType;
    }

    public void setDebitCardType(DebitCardType debitCardType) {
        this.debitCardType = debitCardType;
    }

    public GNGWorkflowConstant getType() {
        return type;
    }

    public void setType(GNGWorkflowConstant type) {
        this.type = type;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
