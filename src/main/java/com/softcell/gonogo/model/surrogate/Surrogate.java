/**
 * kishorp11:45:30 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.surrogate;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.SurrogateType;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author kishorp
 *
 *         <pre>
 * A <em> SurrogateType</em> is used to  capture details under surrogate program  of customer.
 * </pre>
 */
public class Surrogate implements Serializable {
    @JsonProperty("aCar")
    private List<CarSurrogate> carSurrogate;
    @JsonProperty("aOwnHouse")
    private List<OwnHouseSurrogate> houseSurrogate;
    @JsonProperty("aSalary")
    private List<SalariedSurrogate> salariedSurrogate;
    @JsonProperty("aTrader")
    private List<TraderSurrogate> traderSurrogate;
    @JsonProperty("aCreditCard")
    private List<CreditCardSurrogate> creditCardSurrogate;
    @JsonProperty("aDebitCard")
    private List<DebitCardSurrogate> debitCardSurrogate;
    @JsonProperty("aBankAccount")
    private List<BankSurrogate> bankSurragates;
    @JsonProperty("aPostPaidBill")
    private List<PostPaidBillSurrogate> postPaidBillSurrogates;
    @JsonProperty("aOldMobilePhoneSurrogate")
    private List<OldMobilePhoneSurrogate> oldMobilePhoneSurrogates;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    @JsonProperty("sSelectedSurrogate")
    private SurrogateType selectedSurrogate;

    public List<BankSurrogate> getBankSurragates() {
        return bankSurragates;
    }

    public void setBankSurragates(List<BankSurrogate> bankSurragates) {
        this.bankSurragates = bankSurragates;
    }

    /**
     * @return the carSurrogate
     */
    public List<CarSurrogate> getCarSurrogate() {
        return carSurrogate;
    }

    /**
     * @param carSurrogate
     *            the carSurrogate to set
     */
    public void setCarSurrogate(List<CarSurrogate> carSurrogate) {
        this.carSurrogate = carSurrogate;
    }

    /**
     * @return the houseSurrogate
     */
    public List<OwnHouseSurrogate> getHouseSurrogate() {
        return houseSurrogate;
    }

    /**
     * @param houseSurrogate
     *            the houseSurrogate to set
     */
    public void setHouseSurrogate(List<OwnHouseSurrogate> houseSurrogate) {
        this.houseSurrogate = houseSurrogate;
    }

    /**
     * @return the salariedSurrogate
     */
    public List<SalariedSurrogate> getSalariedSurrogate() {
        return salariedSurrogate;
    }

    /**
     * @param salariedSurrogate
     *            the salariedSurrogate to set
     */
    public void setSalariedSurrogate(List<SalariedSurrogate> salariedSurrogate) {
        this.salariedSurrogate = salariedSurrogate;
    }

    /**
     * @return the traderSurrogate
     */
    public List<TraderSurrogate> getTraderSurrogate() {
        return traderSurrogate;
    }

    /**
     * @param traderSurrogate
     *            the traderSurrogate to set
     */
    public void setTraderSurrogate(List<TraderSurrogate> traderSurrogate) {
        this.traderSurrogate = traderSurrogate;
    }

    /**
     * @return the creditCardSurrogate
     */
    public List<CreditCardSurrogate> getCreditCardSurrogate() {
        return creditCardSurrogate;
    }

    /**
     * @param creditCardSurrogate
     *            the creditCardSurrogate to set
     */
    public void setCreditCardSurrogate(
            List<CreditCardSurrogate> creditCardSurrogate) {
        this.creditCardSurrogate = creditCardSurrogate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public List<DebitCardSurrogate> getDebitCardSurrogate() {
        return debitCardSurrogate;
    }

    public void setDebitCardSurrogate(List<DebitCardSurrogate> debitCardSurrogate) {
        this.debitCardSurrogate = debitCardSurrogate;
    }

    public List<PostPaidBillSurrogate> getPostPaidBillSurrogates() {
        return postPaidBillSurrogates;
    }

    public void setPostPaidBillSurrogates(List<PostPaidBillSurrogate> postPaidBillSurrogates) {
        this.postPaidBillSurrogates = postPaidBillSurrogates;
    }

    public List<OldMobilePhoneSurrogate> getOldMobilePhoneSurrogates() {
        return oldMobilePhoneSurrogates;
    }

    public void setOldMobilePhoneSurrogates(List<OldMobilePhoneSurrogate> oldMobilePhoneSurrogates) {
        this.oldMobilePhoneSurrogates = oldMobilePhoneSurrogates;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public SurrogateType getSelectedSurrogate() {
        return selectedSurrogate;
    }

    public void setSelectedSurrogate(SurrogateType selectedSurrogate) {
        this.selectedSurrogate = selectedSurrogate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Surrogate{");
        sb.append("carSurrogate=").append(carSurrogate);
        sb.append(", houseSurrogate=").append(houseSurrogate);
        sb.append(", salariedSurrogate=").append(salariedSurrogate);
        sb.append(", traderSurrogate=").append(traderSurrogate);
        sb.append(", creditCardSurrogate=").append(creditCardSurrogate);
        sb.append(", debitCardSurrogate=").append(debitCardSurrogate);
        sb.append(", bankSurragates=").append(bankSurragates);
        sb.append(", postPaidBillSurrogates=").append(postPaidBillSurrogates);
        sb.append(", oldMobilePhoneSurrogates=").append(oldMobilePhoneSurrogates);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append(", selectedSurrogate=").append(selectedSurrogate);
        sb.append('}');
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Surrogate surrogate = (Surrogate) o;

        if (getCarSurrogate() != null ? !getCarSurrogate().equals(surrogate.getCarSurrogate()) : surrogate.getCarSurrogate() != null)
            return false;
        if (getHouseSurrogate() != null ? !getHouseSurrogate().equals(surrogate.getHouseSurrogate()) : surrogate.getHouseSurrogate() != null)
            return false;
        if (getSalariedSurrogate() != null ? !getSalariedSurrogate().equals(surrogate.getSalariedSurrogate()) : surrogate.getSalariedSurrogate() != null)
            return false;
        if (getTraderSurrogate() != null ? !getTraderSurrogate().equals(surrogate.getTraderSurrogate()) : surrogate.getTraderSurrogate() != null)
            return false;
        if (getCreditCardSurrogate() != null ? !getCreditCardSurrogate().equals(surrogate.getCreditCardSurrogate()) : surrogate.getCreditCardSurrogate() != null)
            return false;
        if (getDebitCardSurrogate() != null ? !getDebitCardSurrogate().equals(surrogate.getDebitCardSurrogate()) : surrogate.getDebitCardSurrogate() != null)
            return false;
        if (getBankSurragates() != null ? !getBankSurragates().equals(surrogate.getBankSurragates()) : surrogate.getBankSurragates() != null)
            return false;
        if (getPostPaidBillSurrogates() != null ? !getPostPaidBillSurrogates().equals(surrogate.getPostPaidBillSurrogates()) : surrogate.getPostPaidBillSurrogates() != null)
            return false;
        if (getOldMobilePhoneSurrogates() != null ? !getOldMobilePhoneSurrogates().equals(surrogate.getOldMobilePhoneSurrogates()) : surrogate.getOldMobilePhoneSurrogates() != null)
            return false;
        if (getAdditionalProperties() != null ? !getAdditionalProperties().equals(surrogate.getAdditionalProperties()) : surrogate.getAdditionalProperties() != null)
            return false;
        return getSelectedSurrogate() == surrogate.getSelectedSurrogate();
    }

    @Override
    public int hashCode() {
        int result = getCarSurrogate() != null ? getCarSurrogate().hashCode() : 0;
        result = 31 * result + (getHouseSurrogate() != null ? getHouseSurrogate().hashCode() : 0);
        result = 31 * result + (getSalariedSurrogate() != null ? getSalariedSurrogate().hashCode() : 0);
        result = 31 * result + (getTraderSurrogate() != null ? getTraderSurrogate().hashCode() : 0);
        result = 31 * result + (getCreditCardSurrogate() != null ? getCreditCardSurrogate().hashCode() : 0);
        result = 31 * result + (getDebitCardSurrogate() != null ? getDebitCardSurrogate().hashCode() : 0);
        result = 31 * result + (getBankSurragates() != null ? getBankSurragates().hashCode() : 0);
        result = 31 * result + (getPostPaidBillSurrogates() != null ? getPostPaidBillSurrogates().hashCode() : 0);
        result = 31 * result + (getOldMobilePhoneSurrogates() != null ? getOldMobilePhoneSurrogates().hashCode() : 0);
        result = 31 * result + (getAdditionalProperties() != null ? getAdditionalProperties().hashCode() : 0);
        result = 31 * result + (getSelectedSurrogate() != null ? getSelectedSurrogate().hashCode() : 0);
        return result;
    }
}
