package com.softcell.gonogo.model.qde;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 17/8/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExistingCustomerServiceRequest {
  // date of birth in yyyy-MM-dd format
  @JsonProperty("dob")
  private String dateOfBirth;

  // Mobile number
  @JsonProperty("mobile_no")
  private String mobileNumber;

  // KYC docs
  @JsonProperty("pan_no")
  private String panNumber;

  @JsonProperty("aadhar_no")
  private String aadharNumber;

  @JsonProperty("driving_lc")
  private String drivingLicence;

  @JsonProperty("voter_id")
  private String voterId;
}
