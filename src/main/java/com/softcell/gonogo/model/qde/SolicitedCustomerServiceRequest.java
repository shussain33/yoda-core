package com.softcell.gonogo.model.qde;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 27/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SolicitedCustomerServiceRequest {
    @JsonProperty("mobile_no")
    private String mobileNumber;

    @JsonProperty("request_id")
    private String requestId;

}
