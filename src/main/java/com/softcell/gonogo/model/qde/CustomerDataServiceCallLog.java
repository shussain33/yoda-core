package com.softcell.gonogo.model.qde;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by archana on 27/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "customerDataServiceCallLog")
public class CustomerDataServiceCallLog {

    @JsonProperty("sApplicantType")
    private String applicantType;

    @JsonProperty("sCouponCode")
    private String couponCode;

    @JsonProperty("sMobileNo")
    private String mobileNumber;

    @JsonProperty("skycType")
    private String kycType;

    @JsonProperty("skycNumber")
    private String kycNumber;

    @JsonProperty("sDob")
    private String dateOfBirth;

    // Response fields
    @JsonProperty("sResponseCode")
    private String responseCode;

    @JsonProperty("sSuccess")
    private String success;

    @JsonProperty("sDescription")
    private String description;

}
