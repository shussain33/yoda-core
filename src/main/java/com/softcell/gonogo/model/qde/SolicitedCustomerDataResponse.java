package com.softcell.gonogo.model.qde;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 27/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SolicitedCustomerDataResponse {
    /*
    {    "response_code": "3",    "description": "success",    "success": "true",
     "AppId": "14072017104044",    "user_id": "HDB36801",    "upload_date": "2017-07-14 10:40:45.077",
     "first_name": "TUSGYSG",    "middle_name": "",    "last_name": "HSHSGUSGUS",
      "phone_no": "6464348454",    "pan_no": "HJXCC4252B",    "dob": "19520714",
       "voter_id": "",    "driving_lic": "",    "passport": "",
       "loan_availed": "No",    "pan_avial": "null",    "any_loan": "No",    "cc_avial": "No",
       "constitution": "Salaried",    "net_salary": "647878.0",    "tax_paid_amt": "0.0",    "state_code": "15",
       "dealer_id": null,    "product_code": null,    "pincode": "400008",
       "Score": null,    "Eligibility_Amt": null,    "Eligibility_Status": null,    "Application_Status": null }
     */

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("upload_date")
    private String uploadDate;

    @JsonProperty("success")
    private String success;

    @JsonProperty("description")
    private String description;

    @JsonProperty("AppId")
    private String appId;   // This is couponCode

    @JsonProperty("user_id")
    private String userId;

    // --------- Customer Data Fields
    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("middle_name")
    private String middleName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("phone_no")
    private String phoneNumber;    // This is mobile n. sent in request

    @JsonProperty("pan_no")
    private String panNumber;

    @JsonProperty("voter_id")
    private String voterId;

    @JsonProperty("driving_lic")
    private String drivingLicence;

    @JsonProperty("passport")
    private String passport;

    @JsonProperty("loan_availed")
    private String loanAvailed;

    @JsonProperty("pan_avial")
    private String panAvailable;

    @JsonProperty("any_loan")
    private String anyLoan;

    @JsonProperty("cc_avial")
    private String ccAvailable;

    @JsonProperty("constitution")
    private String constitution;

    @JsonProperty("net_salary")//"647878.0",
    private double netSalary;

    @JsonProperty("tax_paid_amt")// "0.0",
    private double taxPaidAmt;

    @JsonProperty("state_code")// "15",
    private String stateCode;

    @JsonProperty("dealer_id")
    private String dealerId;

    @JsonProperty("product_code")
    private String productCode;

    @JsonProperty("pincode")
    private String pincode;

    @JsonProperty("Score")
    private String score;

    @JsonProperty("Eligibility_Amt")
    private String eligibilityAmt;

    @JsonProperty("Eligibility_Status")
    private String eligibilityStatus;

    @JsonProperty("Application_Status")
    private String applicationStatus;

}
