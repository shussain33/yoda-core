package com.softcell.gonogo.model.qde;

/**
 * Created by archana on 22/8/17.
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * POJO for fields from ExistingCustomer Data and Solicitred Customer Data.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDataResponse {

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("success")
    private String success;

    @JsonProperty("description")
    private String description;

    // --------- Customer Data Fields
    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("middle_name")
    private String middleName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("marital_status")
    private String maritalStatus;

    @JsonProperty("education_qualification")
    private String educationQualification;

    // KYC docs
    @JsonProperty("aadhar_no")
    private String aadharNumber;

    @JsonProperty("driving_lic")
    private String drivingLicence;

    // Addresses
    // Residence
    @JsonProperty("residence_address_type")
    private String residenceAddressType;
    @JsonProperty("res_address_line_1")
    private String resAddressLine1;
    @JsonProperty("res_address_line_2")
    private String resAddressLine2;
    @JsonProperty("res_address_line_3")
    private String resAddressLine3;
    @JsonProperty("res_landmark")
    private String resLandmark;
    @JsonProperty("res_pincode")
    private String resPincode;
    @JsonProperty("res_city")
    private String resCity;
    @JsonProperty("res_state")
    private String resState;
    // Permanent
    @JsonProperty("permant_address")
    private String permantAddress;
    @JsonProperty("per_address_line_1")
    private String perAddressLine1;
    @JsonProperty("per_address_line_2")
    private String perAddressLine2;
    @JsonProperty("per_address_line_3")
    private String perAddressLine3;
    @JsonProperty("per_landmark")
    private String perLandmark;
    @JsonProperty("per_pincode")
    private String perPincode;
    @JsonProperty("per_city")
    private String perCity;
    @JsonProperty("per_state")
    private String perState;

    // Employer Details
    @JsonProperty("employer_name")
    private String employerName;
    @JsonProperty("employement_type")
    private String employementType;
    @JsonProperty("emp_address_line_1")
    private String empAddressLine1;
    @JsonProperty("emp_address_line_2")
    private String empAddressLine2;
    @JsonProperty("emp_address_line_3")
    private String empAddressLine3;
    @JsonProperty("emp_landmark")
    private String empLandmark;
    @JsonProperty("emp_pincode")
    private String empPincode;
    @JsonProperty("emp_city")
    private String empCity;
    @JsonProperty("emp_state")
    private String empState;

    @JsonProperty("loan_amount")
    private double loanAmount;

    @JsonProperty("constitution")
    private String constitution;
////////////////////////
    // SOLICITED

    @JsonProperty("app_Id")
    private String appId;   // This is couponCode

    @JsonProperty("user_id")
    private String userId;

    // --------- Customer Data Fields

    @JsonProperty("phone_no")
    private String phoneNumber;    // This is mobile n. sent in request

    @JsonProperty("pan_no")
    private String panNumber;

    @JsonProperty("voter_id")
    private String voterId;

    @JsonProperty("passport")
    private String passport;

    @JsonProperty("loan_availed")
    private String loanAvailed;

    @JsonProperty("pan_avial")
    private String panAvailable;

    @JsonProperty("any_loan")
    private String anyLoan;

    @JsonProperty("cc_avial")
    private String ccAvailable;

    @JsonProperty("net_salary")//"647878.0",
    private double netSalary;

    @JsonProperty("tax_paid_amt")// "0.0",
    private double taxPaidAmt;

    @JsonProperty("state_code")// "15",
    private String stateCode;

    @JsonProperty("dealer_id")
    private String dealerId;

    @JsonProperty("product_code")
    private String productCode;

    @JsonProperty("score")
    private String score;

    @JsonProperty("eligibility_amt")
    private String eligibilityAmt;

    @JsonProperty("eligibility_status")
    private String eligibilityStatus;

    @JsonProperty("application_status")
    private String applicationStatus;


}
