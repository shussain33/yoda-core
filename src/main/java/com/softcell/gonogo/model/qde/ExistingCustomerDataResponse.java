package com.softcell.gonogo.model.qde;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/8/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExistingCustomerDataResponse {

    /*
     "response_code": "3",   "description": "success",   "success": "true",
   "first_name": "VIJAY",   "middle_name": "J",   "last_name": "PANVELKAR",   "dob": "1990-03-19",
   "mobile_no": "9987007555  ",
   "aadhar_no": "123654 ",   "pan_no": "QWES454R",   "voter_id": "25254565",   "driving_lc": "MH45636454",
   "loan_amount": "15000000.0000",
   "gender": "MALE",   "marital_status": "SINGLE",   "education_qualification": "PG",
   "constitution": "ASET",
   "residence_address_type": "RESI TYPE",
   "res_address_line_1": "ADDRESSS 11",
   "res_address_line_2": "ADDRESSS 22",
   "res_address_line_3": "ADDRESSS 33",
   "res_landmark": "RES LANDMARK",
   "res_pincode": "400094    ",
   "res_city": "MUMBAI",
   "res_state": "MAHARASHTRA",
   "permant_address": "PERMANANT ADDRESS",
   "per_address_line_1": "PER ADDRESS 1",
   "per_address_line_2": "PER ADDRESS 2",
   "per_address_line_3": "PER ADDRESS 3",
   "per_landmark": "PER LANDMARK 1",
   "per_pincode": "400093",
   "per_city": "MUMBAI",
   "per_state": "MAHARASHTRA",
   "employer_name": "HDB",
   "employement_type": "FINANCE",
   "emp_address_line_1": "EMP ADDRESS 1",
   "emp_address_line_2": "EMP ADDRESS 2",
   "emp_address_line_3": "EMP ADDRESS 3",
   "emp_landmark": "EMP LANDMARK",
   "emp_pincode": "4001512   ",
   "emp_city": "THANE",
   "emp_state": "MAHARASHTRA"
}
     */

    @JsonProperty("response_code")
    private String responseCode;

    @JsonProperty("success")
    private String success;

    @JsonProperty("description")
    private String description;

    // --------- Customer Data Fields
    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("middle_name")
    private String middleName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("marital_status")
    private String maritalStatus;

    @JsonProperty("education_qualification")
    private String educationQualification;

    // Mobile number
    @JsonProperty("mobile_no")
    private String mobileNumber;

    // KYC docs
    @JsonProperty("pan_no")
    private String panNumber;

    @JsonProperty("aadhar_no")
    private String aadharNumber;

    @JsonProperty("driving_lc")
    private String drivingLicence;

    @JsonProperty("voter_id")
    private String voterId;

    // Addresses
    // Residence
    @JsonProperty("residence_address_type")
    private String residenceAddressType;
    @JsonProperty("res_address_line_1")
    private String resAddressLine1;
    @JsonProperty("res_address_line_2")
    private String resAddressLine2;
    @JsonProperty("res_address_line_3")
    private String resAddressLine3;
    @JsonProperty("res_landmark")
    private String resLandmark;
    @JsonProperty("res_pincode")
    private String resPincode;
    @JsonProperty("res_city")
    private String resCity;
    @JsonProperty("res_state")
    private String resState;
    // Permanent
    @JsonProperty("permant_address")
    private String permantAddress;
    @JsonProperty("per_address_line_1")
    private String perAddressLine1;
    @JsonProperty("per_address_line_2")
    private String perAddressLine2;
    @JsonProperty("per_address_line_3")
    private String perAddressLine3;
    @JsonProperty("per_landmark")
    private String perLandmark;
    @JsonProperty("per_pincode")
    private String perPincode;
    @JsonProperty("per_city")
    private String perCity;
    @JsonProperty("per_state")
    private String perState;

    // Employer Details
    @JsonProperty("employer_name")
    private String employerName;
    @JsonProperty("employement_type")
    private String employementType;
    @JsonProperty("emp_address_line_1")
    private String empAddressLine1;
    @JsonProperty("emp_address_line_2")
    private String empAddressLine2;
    @JsonProperty("emp_address_line_3")
    private String empAddressLine3;
    @JsonProperty("emp_landmark")
    private String empLandmark;
    @JsonProperty("emp_pincode")
    private String empPincode;
    @JsonProperty("emp_city")
    private String empCity;
    @JsonProperty("emp_state")
    private String empState;

    @JsonProperty("loan_amount")
    private double loanAmount;

    @JsonProperty("constitution")
    private String constitution;

}
