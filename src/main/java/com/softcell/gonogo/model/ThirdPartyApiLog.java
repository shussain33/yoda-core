package com.softcell.gonogo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by ssg0268 on 8/11/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThirdPartyApiLog {

    private String refId;

    private String institutionId;

    private String applicantId;

    private String collateralId;

    private String acknowledgementId;

    private String requestType;

    private Date callDate = new Date();

    private Object request;

    private Object response;

    private String requestString;

    private String responseString;

    @Deprecated
    private String strResponse;

}
