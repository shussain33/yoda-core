package com.softcell.gonogo.model.search;

import java.util.Date;
import java.util.Objects;

/*
This domain class is already used from Elastic Search jar.
 */
@Deprecated
public class IndexingRequest {

    private String institutionId;

    private String product;

    private String id;

    private String document;

    private Date insertDate;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexingRequest that = (IndexingRequest) o;
        return Objects.equals(institutionId, that.institutionId) &&
                Objects.equals(product, that.product) &&
                Objects.equals(id, that.id) &&
                Objects.equals(document, that.document) &&
                Objects.equals(insertDate, that.insertDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(institutionId, product, id, document, insertDate);
    }
}
