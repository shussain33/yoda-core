package com.softcell.gonogo.model.core.request.scoring;


public class CibilRespHeader {

    private String version;

    private String memberReferenceNumber;

    private String enquiryMemberUserID;

    private String subjectReturnCode;

    private String enquiryControlNumber;

    private String dateProceed;

    private String timeProceed;

    public String getEnquiryControlNumber() {
        return enquiryControlNumber;
    }

    public void setEnquiryControlNumber(String enquiryControlNumber) {
        this.enquiryControlNumber = enquiryControlNumber;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMemberReferenceNumber() {
        return memberReferenceNumber;
    }

    public void setMemberReferenceNumber(String memberReferenceNumber) {
        this.memberReferenceNumber = memberReferenceNumber;
    }

    public String getEnquiryMemberUserID() {
        return enquiryMemberUserID;
    }

    public void setEnquiryMemberUserID(String enquiryMemberUserID) {
        this.enquiryMemberUserID = enquiryMemberUserID;
    }

    public String getSubjectReturnCode() {
        return subjectReturnCode;
    }

    public void setSubjectReturnCode(String subjectReturnCode) {
        this.subjectReturnCode = subjectReturnCode;
    }

    public String getDateProceed() {
        return dateProceed;
    }

    public void setDateProceed(String dateProceed) {
        this.dateProceed = dateProceed;
    }

    public String getTimeProceed() {
        return timeProceed;
    }

    public void setTimeProceed(String timeProceed) {
        this.timeProceed = timeProceed;
    }

    @Override
    public String toString() {
        return "CibilRespHeader [version=" + version
                + ", memberReferenceNumber=" + memberReferenceNumber
                + ", enquiryMemberUserID=" + enquiryMemberUserID
                + ", subjectReturnCode=" + subjectReturnCode
                + ", enquiryControlNumber=" + enquiryControlNumber
                + ", dateProceed=" + dateProceed + ", timeProceed="
                + timeProceed + "]";
    }


}
