package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 10/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BalanceSheet {

    @JsonProperty("sPrevYear")
    private String previousYear;

    @JsonProperty("sCurrentYear")
    private String currentYear;

    @JsonProperty("aYears")
    private List<String> yearsList;

    @JsonProperty("oShareCapital")
    private StatementFieldDetails shareCapital;

    @JsonProperty("oReservesAndSurplus")
    private StatementFieldDetails reservesAndSurplus;

    @JsonProperty("oAdvancesToGroup")
    private StatementFieldDetails advancesToGroup;

    @JsonProperty("oFromFamilyAndFriends")
    private StatementFieldDetails fromFamilyAndFriends; //From Family and Friends (non interest bearing)

    @JsonProperty("oMiscExpNotWrittenOff")
    private StatementFieldDetails miscExpNotWrittenOff;

    @JsonProperty("oTotalEquityFund")
    private StatementFieldDetails totalEquityFund;

    @JsonProperty("oSecuredDebt")
    private StatementFieldDetails securedDebt;

    @JsonProperty("oCcLimit")
    private StatementFieldDetails ccLimit;

    @JsonProperty("oUnsecuredDebtPersonal")
    private StatementFieldDetails unsecuredDebtPersonal;

    @JsonProperty("oUnsecuredDebtOthers")
    private StatementFieldDetails unsecuredDebtOthers;

    @JsonProperty("oTotalOutsideDebts")
    private StatementFieldDetails totalOutsideDebts;

    @JsonProperty("oTotalFundInBusiness")
    private StatementFieldDetails totalFundInBusiness;

    @JsonProperty("oGrossFixedAssets")
    private StatementFieldDetails grossFixedAssets;

    //Less : Depreciation
    @JsonProperty("oDepreciation")
    private StatementFieldDetails depreciation;

    @JsonProperty("oNetFixedAssets")
    private StatementFieldDetails netFixedAssets;

    @JsonProperty("oInvestments")
    private StatementFieldDetails investments;

    @JsonProperty("oCurrentAssets")
    private StatementFieldDetails currentAssets;

    @JsonProperty("oInventory")
    private StatementFieldDetails inventory;

    @JsonProperty("oDebtors")
    private StatementFieldDetails debtors;

    @JsonProperty("oDebtorsExceedsSixMonths")
    private StatementFieldDetails debtorsExceedsSixMonths;

    @JsonProperty("oDebtorsLessthanSixMonths")
    private StatementFieldDetails debtorsLessthanSixMonths;

    @JsonProperty("oLoansAndAdvancess")
    private StatementFieldDetails loansAndAdvancess; //other than those given to group co. / friends

    @JsonProperty("oOtherGenuineAdvances")
    private StatementFieldDetails otherGenuineAdvances;

    @JsonProperty("oCashAndBank")
    private StatementFieldDetails cashAndBank;

    @JsonProperty("oTotalCurrentAssets")
    private StatementFieldDetails totalCurrentAssets;

    @JsonProperty("oTotalAssets")
    private StatementFieldDetails totalAssets;

    @JsonProperty("oCurrentLiabilities")
    private StatementFieldDetails currentLiabilities;

    @JsonProperty("oCreditors")
    private StatementFieldDetails creditors;

    @JsonProperty("oOtherCurrentLiabilities")
    private StatementFieldDetails otherCurrentLiabilities;

    @JsonProperty("oProvisions")
    private StatementFieldDetails provisions; //for exps, tax, div. etc.

    @JsonProperty("oResidualCurrentAssets")
    private StatementFieldDetails residualCurrentAssets;

    @JsonProperty("oAnyWorkingCapitalLimits")
    private StatementFieldDetails anyWorkingCapitalLimits; // Any working capital limts which is not part of secured loans.

    @JsonProperty("oNetWorkingCapital")
    private StatementFieldDetails netWorkingCapital;

    @JsonProperty("oDeferredTaxAssetsLiab")
    private StatementFieldDetails deferredTaxAssetsLiab;

    @JsonProperty("oTotalApplicationOfFunds")
    private StatementFieldDetails totalApplicationOfFunds;

    @JsonProperty("oTotalFundsforBusiness")
    private StatementFieldDetails totalFundsforBusiness;

    @JsonProperty("oDiffInBalanceSheet")
    private StatementFieldDetails diffInBalanceSheet;

    @JsonProperty("oTangibleNetworth")
    private StatementFieldDetails tangibleNetworth;

    @JsonProperty("oLongTermDebt")
    private StatementFieldDetails longTermDebt;

    @JsonProperty("oShortTermDebt")
    private StatementFieldDetails shortTermDebt;

    @JsonProperty("oUnsecuredLoansPromoters")
    private StatementFieldDetails unsecuredLoansPromoters;

    @JsonProperty("oTotalLiabilities")
    private StatementFieldDetails totalLiabilities;

    @JsonProperty("oOtherCurrentAssets")
    private StatementFieldDetails otherCurrentAssets;

    @JsonProperty("oDebtRepaymentduringYear")
    private StatementFieldDetails debtRepaymentduringYear;

    @JsonProperty("oPaidupProprietorProprietor")
    private StatementFieldDetails paidupProprietorProprietor;

    @JsonProperty("oReserveSurplus")
    private StatementFieldDetails reserveSurplus;

    @JsonProperty("oOtherEquity")
    private StatementFieldDetails otherEquity;

    @JsonProperty("oLongtermBorrowing")
    private StatementFieldDetails longtermBorrowing;

    @JsonProperty("oDefferedTaxLiabilities")
    private StatementFieldDetails defferedTaxLiabilities;

    @JsonProperty("oUnsecuredLoanFromPromotors")
    private StatementFieldDetails unsecuredLoanFromPromotors;

    @JsonProperty("oOtherLongtermLiabilities")
    private StatementFieldDetails otherLongtermLiabilities;

    @JsonProperty("oLongTermProvisions")
    private StatementFieldDetails longTermProvisions;

    @JsonProperty("oTotalNonCurrentLiabilities")
    private StatementFieldDetails totalNonCurrentLiabilities;

    @JsonProperty("oShortTermBorrowing")
    private StatementFieldDetails shortTermBorrowing;

    @JsonProperty("oTradePayable")
    private StatementFieldDetails tradePayable;

    @JsonProperty("oCurrentPortionLTDebt")
    private StatementFieldDetails currentPortionLTDebt;

    @JsonProperty("oShortTermProvision")
    private StatementFieldDetails shortTermProvision;

    @JsonProperty("oTotalCurrentLiabilities")
    private StatementFieldDetails totalCurrentLiabilities;

    @JsonProperty("oTangible")
    private StatementFieldDetails tangible;

    @JsonProperty("oIntangible")
    private StatementFieldDetails intangible;

    @JsonProperty("oTotalNetFixedAssets")
    private StatementFieldDetails totalNetFixedAssets;

    @JsonProperty("oCapitalWorkInProgress")
    private StatementFieldDetails capitalWorkInProgress;

    @JsonProperty("oNonCurrentInvestments")
    private StatementFieldDetails nonCurrentInvestments;

    @JsonProperty("oDefferedTaxAssets")
    private StatementFieldDetails defferedTaxAssets;

    @JsonProperty("oLongTermLoansAdvances")
    private StatementFieldDetails longTermLoansAdvances;

    @JsonProperty("oOtherNonCurrentAssets")
    private StatementFieldDetails otherNonCurrentAssets;

    @JsonProperty("oTotalOtherNonCurrentAssets")
    private StatementFieldDetails totalOtherNonCurrentAssets;

    @JsonProperty("oCurrentInvestments")
    private StatementFieldDetails currentInvestments;

    @JsonProperty("oInventories")
    private StatementFieldDetails inventories;

    @JsonProperty("oTradeReceivables")
    private StatementFieldDetails tradeReceivables;

    @JsonProperty("oCashEquivalent")
    private StatementFieldDetails cashEquivalent;

    @JsonProperty("oShortTermLoanAdvances")
    private StatementFieldDetails shortTermLoanAdvances;

    @JsonProperty("oContingentLiabilities")
    private StatementFieldDetails contingentLiabilities;

    @JsonProperty("oOtherAssets")
    private StatementFieldDetails otherAssets;

    @JsonProperty("oSecurityDeposits")
    private StatementFieldDetails securityDeposits;

    @JsonProperty("oAccountsPayable")
    private StatementFieldDetails accountsPayable;

    @JsonProperty("oNonCurrentLiabilities")
    private StatementFieldDetails nonCurrentLiabilities;

    @JsonProperty("oSecUnsecLoan")
    private StatementFieldDetails secUnsecLoan;

    @JsonProperty("oLoanFromOthers")
    private StatementFieldDetails loanFromOthers;

    @JsonProperty("oUnsecuredLoansFromOthers")
    private StatementFieldDetails unsecuredLoansFromOthers;

    @JsonProperty("oOtherNonCurrentLiabilities")
    private StatementFieldDetails otherNonCurrentLiabilities;

    /*Share Capital
    Reserves and Surplus
    Advances to group co/friends
    From Family and Friends (non interest bearing)
    Misc. Exp. Not written off
    Total Equity Fund
    Secured Debt (Term Loans)
    Cash Credit limit (CC Limit)
    Unsecured Debt - Personal Loans
    Unsecured Debt - others (including intt bearing loans from family & friends)
    Total Outside Debts
    Total Fund in the business

    Gross Fixed Assets
    Less : Depreciation
    Net Fixed Assets
            Investments

    Current Assets
    Inventory
    Debtors    - exceeding six months
    Debtors - less than six months later
    Loans and Advances  ( other than those given to group co. / friends)
    Other genuine advances
    Cash and Bank
    Total Current Assets
    Total Assets

    Current Liabilities
    Creditors
    Other current liabilities
    Provisions (for exps, tax, div. etc.)

    Residual Current Assets
    Any working capital limts which is not part of secured loans
    Net Working Capital
    Deferred Tax Assets / (Liab.)
    Total Application of Funds
    Total Funds for the business
    Difference in B/S*/
}
