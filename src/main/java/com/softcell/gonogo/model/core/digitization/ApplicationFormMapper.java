package com.softcell.gonogo.model.core.digitization;

import com.softcell.constants.DigitizationConstant;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by mahesh on 5/6/17.
 */
public class ApplicationFormMapper {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationFormMapper.class);

    public static VelocityContext mapApplicationForm(ApplicationForm applicationForm) {
        List<Character> list;

        VelocityContext context = new VelocityContext();

        if (StringUtils.isNotBlank(applicationForm.getGngRefID())) {
            context.put("gngRefId", applicationForm.getGngRefID());
        }
        //for tvs application form
        if (StringUtils.isNotBlank(applicationForm.getGngRefID())) {
            list = GngUtils.getCharacterList(applicationForm.getGngRefID());
            context.put("referenceOrProspectNoList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getApplicationDate())) {
            list = GngUtils.getCharacterList(applicationForm.getApplicationDate());

            context.put("applicationDateList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getLosNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getLosNumber());
            context.put("losNumberList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getApplicantType())) {
            context.put("applicantType", applicationForm.getApplicantType());// list
        }
        if (StringUtils.isNotBlank(applicationForm.getProductName())) {
            context.put("productName", applicationForm.getProductName());//string
        }
        if (StringUtils.isNotBlank(applicationForm.getOccuption())) {
            context.put("occupation", applicationForm.getOccuption()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getProfession())) {
            context.put("profession", applicationForm.getProfession()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getSalariedCompanyType())) {
            context.put("salariedCompanyType", applicationForm
                    .getSalariedCompanyType().toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getBusinessCompanyType())) {
            context.put("businessCompanyType", applicationForm
                    .getBusinessCompanyType().toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getNatureOfBusiness())) {
            context.put("natureOfBusiness", applicationForm
                    .getNatureOfBusiness().toUpperCase()); // string
        }
        if (StringUtils.isNotBlank(applicationForm.getDesignation())) {
            context.put("designation", applicationForm.getDesignation()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getDesignation())) {
            list = GngUtils.getCharacterList(applicationForm.getDesignation());
            context.put("designationList", list);// string
        }
        if (StringUtils.isNotBlank(applicationForm.getLoanNumber())) {
            context.put("loanNumber", applicationForm.getLoanNumber());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getPrefix())) {
            list = GngUtils.getCharacterList(applicationForm.getPrefix());
            context.put("prefixList", list); // list
        }
        if (StringUtils.isNotBlank(applicationForm.getApplicantName())) {
            list = GngUtils.getCharacterList(applicationForm.getApplicantName());
            context.put("applicantNameList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getSalutation())) {
            list = GngUtils.getCharacterList(applicationForm.getSalutation());
            context.put("salutationList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getFatherName())) {
            list = GngUtils.getCharacterList(applicationForm.getFatherName().toUpperCase());
            context.put("fatherNameList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getSpouseName())) {
            list = GngUtils.getCharacterList(applicationForm.getSpouseName().toUpperCase());
            context.put("spouseNameList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getMotherName())) {
            list = GngUtils.getCharacterList(applicationForm.getMotherName().toUpperCase());
            context.put("motherNameList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getDateOfBirth())) {
            list = GngUtils.getCharacterList(applicationForm.getDateOfBirth());
            context.put("dateOfBirthList", list); // list
        }
        if (StringUtils.isNotBlank(applicationForm.getPanNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getPanNumber());
            context.put("panNumberList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getGender())) {
            context.put("gender", applicationForm.getGender().toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getAge())) {
            list = GngUtils.getCharacterList(applicationForm.getAge());
            context.put("ageList", list);// list
        }

        if (StringUtils.isNotBlank(applicationForm.getMaritalStatus())) {
            context.put("maritalStatus", applicationForm.getMaritalStatus()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getNumberOfDependent())) {
            list = GngUtils.getCharacterList(applicationForm.getNumberOfDependent());
            context.put("numberOfDependentList", list); // list
        }
        if (StringUtils.isNotBlank(applicationForm.getEducation())) {
            context.put("education", applicationForm.getEducation()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getMobile())) {
            context.put("mobileNo", applicationForm.getMobile());
            list = GngUtils.getCharacterList(applicationForm.getMobile());
            context.put("mobileList", list); // list
        }
        if (StringUtils.isNotBlank(applicationForm.getReligion())) {
            list = GngUtils.getCharacterList(applicationForm.getReligion());// list
            context.put("religionList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getCast())) {
            context.put("cast", applicationForm.getCast().toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getEmailID())) {
            context.put("emailID", applicationForm.getEmailID());
            list = GngUtils.getCharacterList(applicationForm.getEmailID());// list
            context.put("emailIDList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getDrivingLicenseNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getDrivingLicenseNumber());// list
            context.put("drivingLicenseNumberList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getDrivingLicenseExpDate())) {
            list = GngUtils.getCharacterList(applicationForm.getDrivingLicenseExpDate());// list
            context.put("drivingLicenseExpDate", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getAadharNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getAadharNumber());// list
            context.put("aadharNumberList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getVoterIDNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getVoterIDNumber());// list
            context.put("voterIDNumberList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPassportNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getPassportNumber());// list
            context.put("passportNumberList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getPassportExpiryDate())) {
            list = GngUtils.getCharacterList(applicationForm.getPassportExpiryDate());// list
            context.put("passportExpDate", list);
        }

		/*----------------Residense Addess Details Start Here---------------------------*/

        if (StringUtils.isNotBlank(applicationForm.getPresentResidence())) {
            context.put("presentResidence",
                    applicationForm.getPresentResidence().toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getMonthlyResidenceAddRent())
                && ! StringUtils.equals(applicationForm.getMonthlyResidenceAddRent(), DigitizationConstant.ZERO.toFaceValue())) {
            list = GngUtils.getCharacterList(applicationForm.getMonthlyResidenceAddRent());// list
            context.put("residenceMonthlyRentList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getResidenseAddress())) {
            list = GngUtils.getCharacterList(applicationForm.getResidenseAddress());
            context.put("residenseAddressList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getResidenseState())) {
            list = GngUtils.getCharacterList(applicationForm.getResidenseState());// list
            context.put("residenseStateList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getResidenseCity())) {
            list = GngUtils.getCharacterList(applicationForm.getResidenseCity());// list
            context.put("residenseCityList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getResidensePin())) {
            list = GngUtils.getCharacterList(applicationForm.getResidensePin());// list
            context.put("residensePinList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getResidenseStd())) {
            list = GngUtils.getCharacterList(applicationForm.getResidenseStd());// list
            context.put("residenseStdList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getResidenceMobile())) {
            list = GngUtils.getCharacterList(applicationForm.getResidenceMobile());// list
            context.put("residenseMobList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getResidenseTelephone())) {
            list = GngUtils.getCharacterList(applicationForm.getResidenseTelephone());// list
            context.put("residenseTelephoneList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getYearAtResidense())) {
            list = GngUtils.getCharacterList(applicationForm.getYearAtResidense());// list
            context.put("yearAtResidenseList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getYearAtCity())) {
            list = GngUtils.getCharacterList(applicationForm.getYearAtCity());// list
            context.put("yearAtCityResidence", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getResidenseLandMark())) {
            list = GngUtils.getCharacterList(applicationForm.getResidenseLandMark());// list
            context.put("residenceLandMarkList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getMonthAtAddress())) {
            list = GngUtils.getCharacterList(applicationForm.getMonthAtAddress());// list
            context.put("monthAtResAddressList", list);
        }





		/*-------------------------Residense Address Details End Here--------------------------------------------------*/

		/*-------------------------Employment  Details Start Here--------------------------------------------------*/
        if (StringUtils.isNotBlank(applicationForm.getEmployerName())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerName());// list
            context.put("employerNameList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerCity())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerCity());
            context.put("employerCityList", list);// list
        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerExtension())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerExtension());// list
            context.put("employerExtensionList", list);

        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerAddress())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerAddress());// list
            context.put("employerAddressList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerCombAddress())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerCombAddress());// list
            context.put("employerCombAddressList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerLandMark())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerLandMark());// list
            context.put("employerLandMarkList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerPhone())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerPhone());// list
            context.put("employerPhoneList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getEmployerState())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerState());// list
            context.put("employerStateList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerPin())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerPin());// list
            context.put("employerPinList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getEmployerStd())) {
            list = GngUtils.getCharacterList(applicationForm.getEmployerStd());// list
            context.put("employerStdList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getTotalYearInBusiness())) {
            list = GngUtils.getCharacterList(applicationForm.getTotalYearInBusiness());// list
            context.put("totalYearInBusinessList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getNumberOfYearInCurrentBusiness())) {
            list = GngUtils.getCharacterList(applicationForm
                    .getNumberOfYearInCurrentBusiness());// list
            context.put("numberOfYearInCurrentBusinessList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPreferredMailingAddress())) {
            context.put("preferredMailingAddress", applicationForm
                    .getPreferredMailingAddress().toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getOfficeEmailID())) {
            list = GngUtils.getCharacterList(applicationForm.getOfficeEmailID());// list
            context.put("officeEmailIDList", list);
        }

        if(StringUtils.isNotBlank(applicationForm.getEmploymentType())){
            context.put("employmentType",applicationForm.getEmploymentType());
        }

		/*-------------------------Employment  Details End Here--------------------------------------------------*/

		/*-------------------------Permanemt Address  Details Start Here--------------------------------------------------*/
        if (StringUtils.isNotBlank(applicationForm.getPermanentResidence())) {
            context.put("permanentResidence",
                    applicationForm.getPermanentResidence());
        }
        if (StringUtils.isNotBlank(applicationForm.getPermanentAddress())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentAddress());// list
            context.put("permanentAddressList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPermanentCombAddress())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentCombAddress());// list
            context.put("prmntCmbndAddressList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getMonthlyPermenentAddRent())
                && !StringUtils.equals(applicationForm.getMonthlyPermenentAddRent(), DigitizationConstant.ZERO.toFaceValue())) {
            list = GngUtils.getCharacterList(applicationForm.getMonthlyPermenentAddRent());// list
            context.put("permanentMonthlyRentList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getPermanentLandMark())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentLandMark());// list
            context.put("permanentLandMarkList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPermanentState())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentState());// list
            context.put("permanentStateList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPermanentCity())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentCity());// list
            context.put("permanentCityList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPermanentPin())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentPin());// list
            context.put("permanentPinList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPermanentStd())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentStd());// list
            context.put("permanentStdList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getPermanentTelephone())) {
            list = GngUtils.getCharacterList(applicationForm.getPermanentTelephone());// list
            context.put("permanentTelephoneList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getLogo())) {
            context.put("logo", applicationForm.getLogo());

        }

        /**
         * second page fields start here for application form
         */

        if (StringUtils.isNotBlank(applicationForm.getAnnualSalary())) {
            list = GngUtils.getCharacterList(applicationForm.getAnnualSalary());
            context.put("annualSalaryList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getOtherAnnualSalary())) {
            list = GngUtils.getCharacterList(applicationForm.getOtherAnnualSalary());
            context.put("otherAnnualSalaryList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getBankName())) {
            context.put("bankName", applicationForm.getBankName());
            list = GngUtils.getCharacterList(applicationForm.getBankName());
            context.put("bankNameList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getAccountHolderName())) {
            context.put("accountHolderName", applicationForm.getAccountHolderName());
            list = GngUtils.getCharacterList(applicationForm.getAccountHolderName());
            context.put("accountHolderNameList", list);
        }
        if(StringUtils.isNotBlank(applicationForm.getIfscCode())){
            list = GngUtils.getCharacterList(applicationForm.getIfscCode());
            context.put("ifscCodeList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getYearHeld())) {
            list = GngUtils.getCharacterList(applicationForm.getYearHeld());
            context.put("yearOpendList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getBankBranchName())) {
            list = GngUtils.getCharacterList(applicationForm.getBankBranchName());
            context.put("branchList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getCustomerID())) {
            list = GngUtils.getCharacterList(applicationForm.getCustomerID());// list
            context.put("customerIDList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getAccountNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getAccountNumber());
            context.put("accountNumberList", list);
        }
        if(StringUtils.isNotBlank(applicationForm.getChequeNumber())){
            list = GngUtils.getCharacterList(applicationForm.getAccountNumber());
            context.put("checkNumberList" ,list);
        }
        if (StringUtils.isNotBlank(applicationForm.getTypeOfAccount())) {
            context.put("typeOfAccount", applicationForm.getTypeOfAccount()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getCreditCardNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getCreditCardNumber());// list
            context.put("creditCardNumberList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getCreditCardExpiryDate())) {
            list = GngUtils.getCharacterList(applicationForm.getCreditCardExpiryDate());// list
            context.put("creditCardExpiryDateList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getCreditCardBankName())) {
            list = GngUtils.getCharacterList(applicationForm.getCreditCardBankName().toUpperCase());// list
            context.put("creditCardBankNameList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getDebitCardNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getDebitCardNumber());// list
            context.put("debitCardNumberList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getCreditCardType())) {
            context.put("creditCardType", applicationForm.getCreditCardType().toUpperCase());
        }
        if (StringUtils.isNotBlank(applicationForm.getCreditCardType())) {
            context.put("creditCardType", applicationForm.getCreditCardType()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getAssetOwned())) {
            context.put("assetOwned", applicationForm.getAssetOwned()
                    .toUpperCase());// string
        }
        if (applicationForm.getLoanAmount() != 0) {
            list = GngUtils.getCharacterList(String.valueOf(applicationForm
                    .getLoanAmount()));// list
            context.put("loanAmountList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getRevisedLoanAmount())) {
            list = GngUtils.getCharacterList(String.valueOf(applicationForm
                    .getRevisedLoanAmount()));// list
            context.put("revisedLoanAmountList", list); // newly added loan amount
        }

        if(StringUtils.isNotBlank(applicationForm.getInsEwAmtTotal())){

            list = GngUtils.getCharacterList(applicationForm
                    .getInsEwAmtTotal());// list
            context.put("ewInsAmtTotalList", list); // newly added loan amount
        }

        if (StringUtils.isNotBlank(applicationForm.getFirstEmiDueDtInDDmmYYYY())) {
            list = GngUtils.getCharacterList(applicationForm.getFirstEmiDueDtInDDmmYYYY());// list
            context.put("emiDueDateList", list); // newly added loan amount
        }
        if (StringUtils.isNotBlank(applicationForm.getTotalDownPayment())) {
            list = GngUtils.getCharacterList(applicationForm
                    .getTotalDownPayment());// list
            context.put("totalDownPaymentList", list); // newly added loan amount
        }

        if (StringUtils.isNotBlank(applicationForm.getRevisedEmiAmount())) {
            list = GngUtils.getCharacterList(applicationForm
                    .getRevisedEmiAmount());// list
            context.put("revisedEmiAmountList", list); // newly added loan amount
        }
        if (StringUtils.isNotBlank(applicationForm.getTenure())) {
            list = GngUtils.getCharacterList(applicationForm.getTenure());// list
            context.put("tenureList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getAdvanceEMI())) {
            list = GngUtils.getCharacterList(applicationForm.getAdvanceEMI());// list
            context.put("advanceEMIList", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getDealerName())) {
            list = GngUtils.getCharacterList(applicationForm.getDealerName().toUpperCase());
            context.put("dealerNameList", list);// string
        }
        if (StringUtils.isNotBlank(applicationForm.getProductType())) {
            context.put("productType", applicationForm.getProductType()
                    .toUpperCase());// string
        }
        //for tvs purpose
        if (StringUtils.isNotBlank(applicationForm.getProductType())) {
            list = GngUtils.getCharacterList(applicationForm.getProductType().toUpperCase());
            context.put("productTypeList",list);// string
        }

        //added for tvs application form purpose
        if (StringUtils.isNotBlank(applicationForm.getManufacturer())) {
            list = GngUtils.getCharacterList(applicationForm.getManufacturer().toUpperCase());
            context.put("manufacturerList", list);// string
        }

        if (StringUtils.isNotBlank(applicationForm.getManufacturer())) {
            context.put("manufacturer", applicationForm.getManufacturer()
                    .toUpperCase());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getModel())) {
            context.put("model", applicationForm.getModel().toUpperCase());// string
        }
        if(StringUtils.isNotBlank(applicationForm.getModel()) && StringUtils.isNotBlank(applicationForm.getManufacturer())){
            String productAndModelNo= applicationForm.getManufacturer()+' ' +applicationForm.getModel();
            list = GngUtils.getCharacterList(productAndModelNo);

            context.put("modelNo_assetNameList", list);// string
        }

        //for tvs purpose
        if (StringUtils.isNotBlank(applicationForm.getModel())) {
            list = GngUtils.getCharacterList(applicationForm.getModel().toUpperCase());

            context.put("modelNoList", list);// string
        }
        //total asset cost for tvs
        if (StringUtils.isNotBlank(applicationForm.getTotalAssetCost())) {
            list = GngUtils.getCharacterList(applicationForm.getTotalAssetCost().toUpperCase());

            context.put("totalAssetCostList", list);// string
        }
        //for tvs i.e Applied Loan Amount on application form
        if (StringUtils.isNotBlank(applicationForm.getFinanceAmount())) {
            list = GngUtils.getCharacterList(applicationForm.getFinanceAmount().toUpperCase());

            context.put("financeAmountList", list);// string
        }
        //for tvs application form

            list = GngUtils.getCharacterList(String.valueOf(applicationForm.getEmi()));

            context.put("emiList", list);// string

        if (StringUtils.isNotBlank(applicationForm.getOtherCharges())) {
            list = GngUtils.getCharacterList(applicationForm.getOtherCharges().toUpperCase());

            context.put("otherChargesList", list);// string
        }
        if (StringUtils.isNotBlank(applicationForm.getAdvanceEMITenor())) {
            list = GngUtils.getCharacterList(applicationForm.getAdvanceEMITenor().toUpperCase());

            context.put("advanceEmiTenureList", list);// string
        }


        /**
         * reference details information start here.
         */
        if (StringUtils.isNotBlank(applicationForm.getRefName1())) {
            context.put("refName1", applicationForm.getRefName1());
            list = GngUtils.getCharacterList(applicationForm.getRefName1());// list
            context.put("refName1List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefRelationshipWithApplicant1())) {
            list = GngUtils.getCharacterList(applicationForm
                    .getRefRelationshipWithApplicant1());// list
            context.put("refRelationshipWithApplicant1List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefResidentialAddress1())) {
            list = GngUtils.getCharacterList(applicationForm.getRefResidentialAddress1());// list
            context.put("refResidentialAddress1List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefCity1())) {
            list = GngUtils.getCharacterList(applicationForm.getRefCity1());// list
            context.put("refCity1List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefState1())) {
            list = GngUtils.getCharacterList(applicationForm.getRefState1());// list
            context.put("refState1List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefPin1())) {
            list = GngUtils.getCharacterList(applicationForm.getRefPin1());// list
            context.put("refPin1List", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getRefSTD1())) {
            list = GngUtils.getCharacterList(applicationForm.getRefSTD1());// list
            context.put("refSTD1List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefPhone1())) {
            list = GngUtils.getCharacterList(applicationForm.getRefPhone1());// list
            context.put("refPhone1List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefMobile1())) {
            list = GngUtils.getCharacterList(applicationForm.getRefMobile1());// list
            context.put("refMobile1List", list);
        }
        /**
         * information regarding  reference two
         */

        if (StringUtils.isNotBlank(applicationForm.getRefName2())) {
            context.put("refName2", applicationForm.getRefName2());
            list = GngUtils.getCharacterList(applicationForm.getRefName2());// list
            context.put("refName2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefRelationshipWithApplicant2())) {
            list = GngUtils.getCharacterList(applicationForm
                    .getRefRelationshipWithApplicant2());// list
            context.put("refRelationshipWithApplicant2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefResidentialAddress2())) {
            list = GngUtils.getCharacterList(applicationForm.getRefResidentialAddress2());// list
            context.put("refResidentialAddress2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefCity2())) {
            list = GngUtils.getCharacterList(applicationForm.getRefCity2());// list
            context.put("refCity2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefState2())) {
            list = GngUtils.getCharacterList(applicationForm.getRefState2());// list
            context.put("refState2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefPin2())) {
            list = GngUtils.getCharacterList(applicationForm.getRefPin2());// list
            context.put("refPin2List", list);
        }

        if (StringUtils.isNotBlank(applicationForm.getRefSTD2())) {
            list = GngUtils.getCharacterList(applicationForm.getRefSTD2());// list
            context.put("refSTD2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefPhone2())) {
            list = GngUtils.getCharacterList(applicationForm.getRefPhone2());// list
            context.put("refPhone2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRefMobile2())) {
            list = GngUtils.getCharacterList(applicationForm.getRefMobile2());// list
            context.put("refMobile2List", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getApplicationFees())) {
            context.put("applicationFees", applicationForm.getApplicationFees());// string
        }

        /**
         * ins
         */
        if (StringUtils.isNotBlank(applicationForm.getSoCode())) {
            list = GngUtils.getCharacterList(applicationForm.getSoCode());// list
            context.put("soCodeList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getSmCode())) {
            list = GngUtils.getCharacterList(applicationForm.getSmCode());// list
            context.put("smCodeList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getAsmCmCode())) {
            list = GngUtils.getCharacterList(applicationForm.getAsmCmCode());// list
            context.put("asmCmCodeList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getTmAsmCode())) {
            list = GngUtils.getCharacterList(applicationForm.getTmAsmCode());// list
            context.put("tmAsmCodeList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getRmCode())) {
            list = GngUtils.getCharacterList(applicationForm.getRmCode());// list
            context.put("rmCodeList", list);

        }
        if (StringUtils.isNotBlank(applicationForm.getDsaCode())) {
            list = GngUtils.getCharacterList(applicationForm.getDsaCode());// list
            context.put("dsaCodeList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getDealerCode())) {
            list = GngUtils.getCharacterList(applicationForm.getDealerCode());// list
            context.put("dealerCodeList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getSchemeCode())) {
            list = GngUtils.getCharacterList(applicationForm.getSchemeCode());// list
            context.put("schemeCodeList", list);

        }
        if (StringUtils.isNotBlank(applicationForm.getBranchName())) {
            list = GngUtils.getCharacterList(applicationForm.getBranchName().toUpperCase());// string
            context.put("branchNameList", list);

        }
        // this is for tvs application form purpose
        if (StringUtils.isNotBlank(applicationForm.getBranchName())) {
            context.put("branchName", applicationForm.getBranchName()
                    .toUpperCase());// string

        }

        if (StringUtils.isNotBlank(applicationForm.getLocation())) {
            list = GngUtils.getCharacterList(applicationForm.getLocation());// list
            context.put("locationList", list);

        }
        if (StringUtils.isNotBlank(applicationForm.getDateAndTimeOfReceipt())) {
            context.put("dateAndTimeOfReceipt",
                    applicationForm.getDateAndTimeOfReceipt());// string

        }
        if (StringUtils.isNotBlank(applicationForm.getHdbContactPersonName())) {
            list = GngUtils.getCharacterList(applicationForm.getHdbContactPersonName());// list
            context.put("hdbContactPersonNameList", list);

        }
        if (StringUtils.isNotBlank(applicationForm.getContactNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getContactNumber());// list
            context.put("contactNumberList", list);
        }
        if (StringUtils.isNotBlank(applicationForm.getApplicantPhoto())) {
            context.put("applicantPhoto", applicationForm.getApplicantPhoto());
        }
        if (StringUtils.isNotBlank(applicationForm.getDateOfApplication())) {
            list = GngUtils.getCharacterList(applicationForm.getDateOfApplication());// list
            context.put("dateOfApplicationList", list);
        }
        if(StringUtils.isNotBlank(applicationForm.getResAddressSameAsPer())){
            context.put("resAddressSameAsPer", applicationForm.getResAddressSameAsPer());
        }
        if(StringUtils.isNotBlank(applicationForm.getPanNumber())){
            context.put("panCardProof", "true");
        }
        if(StringUtils.isNotBlank(applicationForm.getAadharNumber())){
            context.put("aadharCardProof","true");

        }
        if(StringUtils.isNotBlank(applicationForm.getDrivingLicenseNumber())){
            context.put("drivingLicenceProof","true");

        }
        if (StringUtils.isNotBlank(applicationForm.getLpgId())) {
            context.put("lpgProof", "true");
            list = GngUtils.getCharacterList(applicationForm.getLpgId());// list
            context.put("lpgIdList", list);
        }

        if(StringUtils.isNotBlank(applicationForm.getVoterIDNumber())){
            context.put("voterIdProof","true");

        }
        if(StringUtils.isNotBlank(applicationForm.getPassportNumber())){
            context.put("passportProof","true");

        }
        if (StringUtils.isNotBlank(applicationForm.getAddressProof())) {
            list = GngUtils.getCharacterList(applicationForm.getAddressProof());// list
            context.put("addressProofList",list);

        }

        if(StringUtils.isNotEmpty(applicationForm.getBankSurrogateProof())){
            context.put("bankSurrogateProof",applicationForm.getBankSurrogateProof());
        }
        if(StringUtils.isNotEmpty(applicationForm.getBusinessSurrogateProof())){
            context.put("businessSurrogateProof",applicationForm.getBusinessSurrogateProof());
        }
        if(StringUtils.isNotEmpty(applicationForm.getSalarySlipSurrogate())){
            context.put("salarySlipSurrogateProof",applicationForm.getSalarySlipSurrogate());
        }

        //applicant income details
        if(StringUtils.isNotBlank(applicationForm.getGrossMonthlyIncome())){
            list = GngUtils.getCharacterList(applicationForm.getGrossMonthlyIncome());// list
            context.put("grossMonthlyIncomeList", list);
        }

        if(StringUtils.isNotBlank(applicationForm.getAdditionalIncomeSource())){
            list = GngUtils.getCharacterList(applicationForm.getAdditionalIncomeSource());// list
            context.put("additionalIncomeSourceList", list);
        }
        if(StringUtils.isNotBlank(applicationForm.getElectricityNo())){
            list = GngUtils.getCharacterList(applicationForm.getElectricityNo());// list
            context.put("electricityNoList", list);
        }

        //dsa details
        if(StringUtils.isNotBlank(applicationForm.getDsaName())){
            list = GngUtils.getCharacterList(applicationForm.getDsaName());// list
            context.put("dsaNameList", list);
        }
        if(StringUtils.isNotBlank(applicationForm.getDsaMoNumber())){
            list = GngUtils.getCharacterList(applicationForm.getDsaMoNumber());// list
            context.put("dsaMoNumberList", list);
        }

        if(StringUtils.isNotBlank(applicationForm.getRelationWithTvs())){
            context.put("relationWithTvs",applicationForm.getRelationWithTvs());
        }

        if(StringUtils.isNotBlank(applicationForm.getCreditCardLnAccNo())){

            list = GngUtils.getCharacterList(applicationForm.getCreditCardLnAccNo());// list
            context.put("creditCardLnNoList",list);
        }
        if(StringUtils.isNotBlank(applicationForm.getCustomerIRR())){

            list = GngUtils.getCharacterList(applicationForm.getCustomerIRR());// list
            context.put("customerIrrList",list);
        }
        if(StringUtils.isNotBlank(applicationForm.getRateOfInterest())){

            list = GngUtils.getCharacterList(applicationForm.getRateOfInterest());// list
            context.put("rateOfInterestList",list);
        }
        if(StringUtils.isNotBlank(applicationForm.getTotalFamMember())){

            list = GngUtils.getCharacterList(applicationForm.getTotalFamMember());// list
            context.put("totlNoOfFmlyMbrList",list);
        }

        if(StringUtils.isNotBlank(applicationForm.getEarningMember())){

            list = GngUtils.getCharacterList(applicationForm.getEarningMember());// list
            context.put("totlNoOfErnMbrList",list);
        }
        if(StringUtils.isNotBlank(applicationForm.getCarType())){

            list = GngUtils.getCharacterList(applicationForm.getCarType());// list
            context.put("carType",list);
        }

        if(StringUtils.isNotBlank(applicationForm.getOfficeMobile())){

            list = GngUtils.getCharacterList(applicationForm.getOfficeMobile());// list
            context.put("officeMobileList",list);
        }
        if(StringUtils.isNotBlank(applicationForm.getGstInNo())){
            list = GngUtils.getCharacterList(applicationForm.getGstInNo());// list
            context.put("gstInNoList",list);
        }
        if (StringUtils.isNotBlank(applicationForm.getGstEffectiveDate())) {
            list = GngUtils.getCharacterList(applicationForm.getGstEffectiveDate());
            context.put("gstEffectiveDateList", list);
        }

        return context;
    }

}
