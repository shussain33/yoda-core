package com.softcell.gonogo.model.core.digitization;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahesh on 17/12/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AchMandateForm {

    private String applicationDate;
    private String hdbfsLogo;
    private String bankName;
    private String banckAccNo;
    private String ifscCode;
    private String revisedEmi;
    private String mobileNo;
    private String emailId;
    private String accHolderName;
    private String financeAmount;
    private String tickMark;
    private String tickMarkWithBox;
    private String crossBox;
    private String indianRupee;
    private String scissor;


}
