package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.*;
@XmlRootElement(name="KycRes")
@XmlAccessorType(XmlAccessType.FIELD)
public class KycResp {
	@XmlAttribute(name = "ret")
	private String ret;
	@XmlAttribute(name = "code")
	private String code;
	@XmlAttribute(name = "txn")
	private String txn;
	@XmlAttribute(name = "err")
	private String err;
	@XmlAttribute(name = "ts")
	private String ts;
	@XmlElement(name="Rar")
	private String rar;
	@XmlElement(name="UidData")
	private UidData uidData;
	@XmlElement(name="Signature")
	private String signature;
	public String getRet() {
		return ret;
	}
	public void setRet(String ret) {
		this.ret = ret;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTxn() {
		return txn;
	}
	public void setTxn(String txn) {
		this.txn = txn;
	}
	public String getErr() {
		return err;
	}
	public void setErr(String err) {
		this.err = err;
	}
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getRar() {
		return rar;
	}
	public void setRar(String rar) {
		this.rar = rar;
	}
	public UidData getUidData() {
		return uidData;
	}
	public void setUidData(UidData uidData) {
		this.uidData = uidData;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	@Override
	public String toString() {
		return "KycResp [ret=" + ret + ", code=" + code + ", txn=" + txn
				+ ", err=" + err + ", ts=" + ts + ", rar=" + rar + ", uidData="
				+ uidData + ", signature=" + signature + "]";
	}
	
	
}
