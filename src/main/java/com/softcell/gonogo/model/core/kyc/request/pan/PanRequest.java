package com.softcell.gonogo.model.core.kyc.request.pan;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.kyc.KycHeader;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author yogeshb
 */
@Document(collection = "PanRequest")
public class PanRequest extends AuditEntity {
    @Id
    @JsonIgnore
    private String gngRefId;

    @JsonIgnore
    private Date dateTime = new Date();

    @JsonProperty("HEADER")
    private KycHeader header;

    @JsonProperty("KYC-REQUEST")
    private KycRequest kycRequest;

    public KycHeader getHeader() {
        return header;
    }

    public void setHeader(KycHeader header) {
        this.header = header;
    }

    public KycRequest getKycRequest() {
        return kycRequest;
    }

    public void setKycRequest(KycRequest kycRequest) {
        this.kycRequest = kycRequest;
    }

    public String getGngRefId() {
        return gngRefId;
    }

    public void setGngRefId(String gngRefId) {
        this.gngRefId = gngRefId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PanRequest{");
        sb.append("gngRefId='").append(gngRefId).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", header=").append(header);
        sb.append(", kycRequest=").append(kycRequest);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PanRequest)) return false;
        PanRequest that = (PanRequest) o;
        return Objects.equal(getGngRefId(), that.getGngRefId()) &&
                Objects.equal(getDateTime(), that.getDateTime()) &&
                Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getKycRequest(), that.getKycRequest());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getGngRefId(), getDateTime(), getHeader(), getKycRequest());
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {

        private PanRequest panRequest = new PanRequest();

        public PanRequest build(){
            return this.panRequest;
        }

        public Builder header(KycHeader header){
            this.panRequest.setHeader(header);
            return this;
        }

        public Builder kycRequest(KycRequest kycRequest){
            this.panRequest.setKycRequest(kycRequest);
            return this;
        }

        public Builder dateTime(Date date){
            this.panRequest.setDateTime(date);
            return this;
        }

        public Builder gngRefId(String gngRefId){
            this.panRequest.setGngRefId(gngRefId);
            return this;
        }

    }
}
