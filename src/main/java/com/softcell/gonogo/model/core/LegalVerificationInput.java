package com.softcell.gonogo.model.core;

import java.util.*;
import lombok.*;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Name;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LegalVerificationInput{

    @JsonProperty("oApplicantName")
    private Name applicantName;

    @JsonProperty("aCoApplName")
    private List<Name> coApplicantName;

    @JsonProperty("aOwnerNames")
    private List<Name> propOwnerName;

    @JsonProperty("sPropType")
    private String propType;

    @JsonProperty("sPropUsage")
    private String propUsage;

    @JsonProperty("sDocDetails")
    private String docDetails;

    @JsonProperty("oPropertyAddress")
    private CustomerAddress propertyAddress;

}
