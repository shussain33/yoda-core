package com.softcell.gonogo.model.core.kyc.response;

import com.google.common.base.Objects;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponse;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "panServiceResponse")
public class PanServiceResponse extends AuditEntity {

    private String refId;
    private String coApplicantId;
    private PanResponse panResponse;

    public String getCoApplicantId() {
        return coApplicantId;
    }

    public void setCoApplicantId(String coApplicantId) {
        this.coApplicantId = coApplicantId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public PanResponse getPanResponse() {
        return panResponse;
    }

    public void setPanResponse(PanResponse panResponse) {
        this.panResponse = panResponse;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PanServiceResponse{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", panResponse=").append(panResponse);
        sb.append(", coApplicantId='").append(coApplicantId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PanServiceResponse)) return false;
        PanServiceResponse that = (PanServiceResponse) o;
        return Objects.equal(getRefId(), that.getRefId()) &&
                Objects.equal(getPanResponse(), that.getPanResponse()) &&
                Objects.equal(getCoApplicantId(), that.getCoApplicantId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getRefId(), getPanResponse(), getCoApplicantId());
    }
}
