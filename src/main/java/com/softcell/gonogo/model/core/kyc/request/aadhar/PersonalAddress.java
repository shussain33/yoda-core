package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;


/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonalAddress {

    @JsonProperty("MATCH-STRATEGY")
    private String matchStrategy;

    @JsonProperty("CARE-OF")
    private String careOf;

    @JsonProperty("HOUSE-NO")
    private String houseNo;

    @JsonProperty("STREET")
    private String street;

    @JsonProperty("LANDMARK")
    private String landMark;

    @JsonProperty("LOCATION")
    private String location;

    @JsonProperty("VILLAGE-TOWN-CITY")
    private String villageTownCity;

    @JsonProperty("SUBDIST")
    private String subDist;

    @JsonProperty("DIST")
    private String dist;

    @JsonProperty("STATE")
    private String state;

    @JsonProperty("POSTAL-CODE")
    private String postalCode;

    @JsonProperty("POST-OFFICE")
    private String postOffice;

    @JsonIgnore
    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getMatchStrategy() {
        return matchStrategy;
    }

    public void setMatchStrategy(String matchStrategy) {
        this.matchStrategy = matchStrategy;
    }

    public String getCareOf() {
        return careOf;
    }

    public void setCareOf(String careOf) {
        this.careOf = careOf;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLandMark() {
        return landMark;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getVillageTownCity() {
        return villageTownCity;
    }

    public void setVillageTownCity(String villageTownCity) {
        this.villageTownCity = villageTownCity;
    }

    public String getSubDist() {
        return subDist;
    }

    public void setSubDist(String subDist) {
        this.subDist = subDist;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PersonalAddress{");
        sb.append("matchStrategy='").append(matchStrategy).append('\'');
        sb.append(", careOf='").append(careOf).append('\'');
        sb.append(", houseNo='").append(houseNo).append('\'');
        sb.append(", street='").append(street).append('\'');
        sb.append(", landMark='").append(landMark).append('\'');
        sb.append(", location='").append(location).append('\'');
        sb.append(", villageTownCity='").append(villageTownCity).append('\'');
        sb.append(", subDist='").append(subDist).append('\'');
        sb.append(", dist='").append(dist).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", postalCode='").append(postalCode).append('\'');
        sb.append(", postOffice='").append(postOffice).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonalAddress)) return false;
        PersonalAddress that = (PersonalAddress) o;
        return Objects.equal(getMatchStrategy(), that.getMatchStrategy()) &&
                Objects.equal(getCareOf(), that.getCareOf()) &&
                Objects.equal(getHouseNo(), that.getHouseNo()) &&
                Objects.equal(getStreet(), that.getStreet()) &&
                Objects.equal(getLandMark(), that.getLandMark()) &&
                Objects.equal(getLocation(), that.getLocation()) &&
                Objects.equal(getVillageTownCity(), that.getVillageTownCity()) &&
                Objects.equal(getSubDist(), that.getSubDist()) &&
                Objects.equal(getDist(), that.getDist()) &&
                Objects.equal(getState(), that.getState()) &&
                Objects.equal(getPostalCode(), that.getPostalCode()) &&
                Objects.equal(getPostOffice(), that.getPostOffice());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getMatchStrategy(), getCareOf(), getHouseNo(), getStreet(), getLandMark(), getLocation(), getVillageTownCity(), getSubDist(), getDist(), getState(), getPostalCode(), getPostOffice());
    }

    
    public static class Builder {
        private PersonalAddress personalAddress = new PersonalAddress();

        public PersonalAddress build() {
            return personalAddress;
        }

        public Builder matchStrategy(String matchStrategy) {
            this.personalAddress.matchStrategy = matchStrategy;
            return this;
        }

        public Builder careOf(String careOf) {
            this.personalAddress.careOf = careOf;
            return this;
        }

        public Builder houseNo(String houseNo) {
            this.personalAddress.houseNo = houseNo;
            return this;
        }

        public Builder street(String street) {
            this.personalAddress.street = street;
            return this;
        }

        public Builder landMark(String landMark) {
            this.personalAddress.landMark = landMark;
            return this;
        }

        public Builder location(String location) {
            this.personalAddress.location = location;
            return this;
        }

        public Builder villageTownCity(String villageTownCity) {
            this.personalAddress.villageTownCity = villageTownCity;
            return this;
        }

        public Builder subDist(String subDist) {
            this.personalAddress.subDist = subDist;
            return this;
        }

        public Builder dist(String dist) {
            this.personalAddress.dist = dist;
            return this;
        }

        public Builder state(String state) {
            this.personalAddress.state = state;
            return this;
        }

        public Builder postalCode(String postalCode) {
            this.personalAddress.postalCode = postalCode;
            return this;
        }

        public Builder postOffice(String postOffice) {
            this.personalAddress.postOffice = postOffice;
            return this;
        }
    }
}
