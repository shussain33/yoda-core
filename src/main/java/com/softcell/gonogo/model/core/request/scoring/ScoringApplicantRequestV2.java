package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.adroit.VerificationRequest;
import com.softcell.gonogo.model.core.DedupeMatch;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.mifin.Verification;
import com.softcell.gonogo.model.ops.LoanCharges;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * Created by yogesh on 9/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "ScoringApplicantRequest")
public class ScoringApplicantRequestV2 {


    // @Id ToDo migrate previous data according to _id
    @JsonIgnore
    private String gngRefId;

    @JsonIgnore
    private Date dateTime = new Date();

    @JsonProperty
    private String policyName;
    /**
     * scoring header
     */
    @JsonProperty("HEADER")
    ScoringHeader header;

    @JsonProperty("APPLICANTS")
    private List<ApplicantRequest> applicants;

    @JsonProperty("APPLICATION")
    private Application application;

    @JsonProperty("FINANCIAL_DATA")
    private FinancialData finData;

    @JsonProperty("CAM_DETAIL")
    private CamDetails camDetails;

    @JsonProperty("VALUATION_REQ")
    private  Valuation valuation;

    @JsonProperty("VERIFICATION")
    private VerificationDetails verification;

    @JsonProperty("DEDUPE_MATCH")
    private DedupeMatch dedupeMatch;

    @JsonProperty("LOAN_CHARGES")
    private LoanCharges loanCharges;

    @JsonProperty("ELIGIBILITY")
    private EligibilityDetails eligibility;

    //For set request in String Format
    private String request;
}