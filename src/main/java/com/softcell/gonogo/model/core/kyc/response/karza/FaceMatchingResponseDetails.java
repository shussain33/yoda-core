package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class FaceMatchingResponseDetails {

    @JsonProperty("conf")
    private String conf;

    @JsonProperty("match")
    private String match;

    @JsonProperty("match-score")
    private String match_score;

    /*Description : Duplicate in Documentation
    @JsonProperty("match-score")
    private String match_score;*/

    @JsonProperty("to-be-reviewed")
    private String to_be_reviewed;
}