package com.softcell.gonogo.model.core.kyc.response.karza.mciauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MciMembershipAuthenticationResponseDetails {

    @JsonProperty("emailId")
    private String emailId;

    @JsonProperty("eligbleToVote")
    private String eligbleToVote;

    @JsonProperty("doctorEducationId")
    private String doctorEducationId;

    @JsonProperty("doctRegistrationNo")
    private String doctRegistrationNo;

    @JsonProperty("registrationDatePrevious")
    private String registrationDatePrevious;

    @JsonProperty("photo")
    private String photo;

    @JsonProperty("parentName")
    private String parentName;

    @JsonProperty("universityId_view")
    private String universityId_view;

    @JsonProperty("regDate")
    private String regDate;

    @JsonProperty("birthDate")
    private String birthDate;

    @JsonProperty("restoredStatus")
    private String restoredStatus;

    @JsonProperty("universityId")
    private String universityId;

    @JsonProperty("college")
    private String college;

    @JsonProperty("yearOfPassing")
    private String yearOfPassing;

    @JsonProperty("addressLine1")
    private String addressLine1;

    @JsonProperty("salutation")
    private String salutation;

    @JsonProperty("economicStatus")
    private String economicStatus;

    @JsonProperty("otherSubject")
    private String otherSubject;

    @JsonProperty("city")
    private String city;

    @JsonProperty("restoredOn")
    private String restoredOn;

    @JsonProperty("uprnNo")
    private String uprnNo;

    @JsonProperty("middleName")
    private String middleName;

    @JsonProperty("registrationNoPrevious")
    private String registrationNoPrevious;

    @JsonProperty("trasanctionStatus")
    private String trasanctionStatus;

    @JsonProperty("monthandyearOfPass")
    private String monthandyearOfPass;

    @JsonProperty("state")
    private String state;

    @JsonProperty("yearInfo")
    private String yearInfo;

    @JsonProperty("role")
    private String role;

    @JsonProperty("removedOn")
    private String removedOn;

    @JsonProperty("catagory")
    private String catagory;

    @JsonProperty("addressLine2")
    private String addressLine2;

    @JsonProperty("registrationNo")
    private String registrationNo;

    @JsonProperty("officeAddress")
    private String officeAddress;

    @JsonProperty("smcNamePrevious")
    private String smcNamePrevious;

    @JsonProperty("registrationDate")
    private String registrationDate;

    @JsonProperty("addlqualuniv3")
    private String addlqualuniv3;

    @JsonProperty("addlqualuniv2")
    private String addlqualuniv2;

    @JsonProperty("addlqualuniv1")
    private String addlqualuniv1;

    @JsonProperty("passoutCollege")
    private String passoutCollege;

    @JsonProperty("birthDateStr")
    private String birthDateStr;

    @JsonProperty("birthPlace")
    private String birthPlace;

    @JsonProperty("addlqualyear3")
    private String addlqualyear3;

    @JsonProperty("addlqualyear1")
    private String addlqualyear1;

    @JsonProperty("smcIds")
    private String smcIds;

    @JsonProperty("doctorId")
    private String doctorId;

    @JsonProperty("stateMedicalCouncil")
    private String stateMedicalCouncil;

    @JsonProperty("removedStatus")
    private String removedStatus;

    @JsonProperty("remarks")
    private String remarks;

    @JsonProperty("nationality")
    private String nationality;

    @JsonProperty("checkExistingUser")
    private String checkExistingUser;

    @JsonProperty("pincode")
    private String pincode;

    @JsonProperty("officeaddress")
    private String officeaddress;

    @JsonProperty("uprnNoPrevious")
    private String uprnNoPrevious;

    @JsonProperty("phoneNo")
    private String phoneNo;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("stateId")
    private String stateId;

    @JsonProperty("isNewDoctor")
    private String isNewDoctor;

    @JsonProperty("doctorDegree")
    private String doctorDegree;

    @JsonProperty("university")
    private String university;

    @JsonProperty("addlqualyear2")
    private String addlqualyear2;

    @JsonProperty("smcName")
    private String smcName;

    @JsonProperty("smcId")
    private String smcId;

    @JsonProperty("adharNo")
    private String adharNo;

    @JsonProperty("addlqual3")
    private String addlqual3;

    @JsonProperty("addlqual2")
    private String addlqual2;

    @JsonProperty("addlqual1")
    private String addlqual1;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("catagory_view")
    private String catagory_view;

    @JsonProperty("bloodGroup")
    private String bloodGroup;

    @JsonProperty("country")
    private String country;

    @JsonProperty("homeAddress")
    private String homeAddress;

    @JsonProperty("collegeId")
    private String collegeId;

    @JsonProperty("address")
    private String address;

    @JsonProperty("regnNo")
    private String regnNo;


}