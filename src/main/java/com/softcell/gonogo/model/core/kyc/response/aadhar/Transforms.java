package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Transforms {

	/**
	 * @author Deepak Bhargava
	 *
	 *
	 */
	@XmlElement(name="Transform")
	private Transform transForm;

	public Transform getTransForm() {
		return transForm;
	}

	public void setTransForm(Transform transForm) {
		this.transForm = transForm;
	}

	@Override
	public String toString() {
		return "Transforms [transForm=" + transForm + "]";
	}
	
	
}
