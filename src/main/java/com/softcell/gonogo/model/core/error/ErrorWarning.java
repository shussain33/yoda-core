package com.softcell.gonogo.model.core.error;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorWarning {
    @JsonProperty("CODE")
    private String code;

    @JsonProperty("DESCRIPTION")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ErrorWarning [code=" + code + ", description=" + description
                + "]";
    }
}
