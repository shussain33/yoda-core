package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SessionKey {

    @JsonProperty("CERTIFICATE-ID")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp.class})
    private String certificateID;

    @JsonProperty("VALUE")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp.class})
    private String value;

    public static Builder builder() {
        return new Builder();
    }

    public String getCertificateID() {
        return certificateID;
    }

    public void setCertificateID(String certificateID) {
        this.certificateID = certificateID;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SessionKey [certificateID=");
        builder.append(certificateID);
        builder.append(", value=");
        builder.append(value);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((certificateID == null) ? 0 : certificateID.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof SessionKey))
            return false;
        SessionKey other = (SessionKey) obj;
        if (certificateID == null) {
            if (other.certificateID != null)
                return false;
        } else if (!certificateID.equals(other.certificateID))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    public static class Builder {
        private SessionKey sessionKey = new SessionKey();

        public SessionKey build() {
            return sessionKey;
        }

        public Builder certificateID(String certificateID) {
            this.sessionKey.certificateID = certificateID;
            return this;
        }

        public Builder value(String value) {
            this.sessionKey.value = value;
            return this;
        }
    }
}
