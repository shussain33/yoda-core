package com.softcell.gonogo.model.core.scoring.response;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {
    private Error error;
    private Score score;

    @JsonProperty("SCORING-REF-ID")
    private String ackId;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("SCORE-DATA")
    private ScoreData scoreData;
    @JsonProperty("CUSTOM_ATTRIBUTES")
    private CustomAttributes custAttribute;

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "Response [error=" + error + ", score=" + score + "]";
    }


}
