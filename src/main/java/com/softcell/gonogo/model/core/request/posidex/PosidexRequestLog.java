package com.softcell.gonogo.model.core.request.posidex;

import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.posidex.DedupeEnquiryRequest;
import com.softcell.gonogo.model.posidex.GetEnquiryRequest;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by yogeshb on 23/8/17.
 */
@Document(collection = "posidexRequestLog")
public class PosidexRequestLog extends AuditEntity {
    private String refId;
    private Date date;
    private DedupeEnquiryRequest dedupeDoEnquiryRequest;
    private GetEnquiryRequest getEnquiryStatusRequest;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DedupeEnquiryRequest getDedupeDoEnquiryRequest() {
        return dedupeDoEnquiryRequest;
    }

    public void setDedupeDoEnquiryRequest(DedupeEnquiryRequest dedupeDoEnquiryRequest) {
        this.dedupeDoEnquiryRequest = dedupeDoEnquiryRequest;
    }

    public GetEnquiryRequest getGetEnquiryStatusRequest() {
        return getEnquiryStatusRequest;
    }

    public void setGetEnquiryStatusRequest(GetEnquiryRequest getEnquiryStatusRequest) {
        this.getEnquiryStatusRequest = getEnquiryStatusRequest;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PosidexRequestLog{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", date=").append(date);
        sb.append(", dedupeDoEnquiryRequest=").append(dedupeDoEnquiryRequest);
        sb.append(", getEnquiryStatusRequest=").append(getEnquiryStatusRequest);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PosidexRequestLog that = (PosidexRequestLog) o;

        if (refId != null ? !refId.equals(that.refId) : that.refId != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (dedupeDoEnquiryRequest != null ? !dedupeDoEnquiryRequest.equals(that.dedupeDoEnquiryRequest) : that.dedupeDoEnquiryRequest != null)
            return false;
        return getEnquiryStatusRequest != null ? getEnquiryStatusRequest.equals(that.getEnquiryStatusRequest) : that.getEnquiryStatusRequest == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (refId != null ? refId.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (dedupeDoEnquiryRequest != null ? dedupeDoEnquiryRequest.hashCode() : 0);
        result = 31 * result + (getEnquiryStatusRequest != null ? getEnquiryStatusRequest.hashCode() : 0);
        return result;
    }
}
