package com.softcell.gonogo.model.core.perfios;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by archana on 1/10/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "perfiosData")
public class PerfiosData {
    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("aPerfiosData")
    List<BankData> saveDataList;

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sProduct")
    private Product product;


    @Deprecated
    @JsonProperty("sAckId")
    private String acknowledgeId;

    @Deprecated
    @JsonProperty("oData")
    private Map<String, Object> data;

    @Deprecated
    @JsonProperty("callDate")
    private Date callDate = new Date();
}

