package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@XmlAccessorType(XmlAccessType.FIELD)
public class Prn {

	@XmlAttribute(name="type")
	private String type;
	
	@XmlValue
	private String letterValue;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLetterValue() {
		return letterValue;
	}

	public void setLetterValue(String letterValue) {
		this.letterValue = letterValue;
	}

	@Override
	public String toString() {
		return "Prn [type=" + type + ", letterValue=" + letterValue + "]";
	}

}
