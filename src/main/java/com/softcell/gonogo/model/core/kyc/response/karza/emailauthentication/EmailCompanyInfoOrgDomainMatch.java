package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailCompanyInfoOrgDomainMatch {

    @JsonProperty("domain")
    private String domain;

    @JsonProperty("match")
    private Boolean match;

    @JsonProperty("source")
    private String source;

    @JsonProperty("org_name")
    private String org_name;

    @JsonProperty("orgName")
    private String orgName;

}
