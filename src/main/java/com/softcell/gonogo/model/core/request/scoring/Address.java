package com.softcell.gonogo.model.core.request.scoring;

public class Address {

    private String address;
    private String postal;
    private String reportedDate;
    private Integer seq;
    private String state;
    private String type;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Address [address=" + address + ", postal=" + postal
                + ", reportedDate=" + reportedDate + ", seq=" + seq
                + ", state=" + state + ", type=" + type + "]";
    }
}