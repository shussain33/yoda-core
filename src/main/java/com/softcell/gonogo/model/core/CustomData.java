package com.softcell.gonogo.model.core;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** @amit.b
 * This class contains different attribute which are maintained for MIS data.
 * Class variables dose not have any relationship with each other
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomData {

    // Is cibil score from application scoring response
    @JsonProperty("sCibilScore")
    private String cibilScore;
    // Year value is used for PLBS - sales value against some year
    @JsonProperty("sYearValue")
    private double yearValue;
}
