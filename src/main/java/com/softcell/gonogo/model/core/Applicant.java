package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.KarzaProcessingInfo;
import com.softcell.gonogo.model.ApplicantAadharDetails;
import com.softcell.gonogo.model.ThirdPartyCall;
import com.softcell.gonogo.model.address.Address;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.address.MifinAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.ocr.OCRDetails;
import com.softcell.gonogo.model.core.request.thirdparty.FaceMatchDetails;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.surrogate.Surrogate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.Past;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * @author yogeshb
 */
public class Applicant implements Serializable, Comparable<Applicant>{

    @JsonProperty("sApplID")
    private String applicantId;

    // Applicant /Coapplicant /Guarantor / Group of Companies
    @JsonProperty("sEntityType")
    private String entityType;

    @JsonProperty("oApplName")
    private Name applicantName;

    @JsonProperty("oFatherName")
    private Name fatherName;

    @JsonProperty("oSpouseName")
    private Name spouseName;

    @JsonProperty("sTinNo")
    private String tinNo;

    @JsonProperty("oMotherName")
    private Name motherName;

    @JsonProperty("sMothersMaidenName")
    private String mothersMaidenName;

    @JsonProperty("sReligion")
    private String religion;

    @JsonProperty("sCaste")
    private String caste;

    @JsonProperty("sApplGndr")
    private String gender;

    @JsonProperty("sDob")
    @Past
    private String dateOfBirth;

    @JsonProperty("iAge")
    private int age;

    @JsonProperty("sMarStat")
    private String maritalStatus;

    @JsonProperty("sNationality")
    private String nationality;

    @JsonProperty("sOtherNationality")
    private String sOthrNationality;


    @JsonProperty("sResiStatus")
    private String residenceStatus;

    @JsonProperty("sDesignation")
    private String designation;

    @JsonProperty("aKycDocs")
    private List<Kyc> kyc;

    @JsonProperty("bSameAbove")
    private boolean residenceAddSameAsAbove;

    @JsonProperty("aMifinAddress")
    private List<MifinAddress> mifinAddressList;

    @JsonProperty("aAddr")
    private List<CustomerAddress> address;

    @JsonProperty("aPhone")
    private List<Phone> phone;

    @JsonProperty("aEmail")
    private List<Email> email;

    @JsonProperty("aEmpl")
    private List<Employment> employment;

    @JsonProperty("iNoOfDep")
    private int noOfDependents;

    @JsonProperty("iNoOfChildren")
    private int iNoOfChildren;

    @JsonProperty("iEarnMem")
    private int noOfEarningMembers;

    @JsonProperty("iFamilyMem")
    private int noOfFamilyMembers;

    @JsonProperty("aApplRef")
    private List<ApplicantReference> applicantReferences;

    @JsonProperty("sEdu")
    private String education;

    @JsonProperty("sCreditCardNum")
    private String creditCardNumber;

    @JsonProperty("aCreditCardLnAccNumbers")
    private List<String> creditCardLnAccNumbers;

    @JsonProperty("bMobVer")
    private boolean mobileVerified;

    @JsonProperty("bAdharVer")
    @Field("addharVerified")
    private boolean aadhaarVerified;

    @JsonProperty("sHasBankAccount")
    private String hasBankAccount;

    @JsonProperty("aBankingDetails")
    private List<BankingDetails> bankingDetails;

    @JsonProperty("aLoanDetails")
    private List<LoanDetails> loanDetails;

    @JsonProperty("oIncomeDetails")
    private IncomeDetails incomeDetails;

    @JsonProperty("oSurrogate")
    private Surrogate surrogate;

    @JsonProperty("oMifinData")
    private AmbitMifinData ambitMifinData;

    @JsonProperty("aPartner")
    private List<Partner> partners;

    // Individual/Proprietorship/Partnership/Pvt Ltd/Ltd
    @JsonProperty("sType")
    private String applicantType;

    @JsonProperty("bConsentToCall")
    private boolean consentToCall;

    @JsonProperty("sContactPerson")
    private String contactPerson;

    @JsonProperty("sGstNo")
    private String gstNo;

    @JsonProperty("bGstStatus")
    private boolean gstStatus;

    @JsonProperty("sTanNo")
    private String tanNo;

    @JsonProperty("oAadharDetails")
    private ApplicantAadharDetails aadharDetails;

    @JsonProperty("oProfessionIncomeDetails")
    private ProfessionIncomeDetails professionIncomeDetails;

    @JsonProperty("oEligibilityDetail")
    private EligibilityDetails eligibilityDetails;

    @JsonProperty("bMinor")
    private boolean minor;

    @JsonProperty("sPlaceOfBirth")
    private String placeOfBirth;

    @JsonProperty("bPhysicallyHandicapped")
    private boolean physicallyHandicapped;

    @JsonProperty("sOtherNatureOccupation")
    private String otherNatureOccupation;

    // TODO : Migrate to
    @JsonProperty("bCreditVidyaFlag")
    private boolean creditVidyaFlag;

    @JsonProperty("sPropertyNature")
    private String propertyNature;

    @JsonProperty("iAgeDuringMatOfLoan")
    private int ageDuringMaturityOfLoan;

    @JsonProperty("oThirdPartyCall")
    private ThirdPartyCall thirdPartyCall;

    // for sending to sobre
    @JsonProperty("sCurrentStage")
    private String currentStage;

    @JsonProperty("sOldCutomerCode")
    private String oldCustomerCode;

    @JsonProperty("aInsurancePolicy")
    private List<InsurancePolicy> insurancePolicyList;

    //Insurance flag required
    @JsonProperty("bWaived")
    private boolean waived;

    @JsonProperty("sOwnership")
    private String ownership;

    @JsonProperty("sPanNoLap")
    private String panNoLap;

    @JsonProperty("sPassport")
    private String passport;

    @JsonProperty("sPassportExpiryDate")
    private String passportExpiryDate;

    @JsonProperty("sPassportType")
    private String passportType;

    @JsonProperty("sPngAuth")
    private String pngAuth;

    @JsonProperty("sPngServiceProvider")
    private String pngServiceProvider;

    @JsonProperty("sLpgAuth")
    private String lpgAuth;

    @JsonProperty("sLpgServiceProvider")
    private String lpgServiceProvider;

    @JsonProperty("sElctAuth")
    private String elctAuth;

    @JsonProperty("sServiceProvider")
    private String serviceProvider;

    @JsonProperty("sVoterId")
    private String voterId;

    @JsonProperty("sLicence")
    private String licence;

    @JsonProperty("sDlExpiryDate")
    private String dlExpiryDate;

    @JsonProperty("oKarzaProcessingInfo")
    public List<KarzaProcessingInfo> karzaProcessingInfoList;

    @JsonProperty("sUdyogAdhaar")
    private String udyogAdhaar;

    @JsonProperty("sCinNo")
    private String cinNo;

    @JsonProperty("bForm60CellFlag")
    private boolean form60CellFlag;

    @JsonProperty("iBureauHitCnt")
    private  int bureauHitCnt ;

    @JsonProperty("sBureauHitMsg")
    private  String bureauMsg ;

    @JsonProperty("sCastCategory")
    private String castCategory;

    @JsonProperty("sSocialCategory")
    private String socialCategory;

    @JsonProperty("sPersonalLandLine")
    private String perosnalLandline;

    @JsonProperty("sPhysicallyHandicapped")
    private String physicallyHandicappd;

    @JsonProperty("sNonFinancial")
    private String nonFinancial;

    //DigiPL Fields Started
    @JsonProperty("sApplicationBucket")
    private String applicationBucket;

    @JsonProperty("sForm60Name")
    private String form60Name;

    @JsonProperty("sImgBase64Form60")
    private String imageform60;

    @JsonProperty("sCity")
    private String city;

    @JsonProperty("oPostEligibilityDetail")
    private EligibilityDetails postEligibility;// for DigiPL

    //Added for DigiPl to send Perfios data to sobre
    @Transient
    @JsonProperty("oPerfiosData")
    private List<LinkedHashMap> perfiosData;

    //Added for DigiPl to send Ocr data to sobre
    @Transient
    @JsonProperty("oOcrDetails")
    private HashMap<String, String> ocrDetails;

    @Transient
    @JsonProperty("oLoanCharges")
    private LoanCharges loanCharges;

    @Transient
    @JsonProperty("oFaceMatchDetails")
    private FaceMatchDetails faceMatchDetails;

    @JsonProperty("sMinimumOfferedValue")
    private String minimumOfferedValue;

    public String getMinimumOfferedValue() { return minimumOfferedValue; }

    public void setMinimumOfferedValue(String minimumOfferedValue) { this.minimumOfferedValue = minimumOfferedValue;}

    public String getApplicationBucket() { return applicationBucket; }

    public void setApplicationBucket(String applicationBucket) { this.applicationBucket = applicationBucket; }

    public String getCity() {
        return city;
    }

    public void setCity(String city) { this.city = city; }

    public EligibilityDetails getPostEligibility() {
        return postEligibility;
    }

    public void setPostEligibility(EligibilityDetails eligibilityDetails) {
        this.postEligibility = eligibilityDetails;
    }

    public EligibilityDetails getDigiPLEligibility(){
        if(null!= this.postEligibility)
            return this.postEligibility;
        else
            return this.eligibilityDetails;
    }

    public void setAmbitMifinData(AmbitMifinData ambitMifinData){this.ambitMifinData=ambitMifinData;}

    public AmbitMifinData getAmbitMifinData(){return ambitMifinData;}

    public List<LinkedHashMap> getPerfiosData(){ return perfiosData; }

    public void setPerfiosData(List<LinkedHashMap> perfiosData) { this.perfiosData = perfiosData; }

    public HashMap<String, String> getOcrDetails(){ return ocrDetails; }

    public void setOcrDetails(HashMap ocrDetails) { this.ocrDetails = ocrDetails; }

    public LoanCharges getLoanCharges(){ return loanCharges; }

    public void setLoanCharges(LoanCharges loanCharges) { this.loanCharges = loanCharges; }

    public FaceMatchDetails getFaceMatchDetails(){ return faceMatchDetails; }

    public void setFaceMatchDetails(FaceMatchDetails faceMatchDetails) { this.faceMatchDetails = faceMatchDetails; }


    public String getOldCustomerCode() {
        return oldCustomerCode;
    }

    public void setOldCustomerCode(String oldCustomerCode) {
        this.oldCustomerCode = oldCustomerCode;
    }


    public String getCurrentStage() {   return currentStage;    }

    public void setCurrentStage(String currentStage) {  this.currentStage = currentStage;   }

    public ThirdPartyCall getThirdPartyCall() {
        return thirdPartyCall;
    }

    public void setThirdPartyCall(ThirdPartyCall thirdPartyCall) {
        this.thirdPartyCall = thirdPartyCall;
    }

    public EligibilityDetails getEligibilityDetails() {
        return eligibilityDetails;
    }

    public void setEligibilityDetails(EligibilityDetails eligibilityDetails) {
        this.eligibilityDetails = eligibilityDetails;
    }

    public List<Partner> getPartners() {
        return partners;
    }

    public void setPartners(List<Partner> partners) {
        this.partners = partners;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Name getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(Name applicantName) {
        this.applicantName = applicantName;
    }

    public Name getFatherName() {
        return fatherName;
    }

    public void setFatherName(Name fatherName) {
        this.fatherName = fatherName;
    }

    public Name getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(Name spouseName) {
        this.spouseName = spouseName;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public List<Kyc> getKyc() {
        return kyc;
    }

    public void setKyc(List<Kyc> kyc) {
        this.kyc = kyc;
    }

    public boolean isResidenceAddSameAsAbove() {
        return residenceAddSameAsAbove;
    }

    public void setResidenceAddSameAsAbove(boolean residenceAddSameAsAbove) {
        this.residenceAddSameAsAbove = residenceAddSameAsAbove;
    }

    public List<CustomerAddress> getAddress() {
        return address;
    }

    public void setAddress(List<CustomerAddress> address) {
        this.address = address;
    }

    public List<Phone> getPhone() {
        return phone;
    }

    public void setPhone(List<Phone> phone) {
        this.phone = phone;
    }

    public List<Email> getEmail() {
        return email;
    }

    public void setEmail(List<Email> email) {
        this.email = email;
    }

    public List<Employment> getEmployment() {
        return employment;
    }

    public void setEmployment(List<Employment> employment) {
        this.employment = employment;
    }

    public int getNoOfDependents() {
        return noOfDependents;
    }

    public void setNoOfDependents(int noOfDependents) {
        this.noOfDependents = noOfDependents;
    }

    public int getNoOfEarningMembers() {
        return noOfEarningMembers;
    }

    public void setNoOfEarningMembers(int noOfEarningMembers) {
        this.noOfEarningMembers = noOfEarningMembers;
    }

    public int getNoOfFamilyMembers() {
        return noOfFamilyMembers;
    }

    public void setNoOfFamilyMembers(int noOfFamilyMembers) {
        this.noOfFamilyMembers = noOfFamilyMembers;
    }


    public List<ApplicantReference> getApplicantReferences() {
        return applicantReferences;
    }

    public void setApplicantReferences(
            List<ApplicantReference> applicantReferences) {
        this.applicantReferences = applicantReferences;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }


    public List<String> getCreditCardLnAccNumbers() { return creditCardLnAccNumbers; }

    public void setCreditCardLnAccNumbers(List<String> creditCardLnAccNumbers) {
        this.creditCardLnAccNumbers = creditCardLnAccNumbers;
    }

    public boolean isMobileVerified() {
        return mobileVerified;
    }

    public void setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public List<BankingDetails> getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(List<BankingDetails> bankingDetails) {
        this.bankingDetails = bankingDetails;
    }

    public List<LoanDetails> getLoanDetails() {
        return loanDetails;
    }

    public void setLoanDetails(List<LoanDetails> loanDetails) {
        this.loanDetails = loanDetails;
    }

    public IncomeDetails getIncomeDetails() {
        return incomeDetails;
    }

    public void setIncomeDetails(IncomeDetails incomeDetails) {
        this.incomeDetails = incomeDetails;
    }

    public Surrogate getSurrogate() {
        return surrogate;
    }

    public void setSurrogate(Surrogate surrogate) {
        this.surrogate = surrogate;
    }

    public String getApplicantType() {
        return applicantType;
    }

    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    public boolean isAadhaarVerified() {
        return aadhaarVerified;
    }

    public void setAadhaarVerified(boolean aadhaarVerified) {
        this.aadhaarVerified = aadhaarVerified;
    }

    public Name getMotherName() {
        return motherName;
    }

    public void setMotherName(Name motherName) {
        this.motherName = motherName;
    }

    public boolean isConsentToCall() {
        return consentToCall;
    }

    public void setConsentToCall(boolean consentToCall) {
        this.consentToCall = consentToCall;
    }

    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }

    public String getHasBankAccount() {
        return hasBankAccount;
    }

    public void setHasBankAccount(String hasBankAccount) {
        this.hasBankAccount = hasBankAccount;
    }

    public ApplicantAadharDetails getAadharDetails() {
        return aadharDetails;
    }

    public void setAadharDetails(ApplicantAadharDetails aadharDetails) {
        this.aadharDetails = aadharDetails;
    }

    public ProfessionIncomeDetails getProfessionIncomeDetails() {
        return professionIncomeDetails;
    }

    public void setProfessionIncomeDetails(ProfessionIncomeDetails professionIncomeDetails) {
        this.professionIncomeDetails = professionIncomeDetails;
    }
    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getTanNo() {
        return tanNo;
    }

    public void setTanNo(String tanNo) {
        this.tanNo = tanNo;
    }

    public boolean isGstStatus() {
        return gstStatus;
    }

    public void setGstStatus(boolean gstStatus) {
        this.gstStatus = gstStatus;
    }

    public boolean isMinor() {
        return minor;
    }

    public void setMinor(boolean minor) {
        this.minor = minor;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public boolean isPhysicallyHandicapped() {
        return physicallyHandicapped;
    }

    public void setPhysicallyHandicapped(boolean physicallyHandicapped) {
        this.physicallyHandicapped = physicallyHandicapped;
    }

    public String getOtherNatureOccupation() {
        return otherNatureOccupation;
    }

    public void setOtherNatureOccupation(String otherNatureOccupation) {
        this.otherNatureOccupation = otherNatureOccupation;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getsOthrNationality() {
        return sOthrNationality;
    }

    public void setsOthrNationality(String sOthrNationality) {
        this.sOthrNationality = sOthrNationality;
    }

    public String getResidenceStatus() {
        return residenceStatus;
    }

    public void setResidenceStatus(String residenceStatus) {
        this.residenceStatus = residenceStatus;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getiNoOfChildren() {
        return iNoOfChildren;
    }

    public void setiNoOfChildren(int iNoOfChildren) {
        this.iNoOfChildren = iNoOfChildren;
    }

    public boolean isCreditVidyaFlag() {
        return creditVidyaFlag;
    }

    public void setCreditVidyaFlag(boolean creditVidyaFlag) {
        this.creditVidyaFlag = creditVidyaFlag;
    }

    public int getAgeDuringMaturityOfLoan() {
        return ageDuringMaturityOfLoan;
    }

    public void setAgeDuringMaturityOfLoan(int ageDuringMaturityOfLoan) {
        this.ageDuringMaturityOfLoan = ageDuringMaturityOfLoan;
    }
    public List<InsurancePolicy> getInsurancePolicyList() {
        return insurancePolicyList;
    }

    public void setInsurancePolicyList(List<InsurancePolicy> insurancePolicyList) {
        this.insurancePolicyList = insurancePolicyList;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public String getPanNoLap() {
        return panNoLap;
    }

    public void setPanNoLap(String panNoLap) {
        this.panNoLap = panNoLap;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPassportExpiryDate() {
        return passportExpiryDate;
    }

    public void setPassportExpiryDate(String passportExpiryDate) {
        this.passportExpiryDate = passportExpiryDate;
    }

    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    public String getPngAuth() {
        return pngAuth;
    }

    public void setPngAuth(String pngAuth) {
        this.pngAuth = pngAuth;
    }

    public String getPngServiceProvider() {
        return pngServiceProvider;
    }

    public void setPngServiceProvider(String pngServiceProvider) {
        this.pngServiceProvider = pngServiceProvider;
    }

    public String getLpgAuth() {
        return lpgAuth;
    }

    public void setLpgAuth(String lpgAuth) {
        this.lpgAuth = lpgAuth;
    }

    public String getLpgServiceProvider() {
        return lpgServiceProvider;
    }

    public void setLpgServiceProvider(String lpgServiceProvider) {
        this.lpgServiceProvider = lpgServiceProvider;
    }

    public String getElctAuth() {
        return elctAuth;
    }

    public void setElctAuth(String elctAuth) {
        this.elctAuth = elctAuth;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getDlExpiryDate() {
        return dlExpiryDate;
    }

    public void setDlExpiryDate(String dlExpiryDate) {
        this.dlExpiryDate = dlExpiryDate;
    }

    public String getPropertyNature() {
        return propertyNature;
    }

    public void setPropertyNature(String propertyNature) {
        this.propertyNature = propertyNature;
    }

    public List<KarzaProcessingInfo> getKarzaProcessingInfoList() {
        return karzaProcessingInfoList;
    }

    public void setKarzaProcessingInfoList(List<KarzaProcessingInfo> karzaProcessingInfoList) {
        this.karzaProcessingInfoList = karzaProcessingInfoList;
    }

    public String getUdyogAdhaar() {
        return udyogAdhaar;
    }

    public void setUdyogAdhaar(String udyogAdhaar) {
        this.udyogAdhaar = udyogAdhaar;
    }

    public String getTinNo() {
        return tinNo;
    }

    public void setTinNo(String tinNo) {
        this.tinNo = tinNo;
    }


    public String getCinNo() {
        return cinNo;
    }

    public void setCinNo(String cinNo) {
        this.cinNo = cinNo;
    }

    public int getBureauHitCnt() { return bureauHitCnt; }

    public void setBureauHitCnt(int bureauHitCnt) { this.bureauHitCnt = bureauHitCnt; }

    public String getBureauMsg() { return bureauMsg; }

    public void setBureauMsg(String bureauMsg) { this.bureauMsg = bureauMsg; }

    public String getCastCategory() { return castCategory; }

    public void setCastCategory(String castCategory) { this.castCategory = castCategory; }

    public String getPhysicallyHandicappd() { return physicallyHandicappd; }

    public void setPhysicallyHandicappd(String physicallyHandicappd) { this.physicallyHandicappd = physicallyHandicappd; }

    public String getNonFinancial() { return nonFinancial; }

    public void setNonFinancial(String nonFinancial) { this.nonFinancial = nonFinancial; }

    public void setSocialCategory(String socialCategory) { this.socialCategory = socialCategory; }

    public String getSocialCategory() { return socialCategory; }

    public void setPersonalLandline(String perosnalLandline) { this.perosnalLandline = perosnalLandline; }

    public String getPersonalLandline() { return perosnalLandline; }

    public void setMifinAddressList(List<MifinAddress> mifinAddressList) { this.mifinAddressList = mifinAddressList; }

    public List<MifinAddress> getMifinAddressList() { return mifinAddressList; }

    public void setForm60CellFlag(boolean form60CellFlag) { this.form60CellFlag = form60CellFlag; }

    public boolean getForm60CellFlag() { return form60CellFlag; }

    public void  setImageform60(String imageform60){this.imageform60=imageform60;}

    public String getImageform60(){return  imageform60;}

    public void setForm60Name(String form60Name) { this.form60Name = form60Name; }

    public String getForm60Name() { return form60Name; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Applicant{");
        sb.append("applicantId='").append(applicantId).append('\'');
        sb.append(", entityType=").append(entityType);
        sb.append(", applicantName=").append(applicantName);
        sb.append(", fatherName=").append(fatherName);
        sb.append(", spouseName=").append(spouseName);
        sb.append(", motherName=").append(motherName);
        sb.append(", mothersMaidenName='").append(mothersMaidenName).append('\'');
        sb.append(", religion='").append(religion).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", age=").append(age);
        sb.append(", maritalStatus='").append(maritalStatus).append('\'');
        sb.append(", nationality='").append(nationality).append('\'');
        sb.append(", kyc=").append(kyc);
        sb.append(", residenceAddSameAsAbove=").append(residenceAddSameAsAbove);
        sb.append(", address=").append(address);
        sb.append(", phone=").append(phone);
        sb.append(", email=").append(email);
        sb.append(", employment=").append(employment);
        sb.append(", noOfDependents=").append(noOfDependents);
        sb.append(", noOfEarningMembers=").append(noOfEarningMembers);
        sb.append(", noOfFamilyMembers=").append(noOfFamilyMembers);
        sb.append(", applicantReferences=").append(applicantReferences);
        sb.append(", education='").append(education).append('\'');
        sb.append(", creditCardNumber='").append(creditCardNumber).append('\'');
        sb.append(", creditCardLnAccNumbers=").append(creditCardLnAccNumbers);
        sb.append(", mobileVerified=").append(mobileVerified);
        sb.append(", aadhaarVerified=").append(aadhaarVerified);
        sb.append(", hasBankAccount='").append(hasBankAccount).append('\'');
        sb.append(", bankingDetails=").append(bankingDetails);
        sb.append(", loanDetails=").append(loanDetails);
        sb.append(", incomeDetails=").append(incomeDetails);
        sb.append(", incomeDetails=").append(professionIncomeDetails);
        sb.append(", surrogate=").append(surrogate);
        sb.append(", partners=").append(partners);
        sb.append(", applicantType='").append(applicantType).append('\'');
        sb.append(", consentToCall=").append(consentToCall);
        sb.append(", aadharDetails=").append(aadharDetails);
        sb.append(", contactPerson=").append(contactPerson);
        sb.append(", tanNo=").append(tanNo);
        sb.append(", gstNo=").append(gstNo);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Applicant applicant = (Applicant) o;
        return Objects.equals(age, applicant.age) &&
                Objects.equals(residenceAddSameAsAbove, applicant.residenceAddSameAsAbove) &&
                Objects.equals(noOfDependents, applicant.noOfDependents) &&
                Objects.equals(noOfEarningMembers, applicant.noOfEarningMembers) &&
                Objects.equals(noOfFamilyMembers, applicant.noOfFamilyMembers) &&
                Objects.equals(mobileVerified, applicant.mobileVerified) &&
                Objects.equals(aadhaarVerified, applicant.aadhaarVerified) &&
                Objects.equals(consentToCall, applicant.consentToCall) &&
                Objects.equals(applicantId, applicant.applicantId) &&
                Objects.equals(applicantName, applicant.applicantName) &&
                Objects.equals(fatherName, applicant.fatherName) &&
                Objects.equals(spouseName, applicant.spouseName) &&
                Objects.equals(motherName, applicant.motherName) &&
                Objects.equals(mothersMaidenName, applicant.mothersMaidenName) &&
                Objects.equals(religion, applicant.religion) &&
                Objects.equals(gender, applicant.gender) &&
                Objects.equals(dateOfBirth, applicant.dateOfBirth) &&
                Objects.equals(maritalStatus, applicant.maritalStatus) &&
                Objects.equals(nationality, applicant.nationality) &&
                Objects.equals(kyc, applicant.kyc) &&
                Objects.equals(address, applicant.address) &&
                Objects.equals(phone, applicant.phone) &&
                Objects.equals(email, applicant.email) &&
                Objects.equals(employment, applicant.employment) &&
                Objects.equals(applicantReferences, applicant.applicantReferences) &&
                Objects.equals(education, applicant.education) &&
                Objects.equals(creditCardNumber, applicant.creditCardNumber) &&
                Objects.equals(creditCardLnAccNumbers, applicant.creditCardLnAccNumbers) &&
                Objects.equals(hasBankAccount, applicant.hasBankAccount) &&
                Objects.equals(bankingDetails, applicant.bankingDetails) &&
                Objects.equals(loanDetails, applicant.loanDetails) &&
                Objects.equals(incomeDetails, applicant.incomeDetails) &&
                Objects.equals(professionIncomeDetails, applicant.professionIncomeDetails) &&
                Objects.equals(surrogate, applicant.surrogate) &&
                Objects.equals(partners, applicant.partners) &&
                Objects.equals(applicantType, applicant.applicantType) &&
                Objects.equals(aadharDetails, applicant.aadharDetails) &&
                Objects.equals(contactPerson, applicant.contactPerson) &&
                Objects.equals(gstNo, applicant.gstNo) &&
                Objects.equals(tanNo, applicant.tanNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applicantId, applicantName, fatherName, spouseName, motherName, mothersMaidenName, religion, gender, dateOfBirth, age, maritalStatus, kyc, residenceAddSameAsAbove, address, phone, email, employment, noOfDependents, noOfEarningMembers, noOfFamilyMembers, applicantReferences, education, creditCardNumber, creditCardLnAccNumbers, mobileVerified, aadhaarVerified, hasBankAccount, bankingDetails, loanDetails, incomeDetails, surrogate, partners, applicantType, consentToCall, aadharDetails);
    }

    public boolean equalsApplicant(Object o)
    {
        Applicant applicant = (Applicant) o;
        return Objects.equals(applicantType, applicant.getApplicantType())&&
                Objects.equals(applicantId, applicant.getApplicantId())&&
                Objects.equals(applicantName.getFirstName(), applicant.applicantName.getFirstName())&&
                Objects.equals(kyc, applicant.getKyc())&&
                Objects.equals(applicantName.getMiddleName(), applicant.applicantName.getMiddleName())&&
                Objects.equals(applicantName.getLastName(), applicant.applicantName.getLastName())&&
                Objects.equals(fatherName.getFirstName(), applicant.getFatherName().getFirstName())&&
                Objects.equals(motherName.getFirstName(), applicant.getMotherName().getFirstName())&&
                Objects.equals(dateOfBirth, applicant.getDateOfBirth())&&
                Objects.equals(email, applicant.getEmail())&&
                Objects.equals(gender, applicant.getGender())&&
                Objects.equals(phone, applicant.getPhone())&&
                Objects.equals(address, applicant.getAddress());
    }

    public boolean isWaived() {
        return waived;
    }

    public void setWaived(boolean waived) {
        this.waived = waived;
    }

    @Override
    public int compareTo(Applicant o) {
        this.getApplicantId().compareTo(o.applicantId);
        return 0;
    }
}