package com.softcell.gonogo.model.core;

/**
 * Created by Softcell on 27/09/17.
 */
public enum TCLosGngMappingDetails {

    LOANDISBURSALACCOUNT_DP("LOANDISBURSALACCOUNT_DP"),
    PROCESSING_FEES("PROCESSING_FEES"),
    EXTENDED_WARRANTY("EXTENDED_WARRANTY"),
    INSURANCE_PREMIUM_EMI("INSURANCE_PREMIUM_EMI"),
    INSURANCE_PREMIUM_DP("INSURANCE_PREMIUM_DP"),
    ADVANCE_EMI("ADVANCE_EMI"),
    INITIALHIRE_DP("INITIALHIRE_DP"),
    DEALER_BUYDOWN("DEALER_BUYDOWN"),
    OTHER_CHARGES("OTHER_CHARGES"),
    LOANDISBURSALACCOUNT_DIS("LOANDISBURSALACCOUNT_DIS"),
    DEALER_ACCOUNT("DEALER_ACCOUNT"),
    SUBVENTION_INCOME("SUBVENTION_INCOME"),
    MANUFACTURER_BUYDOWN("MANUFACTURER_BUYDOWN"),
    SOH("SOH"),
    INITIALHIRE_AGR("INITIALHIRE_AGR"),
    LOANDISBURSALACCOUNT_AGR("LOANDISBURSALACCOUNT_AGR"),
    UMFC("UMFC");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    TCLosGngMappingDetails(String value) {
        this.value = value;
    }
}
