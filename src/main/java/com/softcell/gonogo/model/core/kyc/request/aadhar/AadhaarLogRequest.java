package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by yogeshb on 14/4/17.
 */
public class AadhaarLogRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("dtStart")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    AadhaarLogRequest.FetchGrp.class
            }
    )
    private Date dateStartFrom;

    @JsonProperty("dtEnd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    AadhaarLogRequest.FetchGrp.class
            }
    )
    private Date endDate;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Date getDateStartFrom() {
        return dateStartFrom;
    }

    public void setDateStartFrom(Date dateStartFrom) {
        this.dateStartFrom = dateStartFrom;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AadhaarLogRequest{");
        sb.append("header=").append(header);
        sb.append(", dateStartFrom=").append(dateStartFrom);
        sb.append(", endDate=").append(endDate);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AadhaarLogRequest that = (AadhaarLogRequest) o;

        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (dateStartFrom != null ? !dateStartFrom.equals(that.dateStartFrom) : that.dateStartFrom != null)
            return false;
        return endDate != null ? endDate.equals(that.endDate) : that.endDate == null;
    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + (dateStartFrom != null ? dateStartFrom.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }

    public interface FetchGrp {
    }
}
