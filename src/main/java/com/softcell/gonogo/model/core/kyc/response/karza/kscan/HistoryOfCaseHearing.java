package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class HistoryOfCaseHearing {

    @JsonProperty("businessOnDate")
    public String businessOnDate;

    @JsonProperty("hearingDate")
    public String hearingDate;

    @JsonProperty("judge")
    public String judge;

    @JsonProperty("purposeOfHearing")
    public String purposeOfHearing;

    @JsonProperty("registrationNumber")
    public String registrationNumber;

}