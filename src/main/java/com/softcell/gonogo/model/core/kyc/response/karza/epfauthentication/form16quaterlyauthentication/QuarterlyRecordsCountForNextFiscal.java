package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.form16quaterlyauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class QuarterlyRecordsCountForNextFiscal {

    @JsonProperty("Q-1")
    private String Q1;

    @JsonProperty("Q-2")
    private String Q2;

    @JsonProperty("Q-3")
    private String Q3;

    @JsonProperty("Q-4")
    private String Q4;

}