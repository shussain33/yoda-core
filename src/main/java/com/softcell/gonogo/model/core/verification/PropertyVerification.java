package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PropertyVerification extends ThirdPartyVerification{

    @JsonProperty("oInput")
    protected PropertyVerificationInput input;

    @JsonProperty("oOutput")
    protected PropertyVerificationOutput output;

    @JsonProperty("sCopiedFrom")
    private String copiedFrom;

    @JsonProperty("bSameAsSelected")
    private boolean isSameAsSelected;

}




