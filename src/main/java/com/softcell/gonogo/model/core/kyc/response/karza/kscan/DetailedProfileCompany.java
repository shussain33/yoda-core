package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DetailedProfileCompany {

    @JsonProperty("activityClass")
    private String activityClass;

    @JsonProperty("activityGroup")
    private String activityGroup;

    @JsonProperty("activitySubClass")
    private String activitySubClass;

    @JsonProperty("alternativeAddress")
    private String alternativeAddress;

    @JsonProperty("authorisedCapital")
    private String authorisedCapital;

    @JsonProperty("category")
    private String category;

    @JsonProperty("cin")
    private String cin;

    @JsonProperty("dateOfBalanceSheet")
    private String dateOfBalanceSheet;

    @JsonProperty("dateOfIncorporation")
    private String dateOfIncorporation;

    @JsonProperty("dateOfLastAGM")
    private String dateOfLastAGM;

    @JsonProperty("emailId")
    private String emailId;

    @JsonProperty("entityClass")
    private String entityClass;

    @JsonProperty("entityName")
    private String entityName;

    @JsonProperty("industry")
    private String industry;

    @JsonProperty("numberOfMembers")
    private String numberOfMembers;

    @JsonProperty("paidUpCapital")
    private String paidUpCapital;

    @JsonProperty("registeredAddress")
    private String registeredAddress;

    @JsonProperty("registrationNumber")
    private String registrationNumber;

    @JsonProperty("rocCode")
    private String rocCode;

    @JsonProperty("status")
    private String status;

    @JsonProperty("subIndustry")
    private String subIndustry;

    @JsonProperty("subcategory")
    private String subcategory;

    @JsonProperty("suspendedAtStockExchange")
    private String suspendedAtStockExchange;

    @JsonProperty("whetherListedOrNot")
    private String whetherListedOrNot;
}