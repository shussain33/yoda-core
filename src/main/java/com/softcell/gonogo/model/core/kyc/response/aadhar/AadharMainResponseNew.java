package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;

import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AadharMainResponseNew {

    @JsonProperty("dtResponse")
    private Date dateTime = new Date();

    @JsonProperty("HEADER")
    private AadharResponseHeader header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private String acknowledgementId;

    @JsonProperty("TXN-STATUS")
    private String txnStatus;

    @JsonProperty("KYC-RESPONSE")
    private KycResponse kycResponse;

    @JsonProperty("UIDAI-KYC-RESPONSE")
    private String rawXmlResponse;

    @JsonProperty("ERRORS")
    private List<Errors> errors;

    /**
     * @return the header
     */
    public AadharResponseHeader getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(AadharResponseHeader header) {
        this.header = header;
    }

    /**
     * @return the acknowledgementId
     */
    public String getAcknowledgementId() {
        return acknowledgementId;
    }

    /**
     * @param acknowledgementId the acknowledgementId to set
     */
    public void setAcknowledgementId(String acknowledgementId) {
        this.acknowledgementId = acknowledgementId;
    }

    /**
     * @return the txnStatus
     */
    public String getTxnStatus() {
        return txnStatus;
    }

    /**
     * @param txnStatus the txnStatus to set
     */
    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    /**
     * @return the kycResponse
     */
    public KycResponse getKycResponse() {
        return kycResponse;
    }

    /**
     * @param kycResponse the kycResponse to set
     */
    public void setKycResponse(KycResponse kycResponse) {
        this.kycResponse = kycResponse;
    }

    public String getRawXmlResponse() {
        return rawXmlResponse;
    }

    public void setRawXmlResponse(String rawXmlResponse) {
        this.rawXmlResponse = rawXmlResponse;
    }
    /**
     * @return the errors
     */
    public List<Errors> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AadharMainResponse{");
        sb.append("dateTime=").append(dateTime);
        sb.append(", header=").append(header);
        sb.append(", acknowledgementId='").append(acknowledgementId).append('\'');
        sb.append(", txnStatus='").append(txnStatus).append('\'');
        sb.append(", kycResponse=").append(kycResponse);
        sb.append(", rawXmlResponse='").append(rawXmlResponse).append('\'');
        sb.append(", errors=").append(errors);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AadharMainResponseNew that = (AadharMainResponseNew) o;

        if (dateTime != null ? !dateTime.equals(that.dateTime) : that.dateTime != null) return false;
        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (acknowledgementId != null ? !acknowledgementId.equals(that.acknowledgementId) : that.acknowledgementId != null)
            return false;
        if (txnStatus != null ? !txnStatus.equals(that.txnStatus) : that.txnStatus != null) return false;
        if (kycResponse != null ? !kycResponse.equals(that.kycResponse) : that.kycResponse != null) return false;
        if (rawXmlResponse != null ? !rawXmlResponse.equals(that.rawXmlResponse) : that.rawXmlResponse != null)
            return false;
        return errors != null ? errors.equals(that.errors) : that.errors == null;
    }

    @Override
    public int hashCode() {
        int result = dateTime != null ? dateTime.hashCode() : 0;
        result = 31 * result + (header != null ? header.hashCode() : 0);
        result = 31 * result + (acknowledgementId != null ? acknowledgementId.hashCode() : 0);
        result = 31 * result + (txnStatus != null ? txnStatus.hashCode() : 0);
        result = 31 * result + (kycResponse != null ? kycResponse.hashCode() : 0);
        result = 31 * result + (rawXmlResponse != null ? rawXmlResponse.hashCode() : 0);
        result = 31 * result + (errors != null ? errors.hashCode() : 0);
        return result;
    }
}
