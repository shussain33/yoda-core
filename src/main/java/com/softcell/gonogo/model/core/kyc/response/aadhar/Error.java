package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Error {
	@SerializedName("CODE")
	@XmlElement(name="CODE")
	String code;
	
	@SerializedName("DESCRIPTION")
	@XmlElement(name="DESCRIPTION")
	String description;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Error [code=" + code + ", description=" + description + "]";
	}
	

	
}
