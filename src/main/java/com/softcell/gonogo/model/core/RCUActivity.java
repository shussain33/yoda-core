package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by ssg228 on 8/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RCUActivity {

    @JsonProperty("sRemark")
    public String remark;

    @JsonProperty("sCurrentStatus")
    public String currentStatus;

    @JsonProperty("dtDate")
    public Date date;

    @JsonProperty("sDoneBy")
    public String doneBy;

    @JsonProperty("sDoneByRole")
    public String doneByRole;
}
