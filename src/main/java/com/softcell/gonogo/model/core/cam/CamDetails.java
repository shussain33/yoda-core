package com.softcell.gonogo.model.core.cam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Past;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by yogesh on 17/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "camDetails")
public class CamDetails {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("oSummary")
    private CamSummary summary;

    @JsonProperty("sRemarks")
    private String remarks;

    @JsonProperty("aRTRDetail")
    protected List<RTRDetail> rtrDetailList;

    @JsonProperty("aBankingStatement")
    private List<BankingStatement> bankingStatements;

    @JsonProperty("aPLAndBSAnalysis")
    private List<PLAndBSAnalysis> plAndBSAnalysis;

    @JsonProperty("oAppCoAppData")
    Map<String, CustomData> applicantCoApplicantData;

    @JsonProperty("aGstDetails")
    private List<GstDetailsList> gstDetails;

    @JsonProperty("bSBFC")
    private boolean sbfc;

    @JsonProperty("oNonBusiness")
    private BankStatementsSummary nonBusinessBnkStmtSummary = new BankStatementsSummary();

    @JsonProperty("oNetBusiness")
    private BankStatementsSummary netBusinessBnkStmtSummary = new BankStatementsSummary();

    @JsonProperty("dTotal_ABB")
    private double totalAbb;

    @JsonProperty("dAverage_Credit_Summation")
    private double avgCreditSummation;

    @JsonProperty("oTotal_No_Of_Transaction")
    private BankStatementsSummary totalNoOfTransactionSummary = new BankStatementsSummary();

    @JsonProperty("oI_W_Returns")
    private BankStatementsSummary i_W_ReturnsSummary = new BankStatementsSummary();

    @JsonProperty("dTotal_Credit_Summation")
    private double totalCreditSummation;

    public interface InsertGrp {
    }
}


