package com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IfscCodeCheckResponseDetails {

    @JsonProperty("city")
    private String city;

    @JsonProperty("district")
    private String district;

    @JsonProperty("ifsc")
    private String ifsc;

    @JsonProperty("micr")
    private String micr;

    @JsonProperty("state")
    private String state;

    @JsonProperty("contact")
    private String contact;

    @JsonProperty("branch")
    private String branch;

    @JsonProperty("address")
    private String address;

    @JsonProperty("bank")
    private String bank;

}