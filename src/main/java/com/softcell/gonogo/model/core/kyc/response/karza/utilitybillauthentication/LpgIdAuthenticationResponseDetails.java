package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LpgIdAuthenticationResponseDetails {

    @JsonProperty("status")
    private String status;

    @JsonProperty("ApproximateSubsidyAvailed")
    private String ApproximateSubsidyAvailed;

    @JsonProperty("SubsidizedRefillConsumed")
    private String SubsidizedRefillConsumed;

    @JsonProperty("pin")
    private String pin;

    @JsonProperty("ConsumerEmail")
    private String ConsumerEmail;

    @JsonProperty("DistributorCode")
    private String DistributorCode;

    @JsonProperty("BankName")
    private String BankName;

    @JsonProperty("IFSCCode")
    private String IFSCCode;

    @JsonProperty("city/town")
    private String city_town;

    @JsonProperty("AadhaarNo")
    private String AadhaarNo;

    @JsonProperty("ConsumerContact")
    private String ConsumerContact;

    @JsonProperty("DistributorAddress")
    private String DistributorAddress;

    @JsonProperty("ConsumerName")
    private String ConsumerName;

    @JsonProperty("ConsumerNo")
    private String ConsumerNo;

    @JsonProperty("DistributorName")
    private String DistributorName;

    @JsonProperty("BankAccountNo")
    private String BankAccountNo;

    @JsonProperty("GivenUpSubsidy")
    private String GivenUpSubsidy;

    @JsonProperty("ConsumerAddress")
    private String ConsumerAddress;

    @JsonProperty("LastBookingDate")
    private String LastBookingDate;

    @JsonProperty("TotalRefillConsumed")
    private String TotalRefillConsumed;

}