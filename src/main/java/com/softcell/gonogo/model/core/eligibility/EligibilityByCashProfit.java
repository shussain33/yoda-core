package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityByCashProfit {

    @JsonProperty("dPat")
    private double pat;

    @JsonProperty("dDepreciationPercentage")
    private double depreciationPercentage;

    @JsonProperty("dDepreciation")
    private double depreciation;

    @JsonProperty("dIntrstPaidToPartnersCapital")
    private double intrstPaidToPartnersCapital;

    @JsonProperty("dRenumPaidToPartners")
    private double  renumPaidToPartners;

    @JsonProperty("dRenumPaidToDirectors")
    private double  renumPaidToDirectors;

    @JsonProperty("dNetIncome")
    private double netIncome;

    @JsonProperty("dOtherIncome")
    private double otherIncome;

    @JsonProperty("dFixedObligation")
    private double fixedObligation;

    @JsonProperty("dGrossIncome")
    private double grossIncome;

}
