package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by amit on 18/4/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DecisionData {
//Stores only approved or declined decisions
    @JsonProperty("oFirstDecision")
    private CroJustification firstDecision;

    @JsonProperty("oLastDecision")
    private CroJustification lastDecision;
}
