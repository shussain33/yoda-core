package com.softcell.gonogo.model.core;

import com.softcell.ssl2.hunter.model.matchresponse.HunterResponse;
import lombok.Data;

import java.util.List;

@Data
public class CallHunterResponse {

    List<String> hunterIds;
    List<HunterResponse> hunterResponses;

    List<Object> errors;

}
