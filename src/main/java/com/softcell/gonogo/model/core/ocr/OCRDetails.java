package com.softcell.gonogo.model.core.ocr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.RCUSummary;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by ssg228 on 6/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "OCRDetails")
public class OCRDetails {

    @Id
    @JsonProperty("sRefId")
    @NotEmpty
    protected String refId;

    @JsonProperty("sInstID")
    protected String institutionId;

    @JsonProperty("aOCRDetailsObj")
    public List<OCRDetailsObj> ocrDetailsList;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sAgencyCode")
    public String agencyCode;

    @JsonProperty("sInitiatedBy")
    private String initiatedBy;

    @JsonProperty(value = "sInitiatedByRole")
    private String initiatedByRole;

    @JsonProperty("oRCUSummary")
    private RCUSummary rcuSummary;

    @JsonProperty("sAgencyName")
    public String agencyName;

    @JsonProperty("sAgencyStatus")
    public String agencyStatus;


}
