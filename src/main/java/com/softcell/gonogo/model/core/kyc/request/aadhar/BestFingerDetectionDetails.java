package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BestFingerDetectionDetails {

    @JsonProperty("uid")
    private String uid;

    @JsonProperty("tid")
    private String tid;

    @JsonProperty("ac")
    private String ac;

    @JsonProperty("sa")
    private String sa;

    @JsonProperty("ver")
    private String ver;

    @JsonProperty("txn")
    private String txn;

    @JsonProperty("lk")
    private String lk;

    @JsonProperty("data")
    private Data data;

    public static Builder builder() {
        return new Builder();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getAc() {
        return ac;
    }

    public void setAc(String ac) {
        this.ac = ac;
    }

    public String getSa() {
        return sa;
    }

    public void setSa(String sa) {
        this.sa = sa;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getTxn() {
        return txn;
    }

    public void setTxn(String txn) {
        this.txn = txn;
    }

    public String getLk() {
        return lk;
    }

    public void setLk(String lk) {
        this.lk = lk;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BestFingerDetectionDetails{");
        sb.append("uid='").append(uid).append('\'');
        sb.append(", tid='").append(tid).append('\'');
        sb.append(", ac='").append(ac).append('\'');
        sb.append(", sa='").append(sa).append('\'');
        sb.append(", ver='").append(ver).append('\'');
        sb.append(", txn='").append(txn).append('\'');
        sb.append(", lk='").append(lk).append('\'');
        sb.append(", data=").append(data);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BestFingerDetectionDetails)) return false;
        BestFingerDetectionDetails that = (BestFingerDetectionDetails) o;
        return Objects.equal(getUid(), that.getUid()) &&
                Objects.equal(getTid(), that.getTid()) &&
                Objects.equal(getAc(), that.getAc()) &&
                Objects.equal(getSa(), that.getSa()) &&
                Objects.equal(getVer(), that.getVer()) &&
                Objects.equal(getTxn(), that.getTxn()) &&
                Objects.equal(getLk(), that.getLk()) &&
                Objects.equal(getData(), that.getData());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUid(), getTid(), getAc(), getSa(), getVer(), getTxn(), getLk(), getData());
    }


    public static class Builder {
        private BestFingerDetectionDetails bestFingerDetectionDetails = new BestFingerDetectionDetails();

        public BestFingerDetectionDetails build() {
            return bestFingerDetectionDetails;
        }

        public Builder uid(String uid) {
            this.bestFingerDetectionDetails.uid = uid;
            return this;
        }

        public Builder tid(String tid) {
            this.bestFingerDetectionDetails.tid = tid;
            return this;
        }

        public Builder ac(String ac) {
            this.bestFingerDetectionDetails.ac = ac;
            return this;
        }

        public Builder sa(String sa) {
            this.bestFingerDetectionDetails.sa = sa;
            return this;
        }

        public Builder ver(String ver) {
            this.bestFingerDetectionDetails.ver = ver;
            return this;
        }

        public Builder txn(String txn) {
            this.bestFingerDetectionDetails.txn = txn;
            return this;
        }

        public Builder lk(String lk) {
            this.bestFingerDetectionDetails.lk = lk;
            return this;
        }

        public Builder data(Data data) {
            this.bestFingerDetectionDetails.data = data;
            return this;
        }
    }
}
