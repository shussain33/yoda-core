package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GstProfileResponseDetails {

    @JsonProperty("canFlag")
    private String canFlag;

    @JsonProperty("contacted")
    private String contacted;

    @JsonProperty("ppr")
    private String ppr;

    @JsonProperty("cmpRt")
    private String cmpRt;

    @JsonProperty("rgdt")
    private String rgdt;

    @JsonProperty("tradeNam")
    private String tradeNam;

    @JsonProperty("nba")
    private List<String> nba;

    @JsonProperty("mbr")
    private List<String> mbr;

    @JsonProperty("adadr")
    private List<GstProfileAdadr> adadr;

    @JsonProperty("pradr")
    private GstProfileAdadr pradr;

    @JsonProperty("stjCd")
    private String stjCd;

    @JsonProperty("lstupdt")
    private String lstupdt;

    @JsonProperty("gstin")
    private String gstin;

    @JsonProperty("ctjCd")
    private String ctjCd;

    @JsonProperty("stj")
    private String stj;

    @JsonProperty("dty")
    private String dty;

    @JsonProperty("cxdt")
    private String cxdt;

    @JsonProperty("ctb")
    private String ctb;

    @JsonProperty("sts")
    private String sts;

    @JsonProperty("lgnm")
    private String lgnm;

    @JsonProperty("ctj")
    private String ctj;
}