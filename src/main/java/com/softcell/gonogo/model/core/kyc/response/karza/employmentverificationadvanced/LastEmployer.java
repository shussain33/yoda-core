package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LastEmployer {

    @JsonProperty("dateOfExit")
    private String dateOfExit;

    @JsonProperty("dateOfJoining")
    private String dateOfJoining;

    @JsonProperty("employerName")
    private String employerName;

    @JsonProperty("vintageInMonths")
    private int vintageInMonths;

}
