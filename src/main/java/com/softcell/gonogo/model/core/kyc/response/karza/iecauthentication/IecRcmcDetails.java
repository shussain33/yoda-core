package com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IecRcmcDetails {

    @JsonProperty("rcmc_type")
    private String rcmc_type;

    @JsonProperty("rcmc_no")
    private String rcmc_no;

    @JsonProperty("rcmc_issued_by")
    private String rcmc_issued_by;

    @JsonProperty("rcmc_issue_date")
    private String rcmc_issue_date;

    @JsonProperty("rcmc_exp_date")
    private String rcmc_exp_date;
}