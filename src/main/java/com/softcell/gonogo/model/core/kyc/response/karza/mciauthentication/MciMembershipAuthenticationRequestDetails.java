package com.softcell.gonogo.model.core.kyc.response.karza.mciauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MciMembershipAuthenticationRequestDetails {

    @JsonProperty("registration_no")
    private String registration_no;

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("year_of_reg")
    private String year_of_reg;

    @JsonProperty("medical_council")
    private String medical_council;

}