package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suhasini on 2/3/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditorsDebtors {

    @JsonProperty("sName")
    private String name;

    @JsonProperty("sContactNumber")
    private String contactNumber;

    @JsonProperty("sMonthlyVolumeOfBusiness")
    private String monthlyVolumeOfBusiness;

    @JsonProperty("sBusinessYears")
    private String businessYears;

    @JsonProperty("sPaymentTerms")
    private String paymentTerms;

    @JsonProperty("sCreditPeriod")
    private String creditPeriod;

    @JsonProperty("sPercentageCash")
    private String percentageCash;

    @JsonProperty("sComments")
    private String comments;
}
