package com.softcell.gonogo.model.core.cam;

import com.softcell.gonogo.model.core.Name;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RTRDetail {
	@JsonProperty("oApplicantName")
	private Name applicantName;

	@JsonProperty("sApplicantId")
	private String applicantId;

	@JsonProperty("aRTRTransaction")
	private List<RTRTransaction> rtrTransactions;

	@JsonProperty("dTotalOutstandingAmount")
	private double totalOutstandingAmount;

	@JsonProperty("dNetFoAmount")
	private double netFoAmount;

	@JsonProperty("dOEOperatingFOAmount")
	private double oeOperatingFOAmount;

	@JsonProperty("dFutureObligations")
	private double futureObligations;

	@JsonProperty("dTotalFutureObligations")
	private double totalFutureObligations;

	@JsonProperty("dTotalEMINotConsiderAsFOAmount")
	private double totalEMINotConsiderAsFOAmount;

	@JsonProperty("dTotalNetFoAmount")
	private double totalNetFoAmount;

	@JsonProperty("dEMINotConsiderAsFOAmount")
	private double emiNotConsiderAsFOAmount;

	@JsonProperty("sRemarks")
	private String remarks;

	@JsonProperty("sMifinOtherLoanDetailsId")
	private String mifinOtherLoanDetailsId;

	@Override
	public boolean equals(Object o){
		if (this == o) return true;
		if(!(o instanceof RTRDetail)) return false;
		RTRDetail that = (RTRDetail) o;
		return Objects.equals(getApplicantId(), that.applicantId);
	}

	@Override
	public int hashCode(){
		return Objects.hash(getApplicantId());
	}
}

