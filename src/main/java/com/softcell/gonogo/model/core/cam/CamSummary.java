package com.softcell.gonogo.model.core.cam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Collateral;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.eligibility.IncomeDetail;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.ops.SanctionConditions;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.List;

/**
 * Created by suhasini on 21/3/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CamSummary {

    @JsonProperty("sLAN")
    private String lan;

    @JsonProperty("dtLoginDate")
    private Date loginDate;

    @JsonProperty("dtApprovalDate")
    private Date approvalDate;

    @JsonProperty("sRMName")
    private String rmName;

    @JsonProperty("sSourcingChannel")
    private String sourcingChannel;

    @JsonProperty("sLocation")
    private String location;

    @JsonProperty("oApplicantName")
    private Name applicantName;

    @JsonProperty("dLoanAmount")
    private double loanAmount;

    @JsonProperty("sMobileNumber")
    private String mobileNumber;

    @JsonProperty("sDob")
    @Past
    private String dateOfBirth;

    @JsonProperty("sTelephoneNumber")
    private String telephoneNumber;

    @JsonProperty("sROIOffered")
    private String roiOffered;

    @JsonProperty("sTenorRequested")
    private String tenorRequested;

    @JsonProperty("dProcessingFees")
    private double processingFees;

    @JsonProperty("sPFPercentage")
    private String pfPercentage;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("sCustomerType")
    private String customerType;

    @JsonProperty("sCompanyName")
    private String companyName;

    @JsonProperty("sBusinessVintage")
    private String businessVintage;

    @JsonProperty("bPSL")
    private boolean psl;

    @JsonProperty("sScheme")
    private String scheme;

    @JsonProperty("sIndustry")
    private String industry;

    @JsonProperty("sWorkingSince")
    private String workingSince;

    @JsonProperty("iTotalWorkExperience")
    private int totalWorkExperience;

    @JsonProperty("sPSLSubClassification")
    private String pslSubClassification;

    @JsonProperty("sBTBankOrNBFCName")
    private String btBankOrNBFCName;

    @JsonProperty("dOriginalLoanAmount")
    private double originalLoanAmount;

    @JsonProperty("dTotalTenure")
    private double totalTenure;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("dExistingLAN")
    private double existingLan;

    @JsonProperty("iMOB")
    private int mob;

    @JsonProperty("sBalanceTenor")
    private String balanceTenor;

    @JsonProperty("sPOS")
    private String pos;

    @JsonProperty("aCollateral")
    private List<Collateral> collateralList;

    @JsonProperty("dFinalLoanApproved")
    private double finalLoanApproved;

    @JsonProperty("dBTOrExistingLoanAmount")
    private double btOrExistingLoanAmt;

    @JsonProperty("dTopUpOrAdditionalAmount")
    private double topUpOrAdditionalAmt;

    @JsonProperty("sTenure")
    private String tenure;

    @JsonProperty("dROI")
    private double roi;

    @JsonProperty("dInsuranceOfferedAmount")
    private double insuranceOfferedAmt;

    @JsonProperty("dtPDDoneDate")
    private Date pdDoneDate;

    @JsonProperty("dMaxLTV")
    private double maxLTV;

    @JsonProperty("dLTVForLoanApproved")
    private double ltvForLoanApproved;

    @JsonProperty("dFOIR")
    private double foir;

    @JsonProperty("dEMI")
    private double emi;

    @JsonProperty("dTopupEMI")
    private double topupEMI;

    @JsonProperty("bInsuranceOfferedAsLoan")
    private boolean insuranceOfferedAsLoan;

    @JsonProperty("bPDStatus")
    private boolean pdStatus;

    @JsonProperty("sInstallMentType")
    private String installmentType;

    @JsonProperty("sRoiType")
    private String roiType;

    @JsonProperty("dPropertyvalueConsidr")
    private String propertyvalueConsidr;

    @JsonProperty("sFinalLoanApprovedBy")
    private String finalLoanApprovedBy;

    @JsonProperty("aRemarks")
    private List<Remark> remarkList;

    //fields for pl
    @JsonProperty("aAddr")
    private List<CustomerAddress> address;

    @JsonProperty("sSpouseWorking")
   private String spouseWorking;

    @JsonProperty("iNoOfYearsAtResd")
    private int noOfYearAtResidence;

    @JsonProperty("iAge")
    private int age;

    @JsonProperty("dFinalFOIR")
    private double finalFoir;

    @Transient
    @JsonProperty("oDeviationDtls")
    private DeviationDetails deviationDetails;

    @Transient
    @JsonProperty("oSanctionCndtn")
    private SanctionConditions sanctionConditions;

    @JsonProperty("sDtJoin")
    private String dateOfJoining;

    @JsonProperty("sSubProgram")
    private String subProgram;

    @JsonProperty("dCombinedLtv")
    public double combinedLtv;

    @JsonProperty("sCaseLoginDate")
    private String caseloginDate;

    //for Ambit
    @JsonProperty("dGrossIncmeForAppl")
    private double grossIncome;

    @JsonProperty("IncomeDetailList")
    private List<IncomeDetail> incomeDetailList;

    @JsonProperty("dFixedObligationForAppl")
    private double fixedOblgation;

    @JsonProperty("oDerivedFields")
    private Object derivedFields;

    @JsonProperty("dtCreditDiscussion")
    private Date creditDiscussionDate;

    @JsonProperty("dMonthlyCollectionEmi")
    private  double monthlyCollectionEmi;

    @JsonProperty("dABBEmi")
    private double abbEmi;

    @JsonProperty("dAvgMonthlyCCUtilization")
    private double avgMonthlyCCUtilization;

    @JsonProperty("dBTONorms")
    private  double bTNorms;

    @JsonProperty("sSector")
    private String sector;

    @JsonProperty("sBusinessNature")
    private String businessNature;

    @JsonProperty("sPDDoneBy")
    private String pdDoneBy;

    @JsonProperty("dAdvEMI")
    private double advEmi;

    @JsonProperty("sRiskProfile")
    private String riskProfile;

    @JsonProperty("dLtvDisbAmt")
    private String ltvDisbAmt;

    @JsonProperty("dLtvCombExposure")
    private String ltvCombExposure;

    @JsonProperty("sPdIndustry")
    private String pdIndustry;

    @JsonProperty("sBusinessType")
    private String businessType;

    @JsonProperty("aApplicantList")
    private List<ApplicantDetails> applicantList;

    @JsonProperty("iTmWithEmplr")
    private int timeWithEmplr;

    @JsonProperty("dDisbAmt")
    private double disbAmt;

    @JsonProperty("sCustomerCategory")
    private String customerCategory;
}

