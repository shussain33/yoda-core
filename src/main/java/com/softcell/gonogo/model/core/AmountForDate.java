package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg237 on 3/2/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AmountForDate {
    @JsonProperty("iMonthDate")
    private int monthDate;

    @JsonProperty("dBalance")
    private double balanceAmt;
}

