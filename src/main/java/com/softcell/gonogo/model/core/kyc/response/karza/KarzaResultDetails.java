package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.EFiledlistDetails;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class KarzaResultDetails {

    @JsonProperty("membershipId")
    private String membershipId;

    @JsonProperty("settelment")
    private String settelment;

    @JsonProperty("estName")
    private String estName;

    @JsonProperty("City")
    private String City;

    @JsonProperty("Designation")
    private String Designation;

    @JsonProperty("BenevolentMember")
    private String BenevolentMember;

    @JsonProperty("Phone")
    private String Phone;

    @JsonProperty("MembershipNumber")
    private String MembershipNumber;

    @JsonProperty("Mob")
    private String Mob;

    @JsonProperty("CP_Number")
    private String CP_Number;

    @JsonProperty("address")
    private String address;

    @JsonProperty("Organization")
    private String Organization;

    @JsonProperty("Email")
    private String Email;

    @JsonProperty("financial_year")
    private String financial_year;

    @JsonProperty("gstr1_filing_frequency")
    private String gstr1_filing_frequency;

    @JsonProperty("EFiledlistDetails")
    public List<EFiledlistDetails> eFiledlistDetailsList;

    @JsonProperty("emailId")
    private String emailId;

    @JsonProperty("applicationStatus")
    private String applicationStatus;

    @JsonProperty("mobNum")
    private String mobNum;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("gstinRefId")
    private String gstinRefId;

    @JsonProperty("regType")
    private String regType;

    @JsonProperty("authStatus")
    private String authStatus;

    @JsonProperty("gstinId")
    private String gstinId;

    @JsonProperty("registrationName")
    private String registrationName;

    @JsonProperty("tinNumber")
    private String tinNumber;

    @JsonProperty("companyName")
    private String companyName;

    @JsonProperty("companyID")
    private String companyID;

    @JsonProperty("score")
    private String score;

    @JsonProperty("cin")
    private String cin;

    @JsonProperty("comapany_name")
    private String comapany_name;

    @JsonProperty("date_of_appointment")
    private String date_of_appointment;

    @JsonProperty("designation")
    private String designation;

    @JsonProperty("dsc_expiry_date")
    private String dsc_expiry_date;

    @JsonProperty("DIN/DPIN/PAN")
    private String DIN_DPIN_PAN;

    @JsonProperty("full_name")
    private String full_name;

    @JsonProperty("wheather_dsc_registered")
    private String wheather_dsc_registered;

    @JsonProperty("entityName")
    private String entityName;

}