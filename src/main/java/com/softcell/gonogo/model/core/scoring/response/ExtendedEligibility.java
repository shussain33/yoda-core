package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExtendedEligibility {

    @JsonProperty("AvlIncmFOIR")
    private double availableIncomeFOIR;

    @JsonProperty("AvlIncmINSR")
    private double availableIncomeINSR;

    @JsonProperty("IncmAvlToLoanRepay")
    private double incomeAvailableTowardsLoanRepay;

    @JsonProperty("EmiPerLakh")
    private double emiPerLakh;

    @JsonProperty("ActualEmi")
    private double actualEmi;

    @JsonProperty("ActualFOIRPercent")
    private double actualFOIRPercentage;

    @JsonProperty("ActualINSRPercent")
    private double actualINSRPercentage;

    @JsonProperty("PercentLCR")
    private double percentLCRCalculated;

    @JsonProperty("PercentLTV")
    private double percentLTVCalculated;

    @JsonProperty("ROI")
    private double rateOfInterest;

    @JsonProperty("Tenor")
    private double maxTenure;

    /**
     * @return the availableIncomeFOIR
     */
    public double getAvailableIncomeFOIR() {
        return availableIncomeFOIR;
    }

    /**
     * @param availableIncomeFOIR the availableIncomeFOIR to set
     */
    public void setAvailableIncomeFOIR(double availableIncomeFOIR) {
        this.availableIncomeFOIR = availableIncomeFOIR;
    }

    /**
     * @return the availableIncomeINSR
     */
    public double getAvailableIncomeINSR() {
        return availableIncomeINSR;
    }

    /**
     * @param availableIncomeINSR the availableIncomeINSR to set
     */
    public void setAvailableIncomeINSR(double availableIncomeINSR) {
        this.availableIncomeINSR = availableIncomeINSR;
    }

    /**
     * @return the incomeAvailableTowardsLoanRepay
     */
    public double getIncomeAvailableTowardsLoanRepay() {
        return incomeAvailableTowardsLoanRepay;
    }

    /**
     * @param incomeAvailableTowardsLoanRepay the incomeAvailableTowardsLoanRepay to set
     */
    public void setIncomeAvailableTowardsLoanRepay(
            double incomeAvailableTowardsLoanRepay) {
        this.incomeAvailableTowardsLoanRepay = incomeAvailableTowardsLoanRepay;
    }

    /**
     * @return the emiPerLakh
     */
    public double getEmiPerLakh() {
        return emiPerLakh;
    }

    /**
     * @param emiPerLakh the emiPerLakh to set
     */
    public void setEmiPerLakh(double emiPerLakh) {
        this.emiPerLakh = emiPerLakh;
    }

    /**
     * @return the actualEmi
     */
    public double getActualEmi() {
        return actualEmi;
    }

    /**
     * @param actualEmi the actualEmi to set
     */
    public void setActualEmi(double actualEmi) {
        this.actualEmi = actualEmi;
    }

    /**
     * @return the actualFOIRPercentage
     */
    public double getActualFOIRPercentage() {
        return actualFOIRPercentage;
    }

    /**
     * @param actualFOIRPercentage the actualFOIRPercentage to set
     */
    public void setActualFOIRPercentage(double actualFOIRPercentage) {
        this.actualFOIRPercentage = actualFOIRPercentage;
    }

    /**
     * @return the actualINSRPercentage
     */
    public double getActualINSRPercentage() {
        return actualINSRPercentage;
    }

    /**
     * @param actualINSRPercentage the actualINSRPercentage to set
     */
    public void setActualINSRPercentage(double actualINSRPercentage) {
        this.actualINSRPercentage = actualINSRPercentage;
    }

    /**
     * @return the percentLCRCalculated
     */
    public double getPercentLCRCalculated() {
        return percentLCRCalculated;
    }

    /**
     * @param percentLCRCalculated the percentLCRCalculated to set
     */
    public void setPercentLCRCalculated(double percentLCRCalculated) {
        this.percentLCRCalculated = percentLCRCalculated;
    }

    /**
     * @return the percentLTVCalculated
     */
    public double getPercentLTVCalculated() {
        return percentLTVCalculated;
    }

    /**
     * @param percentLTVCalculated the percentLTVCalculated to set
     */
    public void setPercentLTVCalculated(double percentLTVCalculated) {
        this.percentLTVCalculated = percentLTVCalculated;
    }

    /**
     * @return the rateOfInterest
     */
    public double getRateOfInterest() {
        return rateOfInterest;
    }

    /**
     * @param rateOfInterest the rateOfInterest to set
     */
    public void setRateOfInterest(double rateOfInterest) {
        this.rateOfInterest = rateOfInterest;
    }

    /**
     * @return the maxTenure
     */
    public double getMaxTenure() {
        return maxTenure;
    }

    /**
     * @param maxTenure the maxTenure to set
     */
    public void setMaxTenure(double maxTenure) {
        this.maxTenure = maxTenure;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ExtendedEligibility [availableIncomeFOIR=");
        builder.append(availableIncomeFOIR);
        builder.append(", availableIncomeINSR=");
        builder.append(availableIncomeINSR);
        builder.append(", incomeAvailableTowardsLoanRepay=");
        builder.append(incomeAvailableTowardsLoanRepay);
        builder.append(", emiPerLakh=");
        builder.append(emiPerLakh);
        builder.append(", actualEmi=");
        builder.append(actualEmi);
        builder.append(", actualFOIRPercentage=");
        builder.append(actualFOIRPercentage);
        builder.append(", actualINSRPercentage=");
        builder.append(actualINSRPercentage);
        builder.append(", percentLCRCalculated=");
        builder.append(percentLCRCalculated);
        builder.append(", percentLTVCalculated=");
        builder.append(percentLTVCalculated);
        builder.append(", rateOfInterest=");
        builder.append(rateOfInterest);
        builder.append(", maxTenure=");
        builder.append(maxTenure);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(actualEmi);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(actualFOIRPercentage);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(actualINSRPercentage);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(availableIncomeFOIR);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(availableIncomeINSR);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(emiPerLakh);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(incomeAvailableTowardsLoanRepay);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maxTenure);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(percentLCRCalculated);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(percentLTVCalculated);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(rateOfInterest);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ExtendedEligibility other = (ExtendedEligibility) obj;
        if (Double.doubleToLongBits(actualEmi) != Double
                .doubleToLongBits(other.actualEmi))
            return false;
        if (Double.doubleToLongBits(actualFOIRPercentage) != Double
                .doubleToLongBits(other.actualFOIRPercentage))
            return false;
        if (Double.doubleToLongBits(actualINSRPercentage) != Double
                .doubleToLongBits(other.actualINSRPercentage))
            return false;
        if (Double.doubleToLongBits(availableIncomeFOIR) != Double
                .doubleToLongBits(other.availableIncomeFOIR))
            return false;
        if (Double.doubleToLongBits(availableIncomeINSR) != Double
                .doubleToLongBits(other.availableIncomeINSR))
            return false;
        if (Double.doubleToLongBits(emiPerLakh) != Double
                .doubleToLongBits(other.emiPerLakh))
            return false;
        if (Double.doubleToLongBits(incomeAvailableTowardsLoanRepay) != Double
                .doubleToLongBits(other.incomeAvailableTowardsLoanRepay))
            return false;
        if (Double.doubleToLongBits(maxTenure) != Double
                .doubleToLongBits(other.maxTenure))
            return false;
        if (Double.doubleToLongBits(percentLCRCalculated) != Double
                .doubleToLongBits(other.percentLCRCalculated))
            return false;
        if (Double.doubleToLongBits(percentLTVCalculated) != Double
                .doubleToLongBits(other.percentLTVCalculated))
            return false;
        if (Double.doubleToLongBits(rateOfInterest) != Double
                .doubleToLongBits(other.rateOfInterest))
            return false;
        return true;
    }
}
