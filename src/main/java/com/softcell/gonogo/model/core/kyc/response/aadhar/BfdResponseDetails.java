package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;

import java.util.List;

/**
 * @author yogeshb
 */
public class BfdResponseDetails {
    @JsonProperty("UIDAI-STATUS")
    private String uidaiStatus;

    @JsonProperty("BFD-RESPONSE")
    private BfdResponse bfdResponse;

    @JsonProperty("ERRORS")
    private List<Errors> errors;

    public String getUidaiStatus() {
        return uidaiStatus;
    }

    public void setUidaiStatus(String uidaiStatus) {
        this.uidaiStatus = uidaiStatus;
    }

    public BfdResponse getBfdResponse() {
        return bfdResponse;
    }

    public void setBfdResponse(BfdResponse bfdResponse) {
        this.bfdResponse = bfdResponse;
    }

    public List<Errors> getErrors() {
        return errors;
    }

    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "BfdResponseDetails [uidaiStatus=" + uidaiStatus
                + ", bfdResponse=" + bfdResponse + ", errors=" + errors + "]";
    }


}
