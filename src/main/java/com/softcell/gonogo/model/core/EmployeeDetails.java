package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg237 on 8/7/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDetails {

    @JsonProperty("sEmpCode")
    private String empCode;

    @JsonProperty("sEmpName")
    private String empName;

    @JsonProperty("sRole")
    private String role;
}