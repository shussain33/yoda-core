package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Meta {
    @JsonProperty("fdc")
    private String fdc;

    @JsonProperty("apc")
    private String apc;

    @JsonProperty("Locn")
    private Location location;

    public static Builder builder() {
        return new Builder();
    }

    public String getFdc() {
        return fdc;
    }

    public void setFdc(String fdc) {
        this.fdc = fdc;
    }

    public String getApc() {
        return apc;
    }

    public void setApc(String apc) {
        this.apc = apc;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Meta{");
        sb.append("fdc='").append(fdc).append('\'');
        sb.append(", apc='").append(apc).append('\'');
        sb.append(", location=").append(location);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Meta)) return false;
        Meta meta = (Meta) o;
        return Objects.equal(getFdc(), meta.getFdc()) &&
                Objects.equal(getApc(), meta.getApc()) &&
                Objects.equal(getLocation(), meta.getLocation());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getFdc(), getApc(), getLocation());
    }

    public static class Builder {
        private Meta meta = new Meta();

        public Meta build() {
            return meta;
        }

        public Builder fdc(String fdc) {
            this.meta.fdc = fdc;
            return this;
        }

        public Builder apc(String apc) {
            this.meta.apc = apc;
            return this;
        }

        private Builder location(Location location) {
            this.meta.location = location;
            return this;
        }
    }

}
