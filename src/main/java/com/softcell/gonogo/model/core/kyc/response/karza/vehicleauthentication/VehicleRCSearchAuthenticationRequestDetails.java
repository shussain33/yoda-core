package com.softcell.gonogo.model.core.kyc.response.karza.vehicleauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VehicleRCSearchAuthenticationRequestDetails {

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("engine_no")
    private String engine_no;

    @JsonProperty("chassis_no")
    private String chassis_no;

    @JsonProperty("state")
    private String state;

}