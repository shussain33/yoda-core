package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.BankingDetailsRequest;
import com.softcell.gonogo.model.request.UpdateReferencesRequest;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**
 * @author yogeshb
 */
public class Name implements Serializable {

    @JsonProperty("sFirstName")
    @NotBlank(groups = {UpdateReferencesRequest.FetchGrp.class,BankingDetailsRequest.FetchGrp.class})
    private String firstName;

    @JsonProperty("sMiddleName")
    private String middleName;

    @JsonProperty("sLastName")
    @NotBlank(groups = {UpdateReferencesRequest.FetchGrp.class,BankingDetailsRequest.FetchGrp.class})
    private String lastName;

    @JsonProperty("sPrefix")
    private String prefix;

    @JsonProperty("sSuffix")
    private String suffix;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name [firstName=");
        builder.append(firstName);
        builder.append(", middleName=");
        builder.append(middleName);
        builder.append(", lastName=");
        builder.append(lastName);
        builder.append(", prefix=");
        builder.append(prefix);
        builder.append(", suffix=");
        builder.append(suffix);
        builder.append("]");
        return builder.toString();
    }

    public String getFullName() {
        StringBuilder stringBuilder = new StringBuilder();
        if(StringUtils.isNotEmpty(this.prefix)){
            stringBuilder.append(this.prefix).append(" ");
        }
        if(StringUtils.isNotEmpty(this.firstName)){
            stringBuilder.append(this.firstName).append(" ");
        }
        if(StringUtils.isNotEmpty(this.middleName)){
            stringBuilder.append(this.middleName).append(" ");
        }
        if(StringUtils.isNotEmpty(this.lastName)){
            stringBuilder.append(this.lastName).append(" ");
        }
        if(StringUtils.isNotEmpty(this.suffix)){
            stringBuilder.append(this.suffix);
        }
        return stringBuilder.toString();
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result
                + ((middleName == null) ? 0 : middleName.hashCode());
        result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
        result = prime * result + ((suffix == null) ? 0 : suffix.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Name other = (Name) obj;
        if(StringUtils.equalsIgnoreCase(this.firstName.trim(), other.firstName.trim())
                && StringUtils.equalsIgnoreCase(this.middleName.trim(), other.middleName.trim())
                && StringUtils.equalsIgnoreCase(this.lastName.trim(), other.lastName.trim())){
            return true;
        }else  if(StringUtils.equalsIgnoreCase(this.firstName.trim(), other.firstName.trim())
                && StringUtils.equalsIgnoreCase(this.lastName.trim(), other.lastName.trim())){
            return true ;

        }
        return false;
    }


}
