package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonalDiscussion extends ApplicationVerification {

    @JsonProperty("dtDiscussion")
    private Date discussiondate;

    @JsonProperty("sPDDOneBy")
    private String pdDoneBy;

    @JsonProperty("bOriginalKYCVerified")
    private boolean originalKYCVerified;

    @JsonProperty("sContactedTo")
    private String contactedto;

    @JsonProperty("sMeetingPlace")
    private String meetingplace;

    @JsonProperty("sDistanceFromBranch")
    private String branchdistance;

    @JsonProperty("bCoAppStayTogether")
    private boolean coAppstayTogether;

    @JsonProperty("iTotalMembers")
    private int totalmembers;

    @JsonProperty("aEarningMembers")
    private List<EarningMember> earningmembers;

    @JsonProperty("iTotalNoOfEarningMembers")
    private int totalNoOfEarningMembers;

    @JsonProperty("bDivisionInFamilyBusiness")
    private boolean divisionInFamilyBusiness;

    @JsonProperty("sFamilyBusinessGeneration")
    private String familyBusinessGeneration;

    @JsonProperty("bHasTv")
    private boolean tv;

    @JsonProperty("bHas4Wheeler")
    private boolean fourwheeler;

    @JsonProperty("bHas2Wheeler")
    private boolean twowheeler;

    @JsonProperty("bAC")
    private boolean ac;

    @JsonProperty("bPostpaidMobile")
    private boolean postpaidMobile;

    @JsonProperty("dMonthlySavings")
    private double monthlySavings;

    @JsonProperty("sElecBill")
    private String electricity;

    @JsonProperty("sLifestyle")
    private String lifestyle;

    @JsonProperty("sBusinessNature")
    private String businessNature;

    @JsonProperty("sSector")
    private String sector;

    @JsonProperty("iAvgMargin")
    private int avgSectorMargin;

    @JsonProperty("bSeaonality")
    private boolean seasonal;

    @JsonProperty("bBusinessRegistration")
    private boolean businessRegistration;

    @JsonProperty("sMarginLevel")
    private String marginLevel;

    @JsonProperty("dDailyTurnover")
    private double dailyTurnover;

    @JsonProperty("dMonthlyTurnover")
    private double monthlyTurnover;

    @JsonProperty("dMonthlyMaterialCost")
    private double monthlyMaterialCost;

    @JsonProperty("dMonthlySalaryToEmployees")
    private double monthlySalaryToEmployees;

    @JsonProperty("dMonthlyRent")
    private double monthlyRent;

    @JsonProperty("dMonthlyOfficeUtilityBills")
    private double monthlyOfficeUtilityBills;

    @JsonProperty("dOtherMonthlyExpenses")
    private double otherMonthlyExpenses;

    @JsonProperty("dMonthlyLivingExpenseOfCustomer")
    private double monthlyLivingExpenseOfCustomer;

    @JsonProperty("dMonthlyEducationExpensesForKids")
    private double monthlyEducationExpensesForKids;

    @JsonProperty("dMonthlyRentForHome")
    private double monthlyRentForHome;

    @JsonProperty("dMonthlyConveyance")
    private double monthlyConveyance;

    @JsonProperty("dOtherMonthlyPersonalExpense")
    private double otherMonthlyPersonalExpense;

    @JsonProperty("dMonthlyProfit")
    private double monthlyProfit;

    @JsonProperty("sTrendInLast3Years")
    private String trendInLast3Years;

    @JsonProperty("iNoOfPartners")
    private int noOfPartners;

    @JsonProperty("dMonthlyGrossIncome")
    private double monthlyGrossIncome;

    @JsonProperty("dMonthlyNetIncome")
    private double monthlyNetIncome;

    @JsonProperty("dMonthlyObligations")
    private double monthlyObligations;

    @JsonProperty("dRentalOtherIncome")
    private double rentalOrOtherIncome;

    @JsonProperty("sBusinessPremisesOwnership")
    private String businessPremisesOwnership;

    @JsonProperty("iCurrentPremisesYears")
    private int currentPremisesYears;

    @JsonProperty("iCurrentBusinessYears")
    private int currentBusinessYears;

    @JsonProperty("sStaffCount")
    private String staffCount;

    @JsonProperty("sActivityLevel")
    private String activityLevel;

    @JsonProperty("sStockLevel")
    private String stockLevel;

    @JsonProperty("sLocatingAddress")
    private String locatingAddress;

    @JsonProperty("sAccesibility")
    private String accesibility;

    @JsonProperty("sLocality")
    private String locality;

    @JsonProperty("sOfficeType")
    private String officeType;

    @JsonProperty("bOfficeSetUp")
    private boolean officeSetUp;

    @JsonProperty("dtBusinessStartDate")
    private Date businessStartDate;

    @JsonProperty("sAreaOfBusinessPremise")
    private String areaOfBusinessPremise;

    @JsonProperty("bNameBoardSeenOutsideOffice")
    private boolean nameBoardSeenOutsideOffice;

    @JsonProperty("sBusinessProofSeen")
    private String businessProofSeen;

    @JsonProperty("sStockValue")
    private String stockValue;

    @JsonProperty("bStockRegisterAvailable")
    private boolean stockRegisterAvailable;

    @JsonProperty("sAppearanceOfStock")
    private String appearanceOfStock;

    @JsonProperty("sPercentageBusiness")
    private String percentageBusiness;

    @JsonProperty("sMarketReputation")
    private String marketReputation;

    @JsonProperty("sBillsChecked")
    private String billsChecked;

    @JsonProperty("aCreditors")
    private List<CreditorsDebtors> creditors;

    @JsonProperty("aDebtors")
    private List<CreditorsDebtors> debtors;

    @JsonProperty("sLoanPurpose")
    private String loanPurpose;

    @JsonProperty("bOwned")
    private boolean owned;

    @JsonProperty("sPresentAddressYrs")
    private String presentAddressYrs;

    @JsonProperty("dAcceptableLoanAmt")
    private double acceptableLoanAmt;

    @JsonProperty("sResidence")
    private String residence;

    @JsonProperty("sResidenceOther")
    private String otherResidence;

    @JsonProperty("sBusiness")
    private String business;

    @JsonProperty("sPlots")
    private String plots;

    @JsonProperty("sTotalInvestment")
    private String totalInvestment;

    @JsonProperty("sMonthlyInvestment")
    private String monthlyInvestment;

    @JsonProperty("sResidenceValue")
    private String residenceValue;

    @JsonProperty("sBusinessValue")
    private String businessValue;

    @JsonProperty("sPlotValue")
    private String plotsValue;

    @JsonProperty("sMonthlyInvestmentValue")
    private String monthlyInvestmentValue;

    @JsonProperty("aDelayedRepaymentDetails")
    private List<DelayedRepaymentDetails> delayedRepaymentDetailsList;

    @JsonProperty("sNoOfCurrentLoansRunning")
    private String noOfCurrentLoansRunning;

    @JsonProperty("sCurrentMonthlyObligation")
    private String currentMonthlyObligation;

    @JsonProperty("bPositive")
    private boolean positive;

    @JsonProperty("sComfortableLoanAmount")
    private String comfortableLoanAmount;

    @JsonProperty("sRecommendedLoanAmount")
    private String recommendedLoanAmount;

    @JsonProperty("sComments")
    private String comments;

    @JsonProperty("sOtherIncomeSources")
    private String otherIncomeSources;

    @JsonProperty("sBusinessType")
    private String businessType;

    @JsonProperty("sGeneralDtlsRemarks")
    private String generalDtlsRemarks;

    @JsonProperty("sFamilyBkgroundRemarks")
    private String familyBkgroundRemarks;

    @JsonProperty("sBusinessDtlsRemarks")
    private String businessDtlsRemarks;

    @JsonProperty("sCashFlowDtlsRemarks")
    private String cashFlowDtlsRemarks;

    @JsonProperty("sBusinessOrResidenceRemarks")
    private String businessOrResidenceRemarks;

    @JsonProperty("sSupplierOrCreditorRemarks")
    private String supplierOrCreditorRemarks;

    @JsonProperty("sCustOrDebtorsRemarks")
    private String custOrDebtorsRemarks;

    @JsonProperty("sCashFlowStmtRemarks")
    private String cashFlowStmtRemarks;

    @JsonProperty("sAssetsOrInvestmentsRemarks")
    private String assetsOrInvestmentsRemarks;

    @JsonProperty("sRepaymentHistoryRemarks")
    private String repaymentHistoryRemarks;

    //Fields for PL

    @JsonProperty("sOtherRemarks")
    private String othersRemarks;

    @JsonProperty("sObligationsRemarks")
    private String obligationRemarks;

    @JsonProperty("sPdLoanAmt")
    private String loanAmount;

   @JsonProperty("sPdTenor")
   private String tenor;

    @JsonProperty("sQualification")
    private String qualification;

    @JsonProperty("sWorkingWith")
    private String workingWith;

    @JsonProperty("sSince")
    private String since;

    @JsonProperty("sDesignation")
    private String designation;

    @JsonProperty("sPriorTo")
    private String priorTo;

    @JsonProperty("sTwe")
    private String twe;

    @JsonProperty("sOfficeEmail")
    private String officeEmail;

    @JsonProperty("sAddrOne")
    private String addrOne;

    @JsonProperty("sAddrTwo")
    private String addrTwo;

    @JsonProperty("sLand")
    private String land;

    @JsonProperty("sPinCode")
    private String pin;

   @JsonProperty("sCity")
   private String city;

    @JsonProperty("sState")
   private String state;

    @JsonProperty("sYearCurrent")
    private String yearCurrent;

    @JsonProperty("sPdStatus")
    private String pdStatus;

    //adding field to store cibil score

    @JsonProperty("sCibilScrPd")
    private String cibilScore;

    @JsonProperty("sIndustry")
    private String industry;

    //for tricolour
    @JsonProperty("sPromoterProfile")
    private String promoterProfile;

    @JsonProperty("sFinancialProfile")
    private String financialProfile;

    @JsonProperty("sEndUse")
    private String endUse;

    @JsonProperty("sBureauProfile")
    private String bureauProfile;

    @JsonProperty("sSubIndustry")
    private String subIndustry;

    @JsonProperty("dtCreditDiscussion")
    private Date creditDiscussion;
}

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class DelayedRepaymentDetails {

    @JsonProperty("sReasonForDelay")
    private String reasonForDelay;

    @JsonProperty("sNoOfBouncing")
    private String noOfBouncing;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("bAnyProofSubmitted")
    private boolean anyProofSubmitted;

    @JsonProperty("sMitigant")
    private String mitigant;

    @JsonProperty("sOtherMitigant")
    private String OtherMitigant;

}
