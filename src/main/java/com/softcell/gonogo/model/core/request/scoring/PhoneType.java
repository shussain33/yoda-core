package com.softcell.gonogo.model.core.request.scoring;


public class PhoneType {

    private String countryCode;
    private String areaCode;
    private String number;
    private String phoneNumberExtension;
    private String reportedDate;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneNumberExtension() {
        return phoneNumberExtension;
    }

    public void setPhoneNumberExtension(String phoneNumberExtension) {
        this.phoneNumberExtension = phoneNumberExtension;
    }

    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    @Override
    public String toString() {
        return "PhoneType [countryCode=" + countryCode + ", areaCode="
                + areaCode + ", number=" + number + ", phoneNumberExtension="
                + phoneNumberExtension + ", reportedDate=" + reportedDate + "]";
    }
}
