package com.softcell.gonogo.model.core.request.scoring;

/**
 * @author yogeshb
 */

public class LoanDetails {
    private String acctType;
    private String freq;
    private String status;
    private String acctNumber;
    private String disbursedAmt;
    private String currentBal;
    private String installmentAmt;
    private String overdueAmt;
    private String writeOffAmt;
    private String disbursedDt;
    private String closedDt;
    private String recentDelinqDt;
    private String dpd;
    private String inqCnt;
    private String infoAsOn;
    private String worstDelequencyAmount;
    private String paymentHistory;
    private String isActiveBorrower;
    private String noOfBorrowers;
    private String comment;

    public String getAcctType() {
        return acctType;
    }

    public void setAcctType(String acctType) {
        this.acctType = acctType;
    }

    public String getFreq() {
        return freq;
    }

    public void setFreq(String freq) {
        this.freq = freq;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }

    public String getDisbursedAmt() {
        return disbursedAmt;
    }

    public void setDisbursedAmt(String disbursedAmt) {
        this.disbursedAmt = disbursedAmt;
    }

    public String getCurrentBal() {
        return currentBal;
    }

    public void setCurrentBal(String currentBal) {
        this.currentBal = currentBal;
    }

    public String getInstallmentAmt() {
        return installmentAmt;
    }

    public void setInstallmentAmt(String installmentAmt) {
        this.installmentAmt = installmentAmt;
    }

    public String getOverdueAmt() {
        return overdueAmt;
    }

    public void setOverdueAmt(String overdueAmt) {
        this.overdueAmt = overdueAmt;
    }

    public String getWriteOffAmt() {
        return writeOffAmt;
    }

    public void setWriteOffAmt(String writeOffAmt) {
        this.writeOffAmt = writeOffAmt;
    }

    public String getDisbursedDt() {
        return disbursedDt;
    }

    public void setDisbursedDt(String disbursedDt) {
        this.disbursedDt = disbursedDt;
    }

    public String getClosedDt() {
        return closedDt;
    }

    public void setClosedDt(String closedDt) {
        this.closedDt = closedDt;
    }

    public String getRecentDelinqDt() {
        return recentDelinqDt;
    }

    public void setRecentDelinqDt(String recentDelinqDt) {
        this.recentDelinqDt = recentDelinqDt;
    }

    public String getDpd() {
        return dpd;
    }

    public void setDpd(String dpd) {
        this.dpd = dpd;
    }

    public String getInqCnt() {
        return inqCnt;
    }

    public void setInqCnt(String inqCnt) {
        this.inqCnt = inqCnt;
    }

    public String getInfoAsOn() {
        return infoAsOn;
    }

    public void setInfoAsOn(String infoAsOn) {
        this.infoAsOn = infoAsOn;
    }

    public String getWorstDelequencyAmount() {
        return worstDelequencyAmount;
    }

    public void setWorstDelequencyAmount(String worstDelequencyAmount) {
        this.worstDelequencyAmount = worstDelequencyAmount;
    }

    public String getPaymentHistory() {
        return paymentHistory;
    }

    public void setPaymentHistory(String paymentHistory) {
        this.paymentHistory = paymentHistory;
    }

    public String getIsActiveBorrower() {
        return isActiveBorrower;
    }

    public void setIsActiveBorrower(String isActiveBorrower) {
        this.isActiveBorrower = isActiveBorrower;
    }

    public String getNoOfBorrowers() {
        return noOfBorrowers;
    }

    public void setNoOfBorrowers(String noOfBorrowers) {
        this.noOfBorrowers = noOfBorrowers;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "LoanDetails [acctType=" + acctType + ", freq=" + freq
                + ", status=" + status + ", acctNumber=" + acctNumber
                + ", disbursedAmt=" + disbursedAmt + ", currentBal="
                + currentBal + ", installmentAmt=" + installmentAmt
                + ", overdueAmt=" + overdueAmt + ", writeOffAmt=" + writeOffAmt
                + ", disbursedDt=" + disbursedDt + ", closedDt=" + closedDt
                + ", recentDelinqDt=" + recentDelinqDt + ", dpd=" + dpd
                + ", inqCnt=" + inqCnt + ", infoAsOn=" + infoAsOn
                + ", worstDelequencyAmount=" + worstDelequencyAmount
                + ", paymentHistory=" + paymentHistory + ", isActiveBorrower="
                + isActiveBorrower + ", noOfBorrowers=" + noOfBorrowers
                + ", comment=" + comment + "]";
    }
}
