package com.softcell.gonogo.model.core.request.scoring;

public class CibilRespName {
    private String name1;

    private String name2;

    private String name3;

    private String name4;

    private String name5;

    private String dob;

    private String gender;

    private String dateOfEntryForErrorCode;

    private String errorSegmentTag;

    private String errorCode;

    private String dateOfEntryForCibilRemarksCode;

    private String cibilRemarkCode;

    private String dateOfEntryForErrorOrDisputeRemarksCode;

    private String errorOrDisputeRemarksCode1;

    private String errorOrDisputeRemarksCode2;

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public String getName5() {
        return name5;
    }

    public void setName5(String name5) {
        this.name5 = name5;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfEntryForErrorCode() {
        return dateOfEntryForErrorCode;
    }

    public void setDateOfEntryForErrorCode(String dateOfEntryForErrorCode) {
        this.dateOfEntryForErrorCode = dateOfEntryForErrorCode;
    }

    public String getErrorSegmentTag() {
        return errorSegmentTag;
    }

    public void setErrorSegmentTag(String errorSegmentTag) {
        this.errorSegmentTag = errorSegmentTag;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDateOfEntryForCibilRemarksCode() {
        return dateOfEntryForCibilRemarksCode;
    }

    public void setDateOfEntryForCibilRemarksCode(
            String dateOfEntryForCibilRemarksCode) {
        this.dateOfEntryForCibilRemarksCode = dateOfEntryForCibilRemarksCode;
    }

    public String getCibilRemarkCode() {
        return cibilRemarkCode;
    }

    public void setCibilRemarkCode(String cibilRemarkCode) {
        this.cibilRemarkCode = cibilRemarkCode;
    }

    public String getDateOfEntryForErrorOrDisputeRemarksCode() {
        return dateOfEntryForErrorOrDisputeRemarksCode;
    }

    public void setDateOfEntryForErrorOrDisputeRemarksCode(
            String dateOfEntryForErrorOrDisputeRemarksCode) {
        this.dateOfEntryForErrorOrDisputeRemarksCode = dateOfEntryForErrorOrDisputeRemarksCode;
    }

    public String getErrorOrDisputeRemarksCode1() {
        return errorOrDisputeRemarksCode1;
    }

    public void setErrorOrDisputeRemarksCode1(String errorOrDisputeRemarksCode1) {
        this.errorOrDisputeRemarksCode1 = errorOrDisputeRemarksCode1;
    }

    public String getErrorOrDisputeRemarksCode2() {
        return errorOrDisputeRemarksCode2;
    }

    public void setErrorOrDisputeRemarksCode2(String errorOrDisputeRemarksCode2) {
        this.errorOrDisputeRemarksCode2 = errorOrDisputeRemarksCode2;
    }

    @Override
    public String toString() {
        return "CibilRespName [name1=" + name1 + ", name2=" + name2
                + ", name3=" + name3 + ", name4=" + name4 + ", name5=" + name5
                + ", dob=" + dob + ", gender=" + gender
                + ", dateOfEntryForErrorCode=" + dateOfEntryForErrorCode
                + ", errorSegmentTag=" + errorSegmentTag + ", errorCode="
                + errorCode + ", dateOfEntryForCibilRemarksCode="
                + dateOfEntryForCibilRemarksCode + ", cibilRemarkCode="
                + cibilRemarkCode
                + ", dateOfEntryForErrorOrDisputeRemarksCode="
                + dateOfEntryForErrorOrDisputeRemarksCode
                + ", errorOrDisputeRemarksCode1=" + errorOrDisputeRemarksCode1
                + ", errorOrDisputeRemarksCode2=" + errorOrDisputeRemarksCode2
                + "]";
    }


}
