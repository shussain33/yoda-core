package com.softcell.gonogo.model.core.scoring.response;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoreAndDecisionDomain {

    @JsonProperty("isErrorMsg")
    private boolean errorMsg;
    private boolean flag;
    @JsonProperty("errorMsg")
    private String errorMessage;

    @JsonProperty("policyId")
    private Long policyId;

    @JsonProperty("Score")
    private Long score;

    @JsonProperty("Decision")
    private String decision;

    @JsonProperty("AppScoreJson")
    private String appScoreJson;

    @JsonProperty("Scoring")
    private String scoring;

    @JsonProperty("Eligibility")
    private Eligibility eligibility;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("SCORE-DATA")
    private ScoreData scoreData;

    @JsonProperty("SCORE_TREE")
    private Object scoreTree;

    @JsonProperty("DECISION_RESPONSE")
    private DecisionResponse decisionResponse;

    @JsonProperty("DERIVED_FIELDS")
    private Object derivedFields;

    @JsonProperty("APPLICANT_ID")
    private String applicantId;

    @JsonProperty("SCORE_AND_DECISION_DOMAINS")
    private List<?> scoreAndDecisionDomains;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * @param additionalProperties the additionalProperties to set
     */
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    /**
     * @return the errorMsg
     */
    public boolean isErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(boolean errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * @return the flag
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the policyId
     */
    public Long getPolicyId() {
        return policyId;
    }

    /**
     * @param policyId the policyId to set
     */
    public void setPolicyId(Long policyId) {
        this.policyId = policyId;
    }

    /**
     * @return the score
     */
    public Long getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(Long score) {
        this.score = score;
    }

    /**
     * @return the decision
     */
    public String getDecision() {
        return decision;
    }

    /**
     * @param decision the decision to set
     */
    public void setDecision(String decision) {
        this.decision = decision;
    }

    /**
     * @return the appScoreJson
     */
    public String getAppScoreJson() {
        return appScoreJson;
    }

    /**
     * @param appScoreJson the appScoreJson to set
     */
    public void setAppScoreJson(String appScoreJson) {
        this.appScoreJson = appScoreJson;
    }

    /**
     * @return the scoring
     */
    public String getScoring() {
        return scoring;
    }

    /**
     * @param scoring the scoring to set
     */
    public void setScoring(String scoring) {
        this.scoring = scoring;
    }

    /**
     * @return the eligibility
     */
    public Eligibility getEligibility() {
        return eligibility;
    }

    /**
     * @param eligibility the eligibility to set
     */
    public void setEligibility(Eligibility eligibility) {
        this.eligibility = eligibility;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the scoreData
     */
    public ScoreData getScoreData() {
        return scoreData;
    }

    /**
     * @param scoreData the scoreData to set
     */
    public void setScoreData(ScoreData scoreData) {
        this.scoreData = scoreData;
    }

    /**
     * @return the scoreTree
     */
    public Object getScoreTree() {
        return scoreTree;
    }

    /**
     * @param scoreTree the scoreTree to set
     */
    public void setScoreTree(Object scoreTree) {
        this.scoreTree = scoreTree;
    }

    /**
     * @return the decisionResponse
     */
    public DecisionResponse getDecisionResponse() {
        return decisionResponse;
    }

    /**
     * @param decisionResponse the decisionResponse to set
     */
    public void setDecisionResponse(DecisionResponse decisionResponse) {
        this.decisionResponse = decisionResponse;
    }

    /**
     * @return the derivedFields
     */
    public Object getDerivedFields() {
        return derivedFields;
    }

    /**
     * @param derivedFields the derivedFields to set
     */
    public void setDerivedFields(Object derivedFields) {
        this.derivedFields = derivedFields;
    }

    /**
     * @return the applicantId
     */
    public String getApplicantId() {
        return applicantId;
    }

    /**
     * @param applicantId the applicantId to set
     */
    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    /**
     * @return the scoreAndDecisionDomains
     */
    public List<?> getScoreAndDecisionDomains() {
        return scoreAndDecisionDomains;
    }

    /**
     * @param scoreAndDecisionDomains the scoreAndDecisionDomains to set
     */
    public void setScoreAndDecisionDomains(
            List<ScoreAndDecisionDomain> scoreAndDecisionDomains) {
        this.scoreAndDecisionDomains = scoreAndDecisionDomains;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ScoreAndDecisionDomain [errorMsg=");
        builder.append(errorMsg);
        builder.append(", flag=");
        builder.append(flag);
        builder.append(", errorMessage=");
        builder.append(errorMessage);
        builder.append(", policyId=");
        builder.append(policyId);
        builder.append(", score=");
        builder.append(score);
        builder.append(", decision=");
        builder.append(decision);
        builder.append(", appScoreJson=");
        builder.append(appScoreJson);
        builder.append(", scoring=");
        builder.append(scoring);
        builder.append(", eligibility=");
        builder.append(eligibility);
        builder.append(", status=");
        builder.append(status);
        builder.append(", scoreData=");
        builder.append(scoreData);
        builder.append(", scoreTree=");
        builder.append(scoreTree);
        builder.append(", decisionResponse=");
        builder.append(decisionResponse);
        builder.append(", derivedFields=");
        builder.append(derivedFields);
        builder.append(", applicantId=");
        builder.append(applicantId);
        builder.append(", scoreAndDecisionDomains=");
        builder.append(scoreAndDecisionDomains);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result
                + ((appScoreJson == null) ? 0 : appScoreJson.hashCode());
        result = prime * result
                + ((applicantId == null) ? 0 : applicantId.hashCode());
        result = prime * result
                + ((decision == null) ? 0 : decision.hashCode());
        result = prime
                * result
                + ((decisionResponse == null) ? 0 : decisionResponse.hashCode());
        result = prime * result
                + ((derivedFields == null) ? 0 : derivedFields.hashCode());
        result = prime * result
                + ((eligibility == null) ? 0 : eligibility.hashCode());
        result = prime * result
                + ((errorMessage == null) ? 0 : errorMessage.hashCode());
        result = prime * result + (errorMsg ? 1231 : 1237);
        result = prime * result + (flag ? 1231 : 1237);
        result = prime * result
                + ((policyId == null) ? 0 : policyId.hashCode());
        result = prime * result + ((score == null) ? 0 : score.hashCode());
        result = prime
                * result
                + ((scoreAndDecisionDomains == null) ? 0
                : scoreAndDecisionDomains.hashCode());
        result = prime * result
                + ((scoreData == null) ? 0 : scoreData.hashCode());
        result = prime * result
                + ((scoreTree == null) ? 0 : scoreTree.hashCode());
        result = prime * result + ((scoring == null) ? 0 : scoring.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ScoreAndDecisionDomain other = (ScoreAndDecisionDomain) obj;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (appScoreJson == null) {
            if (other.appScoreJson != null)
                return false;
        } else if (!appScoreJson.equals(other.appScoreJson))
            return false;
        if (applicantId == null) {
            if (other.applicantId != null)
                return false;
        } else if (!applicantId.equals(other.applicantId))
            return false;
        if (decision == null) {
            if (other.decision != null)
                return false;
        } else if (!decision.equals(other.decision))
            return false;
        if (decisionResponse == null) {
            if (other.decisionResponse != null)
                return false;
        } else if (!decisionResponse.equals(other.decisionResponse))
            return false;
        if (derivedFields == null) {
            if (other.derivedFields != null)
                return false;
        } else if (!derivedFields.equals(other.derivedFields))
            return false;
        if (eligibility == null) {
            if (other.eligibility != null)
                return false;
        } else if (!eligibility.equals(other.eligibility))
            return false;
        if (errorMessage == null) {
            if (other.errorMessage != null)
                return false;
        } else if (!errorMessage.equals(other.errorMessage))
            return false;
        if (errorMsg != other.errorMsg)
            return false;
        if (flag != other.flag)
            return false;
        if (policyId == null) {
            if (other.policyId != null)
                return false;
        } else if (!policyId.equals(other.policyId))
            return false;
        if (score == null) {
            if (other.score != null)
                return false;
        } else if (!score.equals(other.score))
            return false;
        if (scoreAndDecisionDomains == null) {
            if (other.scoreAndDecisionDomains != null)
                return false;
        } else if (!scoreAndDecisionDomains
                .equals(other.scoreAndDecisionDomains))
            return false;
        if (scoreData == null) {
            if (other.scoreData != null)
                return false;
        } else if (!scoreData.equals(other.scoreData))
            return false;
        if (scoreTree == null) {
            if (other.scoreTree != null)
                return false;
        } else if (!scoreTree.equals(other.scoreTree))
            return false;
        if (scoring == null) {
            if (other.scoring != null)
                return false;
        } else if (!scoring.equals(other.scoring))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }
}
