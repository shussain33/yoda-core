package com.softcell.gonogo.model.core.excelConfiguration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.error.Error;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 2/7/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelResponse {

    @JsonProperty("sFormat")
    private String format;

    @JsonProperty("sExcelName")
    private String excelName;

    @JsonProperty("aByte")
    private byte[] fileContent;

    @JsonProperty("aError")
    private Error error;
}
