package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.actions.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TimelineData {

    @JsonProperty("sAction")
    private String action;

    @JsonProperty("sRemarks")
    private String remarks;

    @JsonProperty("dtActionDate")
    private Date actionDate;

    @JsonProperty("lDuration")
    private long duration;

    @JsonProperty("sCustomeMsg")
    private String customMsg;

    @JsonProperty("oUserDetails")
    private User loggedInUserDetails;

    @JsonProperty("sCurrentStage")
    private String stage;

    @JsonProperty("sStageChangedTo")
    private String stageChangedTo;

    @JsonProperty("sLockedUserId")
    private String lockedUserId;

    @JsonProperty("sLockedUserName")
    private String lockedUserName;

    @JsonProperty("sChangedUserId")
    private String changedUserId;

    @JsonProperty("sChangedUserName")
    private String changedUserName;
}
