package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityByRTR {

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("dOroginalLoanAmount")
    private double originalLoanAmount;

    @JsonProperty("iMOB")
    private int mob;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("iTotalTenor")
    private int totalTenor;

    @JsonProperty("iBalanceTenor")
    private int balanceTenor;

    @JsonProperty("dPOS")
    private double pos;

    @JsonProperty("dApplicablePercet")
    private double applicablePercent;

    @JsonProperty("dPerposedLoanAMT")
    private double proposedLoanAmount;

    @JsonProperty("dAppliedPercent")
    private double appliedPercent;

    @JsonProperty("iLoanTenor")
    private int loanTenor;

    @JsonProperty("dRoi")
    private double roi;

    @JsonProperty("dEmi")
    private double emi;

}
