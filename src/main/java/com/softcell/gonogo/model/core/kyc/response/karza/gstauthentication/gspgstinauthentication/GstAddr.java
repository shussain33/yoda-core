package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GstAddr {

    @JsonProperty("loc")
    private String loc;

    @JsonProperty("city")
    private String city;

    @JsonProperty("dst")
    private String dst;

    @JsonProperty("pncd")
    private String pncd;

    @JsonProperty("stcd")
    private String stcd;

    @JsonProperty("st")
    private String st;

    @JsonProperty("lt")
    private String lt;

    @JsonProperty("bno")
    private String bno;

    @JsonProperty("lg")
    private String lg;

    @JsonProperty("bnm")
    private String bnm;

    @JsonProperty("flno")
    private String flno;
}