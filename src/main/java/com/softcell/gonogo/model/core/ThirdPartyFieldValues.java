package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "thirdPartyFieldValues")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThirdPartyFieldValues {

    public String instituteName;

    //    e.g. POWERCURVE	 addressType	ssgValue 3piValue
    public Map<String, Map<String, Map<String, String>>> thirdParyApi;
}
