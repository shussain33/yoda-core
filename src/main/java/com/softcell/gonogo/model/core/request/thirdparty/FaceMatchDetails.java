package com.softcell.gonogo.model.core.request.thirdparty;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suraj on 13/10/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FaceMatchDetails {

    @JsonProperty("iStatusCode")
    private int statusCode;

    @JsonProperty("sRequestId")
    private String requestId;

    @JsonProperty("oResult")
    private FaceMatchResult result;

}
