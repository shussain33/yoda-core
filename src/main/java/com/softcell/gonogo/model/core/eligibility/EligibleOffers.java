package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 23/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibleOffers {

    @JsonProperty("sEligibilityName")
    private String eligibilityName;

    @JsonProperty("dEligibilityAmount")
    private double eligibilityAmount;

    @JsonProperty("sEligibleTenor")
    private String eligibleTenor;

    @JsonProperty("dFixedObligations")
    private double fixedObligations;

    @JsonProperty("dIncome")
    private double income;

    @JsonProperty("dEmi")
    private double emi;

    @JsonProperty("bSelectedOffer")
    private boolean selectedOffer;
}
