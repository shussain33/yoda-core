package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtpOption {

    @JsonProperty("OTP_CHANNEL")
    private String otpChannel;

    public static Builder builder() {
        return new Builder();
    }

    public String getOtpChannel() {
        return otpChannel;
    }

    public void setOtpChannel(String otpChannel) {
        this.otpChannel = otpChannel;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OtpOption{");
        sb.append("otpChannel='").append(otpChannel).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OtpOption)) return false;
        OtpOption otpOption = (OtpOption) o;
        return Objects.equal(getOtpChannel(), otpOption.getOtpChannel());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getOtpChannel());
    }

    public static class Builder {
        private OtpOption otpOption = new OtpOption();

        public OtpOption build() {
            return otpOption;
        }

        public Builder otpChannel(String otpChannel) {
            this.otpOption.otpChannel = otpChannel;
            return this;
        }
    }
}
