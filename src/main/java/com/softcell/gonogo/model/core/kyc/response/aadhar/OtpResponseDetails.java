package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;

import java.util.List;

/**
 * @author yogeshb
 */
public class OtpResponseDetails {
    @JsonProperty("UIDAI-STATUS")
    private String uidaiStatus;

    @JsonProperty("OTP-RESPONSE")
    private OtpResponse otpResponse;

    @JsonProperty("ERRORS")
    private List<Errors> errors;

    public String getUidaiStatus() {
        return uidaiStatus;
    }

    public void setUidaiStatus(String uidaiStatus) {
        this.uidaiStatus = uidaiStatus;
    }

    public OtpResponse getOtpResponse() {
        return otpResponse;
    }

    public void setOtpResponse(OtpResponse otpResponse) {
        this.otpResponse = otpResponse;
    }

    public List<Errors> getErrors() {
        return errors;
    }

    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "OtpResponseDetails [uidaiStatus=" + uidaiStatus
                + ", otpResponse=" + otpResponse + ", errors=" + errors + "]";
    }


}
