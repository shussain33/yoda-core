package com.softcell.gonogo.model.core.scoring.response;

public class Score {
    private Object Details;
    private Object Scoring;

    public Object getDetails() {
        return Details;
    }

    public void setDetails(Object details) {
        Details = details;
    }

    public Object getScoring() {
        return Scoring;
    }

    public void setScoring(Object scoring) {
        Scoring = scoring;
    }

    @Override
    public String toString() {
        return "Score [Details=" + Details + ", Scoring=" + Scoring + "]";
    }


}
