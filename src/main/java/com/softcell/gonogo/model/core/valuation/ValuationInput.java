package com.softcell.gonogo.model.core.valuation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Name;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by suhasini on 3/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValuationInput {

    @JsonProperty("oApplicantName")
    private Name applicantName;

    @JsonProperty("oCoApplicantName")
    private Name coApplicantName;

    @JsonProperty("aPropertyOwnerNames")
    private List<Name> propertyOwnerNames;

    @JsonProperty("sPropertyType")
    private String propertyType;

    @JsonProperty("oPropertyAddress")
    private CustomerAddress propertyAddress;

    @JsonProperty("oContactPersonName")
    private Name contactPersonName;

    @JsonProperty("sContactNumber")
    private String contactNumber;

    @JsonProperty("bPropertyDocsProvided")
    private boolean propertyDocsprovided;

    @JsonProperty("bApprovedMapProvided")
    private boolean approvedMapProvided;

    @JsonProperty("bLatestDeedCopyProvided")
    private boolean latestDeedCopyProvided;
}
