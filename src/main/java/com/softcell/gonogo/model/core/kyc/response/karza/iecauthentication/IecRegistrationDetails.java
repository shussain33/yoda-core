package com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IecRegistrationDetails {

    @JsonProperty("reg_date")
    private String reg_date;

    @JsonProperty("reg_no")
    private String reg_no;

    @JsonProperty("reg_place")
    private String reg_place;

    @JsonProperty("reg_type")
    private String reg_type;

}