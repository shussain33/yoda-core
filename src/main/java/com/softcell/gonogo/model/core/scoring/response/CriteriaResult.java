package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * @author sumit kumar class for set criteria rules outcome result
 */

public class CriteriaResult {

    @JsonProperty("CriteriaID")
    private long criteriaID;

    @JsonProperty("RuleName")
    private String ruleName;

    @JsonProperty("Outcome")
    private String outcome;

    @JsonProperty("Remark")
    private String remark;

    @JsonProperty("Exp")
    private String expression;

    @JsonProperty("Value")
    private String fieldValue;

    @JsonProperty("Values")
    private Map<String, String> fieldValues;

    public Map<String, String> getFieldValues() {
        return fieldValues;
    }

    public void setFieldValues(Map<String, String> fieldValues) {
        this.fieldValues = fieldValues;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public long getCriteriaID() {
        return criteriaID;
    }

    public void setCriteriaID(long criteriaID) {
        this.criteriaID = criteriaID;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CriteriaResult [criteriaID=");
        builder.append(criteriaID);
        builder.append(", ruleName=");
        builder.append(ruleName);
        builder.append(", outcome=");
        builder.append(outcome);
        builder.append(", remark=");
        builder.append(remark);
        builder.append(", expression=");
        builder.append(expression);
        builder.append(", fieldValue=");
        builder.append(fieldValue);
        builder.append(", fieldValues=");
        builder.append(fieldValues);
        builder.append("]");
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (criteriaID ^ (criteriaID >>> 32));
        result = prime * result
                + ((expression == null) ? 0 : expression.hashCode());
        result = prime * result
                + ((fieldValue == null) ? 0 : fieldValue.hashCode());
        result = prime * result
                + ((fieldValues == null) ? 0 : fieldValues.hashCode());
        result = prime * result + ((outcome == null) ? 0 : outcome.hashCode());
        result = prime * result + ((remark == null) ? 0 : remark.hashCode());
        result = prime * result
                + ((ruleName == null) ? 0 : ruleName.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CriteriaResult other = (CriteriaResult) obj;
        if (criteriaID != other.criteriaID)
            return false;
        if (expression == null) {
            if (other.expression != null)
                return false;
        } else if (!expression.equals(other.expression))
            return false;
        if (fieldValue == null) {
            if (other.fieldValue != null)
                return false;
        } else if (!fieldValue.equals(other.fieldValue))
            return false;
        if (fieldValues == null) {
            if (other.fieldValues != null)
                return false;
        } else if (!fieldValues.equals(other.fieldValues))
            return false;
        if (outcome == null) {
            if (other.outcome != null)
                return false;
        } else if (!outcome.equals(other.outcome))
            return false;
        if (remark == null) {
            if (other.remark != null)
                return false;
        } else if (!remark.equals(other.remark))
            return false;
        if (ruleName == null) {
            if (other.ruleName != null)
                return false;
        } else if (!ruleName.equals(other.ruleName))
            return false;
        return true;
    }

}
