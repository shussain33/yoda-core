package com.softcell.gonogo.model.core.digitization;

import com.softcell.utils.GngNumUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mahesh on 5/6/17.
 */
public class AgreementFormMapper {

    private static final Logger logger = LoggerFactory.getLogger(AgreementFormMapper.class);

    public static VelocityContext mapAgreementForm(ApplicationForm applicationForm) throws ParseException {

        List<Character> list;
        VelocityContext context = new VelocityContext();
        // Applicant Name Is the borrower Name
        if (StringUtils.isNotBlank(applicationForm.getApplicantName())) {
            context.put("borrowerName", applicationForm.getApplicantName());// string
        }

        if (StringUtils.isNotBlank(applicationForm.getEmailID())) {
            context.put("emailID", applicationForm.getEmailID());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getMobile())) {
            context.put("mobileNo", applicationForm.getMobile()); // list
        }

        if (StringUtils.isNotBlank(applicationForm.getResidenseAddress())) {
            context.put("residenseAddress",
                    applicationForm.getResidenseAddress());
        }

        context.put("residenseAddrOtherDetails",
                getResidenseAddressOtherDeatils(applicationForm));

        // merchant name as a dealer name.
        if (StringUtils.isNotBlank(applicationForm.getDealerName())) {
            context.put("merchantName", applicationForm.getDealerName()
                    .toUpperCase());// string
        }

        // Loan amount is the principal amount.
        if (StringUtils.isNotBlank(String.valueOf(applicationForm.getLoanAmount()))) {
            context.put("principalAmount",
                    String.valueOf(applicationForm.getLoanAmount()));
        }
        // principal amount in the word format.
        if (StringUtils.isNotBlank(String.valueOf(applicationForm.getLoanAmount()))) {

            try {
                context.put("principalAmountInWords", GngNumUtil
                        .convert(Double.valueOf(applicationForm.getLoanAmount()).longValue()));
            } catch (Exception e) {
                logger.error("Exception occur at the time of converting number to words with probable cause {} ",e.getMessage());
            }
        }

        // revised principal amount(revised loan amount )
        if (StringUtils.isNotBlank(applicationForm.getRevisedLoanAmount())) {
            context.put("revisedPrincipalAmount", applicationForm.getRevisedLoanAmount());

        }

        // revised principal amount(revised loan amount ) in the word format.
        if (StringUtils.isNotBlank(applicationForm.getRevisedLoanAmount())) {

            try {

                context.put("revisedPrincipalAmtInWords", GngNumUtil.convert(new Double(applicationForm.getRevisedLoanAmount()).longValue()));

            }catch (Exception e) {
                logger.error("Exception occur at the time of converting number to words with probable cause {} ",e.getMessage());
            }

        }
        // model number is the product model

        context.put("productModelDetails", getModeldetails(applicationForm));// string

        if (StringUtils.isNotBlank(applicationForm.getTenure())) {
            context.put("tenure", applicationForm.getTenure());
        }
        // EMI in rs is the AdvanceEMI
        double emiAmount = applicationForm.getEmi();
        context.put("EMIInRs", String.format("%.2f",emiAmount));

        if (StringUtils.isNotBlank(applicationForm.getAdvanceEMITenor())) {
            context.put("noOfAdvanceInstallmentEMI",
                    applicationForm.getAdvanceEMITenor());
        }

        if (StringUtils.isNotBlank(applicationForm.getProceFees())) {
            context.put("processingFees", applicationForm.getProceFees());
        }

        // commented because of bug reported by hdbfs
       /*if (StringUtils.isNotBlank(applicationForm.getApplicantPhoto())) {
            context.put("applicantPhoto", applicationForm.getApplicantPhoto());
        }*/

        // application Date
        if (StringUtils.isNotBlank(applicationForm.getApplicationDate())) {
            context.put("applicationDate", applicationForm.getApplicationDate());// list
        }

        if (StringUtils.isNotBlank(applicationForm.getDateOfApplication())) {
            list = getCharacterList(applicationForm.getDateOfApplication());
            context.put("dateOfApplicationList", list);
        }
        // date of application in specific format
        if (StringUtils.isNotBlank(applicationForm.getDateOfApplication())) {

            context.put("dateOfApplicationInFormat",
                    formatDate(applicationForm.getDateOfApplication()));
        }

        if (StringUtils.isNotBlank(applicationForm.getLogo())) {
            context.put("logo", applicationForm.getLogo());

        }

        if(StringUtils.isNotBlank(applicationForm.getRevisedEmiAmount())){
            context.put("revisedEMIInRs", applicationForm.getRevisedEmiAmount());
        }

        if (StringUtils.isNotBlank(applicationForm.getBankName())) {
            context.put("bankName", applicationForm.getBankName()); //string
        }
        if (StringUtils.isNotBlank(applicationForm.getAccountHolderName())) {
            context.put("accountHolderName", applicationForm.getAccountHolderName());//string
        }
        if(StringUtils.isNotBlank(applicationForm.getIfscCode())){
            list = GngUtils.getCharacterList(applicationForm.getIfscCode());
            context.put("ifscCodeList", list); //list
        }

        if (StringUtils.isNotBlank(applicationForm.getBankBranchName())) {
            context.put("branchName", applicationForm.getBankBranchName()); //string
        }
        if (StringUtils.isNotBlank(applicationForm.getCustomerID())) {
            list = GngUtils.getCharacterList(applicationForm.getCustomerID());// list
            context.put("customerIDList", list); //string
        }
        if (StringUtils.isNotBlank(applicationForm.getAccountNumber())) {
            list = GngUtils.getCharacterList(applicationForm.getAccountNumber());
            context.put("accountNumberList", list); //list
        }
        if(StringUtils.isNotBlank(applicationForm.getIsHdfcBankAccount())){
            context.put("isHdfcBankAccount", applicationForm.getIsHdfcBankAccount()); //list
        }

        if(StringUtils.isNotBlank(applicationForm.getBranchName())){
            context.put("branchName", applicationForm.getBranchName()); //list
        }

        //fields for tvs
        if(StringUtils.isNotBlank(applicationForm.getGngRefID())){
            context.put("prospectNo", applicationForm.getGngRefID()); //list
        }

        if(StringUtils.isNotBlank(applicationForm.getModel()) && StringUtils.isNotBlank(applicationForm.getManufacturer())){
            String productAndModelNo= applicationForm.getManufacturer()+' '+applicationForm.getModel();
            context.put("modelNo_Manfactr", productAndModelNo);// string
        }

        if (StringUtils.isNotBlank(applicationForm.getFinanceAmount())) {
            context.put("financeAmount", applicationForm.getFinanceAmount().toUpperCase());// string
        }

        if (StringUtils.isNotBlank(applicationForm.getFinanceAmount())) {
            try {

                context.put("financeAmountInWords", GngNumUtil.convert(new Double(applicationForm.getFinanceAmount()).longValue()));

            }catch (Exception e) {
                logger.error("Exception occur at the time of converting number to words with probable cause {} ",e.getMessage());
            }
        }
        if (StringUtils.isNotBlank(applicationForm.getTenure())) {
            context.put("tenure", applicationForm.getTenure().toUpperCase());// string
        }

        if(StringUtils.isNotBlank(applicationForm.getRateOfInterest())){
            context.put("rateOfInterest", applicationForm.getRateOfInterest());
        }

        if (StringUtils.isNotBlank(applicationForm.getOtherCharges())) {
            context.put("otherCharges", applicationForm.getOtherCharges().toUpperCase());// string
        }


        double totalEmi = applicationForm.getEmi() + applicationForm.getInsuranceEmi() + applicationForm.getEwEmi();
        context.put("ewInsrnceAssetEmi", String.format("%.2f",totalEmi));// string

        context.put("ewEmi",String.format("%.2f", applicationForm.getEwEmi()));// string

        context.put("insuranceEmi", String.format("%.2f", applicationForm.getInsuranceEmi()));// string

        if (StringUtils.isNotBlank(applicationForm.getBranchName())) {
            context.put("dealerPlace", applicationForm.getBranchName().toUpperCase());// string
        }

        if (StringUtils.isNotBlank(applicationForm.getFirstEmiDueDt())) {
            context.put("firstEmiDueDate", applicationForm.getFirstEmiDueDt());// string
        }
        if (StringUtils.isNotBlank(applicationForm.getLastEmiDueDate())) {
            context.put("lastEmiDueDate", applicationForm.getLastEmiDueDate());// string
        }

        return context;

    }


    private static String getResidenseAddressOtherDeatils(
            ApplicationForm applicationForm) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null != applicationForm.getResidenseState()) {
            stringBuilder.append(applicationForm.getResidenseState());
            stringBuilder.append(", ");
        }
        if (null != applicationForm.getResidenseCity()) {
            stringBuilder.append(applicationForm.getResidenseCity());
            stringBuilder.append(", ");
        }
        if (null != applicationForm.getResidensePin()) {
            stringBuilder.append(applicationForm.getResidensePin());
        }
        return stringBuilder.toString();
    }

    private static String getModeldetails(ApplicationForm applicationForm) {
        StringBuilder stringBuilder = new StringBuilder();

        if (null != applicationForm.getManufacturer()) {
            stringBuilder.append(applicationForm.getManufacturer());
            stringBuilder.append(", ");
        }

        if (null != applicationForm.getModel()) {
            stringBuilder.append(applicationForm.getModel());

        }
        return stringBuilder.toString();
    }

    private static List<Character> getCharacterList(String stringData) {
        List<Character> list = new ArrayList<Character>();
        for (char c : (stringData.toCharArray())) {
            list.add(c);
        }
        return list;
    }

    private static String formatDate(String date) throws ParseException {

        String initDateFormat = "ddMMyyyy";
        String endDateFormat = "dd/MM/yyyy";
        Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        String parsedDate = formatter.format(initDate);

        return parsedDate;
    }


}
