package com.softcell.gonogo.model.core.request.scoring;


public class HomePhone {

    private String phoneNumber;
    private String reportedDate;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    @Override
    public String toString() {
        return "HomePhone [phoneNumber=" + phoneNumber + ", reportedDate="
                + reportedDate + "]";
    }
}
