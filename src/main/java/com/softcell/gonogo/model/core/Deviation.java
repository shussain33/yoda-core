package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * Created by yogesh on 15/3/18.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Deviation {

    @JsonProperty("sCategory")
    private String category;

    @JsonProperty("sDeviation")
    private String deviation;

    @JsonProperty("sAuthority")
    private String authority;

    @JsonProperty("aAuthorities")
    private List<String> authorities;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sRaisedBy")
    private String raisedBy;

    @JsonProperty("dtRaisedDate")
    private Date raisedDate;

    @JsonProperty("sResolvedBy")
    private String resolvedBy;

    @JsonProperty("dtResolvedDate")
    private Date resolvedDate;

    @JsonProperty("sDeviationRemarks")
    private String deviationRemarks;

    @JsonProperty("sMitigationRemarks")
    private String mitigationRemarks;
}
