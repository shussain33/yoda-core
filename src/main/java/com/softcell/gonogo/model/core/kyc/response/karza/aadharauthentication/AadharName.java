package com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "matching-strategy",
        "name-value"
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AadharName {

    @JsonProperty("matching-strategy")
    public String matchingStrategy;

    @JsonProperty("name-value")
    public String nameValue;

}