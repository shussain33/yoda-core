package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author yogeshb Pojo For LastMonthIncome details
 */
@Document
public class LastMonthIncome extends AuditEntity {

    @JsonProperty("sMonthName")
    private String monthName;

    @JsonProperty("dMonthIncome")
    private double monthIncome;

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public double getMonthIncome() {
        return monthIncome;
    }

    public void setMonthIncome(double monthIncome) {
        this.monthIncome = monthIncome;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LastMonthIncome [monthName=");
        builder.append(monthName);
        builder.append(", monthIncome=");
        builder.append(monthIncome);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(monthIncome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((monthName == null) ? 0 : monthName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LastMonthIncome other = (LastMonthIncome) obj;
        if (Double.doubleToLongBits(monthIncome) != Double
                .doubleToLongBits(other.monthIncome))
            return false;
        if (monthName == null) {
            if (other.monthName != null)
                return false;
        } else if (!monthName.equals(other.monthName))
            return false;
        return true;
    }

}
