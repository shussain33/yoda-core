package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailSpamRecord {

    @JsonProperty("ip_blacklist")
    private Boolean ip_blacklist;

    @JsonProperty("ipBlacklist")
    private Boolean ipBlacklist;

    @JsonProperty("report_count")
    private long report_count;

    @JsonProperty("reportCount")
    private long reportCount;

    @JsonProperty("spam_email")
    private Boolean spam_email;

    @JsonProperty("spamEmail")
    private Boolean spamEmail;
}
