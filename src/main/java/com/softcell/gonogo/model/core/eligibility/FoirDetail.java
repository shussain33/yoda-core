package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 5/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FoirDetail {

    @JsonProperty("sCollateralType")
    public String collateralType;

    @JsonProperty("sOccupationType")
    public String occupationType;

    @JsonProperty("sPropertyType")
    public String propertyType;

    @JsonProperty("sPropertyValue")
    public String propertyValue;

    @JsonProperty("dEligibleAmount")
    public String eligibleAmount;

    @JsonProperty("dLtv")
    public double ltv;

    @JsonProperty("dDSCR")
    public double dscr;

    @JsonProperty("sApprovingAuthority")
    public String approvingAuthority;

    @JsonProperty("sIncomeMethod")
    public String incomeMethod;

    @JsonProperty("dCombinedLtv")
    public double combinedLtv;
}
