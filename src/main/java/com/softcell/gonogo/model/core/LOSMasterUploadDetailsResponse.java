package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.LosUtrDetailsMaster;

import java.util.List;

/**
 * Created by mahesh on 20/2/17.
 */
public class LOSMasterUploadDetailsResponse {

    @JsonProperty("iNoOfSucceedRecord")
    private int noOfSucceedRecord;

    @JsonProperty("iNoOfFailedRecord")
    private int noOfFailedRecord;

    @JsonProperty("sCsvFileStatus")
    private String csvFileStatus;

    @JsonProperty("sCsvFileErrorDesc")
    private String csvFileErrorDesc;

    @JsonProperty("aoLosDetails")
    List<LosUtrDetailsMaster> losDetailsList;

    public String getCsvFileErrorDesc() {
        return csvFileErrorDesc;
    }

    public void setCsvFileErrorDesc(String csvFileErrorDesc) {
        this.csvFileErrorDesc = csvFileErrorDesc;
    }

    public String getCsvFileStatus() {
        return csvFileStatus;
    }

    public void setCsvFileStatus(String csvFileStatus) {
        this.csvFileStatus = csvFileStatus;
    }

    public List<LosUtrDetailsMaster> getLosDetailsList() {
        return losDetailsList;
    }

    public void setLosDetailsList(List<LosUtrDetailsMaster> losDetailsList) {
        this.losDetailsList = losDetailsList;
    }

    public int getNoOfFailedRecord() {
        return noOfFailedRecord;
    }

    public void setNoOfFailedRecord(int noOfFailedRecord) {
        this.noOfFailedRecord = noOfFailedRecord;
    }

    public int getNoOfSucceedRecord() {
        return noOfSucceedRecord;
    }

    public void setNoOfSucceedRecord(int noOfSucceedRecord) {
        this.noOfSucceedRecord = noOfSucceedRecord;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LOSMasterUploadDetailsResponse{");
        sb.append("csvFileErrorDesc='").append(csvFileErrorDesc).append('\'');
        sb.append(", losDetailsList=").append(losDetailsList);
        sb.append(", noOfSucceedRecord=").append(noOfSucceedRecord);
        sb.append(", noOfFailedRecord=").append(noOfFailedRecord);
        sb.append(", csvFileStatus='").append(csvFileStatus).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
