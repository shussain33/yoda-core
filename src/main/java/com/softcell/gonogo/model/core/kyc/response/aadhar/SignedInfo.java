package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class SignedInfo {

	/**
	 * @author Deepak Bhargava
	 *
	 *
	 */
	@XmlElement(name="CanonicalizationMethod")
	private CanonicalizationMethod canMethod;
	@XmlElement(name="SignatureMethod")
	private SignatureMethod signMethod;
	@XmlElement(name="Reference")
	private Reference reference;
	
	public CanonicalizationMethod getCanMethod() {
		return canMethod;
	}
	public void setCanMethod(CanonicalizationMethod canMethod) {
		this.canMethod = canMethod;
	}
	public SignatureMethod getSignMethod() {
		return signMethod;
	}
	public void setSignMethod(SignatureMethod signMethod) {
		this.signMethod = signMethod;
	}
	public Reference getReference() {
		return reference;
	}
	public void setReference(Reference reference) {
		this.reference = reference;
	}
	@Override
	public String toString() {
		return "SignedInfo [canMethod=" + canMethod + ", signMethod="
				+ signMethod + ", reference=" + reference + "]";
	}
	
	
}
