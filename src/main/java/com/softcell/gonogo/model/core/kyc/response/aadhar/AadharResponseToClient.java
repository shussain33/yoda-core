/**
 * yogeshb5:00:11 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author yogeshb
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AadharResponseToClient {

    @JsonProperty("sTxnStatus")
    private String txnStatus;

    @JsonProperty("sUidaiStatus")
    private String uidaiStatus;

    @JsonProperty("aAckID")
    private String acknowledgementId;

    @JsonProperty("sResult")
    private String result;

    @JsonProperty("oKycResponse")
    private KycResponseDetails kycResponseDetails;

    @JsonProperty("oErr")
    private AadharError error;

    @JsonProperty("oErrorList")
    private List<String> errorList;

    @JsonProperty("sTranscationId")
    private String transactionId;

    public String getRawXmlResponse() {
        return rawXmlResponse;
    }

    public void setRawXmlResponse(String rawXmlResponse) {
        this.rawXmlResponse = rawXmlResponse;
    }

    @JsonProperty("sRawXmlResponse")
    private String rawXmlResponse;

    /**
     * @return the kycResponseDetails
     */
    public KycResponseDetails getKycResponseDetails() {
        return kycResponseDetails;
    }

    /**
     * @param kycResponseDetails the kycResponseDetails to set
     */
    public void setKycResponseDetails(KycResponseDetails kycResponseDetails) {
        this.kycResponseDetails = kycResponseDetails;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getUidaiStatus() {
        return uidaiStatus;
    }

    public void setUidaiStatus(String uidaiStatus) {
        this.uidaiStatus = uidaiStatus;
    }

    public String getAcknowledgementId() {
        return acknowledgementId;
    }

    public void setAcknowledgementId(String acknowledgementId) {
        this.acknowledgementId = acknowledgementId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public AadharError getError() {
        return error;
    }

    public void setError(AadharError error) {
        this.error = error;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public List<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<String> errorList) {
        this.errorList = errorList;
    }

    @Override

    public String toString() {
        final StringBuilder sb = new StringBuilder("AadharResponseToClient{");
        sb.append("txnStatus='").append(txnStatus).append('\'');
        sb.append(", uidaiStatus='").append(uidaiStatus).append('\'');
        sb.append(", acknowledgementId='").append(acknowledgementId).append('\'');
        sb.append(", result='").append(result).append('\'');
        sb.append(", kycResponseDetails=").append(kycResponseDetails);
        sb.append(", error=").append(error);
        sb.append(", transactionId='").append(transactionId).append('\'');
        sb.append(", rawXmlResponse='").append(rawXmlResponse).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AadharResponseToClient that = (AadharResponseToClient) o;

        if (txnStatus != null ? !txnStatus.equals(that.txnStatus) : that.txnStatus != null) return false;
        if (uidaiStatus != null ? !uidaiStatus.equals(that.uidaiStatus) : that.uidaiStatus != null) return false;
        if (acknowledgementId != null ? !acknowledgementId.equals(that.acknowledgementId) : that.acknowledgementId != null)
            return false;
        if (result != null ? !result.equals(that.result) : that.result != null) return false;
        if (kycResponseDetails != null ? !kycResponseDetails.equals(that.kycResponseDetails) : that.kycResponseDetails != null)
            return false;
        if (error != null ? !error.equals(that.error) : that.error != null) return false;
        if (transactionId != null ? !transactionId.equals(that.transactionId) : that.transactionId != null)
            return false;
        return rawXmlResponse != null ? rawXmlResponse.equals(that.rawXmlResponse) : that.rawXmlResponse == null;
    }

    @Override
    public int hashCode() {
        int result1 = txnStatus != null ? txnStatus.hashCode() : 0;
        result1 = 31 * result1 + (uidaiStatus != null ? uidaiStatus.hashCode() : 0);
        result1 = 31 * result1 + (acknowledgementId != null ? acknowledgementId.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (kycResponseDetails != null ? kycResponseDetails.hashCode() : 0);
        result1 = 31 * result1 + (error != null ? error.hashCode() : 0);
        result1 = 31 * result1 + (transactionId != null ? transactionId.hashCode() : 0);
        result1 = 31 * result1 + (rawXmlResponse != null ? rawXmlResponse.hashCode() : 0);
        return result1;
    }
}
