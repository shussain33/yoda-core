package com.softcell.gonogo.model.core.valuation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 28/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValuationRequest {
    @JsonProperty("oHeader")
    @NotNull(groups = {Header.InsertGrp.class, Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {ValuationRequest.InsertGrp.class,ValuationRequest.FetchGrp.class})
    public String refId;

    @JsonProperty("oValuation")
    @NotNull(groups = {Valuation.InsertGrp.class})
    private Valuation valuation;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
