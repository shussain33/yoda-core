package com.softcell.gonogo.model.core.request.scoring;


import java.util.List;

public class AdditionalMFIDetailsType {

    private String MFIClientFullname;
    private String MFIDOB;
    private String MFIGender;
    private MFIAdditionalIdentityInfoType MFIIdentification;
    private List<MFIAddlAdrsDetailsType> MFIAddress;
    private List<PhoneType> phone;
    private String memberId;
    private int id;

    public String getMFIClientFullname() {
        return MFIClientFullname;
    }

    public void setMFIClientFullname(String mFIClientFullname) {
        MFIClientFullname = mFIClientFullname;
    }

    public String getMFIDOB() {
        return MFIDOB;
    }

    public void setMFIDOB(String mFIDOB) {
        MFIDOB = mFIDOB;
    }

    public String getMFIGender() {
        return MFIGender;
    }

    public void setMFIGender(String mFIGender) {
        MFIGender = mFIGender;
    }

    public MFIAdditionalIdentityInfoType getMFIIdentification() {
        return MFIIdentification;
    }

    public void setMFIIdentification(MFIAdditionalIdentityInfoType mFIIdentification) {
        MFIIdentification = mFIIdentification;
    }

    public List<MFIAddlAdrsDetailsType> getMFIAddress() {
        return MFIAddress;
    }

    public void setMFIAddress(List<MFIAddlAdrsDetailsType> mFIAddress) {
        MFIAddress = mFIAddress;
    }

    public List<PhoneType> getPhone() {
        return phone;
    }

    public void setPhone(List<PhoneType> phone) {
        this.phone = phone;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AdditionalMFIDetailsType [MFIClientFullname="
                + MFIClientFullname + ", MFIDOB=" + MFIDOB + ", MFIGender="
                + MFIGender + ", MFIIdentification=" + MFIIdentification
                + ", MFIAddress=" + MFIAddress + ", phone=" + phone
                + ", memberId=" + memberId + ", id=" + id + "]";
    }
}
