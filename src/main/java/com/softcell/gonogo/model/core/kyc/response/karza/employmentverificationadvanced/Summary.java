package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Summary {

    @JsonProperty("emailParameters")
    private EmailParameters emailParameters;

    @JsonProperty("emailValid")
    private boolean emailValid;

    @JsonProperty("lastEmployer")
    private LastEmployer lastEmployer;

    @JsonProperty("nameLookup")
    private NameLookup nameLookup;

    @JsonProperty("totalWorkExperienceInMonths")
    private int totalWorkExperienceInMonths;

    @JsonProperty("uanLookup")
    private UanLookup uanLookup;

    @JsonProperty("waiveFi")
    private boolean waiveFi;
}
