/**
 * Administrator5:19:46 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Administrator
 *
 */
public class SENPProfileIncome implements Serializable {
    @JsonProperty("sYr")
    private String year;

    @JsonProperty("sItrDt")
    private String itrDate;

    @JsonProperty("dTrnOvr")
    private double turnOver;

    @JsonProperty("dDprciatn")
    private double depreciation;

    @JsonProperty("dNtPrfit")
    private double netProfit;

    @JsonProperty("dOthrIncm")
    private double otherIncome;

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year
     *            the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the itrDate
     */
    public String getItrDate() {
        return itrDate;
    }

    /**
     * @param itrDate
     *            the itrDate to set
     */
    public void setItrDate(String itrDate) {
        this.itrDate = itrDate;
    }

    /**
     * @return the turnOver
     */
    public double getTurnOver() {
        return turnOver;
    }

    /**
     * @param turnOver
     *            the turnOver to set
     */
    public void setTurnOver(double turnOver) {
        this.turnOver = turnOver;
    }

    /**
     * @return the depreciation
     */
    public double getDepreciation() {
        return depreciation;
    }

    /**
     * @param depreciation
     *            the depreciation to set
     */
    public void setDepreciation(double depreciation) {
        this.depreciation = depreciation;
    }

    /**
     * @return the netProfit
     */
    public double getNetProfit() {
        return netProfit;
    }

    /**
     * @param netProfit
     *            the netProfit to set
     */
    public void setNetProfit(double netProfit) {
        this.netProfit = netProfit;
    }

    /**
     * @return the otherIncome
     */
    public double getOtherIncome() {
        return otherIncome;
    }

    /**
     * @param otherIncome
     *            the otherIncome to set
     */
    public void setOtherIncome(double otherIncome) {
        this.otherIncome = otherIncome;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SENPProfileIncome [year=");
        builder.append(year);
        builder.append(", itrDate=");
        builder.append(itrDate);
        builder.append(", turnOver=");
        builder.append(turnOver);
        builder.append(", depreciation=");
        builder.append(depreciation);
        builder.append(", netProfit=");
        builder.append(netProfit);
        builder.append(", otherIncome=");
        builder.append(otherIncome);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(depreciation);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((itrDate == null) ? 0 : itrDate.hashCode());
        temp = Double.doubleToLongBits(netProfit);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherIncome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(turnOver);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SENPProfileIncome other = (SENPProfileIncome) obj;
        if (Double.doubleToLongBits(depreciation) != Double
                .doubleToLongBits(other.depreciation))
            return false;
        if (itrDate == null) {
            if (other.itrDate != null)
                return false;
        } else if (!itrDate.equals(other.itrDate))
            return false;
        if (Double.doubleToLongBits(netProfit) != Double
                .doubleToLongBits(other.netProfit))
            return false;
        if (Double.doubleToLongBits(otherIncome) != Double
                .doubleToLongBits(other.otherIncome))
            return false;
        if (Double.doubleToLongBits(turnOver) != Double
                .doubleToLongBits(other.turnOver))
            return false;
        if (year == null) {
            if (other.year != null)
                return false;
        } else if (!year.equals(other.year))
            return false;
        return true;
    }

}
