package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailAdditionalInfo {

    @JsonProperty("company_info")
    private EmailCompanyInfo company_info;

    @JsonProperty("companyInfo")
    private EmailCompanyInfo companyInfo;

    @JsonProperty("individual_match")
    private List<EmailIndividualMatch> individual_match;

    @JsonProperty("individualMatch")
    private List<EmailIndividualMatch> individualMatch;

    @JsonProperty("spam_record")
    private EmailSpamRecord spam_record;

    @JsonProperty("spamRecord")
    private EmailSpamRecord spamRecord;

    @JsonProperty("whois_info")
    private EmailWhoIsInfo whois_info;

    @JsonProperty("whoisInfo")
    private EmailWhoIsInfo whoisInfo;

}
