package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityByITR {

    @JsonProperty("dBusinessIncome")
    private double businessIncome;

    @JsonProperty("dTax")
    private double tax;

    @JsonProperty("dNetIncome")
    private double netIncome;

    @JsonProperty("dOtherIncome")
    private double otherIncome;

    @JsonProperty("dGrossIncome")
    private double grossIncome;

    @JsonProperty("dFixedObligation")
    private double fixedObligation;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("dRoi")
    private double roi;

    @JsonProperty("dEmi")
    private double emi;
}
