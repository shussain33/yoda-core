package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

/**
 * Created by suhasini on 23/2/18.
 */

public class ApplicationVerification{

    @Id
    @JsonProperty("sRefId")
    @NotEmpty(groups = {ApplicationVerification.InsertGrp.class,ApplicationVerification.FetchGrp.class})
    protected String refId;

    @JsonProperty("sInstID")
    protected String institutionId;

    @JsonProperty("sApplicantID")
    protected String applicantId;

    @JsonProperty("oApplicantName")
    protected Name applicantName;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }

    public String getRefId() {
        return refId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public Name getApplicantName() {
        return applicantName;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public void setApplicantName(Name applicantName) {
        this.applicantName = applicantName;
    }
}
