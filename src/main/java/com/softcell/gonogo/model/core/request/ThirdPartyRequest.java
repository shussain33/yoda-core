package com.softcell.gonogo.model.core.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg222 on 23/3/19.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThirdPartyRequest {
    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sType")
    private String type ;

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;
}
