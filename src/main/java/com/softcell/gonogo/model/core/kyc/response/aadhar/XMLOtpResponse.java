package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="OtpRes")
@XmlAccessorType(XmlAccessType.FIELD)
public class XMLOtpResponse {

    @XmlAttribute(name="ret")
    private String ret;
    @XmlAttribute(name="code")
    private String code;
    @XmlAttribute(name="txn")
    private String txn;
    @XmlAttribute(name="err")
    private String err;
    @XmlAttribute(name="ts")
    private String ts;
    @XmlAttribute(name="info")
    private String info;

    public String getRet() {
        return ret;
    }
    public void setRet(String ret) {
        this.ret = ret;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getTxn() {
        return txn;
    }
    public void setTxn(String txn) {
        this.txn = txn;
    }
    public String getErr() {
        return err;
    }
    public void setErr(String err) {
        this.err = err;
    }
    public String getTs() {
        return ts;
    }
    public void setTs(String ts) {
        this.ts = ts;
    }
    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "OtpResponse [ret=" + ret + ", code=" + code + ", txn=" + txn
                + ", err=" + err + ", ts=" + ts + ", info=" + info + "]";
    }

}