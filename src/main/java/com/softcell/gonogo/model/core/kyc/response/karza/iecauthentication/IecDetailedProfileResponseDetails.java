package com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IecDetailedProfileResponseDetails {

    @JsonProperty("file_number")
    private String file_number;

    @JsonProperty("bin_pan_extension")
    private String bin_pan_extension;

    @JsonProperty("ie_code")
    private String ie_code;

    @JsonProperty("iec_status")
    private String iec_status;

    @JsonProperty("no_of_branches")
    private String no_of_branches;

    @JsonProperty("registration_details")
    public List<IecRegistrationDetails> iecRegistrationDetailsList;

    @JsonProperty("nature_of_concern")
    private String nature_of_concern;

    @JsonProperty("del_status")
    private String del_status;

    @JsonProperty("directors")
    public List<IecDirectorDetails> iecDirectorDetailList;

    @JsonProperty("party_name_and_address")
    private String party_name_and_address;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("pan_issued_by")
    private String pan_issued_by;

    @JsonProperty("date_of_establishment")
    private String date_of_establishment;

    @JsonProperty("address")
    private String address;

    @JsonProperty("iec_allotment_date")
    private String iec_allotment_date;

    @JsonProperty("branches")
    public List<IecBrancheDetails> iecBrancheDetailList;

    @JsonProperty("name")
    private String name;

    @JsonProperty("e_mail")
    private String e_mail;

    @JsonProperty("file_date")
    private String file_date;

    @JsonProperty("phone_no")
    private String phone_no;

    @JsonProperty("exporter_type")
    private String exporter_type;

    @JsonProperty("pan_issue_date")
    private String pan_issue_date;

    @JsonProperty("bank_details")
    public IecBankDetails iecBankDetailList;

    @JsonProperty("rcmc_details")
    public List<IecRcmcDetails> iecRCmcDetailList;

}