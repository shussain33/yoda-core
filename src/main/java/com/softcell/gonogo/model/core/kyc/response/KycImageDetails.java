package com.softcell.gonogo.model.core.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yogeshb
 */
public class KycImageDetails {
    @JsonProperty("sApplID")
    private String applicantId;

    @JsonProperty("aImgMap")
    private List<ImagesDetails> imageMap = new ArrayList<ImagesDetails>();

    @JsonProperty("sImageBlock")
    private String imageBlock;

    public String getImageBlock() {
        return imageBlock;
    }

    public void setImageBlock(String imageBlock) {
        this.imageBlock = imageBlock;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public List<ImagesDetails> getImageMap() {
        return imageMap;
    }

    public void setImageMap(List<ImagesDetails> imageMap) {
        this.imageMap = imageMap;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("KycImageDetails [applicantId=");
        builder.append(applicantId);
        builder.append(", imageMap=");
        builder.append(imageMap);
        builder.append(", imageBlock=");
        builder.append(imageBlock);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((applicantId == null) ? 0 : applicantId.hashCode());
        result = prime * result
                + ((imageBlock == null) ? 0 : imageBlock.hashCode());
        result = prime * result
                + ((imageMap == null) ? 0 : imageMap.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof KycImageDetails))
            return false;
        KycImageDetails other = (KycImageDetails) obj;
        if (applicantId == null) {
            if (other.applicantId != null)
                return false;
        } else if (!applicantId.equals(other.applicantId))
            return false;
        if (imageBlock == null) {
            if (other.imageBlock != null)
                return false;
        } else if (!imageBlock.equals(other.imageBlock))
            return false;
        if (imageMap == null) {
            if (other.imageMap != null)
                return false;
        } else if (!imageMap.equals(other.imageMap))
            return false;
        return true;
    }

}
