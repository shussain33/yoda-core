package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by yogesh Khandare on 7/10/18.
 */
@Data
public class Error {

    @JsonProperty("message")
    private String message;
}
