package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by suhasini on 22/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PropertyVerificationOutput {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("bAddressConfirmed")
    private boolean addressConfirmed;

    @JsonProperty("sContactedTo")
    private String contactedTo;

    // For office it is "Office here since"
    // For residence it is "Living here since"
    @JsonProperty("iOccupiedSince")
    private int occupiedSince;

    @JsonProperty("iNoOfStoreys")
    private int noOfStoreys;

    @JsonProperty("bNamePlateVisible")
    private boolean namePlateVisible;

    // for Residence
    @JsonProperty("iNoOfFamilyMembers")
    private int noOfFamilyMembers;

    // for Residence
    @JsonProperty("iNoOfEarningMembers")
    private int noOfEarningMembers;

    // for office
    @JsonProperty("sOwnershipType")
    private String ownershipType;

    // for office
    @JsonProperty("iNoOfEmployees")
    private int noOfEmployees;

    @JsonProperty("bNeighbourCheck")
    private boolean neighbourCheck;

    @JsonProperty("sAccessability")
    private String accessability;

    @JsonProperty("sIdVerificationDocuments")
    private String idVerificationDocuments;

    @JsonProperty("bSameAddressAsMentioned")
    private boolean sameAddressAsMentioned;

    @JsonProperty("sEarningMemberRelationWithApplicant")
    private String earningMemberRelationWithApplicant;

    @JsonProperty("dOtherMembersMonthlyIncome")
    private double otherMembersMonthlyIncome;

    @JsonProperty("sOwnershipInNameOf")
    private String ownershipInNameOf;

    // for office
    @JsonProperty("bStockSeen")
    private boolean stockSeen;

    @JsonProperty("dMonthlyRentAmount")
    private double monthlyRentAmount;

    @JsonProperty("bNegativeArea")
    private boolean negativeArea;

    @JsonProperty("bCommunityDominated")
    private boolean communityDominated;

    @JsonProperty("fDistanceFromNearestBranch")
    private float distanceFromNearestBranch;

    @JsonProperty("sInternalDedupeChkStatus")
    private String internalDedupeChkStatus;

    @JsonProperty("sAnyConcerningIssue")
    private String anyConcerningIssue;

    @JsonProperty("sCPVObservation")
    private String cpvObservation;

    @JsonProperty("sCPVDoneBy")
    private String cpvDoneBy;

    @JsonProperty("sCPVReviewedBy")
    private String cpvReviewedBy;

    @JsonProperty("dtInitiateDate")
    private Date initiateDate;

    @JsonProperty("dtCompletionDate")
    private Date completionDate;

    @JsonProperty("oRemarks")
    private Remark remarks;

    @JsonProperty("sDifferentAddress")
    private String differentAddress;

    @JsonProperty("bEmplmConfirmed")
    private boolean emplmConfirmed;

    @JsonProperty("bIsSetupProper")
    private boolean isSetupProper;

    @JsonProperty("bSignBoardVisi")
    private boolean signBoardVisible;

    @JsonProperty("bRecepSetup")
   private boolean recepSetup;

    //adding for pl
    @JsonProperty("sDistanceFromBranch")
     private String distanceFromBranch;
}
