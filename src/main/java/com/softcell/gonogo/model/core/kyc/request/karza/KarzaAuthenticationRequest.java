package com.softcell.gonogo.model.core.kyc.request.karza;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KarzaAuthenticationRequest {

    @JsonProperty("data")
    @NotNull
    private KarzaRequestData data;

    @JsonProperty("requestType")
    @NotNull
    private String requestType;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;
}
