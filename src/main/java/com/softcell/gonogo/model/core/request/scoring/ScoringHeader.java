package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ScoringHeader implements Serializable{

    @JsonProperty("APPLICATION-ID")
    private String applicationId;

    @JsonProperty("CUSTOMER-ID")
    private String customerId;

    @JsonProperty("INSTITUTION-ID")
    private String institutionId;

    @JsonProperty("REQUEST-TIME")
    private String requestTime;

    @JsonProperty("REQUEST-TYPE")
    private String requestType;

    @JsonProperty("APP-TYPE")
    private String appType;

    /*
    * new field for applicant level scoring*/

    @JsonProperty("CLIENT-ID")
    private String clientId;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "ScoringHeader [applicationId=" + applicationId
                + ", customerId=" + customerId + ", institutionId="
                + institutionId + ", requestTime=" + requestTime
                + ", requestType=" + requestType + ", appType=" + appType + "]";
    }


}
