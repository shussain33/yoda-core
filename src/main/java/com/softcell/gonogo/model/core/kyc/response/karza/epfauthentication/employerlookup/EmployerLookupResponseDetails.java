package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.employerlookup;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.KarzaResultDetails;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmployerLookupResponseDetails {

    @JsonProperty("result")
    private List<KarzaResultDetails> resultDetailsList;

}