package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailParameters {

    @JsonProperty("alerts")
    private boolean alerts;

    @JsonProperty("isValid")
    private String isValid;

    @JsonProperty("organization")
    private Organization organization;

    @JsonProperty("person")
    private Person person;

    @JsonProperty("emailValid")
    private boolean emailValid;

}
