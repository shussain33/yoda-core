package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AadhaarHeader {

    /**
     * unique applicationId for Pan Service and AADHAR
     */
    @JsonProperty("APPLICATION-ID")
    private String applicationId;
    /**
     * requestType should be REQUEST
     */
    @JsonProperty("REQUEST-TYPE")
    private String requestType;
    /**
     * date format(ddMMyyyy HHmmss)
     */
    @JsonProperty("REQUEST-TIME")
    private String requestTime;

    @JsonProperty("KYC-TYPE")
    private String kycType;/*OTP*/

    @JsonProperty("VERSION")
    private String version;/*1.6*/

    @JsonProperty("AADHAAR-NUMBER-0")
    private String aadhaarNumber0;/*1st aadhaar number*/

    @JsonProperty("AADHAAR-NUMBER-1")
    private String aadhaarNumber1;/*2nd aadhaar number*/

    @JsonProperty("TRANSACTION-IDENTIFIER")
    private String transactionIdentifier;/*UKC:ABC:12345*/

    @JsonProperty("AUA-CODE")
    private String auaCode;/*AUACode*/

    @JsonProperty("SUB-AUA-CODE")
    private String subAuaCode;/*SubAUACode*/

    @JsonProperty("TERMINAL-ID")
    private String terminalId;/*public*/

    @JsonProperty("ASA-SIGN-REQD")
    private String asaSignReqd;/*Y*/

}
