package com.softcell.gonogo.model.core.kyc.response.karza.addressmappingauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AddressMatchingResponseDetails {

    @JsonProperty("score")
    private String score;

    @JsonProperty("match")
    private String match;

    @JsonProperty("address1")
    private Address1MatchingResponseDetails address1MatchingDetails;

    @JsonProperty("address2")
    private Address2MatchingResponseDetails address2MatchingDetails;

}