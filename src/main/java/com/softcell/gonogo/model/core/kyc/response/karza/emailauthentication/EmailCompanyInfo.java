package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailCompanyInfo {

    @JsonProperty("org_domain_match")
    private List<EmailCompanyInfoOrgDomainMatch> org_domain_match;

    @JsonProperty("orgDomainMatch")
    private List<EmailCompanyInfoOrgDomainMatch> orgDomainMatch;

    @JsonProperty("org_email_match")
    private List<EmailCompanyInfoOrgEmailMatch> org_email_match;

    @JsonProperty("orgEmailMatch")
    private List<EmailCompanyInfoOrgEmailMatch> orgEmailMatch;

    @JsonProperty("usr_director_match")
    private List<EmailCompanyInfoUsrDirectorMatch> usr_director_match;

    @JsonProperty("usrDirectorMatch")
    private List<EmailCompanyInfoUsrDirectorMatch> usrDirectorMatch;
}
