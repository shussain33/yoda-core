package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CheckOAuthTokenRequest {

    @JsonProperty("sToken")
    @NotBlank(message = "Token must not be blank !!")
    private String token;

    @JsonProperty("bUserDetails")
    @NotNull(message = "User must not be blank !!")
    private Boolean userDetails;

    @NotBlank(message = "InstitutionName must not be blank !!")
    @JsonProperty("sInstitutionName")
    private String institutionName;

    @NotBlank(message = "Application must not be blank !!")
    @JsonProperty("sApplication")
    private String application;
}
