package com.softcell.gonogo.model.core.kyc.response.karza.websiteauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class WebsiteDomainAuthenticationResponseDetails {

    @JsonProperty("domain")
    private WebsiteDomainDetails websiteDomainDetails;

    @JsonProperty("update_date")
    private String update_date;

    @JsonProperty("expiration_date")
    private String expiration_date;

    @JsonProperty("creation_date")
    private String creation_date;

    @JsonProperty("admin")
    private WebsiteAdminDetails websiteAdminDetails;

    @JsonProperty("tech")
    private WebsiteTechDetails websiteTechDetails;

    @JsonProperty("registry")
    private RegistryDetails registryDetails;

    @JsonProperty("registrar")
    private RegistrarDetails registrarDetails;

    @JsonProperty("nameserver")
    private NameServerDetails nameServerDetails;

    @JsonProperty("registrant")
    private RegistrantDetails registrantDetails;

}