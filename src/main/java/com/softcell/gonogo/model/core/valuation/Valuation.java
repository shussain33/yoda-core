package com.softcell.gonogo.model.core.valuation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by suhasini on 5/3/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "valuationDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Valuation {

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstitutionId")
    public String institutionId;

    @JsonProperty("aValuationDetails")
    public List<ValuationDetails> valuationDetailsList;

    public interface InsertGrp {

    }
}
