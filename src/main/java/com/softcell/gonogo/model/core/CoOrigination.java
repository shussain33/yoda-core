package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by ssg222 on 28/12/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
@Document(collection = "coOriginationDetails")
public class CoOrigination {

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstitutionId")
    public String institutionId;

    @JsonProperty("aCoOriginationDetails")
    private List<CoOriginationDetails> coOriginationDetailsList;

    @JsonProperty("oRoiMasterData")
    private CoOriginationDetails roiMasterdata;

    @JsonProperty("oMisReleatedData")
    private MisReleatedData misReleatedData;

    @JsonProperty("bPopulate")
    private  boolean populate;
}
