package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DeviceDetails {

    @JsonProperty("3g_support")
    private String support_3g;

    @JsonProperty("device_activation_date")
    private String device_activation_date;

    @JsonProperty("imei")
    private String imei;

    @JsonProperty("model")
    private String model;

}