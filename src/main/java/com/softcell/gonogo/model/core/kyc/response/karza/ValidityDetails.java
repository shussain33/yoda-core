package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ValidityDetails {

    @JsonProperty("transport")
    private String transport;

    @JsonProperty("non-transport")
    private String nonTransport;

}
