package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by mahesh on 19/9/17.
 */
@Data
public class DoNotRaisedApplicationCountResponse {

    @JsonProperty("sApplicationCount")
    private int applicationCount;

    @JsonProperty("aApplicationRefId")
    private List<String> appplicationRefId;


}
