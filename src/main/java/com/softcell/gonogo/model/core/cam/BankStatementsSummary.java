package com.softcell.gonogo.model.core.cam;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 6/2/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BankStatementsSummary {
    @JsonProperty("dCredit")
    private double creditValue;

    @JsonProperty("dTotalInward")
    private double totalInward;

    @JsonProperty("dTotalOutward")
    private double totalOutward;

    @JsonProperty("dInwardReturn")
    private double inWardReturn;

    @JsonProperty("dOutwardReturn")
    private double outWardReturn;

    @JsonProperty("dDebit")
    private double debitValue;
}
