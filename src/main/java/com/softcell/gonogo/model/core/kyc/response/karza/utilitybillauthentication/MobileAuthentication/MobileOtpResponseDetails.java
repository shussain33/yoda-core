package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MobileOtpResponseDetails {

    @JsonProperty("message")
    private String message;

    @JsonProperty("request_id")
    private String request_id;

}