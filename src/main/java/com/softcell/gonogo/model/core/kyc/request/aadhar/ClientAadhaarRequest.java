/**
 * yogeshb10:53:22 am  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Transient;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientAadhaarRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sMobNumber")
    private String mobileNumber;

    @JsonProperty("sAadharNumber")
    @NotBlank(groups = {ClientAadhaarRequest.OtpGrp.class,ClientAadhaarRequest.OtpVerificationGrp.class,
            ClientAadhaarRequest.IrisVerificationGrp.class, ClientAadhaarRequest.OtpVerificationGrpV2_1.class})
    @Pattern(regexp = "(^$|[0-9]{12})", groups = {ClientAadhaarRequest.OtpGrp.class,ClientAadhaarRequest.OtpVerificationGrp.class,
            ClientAadhaarRequest.IrisVerificationGrp.class, ClientAadhaarRequest.OtpVerificationGrpV2_1.class},
            message = "must be valid Aadhaar Number")
    private String aadharNumber;

    @JsonProperty("oCustName")
    private Name name;

    @JsonProperty("sGender")
    private String gender;

    @JsonProperty("sDob")
    private String dateOfBirth;

    @JsonProperty("iAge")
    private int age;

    @JsonProperty("sOtp")
    @NotBlank(groups = {ClientAadhaarRequest.OtpVerificationGrp.class, ClientAadhaarRequest.OtpVerificationGrpV2_1.class})
    @Transient
    private String otp;

    @JsonProperty("aBios")
    private List<BiometricType> bios;

    /**
     * newly added for pid block request.
     */
    @JsonProperty("sHMac")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp.class})
    @Transient
    private String hMac;

    @JsonProperty("oSessionKey")
    @NotNull(groups = {ClientAadhaarRequest.IrisVerificationGrp.class})
    @Valid
    @Transient
    private SessionKey sessionKey;

    @JsonProperty("sEncPidBlk")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp.class})
    @Transient
    private String encryptedPIDBlock;

    @JsonProperty("dtTimeStamp")
    @NotNull(groups = {ClientAadhaarRequest.IrisVerificationGrp.class})
    private Date timeStamp;

    @JsonProperty("sIrisDvceCode")
    private String irisDeviceCode;

    @JsonProperty("sTrmDvcCode")
    private String terminalDeviceCode;

    @JsonProperty("sDpId")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp2_1.class})
    private String dpId;

    @JsonProperty("sRdsId")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp2_1.class})
    private String rdsId;

    @JsonProperty("sRdsVer")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp2_1.class})
    private String rdsVer;

    @JsonProperty("sDc")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp2_1.class})
    private String dc;

    @JsonProperty("sMi")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp2_1.class})
    private String mi;

    @JsonProperty("sMc")
    @NotBlank(groups = {ClientAadhaarRequest.IrisVerificationGrp2_1.class})
    private String mc;

    @JsonProperty("sTranscationId")
    @NotBlank(groups = {ClientAadhaarRequest.OtpVerificationGrpV2_1.class})
    private String transactionId;


    /**
     *
     * @return
     */
    public String getIrisDeviceCode() {
        return irisDeviceCode;
    }

    /**
     *
     * @param irisDeviceCode
     */
    public void setIrisDeviceCode(String irisDeviceCode) {
        this.irisDeviceCode = irisDeviceCode;
    }

    /**
     *
     * @return
     */
    public String getTerminalDeviceCode() {
        return terminalDeviceCode;
    }

    /**
     *
     * @param terminalDeviceCode
     */
    public void setTerminalDeviceCode(String terminalDeviceCode) {
        this.terminalDeviceCode = terminalDeviceCode;
    }

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the aadharNumber
     */
    public String getAadharNumber() {
        return aadharNumber;
    }

    /**
     * @param aadharNumber the aadharNumber to set
     */
    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    /**
     * @return the name
     */
    public Name getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(Name name) {
        this.name = name;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the otp
     */
    public String getOtp() {
        return otp;
    }

    /**
     * @param otp the otp to set
     */
    public void setOtp(String otp) {
        this.otp = otp;
    }

    /**
     * @return the bios
     */
    public List<BiometricType> getBios() {
        return bios;
    }

    /**
     * @param bios the bios to set
     */
    public void setBios(List<BiometricType> bios) {
        this.bios = bios;
    }

    public String gethMac() {
        return hMac;
    }

    public void sethMac(String hMac) {
        this.hMac = hMac;
    }

    public SessionKey getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(SessionKey sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getEncryptedPIDBlock() {
        return encryptedPIDBlock;
    }

    public void setEncryptedPIDBlock(String encryptedPIDBlock) {
        this.encryptedPIDBlock = encryptedPIDBlock;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDpId() {
        return dpId;
    }

    public void setDpId(String dpId) {
        this.dpId = dpId;
    }

    public String getRdsId() {
        return rdsId;
    }

    public void setRdsId(String rdsId) {
        this.rdsId = rdsId;
    }

    public String getRdsVer() {
        return rdsVer;
    }

    public void setRdsVer(String rdsVer) {
        this.rdsVer = rdsVer;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public String getMi() {
        return mi;
    }

    public void setMi(String mi) {
        this.mi = mi;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ClientAadhaarRequest{");
        sb.append("header=").append(header);
        sb.append(", mobileNumber='").append(mobileNumber).append('\'');
        sb.append(", aadharNumber='").append(aadharNumber).append('\'');
        sb.append(", name=").append(name);
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", age=").append(age);
        sb.append(", otp='").append(otp).append('\'');
        sb.append(", bios=").append(bios);
        sb.append(", hMac='").append(hMac).append('\'');
        sb.append(", sessionKey=").append(sessionKey);
        sb.append(", encryptedPIDBlock='").append(encryptedPIDBlock).append('\'');
        sb.append(", timeStamp=").append(timeStamp);
        sb.append(", irisDeviceCode='").append(irisDeviceCode).append('\'');
        sb.append(", terminalDeviceCode='").append(terminalDeviceCode).append('\'');
        sb.append(", dpId='").append(dpId).append('\'');
        sb.append(", rdsId='").append(rdsId).append('\'');
        sb.append(", rdsVer='").append(rdsVer).append('\'');
        sb.append(", dc='").append(dc).append('\'');
        sb.append(", mi='").append(mi).append('\'');
        sb.append(", mc='").append(mc).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientAadhaarRequest that = (ClientAadhaarRequest) o;
        return Objects.equals(age, that.age) &&
                Objects.equals(header, that.header) &&
                Objects.equals(mobileNumber, that.mobileNumber) &&
                Objects.equals(aadharNumber, that.aadharNumber) &&
                Objects.equals(name, that.name) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(dateOfBirth, that.dateOfBirth) &&
                Objects.equals(otp, that.otp) &&
                Objects.equals(bios, that.bios) &&
                Objects.equals(hMac, that.hMac) &&
                Objects.equals(sessionKey, that.sessionKey) &&
                Objects.equals(encryptedPIDBlock, that.encryptedPIDBlock) &&
                Objects.equals(timeStamp, that.timeStamp) &&
                Objects.equals(irisDeviceCode, that.irisDeviceCode) &&
                Objects.equals(terminalDeviceCode, that.terminalDeviceCode) &&
                Objects.equals(dpId, that.dpId) &&
                Objects.equals(rdsId, that.rdsId) &&
                Objects.equals(rdsVer, that.rdsVer) &&
                Objects.equals(dc, that.dc) &&
                Objects.equals(mi, that.mi) &&
                Objects.equals(mc, that.mc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, mobileNumber, aadharNumber, name, gender, dateOfBirth, age, otp, bios, hMac, sessionKey, encryptedPIDBlock, timeStamp, irisDeviceCode, terminalDeviceCode, dpId, rdsId, rdsVer, dc, mi, mc);
    }

    public interface OtpGrp{}
    public interface OtpVerificationGrp{}
    public interface OtpVerificationGrpV2_1{}
    public interface IrisVerificationGrp{}
    public interface IrisVerificationGrp2_1{}
}