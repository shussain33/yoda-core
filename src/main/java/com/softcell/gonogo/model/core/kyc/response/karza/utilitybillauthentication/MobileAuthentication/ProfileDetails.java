package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ProfileDetails {

    @JsonProperty("education")
    private String education;

    @JsonProperty("language")
    private String language;

    @JsonProperty("marital_status")
    private String marital_status;

    @JsonProperty("occupation")
    private String occupation;

    @JsonProperty("relationships")
    private String relationships;

    @JsonProperty("workplace")
    private String workplace;

}