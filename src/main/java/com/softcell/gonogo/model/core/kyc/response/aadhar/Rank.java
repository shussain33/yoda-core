package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class Rank {
    @JsonProperty("pos")
    private String pos;

    @JsonProperty("clr")
    private String clr;

    @JsonProperty("value")
    private String value;

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getClr() {
        return clr;
    }

    public void setClr(String clr) {
        this.clr = clr;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Rank [pos=" + pos + ", clr=" + clr + ", value=" + value + "]";
    }
}
