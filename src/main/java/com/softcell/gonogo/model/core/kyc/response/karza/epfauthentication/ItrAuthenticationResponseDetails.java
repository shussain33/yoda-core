package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ItrAuthenticationResponseDetails {

    @JsonProperty("status")
    private String status;

    @JsonProperty("validity")
    private String validity;
}