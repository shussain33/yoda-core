package com.softcell.gonogo.model.core.kyc.response.pan;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class KycResponse {

    @JsonProperty("PAN-RESPONSE-DETAILS")
    private PanResponseDetails panResponseDetails;

    public PanResponseDetails getPanResponseDetails() {
        return panResponseDetails;
    }

    public void setPanResponseDetails(PanResponseDetails panResponseDetails) {
        this.panResponseDetails = panResponseDetails;
    }

    @Override
    public String toString() {
        return "KycResponse [panResponseDetails=" + panResponseDetails + "]";
    }
}
