package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by mahesh on 24/10/17.
 */
@Document(collection = "cancelApplicationDetails")
@Data
public class CancelApplicationDetails {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sParentRefId")
    private String parentRefId;

    @JsonProperty("sCurrentStatus")
    private String currentStatus;

    @JsonProperty("sCurrentStage")
    private String currentStage;

    @JsonProperty("sPreviousStatus")
    private String previousStatus;

    @JsonProperty("sPreviousStage")
    private String previousStage;

    @JsonProperty("sStatusChangeReason")
    private String statusChangeReason;

    @JsonProperty
    private Date statusUpdateDate = new Date();
}
