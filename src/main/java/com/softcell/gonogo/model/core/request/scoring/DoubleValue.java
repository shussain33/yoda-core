package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 31/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DoubleValue {
    @JsonProperty("dValue")
    private Double value;
}