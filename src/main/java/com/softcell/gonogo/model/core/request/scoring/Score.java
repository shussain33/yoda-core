package com.softcell.gonogo.model.core.request.scoring;

public class Score {

    private String bureauScore;
    private String bureauScoreConfidLevel;
    private String creditRating;


    public String getBureauScore() {
        return bureauScore;
    }

    public void setBureauScore(String bureauScore) {
        this.bureauScore = bureauScore;
    }

    public String getBureauScoreConfidLevel() {
        return bureauScoreConfidLevel;
    }

    public void setBureauScoreConfidLevel(String bureauScoreConfidLevel) {
        this.bureauScoreConfidLevel = bureauScoreConfidLevel;
    }

    public String getCreditRating() {
        return creditRating;
    }

    public void setCreditRating(String creditRating) {
        this.creditRating = creditRating;
    }

    @Override
    public String toString() {
        return "Score [bureauScore=" + bureauScore
                + ", bureauScoreConfidLevel=" + bureauScoreConfidLevel
                + ", creditRating=" + creditRating + "]";
    }
}
