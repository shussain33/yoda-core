
package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Tribunal {

    @JsonProperty("appealNumber")
    public String appealNumber;
    @JsonProperty("appellant")
    public String appellant;
    @JsonProperty("assessmentYear")
    public String assessmentYear;
    @JsonProperty("bench")
    public String bench;
    @JsonProperty("benchAlloted")
    public String benchAlloted;
    @JsonProperty("caseNo")
    public String caseNo;
    @JsonProperty("caseStatus")
    public String caseStatus;
    @JsonProperty("caseTypeCaseNoYear")
    public String caseTypeCaseNoYear;
    @JsonProperty("dateOfDisposal")
    public String dateOfDisposal;
    @JsonProperty("dateOfFiling")
    public String dateOfFiling;
    @JsonProperty("dateOfFinalHearing")
    public String dateOfFinalHearing;
    @JsonProperty("dateOfFirstHearing")
    public String dateOfFirstHearing;
    @JsonProperty("dateOfLastHearing")
    public String dateOfLastHearing;
    @JsonProperty("dateOfNextHearing")
    public String dateOfNextHearing;
    @JsonProperty("dateOfPronouncement")
    public String dateOfPronouncement;
    @JsonProperty("dateOfTribunalOrder")
    public String dateOfTribunalOrder;
    @JsonProperty("diaryNoYear")
    public String diaryNoYear;
    @JsonProperty("disposalNature")
    public String disposalNature;
    @JsonProperty("filedOn")
    public String filedOn;
    @JsonProperty("hearingDate")
    public String hearingDate;
    @JsonProperty("lastActionInCourt")
    public String lastActionInCourt;
    @JsonProperty("nextListingPurpose")
    public String nextListingPurpose;
    @JsonProperty("orderResult")
    public String orderResult;
    @JsonProperty("petitionerName")
    public String petitionerName;
    @JsonProperty("previousListingDate")
    public String previousListingDate;
    @JsonProperty("respondentName")
    public String respondentName;
    @JsonProperty("tribunal")
    public String tribunal;
    @JsonProperty("tribunalOrder")
    public String tribunalOrder;

}
