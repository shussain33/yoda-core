package com.softcell.gonogo.model.core.ocr;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg228 on 6/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OCRData {

    @JsonProperty("sKeyName")
    public String keyName;

    @JsonProperty("sKeyValue")
    public String keyValue;

    @JsonProperty("sKeyValidate")
    public String keyValidate;

    @JsonProperty("sKeyMapTo")
    public String keyMapTo;

    @JsonProperty("sAPIValue")
    public String apiValue;

    @JsonProperty("sAppValue")
    public String appValue;

    @JsonProperty("dPercentMatch")
    public double percentMatch;

    @JsonProperty("bChanged")
    public boolean changed;

    @JsonProperty("dConfidenceScore")
    public double confidenceScore;

}
