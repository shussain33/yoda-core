package com.softcell.gonogo.model.core.valuation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.adroit.ImageList;
import com.softcell.gonogo.model.core.Name;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

import java.util.Date;
import java.util.List;

/**
 * Created by suhasini on 3/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValuationOutput {
    @JsonProperty("oCurrentPropertyOwner")
    private Name currentPropertyOwner;

    @JsonProperty("oPropertyAddress")
    private CustomerAddress propertyAddress;

    @JsonProperty("oInspectionAddress")
    private CustomerAddress inspectionAddress;

    @JsonProperty("bPropertyWithinMC")
    private boolean propertyWithinMC;

    @JsonProperty("bDemarcation")
    private boolean demarcation;

    @JsonProperty("sDistanceFromBranch")
    private String branchdistance;

    @JsonProperty("bPlanSanctioned")
    private boolean planSanctioned;

    @JsonProperty("bBuildingSanctioned")
    private boolean buildingSanctioned;

    /* (Resi, Comm, Mixed use) */
    @JsonProperty("sPropertyUsage")
    private String propertyUsage;

    /* (Residential, Commercial, Mixed use, Industrial) */
    @JsonProperty("sPropertyNature")
    private String propertyNature;

    /* (Self occupied, rented) */
    @JsonProperty("sPropertyOccupancy")
    private String propertyOccupancy;

    @JsonProperty("iNoOfTenants")
    private int noOfTenants;

    @JsonProperty("sLocalityType")
    private String localityType;

    /* (Poor, Average, Good) */
    @JsonProperty("sPropertyCondition")
    private String propertyCondition;

    @JsonProperty("sResidualAge")
    private String residualAge;

    @JsonProperty("sLandArea")
    private String landArea;

    @JsonProperty("sLandAreaUnit")
    private String landAreaUnit;

    @JsonProperty("sLandRate")
    private String landRate;

    @JsonProperty("sBuiltUpArea")
    private String builtUpArea;

    @JsonProperty("sBuiltUpAreaUnit")
    private String builtUpAreaUnit;

    @JsonProperty("sPermissibleArea")
    private String permissibleArea;

    @JsonProperty("sPermissibleAreaUnit")
    private String permissibleAreaUnit;

    @JsonProperty("sAdoptedArea")
    private String adoptedArea;

    @JsonProperty("sAdoptedAreaUnit")
    private String adoptedAreaUnit;

    @JsonProperty("sConstructionRate")
    private String constructionRate;

    @JsonProperty("sGovtApprovedRate")
    private String govtApprovedRate;

    @JsonProperty("sCurrentMarketValue")
    private String currentMarketValue;

    @JsonProperty("sRemarks")
    private String remarks;

    @JsonProperty("iValueToConsider")
    private int valueToConsider;

    @JsonProperty("dtPropertyValuationDate")
    private Date propertyValuationDate;

    @Transient
    private List<ImageList> imageList;
}
