package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.KarzaResultDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication.ComplianceStatus;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GspGstReturnFillingResponseDetails {

    @JsonProperty("compliance_status")
    private ComplianceStatus compliance_status;

    @JsonProperty("gstinId")
    private String gstinId;

    @JsonProperty("result")
    public List<KarzaResultDetails> karzaResultDetailsList;


}