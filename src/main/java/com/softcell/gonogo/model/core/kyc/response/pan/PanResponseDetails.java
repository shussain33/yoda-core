package com.softcell.gonogo.model.core.kyc.response.pan;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;

import java.util.List;

/**
 * @author yogeshb
 */
public class PanResponseDetails {

    @JsonProperty("NSDL-STATUS")
    private String nsdlStatus;

    @JsonProperty("ERRORS")
    private List<Errors> errorList;

    @JsonProperty("PAN-RESPONSE")
    private PanInfo panInfo;

    public String getNsdlStatus() {
        return nsdlStatus;
    }

    public void setNsdlStatus(String nsdlStatus) {
        this.nsdlStatus = nsdlStatus;
    }

    public List<Errors> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Errors> errorList) {
        this.errorList = errorList;
    }

    public PanInfo getPanInfo() {
        return panInfo;
    }

    public void setPanInfo(PanInfo panInfo) {
        this.panInfo = panInfo;
    }

    @Override
    public String toString() {
        return "PanResponseDetails [nsdlStatus=" + nsdlStatus + ", errorList="
                + errorList + ", panInfo=" + panInfo + "]";
    }
}
