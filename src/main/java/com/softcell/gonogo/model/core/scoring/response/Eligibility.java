package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * @author prateek
 */
public class Eligibility {
    @JsonProperty("EXT-ELGIBLITY")
    ExtendedEligibility extendedEligibilty;
    @JsonProperty("VALUES")
    private Object values;
    @JsonProperty("ElgbltyID")
    private String eligibleId;
    @JsonProperty("GridID")
    private Long gridId;
    @JsonProperty("FOIR_AMOUNT")
    private Double foir_amount;
    @JsonProperty("APPROVED_AMOUNT")
    private Double approvedAmount;
    @JsonProperty("Error")
    private String error;
    @JsonProperty("DECISION")
    private String decision;
    @JsonProperty("COMPUTE_DISP")
    private String computeDisp;
    @JsonProperty("COMPUTE_LOGIC")
    private String computeLogic;
    @JsonProperty("MAX_AMOUNT")
    private double maxAmount;
    @JsonProperty("MIN_AMOUNT")
    private double minAmount;
    @JsonProperty("DP")
    private double dp;
    @JsonProperty("MAX_TENOR")
    private double maxTenor;
    @JsonProperty("REMARK")
    private String reMark;
    @JsonProperty("COMPUTED_AMOUNT")
    private double computedAmount;
    @JsonProperty("ELIGIBILITY_AMOUNT")
    private double eligibilityAmount;
    @JsonProperty("CNT")
    private double cnt;
    @JsonProperty("RULE-SEQ")
    private Double ruleExucSeq;
    @JsonProperty("GRID_EXP")
    private String gridExpression;
    @JsonProperty("MUL_PROD")
    private int productsAllowed;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("ADDITIONAL_FIELDS")
    private Map<String, Object> additionalFields = new HashMap<>();

    public Object getValues() {
        return values;
    }

    public void setValues(Object values) {
        this.values = values;
    }

    public String getGridExpression() {
        return gridExpression;
    }

    public void setGridExpression(String gridExpression) {
        this.gridExpression = gridExpression;
    }

    public Double getRuleExucSeq() {
        return ruleExucSeq;
    }

    public void setRuleExucSeq(Double ruleExucSeq) {
        this.ruleExucSeq = ruleExucSeq;
    }

    public String getEligibleId() {
        return eligibleId;
    }

    public void setEligibleId(String eligibleId) {
        this.eligibleId = eligibleId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public String getComputeDisp() {
        return computeDisp;
    }

    public void setComputeDisp(String computeDisp) {
        this.computeDisp = computeDisp;
    }

    public String getComputeLogic() {
        return computeLogic;
    }

    public void setComputeLogic(String computeLogic) {
        this.computeLogic = computeLogic;
    }

    public double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(double minAmount) {
        this.minAmount = minAmount;
    }

    public double getDp() {
        return dp;
    }

    public void setDp(double dp) {
        this.dp = dp;
    }

    public double getMaxTenor() {
        return maxTenor;
    }

    public void setMaxTenor(double maxTenor) {
        this.maxTenor = maxTenor;
    }

    public String getReMark() {
        return reMark;
    }

    public void setReMark(String reMark) {
        this.reMark = reMark;
    }

    public double getComputedAmount() {
        return computedAmount;
    }

    public void setComputedAmount(double computedAmount) {
        this.computedAmount = computedAmount;
    }

    public double getEligibilityAmount() {
        return eligibilityAmount;
    }

    public void setEligibilityAmount(double eligibilityAmount) {
        this.eligibilityAmount = eligibilityAmount;
    }

    public double getCnt() {
        return cnt;
    }

    public void setCnt(double cnt) {
        this.cnt = cnt;
    }

    public Long getGridId() {
        return gridId;
    }

    public void setGridId(Long gridId) {
        this.gridId = gridId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * @param additionalProperties the additionalProperties to set
     */
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Double getFoir_amount() {
        return foir_amount;
    }

    public void setFoir_amount(Double foir_amount) {
        this.foir_amount = foir_amount;
    }

    public Double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(Double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public int getProductsAllowed() {
        return productsAllowed;
    }

    public void setProductsAllowed(int productsAllowed) {
        this.productsAllowed = productsAllowed;
    }


    /**
     * @return the extendedEligibilty
     */
    public ExtendedEligibility getExtendedEligibilty() {
        return extendedEligibilty;
    }

    /**
     * @param extendedEligibilty the extendedEligibilty to set
     */
    public void setExtendedEligibilty(ExtendedEligibility extendedEligibilty) {
        this.extendedEligibilty = extendedEligibilty;
    }

    public Map<String, Object> getAdditionalFields() { return additionalFields; }

    public void setAdditionalFields(Map<String, Object> additionalFields) { this.additionalFields = additionalFields;  }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Eligibility [values=");
        builder.append(values);
        builder.append(", eligibleId=");
        builder.append(eligibleId);
        builder.append(", gridId=");
        builder.append(gridId);
        builder.append(", foir_amount=");
        builder.append(foir_amount);
        builder.append(", approvedAmount=");
        builder.append(approvedAmount);
        builder.append(", error=");
        builder.append(error);
        builder.append(", decision=");
        builder.append(decision);
        builder.append(", computeDisp=");
        builder.append(computeDisp);
        builder.append(", computeLogic=");
        builder.append(computeLogic);
        builder.append(", maxAmount=");
        builder.append(maxAmount);
        builder.append(", minAmount=");
        builder.append(minAmount);
        builder.append(", dp=");
        builder.append(dp);
        builder.append(", maxTenor=");
        builder.append(maxTenor);
        builder.append(", reMark=");
        builder.append(reMark);
        builder.append(", computedAmount=");
        builder.append(computedAmount);
        builder.append(", eligibilityAmount=");
        builder.append(eligibilityAmount);
        builder.append(", cnt=");
        builder.append(cnt);
        builder.append(", ruleExucSeq=");
        builder.append(ruleExucSeq);
        builder.append(", gridExpression=");
        builder.append(gridExpression);
        builder.append(", productsAllowed=");
        builder.append(productsAllowed);
        builder.append(", extendedEligibilty=");
        builder.append(extendedEligibilty);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append(", additionalFields=");
        builder.append(additionalFields);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result
                + ((approvedAmount == null) ? 0 : approvedAmount.hashCode());
        long temp;
        temp = Double.doubleToLongBits(cnt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((computeDisp == null) ? 0 : computeDisp.hashCode());
        result = prime * result
                + ((computeLogic == null) ? 0 : computeLogic.hashCode());
        temp = Double.doubleToLongBits(computedAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((decision == null) ? 0 : decision.hashCode());
        temp = Double.doubleToLongBits(dp);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(eligibilityAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((eligibleId == null) ? 0 : eligibleId.hashCode());
        result = prime * result + ((error == null) ? 0 : error.hashCode());
        result = prime
                * result
                + ((extendedEligibilty == null) ? 0 : extendedEligibilty
                .hashCode());
        result = prime * result
                + ((foir_amount == null) ? 0 : foir_amount.hashCode());
        result = prime * result
                + ((gridExpression == null) ? 0 : gridExpression.hashCode());
        result = prime * result + ((gridId == null) ? 0 : gridId.hashCode());
        temp = Double.doubleToLongBits(maxAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maxTenor);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(minAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + productsAllowed;
        result = prime * result + ((reMark == null) ? 0 : reMark.hashCode());
        result = prime * result
                + ((ruleExucSeq == null) ? 0 : ruleExucSeq.hashCode());
        result = prime * result + ((values == null) ? 0 : values.hashCode());
        result = prime * result + ((additionalFields == null) ? 0 : additionalFields.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Eligibility other = (Eligibility) obj;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (approvedAmount == null) {
            if (other.approvedAmount != null)
                return false;
        } else if (!approvedAmount.equals(other.approvedAmount))
            return false;
        if (Double.doubleToLongBits(cnt) != Double.doubleToLongBits(other.cnt))
            return false;
        if (computeDisp == null) {
            if (other.computeDisp != null)
                return false;
        } else if (!computeDisp.equals(other.computeDisp))
            return false;
        if (computeLogic == null) {
            if (other.computeLogic != null)
                return false;
        } else if (!computeLogic.equals(other.computeLogic))
            return false;
        if (Double.doubleToLongBits(computedAmount) != Double
                .doubleToLongBits(other.computedAmount))
            return false;
        if (decision == null) {
            if (other.decision != null)
                return false;
        } else if (!decision.equals(other.decision))
            return false;
        if (Double.doubleToLongBits(dp) != Double.doubleToLongBits(other.dp))
            return false;
        if (Double.doubleToLongBits(eligibilityAmount) != Double
                .doubleToLongBits(other.eligibilityAmount))
            return false;
        if (eligibleId == null) {
            if (other.eligibleId != null)
                return false;
        } else if (!eligibleId.equals(other.eligibleId))
            return false;
        if (error == null) {
            if (other.error != null)
                return false;
        } else if (!error.equals(other.error))
            return false;
        if (extendedEligibilty == null) {
            if (other.extendedEligibilty != null)
                return false;
        } else if (!extendedEligibilty.equals(other.extendedEligibilty))
            return false;
        if (foir_amount == null) {
            if (other.foir_amount != null)
                return false;
        } else if (!foir_amount.equals(other.foir_amount))
            return false;
        if (gridExpression == null) {
            if (other.gridExpression != null)
                return false;
        } else if (!gridExpression.equals(other.gridExpression))
            return false;
        if (gridId == null) {
            if (other.gridId != null)
                return false;
        } else if (!gridId.equals(other.gridId))
            return false;
        if (Double.doubleToLongBits(maxAmount) != Double
                .doubleToLongBits(other.maxAmount))
            return false;
        if (Double.doubleToLongBits(maxTenor) != Double
                .doubleToLongBits(other.maxTenor))
            return false;
        if (Double.doubleToLongBits(minAmount) != Double
                .doubleToLongBits(other.minAmount))
            return false;
        if (productsAllowed != other.productsAllowed)
            return false;
        if (reMark == null) {
            if (other.reMark != null)
                return false;
        } else if (!reMark.equals(other.reMark))
            return false;
        if (ruleExucSeq == null) {
            if (other.ruleExucSeq != null)
                return false;
        } else if (!ruleExucSeq.equals(other.ruleExucSeq))
            return false;
        if (values == null) {
            if (other.values != null)
                return false;
        } else if (!values.equals(other.values))
            return false;
        if (additionalFields == null) {
            if (other.additionalFields != null)
                return false;
        } else if (!additionalFields.equals(other.additionalFields))
            return false;
        return true;
    }
}
