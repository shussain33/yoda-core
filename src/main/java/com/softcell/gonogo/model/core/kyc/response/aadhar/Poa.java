package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Poa {
	@XmlAttribute(name="co")
	private String co;
	@XmlAttribute(name="house")
	private String house;
	@XmlAttribute(name="street")
	private String street;
	@XmlAttribute(name="lm")
	private String lm;
	@XmlAttribute(name="loc")
	private String loc;
	@XmlAttribute(name="vtc")
	private String vtc;
	@XmlAttribute(name="subdist")
	private String subdist;
	@XmlAttribute(name="dist")
	private String dist;
	@XmlAttribute(name="state")
	private String state;
	@XmlAttribute(name="pc")
	private String pc;
	@XmlAttribute(name="po")
	private String po;
	@XmlAttribute(name="country")
	private String country;
	
	public String getCo() {
		return co;
	}
	public void setCo(String co) {
		this.co = co;
	}
	public String getHouse() {
		return house;
	}
	public void setHouse(String house) {
		this.house = house;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getLm() {
		return lm;
	}
	public void setLm(String lm) {
		this.lm = lm;
	}
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	public String getVtc() {
		return vtc;
	}
	public void setVtc(String vtc) {
		this.vtc = vtc;
	}
	public String getSubdist() {
		return subdist;
	}
	public void setSubdist(String subdist) {
		this.subdist = subdist;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPc() {
		return pc;
	}
	public void setPc(String pc) {
		this.pc = pc;
	}
	public String getPo() {
		return po;
	}
	public void setPo(String po) {
		this.po = po;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString() {
		return "Poa [co=" + co + ", house=" + house + ", street=" + street
				+ ", lm=" + lm + ", loc=" + loc + ", vtc=" + vtc + ", subdist="
				+ subdist + ", dist=" + dist + ", state=" + state + ", pc="
				+ pc + ", po=" + po + ", country=" + country + "]";
	}
}
