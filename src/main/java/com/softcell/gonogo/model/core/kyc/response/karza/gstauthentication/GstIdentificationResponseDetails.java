package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GstIdentificationResponseDetails {

    @JsonProperty("emailId")
    private String emailId;

    @JsonProperty("applicationStatus")
    private String applicationStatus;

    @JsonProperty("mobNum")
    private String mobNum;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("gstinRefId")
    private String gstinRefId;

    @JsonProperty("regType")
    private String regType;

    @JsonProperty("gstinId")
    private String gstinId;

    @JsonProperty("registrationName")
    private String registrationName;

    @JsonProperty("tinNumber")
    private String tinNumber;

}