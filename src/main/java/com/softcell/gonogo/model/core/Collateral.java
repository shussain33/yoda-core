package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.Address;
import com.softcell.gonogo.model.address.CustomerAddress;

import java.util.List;

/**
 * @author suhasinis
 *
 */

public class Collateral {

	@JsonProperty("sCollId")
	private String collateralId;

	@JsonProperty("aOwnerNames")
	private List<Name> ownerNames;

	@JsonProperty("bWithinCityLimit")
	private boolean withinCityLimit; // within city limit

	@JsonProperty("sOccupation")
	private String occupation;

	@JsonProperty("oAddress")
	private CustomerAddress address;

	@JsonProperty("sType")
	private String type;

	@JsonProperty("sStatus")
	private String status;

	@JsonProperty("sUsage")
	private String usage;

	@JsonProperty("sOwnerShip")
	private String ownerShip;

	@JsonProperty("sPercentageOfCompletion")
	private String percentageOfCompletion;

	@JsonProperty("dPropertyValue1")
	private Double propertyValue1;

	@JsonProperty("dPropertyValue2")
	private Double propertyValue2;

	@JsonProperty("dAvgPropertyValue")
	private double avgPropertyValue;

      @JsonProperty("iValueToConsider")
      private int valueToConsider;

	@JsonProperty("sPropertyArea")
	private String propertyArea;

	@JsonProperty("sPropertyAreaUnit")
	private String propertyAreaUnit;

	@JsonProperty("sPropertyCategorization")
	private String propertyCategorization;

	public String getCollateralId() {
		return collateralId;
	}

	public void setCollateralId(String collateralId) {
		this.collateralId = collateralId;
	}

	public String getPercentageOfCompletion() {
		return percentageOfCompletion;
	}

	public void setPercentageOfCompletion(String percentageOfCompletion) {
		this.percentageOfCompletion = percentageOfCompletion;
	}

	public boolean isWithinCityLimit() {
		return withinCityLimit;
	}

	public void setWithinCityLimit(boolean isWithinCityLimit) {
		this.withinCityLimit = isWithinCityLimit;
	}

    public CustomerAddress getAddress() {
        return address;
    }

    public void setAddress(CustomerAddress address) {
        this.address = address;
    }

    public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUsage() {
		return usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}

	public String getOwnerShip() {
		return ownerShip;
	}

	public void setOwnerShip(String ownerShip) {
		this.ownerShip = ownerShip;
	}


	public List<Name> getOwnerNames() {
		return ownerNames;
	}

	public void setOwnerNames(List<Name> ownerNames) {
		this.ownerNames = ownerNames;
	}

	public Double getPropertyValue1() {
		return propertyValue1;
	}

	public void setPropertyValue1(Double propertyValue1) {
		this.propertyValue1 = propertyValue1;
	}

	public Double getPropertyValue2() {
		return propertyValue2;
	}

	public void setPropertyValue2(Double propertyValue2) {
		this.propertyValue2 = propertyValue2;
	}

	public double getAvgPropertyValue() {
		return avgPropertyValue;
	}

	public void setAvgPropertyValue(double avgPropertyValue) {
		this.avgPropertyValue = avgPropertyValue;
	}

    public int getValueToConsider() {
        return valueToConsider;
    }

    public void setValueToConsider(int valueToConsider) {
        this.valueToConsider = valueToConsider;
    }

	public String getPropertyArea() {
		return propertyArea;
	}

	public void setPropertyArea(String propertyArea) {
		this.propertyArea = propertyArea;
	}

	public String getPropertyAreaUnit() {
		return propertyAreaUnit;
	}

	public void setPropertyAreaUnit(String propertyAreaUnit) {
		this.propertyAreaUnit = propertyAreaUnit;
	}

	public String getPropertyCategorization() { return propertyCategorization; }

	public void setPropertyCategorization(String propertyCategorization) { this.propertyCategorization = propertyCategorization; }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Collateral [collateralId=");
		builder.append(collateralId);
		builder.append(", withinCityLimit=");
		builder.append(withinCityLimit);
		builder.append(", occupation=");
		builder.append(occupation);
		builder.append(", status=");
		builder.append(status);
		builder.append(", type=");
		builder.append(type);
		builder.append(", address=");
		builder.append(address);
		builder.append(", usage=");
		builder.append(usage);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((occupation == null) ? 0 : occupation.hashCode());
		result = prime * result + ((usage == null) ? 0 : usage.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = 31 * result + (withinCityLimit ? 1 : 0);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Collateral other = (Collateral) obj;

		if (collateralId == null) {
			if (other.collateralId != null)
				return false;
		} else if (!collateralId.equals(other.collateralId))
			return false;


		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (occupation == null) {
			if (other.occupation != null)
				return false;
		} else if (!occupation.equals(other.occupation))
			return false;
		if (usage == null) {
			if (other.usage != null)
				return false;
		} else if (!usage.equals(other.usage))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
          return withinCityLimit == other.withinCityLimit;
      }
}
