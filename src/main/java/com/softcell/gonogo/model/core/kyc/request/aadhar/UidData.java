package com.softcell.gonogo.model.core.kyc.request.aadhar;

public class UidData {
	
	private PersonalIdData pid;
	private String type;
	
	
	public PersonalIdData getPid() {
		return pid;
	}

	public void setPid(PersonalIdData pid) {
		this.pid = pid;
	}

	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "UidData [pid=" + pid + ", type=" + type + "]";
	}
	
	

}
