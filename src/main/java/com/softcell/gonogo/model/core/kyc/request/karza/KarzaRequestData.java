package com.softcell.gonogo.model.core.kyc.request.karza;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.AadharDemographics;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KarzaRequestData {

    @JsonProperty("consent")
    private String consent;

    @JsonProperty("dl_no")
    private String dl_no;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("epic_no")
    private String epic_no;

    @JsonProperty("aadhaar-id")
    private String aadhaarid;

    @JsonProperty("demographics")
    public AadharDemographics demographics;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("jobcardid")
    private String jobcardid;

    @JsonProperty("name")
    public String name;

    @JsonProperty("last_name")
    public String lastName;

    @JsonProperty("doe")
    public String doe;

    @JsonProperty("gender")
    public String gender;

    @JsonProperty("passport_no")
    public String passportNo;

    @JsonProperty("type")
    public String type;

    @JsonProperty("country")
    public String country;

    @JsonProperty("tan")
    private String tan;

    @JsonProperty("cin")
    private String cin;

    @JsonProperty("company_name")
    private String company_name;

    @JsonProperty("iec")
    private String iec;

    @JsonProperty("aadhar")
    private String aadhar;

    @JsonProperty("uan")
    private String uan;

    @JsonProperty("gstin")
    private String gstin;

    @JsonProperty("reg_no")
    private String reg_no;

    @JsonProperty("area_code")
    private String area_code;

    @JsonProperty("licence_no")
    private String licence_no;

    @JsonProperty("state")
    private String state;

    @JsonProperty("membership_no")
    private String membership_no;

    @JsonProperty("cp_no")
    private String cp_no;

    @JsonProperty("registration_no")
    private String registration_no;

    @JsonProperty("year_of_reg")
    private String year_of_reg;

    @JsonProperty("medical_council")
    private String medical_council;

    @JsonProperty("consumer_id")
    private String consumer_id;

    @JsonProperty("bp_no")
    private String bp_no;

    @JsonProperty("service_provider")
    private String service_provider;

    @JsonProperty("tel_no")
    private String tel_no;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("request_id")
    private String request_id;

    @JsonProperty("otp")
    private String otp;

    @JsonProperty("lpg_id")
    private String lpg_id;

    @JsonProperty("esic_id")
    private String esic_id;

    @JsonProperty("cert_no")
    private String cert_no;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("fiscal_year")
    private String fiscal_year;

    @JsonProperty("ack")
    private String ack;

    @JsonProperty("address1")
    private String address1;

    @JsonProperty("address2")
    private String address2;

    @JsonProperty("email")
    private String email;

    @JsonProperty("name1")
    private String name1;

    @JsonProperty("name2")
    private String name2;

    @JsonProperty("hsCode")
    private String hsCode;

    @JsonProperty("ifsc")
    private String ifsc;

    @JsonProperty("accountNumber")
    private String accountNumber;

    @JsonProperty("domain")
    private String domain;

    @JsonProperty("engine_no")
    private String engine_no;

    @JsonProperty("chassis_no")
    private String chassis_no;

    @JsonProperty("entityId")
    private String entityId;

    @JsonProperty("nameOfCorporateDebtor")
    private String nameOfCorporateDebtor;

    @JsonProperty("relation")
    private String relation;

    @JsonProperty("court_complex")
    private String court_complex;

    @JsonProperty("results_per_page")
    private String results_per_page;

    @JsonProperty("page_no")
    private String page_no;

    @JsonProperty("username")
    private String gstUserName;

    @JsonProperty("password")
    private String gstPassword;

    @JsonProperty("input")
    private String input;

    @JsonProperty("tanNo")
    private String tanNo;

    @JsonProperty("gstNo")
    private String gstNo;

    @JsonProperty("tinNo")
    private String tinNo;

    @JsonProperty("individualName")
    private String individualName;

    @JsonProperty("organizationName")
    private String organizationName;

    @JsonProperty("employeeName")
    private String employeeName;

    @JsonProperty("employerName")
    private String employerName;

    @JsonProperty("isPdf")
    private boolean isPdf;

    @JsonProperty("emailId")
    private String emailId;
}
