package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 10/8/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StringValue {
    @JsonProperty("sValue")
    private String value;
}
