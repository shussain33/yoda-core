package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Uses {
    @JsonProperty("PI")
    private String pi;

    @JsonProperty("PA")
    private String pa;

    @JsonProperty("PFA")
    private String pfa;

    @JsonProperty("BIO")
    private String bio;

    @JsonProperty("BT")
    private String bt;

    @JsonProperty("PIN")
    private String pin;

    @JsonProperty("OTP")
    private String otp;

    public static Builder builder() {
        return new Builder();
    }

    public String getPi() {
        return pi;
    }

    public void setPi(String pi) {
        this.pi = pi;
    }

    public String getPa() {
        return pa;
    }

    public void setPa(String pa) {
        this.pa = pa;
    }

    public String getPfa() {
        return pfa;
    }

    public void setPfa(String pfa) {
        this.pfa = pfa;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getBt() {
        return bt;
    }

    public void setBt(String bt) {
        this.bt = bt;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Uses{");
        sb.append("pi='").append(pi).append('\'');
        sb.append(", pa='").append(pa).append('\'');
        sb.append(", pfa='").append(pfa).append('\'');
        sb.append(", bio='").append(bio).append('\'');
        sb.append(", bt='").append(bt).append('\'');
        sb.append(", pin='").append(pin).append('\'');
        sb.append(", otp='").append(otp).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Uses)) return false;
        Uses uses = (Uses) o;
        return Objects.equal(getPi(), uses.getPi()) &&
                Objects.equal(getPa(), uses.getPa()) &&
                Objects.equal(getPfa(), uses.getPfa()) &&
                Objects.equal(getBio(), uses.getBio()) &&
                Objects.equal(getBt(), uses.getBt()) &&
                Objects.equal(getPin(), uses.getPin()) &&
                Objects.equal(getOtp(), uses.getOtp());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPi(), getPa(), getPfa(), getBio(), getBt(), getPin(), getOtp());
    }

    public static class Builder {
        private Uses uses = new Uses();

        public Uses build() {
            return uses;
        }

        public Builder pi(String pi) {
            this.uses.pi = pi;
            return this;
        }

        public Builder pa(String pa) {
            this.uses.pa = pa;
            return this;
        }

        public Builder pfa(String pfa) {
            this.uses.pfa = pfa;
            return this;
        }

        public Builder bio(String bio) {
            this.uses.bio = bio;
            return this;
        }

        public Builder bt(String bt) {
            this.uses.bt = bt;
            return this;
        }

        public Builder pin(String pin) {
            this.uses.pin = pin;
            return this;
        }

        public Builder otp(String otp) {
            this.uses.otp = otp;
            return this;
        }

    }
}
