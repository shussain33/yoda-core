package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import com.softcell.ssl2.hunter.model.matchresponse.HunterResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by ssg228 on 8/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RCUSummary {

    @JsonProperty("iTotalDocs")
    private int totalDocs;

    @JsonProperty("iSampleDocs")
    private int sampleDocs;

    @JsonProperty("sFinalDecision")
    public String finalDecision;

    @JsonProperty("sFinalDecisionPDD")
    public String finalDecisionPDD;

    @JsonProperty("sSamplerFinalDecision")
    public String samplerFinalDecision;

    @JsonProperty("sFinalAgencyDedupe")
    public String finalAgencyDedupe;

    @JsonProperty("oFinalDecisionRemark")
    public Remark finalDecisionRemark;

    @JsonProperty("iTotalNoOfDocs")
    public int totalNoOfDocs;

    // MATCH /NO-MATCH
    @JsonProperty("sHunterStatus")
    public String hunterStatus;

    // resons from initiate trigger / APS notification response
    @JsonProperty("sHunterDecision")
    public String hunterDecision;

    @JsonProperty("sHunterStage")
    public String hunterSatge;

    // Hunter decision - received for each trigger - there can be more triggers if no. of coapplicants are more
    // This map is ackId vs decision. Useful in deciding final decision.
    @JsonIgnore
    Map<String, String> hunterDecisionMap;

    @JsonProperty("sRcuStatus")
    public String rcuStatus;

    @JsonProperty("sRcuTat")
    public String rcuTAT;

    @JsonProperty("aActivity")
    public List<RCUActivity> rcuActivityList;

    @JsonProperty("oSamplerRemark")
    public Remark samplerRemark;

    @JsonProperty("oCallHunterResponse")
    CallHunterResponse callHunterResponse;

    // added for abfl
    @JsonProperty("sCreditcomments")
    public String creditComments;

    @JsonProperty("sCreditapproval")
    public String creditApproval;

    @JsonProperty("sFCUapproval")
    public String fcuApproval;

    @JsonProperty("sFinalStatus")
    public String finalStatus;

    //added for hdfc
    @JsonProperty("sSamplingSource")
    public String samplingSource;

    @JsonProperty("dtSamplingDate")
    public Date rcuSamplingDate;

    @JsonProperty("dtSamplingQueueDate")
    public Date rcuSamplingQueueDate;

    @JsonProperty("sRicCheckType")
    public String ricCheckType;

    @JsonProperty("sLimits")
    public String limits;

    @JsonProperty("sSamplingName")
    public String samplingName;

    @JsonProperty("sVerificationRemark")
    public String verificationRemark;

    @JsonProperty("sVerificationStatus")
    public String verificationStatus;

    @JsonProperty("sFCUapproverComments")
    public String fcuApproverComments;

    @JsonProperty("sRole")
    private String role;

    @JsonProperty("sVerificationLocation")
    public String verificationLoc;

    @JsonProperty("iTatInDays")
    public int tatInDays;

    @JsonProperty("iSystemTatInDays")
    public int systemTatInDays;

    @JsonProperty("iTatDiff")
    public int tatDiff;

    @JsonProperty("iOverAllTat")
    public int overAllTat;

    @JsonProperty("oTatRemark")
    public Remark tatRemark;

    @JsonProperty("sPointOfVisit")
    public String pointOfVisit;

    @JsonProperty("sGeographicalLimit")
    public String geographicalLimit;

    @JsonProperty("sFirstApprvAgency")
    public String firstApprvAgency;

    @JsonProperty("sFirstApprvllRemarks")
    public String firstApprvllRemarks;

    @JsonProperty("sFirstAppStatus")
    public String firstAppStatus;

    @JsonProperty("sSecondApprvAgency")
    public String secondApprvAgency;

    @JsonProperty("sSecondApprvllRemarks")
    public String secondApprvllRemarks;

    @JsonProperty("sSecondAppStatus")
    public String secondAppStatus;

    @JsonProperty("iFraudScore")
    public int fraudScore;

    @JsonProperty("aIdentifierStatusList")
    List<IdentifierStatusList> identifierStatusList = null;

    @JsonProperty("sPickupCriteria")
    public String pickupCriteria;

    @JsonProperty("sHuntercomments")
    public String huntercomments;

    @JsonProperty("sCreditLevelofAuthority")
    public String creditLevelofAuthority;

    @JsonProperty("sCreditapprovalComments")
    public String creditapprovalComments;

    @JsonProperty("sCreditRemarks")
    public String creditRemarks;

    @JsonProperty("iRcuStatusValue")
    public int rcuStatusValue;

    @JsonProperty("sSamplerName")
    public String samplerName;

    @JsonProperty("sSamplerMobNo")
    public String samplerMobNo;

    @JsonProperty("sSamplerEmailId")
    public String samplerEmailId;

    public void addHunterResponse(HunterResponse response){
        if(callHunterResponse==null){
            callHunterResponse=new CallHunterResponse();
        }
        List<HunterResponse> hunterResponses=null;
        if(CollectionUtils.isEmpty(callHunterResponse.getHunterResponses() ) ){
            hunterResponses= new ArrayList<>();
        }else{
            hunterResponses=callHunterResponse.getHunterResponses();
        }
        hunterResponses.add(response);
        callHunterResponse.setHunterResponses(hunterResponses);

        /***Save Identifier and Status In List***/
        //updateHunterIdentifierStatusList(callHunterResponse);
    }

    //  use this method for showing hunter identifier
    public void addHunterId(String hunterId){
        if(callHunterResponse == null){
            callHunterResponse = new CallHunterResponse();
        }
        List<String> hunterIdList = callHunterResponse.getHunterIds();
        if(CollectionUtils.isEmpty(hunterIdList ) ){
            hunterIdList = new ArrayList<>();
            callHunterResponse.setHunterIds(hunterIdList);
        }
        hunterIdList.add(hunterId);
    }

    public void addErrorResponse(Object errorResponse){
        if(callHunterResponse==null){
            callHunterResponse=new CallHunterResponse();
        }
        List<Object> errors=null;
        if(CollectionUtils.isEmpty(callHunterResponse.getErrors())){
            errors=new ArrayList<>();
        }else{
            errors=callHunterResponse.getErrors();
        }
        errors.add(errorResponse);
        callHunterResponse.setErrors(errors);
    }
}
