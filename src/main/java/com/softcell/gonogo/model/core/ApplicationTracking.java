package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.workflow.component.module.ModuleSetting;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "gngApplicationTracking")
public class ApplicationTracking extends AuditEntity {

    @JsonIgnore
    private String methodName;

    @JsonProperty("className")
    private String className;

    @JsonIgnore
    @JsonProperty("sInstId")
    private String institutionId;

    @JsonIgnore
    @JsonProperty("sRefID")
    private String gngRefId;

    @JsonProperty("sDsaId")
    private String dsaId;

    @JsonProperty("sAppStat")
    private String applicationStatus;

    @JsonProperty("sCurrentStageId")
    private String currentStageId;

    @JsonProperty("oStDate")
    private Date startDate;
    @JsonIgnore
    @JsonProperty("oEndDate")
    private Date endDate;

    @JsonProperty("lTmDiff")
    private long timeDiffrence;
    @JsonIgnore
    @JsonProperty("oModuleSetting")
    private ModuleSetting moduleSetting;

    @JsonProperty("sNodeDesc")
    private String nodeDescription;

    @JsonProperty("lNodeEdge")
    private long nodeEdge;

    @JsonProperty("lEdgeCumulative")
    private long edgeCumulative;

    @JsonProperty("sNodeEdgeReadable")
    private String nodeEdgeReadable;

    @JsonProperty("sSdgeCumulativeReadable")
    private String edgeCumulativeReadable;
    @JsonProperty("sStepId")
    private String stepId;
    @JsonProperty("oExptn")
    private TrackingException exception;

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getNodeEdgeReadable() {
        return nodeEdgeReadable;
    }

    public void setNodeEdgeReadable(String nodeEdgeReadable) {
        this.nodeEdgeReadable = nodeEdgeReadable;
    }

    public String getEdgeCumulativeReadable() {
        return edgeCumulativeReadable;
    }

    public void setEdgeCumulativeReadable(String edgeCumulativeReadable) {
        this.edgeCumulativeReadable = edgeCumulativeReadable;
    }

    /**
     * @return the methodName
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * @param methodName the methodName to set
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * @return the exception
     */
    public TrackingException getException() {
        return exception;
    }

    /**
     * @param exception the exception to set
     */
    public void setException(TrackingException exception) {
        this.exception = exception;
    }

    /**
     * @return the institutionId
     */
    public String getInstitutionId() {
        return institutionId;
    }

    /**
     * @param institutionId the institutionId to set
     */
    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    /**
     * @return the gngRefId
     */
    public String getGngRefId() {
        return gngRefId;
    }

    /**
     * @param gngRefId the gngRefId to set
     */
    public void setGngRefId(String gngRefId) {
        this.gngRefId = gngRefId;
    }

    /**
     * @return the dsaId
     */
    public String getDsaId() {
        return dsaId;
    }

    /**
     * @param dsaId the dsaId to set
     */
    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }

    /**
     * @return the applicationStatus
     */
    public String getApplicationStatus() {
        return applicationStatus;
    }

    /**
     * @param applicationStatus the applicationStatus to set
     */
    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    /**
     * @return the currentStageId
     */
    public String getCurrentStageId() {
        return currentStageId;
    }

    /**
     * @param currentStageId the currentStageId to set
     */
    public void setCurrentStageId(String currentStageId) {
        this.currentStageId = currentStageId;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the moduleSetting
     */
    public ModuleSetting getModuleSetting() {
        return moduleSetting;
    }

    /**
     * @param moduleSetting the moduleSetting to set
     */
    public void setModuleSetting(ModuleSetting moduleSetting) {
        this.moduleSetting = moduleSetting;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the timeDiffrence
     */
    public long getTimeDiffrence() {
        return timeDiffrence;
    }

    /**
     * @param timeDiffrence the timeDiffrence to set
     */
    public void setTimeDiffrence(long timeDiffrence) {
        this.timeDiffrence = timeDiffrence;
    }

    /**
     * @return the nodeDescription
     */
    public String getNodeDescription() {
        return nodeDescription;
    }

    /**
     * @param nodeDescription the nodeDescription to set
     */
    public void setNodeDescription(String nodeDescription) {
        this.nodeDescription = nodeDescription;
    }

    /**
     * @return the nodeEdge
     */
    public long getNodeEdge() {
        return nodeEdge;
    }

    /**
     * @param nodeEdge the nodeEdge to set
     */
    public void setNodeEdge(long nodeEdge) {
        this.nodeEdge = nodeEdge;
    }

    /**
     * @return the edgeCumulative
     */
    public long getEdgeCumulative() {
        return edgeCumulative;
    }

    /**
     * @param edgeCumulative the edgeCumulative to set
     */
    public void setEdgeCumulative(long edgeCumulative) {
        this.edgeCumulative = edgeCumulative;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((applicationStatus == null) ? 0 : applicationStatus
                .hashCode());
        result = prime * result
                + ((currentStageId == null) ? 0 : currentStageId.hashCode());
        result = prime * result + ((dsaId == null) ? 0 : dsaId.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result
                + ((gngRefId == null) ? 0 : gngRefId.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((startDate == null) ? 0 : startDate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApplicationTracking other = (ApplicationTracking) obj;
        if (applicationStatus == null) {
            if (other.applicationStatus != null)
                return false;
        } else if (!applicationStatus.equals(other.applicationStatus))
            return false;
        if (currentStageId == null) {
            if (other.currentStageId != null)
                return false;
        } else if (!currentStageId.equals(other.currentStageId))
            return false;
        if (dsaId == null) {
            if (other.dsaId != null)
                return false;
        } else if (!dsaId.equals(other.dsaId))
            return false;
        if (endDate == null) {
            if (other.endDate != null)
                return false;
        } else if (!endDate.equals(other.endDate))
            return false;
        if (gngRefId == null) {
            if (other.gngRefId != null)
                return false;
        } else if (!gngRefId.equals(other.gngRefId))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (startDate == null) {
            if (other.startDate != null)
                return false;
        } else if (!startDate.equals(other.startDate))
            return false;
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ApplicationTracking [methodName=");
        builder.append(methodName);
        builder.append(", institutionId=");
        builder.append(institutionId);
        builder.append(", gngRefId=");
        builder.append(gngRefId);
        builder.append(", dsaId=");
        builder.append(dsaId);
        builder.append(", applicationStatus=");
        builder.append(applicationStatus);
        builder.append(", currentStageId=");
        builder.append(currentStageId);
        builder.append(", startDate=");
        builder.append(startDate);
        builder.append(", moduleSetting=");
        builder.append(moduleSetting);
        builder.append(", nodeDescription=");
        builder.append(nodeDescription);
        builder.append(", nodeAge=");
        builder.append(nodeEdge);
        builder.append(", endDate=");
        builder.append(endDate);
        builder.append(", exception=");
        builder.append(exception);
        builder.append("]");
        return builder.toString();
    }

    public class TrackingException {
        private String devMessage;
        private String cause;

        /**
         * @return the devMessage
         */
        public String getDevMessage() {
            return devMessage;
        }

        /**
         * @param devMessage the devMessage to set
         */
        public void setDevMessage(String devMessage) {
            this.devMessage = devMessage;
        }

        /**
         * @return the cause
         */
        public String getCause() {
            return cause;
        }

        /**
         * @param cause the cause to set
         */
        public void setCause(String cause) {
            this.cause = cause;
        }


    }


}
