package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DetailedProfileDirectors {

    @JsonProperty("address")
    private String address;

    @JsonProperty("designation")
    private String designation;

    @JsonProperty("din")
    private String din;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("fatherName")
    private String fatherName;

    @JsonProperty("name")
    private String name;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("tenureBeginDate")
    private String tenureBeginDate;

    @JsonProperty("tenureEndDate")
    private String tenureEndDate;
}