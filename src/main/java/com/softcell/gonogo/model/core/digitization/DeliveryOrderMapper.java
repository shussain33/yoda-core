package com.softcell.gonogo.model.core.digitization;

import com.softcell.constants.CustomFormat;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.DeliveryOrderReport;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;

/**
 * Created by mahesh on 22/11/17.
 */
public class DeliveryOrderMapper {

    public static VelocityContext mapDeliveryOrder(DeliveryOrderReport deliveryOrderReport) {


        VelocityContext context = new VelocityContext();

        if (StringUtils.isNotBlank(deliveryOrderReport.getLogoByteCode())) {
            context.put("logoforDO", deliveryOrderReport.getLogoByteCode());
        }

        context.put("subDeliveryOrder", getSubDeliveryOrderCombinedString(deliveryOrderReport));

        context.put("product", deliveryOrderReport.getProduct().name());

        if (StringUtils.isNotBlank(deliveryOrderReport.getImeiNumber())) {
            context.put("imeiNumber", deliveryOrderReport.getImeiNumber());
        }

        context.put("doCreatedDate", GngDateUtil.changeDateFormat(deliveryOrderReport.getDeliveryOrderDate(), "dd-MM-yyyy"));

        //7.5 is the size require for one char in pixel
        if (StringUtils.isNotBlank(getCombinedModelNo_ProductBrand(deliveryOrderReport))) {

            context.put("ProductAndModelNo", getCombinedModelNo_ProductBrand(deliveryOrderReport));

            context.put("ProductAndModelNoSize", getCombinedModelNo_ProductBrand(deliveryOrderReport).length() * 7.8);
        }

        if (StringUtils.isNotBlank(deliveryOrderReport.getDealerName())) {
            context.put("dealerName", deliveryOrderReport.getDealerName());
        }

        if (StringUtils.isNotBlank(deliveryOrderReport.getProductBrand())) {
            context.put("productBrand", deliveryOrderReport.getProductBrand());
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getProductModel())) {
            context.put("productModel", deliveryOrderReport.getProductModel());
        }

        //7.5 is the size require for one char in pixel
        if (StringUtils.isNotBlank(deliveryOrderReport.getCustomerName())) {
            context.put("customerName", deliveryOrderReport.getCustomerName());
            context.put("customerNameSize", deliveryOrderReport.getCustomerName().length() * 7.8);
        }


        if (StringUtils.isNotBlank(deliveryOrderReport.getMobileNumber())) {
            context.put("mobileNumber", deliveryOrderReport.getMobileNumber());
        }

        if (StringUtils.isNotBlank(deliveryOrderReport.getCustomerAddress())) {
            context.put("customerAddress", deliveryOrderReport.getCustomerAddress());
        }

        if (StringUtils.isNotBlank(deliveryOrderReport.getProductBrand())) {
            context.put("productBrand", deliveryOrderReport.getProductBrand());
        }

        if (StringUtils.isNotBlank(deliveryOrderReport.getProductBrand())) {
            context.put("productCategoryAndMake", getProductCategoryAndMake(deliveryOrderReport));
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getProductBrand())) {
            context.put("productMake", deliveryOrderReport.getProductMake());
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getProductModel())) {
            context.put("productModel", deliveryOrderReport.getProductModel());
        }

        context.put("schemeCode_Desc_emi", getCombinedString(deliveryOrderReport));

        context.put("productCost", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getProductCost()));

        context.put("processingFees", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getProcessingFees()));

        context.put("marginMoney", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getMarginMoney()));

        context.put("advanceEmi", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getAdvanceEMI()));

        context.put("financeAmount", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getFinanceAmount()));

        context.put("dealerSubvention", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getDealerSubvention()));

        context.put("deductionByHdbfs", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getDeductionsByHDBFS()));

        context.put("deductionByTvsc", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getDeductionByTvsc()));

        context.put("manfSubBydealer", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getManufSubBorneByDealer()));

        context.put("totalAmtCollectedByCus", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getTotalAmtCollectedFromCustomer()));

        context.put("otherChargesIfAny", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getOtherChargesIfAny()));

        context.put("netFundingAmount", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getNetFundingAmount()));

        context.put("sumOfDeduction", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getSumOfDeductions()));

        context.put("sumOfDeductionByTvsc", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getSumOfDeductionForTvsc()));

        context.put("finalDisburseAmount", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getFinalDisbursementAmount()));

        context.put("mnfcrSubvMbd", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getManufSubventionMbd()));

        context.put("sumExtWrntAmtInsuranceAmt", CustomFormat.NUMBER_0.format(deliveryOrderReport
                .getSumExtWrntAmtInsuranceAmt()));

        return context;

    }

    public static String getProductCategoryAndMake(DeliveryOrderReport deliveryOrderReport) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(deliveryOrderReport.getProductCategory())) {
            sb.append(deliveryOrderReport.getProductCategory()).append(" - ");
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getProductMake())) {
            sb.append(deliveryOrderReport.getProductMake());
        }
        return sb.toString();
    }

    public static String getCombinedModelNo_ProductBrand(DeliveryOrderReport deliveryOrderReport) {

        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(deliveryOrderReport.getProductBrand())) {
            sb.append(deliveryOrderReport.getProductBrand()).append(" - ");
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getProductModel())) {
            sb.append(deliveryOrderReport.getProductModel());
        }
        return sb.toString();
    }

    public static String getSubDeliveryOrderCombinedString(DeliveryOrderReport deliveryOrderReport) {

        StringBuilder sb = new StringBuilder();

        sb.append(Cache.institutionNameMap.get(deliveryOrderReport.getInstID())).append("/");

        if (StringUtils.isNotBlank(deliveryOrderReport.getProduct().name())) {
            sb.append(deliveryOrderReport.getProduct().name()).append("/DO/");
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getRefID())) {
            sb.append(deliveryOrderReport.getRefID()).append(" - Dealer Copy");
        }
        return sb.toString();
    }

    public static String getCombinedString(DeliveryOrderReport deliveryOrderReport) {

        StringBuilder sb = new StringBuilder();

        if (StringUtils.isNotBlank(deliveryOrderReport.getSchemeCode())) {
            sb.append(deliveryOrderReport.getSchemeCode()).append(" - ");
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getSchemeDscr())) {
            sb.append(deliveryOrderReport.getSchemeDscr()).append(" - ");
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getSchemeType())) {
            sb.append(deliveryOrderReport.getSchemeType()).append(" - ");
        }
        if (StringUtils.isNotBlank(deliveryOrderReport.getSchemeType())) {
            sb.append(" & EMI Rs ").append(CustomFormat.NUMBER_0.format(deliveryOrderReport
                    .getEmi()));
        }

        return sb.toString();
    }


}
