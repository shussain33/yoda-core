package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by ssg228 on 8/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IdentifierStatusList {

    @JsonProperty("shunterId")
    public String hunterId;

    @JsonProperty("sHunterMatchStatus")
    public String hunterMatchStatus;

    @JsonProperty("sHunterDecision")
    public String hunterDecision;

    @JsonProperty("sRuleId")
    public String ruleId;

    @JsonProperty("sRuleDescription")
    public String ruleDescription;

    @JsonProperty("aApplicantList")
    public List<String> applicantList;

    @JsonProperty("sRole")
    public String role;

    @JsonProperty("dtDateTime")
    public Date date;
}
