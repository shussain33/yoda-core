package com.softcell.gonogo.model.core.kyc.request.aadhar;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="Pid")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonalIdData {
	@XmlAttribute(name="ts")
	String ts;
	@XmlAttribute(name="ver")
	String ver;
	@XmlAttribute(name="wadh")
	String wadh;
	@XmlElement(name="Demo")
	DemoGraphicData demo;
	@XmlElement(name="Bios")
	BioMetric bios;
	@XmlElement(name="Pv")
	PinValue pv;
	
	String encPidBlock;
	
	public String getTs() {
		return ts;
	}
	public void setTs(String ts) {
		this.ts = ts;
	}
	public String getVer() {
		return ver;
	}
	public void setVer(String ver) {
		this.ver = ver;
	}
	public String getWadh() {
		return wadh;
	}
	public void setWadh(String wadh) {
		this.wadh = wadh;
	}
	public DemoGraphicData getDemo() {
		return demo;
	}
	public void setDemo(DemoGraphicData demo) {
		this.demo = demo;
	}
	public BioMetric getBios() {
		return bios;
	}
	public void setBios(BioMetric bios) {
		this.bios = bios;
	}
	public PinValue getPv() {
		return pv;
	}
	public void setPv(PinValue pv) {
		this.pv = pv;
	}
	
	
	public String getEncPidBlock() {
		return encPidBlock;
	}
	public void setEncPidBlock(String encPidBlock) {
		this.encPidBlock = encPidBlock;
	}
	
	@Override
	public String toString() {
		return "PersonalIdData [ts=" + ts + ", ver=" + ver + ", wadh=" + wadh + ", demo=" + demo
				+ ", bios=" + bios + ", pv=" + pv + ", encPidBlock="
				+ encPidBlock + "]";
	}
	
}
