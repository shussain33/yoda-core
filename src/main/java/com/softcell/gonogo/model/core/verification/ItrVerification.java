package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by yogesh on 8/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItrVerification extends ThirdPartyVerification{

    @JsonProperty("sPanNumber")
    private String panNumber;

    @JsonProperty("sAssessmentYr")
    private String assessmentYear;

    @JsonProperty("sAckNumber")
    private String acknowledgementNumber;

    @JsonProperty("dtItrFillingDate")
    private Date itrFillingDate;

    @JsonProperty("sLocation")
    private String location;

    //These are additional fields in output along with above fields excluding document.
    @JsonProperty("dTaxPaidAmount")
    private double taxPaidAmount;

    @JsonProperty("sTotalIncomeOrGTI")
    private String totalIncomeOrGTI;

    @JsonProperty("dTotalIncomeOrGTIAmount")
    private double totalIncomeOrGTIAmount;

    @JsonProperty("aVendorRemarks")
    private List<Remark> vendorRemarks;

    @JsonProperty("sFinalStatus")
    private String finalStatus;

    @JsonProperty("sMifinItrId")
    private String mifinItrId;

}

