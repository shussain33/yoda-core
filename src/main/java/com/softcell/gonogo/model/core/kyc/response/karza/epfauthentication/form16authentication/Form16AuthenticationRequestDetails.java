package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.form16authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Form16AuthenticationRequestDetails {

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("tan")
    private String tan;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("cert_no")
    private String cert_no;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("fiscal_year")
    private String fiscal_year;

}