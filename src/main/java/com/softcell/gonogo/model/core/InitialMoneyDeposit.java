package com.softcell.gonogo.model.core;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author suhasinis
 *
 */

public class InitialMoneyDeposit {

	@JsonProperty("sBankName")
	private String bankName;

	@JsonProperty("sMifinBankID")
	private String mifinBankCode;

	@JsonProperty("sBankBranch")
	private String bankBranch;

	@JsonProperty("sInstrumentNumber")
	private String instrumentNumber;

	// Cheque / DD
	@JsonProperty("sInstrumentType")
	private String instrumentType;

	@JsonProperty("dtInstrumentDate")
	private Date instrumentDate;

	@JsonProperty("dAmount")
	private double amount;

	@JsonProperty("dtDepositDate")
	private Date depositDate;

	@JsonProperty("dtClearingDate")
	private Date clearingDate;

	@JsonProperty("sStatus")
	private String status;

	@JsonProperty("dtReportingDate")
	private Date reportingDate;

	@JsonProperty("bIsCleared")
	private boolean isCleared;

	// SBFC waiver option imd screen
	@JsonProperty("bWaiverState")
	private boolean waiverState;

	@JsonProperty("bIsBounce")
	private boolean isBounce;

	public boolean isBounce() {
		return isBounce;
	}

	public void setBounce(boolean bounce) {
		isBounce = bounce;
	}
	public String getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getInstrumentNumber() {
		return instrumentNumber;
	}

	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}

	public Date getInstrumentDate() {
		return instrumentDate;
	}

	public void setInstrumentDate(Date instrumentDate) {
		this.instrumentDate = instrumentDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	public Date getClearingDate() {
		return clearingDate;
	}

	public void setClearingDate(Date clearingDate) {
		this.clearingDate = clearingDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getReportingDate() {
		return reportingDate;
	}

	public void setReportingDate(Date reportingDate) {
		this.reportingDate = reportingDate;
	}

	public boolean isCleared() {
		return isCleared;
	}

	public void setCleared(boolean cleared) {
		isCleared = cleared;
	}

	public boolean isWaiverState() {
		return waiverState;
	}

	public void setWaiverState(boolean waiverState) {
		this.waiverState = waiverState;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IMDChequeDetails [bankName=");
		builder.append(bankName);
		builder.append(", bankBranch=");
		builder.append(bankBranch);
		builder.append(", instrumentNumber=");
		builder.append(instrumentNumber);
		builder.append(", instrumentDate=");
		builder.append(instrumentDate);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", depositDate=");
		builder.append(depositDate);
		builder.append(", clearingDate=");
		builder.append(clearingDate);
		builder.append(", status=");
		builder.append(status);
		builder.append(", reportingDate=");
		builder.append(reportingDate);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		result = prime * result + ((bankBranch == null) ? 0 : bankBranch.hashCode());
		result = prime * result + ((bankName == null) ? 0 : bankName.hashCode());
		result = prime * result + ((instrumentNumber == null) ? 0 : instrumentNumber.hashCode());
		result = prime * result + ((instrumentDate == null) ? 0 : instrumentDate.hashCode());
		temp = Double.doubleToLongBits(amount);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((reportingDate == null) ? 0 : reportingDate.hashCode());
		result = prime * result + ((depositDate == null) ? 0 : depositDate.hashCode());
		result = prime * result + ((clearingDate == null) ? 0 : clearingDate.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InitialMoneyDeposit other = (InitialMoneyDeposit) obj;
		if (bankBranch == null) {
			if (other.bankBranch != null)
				return false;
		} else if (!bankBranch.equals(other.bankBranch))
			return false;
		if (bankName == null) {
			if (other.bankName != null)
				return false;
		} else if (!bankName.equals(other.bankName))
			return false;
		if (instrumentDate == null) {
			if (other.instrumentDate != null)
				return false;
		} else if (!instrumentDate.equals(other.instrumentDate))
			return false;
		if (instrumentNumber == null) {
			if (other.instrumentNumber != null)
				return false;
		} else if (!instrumentNumber.equals(other.instrumentNumber))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (reportingDate == null) {
			if (other.reportingDate != null)
				return false;
		} else if (!reportingDate.equals(other.reportingDate))
			return false;
		if (depositDate == null) {
			if (other.depositDate != null)
				return false;
		} else if (!depositDate.equals(other.depositDate))
			return false;
		if (clearingDate == null) {
			if (other.clearingDate != null)
				return false;
		} else if (!clearingDate.equals(other.clearingDate))
			return false;
		if (Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		return true;
	}
}
