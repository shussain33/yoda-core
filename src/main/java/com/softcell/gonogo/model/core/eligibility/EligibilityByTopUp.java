package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** 
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityByTopUp {

    @JsonProperty("sLanNo")
    private String lanNo;

    @JsonProperty("dLatestSanctionAmount")
    private double latestSanctionAmount;

    @JsonProperty("dLiveOutstanding")
    private double liveOutStanding;

    @JsonProperty("dAmountConsider")
    private double amountConsider;

    @JsonProperty("dAddtnalAmount")
    private double additionalAmount;

    @JsonProperty("sLoanType")
    private String  loanType;

    @JsonProperty("iTotalTenor")
    private int totalTenor;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("iBalanceTenor")
    private int balanceTenor;

    @JsonProperty("dPrincplOutstanding")
    private double principleOutstanding;

    @JsonProperty("iMob")
    private int Mob;

    @JsonProperty("dApplicablePrcnt")
    private double applicablePercent;

    @JsonProperty("dAppliedPrcnt")
    private double appliedPercent;

    @JsonProperty("dRoi")
    private double roi;

    @JsonProperty("dEmi")
    private double emi;

}
