package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.List;

/**
 * Created by yogesh on 9/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BankingStatement {
    @JsonProperty("oApplicantName")
    private Name applicantName;

    @JsonProperty("sApplicantId")
    private String applicantId;

    @JsonProperty("oOthApplicantName")
    private Name otherApplicant;

    @JsonProperty("sAccountHolder")
    private String accountHolder;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sMifinBankID")
    private String mifinBankCode;

    @JsonProperty("sAccountNumber")
    private String accountNumber;

    @JsonProperty("sAccountType")
    private String accountType;

    @JsonProperty("aBankTransactions")
    private List<BankingTransaction> bankingTransactions;

    @JsonProperty("dNonBusinessCreditTransctionsAmt")
    private double nonBusinessCreditTransctionsAmt;

    @JsonProperty("dNonBusinessDebitTransctionsAmt")
    private double nonBusinessDebitTransctionsAmt;

    @JsonProperty("dNetBusinessCreditTransctionsAmt")
    private double netBusinessCreditTransctionsAmt;

    @JsonProperty("dNetBusinessDebitTransctionsAmt")
    private double netBusinessDebitTransctionsAmt;

    @JsonProperty("dAverageAbb")
    private double averageAbb;

    @JsonProperty("dAverageCreditSum")
    private double averageCreditSum;

    @JsonProperty("iTotalNoOfInwardTransaction")
    private int totalNoOfInwardTransaction;

    @JsonProperty("iTotalNoOfOutwardTransaction")
    private int totalNoOfOutwardTransaction;

    @JsonProperty("sBankingRemarks")
    private String bankingRemarks;

    @JsonProperty("dPercentOfInwardReturns")
    private double percentOfInwardReturns;

    @JsonProperty("dPercentOfOutwardReturns")
    private double percentOfOutwardReturns;

    @JsonProperty("dTotalCashCredit")
    private double totalCashCredit;

    @JsonProperty("dTotalCredit")
    private double totalCredit;

    @JsonProperty("dTotalGST")
    private double totalGST;

    @JsonProperty("dTotalIntColl")
    private double totalIntColl;

    @JsonProperty("dTotalOtherCredit")
    private double totalOtherCredit;

    @JsonProperty("dTotalCCUtilisation")
    private double totalCCUtilisation;

    @JsonProperty("iCcodLimit")
    private int ccodLimit;

    @JsonProperty("bIsConsolidated")
    private boolean consolidated;

    @JsonProperty("dTotalIW")
    private double totalIW;

    @JsonProperty("dTotalOW")
    private double totalOW;

    @JsonProperty("sIFSC")
    private String ifscCode;

    @JsonProperty("bRepaybank")
    private boolean repayBank;

    @JsonProperty("sMifinBankingId")
    private String mifinBankingId;

    public static final Comparator<BankingStatement> AppIdComparator = new Comparator<BankingStatement>(){

        @Override
        public int compare(BankingStatement o1, BankingStatement o2) {
            return (int) (Integer.parseInt(o1.getApplicantId())-Integer.parseInt(o2.getApplicantId()));
        }

    };
}

