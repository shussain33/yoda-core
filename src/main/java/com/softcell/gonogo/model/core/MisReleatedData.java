package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 29/5/19.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
public class MisReleatedData {

    @JsonProperty("oPropertyFile")
    private PropertyFileData propertyFileData;

    @JsonProperty("oLoanAgreement")
    private  LoanAgreement loanAgreement;

    @JsonProperty("sRefIdWithAppend")
    private String  refId;
}

