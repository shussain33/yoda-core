package com.softcell.gonogo.model.core.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.softcell.gonogo.model.response.GenericResponse;

import java.util.List;

/**
 * This model holds following property details of master upload operation.
 *
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileUploadStatus extends GenericResponse {
    private long numberOfRecords;
    private long numberOfRecordsFailed;
    private long numberOfRecordSuccess;
    private long numberOfRecordsDbAddOrUpdate;
    private long numberOfRecordsDbRemoved;
    private long numberOfRecordsDbFailed;
    private String errorDesc;
    private String additionalFields;
    private List<ErrorDescription> recordErrors;

    /**
     * @return the numberOfRecords
     */
    public long getNumberOfRecords() {
        return numberOfRecords;
    }

    /**
     * @param numberOfRecords the numberOfRecords to set
     */
    public void setNumberOfRecords(long numberOfRecords) {
        this.numberOfRecords = numberOfRecords;
    }

    /**
     * @return the numberOfRecordsFailed
     */
    public long getNumberOfRecordsFailed() {
        return numberOfRecordsFailed;
    }

    /**
     * @param numberOfRecordsFailed the numberOfRecordsFailed to set
     */
    public void setNumberOfRecordsFailed(long numberOfRecordsFailed) {
        this.numberOfRecordsFailed = numberOfRecordsFailed;
    }

    /**
     * @return the numberOfRecordSuccess
     */
    public long getNumberOfRecordSuccess() {
        return numberOfRecordSuccess;
    }

    /**
     * @param numberOfRecordSuccess the numberOfRecordSuccess to set
     */
    public void setNumberOfRecordSuccess(long numberOfRecordSuccess) {
        this.numberOfRecordSuccess = numberOfRecordSuccess;
    }

    /**
     * @return the errorDesc
     */
    public String getErrorDesc() {
        return errorDesc;
    }

    /**
     * @param errorDesc the errorDesc to set
     */
    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc;
    }

    /**
     * @return the recordErrors
     */
    public List<ErrorDescription> getRecordErrors() {
        return recordErrors;
    }

    /**
     * @param recordErrors the recordErrors to set
     */
    public void setRecordErrors(List<ErrorDescription> recordErrors) {
        this.recordErrors = recordErrors;
    }

    public long getNumberOfRecordsDbAddOrUpdate() {
        return numberOfRecordsDbAddOrUpdate;
    }

    public void setNumberOfRecordsDbAddOrUpdate(long numberOfRecordsDbAddOrUpdate) {
        this.numberOfRecordsDbAddOrUpdate = numberOfRecordsDbAddOrUpdate;
    }

    public long getNumberOfRecordsDbRemoved() {
        return numberOfRecordsDbRemoved;
    }

    public void setNumberOfRecordsDbRemoved(long numberOfRecordsDbRemoved) {
        this.numberOfRecordsDbRemoved = numberOfRecordsDbRemoved;
    }

    public long getNumberOfRecordsDbFailed() {
        return numberOfRecordsDbFailed;
    }

    public void setNumberOfRecordsDbFailed(long numberOfRecordsDbFailed) {
        this.numberOfRecordsDbFailed = numberOfRecordsDbFailed;
    }



    public String getAdditionalFields() {
        return additionalFields;
    }

    public void setAdditionalFields(String additionalFields) {
        this.additionalFields = additionalFields;
    }



    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FileUploadStatus{");
        sb.append("numberOfRecords=").append(numberOfRecords);
        sb.append(", numberOfRecordsFailed=").append(numberOfRecordsFailed);
        sb.append(", numberOfRecordSuccess=").append(numberOfRecordSuccess);
        sb.append(", numberOfRecordsDbAddOrUpdate=").append(numberOfRecordsDbAddOrUpdate);
        sb.append(", numberOfRecordsDbRemoved=").append(numberOfRecordsDbRemoved);
        sb.append(", numberOfRecordsDbFailed=").append(numberOfRecordsDbFailed);
        sb.append(", errorDesc='").append(errorDesc).append('\'');
        sb.append(", additionalFields='").append(additionalFields).append('\'');
        sb.append(", recordErrors=").append(recordErrors);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        FileUploadStatus that = (FileUploadStatus) o;

        if (getNumberOfRecords() != that.getNumberOfRecords()) return false;
        if (getNumberOfRecordsFailed() != that.getNumberOfRecordsFailed()) return false;
        if (getNumberOfRecordSuccess() != that.getNumberOfRecordSuccess()) return false;
        if (getNumberOfRecordsDbAddOrUpdate() != that.getNumberOfRecordsDbAddOrUpdate()) return false;
        if (getNumberOfRecordsDbRemoved() != that.getNumberOfRecordsDbRemoved()) return false;
        if (getNumberOfRecordsDbFailed() != that.getNumberOfRecordsDbFailed()) return false;
        if (getErrorDesc() != null ? !getErrorDesc().equals(that.getErrorDesc()) : that.getErrorDesc() != null)
            return false;
        if (getAdditionalFields() != null ? !getAdditionalFields().equals(that.getAdditionalFields()) : that.getAdditionalFields() != null)
            return false;
        return getRecordErrors() != null ? getRecordErrors().equals(that.getRecordErrors()) : that.getRecordErrors() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (getNumberOfRecords() ^ (getNumberOfRecords() >>> 32));
        result = 31 * result + (int) (getNumberOfRecordsFailed() ^ (getNumberOfRecordsFailed() >>> 32));
        result = 31 * result + (int) (getNumberOfRecordSuccess() ^ (getNumberOfRecordSuccess() >>> 32));
        result = 31 * result + (int) (getNumberOfRecordsDbAddOrUpdate() ^ (getNumberOfRecordsDbAddOrUpdate() >>> 32));
        result = 31 * result + (int) (getNumberOfRecordsDbRemoved() ^ (getNumberOfRecordsDbRemoved() >>> 32));
        result = 31 * result + (int) (getNumberOfRecordsDbFailed() ^ (getNumberOfRecordsDbFailed() >>> 32));
        result = 31 * result + (getErrorDesc() != null ? getErrorDesc().hashCode() : 0);
        result = 31 * result + (getAdditionalFields() != null ? getAdditionalFields().hashCode() : 0);
        result = 31 * result + (getRecordErrors() != null ? getRecordErrors().hashCode() : 0);
        return result;
    }
}
