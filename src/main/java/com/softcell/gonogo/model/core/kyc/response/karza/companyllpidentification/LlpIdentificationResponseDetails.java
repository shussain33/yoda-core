package com.softcell.gonogo.model.core.kyc.response.karza.companyllpidentification;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LlpIdentificationResponseDetails {

    @JsonProperty("Total_Obligation_of_Contribution")
    private String Total_Obligation_of_Contribution;

    @JsonProperty("Previous_firm")
    private String Previous_firm;

    @JsonProperty("ROC_Code")
    private String ROC_Code;

    @JsonProperty("Number_of_Partners")
    private String Number_of_Partners;

    @JsonProperty("Email_Id")
    private String Email_Id;

    @JsonProperty("Annual_Return_filed")
    private String Annual_Return_filed;

    @JsonProperty("Solvency_filed")
    private String Solvency_filed;

    @JsonProperty("LLP_Status")
    private String LLP_Status;

    @JsonProperty("Number_of_Designated_Partners")
    private String Number_of_Designated_Partners;

    @JsonProperty("Date_of_Incorporation")
    private String Date_of_Incorporation;

    @JsonProperty("Main_division")
    private String Main_division;

    @JsonProperty("LLPIN")
    private String LLPIN;

    @JsonProperty("LLP_Name")
    private String LLP_Name;

    @JsonProperty("Registered_Address")
    private String Registered_Address;

    @JsonProperty("Description_of_main_division")
    private String Description_of_main_division;

}