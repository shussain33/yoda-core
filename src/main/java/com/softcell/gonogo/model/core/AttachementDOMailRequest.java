package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.scoring.response.Header;

import java.util.Arrays;


public class AttachementDOMailRequest {

    @JsonIgnore
    private Header header;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sRcpt")
    private String recipient;

    @JsonProperty("aRcpt")
    private String[] recipients;

    @JsonProperty("sCC")
    private String[] mailToCC;

    @JsonProperty("sText")
    private String content;

    @JsonProperty("sSub")
    private String subject;

    @JsonProperty("sAtch")
    private byte[] attachedDoPdf;

    @JsonProperty("sContentType")
    private String contentType;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String[] getRecipients() {
        return recipients;
    }

    public void setRecipients(String[] recipients) {
        this.recipients = recipients;
    }

    public String[] getMailToCC() {
        return mailToCC;
    }

    public void setMailToCC(String[] mailToCC) {
        this.mailToCC = mailToCC;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public byte[] getAttachedDoPdf() {
        return attachedDoPdf;
    }

    public void setAttachedDoPdf(byte[] attachedDoPdf) {
        this.attachedDoPdf = attachedDoPdf;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String toString() {
        return "AttachementDOMailRequest [header=" + header + ", refID="
                + refID + ", recipient=" + recipient + ", recipients="
                + Arrays.toString(recipients) + ", mailToCC="
                + Arrays.toString(mailToCC) + ", content=" + content
                + ", subject=" + subject + ", attachedDoPdf="
                + Arrays.toString(attachedDoPdf) + ", contentType="
                + contentType + "]";
    }
}
