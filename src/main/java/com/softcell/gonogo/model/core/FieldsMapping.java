package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FieldsMapping {

    // here map's key is Excel Column name and value of map is db mapping of that field in particular Collection
    @JsonProperty("aFieldMap")
    private Map<String, String> fields;

    @JsonProperty("sClassName")
    private String className;



}
