package com.softcell.gonogo.model.core.request.scoring;

/**
 * @author yogeshb
 */
public class CibilRespAccount {

    private String reportingMemberShortName = null;

    private String accountNumber = null;

    private String accountType = null;

    private String ownershipIndicator = null;

    private String dateOpenedOrDisbursed = null;

    private String dateOfLastPayment = null;

    private String dateClosed = null;

    private String dateReportedAndCertified = null;

    private String highCreditOrSanctionedAmount = null;

    private String currentBalance = null;

    private String overdueAmount = null;

    private String paymentHistory1 = null;

    private String paymentHistory2 = null;

    private String paymentHistoryStartDate = null;

    private String paymentHistoryEndDate = null;

    private String suitFiledOrWilfulDefault = null;

    private String writtenOffAndSettledStatus = null;

    private String valueOfCollateral = null;

    private String typeOfCollateral = null;

    private String creditLimit = null;

    private String cashLimit = null;

    private String rateOfInterest = null;

    private String repaymentTenure = null;

    private String emiAmount = null;

    private String writtenOffAmountTotal = null;

    private String writtenOffAmountPrincipal = null;

    private String settlementAmount = null;

    private String paymentFrequence = null;

    private String actualPaymentAmount = null;

    private String dateOfEntryForErrorCode = null;

    private String errorCode = null;

    private String dateOfEntryForCibilRemarksCode = null;

    private String cibilRemarkCode = null;

    private String dateOfEntryForErrorOrDisputeRemarksCode = null;

    private String errorOrDisputeRemarksCode1 = null;

    private String errorOrDisputeRemarksCode2 = null;

    public String getReportingMemberShortName() {
        return reportingMemberShortName;
    }

    public void setReportingMemberShortName(String reportingMemberShortName) {
        this.reportingMemberShortName = reportingMemberShortName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getOwnershipIndicator() {
        return ownershipIndicator;
    }

    public void setOwnershipIndicator(String ownershipIndicator) {
        this.ownershipIndicator = ownershipIndicator;
    }

    public String getDateOpenedOrDisbursed() {
        return dateOpenedOrDisbursed;
    }

    public void setDateOpenedOrDisbursed(String dateOpenedOrDisbursed) {
        this.dateOpenedOrDisbursed = dateOpenedOrDisbursed;
    }

    public String getDateOfLastPayment() {
        return dateOfLastPayment;
    }

    public void setDateOfLastPayment(String dateOfLastPayment) {
        this.dateOfLastPayment = dateOfLastPayment;
    }

    public String getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(String dateClosed) {
        this.dateClosed = dateClosed;
    }

    public String getDateReportedAndCertified() {
        return dateReportedAndCertified;
    }

    public void setDateReportedAndCertified(String dateReportedAndCertified) {
        this.dateReportedAndCertified = dateReportedAndCertified;
    }

    public String getHighCreditOrSanctionedAmount() {
        return highCreditOrSanctionedAmount;
    }

    public void setHighCreditOrSanctionedAmount(String highCreditOrSanctionedAmount) {
        this.highCreditOrSanctionedAmount = highCreditOrSanctionedAmount;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getOverdueAmount() {
        return overdueAmount;
    }

    public void setOverdueAmount(String overdueAmmount) {
        this.overdueAmount = overdueAmmount;
    }

    public String getPaymentHistory1() {
        return paymentHistory1;
    }

    public void setPaymentHistory1(String paymentHistory1) {
        this.paymentHistory1 = paymentHistory1;
    }

    public String getPaymentHistory2() {
        return paymentHistory2;
    }

    public void setPaymentHistory2(String paymentHistory2) {
        this.paymentHistory2 = paymentHistory2;
    }

    public String getPaymentHistoryStartDate() {
        return paymentHistoryStartDate;
    }

    public void setPaymentHistoryStartDate(String paymentHistoryStartDate) {
        this.paymentHistoryStartDate = paymentHistoryStartDate;
    }

    public String getPaymentHistoryEndDate() {
        return paymentHistoryEndDate;
    }

    public void setPaymentHistoryEndDate(String paymentHistoryEndDate) {
        this.paymentHistoryEndDate = paymentHistoryEndDate;
    }

    public String getSuitFiledOrWilfulDefault() {
        return suitFiledOrWilfulDefault;
    }

    public void setSuitFiledOrWilfulDefault(String suitFiledOrWilfulDefault) {
        this.suitFiledOrWilfulDefault = suitFiledOrWilfulDefault;
    }

    public String getWrittenOffAndSettledStatus() {
        return writtenOffAndSettledStatus;
    }

    public void setWrittenOffAndSettledStatus(String writtenOffAndSettledStatus) {
        this.writtenOffAndSettledStatus = writtenOffAndSettledStatus;
    }

    public String getValueOfCollateral() {
        return valueOfCollateral;
    }

    public void setValueOfCollateral(String valueOfCollateral) {
        this.valueOfCollateral = valueOfCollateral;
    }

    public String getTypeOfCollateral() {
        return typeOfCollateral;
    }

    public void setTypeOfCollateral(String typeOfCollateral) {
        this.typeOfCollateral = typeOfCollateral;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCashLimit() {
        return cashLimit;
    }

    public void setCashLimit(String cashLimit) {
        this.cashLimit = cashLimit;
    }

    public String getRateOfInterest() {
        return rateOfInterest;
    }

    public void setRateOfInterest(String rateOfInterest) {
        this.rateOfInterest = rateOfInterest;
    }

    public String getRepaymentTenure() {
        return repaymentTenure;
    }

    public void setRepaymentTenure(String repaymentTenure) {
        this.repaymentTenure = repaymentTenure;
    }

    public String getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(String emiAmount) {
        this.emiAmount = emiAmount;
    }

    public String getWrittenOffAmountTotal() {
        return writtenOffAmountTotal;
    }

    public void setWrittenOffAmountTotal(String writtenOffAmountTotal) {
        this.writtenOffAmountTotal = writtenOffAmountTotal;
    }

    public String getWrittenOffAmountPrincipal() {
        return writtenOffAmountPrincipal;
    }

    public void setWrittenOffAmountPrincipal(String writtenOffAmountPrincipal) {
        this.writtenOffAmountPrincipal = writtenOffAmountPrincipal;
    }

    public String getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(String settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getPaymentFrequence() {
        return paymentFrequence;
    }

    public void setPaymentFrequence(String paymentFrequence) {
        this.paymentFrequence = paymentFrequence;
    }

    public String getActualPaymentAmount() {
        return actualPaymentAmount;
    }

    public void setActualPaymentAmount(String actualPaymentAmount) {
        this.actualPaymentAmount = actualPaymentAmount;
    }

    public String getDateOfEntryForErrorCode() {
        return dateOfEntryForErrorCode;
    }

    public void setDateOfEntryForErrorCode(String dateOfEntryForErrorCode) {
        this.dateOfEntryForErrorCode = dateOfEntryForErrorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDateOfEntryForCibilRemarksCode() {
        return dateOfEntryForCibilRemarksCode;
    }

    public void setDateOfEntryForCibilRemarksCode(
            String dateOfEntryForCibilRemarksCode) {
        this.dateOfEntryForCibilRemarksCode = dateOfEntryForCibilRemarksCode;
    }

    public String getCibilRemarkCode() {
        return cibilRemarkCode;
    }

    public void setCibilRemarkCode(String cibilRemarkCode) {
        this.cibilRemarkCode = cibilRemarkCode;
    }

    public String getDateOfEntryForErrorOrDisputeRemarksCode() {
        return dateOfEntryForErrorOrDisputeRemarksCode;
    }

    public void setDateOfEntryForErrorOrDisputeRemarksCode(
            String dateOfEntryForErrorOrDisputeRemarksCode) {
        this.dateOfEntryForErrorOrDisputeRemarksCode = dateOfEntryForErrorOrDisputeRemarksCode;
    }

    public String getErrorOrDisputeRemarksCode1() {
        return errorOrDisputeRemarksCode1;
    }

    public void setErrorOrDisputeRemarksCode1(String errorOrDisputeRemarksCode1) {
        this.errorOrDisputeRemarksCode1 = errorOrDisputeRemarksCode1;
    }

    public String getErrorOrDisputeRemarksCode2() {
        return errorOrDisputeRemarksCode2;
    }

    public void setErrorOrDisputeRemarksCode2(String errorOrDisputeRemarksCode2) {
        this.errorOrDisputeRemarksCode2 = errorOrDisputeRemarksCode2;
    }

    @Override
    public String toString() {
        return "CibilRespAccount [reportingMemberShortName="
                + reportingMemberShortName + ", accountNumber=" + accountNumber
                + ", accountType=" + accountType + ", ownershipIndicator="
                + ownershipIndicator + ", dateOpenedOrDisbursed="
                + dateOpenedOrDisbursed + ", dateOfLastPayment="
                + dateOfLastPayment + ", dateClosed=" + dateClosed
                + ", dateReportedAndCertified=" + dateReportedAndCertified
                + ", highCreditOrSanctionedAmount="
                + highCreditOrSanctionedAmount + ", currentBalance="
                + currentBalance + ", overdueAmount=" + overdueAmount
                + ", paymentHistory1=" + paymentHistory1 + ", paymentHistory2="
                + paymentHistory2 + ", paymentHistoryStartDate="
                + paymentHistoryStartDate + ", paymentHistoryEndDate="
                + paymentHistoryEndDate + ", suitFiledOrWilfulDefault="
                + suitFiledOrWilfulDefault + ", writtenOffAndSettledStatus="
                + writtenOffAndSettledStatus + ", valueOfCollateral="
                + valueOfCollateral + ", typeOfCollateral=" + typeOfCollateral
                + ", creditLimit=" + creditLimit + ", cashLimit=" + cashLimit
                + ", rateOfInterest=" + rateOfInterest + ", repaymentTenure="
                + repaymentTenure + ", emiAmount=" + emiAmount
                + ", writtenOffAmountTotal=" + writtenOffAmountTotal
                + ", writtenOffAmountPrincipal=" + writtenOffAmountPrincipal
                + ", settlementAmount=" + settlementAmount
                + ", paymentFrequence=" + paymentFrequence
                + ", actualPaymentAmount=" + actualPaymentAmount
                + ", dateOfEntryForErrorCode=" + dateOfEntryForErrorCode
                + ", errorCode=" + errorCode
                + ", dateOfEntryForCibilRemarksCode="
                + dateOfEntryForCibilRemarksCode + ", cibilRemarkCode="
                + cibilRemarkCode
                + ", dateOfEntryForErrorOrDisputeRemarksCode="
                + dateOfEntryForErrorOrDisputeRemarksCode
                + ", errorOrDisputeRemarksCode1=" + errorOrDisputeRemarksCode1
                + ", errorOrDisputeRemarksCode2=" + errorOrDisputeRemarksCode2
                + "]";
    }


}
