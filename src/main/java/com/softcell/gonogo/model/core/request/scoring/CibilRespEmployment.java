package com.softcell.gonogo.model.core.request.scoring;

/**
 * @author yogeshb
 */
public class CibilRespEmployment {

    private String accountType;

    private String dateReported;

    private String occupationCode;

    private String income;

    private String netGrossIndicator;

    private String monthlyAnnuallyIndicator;

    private String dateOfEntryForErrorCode;

    private String errorCode;

    private String dateOfEntryForCibilRemarksCode;

    private String cibilRemarkCode;

    private String dateOfEntryForErrorOrDisputeRemarksCode;

    private String errorOrDisputeRemarksCode1;

    private String errorOrDisputeRemarksCode2;

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getDateReported() {
        return dateReported;
    }

    public void setDateReported(String dateReported) {
        this.dateReported = dateReported;
    }

    public String getOccupationCode() {
        return occupationCode;
    }

    public void setOccupationCode(String occupationCode) {
        this.occupationCode = occupationCode;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getNetGrossIndicator() {
        return netGrossIndicator;
    }

    public void setNetGrossIndicator(String netGrossIndicator) {
        this.netGrossIndicator = netGrossIndicator;
    }

    public String getMonthlyAnnuallyIndicator() {
        return monthlyAnnuallyIndicator;
    }

    public void setMonthlyAnnuallyIndicator(String monthlyAnnuallyIndicator) {
        this.monthlyAnnuallyIndicator = monthlyAnnuallyIndicator;
    }

    public String getDateOfEntryForErrorCode() {
        return dateOfEntryForErrorCode;
    }

    public void setDateOfEntryForErrorCode(String dateOfEntryForErrorCode) {
        this.dateOfEntryForErrorCode = dateOfEntryForErrorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDateOfEntryForCibilRemarksCode() {
        return dateOfEntryForCibilRemarksCode;
    }

    public void setDateOfEntryForCibilRemarksCode(
            String dateOfEntryForCibilRemarksCode) {
        this.dateOfEntryForCibilRemarksCode = dateOfEntryForCibilRemarksCode;
    }

    public String getCibilRemarkCode() {
        return cibilRemarkCode;
    }

    public void setCibilRemarkCode(String cibilRemarkCode) {
        this.cibilRemarkCode = cibilRemarkCode;
    }

    public String getDateOfEntryForErrorOrDisputeRemarksCode() {
        return dateOfEntryForErrorOrDisputeRemarksCode;
    }

    public void setDateOfEntryForErrorOrDisputeRemarksCode(
            String dateOfEntryForErrorOrDisputeRemarksCode) {
        this.dateOfEntryForErrorOrDisputeRemarksCode = dateOfEntryForErrorOrDisputeRemarksCode;
    }

    public String getErrorOrDisputeRemarksCode1() {
        return errorOrDisputeRemarksCode1;
    }

    public void setErrorOrDisputeRemarksCode1(String errorOrDisputeRemarksCode1) {
        this.errorOrDisputeRemarksCode1 = errorOrDisputeRemarksCode1;
    }

    public String getErrorOrDisputeRemarksCode2() {
        return errorOrDisputeRemarksCode2;
    }

    public void setErrorOrDisputeRemarksCode2(String errorOrDisputeRemarksCode2) {
        this.errorOrDisputeRemarksCode2 = errorOrDisputeRemarksCode2;
    }

    @Override
    public String toString() {
        return "CibilRespEmployment [accountType=" + accountType
                + ", dateReported=" + dateReported + ", occupationCode="
                + occupationCode + ", income=" + income
                + ", netGrossIndicator=" + netGrossIndicator
                + ", monthlyAnnuallyIndicator=" + monthlyAnnuallyIndicator
                + ", dateOfEntryForErrorCode=" + dateOfEntryForErrorCode
                + ", errorCode=" + errorCode
                + ", dateOfEntryForCibilRemarksCode="
                + dateOfEntryForCibilRemarksCode + ", cibilRemarkCode="
                + cibilRemarkCode
                + ", dateOfEntryForErrorOrDisputeRemarksCode="
                + dateOfEntryForErrorOrDisputeRemarksCode
                + ", errorOrDisputeRemarksCode1=" + errorOrDisputeRemarksCode1
                + ", errorOrDisputeRemarksCode2=" + errorOrDisputeRemarksCode2
                + "]";
    }
}
