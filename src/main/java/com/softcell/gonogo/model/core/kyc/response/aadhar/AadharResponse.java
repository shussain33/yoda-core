package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class AadharResponse {

    @JsonProperty("result")
    private String result;

    @JsonProperty("code")
    private String code;

    @JsonProperty("transaction-identifier")
    private String transactionIdentifier;

    @JsonProperty("ts")
    private String ts;

    @JsonProperty("info")
    private String info;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "AadharResponse [result=" + result + ", code=" + code
                + ", transactionIdentifier=" + transactionIdentifier + ", ts="
                + ts + ", info=" + info + "]";
    }
}
