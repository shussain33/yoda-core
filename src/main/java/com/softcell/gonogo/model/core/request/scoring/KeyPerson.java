package com.softcell.gonogo.model.core.request.scoring;


public class KeyPerson {
    private RelationInfo relationInfoType;

    public RelationInfo getRelationInfoType() {
        return relationInfoType;
    }

    public void setRelationInfoType(RelationInfo relationInfoType) {
        this.relationInfoType = relationInfoType;
    }

    @Override
    public String toString() {
        return "KeyPerson [relationInfoType=" + relationInfoType + "]";
    }
}
