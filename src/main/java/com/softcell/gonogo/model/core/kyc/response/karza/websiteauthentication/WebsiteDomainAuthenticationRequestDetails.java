package com.softcell.gonogo.model.core.kyc.response.karza.websiteauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WebsiteDomainAuthenticationRequestDetails {

    @JsonProperty("domain")
    private String domain;

}