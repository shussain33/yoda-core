package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailWhoIsInfo {

    @JsonProperty("expired")
    private String expired;

    @JsonProperty("age_year")
    private double age_year;

    @JsonProperty("ageYear")
    private double ageYear;

    @JsonProperty("creation_date")
    private String creation_date;

    @JsonProperty("creationDate")
    private String creationDate;

    @JsonProperty("expiration_date")
    private String expiration_date;

    @JsonProperty("expirationDate")
    private String expirationDate;

    @JsonProperty("update_date")
    private String update_date;

    @JsonProperty("updateDate")
    private String updateDate;

    @JsonProperty("whois_email_domain_match")
    private List<EmailWhoIsDomainMatch> whois_email_domain_match;

    @JsonProperty("whoisEmailDomainMatch")
    private List<EmailWhoIsDomainMatch> whoisEmailDomainMatch;

    @JsonProperty("whois_email_match")
    private List<EmailWhoIsMatch> whois_email_match;

    @JsonProperty("whoisEmailMatch")
    private List<EmailWhoIsMatch> whoisEmailMatch;

    @JsonProperty("whois_indv_name")
    private List<EmailWhoIsIndvName> whois_indv_name;

    @JsonProperty("whoisIndvName")
    private List<EmailWhoIsIndvName> whoisIndvName;

    @JsonProperty("whois_org_name")
    private List<EmailWhoIsOrgName> whois_org_name;

    @JsonProperty("whoisOrgName")
    private List<EmailWhoIsOrgName> whoisOrgName;

}
