package com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "dob",
        "phone",
        "email",
        "gender",
        "address-format",
        "address-freetext"
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AadharDemographics {

    @JsonProperty("name")
    public AadharName name;

    @JsonProperty("dob")
    public AadharDob dob;

    @JsonProperty("phone")
    public String phone;

    @JsonProperty("email")
    public String email;

    @JsonProperty("gender")
    public String gender;

    @JsonProperty("address-format")
    public String addressFormat;

    @JsonProperty("address-freetext")
    public AadharAddressFreeText addressFreetext;

}