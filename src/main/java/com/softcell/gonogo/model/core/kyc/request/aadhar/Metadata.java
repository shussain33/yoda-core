package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Metadata {
    @JsonProperty("FINGERPRINT-DEVICE-CODE")
    private String fingerPrintDeviceCode;

    @JsonProperty("IRIS-DEVICE-CODE")
    private String irisDeviceCode;

    @JsonProperty("UNIQUE-HOST/TERMINAL-DEVICE-CODE")
    private String uniqueHostOrTerminalDeviceCode;

    @JsonProperty("PUBLIC-IP-ADDRESS")
    private String publicIpAddress;

    @JsonProperty("LOCATION-TYPE")
    private String locationType;

    @JsonProperty("LOCATION-VALUE")
    private String locationValue;

    @JsonProperty(value="REG-DEVICE-CODE")
    String dpId;

    @JsonProperty(value="RDS-ID")
    String rdsId;

    @JsonProperty(value="RDS-VERSION")
    String rdsVer;

    @JsonProperty(value="DEVICE-CODE")
    String dc;

    @JsonProperty(value="DEVICE-MODEL-ID")
    String mi;

    @JsonProperty(value="DEVICE-PUBLIC-KEY")
    String mc;

    public static Builder builder() {
        return new Builder();
    }

    public String getFingerPrintDeviceCode() {
        return fingerPrintDeviceCode;
    }

    public void setFingerPrintDeviceCode(String fingerPrintDeviceCode) {
        this.fingerPrintDeviceCode = fingerPrintDeviceCode;
    }

    public String getIrisDeviceCode() {
        return irisDeviceCode;
    }

    public void setIrisDeviceCode(String irisDeviceCode) {
        this.irisDeviceCode = irisDeviceCode;
    }

    public String getUniqueHostOrTerminalDeviceCode() {
        return uniqueHostOrTerminalDeviceCode;
    }

    public void setUniqueHostOrTerminalDeviceCode(
            String uniqueHostOrTerminalDeviceCode) {
        this.uniqueHostOrTerminalDeviceCode = uniqueHostOrTerminalDeviceCode;
    }

    public String getPublicIpAddress() {
        return publicIpAddress;
    }

    public void setPublicIpAddress(String publicIpAddress) {
        this.publicIpAddress = publicIpAddress;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getLocationValue() {
        return locationValue;
    }

    public void setLocationValue(String locationValue) {
        this.locationValue = locationValue;
    }

    public String getDpId() {
        return dpId;
    }

    public void setDpId(String dpId) {
        this.dpId = dpId;
    }

    public String getRdsId() {
        return rdsId;
    }

    public void setRdsId(String rdsId) {
        this.rdsId = rdsId;
    }

    public String getRdsVer() {
        return rdsVer;
    }

    public void setRdsVer(String rdsVer) {
        this.rdsVer = rdsVer;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }

    public String getMi() {
        return mi;
    }

    public void setMi(String mi) {
        this.mi = mi;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Metadata{");
        sb.append("fingerPrintDeviceCode='").append(fingerPrintDeviceCode).append('\'');
        sb.append(", irisDeviceCode='").append(irisDeviceCode).append('\'');
        sb.append(", uniqueHostOrTerminalDeviceCode='").append(uniqueHostOrTerminalDeviceCode).append('\'');
        sb.append(", publicIpAddress='").append(publicIpAddress).append('\'');
        sb.append(", locationType='").append(locationType).append('\'');
        sb.append(", locationValue='").append(locationValue).append('\'');
        sb.append(", dpId='").append(dpId).append('\'');
        sb.append(", rdsId='").append(rdsId).append('\'');
        sb.append(", rdsVer='").append(rdsVer).append('\'');
        sb.append(", dc='").append(dc).append('\'');
        sb.append(", mi='").append(mi).append('\'');
        sb.append(", mc='").append(mc).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Metadata metadata = (Metadata) o;
        return Objects.equals(fingerPrintDeviceCode, metadata.fingerPrintDeviceCode) &&
                Objects.equals(irisDeviceCode, metadata.irisDeviceCode) &&
                Objects.equals(uniqueHostOrTerminalDeviceCode, metadata.uniqueHostOrTerminalDeviceCode) &&
                Objects.equals(publicIpAddress, metadata.publicIpAddress) &&
                Objects.equals(locationType, metadata.locationType) &&
                Objects.equals(locationValue, metadata.locationValue) &&
                Objects.equals(dpId, metadata.dpId) &&
                Objects.equals(rdsId, metadata.rdsId) &&
                Objects.equals(rdsVer, metadata.rdsVer) &&
                Objects.equals(dc, metadata.dc) &&
                Objects.equals(mi, metadata.mi) &&
                Objects.equals(mc, metadata.mc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fingerPrintDeviceCode, irisDeviceCode, uniqueHostOrTerminalDeviceCode, publicIpAddress, locationType, locationValue, dpId, rdsId, rdsVer, dc, mi, mc);
    }

    public static class Builder {
        private Metadata metadata = new Metadata();

        public Metadata build() {
            return metadata;
        }

        public Builder fingerPrintDeviceCode(String fingerPrintDeviceCode) {
            this.metadata.fingerPrintDeviceCode = fingerPrintDeviceCode;
            return this;
        }

        public Builder irisDeviceCode(String irisDeviceCode) {
            this.metadata.irisDeviceCode = irisDeviceCode;
            return this;
        }

        public Builder uniqueHostOrTerminalDeviceCode(String uniqueHostOrTerminalDeviceCode) {
            this.metadata.uniqueHostOrTerminalDeviceCode = uniqueHostOrTerminalDeviceCode;
            return this;
        }

        public Builder publicIpAddress(String publicIpAddress) {
            this.metadata.publicIpAddress = publicIpAddress;
            return this;
        }

        public Builder locationType(String locationType) {
            this.metadata.locationType = locationType;
            return this;
        }

        public Builder locationValue(String locationValue) {
            this.metadata.locationValue = locationValue;
            return this;
        }

        public Builder dpId(String dpId) {
            this.metadata.dpId = dpId;
            return this;
        }

        public Builder rdsId(String rdsId) {
            this.metadata.rdsId = rdsId;
            return this;
        }

        public Builder rdsVer(String rdsVer) {
            this.metadata.rdsVer = rdsVer;
            return this;
        }

        public Builder dc(String dc) {
            this.metadata.dc = dc;
            return this;
        }

        public Builder mi(String mi) {
            this.metadata.mi = mi;
            return this;
        }

        public Builder mc(String mc) {
            this.metadata.mc = mc;
            return this;
        }
    }
}