package com.softcell.gonogo.model.core.kyc;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * Created by yogesh on 7/12/17.
 */
@Builder
@AllArgsConstructor
public class DefaultBaseResponse {

    @JsonProperty("sStatus")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResMessage() {
        return resMessage;
    }

    public void setResMessage(String resMessage) {
        this.resMessage = resMessage;
    }

    @JsonProperty("sResMessage")

    private String resMessage;

    @Override
    public String toString() {
        return "DeafaultBaseResponse{" +
                "status='" + status + '\'' +
                ", resMessage='" + resMessage + '\'' +
                '}';
    }
}
