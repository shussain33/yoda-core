package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Applicant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 9/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicantRequest {

    @JsonProperty("REQUEST")
    private Applicant request;

    @JsonProperty("CIBIL_RESPONSE")
    private Object cibilRespopnse;

    @JsonProperty("MERGED_RESPONSE")
    private Object mergedResponse;
}
