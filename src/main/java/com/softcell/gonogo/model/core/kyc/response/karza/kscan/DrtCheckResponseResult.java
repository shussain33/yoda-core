package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DrtCheckResponseResult {

    @JsonProperty("status")
    private String status;

    @JsonProperty("caseType")
    private String caseType;

    @JsonProperty("petitionerList")
    private List<String> petitionerList;

    @JsonProperty("respondentAdvocate")
    private String respondentAdvocate;

    @JsonProperty("number")
    private String number;

    @JsonProperty("nextHearingDate")
    private String nextHearingDate;

    @JsonProperty("caseNo")
    private String caseNo;

    @JsonProperty("linkedEntities")
    private List<String>  linkedEntities;

    @JsonProperty("location")
    private String location;

    @JsonProperty("petitionerAdvocate")
    private String petitionerAdvocate;

    @JsonProperty("caseStage")
    private String caseStage;

    @JsonProperty("action")
    private String action;

    @JsonProperty("rcStatus")
    private String rcStatus;

    @JsonProperty("respondentList")
    private List<String> respondentList;
}