package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GstTransactionStateWiseResponse {

    @JsonProperty("state_code")
    private String stateCode;

    @JsonProperty("state_name")
    private String stateName;

    @JsonProperty("gstTransactions")
    private GstTransactionStateWiseTransactionResponse gstTransactions;
}