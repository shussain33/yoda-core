package com.softcell.gonogo.model.core.kyc.response.karza.addressmappingauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressMappingRequestDetails {

    @JsonProperty("address1")
    private String address1;

    @JsonProperty("address2")
    private String address2;

}