package com.softcell.gonogo.model.core.digitization;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yogeshb
 * @author mahesh
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationForm {

    private String gngRefID;
    private String institutionId;
    private String logo;
    private String applicationDate;
    private String losNumber;
    private String occuption;
    private String profession;
    private String salariedCompanyType;
    private String businessCompanyType;
    private String natureOfBusiness;
    private String designation;
    private String applicantType;
    private String loanNumber;
    private String prefix;
    private String applicantName;
    private String age;
    private String fatherName;
    private String motherName;
    private String spouseName;
    private String salutation;
    private String dateOfBirth;
    private String panNumber;
    private String gender;
    private String maritalStatus;
    private String numberOfDependent;
    private String education;
    private String mobile;
    private String religion;
    private String cast;
    private String emailID;
    private String drivingLicenseNumber;
    private String drivingLicenseExpDate;
    private String aadharNumber;
    private String voterIDNumber;
    private String passportNumber;
    private String passportExpiryDate;
    private String monthlyResidenceAddRent;
    private String resAddressSameAsPer;
    private String presentResidence;
    private String residenseLandMark;
    private String residenceMobile;
    private String residenseAddress;
    private String residenseState;
    private String residenseCity;
    private String residensePin;
    private String residenseStd;
    private String residenseTelephone;
    private String yearAtResidense;
    private String monthAtAddress;
    private String yearAtCity;
    private String employerName;
    private String employmentType;
    private String employerAddress;
    private String employerLandMark;
    private String employerCity;
    private String employerState;
    private String employerPin;
    private String employerStd;
    private String employerPhone;
    private String employerExtension;
    private String employerCombAddress;
    private String totalYearInBusiness;
    private String numberOfYearInCurrentBusiness;
    private String preferredMailingAddress;
    private String officeEmailID;

    private String electricityNo;

    private String permanentResidence;
    private String permanentAddress;
    private String permanentLandMark;
    private String permanentState;
    private String permanentCity;
    private String permanentPin;
    private String permanentStd;
    private String permanentTelephone;
    private String monthlyPermenentAddRent;
    private String permanentCombAddress;


    private String addressProof;

    //applicant income details
    private String grossMonthlyIncome;
    private String additionalIncomeSource;

   //actually its sales officer details
    private String dsaId;
    private String dsaName;
    private String dsaMoNumber;

    private String relationWithTvs;

    private String firstEmiDueDt;
    private String firstEmiDueDtInDDmmYYYY;
    private String lastEmiDueDate;
    private String insEwAmtTotal;

    private String totalDownPayment;

    /**
     * banking related information to be display in the  agreement form and application Form of banking section.
     *
     * @return
     */

    private String annualSalary;
    private String otherAnnualSalary;
    private String yearOpend;
    private String customerID;
    private String typeOfAccount;
    private String creditCardNumber;
    private String creditCardExpiryDate;
    private String creditCardBankName;
    private String creditCardType;
    private String debitCardNumber;
    private String debitCardType;
    private String debitCardBank;
    private String accountHolderName;
    private String bankBranchName;
    private String bankName;
    private String accountNumber;
    private String ifscCode;
    private String salaryAccount;
    private String avgBankBalance;
    private String deductedEmiAmt;
    private String chequeNumber;
    private String yearHeld;
    private String bankingType;
    private String isHdfcBankAccount;


    private String assetOwned;
    private double loanAmount;
    private String totalAssetCost;
    private String tenure;
    private double emi;
    private String advanceEMI;
    private String proceFees;
    private String advanceEMITenor;
    private String dealerName;
    private String productType;
    private String manufacturer;
    private String assetModelMake;
    private String model;
    private String financeAmount;
    private String otherCharges;
    /**
     * this two fields are used to store the details of revised emi and revised loan amount after the calculation of
     * extended warranty + Loyalty Card + Insurance Details.
     */
    private String revisedEmiAmount;
    private String revisedLoanAmount;

    /**
     * Referense Details
     */
    private String refName1;
    private String refRelationshipWithApplicant1;
    private String refResidentialAddress1;
    private String refCity1;
    private String refState1;
    private String refPin1;
    private String refSTD1;
    private String refPhone1;
    private String refMobile1;

    private String refName2;
    private String refRelationshipWithApplicant2;
    private String refResidentialAddress2;
    private String refCity2;
    private String refState2;
    private String refPin2;
    private String refSTD2;
    private String refPhone2;
    private String refMobile2;

    /**
     * sourcing details
     */
    private String applicationFees;
    private String soCode;
    private String smCode;
    private String rmCode;
    private String tmAsmCode;
    private String asmCmCode;
    private String dsaCode;
    private String dealerCode;
    private String schemeCode;
    private String branchName;
    private String location;
    private String dateAndTimeOfReceipt;
    private String hdbContactPersonName;
    private String contactNumber;
    private String applicantPhoto;
    private String dateOfApplication;
    private String bankSurrogateProof;
    private String salarySlipSurrogate;
    private String businessSurrogateProof;
    private String creditCardLnAccNo;
    //from scheme master
    private String customerIRR;
    private String rateOfInterest;
    private String earningMember;
    private String totalFamMember;
    private String bankMemberSince;
    private String lpgId;

    private String carType;

    private String officeMobile;

    //for tvs agreement form

    private double ewEmi;
    private double insuranceEmi;

    private double ewInsuranceAssetEmi;

    private String productName;
    private String gstInNo;
    private String gstEffectiveDate;
}
