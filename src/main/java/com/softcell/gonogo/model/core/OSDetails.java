package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author yogeshb
 */
public class OSDetails implements Serializable {

    @JsonProperty("sMacAddress")
    private String macAddress;

    @JsonProperty("sOS")
    private String osName;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    /**
     * @return the osName
     */
    public String getOsName() {
        return osName;
    }

    /**
     * @param osName the osName to set
     */
    public void setOsName(String osName) {
        this.osName = osName;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("OSDetails [macAddress=");
        builder.append(macAddress);
        builder.append(", osName=");
        builder.append(osName);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((macAddress == null) ? 0 : macAddress.hashCode());
        result = prime * result + ((osName == null) ? 0 : osName.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OSDetails other = (OSDetails) obj;
        if (macAddress == null) {
            if (other.macAddress != null)
                return false;
        } else if (!macAddress.equals(other.macAddress))
            return false;
        if (osName == null) {
            if (other.osName != null)
                return false;
        } else if (!osName.equals(other.osName))
            return false;
        return true;
    }
}
