/**
 * kishorp6:10:39 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.multiproduct.MultiProductRequest;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;

/**
 * @author kishorp
 *
 */

public class AssetDetails implements Serializable {

    @JsonProperty("sAssetCtg")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class, MultiProductRequest.ProductSelectionGrp.class})
    private String assetCtg;

    @JsonProperty("sDlrName")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class})
    private String dlrName;


    /**
     * Its actually manufacture
     */
    @JsonProperty("sAssetMake")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class})
    private String assetMake;

    /**
     * Manufacturer Code
     */
    @JsonProperty("sAssetMakeCode")
    private String assetMakeCode;

    @JsonProperty("sAssetModelMake")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class})
    private String assetModelMake;

    @JsonProperty("sModelNo")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class})
    private String modelNo;

    @JsonProperty("sPrice")
    private String price;

    public String getAssetCtg() {
        return assetCtg;
    }

    public void setAssetCtg(String assetCtg) {
        this.assetCtg = assetCtg;
    }

    public String getDlrName() {
        return dlrName;
    }

    public void setDlrName(String dlrName) {
        this.dlrName = dlrName;
    }

    public String getAssetMake() {
        return assetMake;
    }

    public void setAssetMake(String assetMake) {
        this.assetMake = assetMake;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAssetModelMake() {
        return assetModelMake;
    }

    public void setAssetModelMake(String assetModelMake) {
        this.assetModelMake = assetModelMake;
    }

    public String getAssetMakeCode() { return assetMakeCode; }

    public void setAssetMakeCode(String assetMakeCode) { this.assetMakeCode = assetMakeCode; }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AssetDetails [assetCtg=");
        builder.append(assetCtg);
        builder.append(", dlrName=");
        builder.append(dlrName);
        builder.append(", assetMakeCode=");
        builder.append(assetMakeCode);
        builder.append(", assetMake=");
        builder.append(assetMake);
        builder.append(", assetModelMake=");
        builder.append(assetModelMake);
        builder.append(", modelNo=");
        builder.append(modelNo);
        builder.append(", price=");
        builder.append(price);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((assetCtg == null) ? 0 : assetCtg.hashCode());
        result = prime * result
                + ((assetMake == null) ? 0 : assetMake.hashCode());
        result = prime * result
                + ((assetMakeCode == null) ? 0 : assetMakeCode.hashCode());
        result = prime * result
                + ((assetModelMake == null) ? 0 : assetModelMake.hashCode());
        result = prime * result + ((dlrName == null) ? 0 : dlrName.hashCode());
        result = prime * result + ((modelNo == null) ? 0 : modelNo.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof AssetDetails))
            return false;
        AssetDetails other = (AssetDetails) obj;
        if (assetCtg == null) {
            if (other.assetCtg != null)
                return false;
        } else if (!assetCtg.equals(other.assetCtg))
            return false;
        if (assetMake == null) {
            if (other.assetMake != null)
                return false;
        } else if (!assetMake.equals(other.assetMake))
            return false;
        if (assetMakeCode == null) {
            if (other.assetMakeCode != null)
                return false;
        } else if (!assetMakeCode.equals(other.assetMakeCode))
            return false;
        if (assetModelMake == null) {
            if (other.assetModelMake != null)
                return false;
        } else if (!assetModelMake.equals(other.assetModelMake))
            return false;
        if (dlrName == null) {
            if (other.dlrName != null)
                return false;
        } else if (!dlrName.equals(other.dlrName))
            return false;
        if (modelNo == null) {
            if (other.modelNo != null)
                return false;
        } else if (!modelNo.equals(other.modelNo))
            return false;
        if (price == null) {
            if (other.price != null)
                return false;
        } else if (!price.equals(other.price))
            return false;
        return true;
    }

}
