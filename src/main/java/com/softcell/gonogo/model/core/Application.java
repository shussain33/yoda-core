package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author prateek
 */
public class Application implements Serializable {

    @JsonProperty("sAppID")
    private String applicationID;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("sProductID")
    private String productID;

    @JsonProperty("sSubProduct")
    private String subproduct;

    @JsonProperty("sAppliedFor")
    private String appliedfor;

    @JsonProperty("dLoanAmt")
    private double loanAmount;

    @JsonProperty("iLoanTenor")
    private int loanTenor;

    @JsonProperty("oProperty")
    private Property property;

    @JsonProperty("sLnPurp")
    private String loanPurpose;

    @JsonProperty("dLnApr")
    private double loanApr;



    /**
     * Newly Added attributes of HDBFS
     */

    @JsonProperty("dEmi")
    private double emi;

    @JsonProperty("iAdvEmi")
    private int numberOfAdvanceEmi;

    @JsonProperty("dDedupeEmiPaid")
    private double dedupeEmiPaid;

    @JsonProperty("iDedupeTenor")
    private int dedupeTenor;

    @JsonProperty("sDedupeRemark")
    private String dedupeRemark;

    @JsonProperty("dMarginAmt")
    private double marginAmount;

    @JsonProperty("aAssetDetail")
    private List<AssetDetails> asset;

    @JsonProperty("aOwndAst")
    private List<AssetDetails> ownedAsset;

    @JsonProperty("oCarDetails")
    private CarDetails carDetails;

    @JsonProperty("bTermsConditionAgreement")
    private boolean termsConditionAgreement;

    @JsonProperty("bDNDAgrrement")
    private boolean dndAgrrement;

    /**
     * this field added for Existing Qde customer
     */
    @JsonProperty("dPreApprovedAmt")
    private double preApprovedAmount;

    @JsonProperty("sExistingLoanAccNumber")
    private String existingLoanAccNumber;

    @JsonProperty("sChannel")
    private String channel; // Branch / D2C

    @JsonProperty("oInitialMoneyDeposit")
    private InitialMoneyDeposit initialMoneyDeposit;

    //SBFC IMD2
    @JsonProperty("aIMD2")
    private List<InitialMoneyDeposit> imd2;

    @JsonProperty("aCollateral")
    private List<Collateral> collateral;

    @JsonProperty("sLoanPurposeOther")
    private String loanPurposeOther;

    @JsonProperty("dtDocReceivedDate")
    private Date docReceivedDate;

    @JsonProperty("sTenorRequested")
    private String tenorRequested;

    // for SBFC PL
    @JsonProperty("sPromoCode")
    private String promoCode;

    // for SBFC PL
    @JsonProperty("sCity")
    private String city;

    @JsonProperty("bCoOrigination")
    private boolean coOrigination;

    @JsonProperty("sCompanyName")
    private String companyName;

    @JsonProperty("sSchemeName")
    private String schemeName;

    @JsonProperty("sTopup")
    private String topup;

    @JsonProperty("sDedupeParameterName")
    private String dedupeParameterName;


    @JsonProperty("sDedupeParameterValue")
    private String dedupeParameterValue;

    @JsonProperty("sReferalMob")
    private String referalMob;

    @JsonProperty("sReferralCode")
    private String referalCode;

    @JsonProperty("sSourcingTeam")
    private String sourcingTeam;

    @JsonProperty("sDsaValues")
    private String dsaVaules;

    @JsonProperty("sDsaCode")
    private String dsaCode;

    @JsonProperty("sDsaName")
    private String dsaName;

    @JsonProperty("sDsaPhone")
    private String dsaPhone;

    @JsonProperty("sDSAContactPerson")
    private String dsaContactPerson;

    @JsonProperty("sIciciFormNumber")
    private String iciciFormNumber;

    @JsonProperty("oEmployeeDetail")
    private EmployeeDetails employeeDetail;

    //DigiPl Fields Started
    @JsonProperty("dRoi")
    private Double roi;

    @JsonProperty("oRCUSummary")
    private RCUSummary rcuSummary;

    @JsonProperty("bBranchQuery")
    private boolean branchQuery;

    @JsonProperty("sOcrStatus")
    private String ocrStatus;

    @JsonProperty("sAgencyCode")
    public String agencyCode;

    @JsonProperty("sAgencyName")
    public String agencyName;

    @JsonProperty("sAgencyStatus")
    public String agencyStatus;

    @JsonProperty("bTermsAndConditions")
    private boolean termsAndConditions;

    @JsonProperty("sTranchAmount")
    private String tranchAmount;

    public Double getRoi() {        return roi;    }

    public void setRoi(Double roi) {        this.roi = roi;    }

    @JsonProperty("sMifinAgencyId")
    private String mifinAgencyId;

    @JsonProperty("sMifinAgentId")
    private String mifinAgentId;

    @JsonProperty("bAndroidCase")
    private boolean androidCase;

    @JsonProperty("bAndroidOfflineCase")
    private boolean androidOfflineCase;

    public RCUSummary getRcuSummary() {
        return rcuSummary;
    }

    public void setRcuSummary(RCUSummary rcuSummary) { this.rcuSummary = rcuSummary; }

    public boolean isBranchQuery() {
        return branchQuery;
    }

    public void setBranchQuery(boolean branchQuery) { this.branchQuery = branchQuery; }

    public String getOcrStatus() {
        return ocrStatus;
    }

    public void setOcrStatus(String ocrStatus) {
        this.ocrStatus = ocrStatus;
    }

    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgencyStatus() {
        return agencyStatus;
    }

    public void setAgencyStatus(String agencyStatus) { this.agencyStatus = agencyStatus; }

    public boolean isTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(boolean termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getTranchAmount() {  return tranchAmount;    }

    public void setTranchAmount(String tranchAmount) {  this.tranchAmount = tranchAmount;    }

    public String getIciciFormNumber() {
        return iciciFormNumber;
    }

    public void setIciciFormNumber(String iciciFormNumber) {
        this.iciciFormNumber = iciciFormNumber;
    }

    public String getDedupeParameterName() {
        return dedupeParameterName;
    }

    public void setDedupeParameterName(String dedupeParameterName) {
        this.dedupeParameterName = dedupeParameterName;
    }

    public String getDedupeParameterValue() {
        return dedupeParameterValue;
    }

    public void setDedupeParameterValue(String dedupeParameterValue) {
        this.dedupeParameterValue = dedupeParameterValue;
    }


    public String getInstallmentType() {
        return installmentType;
    }

    public void setInstallmentType(String installmentType) {
        this.installmentType = installmentType;
    }

    @JsonProperty("sInstallmentType")
    private String installmentType;

    public String getTopup() {
        return topup;
    }

    public void setTopup(String topup) {
        this.topup = topup;
    }

    public String getTopupType() {
        return topupType;
    }

    public void setTopupType(String topupType) {
        this.topupType = topupType;
    }

    @JsonProperty("sTopUpType")
    private String topupType;

    public boolean isCoOrigination() {  return coOrigination;   }

    public void setCoOrigination(boolean coOrigination) {   this.coOrigination = coOrigination; }

    public String getCompanyName() {    return companyName; }

    public void setCompanyName(String companyName) {    this.companyName = companyName; }

    public String getSchemeName() { return schemeName;  }

    public void setSchemeName(String schemeName) {  this.schemeName = schemeName;   }

    public String getCity() {   return city;   }

    public void setCity(String city) {    this.city = city;   }

    public String getPromoCode() {  return promoCode;   }

    public void setPromoCode(String promoCode) {    this.promoCode = promoCode; }

    public double getPreApprovedAmount() {
        return preApprovedAmount;
    }

    public void setPreApprovedAmount(double preApprovedAmount) {
        this.preApprovedAmount = preApprovedAmount;
    }

    public CarDetails getCarDetails() {
        return carDetails;
    }

    public void setCarDetails(CarDetails carDetails) {
        this.carDetails = carDetails;
    }

    public String getApplicationID() {
        return applicationID;
    }

    public void setApplicationID(String applicationID) {
        this.applicationID = applicationID;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getAppliedfor() {
        return appliedfor;
    }

    public void setAppliedfor(String appliedfor) {
        this.appliedfor = appliedfor;
    }

    public double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public int getLoanTenor() {
        return loanTenor;
    }

    public void setLoanTenor(int loanTenor) {
        this.loanTenor = loanTenor;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public List<AssetDetails> getAsset() {
        return asset;
    }

    public void setAsset(List<AssetDetails> asset) {
        this.asset = asset;
    }

    public double getEmi() {
        return emi;
    }

    public void setEmi(double emi) {
        this.emi = emi;
    }

    public int getNumberOfAdvanceEmi() {
        return numberOfAdvanceEmi;
    }

    public void setNumberOfAdvanceEmi(int numberOfAdvanceEmi) {
        this.numberOfAdvanceEmi = numberOfAdvanceEmi;
    }

    public double getMarginAmount() {
        return marginAmount;
    }

    public void setMarginAmount(double marginAmount) {
        this.marginAmount = marginAmount;
    }

    public String getLoanPurpose() {
        return loanPurpose;
    }

    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    public double getLoanApr() {
        return loanApr;
    }

    public void setLoanApr(double loanApr) {
        this.loanApr = loanApr;
    }

    public List<AssetDetails> getOwnedAsset() {
        return ownedAsset;
    }

    public void setOwnedAsset(List<AssetDetails> ownedAsset) {
        this.ownedAsset = ownedAsset;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public boolean isTermsConditionAgreement() {
        return termsConditionAgreement;
    }

    public void setTermsConditionAgreement(boolean termsConditionAgreement) {
        this.termsConditionAgreement = termsConditionAgreement;
    }

    public boolean isDndAgrrement() {
        return dndAgrrement;
    }

    public void setDndAgrrement(boolean dndAgrrement) {
        this.dndAgrrement = dndAgrrement;
    }

    public double getDedupeEmiPaid() {
        return dedupeEmiPaid;
    }

    public void setDedupeEmiPaid(double dedupeEmiPaid) {
        this.dedupeEmiPaid = dedupeEmiPaid;
    }

    public int getDedupeTenor() {
        return dedupeTenor;
    }

    public void setDedupeTenor(int dedupeTenor) {
        this.dedupeTenor = dedupeTenor;
    }

    public String getDedupeRemark() {
        return dedupeRemark;
    }

    public void setDedupeRemark(String dedupeRemark) {
        this.dedupeRemark = dedupeRemark;
    }

    public String getExistingLoanAccNumber() {
        return existingLoanAccNumber;
    }

    public void setExistingLoanAccNumber(String existingLoanAccNumber) {
        this.existingLoanAccNumber = existingLoanAccNumber;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public List<Collateral> getCollateral() {
        return collateral;
    }

    public void setCollateral(List<Collateral> collateral) {
        this.collateral = collateral;
    }

    public InitialMoneyDeposit getInitialMoneyDeposit() {
        return initialMoneyDeposit;
    }

    public void setInitialMoneyDeposit(InitialMoneyDeposit initialMoneyDeposit) {
        this.initialMoneyDeposit = initialMoneyDeposit;
    }

    public List<InitialMoneyDeposit> getImd2() {
        return imd2;
    }

    public void setImd2(List<InitialMoneyDeposit> imd2) {
        this.imd2 = imd2;
    }

    public String getLoanPurposeOther() {
        return loanPurposeOther;
    }

    public void setLoanPurposeOther(String loanPurposeOther) {
        this.loanPurposeOther = loanPurposeOther;
    }

    public Date getDocReceivedDate() {
        return docReceivedDate;
    }

    public void setDocReceivedDate(Date docReceivedDate) {
        this.docReceivedDate = docReceivedDate;
    }

    public String getTenorRequested() {
        return tenorRequested;
    }

    public void setTenorRequested(String tenorRequested) {
        this.tenorRequested = tenorRequested;
    }


    public String getSubproduct() {
        return subproduct;
    }

    public void setSubproduct(String subproduct) {
        this.subproduct = subproduct;
    }

    public String getDsaVaules() {
        return dsaVaules;
    }

    public void setDsaVaules(String dsaVaules) {
        this.dsaVaules = dsaVaules;
    }

    public String getReferalMob() {
        return referalMob;
    }

    public void setReferalMob(String referalMob) {
        this.referalMob = referalMob;
    }

    public String getReferalCode() {
        return referalCode;
    }

    public void setReferalCode(String referalCode) {
        this.referalCode = referalCode;
    }

    public String getSourcingTeam() {
        return sourcingTeam;
    }

    public void setSourcingTeam(String sourcingTeam) {
        this.sourcingTeam = sourcingTeam;
    }

    public String getDsaCode() {
        return dsaCode;
    }

    public void setDsaCode(String dsaCode) {
        this.dsaCode = dsaCode;
    }

    public String getDsaName() {
        return dsaName;
    }

    public void setDsaName(String dsaName) {
        this.dsaName = dsaName;
    }

    public String getDsaPhone() {
        return dsaPhone;
    }

    public void setDsaPhone(String dsaPhone) {
        this.dsaPhone = dsaPhone;
    }

    public String getDsaContactPerson() {
        return dsaContactPerson;
    }

    public void setDsaContactPerson(String dsaContactPerson) {
        this.dsaContactPerson = dsaContactPerson;
    }

    public EmployeeDetails getEmployeeDetail() { return employeeDetail; }

    public void setEmployeeDetail(EmployeeDetails employeeDetail) { this.employeeDetail = employeeDetail; }

    public void setMifinAgencyId(String mifinAgencyId) { this.mifinAgencyId = mifinAgencyId;}

    public String getMifinAgencyId() { return mifinAgencyId; }

    public void setMifinAgentId(String mifinAgentId) { this.mifinAgentId = mifinAgentId;}

    public String getMifinAgentId() { return mifinAgentId; }

    public void setAndroidCase(boolean androidCase) { this.androidCase = androidCase; }

    public boolean getAndroidCase() { return androidCase; }

    public boolean isAndroidOfflineCase() {
        return androidOfflineCase;
    }

    public void setAndroidOfflineCase(boolean androidOfflineCase) {
        this.androidOfflineCase = androidOfflineCase;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Application{");
        sb.append("applicationID='").append(applicationID).append('\'');
        sb.append(", loanType='").append(loanType).append('\'');
        sb.append(", productID='").append(productID).append('\'');
        sb.append(", appliedfor='").append(appliedfor).append('\'');
        sb.append(", loanAmount=").append(loanAmount);
        sb.append(", loanTenor=").append(loanTenor);
        sb.append(", property=").append(property);
        sb.append(", loanPurpose='").append(loanPurpose).append('\'');
        sb.append(", loanApr=").append(loanApr);
        sb.append(", emi=").append(emi);
        sb.append(", numberOfAdvanceEmi=").append(numberOfAdvanceEmi);
        sb.append(", dedupeEmiPaid=").append(dedupeEmiPaid);
        sb.append(", dedupeTenor=").append(dedupeTenor);
        sb.append(", dedupeRemark='").append(dedupeRemark).append('\'');
        sb.append(", marginAmount=").append(marginAmount);
        sb.append(", asset=").append(asset);
        sb.append(", ownedAsset=").append(ownedAsset);
        sb.append(", carDetails=").append(carDetails);
        sb.append(", termsConditionAgreement=").append(termsConditionAgreement);
        sb.append(", dndAgrrement=").append(dndAgrrement);
        sb.append(", preApprovedAmount=").append(preApprovedAmount);
        sb.append(", existingLoanAccNumber=").append(existingLoanAccNumber);
        sb.append(", channel=").append(channel);
        sb.append(", collateral=");
        sb.append(collateral);
        sb.append(", initialMoneyDeposit=");
        sb.append(initialMoneyDeposit);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Application that = (Application) o;

        if (Double.compare(that.loanAmount, loanAmount) != 0) return false;
        if (loanTenor != that.loanTenor) return false;
        if (Double.compare(that.loanApr, loanApr) != 0) return false;
        if (Double.compare(that.emi, emi) != 0) return false;
        if (numberOfAdvanceEmi != that.numberOfAdvanceEmi) return false;
        if (Double.compare(that.dedupeEmiPaid, dedupeEmiPaid) != 0) return false;
        if (dedupeTenor != that.dedupeTenor) return false;
        if (Double.compare(that.marginAmount, marginAmount) != 0) return false;
        if (termsConditionAgreement != that.termsConditionAgreement) return false;
        if (dndAgrrement != that.dndAgrrement) return false;
        if (Double.compare(that.preApprovedAmount, preApprovedAmount) != 0) return false;
        if (applicationID != null ? !applicationID.equals(that.applicationID) : that.applicationID != null)
            return false;
        if (loanType != null ? !loanType.equals(that.loanType) : that.loanType != null) return false;
        if (productID != null ? !productID.equals(that.productID) : that.productID != null) return false;
        if (appliedfor != null ? !appliedfor.equals(that.appliedfor) : that.appliedfor != null) return false;
        if (property != null ? !property.equals(that.property) : that.property != null) return false;
        if (loanPurpose != null ? !loanPurpose.equals(that.loanPurpose) : that.loanPurpose != null) return false;
        if (dedupeRemark != null ? !dedupeRemark.equals(that.dedupeRemark) : that.dedupeRemark != null) return false;
        if (asset != null ? !asset.equals(that.asset) : that.asset != null) return false;
        if (ownedAsset != null ? !ownedAsset.equals(that.ownedAsset) : that.ownedAsset != null) return false;
        if (existingLoanAccNumber != null ? !existingLoanAccNumber.equals(that.existingLoanAccNumber) : that.existingLoanAccNumber != null)
            return false;
        if (channel != null ? !channel.equals(that.channel) : that.channel != null) return false;
        if (collateral != null ? !collateral.equals(that.collateral) : that.collateral != null) return false;
        if (initialMoneyDeposit != null ? !initialMoneyDeposit.equals(that.initialMoneyDeposit) : that.initialMoneyDeposit != null)
            return false;
        return !(carDetails != null ? !carDetails.equals(that.carDetails) : that.carDetails != null);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = applicationID != null ? applicationID.hashCode() : 0;
        result = 31 * result + (loanType != null ? loanType.hashCode() : 0);
        result = 31 * result + (productID != null ? productID.hashCode() : 0);
        result = 31 * result + (appliedfor != null ? appliedfor.hashCode() : 0);
        temp = Double.doubleToLongBits(loanAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + loanTenor;
        result = 31 * result + (property != null ? property.hashCode() : 0);
        result = 31 * result + (loanPurpose != null ? loanPurpose.hashCode() : 0);
        temp = Double.doubleToLongBits(loanApr);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(emi);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + numberOfAdvanceEmi;
        temp = Double.doubleToLongBits(dedupeEmiPaid);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + dedupeTenor;
        result = 31 * result + (dedupeRemark != null ? dedupeRemark.hashCode() : 0);
        temp = Double.doubleToLongBits(marginAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (asset != null ? asset.hashCode() : 0);
        result = 31 * result + (ownedAsset != null ? ownedAsset.hashCode() : 0);
        result = 31 * result + (carDetails != null ? carDetails.hashCode() : 0);
        result = 31 * result + (termsConditionAgreement ? 1 : 0);
        result = 31 * result + (dndAgrrement ? 1 : 0);
        temp = Double.doubleToLongBits(preApprovedAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (existingLoanAccNumber != null ? existingLoanAccNumber.hashCode() : 0);
        result = 31 * result + (channel != null ? channel.hashCode() : 0);
        result = 31 * result + (collateral != null ? collateral.hashCode() : 0);
        result = 31 * result + (initialMoneyDeposit != null ? initialMoneyDeposit.hashCode() : 0);
        return result;
    }
}
