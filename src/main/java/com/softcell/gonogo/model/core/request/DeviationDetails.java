package com.softcell.gonogo.model.core.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Deviation;
import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by yogesh on 15/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "deviationMatrix")
public class DeviationDetails {

    @Id
    @JsonProperty("sRefId")
    @NotEmpty(groups = {ApplicationVerification.InsertGrp.class,ApplicationVerification.FetchGrp.class})
    protected String refId;

    @JsonProperty("sInstID")
    protected String institutionId;

    @JsonProperty("aDeviation")
    private List<Deviation> deviationList;

}