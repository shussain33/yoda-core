package com.softcell.gonogo.model.core.kyc.request.aadhar;

public class MetadataForDeviceAndTxn {
	
	String fdc;
	String idc;
	String pip;
	String udc;
	String lot;
	String lov;
	String cdc;
	String fpmi;
	String fpmc;
	String irmi;
	String irmc;
	String fdmi;
	String fdmc;	
	String dpId;
	String rdsId;
	String rdsVer;
	String dc;
	String mi;
	String mc;	
	 
	
	public String getFdc() {
		return fdc;
	}
	public String getPip() {
		return pip;
	}
	public void setPip(String pip) {
		this.pip = pip;
	}
	public String getUdc() {
		return udc;
	}
	public void setUdc(String udc) {
		this.udc = udc;
	}
	public String getLot() {
		return lot;
	}
	public void setLot(String lot) {
		this.lot = lot;
	}
	public String getLov() {
		return lov;
	}
	public void setLov(String lov) {
		this.lov = lov;
	}
	public void setFdc(String fdc) {
		this.fdc = fdc;
	}
	public String getDpId() {
		return dpId;
	}
	public void setDpId(String dpId) {
		this.dpId = dpId;
	}
	public String getRdsId() {
		return rdsId;
	}
	public void setRdsId(String rdsId) {
		this.rdsId = rdsId;
	}
	public String getRdsVer() {
		return rdsVer;
	}
	public void setRdsVer(String rdsVer) {
		this.rdsVer = rdsVer;
	}
	public String getDc() {
		return dc;
	}
	public void setDc(String dc) {
		this.dc = dc;
	}
	public String getMi() {
		return mi;
	}
	public void setMi(String mi) {
		this.mi = mi;
	}
	public String getMc() {
		return mc;
	}
	public void setMc(String mc) {
		this.mc = mc;
	}
	public String getIdc() {
		return idc;
	}
	public void setIdc(String idc) {
		this.idc = idc;
	}
	public String getCdc() {
		return cdc;
	}
	public void setCdc(String cdc) {
		this.cdc = cdc;
	}
	public String getFpmi() {
		return fpmi;
	}
	public void setFpmi(String fpmi) {
		this.fpmi = fpmi;
	}
	public String getFpmc() {
		return fpmc;
	}
	public void setFpmc(String fpmc) {
		this.fpmc = fpmc;
	}
	public String getIrmi() {
		return irmi;
	}
	public void setIrmi(String irmi) {
		this.irmi = irmi;
	}
	public String getIrmc() {
		return irmc;
	}
	public void setIrmc(String irmc) {
		this.irmc = irmc;
	}
	public String getFdmi() {
		return fdmi;
	}
	public void setFdmi(String fdmi) {
		this.fdmi = fdmi;
	}
	public String getFdmc() {
		return fdmc;
	}
	public void setFdmc(String fdmc) {
		this.fdmc = fdmc;
	}
	
	@Override
	public String toString() {
		return "MetadataForDeviceAndTxn [fdc=" + fdc + ", idc=" + idc
				+ ", pip=" + pip + ", udc=" + udc + ", lot=" + lot + ", lov="
				+ lov + ", cdc=" + cdc + ", fpmi=" + fpmi + ", fpmc=" + fpmc
				+ ", irmi=" + irmi + ", irmc=" + irmc + ", fdmi=" + fdmi
				+ ", fdmc=" + fdmc + "]";
	}
	
}
