package com.softcell.gonogo.model.core.excelConfiguration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.FieldsMapping;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "excelConfiguration")
public class ExcelConfiguration {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sMapping")
    private List<FieldsMapping> mappings;

    @JsonProperty("sType")
    private String type;

    @JsonProperty("sFormat")
    private String format;

}
