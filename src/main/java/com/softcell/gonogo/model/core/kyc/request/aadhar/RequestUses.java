package com.softcell.gonogo.model.core.kyc.request.aadhar;

public class RequestUses {
	
	String pi;
	String pa;
	String pfa;
	String bio;
	String bt;
	String pin;
	String otp;
	public String getPi() {
		return pi;
	}
	public void setPi(String pi) {
		this.pi = pi;
	}
	public String getPa() {
		return pa;
	}
	public void setPa(String pa) {
		this.pa = pa;
	}
	public String getPfa() {
		return pfa;
	}
	public void setPfa(String pfa) {
		this.pfa = pfa;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public String getBt() {
		return bt;
	}
	public void setBt(String bt) {
		this.bt = bt;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	@Override
	public String toString() {
		return "RequestUses [pi=" + pi + ", pa=" + pa + ", pfa=" + pfa + ", bio=" + bio + ", bt=" + bt + ", pin=" + pin
				+ ", otp=" + otp + "]";
	}
	
	
	
	

}
