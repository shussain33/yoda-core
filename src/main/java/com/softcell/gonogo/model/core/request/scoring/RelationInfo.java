package com.softcell.gonogo.model.core.request.scoring;


public class RelationInfo {
    private String name;
    private String relationType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    @Override
    public String toString() {
        return "RelationInfoType [name=" + name + ", relationType="
                + relationType + "]";
    }
}
