package com.softcell.gonogo.model.core.request.scoring;


public class MFIAddlAdrsDetailsType {

    private String mfiAddressline;
    private String mfiState;
    private String mfiPostalPIN;

    public String getMfiAddressline() {
        return mfiAddressline;
    }

    public void setMfiAddressline(String mfiAddressline) {
        this.mfiAddressline = mfiAddressline;
    }

    public String getMfiState() {
        return mfiState;
    }

    public void setMfiState(String mfiState) {
        this.mfiState = mfiState;
    }

    public String getMfiPostalPIN() {
        return mfiPostalPIN;
    }

    public void setMfiPostalPIN(String mfiPostalPIN) {
        this.mfiPostalPIN = mfiPostalPIN;
    }

    @Override
    public String toString() {
        return "MFIAddlAdrsDetailsType [mfiAddressline=" + mfiAddressline
                + ", mfiState=" + mfiState + ", mfiPostalPIN=" + mfiPostalPIN
                + "]";
    }
}
