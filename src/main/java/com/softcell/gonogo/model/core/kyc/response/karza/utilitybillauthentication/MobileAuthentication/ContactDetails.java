package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ContactDetails {

    @JsonProperty("address")
    private String address;

    @JsonProperty("alt_contact")
    private String alt_contact;

    @JsonProperty("email_id")
    private String email_id;

    @JsonProperty("work_email")
    private String work_email;

}