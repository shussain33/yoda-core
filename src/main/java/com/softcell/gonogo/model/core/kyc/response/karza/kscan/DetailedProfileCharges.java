package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DetailedProfileCharges {

    @JsonProperty("address")
    private String address;

    @JsonProperty("assetsUnderCharge")
    private String assetsUnderCharge;

    @JsonProperty("chargeAmount")
    private String chargeAmount;

    @JsonProperty("chargeHolderName")
    private String chargeHolderName;

    @JsonProperty("chargeId")
    private String chargeId;

    @JsonProperty("dateOfCreation")
    private String dateOfCreation;

    @JsonProperty("dateOfModification")
    private String dateOfModification;

    @JsonProperty("dateOfSatisfaction")
    private String dateOfSatisfaction;

    @JsonProperty("srn")
    private String srn;

    @JsonProperty("status")
    private String status;

}