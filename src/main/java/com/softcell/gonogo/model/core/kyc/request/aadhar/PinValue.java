package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PinValue {

    @JsonProperty("PIN")
    private String pin;

    @JsonProperty("OTP")
    private String otp;

    public static Builder builder() {
        return new Builder();
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PinValue{");
        sb.append("pin='").append(pin).append('\'');
        sb.append(", otp='").append(otp).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PinValue)) return false;
        PinValue pinValue = (PinValue) o;
        return Objects.equal(getPin(), pinValue.getPin()) &&
                Objects.equal(getOtp(), pinValue.getOtp());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPin(), getOtp());
    }

    public static class Builder {

        private PinValue pinValue = new PinValue();

        public PinValue build() {
            return pinValue;
        }

        public Builder pin(String pin) {
            this.pinValue.pin = pin;
            return this;
        }

        public Builder otp(String otp) {
            this.pinValue.otp = otp;
            return this;
        }
    }
}
