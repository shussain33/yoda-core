package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import com.softcell.gonogo.model.core.verification.PropertyVisitDetails;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**

 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "legalVerification")
public class LegalVerification {


    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstitutionId")
    public String institutionId;

    @JsonProperty("aLegalVerificationDetails")
    List<LegalVerificationDetails> legalVerificationDetailsList;


    public interface InsertGrp {
    }
    public interface FetchGrp{

    }

}
