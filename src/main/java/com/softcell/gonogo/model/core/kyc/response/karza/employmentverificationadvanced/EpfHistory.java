package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EpfHistory {

    @JsonProperty("formatted_wage_month")
    private String formatted_wage_month;

    @JsonProperty("totalAmount")
    private String totalAmount;

    @JsonProperty("totalMembers")
    private String totalMembers;

    @JsonProperty("wageMonth")
    private String wageMonth;
}
