package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import java.util.List;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResidentBiometricData {
    @JsonProperty("ts")
    private String ts;

    @JsonProperty("ver")
    private String ver;

    @JsonProperty("Meta")
    private Meta meta;

    @JsonProperty("Bios")
    private List<BiometricElement> bios;

    public static Builder builder() {
        return new Builder();
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<BiometricElement> getBios() {
        return bios;
    }

    public void setBios(List<BiometricElement> bios) {
        this.bios = bios;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ResidentBiometricData{");
        sb.append("ts='").append(ts).append('\'');
        sb.append(", ver='").append(ver).append('\'');
        sb.append(", meta=").append(meta);
        sb.append(", bios=").append(bios);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResidentBiometricData)) return false;
        ResidentBiometricData that = (ResidentBiometricData) o;
        return Objects.equal(getTs(), that.getTs()) &&
                Objects.equal(getVer(), that.getVer()) &&
                Objects.equal(getMeta(), that.getMeta()) &&
                Objects.equal(getBios(), that.getBios());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getTs(), getVer(), getMeta(), getBios());
    }

    public static class Builder {
        private ResidentBiometricData residentBiometricData = new ResidentBiometricData();

        public ResidentBiometricData build() {
            return residentBiometricData;
        }

        public Builder ts(String ts) {
            this.residentBiometricData.ts = ts;
            return this;
        }

        public Builder ver(String ver) {
            this.residentBiometricData.ver = ver;
            return this;
        }

        public Builder meta(Meta meta) {
            this.residentBiometricData.meta = meta;
            return this;
        }

        public Builder bios(List<BiometricElement> bios) {
            this.residentBiometricData.bios = bios;
            return this;
        }
    }


}
