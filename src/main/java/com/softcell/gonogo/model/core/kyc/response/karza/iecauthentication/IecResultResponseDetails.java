package com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 * Description :
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IecResultResponseDetails {

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("IE Code")
    private String IE_Code;

    @JsonProperty("Address")
    private String Address;

    @JsonProperty("No_of_branches")
    private String No_of_branches;

    @JsonProperty("IEC Status")
    private String IEC_Status;

    @JsonProperty("PAN")
    private String PAN;

}