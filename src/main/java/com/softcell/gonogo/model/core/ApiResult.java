package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by ssguser on 28/10/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResult {

    @JsonProperty("sName")
    private String name;

    @JsonProperty("bDobMatch")
    private Boolean dobMatch;

    @JsonProperty("bDuplicate")
    private Boolean duplicate;

    @JsonProperty("bActive")
    private Boolean active;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("bNameMatch")
    private Boolean nameMatch;

}
