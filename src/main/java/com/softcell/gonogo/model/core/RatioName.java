package com.softcell.gonogo.model.core;

/**
 * Created by yogesh on 19/2/18.
 */
public enum RatioName {
    LEVERAGE,PROFITABILITY,WORKING_CAPITAL,TURNOVER,LIQUIDITY,OTHER_RATIO
}
