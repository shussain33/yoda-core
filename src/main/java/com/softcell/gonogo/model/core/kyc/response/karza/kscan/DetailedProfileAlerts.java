package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DetailedProfileAlerts {

    @JsonProperty("companyNotFiled5INV")
    private String companyNotFiled5INV;

    @JsonProperty("companyStrikedOff")
    private String companyStrikedOff;

    @JsonProperty("companyUnderStrikeOff")
    private String companyUnderStrikeOff;

    @JsonProperty("defaulterCompany")
    private String defaulterCompany;

    @JsonProperty("defaulterDirector")
    private String defaulterDirector;

    @JsonProperty("disqualifiedDirector")
    private String disqualifiedDirector;

    @JsonProperty("dormantCompany")
    private String dormantCompany;

    @JsonProperty("fiuRedFlaggedCompany")
    private String fiuRedFlaggedCompany;

    @JsonProperty("formerBusinesses")
    private List<String> formerBusinesses;

    @JsonProperty("formerDirectors")
    private List<String> formerDirectors;

    @JsonProperty("mlm")
    private String mlm;

    @JsonProperty("prosecutions")
    private List<String> prosecutions;

    @JsonProperty("shellCompany")
    private String shellCompany;

    @JsonProperty("vanishingCompany")
    private String vanishingCompany;

}