/**
 * kishorp10:42:00 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

import java.util.Date;

/**
 * @author kishorp
 */
public class CroJustification implements Comparable<CroJustification>{

    @JsonProperty("sJCode")
    private String justificationCode;

    @JsonProperty("sDescrip")
    private String description;

    @JsonProperty("sDocName")
    private String documentName;

    @JsonProperty("sSubTo")
    private String subjectTo;

    @JsonProperty("sRemark")
    private String remark;

    @JsonProperty("sCroID")
    private String croID;

    @JsonProperty("sCase")
    private String decisionCase;

    @JsonProperty("dtUpdateDate")
    private Date croJustificationUpdateDate = new Date();

    @JsonProperty("sMasterRemark")
    private String masterRemark;

    @JsonProperty("sRole")
    private String role;

    public String getCroID() {
        return croID;
    }
    public void setCroID(String croID) {
        this.croID = croID;
    }

    public String getDecisionCase() {
        return decisionCase;
    }

    public void setDecisionCase(String decisionCase) {
        this.decisionCase = decisionCase;
    }

    public Date getCroJustificationUpdateDate() {
        return croJustificationUpdateDate;
    }

    public void setCroJustificationUpdateDate(Date croJustificationUpdateDate) {
        this.croJustificationUpdateDate = croJustificationUpdateDate;
    }

    public String getSubjectTo() {
        return subjectTo;
    }

    public void setSubjectTo(String subjectTo) {
        this.subjectTo = subjectTo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getJustificationCode() {
        return justificationCode;
    }

    public void setJustificationCode(String justificationCode) {
        this.justificationCode = justificationCode;
    }

    public String getDescription() {
        if(null != description){
            description = description.replaceAll("\\r\\n|\\r|\\n|\\t", "");
        }
        return description;
    }

    public void setDescription(String description) {
        if(null != description){
            description = description.replaceAll("\\r\\n|\\r|\\n|\\t", "");
        }
        this.description = description;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }


    public String getMasterRemark() {
        return masterRemark;
    }

    public void setMasterRemark(String masterRemark) {
        this.masterRemark = masterRemark;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CroJustification)) return false;
        CroJustification that = (CroJustification) o;
        return Objects.equal(getJustificationCode(), that.getJustificationCode()) &&
                Objects.equal(getDescription(), that.getDescription()) &&
                Objects.equal(getDocumentName(), that.getDocumentName()) &&
                Objects.equal(getSubjectTo(), that.getSubjectTo()) &&
                Objects.equal(getRemark(), that.getRemark()) &&
                Objects.equal(getCroID(), that.getCroID()) &&
                Objects.equal(getDecisionCase(), that.getDecisionCase()) &&
                Objects.equal(getCroJustificationUpdateDate(), that.getCroJustificationUpdateDate()) &&
                Objects.equal(getMasterRemark(), that.getMasterRemark()) &&
                Objects.equal(getRole(), that.getRole());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getJustificationCode(), getDescription(), getDocumentName(), getSubjectTo(), getRemark(), getCroID(), getDecisionCase(), getCroJustificationUpdateDate(), getMasterRemark(), getRole());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CroJustification{");
        sb.append("justificationCode='").append(justificationCode).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", documentName='").append(documentName).append('\'');
        sb.append(", subjectTo='").append(subjectTo).append('\'');
        sb.append(", remark='").append(remark).append('\'');
        sb.append(", croID='").append(croID).append('\'');
        sb.append(", decisionCase='").append(decisionCase).append('\'');
        sb.append(", croJustificationUpdateDate=").append(croJustificationUpdateDate).append('\'');
        sb.append(", masterRemark='").append(masterRemark);
        sb.append(", role='").append(role);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(CroJustification croJustification) {
        return this.croJustificationUpdateDate.compareTo(croJustification.getCroJustificationUpdateDate());
    }
}
