package com.softcell.gonogo.model.core.kyc.request.pan;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
public class PanDetails {

    @JsonProperty("PAN-NUMBER")
    private String panNumber;

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PanDetails{");
        sb.append("panNumber='").append(panNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PanDetails)) return false;
        PanDetails that = (PanDetails) o;
        return Objects.equal(getPanNumber(), that.getPanNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPanNumber());
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private PanDetails panDetails = new PanDetails();

        public PanDetails build(){
            return this.panDetails;
        }

        public Builder panNumber(String panNumber){
            panDetails.setPanNumber(panNumber);
            return this;
        }
    }
}
