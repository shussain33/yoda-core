package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 05/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SearchByNameResponseDetails {

    result result;

    public class result {
        @JsonProperty("score")
        private String score;

        @JsonProperty("cin")
        private String cin;

        @JsonProperty("comapany_name")
        private String comapany_name;

    }

}