package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ssg408 on 19/6/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "legalVerification")
public class LegalVerificationDetails extends ThirdPartyVerification  {

    @JsonProperty("sCollId")
    private String collateralId;

    @JsonProperty("sPropNature")
    private String natureOfProp;

    @JsonProperty("oLegalVeriInput")
    private LegalVerificationInput legalVerificationInput;

    @JsonProperty("oLegalVeriOutput")
    private LegalVerificationOutput legalVerificationOutput;

}
