package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Reference {

	/**
	 * @author Deepak Bhargava
	 *
	 *
	 */
	@XmlAttribute(name="URI")
	private String uri;
	@XmlElement(name="Transforms")
	private Transforms transforms;
	@XmlElement(name="DigestMethod")
	private DigestMethod digestedMethod;
	@XmlElement(name="DigestValue")
	private String digestedvalue;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public Transforms getTransforms() {
		return transforms;
	}
	public void setTransforms(Transforms transforms) {
		this.transforms = transforms;
	}
	public DigestMethod getDigestedMethod() {
		return digestedMethod;
	}
	public void setDigestedMethod(DigestMethod digestedMethod) {
		this.digestedMethod = digestedMethod;
	}
	public String getDigestedvalue() {
		return digestedvalue;
	}
	public void setDigestedvalue(String digestedvalue) {
		this.digestedvalue = digestedvalue;
	}
	@Override
	public String toString() {
		return "Reference [uri=" + uri + ", transforms=" + transforms
				+ ", digestedMethod=" + digestedMethod + ", digestedvalue="
				+ digestedvalue + "]";
	}
	
	
}
