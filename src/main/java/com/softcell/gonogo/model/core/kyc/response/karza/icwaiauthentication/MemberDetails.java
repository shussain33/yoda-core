package com.softcell.gonogo.model.core.kyc.response.karza.icwaiauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MemberDetails {

    @JsonProperty("City")
    private String City;

    @JsonProperty("MemberNo")
    private String MemberNo;

    @JsonProperty("Pin")
    private String Pin;

    @JsonProperty("MemberName")
    private String MemberName;

    @JsonProperty("State")
    private String State;

    @JsonProperty("Address")
    private String Address;

}