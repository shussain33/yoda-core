package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author vinodk
 */
public class ExtendedWarranty {

    @JsonProperty("bOpted")
    private boolean opted;
    @JsonProperty("oWarrantyTenor")
    private WarrantyTenor warrantyTenor;
    @JsonProperty("dWarrantyAmount")
    private double warrantyAmount;
    //Above enum type{@PaymentMethod} is used to set the value here.
    @JsonProperty("sPaymentMethod")
    private String paymentMethod;
    @JsonProperty("sProductSerialNumber")
    private String productSerialNumber;

    /**
     * @return the opted
     */
    public boolean isOpted() {
        return opted;
    }

    /**
     * @param opted the opted to set
     */
    public void setOpted(boolean opted) {
        this.opted = opted;
    }

    /**
     * @return the warrantyTenor
     */
    public WarrantyTenor getWarrantyTenor() {
        return warrantyTenor;
    }

    /**
     * @param warrantyTenor the warrantyTenor to set
     */
    public void setWarrantyTenor(WarrantyTenor warrantyTenor) {
        this.warrantyTenor = warrantyTenor;
    }

    /**
     * @return the warrantyAmount
     */
    public double getWarrantyAmount() {
        return warrantyAmount;
    }

    /**
     * @param warrantyAmount the warrantyAmount to set
     */
    public void setWarrantyAmount(double warrantyAmount) {
        this.warrantyAmount = warrantyAmount;
    }

    /**
     * @return the paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * @param paymentMethod the paymentMethod to set
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * @return the productSerialNumber
     */
    public String getProductSerialNumber() {
        return productSerialNumber;
    }

    /**
     * @param productSerialNumber the productSerialNumber to set
     */
    public void setProductSerialNumber(String productSerialNumber) {
        this.productSerialNumber = productSerialNumber;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ExtendedWarranty [opted=");
        builder.append(opted);
        builder.append(", warrantyTenor=");
        builder.append(warrantyTenor);
        builder.append(", warrantyAmount=");
        builder.append(warrantyAmount);
        builder.append(", paymentMethod=");
        builder.append(paymentMethod);
        builder.append(", productSerialNumber=");
        builder.append(productSerialNumber);
        builder.append("]");
        return builder.toString();
    }

    public static enum PaymentMethod {
        DOWN_PAYMENT("DOWN PAYMENT"),
        EMI("EMI");

        String value;

        private PaymentMethod(String val) {
            this.value = val;
        }

        /**
         * @return the value
         */
        public String getValue() {
            return value;
        }
    }
}

class WarrantyTenor {

    @JsonProperty("iYears")
    private int years;

    @JsonProperty("iMonths")
    private int months;

    /**
     * @return the years
     */
    public int getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(int years) {
        this.years = years;
    }

    /**
     * @return the months
     */
    public int getMonths() {
        return months;
    }

    /**
     * @param months the months to set
     */
    public void setMonths(int months) {
        this.months = months;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WarrantyTenor [years=");
        builder.append(years);
        builder.append(", months=");
        builder.append(months);
        builder.append("]");
        return builder.toString();
    }
}
