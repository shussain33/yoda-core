package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced.EmploymentVerificationAdvancedResponseDetails;
import lombok.Data;

import java.util.ArrayList;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmploymentVerificationAdvancedResponse {
    @JsonProperty("path")
    private String path;

    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("statusCode")
    private int statusCode;

    @JsonProperty("payload")
    private EmploymentVerificationAdvancedResponseDetails payload;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("error")
    private String error;

    @JsonProperty("errors")
    private ArrayList<Error> errors;

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;
}
