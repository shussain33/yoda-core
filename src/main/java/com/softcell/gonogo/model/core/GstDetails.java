package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yogeshb on 19/6/17.
 */

public class GstDetails implements Serializable {

    @JsonProperty("bIsGstEntered")
    private boolean gstDetailsEntered;

    @JsonProperty("sGstNumber")
    private String gstNumber;

    @JsonProperty("sEmailId")
    private String emailId;

    @JsonProperty("oAddress")
    private CustomerAddress address;

    @JsonProperty("dtFrom")
    private Date fromDate;

    @JsonProperty("dtTo")
    private Date toDate;

    public boolean isGstDetailsEntered() {
        return gstDetailsEntered;
    }

    public void setGstDetailsEntered(boolean gstDetailsEntered) {
        this.gstDetailsEntered = gstDetailsEntered;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public CustomerAddress getAddress() {
        return address;
    }

    public void setAddress(CustomerAddress address) {
        this.address = address;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GstDetails{");
        sb.append("gstDetailsEntered=").append(gstDetailsEntered);
        sb.append(", gstNumber='").append(gstNumber).append('\'');
        sb.append(", emailId='").append(emailId).append('\'');
        sb.append(", address=").append(address);
        sb.append(", fromDate=").append(fromDate);
        sb.append(", toDate=").append(toDate);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GstDetails that = (GstDetails) o;

        if (gstDetailsEntered != that.gstDetailsEntered) return false;
        if (gstNumber != null ? !gstNumber.equals(that.gstNumber) : that.gstNumber != null) return false;
        if (emailId != null ? !emailId.equals(that.emailId) : that.emailId != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (fromDate != null ? !fromDate.equals(that.fromDate) : that.fromDate != null) return false;
        return toDate != null ? toDate.equals(that.toDate) : that.toDate == null;
    }

    @Override
    public int hashCode() {
        int result = (gstDetailsEntered ? 1 : 0);
        result = 31 * result + (gstNumber != null ? gstNumber.hashCode() : 0);
        result = 31 * result + (emailId != null ? emailId.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (fromDate != null ? fromDate.hashCode() : 0);
        result = 31 * result + (toDate != null ? toDate.hashCode() : 0);
        return result;
    }
}
