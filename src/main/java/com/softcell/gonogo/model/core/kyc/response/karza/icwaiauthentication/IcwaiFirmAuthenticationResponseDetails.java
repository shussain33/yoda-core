package com.softcell.gonogo.model.core.kyc.response.karza.icwaiauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IcwaiFirmAuthenticationResponseDetails {

    @JsonProperty("approvalDate")
    private String approvalDate;

    @JsonProperty("FirmType")
    private String FirmType;

    @JsonProperty("FirmName")
    private String FirmName;

    @JsonProperty("Pin")
    private String Pin;

    @JsonProperty("City")
    private String City;

    @JsonProperty("reConDate")
    private String reConDate;

    @JsonProperty("Region")
    private String Region;

    @JsonProperty("Address")
    private String Address;

    @JsonProperty("deedDt")
    private String deedDt;

    @JsonProperty("State")
    private String State;

    @JsonProperty("Contact")
    private String Contact;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("ldt")
    private String ldt;

    @JsonProperty("Dist")
    private String Dist;

    @JsonProperty("email")
    private String email;

    @JsonProperty("MemberDetails")
    public List<MemberDetails> memberDetailsList;

}