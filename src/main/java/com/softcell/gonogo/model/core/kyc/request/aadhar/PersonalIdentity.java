package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonalIdentity {

    @JsonProperty("MATCH-STRATEGY")
    private String matchStrategy;

    @JsonProperty("MATCH-VALUE")
    private String matchValue;

    @JsonProperty("NAME")
    private String name;

    @JsonProperty("LNAME")
    private String lname;

    @JsonProperty("LMV")
    private String lmv;

    @JsonProperty("GENDER")
    private String gender;

    @JsonProperty("DATE-OF-BIRTH")
    private String dateOfBirth;

    @JsonProperty("DATE-OF-BIRTH-TYPE")
    private String dateOfBirthType;

    @JsonProperty("AGE")
    private String age;

    @JsonProperty("PHONE")
    private String phone;

    @JsonProperty("EMAIL")
    private String email;

    public static Builder builder() {
        return new Builder();
    }

    public String getMatchStrategy() {
        return matchStrategy;
    }

    public void setMatchStrategy(String matchStrategy) {
        this.matchStrategy = matchStrategy;
    }

    public String getMatchValue() {
        return matchValue;
    }

    public void setMatchValue(String matchValue) {
        this.matchValue = matchValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getLmv() {
        return lmv;
    }

    public void setLmv(String lmv) {
        this.lmv = lmv;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfBirthType() {
        return dateOfBirthType;
    }

    public void setDateOfBirthType(String dateOfBirthType) {
        this.dateOfBirthType = dateOfBirthType;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PersonalIdentity{");
        sb.append("matchStrategy='").append(matchStrategy).append('\'');
        sb.append(", matchValue='").append(matchValue).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", lname='").append(lname).append('\'');
        sb.append(", lmv='").append(lmv).append('\'');
        sb.append(", gender='").append(gender).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", dateOfBirthType='").append(dateOfBirthType).append('\'');
        sb.append(", age='").append(age).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonalIdentity)) return false;
        PersonalIdentity that = (PersonalIdentity) o;
        return Objects.equal(getMatchStrategy(), that.getMatchStrategy()) &&
                Objects.equal(getMatchValue(), that.getMatchValue()) &&
                Objects.equal(getName(), that.getName()) &&
                Objects.equal(getLname(), that.getLname()) &&
                Objects.equal(getLmv(), that.getLmv()) &&
                Objects.equal(getGender(), that.getGender()) &&
                Objects.equal(getDateOfBirth(), that.getDateOfBirth()) &&
                Objects.equal(getDateOfBirthType(), that.getDateOfBirthType()) &&
                Objects.equal(getAge(), that.getAge()) &&
                Objects.equal(getPhone(), that.getPhone()) &&
                Objects.equal(getEmail(), that.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getMatchStrategy(), getMatchValue(), getName(), getLname(), getLmv(), getGender(), getDateOfBirth(), getDateOfBirthType(), getAge(), getPhone(), getEmail());
    }

    public static class Builder {
        private PersonalIdentity personalIdentity = new PersonalIdentity();

        public PersonalIdentity build() {
            return personalIdentity;
        }

        public Builder matchStrategy(String matchStrategy) {
            this.personalIdentity.matchStrategy = matchStrategy;
            return this;
        }


        public Builder matchValue(String matchValue) {
            this.personalIdentity.matchValue = matchValue;
            return this;
        }

        public Builder name(String name) {
            this.personalIdentity.name = name;
            return this;
        }

        public Builder lname(String lname) {
            this.personalIdentity.lname = lname;
            return this;
        }

        public Builder lmv(String lmv) {
            this.personalIdentity.lmv = lmv;
            return this;
        }

        public Builder gender(String gender) {
            this.personalIdentity.gender = gender;
            return this;
        }

        public Builder dateOfBirth(String dateOfBirth) {
            this.personalIdentity.dateOfBirth = dateOfBirth;
            return this;
        }

        public Builder dateOfBirthType(String dateOfBirthType) {
            this.personalIdentity.dateOfBirthType = dateOfBirthType;
            return this;
        }

        public Builder age(String age) {
            this.personalIdentity.age = age;
            return this;
        }

        public Builder phone(String phone) {
            this.personalIdentity.phone = phone;
            return this;
        }

        public Builder email(String email) {
            this.personalIdentity.email = email;
            return this;
        }
    }
}
