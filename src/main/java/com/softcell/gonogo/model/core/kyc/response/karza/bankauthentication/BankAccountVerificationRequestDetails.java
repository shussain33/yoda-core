package com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankAccountVerificationRequestDetails {

    @JsonProperty("ifsc")
    private String ifsc;

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("accountNumber")
    private String accountNumber;

}