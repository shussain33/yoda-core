package com.softcell.gonogo.model.core.kyc.request.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by yogesh Khandare on 6/10/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KarzaClientRequest {

    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sDocumentId")
    @NotEmpty(groups = {VoterIdGrp.class,PanCardGrp.class,ElectricityBillGrp.class,DlGrp.class})
    private String documentId;

    @JsonProperty("sDocumentType")
    @NotEmpty(groups = {VoterIdGrp.class,PanCardGrp.class,ElectricityBillGrp.class,DlGrp.class})
    private String documentType;

    @JsonProperty("sApplicantDob")
    @NotEmpty(groups = {DlGrp.class})
    private String applicantDob;

    @JsonProperty("sServiceProvider")
    @NotEmpty(groups = {ElectricityBillGrp.class})
    private String serviceProvider;

    public interface VoterIdGrp {
    }

    public interface PanCardGrp {
    }

    public interface ElectricityBillGrp {
    }

    public interface DlGrp {
    }

}
