package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by suhasini on 22/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PropertyVerificationInput {

    @JsonProperty("oAddress")
    private CustomerAddress address;

    @JsonProperty("aPhone")
    private List<Phone> phone;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("sVerificationAgency")
    private String verificationAgency;

    @JsonProperty("oRemarks")
    private Remark remarks;

}
