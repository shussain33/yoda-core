package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by ssg0302 on 30/8/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IdentityDetails {

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("date_of_birth")
    private String date_of_birth;

    @JsonProperty("name")
    private String name;
}
