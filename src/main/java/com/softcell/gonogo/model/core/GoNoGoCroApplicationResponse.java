package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.masters.HierarchyNode;
import com.softcell.gonogo.model.masters.HierarchyUser;
import com.softcell.gonogo.model.response.EligibilityGridOutcome;
import com.softcell.workflow.ModuleConfiguration;

import java.util.List;
import java.util.TreeMap;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GoNoGoCroApplicationResponse extends GoNoGoCustomerApplication {
    @JsonProperty("moduleConfig")
    List<ModuleConfiguration> moduleConfiguration;
    @JsonProperty("aAppImgDtl")
    private List<KycImageDetails> appImgDetail;
    @JsonProperty("oElgGrdOut")
    private EligibilityGridOutcome eligibilityGridOutcome;
    @JsonProperty("oPostIPA")
    private PostIPA postIPA;
    @JsonProperty("oAdditionalDoc")
    private TreeMap<String, List<ImagesDetails>> additionaDocImg;
    @JsonProperty("aHierarchyNodes")
    private List<HierarchyUser> hierarchyDetails;

    public TreeMap<String, List<ImagesDetails>> getAdditionaDocImg() {
        return additionaDocImg;
    }

    public void setAdditionaDocImg(
            TreeMap<String, List<ImagesDetails>> additionaDocImg) {
        this.additionaDocImg = additionaDocImg;
    }

    public List<KycImageDetails> getAppImgDetail() {
        return appImgDetail;
    }

    public void setAppImgDetail(List<KycImageDetails> appImgDetail) {
        this.appImgDetail = appImgDetail;
    }

    public EligibilityGridOutcome getEligibilityGridOutcome() {
        return eligibilityGridOutcome;
    }

    public void setEligibilityGridOutcome(
            EligibilityGridOutcome eligibilityGridOutcome) {
        this.eligibilityGridOutcome = eligibilityGridOutcome;
    }

    public PostIPA getPostIPA() {
        return postIPA;
    }

    public void setPostIPA(PostIPA postIPA) {
        this.postIPA = postIPA;
    }

    public List<HierarchyUser> getHierarchyDetails() {        return hierarchyDetails;    }

    public void setHierarchyDetails(List<HierarchyUser> hierarchyDetails) {        this.hierarchyDetails = hierarchyDetails;    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GoNoGoCroApplicationResponse{");
        sb.append("moduleConfiguration=").append(moduleConfiguration);
        sb.append(", appImgDetail=").append(appImgDetail);
        sb.append(", eligibilityGridOutcome=").append(eligibilityGridOutcome);
        sb.append(", postIPA=").append(postIPA);
        sb.append(", additionaDocImg=").append(additionaDocImg);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GoNoGoCroApplicationResponse)) return false;
        if (!super.equals(o)) return false;
        GoNoGoCroApplicationResponse that = (GoNoGoCroApplicationResponse) o;
        return Objects.equal(moduleConfiguration, that.moduleConfiguration) &&
                Objects.equal(getAppImgDetail(), that.getAppImgDetail()) &&
                Objects.equal(getEligibilityGridOutcome(), that.getEligibilityGridOutcome()) &&
                Objects.equal(getPostIPA(), that.getPostIPA()) &&
                Objects.equal(getAdditionaDocImg(), that.getAdditionaDocImg());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), moduleConfiguration, getAppImgDetail(), getEligibilityGridOutcome(), getPostIPA(), getAdditionaDocImg());
    }
}
