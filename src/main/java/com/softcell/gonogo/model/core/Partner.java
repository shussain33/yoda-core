package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;

import java.io.Serializable;
import java.util.List;

/**
 * @author yogeshb
 */
public class Partner implements Serializable {

    @JsonProperty("oName")
    private Name name;

    @JsonProperty("aAddr")
    private List<CustomerAddress> addresses;

    @JsonProperty("aContact")
    private List<Phone> contactNumbers;

    @JsonProperty("aKyc")
    private List<Kyc> kycDocuments;

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public List<CustomerAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<CustomerAddress> addresses) {
        this.addresses = addresses;
    }

    public List<Phone> getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(List<Phone> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public List<Kyc> getKycDocuments() {
        return kycDocuments;
    }

    public void setKycDocuments(List<Kyc> kycDocuments) {
        this.kycDocuments = kycDocuments;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Partner [name=");
        builder.append(name);
        builder.append(", addresses=");
        builder.append(addresses);
        builder.append(", contactNumbers=");
        builder.append(contactNumbers);
        builder.append(", kycDocuments=");
        builder.append(kycDocuments);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((addresses == null) ? 0 : addresses.hashCode());
        result = prime * result
                + ((contactNumbers == null) ? 0 : contactNumbers.hashCode());
        result = prime * result
                + ((kycDocuments == null) ? 0 : kycDocuments.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Partner other = (Partner) obj;
        if (addresses == null) {
            if (other.addresses != null)
                return false;
        } else if (!addresses.equals(other.addresses))
            return false;
        if (contactNumbers == null) {
            if (other.contactNumbers != null)
                return false;
        } else if (!contactNumbers.equals(other.contactNumbers))
            return false;
        if (kycDocuments == null) {
            if (other.kycDocuments != null)
                return false;
        } else if (!kycDocuments.equals(other.kycDocuments))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
