package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by mahesh on 27/10/17.
 */
@Data
public class CancelApplicationResponse {

    @JsonProperty("dCancelAppCount")
    private double cancelAppCount;

    @JsonProperty("aCancelAppList")
    private List<String> cancelAppList;
}
