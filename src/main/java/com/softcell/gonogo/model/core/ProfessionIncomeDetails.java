package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by archana on 1/2/18.
 */
public class ProfessionIncomeDetails {

    @JsonProperty("sLoanScheme")
    private String loanScheme;

    @JsonProperty("sBusiness")
    private String business;

    @JsonProperty("sProfession")
    private String profession;

    @JsonProperty("sSpecialization")
    private String specialization;

    /*@JsonProperty("iEmiBouncedCountInAYear")
    private int emiBouncedCountInAYear; // TODO to be removed
*/
    @JsonProperty("sRemarks")
    private String remarks;

    /*@JsonProperty("iMobOfBtLoan")
    private int mobOfBtLoan; // TODO to be removed
*/
    @JsonProperty("bNegativeIndustry")
    private boolean negativeIndustry;

    @JsonProperty("bResidenceOutsideGeoLimit")
    private boolean residenceOutsideGeoLimit;

    @JsonProperty("bBusinessOutsideGeoLimit")
    private boolean businessOutsideGeoLimit;

    @JsonProperty("bNegativeArea")
    private boolean negativeArea;

    @JsonProperty("bCollateralWithinCityLimit")
    private boolean collateralWithinCityLimit;

    @JsonProperty("bPsl")
    private boolean psl;

    //for PL
    @JsonProperty("sOfficeEmail")
    private String officeEmail;

    @JsonProperty("sModeOfPaymnt")
    private String modeOfPayment;

    @JsonProperty("sExistEmi")
    private String existingEmi;

    @JsonProperty("dTotalAmtBnkCredit")
    private double totalBankCredit;

    @JsonProperty("dDscr")
    private double dscr;

    @JsonProperty("dSalesTurnover")
    private double salesTurnover;

    //AMBT
    @JsonProperty("sClassification")
    private String classification;

    @JsonProperty("sIndustry")
    private String industry;

    @JsonProperty("sIndustryOther")
    private String industryOther;

    @JsonProperty("iAgeUtilisation")
    private int ageUtilisation;

    @JsonProperty("iCreditLastSixMonth")
    private int creditLastSixMonths;

    @JsonProperty("iNoInwardSixMonth")
    private int noInwardSixMonth;

    @JsonProperty("iAvgBnkBalSixMonth")
    private int abbSixMonth;

    @JsonProperty("iExistUnsecurLoan")
    private int existingUnsecuredLoan;

    @JsonProperty("iPropsedEmi")
    private int proposedEmi;

    @JsonProperty("iPos")
    private int outstandingPrincipal;

    public String getExistingEmi() {    return existingEmi; }

    public void setExistingEmi(String existingEmi) {    this.existingEmi = existingEmi; }

    public String getLoanScheme() {
        return loanScheme;
    }

    public void setLoanScheme(String loanScheme) {
        this.loanScheme = loanScheme;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }


    public String getOfficeEmail() {
        return officeEmail;
    }

    public void setOfficeEmail(String officeEmail) {
        this.officeEmail = officeEmail;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }


    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isNegativeIndustry() {
        return negativeIndustry;
    }

    public void setNegativeIndustry(boolean negativeIndustry) {
        this.negativeIndustry = negativeIndustry;
    }

    public boolean isResidenceOutsideGeoLimit() {
        return residenceOutsideGeoLimit;
    }

    public void setResidenceOutsideGeoLimit(boolean residenceOutsideGeoLimit) {
        this.residenceOutsideGeoLimit = residenceOutsideGeoLimit;
    }

    public boolean isBusinessOutsideGeoLimit() {
        return businessOutsideGeoLimit;
    }

    public void setBusinessOutsideGeoLimit(boolean businessOutsideGeoLimit) {
        this.businessOutsideGeoLimit = businessOutsideGeoLimit;
    }

    public boolean isNegativeArea() {
        return negativeArea;
    }

    public void setNegativeArea(boolean negativeArea) {
        this.negativeArea = negativeArea;
    }

    public boolean isCollateralWithinCityLimit() {
        return collateralWithinCityLimit;
    }

    public void setCollateralWithinCityLimit(boolean collateralWithinCityLimit) {
        this.collateralWithinCityLimit = collateralWithinCityLimit;
    }

    public boolean isPsl() {
        return psl;
    }

    public void setPsl(boolean psl) {
        this.psl = psl;
    }

    public double getDscr() {
        return dscr;
    }

    public void setDscr(double dscr) {
        this.dscr = dscr;
    }

    public double getTotalBankCredit() {
        return totalBankCredit;
    }

    public void setTotalBankCredit(double totalBankCredit) {
        this.totalBankCredit = totalBankCredit;
    }

    public double getSalesTurnover() {
        return salesTurnover;
    }

    public void setSalesTurnover(double salesTurnover) {
        this.salesTurnover = salesTurnover;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getIndustryOther() {
        return industryOther;
    }

    public void setIndustryOther(String industryOther) {
        this.industryOther = industryOther;
    }

    public int getAgeUtilisation() {
        return ageUtilisation;
    }

    public void setAgeUtilisation(int ageUtilisation) {
        this.ageUtilisation = ageUtilisation;
    }

    public int getCreditLastSixMonths() {
        return creditLastSixMonths;
    }

    public void setCreditLastSixMonths(int creditLastSixMonths) {
        this.creditLastSixMonths = creditLastSixMonths;
    }

    public int getNoInwardSixMonth() {
        return noInwardSixMonth;
    }

    public void setNoInwardSixMonth(int noInwardSixMonth) {
        this.noInwardSixMonth = noInwardSixMonth;
    }

    public int getAbbSixMonth() {
        return abbSixMonth;
    }

    public void setAbbSixMonth(int abbSixMonth) {
        this.abbSixMonth = abbSixMonth;
    }

    public int getExistingUnsecuredLoan() {
        return existingUnsecuredLoan;
    }

    public void setExistingUnsecuredLoan(int existingUnsecuredLoan) {
        this.existingUnsecuredLoan = existingUnsecuredLoan;
    }

    public int getProposedEmi() {
        return proposedEmi;
    }

    public void setProposedEmi(int proposedEmi) {
        this.proposedEmi = proposedEmi;
    }

    public int getOutstandingPrincipal() {
        return outstandingPrincipal;
    }

    public void setOutstandingPrincipal(int outstandingPrincipal) {
        this.outstandingPrincipal = outstandingPrincipal;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProfessionIncomeDetails{");
        sb.append("loanScheme='").append(loanScheme).append('\'');
        sb.append(", business='").append(business).append('\'');
        sb.append(", profession='").append(profession).append('\'');
        sb.append(", specialization='").append(specialization).append('\'');
        sb.append(", remarks='").append(remarks).append('\'');
        sb.append(", negativeIndustry=").append(negativeIndustry);
        sb.append(", residenceOutsideGeoLimit=").append(residenceOutsideGeoLimit);
        sb.append(", businessOutsideGeoLimit=").append(businessOutsideGeoLimit);
        sb.append(", negativeArea=").append(negativeArea);
        sb.append(", collateralWithinCityLimit=").append(collateralWithinCityLimit);
        sb.append(", psl=").append(psl);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfessionIncomeDetails that = (ProfessionIncomeDetails) o;

        if (negativeIndustry != that.negativeIndustry) return false;
        if (residenceOutsideGeoLimit != that.residenceOutsideGeoLimit) return false;
        if (businessOutsideGeoLimit != that.businessOutsideGeoLimit) return false;
        if (negativeArea != that.negativeArea) return false;
        if (collateralWithinCityLimit != that.collateralWithinCityLimit) return false;
        if (psl != that.psl) return false;
        if (!loanScheme.equals(that.loanScheme)) return false;
        if (!business.equals(that.business)) return false;
        if (!profession.equals(that.profession)) return false;
        if (!specialization.equals(that.specialization)) return false;
        return remarks.equals(that.remarks);
    }

    @Override
    public int hashCode() {
        int result = loanScheme.hashCode();
        result = 31 * result + business.hashCode();
        result = 31 * result + profession.hashCode();
        result = 31 * result + specialization.hashCode();
        result = 31 * result + remarks.hashCode();
        result = 31 * result + (negativeIndustry ? 1 : 0);
        result = 31 * result + (residenceOutsideGeoLimit ? 1 : 0);
        result = 31 * result + (businessOutsideGeoLimit ? 1 : 0);
        result = 31 * result + (negativeArea ? 1 : 0);
        result = 31 * result + (collateralWithinCityLimit ? 1 : 0);
        result = 31 * result + (psl ? 1 : 0);
        return result;
    }
}
