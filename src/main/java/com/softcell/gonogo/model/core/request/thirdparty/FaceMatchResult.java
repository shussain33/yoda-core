package com.softcell.gonogo.model.core.request.thirdparty;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by suraj on 13/10/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FaceMatchResult {

    @JsonProperty("dMatchScore")
    private double matchScore;

    @JsonProperty("dConfidence")
    private double confidence;

    @JsonProperty("sMatch")
    private String match;

    @JsonProperty("sReviewNeeded")
    private String reviewNeeded;
}
