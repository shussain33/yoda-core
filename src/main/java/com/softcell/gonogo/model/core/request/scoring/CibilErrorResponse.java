package com.softcell.gonogo.model.core.request.scoring;

import com.softcell.gonogo.model.core.request.scoring.ErrorDomain;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class CibilErrorResponse {

	//ERRR
	private String dateProcessed;
	private String timeProcessed;
	
	//UR
	private String mbrRefNbr;
	private String invVersion;
	private String invFieldLength;
	private String invTotalLength;
	private String invEnquiryPurpose;
	private String invEnquiryAmount;
	private String invUserPwd;
	private String reqSegMissing;
	private List<String> invEnqDataList;
	private String cibilSystemError;
	private String invSegTag;
	private String invSegOrder;
	private String invFieldTagOrder;
	private List<String>  reqFieldMissingList;
	private String reqResSizeExceeded;
	private String invIpOpMedia;
	
	private List<ErrorDomain> errorDomainList;

	public String getDateProcessed() {
		return dateProcessed;
	}

	public void setDateProcessed(String dateProcessed) {
		this.dateProcessed = dateProcessed;
	}

	public String getTimeProcessed() {
		return timeProcessed;
	}

	public void setTimeProcessed(String timeProcessed) {
		this.timeProcessed = timeProcessed;
	}

	public String getMbrRefNbr() {
		return mbrRefNbr;
	}

	public void setMbrRefNbr(String mbrRefNbr) {
		this.mbrRefNbr = mbrRefNbr;
	}

	public String getInvVersion() {
		return invVersion;
	}

	public void setInvVersion(String invVersion) {
		this.invVersion = invVersion;
	}

	public String getInvFieldLength() {
		return invFieldLength;
	}

	public void setInvFieldLength(String invFieldLength) {
		this.invFieldLength = invFieldLength;
	}

	public String getInvTotalLength() {
		return invTotalLength;
	}

	public void setInvTotalLength(String invTotalLength) {
		this.invTotalLength = invTotalLength;
	}

	public String getInvEnquiryPurpose() {
		return invEnquiryPurpose;
	}

	public void setInvEnquiryPurpose(String invEnquiryPurpose) {
		this.invEnquiryPurpose = invEnquiryPurpose;
	}

	public String getInvEnquiryAmount() {
		return invEnquiryAmount;
	}

	public void setInvEnquiryAmount(String invEnquiryAmount) {
		this.invEnquiryAmount = invEnquiryAmount;
	}

	public String getInvUserPwd() {
		return invUserPwd;
	}

	public void setInvUserPwd(String invUserPwd) {
		this.invUserPwd = invUserPwd;
	}

	public String getReqSegMissing() {
		return reqSegMissing;
	}

	public void setReqSegMissing(String reqSegMissing) {
		this.reqSegMissing = reqSegMissing;
	}

	public List<String> getInvEnqDataList() {
		return invEnqDataList;
	}

	public void setInvEnqDataList(List<String> invEnqDataList) {
		this.invEnqDataList = invEnqDataList;
	}

	public String getCibilSystemError() {
		return cibilSystemError;
	}

	public void setCibilSystemError(String cibilSystemError) {
		this.cibilSystemError = cibilSystemError;
	}

	public String getInvSegTag() {
		return invSegTag;
	}

	public void setInvSegTag(String invSegTag) {
		this.invSegTag = invSegTag;
	}

	public String getInvSegOrder() {
		return invSegOrder;
	}

	public void setInvSegOrder(String invSegOrder) {
		this.invSegOrder = invSegOrder;
	}

	public String getInvFieldTagOrder() {
		return invFieldTagOrder;
	}

	public void setInvFieldTagOrder(String invFieldTagOrder) {
		this.invFieldTagOrder = invFieldTagOrder;
	}

	public List<String> getReqFieldMissingList() {
		return reqFieldMissingList;
	}

	public void setReqFieldMissingList(List<String> reqFieldMissingList) {
		this.reqFieldMissingList = reqFieldMissingList;
	}

	public String getReqResSizeExceeded() {
		return reqResSizeExceeded;
	}

	public void setReqResSizeExceeded(String reqResSizeExceeded) {
		this.reqResSizeExceeded = reqResSizeExceeded;
	}

	public String getInvIpOpMedia() {
		return invIpOpMedia;
	}

	public void setInvIpOpMedia(String invIpOpMedia) {
		this.invIpOpMedia = invIpOpMedia;
	}

	public List<ErrorDomain> getErrorDomainList() {
		return errorDomainList;
	}

	public void setErrorDomainList(List<ErrorDomain> errorDomainList) {
		this.errorDomainList = errorDomainList;
	}

	@Override
	public String toString() {
		return "CibilErrorResponse [dateProcessed=" + dateProcessed
				+ ", timeProcessed=" + timeProcessed + ", mbrRefNbr="
				+ mbrRefNbr + ", invVersion=" + invVersion
				+ ", invFieldLength=" + invFieldLength + ", invTotalLength="
				+ invTotalLength + ", invEnquiryPurpose=" + invEnquiryPurpose
				+ ", invEnquiryAmount=" + invEnquiryAmount + ", invUserPwd="
				+ invUserPwd + ", reqSegMissing=" + reqSegMissing
				+ ", invEnqDataList=" + invEnqDataList + ", cibilSystemError="
				+ cibilSystemError + ", invSegTag=" + invSegTag
				+ ", invSegOrder=" + invSegOrder + ", invFieldTagOrder="
				+ invFieldTagOrder + ", reqFieldMissingList="
				+ reqFieldMissingList + ", reqResSizeExceeded="
				+ reqResSizeExceeded + ", invIpOpMedia=" + invIpOpMedia
				+ ", errorDomainList=" + errorDomainList + "]";
	}

}