package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GstTransactionApiVerifyOtpResponseDetails {

    @JsonProperty("responseStatusCode")
    private String responseStatusCode;

    @JsonProperty("response")
    private String response;
}