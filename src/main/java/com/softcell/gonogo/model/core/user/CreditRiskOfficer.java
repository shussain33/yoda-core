/**
 * kishorp8:49:57 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;

import javax.ws.rs.DefaultValue;

/**
 * @author kishorp
 *
 */
public class CreditRiskOfficer {
    @DefaultValue("")
    @JsonProperty("sName")
    private String name;
    @DefaultValue("")
    @JsonProperty("sCroID")
    private String croId;
    @DefaultValue("")
    @JsonProperty("oPhone")
    private Phone phone;
    @DefaultValue("")
    @JsonProperty("oEmail")
    private Email email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCroId() {
        return croId;
    }

    public void setCroId(String croId) {
        this.croId = croId;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CreditRiskOfficer [name=" + name + ", croId=" + croId
                + ", phone=" + phone + ", email=" + email + "]";
    }


}
