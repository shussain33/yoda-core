package com.softcell.gonogo.model.core.kyc.response.pan;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class PanInfo {
    @JsonProperty("pan")
    private String panNumber;

    @JsonProperty("panStatus")
    private String panStatus;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("middleName")
    private String middleName;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("panTitle")
    private String panTitle;

    @JsonProperty("lastUpdateDate")
    private String lastUpdateDate;

    @JsonProperty("filler1")
    private String filler1;

    @JsonProperty("filler2")
    private String filler2;

    @JsonProperty("filler3")
    private String filler3;

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getPanStatus() {
        return panStatus;
    }

    public void setPanStatus(String panStatus) {
        this.panStatus = panStatus;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getFiller1() {
        return filler1;
    }

    public void setFiller1(String filler1) {
        this.filler1 = filler1;
    }

    public String getFiller2() {
        return filler2;
    }

    public void setFiller2(String filler2) {
        this.filler2 = filler2;
    }

    public String getFiller3() {
        return filler3;
    }

    public void setFiller3(String filler3) {
        this.filler3 = filler3;
    }


    public String getPanTitle() {
        return panTitle;
    }

    public void setPanTitle(String panTitle) {
        this.panTitle = panTitle;
    }

    @Override
    public String toString() {
        return "PanInfo [panNumber=" + panNumber + ", panStatus=" + panStatus
                + ", firstName=" + firstName + ", middleName=" + middleName
                + ", lastName=" + lastName + ", panTitle=" + panTitle
                + ", lastUpdateDate=" + lastUpdateDate + ", filler1=" + filler1
                + ", filler2=" + filler2 + ", filler3=" + filler3 + "]";
    }
}
