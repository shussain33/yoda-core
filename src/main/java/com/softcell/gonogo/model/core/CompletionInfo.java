package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by archana on 12/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompletionInfo {

    @JsonProperty("sUserId")
    private String userId;

    @JsonProperty("sRole")
    private String role;

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("dtCompletedOn")
    private Date completedOn = new Date();

    @JsonProperty("dtUpdatedOn")
    private Date updatedOn = new Date();

    @JsonProperty("sStageChangeTo")
    private String stageChangedTo;

}
