package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by yogesh on 28/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityRequest {

    @JsonProperty("oHeader")
    @NotEmpty(groups = {Header.InsertGroup.class})
    public Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {EligibilityRequest.InsertGroup.class, EligibilityRequest.FetchGroup.class})
    public String refId;

    @JsonProperty("oEligibilityDetails")
    public EligibilityDetails eligibilityDetails;

    public interface InsertGroup {

    }

    public interface FetchGroup {

    }

}

