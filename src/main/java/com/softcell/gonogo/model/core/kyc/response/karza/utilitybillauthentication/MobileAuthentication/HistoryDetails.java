package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class HistoryDetails {

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("payment_date")
    private String payment_date;

    @JsonProperty("payment_type")
    private String payment_type;

}