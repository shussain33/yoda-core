package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GstTransactionStateWiseTransactionResponse {

    @JsonProperty("tax")
    private Double tax;

    @JsonProperty("ttl_cess")
    private Double ttlCess;

    @JsonProperty("ttl_cgst")
    private Double ttlCgst;

    @JsonProperty("ttl_igst")
    private Double ttlIgst;

    @JsonProperty("ttl_rec")
    private Double ttlRec;

    @JsonProperty("ttl_sgst")
    private Double ttlSgst;

    @JsonProperty("ttl_tax")
    private Double ttlTax;

    @JsonProperty("ttl_val")
    private Double ttlVal;

}