package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.ocr.OcrUploadFileDetails;
import com.softcell.gonogo.model.request.FileUploadRequest;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author yogeshb
 */
public class UploadFileDetails {

    @JsonProperty("sStat")
    @NotBlank(groups = {FileUploadRequest.UpdateOrDelete.class, FileUploadRequest.ApproveOrReject.class})
    public String status;
    @JsonProperty("sReason")
    @NotBlank(groups = {FileUploadRequest.UpdateOrDelete.class})
    public String reason;
    @JsonProperty("bDeleted")
    public boolean deleted;
    @JsonProperty("sFileID")
    @NotBlank(groups = {FileUploadRequest.InsertGrp.class, FileUploadRequest.DocumentInsertGrp.class})
    private String fileId;

    // PAN/ADHAR
    @JsonProperty("sFileName")
    @NotBlank(groups = {FileUploadRequest.InsertGrp.class, FileUploadRequest.DocumentInsertGrp.class})
    private String fileName;

    // Section
    @JsonProperty("sFileType")
    @NotBlank(groups = {FileUploadRequest.InsertGrp.class, FileUploadRequest.DocumentInsertGrp.class})
    private String fileType;
    @JsonProperty("sfileData")
    @Transient
    @NotBlank(groups = {FileUploadRequest.InsertGrp.class})
    private String fileData;
    @JsonProperty("sAmazonS3Url")
    private boolean amazonFileUrl;

    @JsonProperty("sFileExtension")
    private String fileExtension;

    @JsonProperty("sDocumentType")
    @NotEmpty(groups = {FileUploadRequest.DocumentInsertGrp.class, FileUploadRequest.DocumentFetchGrp.class})
    private String documentType;

    @JsonProperty("sDocumentSubType")
    @NotBlank(groups = {FileUploadRequest.DocumentInsertGrp.class, FileUploadRequest.DocumentFetchGrp.class})
    private String documentSubType;

    @JsonProperty("bESigned")
    private boolean eSigned;

    @JsonProperty("sRemark")
    private String remark;

    @JsonProperty("sCollateralId")
    private String collateralId;

    @JsonProperty("sApplicantId")
    private String applicantId;

    //DigiPl Fields Started
    @JsonProperty("sImgID")
    private String imageId;

   @JsonProperty("sLatitude")
    private String latitude;

    @JsonProperty("sLongitude")
    private String longitude;

    @JsonProperty("sImageSide")
    private String imageSide;

    @JsonProperty("dtUploadDate")
    public Date uploadDate;

    @JsonProperty("sUserRole")
    private String userRole;

    @JsonProperty("sUserId")
    private String userId;

    @JsonProperty("bIsThumbnail")
    private boolean isThumbnail;

    @JsonProperty("sImgCat")
    private String imageCategory;

    @JsonProperty("sImgTitle")
    private String imageTitle;

    @JsonProperty("aFileTags")
    private List<String> fileTags;

    @JsonProperty("bEncryptBinary")
    private boolean encryptBinary;

    @JsonProperty("sAddressType")
    private String addressType;

    @JsonProperty("bRopedOut")
    public boolean ropedOut;

    @JsonProperty("sRcuStatus")
    private String rcuStatus;

    @JsonProperty("sLocation")
    private String location;

    @JsonProperty("aSection")
    private List<String> sectionList;

    @JsonProperty("aDocValuation")
    private List<String> docValuation;

    @JsonProperty("sOtherDocName")
    private String otherDocName;

    @JsonProperty("bAddToChkList")
    private boolean addToChkList;

    @JsonProperty("bRCUDoc")
    private boolean rcuDoc;

    @JsonProperty("dtImageClickedDate")
    private Date imageClickedDate;

    @JsonProperty("bDeleteFromCheckList")
    public boolean deleteFromCheckList;

    @JsonProperty("bDeleteFromRCU")
    public boolean deleteFromRCU;

    @JsonProperty("documentsNames")
    public List<HashMap<String,String>> documentNames;

    @JsonProperty("documentSectionNames")
    public List<HashMap<String,String>> documentSectionNames;

    @JsonProperty("oUpldDtl")
    public OcrUploadFileDetails ocrUploadFileDetails;

    @JsonProperty("showDropdown")
    public boolean showDropdown;

    @JsonProperty("bOCRDone")
    private boolean ocrDone;

    @JsonProperty("sDocLevel")
    private String level;

    @JsonProperty("sFileUploadSource")
    private String fileUploadSource;

    //This is added for FileX. Json mapping is different from the remark's mapping above.
    @JsonProperty("sRemarks")
    private String remarks;

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isThumbnail() {
        return isThumbnail;
    }

    public void setThumbnail(boolean thumbnail) {
        isThumbnail = thumbnail;
    }

    public String getImageCategory() {
        return imageCategory;
    }

    public void setImageCategory(String imageCategory) {
        this.imageCategory = imageCategory;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public List<String> getFileTags() {
        return fileTags;
    }

    public void setFileTags(List<String> fileTags) {
        this.fileTags = fileTags;
    }

    public boolean isEncryptBinary() {
        return encryptBinary;
    }

    public void setEncryptBinary(boolean encryptBinary) {
        this.encryptBinary = encryptBinary;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public boolean isRopedOut() {
        return ropedOut;
    }

    public void setRopedOut(boolean ropedOut) {
        this.ropedOut = ropedOut;
    }

    public String getRcuStatus() {
        return rcuStatus;
    }

    public void setRcuStatus(String rcuStatus) {
        this.rcuStatus = rcuStatus;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<String> sectionList) {
        this.sectionList = sectionList;
    }

    public List<String> getDocValuation() {
        return docValuation;
    }

    public void setDocValuation(List<String> docValuation) {
        this.docValuation = docValuation;
    }

    public String getOtherDocName() {
        return otherDocName;
    }

    public void setOtherDocName(String otherDocName) {
        this.otherDocName = otherDocName;
    }

    public boolean isAddToChkList() {
        return addToChkList;
    }

    public void setAddToChkList(boolean addToChkList) {
        this.addToChkList = addToChkList;
    }

    public boolean isRcuDoc() {
        return rcuDoc;
    }

    public void setRcuDoc(boolean rcuDoc) {
        this.rcuDoc = rcuDoc;
    }

    public Date getImageClickedDate() {
        return imageClickedDate;
    }

    public void setImageClickedDate(Date imageClickedDate) {
        this.imageClickedDate = imageClickedDate;
    }

    public boolean isDeleteFromCheckList() {
        return deleteFromCheckList;
    }

    public void setDeleteFromCheckList(boolean deleteFromCheckList) {
        this.deleteFromCheckList = deleteFromCheckList;
    }

    public boolean isDeleteFromRCU() {
        return deleteFromRCU;
    }

    public void setDeleteFromRCU(boolean deleteFromRCU) {
        this.deleteFromRCU = deleteFromRCU;
    }

    public List<HashMap<String, String>> getDocumentNames() {
        return documentNames;
    }

    public void setDocumentNames(List<HashMap<String, String>> documentNames) {
        this.documentNames = documentNames;
    }

    public List<HashMap<String, String>> getDocumentSectionNames() {
        return documentSectionNames;
    }

    public void setDocumentSectionNames(List<HashMap<String, String>> documentSectionNames) {
        this.documentSectionNames = documentSectionNames;
    }

    public OcrUploadFileDetails getOcrUploadFileDetails() {
        return ocrUploadFileDetails;
    }

    public void setOcrUploadFileDetails(OcrUploadFileDetails ocrUploadFileDetails) {
        this.ocrUploadFileDetails = ocrUploadFileDetails;
    }

    public boolean isShowDropdown() {
        return showDropdown;
    }

    public void setShowDropdown(boolean showDropdown) {
        this.showDropdown = showDropdown;
    }

    public boolean isOcrDone() {
        return ocrDone;
    }

    public void setOcrDone(boolean ocrDone) {
        this.ocrDone = ocrDone;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getFileUploadSource() {
        return fileUploadSource;
    }

    public void setFileUploadSource(String fileUploadSource) {
        this.fileUploadSource = fileUploadSource;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getImageId() { return imageId; }

    public void setImageId(String imageId) { this.imageId = imageId; }

    public String getLatitude() { return latitude; }

    public void setLatitude(String latitude) { this.latitude = latitude; }

    public String getLongitude() { return longitude; }

    public void setLongitude(String longitude) { this.longitude = longitude; }

    public String getImageSide() { return imageSide; }

    public void setImageSide(String imageSide) { this.imageSide = imageSide; }

    public Date getUploadDate() { return uploadDate; }

    public void setUploadDate(Date uploadDate) { this.uploadDate = uploadDate;}

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileData() {
        return fileData;
    }

    public void setFileData(String fileData) {
        this.fileData = fileData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * @return the amazonFileUrl
     */
    public boolean isAmazonFileUrl() {
        return amazonFileUrl;
    }

    /**
     * @param amazonFileUrl the amazonFileUrl to set
     */
    public void setAmazonFileUrl(boolean amazonFileUrl) {
        this.amazonFileUrl = amazonFileUrl;
    }

    /**
     * @return the fileExtension
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * @param fileExtension the fileExtension to set
     */
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }


    public String getDocumentType() { return documentType; }

    public void setDocumentType(String documentType) { this.documentType = documentType; }

    public String getDocumentSubType() { return documentSubType; }

    public void setDocumentSubType(String documentSubType) { this.documentSubType = documentSubType; }


    public boolean iseSigned() {
        return eSigned;
    }

    public void seteSigned(boolean eSigned) {
        this.eSigned = eSigned;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCollateralId() {
        return collateralId;
    }

    public void setCollateralId(String collateralId) {
        this.collateralId = collateralId;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UploadFileDetails{");
        sb.append("status='").append(status).append('\'');
        sb.append(", reason='").append(reason).append('\'');
        sb.append(", deleted=").append(deleted);
        sb.append(", fileId='").append(fileId).append('\'');
        sb.append(", fileName='").append(fileName).append('\'');
        sb.append(", fileType='").append(fileType).append('\'');
        sb.append(", fileData='").append(fileData).append('\'');
        sb.append(", amazonFileUrl=").append(amazonFileUrl);
        sb.append(", fileExtension='").append(fileExtension).append('\'');
        sb.append(", documentType='").append(documentType).append('\'');
        sb.append(", documentSubType='").append(documentSubType).append('\'');
        sb.append(", eSigned=").append(eSigned);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UploadFileDetails that = (UploadFileDetails) o;

        if (deleted != that.deleted) return false;
        if (amazonFileUrl != that.amazonFileUrl) return false;
        if (eSigned != that.eSigned) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (reason != null ? !reason.equals(that.reason) : that.reason != null) return false;
        if (fileId != null ? !fileId.equals(that.fileId) : that.fileId != null) return false;
        if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null) return false;
        if (fileType != null ? !fileType.equals(that.fileType) : that.fileType != null) return false;
        if (fileData != null ? !fileData.equals(that.fileData) : that.fileData != null) return false;
        if (fileExtension != null ? !fileExtension.equals(that.fileExtension) : that.fileExtension != null)
            return false;
        if (documentType != null ? !documentType.equals(that.documentType) : that.documentType != null) return false;
        return documentSubType != null ? documentSubType.equals(that.documentSubType) : that.documentSubType == null;
    }

    @Override
    public int hashCode() {
        int result = status != null ? status.hashCode() : 0;
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + (deleted ? 1 : 0);
        result = 31 * result + (fileId != null ? fileId.hashCode() : 0);
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + (fileType != null ? fileType.hashCode() : 0);
        result = 31 * result + (fileData != null ? fileData.hashCode() : 0);
        result = 31 * result + (amazonFileUrl ? 1 : 0);
        result = 31 * result + (fileExtension != null ? fileExtension.hashCode() : 0);
        result = 31 * result + (documentType != null ? documentType.hashCode() : 0);
        result = 31 * result + (documentSubType != null ? documentSubType.hashCode() : 0);
        result = 31 * result + (eSigned ? 1 : 0);
        return result;
    }
}
