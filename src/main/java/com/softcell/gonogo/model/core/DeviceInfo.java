package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.logger.GeoLocation;

import java.io.Serializable;

/**
 * @author yogeshb
 */
public class DeviceInfo implements Serializable {

    @JsonProperty("sIMEI")
    private String imeiNumber;

    @JsonProperty("sDeviceID")
    private String deviceID;

    @JsonProperty("sDeviceManufacturer")
    private String deviceManufacturer;

    @JsonProperty("sDeviceModel")
    private String deviceModel;

    @JsonProperty("oGeoLocation")
    private GeoLocation geoLocation;

    @JsonProperty("sIpAddress")
    private String ipAddress;

    @JsonProperty("oBrowserDetails")
    private BrowserDetails browserDetails;

    @JsonProperty("oOsDetails")
    private OSDetails osDetails;

    /**
     * @return the imeiNumber
     */
    public String getImeiNumber() {
        return imeiNumber;
    }

    /**
     * @param imeiNumber the imeiNumber to set
     */
    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    /**
     * @return the deviceID
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * @param deviceID the deviceID to set
     */
    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    /**
     * @return the geoLocation
     */
    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    /**
     * @param geoLocation the geoLocation to set
     */
    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress the ipAddress to set
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the browserDetails
     */
    public BrowserDetails getBrowserDetails() {
        return browserDetails;
    }

    /**
     * @param browserDetails the browserDetails to set
     */
    public void setBrowserDetails(BrowserDetails browserDetails) {
        this.browserDetails = browserDetails;
    }

    /**
     * @return the osDetails
     */
    public OSDetails getOsDetails() {
        return osDetails;
    }

    /**
     * @param osDetails the osDetails to set
     */
    public void setOsDetails(OSDetails osDetails) {
        this.osDetails = osDetails;
    }

    /**
     * @return the deviceManufacturer
     */
    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    /**
     * @param deviceManufacturer the deviceManufacturer to set
     */
    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    /**
     * @return the deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * @param deviceModel the deviceModel to set
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DeviceInfo [imeiNumber=");
        builder.append(imeiNumber);
        builder.append(", deviceID=");
        builder.append(deviceID);
        builder.append(", deviceManufacturer=");
        builder.append(deviceManufacturer);
        builder.append(", deviceModel=");
        builder.append(deviceModel);
        builder.append(", geoLocation=");
        builder.append(geoLocation);
        builder.append(", ipAddress=");
        builder.append(ipAddress);
        builder.append(", browserDetails=");
        builder.append(browserDetails);
        builder.append(", osDetails=");
        builder.append(osDetails);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((browserDetails == null) ? 0 : browserDetails.hashCode());
        result = prime * result
                + ((deviceID == null) ? 0 : deviceID.hashCode());
        result = prime
                * result
                + ((deviceManufacturer == null) ? 0 : deviceManufacturer
                .hashCode());
        result = prime * result
                + ((deviceModel == null) ? 0 : deviceModel.hashCode());
        result = prime * result
                + ((geoLocation == null) ? 0 : geoLocation.hashCode());
        result = prime * result
                + ((imeiNumber == null) ? 0 : imeiNumber.hashCode());
        result = prime * result
                + ((ipAddress == null) ? 0 : ipAddress.hashCode());
        result = prime * result
                + ((osDetails == null) ? 0 : osDetails.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeviceInfo other = (DeviceInfo) obj;
        if (browserDetails == null) {
            if (other.browserDetails != null)
                return false;
        } else if (!browserDetails.equals(other.browserDetails))
            return false;
        if (deviceID == null) {
            if (other.deviceID != null)
                return false;
        } else if (!deviceID.equals(other.deviceID))
            return false;
        if (deviceManufacturer == null) {
            if (other.deviceManufacturer != null)
                return false;
        } else if (!deviceManufacturer.equals(other.deviceManufacturer))
            return false;
        if (deviceModel == null) {
            if (other.deviceModel != null)
                return false;
        } else if (!deviceModel.equals(other.deviceModel))
            return false;
        if (geoLocation == null) {
            if (other.geoLocation != null)
                return false;
        } else if (!geoLocation.equals(other.geoLocation))
            return false;
        if (imeiNumber == null) {
            if (other.imeiNumber != null)
                return false;
        } else if (!imeiNumber.equals(other.imeiNumber))
            return false;
        if (ipAddress == null) {
            if (other.ipAddress != null)
                return false;
        } else if (!ipAddress.equals(other.ipAddress))
            return false;
        if (osDetails == null) {
            if (other.osDetails != null)
                return false;
        } else if (!osDetails.equals(other.osDetails))
            return false;
        return true;
    }


}
