package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication.GstAdadr;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication.GstPradr;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GstAuthenticationResponseDetails {

    @JsonProperty("mbr")
    private List<String> mbr;

    @JsonProperty("cmpRt")
    private String cmpRt;

    @JsonProperty("pradr")
    public GstPradr pradr;

    @JsonProperty("contacted")
    private String contacted;

    @JsonProperty("stj")
    private String stj;

    @JsonProperty("ppr")
    private String ppr;

    @JsonProperty("dty")
    private String dty;

    @JsonProperty("rgdt")
    private String rgdt;

    @JsonProperty("ctb")
    private String ctb;

    @JsonProperty("sts")
    private String sts;

    @JsonProperty("gstin")
    private String gstin;

    @JsonProperty("adadr")
    public List<GstAdadr> adadrList;

    @JsonProperty("lgnm")
    private String lgnm;

    @JsonProperty("nba")
    private List<String> nbaDetailsList;

    @JsonProperty("ctj")
    private String ctj;

    @JsonProperty("cxdt")
    private String cxdt;

    @JsonProperty("stjCd")
    private String stjCd;

    @JsonProperty("canFlag")
    private String canFlag;

    @JsonProperty("lstupdt")
    private String lstupdt;

    @JsonProperty("ctjCd")
    private String ctjCd;

    @JsonProperty("tradeNam")
    private String tradeNam;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
}