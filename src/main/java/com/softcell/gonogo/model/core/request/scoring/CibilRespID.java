package com.softcell.gonogo.model.core.request.scoring;


public class CibilRespID {

    private String idType;

    private String idValue;

    private String issueDate;

    private String expiryDate;

    private String enrichedThroughtEnquiry;

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdValue() {
        return idValue;
    }

    public void setIdValue(String idValue) {
        this.idValue = idValue;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getEnrichedThroughtEnquiry() {
        return enrichedThroughtEnquiry;
    }

    public void setEnrichedThroughtEnquiry(String enrichedThroughtEnquiry) {
        this.enrichedThroughtEnquiry = enrichedThroughtEnquiry;
    }

    @Override
    public String toString() {
        return "CibilRespID [idType=" + idType + ", idValue=" + idValue
                + ", issueDate=" + issueDate + ", expiryDate=" + expiryDate
                + ", enrichedThroughtEnquiry=" + enrichedThroughtEnquiry + "]";
    }


}
