package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.NetDisbursalDetailsMaster;
import lombok.Data;

import java.util.List;

/**
 * Created by mahesh on 26/10/17.
 */
@Data
public class NetDisbursalAmtUpdateResponse {

    @JsonProperty("aNetDisbursalDetails")
    List<NetDisbursalDetailsMaster> netDisbursalDetailsList;

    @JsonProperty("iNoOfSucceedRecord")
    private int noOfSucceedRecord;

    @JsonProperty("iNoOfFailedRecord")
    private int noOfFailedRecord;

    @JsonProperty("sCsvFileStatus")
    private String csvFileStatus;

    @JsonProperty("sCsvFileErrorDesc")
    private String csvFileErrorDesc;
}
