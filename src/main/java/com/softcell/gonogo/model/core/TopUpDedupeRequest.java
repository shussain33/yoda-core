package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.topup.Applicant;
import com.softcell.gonogo.model.mifin.topup.DedupeInfo;
import com.softcell.gonogo.model.mifin.topup.TopUpDedupeResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TopUpDedupeRequest
{
    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {com.softcell.gonogo.model.request.core.Header.FetchGrp.class})
    private Header header;

    @JsonProperty("sDedupeParameterName")
    public String fieldName;

    @JsonProperty("sDedupeParameterValue")
    public String fieldValue;

    @JsonProperty("sRefId")
    @NotEmpty
    public String refId;

    @JsonProperty("oAppReq")
    public ApplicationRequest applicationRequest;

    @JsonProperty("sUpdatedUserId")
    public String updatedUserId;

    @JsonProperty("aProspectCode")
    @NotEmpty
    public List<String> prospectCodeList;

    @JsonProperty("sLoanDetailTypeParameter")
    @NotEmpty
    public String LoanDetailType;

    @JsonProperty("sLoanDetailTypeValue")
    @NotEmpty
    public String LoanDetailTypeValue;

    @JsonProperty("aCustomerId")
    @NotEmpty
    public List<String> customerIdList;

    @JsonProperty("aApplicantList")
    @NotEmpty
    public List<Applicant> applicantList;

    @JsonProperty("DEDUPEINFO")
    private List<TopUpDedupeResponse> dedupeResponseList;

    /*@JsonProperty("sTopUpType")
    @NotEmpty
    public String topUpType;
*/

}

