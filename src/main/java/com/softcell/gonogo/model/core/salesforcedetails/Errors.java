package com.softcell.gonogo.model.core.salesforcedetails;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dev-intern on 21/7/17.
 */
public class Errors {
    @JsonProperty("sErrorMessage")
    private String errorMessage;
    @JsonProperty("sObjType")
    private String objType;
    @JsonProperty("sObjId")
    private String objId;
    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    public String getObjType() {
        return objType;
    }
    public void setObjType(String objType) {
        this.objType = objType;
    }
    public String getObjId() {
        return objId;
    }
    public void setObjId(String objId) {
        this.objId = objId;
    }
    @Override
    public String toString() {
        return "Errors{" +
                "errorMessage='" + errorMessage + '\'' +
                ", objType='" + objType + '\'' +
                ", objId='" + objId + '\'' +
                '}';
    }
}

