package com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IecBankDetails {

    @JsonProperty("bank_name")
    private String bank_name;

    @JsonProperty("account_type")
    private String account_type;

    @JsonProperty("account_number")
    private String account_number;
}