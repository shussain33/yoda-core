package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yogesh on 9/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Result {

    @JsonProperty("APPLICANT-ID")
    private String applicantId;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("SCORING-REF-ID")
    private String scoringRefId;

    @JsonProperty("DECISION")
    private String decision;

    @JsonProperty("RULES")
    private List<RulesDetails> rules;

    @JsonProperty("ELIGIBILITY-AMOUNT")
    private double eligibilityAmount;

    @JsonProperty("ELIGIBILITY-DECISION")
    private String eligibilityDecision;

    @JsonProperty("SCORE_DATA")
    private ScoreData scoreData;

    @JsonProperty("ELIGIBILITY_RESPONSE")
    private Eligibility eligibilityResponse;

    @JsonProperty("DERIVED_FIELDS")
    private Object derivedFields;

    @JsonProperty("POLICY_ID")
    private int policyId;

    @JsonProperty("POLICY_NAME")
    private String policyName;

    @JsonProperty("DEVIATION_RULES")
    private List<String> deviationRules;

    // and then "other" stuff:
    @JsonIgnore(value = true)
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
