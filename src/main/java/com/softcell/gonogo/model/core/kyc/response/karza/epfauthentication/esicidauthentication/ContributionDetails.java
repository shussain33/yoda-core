package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.esicidauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ContributionDetails {

    @JsonProperty("Employee_Conrtibution")
    private String Employee_Conrtibution;

    @JsonProperty("Wage_Period")
    private String Wage_Period;

    @JsonProperty("Total_Monthly_Wages")
    private String Total_Monthly_Wages;

    @JsonProperty("Number_of_Days_wages_paid")
    private String Number_of_Days_wages_paid;
}