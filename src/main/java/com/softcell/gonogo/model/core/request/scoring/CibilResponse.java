package com.softcell.gonogo.model.core.request.scoring;


import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;


public class CibilResponse implements Serializable{


    private CibilRespHeader header;

    private CibilRespName name;

    private List<CibilRespID> idList;

    private List<CibilRespPhone> phoneList;

    private List<CibilRespEmployment> employmentList;

    private List<CibilRespEnqActNo> enqActNoList;

    private List<CibilRespScore> scoreList;

    private List<CibilRespAddress> addressList;

    private List<CibilRespAccount> accountList;

    private List<CibilRespEnquiry> enquiryList;

    private CibilRespDisputeRemarks disputeRemarks;

    private List<SecondaryMatches> secondaryMatches;

    public CibilRespHeader getHeader() {
        return header;
    }

    public void setHeader(CibilRespHeader header) {
        this.header = header;
    }

    public CibilRespName getName() {
        return name;
    }

    public void setName(CibilRespName name) {
        this.name = name;
    }

    public List<CibilRespID> getIdList() {
        return idList;
    }

    public void setIdList(List<CibilRespID> idList) {
        this.idList = idList;
    }

    public List<CibilRespPhone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<CibilRespPhone> phoneList) {
        this.phoneList = phoneList;
    }

    public List<CibilRespEmployment> getEmploymentList() {
        return employmentList;
    }

    public void setEmploymentList(List<CibilRespEmployment> employmentList) {
        this.employmentList = employmentList;
    }

    public List<CibilRespEnqActNo> getEnqActNoList() {
        return enqActNoList;
    }

    public void setEnqActNoList(List<CibilRespEnqActNo> enqActNoList) {
        this.enqActNoList = enqActNoList;
    }

    public List<CibilRespScore> getScoreList() {
        return scoreList;
    }

    public void setScoreList(List<CibilRespScore> scoreList) {
        this.scoreList = scoreList;
    }

    public List<CibilRespAddress> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<CibilRespAddress> addressList) {
        this.addressList = addressList;
    }

    public List<CibilRespAccount> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<CibilRespAccount> accountList) {
        this.accountList = accountList;
    }

    public List<CibilRespEnquiry> getEnquiryList() {
        return enquiryList;
    }

    public void setEnquiryList(List<CibilRespEnquiry> enquiryList) {
        this.enquiryList = enquiryList;
    }

    public CibilRespDisputeRemarks getDisputeRemarks() {
        return disputeRemarks;
    }

    public void setDisputeRemarks(CibilRespDisputeRemarks disputeRemarks) {
        this.disputeRemarks = disputeRemarks;
    }

    public List<SecondaryMatches> getSecondaryMatches() {
        return secondaryMatches;
    }

    public void setSecondaryMatches(List<SecondaryMatches> secondaryMatches) {
        this.secondaryMatches = secondaryMatches;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CibilResponse)) return false;
        CibilResponse that = (CibilResponse) o;
        return Objects.equals(header, that.header) &&
                Objects.equals(name, that.name) &&
                Objects.equals(idList, that.idList) &&
                Objects.equals(phoneList, that.phoneList) &&
                Objects.equals(employmentList, that.employmentList) &&
                Objects.equals(enqActNoList, that.enqActNoList) &&
                Objects.equals(scoreList, that.scoreList) &&
                Objects.equals(addressList, that.addressList) &&
                Objects.equals(accountList, that.accountList) &&
                Objects.equals(enquiryList, that.enquiryList) &&
                Objects.equals(disputeRemarks, that.disputeRemarks) &&
                Objects.equals(secondaryMatches, that.secondaryMatches);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, name, idList, phoneList, employmentList, enqActNoList, scoreList, addressList, accountList, enquiryList, disputeRemarks, secondaryMatches);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("header", header)
                .append("name", name)
                .append("idList", idList)
                .append("phoneList", phoneList)
                .append("employmentList", employmentList)
                .append("enqActNoList", enqActNoList)
                .append("scoreList", scoreList)
                .append("addressList", addressList)
                .append("accountList", accountList)
                .append("enquiryList", enquiryList)
                .append("disputeRemarks", disputeRemarks)
                .append("secondaryMatches", secondaryMatches)
                .toString();
    }
}
