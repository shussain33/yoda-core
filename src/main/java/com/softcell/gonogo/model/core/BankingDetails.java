/**
 *
 */
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.Address;
import com.softcell.gonogo.model.request.BankingDetailsRequest;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author kishorp The banking details of customers
 */
public class BankingDetails implements Serializable {

    @JsonProperty("oActHolderName")
    @NotNull(groups = {BankingDetailsRequest.FetchGrp.class})
    @Valid
    private Name accountHolderName;

    @JsonProperty("sBankName")
    @NotBlank(groups = {BankingDetailsRequest.FetchGrp.class})
    private String bankName;

    @JsonProperty("sMifinBankID")
    private String mifinBankCode;

    @JsonProperty("sBrnchName")
    @NotBlank(groups = {BankingDetailsRequest.FetchGrp.class})
    private String branchName;

    @JsonProperty("sActType")
    @NotBlank(groups = {BankingDetailsRequest.FetchGrp.class})
    private String accountType;

    @JsonProperty("sActNo")
    @NotBlank(groups = {BankingDetailsRequest.FetchGrp.class})
    private String accountNumber;

    @JsonProperty("sIFSC")
    @NotBlank(groups = {BankingDetailsRequest.FetchGrp.class})
    private String ifscCode;

    @JsonProperty("bSalaryAct")
    private boolean salaryAccount;

    @JsonProperty("sInwardChequeReturn")
    private String inwardChequeReturn;

    @JsonProperty("sOutwardChequeReturn")
    private String outwardChequeReturn;

    @JsonProperty("dAvgBankBalance")
    private double avgBankBalance;

    @JsonProperty("dDeductedEmiAmt")
    private double deductedEmiAmt;

    @JsonProperty("sAnyEmi")
    private String anyEmi;

    @JsonProperty("sCurRunLon")
    private String currentlyRunningLoan;

    @JsonProperty("dMntnAmt")
    private double mentionAmount;

    @JsonProperty("sChequeNumber")
    private String chequeNumber;

    @JsonProperty("iYearHeld")
    private int yearHeld;

    @JsonProperty("sBankingType")
    @NotBlank(groups = {BankingDetailsRequest.FetchGrp.class})
    private String bankingType;

    @JsonProperty("sRepaymentType")
    private String repaymentType;

    @JsonProperty("sMICRCode")
    private String mICRCode;

    @JsonProperty("bIsSelected")
    private boolean selected;

    @JsonProperty("dDateTime")
    private Date dateTime;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sRemarks")
    private String remarks;

    //extra field for pl

    @JsonProperty("sExistEmi")
    private String existingEmi;

    @JsonProperty("oBankAdrs")
    private Address bankAddress;


    public String getExistingEmi() {
        return existingEmi;
    }

    public void setExistingEmi(String existingEmi) {
        this.existingEmi = existingEmi;
    }

    public Address getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(Address bankAddress) {
        this.bankAddress = bankAddress;
    }

    public Name getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(Name accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * @param accountType the accountType to set
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /**
     * @return the salaryAccount
     */
    public boolean isSalaryAccount() {
        return salaryAccount;
    }

    /**
     * @param salaryAccount the salaryAccount to set
     */
    public void setSalaryAccount(boolean salaryAccount) {
        this.salaryAccount = salaryAccount;
    }

    /**
     * @return the inwardChequeReturn
     */
    public String getInwardChequeReturn() {
        return inwardChequeReturn;
    }

    /**
     * @param inwardChequeReturn the inwardChequeReturn to set
     */
    public void setInwardChequeReturn(String inwardChequeReturn) {
        this.inwardChequeReturn = inwardChequeReturn;
    }

    /**
     * @return the outwardChequeReturn
     */
    public String getOutwardChequeReturn() {
        return outwardChequeReturn;
    }

    /**
     * @param outwardChequeReturn the outwardChequeReturn to set
     */
    public void setOutwardChequeReturn(String outwardChequeReturn) {
        this.outwardChequeReturn = outwardChequeReturn;
    }

    /**
     * @return the avgBankBalance
     */
    public double getAvgBankBalance() {
        return avgBankBalance;
    }

    /**
     * @param avgBankBalance the avgBankBalance to set
     */
    public void setAvgBankBalance(double avgBankBalance) {
        this.avgBankBalance = avgBankBalance;
    }

    /**
     * @return the deductedEmiAmt
     */
    public double getDeductedEmiAmt() {
        return deductedEmiAmt;
    }

    /**
     * @param deductedEmiAmt the deductedEmiAmt to set
     */
    public void setDeductedEmiAmt(double deductedEmiAmt) {
        this.deductedEmiAmt = deductedEmiAmt;
    }

    /**
     * @return the anyEmi
     */
    public String getAnyEmi() {
        return anyEmi;
    }

    /**
     * @param anyEmi the anyEmi to set
     */
    public void setAnyEmi(String anyEmi) {
        this.anyEmi = anyEmi;
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the currentlyRunningLoan
     */
    public String getCurrentlyRunningLoan() {
        return currentlyRunningLoan;
    }

    /**
     * @param currentlyRunningLoan the currentlyRunningLoan to set
     */
    public void setCurrentlyRunningLoan(String currentlyRunningLoan) {
        this.currentlyRunningLoan = currentlyRunningLoan;
    }

    public double getMentionAmount() {
        return mentionAmount;
    }

    public void setMentionAmount(double mentionAmount) {
        this.mentionAmount = mentionAmount;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public String getMifinBankCode() { return mifinBankCode; }

    public void setMifinBankCode(String mifinBankCode) { this.mifinBankCode = mifinBankCode; }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public int getYearHeld() {
        return yearHeld;
    }

    public void setYearHeld(int yearHeld) {
        this.yearHeld = yearHeld;
    }

    public String getBankingType() {
        return bankingType;
    }

    public void setBankingType(String bankingType) {
        this.bankingType = bankingType;
    }

    public String getRepaymentType() {return repaymentType;}

    public void setRepaymentType(String repaymentType) {this.repaymentType = repaymentType;}

    public String getmICRCode() {return mICRCode;}

    public void setmICRCode(String mICRCode) {this.mICRCode = mICRCode;}

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BankingDetails{");
        sb.append("accountHolderName=").append(accountHolderName);
        sb.append(", bankName='").append(bankName).append('\'');
        sb.append(", branchName='").append(branchName).append('\'');
        sb.append(", accountType='").append(accountType).append('\'');
        sb.append(", accountNumber='").append(accountNumber).append('\'');
        sb.append(", ifscCode='").append(ifscCode).append('\'');
        sb.append(", salaryAccount=").append(salaryAccount);
        sb.append(", inwardChequeReturn='").append(inwardChequeReturn).append('\'');
        sb.append(", outwardChequeReturn='").append(outwardChequeReturn).append('\'');
        sb.append(", avgBankBalance=").append(avgBankBalance);
        sb.append(", deductedEmiAmt=").append(deductedEmiAmt);
        sb.append(", anyEmi='").append(anyEmi).append('\'');
        sb.append(", currentlyRunningLoan='").append(currentlyRunningLoan).append('\'');
        sb.append(", mentionAmount=").append(mentionAmount);
        sb.append(", chequeNumber='").append(chequeNumber).append('\'');
        sb.append(", yearHeld=").append(yearHeld);
        sb.append(", bankingType='").append(bankingType).append('\'');
        sb.append(", repaymentType='").append(repaymentType).append('\'');
        sb.append(", mICRCode='").append(mICRCode).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankingDetails that = (BankingDetails) o;

        if (salaryAccount != that.salaryAccount) return false;
        if (Double.compare(that.avgBankBalance, avgBankBalance) != 0) return false;
        if (Double.compare(that.deductedEmiAmt, deductedEmiAmt) != 0) return false;
        if (Double.compare(that.mentionAmount, mentionAmount) != 0) return false;
        if (yearHeld != that.yearHeld) return false;
        if (accountHolderName != null ? !accountHolderName.equals(that.accountHolderName) : that.accountHolderName != null)
            return false;
        if (bankName != null ? !bankName.equals(that.bankName) : that.bankName != null) return false;
        if (branchName != null ? !branchName.equals(that.branchName) : that.branchName != null) return false;
        if (accountType != null ? !accountType.equals(that.accountType) : that.accountType != null) return false;
        if (accountNumber != null ? !accountNumber.equals(that.accountNumber) : that.accountNumber != null)
            return false;
        if (ifscCode != null ? !ifscCode.equals(that.ifscCode) : that.ifscCode != null) return false;
        if (inwardChequeReturn != null ? !inwardChequeReturn.equals(that.inwardChequeReturn) : that.inwardChequeReturn != null)
            return false;
        if (outwardChequeReturn != null ? !outwardChequeReturn.equals(that.outwardChequeReturn) : that.outwardChequeReturn != null)
            return false;
        if (anyEmi != null ? !anyEmi.equals(that.anyEmi) : that.anyEmi != null) return false;
        if (currentlyRunningLoan != null ? !currentlyRunningLoan.equals(that.currentlyRunningLoan) : that.currentlyRunningLoan != null)
            return false;
        if (chequeNumber != null ? !chequeNumber.equals(that.chequeNumber) : that.chequeNumber != null) return false;
        if (mICRCode != null ? !mICRCode.equals(that.mICRCode) : that.mICRCode != null) return false;
        if (repaymentType != null ? !repaymentType.equals(that.repaymentType) : that.repaymentType != null) return false;
        return bankingType != null ? bankingType.equals(that.bankingType) : that.bankingType == null;
    }

    public boolean isEquals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankingDetails that = (BankingDetails) o;

        if (accountHolderName != null ? !accountHolderName.equals(that.accountHolderName) : that.accountHolderName != null)
            return false;
        if (bankName != null ? !bankName.equals(that.bankName) : that.bankName != null) return false;
        if (branchName != null ? !branchName.equals(that.branchName) : that.branchName != null) return false;
        if (accountType != null ? !accountType.equals(that.accountType) : that.accountType != null) return false;
        if (accountNumber != null ? !accountNumber.equals(that.accountNumber) : that.accountNumber != null)
            return false;
        if(status != null ? !status.equals(that.status) : that.status != null) return false;
        return ifscCode != null ? ifscCode.equals(that.ifscCode) : that.ifscCode == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = accountHolderName != null ? accountHolderName.hashCode() : 0;
        result = 31 * result + (bankName != null ? bankName.hashCode() : 0);
        result = 31 * result + (branchName != null ? branchName.hashCode() : 0);
        result = 31 * result + (accountType != null ? accountType.hashCode() : 0);
        result = 31 * result + (accountNumber != null ? accountNumber.hashCode() : 0);
        result = 31 * result + (ifscCode != null ? ifscCode.hashCode() : 0);
        result = 31 * result + (salaryAccount ? 1 : 0);
        result = 31 * result + (inwardChequeReturn != null ? inwardChequeReturn.hashCode() : 0);
        result = 31 * result + (outwardChequeReturn != null ? outwardChequeReturn.hashCode() : 0);
        temp = Double.doubleToLongBits(avgBankBalance);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(deductedEmiAmt);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (anyEmi != null ? anyEmi.hashCode() : 0);
        result = 31 * result + (currentlyRunningLoan != null ? currentlyRunningLoan.hashCode() : 0);
        temp = Double.doubleToLongBits(mentionAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (chequeNumber != null ? chequeNumber.hashCode() : 0);
        result = 31 * result + yearHeld;
        result = 31 * result + (bankingType != null ? bankingType.hashCode() : 0);
        result = 31 * result + (mICRCode != null ? mICRCode.hashCode() : 0);
        result = 31 * result + (repaymentType != null ? repaymentType.hashCode() : 0);
        return result;
    }
}
