/**
 * kishorp8:42:30 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Comparator;
import java.util.Date;

/**
 * @author kishorp
 *
 */
public class CroDecision implements Comparable<CroDecision>{

    @JsonProperty("dAmtAppr")
    private double amtApproved;

    @JsonProperty("dItrRt")
    private double interestRate;

    @JsonProperty("dDpay")
    private double downPayment;

    @JsonProperty("dEmi")
    private double emi;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("dEligibleAmt")
    private double eligibleAmt;

    @JsonProperty("dUnUtilizedAmt")
    private double unUtilizedAmt;

    @JsonProperty("dUtilizedAmt")
    private double utilizedAmt;

    @JsonProperty("dtDecisionUpdateDate")
    private Date decisionUpdateDate = new Date();

    @JsonProperty("dLtv")
    private double ltv;

    public double getLtv() { return ltv; }

    public void setLtv(double ltv) { this.ltv = ltv; }

    public Date getDecisionUpdateDate() {
        return decisionUpdateDate;
    }

    public void setDecisionUpdateDate(Date decisionUpdateDate) {
        this.decisionUpdateDate = decisionUpdateDate;
    }

    public double getEligibleAmt() {
        return eligibleAmt;
    }

    public void setEligibleAmt(double eligibleAmt) {
        this.eligibleAmt = eligibleAmt;
    }

    public double getEmi() {
        return emi;
    }

    public void setEmi(double emi) {
        this.emi = emi;
    }

    public int getTenor() {
        return tenor;
    }

    public void setTenor(int tenor) {
        this.tenor = tenor;
    }

    public double getAmtApproved() {
        return amtApproved;
    }

    public void setAmtApproved(double amtApproved) {
        this.amtApproved = amtApproved;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(double downPayment) {
        this.downPayment = downPayment;
    }

    public double getUnUtilizedAmt() {
        return unUtilizedAmt;
    }

    public void setUnUtilizedAmt(double unUtilizedAmt) {
        this.unUtilizedAmt = unUtilizedAmt;
    }

    public double getUtilizedAmt() {
        return utilizedAmt;
    }

    public void setUtilizedAmt(double utilizedAmt) {
        this.utilizedAmt = utilizedAmt;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CroDecision [amtApproved=");
        builder.append(amtApproved);
        builder.append(", interestRate=");
        builder.append(interestRate);
        builder.append(", downPayment=");
        builder.append(downPayment);
        builder.append(", emi=");
        builder.append(emi);
        builder.append(", tenor=");
        builder.append(tenor);
        builder.append(", eligibleAmt=");
        builder.append(eligibleAmt);
        builder.append(", unUtilizedAmt=");
        builder.append(unUtilizedAmt);
        builder.append(", utilizedAmt=");
        builder.append(utilizedAmt);
        builder.append(", decisionUpdateDate=");
        builder.append(decisionUpdateDate);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int compareTo(CroDecision croDecision) {
        return this.decisionUpdateDate.compareTo(croDecision.getDecisionUpdateDate());
    }
}
