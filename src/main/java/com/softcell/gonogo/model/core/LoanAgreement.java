package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 17/8/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
public class LoanAgreement {
    @JsonProperty("INTTYPE")
    private String intType = "E";

    @JsonProperty("SINGLE_INSTALLMENT_FLAG")
    private String singleInstallmentFlag = "N" ;

    @JsonProperty("ACCT_NUMBER")
    private String acctNumber = "NA";

    @JsonProperty("MARITAL_STATUS")
    private String maritialStatus ;

    @JsonProperty("PAYMENTMODE")
    private String paymentMode = "Q";

    @JsonProperty("PRODUCT")
    private String product ="B";

    @JsonProperty("DEALINGBANKID")
    private String dealingBankId = "4898";

    @JsonProperty("BROKER")
    //COGEN
    private String broker = "SBFC";

    @JsonProperty("QUALIFICATION")
    private String qualification ;

    @JsonProperty("PROPERTY_TYPE")
    private String propertyType = "Old Asset";

    //value will be set according to I/C (Individual/NonIndividual)
    @JsonProperty("IND_CORP_FLAG")
    private String indCorpFlag;

    //According to camSummary as M,H,Y
    @JsonProperty("FREQ")
    private String freq ;

    //Values to be set from demographi Screen
    @JsonProperty("GENDER")
    private String gender ;

    @JsonProperty("INFAVOUROF")
    private String InFavourOf = "DUMMY";

    @JsonProperty("BANK_ACC_NO")
    private String bankAccNo = "NA";

    @JsonProperty("LOANTYPE")
    private String loanType = "R" ;

    @JsonProperty("BUILDER")
    private String builder = "NA";
    @JsonProperty("REP_MODE")
    private String repMode = "S";

    @JsonProperty("CHEQUENUM")
    private String chequenum = "1111";

    @JsonProperty("DRAWNON")
    private String drawanOn = "DUMMY";

    @JsonProperty("INSTL_TYPE")
    private String instlType = "E";

    @JsonProperty("DUEDAY")
    private String dueDay = "5" ;

    @JsonProperty("TOWARDS")
    private String towards = "DUMMY";

    //neeed todo mapping
    @JsonProperty("BRANCH_CODE")
    private String branchCode;

    @JsonProperty("SCHEME")
    private  String scheme = " ";

    @JsonProperty("CUST_FNAME")
    private String  custFname;

    @JsonProperty("CUST_MNAME")
    private String  custMname;

    @JsonProperty("CUST_LNAME")
    private String  custLname;

    @JsonProperty("DOB (DD/ MM/ YYYY)")
    private String  dob;

    /************ Demographic address*******************/
    @JsonProperty("ADD1")
    private String  add1;

    @JsonProperty("ADD2")
    private String  add2 ;

    @JsonProperty("ADD3")
    private String  add3;

    @JsonProperty("CITY")
    private String  city;

    @JsonProperty("STATE")
    private String  state;
    /************ Demographic address*******************/
    @JsonProperty("PHONE1")
    private String  phone1 = "";

    @JsonProperty("PHONE2")
    private String  phone2 = "";

    @JsonProperty("MOBILE")
    private String  mobile = "";

    @JsonProperty("ZIPCODE")
    private String  zipCode;

    //ToDO already Mapped need to test
    /***************** Collateral Address ***********************/
    @JsonProperty("PROPERTY_ADDR1")
    private  String propertyAddr1;

    @JsonProperty("PROPERTY_ADDR2")
    private  String propertyAddr2;

    @JsonProperty("PROPERTY_ADDR3")
    private  String propertyAddr3;

    @JsonProperty("PROPERTY_STATE")
    private  String propertyState;

    @JsonProperty("PROPERTY_ZIP")
    private  String propertyZip;
    /***************** Collateral Address ***********************/
    @JsonProperty("PROPERTY_COST")
    private  String propertyCost = " ";

    @JsonProperty("APPRAISAL_VALUE")
    private  int appraisalValue;

    //ToDo mapped value from emical
    @JsonProperty("AMOUNTFINANCED")
    private  String amtFinanced;
    //ToDoMapping
    @JsonProperty("TENURE")
    private  int tenure;

    @JsonProperty("INSTALMENT_START_DATE (DD/ MM/ YYYY)")
    private String emiStartDate;

    /******************To Do confirmation*************************/
    @JsonProperty("ECS_ACCTNO")
    private  String ecsAcctNo= "";

    @JsonProperty("ECS_MICR")
    private  String ecsMICR = "";

    @JsonProperty("DAS_EMP")
    private  String dasEmp = "";

    @JsonProperty("DAS_NO")
    private  String dasNo = "";

    @JsonProperty("INDUSTRYDESC")
    private  String industryDesc = "";

    @JsonProperty("PROMOTION_DESC")
    private  String promotionDesc = "";

    @JsonProperty("CHANNELCODE")
    private  String channelCode = "";

    @JsonProperty("EMPLOYEE_NAME")
    private  String employeeName = "";

    @JsonProperty("DME")
    private  String dme  = "";

    @JsonProperty("MKTG_OFFICER")
    private  String mktgOfficer  = "";

    @JsonProperty("FIRST_SOURCE")
    private  String firstSource  = "";

    @JsonProperty("UN_FIRST_SOURCE")
    private  String unFirstSource  = "";

    @JsonProperty("UN_FINAL_SOURCE")
    private  String unFinalSource  = "";

    @JsonProperty("CONNECTOR_NAME")
    private  String connectorName  = "";

    @JsonProperty("RELIGION")
    private  String religion = "";

    @JsonProperty("PROFESSION")
    private  String profession ;

    @JsonProperty("SC_ST_FLAG")
    private  String scStFlag = "";

    @JsonProperty("ADDRESSTYPE")
    private  String addressType = "RESIDENCE ADDRESS";

    @JsonProperty("EMAIL_COMMUNICATION")
    private  String emailCommunication = "";

    @JsonProperty("CALL_COMMUNICATION")
    private  String callCommunication = "";

    @JsonProperty("PPI_AMT")
    private  String ppiAmt = "";

    //mapRegDate
    @JsonProperty("APPLICATION_DATE")
    private  String applicationDate ;

    //Map value to disbDate
    @JsonProperty("UPLOAD_DISBURSAL_DATE")
    private  String uploadDisbursalDate ;

    @JsonProperty("EFFRATE")
    private  double effrate; //roi value

    @JsonProperty("INTSTART_DATE")
    private  String intStartDate ; //disbDate

    //To DO confirmation
    /*@JsonProperty("CHARGE_CODE1")
    private  String chargeCode1 ;

    @JsonProperty("CHARGE_AMOUNT1")
    private  String chargeAmount1 ;*/

    @JsonProperty("PSL_FLAG")
    private  String pslFlag = "Y" ;

    @JsonProperty("PLS_CATEGORY")
    private String plsCategory ;

    @JsonProperty("CONSTID")
    public String constId ;

    @JsonProperty("EMI")
    private String emi ;

    @JsonProperty("ADVANCE_EMI")
    private double advamceEmi;
}
