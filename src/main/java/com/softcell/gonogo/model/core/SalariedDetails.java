/**
 * @date Mar 2, 2016 1:18:19 AM
 */
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author kishorp
 *
 */
public class SalariedDetails implements Serializable {

    @JsonProperty("dBasIncm")
    private double basicIncome;

    @JsonProperty("dDernsAllow")
    private double dearnessAllowance;

    @JsonProperty("dHousRenAllow")
    private double houseRentAllowance;

    @JsonProperty("dCtyCompAllwnc")
    private double cityCompensatoryAllowance;

    @JsonProperty("dOthrAllwnc")
    private double otherAllowance;

    @JsonProperty("dOthrSrcsIncm")
    private double otherSourcesIncome;

    @JsonProperty("dGrssIncm")
    private double grossIncome;

    @JsonProperty("dNetIncm")
    private double netIncome;

    @JsonProperty("dBsic")
    private double basic;

    @JsonProperty("dDA")
    private double DA;

    @JsonProperty("dHra")
    private double HRA;

    @JsonProperty("dCca")
    private double CCA;

    @JsonProperty("dInctv")
    private double incentive;

    @JsonProperty("dOthr")
    private double other;

    @JsonProperty("dOthrSrcIncm")
    private double otherSourceIncome;

    @JsonProperty("dPF")
    private double PF;

    @JsonProperty("dPrfTax")
    private double professionalTax;

    @JsonProperty("dLIC")
    private double LIC;

    @JsonProperty("dESI")
    private double ESI;

    @JsonProperty("aLstMnthIncm")
    private List<LastMonthIncome> lastMonthIncomeList;

    @JsonProperty("dPf")
    private double providentFund;

    @JsonProperty("dSic")
    private double stateInsurance;

    @JsonProperty("dOthrDdctn")
    private double otherDeductions;

    @JsonProperty("dLstMntSal")
    private double lastTwoMonthSalary;

    @JsonProperty("dObligtn")
    private double obligations;

    @JsonProperty("sRemarks")
    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getBasic() {
        return basic;
    }

    public void setBasic(double basic) {
        this.basic = basic;
    }

    public double getDA() {
        return DA;
    }

    public void setDA(double dA) {
        DA = dA;
    }

    public double getHRA() {
        return HRA;
    }

    public void setHRA(double hRA) {
        HRA = hRA;
    }

    public double getCCA() {
        return CCA;
    }

    public void setCCA(double cCA) {
        CCA = cCA;
    }

    public double getIncentive() {
        return incentive;
    }

    public void setIncentive(double incentive) {
        this.incentive = incentive;
    }

    public double getOther() {
        return other;
    }

    public void setOther(double other) {
        this.other = other;
    }

    public double getPF() {
        return PF;
    }

    public void setPF(double pF) {
        PF = pF;
    }

    public double getProfessionalTax() {
        return professionalTax;
    }

    public void setProfessionalTax(double professionalTax) {
        this.professionalTax = professionalTax;
    }

    public double getStateInsurance() {
        return stateInsurance;
    }

    public void setStateInsurance(double stateInsurance) {
        this.stateInsurance = stateInsurance;
    }

    public double getOtherDeductions() {
        return otherDeductions;
    }

    public void setOtherDeductions(double otherDeductions) {
        this.otherDeductions = otherDeductions;
    }

    public double getObligations() {
        return obligations;
    }

    public void setObligations(double obligations) {
        this.obligations = obligations;
    }

    /**
     * @return the lastTwoMonthSalary
     */
    public double getLastTwoMonthSalary() {
        return lastTwoMonthSalary;
    }

    /**
     * @param lastTwoMonthSalary
     *            the lastTwoMonthSalary to set
     */
    public void setLastTwoMonthSalary(double lastTwoMonthSalary) {
        this.lastTwoMonthSalary = lastTwoMonthSalary;
    }

    public double getBasicIncome() {
        return basicIncome;
    }

    public void setBasicIncome(double basicIncome) {
        this.basicIncome = basicIncome;
    }

    public double getDearnessAllowance() {
        return dearnessAllowance;
    }

    public void setDearnessAllowance(double dearnessAllowance) {
        this.dearnessAllowance = dearnessAllowance;
    }

    public double getHouseRentAllowance() {
        return houseRentAllowance;
    }

    public void setHouseRentAllowance(double houseRentAllowance) {
        this.houseRentAllowance = houseRentAllowance;
    }

    public double getCityCompensatoryAllowance() {
        return cityCompensatoryAllowance;
    }

    public void setCityCompensatoryAllowance(double cityCompensatoryAllowance) {
        this.cityCompensatoryAllowance = cityCompensatoryAllowance;
    }

    public double getOtherAllowance() {
        return otherAllowance;
    }

    public void setOtherAllowance(double otherAllowance) {
        this.otherAllowance = otherAllowance;
    }

    public double getOtherSourcesIncome() {
        return otherSourcesIncome;
    }

    public void setOtherSourcesIncome(double otherSourcesIncome) {
        this.otherSourcesIncome = otherSourcesIncome;
    }

    public double getGrossIncome() {
        return grossIncome;
    }

    public void setGrossIncome(double grossIncome) {
        this.grossIncome = grossIncome;
    }

    public double getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(double netIncome) {
        this.netIncome = netIncome;
    }

    public double getOtherSourceIncome() {
        return otherSourceIncome;
    }

    public void setOtherSourceIncome(double otherSourceIncome) {
        this.otherSourceIncome = otherSourceIncome;
    }

    public double getLIC() {
        return LIC;
    }

    public void setLIC(double lIC) {
        LIC = lIC;
    }

    public List<LastMonthIncome> getLastMonthIncomeList() {
        return lastMonthIncomeList;
    }

    public void setLastMonthIncomeList(List<LastMonthIncome> lastMonthIncomeList) {
        this.lastMonthIncomeList = lastMonthIncomeList;
    }

    public double getProvidentFund() {
        return providentFund;
    }

    public void setProvidentFund(double providentFund) {
        this.providentFund = providentFund;
    }

    public double getESI() {
        return ESI;
    }

    public void setESI(double eSI) {
        ESI = eSI;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SalariedDetails [basicIncome=");
        builder.append(basicIncome);
        builder.append(", dearnessAllowance=");
        builder.append(dearnessAllowance);
        builder.append(", houseRentAllowance=");
        builder.append(houseRentAllowance);
        builder.append(", cityCompensatoryAllowance=");
        builder.append(cityCompensatoryAllowance);
        builder.append(", otherAllowance=");
        builder.append(otherAllowance);
        builder.append(", otherSourcesIncome=");
        builder.append(otherSourcesIncome);
        builder.append(", grossIncome=");
        builder.append(grossIncome);
        builder.append(", netIncome=");
        builder.append(netIncome);
        builder.append(", basic=");
        builder.append(basic);
        builder.append(", DA=");
        builder.append(DA);
        builder.append(", HRA=");
        builder.append(HRA);
        builder.append(", CCA=");
        builder.append(CCA);
        builder.append(", incentive=");
        builder.append(incentive);
        builder.append(", other=");
        builder.append(other);
        builder.append(", otherSourceIncome=");
        builder.append(otherSourceIncome);
        builder.append(", PF=");
        builder.append(PF);
        builder.append(", professionalTax=");
        builder.append(professionalTax);
        builder.append(", LIC=");
        builder.append(LIC);
        builder.append(", ESI=");
        builder.append(ESI);
        builder.append(", lastMonthIncomeList=");
        builder.append(lastMonthIncomeList);
        builder.append(", providentFund=");
        builder.append(providentFund);
        builder.append(", stateInsurance=");
        builder.append(stateInsurance);
        builder.append(", otherDeductions=");
        builder.append(otherDeductions);
        builder.append(", lastTwoMonthSalary=");
        builder.append(lastTwoMonthSalary);
        builder.append(", obligations=");
        builder.append(obligations);
        builder.append(", remarks=");
        builder.append(remarks);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(CCA);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(DA);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ESI);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(HRA);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(LIC);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(PF);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(basic);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(basicIncome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(cityCompensatoryAllowance);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(dearnessAllowance);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(grossIncome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(houseRentAllowance);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(incentive);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime
                * result
                + ((lastMonthIncomeList == null) ? 0 : lastMonthIncomeList
                .hashCode());
        temp = Double.doubleToLongBits(lastTwoMonthSalary);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(netIncome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(obligations);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(other);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherAllowance);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherDeductions);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherSourceIncome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherSourcesIncome);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(professionalTax);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(providentFund);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((remarks == null) ? 0 : remarks.hashCode());
        temp = Double.doubleToLongBits(stateInsurance);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SalariedDetails other = (SalariedDetails) obj;
        if (Double.doubleToLongBits(CCA) != Double.doubleToLongBits(other.CCA))
            return false;
        if (Double.doubleToLongBits(DA) != Double.doubleToLongBits(other.DA))
            return false;
        if (Double.doubleToLongBits(ESI) != Double.doubleToLongBits(other.ESI))
            return false;
        if (Double.doubleToLongBits(HRA) != Double.doubleToLongBits(other.HRA))
            return false;
        if (Double.doubleToLongBits(LIC) != Double.doubleToLongBits(other.LIC))
            return false;
        if (Double.doubleToLongBits(PF) != Double.doubleToLongBits(other.PF))
            return false;
        if (Double.doubleToLongBits(basic) != Double
                .doubleToLongBits(other.basic))
            return false;
        if (Double.doubleToLongBits(basicIncome) != Double
                .doubleToLongBits(other.basicIncome))
            return false;
        if (Double.doubleToLongBits(cityCompensatoryAllowance) != Double
                .doubleToLongBits(other.cityCompensatoryAllowance))
            return false;
        if (Double.doubleToLongBits(dearnessAllowance) != Double
                .doubleToLongBits(other.dearnessAllowance))
            return false;
        if (Double.doubleToLongBits(grossIncome) != Double
                .doubleToLongBits(other.grossIncome))
            return false;
        if (Double.doubleToLongBits(houseRentAllowance) != Double
                .doubleToLongBits(other.houseRentAllowance))
            return false;
        if (Double.doubleToLongBits(incentive) != Double
                .doubleToLongBits(other.incentive))
            return false;
        if (lastMonthIncomeList == null) {
            if (other.lastMonthIncomeList != null)
                return false;
        } else if (!lastMonthIncomeList.equals(other.lastMonthIncomeList))
            return false;
        if (Double.doubleToLongBits(lastTwoMonthSalary) != Double
                .doubleToLongBits(other.lastTwoMonthSalary))
            return false;
        if (Double.doubleToLongBits(netIncome) != Double
                .doubleToLongBits(other.netIncome))
            return false;
        if (Double.doubleToLongBits(obligations) != Double
                .doubleToLongBits(other.obligations))
            return false;
        if (Double.doubleToLongBits(this.other) != Double
                .doubleToLongBits(other.other))
            return false;
        if (Double.doubleToLongBits(otherAllowance) != Double
                .doubleToLongBits(other.otherAllowance))
            return false;
        if (Double.doubleToLongBits(otherDeductions) != Double
                .doubleToLongBits(other.otherDeductions))
            return false;
        if (Double.doubleToLongBits(otherSourceIncome) != Double
                .doubleToLongBits(other.otherSourceIncome))
            return false;
        if (Double.doubleToLongBits(otherSourcesIncome) != Double
                .doubleToLongBits(other.otherSourcesIncome))
            return false;
        if (Double.doubleToLongBits(professionalTax) != Double
                .doubleToLongBits(other.professionalTax))
            return false;
        if (Double.doubleToLongBits(providentFund) != Double
                .doubleToLongBits(other.providentFund))
            return false;
        if (remarks == null) {
            if (other.remarks != null)
                return false;
        } else if (!remarks.equals(other.remarks))
            return false;
        if (Double.doubleToLongBits(stateInsurance) != Double
                .doubleToLongBits(other.stateInsurance))
            return false;
        return true;
    }

}
