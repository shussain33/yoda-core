package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.response.AmbitMifinResponse.DisbursalMaker.DisbursalDetailResponse;
import com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe.CustomerMatchInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AmbitMifinData {

    @JsonProperty("sMiFinApplicantCode")
    private String miFinApplicantCode;

    @JsonProperty("sEmploymentDetailsID")
    private String mifinEmploymentDetailsId;

    @JsonProperty("sMifinApplicantMessage")
    private String mifinApplicantMessage;

    @JsonProperty("sMifinPancardSearchMessage")
    private String mifinPancardMessage;

    @JsonProperty("sMifinSearchExistingApplicantMessage")
    private String mifinsearchApplicantMessage;

    @JsonProperty("sMifinCRDecisionMessage")
    private String mifinCRDecisionMessage;

    @JsonProperty("sMifinCRDecisionId")
    private String mifinCRDecisionId;

    @JsonProperty("aMifinDedupeMessage")
    private String mifinDedupeMessage;

    @JsonProperty("sMifinVerficationMessage")
    private String mifinVerificationMessage;

    @JsonProperty("sMifinLoanDetailMessage")
    private String mifinLoanDeatilMessage;

    @JsonProperty("aMifinDedupeFlag")
    private boolean mifinDedupeflag;

    @JsonProperty("bMifinProcessDedupeFlag")
    private boolean mifinProcessDedupeFlag;

    @JsonProperty("aMifinDedupeApplications")
    private List<CustomerMatchInfo> mifinAmbitDedupeApplicationList;

    @JsonProperty("sMifinUpdateDedupeMessage")
    private String mifinUpdateDedupeMessage;

    @JsonProperty("sMifinSaveFinancialMessage")
    private String mifinSaveFinancialMessage;

    @JsonProperty("aMifinDisbDetails")
    private List<DisbursalDetailResponse> mifinDisbDetailList;

    @JsonProperty("sMifinDisbursalMakerMessage")
    private String mifinDisbMakerMessage;

    @JsonProperty("sMifinInsuranceMessage")
    private String mifinInsuranceMesssage;

    @JsonProperty("sRemark")
    private String remarks;

    @JsonProperty("sCustomerDedupeStatus")
    private String customerDedupeStatus;

    @JsonProperty("bExisting_flag")
    private boolean existingMifinApplicant;

}
