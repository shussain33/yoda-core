package com.softcell.gonogo.model.core.scoring.response;

public class CustomAttributes {

    private String temp;

    /**
     * @return the temp
     */
    public String getTemp() {
        return temp;
    }

    /**
     * @param temp the temp to set
     */
    public void setTemp(String temp) {
        this.temp = temp;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((temp == null) ? 0 : temp.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CustomAttributes other = (CustomAttributes) obj;
        if (temp == null) {
            if (other.temp != null)
                return false;
        } else if (!temp.equals(other.temp))
            return false;
        return true;
    }

}
