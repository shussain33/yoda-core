package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.PropertyAddress;

import java.io.Serializable;

public class Property implements Serializable {
    @JsonProperty("sPropertyId")
    private String propertyId;

    @JsonProperty("sPropertyName")
    private String propertyName;

    @JsonProperty("sLocation")
    private String location;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sPropertyType")
    private String propertyType;

    @JsonProperty("sPropertyValue")
    private String propertyValue;

    @JsonProperty("oPropertyAddress")
    private PropertyAddress propertyAddress;

    @JsonProperty("iPropertyAge")
    private int propertyAge;

    @JsonProperty("sPurpose")
    private String purpose;

    @JsonProperty("sPropertyArea")
    private String propertyArea;

    @JsonProperty("sPropUnt")
    private String propertyUnit;

    @JsonProperty("sMarketValue")
    private String marketValue;

    @JsonProperty("dOCRAmt")
    private double OCRAmt;

    @JsonProperty("sRemark")
    private String remark;


    /**
     * @return the purpose
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * @param purpose the purpose to set
     */
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    /**
     * @return the propertyArea
     */
    public String getPropertyArea() {
        return propertyArea;
    }

    /**
     * @param propertyArea the propertyArea to set
     */
    public void setPropertyArea(String propertyArea) {
        this.propertyArea = propertyArea;
    }

    /**
     * @return the marketValue
     */
    public String getMarketValue() {
        return marketValue;
    }

    /**
     * @param marketValue the marketValue to set
     */
    public void setMarketValue(String marketValue) {
        this.marketValue = marketValue;
    }

    /**
     * @return the oCRAmt
     */
    public double getOCRAmt() {
        return OCRAmt;
    }

    /**
     * @param oCRAmt the oCRAmt to set
     */
    public void setOCRAmt(double oCRAmt) {
        OCRAmt = oCRAmt;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public int getPropertyAge() {
        return propertyAge;
    }

    public void setPropertyAge(int propertyAge) {
        this.propertyAge = propertyAge;
    }

    public PropertyAddress getPropertyAddress() {
        return propertyAddress;
    }

    public void setPropertyAddress(PropertyAddress propertyAddress) {
        this.propertyAddress = propertyAddress;
    }

    /**
     * @return the propertyUnit
     */
    public String getPropertyUnit() {
        return propertyUnit;
    }

    /**
     * @param propertyUnit the propertyUnit to set
     */
    public void setPropertyUnit(String propertyUnit) {
        this.propertyUnit = propertyUnit;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Property [propertyId=");
        builder.append(propertyId);
        builder.append(", propertyName=");
        builder.append(propertyName);
        builder.append(", location=");
        builder.append(location);
        builder.append(", status=");
        builder.append(status);
        builder.append(", propertyType=");
        builder.append(propertyType);
        builder.append(", propertyValue=");
        builder.append(propertyValue);
        builder.append(", propertyAddress=");
        builder.append(propertyAddress);
        builder.append(", propertyAge=");
        builder.append(propertyAge);
        builder.append(", purpose=");
        builder.append(purpose);
        builder.append(", propertyArea=");
        builder.append(propertyArea);
        builder.append(", propertyUnit=");
        builder.append(propertyUnit);
        builder.append(", marketValue=");
        builder.append(marketValue);
        builder.append(", OCRAmt=");
        builder.append(OCRAmt);
        builder.append(", remark=");
        builder.append(remark);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(OCRAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((location == null) ? 0 : location.hashCode());
        result = prime * result
                + ((marketValue == null) ? 0 : marketValue.hashCode());
        result = prime * result
                + ((propertyAddress == null) ? 0 : propertyAddress.hashCode());
        result = prime * result + propertyAge;
        result = prime * result
                + ((propertyArea == null) ? 0 : propertyArea.hashCode());
        result = prime * result
                + ((propertyId == null) ? 0 : propertyId.hashCode());
        result = prime * result
                + ((propertyName == null) ? 0 : propertyName.hashCode());
        result = prime * result
                + ((propertyType == null) ? 0 : propertyType.hashCode());
        result = prime * result
                + ((propertyUnit == null) ? 0 : propertyUnit.hashCode());
        result = prime * result
                + ((propertyValue == null) ? 0 : propertyValue.hashCode());
        result = prime * result + ((purpose == null) ? 0 : purpose.hashCode());
        result = prime * result + ((remark == null) ? 0 : remark.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Property other = (Property) obj;
        if (Double.doubleToLongBits(OCRAmt) != Double
                .doubleToLongBits(other.OCRAmt))
            return false;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (marketValue == null) {
            if (other.marketValue != null)
                return false;
        } else if (!marketValue.equals(other.marketValue))
            return false;
        if (propertyAddress == null) {
            if (other.propertyAddress != null)
                return false;
        } else if (!propertyAddress.equals(other.propertyAddress))
            return false;
        if (propertyAge != other.propertyAge)
            return false;
        if (propertyArea == null) {
            if (other.propertyArea != null)
                return false;
        } else if (!propertyArea.equals(other.propertyArea))
            return false;
        if (propertyId == null) {
            if (other.propertyId != null)
                return false;
        } else if (!propertyId.equals(other.propertyId))
            return false;
        if (propertyName == null) {
            if (other.propertyName != null)
                return false;
        } else if (!propertyName.equals(other.propertyName))
            return false;
        if (propertyType == null) {
            if (other.propertyType != null)
                return false;
        } else if (!propertyType.equals(other.propertyType))
            return false;
        if (propertyUnit == null) {
            if (other.propertyUnit != null)
                return false;
        } else if (!propertyUnit.equals(other.propertyUnit))
            return false;
        if (propertyValue == null) {
            if (other.propertyValue != null)
                return false;
        } else if (!propertyValue.equals(other.propertyValue))
            return false;
        if (purpose == null) {
            if (other.purpose != null)
                return false;
        } else if (!purpose.equals(other.purpose))
            return false;
        if (remark == null) {
            if (other.remark != null)
                return false;
        } else if (!remark.equals(other.remark))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }


}
