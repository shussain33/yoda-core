package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailVerificationResponseDetails {

    @JsonProperty("data")
    private EmailData data;

    @JsonProperty("result")
    private Boolean result;

    @JsonProperty("additional_info")
    private EmailAdditionalInfo additional_info;
}
