/**
 * @date Mar 2, 2016 1:22:06 AM
 */
package com.softcell.gonogo.model.core;

import java.util.Date;

/**
 * @author kishorp
 *
 */
public class SelfEmployedDetails {
    private String financialYear;
    private Date dateOfReturnsFiling;
    private double turnover;
    private double depreciation;
    private double netProfit;
    private double otherIncome;

    public String getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(String financialYear) {
        this.financialYear = financialYear;
    }

    public Date getDateOfReturnsFiling() {
        return dateOfReturnsFiling;
    }

    public void setDateOfReturnsFiling(Date dateOfReturnsFiling) {
        this.dateOfReturnsFiling = dateOfReturnsFiling;
    }

    public double getTurnover() {
        return turnover;
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    public double getDepreciation() {
        return depreciation;
    }

    public void setDepreciation(double depreciation) {
        this.depreciation = depreciation;
    }

    public double getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(double netProfit) {
        this.netProfit = netProfit;
    }

    public double getOtherIncome() {
        return otherIncome;
    }

    public void setOtherIncome(double otherIncome) {
        this.otherIncome = otherIncome;
    }


}
