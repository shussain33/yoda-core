package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Name;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by archana on 5/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IncomeDetail {

    @JsonProperty("sApplicantID")
    public String applicantId;

    @JsonProperty("oApplicantName")
    @NotEmpty
    public Name applicantName;

    @JsonProperty("sApplicantType")
    public String applicantType;

    @JsonProperty("bIncomeConsidered")
    public boolean incomeConsidered;

    @JsonProperty("sIncomeMethod")
    public String incomeMethod;

    @JsonProperty("oCashProfit")
    public EligibilityByCashProfit cashProfit;

    @JsonProperty("oGrossProfit")
    public EligibilityByGrossProfit grossProfit;

    @JsonProperty("oItr")
    public EligibilityByITR itr;

    @JsonProperty("oRtr")
    public EligibilityByRTR rtr;

    @JsonProperty("oTopUp")
    public EligibilityByTopUp topUp;

    @JsonProperty("oSalaried")
    public EligibilityBySalaried salaried;
}
