
package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SupremeCourt {

    @JsonProperty("USection")
    public String uSection;
    @JsonProperty("act")
    public String act;
    @JsonProperty("caseNo")
    public String caseNo;
    @JsonProperty("caseStatus")
    public String caseStatus;
    @JsonProperty("category")
    public String category;
    @JsonProperty("dairyNo")
    public String dairyNo;
    @JsonProperty("dispType")
    public String dispType;
    @JsonProperty("disposalDate")
    public String disposalDate;
    @JsonProperty("petitionerName")
    public String petitionerName;
    @JsonProperty("presentLastListedOn")
    public String presentLastListedOn;
    @JsonProperty("registrationDate")
    public String registrationDate;
    @JsonProperty("respondentName")
    public String respondentName;
    @JsonProperty("sNo")
    public String sNo;
    @JsonProperty("year")
    public String year;

}
