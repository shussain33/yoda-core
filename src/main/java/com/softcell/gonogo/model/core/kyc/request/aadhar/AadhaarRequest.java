package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.kyc.KycHeader;

import java.util.Date;

/**
 * @author yogeshb
 *         This Domain is used to request adhar cam.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AadhaarRequest extends AuditEntity {


    @JsonIgnore
    private String gngRefId;

    @JsonProperty("dtRequest")
    private Date dateTime = new Date();

    @JsonProperty("HEADER")
    private KycHeader header;

    @JsonProperty("KYC-REQUEST")
    private KycRequest kycRequest;

    public static Builder builder() {
        return new Builder();
    }

    public String getGngRefId() {
        return gngRefId;
    }

    public void setGngRefId(String gngRefId) {
        this.gngRefId = gngRefId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public KycHeader getHeader() {
        return header;
    }

    public void setHeader(KycHeader header) {
        this.header = header;
    }

    public KycRequest getKycRequest() {
        return kycRequest;
    }

    public void setKycRequest(KycRequest kycRequest) {
        this.kycRequest = kycRequest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AadhaarRequest that = (AadhaarRequest) o;

        if (gngRefId != null ? !gngRefId.equals(that.gngRefId) : that.gngRefId != null) return false;
        if (dateTime != null ? !dateTime.equals(that.dateTime) : that.dateTime != null) return false;
        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        return kycRequest != null ? kycRequest.equals(that.kycRequest) : that.kycRequest == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (gngRefId != null ? gngRefId.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (header != null ? header.hashCode() : 0);
        result = 31 * result + (kycRequest != null ? kycRequest.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AadhaarRequest{");
        sb.append("gngRefId='").append(gngRefId).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", header=").append(header);
        sb.append(", kycRequest=").append(kycRequest);
        sb.append('}');
        return sb.toString();
    }

    public static class Builder {
        private AadhaarRequest aadhaarRequest = new AadhaarRequest();

        public AadhaarRequest build() {
            return aadhaarRequest;
        }

        public Builder gngRefId(String gngRefId) {
            this.aadhaarRequest.gngRefId = gngRefId;
            return this;
        }

        public Builder dateTime(Date dateTime) {
            this.aadhaarRequest.dateTime = dateTime;
            return this;
        }

        public Builder header(KycHeader header) {
            this.aadhaarRequest.header = header;
            return this;
        }

        public Builder kycRequest(KycRequest kycRequest) {
            this.aadhaarRequest.kycRequest = kycRequest;
            return this;
        }
    }
}
