package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Created by ssg237 on 8/11/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GstDetailsList {

    @JsonProperty("sMonth")
    private String month;

    @JsonProperty("dAmount")
    private double amount;

    @JsonProperty("dTotalAmount")
    private double totalAmount;
}
