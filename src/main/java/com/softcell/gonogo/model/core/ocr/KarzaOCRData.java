package com.softcell.gonogo.model.core.ocr;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0249 on 22/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KarzaOCRData {

    @JsonProperty("sOCRValue")
    public String ocrValue;

    @JsonProperty("sKarzaValue")
    public String karzaValue;

    @JsonProperty("sAppValue")
    public String appValue;

    @JsonProperty("iUtilityBillMonthDiff")
    public int utilityBillMonthDiff;

    @JsonProperty("sCibilAddress")
    public String cibilAddress;
}
