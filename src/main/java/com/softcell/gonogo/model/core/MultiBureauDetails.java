package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MultiBureauDetails {
    @JsonProperty("sCibilScore")
    private String cibilScore;
    @JsonProperty("sEquifaxlScore")
    private String equifaxlScore;
    @JsonProperty("sCHMScore")
    private String chmScore;
    @JsonProperty("sExperianScore")
    private String experianScore;

    public String getCibilScore() {
        return cibilScore;
    }

    public void setCibilScore(String cibilScore) {
        this.cibilScore = cibilScore;
    }

    public String getEquifaxlScore() {
        return equifaxlScore;
    }

    public void setEquifaxlScore(String equifaxlScore) {
        this.equifaxlScore = equifaxlScore;
    }

    public String getChmScore() {
        return chmScore;
    }

    public void setChmScore(String chmScore) {
        this.chmScore = chmScore;
    }

    public String getExperianScore() {
        return experianScore;
    }

    public void setExperianScore(String experianScore) {
        this.experianScore = experianScore;
    }

}
