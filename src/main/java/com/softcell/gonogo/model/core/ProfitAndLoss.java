package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 10/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfitAndLoss {

    @JsonProperty("sPrevYear")
    private String previousYear;

    @JsonProperty("sCurrentYear")
    private String currentYear;

    @JsonProperty("aYears")
    private List<String> yearsList;

    @JsonProperty("oSales")
    private StatementFieldDetails sales;

    @JsonProperty("oOtherIncome")
    private StatementFieldDetails otherIncome; //Other Income (Incidental to business)

    @JsonProperty("oCostOfGoodsSold")
    private StatementFieldDetails costOfGoodsSold;

    @JsonProperty("oGrossProfits")
    private StatementFieldDetails grossProfits;

    @JsonProperty("oGrossMarginPercentage")
    private StatementFieldDetails grossMarginPercentage;

    @JsonProperty("oAdminExpences")
    private StatementFieldDetails adminExpences;

    @JsonProperty("oSellingAndMktgExpenses")
    private StatementFieldDetails sellingAndMktgExpenses;

    @JsonProperty("oSalaryOrEmployeeBenifitExpenses")
    private StatementFieldDetails salaryOrEmployeeBenifitExpenses;

    @JsonProperty("oTotalOperatingExpense")
    private StatementFieldDetails totalOperatingExpense;

    @JsonProperty("oPartnerSalary")
    private StatementFieldDetails partnerSalary; //Add back Partner's/Directors Salary.

    @JsonProperty("oDirectorsSalary")
    private StatementFieldDetails directorsSalary;

    @JsonProperty("oPartnersInterest")
    private StatementFieldDetails partnersInterest;

    @JsonProperty("oOperatingMargin")
    private StatementFieldDetails operatingMargin;

    @JsonProperty("oOperatingMarginPercentage")
    private StatementFieldDetails operatingMarginPercentage;

    @JsonProperty("oOtherIncomeNonIncidentalToBusiness")
    private StatementFieldDetails otherIncomeNonIncidentalToBusiness;

    //Profit Before Depreciation,Interest & Tax
    @JsonProperty("oPDBIT")
    private StatementFieldDetails PBDIT;

    @JsonProperty("oPBDITexcludingPartnerSalaryAndInterest")
    private StatementFieldDetails PBDITexcludingPartnerSalaryAndInterest;


    //Interest to FI and Banks
    @JsonProperty("dInterestToFIandBanks")
    private StatementFieldDetails interestToFIandBanks;

    //Interest to Private Parties
    @JsonProperty("dInterestToPvtParties")
    private StatementFieldDetails interestToPvtParties;

    //Total Interest Paid
    @JsonProperty("dTotalInterestPaid")
    private StatementFieldDetails totalInterestPaid;


    @JsonProperty("oPDBT")
    private StatementFieldDetails PBDT;

    @JsonProperty("oDepreciation")
    private StatementFieldDetails depreciation;

    @JsonProperty("oPBT")
    private StatementFieldDetails PBT;

    @JsonProperty("oTaxPaid")
    private StatementFieldDetails taxPaid;

    //Profit After Tax
    @JsonProperty("oPAT")
    private StatementFieldDetails PAT;

    // Post tax Cash profits + sal. + partners int.
    @JsonProperty("oPostTaxCashProfitsWithSalAndPartnerInterest")
    private StatementFieldDetails postTaxCashProfitsWithSalAndPartnerInterest;

    @JsonProperty("oNetProfitRatio")
    private StatementFieldDetails netProfitRatio;

    @JsonProperty("oDomestic")
    private StatementFieldDetails domestic;

    @JsonProperty("oExport")
    private StatementFieldDetails export;

    @JsonProperty("oSaleOfServices")
    private StatementFieldDetails saleOfServices;

    @JsonProperty("oDirectorPartnersRemuniration")
    private StatementFieldDetails directorPartnersRemuniration;

    @JsonProperty("oOPBIT")
    private StatementFieldDetails opbit;

    @JsonProperty("oFinanceCost")
    private StatementFieldDetails financeCost;

    @JsonProperty("oInterestOnPartnersCapital")
    private StatementFieldDetails interestOnPartnersCapital;

    @JsonProperty("oOPBT")
    private StatementFieldDetails opbt;

    @JsonProperty("oNonOperatingIncome")
    private StatementFieldDetails nonOperatingIncome;

    @JsonProperty("oNOPBT")
    private StatementFieldDetails nopbt;

    @JsonProperty("oTaxExpenses")
    private StatementFieldDetails taxExpenses;

    @JsonProperty("oDividendDrawing")
    private StatementFieldDetails dividendDrawing;

    @JsonProperty("oRetainedEarning")
    private StatementFieldDetails retainedEarning;

    @JsonProperty("oTotalSalesRevenue")
    private StatementFieldDetails totalSalesRevenue;

    @JsonProperty("oOPBDIT")
    private StatementFieldDetails opbdit;

    @JsonProperty("oRentRepairMaintence")
    private StatementFieldDetails rentRepairMaintence;

    @JsonProperty("oElectricityCharge")
    private StatementFieldDetails electricityCharge;

    @JsonProperty("oEBITDA")
    private StatementFieldDetails ebitda;

    @JsonProperty("oCurrentTax")
    private StatementFieldDetails currentTax;

    @JsonProperty("oFinanceExpenses")
    private StatementFieldDetails financeExpenses;

    @JsonProperty("oCommissionCharges")
    private StatementFieldDetails commissionCharges;

    @JsonProperty("oDeferredTaxReversal")
    private StatementFieldDetails deferredTaxReversal;





   /* year
            Sales
    Other Income (Incidental to business)
    Cost of goods sold
    Gross Profits
    Gross Margin percentage
    Admin Expenses
    Selling & Mktg Expenses
    Salary/ Employee benefit Expenses
    Total Operating Expense

    Add back Partner's/Directors Salary
    Add Partner's Interest

    Operating Margins
    Operating Margins %

    Other Income (non incidental to Business)

    Profit before Depreciation, Interest & Tax (PBDIT)
    PBDIT
            (excluding Partner's salary & interest)"

                    Interest to FI and Banks
                    Interest to Private Parties
                    Total Interest Paid

                    Profit Before Depreciation & Tax (PBDT)

    Depreciation

    Profit before Tax (PBT)

    Tax paid

    Profit After Tax (PAT)
    Post tax Cash profits + sal. + partners int.
    Net Profit Ratio*/

}
