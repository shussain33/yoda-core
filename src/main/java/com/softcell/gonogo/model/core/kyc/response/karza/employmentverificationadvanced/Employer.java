package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Employer {

    @JsonProperty("emplrScore")
    private double emplrScore;

    @JsonProperty("isEmployed")
    private boolean isEmployed;

    @JsonProperty("isNameExact")
    private boolean isNameExact;

    @JsonProperty("isNameUnique")
    private boolean isNameUnique;

    @JsonProperty("isRecent")
    private boolean isRecent;

    @JsonProperty("lastMonth")
    private String lastMonth;

    @JsonProperty("matchName")
    private String matchName;

    @JsonProperty("memberId")
    private String memberId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("nameConfidence")
    private double nameConfidence;

    @JsonProperty("settled")
    private boolean settled;

    @JsonProperty("uanNameMatch")
    private boolean uanNameMatch;

}
