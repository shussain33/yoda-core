package com.softcell.gonogo.model.core.ocr;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

/**
 * Created by suraj on 28/10/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OcrUploadFileDetails {

    @JsonProperty("sFileID")
    private String fileId;

    @JsonProperty("sFileName")
    private String fileName;

    @JsonProperty("sfileData")
    @Transient
    private String fileData;

    @JsonProperty("sFileExtension")
    private String fileExtension;

    @JsonProperty("sReason")
    public String reason;

    @JsonProperty("sApplicantId")
    private String applicantId;

    @JsonProperty("sImgID")
    private String imageId;
}
