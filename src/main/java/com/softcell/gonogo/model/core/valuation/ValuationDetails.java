package com.softcell.gonogo.model.core.valuation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

import java.util.Objects;

/**

 * Created by Amit on 26/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValuationDetails extends ThirdPartyVerification{

	@JsonProperty("sCollId")
	private String collateralId;

	@JsonProperty("oValuationInput")
	protected ValuationInput valuationInput;

	@JsonProperty("oValuationOutput")
	protected ValuationOutput valuationOutput;

	/* This is id from thirdparty */
	@Transient
	private String acknowledgmentId;

	// Index in the array, as required to form file name
	@Transient
	private int index;

	public interface InsertGrp {

	}

	@Override
	/** Equality on the basis of collateralId, status and agencycode*/
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		ValuationDetails that = (ValuationDetails) o;
		return Objects.equals(collateralId, that.collateralId)
				&& Objects.equals(getStatus(), that.getStatus())
				&& Objects.equals(getAgencyCode(), that.getAgencyCode()) ;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), collateralId) +
				Objects.hash(super.hashCode(), getStatus()) +
				Objects.hash(super.hashCode(), getAgencyCode()) ;
	}
}
