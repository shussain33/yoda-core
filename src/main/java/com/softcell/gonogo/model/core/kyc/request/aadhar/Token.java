package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Token {

    @JsonProperty("TYPE")
    private String type;

    @JsonProperty("VALUE")
    private String value;

    public static Builder builder() {
        return new Builder();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Token{");
        sb.append("type='").append(type).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Token)) return false;
        Token token = (Token) o;
        return Objects.equal(getType(), token.getType()) &&
                Objects.equal(getValue(), token.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getType(), getValue());
    }

    public static class Builder {
        private Token token = new Token();

        public Token build() {
            return token;
        }

        public Builder type(String type) {
            this.token.type = type;
            return this;
        }

        public Builder value(String value) {
            this.token.value = value;
            return this;
        }

    }
}
