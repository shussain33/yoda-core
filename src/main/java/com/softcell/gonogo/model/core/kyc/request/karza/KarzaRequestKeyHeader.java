package com.softcell.gonogo.model.core.kyc.request.karza;

/**
 * Created by yogesh Khandare on 7/10/18.
 */
public class KarzaRequestKeyHeader {

    public static final String INSTITUTION_NAME = "sInstituteName";
    public static final String LOGIN_ID = "sLoginId";
    public static final String PASSWORD = "sPassword";
    public static final String APPLICATION_ID = "applicationId";
    public static final String PRODUCT = "sProduct";

}
