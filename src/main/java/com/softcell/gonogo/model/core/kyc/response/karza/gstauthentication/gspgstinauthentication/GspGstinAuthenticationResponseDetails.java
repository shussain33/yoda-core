package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GspGstinAuthenticationResponseDetails {

    @JsonProperty("nba")
    private List<String> nbaDetailsList;

    @JsonProperty("pradr")
    public GstPradr pradr;

    @JsonProperty("lgnm")
    private String lgnm;

    @JsonProperty("ctjCd")
    private String ctjCd;

    @JsonProperty("ctj")
    private String ctj;

    @JsonProperty("rgdt")
    private String rgdt;

    @JsonProperty("ctb")
    private String ctb;

    @JsonProperty("sts")
    private String sts;

    @JsonProperty("stjCd")
    private String stjCd;

    @JsonProperty("stj")
    private String stj;

    @JsonProperty("dty")
    private String dty;

    @JsonProperty("lstupdt")
    private String lstupdt;

    @JsonProperty("tradeNam")
    private String tradeNam;

    @JsonProperty("cxdt")
    private String cxdt;

    @JsonProperty("gstin")
    private String gstin;

    @JsonProperty("adadr")
    public List<GstAdadr> adadrList;
}