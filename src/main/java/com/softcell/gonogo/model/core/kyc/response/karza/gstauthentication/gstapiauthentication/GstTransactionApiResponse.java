package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GstTransactionApiResponse {

    @JsonProperty("gstTransactionApiAveragesResponse")
    private GstTransactionApiAveragesResponse gstTransactionApiAveragesResponse;

    @JsonProperty("oGstTransactionApiBusinessSummryResponse")
    private GstTransactionApiBusinessSummryResponse gstTransactionApiBusinessSummryResponse;

    @JsonProperty("oGstTransactionSalesStateWiseResponse")
    private List<GstTransactionStateWiseResponse> gstTransactionSalesStateWiseResponseList;

    @JsonProperty("oGstTransactionPurchaseStateWiseResponse")
    private List<GstTransactionStateWiseResponse> gstTransactionPurchaseStateWiseResponseList;

}