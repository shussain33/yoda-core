package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LitigationDetailsDistrictCourtResponseDetails {

    @JsonProperty("acts")
    public List<LitigationDetailsDistrictCourtResponseAct> litigationDetailsDistrictCourtResponseActList = null;

    @JsonProperty("caseNo")
    public String caseNo;

    @JsonProperty("caseStatus")
    public String caseStatus;

    @JsonProperty("caseTransferDetails")
    public List<String> caseTransferDetails = null;

    @JsonProperty("caseTransferedFromEstablishment")
    public String caseTransferedFromEstablishment;

    @JsonProperty("caseTransferedToEstablishment")
    public String caseTransferedToEstablishment;

    @JsonProperty("caseType")
    public String caseType;

    @JsonProperty("casetypeCaseNoCaseYr")
    public String casetypeCaseNoCaseYr;

    @JsonProperty("cino")
    public String cino;

    @JsonProperty("cnrNumber")
    public String cnrNumber;

    @JsonProperty("courtNumberAndJudge")
    public String courtNumberAndJudge;

    @JsonProperty("decisionDate")
    public String decisionDate;

    @JsonProperty("district")
    public String district;

    @JsonProperty("districtCode")
    public String districtCode;

    @JsonProperty("documents")
    public List<String> documents = null;

    @JsonProperty("filingDate")
    public String filingDate;

    @JsonProperty("filingNumber")
    public String filingNumber;

    @JsonProperty("firDetails")
    public String firDetails;

    @JsonProperty("firstHearingDate")
    public String firstHearingDate;

    @JsonProperty("historyOfCaseHearing")
    public List<HistoryOfCaseHearing> historyOfCaseHearing = null;

    @JsonProperty("natureOfDisposal")
    public String natureOfDisposal;

    @JsonProperty("nextHearingDate")
    public String nextHearingDate;

    @JsonProperty("orders")
    public List<String> orders = null;

    @JsonProperty("petitionerAndAdvocate")
    public List<PetitionerAndAdvocate> petitionerAndAdvocate = null;

    @JsonProperty("petitionerName")
    public String petitionerName;

    @JsonProperty("registrationDate")
    public String registrationDate;

    @JsonProperty("registrationNumber")
    public String registrationNumber;

    @JsonProperty("respondentAndAdvocate")
    public List<RespondentAndAdvocate> respondentAndAdvocate = null;

    @JsonProperty("respondentName")
    public String respondentName;

    @JsonProperty("stageOfCase")
    public String stageOfCase;

    @JsonProperty("state")
    public String state;

    @JsonProperty("stateCode")
    public String stateCode;

    @JsonProperty("subordinateCourtInfo")
    public String subordinateCourtInfo;

    @JsonProperty("transferDate")
    public String transferDate;

}