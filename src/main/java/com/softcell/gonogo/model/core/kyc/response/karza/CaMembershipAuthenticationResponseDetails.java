package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CaMembershipAuthenticationResponseDetails {

    @JsonProperty("AssociateYear")
    private String AssociateYear;

    @JsonProperty("COPStatus")
    private String COPStatus;

    @JsonProperty("name")
    private String name;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("FellowYear")
    private String FellowYear;

    @JsonProperty("Qualification")
    private String Qualification;

    @JsonProperty("address")
    private String address;

}