package com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UdyogAadharNumberResponseDetails {

    @JsonProperty("pin")
    private String pin;

    @JsonProperty("DateOFCommencement")
    private String DateOFCommencement;

    @JsonProperty("aadhar")
    private String aadhar;

    @JsonProperty("district")
    private String district;

    @JsonProperty("DistrictIndustryCentre")
    private String DistrictIndustryCentre;

    @JsonProperty("NameofEnterPrise")
    private String NameofEnterPrise;

    @JsonProperty("NumberofEmp")
    private String NumberofEmp;

    @JsonProperty("state")
    private String state;

    @JsonProperty("OwnerName")
    private String OwnerName;

    @JsonProperty("MajorActivity(Rs)")
    private String MajorActivity;

    @JsonProperty("email")
    private String email;

    @JsonProperty("pan")
    private String pan;

    @JsonProperty("ifsc")
    private String ifsc;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("address")
    private String address;

    @JsonProperty("social_category")
    private String social_category;

    @JsonProperty("AccountNumber")
    private String AccountNumber;

    @JsonProperty("EntType")
    private String EntType;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("type_of_org")
    private String type_of_org;

    @JsonProperty("Investment")
    private String Investment;

    @JsonProperty("NIC_Digit_Code")
    private String NIC_Digit_Code;

}