package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IBBIResponseResult {

    @JsonProperty("nameOfCorporateDebtor")
    private String nameOfCorporateDebtor;

    @JsonProperty("dateOfIssuanceOfEvaluationMatrix")
    private String dateOfIssuanceOfEvaluationMatrix;

    @JsonProperty("cin")
    private String cin;

    @JsonProperty("nameOfResolutionProfessional")
    private String nameOfResolutionProfessional;

    @JsonProperty("remarks")
    private String remarks;

    @JsonProperty("dateOfInvitationOfResolutionPlans")
    private String dateOfInvitationOfResolutionPlans;

    @JsonProperty("lastDateOfSubmissionOfResolutionPlans")
    private String lastDateOfSubmissionOfResolutionPlans;

    @JsonProperty("type")
    private String type;

    @JsonProperty("nameOfApplicant")
    private String nameOfApplicant;

    @JsonProperty("lastDateOfSubmission")
    private String lastDateOfSubmission;

    @JsonProperty("addressOfInsolvencyProfessional")
    private String addressOfInsolvencyProfessional;

    @JsonProperty("nameOfInsolvencyProfessional")
    private String nameOfInsolvencyProfessional;

    @JsonProperty("dateOfAnnouncement")
    private String dateOfAnnouncement;
}