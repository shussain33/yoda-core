package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 9/3/18.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScoringApplicantResponse {

    @JsonProperty("SUMMARY")
    private Summary summary;

    @JsonProperty("APPLICANT-RESULT")
    private List<Result> applicantResult;

    private String policyName;
}
