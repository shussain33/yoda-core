package com.softcell.gonogo.model.core.ocr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by ssg228 on 8/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RCUData {

    @JsonProperty("sRcuVerification")
    public String rcuVerification;

    @JsonProperty("oRemark")
    public Remark remark;

    @JsonProperty("sRcuSampleStatus")
    public String rcuSampleStatus;

    @JsonProperty("sSamplerName")
    public String samplerName;

    @JsonProperty("sFinalDecision")
    public String finalDecision;

    @JsonProperty("sRcuNRDocProf")
    public String rcuNRDocProf;

    @JsonProperty("sAgencyCode")
    public String agencyCode;

    @JsonProperty("bVerified")
    public boolean verified;

    //added for hdfc
    @JsonProperty("sSamplingSource")
    public String samplingSource;

    @JsonProperty("sRicCheckType")
    public String ricCheckType;

    @JsonProperty("sLimits")
    public String limits;

    @JsonProperty("sVerificationRemark")
    public String verificationRemark;

    @JsonProperty("sVerificationStatus")
    public String verificationStatus;

    @JsonProperty("sSamplerRemark")
    public String samplerRemark;

    @JsonProperty("dtSamplingDate")
    public Date samplingDate;

    @JsonProperty("dProfileCheckCount")
    public double profileCheckCount;
}
