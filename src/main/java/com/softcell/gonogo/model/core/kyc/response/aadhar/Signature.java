package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
@XmlAccessorType(XmlAccessType.FIELD)
public class Signature {

	/**
	 * @author Deepak Bhargava
	 *
	 *
	 */
	@XmlAttribute(name="xmlns")
	private String xmlns;
	@XmlElement(name="SignedInfo")
	private SignedInfo signedInfo;
	@XmlElement(name="SignatureValue")
	private String signValue;
	
	public String getXmlns() {
		return xmlns;
	}
	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}
	public SignedInfo getSignedInfo() {
		return signedInfo;
	}
	public void setSignedInfo(SignedInfo signedInfo) {
		this.signedInfo = signedInfo;
	}
	public String getSignValue() {
		return signValue;
	}
	public void setSignValue(String signValue) {
		this.signValue = signValue;
	}
	@Override
	public String toString() {
		return "Signature [xmlns=" + xmlns + ", signedInfo=" + signedInfo
				+ ", signValue=" + signValue + "]";
	}
	
	
}
