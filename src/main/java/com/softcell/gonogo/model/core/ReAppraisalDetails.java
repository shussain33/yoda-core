package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.surrogate.Surrogate;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author vinodk
 *         <p/>
 *         Class represent properties related to reappraisal
 */
public class ReAppraisalDetails {

    @JsonProperty("iReApprsCnt")
    @NotEmpty(groups = {ReAppraisalDetails.FetchGrp.class})
    private int reAppraiseCount;

    @JsonProperty("sReApprsReas")
    @NotEmpty(groups = {ReAppraisalDetails.FetchGrp.class})
    private String reappraiseReason;

    @JsonProperty("sReApprsRmk")
    @NotEmpty(groups = {ReAppraisalDetails.FetchGrp.class})
    private String reappraiseRemark;

    @JsonProperty("oSurrogate")
    @NotEmpty(groups = {ReAppraisalDetails.FetchGrp.class})
    private Surrogate additionalsurrogate;

    @JsonProperty("dHigrLnAmnt")
    @NotEmpty(groups = {ReAppraisalDetails.FetchGrp.class})
    private double higherLoanAmount;

    /**
     * @return the reAppraiseCount
     */
    public int getReAppraiseCount() {
        return reAppraiseCount;
    }

    /**
     * @param reAppraiseCount the reAppraiseCount to set
     */
    public void setReAppraiseCount(int reAppraiseCount) {
        this.reAppraiseCount = reAppraiseCount;
    }

    /**
     * @return the reappraiseReason
     */
    public String getReappraiseReason() {
        return reappraiseReason;
    }

    /**
     * @param reappraiseReason the reappraiseReason to set
     */
    public void setReappraiseReason(String reappraiseReason) {
        this.reappraiseReason = reappraiseReason;
    }

    /**
     * @return the reappraiseRemark
     */
    public String getReappraiseRemark() {
        return reappraiseRemark;
    }

    /**
     * @param reappraiseRemark the reappraiseRemark to set
     */
    public void setReappraiseRemark(String reappraiseRemark) {
        this.reappraiseRemark = reappraiseRemark;
    }

    /**
     * @return the additionalsurrogate
     */
    public Surrogate getAdditionalsurrogate() {
        return additionalsurrogate;
    }

    /**
     * @param additionalsurrogate the additionalsurrogate to set
     */
    public void setAdditionalsurrogate(Surrogate additionalsurrogate) {
        this.additionalsurrogate = additionalsurrogate;
    }

    /**
     * @return the higherLoanAmount
     */
    public double getHigherLoanAmount() {
        return higherLoanAmount;
    }

    /**
     * @param higherLoanAmount the higherLoanAmount to set
     */
    public void setHigherLoanAmount(double higherLoanAmount) {
        this.higherLoanAmount = higherLoanAmount;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ReAppraisalDetails [reAppraiseCount=");
        builder.append(reAppraiseCount);
        builder.append(", reappraiseReason=");
        builder.append(reappraiseReason);
        builder.append(", reappraiseRemark=");
        builder.append(reappraiseRemark);
        builder.append(", additionalsurrogate=");
        builder.append(additionalsurrogate);
        builder.append(", higherLoanAmount=");
        builder.append(higherLoanAmount);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalsurrogate == null) ? 0 : additionalsurrogate
                .hashCode());
        long temp;
        temp = Double.doubleToLongBits(higherLoanAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + reAppraiseCount;
        result = prime
                * result
                + ((reappraiseReason == null) ? 0 : reappraiseReason.hashCode());
        result = prime
                * result
                + ((reappraiseRemark == null) ? 0 : reappraiseRemark.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReAppraisalDetails other = (ReAppraisalDetails) obj;
        if (additionalsurrogate == null) {
            if (other.additionalsurrogate != null)
                return false;
        } else if (!additionalsurrogate.equals(other.additionalsurrogate))
            return false;
        if (Double.doubleToLongBits(higherLoanAmount) != Double
                .doubleToLongBits(other.higherLoanAmount))
            return false;
        if (reAppraiseCount != other.reAppraiseCount)
            return false;
        if (reappraiseReason == null) {
            if (other.reappraiseReason != null)
                return false;
        } else if (!reappraiseReason.equals(other.reappraiseReason))
            return false;
        if (reappraiseRemark == null) {
            if (other.reappraiseRemark != null)
                return false;
        } else if (!reappraiseRemark.equals(other.reappraiseRemark))
            return false;
        return true;
    }

    public interface FetchGrp{

    }


}
