package com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IecDirectorDetails {

    @JsonProperty("contact_no")
    private String contact_no;

    @JsonProperty("dir_name")
    private String dir_name;

    @JsonProperty("father_name")
    private String father_name;

    @JsonProperty("address")
    private String address;
}