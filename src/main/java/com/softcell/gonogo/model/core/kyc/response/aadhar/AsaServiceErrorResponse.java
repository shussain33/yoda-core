package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.softcell.gonogo.model.core.kyc.response.Errors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name="ERROR_RESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
public class AsaServiceErrorResponse {
	@XmlElement(name="ERRORS")
	private List<Errors> errors;

	public List<Errors> getErrors() {
		return errors;
	}
	

	public void setErrors(List<Errors> errors) {
		this.errors = errors;
	}


	@Override
	public String toString() {
		return "AsaServiceErrorResponse [errors=" + errors + "]";
	}
	
	
	
}
