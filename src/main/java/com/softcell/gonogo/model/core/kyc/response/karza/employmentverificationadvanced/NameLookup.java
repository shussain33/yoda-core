package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class NameLookup {

    @JsonProperty("employeeName")
    private String employeeName;

    @JsonProperty("epfHistory")
    private List<EpfHistory> epfHistory;

    @JsonProperty("estInfo")
    private List<EstInfo> estInfo;

    @JsonProperty("isEmployed")
    private boolean isEmployed;

    @JsonProperty("isLatest")
    private boolean isLatest;

    @JsonProperty("isNameExact")
    private boolean isNameExact;

    @JsonProperty("isNameUnique")
    private boolean isNameUnique;

    @JsonProperty("isRecent")
    private boolean isRecent;

    @JsonProperty("isUnique")
    private boolean isUnique;

    @JsonProperty("matchName")
    private String matchName;

    @JsonProperty("matches")
    private List<Match> matches;

    @JsonProperty("organizationName")
    private String organizationName;

    @JsonProperty("result")
    private boolean result;
}
