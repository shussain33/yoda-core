
package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DistrictCourt {

    @JsonProperty("caseNo")
    public String caseNo;
    @JsonProperty("caseStatus")
    public String caseStatus;
    @JsonProperty("caseType")
    public String caseType;
    @JsonProperty("cino")
    public String cino;
    @JsonProperty("courtComplex")
    public String courtComplex;
    @JsonProperty("district")
    public String district;
    @JsonProperty("mainCourtComplex")
    public String mainCourtComplex;
    @JsonProperty("petitionerName")
    public String petitionerName;
    @JsonProperty("respondentName")
    public String respondentName;
    @JsonProperty("state")
    public String state;

}
