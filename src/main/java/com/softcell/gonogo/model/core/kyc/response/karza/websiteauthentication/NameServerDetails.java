package com.softcell.gonogo.model.core.kyc.response.karza.websiteauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class NameServerDetails {

    @JsonProperty("name")
    private String name;

}