package com.softcell.gonogo.model.core.kyc.response.pan;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class ResponseHeader {
    @JsonProperty("APPLICATION-ID")
    private String applicationId;

    @JsonProperty("RESPONSE-TYPE")
    private String responseType;

    @JsonProperty("REQUEST-RECEIVED-TIME")
    private String requestReceivedTime;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public String getRequestReceivedTime() {
        return requestReceivedTime;
    }

    public void setRequestReceivedTime(String requestReceivedTime) {
        this.requestReceivedTime = requestReceivedTime;
    }

    @Override
    public String toString() {
        return "ResponseHeader [applicationId=" + applicationId
                + ", responseType=" + responseType + ", requestReceivedTime="
                + requestReceivedTime + "]";
    }

}
