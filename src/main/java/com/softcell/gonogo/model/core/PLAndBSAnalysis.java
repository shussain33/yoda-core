package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 10/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PLAndBSAnalysis {
    @JsonProperty("oApplicantName")
    public Name applicantName;

    @JsonProperty("sApplicantId")
    private String applicantId;

    @JsonProperty("oProfitAndLoss")
    private ProfitAndLoss profitAndLoss;

    @JsonProperty("oBalanceSheet")
    private BalanceSheet balanceSheet;

    @JsonProperty("aRatios")
    private List<Ratios> ratios;

    @JsonProperty("sRemarks")
    private String remarks;

    @JsonProperty("sMifinFinancialTabId")
    private String mifinFinancialTabId;
}
