
package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TotalRecords {

    @JsonProperty("districtCourts")
    public int districtCourts;
    @JsonProperty("highCourts")
    public int highCourts;
    @JsonProperty("supremeCourt")
    public int supremeCourt;
    @JsonProperty("total")
    public int total;
    @JsonProperty("tribunals")
    public int tribunals;

}
