package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PngAuthenticationResponseDetails {

    @JsonProperty("Bill_No")
    private String Bill_No;

    @JsonProperty("Due_Date")
    private String Due_Date;

    @JsonProperty("Bill_Amount")
    private String Bill_Amount;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("Customer_Address")
    private String Customer_Address;

    @JsonProperty("Bill_Date")
    private String Bill_Date;

    @JsonProperty("Email")
    private String Email;

    @JsonProperty("Customer_Name")
    private String Customer_Name;

}