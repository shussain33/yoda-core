package com.softcell.gonogo.model.core;

/**
 * Created by yogesh on 19/2/18.
 */
public enum RatioFields {

    //Ratio Analysis: Leverage Ratios
    INTERESTCOVERAGERATIO,
    DEBTTOEQUITY,
    //Ratio Analysis: Profitability
    GROSSMARGINS,
    OPERATINGMARGINS,
    NETPROFITMARGINS,
    //Ratio Analysis: Working Capital
    DEBTORCOLLECTIONPERIOD,
    CREDITORPAYMENTPERIOD,
    //Ratio Analysis:  Turnover Ratios
    CURRENTASSETSTURNOVER,
    FIXEDASSETTURNOVER,
    TOTALASSETTURNOVER,
    //Ratio Analysis:  Liquidity
    CURRENTRATIO,
    QUICKRATIO,
    CASHRATIO,
    //Leverage Ratios
    DEBTTOASSETSRATIO,
    LONGTERMDEBTTOEQUITY,
    //Profitability Ratios
    RETURNONTOTALASSETS,
    RETURNONEQUITY,
    //additional fields
    DAYSINVENTORYOUTSTANDING,
    CASHCONVERSIONCYCLE,
    CASHCREDITDAYS,

    // ratio analysis : OTHER_RATIO
    TOLTNW,
    EBIDTAINTEREST,
    AVGDAYSININVENTORY,
    LIQUIDITYRATIO,
    CASHPROFITRATIO,

    NETMARGIN,
    CASHPROFITMARGIN,
    ROCE,
    CURRENTRATION,
    INTERESTCOVERAGE,
    DSCR,
    DEBTEQUITYRATION,
    TOLTANGIBLE,
    TOLTANGIBLENW,
    CURRENTASSETSSALES,
    TOTALASSETSSALES,
    DEBTORDAY,
    INVENTORYDAY,
    CREDITORDAY,
    WORKINGCAPITALGAP,
    PROMOTORSCONTRIBUTIONWC,
    INVESTMENTINGROUPCOMPANIES,
    TANGIBLENETWORTHFACILITY,
    GROSSPROFIT,
    NETPROFIT,
    CASHPROFIT,
    EBITDA,
    ROE,
    RETAINTIONRATION,
    INTERESTINSALESREVENUE,
    INTERESTCOVERAGERATION,
    TAXRATE,
    DEBTEQUITY,
    DEBTADJUSTED,
    CAPITALIZATIONRATIONS,
    TANGIBLENETWORTH,
    TANGIBLENETWORTHFROMPROMOTE,
    CURRENTASSETS,
    DEBTORS,
    CREDITORS,
    INVENTORY,
    WORKINGCAPITALTORATIOS,
    PROMOTORSCONTRIBUTIONCAPITALGAP,
    PROMOTORSCONTRIBUTIONCAPITALGAPPER,
    INVESTMENTGROUP,
    INVESTMENTGROUPTNW,
    TNWFACILITYVALUE,
    FACILITYVALUE,
    COLLECTIONPERIOD,
    ACCOUNTPAYABLE,
    INVENTORYHOLDINGPERIOD,
    TOTALDEBT




    /*
    /*
    Ratio Analysis: Leverage Ratios
     *//*
    @JsonProperty("oInterestCoverageRatio")
    private StatementFieldDetails interestCoverageRatio;
    @JsonProperty("oDebtToEquity")
    private StatementFieldDetails debtToEquity; //(Total Liability to Net Worth)
    *//*
    * Ratio Analysis: Profitability
    *//*
    @JsonProperty("oGrossMargins")
    private StatementFieldDetails grossMargins;
    @JsonProperty("oOperatingMargins")
    private StatementFieldDetails operatingMargins;
    @JsonProperty("oNetProfitMargins")
    private StatementFieldDetails netProfitMargins;
    *//*
            * Ratio Analysis: Working Capital
    *//*
    @JsonProperty("oDebtorCollectionPeriod")
    private StatementFieldDetails debtorCollectionPeriod;
    @JsonProperty("oCreditorPaymentPeriod")
    private StatementFieldDetails creditorPaymentPeriod;
    *//*
            * Ratio Analysis:  Turnover Ratios
    *//*
    @JsonProperty("oCurrentAssetsTurnover")
    private StatementFieldDetails currentAssetsTurnover;
    @JsonProperty("oFixedAssetTurnover")
    private StatementFieldDetails fixedAssetTurnover;
    @JsonProperty("oTotalAssetTurnover")
    private StatementFieldDetails totalAssetTurnover;
    *//*
    Ratio Analysis:  Liquidity
     *//*
     CurrentRatio,QuickRatio,CashRatio,
    @JsonProperty("oCurrentRatio")
    private StatementFieldDetails currentRatio;
    @JsonProperty("oQuickRatio")
    private StatementFieldDetails quickRatio;
    @JsonProperty("oCashRatio")
    private StatementFieldDetails cashRatio;
    *//*
    Leverage Ratios
    *//*
    DebtToAssetsRatio,LongTermDebtToEquity,
    @JsonProperty("oDebtToAssetsRatio")
    private StatementFieldDetails debtToAssetsRatio;
    @JsonProperty("oLongTermDebtToEquity")
    private StatementFieldDetails longTermDebtToEquity;
    *//*
    Profitability Ratios
    *//*
    ReturnOnTotalAssets,ReturnOnEquity,
    @JsonProperty("oReturnOnTotalAssets")
    private StatementFieldDetails returnOnTotalAssets;
    @JsonProperty("oReturnOnEquity")
    private StatementFieldDetails returnOnEquity;

    //additional fields
    DaysInventoryOutstanding,CashConversionCycle,CashCreditDays
    @JsonProperty("oDaysInventoryOutstanding")
    private StatementFieldDetails daysInventoryOutstanding;
    @JsonProperty("oCashConversionCycle")
    private StatementFieldDetails cashConversionCycle;
    @JsonProperty("oCashCreditDays")
    private StatementFieldDetails cashCreditDays;*/
}
