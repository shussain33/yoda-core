package com.softcell.gonogo.model.core.kyc.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Transient;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImagesDetails {

    @JsonProperty("sImgID")
    private String imageId;

    @JsonProperty("sImgType")
    private String imageType;

    @JsonProperty("sImgVal")
    private String imageValue;

    @JsonProperty("sStat")
    private String status;

    @JsonProperty("sReason")
    private String reason;

    @JsonProperty("sApplicantId")
    private String applicantId;

    @JsonProperty("sImageExtension")
    private String imageExtension;

    @JsonProperty("sImgName")
    private String imageName;

    @JsonProperty("sBlockName")
    private String blockName;

    @JsonProperty("sRefID")
    private String refID;

    @Transient
    @JsonProperty("sUploadDate")
    private Date uploadDate;

    @JsonProperty("sRemark")
    private String remark;

    @JsonProperty("sCollateralId")
    private String collateralId;

    //DigiPl Fields Added
    @JsonProperty("sFileID")
    private String fileId;

    @JsonProperty("sLatitude")
    private String latitude;

    @JsonProperty("sLongitude")
    private String longitude;

    @JsonProperty("aSection")
    private List<String> sectionList;

    @JsonProperty("bDeleted")
    public boolean deleted;

    @JsonProperty("sRcuStatus")
    private String rcuStatus;

    @JsonProperty("sImageSide")
    private String imageSide;

    @JsonProperty("bRopedOut")
    public boolean ropedOut;

    @JsonProperty("aDocValuation")
    public List<String> docValuation;

    @JsonProperty("documentsNames")
    public List<HashMap<String,String>> documentNames;

    @JsonProperty("showDropdown")
    public boolean showDropdown;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) { this.fileId = fileId;}

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public List<String> getSectionList() { return sectionList;}

    public void setSectionList(List<String> sectionList) { this.sectionList = sectionList;}

    public boolean isDeleted() { return deleted;}

    public void setDeleted(boolean deleted) { this.deleted = deleted; }

    public String getRcuStatus() { return rcuStatus; }

    public void setRcuStatus(String rcuStatus) { this.rcuStatus = rcuStatus; }

    public String getImageSide() { return imageSide; }

    public void setImageSide(String imageSide) { this.imageSide = imageSide; }

    public boolean isRopedOut() { return ropedOut; }

    public void setRopedOut(boolean ropedOut) { this.ropedOut = ropedOut; }

    public List<String> getDocValuation() { return docValuation; }

    public void setDocValuation(List<String> docValuation) { this.docValuation = docValuation; }

    public List<HashMap<String, String>> getDocumentNames() { return documentNames; }

    public void setDocumentNames(List<HashMap<String, String>> documentNames) { this.documentNames = documentNames; }

    public boolean isShowDropdown() { return showDropdown; }

    public void setShowDropdown(boolean showDropdown) { this.showDropdown = showDropdown; }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the imageValue
     */
    public String getImageValue() {
        return imageValue;
    }

    /**
     * @param imageValue the imageValue to set
     */
    public void setImageValue(String imageValue) {
        this.imageValue = imageValue;
    }


    /**
     * @return the imageExtension
     */
    public String getImageExtension() {
        return imageExtension;
    }

    /**
     * @param imageExtension the imageExtension to set
     */
    public void setImageExtension(String imageExtension) {
        this.imageExtension = imageExtension;
    }

    /**
     * @return the imageName
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * @param imageName the imageName to set
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCollateralId() {
        return collateralId;
    }

    public void setCollateralId(String collateralId) {
        this.collateralId = collateralId;
    }

    /* (non-Javadoc)
                     * @see java.lang.Object#toString()
                     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ImagesDetails [imageId=");
        builder.append(imageId);
        builder.append(", imageType=");
        builder.append(imageType);
        builder.append(", imageValue=");
        builder.append(imageValue);
        builder.append(", status=");
        builder.append(status);
        builder.append(", reason=");
        builder.append(reason);
        builder.append(", applicantId=");
        builder.append(applicantId);
        builder.append(", imageExtension=");
        builder.append(imageExtension);
        builder.append(", imageName=");
        builder.append(imageName);
        builder.append(", blockName=");
        builder.append(blockName);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((applicantId == null) ? 0 : applicantId.hashCode());
        result = prime * result
                + ((blockName == null) ? 0 : blockName.hashCode());
        result = prime * result
                + ((imageExtension == null) ? 0 : imageExtension.hashCode());
        result = prime * result + ((imageId == null) ? 0 : imageId.hashCode());
        result = prime * result
                + ((imageName == null) ? 0 : imageName.hashCode());
        result = prime * result
                + ((imageType == null) ? 0 : imageType.hashCode());
        result = prime * result
                + ((imageValue == null) ? 0 : imageValue.hashCode());
        result = prime * result + ((reason == null) ? 0 : reason.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ImagesDetails other = (ImagesDetails) obj;
        if (applicantId == null) {
            if (other.applicantId != null)
                return false;
        } else if (!applicantId.equals(other.applicantId))
            return false;
        if (blockName == null) {
            if (other.blockName != null)
                return false;
        } else if (!blockName.equals(other.blockName))
            return false;
        if (imageExtension == null) {
            if (other.imageExtension != null)
                return false;
        } else if (!imageExtension.equals(other.imageExtension))
            return false;
        if (imageId == null) {
            if (other.imageId != null)
                return false;
        } else if (!imageId.equals(other.imageId))
            return false;
        if (imageName == null) {
            if (other.imageName != null)
                return false;
        } else if (!imageName.equals(other.imageName))
            return false;
        if (imageType == null) {
            if (other.imageType != null)
                return false;
        } else if (!imageType.equals(other.imageType))
            return false;
        if (imageValue == null) {
            if (other.imageValue != null)
                return false;
        } else if (!imageValue.equals(other.imageValue))
            return false;
        if (reason == null) {
            if (other.reason != null)
                return false;
        } else if (!reason.equals(other.reason))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }
}
