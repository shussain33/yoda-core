package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data

public class SuitFiledSearchResponseDirector {


    @JsonProperty("panNo")
    private String panNo;

    @JsonProperty("dinNo")
    private String dinNo;

    @JsonProperty("directorName")
    private String directorName;

}