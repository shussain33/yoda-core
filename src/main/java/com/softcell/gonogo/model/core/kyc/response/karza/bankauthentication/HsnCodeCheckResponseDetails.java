package com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class HsnCodeCheckResponseDetails {

    @JsonProperty("policyLink")
    private String policyLink;

    @JsonProperty("chapterNotes")
    private String chapterNotes;

    @JsonProperty("headingDesc")
    private String headingDesc;

    @JsonProperty("sectionDesc")
    private String sectionDesc;

    @JsonProperty("itemDesc2")
    private String itemDesc2;

    @JsonProperty("itemDesc1")
    private String itemDesc1;

    @JsonProperty("chapterNo")
    private String chapterNo;

    @JsonProperty("policy")
    private String policy;

    @JsonProperty("chapterDesc")
    private String chapterDesc;

    @JsonProperty("policyConditions")
    private String policyConditions;

    @JsonProperty("sectionNo")
    private String sectionNo;

}