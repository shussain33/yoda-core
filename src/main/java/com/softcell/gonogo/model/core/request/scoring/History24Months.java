package com.softcell.gonogo.model.core.request.scoring;


import java.util.List;


public class History24Months {
    private List<Month> month;

    public List<Month> getMonth() {
        return month;
    }

    public void setMonth(List<Month> month) {
        this.month = month;
    }

    @Override
    public String toString() {
        return "History24Months [month=" + month + "]";
    }
}
