package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KycRquestTypesEnum;

import java.io.Serializable;

/**
 * @author prateek
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Kyc implements Serializable {

    @JsonProperty("sKycName")
    private String kycName;

    @JsonProperty("sKycNumber")
    private String kycNumber;

    @JsonProperty("bKycStat")
    private boolean kycStatus;

    @JsonProperty("sIssueDate")
    private String issueDate;

    @JsonProperty("sExpiryDate")
    private String expiryDate;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("sCountryOfIssue")
    private String countryOfIssue;

    //for suilt filled
    @JsonProperty("name")
    private String name;

    @JsonProperty("doe")
    private String doe;

    @JsonProperty("state")
    private String state;

    @JsonProperty("input")
    private String gstInput;

    @JsonProperty("area_code")
    private String areaCode;

    //for IcsiMembership
    @JsonProperty("cp_no")
    private String cpNo;

    //for MciMembership
    @JsonProperty("year_of_reg")
    private String yearOfReg;

    @JsonProperty("medical_council")
    private String medicalCouncil;

    // for PngMembership
    @JsonProperty("bp_no")
    private String bpNo;

    @JsonProperty("service_provider")
    private String serviceProvider;

    // for FormI6
    @JsonProperty("cert_no")
    private String certNo;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("fiscal_year")
    private String fiscalYear;

    // for ITR
    @JsonProperty("ack")
    private String ack;

    // for Name similarity
    @JsonProperty("name1")
    private String name1;

    @JsonProperty("name2")
    private String name2;

    // for Bank verification
    @JsonProperty("ifsc")
    private String ifsc;

    @JsonProperty("accountNumber")
    private String accountNumber;

    // for Vehicle
    @JsonProperty("chassis_no")
    private String chassis_no;

    @JsonProperty("nameOfCorporateDebtor")
    private String nameOfCorporateDebtor;

    @JsonProperty("type")
    private String type;

    // for LitigationSearch
    @JsonProperty("court_complex")
    private String courtComplex;

    @JsonProperty("results_per_page")
    private String resultsPerPage;

    @JsonProperty("page_no")
    private String pageNo;

    @JsonProperty("relation")
    private String relation;

    //Added for TMF
    @JsonProperty("username")
    private String gstUserName;

    @JsonProperty("password")
    private String gstPassword;


    @JsonProperty("bKycApplied")
    private boolean kycApplied;

    @JsonProperty("bKycMatch")
    private boolean kycMatch;

    @JsonProperty("bDobMatch")
    private boolean dobMatch;

    @JsonProperty("bDuplicate")
    private boolean duplicate;

    public boolean isDobMatch() { return dobMatch; }

    public void setDobMatch(boolean dobMatch) { this.dobMatch = dobMatch;}

    public boolean isDuplicate() { return duplicate;}

    public void setDuplicate(boolean duplicate) { this.duplicate = duplicate; }

    public String getKycName() {
        return kycName;
    }

    public void setKycName(String kycName) {
        this.kycName = kycName;
    }

    public String getKycNumber() {
        return kycNumber;
    }

    public void setKycNumber(String kycNumber) {
        this.kycNumber = kycNumber;
    }

    public boolean isKycStatus() {
        return kycStatus;
    }

    public void setKycStatus(boolean kycStatus) {
        this.kycStatus = kycStatus;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getKycRequestType() { return KycRquestTypesEnum.getRequestType(kycName); }

    public boolean isKycApplied() {
        return kycApplied;
    }

    public void setKycApplied(boolean kycApplied) {
        this.kycApplied = kycApplied;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCountryOfIssue() {
        return countryOfIssue;
    }

    public void setCountryOfIssue(String countryOfIssue) {
        this.countryOfIssue = countryOfIssue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoe() {
        return doe;
    }

    public void setDoe(String doe) {
        this.doe = doe;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGstInput() {
        return gstInput;
    }

    public void setGstInput(String gstInput) {
        this.gstInput = gstInput;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCpNo() {
        return cpNo;
    }

    public void setCpNo(String cpNo) {
        this.cpNo = cpNo;
    }

    public String getYearOfReg() {
        return yearOfReg;
    }

    public void setYearOfReg(String yearOfReg) {
        this.yearOfReg = yearOfReg;
    }

    public String getMedicalCouncil() {
        return medicalCouncil;
    }

    public void setMedicalCouncil(String medicalCouncil) {
        this.medicalCouncil = medicalCouncil;
    }

    public String getBpNo() {
        return bpNo;
    }

    public void setBpNo(String bpNo) {
        this.bpNo = bpNo;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getAck() {
        return ack;
    }

    public void setAck(String ack) {
        this.ack = ack;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getChassis_no() {
        return chassis_no;
    }

    public void setChassis_no(String chassis_no) {
        this.chassis_no = chassis_no;
    }

    public String getNameOfCorporateDebtor() {
        return nameOfCorporateDebtor;
    }

    public void setNameOfCorporateDebtor(String nameOfCorporateDebtor) {
        this.nameOfCorporateDebtor = nameOfCorporateDebtor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCourtComplex() {
        return courtComplex;
    }

    public void setCourtComplex(String courtComplex) {
        this.courtComplex = courtComplex;
    }

    public String getResultsPerPage() {
        return resultsPerPage;
    }

    public void setResultsPerPage(String resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public String getPageNo() {
        return pageNo;
    }

    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getGstUserName() {
        return gstUserName;
    }

    public void setGstUserName(String gstUserName) {
        this.gstUserName = gstUserName;
    }

    public String getGstPassword() {
        return gstPassword;
    }

    public void setGstPassword(String gstPassword) {
        this.gstPassword = gstPassword;
    }

    public boolean isKycMatch() {
        return kycMatch;
    }

    public void setKycMatch(boolean kycMatch) {
        this.kycMatch = kycMatch;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Kyc [kycName=");
        builder.append(kycName);
        builder.append(", kycNumber=");
        builder.append(kycNumber);
        builder.append(", kycStatus=");
        builder.append(kycStatus);
        builder.append(", issueDate=");
        builder.append(issueDate);
        builder.append(", expiryDate=");
        builder.append(expiryDate);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((expiryDate == null) ? 0 : expiryDate.hashCode());
        result = prime * result
                + ((issueDate == null) ? 0 : issueDate.hashCode());
        result = prime * result + ((kycName == null) ? 0 : kycName.hashCode());
        result = prime * result
                + ((kycNumber == null) ? 0 : kycNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Kyc other = (Kyc) obj;
        if (expiryDate == null) {
            if (other.expiryDate != null)
                return false;
        } else if (!expiryDate.equals(other.expiryDate))
            return false;
        if (issueDate == null) {
            if (other.issueDate != null)
                return false;
        } else if (!issueDate.equals(other.issueDate))
            return false;
        if (kycName == null) {
            if (other.kycName != null)
                return false;
        } else if (!kycName.equals(other.kycName))
            return false;
        if (kycNumber == null) {
            if (other.kycNumber != null)
                return false;
        } else if (!kycNumber.equals(other.kycNumber))
            return false;
        return true;
    }


}
