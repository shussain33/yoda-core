package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class FdaLicenseAuthenticationResponseDetails {

    @JsonProperty("store_name")
    private String store_name;

    @JsonProperty("contact_number")
    private String contact_number;

    @JsonProperty("license_detail")
    private String license_detail;

    @JsonProperty("name")
    private String name;

    @JsonProperty("address")
    private String address;

}