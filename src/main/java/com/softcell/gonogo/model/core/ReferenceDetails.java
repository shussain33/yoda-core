package com.softcell.gonogo.model.core;

import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReferenceDetails extends ThirdPartyVerification{

	@JsonProperty("oRefName")
	private Name referenceName;

	@JsonProperty("sRelationship")
	private String relationship;

	@JsonProperty("oAddress")
	private CustomerAddress address;

	@JsonProperty("oPhone")
	private Phone phoneNo;

	@JsonProperty("sAadhar")
	private String aadharNo;

	@JsonProperty("sStatusCmnt")
	private String statusComment;

	@JsonProperty("oRemarks")
	private Remark remarks;
}
