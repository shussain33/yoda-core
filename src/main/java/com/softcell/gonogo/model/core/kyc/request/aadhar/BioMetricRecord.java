package com.softcell.gonogo.model.core.kyc.request.aadhar;

public class BioMetricRecord {
	
	String dih;
	String type;
	String posh;
	String value;
	String bs;
	
	public String getDih() {
		return dih;
	}
	public void setDih(String dih) {
		this.dih = dih;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPosh() {
		return posh;
	}
	public void setPosh(String posh) {
		this.posh = posh;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getBs() {
		return bs;
	}
	public void setBs(String bs) {
		this.bs = bs;
	}
	@Override
	public String toString() {
		return "BioMetricRecord [dih=" + dih + ", type=" + type + ", posh=" + posh + ", value="
				+ value + ", bs=" + bs + "]";
	}
	
	

}
