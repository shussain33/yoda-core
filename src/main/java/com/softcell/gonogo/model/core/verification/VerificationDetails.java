package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.ReferenceDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by suhasini on 17/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "verificationDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VerificationDetails {

    @Id
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sInstitutionId")
    public String institutionId;

    @JsonProperty("aResidenceVerification")
    private List<PropertyVerification> residenceVerification;

    @JsonProperty("aOfficeVerification")
    private List<PropertyVerification> officeVerification;

    @JsonProperty("aReferenceDetails")
    private List<ReferenceDetails> referenceDetails;

    @JsonProperty("aItrVerification")
    private List<ItrVerification> itrVerification;

    @JsonProperty("aBankingVerification")
    private List<BankingVerification> bankingVerification;

    @JsonProperty("sRemarks")
    private String remarks;

    public interface InsertGrp {

    }
}
