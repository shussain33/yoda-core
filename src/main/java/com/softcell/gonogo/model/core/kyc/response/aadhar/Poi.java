package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Poi {
	@XmlAttribute(name="name")
	private String name;
	@XmlAttribute(name="dob")
	private String dob;
	@XmlAttribute(name="gender")
	private String gender;
	@XmlAttribute(name="phone")
	private String phone;
	@XmlAttribute(name="email")
	private String email;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Poi [name=" + name + ", dob=" + dob + ", gender=" + gender + ", phone=" + phone + ", email=" + email
				+ "]";
	}
	
	
}
