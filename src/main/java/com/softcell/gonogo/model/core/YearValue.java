package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Created by ssg408 on 27/6/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class YearValue {

    @JsonProperty("sYear")
    private String year;

    @JsonProperty("dValue")
    private double value;

}