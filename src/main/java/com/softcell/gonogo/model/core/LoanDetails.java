/**
 * Administrator4:12:01 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Administrator
 *
 */
public class LoanDetails implements Serializable {
    /**
     * Individual / Joint
     */

    @JsonProperty("sLnOwnrshp")
    private String loanOwnership;
    @JsonProperty("sLnTyp")
    private String loanType;
    @JsonProperty("sLnPurp")
    private String loanPurpose;
    @JsonProperty("sLnAccNo")
    private String loanAcountNo;
    @JsonProperty("sCrdtGrntr")
    private String creditGranter;
    @JsonProperty("dLnAmt")
    private double loanAmt;
    @JsonProperty("dEmiAmt")
    private double emiAmt;
    @JsonProperty("iNoEmiBounce")
    private int emiBouncedCountInAYear;
    @JsonProperty("iMob")
    private int mob;
    @JsonProperty("iBalTenur")
    private int balanceTenure;
    @JsonProperty("sRpymntBnkNm")
    private String repaymentBankName;
    @JsonProperty("sMifinBankID")
    private String mifinBankCode;
    @JsonProperty("sOblgt")
    private String obligate;
    @JsonProperty("dLnApr")
    private double loanApr;
    @JsonProperty("sPOS")
    private String pos;

    /**
     * adding extra field for pl
     */

    @JsonProperty("sExistingEmi")
    private String existingEmi;

    public String getExistingEmi() {
        return existingEmi;
    }

    public void setExistingEmi(String existingEmi) {
        this.existingEmi = existingEmi;
    }



    /**
     * @return the loanOwnership
     */
    public String getLoanOwnership() {
        return loanOwnership;
    }

    /**
     * @param loanOwnership the loanOwnership to set
     */
    public void setLoanOwnership(String loanOwnership) {
        this.loanOwnership = loanOwnership;
    }

    /**
     * @return the loanType
     */
    public String getLoanType() {
        return loanType;
    }

    /**
     * @param loanType the loanType to set
     */
    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    /**
     * @return the loanAcountNo
     */
    public String getLoanAcountNo() {
        return loanAcountNo;
    }

    /**
     * @param loanAcountNo the loanAcountNo to set
     */
    public void setLoanAcountNo(String loanAcountNo) {
        this.loanAcountNo = loanAcountNo;
    }

    /**
     * @return the creditGranter
     */
    public String getCreditGranter() {
        return creditGranter;
    }

    /**
     * @param creditGranter the creditGranter to set
     */
    public void setCreditGranter(String creditGranter) {
        this.creditGranter = creditGranter;
    }

    /**
     * @return the loanAmt
     */
    public double getLoanAmt() {
        return loanAmt;
    }

    /**
     * @param loanAmt the loanAmt to set
     */
    public void setLoanAmt(double loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return the emiAmt
     */
    public double getEmiAmt() {
        return emiAmt;
    }

    /**
     * @param emiAmt the emiAmt to set
     */
    public void setEmiAmt(double emiAmt) {
        this.emiAmt = emiAmt;
    }

    /**
     * @return the mob
     */
    public int getMob() {
        return mob;
    }

    /**
     * @param mob the mob to set
     */
    public void setMob(int mob) {
        this.mob = mob;
    }

    /**
     * @return the balanceTenure
     */
    public int getBalanceTenure() {
        return balanceTenure;
    }

    /**
     * @param balanceTenure the balanceTenure to set
     */
    public void setBalanceTenure(int balanceTenure) {
        this.balanceTenure = balanceTenure;
    }

    /**
     * @return the repaymentBankName
     */
    public String getRepaymentBankName() {
        return repaymentBankName;
    }

    /**
     * @param repaymentBankName the repaymentBankName to set
     */
    public void setRepaymentBankName(String repaymentBankName) {
        this.repaymentBankName = repaymentBankName;
    }

    /**
     * @return the obligate
     */
    public String getObligate() {
        return obligate;
    }

    /**
     * @param obligate the obligate to set
     */
    public void setObligate(String obligate) {
        this.obligate = obligate;
    }

    /**
     * @return the loanPurpose
     */
    public String getLoanPurpose() {
        return loanPurpose;
    }

    /**
     * @param loanPurpose the loanPurpose to set
     */
    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    /**
     * @return the loanApr
     */
    public double getLoanApr() {
        return loanApr;
    }

    /**
     * @param loanApr the loanApr to set
     */
    public void setLoanApr(double loanApr) {
        this.loanApr = loanApr;
    }

    public int getEmiBouncedCountInAYear() {
        return emiBouncedCountInAYear;
    }

    public void setEmiBouncedCountInAYear(int emiBouncedCountInAYear) {
        this.emiBouncedCountInAYear = emiBouncedCountInAYear;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getMifinBankCode(){return mifinBankCode;}

    public void setMifinBankCode(String mifinBankCode){this.mifinBankCode=mifinBankCode;}
}
