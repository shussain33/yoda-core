package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.multiproduct.MultiProductRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */

public class PostIPA {

    @JsonProperty("dApvAmt")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    @Valid
    private double approvedAmount;

    @JsonProperty("dBalAmt")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    @Valid
    private double balanceAmount;

    @JsonProperty("sScheme")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class})
    private String scheme;

    @JsonProperty("sSchemeDscr")
    @NotEmpty(groups = {PostIpaRequest.InsertGrp.class})
    private String schemeDscr;

    @JsonProperty("dTotAssCost")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double totalAssetCost;

    @JsonProperty("dMarMoney")
    private double marginMoney;

    @JsonProperty("sMarginMoneyInstru")
    private String marginMoneyInstrument;

    @JsonProperty("sMarMoneyConfirm")
    private String marginMoneyConfirmation;

    @JsonProperty("dAdvEmi")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double advanceEmi;

    @JsonProperty("dProcFees")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double processingFees;


    @JsonProperty("aAssMdl")
    private List<String> assetModel;


    @JsonProperty("aAssMdls")
    @NotNull(groups = {
            PostIpaRequest.InsertGrp.class,
            MultiProductRequest.ProductSelectionGrp.class
    })
    @Size(min = 1, groups = {
            PostIpaRequest.InsertGrp.class,
            MultiProductRequest.ProductSelectionGrp.class
    })
    @Valid
    private List<AssetDetails> assetDetails;

    @JsonProperty("dManfSubDel")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double manufSubBorneByDealer;

    @JsonProperty("dDelSubven")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double dealerSubvention;

    @JsonProperty("dOtherChrg")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double otherChargesIfAny;

    @JsonProperty("dFinanceAmt")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double financeAmount;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("iAdvEmiTenor")
    private int advanceEMITenor;

    @JsonProperty("dNetFundingAmt")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double netFundingAmount;

    @JsonProperty("dNetDisbursalAmt")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double netDisbursalAmount;

    @JsonProperty("dManSubMbd")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double manufSubventionMbd;

    @JsonProperty("dManProcFee")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double manufacturingProcessingFee;

    @JsonProperty("dEmi")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double emi;


    @JsonProperty("bDlrSbvnWaivedAtPOS")
    private boolean dlrSbvnWaivedAtPOS;

    /**
     * The revised emi is the sum of
     * Asset Emi(take from PostIpa) + Loyalty card Emi + Extended Warranty Emi + Insurance Emi
     */
    @JsonProperty("dRevisedEmi")
    private double revisedEmi;

    /**
     * The revised loan amount will be the sum of
     * finance amount + loyalty card fee + extended warranty premium + insurance premium
     */
    @JsonProperty("dRevisedLoanAmt")
    private double revisedLoanAmount;

    @JsonProperty("oExtWarranty")
    private ExtendedWarranty extendedWarranty;

    @JsonProperty("dRoi")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "100.0", groups = {PostIpaRequest.InsertGrp.class})
    private double roi;

    @JsonProperty("dTotalLoanAmt")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "1000000000.0", groups = {PostIpaRequest.InsertGrp.class})
    private double totalLoanAmount;

    @JsonProperty("dLtv")
    @DecimalMin(value = "0.0", groups = {PostIpaRequest.InsertGrp.class})
    @DecimalMax(value = "100.0", groups = {PostIpaRequest.InsertGrp.class})
    private double ltv;

    /**
     * this field added for do cancellation feature.
     * this field is used to store Delivery Order status i.e Created ,Mailed ,Cancelled ,Restored
     */
    @JsonProperty("sDeliveryOrderStatus")
    private String deliveryOrderStatus;

    @JsonProperty("dtEmiStartDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date emiStartDate;

    @JsonProperty("dtEmiEndDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date emiEndDate;


    @JsonProperty("sDownPaymentReceiptNo")
    private String downPaymentReceiptNo;

    @JsonProperty("dtDownPaymentDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date downPaymentDate;

    @JsonProperty("dDownPaymentAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double downPaymentAmt;

    @JsonProperty("dUmfc")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double umfc;

    @JsonProperty("dTotalRecvAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double totalRecvAmt;

    @JsonProperty("dTotalRecDwnAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double totalRecDwnAmt;

    @JsonProperty("dLoanAccAgrAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double loanAccAgrAmt;

    @JsonProperty("dLoanDisbursalAmt")
    @DecimalMin(value = "0.0")
    @DecimalMax(value = "1000000000.0")
    private double loanDisbursalAmt;

    @JsonProperty("dsumExtWrntAmtInsuranceAmt")
    @DecimalMin(value = "0.0")
    private double sumExtWrntAmtInsuranceAmt;

    public double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getSchemeDscr() {
        return schemeDscr;
    }

    public void setSchemeDscr(String schemeDscr) {
        this.schemeDscr = schemeDscr;
    }

    public double getTotalAssetCost() {
        return totalAssetCost;
    }

    public void setTotalAssetCost(double totalAssetCost) {
        this.totalAssetCost = totalAssetCost;
    }

    public double getMarginMoney() {
        return marginMoney;
    }

    public void setMarginMoney(double marginMoney) {
        this.marginMoney = marginMoney;
    }

    public String getMarginMoneyInstrument() {
        return marginMoneyInstrument;
    }

    public void setMarginMoneyInstrument(String marginMoneyInstrument) {
        this.marginMoneyInstrument = marginMoneyInstrument;
    }

    public String getMarginMoneyConfirmation() {
        return marginMoneyConfirmation;
    }

    public void setMarginMoneyConfirmation(String marginMoneyConfirmation) {
        this.marginMoneyConfirmation = marginMoneyConfirmation;
    }

    public double getAdvanceEmi() {
        return advanceEmi;
    }

    public void setAdvanceEmi(double advanceEmi) {
        this.advanceEmi = advanceEmi;
    }

    public double getProcessingFees() {
        return processingFees;
    }

    public void setProcessingFees(double processingFees) {
        this.processingFees = processingFees;
    }

    public List<String> getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(List<String> assetModel) {
        this.assetModel = assetModel;
    }

    public List<AssetDetails> getAssetDetails() {
        return assetDetails;
    }

    public void setAssetDetails(List<AssetDetails> assetDetails) {
        this.assetDetails = assetDetails;
    }

    public double getManufSubBorneByDealer() {
        return manufSubBorneByDealer;
    }

    public void setManufSubBorneByDealer(double manufSubBorneByDealer) {
        this.manufSubBorneByDealer = manufSubBorneByDealer;
    }

    public double getDealerSubvention() {
        return dealerSubvention;
    }

    public void setDealerSubvention(double dealerSubvention) {
        this.dealerSubvention = dealerSubvention;
    }

    public double getOtherChargesIfAny() {
        return otherChargesIfAny;
    }

    public void setOtherChargesIfAny(double otherChargesIfAny) {
        this.otherChargesIfAny = otherChargesIfAny;
    }

    public double getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(double financeAmount) {
        this.financeAmount = financeAmount;
    }

    public int getTenor() {
        return tenor;
    }

    public void setTenor(int tenor) {
        this.tenor = tenor;
    }

    public int getAdvanceEMITenor() {
        return advanceEMITenor;
    }

    public void setAdvanceEMITenor(int advanceEMITenor) {
        this.advanceEMITenor = advanceEMITenor;
    }

    public double getNetFundingAmount() {
        return netFundingAmount;
    }

    public void setNetFundingAmount(double netFundingAmount) {
        this.netFundingAmount = netFundingAmount;
    }

    public double getNetDisbursalAmount() {
        return netDisbursalAmount;
    }

    public void setNetDisbursalAmount(double netDisbursalAmount) {
        this.netDisbursalAmount = netDisbursalAmount;
    }

    public double getManufSubventionMbd() {
        return manufSubventionMbd;
    }

    public void setManufSubventionMbd(double manufSubventionMbd) {
        this.manufSubventionMbd = manufSubventionMbd;
    }

    public double getManufacturingProcessingFee() {
        return manufacturingProcessingFee;
    }

    public void setManufacturingProcessingFee(double manufacturingProcessingFee) {
        this.manufacturingProcessingFee = manufacturingProcessingFee;
    }

    public double getEmi() {
        return emi;
    }

    public void setEmi(double emi) {
        this.emi = emi;
    }

    public boolean isDlrSbvnWaivedAtPOS() {
        return dlrSbvnWaivedAtPOS;
    }

    public void setDlrSbvnWaivedAtPOS(boolean dlrSbvnWaivedAtPOS) {
        this.dlrSbvnWaivedAtPOS = dlrSbvnWaivedAtPOS;
    }

    public double getRevisedEmi() {
        return revisedEmi;
    }

    public void setRevisedEmi(double revisedEmi) {
        this.revisedEmi = revisedEmi;
    }

    public double getRevisedLoanAmount() {
        return revisedLoanAmount;
    }

    public void setRevisedLoanAmount(double revisedLoanAmount) {
        this.revisedLoanAmount = revisedLoanAmount;
    }

    public ExtendedWarranty getExtendedWarranty() {
        return extendedWarranty;
    }

    public void setExtendedWarranty(ExtendedWarranty extendedWarranty) {
        this.extendedWarranty = extendedWarranty;
    }

    public double getRoi() {
        return roi;
    }

    public void setRoi(double roi) {
        this.roi = roi;
    }

    public double getTotalLoanAmount() {
        return totalLoanAmount;
    }

    public void setTotalLoanAmount(double totalLoanAmount) {
        this.totalLoanAmount = totalLoanAmount;
    }

    public double getLtv() {
        return ltv;
    }

    public void setLtv(double ltv) {
        this.ltv = ltv;
    }

    public String getDeliveryOrderStatus() {
        return deliveryOrderStatus;
    }

    public void setDeliveryOrderStatus(String deliveryOrderStatus) {
        this.deliveryOrderStatus = deliveryOrderStatus;
    }

    public Date getEmiStartDate() {
        return emiStartDate;
    }

    public void setEmiStartDate(Date emiStartDate) {
        this.emiStartDate = emiStartDate;
    }

    public Date getEmiEndDate() {
        return emiEndDate;
    }

    public void setEmiEndDate(Date emiEndDate) {
        this.emiEndDate = emiEndDate;
    }

    public String getDownPaymentReceiptNo() {
        return downPaymentReceiptNo;
    }

    public void setDownPaymentReceiptNo(String downPaymentReceiptNo) {
        this.downPaymentReceiptNo = downPaymentReceiptNo;
    }

    public Date getDownPaymentDate() {
        return downPaymentDate;
    }

    public void setDownPaymentDate(Date downPaymentDate) {
        this.downPaymentDate = downPaymentDate;
    }

    public double getDownPaymentAmt() {
        return downPaymentAmt;
    }

    public void setDownPaymentAmt(double downPaymentAmt) {
        this.downPaymentAmt = downPaymentAmt;
    }

    public double getUmfc() {
        return umfc;
    }

    public void setUmfc(double umfc) {
        this.umfc = umfc;
    }

    public double getTotalRecvAmt() {
        return totalRecvAmt;
    }

    public void setTotalRecvAmt(double totalRecvAmt) {
        this.totalRecvAmt = totalRecvAmt;
    }

    public double getTotalRecDwnAmt() {
        return totalRecDwnAmt;
    }

    public void setTotalRecDwnAmt(double totalRecDwnAmt) {
        this.totalRecDwnAmt = totalRecDwnAmt;
    }

    public double getLoanAccAgrAmt() {
        return loanAccAgrAmt;
    }

    public void setLoanAccAgrAmt(double loanAccAgrAmt) {
        this.loanAccAgrAmt = loanAccAgrAmt;
    }

    public double getLoanDisbursalAmt() { return loanDisbursalAmt; }

    public void setLoanDisbursalAmt(double loanDisbursalAmt) { this.loanDisbursalAmt = loanDisbursalAmt; }

    public double getSumExtWrntAmtInsuranceAmt() {
        return sumExtWrntAmtInsuranceAmt;
    }

    public void setSumExtWrntAmtInsuranceAmt(double sumExtWrntAmtInsuranceAmt) {
        this.sumExtWrntAmtInsuranceAmt = sumExtWrntAmtInsuranceAmt;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PostIPA{");
        sb.append("approvedAmount=").append(approvedAmount);
        sb.append(", balanceAmount=").append(balanceAmount);
        sb.append(", scheme='").append(scheme).append('\'');
        sb.append(", schemeDscr='").append(schemeDscr).append('\'');
        sb.append(", totalAssetCost=").append(totalAssetCost);
        sb.append(", marginMoney=").append(marginMoney);
        sb.append(", marginMoneyInstrument='").append(marginMoneyInstrument).append('\'');
        sb.append(", marginMoneyConfirmation='").append(marginMoneyConfirmation).append('\'');
        sb.append(", advanceEmi=").append(advanceEmi);
        sb.append(", processingFees=").append(processingFees);
        sb.append(", assetModel=").append(assetModel);
        sb.append(", assetDetails=").append(assetDetails);
        sb.append(", manufSubBorneByDealer=").append(manufSubBorneByDealer);
        sb.append(", dealerSubvention=").append(dealerSubvention);
        sb.append(", otherChargesIfAny=").append(otherChargesIfAny);
        sb.append(", financeAmount=").append(financeAmount);
        sb.append(", tenor=").append(tenor);
        sb.append(", advanceEMITenor=").append(advanceEMITenor);
        sb.append(", netFundingAmount=").append(netFundingAmount);
        sb.append(", netDisbursalAmount=").append(netDisbursalAmount);
        sb.append(", manufSubventionMbd=").append(manufSubventionMbd);
        sb.append(", manufacturingProcessingFee=").append(manufacturingProcessingFee);
        sb.append(", emi=").append(emi);
        sb.append(", dlrSbvnWaivedAtPOS=").append(dlrSbvnWaivedAtPOS);
        sb.append(", revisedEmi=").append(revisedEmi);
        sb.append(", revisedLoanAmount=").append(revisedLoanAmount);
        sb.append(", extendedWarranty=").append(extendedWarranty);
        sb.append(", roi=").append(roi);
        sb.append(", totalLoanAmount=").append(totalLoanAmount);
        sb.append(", ltv=").append(ltv);
        sb.append(", deliveryOrderStatus='").append(deliveryOrderStatus).append('\'');
        sb.append(", emiStartDate=").append(emiStartDate);
        sb.append(", emiEndDate=").append(emiEndDate);
        sb.append(", downPaymentReceiptNo='").append(downPaymentReceiptNo).append('\'');
        sb.append(", downPaymentDate=").append(downPaymentDate);
        sb.append(", downPaymentAmt=").append(downPaymentAmt);
        sb.append(", umfc=").append(umfc);
        sb.append(", totalRecvAmt=").append(totalRecvAmt);
        sb.append(", totalRecDwnAmt=").append(totalRecDwnAmt);
        sb.append(", loanAccAgrAmt=").append(loanAccAgrAmt);
        sb.append(", loanDisbursalAmt=").append(loanDisbursalAmt);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostIPA postIPA = (PostIPA) o;

        if (Double.compare(postIPA.approvedAmount, approvedAmount) != 0) return false;
        if (Double.compare(postIPA.balanceAmount, balanceAmount) != 0) return false;
        if (Double.compare(postIPA.totalAssetCost, totalAssetCost) != 0) return false;
        if (Double.compare(postIPA.marginMoney, marginMoney) != 0) return false;
        if (Double.compare(postIPA.advanceEmi, advanceEmi) != 0) return false;
        if (Double.compare(postIPA.processingFees, processingFees) != 0) return false;
        if (Double.compare(postIPA.manufSubBorneByDealer, manufSubBorneByDealer) != 0) return false;
        if (Double.compare(postIPA.dealerSubvention, dealerSubvention) != 0) return false;
        if (Double.compare(postIPA.otherChargesIfAny, otherChargesIfAny) != 0) return false;
        if (Double.compare(postIPA.financeAmount, financeAmount) != 0) return false;
        if (tenor != postIPA.tenor) return false;
        if (advanceEMITenor != postIPA.advanceEMITenor) return false;
        if (Double.compare(postIPA.netFundingAmount, netFundingAmount) != 0) return false;
        if (Double.compare(postIPA.netDisbursalAmount, netDisbursalAmount) != 0) return false;
        if (Double.compare(postIPA.manufSubventionMbd, manufSubventionMbd) != 0) return false;
        if (Double.compare(postIPA.manufacturingProcessingFee, manufacturingProcessingFee) != 0) return false;
        if (Double.compare(postIPA.emi, emi) != 0) return false;
        if (dlrSbvnWaivedAtPOS != postIPA.dlrSbvnWaivedAtPOS) return false;
        if (Double.compare(postIPA.revisedEmi, revisedEmi) != 0) return false;
        if (Double.compare(postIPA.revisedLoanAmount, revisedLoanAmount) != 0) return false;
        if (Double.compare(postIPA.roi, roi) != 0) return false;
        if (Double.compare(postIPA.totalLoanAmount, totalLoanAmount) != 0) return false;
        if (Double.compare(postIPA.ltv, ltv) != 0) return false;
        if (Double.compare(postIPA.downPaymentAmt, downPaymentAmt) != 0) return false;
        if (scheme != null ? !scheme.equals(postIPA.scheme) : postIPA.scheme != null) return false;
        if (schemeDscr != null ? !schemeDscr.equals(postIPA.schemeDscr) : postIPA.schemeDscr != null) return false;
        if (marginMoneyInstrument != null ? !marginMoneyInstrument.equals(postIPA.marginMoneyInstrument) : postIPA.marginMoneyInstrument != null)
            return false;
        if (marginMoneyConfirmation != null ? !marginMoneyConfirmation.equals(postIPA.marginMoneyConfirmation) : postIPA.marginMoneyConfirmation != null)
            return false;
        if (assetModel != null ? !assetModel.equals(postIPA.assetModel) : postIPA.assetModel != null) return false;
        if (assetDetails != null ? !assetDetails.equals(postIPA.assetDetails) : postIPA.assetDetails != null)
            return false;
        if (extendedWarranty != null ? !extendedWarranty.equals(postIPA.extendedWarranty) : postIPA.extendedWarranty != null)
            return false;
        if (deliveryOrderStatus != null ? !deliveryOrderStatus.equals(postIPA.deliveryOrderStatus) : postIPA.deliveryOrderStatus != null)
            return false;
        if (emiStartDate != null ? !emiStartDate.equals(postIPA.emiStartDate) : postIPA.emiStartDate != null)
            return false;
        if (emiEndDate != null ? !emiEndDate.equals(postIPA.emiEndDate) : postIPA.emiEndDate != null) return false;
        if (downPaymentReceiptNo != null ? !downPaymentReceiptNo.equals(postIPA.downPaymentReceiptNo) : postIPA.downPaymentReceiptNo != null)
            return false;
        return !(downPaymentDate != null ? !downPaymentDate.equals(postIPA.downPaymentDate) : postIPA.downPaymentDate != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(approvedAmount);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(balanceAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (scheme != null ? scheme.hashCode() : 0);
        result = 31 * result + (schemeDscr != null ? schemeDscr.hashCode() : 0);
        temp = Double.doubleToLongBits(totalAssetCost);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(marginMoney);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (marginMoneyInstrument != null ? marginMoneyInstrument.hashCode() : 0);
        result = 31 * result + (marginMoneyConfirmation != null ? marginMoneyConfirmation.hashCode() : 0);
        temp = Double.doubleToLongBits(advanceEmi);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(processingFees);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (assetModel != null ? assetModel.hashCode() : 0);
        result = 31 * result + (assetDetails != null ? assetDetails.hashCode() : 0);
        temp = Double.doubleToLongBits(manufSubBorneByDealer);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(dealerSubvention);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherChargesIfAny);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(financeAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + tenor;
        result = 31 * result + advanceEMITenor;
        temp = Double.doubleToLongBits(netFundingAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(netDisbursalAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(manufSubventionMbd);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(manufacturingProcessingFee);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(emi);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (dlrSbvnWaivedAtPOS ? 1 : 0);
        temp = Double.doubleToLongBits(revisedEmi);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(revisedLoanAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (extendedWarranty != null ? extendedWarranty.hashCode() : 0);
        temp = Double.doubleToLongBits(roi);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(totalLoanAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ltv);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (deliveryOrderStatus != null ? deliveryOrderStatus.hashCode() : 0);
        result = 31 * result + (emiStartDate != null ? emiStartDate.hashCode() : 0);
        result = 31 * result + (emiEndDate != null ? emiEndDate.hashCode() : 0);
        result = 31 * result + (downPaymentReceiptNo != null ? downPaymentReceiptNo.hashCode() : 0);
        result = 31 * result + (downPaymentDate != null ? downPaymentDate.hashCode() : 0);
        temp = Double.doubleToLongBits(downPaymentAmt);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
