package com.softcell.gonogo.model.core.kyc.response.karza.companyllpidentification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CompanyIdentificationResponseDetails {

    @JsonProperty("Whether_Listed_or_not")
    private String Whether_Listed_or_not;

    @JsonProperty("Company_Status")
    private String Company_Status;

    @JsonProperty("ROC_Code")
    private String ROC_Code;

    @JsonProperty("Company_SubCategory")
    private String Company_SubCategory;

    @JsonProperty("Email_Id")
    private String Email_Id;

    @JsonProperty("Suspended_at_stock_exchange")
    private String Suspended_at_stock_exchange;

    @JsonProperty("alternative_address")
    private String alternative_address;

    @JsonProperty("Date_of_Balance_Sheet")
    private String Date_of_Balance_Sheet;

    @JsonProperty("cin")
    private String cin;

    @JsonProperty("Authorised_Capital(Rs)")
    private String Authorised_Capital_Rs;

    @JsonProperty("Date_of_last_AGM")
    private String Date_of_last_AGM;

    @JsonProperty("Company_Name")
    private String Company_Name;

    @JsonProperty("Paid_up_Capital(Rs)")
    private String Paid_up_Capital_Rs;

    @JsonProperty("Registered_Address")
    private String Registered_Address;

    @JsonProperty("Number_of_Members")
    private String Number_of_Members;

    @JsonProperty("Class_of_Company")
    private String Class_of_Company;

    @JsonProperty("Registration_Number")
    private String Registration_Number;

    @JsonProperty("Date_of_Incorporation")
    private String Date_of_Incorporation;

}