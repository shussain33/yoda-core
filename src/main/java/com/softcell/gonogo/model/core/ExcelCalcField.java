package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 8/11/19.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelCalcField {

    @JsonProperty("sCellNumber")
    private String cellNumber;

    @JsonProperty("iSheetNumber")
    private int sheetNumber;

    @JsonProperty("sCellType")
    private String cellType;

    // cellData represents the domain specific field name like premium, sumAssured
    @JsonProperty("sCellData")
    private String cellData;
}
