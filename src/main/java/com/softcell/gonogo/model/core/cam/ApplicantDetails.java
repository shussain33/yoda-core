package com.softcell.gonogo.model.core.cam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Employment;
import com.softcell.gonogo.model.core.Name;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg237 on 2/4/21.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicantDetails {

    @JsonProperty("sApplID")
    private String applicantId;

    @JsonProperty("oApplName")
    private Name applicantName;

    @JsonProperty("iAge")
    private int age;

    @JsonProperty("sPhoneNumber")
    private String phoneNumber;

    @JsonProperty("sType")
    private String applicantType;

    @JsonProperty("sRelWithAppl")
    private String relationWithApplicant;

    @JsonProperty("sEmplType")
    private String employmentType;

    @JsonProperty("JSON-RESPONSE-OBJECT")
    private Object responseJsonObject;

    @JsonProperty("sDtJoin")
    private String dtJoin;

    @JsonProperty("sAddrType")
    private String addressType;

    @JsonProperty("iTimeAtAddrResi")
    private int timeAtAddrResi;

    @JsonProperty("iTimeAtAddrOfc")
    private int timeAtAddrOfc;

    @JsonProperty("sRvStatus")
    private String rvStatus;

    @JsonProperty("sOvStatus")
    private String ovStatus;

}
