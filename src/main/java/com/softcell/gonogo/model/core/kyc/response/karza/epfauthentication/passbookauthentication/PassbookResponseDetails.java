package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.passbookauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.ESTDetails;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PassbookResponseDetails {

    @JsonProperty("est_details")
    public List<ESTDetails> estDetailsList;

    @JsonProperty("employee_details")
    public EmployeeDetails employeeDetailsList;

}