package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**

 * Created by Amit on 23/2/18.
 */

@Document(collection = "propertyVisit")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PropertyVisit {

	@Id
	@JsonProperty("sRefId")
	private String refId;

	@JsonProperty("sInstitutionId")
	public String institutionId;

	@JsonProperty("aPropertyVisitDetls")
	List<PropertyVisitDetails> propertyVisitDetailsList;

	@JsonProperty("sAgencyCode")
	private String agencyCode;

	@JsonProperty("sStatus")
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAgencyCode() {
		return agencyCode;
	}

	public void setAgencyCode(String agencyCode) {
		this.agencyCode = agencyCode;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(String institutionId) {
		this.institutionId = institutionId;
	}

	public List<PropertyVisitDetails> getPropertyVisitDetailsList() {		return propertyVisitDetailsList;	}

	public void setPropertyVisitDetailsList(List<PropertyVisitDetails> propertyVisitDetailsList) {		this.propertyVisitDetailsList = propertyVisitDetailsList;	}
}

