package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by mahesh on 17/10/17.
 */
@Data
public class CancelDoNotRaisedApplicationRequest {


    @JsonProperty("oHeader")
    @NotNull(groups = {CancelDoNotRaisedApplicationRequest.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("dtStartDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date startDate;

    @JsonProperty("dtEndDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Kolkata", pattern = "ddMMyyyy")
    private Date endDate;

    @JsonProperty("iPastNoOfDays")
    private int pastNoOfDays;

    @JsonProperty("sAppStat")
    @NotEmpty(groups = {CancelDoNotRaisedApplicationRequest.FetchGrp.class})
    private String appStatus;

    @JsonProperty("sAppStage")
    @NotEmpty(groups = {CancelDoNotRaisedApplicationRequest.FetchGrp.class})
    private String appStage;

    @JsonProperty("sBatchCount")
    private int batchCount;

    @JsonProperty("iLimit")
    private int limit;

    @JsonProperty("iSkip")
    private int skip;


    public interface FetchGrp {
    }

}
