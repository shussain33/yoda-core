package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class KycResponse {
    @JsonProperty("AADHAR-RESPONSE-DETAILS")
    private AadharResponseDetails aadharResponseDetails;

    @JsonProperty("BFD-RESPONSE-DETAILS")
    private BfdResponseDetails bfdResponseDetails;

    @JsonProperty("OTP-RESPONSE-DETAILS")
    private OtpResponseDetails otpResponseDetails;

    @JsonProperty("KYC-RESPONSE-DETAILS")
    private KycResponseDetails kycResponseDetails;

    public AadharResponseDetails getAadharResponseDetails() {
        return aadharResponseDetails;
    }

    public void setAadharResponseDetails(AadharResponseDetails aadharResponseDetails) {
        this.aadharResponseDetails = aadharResponseDetails;
    }

    public BfdResponseDetails getBfdResponseDetails() {
        return bfdResponseDetails;
    }

    public void setBfdResponseDetails(BfdResponseDetails bfdResponseDetails) {
        this.bfdResponseDetails = bfdResponseDetails;
    }

    public OtpResponseDetails getOtpResponseDetails() {
        return otpResponseDetails;
    }

    public void setOtpResponseDetails(OtpResponseDetails otpResponseDetails) {
        this.otpResponseDetails = otpResponseDetails;
    }

    public KycResponseDetails getKycResponseDetails() {
        return kycResponseDetails;
    }

    public void setKycResponseDetails(KycResponseDetails kycResponseDetails) {
        this.kycResponseDetails = kycResponseDetails;
    }

    @Override
    public String toString() {
        return "KycResponse [aadharResponseDetails=" + aadharResponseDetails
                + ", bfdResponseDetails=" + bfdResponseDetails
                + ", otpResponseDetails=" + otpResponseDetails
                + ", kycResponseDetails=" + kycResponseDetails + "]";
    }


}
