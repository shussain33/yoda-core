package com.softcell.gonogo.model.core.request.scoring;


import java.math.BigDecimal;


public class Account {


    private String clientName;

    private String accountNumber;

    private String currentBalance;

    private String institution;

    private String accountType;

    private String ownershipType;

    private String balance;

    private String pastDueAmount;

    private String disbursedAmount;
    private String loanCategory;
    private String loanPurpose;

    private String lastPayment;

    private String writeOffAmount;

    private String open;

    private String sanctionAmount;

    private String lastPaymentDate;

    private String highCredit;

    private String reportedDate;

    private String dateOpened;

    private String dateClosed;

    private String reason;

    private KeyPerson keyPerson;
    private String dateWrittenOff;
    private String loanCycleID;
    private String dateSanctioned;
    private String dateApplied;

    private String interestRate;

    private String appliedAmount;
    private String noOfInstallments;

    private String repaymentTenure;

    private String disputeCode;

    private String installmentAmount;


    private Nominee nominee;

    private String termFrequency;

    private String creditLimit;

    private String collateralValue;

    private String collateralType;

    private String accountStatus;

    private String assetClassification;

    private String suitFiledStatus;

    private String dateReported;

    private History48Months history48Months;

    private History24Months history24Months;

    private String tempHistory48Month;

    private String tempHistory24Month;

    private AdditionalMFIDetailsType additionalMFIDetails;
    private String datePastDue;
    private String branchIDMFI;
    private String kendraIDMFI;
    private Integer daysPastDue;
    private String typeOfInsurance;
    private BigDecimal insurancePolicyAmount;
    private Integer numberOfMeetingsHeld;
    private Integer numberOfMeetingsMissed;

    private String typeCode;

    private Integer seq;


    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getOwnershipType() {
        return ownershipType;
    }

    public void setOwnershipType(String ownershipType) {
        this.ownershipType = ownershipType;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPastDueAmount() {
        return pastDueAmount;
    }

    public void setPastDueAmount(String pastDueAmount) {
        this.pastDueAmount = pastDueAmount;
    }

    public String getDisbursedAmount() {
        return disbursedAmount;
    }

    public void setDisbursedAmount(String disbursedAmount) {
        this.disbursedAmount = disbursedAmount;
    }

    public String getLoanCategory() {
        return loanCategory;
    }

    public void setLoanCategory(String loanCategory) {
        this.loanCategory = loanCategory;
    }

    public String getLoanPurpose() {
        return loanPurpose;
    }

    public void setLoanPurpose(String loanPurpose) {
        this.loanPurpose = loanPurpose;
    }

    public String getLastPayment() {
        return lastPayment;
    }

    public void setLastPayment(String lastPayment) {
        this.lastPayment = lastPayment;
    }

    public String getWriteOffAmount() {
        return writeOffAmount;
    }

    public void setWriteOffAmount(String writeOffAmount) {
        this.writeOffAmount = writeOffAmount;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getSanctionAmount() {
        return sanctionAmount;
    }

    public void setSanctionAmount(String sanctionAmount) {
        this.sanctionAmount = sanctionAmount;
    }

    public String getLastPaymentDate() {
        return lastPaymentDate;
    }

    public void setLastPaymentDate(String lastPaymentDate) {
        this.lastPaymentDate = lastPaymentDate;
    }

    public String getHighCredit() {
        return highCredit;
    }

    public void setHighCredit(String highCredit) {
        this.highCredit = highCredit;
    }

    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(String dateOpened) {
        this.dateOpened = dateOpened;
    }

    public String getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(String dateClosed) {
        this.dateClosed = dateClosed;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDateWrittenOff() {
        return dateWrittenOff;
    }

    public void setDateWrittenOff(String dateWrittenOff) {
        this.dateWrittenOff = dateWrittenOff;
    }

    public String getLoanCycleID() {
        return loanCycleID;
    }

    public void setLoanCycleID(String loanCycleID) {
        this.loanCycleID = loanCycleID;
    }

    public String getDateSanctioned() {
        return dateSanctioned;
    }

    public void setDateSanctioned(String dateSanctioned) {
        this.dateSanctioned = dateSanctioned;
    }

    public String getDateApplied() {
        return dateApplied;
    }

    public void setDateApplied(String dateApplied) {
        this.dateApplied = dateApplied;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(String appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public String getNoOfInstallments() {
        return noOfInstallments;
    }

    public void setNoOfInstallments(String noOfInstallments) {
        this.noOfInstallments = noOfInstallments;
    }

    public String getRepaymentTenure() {
        return repaymentTenure;
    }

    public void setRepaymentTenure(String repaymentTenure) {
        this.repaymentTenure = repaymentTenure;
    }

    public String getDisputeCode() {
        return disputeCode;
    }

    public void setDisputeCode(String disputeCode) {
        this.disputeCode = disputeCode;
    }

    public String getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(String installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public KeyPerson getKeyPerson() {
        return keyPerson;
    }

    public void setKeyPerson(KeyPerson keyPerson) {
        this.keyPerson = keyPerson;
    }

    public Nominee getNominee() {
        return nominee;
    }

    public void setNominee(Nominee nominee) {
        this.nominee = nominee;
    }

    public String getTermFrequency() {
        return termFrequency;
    }

    public void setTermFrequency(String termFrequency) {
        this.termFrequency = termFrequency;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCollateralValue() {
        return collateralValue;
    }

    public void setCollateralValue(String collateralValue) {
        this.collateralValue = collateralValue;
    }

    public String getCollateralType() {
        return collateralType;
    }

    public void setCollateralType(String collateralType) {
        this.collateralType = collateralType;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAssetClassification() {
        return assetClassification;
    }

    public void setAssetClassification(String assetClassification) {
        this.assetClassification = assetClassification;
    }

    public String getSuitFiledStatus() {
        return suitFiledStatus;
    }

    public void setSuitFiledStatus(String suitFiledStatus) {
        this.suitFiledStatus = suitFiledStatus;
    }

    public String getDateReported() {
        return dateReported;
    }

    public void setDateReported(String dateReported) {
        this.dateReported = dateReported;
    }

    public History48Months getHistory48Months() {
        return history48Months;
    }

    public void setHistory48Months(History48Months history48Months) {
        this.history48Months = history48Months;
    }

    public History24Months getHistory24Months() {
        return history24Months;
    }

    public void setHistory24Months(History24Months history24Months) {
        this.history24Months = history24Months;
    }

    public String getTempHistory48Month() {
        return tempHistory48Month;
    }

    public void setTempHistory48Month(String tempHistory48Month) {
        this.tempHistory48Month = tempHistory48Month;
    }

    public String getTempHistory24Month() {
        return tempHistory24Month;
    }

    public void setTempHistory24Month(String tempHistory24Month) {
        this.tempHistory24Month = tempHistory24Month;
    }

    public AdditionalMFIDetailsType getAdditionalMFIDetails() {
        return additionalMFIDetails;
    }

    public void setAdditionalMFIDetails(
            AdditionalMFIDetailsType additionalMFIDetails) {
        this.additionalMFIDetails = additionalMFIDetails;
    }

    public String getDatePastDue() {
        return datePastDue;
    }

    public void setDatePastDue(String datePastDue) {
        this.datePastDue = datePastDue;
    }

    public String getBranchIDMFI() {
        return branchIDMFI;
    }

    public void setBranchIDMFI(String branchIDMFI) {
        this.branchIDMFI = branchIDMFI;
    }

    public String getKendraIDMFI() {
        return kendraIDMFI;
    }

    public void setKendraIDMFI(String kendraIDMFI) {
        this.kendraIDMFI = kendraIDMFI;
    }

    public Integer getDaysPastDue() {
        return daysPastDue;
    }

    public void setDaysPastDue(Integer daysPastDue) {
        this.daysPastDue = daysPastDue;
    }

    public String getTypeOfInsurance() {
        return typeOfInsurance;
    }

    public void setTypeOfInsurance(String typeOfInsurance) {
        this.typeOfInsurance = typeOfInsurance;
    }

    public BigDecimal getInsurancePolicyAmount() {
        return insurancePolicyAmount;
    }

    public void setInsurancePolicyAmount(BigDecimal insurancePolicyAmount) {
        this.insurancePolicyAmount = insurancePolicyAmount;
    }

    public Integer getNumberOfMeetingsHeld() {
        return numberOfMeetingsHeld;
    }

    public void setNumberOfMeetingsHeld(Integer numberOfMeetingsHeld) {
        this.numberOfMeetingsHeld = numberOfMeetingsHeld;
    }

    public Integer getNumberOfMeetingsMissed() {
        return numberOfMeetingsMissed;
    }

    public void setNumberOfMeetingsMissed(Integer numberOfMeetingsMissed) {
        this.numberOfMeetingsMissed = numberOfMeetingsMissed;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @Override
    public String toString() {
        return "Account [clientName=" + clientName + ", accountNumber="
                + accountNumber + ", currentBalance=" + currentBalance
                + ", institution=" + institution + ", accountType="
                + accountType + ", ownershipType=" + ownershipType
                + ", balance=" + balance + ", pastDueAmount=" + pastDueAmount
                + ", disbursedAmount=" + disbursedAmount + ", loanCategory="
                + loanCategory + ", loanPurpose=" + loanPurpose
                + ", lastPayment=" + lastPayment + ", writeOffAmount="
                + writeOffAmount + ", open=" + open + ", sanctionAmount="
                + sanctionAmount + ", lastPaymentDate=" + lastPaymentDate
                + ", highCredit=" + highCredit + ", reportedDate="
                + reportedDate + ", dateOpened=" + dateOpened + ", dateClosed="
                + dateClosed + ", reason=" + reason + ", dateWrittenOff="
                + dateWrittenOff + ", loanCycleID=" + loanCycleID
                + ", dateSanctioned=" + dateSanctioned + ", dateApplied="
                + dateApplied + ", interestRate=" + interestRate
                + ", appliedAmount=" + appliedAmount + ", noOfInstallments="
                + noOfInstallments + ", repaymentTenure=" + repaymentTenure
                + ", disputeCode=" + disputeCode + ", installmentAmount="
                + installmentAmount + ", keyPerson=" + keyPerson + ", nominee="
                + nominee + ", termFrequency=" + termFrequency
                + ", creditLimit=" + creditLimit + ", collateralValue="
                + collateralValue + ", collateralType=" + collateralType
                + ", accountStatus=" + accountStatus + ", assetClassification="
                + assetClassification + ", suitFiledStatus=" + suitFiledStatus
                + ", dateReported=" + dateReported + ", history48Months="
                + history48Months + ", history24Months=" + history24Months
                + ", tempHistory48Month=" + tempHistory48Month
                + ", tempHistory24Month=" + tempHistory24Month
                + ", additionalMFIDetails=" + additionalMFIDetails
                + ", datePastDue=" + datePastDue + ", branchIDMFI="
                + branchIDMFI + ", kendraIDMFI=" + kendraIDMFI
                + ", daysPastDue=" + daysPastDue + ", typeOfInsurance="
                + typeOfInsurance + ", insurancePolicyAmount="
                + insurancePolicyAmount + ", numberOfMeetingsHeld="
                + numberOfMeetingsHeld + ", numberOfMeetingsMissed="
                + numberOfMeetingsMissed + ", typeCode=" + typeCode + "]";
    }
}
