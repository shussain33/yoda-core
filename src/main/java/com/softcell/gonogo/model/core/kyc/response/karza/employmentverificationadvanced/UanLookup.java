package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class UanLookup {

    @JsonProperty("currentEmployer")
    private String currentEmployer;

    @JsonProperty("matchScore")
    private double matchScore;

    @JsonProperty("result")
    private boolean result;

    @JsonProperty("uanNameMatch")
    private boolean uanNameMatch;
}
