package com.softcell.gonogo.model.core.request.scoring;

public class Month {

    private String paymentStatus;
    private String suitFiledStatus;
    private String assetClassificationStatus;
    private String monthKey;

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getSuitFiledStatus() {
        return suitFiledStatus;
    }

    public void setSuitFiledStatus(String suitFiledStatus) {
        this.suitFiledStatus = suitFiledStatus;
    }

    public String getAssetClassificationStatus() {
        return assetClassificationStatus;
    }

    public void setAssetClassificationStatus(String assetClassificationStatus) {
        this.assetClassificationStatus = assetClassificationStatus;
    }

    public String getMonthKey() {
        return monthKey;
    }

    public void setMonthKey(String monthKey) {
        this.monthKey = monthKey;
    }

    @Override
    public String toString() {
        return "Month [paymentStatus=" + paymentStatus
                + ", suitFiledStatus=" + suitFiledStatus
                + ", assetClassificationStatus=" + assetClassificationStatus
                + ", monthKey=" + monthKey + "]";
    }
}
