/**
 * Administrator4:57:38 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author Administrator
 *
 */
public class IncomeDetails implements Serializable {

    @JsonProperty("sDocAvail")
    private String incomeDocumentAvailable;
    @JsonProperty("aOthrSrcInc")
    private List<String> otherSourceOfIncome;
    @JsonProperty("dOthrSrcIncAmt")
    private double otherSourceIncomeAmount;
    @JsonProperty("oSalriedDtl")
    private SalariedDetails salariedDetails;
    @JsonProperty("aSenpPflIncm")
    private List<SENPProfileIncome> senpProfileIncome;

    // Net Monthly
    @JsonProperty("dNetMonthlyAmt")
    private double netMonthlyAmt;

    //Gross Monthly
    @JsonProperty("dGrossMonthlyAmt")
    private double grossMonthlyAmt;

    //Additional Monthly Income
    @JsonProperty("dOtherSrcMnthlyIncmAmt")
    private double otherSrcMnthlyIncmAmt;

    //Total Family monthly Income
    @JsonProperty("dTotFmlyMnthlyIncmAmt")
    private double totFmlyMnthlyIncmAmt;

    public double getOtherSourceIncomeAmount() {
        return otherSourceIncomeAmount;
    }

    public void setOtherSourceIncomeAmount(double otherSourceIncomeAmount) {
        this.otherSourceIncomeAmount = otherSourceIncomeAmount;
    }

    public SalariedDetails getSalariedDetails() {
        return salariedDetails;
    }

    public void setSalariedDetails(SalariedDetails salariedDetails) {
        this.salariedDetails = salariedDetails;
    }

    public List<SENPProfileIncome> getSenpProfileIncome() {
        return senpProfileIncome;
    }

    public void setSenpProfileIncome(List<SENPProfileIncome> senpProfileIncome) {
        this.senpProfileIncome = senpProfileIncome;
    }

    /**
     * @return the incomeDocumentAvailable
     */
    public String getIncomeDocumentAvailable() {
        return incomeDocumentAvailable;
    }

    /**
     * @param incomeDocumentAvailable
     *            the incomeDocumentAvailable to set
     */
    public void setIncomeDocumentAvailable(String incomeDocumentAvailable) {
        this.incomeDocumentAvailable = incomeDocumentAvailable;
    }

    /**
     * @return the otherSourceOfIncome
     */
    public List<String> getOtherSourceOfIncome() {
        return otherSourceOfIncome;
    }

    /**
     * @param otherSourceOfIncome
     *            the otherSourceOfIncome to set
     */
    public void setOtherSourceOfIncome(List<String> otherSourceOfIncome) {
        this.otherSourceOfIncome = otherSourceOfIncome;
    }

    public double getNetMonthlyAmt() {
        return netMonthlyAmt;
    }

    public void setNetMonthlyAmt(double netMonthlyAmt) {
        this.netMonthlyAmt = netMonthlyAmt;
    }

    public double getGrossMonthlyAmt() {
        return grossMonthlyAmt;
    }

    public void setGrossMonthlyAmt(double grossMonthlyAmt) {
        this.grossMonthlyAmt = grossMonthlyAmt;
    }

    public double getOtherSrcMnthlyIncmAmt() {
        return otherSrcMnthlyIncmAmt;
    }

    public void setOtherSrcMnthlyIncmAmt(double otherSrcMnthlyIncmAmt) {
        this.otherSrcMnthlyIncmAmt = otherSrcMnthlyIncmAmt;
    }

    public double getTotFmlyMnthlyIncmAmt() {
        return totFmlyMnthlyIncmAmt;
    }

    public void setTotFmlyMnthlyIncmAmt(double totFmlyMnthlyIncmAmt) {
        this.totFmlyMnthlyIncmAmt = totFmlyMnthlyIncmAmt;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("IncomeDetails [incomeDocumentAvailable=");
        builder.append(incomeDocumentAvailable);
        builder.append(", otherSourceOfIncome=");
        builder.append(otherSourceOfIncome);
        builder.append(", otherSourceIncomeAmount=");
        builder.append(otherSourceIncomeAmount);
        builder.append(", salariedDetails=");
        builder.append(salariedDetails);
        builder.append(", senpProfileIncome=");
        builder.append(senpProfileIncome);
        builder.append(", netMonthlyAmt=");
        builder.append(netMonthlyAmt);
        builder.append(", grossMonthlyAmt=");
        builder.append(grossMonthlyAmt);
        builder.append(", otherSrcMnthlyIncmAmt=");
        builder.append(otherSrcMnthlyIncmAmt);
        builder.append(", totFmlyMnthlyIncmAmt=");
        builder.append(totFmlyMnthlyIncmAmt);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((incomeDocumentAvailable == null) ? 0
                : incomeDocumentAvailable.hashCode());
        long temp;
        temp = Double.doubleToLongBits(otherSourceIncomeAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime
                * result
                + ((otherSourceOfIncome == null) ? 0 : otherSourceOfIncome
                .hashCode());
        result = prime * result
                + ((salariedDetails == null) ? 0 : salariedDetails.hashCode());
        result = prime
                * result
                + ((senpProfileIncome == null) ? 0 : senpProfileIncome
                .hashCode());
        temp = Double.doubleToLongBits(netMonthlyAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(grossMonthlyAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherSrcMnthlyIncmAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(totFmlyMnthlyIncmAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IncomeDetails other = (IncomeDetails) obj;
        if (incomeDocumentAvailable == null) {
            if (other.incomeDocumentAvailable != null)
                return false;
        } else if (!incomeDocumentAvailable
                .equals(other.incomeDocumentAvailable))
            return false;
        if (Double.doubleToLongBits(otherSourceIncomeAmount) != Double
                .doubleToLongBits(other.otherSourceIncomeAmount))
            return false;
        if (otherSourceOfIncome == null) {
            if (other.otherSourceOfIncome != null)
                return false;
        } else if (!otherSourceOfIncome.equals(other.otherSourceOfIncome))
            return false;
        if (salariedDetails == null) {
            if (other.salariedDetails != null)
                return false;
        } else if (!salariedDetails.equals(other.salariedDetails))
            return false;
        if (senpProfileIncome == null) {
            if (other.senpProfileIncome != null)
                return false;
        } else if (!senpProfileIncome.equals(other.senpProfileIncome))
            return false;
        if (Double.doubleToLongBits(netMonthlyAmt) != Double
                .doubleToLongBits(other.netMonthlyAmt))
            return false;
        if (Double.doubleToLongBits(grossMonthlyAmt) != Double
                .doubleToLongBits(other.grossMonthlyAmt))
            return false;
        if (Double.doubleToLongBits(otherSrcMnthlyIncmAmt) != Double
                .doubleToLongBits(other.otherSrcMnthlyIncmAmt))
            return false;
        if (Double.doubleToLongBits(totFmlyMnthlyIncmAmt) != Double
                .doubleToLongBits(other.totFmlyMnthlyIncmAmt))
            return false;
        return true;
    }

}
