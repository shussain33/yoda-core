package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg408 on 1/8/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RulesDetails {

    @JsonProperty("CriteriaID")
    private String CriteriaID;

    @JsonProperty("RuleName")
    private String RuleName;

    @JsonProperty("Outcome")
    private String Outcome;

    @JsonProperty("Remark")
    private String Remark;

    @JsonProperty("Exp")
    private String Exp;

    @JsonProperty("Values")
    private Object Values;

}
