package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Transient;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AadharHolderDetails {
    @JsonProperty("KYC-TYPE")
    private String kycType;

    @JsonProperty("TERMINAL-ID")
    private String terminalId;

    @JsonProperty("AUA-CODE")
    private String auaCode;

    @JsonProperty("SUB-AUA-CODE")
    private String subAuaCode;

    @JsonProperty("VERSION")
    private String version;

    @JsonProperty("PFR")
    private String pdfReport;

    @JsonProperty("TRANSACTION-IDENTIFIER")
    private String transactionIdentifier;

    @JsonProperty("LICENSE-KEY")
    private String licenseKey;

    @JsonProperty("AADHAR-NUMBER")
    private String aadharNumber;

    @JsonProperty("TOKEN")
    private Token token;

    @JsonProperty("METADATA")
    private Metadata metaData;

    @JsonProperty("USES")
    private Uses uses;

    @JsonProperty("PERSONAL-IDENTITY-DATA")
    private PersonalIdentityData personalIdentityData;

    /**
     * Newly added for ready made pid block.
     */
    @JsonProperty("HMAC")
    @Transient
    private String hMac;

    /**
     * Newly added for ready made pid block.
     */
    @JsonProperty("SESSION-KEY")
    @Transient
    private SessionKey sessionKey;

    public static Builder builder() {
        return new Builder();
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAuaCode() {
        return auaCode;
    }

    public void setAuaCode(String auaCode) {
        this.auaCode = auaCode;
    }

    public String getSubAuaCode() {
        return subAuaCode;
    }

    public void setSubAuaCode(String subAuaCode) {
        this.subAuaCode = subAuaCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public Metadata getMetaData() {
        return metaData;
    }

    public void setMetaData(Metadata metaData) {
        this.metaData = metaData;
    }

    public Uses getUses() {
        return uses;
    }

    public void setUses(Uses uses) {
        this.uses = uses;
    }

    public PersonalIdentityData getPersonalIdentityData() {
        return personalIdentityData;
    }

    public void setPersonalIdentityData(PersonalIdentityData personalIdentityData) {
        this.personalIdentityData = personalIdentityData;
    }

    public String getKycType() {
        return kycType;
    }

    public void setKycType(String kycType) {
        this.kycType = kycType;
    }

    public String gethMac() {
        return hMac;
    }

    public void sethMac(String hMac) {
        this.hMac = hMac;
    }

    public SessionKey getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(SessionKey sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getPdfReport() {
        return pdfReport;
    }

    public void setPdfReport(String pdfReport) {
        this.pdfReport = pdfReport;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AadharHolderDetails [kycType=");
        builder.append(kycType);
        builder.append(", terminalId=");
        builder.append(terminalId);
        builder.append(", auaCode=");
        builder.append(auaCode);
        builder.append(", subAuaCode=");
        builder.append(subAuaCode);
        builder.append(", version=");
        builder.append(version);
        builder.append(", pdfReport=");
        builder.append(pdfReport);
        builder.append(", transactionIdentifier=");
        builder.append(transactionIdentifier);
        builder.append(", licenseKey=");
        builder.append(licenseKey);
        builder.append(", aadharNumber=");
        builder.append(aadharNumber);
        builder.append(", token=");
        builder.append(token);
        builder.append(", metaData=");
        builder.append(metaData);
        builder.append(", uses=");
        builder.append(uses);
        builder.append(", personalIdentityData=");
        builder.append(personalIdentityData);
        builder.append(", hMac=");
        builder.append(hMac);
        builder.append(", sessionKey=");
        builder.append(sessionKey);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((aadharNumber == null) ? 0 : aadharNumber.hashCode());
        result = prime * result + ((auaCode == null) ? 0 : auaCode.hashCode());
        result = prime * result + ((hMac == null) ? 0 : hMac.hashCode());
        result = prime * result + ((kycType == null) ? 0 : kycType.hashCode());
        result = prime * result
                + ((licenseKey == null) ? 0 : licenseKey.hashCode());
        result = prime * result
                + ((metaData == null) ? 0 : metaData.hashCode());
        result = prime * result
                + ((pdfReport == null) ? 0 : pdfReport.hashCode());
        result = prime
                * result
                + ((personalIdentityData == null) ? 0 : personalIdentityData
                .hashCode());
        result = prime * result
                + ((sessionKey == null) ? 0 : sessionKey.hashCode());
        result = prime * result
                + ((subAuaCode == null) ? 0 : subAuaCode.hashCode());
        result = prime * result
                + ((terminalId == null) ? 0 : terminalId.hashCode());
        result = prime * result + ((token == null) ? 0 : token.hashCode());
        result = prime
                * result
                + ((transactionIdentifier == null) ? 0 : transactionIdentifier
                .hashCode());
        result = prime * result + ((uses == null) ? 0 : uses.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof AadharHolderDetails))
            return false;
        AadharHolderDetails other = (AadharHolderDetails) obj;
        if (aadharNumber == null) {
            if (other.aadharNumber != null)
                return false;
        } else if (!aadharNumber.equals(other.aadharNumber))
            return false;
        if (auaCode == null) {
            if (other.auaCode != null)
                return false;
        } else if (!auaCode.equals(other.auaCode))
            return false;
        if (hMac == null) {
            if (other.hMac != null)
                return false;
        } else if (!hMac.equals(other.hMac))
            return false;
        if (kycType == null) {
            if (other.kycType != null)
                return false;
        } else if (!kycType.equals(other.kycType))
            return false;
        if (licenseKey == null) {
            if (other.licenseKey != null)
                return false;
        } else if (!licenseKey.equals(other.licenseKey))
            return false;
        if (metaData == null) {
            if (other.metaData != null)
                return false;
        } else if (!metaData.equals(other.metaData))
            return false;
        if (pdfReport == null) {
            if (other.pdfReport != null)
                return false;
        } else if (!pdfReport.equals(other.pdfReport))
            return false;
        if (personalIdentityData == null) {
            if (other.personalIdentityData != null)
                return false;
        } else if (!personalIdentityData.equals(other.personalIdentityData))
            return false;
        if (sessionKey == null) {
            if (other.sessionKey != null)
                return false;
        } else if (!sessionKey.equals(other.sessionKey))
            return false;
        if (subAuaCode == null) {
            if (other.subAuaCode != null)
                return false;
        } else if (!subAuaCode.equals(other.subAuaCode))
            return false;
        if (terminalId == null) {
            if (other.terminalId != null)
                return false;
        } else if (!terminalId.equals(other.terminalId))
            return false;
        if (token == null) {
            if (other.token != null)
                return false;
        } else if (!token.equals(other.token))
            return false;
        if (transactionIdentifier == null) {
            if (other.transactionIdentifier != null)
                return false;
        } else if (!transactionIdentifier.equals(other.transactionIdentifier))
            return false;
        if (uses == null) {
            if (other.uses != null)
                return false;
        } else if (!uses.equals(other.uses))
            return false;
        if (version == null) {
            if (other.version != null)
                return false;
        } else if (!version.equals(other.version))
            return false;
        return true;
    }

    public static class Builder {
        private AadharHolderDetails aadharHolderDetails = new AadharHolderDetails();

        public AadharHolderDetails build() {
            return aadharHolderDetails;
        }

        public Builder kycType(String kycType) {
            this.aadharHolderDetails.kycType = kycType;
            return this;
        }

        public Builder terminalId(String terminalId) {
            this.aadharHolderDetails.terminalId = terminalId;
            return this;
        }

        public Builder auaCode(String auaCode) {
            this.aadharHolderDetails.auaCode = auaCode;
            return this;
        }

        public Builder subAuaCode(String subAuaCode) {
            this.aadharHolderDetails.subAuaCode = subAuaCode;
            return this;
        }

        public Builder version(String version) {
            this.aadharHolderDetails.version = version;
            return this;
        }

        public Builder pdfReport(String pdfReport) {
            this.aadharHolderDetails.pdfReport = pdfReport;
            return this;
        }

        public Builder transactionIdentifier(String transactionIdentifier) {
            this.aadharHolderDetails.transactionIdentifier = transactionIdentifier;
            return this;
        }

        public Builder licenseKey(String licenseKey) {
            this.aadharHolderDetails.licenseKey = licenseKey;
            return this;
        }

        public Builder aadharNumber(String aadharNumber) {
            this.aadharHolderDetails.aadharNumber = aadharNumber;
            return this;
        }

        public Builder token(Token token) {
            this.aadharHolderDetails.token = token;
            return this;
        }

        public Builder metaData(Metadata metaData) {
            this.aadharHolderDetails.metaData = metaData;
            return this;
        }

        public Builder uses(Uses uses) {
            this.aadharHolderDetails.uses = uses;
            return this;
        }

        public Builder personalIdentityData(PersonalIdentityData personalIdentityData) {
            this.aadharHolderDetails.personalIdentityData = personalIdentityData;
            return this;
        }


        public Builder hMac(String hMac) {
            this.aadharHolderDetails.hMac = hMac;
            return this;
        }

        public Builder sessionKey(SessionKey sessionKey) {
            this.aadharHolderDetails.sessionKey = sessionKey;
            return this;
        }
    }
}
