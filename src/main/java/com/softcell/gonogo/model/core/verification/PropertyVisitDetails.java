package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Name;

import java.util.Date;
import java.util.List;

/**
 * Created by amit on 14/3/18.
 */
public class PropertyVisitDetails extends ApplicationVerification {

    @JsonProperty("sCollId")
    private String collateralId;

    @JsonProperty("sPrNo")
    private String prNo;

    @JsonProperty("dtVisitDate")
    private Date dateOfVisit;

    @JsonProperty("sContactedTo")
    private String contactedto;

    @JsonProperty("oAddress")
    private CustomerAddress address;

    @JsonProperty("bNameBoardSeen")
    private boolean nameBoardSeen;

    @JsonProperty("sCollateralStatus")
    private String collateralStatus;

    @JsonProperty("sCollateralUsage")
    private String collateralUsage;

    @JsonProperty("bNeighbourCheck")
    private boolean neighbourCheck;

    @JsonProperty("bPlanApproved")
    private boolean planApproved;

    @JsonProperty("sDistanceFromBranch")
    private String distanceFromBranch;

    @JsonProperty("sVisitedBy")
    private String visitedBy;

    @JsonProperty("sRemarks")
    private String remarks;

    @JsonProperty("bPropertyWithinCityLimits")
    private boolean propertyWithinCityLimits;

    @JsonProperty("sLocality")
    private String locality;

    @JsonProperty("aPropertyOwnerNames")
    private List<Name> propertyOwnerNames;

    @JsonProperty("bWaived")
    private boolean waived;

    public String getCollateralId() {        return collateralId;    }

    public void setCollateralId(String collateralId) {        this.collateralId = collateralId;    }

    public String getPrNo() {
        return prNo;
    }

    public Date getDateOfVisit() {
        return dateOfVisit;
    }

    public String getContactedto() {
        return contactedto;
    }

    public CustomerAddress getAddress() {
        return address;
    }

    public boolean isNameBoardSeen() {
        return nameBoardSeen;
    }

    public String getCollateralStatus() {
        return collateralStatus;
    }

    public String getCollateralUsage() {
        return collateralUsage;
    }

    public boolean isNeighbourCheck() {
        return neighbourCheck;
    }


    public boolean isPlanApproved() {
        return planApproved;
    }

    public void setPlanApproved(boolean planApproved) {
        this.planApproved = planApproved;
    }

    public String getVisitedBy() {
        return visitedBy;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setPrNo(String prNo) {
        this.prNo = prNo;
    }

    public void setDateOfVisit(Date dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public void setContactedto(String contactedto) {
        this.contactedto = contactedto;
    }

    public void setAddress(CustomerAddress address) {
        this.address = address;
    }

    public void setNameBoardSeen(boolean nameBoardSeen) {
        this.nameBoardSeen = nameBoardSeen;
    }

    public void setCollateralStatus(String collateralStatus) {
        this.collateralStatus = collateralStatus;
    }

    public void setCollateralUsage(String collateralUsage) {
        this.collateralUsage = collateralUsage;
    }

    public void setNeighbourCheck(boolean neighbourCheck) {
        this.neighbourCheck = neighbourCheck;
    }

    public String getDistanceFromBranch() {
        return distanceFromBranch;
    }

    public void setDistanceFromBranch(String distanceFromBranch) {
        this.distanceFromBranch = distanceFromBranch;
    }

    public void setVisitedBy(String visitedBy) {
        this.visitedBy = visitedBy;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isPropertyWithinCityLimits() {
        return propertyWithinCityLimits;
    }

    public void setPropertyWithinCityLimits(boolean propertyWithinCityLimits) {
        this.propertyWithinCityLimits = propertyWithinCityLimits;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public List<Name> getPropertyOwnerNames() {
        return propertyOwnerNames;
    }

    public void setPropertyOwnerNames(List<Name> propertyOwnerNames) {
        this.propertyOwnerNames = propertyOwnerNames;
    }

    public boolean isWaived() {
        return waived;
    }

    public void setWaived(boolean waived) {
        this.waived = waived;
    }
}
