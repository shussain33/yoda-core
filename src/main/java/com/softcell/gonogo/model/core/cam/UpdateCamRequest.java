package com.softcell.gonogo.model.core.cam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCamRequest {

    @JsonProperty("oHeader")
    @NotEmpty
    public Header header;

    @JsonProperty("sCamType")
    public String camType;

    @JsonProperty("dtStartDate")
    public Date startDate;

    @JsonProperty("dtEndDate")
    public Date endDate;
}
