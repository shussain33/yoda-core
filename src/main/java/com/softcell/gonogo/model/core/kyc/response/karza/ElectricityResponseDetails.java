package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ElectricityResponseDetails {

    @JsonProperty("bill_issue_date")
    private String bill_issue_date;

    @JsonProperty("bill_due_date")
    private String bill_due_date;

    @JsonProperty("address")
    private String address;

    @JsonProperty("bill_amount")
    private String bill_amount;

    @JsonProperty("consumer_name")
    private String consumer_name;

    @JsonProperty("email_address")
    private String email_address;

    @JsonProperty("bill_date")
    private String bill_date;

    @JsonProperty("total_amount")
    private String total_amount;

    @JsonProperty("bill_no")
    private String bill_no;

    @JsonProperty("consumer_number")
    private String consumer_number;

    @JsonProperty("mobile_number")
    private String mobile_number;

    @JsonProperty("amount_payable")
    private String amount_payable;

}
