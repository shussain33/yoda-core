package com.softcell.gonogo.model.core;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Softcell on 21/09/17.
 */
public class TCLosData {

    @JsonProperty("sInterfaceId")
    private String interfaceId;

    @JsonProperty("iTokenCode")
    private String tokenCode;

    @JsonProperty("sDocumentType")
    private String documentType;

    @JsonProperty("sDocumentDate")
    private String documentDate;

    @JsonProperty("sPostingDate")
    private String postingDate;

    @JsonProperty("sTransactionDate")
    private String transactionDate;

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("sReferenceNo")
    private String referenceNo;

    @JsonProperty("eDocumentHead")
    private String documentHead;

    @JsonProperty("sBusTran")
    private String busTran;

    @JsonProperty("sGlItemNo")
    private String glItemNo;

    @JsonProperty("sTaxAutoFlag")
    private String taxAutoFlag;

    @JsonProperty("sPostingKey")
    private String postingKey;

    @JsonProperty("sAccType")
    private String accType;

    @JsonProperty("sDebitCreditIndicator")
    private String debitCreditIndicator;

    @JsonProperty("sBusinessArea")
    private String businessArea;

    @JsonProperty("sTaxCode")
    private String taxCode;

    @JsonProperty("dAmount")
    private double amount;

    @JsonProperty("sCurrencyKey")
    private String currencyKey;

    @JsonProperty("sValueDate")
    private String valueDate;

    @JsonProperty("sAssignmentNo")
    private String assignmentNo;

    @JsonProperty("sItemText")
    private String itemText;

    @JsonProperty("sCostCenter")
    private String costCenter;

    @JsonProperty("sGlAccountNo")
    private String glAccountNo;

    @JsonProperty("sGlDesc")
    private String glDesc;

    @JsonProperty("sCustomerNo")
    private String customerNo;

    @JsonProperty("sVendorNo")
    private String vendorNo;

    @JsonProperty("sVendorId")
    private String vendorId;

    @JsonProperty("sSpecialGl")
    private String specialGl;

    @JsonProperty("sAssSplGl")
    private String assSplGl;

    @JsonProperty("sBaseLineDate")
    private String baseLineDate;

    @JsonProperty("sPandlFlag")
    private String pandlFlag;

    @JsonProperty("sPanDlNo")
    private String panDlNo;

    @JsonProperty("sCostCode")
    private String costCode;

    @JsonProperty("sProfitCenter")
    private String profitCenter;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("sBranchCode")
    private String branchCode;

    @JsonProperty("sTransferFlag")
    private String transferFlag;

    @JsonProperty("sInterfaceRemarks")
    private String interfaceRemarks;

    @JsonProperty("sDtupDate")
    private String dtupDate;

    @JsonProperty("sIncrTranNo")
    private String incrTranNo;

    @JsonProperty("sDocumentNo")
    private String documentNo;

    @JsonProperty("sDocumentYear")
    private String documentYear;

    @JsonProperty("sFavour")
    private String favour;

    @JsonProperty("sChqCity")
    private String chqCity;

    @JsonProperty("sAccNo")
    private String accNo;

    @JsonProperty("sIfscCode")
    private String ifscCode;

    @JsonProperty("sChequeNo")
    private String chequeNo;

    @JsonProperty("sBrsRef1")
    private String brsRef1;

    @JsonProperty("sBrsRef2")
    private String brsRef2;

    @JsonProperty("sBrsRef3")
    private String brsRef3;

    @JsonProperty("sCustomerType")
    private String customerType;

    @JsonProperty("sPayoutTaxFlag")
    private String payoutTaxFlag;

    @JsonProperty("sPaymentMode")
    private String paymentMode;

    @JsonProperty("sSacCode")
    private String sacCode;

    @JsonProperty("sBusinessPlace")
    private String businessPlace;

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public String getTokenCode() {
        return tokenCode;
    }

    public void setTokenCode(String tokenCode) {
        this.tokenCode = tokenCode;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDocumentHead() {
        return documentHead;
    }

    public void setDocumentHead(String documentHead) {
        this.documentHead = documentHead;
    }

    public String getBusTran() {
        return busTran;
    }

    public void setBusTran(String busTran) {
        this.busTran = busTran;
    }

    public String getGlItemNo() {
        return glItemNo;
    }

    public void setGlItemNo(String glItemNo) {
        this.glItemNo = glItemNo;
    }

    public String getTaxAutoFlag() {
        return taxAutoFlag;
    }

    public void setTaxAutoFlag(String taxAutoFlag) {
        this.taxAutoFlag = taxAutoFlag;
    }

    public String getPostingKey() {
        return postingKey;
    }

    public void setPostingKey(String postingKey) {
        this.postingKey = postingKey;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getDebitCreditIndicator() {
        return debitCreditIndicator;
    }

    public void setDebitCreditIndicator(String debitCreditIndicator) {
        this.debitCreditIndicator = debitCreditIndicator;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrencyKey() {
        return currencyKey;
    }

    public void setCurrencyKey(String currencyKey) {
        this.currencyKey = currencyKey;
    }

    public String getValueDate() {
        return valueDate;
    }

    public void setValueDate(String valueDate) {
        this.valueDate = valueDate;
    }

    public String getAssignmentNo() {
        return assignmentNo;
    }

    public void setAssignmentNo(String assignmentNo) {
        this.assignmentNo = assignmentNo;
    }

    public String getItemText() {
        return itemText;
    }

    public void setItemText(String itemText) {
        this.itemText = itemText;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getGlAccountNo() {
        return glAccountNo;
    }

    public void setGlAccountNo(String glAccountNo) {
        this.glAccountNo = glAccountNo;
    }

    public String getGlDesc() {
        return glDesc;
    }

    public void setGlDesc(String glDesc) {
        this.glDesc = glDesc;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getVendorNo() {
        return vendorNo;
    }

    public void setVendorNo(String vendorNo) {
        this.vendorNo = vendorNo;
    }

    public String getVendorId() { return vendorId; }

    public void setVendorId(String vendorId) { this.vendorId = vendorId; }

    public String getSpecialGl() {
        return specialGl;
    }

    public void setSpecialGl(String specialGl) {
        this.specialGl = specialGl;
    }

    public String getAssSplGl() {
        return assSplGl;
    }

    public void setAssSplGl(String assSplGl) {
        this.assSplGl = assSplGl;
    }

    public String getBaseLineDate() {
        return baseLineDate;
    }

    public void setBaseLineDate(String baseLineDate) {
        this.baseLineDate = baseLineDate;
    }

    public String getPandlFlag() {
        return pandlFlag;
    }

    public void setPandlFlag(String pandlFlag) {
        this.pandlFlag = pandlFlag;
    }

    public String getPanDlNo() {
        return panDlNo;
    }

    public void setPanDlNo(String panDlNo) {
        this.panDlNo = panDlNo;
    }

    public String getCostCode() {
        return costCode;
    }

    public void setCostCode(String costCode) {
        this.costCode = costCode;
    }

    public String getProfitCenter() {
        return profitCenter;
    }

    public void setProfitCenter(String profitCenter) {
        this.profitCenter = profitCenter;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getTransferFlag() {
        return transferFlag;
    }

    public void setTransferFlag(String transferFlag) {
        this.transferFlag = transferFlag;
    }

    public String getInterfaceRemarks() {
        return interfaceRemarks;
    }

    public void setInterfaceRemarks(String interfaceRemarks) {
        this.interfaceRemarks = interfaceRemarks;
    }

    public String getDtupDate() { return dtupDate; }

    public void setDtupDate(String dtupDate) { this.dtupDate = dtupDate; }

    public String getIncrTranNo() {
        return incrTranNo;
    }

    public void setIncrTranNo(String incrTranNo) {
        this.incrTranNo = incrTranNo;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public String getDocumentYear() {
        return documentYear;
    }

    public void setDocumentYear(String documentYear) {
        this.documentYear = documentYear;
    }

    public String getFavour() {
        return favour;
    }

    public void setFavour(String favour) {
        this.favour = favour;
    }

    public String getChqCity() {
        return chqCity;
    }

    public void setChqCity(String chqCity) {
        this.chqCity = chqCity;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getBrsRef1() {
        return brsRef1;
    }

    public void setBrsRef1(String brsRef1) {
        this.brsRef1 = brsRef1;
    }

    public String getBrsRef2() {
        return brsRef2;
    }

    public void setBrsRef2(String brsRef2) {
        this.brsRef2 = brsRef2;
    }

    public String getBrsRef3() {
        return brsRef3;
    }

    public void setBrsRef3(String brsRef3) {
        this.brsRef3 = brsRef3;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getPayoutTaxFlag() {
        return payoutTaxFlag;
    }

    public void setPayoutTaxFlag(String payoutTaxFlag) {
        this.payoutTaxFlag = payoutTaxFlag;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getSacCode() {
        return sacCode;
    }

    public void setSacCode(String sacCode) {
        this.sacCode = sacCode;
    }

    public String getBusinessPlace() {
        return businessPlace;
    }

    public void setBusinessPlace(String businessPlace) {
        this.businessPlace = businessPlace;
    }
}
