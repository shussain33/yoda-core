/**
 * kishorp10:25:14 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.contact.Phone;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Past;
import java.util.Date;
import java.util.List;

/**
 * @author kishorp
 *
 */
public class DedupeDetails {

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("dtProcess")
    private Date processDate;

    @JsonProperty("oApplName")
    private Name applicantName;

    @JsonProperty("sDob")
    private String dateOfBirth;

    @JsonProperty("aKycDocs")
    private List<Kyc> kyc;

    @JsonProperty("aPhone")
    private List<Phone> phone;

    @JsonProperty("sSource")
    public String sourceSystem;

    @JsonProperty("sdedupeID")
    private String dedupeID;

    @JsonProperty("sApplID")
    private String applicantId;

    public String getDedupeID() {
        return dedupeID;
    }

    public void setDedupeID(String dedupeID) {
        this.dedupeID = dedupeID;
    }

    public String getApplicantId() {
        return applicantId;
    }
    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public DedupeDetails(){

    }

    public DedupeDetails(String refId) {
        this.refId = refId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public Name getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(Name applicantName) {
        this.applicantName = applicantName;
    }

    public List<Kyc> getKyc() {
        return kyc;
    }

    public void setKyc(List<Kyc> kyc) {
        this.kyc = kyc;
    }

    public List<Phone> getPhone() {
        return phone;
    }

    public void setPhone(List<Phone> phone) {
        this.phone = phone;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DedupeDetails that = (DedupeDetails) o;

        if (refId != null ? !refId.equals(that.refId) : that.refId != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        return processDate != null ? processDate.equals(that.processDate) : that.processDate == null;
    }

    @Override
    public int hashCode() {
        int result = refId != null ? refId.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (processDate != null ? processDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DedupeDetails{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", processDate=").append(processDate);
        sb.append('}');
        return sb.toString();
    }
}
