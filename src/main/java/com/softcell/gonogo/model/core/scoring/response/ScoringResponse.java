package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author prateek
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScoringResponse {

    @JsonProperty("HEADER")
    private Header header;

    @JsonProperty("SCORING-REF-ID")
    private String ackId;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("SCORE-DATA")
    private ScoreData scoreData;

    @JsonProperty("CUSTOM_ATTRIBUTES")
    private CustomAttributes custAttribute;

    @JsonProperty("SCORE_TREE")
    private Object scoreTree;

    @JsonProperty("ELIGIBILITY_RESPONSE")
    private Eligibility eligibilityResponse;

    @JsonProperty("DECISION_RESPONSE")
    private DecisionResponse decisionResponse;

    @JsonProperty("DERIVED_FIELDS")
    private Object derivedFields;

    @JsonProperty("SCORE_AND_DECISION_DOMAINS")
    private List<ScoreAndDecisionDomain> scoreAndDecisionDomains;

    @JsonProperty("APPLICANT_ID")
    private String applicantId;

	@JsonProperty("ADDITIONAL_FIELDS")
	private Map<String, Object> additionalFields;

	@JsonProperty("oApplicantResponse")
    private ScoringApplicantResponse applicantResponse;

    @JsonProperty("oCoApplicantResponse")
    private ScoringApplicantResponse coApplicantResponse;

    @JsonProperty("aMultiBreApplicantResponse")
    private List<ScoringApplicantResponse> breApplicantResponses;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * @param additionalProperties the additionalProperties to set
     */
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getAckId() {
        return ackId;
    }

    public void setAckId(String ackId) {
        this.ackId = ackId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ScoreData getScoreData() {
        return scoreData;
    }

    public void setScoreData(ScoreData scoreData) {
        this.scoreData = scoreData;
    }

    public CustomAttributes getCustAttribute() {
        return custAttribute;
    }

    public void setCustAttribute(CustomAttributes custAttribute) {
        this.custAttribute = custAttribute;
    }

    public Object getScoreTree() {
        return scoreTree;
    }

    public void setScoreTree(Object scoreTree) {
        this.scoreTree = scoreTree;
    }

    public Eligibility getEligibilityResponse() {
        return eligibilityResponse;
    }

    public void setEligibilityResponse(Eligibility eligibilityResponse) {
        this.eligibilityResponse = eligibilityResponse;
    }

    public DecisionResponse getDecisionResponse() {
        return decisionResponse;
    }

    public void setDecisionResponse(DecisionResponse decisionResponse) {
        this.decisionResponse = decisionResponse;
    }

    public Object getDerivedFields() {
        return derivedFields;
    }

    public void setDerivedFields(Object derivedFields) {
        this.derivedFields = derivedFields;
    }

    /**
     * @return the scoreAndDecisionDomains
     */
    public List<ScoreAndDecisionDomain> getScoreAndDecisionDomains() {
        return scoreAndDecisionDomains;
    }

    /**
     * @param scoreAndDecisionDomains the scoreAndDecisionDomains to set
     */
    public void setScoreAndDecisionDomains(
            List<ScoreAndDecisionDomain> scoreAndDecisionDomains) {
        this.scoreAndDecisionDomains = scoreAndDecisionDomains;
    }

    /**
     * @return the applicantId
     */
    public String getApplicantId() {
        return applicantId;
    }

    /**
     * @param applicantId the applicantId to set
     */
    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }


    /**
     * @return the additionalFields
     */
    public Map<String, Object> getAdditionalFields() {
        return additionalFields;
    }

    /**
     * @param additionalFields the additionalFields to set
     */
    public void setAdditionalFields(Map<String, Object> additionalFields) {
        this.additionalFields = additionalFields;
    }

    public ScoringApplicantResponse getApplicantResponse() {
        return applicantResponse;
    }

    public void setApplicantResponse(ScoringApplicantResponse applicantResponse) {
        this.applicantResponse = applicantResponse;
    }

    public ScoringApplicantResponse getCoApplicantResponse() {
        return coApplicantResponse;
    }

    public void setCoApplicantResponse(ScoringApplicantResponse coApplicantResponse) {
        this.coApplicantResponse = coApplicantResponse;
    }

    public List<ScoringApplicantResponse> getBreApplicantResponses() {
        return breApplicantResponses;
    }

    public void setBreApplicantResponses(List<ScoringApplicantResponse> breApplicantResponses) {
        this.breApplicantResponses = breApplicantResponses;
    }

    /* (non-Javadoc)
                 * @see java.lang.Object#toString()
                 */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ScoringResponse [header=");
        builder.append(header);
        builder.append(", ackId=");
        builder.append(ackId);
        builder.append(", status=");
        builder.append(status);
        builder.append(", scoreData=");
        builder.append(scoreData);
        builder.append(", custAttribute=");
        builder.append(custAttribute);
        builder.append(", scoreTree=");
        builder.append(scoreTree);
        builder.append(", eligibilityResponse=");
        builder.append(eligibilityResponse);
        builder.append(", decisionResponse=");
        builder.append(decisionResponse);
        builder.append(", derivedFields=");
        builder.append(derivedFields);
        builder.append(", scoreAndDecisionDomains=");
        builder.append(scoreAndDecisionDomains);
        builder.append(", applicantId=");
        builder.append(applicantId);
        builder.append(", additionalFields=");
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ackId == null) ? 0 : ackId.hashCode());
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result
                + ((applicantId == null) ? 0 : applicantId.hashCode());
        result = prime * result
                + ((custAttribute == null) ? 0 : custAttribute.hashCode());
        result = prime
                * result
                + ((decisionResponse == null) ? 0 : decisionResponse.hashCode());
        result = prime * result
                + ((derivedFields == null) ? 0 : derivedFields.hashCode());
        result = prime
                * result
                + ((eligibilityResponse == null) ? 0 : eligibilityResponse
                .hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime
                * result
                + ((scoreAndDecisionDomains == null) ? 0
                : scoreAndDecisionDomains.hashCode());
        result = prime * result
                + ((scoreData == null) ? 0 : scoreData.hashCode());
        result = prime * result
                + ((scoreTree == null) ? 0 : scoreTree.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((additionalFields == null) ? 0 : additionalFields.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ScoringResponse other = (ScoringResponse) obj;
        if (ackId == null) {
            if (other.ackId != null)
                return false;
        } else if (!ackId.equals(other.ackId))
            return false;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (applicantId == null) {
            if (other.applicantId != null)
                return false;
        } else if (!applicantId.equals(other.applicantId))
            return false;
        if (custAttribute == null) {
            if (other.custAttribute != null)
                return false;
        } else if (!custAttribute.equals(other.custAttribute))
            return false;
        if (decisionResponse == null) {
            if (other.decisionResponse != null)
                return false;
        } else if (!decisionResponse.equals(other.decisionResponse))
            return false;
        if (derivedFields == null) {
            if (other.derivedFields != null)
                return false;
        } else if (!derivedFields.equals(other.derivedFields))
            return false;
        if (eligibilityResponse == null) {
            if (other.eligibilityResponse != null)
                return false;
        } else if (!eligibilityResponse.equals(other.eligibilityResponse))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (scoreAndDecisionDomains == null) {
            if (other.scoreAndDecisionDomains != null)
                return false;
        } else if (!scoreAndDecisionDomains
                .equals(other.scoreAndDecisionDomains))
            return false;
        if (scoreData == null) {
            if (other.scoreData != null)
                return false;
        } else if (!scoreData.equals(other.scoreData))
            return false;
        if (scoreTree == null) {
            if (other.scoreTree != null)
                return false;
        } else if (!scoreTree.equals(other.scoreTree))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (additionalFields == null) {
            if (other.additionalFields != null)
                return false;
        } else if (!additionalFields.equals(other.additionalFields))
            return false;
        return true;
    }
}
