package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DrivingLicenceResponseDetails {

    @JsonProperty("cov_details")
    private ArrayList<CovDetails> cov_details;

    @JsonProperty("img")
    private String img;

    @JsonProperty("address")
    private String address;

    @JsonProperty("issue_date")
    private String issue_date;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("name")
    private String name;

    @JsonProperty("blood_group")
    private String blood_group;

    @JsonProperty("validity")
    private ValidityDetails validity;

    @JsonProperty("father/husband")
    private String fatherhusband;

}
