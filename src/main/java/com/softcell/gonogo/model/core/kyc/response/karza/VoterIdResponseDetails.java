package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class VoterIdResponseDetails {

    @JsonProperty("pc_name")
    private String pc_name;

    @JsonProperty("st_code")
    private String st_code;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("rln_name_v2")
    private String rln_name_v2;

    @JsonProperty("rln_name_v1")
    private String rln_name_v1;

    @JsonProperty("rln_name_v3")
    private String rln_name_v3;

    @JsonProperty("name_v1")
    private String name_v1;

    @JsonProperty("epic_no")
    private String epic_no;

    @JsonProperty("ac_name")
    private String ac_name;

    @JsonProperty("name_v2")
    private String name_v2;

    @JsonProperty("name_v3")
    private String name_v3;

    @JsonProperty("ps_lat_long")
    private String ps_lat_long;

    @JsonProperty("last_update")
    private String last_update;

    @JsonProperty("id")
    private String id;

    @JsonProperty("state")
    private String state;

    @JsonProperty("ps_name")
    private String ps_name;

    @JsonProperty("rln_type")
    private String rln_type;

    @JsonProperty("part_no")
    private String part_no;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("district")
    private String district;

    @JsonProperty("house_no")
    private String house_no;

    @JsonProperty("name")
    private String name;

    @JsonProperty("section_no")
    private String section_no;

    @JsonProperty("ac_no")
    private String ac_no;

    @JsonProperty("rln_name")
    private String rln_name;

    @JsonProperty("slno_inpart")
    private String slno_inpart;

    @JsonProperty("part_name")
    private String part_name;

    @JsonProperty("age")
    private String age;

}
