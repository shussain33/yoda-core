package com.softcell.gonogo.model.core.request.scoring;

public class CibilRespEnqActNo {

    private String accountNumber;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        return "CibilRespEnqActNo [accountNumber=" + accountNumber + "]";
    }


}
