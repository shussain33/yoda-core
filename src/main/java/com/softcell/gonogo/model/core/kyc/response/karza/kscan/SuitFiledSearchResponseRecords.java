package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SuitFiledSearchResponseRecords {

    @JsonProperty("registeredAddress")
    private String registeredAddress;

    @JsonProperty("borrowerName")
    private String borrowerName;

    @JsonProperty("type")
    private SuitFiledSearchResponseTypes suitFiledSearchResponseTypes;

    @JsonProperty("branchId")
    private int branchId;

    @JsonProperty("cin")
    private String cin;

    @JsonProperty("director")
    private List<SuitFiledSearchResponseDirector> suitFiledSearchResponseDirectorList;

    @JsonProperty("state")
    private String state;

    @JsonProperty("borrowerId")
    private String borrowerId;

    @JsonProperty("branchName")
    private String branchName;

    @JsonProperty("bankName")
    private String bankName;

    @JsonProperty("defaultSummary")
    private SuitFiledSearchResponseDefaultSummary suitFiledSearchResponseDefaultSummary;

}