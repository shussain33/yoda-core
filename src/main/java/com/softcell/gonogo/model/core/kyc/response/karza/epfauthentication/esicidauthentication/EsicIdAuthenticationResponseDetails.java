package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.esicidauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EsicIdAuthenticationResponseDetails {

    @JsonProperty("UHID")
    private String UHID;

    @JsonProperty("DOB")
    private String DOB;

    @JsonProperty("AdhaarStatus")
    private String AdhaarStatus;

    @JsonProperty("Relation_with_IP")
    private String Relation_with_IP;

    @JsonProperty("CurrentDateOfAppointment")
    private String CurrentDateOfAppointment;

    @JsonProperty("Nominee_Address")
    private String Nominee_Address;

    @JsonProperty("PresentAddress")
    private String PresentAddress;

    @JsonProperty("DisabilityType")
    private String DisabilityType;

    @JsonProperty("AdhaarNO")
    private String AdhaarNO;

    @JsonProperty("PhoneNO")
    private String PhoneNO;

    @JsonProperty("MaritialStatus")
    private String MaritialStatus;

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("Gender")
    private String Gender;

    @JsonProperty("Contribution_Details")
    public List<ContributionDetails> contributionDetailsList;

    @JsonProperty("Nominee_AdhaarNO")
    private String Nominee_AdhaarNO;

    @JsonProperty("Father_or_Husband")
    private String Father_or_Husband;

    @JsonProperty("RegistrationDate")
    private String RegistrationDate;

    @JsonProperty("PermanentAddress")
    private String PermanentAddress;

    @JsonProperty("Nominee_Name")
    private String Nominee_Name;

    @JsonProperty("InsuranceNO")
    private String InsuranceNO;

    @JsonProperty("FirstDateOfAppointment")
    private String FirstDateOfAppointment;

    @JsonProperty("DispensaryName")
    private String DispensaryName;
}