package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.passbookauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PassbookDetails {

    @JsonProperty("cr_pen_bal")
    private String cr_pen_bal;

    @JsonProperty("approved_on")
    private String approved_on;

    @JsonProperty("db_cr_flag")
    private String db_cr_flag;

    @JsonProperty("tr_approved")
    private String tr_approved;

    @JsonProperty("tr_date_my")
    private String tr_date_my;

    @JsonProperty("r_order")
    private String r_order;

    @JsonProperty("cr_ee_share")
    private String cr_ee_share;

    @JsonProperty("cr_er_share")
    private String cr_er_share;

    @JsonProperty("particular")
    private String particular;

    @JsonProperty("trrno")
    private String trrno;

    @JsonProperty("table_name")
    private String table_name;

    @JsonProperty("month_year")
    private String month_year;

}