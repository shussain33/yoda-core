package com.softcell.gonogo.model.core.kyc.request.aadhar;

public class SKey {
	
	private String ci;
	private String ki;
	private String skeyValue;

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}
	
	

	public String getSkeyValue() {
		return skeyValue;
	}

	public void setSkeyValue(String skeyValue) {
		this.skeyValue = skeyValue;
	}

	
	public String getKi() {
		return ki;
	}

	public void setKi(String ki) {
		this.ki = ki;
	}

	@Override
	public String toString() {
		return "SKey [ci=" + ci + ", ki=" + ki + ", skeyValue=" + skeyValue + "]";
	}

	
	

}
