package com.softcell.gonogo.model.core.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.FieldConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.annotation.CreatedDate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by anupamad on 21/7/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TvrDetails {
    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sTvrRemarks")
    private String remarks;

    @JsonProperty("sDesignation")
    private String designation;

    @JsonProperty("sAssetDelStatus")
    private String sAssetDelStatus;

    @JsonProperty("sEndUsage")
    private String endUsage;

    @JsonProperty("aVerifiedFlags")
    private List<String> verifiedFlags;

    @JsonProperty("dtStatusUpdateDate")
    @CreatedDate
    private Date statusUpdateDate = new Date();

    @JsonProperty("dtTvrInitiatedDate")
    @CreatedDate
    private Date tvrInitiatedDate = new Date();

    public void setVerifiedFlags(List<String> verifiedFlags) {
        if (!CollectionUtils.isEmpty(verifiedFlags)) {
            this.verifiedFlags = verifiedFlags.stream().map(vF -> FieldConstants.getEnumNameForValue(vF))
                    .collect(Collectors.toList());
        }
    }

    public List<String> getVerifiedFlags() {
        List<String> flags = new ArrayList<String>();

        if (!CollectionUtils.isEmpty(this.verifiedFlags)) {
            flags = verifiedFlags.stream().map(vf -> Enum.valueOf(FieldConstants.class,vf).toFaceValue())
                    .collect(Collectors.toList());
        }
        return flags;
    }
}
