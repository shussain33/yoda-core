package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Constant;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.DisbursementDetails;
import com.softcell.gonogo.model.core.user.TvrDetails;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.InvoiceDetails;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.InterimStatus;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.workflow.ComponentStatus;
import com.softcell.workflow.ApplicationLog;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

/**
 * @author kishorp
 *         This Class is used to process GoNoGo Application. After Saving into database.
 */
@Document(collection = "goNoGoCustomerApplication")
/*@CompoundIndexes(
        {
                @CompoundIndex(name = "inst_prod_dt_status",
                        def = "{'applicationRequest.header.institutionId':1, 'applicationRequest.header.dateTime':-1 , 'applicationRequest.request.application.loanType':1, 'applicationStatus':1, 'applicationRequest.header.dsaId':1}")
        }
)*/
public class GoNoGoCustomerApplication extends AuditEntity {

    @JsonProperty("oIntrmStat")
    private InterimStatus intrimStatus = new InterimStatus();

    /* Map storing completed steps and info related to completion*/
    @JsonProperty("oCompletedExecution")
    private Map<String, CompletionInfo> completed = new LinkedHashMap();

    Map<String, CustomData> applicantCoApplicantData;

    @Id
    @JsonProperty("sRefID")
    private String gngRefId;

    @JsonProperty("sParentID")
    private String parentID;

    @JsonProperty("sRootID")
    private String rootID;

    private int productSequenceNumber;

    @JsonProperty("dDate")
    private Date dateTime = new Date();

    @JsonProperty("sAppStat")
    private String applicationStatus;

    @JsonProperty("bStatFlag")
    private boolean statusFlag;

    @JsonProperty("iReInitCount")
    private int reInitiateCount;

    @JsonProperty("bReAppraiseReq")
    private boolean reappraiseReq;

    @JsonIgnore
    private int reProcessCount;

    @JsonProperty("sReInitRmk")
    private String reInitiateRemark;

    @JsonProperty("sAgreementNum")
    private String agreementNum;

    @JsonProperty("bEditable")
    private boolean editable = true;

    private String assignedQueueGroupId;

    // Track access status of application by user
    @JsonProperty("oAllocationStatus")
    private AllocationInfo allocationInfo;

    @JsonProperty("oAppReq")
    private ApplicationRequest applicationRequest;

    @JsonProperty("oCompRes")
    private volatile ComponentResponse applicantComponentResponse = new ComponentResponse();

    @JsonProperty("aCompRes")
    private List<ComponentResponse> applicantComponentResponseList;
    /**
     * Credit Risk officer decisions list
     */
    @JsonProperty("aCroDec")
    private List<CroDecision> croDecisions;

    @JsonProperty("oTvrDetails")
    private TvrDetails tvrDetails;

    @JsonProperty("bNegPinCodeFlag")
    private boolean negativePincode;

    @JsonProperty("oWorkFlowDetail")
    private WorkFlowDetails workFlowDetails;
    /**
     * This property is used by  multiple thread same time. so it has been initialized.
     */
    @JsonProperty("aAppScoRslt")
    private Vector<ModuleOutcome> applScoreVector = new Vector<ModuleOutcome>();
    /**
     * Just use to store log and status of application for audit purpose
     */
    @Transient
    private ApplicationLog applicationLog = new ApplicationLog();

    private ComponentStatus componentStatus;

    @JsonProperty("aDeDupe")
    private Set<String> dedupedApplications;

    @JsonProperty("aCroJustification")
    private List<CroJustification> croJustification;

    @JsonProperty("oDecisionData")
    private DecisionData decisionData;

    @JsonProperty("oLosDtls")
    private LOSDetails losDetails;

    @JsonProperty("oInvDtls")
    private InvoiceDetails invoiceDetails;

    @JsonProperty("oDisbDtls")
    private DisbursementDetails disbursementDetails;

    //DigiPl Fields Started
    @JsonProperty("sdedupeID")
    private String dedupeID;

    @JsonProperty("sApplicationBucket")
    private String applicationBucket = Constant.DEFAULT;

    /**
     * this Field contain last updated date for that case
     * @return
     */
    @JsonProperty("dtLastUpdatedDate")
    private Date lastUpdatedDate;

    @JsonProperty("dtActualDisbDate")
    private Date actualDisbDate;

    public Date getActualDisbDate() {
        return actualDisbDate;
    }

    public void setActualDisbDate(Date actualDisbDate) {
        this.actualDisbDate = actualDisbDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getApplicationBucket() { return applicationBucket; }

    public void setApplicationBucket(String applicationBucket) { this.applicationBucket = applicationBucket; }

    public String getDedupeID() { return dedupeID; }

    public void setDedupeID(String dedupeID) {
        this.dedupeID = dedupeID;
    }

    /**
     * @return the noOfReTry
     */
    public int getReInitiateCount() {
        return reInitiateCount;
    }

    /**
     * @param reInitiateCount the noOfReTry to set
     */
    public void setReInitiateCount(int reInitiateCount) {
        this.reInitiateCount = reInitiateCount;
    }

    /**
     * @return the reInitiateRemark
     */
    public String getReInitiateRemark() {
        return reInitiateRemark;
    }

    /**
     * @param reInitiateRemark the reInitiateRemark to set
     */
    public void setReInitiateRemark(String reInitiateRemark) {
        this.reInitiateRemark = reInitiateRemark;
    }

    public String getAgreementNum() {
        return agreementNum;
    }

    public void setAgreementNum(String agreementNum) {
        this.agreementNum = agreementNum;
    }

    public boolean isEditable() {        return editable;    }

    public void setEditable(boolean editable) {        this.editable = editable;    }

    public String getAssignedQueueGroupId() {        return assignedQueueGroupId;    }

    public void setAssignedQueueGroupId(String assignedQueueGroupId) {        this.assignedQueueGroupId = assignedQueueGroupId;    }

    public AllocationInfo getAllocationInfo() {        return allocationInfo;    }

    public void setAllocationInfo(AllocationInfo allocationInfo) {        this.allocationInfo = allocationInfo;    }

    public List<CroJustification> getCroJustification() {
        return croJustification;
    }

    public void setCroJustification(List<CroJustification> croJustification) {
        this.croJustification = croJustification;
    }

    public DecisionData getDecisionData() {        return decisionData;    }

    public void setDecisionData(DecisionData decisionData) {        this.decisionData = decisionData;    }

    public ApplicationLog getApplicationLog() {
        return applicationLog;
    }

    public void setApplicationLog(ApplicationLog applicationLog) {
        this.applicationLog = applicationLog;
    }

    public Set<String> getDedupedApplications() {
        return dedupedApplications;
    }

    public void setDedupedApplications(Set<String> dedupedApplications) {
        this.dedupedApplications = dedupedApplications;
    }

    public ComponentStatus getComponentStatus() {
        return componentStatus;
    }

    public void setComponentStatus(ComponentStatus componentStatus) {
        this.componentStatus = componentStatus;
    }

    public Vector<ModuleOutcome> getApplScoreVector() {
        return applScoreVector;
    }

    public void setApplScoreVector(Vector<ModuleOutcome> applScoreVector) {
        this.applScoreVector = applScoreVector;
    }

    public List<CroDecision> getCroDecisions() {
        return croDecisions;
    }

    public void setCroDecisions(List<CroDecision> croDecisions) {
        this.croDecisions = croDecisions;
    }

    public TvrDetails getTvrDetails() {
        return tvrDetails;
    }

    public void setTvrDetails(TvrDetails tvrDetails) {
        this.tvrDetails = tvrDetails;
    }

    public InterimStatus getIntrimStatus() {
        return intrimStatus;
    }

    public void setIntrimStatus(InterimStatus intrimStatus) {
        this.intrimStatus = intrimStatus;
    }

    /**
     * @return the reProcessCount
     */
    public int getReProcessCount() {
        return reProcessCount;
    }

    /**
     * @param reProcessCount the reProcessCount to set
     */
    public void setReProcessCount(int reProcessCount) {
        this.reProcessCount = reProcessCount;
    }

    public ApplicationRequest getApplicationRequest() {
        return applicationRequest;
    }

    public void setApplicationRequest(ApplicationRequest applicationRequest) {
        this.applicationRequest = applicationRequest;
    }

    public ComponentResponse getApplicantComponentResponse() {
        return applicantComponentResponse;
    }

    public void setApplicantComponentResponse(
            ComponentResponse applicantComponentResponse) {
        this.applicantComponentResponse = applicantComponentResponse;
    }

    public List<ComponentResponse> getApplicantComponentResponseList() {
        return applicantComponentResponseList;
    }

    public void setApplicantComponentResponseList(
            List<ComponentResponse> applicantComponentResponseList) {
        this.applicantComponentResponseList = applicantComponentResponseList;
    }


    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getGngRefId() {
        return gngRefId;
    }

    public void setGngRefId(String gngRefId) {
        this.gngRefId = gngRefId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(boolean statusFlag) {
        this.statusFlag = statusFlag;
    }

    public boolean isNegativePincode() {
        return negativePincode;
    }

    public void setNegativePincode(boolean negativePincode) {
        this.negativePincode = negativePincode;
    }

    public LOSDetails getLosDetails() {
        return losDetails;
    }

    public void setLosDetails(LOSDetails losDetails) {
        this.losDetails = losDetails;
    }


    /**
     * @return the workFlowDetails
     */
    public WorkFlowDetails getWorkFlowDetails() {
        return workFlowDetails;
    }

    /**
     * @param workFlowDetails the workFlowDetails to set
     */
    public void setWorkFlowDetails(WorkFlowDetails workFlowDetails) {
        this.workFlowDetails = workFlowDetails;
    }

    public InvoiceDetails getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(InvoiceDetails invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    /**
     * @return the isReappraiseReq
     */
    public boolean isReappraiseReq() {
        return reappraiseReq;
    }

    /**
     * @param isReappraiseReq the isReappraiseReq to set
     */
    public void setReappraiseReq(boolean isReappraiseReq) {
        this.reappraiseReq = isReappraiseReq;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
	 */
    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getRootID() {
        return rootID;
    }

    public void setRootID(String rootID) {
        this.rootID = rootID;
    }

    public int getProductSequenceNumber() {
        return productSequenceNumber;
    }

    public void setProductSequenceNumber(int productSequenceNumber) {
        this.productSequenceNumber = productSequenceNumber;
    }

    public DisbursementDetails getDisbursementDetails() {
        return disbursementDetails;
    }

    public void setDisbursementDetails(DisbursementDetails disbursementDetails) {
        this.disbursementDetails = disbursementDetails;
    }

    public Map<String, CompletionInfo>  getCompleted() {
        return completed;
    }

    public void setCompleted(Map<String, CompletionInfo> completed) {
        this.completed = completed;
    }

    public Map<String, CustomData> getApplicantCoApplicantData() {        return applicantCoApplicantData;    }

    public void setApplicantCoApplicantData(Map<String, CustomData> applicantCoApplicantData) {        this.applicantCoApplicantData = applicantCoApplicantData;    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GoNoGoCustomerApplication [gngRefId=");
        builder.append(gngRefId);
        builder.append(", parentID=");
        builder.append(parentID);
        builder.append(", rootID=");
        builder.append(rootID);
        builder.append(", productSequenceNumber=");
        builder.append(productSequenceNumber);
        builder.append(", dateTime=");
        builder.append(dateTime);
        builder.append(", applicationStatus=");
        builder.append(applicationStatus);
        builder.append(", statusFlag=");
        builder.append(statusFlag);
        builder.append(", reInitiateCount=");
        builder.append(reInitiateCount);
        builder.append(", reappraiseReq=");
        builder.append(reappraiseReq);
        builder.append(", reInitiateRemark=");
        builder.append(reInitiateRemark);
        builder.append(", agreementNum=");
        builder.append(agreementNum);
        builder.append(", applicationRequest=");
        builder.append(applicationRequest);
        builder.append(", applicantComponentResponse=");
        builder.append(applicantComponentResponse);
        builder.append(", applicantComponentResponseList=");
        builder.append(applicantComponentResponseList);
        builder.append(", intrimStatus=");
        builder.append(intrimStatus);
        builder.append(", croDecisions=");
        builder.append(croDecisions);
        builder.append(", negativePincode=");
        builder.append(negativePincode);
        builder.append(", workFlowDetails=");
        builder.append(workFlowDetails);
        builder.append(", applScoreVector=");
        builder.append(applScoreVector);
        builder.append(", applicationLog=");
        builder.append(applicationLog);
        builder.append(", componentStatus=");
        builder.append(componentStatus);
        builder.append(", dedupedApplications=");
        builder.append(dedupedApplications);
        builder.append(", croJustification=");
        builder.append(croJustification);
        builder.append(", losDetails=");
        builder.append(losDetails);
        builder.append(", invoiceDetails=");
        builder.append(invoiceDetails);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GoNoGoCustomerApplication)) return false;
        if (!super.equals(o)) return false;
        GoNoGoCustomerApplication that = (GoNoGoCustomerApplication) o;
        return getProductSequenceNumber() == that.getProductSequenceNumber() &&
                isStatusFlag() == that.isStatusFlag() &&
                getReInitiateCount() == that.getReInitiateCount() &&
                isReappraiseReq() == that.isReappraiseReq() &&
                getReProcessCount() == that.getReProcessCount() &&
                isNegativePincode() == that.isNegativePincode() &&
                Objects.equals(getIntrimStatus(), that.getIntrimStatus()) &&
                Objects.equals(getGngRefId(), that.getGngRefId()) &&
                Objects.equals(getParentID(), that.getParentID()) &&
                Objects.equals(getRootID(), that.getRootID()) &&
                Objects.equals(getDateTime(), that.getDateTime()) &&
                Objects.equals(getApplicationStatus(), that.getApplicationStatus()) &&
                Objects.equals(getReInitiateRemark(), that.getReInitiateRemark()) &&
                Objects.equals(getAgreementNum(), that.getAgreementNum()) &&
                Objects.equals(getApplicationRequest(), that.getApplicationRequest()) &&
                Objects.equals(getApplicantComponentResponse(), that.getApplicantComponentResponse()) &&
                Objects.equals(getApplicantComponentResponseList(), that.getApplicantComponentResponseList()) &&
                Objects.equals(getCroDecisions(), that.getCroDecisions()) &&
                Objects.equals(getWorkFlowDetails(), that.getWorkFlowDetails()) &&
                Objects.equals(getApplScoreVector(), that.getApplScoreVector()) &&
                Objects.equals(getApplicationLog(), that.getApplicationLog()) &&
                Objects.equals(getComponentStatus(), that.getComponentStatus()) &&
                Objects.equals(getDedupedApplications(), that.getDedupedApplications()) &&
                Objects.equals(getCroJustification(), that.getCroJustification()) &&
                Objects.equals(getLosDetails(), that.getLosDetails()) &&
                Objects.equals(getInvoiceDetails(), that.getInvoiceDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getIntrimStatus(), getGngRefId(), getParentID(), getRootID(), getProductSequenceNumber(), getDateTime(), getApplicationStatus(), isStatusFlag(), getReInitiateCount(), isReappraiseReq(), getReProcessCount(), getReInitiateRemark(), getAgreementNum(), getApplicationRequest(), getApplicantComponentResponse(), getApplicantComponentResponseList(), getCroDecisions(), isNegativePincode(), getWorkFlowDetails(), getApplScoreVector(), getApplicationLog(), getComponentStatus(), getDedupedApplications(), getCroJustification(), getLosDetails(), getInvoiceDetails());
    }

}
