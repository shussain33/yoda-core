
package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class HighCourt {

    @JsonProperty("bench")
    public String bench;
    @JsonProperty("caseNo")
    public String caseNo;
    @JsonProperty("caseStatus")
    public String caseStatus;
    @JsonProperty("caseType")
    public String caseType;
    @JsonProperty("court")
    public String court;
    @JsonProperty("petitionerName")
    public String petitionerName;
    @JsonProperty("respondentName")
    public String respondentName;
    @JsonProperty("side")
    public String side;
    @JsonProperty("year")
    public String year;

}
