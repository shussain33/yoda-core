package com.softcell.gonogo.model.core.verification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by yogesh on 8/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BankingVerification extends ThirdPartyVerification {

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sMifinBankID")
    private String mifinBankCode;

    @JsonProperty("sIfscCode")
    private String ifscCode;

    @JsonProperty("sBranchName")
    private String branchName;

    @JsonProperty("sAccountNumber")
    private String accountNumber;

    @JsonProperty("sAccountType")
    private String accountType;

    @JsonProperty("dtAccountSince")
    private Date accountSince;

    @JsonProperty("sFinalStatus")
    private String finalStatus;

    @JsonProperty("bRepayment")
    private boolean showBank;

    @JsonProperty("aTransactionDetails")
    private List<TransactionDetails> transactionDetails;



}

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
class TransactionDetails {
    //This are additional fields in output along with above fields excluding document.
    //credit/debit/balance
    @JsonProperty("sTransactionType")
    private String transactionType;

    @JsonProperty("dTransactionAmount")
    private double transactionAmount;

    @JsonProperty("dtTransactionDate")
    private Date transactionDate;

    @JsonProperty("aVendorRemarks")
    private List<Remark> vendorRemarks;
}