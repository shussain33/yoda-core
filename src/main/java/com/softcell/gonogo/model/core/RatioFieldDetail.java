package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 19/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RatioFieldDetail {
    @JsonProperty("eFieldName")
    private RatioFields fieldName;

    @JsonProperty("dPreviousYear")
    private double previousYear;

    @JsonProperty("dCurrentYear")
    private double currentYear;

    @JsonProperty("aYears")
    private List<YearValue> years;

}
