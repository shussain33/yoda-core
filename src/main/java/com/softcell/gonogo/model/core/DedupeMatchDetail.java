package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.Remark;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 29/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DedupeMatchDetail {

    @JsonProperty("oDedupeDetails")
    public DedupeDetails dedupeDetails;

    @JsonProperty("sMatchFound")
    public String matchFound;

    @JsonProperty("sMatchDetails")
    public String matchingParam;

    @JsonProperty("sStatus")
    public String dedupeStatus; // +ve or -ve

    @JsonProperty("oRemarks")
    public Remark remark;


    //DigiPL Fields Added
    @JsonProperty("sApplID")
    private String applicantId;


}
