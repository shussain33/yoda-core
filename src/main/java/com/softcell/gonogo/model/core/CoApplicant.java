package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author kishorp
 *         This class consist Co-Applicant details of  Applicant.
 *         This class inherits Applicant class
 * @author vinodk
 */
public class CoApplicant extends Applicant implements Serializable {
    /**
     * Relation of co-applicant with applicants.
     */
    // @JsonProperty(CustomerJsonKeys.RELATION_WITH_APPLICANT)
    @JsonProperty("sRelWithAppl")
    private String relationWithApplicant;

    @JsonProperty("sRelWithPropertyOwner")
    private String relationWithPropertyOwner;

    @JsonProperty("bEarning")
    private boolean earning;

    @JsonProperty("bSameApplicantAddr")
    private boolean resAddrSameAsApplt;

    /**
     * @return the relationWithApplicant
     */
    public String getRelationWithApplicant() {
        return relationWithApplicant;
    }

    /**
     * @param relationWithApplicant the relationWithApplicant to set
     */
    public void setRelationWithApplicant(String relationWithApplicant) {
        this.relationWithApplicant = relationWithApplicant;
    }

    public String getRelationWithPropertyOwner() {
        return relationWithPropertyOwner;
    }

    public void setRelationWithPropertyOwner(String relationWithPropertyOwner) {
        this.relationWithPropertyOwner = relationWithPropertyOwner;
    }

    /**
     * @return the earning
     */
    public boolean isEarning() {
        return earning;
    }

    public boolean isResAddrSameAsApplt() {
        return resAddrSameAsApplt;
    }

    public void setResAddrSameAsApplt(boolean resAddrSameAsApplt) {
        this.resAddrSameAsApplt = resAddrSameAsApplt;
    }

    /**
     * @param earning the earning to set
     */
    public void setEarning(boolean earning) {
        this.earning = earning;
    }

    /*
         * (non-Javadoc)
         *
         * @see java.lang.Object#toString()
         */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CoApplicant [relationWithApplicant=");
        builder.append(relationWithApplicant);
        builder.append(", isEarning=");
        builder.append(earning);
        builder.append(", relationWithPropertyOwner=");
        builder.append(relationWithPropertyOwner);
        builder.append(", resAddrSameAsApplt=");
        builder.append(resAddrSameAsApplt);
        builder.append("]");
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (earning ? 1231 : 1237);
        result = prime
                * result
                + ((relationWithApplicant == null) ? 0 : relationWithApplicant
                .hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CoApplicant other = (CoApplicant) obj;
        if (earning != other.earning)
            return false;
        if (relationWithApplicant == null) {
            if (other.relationWithApplicant != null)
                return false;
        } else if (!relationWithApplicant.equals(other.relationWithApplicant))
            return false;
        return true;
    }

    public boolean equalsCoApplicant(Object obj)
    {
        CoApplicant coApplicant=(CoApplicant)obj;
        return Objects.equals(this.getApplicantId(), coApplicant.getApplicantId())&&
                Objects.equals(this.getApplicantName(), coApplicant.getApplicantName())&&
                Objects.equals(this.getApplicantName().getFirstName(), coApplicant.getApplicantName().getFirstName())&&
                Objects.equals(this.getApplicantName().getMiddleName(),coApplicant.getApplicantName().getMiddleName()) &&
                Objects.equals(this.getApplicantName().getLastName(),coApplicant.getApplicantName().getLastName()) &&
                Objects.equals(this.getFatherName().getFirstName(),coApplicant.getFatherName().getFirstName()) &&
                Objects.equals(this.getMotherName().getFirstName(),coApplicant.getMotherName().getFirstName()) &&
                Objects.equals(this.getDateOfBirth(),coApplicant.getDateOfBirth()) &&
                Objects.equals(this.getApplicantType(),coApplicant.getApplicantType()) &&
                Objects.equals(this.getEmail(),coApplicant.getEmail()) &&
                Objects.equals(this.getGender(),coApplicant.getGender()) &&
                Objects.equals(this.getPhone(),coApplicant.getPhone()) &&
                Objects.equals(this.getAddress(),coApplicant.getAddress()) &&
                Objects.equals(this.getEmployment(),coApplicant.getEmployment()) &&
                Objects.equals(this.getPhone(),coApplicant.getPhone()) &&
                Objects.equals(this.getPhone(),coApplicant.getPhone()) ;
    }
}
