package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.workflow.ModuleConfiguration;

import java.util.List;

/**
 * Work-flow details is use to register
 * result action  of work, It records all
 * action details of work flow. which will
 * use for re-initiate.
 *
 * @author kishor
 */
public class WorkFlowDetails {
    @JsonProperty("aModulesConfig")
    List<ModuleConfiguration> workFlowDetails;

    public List<ModuleConfiguration> getWorkFlowDetails() {
        return workFlowDetails;
    }

    public void setWorkFlowDetails(List<ModuleConfiguration> workFlowDetails) {
        this.workFlowDetails = workFlowDetails;
    }


}
