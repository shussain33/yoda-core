package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityBySalaried {

    @JsonProperty("sCompanyName")
    private String companyName;

    @JsonProperty("sSalaryCreditMode")
    private String salaryCreditMode;

    @JsonProperty("dGrossIncome")
    private double grossIncome;

    @JsonProperty("dPfDeduction")
    private double pfDeduction;

    @JsonProperty("dOtherDeduction")
    private double otherDeduction;

    @JsonProperty("iElgbltyRoi")
    private int eligibilityRoi;

    @JsonProperty("dAprvAmount")
    private double aprovedAmount;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("dFixedObligation")
    private double fixedObligation;

    @JsonProperty("dOtherIncome")
    private double otherIncome;

    @JsonProperty("dNetIncome")
    private double netIncome;

    @JsonProperty("sDesignation")
    private String designation;

    @JsonProperty("iTotalExp")
    private int totalExperiance;

    @JsonProperty("dIncommeAsITR")
    private double incomeAsItr;

}
