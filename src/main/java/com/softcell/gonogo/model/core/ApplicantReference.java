/**
 *
 */
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.Address;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.request.UpdateReferencesRequest;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;

/**
 * @author kishorp
 */
public class ApplicantReference implements Serializable {
    @JsonProperty("oApplRefName")
    @NotNull(groups = {UpdateReferencesRequest.FetchGrp.class})
    @Valid
    private Name name;

    @JsonProperty("oApplRefAddr")
    @NotNull(groups = {UpdateReferencesRequest.FetchGrp.class})
    @Valid
    private Address address;

    @JsonProperty("sRelType")
    @NotBlank(groups = {UpdateReferencesRequest.FetchGrp.class})
    private String relationType;

    /**
     * Needs to be delete,
     * use aPhone instead of this.
     * dmi using this field.
     */
    @JsonProperty("oPhone")
    private Phone phone;

    @JsonProperty("aPhone")
    @NotNull(groups = {UpdateReferencesRequest.FetchGrp.class})
    @Size(min = 1, groups = {UpdateReferencesRequest.FetchGrp.class})
    @Valid
    private Collection<Phone> phones;

    @JsonProperty("sOccup")
    private String occupation;

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRelationType() {
        return relationType;
    }

    public void setRelationType(String relationType) {
        this.relationType = relationType;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }


    public Collection<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Collection<Phone> phones) {
        this.phones = phones;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApplicantReference{");
        sb.append("name=").append(name);
        sb.append(", address=").append(address);
        sb.append(", relationType='").append(relationType).append('\'');
        sb.append(", phone=").append(phone);
        sb.append(", phones=").append(phones);
        sb.append(", occupation='").append(occupation).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApplicantReference that = (ApplicantReference) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (relationType != null ? !relationType.equals(that.relationType) : that.relationType != null) return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (phones != null ? !phones.equals(that.phones) : that.phones != null) return false;
        return occupation != null ? occupation.equals(that.occupation) : that.occupation == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (relationType != null ? relationType.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (phones != null ? phones.hashCode() : 0);
        result = 31 * result + (occupation != null ? occupation.hashCode() : 0);
        return result;
    }
}
