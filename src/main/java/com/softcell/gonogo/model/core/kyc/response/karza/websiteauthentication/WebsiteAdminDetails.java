package com.softcell.gonogo.model.core.kyc.response.karza.websiteauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class WebsiteAdminDetails {

    @JsonProperty("city")
    private String city;

    @JsonProperty("fax")
    private String fax;

    @JsonProperty("name")
    private String name;

    @JsonProperty("country")
    private String country;

    @JsonProperty("stateprovince")
    private String stateprovince;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("street")
    private String street;

    @JsonProperty("organization")
    private String organization;

    @JsonProperty("postal")
    private String postal;

    @JsonProperty("email")
    private String email;

}