package com.softcell.gonogo.model.core.kyc.response.karza.kscan;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
public class PetitionerAndAdvocate {

    @JsonProperty("advocate")
    public String advocate;

    @JsonProperty("name")
    public String name;

}