package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class FilingStatus {

    @JsonProperty("status")
    private String status;

    @JsonProperty("fy")
    private String fy;

    @JsonProperty("dof")
    private String dof;

    @JsonProperty("rtntype")
    private String rtntype;

    @JsonProperty("taxp")
    private String taxp;

    @JsonProperty("mof")
    private String mof;

    @JsonProperty("arn")
    private String arn;
}