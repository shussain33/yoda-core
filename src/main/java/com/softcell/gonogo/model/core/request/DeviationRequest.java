package com.softcell.gonogo.model.core.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Deviation;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.request.core.Header;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by yogesh on 15/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviationRequest {

    @JsonProperty("oHeader")
    @NotEmpty(groups = {Header.InsertGroup.class,Header.FetchGrp.class,DeviationRequest.FetchGrp.class})
    public Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {DeviationRequest.InsertGrp.class,DeviationRequest.FetchGrp.class})
    public String refId;

    @JsonProperty("oDeviation")
    @NotNull(groups = {DeviationRequest.InsertGrp.class})
    public DeviationDetails deviationDetails;

    @JsonProperty("sMitigationRemark")
    public String mitigationRemark;

    @JsonProperty("sResolvedBy")
    public String resolvedBy;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}