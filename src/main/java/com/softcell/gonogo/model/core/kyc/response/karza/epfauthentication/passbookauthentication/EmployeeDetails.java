package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.passbookauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmployeeDetails {

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("father_name")
    private String father_name;

    @JsonProperty("member_name")
    private String member_name;
}