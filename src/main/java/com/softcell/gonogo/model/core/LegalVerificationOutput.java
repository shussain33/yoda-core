package com.softcell.gonogo.model.core;

import java.util.*;
import com.softcell.gonogo.model.Remark;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LegalVerificationOutput{

	@JsonProperty("aOwnerNmOnPpr")
	private List<Name> ownerNameOnPapers;

	@JsonProperty("aDocProvided")
	private List<String> docsProvided;

	@JsonProperty("sPropHistory")
	private String propHistory;

	/* Dropdown of required documents, user can add multiple document with status */
	@JsonProperty("aPreMortgDoc")
	private List<String> preMortgageDocs;

	/* Dropdown of required documents, user can add multiple document with status */
	@JsonProperty("aPostMortgDoc")
	private List<String> postMortgageDocs;

	@JsonProperty("bDocTitlVerified")
	private boolean docTitleVerified;

	@JsonProperty("bDocHistoryTraced")
	private boolean docHistoryTraced;

	@JsonProperty("bOwnershipVerified")
	private boolean ownershipHistoryVerified;

	@JsonProperty("bSearchVerified")
	private boolean searchReportVerified;

	@JsonProperty("bTitleCertified")
	private boolean titleCertified;

	@JsonProperty("bSharedProp")
	private boolean sharedProp;

	@JsonProperty("bDocSatifactory")
	private boolean docSatisfyMortgage;

	@JsonProperty("bClearTitle")
	private boolean clearTitle;

	@JsonProperty("bCoSrchRpt")
	private boolean searchReportOfCompany;

	@JsonProperty("sObservation")
	private String observation;

	@JsonProperty("sSigningAuth")
	private String signingAuthority;

	/* (0-3,3-8,8-13,13+) */
	@JsonProperty("sDocYears")
	private String propDocYears;

	/* (Leasehold/Freehold) */
	@JsonProperty("sTypeOfProp")
	private String typeOfProp;

	/* (Positive/Negative/Hold) */
	@JsonProperty("sTitleSearch")
	private String titleSearch;

	@JsonProperty("bRegMortReq")
	private boolean registeredMortgageRequired;

	/* (Panchayat property, Khasra property, Municipal property, Lal Dora, Patta) */
	@JsonProperty("sPropBelonsTo")
	private String propBelongsTo;

}
