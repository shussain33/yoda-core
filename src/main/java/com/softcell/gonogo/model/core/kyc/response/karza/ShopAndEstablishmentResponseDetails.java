package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ShopAndEstablishmentResponseDetails {

    @JsonProperty("status")
    private String status;

    @JsonProperty("owner_name")
    private String owner_name;

    @JsonProperty("entity_name")
    private String entity_name;

    @JsonProperty("registration_date")
    private String registration_date;

    @JsonProperty("valid_to")
    private String valid_to;

    @JsonProperty("contact")
    private String contact;

    @JsonProperty("commence_date")
    private String commence_date;

    @JsonProperty("total_workers")
    private String total_workers;

    @JsonProperty("father's_name_of_occupier")
    private String fathers_name_of_occupier;

    @JsonProperty("nature_of_business")
    private String nature_of_business;

    @JsonProperty("address")
    private String address;

    @JsonProperty("valid_from")
    private String valid_from;

    @JsonProperty("email")
    private String email;

    @JsonProperty("website_url")
    private String website_url;

}