package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced.Match;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kishorp
 */

public class Employment implements Serializable,Comparable<Employment> {

    @JsonProperty("sEmplType")
    private String employmentType;

    // private / public
    @JsonProperty("sEmployerType")
    private String employerType;

    @JsonProperty("sEmplName")
    private String employmentName;

    @JsonProperty("iTmWithEmplr")
    private int timeWithEmployer;

    @JsonProperty("sDtJoin")
    private String dateOfJoining;

    @JsonProperty("sDtLeave")
    private String dateOfLeaving;

    @JsonProperty("dmonthSal")
    private double monthlySalary;

    @JsonProperty("dGrossSal")
    private double grossSalary;

    @JsonProperty("aLastMonthIncome")
    private List<LastMonthIncome> lastMonthIncome = new ArrayList<LastMonthIncome>();

    @JsonProperty("sConst")
    private String constitution;

    @JsonProperty("sIndustType")
    private String industType;

    @JsonProperty("sItrID")
    private String itrId;

    @JsonProperty("dItrAmt")
    private double itrAmount;

    /**
     * Newely added for hdbfs
     */
    @JsonProperty("sDesig")
    private String designation;

    @JsonProperty("sEmplrCode")
    private String employerCode;

    @JsonProperty("sEmplrBr")
    private String employerBranch;

    @JsonProperty("sModePayment")
    private String modePayment;

    @JsonProperty("sDeptmt")
    private String department;

    @JsonProperty("sWorkExps")
    private String workExps;

    @JsonProperty("iTotalYrsOfExp")
    private int totalYearsOfExperience;

    @JsonProperty("sBusinesName")
    private String businessName;

    @JsonProperty("dtComencemnt")
    private String commencementDate;

    /**
     * added for hdbfs_pl
     */
    @JsonProperty("sIndstryType")
    private String industryType;

    /**
     +     * added for sbfc_pl
     +     */

    @JsonProperty("sEmployerTypeOther")
   private String employerTypeOther;

    @JsonProperty("sOwnerShip")
    private String ownerShip;

    @JsonProperty("sEmailAddress")
    private String emailAddress;

    @JsonProperty("bEmailStatus")
    private boolean emailStatus;

    @JsonProperty("bDomainStatus")
    private boolean domainStatus;

    @JsonProperty("bEmployerStatus")
    private boolean employerStatus;

    @JsonProperty("sEmploymentCategory")
    private String employmentCategory;

    @JsonProperty("oEpfHistory")
    private Match epfHistory;

    /**
     * added for SBFC emi-calculator screen (company name) (PL)
     */
    @JsonProperty("sEmplNameEMI")
    private String emplNameEMI;

    /**
     * added for SBFC emi-calculator screen (company category) (PL)
     */
    @JsonProperty("sEmplCatEMI")
    private String emplCatEMI;

    public Match getEpfHistory() { return epfHistory; }

    public void setEpfHistory(Match epfHistory) { this.epfHistory = epfHistory; }

    public boolean getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(boolean emailStatus) { this.emailStatus = emailStatus; }

    public boolean getDomainStatus() {
        return domainStatus;
    }

    public void setDomainStatus(boolean domainStatus) {
        this.domainStatus = domainStatus;
    }

    public boolean getEmployerStatus() { return employerStatus;  }

    public void setEmployerStatus(boolean employerStatus) {
        this.employerStatus = employerStatus;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmployerTypeOther() {
        return employerTypeOther;
    }

    public void setEmployerTypeOther(String employerTypeOther) {
        this.employerTypeOther = employerTypeOther;
    }

    public String getEmployerType() {
        return employerType;
    }

    public void setEmployerType(String employerType) {
        this.employerType = employerType;
    }

    public String getModePayment() {
        return modePayment;
    }

    public void setModePayment(String modePayment) {
        this.modePayment = modePayment;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(String employmentType) {
        this.employmentType = employmentType;
    }

    public String getIndustType() {
        return industType;
    }

    public void setIndustType(String industType) {
        this.industType = industType;
    }

    public String getEmploymentName() {
        return employmentName;
    }

    public void setEmploymentName(String employmentName) {
        this.employmentName = employmentName;
    }

    public int getTimeWithEmployer() {
        return timeWithEmployer;
    }

    public void setTimeWithEmployer(int timeWithEmployer) {
        this.timeWithEmployer = timeWithEmployer;
    }

    public String getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(String dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public String getDateOfLeaving() {
        return dateOfLeaving;
    }

    public void setDateOfLeaving(String dateOfLeaving) {
        this.dateOfLeaving = dateOfLeaving;
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    public double getGrossSalary() {
        return grossSalary;
    }

    public void setGrossSalary(double grossSalary) {
        this.grossSalary = grossSalary;
    }

    public List<LastMonthIncome> getLastMonthIncome() {
        return lastMonthIncome;
    }

    public void setLastMonthIncome(List<LastMonthIncome> lastMonthIncome) {
        this.lastMonthIncome = lastMonthIncome;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmployerCode() {
        return employerCode;
    }

    public void setEmployerCode(String employerCode) {
        this.employerCode = employerCode;
    }

    public String getEmployerBranch() {
        return employerBranch;
    }

    public void setEmployerBranch(String employerBranch) {
        this.employerBranch = employerBranch;
    }

    public String getConstitution() {
        return constitution;
    }

    public void setConstitution(String constitution) {
        this.constitution = constitution;
    }

    public String getItrId() {
        return itrId;
    }

    public void setItrId(String itrId) {
        this.itrId = itrId;
    }

    public double getItrAmount() {
        return itrAmount;
    }

    public void setItrAmount(double itrAmount) {
        this.itrAmount = itrAmount;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getWorkExps() {
        return workExps;
    }

    public void setWorkExps(String workExps) {
        this.workExps = workExps;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getCommencementDate() {
        return commencementDate;
    }

    public void setCommencementDate(String commencementDate) {
        this.commencementDate = commencementDate;
    }

    public int getTotalYearsOfExperience() {
        return totalYearsOfExperience;
    }

    public void setTotalYearsOfExperience(int totalYearsOfExperience) {
        this.totalYearsOfExperience = totalYearsOfExperience;
    }

    /**
     * @return the industryType
     */
    public String getIndustryType() {
        return industryType;
    }

    /**
     * @param industryType the industryType to set
     */
    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getOwnerShip() {  return ownerShip;    }

    public void setOwnerShip(String ownerShip) {  this.ownerShip = ownerShip;    }

    public String getEmploymentCategory() { return employmentCategory; }

    public void setEmploymentCategory(String employmentCategory) { this.employmentCategory = employmentCategory; }

    public String getEmplNameEMI() {
        return emplNameEMI;
    }

    public void setEmplNameEMI(String emplNameEMI) {
        this.emplNameEMI = emplNameEMI;
    }

    public String getEmplCatEMI() {
        return emplCatEMI;
    }

    public void setEmplCatEMI(String emplCatEMI) {
        this.emplCatEMI = emplCatEMI;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Employment [employmentType=");
        builder.append(employmentType);
        builder.append(", employmentName=");
        builder.append(employmentName);
        builder.append(", timeWithEmployer=");
        builder.append(timeWithEmployer);
        builder.append(", dateOfJoining=");
        builder.append(dateOfJoining);
        builder.append(", dateOfLeaving=");
        builder.append(dateOfLeaving);
        builder.append(", monthlySalary=");
        builder.append(monthlySalary);
        builder.append(", grossSalary=");
        builder.append(grossSalary);
        builder.append(", lastMonthIncome=");
        builder.append(lastMonthIncome);
        builder.append(", constitution=");
        builder.append(constitution);
        builder.append(", industType=");
        builder.append(industType);
        builder.append(", itrId=");
        builder.append(itrId);
        builder.append(", itrAmount=");
        builder.append(itrAmount);
        builder.append(", designation=");
        builder.append(designation);
        builder.append(", employerCode=");
        builder.append(employerCode);
        builder.append(", employerBranch=");
        builder.append(employerBranch);
        builder.append(", modePayment=");
        builder.append(modePayment);
        builder.append(", department=");
        builder.append(department);
        builder.append(", workExps=");
        builder.append(workExps);
        builder.append(", totalYearsOfExperience=");
        builder.append(totalYearsOfExperience);
        builder.append(", businessName=");
        builder.append(businessName);
        builder.append(", commencementDate=");
        builder.append(commencementDate);
        builder.append(", industryType=");
        builder.append(industryType);
        builder.append(", emplNameEMI=");
        builder.append(emplNameEMI);
        builder.append(", emplCatEMI=");
        builder.append(emplCatEMI);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((businessName == null) ? 0 : businessName.hashCode());
        result = prime
                * result
                + ((commencementDate == null) ? 0 : commencementDate.hashCode());
        result = prime * result
                + ((constitution == null) ? 0 : constitution.hashCode());
        result = prime * result
                + ((dateOfJoining == null) ? 0 : dateOfJoining.hashCode());
        result = prime * result
                + ((dateOfLeaving == null) ? 0 : dateOfLeaving.hashCode());
        result = prime * result
                + ((department == null) ? 0 : department.hashCode());
        result = prime * result
                + ((designation == null) ? 0 : designation.hashCode());
        result = prime * result
                + ((employerBranch == null) ? 0 : employerBranch.hashCode());
        result = prime * result
                + ((employerCode == null) ? 0 : employerCode.hashCode());
        result = prime * result
                + ((employmentName == null) ? 0 : employmentName.hashCode());
        result = prime * result
                + ((employmentType == null) ? 0 : employmentType.hashCode());
        long temp;
        temp = Double.doubleToLongBits(grossSalary);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((industType == null) ? 0 : industType.hashCode());
        result = prime * result
                + ((industryType == null) ? 0 : industryType.hashCode());
        temp = Double.doubleToLongBits(itrAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((itrId == null) ? 0 : itrId.hashCode());
        result = prime * result
                + ((lastMonthIncome == null) ? 0 : lastMonthIncome.hashCode());
        result = prime * result
                + ((modePayment == null) ? 0 : modePayment.hashCode());
        temp = Double.doubleToLongBits(monthlySalary);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + timeWithEmployer;
        result = prime * result
                + ((workExps == null) ? 0 : workExps.hashCode());
        temp = totalYearsOfExperience;
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Employment other = (Employment) obj;
        if (businessName == null) {
            if (other.businessName != null)
                return false;
        } else if (!businessName.equals(other.businessName))
            return false;
        if (commencementDate == null) {
            if (other.commencementDate != null)
                return false;
        } else if (!commencementDate.equals(other.commencementDate))
            return false;
        if (constitution == null) {
            if (other.constitution != null)
                return false;
        } else if (!constitution.equals(other.constitution))
            return false;
        if (dateOfJoining == null) {
            if (other.dateOfJoining != null)
                return false;
        } else if (!dateOfJoining.equals(other.dateOfJoining))
            return false;
        if (dateOfLeaving == null) {
            if (other.dateOfLeaving != null)
                return false;
        } else if (!dateOfLeaving.equals(other.dateOfLeaving))
            return false;
        if (department == null) {
            if (other.department != null)
                return false;
        } else if (!department.equals(other.department))
            return false;
        if (designation == null) {
            if (other.designation != null)
                return false;
        } else if (!designation.equals(other.designation))
            return false;
        if (employerBranch == null) {
            if (other.employerBranch != null)
                return false;
        } else if (!employerBranch.equals(other.employerBranch))
            return false;
        if (employerCode == null) {
            if (other.employerCode != null)
                return false;
        } else if (!employerCode.equals(other.employerCode))
            return false;
        if (employmentName == null) {
            if (other.employmentName != null)
                return false;
        } else if (!employmentName.equals(other.employmentName))
            return false;
        if (employmentType == null) {
            if (other.employmentType != null)
                return false;
        } else if (!employmentType.equals(other.employmentType))
            return false;
        if (Double.doubleToLongBits(grossSalary) != Double
                .doubleToLongBits(other.grossSalary))
            return false;
        if (industType == null) {
            if (other.industType != null)
                return false;
        } else if (!industType.equals(other.industType))
            return false;
        if (industryType == null) {
            if (other.industryType != null)
                return false;
        } else if (!industryType.equals(other.industryType))
            return false;
        if (Double.doubleToLongBits(itrAmount) != Double
                .doubleToLongBits(other.itrAmount))
            return false;
        if (itrId == null) {
            if (other.itrId != null)
                return false;
        } else if (!itrId.equals(other.itrId))
            return false;
        if (lastMonthIncome == null) {
            if (other.lastMonthIncome != null)
                return false;
        } else if (!lastMonthIncome.equals(other.lastMonthIncome))
            return false;
        if (modePayment == null) {
            if (other.modePayment != null)
                return false;
        } else if (!modePayment.equals(other.modePayment))
            return false;
        if (Double.doubleToLongBits(monthlySalary) != Double
                .doubleToLongBits(other.monthlySalary))
            return false;
        if (timeWithEmployer != other.timeWithEmployer)
            return false;
        if (workExps == null) {
            if (other.workExps != null)
                return false;
        } else if (!workExps.equals(other.workExps))
            return false;

        if (totalYearsOfExperience != other.totalYearsOfExperience)
                return false;

        return true;
    }

    @Override
    public int compareTo(Employment employment) {
        return this.getDateOfJoining().compareTo(employment.getDateOfJoining());
    }
}
