package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TelephoneBillAuthenticationResponseDetails {

    @JsonProperty("category")
    private String category;

    @JsonProperty("TelephoneNo")
    private String TelephoneNo;

    @JsonProperty("name")
    private String name;

    @JsonProperty("Installation_Type")
    private String Installation_Type;

    @JsonProperty("address")
    private String address;

}