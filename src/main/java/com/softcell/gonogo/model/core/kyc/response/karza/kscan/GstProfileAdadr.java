package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GstProfileAdadr {
    @JsonProperty("addr")
    private String addr ;

    @JsonProperty("adr")
    private String adr;

    @JsonProperty("em")
    private String em;

    @JsonProperty("lastUpdatedDate")
    private String lastUpdatedDate;

    @JsonProperty("mb")
    private int mb;

    @JsonProperty("ntr")
    private String ntr;
}