package com.softcell.gonogo.model.core.kyc.response.karza.nregaauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 05/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ApplicantDetails {

    @JsonProperty("name")
    private String name;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("age")
    private String age;

    @JsonProperty("bankorpostoffice")
    private String bankorpostoffice;

    @JsonProperty("aadhaarNo")
    private String aadhaarNo;

    @JsonProperty("accountNo")
    private String accountNo;
}