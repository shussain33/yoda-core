package com.softcell.gonogo.model.core.digitization;

import com.softcell.utils.GngNumUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;

/**
 * Created by mahesh on 17/12/17.
 */
public class AchMandateFormMapper {
    private static final Logger logger = LoggerFactory.getLogger(AchMandateFormMapper.class);

    public static VelocityContext mapAchMandateForm(AchMandateForm achMandateForm) throws ParseException {

        VelocityContext context = new VelocityContext();
        if (StringUtils.isNotBlank(achMandateForm.getTickMark())) {
            context.put("tickMark", achMandateForm.getTickMark());
        }
        if (StringUtils.isNotBlank(achMandateForm.getTickMark())) {
            context.put("crossBox", achMandateForm.getCrossBox());
        }
        if (StringUtils.isNotBlank(achMandateForm.getTickMark())) {
            context.put("indianCurrency", achMandateForm.getIndianRupee());
        }
        if(StringUtils.isNotBlank(achMandateForm.getHdbfsLogo())){
            context.put("hdbfsLogo", achMandateForm.getHdbfsLogo());
        }
        if(StringUtils.isNotBlank(achMandateForm.getScissor())){
            context.put("scissors", achMandateForm.getScissor());
        }
        if (StringUtils.isNotBlank(achMandateForm.getAccHolderName())) {
            context.put("accountHolderName", achMandateForm.getAccHolderName());
        }
        if (StringUtils.isNotBlank(achMandateForm.getFinanceAmount())) {
            context.put("financeAmountInRs", achMandateForm.getFinanceAmount());
        }
        if (StringUtils.isNotBlank(achMandateForm.getFinanceAmount())) {
            try {
                context.put("financeAmountInWords", GngNumUtil
                        .convert(new Double(achMandateForm.getFinanceAmount()).longValue()));
            } catch (Exception e) {
                logger.error("Exception occur at the time of converting number to words with probable cause {} ", e.getMessage());
            }

        }
        if (StringUtils.isNotBlank(achMandateForm.getFinanceAmount())) {
            context.put("financeAmountInRs", achMandateForm.getFinanceAmount());
        }

        if (StringUtils.isNotBlank(achMandateForm.getApplicationDate())) {
            context.put("applicationDateList", GngUtils.getCharacterList(achMandateForm.getApplicationDate()));
        }

        if (StringUtils.isNotBlank(achMandateForm.getBankName())) {
            context.put("bankName", achMandateForm.getBankName());
        }
        if (StringUtils.isNotBlank(achMandateForm.getBanckAccNo())) {
            context.put("accountNumberList", GngUtils.getCharacterList(achMandateForm.getBanckAccNo()));
        }
        if (StringUtils.isNotBlank(achMandateForm.getIfscCode())) {
            context.put("ifscCodeList", GngUtils.getCharacterList(achMandateForm.getIfscCode()));
        }
        if (StringUtils.isNotBlank(achMandateForm.getMobileNo())) {
            context.put("mobileNo", achMandateForm.getMobileNo());
        }
        if (StringUtils.isNotBlank(achMandateForm.getEmailId())) {
            context.put("emailID", achMandateForm.getEmailId());
        }

        return context;
    }
}
