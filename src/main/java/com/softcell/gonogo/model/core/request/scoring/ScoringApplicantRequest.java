package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * Created by yogesh on 9/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "ScoringApplicantRequest")
public class ScoringApplicantRequest {


    @Id
    @JsonIgnore
    private String gngRefId;

    @JsonIgnore
    private Date dateTime = new Date();
    /**
     * scoring header
     */
    @JsonProperty("HEADER")
    ScoringHeader header;

    @JsonProperty("APPLICANTS")
    private List<ApplicantRequest> applicants;

    @JsonProperty("APPLICATION")
    private Application application;

    //For set request in String Format
    @JsonProperty("SRequest")
    private String request;

    //DigiPL Fields Started
    @JsonProperty
    private String policyName;

}
