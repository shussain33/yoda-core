package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data {

    @JsonProperty("Rbd")
    private ResidentBiometricData rbd;

    public ResidentBiometricData getRbd() {
        return rbd;
    }

    public void setRbd(ResidentBiometricData rbd) {
        this.rbd = rbd;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Data{");
        sb.append("rbd=").append(rbd);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Data)) return false;
        Data data = (Data) o;
        return Objects.equal(getRbd(), data.getRbd());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getRbd());
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private Data data = new Data();

        public Data build(){
            return data;
        }

        public Builder rbd(ResidentBiometricData rbd){
            this.data.rbd=rbd;
            return this;
        }
    }

}
