package com.softcell.gonogo.model.core.request.scoring;


public class MFIAdditionalIdentityInfoType {

    private String mfiVoterID;
    private String mfiPANCardID;
    private String mfiRationCard;
    private String mfiUID;
    private String mfiOtherID;


    public String getMfiVoterID() {
        return mfiVoterID;
    }

    public void setMfiVoterID(String mfiVoterID) {
        this.mfiVoterID = mfiVoterID;
    }

    public String getMfiPANCardID() {
        return mfiPANCardID;
    }

    public void setMfiPANCardID(String mfiPANCardID) {
        this.mfiPANCardID = mfiPANCardID;
    }

    public String getMfiRationCard() {
        return mfiRationCard;
    }

    public void setMfiRationCard(String mfiRationCard) {
        this.mfiRationCard = mfiRationCard;
    }

    public String getMfiUID() {
        return mfiUID;
    }

    public void setMfiUID(String mfiUID) {
        this.mfiUID = mfiUID;
    }

    public String getMfiOtherID() {
        return mfiOtherID;
    }

    public void setMfiOtherID(String mfiOtherID) {
        this.mfiOtherID = mfiOtherID;
    }

    @Override
    public String toString() {
        return "MFIAdditionalIdentityInfoType [mfiVoterID=" + mfiVoterID
                + ", mfiPANCardID=" + mfiPANCardID + ", mfiRationCard="
                + mfiRationCard + ", mfiUID=" + mfiUID + ", mfiOtherID="
                + mfiOtherID + "]";
    }
}
