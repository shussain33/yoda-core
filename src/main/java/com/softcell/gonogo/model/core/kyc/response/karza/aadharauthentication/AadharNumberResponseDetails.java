package com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AadharNumberResponseDetails {

    @JsonProperty("success")
    private String success;

    @JsonProperty("aadhaar-reference-code")
    private String aadhaar_reference_code;

    @JsonProperty("info")
    private String info;

    @JsonProperty("pid-timestamp")
    private String pid_timestamp;

    @JsonProperty("aadhaar-status-code")
    private String aadhaar_status_code;

}