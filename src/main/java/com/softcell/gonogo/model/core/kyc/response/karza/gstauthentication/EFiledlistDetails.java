package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EFiledlistDetails {

    @JsonProperty("status")
    private String status;

    @JsonProperty("dof")
    private String dof;

    @JsonProperty("ret_prd")
    private String ret_prd;

    @JsonProperty("is_delay")
    private String is_delay;

    @JsonProperty("valid")
    private String valid;

    @JsonProperty("rtntype")
    private String rtntype;

    @JsonProperty("mof")
    private String mof;

    @JsonProperty("arn")
    private String arn;

}