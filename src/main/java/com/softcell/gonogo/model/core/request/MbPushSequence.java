package com.softcell.gonogo.model.core.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by yogeshb on 13/6/17.
 */
@Document(collection = "mbPushSequence")
public class MbPushSequence extends AuditEntity {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sSeq")
    private long sequence;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MbPushSequence{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", sequence=").append(sequence);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MbPushSequence that = (MbPushSequence) o;

        if (sequence != that.sequence) return false;
        return institutionId != null ? institutionId.equals(that.institutionId) : that.institutionId == null;
    }

    @Override
    public int hashCode() {
        int result = institutionId != null ? institutionId.hashCode() : 0;
        result = 31 * result + (int) (sequence ^ (sequence >>> 32));
        return result;
    }
}
