package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.*;



@XmlRootElement(name="AuthRes")
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthResponse {

	/**
	 * @author Deepak Bhargava
	 *
	 *
	 */
	@XmlAttribute(name="ret")
	private String ret;
	@XmlAttribute(name="code")
	private String code;
	@XmlAttribute(name="txn")
	private String txn;
	@XmlAttribute(name="err")
	private String err;
	@XmlAttribute(name="info")
	private String info;
	@XmlAttribute(name="ts")
	private String ts;
	@XmlAttribute(name="actn")
	private String actn;
	@XmlElement(name="Signature")
	private Signature signature;

	public String getRet() {
		return ret;
	}



	public void setRet(String ret) {
		this.ret = ret;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getTxn() {
		return txn;
	}



	public void setTxn(String txn) {
		this.txn = txn;
	}



	public String getErr() {
		return err;
	}



	public void setErr(String err) {
		this.err = err;
	}



	public String getInfo() {
		return info;
	}



	public void setInfo(String info) {
		this.info = info;
	}



	public String getTs() {
		return ts;
	}



	public void setTs(String ts) {
		this.ts = ts;
	}



	



	public Signature getSignature() {
		return signature;
	}
	



	public void setSignature(Signature signature) {
		this.signature = signature;
	}



	public String getActn() {
		return actn;
	}



	public void setActn(String actn) {
		this.actn = actn;
	}



	@Override
	public String toString() {
		return "AuthResponse [ret=" + ret + ", code=" + code + ", txn=" + txn
				+ ", err=" + err + ", info=" + info + ", ts=" + ts + ", actn="
				+ actn + ", signature=" + signature + "]";
	}
	
}
