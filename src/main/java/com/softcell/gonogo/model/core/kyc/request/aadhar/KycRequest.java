package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KycRequest {

    @JsonProperty("AADHAR-HOLDER-DETAILS")
    private AadharHolderDetails aadharHolderDetails;

    @JsonProperty("BFD-DETAILS")
    private BestFingerDetectionDetails bfdDetails;

    @JsonProperty("OTP-DETAILS")
    private OtpDetails otpDetails;

    @JsonProperty("KYC-DETAILS")
    private KycDetails kycDetails;

    public static Builder builder() {
        return new Builder();
    }

    public AadharHolderDetails getAadharHolderDetails() {
        return aadharHolderDetails;
    }

    public void setAadharHolderDetails(AadharHolderDetails aadharHolderDetails) {
        this.aadharHolderDetails = aadharHolderDetails;
    }

    public BestFingerDetectionDetails getBfdDetails() {
        return bfdDetails;
    }

    public void setBfdDetails(BestFingerDetectionDetails bfdDetails) {
        this.bfdDetails = bfdDetails;
    }

    public OtpDetails getOtpDetails() {
        return otpDetails;
    }

    public void setOtpDetails(OtpDetails otpDetails) {
        this.otpDetails = otpDetails;
    }

    public KycDetails getKycDetails() {
        return kycDetails;
    }

    public void setKycDetails(KycDetails kycDetails) {
        this.kycDetails = kycDetails;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("KycRequest{");
        sb.append("aadharHolderDetails=").append(aadharHolderDetails);
        sb.append(", bfdDetails=").append(bfdDetails);
        sb.append(", otpDetails=").append(otpDetails);
        sb.append(", kycDetails=").append(kycDetails);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KycRequest)) return false;
        KycRequest that = (KycRequest) o;
        return Objects.equal(getAadharHolderDetails(), that.getAadharHolderDetails()) &&
                Objects.equal(getBfdDetails(), that.getBfdDetails()) &&
                Objects.equal(getOtpDetails(), that.getOtpDetails()) &&
                Objects.equal(getKycDetails(), that.getKycDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getAadharHolderDetails(), getBfdDetails(), getOtpDetails(), getKycDetails());
    }

    public static class Builder {
        private KycRequest kycRequest = new KycRequest();

        public KycRequest build() {
            return kycRequest;
        }

        public Builder aadharHolderDetails(AadharHolderDetails aadharHolderDetails){
            this.kycRequest.aadharHolderDetails=aadharHolderDetails;
            return this;
        }

        public Builder bfdDetails(BestFingerDetectionDetails bfdDetails){
            this.kycRequest.bfdDetails=bfdDetails;
            return this;
        }

        public Builder otpDetails(OtpDetails otpDetails){
            this.kycRequest.otpDetails=otpDetails;
            return this;
        }

        public Builder kycDetails(KycDetails kycDetails){
            this.kycRequest.kycDetails=kycDetails;
            return this;
        }


    }
}
