package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GstTransactionApiAveragesResponse {

    @JsonProperty("avginvcust")
    private String avginvcust;

    @JsonProperty("avgmonthtax")
    private String avgmonthtax;

    @JsonProperty("avgmonthval")
    private int avgmonthval;

    @JsonProperty("avgttltaxcust")
    private String avgttltaxcust;

    @JsonProperty("avgttltaxinv")
    private String avgttltaxinv;

    @JsonProperty("avgttlvalcust")
    private String avgttlvalcust;

    @JsonProperty("avgttlvalinv")
    private String avgttlvalinv;

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

}