package com.softcell.gonogo.model.core.cam;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RTRTransaction {

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("sMifinBankID")
    private String mifinBankCode;

    @JsonProperty("dLoanAmt")
    private double loanAmount;

    @JsonProperty("dOsAmount")
    private double osAmount;

    @JsonProperty("dEMI")
    private double emi;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("dtSanctionOrEmiStartDate")
    private Date sanctionOrEmiStartDate;

    @JsonProperty("sFO")
    private String fo;

    @JsonProperty("sTenure")
    private String tenure;

    @JsonProperty("dEmiPending")
    private double emiPending;

    @JsonProperty("dEmiPaid")
    private double emiPaid;

    @JsonProperty("sBounceMonth")
    private String bounceMonth;

    @JsonProperty("iNumberOfBounces")
    private int numberOfBounces;

    @JsonProperty("dtDisburseDate")
    private Date disburseDate;

    @JsonProperty("sDisbursalDate")
    private String disbursalDate;

    @JsonProperty("sEmiStartDate")
    private String emiStartDate;

}
