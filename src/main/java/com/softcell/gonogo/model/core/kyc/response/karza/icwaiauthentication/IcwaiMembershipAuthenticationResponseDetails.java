package com.softcell.gonogo.model.core.kyc.response.karza.icwaiauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class IcwaiMembershipAuthenticationResponseDetails {

    @JsonProperty("MemshipDt")
    private String MemshipDt;

    @JsonProperty("Chapter")
    private String Chapter;

    @JsonProperty("Retired")
    private String Retired;

    @JsonProperty("dob")
    private String dob;

    @JsonProperty("Mname")
    private String Mname;

    @JsonProperty("ProtFirmName")
    private String ProtFirmName;

    @JsonProperty("ValidUpDt")
    private String ValidUpDt;

    @JsonProperty("MemCategory")
    private String MemCategory;

    @JsonProperty("Fname")
    private String Fname;

    @JsonProperty("gender")
    private String gender;

    @JsonProperty("SrName")
    private String SrName;

    @JsonProperty("EffectiveDt")
    private String EffectiveDt;

    @JsonProperty("MemRegion")
    private String MemRegion;

    @JsonProperty("CrtEmployer")
    private String CrtEmployer;

    @JsonProperty("FirmEftDt")
    private String FirmEftDt;

    @JsonProperty("CancellationDt")
    private String CancellationDt;

}