package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class UidData {
	@XmlAttribute(name="uid")
	private String uid;
	@XmlElement(name="Poi")
	private Poi poi;
	@XmlElement(name="Poa")
	private Poa poa;
	@XmlElement(name="Pht")
	private String pht;
	@XmlElement(name="Prn")
	private Prn prn;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public Poi getPoi() {
		return poi;
	}
	public void setPoi(Poi poi) {
		this.poi = poi;
	}
	public Poa getPoa() {
		return poa;
	}
	public void setPoa(Poa poa) {
		this.poa = poa;
	}
	public String getPht() {
		return pht;
	}
	public void setPht(String pht) {
		this.pht = pht;
	}
	public Prn getPrn() {
		return prn;
	}
	public void setPrn(Prn prn) {
		this.prn = prn;
	}
	@Override
	public String toString() {
		return "UidData [uid=" + uid + ", poi=" + poi + ", poa=" + poa
				+ ", pht=" + pht + ", prn=" + prn + "]";
	}
	
	
		
	
}
