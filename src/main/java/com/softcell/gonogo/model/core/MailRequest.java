/**
 *
 */
package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

/**
 * @author yogeshb
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MailRequest {

    @JsonProperty("sInstId")
    private String institutionId;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("aTo")
    private String[] to;

    @JsonProperty("aCC")
    private String[] cc;

    @JsonProperty("sSub")
    private String subject;

    @JsonProperty("sText")
    private String content;

    @JsonProperty("bHtmlEnabled")
    private boolean htmlEnabled;

    @JsonProperty("aAttachment")
    private List<Attachment> attachments;

    /**
     * @return the to
     */
    public String[] getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String[] to) {
        this.to = to;
    }

    /**
     * @return the cc
     */
    public String[] getCc() {
        return cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(String[] cc) {
        this.cc = cc;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the attachments
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * @param attachments the attachments to set
     */
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    /**
     * @return the refID
     */
    public String getRefID() {
        return refID;
    }

    /**
     * @param refID the refID to set
     */
    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isHtmlEnabled() {
        return htmlEnabled;
    }

    public void setHtmlEnabled(boolean htmlEnabled) {
        this.htmlEnabled = htmlEnabled;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MailRequest{");
        sb.append("institutionId='").append(institutionId).append('\'');
        sb.append(", refID='").append(refID).append('\'');
        sb.append(", to=").append(Arrays.toString(to));
        sb.append(", cc=").append(Arrays.toString(cc));
        sb.append(", subject='").append(subject).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", htmlEnabled=").append(htmlEnabled);
        sb.append(", attachments=").append(attachments);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MailRequest that = (MailRequest) o;

        if (htmlEnabled != that.htmlEnabled) return false;
        if (institutionId != null ? !institutionId.equals(that.institutionId) : that.institutionId != null)
            return false;
        if (refID != null ? !refID.equals(that.refID) : that.refID != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(to, that.to)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(cc, that.cc)) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        return attachments != null ? attachments.equals(that.attachments) : that.attachments == null;
    }

    @Override
    public int hashCode() {
        int result = institutionId != null ? institutionId.hashCode() : 0;
        result = 31 * result + (refID != null ? refID.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(to);
        result = 31 * result + Arrays.hashCode(cc);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (htmlEnabled ? 1 : 0);
        result = 31 * result + (attachments != null ? attachments.hashCode() : 0);
        return result;
    }
}
