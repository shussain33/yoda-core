package com.softcell.gonogo.model.core.kyc.request.pan;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
public class KycRequest {

    @JsonProperty("PAN-DETAILS")
    private PanDetails panDetails;

    public PanDetails getPanDetails() {
        return panDetails;
    }

    public void setPanDetails(PanDetails panDetails) {
        this.panDetails = panDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KycRequest)) return false;
        KycRequest that = (KycRequest) o;
        return Objects.equal(getPanDetails(), that.getPanDetails());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPanDetails());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("KycRequest{");
        sb.append("panDetails=").append(panDetails);
        sb.append('}');
        return sb.toString();
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private KycRequest kycRequest= new KycRequest();

        public KycRequest build(){
            return  this.kycRequest;
        }

        public Builder panDetails(PanDetails panDetails){
            kycRequest.setPanDetails(panDetails);
            return this;
        }
    }
}
