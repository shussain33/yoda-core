package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EpfUanLookupResponseDetails {

    @JsonProperty("uan")
    private List<String> uan; // Need to verify

}