package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtpDetails {

    @JsonProperty("AADHAR-NUMBER")
    private String aadharNumber;

    @JsonProperty("TERMINAL-ID")
    private String terminalId;

    @JsonProperty("AUA-CODE")
    private String auaCode;

    @JsonProperty("SUB-AUA-CODE")
    private String subAuaCode;

    @JsonProperty("VERSION")
    private String version;

    @JsonProperty("TRANSACTION-IDENTIFIER")
    private String transactionIdentifier;

    @JsonProperty("LICENSE-KEY")
    private String licenseKey;

    @JsonProperty("SIGNATURE")
    private String signature;

    @JsonProperty("OTP-OPTIONS")
    private OtpOption otpOption;

    public static Builder builder() {
        return new Builder();
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getAuaCode() {
        return auaCode;
    }

    public void setAuaCode(String auaCode) {
        this.auaCode = auaCode;
    }

    public String getSubAuaCode() {
        return subAuaCode;
    }

    public void setSubAuaCode(String subAuaCode) {
        this.subAuaCode = subAuaCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public String getLicenseKey() {
        return licenseKey;
    }

    public void setLicenseKey(String licenseKey) {
        this.licenseKey = licenseKey;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public OtpOption getOtpOption() {
        return otpOption;
    }

    public void setOtpOption(OtpOption otpOption) {
        this.otpOption = otpOption;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OtpDetails{");
        sb.append("aadharNumber='").append(aadharNumber).append('\'');
        sb.append(", terminalId='").append(terminalId).append('\'');
        sb.append(", auaCode='").append(auaCode).append('\'');
        sb.append(", subAuaCode='").append(subAuaCode).append('\'');
        sb.append(", version='").append(version).append('\'');
        sb.append(", transactionIdentifier='").append(transactionIdentifier).append('\'');
        sb.append(", licenseKey='").append(licenseKey).append('\'');
        sb.append(", signature='").append(signature).append('\'');
        sb.append(", otpOption=").append(otpOption);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OtpDetails)) return false;
        OtpDetails that = (OtpDetails) o;
        return Objects.equal(getAadharNumber(), that.getAadharNumber()) &&
                Objects.equal(getTerminalId(), that.getTerminalId()) &&
                Objects.equal(getAuaCode(), that.getAuaCode()) &&
                Objects.equal(getSubAuaCode(), that.getSubAuaCode()) &&
                Objects.equal(getVersion(), that.getVersion()) &&
                Objects.equal(getTransactionIdentifier(), that.getTransactionIdentifier()) &&
                Objects.equal(getLicenseKey(), that.getLicenseKey()) &&
                Objects.equal(getSignature(), that.getSignature()) &&
                Objects.equal(getOtpOption(), that.getOtpOption());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getAadharNumber(), getTerminalId(), getAuaCode(), getSubAuaCode(), getVersion(), getTransactionIdentifier(), getLicenseKey(), getSignature(), getOtpOption());
    }

    public static class Builder {

        private OtpDetails otpDetails = new OtpDetails();

        public OtpDetails build() {
            return otpDetails;
        }

        public Builder aadharNumber(String aadharNumber) {
            this.otpDetails.aadharNumber = aadharNumber;
            return this;
        }

        public Builder terminalId(String terminalId) {
            this.otpDetails.terminalId = terminalId;
            return this;
        }

        public Builder auaCode(String auaCode) {
            this.otpDetails.auaCode = auaCode;
            return this;
        }

        public Builder subAuaCode(String subAuaCode) {
            this.otpDetails.subAuaCode = subAuaCode;
            return this;
        }

        public Builder version(String version) {
            this.otpDetails.version = version;
            return this;
        }

        public Builder transactionIdentifier(String transactionIdentifier) {
            this.otpDetails.transactionIdentifier = transactionIdentifier;
            return this;
        }

        public Builder licenseKey(String licenseKey) {
            this.otpDetails.licenseKey = licenseKey;
            return this;
        }

        public Builder signature(String signature) {
            this.otpDetails.signature = signature;
            return this;
        }

        public Builder otpOption(OtpOption otpOption) {
            this.otpDetails.otpOption = otpOption;
            return this;
        }
    }

}
