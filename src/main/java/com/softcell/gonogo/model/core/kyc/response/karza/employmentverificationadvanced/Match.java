package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Match {

    @JsonProperty("confidence")
    private double confidence;

    @JsonProperty("epfHistory")
    private Object epfHistory;

    @JsonProperty("estId")
    private String estId;

    @JsonProperty("name")
    private String name;
}
