package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg408 on 31/8/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Summary {

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("APPLICATION-DECISION")
    private String applicantDecesion;

    @JsonProperty("APPLICATION-APPROVED-AMOUNT")
    private String applicationAprvAmt;

}
