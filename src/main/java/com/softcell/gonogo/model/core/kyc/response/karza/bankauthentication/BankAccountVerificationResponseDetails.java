package com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class BankAccountVerificationResponseDetails {

    @JsonProperty("bankTxnStatus")
    private String bankTxnStatus;

    @JsonProperty("accountNumber")
    private String accountNumber;

    @JsonProperty("ifsc")
    private String ifsc;

    @JsonProperty("accountName")
    private String accountName;

    @JsonProperty("bankResponse")
    private String bankResponse;

}