package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by yogesh on 9/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BankingTransaction {

    @JsonProperty("sDate")
    private String date;

    @JsonProperty("aBalanceInPeriod")
    private List<AmountForDate> inMonthBalance;

    @JsonProperty("dAbbAmount")
    private double abbAmount;

    @JsonProperty("aBusinessDebit")
    private List<Double> businessDebit;

    @JsonProperty("aBusinessCredit")
    private List<Double> businessCredit;

    @JsonProperty("dInterestDebited")
    private double interestDebited;

    @JsonProperty("dChqAndEmiReturnsInward")
    private double chqAndEmiReturnsInward;

    @JsonProperty("dChqAndEmiReturnsOutward")
    private double chqAndEmiReturnsOutward;

    @JsonProperty("dGst")
    private double gst;

    @JsonProperty("dIntColl")
    private double intColl;

    @JsonProperty("iRank")
    private int rank;

    @JsonProperty("dCashCredit")
    private double cashCredit;

    @JsonProperty("dOtherCredit")
    private double otherCredit;

    @JsonProperty("dCreditTotal")
    private double creditTotal;

    @JsonProperty("dCCUtilisation")
    private double ccUtilisation;

    @JsonProperty("dNofIW")
    private double numberOfIW;

    @JsonProperty("dNofOW")
    private double numberOfOW;

    @JsonProperty("sMifinAccDtlId")
    private String mifinAccountDetailsId;

    public static final Comparator<BankingTransaction> AbbComparator = new Comparator<BankingTransaction>(){

        @Override
        public int compare(BankingTransaction o1, BankingTransaction o2) {
            return (int) (o1.abbAmount-o2.abbAmount);
        }

    };

}

