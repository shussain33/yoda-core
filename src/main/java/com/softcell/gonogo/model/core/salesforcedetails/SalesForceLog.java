package com.softcell.gonogo.model.core.salesforcedetails;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by dev-intern on 21/7/17.
 */
@Document(collection = "salesForceLog")
public class SalesForceLog {
    @Id
    @JsonProperty("sRefId")
    @NotEmpty(groups = {SalesForceLog.FetchGrp.class})
    private String refId;
    @JsonProperty("sLeadId")
    private  String leadId;
    @JsonProperty("oHeader")
    private Header header;
    @JsonProperty("aSuccess")
    private List<Success> success;
    @JsonProperty("aError")
    private List<Errors> error;

    public String getRefId() {
        return refId;
    }
    public void setRefId(String refId) {
        this.refId = refId;
    }
    public Header getHeader() {
        return header;
    }
    public void setHeader(Header header) {
        this.header = header;
    }
    public String getLeadId() {
        return leadId;
    }
    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }
    public List<Success> getSuccess() {
        return success;
    }
    public void setSuccess(List<Success> success) {
        this.success = success;
    }
    public List<Errors> getError() {
        return error;
    }
    public void setError(List<Errors> error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "SalesForceLog{" +
                "refId='" + refId + '\'' +
                ", leadId='" + leadId + '\'' +
                ", header=" + header +
                ", success=" + success +
                ", error=" + error +
                '}';
    }
    public interface FetchGrp {
    }
}
