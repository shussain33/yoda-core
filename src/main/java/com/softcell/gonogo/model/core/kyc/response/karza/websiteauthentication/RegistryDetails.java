package com.softcell.gonogo.model.core.kyc.response.karza.websiteauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class RegistryDetails {

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("expiry")
    private String expiry;

    @JsonProperty("id")
    private String id;

    @JsonProperty("email")
    private String email;

}