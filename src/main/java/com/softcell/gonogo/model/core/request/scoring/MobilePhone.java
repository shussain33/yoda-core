package com.softcell.gonogo.model.core.request.scoring;


public class MobilePhone {

    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "MobilePhone [phoneNumber=" + phoneNumber + "]";
    }
}
