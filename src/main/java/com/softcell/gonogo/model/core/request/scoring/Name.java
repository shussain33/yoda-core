package com.softcell.gonogo.model.core.request.scoring;

public class Name {

    private String suffix;
    private String fullName;
    private String firstName;
    private String middleName;
    private String lastName;
    private String additionalMiddleName;

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAdditionalMiddleName() {
        return additionalMiddleName;
    }

    public void setAdditionalMiddleName(String additionalMiddleName) {
        this.additionalMiddleName = additionalMiddleName;
    }

    @Override
    public String toString() {
        return "Name [suffix=" + suffix + ", fullName=" + fullName
                + ", firstName=" + firstName + ", middleName=" + middleName
                + ", lastName=" + lastName + ", additionalMiddleName="
                + additionalMiddleName + "]";
    }
}
