package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GstTransactionApiBusinessSummryResponse {

    @JsonProperty("balance_payable")
    private Double balance_payable;

    @JsonProperty("gst_turnover_cy_inv_val")
    private Double gst_turnover_cy_inv_val;

    @JsonProperty("gst_turnover_cy_tax_val")
    private Double gst_turnover_cy_tax_val;

    @JsonProperty("liability_paid")
    private Double liability_paid;

    @JsonProperty("liability_payable")
    private Double liability_payable;

    @JsonProperty("net_turnover_ly")
    private Double net_turnover_ly;

    @JsonProperty("total_invoices")
    private Double total_invoices;

}
