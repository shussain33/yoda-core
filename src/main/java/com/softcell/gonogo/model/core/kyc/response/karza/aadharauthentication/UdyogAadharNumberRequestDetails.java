package com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UdyogAadharNumberRequestDetails {

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("aadhar")
    private String aadhar;

    @JsonProperty("uan")
    private String uan;

}