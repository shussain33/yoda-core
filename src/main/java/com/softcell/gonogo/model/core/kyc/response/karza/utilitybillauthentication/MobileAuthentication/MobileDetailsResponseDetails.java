package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MobileDetailsResponseDetails {

    @JsonProperty("contact")
    public ContactDetails contactDetails;

    @JsonProperty("device")
    public DeviceDetails deviceDetails;

    @JsonProperty("history")
    public List<HistoryDetails> historyDetailsList;

    @JsonProperty("profile")
    public ProfileDetails profileDetails;

    @JsonProperty("sim_details")
    public SimDetails simDetails;

    @JsonProperty("identity")
    public IdentityDetails identityDetails;

}