package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ssg222 on 27/12/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
public class CoOriginationDetails {

    @JsonProperty("dApploan")
    private double appLoan;

    @JsonProperty("dEmi")
    private double Emi;

    @JsonProperty("dPF")
    private double PF;

    @JsonProperty("dInsurance")
    private double insurance;

    @JsonProperty("dRoi")
    private double ROI;

    @JsonProperty("dCreditVidya")
    private double creditVidya;

    @JsonProperty("dCersaiFeeWithGST")
    private double cersaiFeesWithGST;

    @JsonProperty("dPreEmi")
    private double preEmi;

    @JsonProperty("dGstCharges")
    private double gstCharges;

    @JsonProperty("dImdAmount")
    private double imdAmount;

    @JsonProperty("dImdAmount2")
    private double imdAmount2;

    @JsonProperty("dIMD2GstAmt")
    private double imd2gstAmt;

    @JsonProperty("dNetDisbursal")
    private double netDisbural;

    @JsonProperty("dExternalPercentage")
    private double externalPercentage;

    @JsonProperty("dInternalPercentage")
    private double internalPercentage;

    @JsonProperty("sCompanyName")
    private String companyName;

    @JsonProperty("sScheme")
    private String scheme;

}
