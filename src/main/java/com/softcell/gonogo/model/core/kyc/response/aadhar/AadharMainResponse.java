package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author yogeshb
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AadharMainResponse {

    @JsonProperty("dtResponse")
    private Date dateTime = new Date();

    @JsonProperty("HEADER")
    private AadharResponseHeader header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private String acknowledgementId;

    @JsonProperty("TXN-STATUS")
    private String txnStatus;

    @JsonProperty("KYC-RESPONSE")
    private KycResponse kycResponse;

    @JsonProperty("RESPONSE-XML")
    private String responseXMl;

    @JsonProperty("UIDAI-KYC-RESPONSE")
    private String rawXmlResponse;

    @JsonProperty("ERRORS")
    private List<Errors> errors;


}
