package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GstPradr {

    @JsonProperty("ntr")
    private String ntr;

    @JsonProperty("addr")
    public Object gstAddr;

    @JsonProperty("em")
    private String em;

    @JsonProperty("adr")
    private String adr;

    @JsonProperty("mb")
    private String mb;

    @JsonProperty("lastUpdatedDate")
    private String lastUpdatedDate;

}