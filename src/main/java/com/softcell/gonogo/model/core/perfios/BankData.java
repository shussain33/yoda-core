package com.softcell.gonogo.model.core.perfios;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


import com.softcell.ssl2.perfios.PerfiosResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.Map;

/**
 * Created by ssg0268 on 9/4/19.
 *
 *
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BankData {

    @JsonProperty("sAppName")
    private String applIcantName;

    @JsonProperty("sAppId")
    private String appId;

    @JsonProperty("callDate")
    private Date callDate = new Date();


    @JsonProperty("sAckId")
    private String acknowledgeId;

   /* @JsonProperty("oData")
    private Map<String, Object> data;*/

    @JsonProperty("oData")
    private Object data;

    @JsonProperty("oGenerateFile")
    private Map<String, Object> generateFileResponse;

    @JsonProperty("dtResponseReciveDate")
    private Date responseReciveDate = new Date();

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("dtFromDate")
    private Date fromDate = new Date();

    @JsonProperty("dtToDate")
    private Date toDate = new Date();

}
