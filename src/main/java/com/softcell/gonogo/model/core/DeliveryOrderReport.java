/**
 * yogeshb3:36:35 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.softcell.constants.Product;
import lombok.Data;

import java.util.Date;

/**
 * @author yogeshb
 *
 */
@Data
public class DeliveryOrderReport {
    private String refID;
    private Product product;
    private String dealerName;
    private String assetModelName;
    private String customerName; //customer Name Application Request pojo
    private String mobileNumber;
    private String customerAddress;//customer Address Application Request pojo
    private String productMake;
    private String productModel;
    private String productManufacturer;
    private String productCategory;
    private double emi;
    private String productBrand;
    private String schemeCode;//scheme PostIPA pojo
    private String schemeType;
    private String schemeDscr;
    private double productCost; //later
    private double processingFees; //processingFees PostIPA pojo
    private double marginMoney; //marginMoney Amount PostIPA pojo
    private double advanceEMI; //advanceEmi Amount PostIPA pojo
    private double financeAmount;//(Product Cost - Margin Money)
    private double dealerSubvention;
    private double deductionsByHDBFS;
    private double deductionByTvsc;
    private double manufSubBorneByDealer;
    private double finalDisbursementAmount;//Approved Amount PostIPA pojo
    private double otherChargesIfAny;
    private double manufSubventionMbd;
    private double sumOfDeductions;//(Processing Fees + Advance EMI + Dealer Subvention + Manufacturer Subvention borne by Dealer + Other Charges if any + manufSubventionMbd)
    private double sumOfDeductionForTvsc;
    private double totalAmtCollectedFromCustomer;
    private double netFundingAmount;
    private String instID;//added for Hdbfs logo purpose
    private String imeiNumber;
    private Date deliveryOrderDate;
    private String logoByteCode;
    private double sumExtWrntAmtInsuranceAmt;
}
