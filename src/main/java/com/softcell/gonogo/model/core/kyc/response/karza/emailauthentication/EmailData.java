package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailData {

    @JsonProperty("disposable")
    private Boolean disposable;

    @JsonProperty("email")
    private String email;

    @JsonProperty("regexp")
    private Boolean regexp;

    @JsonProperty("result")
    private String result;

    @JsonProperty("webmail")
    private Boolean webmail;

    @JsonProperty("accept_all")
    private Boolean accept_all;

    @JsonProperty("acceptAll")
    private Boolean acceptAll;

    @JsonProperty("generic_email")
    private Boolean generic_email;

    @JsonProperty("genericEmail")
    private Boolean genericEmail;

    @JsonProperty("mx_records")
    private Boolean mx_records;

    @JsonProperty("mxRecords")
    private Boolean mxRecords;

    @JsonProperty("smtp_check")
    private Boolean smtp_check;

    @JsonProperty("smtpCheck")
    private Boolean smtpCheck;
}
