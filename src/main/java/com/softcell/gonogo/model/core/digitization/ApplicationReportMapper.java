package com.softcell.gonogo.model.core.digitization;

import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by adesh on 18/1/19.
 */
public class ApplicationReportMapper {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationReportMapper.class);

    public static VelocityContext getSanctionReportMap(SanctionReport sanctionReport, boolean entireForm) {
        VelocityContext context = new VelocityContext();
        context.put("form", sanctionReport);
        return context;
    }
}
