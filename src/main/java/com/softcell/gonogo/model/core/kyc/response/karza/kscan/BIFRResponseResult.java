package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class BIFRResponseResult {

    @JsonProperty("status")
    private String status;

    @JsonProperty("entityName")
    private String entityName;

    @JsonProperty("matchConfidenceProbability")
    private String matchConfidenceProbability;

    @JsonProperty("caseNo")
    private String caseNo;

    @JsonProperty("address")
    private String address;

    @JsonProperty("lastOrderDate")
    private String lastOrderDate;

}