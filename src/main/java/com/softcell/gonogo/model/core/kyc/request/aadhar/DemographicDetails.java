package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DemographicDetails {

    @JsonProperty("LANG")
    private String lang;

    @JsonProperty("PERSONAL-IDENTITY")
    private PersonalIdentity personalIdentity;

    @JsonProperty("PERSONAL-ADDRESS")
    private PersonalAddress personalAddress;

    @JsonProperty("PERSONAL-FULL-ADDRESS")
    private PersonalFullAddress personalFullAddress;

    public static Builder builder() {
        return new Builder();
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public PersonalIdentity getPersonalIdentity() {
        return personalIdentity;
    }

    public void setPersonalIdentity(PersonalIdentity personalIdentity) {
        this.personalIdentity = personalIdentity;
    }

    public PersonalAddress getPersonalAddress() {
        return personalAddress;
    }

    public void setPersonalAddress(PersonalAddress personalAddress) {
        this.personalAddress = personalAddress;
    }

    public PersonalFullAddress getPersonalFullAddress() {
        return personalFullAddress;
    }

    public void setPersonalFullAddress(PersonalFullAddress personalFullAddress) {
        this.personalFullAddress = personalFullAddress;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DemographicDetails{");
        sb.append("lang='").append(lang).append('\'');
        sb.append(", personalIdentity=").append(personalIdentity);
        sb.append(", personalAddress=").append(personalAddress);
        sb.append(", personalFullAddress=").append(personalFullAddress);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DemographicDetails)) return false;
        DemographicDetails that = (DemographicDetails) o;
        return Objects.equal(getLang(), that.getLang()) &&
                Objects.equal(getPersonalIdentity(), that.getPersonalIdentity()) &&
                Objects.equal(getPersonalAddress(), that.getPersonalAddress()) &&
                Objects.equal(getPersonalFullAddress(), that.getPersonalFullAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getLang(), getPersonalIdentity(), getPersonalAddress(), getPersonalFullAddress());
    }

    public static class Builder {

        private DemographicDetails demographicDetails = new DemographicDetails();

        public DemographicDetails build() {
            return demographicDetails;
        }

        public Builder lang(String lang) {
            this.demographicDetails.lang = lang;
            return this;
        }

        public Builder personalIdentity(PersonalIdentity personalIdentity) {
            this.demographicDetails.personalIdentity = personalIdentity;
            return this;
        }

        public Builder personalAddress(PersonalAddress personalAddress) {
            this.demographicDetails.personalAddress = personalAddress;
            return this;
        }

        public Builder personalFullAddress(PersonalFullAddress personalFullAddress) {
            this.demographicDetails.personalFullAddress = personalFullAddress;
            return this;
        }
    }
}
