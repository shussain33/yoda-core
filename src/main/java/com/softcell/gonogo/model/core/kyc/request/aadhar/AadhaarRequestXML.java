package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.softcell.gonogo.model.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shyamk
 *         This model is used for creation of aadhaar xml
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AadhaarRequestXML extends AuditEntity {

    String uid;
    String tid;
    String ac;
    String sa;
    String ver;
    String txn;
    String lk;
    MetadataForDeviceAndTxn meta;
    ResidentToken tkn;
    SKey skey;
    RequestUses uses;
    UidData data;
    String hmac;
    String signature;
    String pfr;
}
