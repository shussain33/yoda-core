package com.softcell.gonogo.model.core.kyc.response.pan;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.Objects;

/**
 * @author yogeshb
 */
public class PanResponse {

    @JsonProperty("HEADER")
    private ResponseHeader header;

    @JsonProperty("ACKNOWLEDGEMENT-ID")
    private String acknowledgementId;

    @JsonProperty("TXN-STATUS")
    private String txnStatus;

    @JsonProperty("ERRORS")
    private List<Errors> errorList;

    @JsonProperty("KYC-RESPONSE")
    private KycResponse kycResponse;

    public ResponseHeader getHeader() {
        return header;
    }

    public void setHeader(ResponseHeader header) {
        this.header = header;
    }

    public String getAcknowledgementId() {
        return acknowledgementId;
    }

    public void setAcknowledgementId(String acknowledgementId) {
        this.acknowledgementId = acknowledgementId;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public List<Errors> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Errors> errorList) {
        this.errorList = errorList;
    }

    public KycResponse getKycResponse() {
        return kycResponse;
    }

    public void setKycResponse(KycResponse kycResponse) {
        this.kycResponse = kycResponse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PanResponse)) return false;
        PanResponse that = (PanResponse) o;
        return Objects.equals(header, that.header) &&
                Objects.equals(acknowledgementId, that.acknowledgementId) &&
                Objects.equals(txnStatus, that.txnStatus) &&
                Objects.equals(errorList, that.errorList) &&
                Objects.equals(kycResponse, that.kycResponse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, acknowledgementId, txnStatus, errorList, kycResponse);
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("header", header)
                .append("acknowledgementId", acknowledgementId)
                .append("txnStatus", txnStatus)
                .append("errorList", errorList)
                .append("kycResponse", kycResponse)
                .toString();
    }
}
