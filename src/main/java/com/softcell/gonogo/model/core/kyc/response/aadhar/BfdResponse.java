package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author yogeshb
 */
public class BfdResponse {

    @JsonProperty("code")
    private String code;

    @JsonProperty("transaction-identifier")
    private String transactionIdentifier;

    @JsonProperty("ts")
    private String ts;

    @JsonProperty("actn")
    private String actn;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("ranks")
    private List<Rank> ranks;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getActn() {
        return actn;
    }

    public void setActn(String actn) {
        this.actn = actn;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Rank> getRanks() {
        return ranks;
    }

    public void setRanks(List<Rank> ranks) {
        this.ranks = ranks;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BfdResponse [code=");
        builder.append(code);
        builder.append(", transactionIdentifier=");
        builder.append(transactionIdentifier);
        builder.append(", ts=");
        builder.append(ts);
        builder.append(", actn=");
        builder.append(actn);
        builder.append(", msg=");
        builder.append(msg);
        builder.append(", ranks=");
        builder.append(ranks);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((actn == null) ? 0 : actn.hashCode());
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((msg == null) ? 0 : msg.hashCode());
        result = prime * result + ((ranks == null) ? 0 : ranks.hashCode());
        result = prime
                * result
                + ((transactionIdentifier == null) ? 0 : transactionIdentifier
                .hashCode());
        result = prime * result + ((ts == null) ? 0 : ts.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BfdResponse other = (BfdResponse) obj;
        if (actn == null) {
            if (other.actn != null)
                return false;
        } else if (!actn.equals(other.actn))
            return false;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        if (msg == null) {
            if (other.msg != null)
                return false;
        } else if (!msg.equals(other.msg))
            return false;
        if (ranks == null) {
            if (other.ranks != null)
                return false;
        } else if (!ranks.equals(other.ranks))
            return false;
        if (transactionIdentifier == null) {
            if (other.transactionIdentifier != null)
                return false;
        } else if (!transactionIdentifier.equals(other.transactionIdentifier))
            return false;
        if (ts == null) {
            if (other.ts != null)
                return false;
        } else if (!ts.equals(other.ts))
            return false;
        return true;
    }

}
