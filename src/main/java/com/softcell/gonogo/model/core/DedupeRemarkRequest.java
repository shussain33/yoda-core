package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.request.DeviationRequest;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Created by yogesh on 28/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DedupeRemarkRequest {

    @JsonProperty("oHeader")
    @NotEmpty(groups = {Header.InsertGroup.class})
    public Header header;

    @JsonProperty("sRefID")
    public String refId;

    @JsonProperty("oDedupeMatch")
    private DedupeMatch dedupeMatch;

}
