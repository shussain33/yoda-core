package com.softcell.gonogo.model.core.salesforcedetails;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by dev-intern on 21/7/17.
 */
public class Success {
    @JsonProperty("sSuccessMessage")
    private String successMessage;
    @JsonProperty("sObjType")
    private String objType;
    @JsonProperty("sObjId")
    private String objId;

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId;
    }

    @Override
    public String toString() {
        return "Success{" +
                "successMessage='" + successMessage + '\'' +
                ", objType='" + objType + '\'' +
                ", objId='" + objId + '\'' +
                '}';
    }
}
