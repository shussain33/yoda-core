package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by amit on 15/2/18.
 */
public class AllocationInfo {

    // Is a userId who is currently working on the case
    private String operator;

    private String role;

    // operator had manually locked case or not(made non editable for others) for his working
    @JsonProperty("bManuallyLocked")
    private boolean locked;

    @JsonProperty("sAssignedBy")
    private String assignedBy;

    public AllocationInfo() {
        locked = false;
    }

    public String getOperator() {        return operator;    }

    public void setOperator(String operator) {        this.operator = operator;    }

    public String getRole() {        return role;    }

    public void setRole(String role) {        this.role = role;    }

    public boolean isLocked() {        return locked;    }

    public void setLocked(boolean locked) {        this.locked = locked;    }

    public String getAssignedBy() {        return assignedBy;    }

    public void setAssignedBy(String assignedBy) {        this.assignedBy = assignedBy;    }
}
