package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonalFullAddress {

    @JsonProperty("MATCH-STRATEGY")
    private String matchStrategy;

    @JsonProperty("MATCH-VALUE")
    private String matchValue;

    @JsonProperty("ADDRESS-VALUE")
    private String addressValue;

    @JsonProperty("LAV")
    private String lav;

    @JsonProperty("LMV")
    private String lmv;

    public static Builder builder() {
        return new Builder();
    }

    public String getMatchStrategy() {
        return matchStrategy;
    }

    public void setMatchStrategy(String matchStrategy) {
        this.matchStrategy = matchStrategy;
    }

    public String getMatchValue() {
        return matchValue;
    }

    public void setMatchValue(String matchValue) {
        this.matchValue = matchValue;
    }

    public String getAddressValue() {
        return addressValue;
    }

    public void setAddressValue(String addressValue) {
        this.addressValue = addressValue;
    }

    public String getLav() {
        return lav;
    }

    public void setLav(String lav) {
        this.lav = lav;
    }

    public String getLmv() {
        return lmv;
    }

    public void setLmv(String lmv) {
        this.lmv = lmv;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PersonalFullAddress{");
        sb.append("matchStrategy='").append(matchStrategy).append('\'');
        sb.append(", matchValue='").append(matchValue).append('\'');
        sb.append(", addressValue='").append(addressValue).append('\'');
        sb.append(", lav='").append(lav).append('\'');
        sb.append(", lmv='").append(lmv).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonalFullAddress)) return false;
        PersonalFullAddress that = (PersonalFullAddress) o;
        return Objects.equal(getMatchStrategy(), that.getMatchStrategy()) &&
                Objects.equal(getMatchValue(), that.getMatchValue()) &&
                Objects.equal(getAddressValue(), that.getAddressValue()) &&
                Objects.equal(getLav(), that.getLav()) &&
                Objects.equal(getLmv(), that.getLmv());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getMatchStrategy(), getMatchValue(), getAddressValue(), getLav(), getLmv());
    }

    public static class Builder {

        private PersonalFullAddress personalFullAddress = new PersonalFullAddress();

        public PersonalFullAddress build() {
            return personalFullAddress;
        }

        public Builder matchStrategy(String matchStrategy) {
            this.personalFullAddress.matchStrategy = matchStrategy;
            return this;
        }

        public Builder matchValue(String matchValue) {
            this.personalFullAddress.matchValue = matchValue;
            return this;
        }

        public Builder addressValue(String addressValue) {
            this.personalFullAddress.addressValue = addressValue;
            return this;
        }

        public Builder lav(String lav) {
            this.personalFullAddress.lav = lav;
            return this;
        }

        public Builder lmv(String lmv) {
            this.personalFullAddress.lmv = lmv;
            return this;
        }
    }
}
