/**
 * kishorp6:18:08 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core;

import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author kishorp
 *
 */
@Document(collection = "croDsaMaster")
public class CroDsaMaster extends AuditEntity {
    private String institutionId;
    private String croId;
    private String dsaId;
    private boolean active;
    private Date insertDate = new Date();
    ;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getCroId() {
        return croId;
    }

    public void setCroId(String croId) {
        this.croId = croId;
    }

    public String getDsaId() {
        return dsaId;
    }

    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    @Override
    public String toString() {
        return "CroDsaMaster [institutionId=" + institutionId + ", croId=" + croId
                + ", dsaId=" + dsaId + ", active=" + active + ", insertDate="
                + insertDate + "]";
    }


}
