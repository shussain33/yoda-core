package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class CanonicalizationMethod {

	/**
	 * @author Deepak Bhargava
	 *
	 *
	 */
	@XmlAttribute(name="Algorithm")
	private String algo;

	public String getAlgo() {
		return algo;
	}

	public void setAlgo(String algo) {
		this.algo = algo;
	}

	@Override
	public String toString() {
		return "CanonicalizationMethod [algo=" + algo + "]";
	}
	
	
}
