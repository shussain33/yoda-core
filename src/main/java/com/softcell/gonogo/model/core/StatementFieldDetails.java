package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 10/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StatementFieldDetails {

    @JsonProperty("dPreviousYear")
    private double previousYear;

    @JsonProperty("dCurrentYear")
    private double currentYear;

    /**
     * adding list for value more than one year
     */
    @JsonProperty("aYears")
    private List<YearValue> years;

    @JsonProperty("dChangeInAmount")
    private double changeInAmount;

    @JsonProperty("dChangeInPercentage")
    private double changeInPercentage;
}
