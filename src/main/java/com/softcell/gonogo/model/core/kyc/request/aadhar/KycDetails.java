package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KycDetails {
    @JsonProperty("ver")
    private String ver;

    @JsonProperty("ts")
    private String ts;

    @JsonProperty("ra")
    private String ra;

    @JsonProperty("rc")
    private String rc;

    @JsonProperty("RAD")
    private String rad;

    public static Builder builder() {
        return new Builder();
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getRa() {
        return ra;
    }

    public void setRa(String ra) {
        this.ra = ra;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getRad() {
        return rad;
    }

    public void setRad(String rad) {
        this.rad = rad;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("KycDetails{");
        sb.append("ver='").append(ver).append('\'');
        sb.append(", ts='").append(ts).append('\'');
        sb.append(", ra='").append(ra).append('\'');
        sb.append(", rc='").append(rc).append('\'');
        sb.append(", rad='").append(rad).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KycDetails)) return false;
        KycDetails that = (KycDetails) o;
        return Objects.equal(getVer(), that.getVer()) &&
                Objects.equal(getTs(), that.getTs()) &&
                Objects.equal(getRa(), that.getRa()) &&
                Objects.equal(getRc(), that.getRc()) &&
                Objects.equal(getRad(), that.getRad());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getVer(), getTs(), getRa(), getRc(), getRad());
    }

    public static class Builder {
        private KycDetails kycDetails = new KycDetails();

        public KycDetails build() {
            return kycDetails;
        }

        public Builder ver(String ver) {
            this.kycDetails.ver = ver;
            return this;
        }

        public Builder ts(String ts) {
            this.kycDetails.ts = ts;
            return this;
        }

        public Builder ra(String ra) {
            this.kycDetails.ra = ra;
            return this;
        }

        public Builder rc(String rc) {
            this.kycDetails.rc = rc;
            return this;
        }

        public Builder rad(String rad) {
            this.kycDetails.rad = rad;
            return this;
        }


    }
}
