package com.softcell.gonogo.model.core.scoring.response;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {
    @JsonProperty("ERROR")
    private String errorMsg;

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "Error [errorMsg=" + errorMsg + "]";
    }

}
