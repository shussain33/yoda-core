package com.softcell.gonogo.model.core;

import java.util.*;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonProperty;
/**

 * Created by Amit on 23/2/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EarningMember{

	@JsonProperty("oName")
	private Name name;

	@JsonProperty("sRelnship")
	private String relationship;

	@JsonProperty("bEligibleIncome")
	private boolean eligibleincome;

	@JsonProperty("iDependents")
	private int dependents;

	@JsonProperty("dIncome")
	private double income;

	@JsonProperty("iAge")
	private int age;

	@JsonProperty("sSchoolName")
	private String schoolname;

	@JsonProperty("dSchoolFees")
	private double schoolfees;

}
