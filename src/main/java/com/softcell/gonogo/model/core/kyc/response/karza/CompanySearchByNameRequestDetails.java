package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanySearchByNameRequestDetails {

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("company_name")
    private String company_name;

}