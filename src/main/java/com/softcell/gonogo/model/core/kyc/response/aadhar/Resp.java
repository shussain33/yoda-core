package com.softcell.gonogo.model.core.kyc.response.aadhar;

import javax.xml.bind.annotation.*;

@XmlRootElement(name="Resp")
@XmlAccessorType(XmlAccessType.FIELD)
public class Resp {
	
	@XmlAttribute(name = "status")
	private String status;
	@XmlElement(name = "kycRes")
	private String kycResponse;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKycResponse() {
		return kycResponse;
	}
	
	public void setKycResponse(String kycResponse) {
		this.kycResponse = kycResponse;
	}
	@Override
	public String toString() {
		return "Resp [status=" + status + ", kycResponse=" + kycResponse + "]";
	}
	
	
	
}
