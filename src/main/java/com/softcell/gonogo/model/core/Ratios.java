package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by yogesh on 12/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Ratios {

    @JsonProperty("sApplicantId")
    private String applicantId;

    @JsonProperty("eRatioName")
    private RatioName ratioName;

    @JsonProperty("aRatioFieldDetails")
    List<RatioFieldDetail> ratioFieldDetails;
}
    /*
    Ratio Analysis: Leverage Ratios
     *//*
    @JsonProperty("oInterestCoverageRatio")
    private StatementFieldDetails interestCoverageRatio;
    @JsonProperty("oDebtToEquity")
    private StatementFieldDetails debtToEquity; //(Total Liability to Net Worth)
    *//*
    * Ratio Analysis: Profitability
    *//*
    @JsonProperty("oGrossMargins")
    private StatementFieldDetails grossMargins;
    @JsonProperty("oOperatingMargins")
    private StatementFieldDetails operatingMargins;
    @JsonProperty("oNetProfitMargins")
    private StatementFieldDetails netProfitMargins;
    *//*
    * Ratio Analysis: Working Capital
    *//*
    @JsonProperty("oDebtorCollectionPeriod")
    private StatementFieldDetails debtorCollectionPeriod;
    @JsonProperty("oCreditorPaymentPeriod")
    private StatementFieldDetails creditorPaymentPeriod;
    *//*
    * Ratio Analysis:  Turnover Ratios
    *//*
    @JsonProperty("oCurrentAssetsTurnover")
    private StatementFieldDetails currentAssetsTurnover;
    @JsonProperty("oFixedAssetTurnover")
    private StatementFieldDetails fixedAssetTurnover;
    @JsonProperty("oTotalAssetTurnover")
    private StatementFieldDetails totalAssetTurnover;
    *//*
    Ratio Analysis:  Liquidity
     *//*
    @JsonProperty("oCurrentRatio")
    private StatementFieldDetails currentRatio;
    @JsonProperty("oQuickRatio")
    private StatementFieldDetails quickRatio;
    @JsonProperty("oCashRatio")
    private StatementFieldDetails cashRatio;
    *//*
    Leverage Ratios
    *//*
    @JsonProperty("oDebtToAssetsRatio")
    private StatementFieldDetails debtToAssetsRatio;
    @JsonProperty("oLongTermDebtToEquity")
    private StatementFieldDetails longTermDebtToEquity;
    *//*
    Profitability Ratios
    *//*
    @JsonProperty("oReturnOnTotalAssets")
    private StatementFieldDetails returnOnTotalAssets;
    @JsonProperty("oReturnOnEquity")
    private StatementFieldDetails returnOnEquity;

    //additional fields
    @JsonProperty("oDaysInventoryOutstanding")
    private StatementFieldDetails daysInventoryOutstanding;
    @JsonProperty("oCashConversionCycle")
    private StatementFieldDetails cashConversionCycle;
    @JsonProperty("oCashCreditDays")
    private StatementFieldDetails cashCreditDays;
*/
