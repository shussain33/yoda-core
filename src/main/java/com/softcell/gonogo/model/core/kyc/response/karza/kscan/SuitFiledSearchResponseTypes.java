package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data

    public class SuitFiledSearchResponseTypes {


        @JsonProperty("group25Lac")
        private boolean group25Lac;

        @JsonProperty("group100Lac")
        private boolean group100Lac;

    }