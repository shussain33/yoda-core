package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;

import java.util.List;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KycResponseDetails {
    @JsonProperty("UIDAI-STATUS")
    private String uidaiStatus;

    @JsonProperty("KYC-DATA")
    private KycResponseInner kycResponse;

    @JsonProperty("ERRORS")
    private List<Errors> errors;

    public String getUidaiStatus() {
        return uidaiStatus;
    }

    public void setUidaiStatus(String uidaiStatus) {
        this.uidaiStatus = uidaiStatus;
    }

    public KycResponseInner getKycResponse() {
        return kycResponse;
    }

    public void setKycResponse(KycResponseInner kycResponse) {
        this.kycResponse = kycResponse;
    }

    public List<Errors> getErrors() {
        return errors;
    }

    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "KycResponseDetails [uidaiStatus=" + uidaiStatus
                + ", kycResponse=" + kycResponse + ", errors=" + errors + "]";
    }


}
