package com.softcell.gonogo.model.core.perfios;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.ssl2.perfios.PerfiosResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * Created by ssg0268 on 9/4/19.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RetriveFileData {


    @JsonProperty("sAcknowledgementId")
    private String acknowledgeId;

    @JsonProperty("oData")
    private Object data;

    @JsonProperty("sReportType")
    private String reportType;

    @JsonProperty("sStatus")
    private String status;
}
