package com.softcell.gonogo.model.core.kyc.response.karza.vehicleauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class VehicleRCAuthenticationResponseDetails {

    @JsonProperty("rc_eng_no")
    private String rc_eng_no;

    @JsonProperty("rc_vh_class_desc")
    private String rc_vh_class_desc;

    @JsonProperty("rc_present_address")
    private String rc_present_address;

    @JsonProperty("rc_registered_at")
    private String rc_registered_at;

    @JsonProperty("rc_regn_dt")
    private String rc_regn_dt;

    @JsonProperty("rc_insurance_comp")
    private String rc_insurance_comp;

    @JsonProperty("rc_color")
    private String rc_color;

    @JsonProperty("rc_manu_month_yr")
    private String rc_manu_month_yr;

    @JsonProperty("rc_sleeper_cap")
    private String rc_sleeper_cap;

    @JsonProperty("rc_maker_desc")
    private String rc_maker_desc;

    @JsonProperty("rc_status_as_on")
    private String rc_status_as_on;

    @JsonProperty("rc_insurance_upto")
    private String rc_insurance_upto;

    @JsonProperty("rc_cubic_cap")
    private String rc_cubic_cap;

    @JsonProperty("rc_owner_sr")
    private String rc_owner_sr;

    @JsonProperty("rc_permanent_address")
    private String rc_permanent_address;

    @JsonProperty("rc_financer")
    private String rc_financer;

    @JsonProperty("rc_owner_name")
    private String rc_owner_name;

    @JsonProperty("rc_f_name")
    private String rc_f_name;

    @JsonProperty("rc_unld_wt")
    private String rc_unld_wt;

    @JsonProperty("rc_chasi_no")
    private String rc_chasi_no;

    @JsonProperty("rc_maker_model")
    private String rc_maker_model;

    @JsonProperty("rc_gvw")
    private String rc_gvw;

    @JsonProperty("rc_tax_upto")
    private String rc_tax_upto;

    @JsonProperty("rc_stand_cap")
    private String rc_stand_cap;

    @JsonProperty("rc_fit_upto")
    private String rc_fit_upto;

    @JsonProperty("stautsMessage")
    private String stautsMessage;

    @JsonProperty("rc_insurance_policy_no")
    private String rc_insurance_policy_no;

    @JsonProperty("rc_body_type_desc")
    private String rc_body_type_desc;

    @JsonProperty("rc_wheelbase")
    private String rc_wheelbase;

    @JsonProperty("rc_norms_desc")
    private String rc_norms_desc;

    @JsonProperty("rc_no_cyl")
    private String rc_no_cyl;

    @JsonProperty("rc_regn_no")
    private String rc_regn_no;

    @JsonProperty("rc_fuel_desc")
    private String rc_fuel_desc;

    @JsonProperty("rc_seat_cap")
    private String rc_seat_cap;
}