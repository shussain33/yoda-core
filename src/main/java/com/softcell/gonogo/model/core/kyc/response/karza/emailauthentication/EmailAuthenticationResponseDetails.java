package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailAuthenticationResponseDetails {

   //Need to confirm

}