package com.softcell.gonogo.model.core.request.thirdparty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * Created by suraj on 13/10/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "facematchInfo")
public class FaceMatchRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.InsertGroup.class})
    @Valid
    private Header header;

    @Id
    @JsonProperty("sRefId")
    @NotEmpty(groups = {FaceMatchRequest.InsertGroup.class, FaceMatchRequest.FetchGroup.class})
    private String refId;

    @JsonProperty("oFaceMatchDetails")
    private FaceMatchDetails faceMatchDetails;

    public interface InsertGroup {

    }

    public interface FetchGroup {

    }

}
