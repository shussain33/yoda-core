package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shyamk
 *         This Domain is used to request adhar cam.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AadhaarRequestNew{


    @JsonProperty("REQUEST-XML")
    private String aadhaarXml;

    @JsonProperty("AUTH-XML")
    private String authXML;

    @JsonProperty("AUTH-TYPE")
    private String authType;

    @JsonProperty("HEADER")
    private AadhaarHeader header;

}
