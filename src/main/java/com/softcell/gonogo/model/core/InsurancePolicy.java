package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * Created by archana on 10/12/18.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InsurancePolicy {
    // ICICI /RELIGARE / LIC
    @JsonProperty("sInsuraceCompany")
    private String company;

    // Life / Health / Home / Car / General
    @JsonProperty("sInsuranceProduct")
    private String product;

    // Company defined scheme like Gold / Silver / Previledged , etc
    @JsonProperty("sInsuranceScheme")
    private String scheme;

    // Company defined - Jeevan Suraksha, Smart Life, etc.
    @JsonProperty("sBenefit")
    private String name;

    // Premium may not be set; in that casse it shouldn't be defaulted to 0.0
    @JsonProperty("iPremiumAmount")
    private Double premium;

    // This is SBFC0LAP provided by ICICI for SBFC.
    @JsonProperty("sMasterCode")
    private String masterCode;

    @JsonProperty("sBenefitOption")
    private  String benefitOption;

    @JsonProperty("iSumAssured")
    private Double sumAssured;

    @JsonProperty("iCiBenefit")
    private Double ciBenefit;

    @JsonProperty("sPolicyNumber")
    private String policyNumber;

    @JsonProperty("sUrl")
    private String url;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("bPremium")
    private boolean premiumHit;

    @JsonProperty("oNomineeInfo")
    private NomineeInfo nomineeInfo;

    @JsonProperty("sTenorProposed")
    private String tenorRequested;

    @JsonProperty("sProposedAmount")
    private String proposedAmount;

    @JsonProperty("bException")
    private boolean exception;

    @JsonProperty("aOptDetails")
    private List<OptDetails> optDetailsList;

    @JsonProperty("sInsuranceId")
    private String insuranceId;

    @JsonProperty("sPremiumMsg")
    private String  msg;

    @JsonProperty("dInsuredAmt")
    private Double insuredAmt;
    @JsonProperty("sInsuranceTerm")
    private String insTerm;

    @JsonProperty("oGeneratedDate")
    private Date generatedDate = new Date();

    @JsonProperty("sMifinInsuranceId")
    private String mifinInsuranceId;

}
