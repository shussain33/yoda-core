package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GstCredentialLinkGenerationResponseDetails {

    @JsonProperty("weblink")
    private String weblink;
}