package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "collectionConfig")
public class CollectionConfig {

    @JsonProperty("sCollName")
    @NotNull
    private String collectionName;

    @JsonProperty("sInstIdMapping")
    @NotNull
    private String institutionIdMapping;

    @JsonProperty("sRefIdMapping")
    @NotNull
    private String refIdMapping;

    @Transient
    @JsonProperty("sUpdateStatus")
    private String UpdateStatus;
}
