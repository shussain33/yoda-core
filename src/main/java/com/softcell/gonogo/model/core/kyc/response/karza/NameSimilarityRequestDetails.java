package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NameSimilarityRequestDetails {

    @JsonProperty("name1")
    private String name1;

    @JsonProperty("name2")
    private String name2;

    @JsonProperty("type")
    private String type;

}