package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 23/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class YearData {

    @JsonProperty("dCurrentYearValue")
    double currentYearValue;

    @JsonProperty("dPreviousYearValue")
    double previousYearValue;

    @JsonProperty("dCurrent-2_YearValue")
    double currentMinusTwoYearValue;

    @JsonProperty("dCurrent-3_YearValue")
    double currentMinusThreeYearValue;

    @JsonProperty("dCurrent-4_YearValue")
    double currentMinusFourYearValue;

}
