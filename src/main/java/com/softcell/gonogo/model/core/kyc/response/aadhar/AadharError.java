/**
 * yogeshb5:31:24 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 *
 */
public class AadharError {

    @JsonProperty("sCode")
    private String code;

    @JsonProperty("sDesc")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "AadharError [code=" + code + ", description=" + description
                + "]";
    }
}
