package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class KycResponseInner {
    @JsonProperty("result")
    private String result;

    @JsonProperty("err")
    private String error;


    @JsonProperty("code")
    private String code;

    @JsonProperty("transaction-identifier")
    private String transactionIdentifier;

    @JsonProperty("ts")
    private String ts;

    @JsonProperty("uid")
    private String uid;

    @JsonProperty("auth-response-xml")
    private String authResponseXml;

    @JsonProperty("pht")
    private String pht;

    @JsonProperty("PROOF-OF-IDENTITY")
    private ProofOfIdentity proofOfIdentity;

    @JsonProperty("PROOF-OF-ADDRESS")
    private AddressSegment addressSegment;

    @JsonProperty("prn")
    private String pdfDocument;


    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    public void setTransactionIdentifier(String transactionIdentifier) {
        this.transactionIdentifier = transactionIdentifier;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAuthResponseXml() {
        return authResponseXml;
    }

    public void setAuthResponseXml(String authResponseXml) {
        this.authResponseXml = authResponseXml;
    }

    public String getPht() {
        return pht;
    }

    public void setPht(String pht) {
        this.pht = pht;
    }

    public ProofOfIdentity getProofOfIdentity() {
        return proofOfIdentity;
    }

    public void setProofOfIdentity(ProofOfIdentity proofOfIdentity) {
        this.proofOfIdentity = proofOfIdentity;
    }

    public AddressSegment getAddressSegment() {
        return addressSegment;
    }

    public void setAddressSegment(AddressSegment addressSegment) {
        this.addressSegment = addressSegment;
    }

    public String getPdfDocument() {
        return pdfDocument;
    }

    public void setPdfDocument(String pdfDocument) {
        this.pdfDocument = pdfDocument;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("KycResponseInner [result=");
        builder.append(result);
        builder.append(", error=");
        builder.append(error);
        builder.append(", code=");
        builder.append(code);
        builder.append(", transactionIdentifier=");
        builder.append(transactionIdentifier);
        builder.append(", ts=");
        builder.append(ts);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", authResponseXml=");
        builder.append(authResponseXml);
        builder.append(", pht=");
        builder.append(pht);
        builder.append(", proofOfIdentity=");
        builder.append(proofOfIdentity);
        builder.append(", addressSegment=");
        builder.append(addressSegment);
        builder.append(", pdfDocument=");
        builder.append(pdfDocument);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((addressSegment == null) ? 0 : addressSegment.hashCode());
        result = prime * result
                + ((authResponseXml == null) ? 0 : authResponseXml.hashCode());
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((error == null) ? 0 : error.hashCode());
        result = prime * result
                + ((pdfDocument == null) ? 0 : pdfDocument.hashCode());
        result = prime * result + ((pht == null) ? 0 : pht.hashCode());
        result = prime * result
                + ((proofOfIdentity == null) ? 0 : proofOfIdentity.hashCode());
        result = prime * result
                + ((this.result == null) ? 0 : this.result.hashCode());
        result = prime
                * result
                + ((transactionIdentifier == null) ? 0 : transactionIdentifier
                .hashCode());
        result = prime * result + ((ts == null) ? 0 : ts.hashCode());
        result = prime * result + ((uid == null) ? 0 : uid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof KycResponseInner))
            return false;
        KycResponseInner other = (KycResponseInner) obj;
        if (addressSegment == null) {
            if (other.addressSegment != null)
                return false;
        } else if (!addressSegment.equals(other.addressSegment))
            return false;
        if (authResponseXml == null) {
            if (other.authResponseXml != null)
                return false;
        } else if (!authResponseXml.equals(other.authResponseXml))
            return false;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        if (error == null) {
            if (other.error != null)
                return false;
        } else if (!error.equals(other.error))
            return false;
        if (pdfDocument == null) {
            if (other.pdfDocument != null)
                return false;
        } else if (!pdfDocument.equals(other.pdfDocument))
            return false;
        if (pht == null) {
            if (other.pht != null)
                return false;
        } else if (!pht.equals(other.pht))
            return false;
        if (proofOfIdentity == null) {
            if (other.proofOfIdentity != null)
                return false;
        } else if (!proofOfIdentity.equals(other.proofOfIdentity))
            return false;
        if (result == null) {
            if (other.result != null)
                return false;
        } else if (!result.equals(other.result))
            return false;
        if (transactionIdentifier == null) {
            if (other.transactionIdentifier != null)
                return false;
        } else if (!transactionIdentifier.equals(other.transactionIdentifier))
            return false;
        if (ts == null) {
            if (other.ts != null)
                return false;
        } else if (!ts.equals(other.ts))
            return false;
        if (uid == null) {
            if (other.uid != null)
                return false;
        } else if (!uid.equals(other.uid))
            return false;
        return true;
    }
}
