package com.softcell.gonogo.model.core.kyc.request.aadhar;

import java.util.List;

public class BioMetric {
	
	
	List<BioMetricRecord> bio;

	public List <BioMetricRecord> getBio() {
		return bio;
	}

	public void setBio(List <BioMetricRecord> bio) {
		this.bio = bio;
	}

	@Override
	public String toString() {
		return "BioMetric [bio=" + bio + "]";
	}

	
	
	
	
	

}
