package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.config.KarzaProcessingInfo;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.AadharNumberResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.aadharauthentication.UdyogAadharNumberResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.addressmappingauthentication.AddressMatchingResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication.BankAccountVerificationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication.HsnCodeCheckResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.bankauthentication.IfscCodeCheckResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.companyllpidentification.CompanyIdentificationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.companyllpidentification.LlpIdentificationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication.EmailAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication.EmailVerificationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced.EmploymentVerificationAdvancedResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.EpfUanLookupResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.ItrAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.employerlookup.EmployerLookupResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.esicidauthentication.EsicIdAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.form16authentication.Form16AuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.form16quaterlyauthentication.QuarterlyRecordsCountForNextFiscal;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.otpauthentication.OtpResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.passbookauthentication.PassbookResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.*;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication.GspGstinAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gstapiauthentication.GstTransactionApiResponse;
import com.softcell.gonogo.model.core.kyc.response.karza.icsiuthentication.IcsiMembershipAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.icwaiauthentication.IcwaiFirmAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.icwaiauthentication.IcwaiMembershipAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication.IecDetailedProfileResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.iecauthentication.IecResultResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.kscan.*;
import com.softcell.gonogo.model.core.kyc.response.karza.mciauthentication.MciMembershipAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.nregaauthentication.NregaResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.LpgIdAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication.MobileDetailsResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication.MobileOtpResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.PngAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.TelephoneBillAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.vehicleauthentication.VehicleRCAuthenticationResponseDetails;
import com.softcell.gonogo.model.core.kyc.response.karza.websiteauthentication.WebsiteDomainAuthenticationResponseDetails;
import com.softcell.gonogo.model.kyc.request.PassportResponses.KPassportResult;
import com.softcell.gonogo.model.kyc.request.tanResponses.KTanAuthResult;
import com.softcell.gonogo.model.kyc.request.tanResponses.KTanDetailResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KarzaClientResponse {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sDocumentType")
    private String documentType;

    @JsonProperty("sDocumentId")
    private String documentId;

    @JsonProperty("oVoterAuthResponse")
    private VoterIdResponseDetails voterIdAuthResponse;

    @JsonProperty("oPanAuthResponse")
    private PanCardResponseDetails panAuthResponse;

    @JsonProperty("oDrivingLicenceAuthResponse")
    private DrivingLicenceResponseDetails drivingLicenceAuthResponse;

    @JsonProperty("oElectricityAuthResponse")
    private ElectricityResponseDetails electricityAuthResponse;

    @JsonProperty("sApplicantName")
    private String applicantName;

    @JsonProperty("sDob")
    private String dob;

    @JsonProperty("sAddress")
    private String address;

    @JsonProperty("sKarzaStatus")
    private String karzaStatus;

    @JsonProperty("sErr")
    private String error;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("sSslApplicationId")
    private String sslApplicationId;

    @JsonProperty("oErrorList")
    private ArrayList<Error> errorList;

    @JsonProperty("oAadharAuthResponse")
    private AadharNumberResponseDetails aadharNumberResponseDetails;

    @JsonProperty("oNregaAuthResponse")
    private NregaResponseDetails nregaResponseDetails;

    @JsonProperty("oPassportAuthResponse")
    private KPassportResult kPassportResult;

    @JsonProperty("oTanAuthResponse")
    private KTanAuthResult kTanAuthResult;

    @JsonProperty("oTanDetailResponse")
    private KTanDetailResult kTanDetailResult;

    //Need to confirm
    /*@JsonProperty("oMcaSignatoriesResponse")
    private List<KarzaResultDetails> karzaResultDetailsList;

    @JsonProperty("oCompanySearchNameResponse")
    private List<KarzaResultDetails> karzaResultDetailsList;

    @JsonProperty("oCompanyLlpCinLookupResponse")
    private List<KarzaResultDetails> karzaResultDetailsList;
    */

    @JsonProperty("oIecResultResponse")
    private IecResultResponseDetails iecResultResponseDetails;

    @JsonProperty("oIecDetailProfileResponse")
    private IecDetailedProfileResponseDetails iecDetailedProfileResponseDetails;

    @JsonProperty("oCompanyIdentificationResponse")
    private CompanyIdentificationResponseDetails companyIdentificationResponseDetails;

    @JsonProperty("oLlpIdentificationResponse")
    private LlpIdentificationResponseDetails llpIdentificationResponseDetails;

    @JsonProperty("oUdyogAadharNumberResponse")
    private UdyogAadharNumberResponseDetails udyogAadharNumberResponseDetails;

    @JsonProperty("oGstAuthenticationResponse")
    private GstAuthenticationResponseDetails gstAuthenticationResponseDetails;

    @JsonProperty("oGstIdentificationResponse")
    private GstIdentificationResponseDetails gstIdentificationResponseDetails;

    @JsonProperty("oGstSearchBasisPanResponse")
    private GstSearchBasisPanResponseDetails gstSearchBasisPanResponseDetails;

    @JsonProperty("oGspGstinAuthenticationResponse")
    private GspGstinAuthenticationResponseDetails gspGstinAuthenticationResponseDetails;

    @JsonProperty("oGspGstReturnFillingResponse")
    private GspGstReturnFillingResponseDetails gspGstReturnFillingResponseDetails;

    @JsonProperty("oShopAndEstablishmentResponse")
    private ShopAndEstablishmentResponseDetails shopAndEstablishmentResponseDetails;

    @JsonProperty("oFssaiLicenseResponse")
    private FssaiLicenseResponseDetails fssaiLicenseResponseDetails;

    @JsonProperty("oFdaLicenseAuthenticationResponse")
    private FdaLicenseAuthenticationResponseDetails fdaLicenseAuthenticationResponseDetails;

    @JsonProperty("oCaMembershipAuthenticationResponse")
    private CaMembershipAuthenticationResponseDetails caMembershipAuthenticationResponseDetails;

    @JsonProperty("oIcsiMembershipAuthenticationResponse")
    private IcsiMembershipAuthenticationResponseDetails icsiMembershipAuthenticationResponseDetails;

    @JsonProperty("oIcwaiMembershipAuthenticationResponse")
    private IcwaiMembershipAuthenticationResponseDetails icwaiMembershipAuthenticationResponseDetails;

    @JsonProperty("oIcwaiFirmAuthenticationResponse")
    private IcwaiFirmAuthenticationResponseDetails icwaiFirmAuthenticationResponseDetails;

    @JsonProperty("oMciMembershipAuthenticationResponse")
    private MciMembershipAuthenticationResponseDetails mciMembershipAuthenticationResponseDetails;

    @JsonProperty("oPngAuthenticationResponse")
    private PngAuthenticationResponseDetails pngAuthenticationResponseDetails;

    @JsonProperty("oTelephoneBillAuthenticationResponse")
    private TelephoneBillAuthenticationResponseDetails telephoneBillAuthenticationResponseDetails;

    @JsonProperty("oMobileDetailsResponse")
    private MobileDetailsResponseDetails mobileDetailsResponseDetails;

    @JsonProperty("oMobileOtpResponse")
    private MobileOtpResponseDetails mobileOtpResponseDetails;

    @JsonProperty("oLpgIdAuthenticationResponse")
    private LpgIdAuthenticationResponseDetails lpgIdAuthenticationResponseDetails;

    @JsonProperty("oOtpResponse")
    private OtpResponseDetails otpResponseDetails;

    @JsonProperty("oPassbookResponse")
    private PassbookResponseDetails passbookResponseDetails;

    @JsonProperty("oEpfUanLookupResponse")
    private EpfUanLookupResponseDetails epfUanLookupResponseDetails;

    @JsonProperty("oEmployerLookupResponse")
    private EmployerLookupResponseDetails employerLookupResponseDetails;

    @JsonProperty("oEsicIdAuthenticationResponse")
    private EsicIdAuthenticationResponseDetails esicIdAuthenticationResponseDetails;

    @JsonProperty("oForm16AuthenticationResponse")
    private Form16AuthenticationResponseDetails form16AuthenticationResponseDetails;

    @JsonProperty("oQuarterlyRecordsCountForNextFiscalResponse")
    private QuarterlyRecordsCountForNextFiscal quarterlyRecordsCountForNextFiscal;

    @JsonProperty("oItrAuthenticationResponseDetailsResponse")
    private ItrAuthenticationResponseDetails itrAuthenticationResponseDetails;

    @JsonProperty("oAddressMatchingResponse")
    private AddressMatchingResponseDetails addressMatchingResponseDetails;

    @JsonProperty("oEmailAuthenticationResponse")
    private EmailAuthenticationResponseDetails emailAuthenticationResponseDetails;

    @JsonProperty("oNameSimilarityResponse")
    private NameSimilarityResponseDetails nameSimilarityResponseDetails;

    @JsonProperty("oIfscCodeCheckResponse")
    private IfscCodeCheckResponseDetails ifscCodeCheckResponseDetails;

    @JsonProperty("oBankAccountVerificationResponse")
    private BankAccountVerificationResponseDetails bankAccountVerificationResponseDetails;

    @JsonProperty("oHsnCodeCheckResponse")
    private HsnCodeCheckResponseDetails hsnCodeCheckResponseDetails;

    @JsonProperty("oWebsiteDomainAuthenticationResponse")
    private WebsiteDomainAuthenticationResponseDetails websiteDomainAuthenticationResponseDetails;

    @JsonProperty("oVehicleRCAuthenticationResponse")
    private VehicleRCAuthenticationResponseDetails vehicleRCAuthenticationResponseDetails;

    @JsonProperty("oGstTransactionApiVerifyOtpResponse")
    private GstTransactionApiVerifyOtpResponseDetails gstTransactionApiVerifyOtpResponseDetails;

    @JsonProperty("oGstCredentialLinkGenerationResponse")
    private GstCredentialLinkGenerationResponseDetails gstCredentialLinkGenerationResponseDetails;
    //using same pojo because same response type
    @JsonProperty("oGstCredentialLinkSendResponse")
    private GstCredentialLinkGenerationResponseDetails gstCredentialLinkSendResponseDetails;

    // *******//

    @JsonProperty("oEntitySearchResponse")
    private EntitySearchResponseDetails entitySearchResponseDetails;

    @JsonProperty("oDetailedProfileResponse")
    private DetailedProfileResponseDetails detailedProfileResponseDetails;

    @JsonProperty("oGstFilingStatusResponse")
    private GstFilingStatusResponseDetails gstFilingStatusResponseDetails;

    @JsonProperty("oGstEntitySearchResponse")
    private GstProfileResponseDetails gstProfileResponseDetails;

    @JsonProperty("oIBBIResponse")
    private IBBIResponseDetails ibbiResponseDetails;

    @JsonProperty("oBIFRResponse")
    private BIFRResponseDetails bifrResponseDetails;

    @JsonProperty("oDrtCheckResponse")
    private DrtCheckResponseDetails drtCheckResponseDetails;

    @JsonProperty("oSuitFiledSearchResponse")
    private SuitFiledSearchResponseDetails suitFiledSearchResponseDetails;

    @JsonProperty("oLitigationsSearchResponse")
    private LitigationsSearchResponseDetails litigationsSearchResponseDetails;

    @JsonProperty("oLitigationDetailsDistrictCourtResponse")
    private LitigationDetailsDistrictCourtResponseDetails litigationDetailsDistrictCourtResponseDetails;

    @JsonProperty("oGstTransactionApiResponse")
    private GstTransactionApiResponse gstTransactionApiResponse;

    @JsonProperty("oCompanySearchByNameResponseDetails")
    private CompanySearchByNameResponseDetails companySearchByNameResponseDetails;

    @JsonProperty("oKarzaProcessingInfo")
    public KarzaProcessingInfo karzaProcessingInfo;

    @JsonProperty("oPanAuthenticationResponse")
    private PanStatusCheck panAuthenticationResponse;

    @JsonProperty("oEmailVerificationResponseDetails")
    private EmailVerificationResponseDetails emailVerificationResponseDetails;

    @JsonProperty("oEmploymentVerificationAdvancedResponseDetails")
    private EmploymentVerificationAdvancedResponseDetails employmentVerificationAdvancedResponseDetails;
}