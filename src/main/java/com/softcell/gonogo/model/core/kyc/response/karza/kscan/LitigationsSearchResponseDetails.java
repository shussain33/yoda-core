package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class LitigationsSearchResponseDetails {

    @JsonProperty("records")
    private LitigationSearchRecords records;

    @JsonProperty("page")
    private int page;

    @JsonProperty("totalPages")
    private int totalPages;

    @JsonProperty("statusCode")
    private int statusCode;

    @JsonProperty("totalRecords")
    private TotalRecords totalRecords;
}
