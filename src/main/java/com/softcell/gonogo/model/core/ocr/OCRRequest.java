package com.softcell.gonogo.model.core.ocr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.RCUSummary;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by ssg228 on 6/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OCRRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {OCRRequest.InsertGrp.class,OCRRequest.FetchGrp.class})
    public String refId;

    @JsonProperty("oOCRDetails")
    @NotNull(groups = {OCRRequest.InsertGrp.class})
    private OCRDetails ocrDetails;

    @JsonProperty("oRCUSummary")
    private RCUSummary rcuSummary;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sAgencyCode")
    public String agencyCode;

    @JsonProperty("sInitiatedBy")
    private String initiatedBy;

    @JsonProperty(value = "bIsCaseInitiated")
    private boolean isCaseInitiated;

    @JsonProperty(value = "sInitiatedByRole")
    private String initiatedByRole;

    @JsonProperty("sAgencyName")
    public String agencyName;

    @JsonProperty("sAgencyStatus")
    public String agencyStatus;

    @JsonProperty("bResetStatus")
    public boolean resetStatus = false;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
