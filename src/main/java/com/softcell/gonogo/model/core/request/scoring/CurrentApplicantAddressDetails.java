package com.softcell.gonogo.model.core.request.scoring;


public class CurrentApplicantAddressDetails {

    private String flatNoPlotNoHouseNo;
    private String bldgNumberSocietyName;
    private String roadNumbernameAreaLocality;
    private String city;
    private String landmark;
    private String state;
    private String pinCode;
    private String countryCode;

    public String getFlatNoPlotNoHouseNo() {
        return flatNoPlotNoHouseNo;
    }

    public void setFlatNoPlotNoHouseNo(String flatNoPlotNoHouseNo) {
        this.flatNoPlotNoHouseNo = flatNoPlotNoHouseNo;
    }

    public String getBldgNumberSocietyName() {
        return bldgNumberSocietyName;
    }

    public void setBldgNumberSocietyName(String bldgNumberSocietyName) {
        this.bldgNumberSocietyName = bldgNumberSocietyName;
    }

    public String getRoadNumbernameAreaLocality() {
        return roadNumbernameAreaLocality;
    }

    public void setRoadNumbernameAreaLocality(String roadNumbernameAreaLocality) {
        this.roadNumbernameAreaLocality = roadNumbernameAreaLocality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "CurrentApplicantAddressDetails [flatNoPlotNoHouseNo="
                + flatNoPlotNoHouseNo + ", bldgNumberSocietyName="
                + bldgNumberSocietyName + ", roadNumbernameAreaLocality="
                + roadNumbernameAreaLocality + ", city=" + city + ", landmark="
                + landmark + ", state=" + state + ", pinCode=" + pinCode
                + ", countryCode=" + countryCode + "]";
    }
}
