package com.softcell.gonogo.model.core.request.scoring;

public class CibilRespPhone {
    private String telephoneNumber;

    private String telephoneExtention;

    private String telephoneType;

    private String enrichEnquiryForPhone;

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneExtention() {
        return telephoneExtention;
    }

    public void setTelephoneExtention(String telephoneExtention) {
        this.telephoneExtention = telephoneExtention;
    }

    public String getTelephoneType() {
        return telephoneType;
    }

    public void setTelephoneType(String telephoneType) {
        this.telephoneType = telephoneType;
    }

    public String getEnrichEnquiryForPhone() {
        return enrichEnquiryForPhone;
    }

    public void setEnrichEnquiryForPhone(String enrichEnquiryForPhone) {
        this.enrichEnquiryForPhone = enrichEnquiryForPhone;
    }

    @Override
    public String toString() {
        return "CibilRespPhone [telephoneNumber=" + telephoneNumber
                + ", telephoneExtention=" + telephoneExtention
                + ", telephoneType=" + telephoneType
                + ", enrichEnquiryForPhone=" + enrichEnquiryForPhone + "]";
    }


}
