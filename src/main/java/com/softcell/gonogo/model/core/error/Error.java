package com.softcell.gonogo.model.core.error;

import java.io.Serializable;

/**
 * @author kishorp
 */
public class Error implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String condition;
    private String fieldName;
    private String errorType;
    private String errorCode;
    private String errorDescription;
    private String level;
    private String applicationFor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getApplicationFor() {
        return applicationFor;
    }

    public void setApplicationFor(String applicationFor) {
        this.applicationFor = applicationFor;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }


}
