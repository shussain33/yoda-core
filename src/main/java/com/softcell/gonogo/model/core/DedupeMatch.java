package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by yogesh on 29/3/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "dedupeMatchInfo")
public class DedupeMatch {
    @Id
    @JsonProperty("sRefId")
    public String refId;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("aDedupeMatchDetails")
    private List<DedupeMatchDetail> dedupeMatchDetails;

}
