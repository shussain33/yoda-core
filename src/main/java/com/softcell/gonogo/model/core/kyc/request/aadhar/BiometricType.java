package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BiometricType {

    @JsonProperty("TYPE")
    private String type;

    @JsonProperty("POSH")
    private String posh;

    @JsonProperty("VALUE")
    private String value;

    public static Builder builder() {
        return new Builder();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPosh() {
        return posh;
    }

    public void setPosh(String posh) {
        this.posh = posh;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BiometricType{");
        sb.append("type='").append(type).append('\'');
        sb.append(", posh='").append(posh).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BiometricType)) return false;
        BiometricType that = (BiometricType) o;
        return Objects.equal(getType(), that.getType()) &&
                Objects.equal(getPosh(), that.getPosh()) &&
                Objects.equal(getValue(), that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getType(), getPosh(), getValue());
    }

    public static class Builder {
        private BiometricType biometricType = new BiometricType();

        public BiometricType build() {
            return biometricType;
        }

        public Builder type(String type) {
            this.biometricType.type = type;
            return this;
        }

        public Builder posh(String posh) {
            this.biometricType.posh = posh;
            return this;
        }

        public Builder value(String value) {
            this.biometricType.value = value;
            return this;
        }
    }
}
