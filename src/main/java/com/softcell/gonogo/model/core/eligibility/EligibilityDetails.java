package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "eligibilityDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EligibilityDetails extends ApplicationVerification {

    @JsonProperty("dGrossIncmeForAppl")
    private double grossIncome;

    @JsonProperty("dFixedObligationForAppl")
    private double fixedOblgation;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("dEligibilityRoi")
    private double eligibilityRoi;

    @JsonProperty("dApprovedLoan")
    private double aprvLoan;

    @JsonProperty("dEmi")
    private double emi;

    @JsonProperty("dFoir")
    private double foir;

    @JsonProperty("oFoirDetail")
    private FoirDetail foirDetail;

    @JsonProperty("IncomeDetailList")
    private List<IncomeDetail> incomeDetailList;

    /*
    adding extra fields for pl
     */
    @JsonProperty("dLoanAmt")
    private double appliedLoan;

    @JsonProperty("sTenorRequested")
    private String appliedTenor;

    @JsonProperty("dmonthSal")
    private double monthlySalary;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("dMultiplier")
    private double multiplier;

    @JsonProperty("dEliAsFoir")
    private double eligibilityAsFoir;

    @JsonProperty("dFinalAmtLacks")
    private double finalAmtLacks;

    @JsonProperty("sPF")
    private String PF;

    @JsonProperty("dFinalFoir")
    private double finalFoir;

    @JsonProperty("dInsuranceAmt")
    private double insurenceAmt;

    @JsonProperty("bHLFlag")
    private boolean hlFlag;

    @JsonProperty("dElgTenor")
    private double elgTenor;

    @JsonProperty("dElgfixedOblg")
    private double elgFixedOblg;

    @JsonProperty("sPolicyFOIR")
    private double policyFoir;

    @JsonProperty("dDevAmt")
    private double devAmt;

    @JsonProperty("dBalEmi")
    private double balenceEmi;

    @JsonProperty("dElgFixedObg")
    private double elgFixedObg;

    @JsonProperty("dElgLnAmt")
    private double elgLnAmt;

    @JsonProperty("dProposedSanctionAmount")
    private double proposedSanctionAmount;

    @JsonProperty("sCustomerType")
    private String customerType;

    @JsonProperty("sProductType")
    private String productType;

    //@Values(Normal / BT / Top-up / Addn)
    @JsonProperty("sScheme")
    private String scheme;

    @JsonProperty("dIncome")
    private double income;

    @JsonProperty("dValuation")
    private double valuation;

    @JsonProperty("sPurpose")
    private String purpose;

    @JsonProperty("sLnPurpose2")
    private String loanPurpose2;

    @JsonProperty("dTotalExposure")
    private double totalExposure;

    @JsonProperty("dCustomerIRR")
    private double customerIRR;

    @JsonProperty("dGroupTotalExposure")
    private double groupTotalExposure;

    @JsonProperty("dCombinedDBR")
    private double combinedDBR;

    @JsonProperty("dCombinedLTV")
    private double combinedLTV;

    @JsonProperty("sRole")
    private  String role;

    @JsonProperty("sKYCRating")
    private String kycRating;

    @JsonProperty("sBorrowerRiskRating")
    private String borrowerRiskRating;

    @JsonProperty("sCumulativeLTV")
    private String cumulativeLTV;

    @JsonProperty("sCumulativeDBR")
    private String cumulativeDBR;

    @JsonProperty("sCumulativeExposure")
    private String cumulativeExposure;

    @JsonProperty("sTypeOfPropertyLegal")
    private String typeOfPropertyLegal;

    @JsonProperty("sRiskDocumentLegal")
    private String riskDocumentLegal;

    @JsonProperty("sTotalAssetCost")
    private String totalAssetCost;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("sCustSegment")
    private String custSegment;

    @JsonProperty("sCustSubSegment")
    private String custSubSegment;

    @JsonProperty("sTakeover")
    private String takeOver;

    @JsonProperty("dTakeoverAmt")
    private double takeoverAmount;

    @JsonProperty("sTopup")
    private String topup;

    @JsonProperty("sTrancheDisb")
    private String trancheDisb;

    @JsonProperty("iNoOfAdvEmis")
    private int noOfAdvEmis;

    @JsonProperty("sExecutiveSummary")
    private String executiveSummary;

    @JsonProperty("dMarginRoi")
    private double marginRoi;

    @JsonProperty("dTotalRoi")
    private double totalRoi;

    @JsonProperty("dTotalMarketValue")
    private double totalMarketValue;

    @JsonProperty("sStaffLoan")
    private String staffLoan;

    @JsonProperty("sInsuranceMessage")
    private String insuranceMessage;

    @JsonProperty("oDerivedFields")
    private Object derivedFields;

    @JsonProperty("bTriggerSobreDeviation")
    private boolean triggerSobreDeviation;

    @JsonProperty("dDSCR")
    private double dscr;

    @JsonProperty("dDebttoEquity")
    private double debttoEquity;

    @JsonProperty("dBorrowingTurnoverRatio")
    private double borrowingTurnoverRatio;

    @JsonProperty("dCurrentRatio")
    private double currentRatio;

    @JsonProperty("dBankingThroughput")
    private double bankingThroughput;

    @JsonProperty("iDebtorDays")
    private int debtorDays;

    @JsonProperty("iCreditorDays")
    private int creditorDays;

    @JsonProperty("sTypeOfROI")
    private String typeOfROI;

    //DigiPl Fields Started
    @JsonProperty("aEligibleOffers")
    private List<EligibleOffers> eligibleOffersList = new ArrayList<>();

    @JsonProperty("dCreditCardOutStanding")
    private double creditCardOutStanding;

    @JsonProperty("bIsExistingLoan")
    private boolean isExistingLoan;

    @JsonProperty("dRoi")
    private Double roi;
}
