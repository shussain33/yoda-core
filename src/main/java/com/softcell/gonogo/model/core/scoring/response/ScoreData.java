package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * @author prateek
 */
public class ScoreData {

    @JsonProperty("FINAL_SCORE")
    private String finalScore;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("ERRORS")
    private Errors error;

    @JsonProperty("SCORECARD_NAME")
    private String scoreCardName;

    @JsonProperty("SCORE_VALUE")
    private String scoreValue;

    @JsonProperty("SCORE_DETAILS")
    private Object scoreDetails;

    @JsonProperty("FINAL_BAND")
    private String finalBand;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(String finalScore) {
        this.finalScore = finalScore;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Errors getError() {
        return error;
    }

    public void setError(Errors error) {
        this.error = error;
    }

    public String getScoreCardName() {
        return scoreCardName;
    }

    public void setScoreCardName(String scoreCardName) {
        this.scoreCardName = scoreCardName;
    }

    public String getScoreValue() {
        return scoreValue;
    }

    public void setScoreValue(String scoreValue) {
        this.scoreValue = scoreValue;
    }

    public Object getScoreDetails() {
        return scoreDetails;
    }

    public void setScoreDetails(Object scoreDetails) {
        this.scoreDetails = scoreDetails;
    }

    /**
     * @return the finalBand
     */
    public String getFinalBand() {
        return finalBand;
    }

    /**
     * @param finalBand the finalBand to set
     */
    public void setFinalBand(String finalBand) {
        this.finalBand = finalBand;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ScoreData [finalScore=");
        builder.append(finalScore);
        builder.append(", status=");
        builder.append(status);
        builder.append(", error=");
        builder.append(error);
        builder.append(", scoreCardName=");
        builder.append(scoreCardName);
        builder.append(", scoreValue=");
        builder.append(scoreValue);
        builder.append(", scoreDetails=");
        builder.append(scoreDetails);
        builder.append(", finalBand=");
        builder.append(finalBand);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result + ((error == null) ? 0 : error.hashCode());
        result = prime * result
                + ((finalBand == null) ? 0 : finalBand.hashCode());
        result = prime * result
                + ((finalScore == null) ? 0 : finalScore.hashCode());
        result = prime * result
                + ((scoreCardName == null) ? 0 : scoreCardName.hashCode());
        result = prime * result
                + ((scoreDetails == null) ? 0 : scoreDetails.hashCode());
        result = prime * result
                + ((scoreValue == null) ? 0 : scoreValue.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ScoreData other = (ScoreData) obj;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (error == null) {
            if (other.error != null)
                return false;
        } else if (!error.equals(other.error))
            return false;
        if (finalBand == null) {
            if (other.finalBand != null)
                return false;
        } else if (!finalBand.equals(other.finalBand))
            return false;
        if (finalScore == null) {
            if (other.finalScore != null)
                return false;
        } else if (!finalScore.equals(other.finalScore))
            return false;
        if (scoreCardName == null) {
            if (other.scoreCardName != null)
                return false;
        } else if (!scoreCardName.equals(other.scoreCardName))
            return false;
        if (scoreDetails == null) {
            if (other.scoreDetails != null)
                return false;
        } else if (!scoreDetails.equals(other.scoreDetails))
            return false;
        if (scoreValue == null) {
            if (other.scoreValue != null)
                return false;
        } else if (!scoreValue.equals(other.scoreValue))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }
}
