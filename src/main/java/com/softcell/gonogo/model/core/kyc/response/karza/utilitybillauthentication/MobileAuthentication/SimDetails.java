package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication.MobileAuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SimDetails {

    @JsonProperty("activation_date")
    private String activation_date;

    @JsonProperty("last_activity_date")
    private String last_activity_date;

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("type")
    private String type;

    @JsonProperty("otp_validated")
    private boolean otp_validated;

}