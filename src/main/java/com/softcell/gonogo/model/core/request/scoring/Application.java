package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.ProfessionIncomeDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 9/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Application {

     @JsonProperty("POLICYNAME")
     private String policyName;

     @JsonProperty("sLoanType")
     private String loanType;

     @JsonProperty("dLoanAmt")
     private double loanAmt;

     @JsonProperty("iLoanTenorInMonths")
     private int loanTenorInMonths;

     @JsonProperty("sPriority")
     private String priority;

     @JsonProperty("aggregation")
     private String aggregation;

     @JsonProperty("sBranchName")
     private String branchName;

     @JsonProperty("sPromoCode")
     private String promoCode;

     @JsonProperty("oProfessionIncomeDetails")
     private ProfessionIncomeDetails professionIncomeDetails;

     @JsonProperty("sCity")
     private String city;

     @JsonProperty("sCallDetails")
     private String callDetails;

     @JsonProperty("sChannel")
     private String channel;

}
