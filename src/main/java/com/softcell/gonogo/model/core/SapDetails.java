package com.softcell.gonogo.model.core;

/**
 * Created by Softcell on 21/09/17.
 */
public enum SapDetails {

    LOANDISBURSALACCOUNT_DP("LOAN DISBURSAL ACCOUNT", "DOWN PAYMENT", "D", "101", "Db", "LOAN DISBURSAL A/C"),
    PROCESSINGFEES("PROCESSING FEES", "DOWN PAYMENT", "S", "101", "Cr", "PROCESSING FEES"),
   CREDITVIDHYA("CREDIT VIDHYA", "DOWN PAYMENT", "S", "101", "Cr", "CREDIT VIDHYA"),
   EXTENDEDWARRANTY("EXTENDED WARRANTY", "DOWN PAYMENT", "S", "101", "Cr", "EXTENDED WARRANTY"),
    CREDITSHIELD("CREDIT SHIELD", "DOWN PAYMENT", "S", "101", "Cr", "SPECIAL INSURANCE - CREDIT SHIELD - ICICI"),
    ADVANCEEMI("ADVANCE EMI", "DOWN PAYMENT", "S", "101", "Cr", "ADVANCE EMI"),
    INITIALHIRE_DP("INITIAL HIRE", "DOWN PAYMENT", "D", "101", "Cr", "INITIAL HIRE"),
    DEALERBUYDOWN("DEALER BUYDOWN", "DOWN PAYMENT", "S", "101", "Cr", "DEALER COMMISSION"),

    LOANDISBURSALACCOUNT_DIS("LOAN DISBURSAL A/C", "DEALER DISBURSAL", "D", "102", "Db", "LOAN DISBURSAL A/C"),
    DEALERACCOUNT("DEALER ACCOUNT", "DEALER DISBURSAL", "D", "102", "Cr", "Dealer Account"),
    SUBVENTIONINCOME("SUBVENTION INCOME", "DEALER DISBURSAL", "S", "102", "Cr", "MANUFACTURER BUYDOWN"),
    MANUFACTURERBUYDOWN("MANUFACTURER ACCOUNT", "DEALER DISBURSAL", "D", "102", "Db", "MANUFACTURER BUYDOWN"),

    SOH("SOH", "AGREEMENTATION", "S", "103", "Db", "STOCK ON HIRE"),
    INITIALHIRE_AGR("INITIAL HIRE", "AGREEMENTATION", "D", "103", "Db", "INITIAL HIRE"),
    LOANDISBURSALACCOUNT_AGR("LOAN DISBURSAL A/C", "AGREEMENTATION", "D", "103", "Cr", "LOAN DISBURSAL A/C");


    private String gngField;
    private String accEntrytype;
    private String accType;
    private String accEntryNumber;
    private String creditOrDebit;

    public String getAccType() {
    return accType;
    }

   public void setAccType(String accType) {
       this.accType = accType;
    }

    private String sapField;

    public String getGngField() {
        return gngField;
    }

    public void setGngField(String gngField) {
       this.gngField = gngField;
    }

    public String getAccEntrytype() {
        return accEntrytype;
    }

    public void setAccEntrytype(String accEntrytype) {
       this.accEntrytype = accEntrytype;
    }

    public String getAccEntryNumber() {
        return accEntryNumber;
    }

    public void setAccEntryNumber(String accEntryNumber) {
        this.accEntryNumber = accEntryNumber;
    }

   public String getCreditOrDebit() {
        return creditOrDebit;
    }

    public void setCreditOrDebit(String creditOrDebit) {
       this.creditOrDebit = creditOrDebit;
    }

    public String getSapField() {
        return sapField;
    }

   public void setSapField(String sapField) {
        this.sapField = sapField;
   }

   private SapDetails(String gngField, String accEntrytype, String accType, String accEntryNumber, String creditOrDebit, String sapField)

    {
             this.gngField = gngField;
               this.accEntrytype = accEntrytype;
               this.accType = accType;
                this.accEntryNumber = accEntryNumber;
            this.creditOrDebit = creditOrDebit;
              this.sapField = sapField;
           }
}
