package com.softcell.gonogo.model.core.kyc.request.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by Gunaratna Dahat on 19/02/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KarzaAPIRequest {

    @JsonProperty("oHeader")
    @Valid
    private Header header;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("panNumber")
    private String panNumber;

    @JsonProperty("karzaAuthRequest")
    @NotNull
    private KarzaAuthenticationRequest karzaAuthenticationRequest;
}
