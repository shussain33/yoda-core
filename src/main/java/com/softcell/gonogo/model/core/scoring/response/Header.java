package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Header {

    @JsonProperty("INSTITUTION-ID")
    private String institutionId;

    @JsonProperty("CUSTOMER-ID")
    private String custId;

    @JsonProperty("RESPONSE-DATETIME")
    private String responseDate;

    @JsonProperty("APPLICATION-ID")
    private String applicationId;

    @JsonProperty("ACK-ID")
    private String ackId;

    @JsonProperty("RESPONSE-TYPE")
    private String responseType;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getAckId() {
        return ackId;
    }

    public void setAckId(String ackId) {
        this.ackId = ackId;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Header [institutionId=");
        builder.append(institutionId);
        builder.append(", custId=");
        builder.append(custId);
        builder.append(", responseDate=");
        builder.append(responseDate);
        builder.append(", applicationId=");
        builder.append(applicationId);
        builder.append(", ackId=");
        builder.append(ackId);
        builder.append(", responseType=");
        builder.append(responseType);
        builder.append("]");
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ackId == null) ? 0 : ackId.hashCode());
        result = prime * result
                + ((applicationId == null) ? 0 : applicationId.hashCode());
        result = prime * result + ((custId == null) ? 0 : custId.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((responseDate == null) ? 0 : responseDate.hashCode());
        result = prime * result
                + ((responseType == null) ? 0 : responseType.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Header other = (Header) obj;
        if (ackId == null) {
            if (other.ackId != null)
                return false;
        } else if (!ackId.equals(other.ackId))
            return false;
        if (applicationId == null) {
            if (other.applicationId != null)
                return false;
        } else if (!applicationId.equals(other.applicationId))
            return false;
        if (custId == null) {
            if (other.custId != null)
                return false;
        } else if (!custId.equals(other.custId))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (responseDate == null) {
            if (other.responseDate != null)
                return false;
        } else if (!responseDate.equals(other.responseDate))
            return false;
        if (responseType == null) {
            if (other.responseType != null)
                return false;
        } else if (!responseType.equals(other.responseType))
            return false;
        return true;
    }

}
