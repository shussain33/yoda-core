package com.softcell.gonogo.model.core.kyc.response.karza.icsiuthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IcsiMembershipRequestDetails {

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("membership_no")
    private String membership_no;

    @JsonProperty("cp_no")
    private String cp_no;

}