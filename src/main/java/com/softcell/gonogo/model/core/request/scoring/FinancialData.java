package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.BankingStatement;
import com.softcell.gonogo.model.core.GstDetailsList;
import com.softcell.gonogo.model.core.PLAndBSAnalysis;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by archana on 23/7/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FinancialData {
    @JsonProperty("dROIOffered")
    private double roiOffered;

    @JsonProperty("aCollateralValues")
    private List<DoubleValue> collateralValues;

    @JsonProperty("aCollateralOwnershipType")
    private List<StringValue> collateralOwnershipType;

    @JsonProperty("oGrossProfit")
    private YearData grossProft;

    @JsonProperty("oCashProfit")
    private YearData cashProft;

    @JsonProperty("oTotalEquityFund")
    private YearData totalEqutyFund;

    @JsonProperty("oProfitBeforeTax")
    private YearData profitBeforeTax;

    @JsonProperty("oTaxPaid")
    private YearData taxPaid;

    @JsonProperty("oSales")
    private YearData sales;

    @JsonProperty("oOtherIncome")
    private YearData otherIncome;

    @JsonProperty("oCurrentLiabilities")
    private YearData currentLiabilities;

    @JsonProperty("oMiscExpNotWrittenOff")
    private YearData miscExpNotWrittenOff;

    @JsonProperty("iInwardChqs")
    private int inwardCheques;

    @JsonProperty("iOutwardChqs")
    private int outwardCheques;

    @JsonProperty("iBounces")
    private int chqBounces;

    @JsonProperty("aRTRDetail")
    protected List<RTRDetail> rtrDetailList;

    @JsonProperty("aBankingStatement")
    private List<BankingStatement> bankingStatements;

    @JsonProperty("aPLAndBSAnalysis")
    private List<PLAndBSAnalysis> plAndBSAnalysis;

    @JsonProperty("oSummary")
    private CamSummary summary;

    @JsonProperty("aGstDetails")
    private List<GstDetailsList> gstDetails;
}



