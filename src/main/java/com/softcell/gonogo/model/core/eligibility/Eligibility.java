package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.verification.ApplicationVerification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "eligibilityDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Eligibility extends ApplicationVerification {

    @JsonProperty("dGrossIncmeForAppl")
    private double grossIncome;

    @JsonProperty("dFixedObligationForAppl")
    private double fixedOblgation;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("dEligibilityRoi")
    private double eligibilityRoi;

    @JsonProperty("dApprovedLoan")
    private double aprvLoan;

    @JsonProperty("dEmi")
    private double emi;

    @JsonProperty("dFoir")
    private double foir;

    @JsonProperty("oFoirDetail")
    private FoirDetail foirDetail;

    @JsonProperty("IncomeDetailList")
    private List<IncomeDetail> incomeDetailList;

    /**
     *  adding extra fields for pl
     */
    @JsonProperty("dLoanAmt")
    private double appliedLoan;

    @JsonProperty("sTenorRequested")
    private String appliedTenor;

    @JsonProperty("dmonthSal")
    private double monthlySalary;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("dMultiplier")
    private double multiplier;

    @JsonProperty("dEliAsFoir")
    private double eligibilityAsFoir;

    @JsonProperty("dFinalAmtLacks")
    private double finalAmtLacks;

    @JsonProperty("sPF")
    private String PF;

    @JsonProperty("dFinalFoir")
    private double finalFoir;

    @JsonProperty("dInsuranceAmt")
    private double insurenceAmt;

    @JsonProperty("bHLFlag")
    private boolean hlFlag;

    @JsonProperty("dElgTenor")
    private double elgTenor;

    @JsonProperty("dElgfixedOblg")
    private double elgFixedOblg;

    @JsonProperty("dElgLnAmt")
    private double elgLnAmt;

    @JsonProperty("sPolicyFOIR")
    private double policyFoir;

    @JsonProperty("dDevAmt")
    private double devAmt;

    @JsonProperty("dBalEmi")
    private double balenceEmi;

    @JsonProperty("dElgFixedObg")
    private double elgFixedObg;


    //Added for FiveStar by Somasekhar on 17/09/2018.


    @JsonProperty("dProposedSanctionAmount")
    private double proposedSanctionAmount;

    @JsonProperty("sCustomerType")
    private String customerType;

    @JsonProperty("sProductType")
    private String productType;

    //@Values(Normal / BT / Top-up / Addn)
    @JsonProperty("sScheme")
    private String scheme;

    @JsonProperty("dIncome")
    private double income;

    @JsonProperty("dValuation")
    private double valuation;

    @JsonProperty("sPurpose")
    private String purpose;

    @JsonProperty("sLnPurpose2")
    private String loanPurpose2;


    //required for FVSG
    @JsonProperty("aEligibiltyDetailList")
    private List<EligibilityDetails> eligibilityListDetails;

    @JsonProperty("sKYCRating")
    private String kycRating;

    @JsonProperty("sBorrowerRiskRating")
    private String borrowerRiskRating;

    @JsonProperty("sCumulativeLTV")
    private String cumulativeLTV;

    @JsonProperty("sCumulativeDBR")
    private String cumulativeDBR;

    @JsonProperty("sCumulativeExposure")
    private String cumulativeExposure;


    @JsonProperty("sTypeOfPropertyLegal")
    private String typeOfPropertyLegal;

    @JsonProperty("sRiskDocumentLegal")
    private String riskDocumentLegal;

    @JsonProperty("sTotalAssetCost")
    private String totalAssetCost;

    @JsonProperty("bTriggerSobreDeviation")
    private boolean triggerSobreDeviation;

    @JsonProperty("sRiskDocument")
    private String riskDocument;

    @JsonProperty("sLoanType")
    private String loanType;

    @JsonProperty("sCustSegment")
    private String custSegment;

    @JsonProperty("sCustSubSegment")
    private String custSubSegment;

    @JsonProperty("sTakeover")
    private String takeOver;

    @JsonProperty("dTakeoverAmt")
    private double takeoverAmount;

    @JsonProperty("sTopup")
    private String topup;

    @JsonProperty("sTrancheDisb")
    private String trancheDisb;

    @JsonProperty("iNoOfAdvEmis")
    private int noOfAdvEmis;

    @JsonProperty("sExecutiveSummary")
    private String executiveSummary;

    @JsonProperty("sTypeOfProperty")
    private String typeOfProperty;
}
