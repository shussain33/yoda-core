package com.softcell.gonogo.model.core.scoring.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DecisionResponse {

    @JsonProperty("RuleID")
    private Long ruleId;

    @JsonProperty("Decision")
    private String decision;

    @JsonProperty("Details")
    private List<CriteriaResult> details;

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public List<CriteriaResult> getDetails() {
        return details;
    }

    public void setDetails(List<CriteriaResult> details) {
        this.details = details;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DecisionResponse [ruleId=");
        builder.append(ruleId);
        builder.append(", decision=");
        builder.append(decision);
        builder.append(", details=");
        builder.append(details);
        builder.append("]");
        return builder.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((decision == null) ? 0 : decision.hashCode());
        result = prime * result + ((details == null) ? 0 : details.hashCode());
        result = prime * result + ((ruleId == null) ? 0 : ruleId.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DecisionResponse other = (DecisionResponse) obj;
        if (decision == null) {
            if (other.decision != null)
                return false;
        } else if (!decision.equals(other.decision))
            return false;
        if (details == null) {
            if (other.details != null)
                return false;
        } else if (!details.equals(other.details))
            return false;
        if (ruleId == null) {
            if (other.ruleId != null)
                return false;
        } else if (!ruleId.equals(other.ruleId))
            return false;
        return true;
    }

}
