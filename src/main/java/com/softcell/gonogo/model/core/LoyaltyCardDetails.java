package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 24/2/17.
 */
@Document(collection = "loyaltyCardDetails")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoyaltyCardDetails extends AuditEntity {

    @JsonProperty("oHeader")
    @NotNull(groups = {LoyaltyCardDetails.FetchGrp.class})
    @Valid()
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {LoyaltyCardDetails.FetchGrp.class})
    private String refId;

    @NotEmpty(groups = {LoyaltyCardDetails.FetchGrp.class})
    @JsonProperty("sLoyaltyCardNo")
    private String loyaltyCardNo;

    @JsonProperty("sLoyaltyCardType")
    private LoyaltyCardType loyaltyCardType;

    @JsonProperty("dLoyaltyCardPrice")
    private double loyaltyCardPrice;

    @JsonProperty("dRevisedTotalEmi")
    private double revisedTotalEmi;

    @JsonProperty("bActiveFlag")
    boolean activeFlag;


    public interface FetchGrp {

    }


}
