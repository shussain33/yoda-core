package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.*;

import java.util.HashMap;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ThirdPartyRequest {

    @NonNull
    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("s_Id")
    private String refId;

    @JsonProperty("bLead")
    private boolean lead;

    @JsonProperty("sType")
    private String type;

    @JsonProperty("oGng")
    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    @JsonProperty("aParam")
    private HashMap<String, String> param;

    //use this param for excel download
    @JsonProperty("aRefId")
    private List<String> refIdList;

}
