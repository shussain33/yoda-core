
package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LitigationSearchRecords {

    @JsonProperty("districtCourts")
    public List<DistrictCourt> districtCourts = null;
    @JsonProperty("highCourts")
    public List<HighCourt> highCourts = null;
    @JsonProperty("supremeCourt")
    public List<SupremeCourt> supremeCourt = null;
    @JsonProperty("tribunals")
    public List<Tribunal> tribunals = null;

}
