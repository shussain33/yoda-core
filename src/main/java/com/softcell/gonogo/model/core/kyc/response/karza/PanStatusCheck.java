package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg222 on 13/6/19.
 */
@Data
public class PanStatusCheck {

    @JsonProperty("dobMatch")
    private boolean dobMatch;

    @JsonProperty("duplicate")
    private boolean duplicate;

    @JsonProperty("status")
    private String status;

    @JsonProperty("nameMatch")
    private boolean nameMatch;
}
