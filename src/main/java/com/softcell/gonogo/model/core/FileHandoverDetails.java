package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author vinodk
 */
@Document(collection = "FileHandoverDetails")
public class FileHandoverDetails extends AuditEntity {

    @JsonProperty("oHeader")
    @NotNull(groups = {FileHandoverDetails.FetchGrp.class, FileHandoverDetails.InsertGrp.class})
    @Valid()
    private Header header;

    @Id
    @JsonProperty("sRefID")
    @NotEmpty(groups = {FileHandoverDetails.FetchGrp.class, FileHandoverDetails.InsertGrp.class})
    private String refId;

    @JsonProperty("dtHandoverDate")
    private Date handoverDate;

    @JsonProperty("dtRecievedDate")
    private Date receivedDate;

    @JsonProperty("sFileStatus")
    private String fileStatus;

    @JsonProperty("sHoldReason")
    private String holdReason;

    @JsonProperty("dtDisbDate")
    private Date disbursementDate;

    @JsonProperty("dtNDCDate")
    private Date ndcDate;

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }

    /**
     * @return the handoverDate
     */
    public Date getHandoverDate() {
        return handoverDate;
    }

    /**
     * @param handoverDate the handoverDate to set
     */
    public void setHandoverDate(Date handoverDate) {
        this.handoverDate = handoverDate;
    }

    /**
     * @return the receivedDate
     */
    public Date getReceivedDate() {
        return receivedDate;
    }

    /**
     * @param receivedDate the receivedDate to set
     */
    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    /**
     * @return the fileStatus
     */
    public String getFileStatus() {
        return fileStatus;
    }

    /**
     * @param fileStatus the fileStatus to set
     */
    public void setFileStatus(String fileStatus) {
        this.fileStatus = fileStatus;
    }

    /**
     * @return the holdReason
     */
    public String getHoldReason() {
        return holdReason;
    }

    /**
     * @param holdReason the holdReason to set
     */
    public void setHoldReason(String holdReason) {
        this.holdReason = holdReason;
    }

    /**
     * @return the disbursementDate
     */
    public Date getDisbursementDate() {
        return disbursementDate;
    }

    /**
     * @param disbursementDate the disbursementDate to set
     */
    public void setDisbursementDate(Date disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    /**
     * @return the ndcDate
     */
    public Date getNdcDate() {
        return ndcDate;
    }

    /**
     * @param ndcDate the ndcDate to set
     */
    public void setNdcDate(Date ndcDate) {
        this.ndcDate = ndcDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FileHandoverDetails [header=");
        builder.append(header);
        builder.append(", refId=");
        builder.append(refId);
        builder.append(", handoverDate=");
        builder.append(handoverDate);
        builder.append(", receivedDate=");
        builder.append(receivedDate);
        builder.append(", fileStatus=");
        builder.append(fileStatus);
        builder.append(", holdReason=");
        builder.append(holdReason);
        builder.append(", disbursementDate=");
        builder.append(disbursementDate);
        builder.append(", ndcDate=");
        builder.append(ndcDate);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((disbursementDate == null) ? 0 : disbursementDate.hashCode());
        result = prime * result
                + ((fileStatus == null) ? 0 : fileStatus.hashCode());
        result = prime * result
                + ((handoverDate == null) ? 0 : handoverDate.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result
                + ((holdReason == null) ? 0 : holdReason.hashCode());
        result = prime * result + ((ndcDate == null) ? 0 : ndcDate.hashCode());
        result = prime * result
                + ((receivedDate == null) ? 0 : receivedDate.hashCode());
        result = prime * result + ((refId == null) ? 0 : refId.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FileHandoverDetails other = (FileHandoverDetails) obj;
        if (disbursementDate == null) {
            if (other.disbursementDate != null)
                return false;
        } else if (!disbursementDate.equals(other.disbursementDate))
            return false;
        if (fileStatus == null) {
            if (other.fileStatus != null)
                return false;
        } else if (!fileStatus.equals(other.fileStatus))
            return false;
        if (handoverDate == null) {
            if (other.handoverDate != null)
                return false;
        } else if (!handoverDate.equals(other.handoverDate))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (holdReason == null) {
            if (other.holdReason != null)
                return false;
        } else if (!holdReason.equals(other.holdReason))
            return false;
        if (ndcDate == null) {
            if (other.ndcDate != null)
                return false;
        } else if (!ndcDate.equals(other.ndcDate))
            return false;
        if (receivedDate == null) {
            if (other.receivedDate != null)
                return false;
        } else if (!receivedDate.equals(other.receivedDate))
            return false;
        if (refId == null) {
            if (other.refId != null)
                return false;
        } else if (!refId.equals(other.refId))
            return false;
        return true;
    }

    public interface FetchGrp {
    }

    public interface InsertGrp {
    }
}
