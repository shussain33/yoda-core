package com.softcell.gonogo.model.core.kyc.response.karza.addressmappingauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Address2MatchingResponseDetails {

    @JsonProperty("Building")
    private String Building;

    @JsonProperty("Pin")
    private String Pin;

    @JsonProperty("Floor")
    private String Floor;

    @JsonProperty("District")
    private String District;

    @JsonProperty("House")
    private String House;

    @JsonProperty("locality")
    private String locality;

    @JsonProperty("State")
    private String State;

    @JsonProperty("Complex")
    private String Complex;

    @JsonProperty("Untagged")
    private String Untagged;

    @JsonProperty("C/O")
    private String CO;

    @JsonProperty("Street")
    private String Street;

}