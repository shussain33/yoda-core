package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class AddressSegment {
    @JsonProperty("co")
    private String co;

    @JsonProperty("house")
    private String house;

    @JsonProperty("street")
    private String street;

    @JsonProperty("lm")
    private String lm;

    @JsonProperty("loc")
    private String loc;

    @JsonProperty("vtc")
    private String vtc;

    @JsonProperty("subdist")
    private String subdist;

    @JsonProperty("dist")
    private String dist;

    @JsonProperty("state")
    private String state;

    @JsonProperty("pc")
    private String pc;

    @JsonProperty("po")
    private String po;

    @JsonProperty("country")
    private String country;

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLm() {
        return lm;
    }

    public void setLm(String lm) {
        this.lm = lm;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getVtc() {
        return vtc;
    }

    public void setVtc(String vtc) {
        this.vtc = vtc;
    }

    public String getSubdist() {
        return subdist;
    }

    public void setSubdist(String subdist) {
        this.subdist = subdist;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AddressSegment [co=");
        builder.append(co);
        builder.append(", house=");
        builder.append(house);
        builder.append(", street=");
        builder.append(street);
        builder.append(", lm=");
        builder.append(lm);
        builder.append(", loc=");
        builder.append(loc);
        builder.append(", vtc=");
        builder.append(vtc);
        builder.append(", subdist=");
        builder.append(subdist);
        builder.append(", dist=");
        builder.append(dist);
        builder.append(", state=");
        builder.append(state);
        builder.append(", pc=");
        builder.append(pc);
        builder.append(", po=");
        builder.append(po);
        builder.append(", country=");
        builder.append(country);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((co == null) ? 0 : co.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + ((dist == null) ? 0 : dist.hashCode());
        result = prime * result + ((house == null) ? 0 : house.hashCode());
        result = prime * result + ((lm == null) ? 0 : lm.hashCode());
        result = prime * result + ((loc == null) ? 0 : loc.hashCode());
        result = prime * result + ((pc == null) ? 0 : pc.hashCode());
        result = prime * result + ((po == null) ? 0 : po.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((street == null) ? 0 : street.hashCode());
        result = prime * result + ((subdist == null) ? 0 : subdist.hashCode());
        result = prime * result + ((vtc == null) ? 0 : vtc.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof AddressSegment))
            return false;
        AddressSegment other = (AddressSegment) obj;
        if (co == null) {
            if (other.co != null)
                return false;
        } else if (!co.equals(other.co))
            return false;
        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;
        if (dist == null) {
            if (other.dist != null)
                return false;
        } else if (!dist.equals(other.dist))
            return false;
        if (house == null) {
            if (other.house != null)
                return false;
        } else if (!house.equals(other.house))
            return false;
        if (lm == null) {
            if (other.lm != null)
                return false;
        } else if (!lm.equals(other.lm))
            return false;
        if (loc == null) {
            if (other.loc != null)
                return false;
        } else if (!loc.equals(other.loc))
            return false;
        if (pc == null) {
            if (other.pc != null)
                return false;
        } else if (!pc.equals(other.pc))
            return false;
        if (po == null) {
            if (other.po != null)
                return false;
        } else if (!po.equals(other.po))
            return false;
        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;
        if (street == null) {
            if (other.street != null)
                return false;
        } else if (!street.equals(other.street))
            return false;
        if (subdist == null) {
            if (other.subdist != null)
                return false;
        } else if (!subdist.equals(other.subdist))
            return false;
        if (vtc == null) {
            if (other.vtc != null)
                return false;
        } else if (!vtc.equals(other.vtc))
            return false;
        return true;
    }
}
