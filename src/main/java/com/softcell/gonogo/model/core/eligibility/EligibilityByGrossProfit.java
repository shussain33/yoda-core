package com.softcell.gonogo.model.core.eligibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 4/3/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EligibilityByGrossProfit {


    @JsonProperty("bIsAudited")
    private boolean Audited;

    @JsonProperty("dTurnover")
    private double turnover;

    @JsonProperty("dTurnoverPercentage")
    private double turnoverPercentage;

    @JsonProperty("dGrossMargin")
    private double grossMargin;

    @JsonProperty("dLowerPercent")
    private double dLowerPercent;

    @JsonProperty("dFixedObligation")
    private double fixedObligation;

    @JsonProperty("dRentalIncome")
    private double rentalIncome;

    @JsonProperty("dGrossIncome")
    private double grossIncome;

}
