package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication.EmailAdditionalInfo;
import com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication.EmailData;
import lombok.Data;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Email {

    @JsonProperty("data")
    private EmailData data;

    @JsonProperty("result")
    private Boolean result;

    @JsonProperty("additionalInfo")
    private EmailAdditionalInfo additionalInfo;
}
