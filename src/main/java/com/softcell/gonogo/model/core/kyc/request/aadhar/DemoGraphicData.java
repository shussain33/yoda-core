package com.softcell.gonogo.model.core.kyc.request.aadhar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
@XmlAccessorType(XmlAccessType.FIELD)
public class DemoGraphicData {

	@XmlElement(name="Pi")
	PersonalIdentity pi;
	@XmlElement(name="Pa")
	PersonalAddress pa;
	@XmlElement(name="Pfa")
	PersonalFullAddress pfa;
	@XmlAttribute(name="lang")
	String lang;
	
	public PersonalIdentity getPi() {
		return pi;
	}
	public void setPi(PersonalIdentity pi) {
		this.pi = pi;
	}
	public PersonalAddress getPa() {
		return pa;
	}
	public void setPa(PersonalAddress pa) {
		this.pa = pa;
	}
	public PersonalFullAddress getPfa() {
		return pfa;
	}
	public void setPfa(PersonalFullAddress pfa) {
		this.pfa = pfa;
	}
	
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	@Override
	public String toString() {
		return "DemoGraphicData [pi=" + pi + ", pa=" + pa + ", pfa=" + pfa
				+ ", lang=" + lang + "]";
	}
	
	
	
	
	
}
