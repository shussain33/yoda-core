package com.softcell.gonogo.model.core.ocr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.FaceMatchDetails;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.UploadFileDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ssg228 on 13/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OCRDetailsObj {

    @JsonProperty("sApplicantID")
    protected String applicantId;

    @JsonProperty("oApplicantName")
    protected Name applicantName;

    @JsonProperty("oUploadFileDetails")
    public UploadFileDetails uploadFileDetails;

    @JsonProperty("sDocName")
    public String docName;

    @JsonProperty("sDocType")
    public String docType;

    @JsonProperty("aImageQualityData")
    public List<HashMap<String, String>> imageQualityData;

    @JsonProperty("aOCRData")
    public List<OCRData> ocrData;

    @JsonProperty("oRCUData")
    public RCUData rcuData;

    @JsonProperty("sApplType")
    protected String applType;

    @JsonProperty("sApiValidationError")
    public String apiValidationError;

    @JsonProperty("oFaceMatchDetails")
    private FaceMatchDetails faceMatchDetails;

    @JsonProperty("iPercentMatch")
    protected double percentMatch;

    @JsonProperty("oApplicantAddress")
    private CustomerAddress applicantAddress;

    @JsonProperty("sConstitutionType")
    private String constitutionType;

    @JsonProperty("sAcknowledgementId")
    private String acknowledgementId;
}
