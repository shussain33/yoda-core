package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DetailedProfileResponseDetails {

    @JsonProperty("alerts")
    public DetailedProfileAlerts detailedProfileAlerts;

    @JsonProperty("charges")
    public List<DetailedProfileCharges> detailedProfileChargesList;

    @JsonProperty("company")
    public DetailedProfileCompany detailedProfileCompany;

    @JsonProperty("directors")
    public List<DetailedProfileDirectors> detailedProfileDirectorsList;


}