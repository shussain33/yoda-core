package com.softcell.gonogo.model.core.kyc.response.karza.emailauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by ssg0302 on 3/9/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailCompanyInfoUsrDirectorMatch {


    @JsonProperty("name")
    private String name;

    @JsonProperty("match")
    private Boolean match;

    @JsonProperty("score")
    private double score;

    @JsonProperty("org_name")
    private String org_name;

    @JsonProperty("orgName")
    private String orgName;
}
