package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by ssg0268 on 17/8/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Ignore
public class PropertyFileData {
    //Valuation Data
    @JsonProperty("Unit_Area")
    private String unitArea = "Square Feet";

    @JsonProperty("Builtup Area")
    private String builtUpArea ;

    @JsonProperty("Land Cost")
    private int landCost ;

    @JsonProperty("Electricity Board/Municipal Charges")
    private String electricityBoard = "";

    @JsonProperty("Stage of Cstructi/ Improvement/ Extensi")
    private String improvement = "" ;

    @JsonProperty("Society Charges")
    private String societyCharges = "" ;

    @JsonProperty("Is_the_unit_booked")
    private String isTheUnitBooked = "Y";

    @JsonProperty("Ctractor Estimate")
    private String ctractorEstimate = "" ;

    @JsonProperty("Other Cost")
    private String otherCost = "" ;

    @JsonProperty("Other Cost Desc")
    private String otherCostDesc = "" ;

    @JsonProperty("Landmark")
    private String landMark = "" ;

    @JsonProperty("Latitude Value")
    private String latitudeValue = "" ;

    @JsonProperty("Lgitude Value")
    private String lagitudeValue = "" ;

    @JsonProperty("Bounded by North")
    private String boundedByNorth = "" ;

    @JsonProperty("Bounded by South")
    private String boundedBySouth = "" ;

    @JsonProperty("Bounded by East")
    private String boundedByEast = "" ;

    @JsonProperty("Bounded by West")
    private String boundedByWest = "" ;

    @JsonProperty("marketValueTRP1")
    private String marketValueTRP1;

    @JsonProperty("marketValueTRP2")
    private String marketValueTRP2;

    @JsonProperty("Build date")
    private String buildDate ;

    @JsonProperty("Document Number")
    private String  documentNumber = "NA" ;

    @JsonProperty("Document_Type")
    private String documentType = "SALE_DEED";

    @JsonProperty("Document_Date")
    private String documentDate = "";

    @JsonProperty("Sub_Registrar")
    private String subRegister = "";

    @JsonProperty("State_Desc")
    private String stateDesc = "";

    @JsonProperty("District_Desc")
    private String districtDesc = "";

    @JsonProperty("Taluka")
    private String taluka = "";

    @JsonProperty("Extent Operati Charge")
    private String extentOperationCharge = "";

    @JsonProperty("Others")
    private String others = "";

    @JsonProperty("First Lien Value")
    private String firstLienValue = "";

    @JsonProperty("First Lien By")
    private String firstLienBy = "";

    @JsonProperty("Secd Lien Value ")
    private String secdLienValue = "";

    @JsonProperty("Secd Lien By")
    private String secdLienBy = "";

    @JsonProperty("Third Lien By")
    private String thirdLienBy = "";

    @JsonProperty("Third Lien Value")
    private String thirdLienValue = "";

    @JsonProperty("Fourth Lien Value")
    private String fourthLienValue = "";

    @JsonProperty("Fourth Lien By")
    private String fourthLienBy = "";

    @JsonProperty("Fifth Lien Value")
    private String fifthLienValue = "";

    @JsonProperty("Applid")
    private String applId = "";

    @JsonProperty("StateDesc")
    private String  collateralState = "";

    @JsonProperty("DistrictDesc")
    private String  collateralDesc = "";

    @JsonProperty("City")
    private String  city ="" ;

    @JsonProperty("Prop Valuati Type")
    private String  propValuatiType ="" ;

    @JsonProperty("Trans From Stati")
    private String  transFromStati ="" ;

    @JsonProperty("Lawyer Name")
    private String  LawyerName ="" ;

    @JsonProperty("Investment/Self-Used")
    private String  investment = "SELF USED";

    @JsonProperty("MortgageType")
    private String  mortgageType = "Equitable" ;


    @JsonProperty("Street")
    private String  street ="" ;

    @JsonProperty("Locality")
    private String  locality = "";

    @JsonProperty("Survey")
    private String  survey = "" ;

    @JsonProperty("PlotNo")
    private String  plotNo = "";

    @JsonProperty("Floor No")
    private String  floorNo = "";

    @JsonProperty("Block No")
    private String  blockNo = "";

    @JsonProperty("Building No")
    private String  buildingNo = "";

    @JsonProperty("Stage/Sector No/Ward")
    private String  stage = "";

    @JsonProperty("Village")
    private String  village = "";

    @JsonProperty("Town")
    private String  town = "";

    @JsonProperty("Property Type")
    private String  propertyType ;

    @JsonProperty("PropertyTxnType")
    private String  propertyTxnType = "2014 GUJ-NONBT PURCHASE IN RESALE FROM BUILDER" ;

    @JsonProperty("NoOfProperty")
    private String  noOfProperty = "";

    @JsonProperty("AppraisalDate")
    private String  appraisalDate ;

    @JsonProperty("AppraisalBy")
    private String  appraisalBy = "";

    @JsonProperty("Primary")
    private String  primary  = "PRIMARY" ;

    @JsonProperty("BHK")
    private String  bhk = "0";

    @JsonProperty("Arrangement_with_Corporate")
    private String  arrangementWithCorporate = "N";

    @JsonProperty("Carpet Area")
    private String  carpetArea = "";

    @JsonProperty("Agency Name")
    private String  agencyName = "ICICI";

    @JsonProperty("Disbursement %")
    private String  disbursment = "100";

    @JsonProperty("Verification For")
    private String  verificationFor = "P";

    @JsonProperty("Stage Completion(%)")
    private String  stageCompletion = "100";

    @JsonProperty("HouseNo")
    private String  houseNo = "" ;

    @JsonProperty("Nearest Train Stati")
    private String  nearestTrainStation = "" ;

    //value will be Y/N value depend on applicant And owner Name
    @JsonProperty("SOLE_OWNER_FLAG")
    private String soleOwnerFlag;

    @JsonProperty("IS_THE_LEGAL_TITLE_CLEAR")
    private String legalTitleClear = "";

    @JsonProperty("DO_YOU_PROPOSE_TO_RENT_THE_UNIT")
    private String proposeToRent = "";

    //ToDoMapping from eligibility value
    @JsonProperty("LTV(Property Cost)")
    private double ltv ;

    @JsonProperty("LTV(Market Value)")
    private double ltvMarketValue ;

    @JsonProperty("Project Name")
    private String projectName = "";

    @JsonProperty("Building Name")
    private String buildingName = "";

    @JsonProperty("WingName")
    private String wingName = "";

    @JsonProperty("BuilderName")
    private String builderName = "";

    @JsonProperty("Recommended ADF (%)")
    private String recommendedAdf = "";

    @JsonProperty("DateOfVisit")
    private String dateOfVisit = "";

    @JsonProperty("Recommended Normal (%)")
    private String recommendedNormal = "";

    @JsonProperty("Remarks")
    private String remarks = "";

}
