package com.softcell.gonogo.model.core.kyc.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author yogeshb
 */
public class Errors {
    @SerializedName("CODE")
    @XmlElement(name="CODE")
    @JsonProperty("CODE")
    private String code;

    @SerializedName("DESCRIPTION")
    @XmlElement(name="DESCRIPTION")
    @JsonProperty("DESCRIPTION")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Errors [code=" + code + ", description=" + description + "]";
    }


}