package com.softcell.gonogo.model.core.kyc.response.karza.gstauthentication.gspgstinauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ComplianceStatus {

    @JsonProperty("is_any_delay")
    private String is_any_delay;

    @JsonProperty("is_defaulter")
    private String is_defaulter;
}