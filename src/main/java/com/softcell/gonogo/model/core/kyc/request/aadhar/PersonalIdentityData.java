package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Transient;

import java.util.List;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonalIdentityData {

    @JsonProperty("TIMESTAMP")
    private String timeStamp;

    @JsonProperty("VER")
    private String ver;

    /**
     * Newly added for ready made pid block.
     */
    @JsonProperty("ENCPIDBLOCK")
    @Transient
    private String encryptedPIDBlock;

    @JsonProperty("DEMO")
    private DemographicDetails demo;

    @JsonProperty("PIN-VALUE")
    private PinValue pinValue;

    @JsonProperty("BIOS")
    private List<BiometricType> bios;

    public static Builder builder() {
        return new Builder();
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getVer() {
        return ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public DemographicDetails getDemo() {
        return demo;
    }

    public void setDemo(DemographicDetails demo) {
        this.demo = demo;
    }

    public PinValue getPinValue() {
        return pinValue;
    }

    public void setPinValue(PinValue pinValue) {
        this.pinValue = pinValue;
    }

    /**
     * @return the bios
     */
    public List<BiometricType> getBios() {
        return bios;
    }

    /**
     * @param bios the bios to set
     */
    public void setBios(List<BiometricType> bios) {
        this.bios = bios;
    }

    /**
     * @return
     */
    public String getEncryptedPIDBlock() {
        return encryptedPIDBlock;
    }

    /**
     * @param encryptedPIDBlock
     */
    public void setEncryptedPIDBlock(String encryptedPIDBlock) {
        this.encryptedPIDBlock = encryptedPIDBlock;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("PersonalIdentityData [timeStamp=");
        builder.append(timeStamp);
        builder.append(", ver=");
        builder.append(ver);
        builder.append(", encryptedPIDBlock=");
        builder.append(encryptedPIDBlock);
        builder.append(", demo=");
        builder.append(demo);
        builder.append(", pinValue=");
        builder.append(pinValue);
        builder.append(", bios=");
        builder.append(bios);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bios == null) ? 0 : bios.hashCode());
        result = prime * result + ((demo == null) ? 0 : demo.hashCode());
        result = prime
                * result
                + ((encryptedPIDBlock == null) ? 0 : encryptedPIDBlock
                .hashCode());
        result = prime * result
                + ((pinValue == null) ? 0 : pinValue.hashCode());
        result = prime * result
                + ((timeStamp == null) ? 0 : timeStamp.hashCode());
        result = prime * result + ((ver == null) ? 0 : ver.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PersonalIdentityData))
            return false;
        PersonalIdentityData other = (PersonalIdentityData) obj;
        if (bios == null) {
            if (other.bios != null)
                return false;
        } else if (!bios.equals(other.bios))
            return false;
        if (demo == null) {
            if (other.demo != null)
                return false;
        } else if (!demo.equals(other.demo))
            return false;
        if (encryptedPIDBlock == null) {
            if (other.encryptedPIDBlock != null)
                return false;
        } else if (!encryptedPIDBlock.equals(other.encryptedPIDBlock))
            return false;
        if (pinValue == null) {
            if (other.pinValue != null)
                return false;
        } else if (!pinValue.equals(other.pinValue))
            return false;
        if (timeStamp == null) {
            if (other.timeStamp != null)
                return false;
        } else if (!timeStamp.equals(other.timeStamp))
            return false;
        if (ver == null) {
            if (other.ver != null)
                return false;
        } else if (!ver.equals(other.ver))
            return false;
        return true;
    }

    public static class Builder {
        private PersonalIdentityData personalIdentityData = new PersonalIdentityData();

        public PersonalIdentityData build() {
            return personalIdentityData;
        }

        public Builder timeStamp(String timeStamp) {
            this.personalIdentityData.timeStamp = timeStamp;
            return this;
        }


        public Builder ver(String ver) {
            this.personalIdentityData.ver = ver;
            return this;
        }


        public Builder encryptedPIDBlock(String encryptedPIDBlock) {
            this.personalIdentityData.encryptedPIDBlock = encryptedPIDBlock;
            return this;
        }

        public Builder demo(DemographicDetails demo) {
            this.personalIdentityData.demo = demo;
            return this;
        }

        public Builder pinValue(PinValue pinValue) {
            this.personalIdentityData.pinValue = pinValue;
            return this;
        }

        public Builder bios(List<BiometricType> bios) {
            this.personalIdentityData.bios = bios;
            return this;
        }

    }
}
