package com.softcell.gonogo.model.core.kyc.response.aadhar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.Errors;

import java.util.List;

/**
 * @author yogeshb
 */
public class AadharResponseDetails {

    @JsonProperty("UIDAI-STATUS")
    private String uidaiStatus;

    @JsonProperty("AADHAR-RESPONSE")
    private AadharResponse aadharResponse;

    @JsonProperty("ERRORS")
    private List<Errors> errors;

    public String getUidaiStatus() {
        return uidaiStatus;
    }

    public void setUidaiStatus(String uidaiStatus) {
        this.uidaiStatus = uidaiStatus;
    }

    public AadharResponse getAadharResponse() {
        return aadharResponse;
    }

    public void setAadharResponse(AadharResponse aadharResponse) {
        this.aadharResponse = aadharResponse;
    }


    public List<Errors> getErrors() {
        return errors;
    }

    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "AadharResponseDetails [uidaiStatus=" + uidaiStatus
                + ", aadharResponse=" + aadharResponse + ", errors=" + errors
                + "]";
    }


}
