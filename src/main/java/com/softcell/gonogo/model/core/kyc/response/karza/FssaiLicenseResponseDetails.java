package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class FssaiLicenseResponseDetails {

    @JsonProperty("Status")
    private String Status;

    @JsonProperty("LicType")
    private String LicType;

    @JsonProperty("LicNO")
    private String LicNO;

    @JsonProperty("FirmName")
    private String FirmName;

    @JsonProperty("Address")
    private String Address;

}