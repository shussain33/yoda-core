package com.softcell.gonogo.model.core.request.scoring;


public class MatchResult {

    private String exactMatch;

    public String getExactMatch() {
        return exactMatch;
    }

    public void setExactMatch(String exactMatch) {
        this.exactMatch = exactMatch;
    }

    @Override
    public String toString() {
        return "MatchResult [exactMatch=" + exactMatch + "]";
    }
}
