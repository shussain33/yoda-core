package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.passbookauthentication.PassbookDetails;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 06/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ESTDetails {

    @JsonProperty("est_name")
    private String est_name;

    @JsonProperty("doe_epf")
    private String doe_epf;

    @JsonProperty("office")
    private String office;

    @JsonProperty("doj_epf")
    private String doj_epf;

    @JsonProperty("doe_eps")
    private String doe_eps;

    @JsonProperty("member_id")
    private String member_id;

    @JsonProperty("passbook")
    public List<PassbookDetails> passbookDetailsList;

}