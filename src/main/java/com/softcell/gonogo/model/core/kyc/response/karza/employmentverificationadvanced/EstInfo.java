package com.softcell.gonogo.model.core.kyc.response.karza.employmentverificationadvanced;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by ssg0302 on 10/8/20.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EstInfo {

    @JsonProperty("address")
    private String address;

    @JsonProperty("contactNo")
    private String contactNo;

    @JsonProperty("emailId")
    private String emailId;

    @JsonProperty("estId")
    private String estId;
}
