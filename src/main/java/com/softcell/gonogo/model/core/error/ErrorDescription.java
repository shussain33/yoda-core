package com.softcell.gonogo.model.core.error;

/**
 * This model is used for populate errors occured in masterupload.
 *
 * @author yogeshb
 */
public class ErrorDescription {
    private long recordNumber;
    private String row;
    private String reason;

    /**
     * @return the recordNumber
     */
    public long getRecordNumber() {
        return recordNumber;
    }

    /**
     * @param recordNumber the recordNumber to set
     */
    public void setRecordNumber(long recordNumber) {
        this.recordNumber = recordNumber;
    }

    /**
     * @return the row
     */
    public String getRow() {
        return row;
    }

    /**
     * @param row the row to set
     */
    public void setRow(String row) {
        this.row = row;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ErrorDescription [recordNumber=" + recordNumber + ", row="
                + row + ", reason=" + reason + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((reason == null) ? 0 : reason.hashCode());
        result = prime * result + (int) (recordNumber ^ (recordNumber >>> 32));
        result = prime * result + ((row == null) ? 0 : row.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ErrorDescription))
            return false;
        ErrorDescription other = (ErrorDescription) obj;
        if (reason == null) {
            if (other.reason != null)
                return false;
        } else if (!reason.equals(other.reason))
            return false;
        if (recordNumber != other.recordNumber)
            return false;
        if (row == null) {
            if (other.row != null)
                return false;
        } else if (!row.equals(other.row))
            return false;
        return true;
    }
}
