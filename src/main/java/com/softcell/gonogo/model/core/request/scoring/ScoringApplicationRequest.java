package com.softcell.gonogo.model.core.request.scoring;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.creditVidya.UserProfileResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.DedupeApplicationDetails;
import com.softcell.gonogo.model.request.GngMasterFields;
import com.softcell.gonogo.model.request.WorkflowFields;
import com.softcell.gonogo.model.saathi.SaathiResponse;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Main Request class for Scoring Application.
 *
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "ScoringRequest")
public class ScoringApplicationRequest extends AuditEntity {

    @Id
    @JsonIgnore
    private String gngRefId;

    @JsonIgnore
    private Date dateTime = new Date();

    /**
     * scoring header
     */
    @JsonProperty("HEADER")
    ScoringHeader header;

    /**
     * GoNOGo Request json object
     */
    @JsonProperty("REQUEST")
    ApplicationRequest request;

    @JsonProperty("CO_APPLICANT_REQUEST")
    List<ScoringApplicationRequest> coApplicationRequests;

    /**
     * Multibureau Cibil Response to Scoring
     */
    @JsonProperty("CIBIL_RESPONSE")
    private Object cibil;

    /**
     * Multibureau experian Response to Scoring
     */
    @JsonProperty("EXPERIAN_RESPONSE")
    private Object experian;

    /**
     * Multibureau highmark Response to Scoring
     */
    @JsonProperty("HIGHMARK_RESPONSE")
    private Object highmark;

    /**
     * Multibureau equifax Response to Scoring
     */
    @JsonProperty("EQUIFAX_RESPONSE")
    private Object equifax;

    /**
     * Multibureau mergeReport Response to Scoring
     */
    @JsonProperty("MERGED_RESPONSE")
    private Object mergeReport;

    /**
     * Addition data to Scoring
     */
    @JsonProperty("METADATA")
    private Map<String, List<Map<String, String>>> metaData;

    /**
     * Addition data to Scoring
     */
    @JsonProperty("WORKFLOW_FIELDS")
    private WorkflowFields workflowFields;

    @JsonProperty("SAATHI_RESPONSE")
    public SaathiResponse saathiResponse;

    @JsonProperty("CREDIT_VIDYA_RESPONSE")
    public UserProfileResponse creditVidyaResponse;

    // this list is used to store dedupe application details
    @JsonProperty("DEDUPE_APPLICATION_DETAILS")
    private List<DedupeApplicationDetails> dedupeApplicationDetails;

    @JsonProperty("GNG_MASTER_FIELDS")
    private GngMasterFields gngMasterFields;


    public GngMasterFields getGngMasterFields() {
        return gngMasterFields;
    }

    public void setGngMasterFields(GngMasterFields gngMasterFields) {
        this.gngMasterFields = gngMasterFields;
    }

    public List<ScoringApplicationRequest> getCoApplicationRequests() {
        return coApplicationRequests;
    }

    public void setCoApplicationRequests(
            List<ScoringApplicationRequest> coApplicationRequests) {
        this.coApplicationRequests = coApplicationRequests;
    }

    public WorkflowFields getWorkflowFields() {
        return workflowFields;
    }

    public void setWorkflowFields(WorkflowFields workflowFields) {
        this.workflowFields = workflowFields;
    }

    public ScoringHeader getHeader() {
        return header;
    }

    public void setHeader(ScoringHeader header) {
        this.header = header;
    }

    public ApplicationRequest getRequest() {
        return request;
    }

    public void setRequest(ApplicationRequest request) {
        this.request = request;
    }

    public Object getCibil() {
        return cibil;
    }

    public void setCibil(Object cibil) {
        this.cibil = cibil;
    }

    public Object getExperian() {
        return experian;
    }

    public void setExperian(Object experian) {
        this.experian = experian;
    }

    public Object getHighmark() {
        return highmark;
    }

    public void setHighmark(Object highmark) {
        this.highmark = highmark;
    }

    public Object getEquifax() {
        return equifax;
    }


    public void setEquifax(Object equifax) {
        this.equifax = equifax;
    }

    public Object getMergeReport() {
        return mergeReport;
    }

    public void setMergeReport(Object mergeReport) {
        this.mergeReport = mergeReport;
    }

    public Map<String, List<Map<String, String>>> getMetaData() {
        return metaData;
    }

    public void setMetaData(Map<String, List<Map<String, String>>> metaData) {
        this.metaData = metaData;
    }

    public String getGngRefId() {
        return gngRefId;
    }

    public void setGngRefId(String gngRefId) {
        this.gngRefId = gngRefId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public UserProfileResponse getCreditVidyaResponse() {
        return creditVidyaResponse;
    }

    public void setCreditVidyaResponse(UserProfileResponse creditVidyaResponse) {
        this.creditVidyaResponse = creditVidyaResponse;
    }

    public SaathiResponse getSaathiResponse() {
        return saathiResponse;
    }

    public void setSaathiResponse(SaathiResponse saathiResponse) {
        this.saathiResponse = saathiResponse;
    }

    public List<DedupeApplicationDetails> getDedupeApplicationDetails() {
        return dedupeApplicationDetails;
    }

    public void setDedupeApplicationDetails(List<DedupeApplicationDetails> dedupeApplicationDetails) {
        this.dedupeApplicationDetails = dedupeApplicationDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ScoringApplicationRequest that = (ScoringApplicationRequest) o;

        if (gngRefId != null ? !gngRefId.equals(that.gngRefId) : that.gngRefId != null) return false;
        if (dateTime != null ? !dateTime.equals(that.dateTime) : that.dateTime != null) return false;
        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (request != null ? !request.equals(that.request) : that.request != null) return false;
        if (coApplicationRequests != null ? !coApplicationRequests.equals(that.coApplicationRequests) : that.coApplicationRequests != null)
            return false;
        if (cibil != null ? !cibil.equals(that.cibil) : that.cibil != null) return false;
        if (experian != null ? !experian.equals(that.experian) : that.experian != null) return false;
        if (highmark != null ? !highmark.equals(that.highmark) : that.highmark != null) return false;
        if (equifax != null ? !equifax.equals(that.equifax) : that.equifax != null) return false;
        if (mergeReport != null ? !mergeReport.equals(that.mergeReport) : that.mergeReport != null) return false;
        if (metaData != null ? !metaData.equals(that.metaData) : that.metaData != null) return false;
        if (workflowFields != null ? !workflowFields.equals(that.workflowFields) : that.workflowFields != null)
            return false;
        if (saathiResponse != null ? !saathiResponse.equals(that.saathiResponse) : that.saathiResponse != null)
            return false;
        if (creditVidyaResponse != null ? !creditVidyaResponse.equals(that.creditVidyaResponse) : that.creditVidyaResponse != null)
            return false;
        if (dedupeApplicationDetails != null ? !dedupeApplicationDetails.equals(that.dedupeApplicationDetails) : that.dedupeApplicationDetails != null)
            return false;
        return !(gngMasterFields != null ? !gngMasterFields.equals(that.gngMasterFields) : that.gngMasterFields != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (gngRefId != null ? gngRefId.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (header != null ? header.hashCode() : 0);
        result = 31 * result + (request != null ? request.hashCode() : 0);
        result = 31 * result + (coApplicationRequests != null ? coApplicationRequests.hashCode() : 0);
        result = 31 * result + (cibil != null ? cibil.hashCode() : 0);
        result = 31 * result + (experian != null ? experian.hashCode() : 0);
        result = 31 * result + (highmark != null ? highmark.hashCode() : 0);
        result = 31 * result + (equifax != null ? equifax.hashCode() : 0);
        result = 31 * result + (mergeReport != null ? mergeReport.hashCode() : 0);
        result = 31 * result + (metaData != null ? metaData.hashCode() : 0);
        result = 31 * result + (workflowFields != null ? workflowFields.hashCode() : 0);
        result = 31 * result + (saathiResponse != null ? saathiResponse.hashCode() : 0);
        result = 31 * result + (creditVidyaResponse != null ? creditVidyaResponse.hashCode() : 0);
        result = 31 * result + (dedupeApplicationDetails != null ? dedupeApplicationDetails.hashCode() : 0);
        result = 31 * result + (gngMasterFields != null ? gngMasterFields.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ScoringApplicationRequest{");
        sb.append("gngRefId='").append(gngRefId).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", header=").append(header);
        sb.append(", request=").append(request);
        sb.append(", coApplicationRequests=").append(coApplicationRequests);
        sb.append(", cibil=").append(cibil);
        sb.append(", experian=").append(experian);
        sb.append(", highmark=").append(highmark);
        sb.append(", equifax=").append(equifax);
        sb.append(", mergeReport=").append(mergeReport);
        sb.append(", metaData=").append(metaData);
        sb.append(", workflowFields=").append(workflowFields);
        sb.append(", saathiResponse=").append(saathiResponse);
        sb.append(", creditVidyaResponse=").append(creditVidyaResponse);
        sb.append(", dedupeApplicationDetails=").append(dedupeApplicationDetails);
        sb.append(", gngMasterFields=").append(gngMasterFields);
        sb.append('}');
        return sb.toString();
    }

}