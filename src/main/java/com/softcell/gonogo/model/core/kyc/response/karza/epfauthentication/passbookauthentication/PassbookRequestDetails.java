package com.softcell.gonogo.model.core.kyc.response.karza.epfauthentication.passbookauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PassbookRequestDetails {

    @JsonProperty("request_id")
    private String request_id;

    @JsonProperty("otp")
    private String otp;

}