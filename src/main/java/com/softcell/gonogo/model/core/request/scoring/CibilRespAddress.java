package com.softcell.gonogo.model.core.request.scoring;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

/**
 * @author yogeshb
 */
public class CibilRespAddress {


    private String addressLine1;


    private String addressLine2;


    private String addressLine3;


    private String addressLine4;


    private String addressLine5;


    private String stateCode;


    private String pinCode;


    private String addressCategory;


    private String residenceCode;


    private String dateReported;


    private String memberShortName;


    private String enrichedThroughtEnquiry;

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getAddressLine5() {
        return addressLine5;
    }

    public void setAddressLine5(String addressLine5) {
        this.addressLine5 = addressLine5;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getAddressCategory() {
        return addressCategory;
    }

    public void setAddressCategory(String addressCategory) {
        this.addressCategory = addressCategory;
    }

    public String getResidenceCode() {
        return residenceCode;
    }

    public void setResidenceCode(String residenceCode) {
        this.residenceCode = residenceCode;
    }

    public String getDateReported() {
        return dateReported;
    }

    public void setDateReported(String dateReported) {
        this.dateReported = dateReported;
    }

    public String getMemberShortName() {
        return memberShortName;
    }

    public void setMemberShortName(String memberShortName) {
        this.memberShortName = memberShortName;
    }

    public String getEnrichedThroughtEnquiry() {
        return enrichedThroughtEnquiry;
    }

    public void setEnrichedThroughtEnquiry(String enrichedThroughtEnquiry) {
        this.enrichedThroughtEnquiry = enrichedThroughtEnquiry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CibilRespAddress)) return false;
        CibilRespAddress that = (CibilRespAddress) o;
        return Objects.equals(addressLine1, that.addressLine1) &&
                Objects.equals(addressLine2, that.addressLine2) &&
                Objects.equals(addressLine3, that.addressLine3) &&
                Objects.equals(addressLine4, that.addressLine4) &&
                Objects.equals(addressLine5, that.addressLine5) &&
                Objects.equals(stateCode, that.stateCode) &&
                Objects.equals(pinCode, that.pinCode) &&
                Objects.equals(addressCategory, that.addressCategory) &&
                Objects.equals(residenceCode, that.residenceCode) &&
                Objects.equals(dateReported, that.dateReported) &&
                Objects.equals(memberShortName, that.memberShortName) &&
                Objects.equals(enrichedThroughtEnquiry, that.enrichedThroughtEnquiry);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressLine1, addressLine2, addressLine3, addressLine4, addressLine5, stateCode, pinCode, addressCategory, residenceCode, dateReported, memberShortName, enrichedThroughtEnquiry);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("addressLine1", addressLine1)
                .append("addressLine2", addressLine2)
                .append("addressLine3", addressLine3)
                .append("addressLine4", addressLine4)
                .append("addressLine5", addressLine5)
                .append("stateCode", stateCode)
                .append("pinCode", pinCode)
                .append("addressCategory", addressCategory)
                .append("residenceCode", residenceCode)
                .append("dateReported", dateReported)
                .append("memberShortName", memberShortName)
                .append("enrichedThroughtEnquiry", enrichedThroughtEnquiry)
                .toString();
    }
}
