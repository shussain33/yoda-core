package com.softcell.gonogo.model.core.digitization;

import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.creditVidya.Insurance;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.ops.SanctionConditionDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg222 on 11/1/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SanctionReport {
    private String refId;

    private String termOfloans;

    private String sanctionLoanAmt;

    private String sanctionAmtBt;

    private String loanPurpose;

    private String typeofloan;

    private String emi;

    private String location;

    private String btEmiAmt;

    private String topUpEmiAmt;

    private List<Collateral> collateralList;

    private String residentialAddress;

    private Applicant applicant ;

    private String creditSubmit;

    private List<CoApplicant> coApplicantList;

    private Repayment repayment;

    private CamSummary camSummary;

    private DisbursementMemo disbursementMemo;

    private LoanCharges loanCharges;

    private List<SanctionConditionDetails> sanctionConditionDetails;

    private String logoId;

    private String date;

    private String imdAmt;

    private String insurenceTypeIcici;

    private String premiumIcici;

    private int age;

    private String email;

    private String mobile;

    private String  payableMonthlyAmount;

    List<InsurancePolicy> insurancePolicyList ;

    private NomineeInfo nomineeInfo;

    private Double sumAssured;

    private String religareSumAssured;

    boolean coPayAvailable;

    private String htmlData;

    private String repaymentMode;

    private String frequencyOfInstallments;

    private double noOfVehicles;

    private double intrestRate;


    private String vehicleType;

    private double otherAccessoryAmt;

    private double fundingPercnt;

    private String manufactureDate;

    private double amcCost;

    private double warrentyCost;

    private String sellerName;

    private String agreementDate;

    private String agreementEndDate;

    private String branch;

    private String emiStartDate;

    private  String emiEndDate;

    private int months ;

    private  String disbDate;

    private String numberValueInWords;

    private String currentDateWithMonths ;
    private String currentDate ;
    private String disbValueInWords;
    private String finalLoanApprovedValueInWords;
    private String currentStageId;
    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private String product;
    private  OptDetails optDetails;
    private InsurancePolicy insurancePolicy;

    private List<Applicant> applicants;

    private String currentDateInWords ;

}
