package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg228 on 12/4/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FaceMatchDetails {

    @JsonProperty("match")
    public String match;

    @JsonProperty("conf")
    public int conf;

    @JsonProperty("match-score")
    public int matchScore;

    @JsonProperty("to-be-reviewed")
    public String toBeReviewed;
}
