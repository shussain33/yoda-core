package com.softcell.gonogo.model.core.kyc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class KycHeader {

    /**
     * unique applicationId for Pan Service and AADHAR
     */
    @JsonProperty("APPLICATION-ID")
    private String applicationId;
    /**
     * requestType should be REQUEST
     */
    @JsonProperty("REQUEST-TYPE")
    private String requestType;
    /**
     * date format(ddMMyyyy HHmmss)
     */
    @JsonProperty("REQUEST-TIME")
    private String requestTime;


    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("KycHeader{");
        sb.append("applicationId='").append(applicationId).append('\'');
        sb.append(", requestType='").append(requestType).append('\'');
        sb.append(", requestTime='").append(requestTime).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KycHeader)) return false;
        KycHeader kycHeader = (KycHeader) o;
        return Objects.equal(getApplicationId(), kycHeader.getApplicationId()) &&
                Objects.equal(getRequestType(), kycHeader.getRequestType()) &&
                Objects.equal(getRequestTime(), kycHeader.getRequestTime());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getApplicationId(), getRequestType(), getRequestTime());
    }

    public static Builder builder(){
        return new Builder();
    }
    public static class Builder {

        private KycHeader kycHeader = new KycHeader();

        public KycHeader build() {
            return this.kycHeader;
        }

        public Builder applicationId(String applicationId) {
            this.kycHeader.setApplicationId(applicationId);
            return this;
        }

        public Builder requestType(String requestType) {
            this.kycHeader.setRequestType(requestType);
            return this;
        }

        public Builder requestTime(String requestTime) {
            this.kycHeader.setRequestTime(requestTime);
            return this;
        }

    }
}
