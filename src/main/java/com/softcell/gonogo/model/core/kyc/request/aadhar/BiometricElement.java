package com.softcell.gonogo.model.core.kyc.request.aadhar;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author yogeshb
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BiometricElement {

    @JsonProperty("nfiq")
    private String nfiq;

    @JsonProperty("na")
    private String na;

    @JsonProperty("pos")
    private String pos;

    @JsonProperty("value")
    private String value;

    public static Builder builder() {
        return new Builder();
    }

    public String getNfiq() {
        return nfiq;
    }

    public void setNfiq(String nfiq) {
        this.nfiq = nfiq;
    }

    public String getNa() {
        return na;
    }

    public void setNa(String na) {
        this.na = na;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BiometricElement{");
        sb.append("nfiq='").append(nfiq).append('\'');
        sb.append(", na='").append(na).append('\'');
        sb.append(", pos='").append(pos).append('\'');
        sb.append(", value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BiometricElement)) return false;
        BiometricElement that = (BiometricElement) o;
        return Objects.equal(getNfiq(), that.getNfiq()) &&
                Objects.equal(getNa(), that.getNa()) &&
                Objects.equal(getPos(), that.getPos()) &&
                Objects.equal(getValue(), that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getNfiq(), getNa(), getPos(), getValue());
    }

    public static class Builder {
        private BiometricElement biometricElement = new BiometricElement();

        public BiometricElement build() {
            return biometricElement;
        }

        public Builder nfiq(String nfiq) {
            this.biometricElement.nfiq = nfiq;
            return this;
        }

        public Builder na(String na) {
            this.biometricElement.na = na;
            return this;
        }

        public Builder pos(String pos) {
            this.biometricElement.pos = pos;
            return this;
        }

        public Builder value(String value) {
            this.biometricElement.value = value;
            return this;
        }
    }


}
