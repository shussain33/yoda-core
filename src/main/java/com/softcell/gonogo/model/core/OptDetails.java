package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0268 on 1/8/19.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OptDetails {

    @JsonProperty("oName")
    private Name name;
    @JsonProperty("sGender")
    private String gender;

    @JsonProperty("sDob")
    private String dateOfBirth;

    @JsonProperty("sReln")
    private String relationship;

    //ddmmyyyy
    @JsonProperty("sTempDob")
    private String  tempDob;
}
