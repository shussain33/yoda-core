package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 27/2/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Appointee {

    @JsonProperty("oName")
    private Name name;

    @JsonProperty("sGender")
    private String gender;

    //ddmmyyyy
    @JsonProperty("sDob")
    private String dateOfBirth;

    @JsonProperty("sReln")
    private String relationship;

    /* for sanction letter purpose */
    @JsonProperty("sTempDob")
    private String  tempDob;
}
