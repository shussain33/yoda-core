package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * Created by monu on 17/9/20.
 */
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Ignore
    public class Mismatch {

        @JsonProperty("sReqType")
        private String reqType;

        @JsonProperty("oSystemInput")
        private SystemInput systemInput;

        @JsonProperty("oApiResult")
        private ApiResult apiResult;

}
