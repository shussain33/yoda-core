package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SuitFiledSearchResponseDefaultSummary {


    @JsonProperty("startDate")
    private String startDate;

    @JsonProperty("endAount")
    private String endAount;

    @JsonProperty("startAmount")
    private String startAmount;

    @JsonProperty("endDate")
    private String endDate;
}
