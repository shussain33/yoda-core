package com.softcell.gonogo.model.core.request.scoring;

public class CibilRespEnquiry {

    private String dateReported;

    private String reportingMemberShortName;

    private String enquiryPurpose;

    private String enquiryAmount;

    public String getDateReported() {
        return dateReported;
    }

    public void setDateReported(String dateReported) {
        this.dateReported = dateReported;
    }

    public String getReportingMemberShortName() {
        return reportingMemberShortName;
    }

    public void setReportingMemberShortName(String reportingMemberShortName) {
        this.reportingMemberShortName = reportingMemberShortName;
    }

    public String getEnquiryPurpose() {
        return enquiryPurpose;
    }

    public void setEnquiryPurpose(String enquiryPurpose) {
        this.enquiryPurpose = enquiryPurpose;
    }

    public String getEnquiryAmount() {
        return enquiryAmount;
    }

    public void setEnquiryAmount(String enquiryAmount) {
        this.enquiryAmount = enquiryAmount;
    }

    @Override
    public String toString() {
        return "CibilRespEnquiry [dateReported=" + dateReported
                + ", reportingMemberShortName=" + reportingMemberShortName
                + ", enquiryPurpose=" + enquiryPurpose + ", enquiryAmount="
                + enquiryAmount + "]";
    }


}
