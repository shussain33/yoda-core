package com.softcell.gonogo.model.core.cam;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 17/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CamDetailsRequest {

    @JsonProperty("oHeader")
    @NotEmpty
    public Header header;

    @JsonProperty("sRefId")
    @NotEmpty(groups = {CamDetailsRequest.InsertGrp.class,CamDetailsRequest.FetchGrp.class})
    public String refId;

    @JsonProperty("sCamType")
    public String camType;

    @JsonProperty("oCamDetails")
    @NotNull(groups = {CamDetailsRequest.InsertGrp.class})
    public CamDetails camDetails;

    public interface InsertGrp {
    }
    public interface FetchGrp{

    }
}
