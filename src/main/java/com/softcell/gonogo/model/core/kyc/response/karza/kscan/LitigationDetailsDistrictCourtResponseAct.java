package com.softcell.gonogo.model.core.kyc.response.karza.kscan;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LitigationDetailsDistrictCourtResponseAct {

    @JsonProperty("underActs")
    public String underActs;

    @JsonProperty("underSections")
    public String underSections;

}