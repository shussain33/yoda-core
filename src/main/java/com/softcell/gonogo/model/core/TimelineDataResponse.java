package com.softcell.gonogo.model.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TimelineDataResponse {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("aTimelineData")
    List<TimelineData> timelineDataList;
}
