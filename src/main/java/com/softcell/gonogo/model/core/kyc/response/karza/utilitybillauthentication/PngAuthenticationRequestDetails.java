package com.softcell.gonogo.model.core.kyc.response.karza.utilitybillauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.KConsentEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by saumyta on 06/02/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PngAuthenticationRequestDetails {

    @JsonProperty("consent")
    private KConsentEnum consent;   //possible values:"y/n"

    @JsonProperty("consumer_id")
    private String consumer_id;

    @JsonProperty("bp_no")
    private String bp_no;

    @JsonProperty("service_provider")
    private String service_provider;

}