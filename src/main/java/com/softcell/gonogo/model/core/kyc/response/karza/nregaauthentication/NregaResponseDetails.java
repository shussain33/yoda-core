package com.softcell.gonogo.model.core.kyc.response.karza.nregaauthentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by saumyta on 05/02/19.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class NregaResponseDetails {

    @JsonProperty("familyId1")
    private String familyId1;

    @JsonProperty("address")
    private String address;

    @JsonProperty("nameOfFatherOrHusband")
    private String nameOfFatherOrHusband;

    @JsonProperty("voterId")
    private String voterId;

    @JsonProperty("village")
    private String village;

    @JsonProperty("bplFamilyId")
    private String bplFamilyId;

    @JsonProperty("incomeDetail")
    private List<NregaIncomeDetails> nregaIncomeDetailsList;

    @JsonProperty("familyId")
    private String familyId;

    @JsonProperty("category")
    private String category;

    @JsonProperty("applicantDetail")
    private List<ApplicantDetails> applicantDetailList;

    @JsonProperty("district")
    private String district;

    @JsonProperty("nameOfHead")
    private String nameOfHead;

    @JsonProperty("photoImageUrl")
    private String photoImageUrl;

    @JsonProperty("bplFamily")
    private String bplFamily;

    @JsonProperty("jobcardno")
    private String jobcardno;

    @JsonProperty("dateOfRegistration")
    private String dateOfRegistration;

    @JsonProperty("panchayat")
    private String panchayat;

    @JsonProperty("block")
    private String block;

}