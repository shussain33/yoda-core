package com.softcell.gonogo.model.core.kyc.response.karza;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by yogesh Khandare on 6/10/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DrivingLicenceResponse {

    @JsonProperty("path")
    private String path;

    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("statusCode")
    private int statusCode;

    @JsonProperty("payload")
    private DrivingLicenceResponseDetails payload;

    @JsonProperty("acknowledgementId")
    private String acknowledgementId;

    @JsonProperty("error")
    private String error;

    @JsonProperty("errors")
    private ArrayList<Error> errors;

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;
}
