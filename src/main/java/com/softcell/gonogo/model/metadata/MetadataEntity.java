package com.softcell.gonogo.model.metadata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Objects;

/**
 * Created by prateek on 10/3/17.
 */
@Validated
@Document(collection = "gng_metadata_dict")
//@CompoundIndexes({@CompoundIndex(name = "type_inst_product", unique = true, def = "{'type':1,'institutionId':1 ,'product':1}")})
public class MetadataEntity extends AuditEntity {

    @JsonProperty(value = "sId", access = JsonProperty.Access.WRITE_ONLY)
    @Id
    @Field("id")
    public ObjectId id;

    @NotBlank
    @JsonProperty("sType")
    @Field("type")
    private String type;

    @NotNull(groups = {updateGrp.class, insertGrp.class})
    @JsonProperty("cValues")
    @Field("values")
    private Collection<String> values;

    @NotNull
    @JsonProperty(value = "bIsEnabled", access = JsonProperty.Access.WRITE_ONLY)
    private Boolean isEnabled;

    @NotBlank
    @JsonProperty(value = "sInstitutionId", access = JsonProperty.Access.WRITE_ONLY)
    @Field("institutionId")
    private String institutionId;

    @NotBlank
    @JsonProperty(value = "sProduct", access = JsonProperty.Access.WRITE_ONLY)
    @Field("product")
    private String product;

    @JsonProperty("cTransformedValue")
    private Collection<String> transformedValue;

    public static Builder builder() {
        return new Builder();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @ApiModelProperty(value = "Type of dictionary ")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<String> getValues() {
        return values;
    }

    @ApiModelProperty(value = "Collection of values in typed dictionary ")
    public void setValues(Collection<String> values) {
        this.values = values;
    }

    @JsonIgnore
    @ApiModelProperty(value = "Tells if typed dictionary is available or not ")
    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    @ApiModelProperty(value = "Institution to which dictionary belongs to. ")
    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    @ApiModelProperty(value = "product to which dictionary belongs to. ")
    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Collection<String> getTransformedValue() {
        return transformedValue;
    }

    public void setTransformedValue(Collection<String> transformedValue) {
        this.transformedValue = transformedValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetadataEntity)) return false;
        if (!super.equals(o)) return false;
        MetadataEntity that = (MetadataEntity) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getValues(), that.getValues()) &&
                Objects.equals(isEnabled, that.isEnabled) &&
                Objects.equals(getInstitutionId(), that.getInstitutionId()) &&
                Objects.equals(getProduct(), that.getProduct()) &&
                Objects.equals(getTransformedValue(), that.getTransformedValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId(), getType(), getValues(), isEnabled, getInstitutionId(), getProduct(), getTransformedValue());
    }

    public interface MetadataEntityConst {

        String INSTITUTIONID = "institutionId";

        String TYPE = "type";

        String PRODUCT = "product";

        String VALUES = "values";

        String IS_ENABLED = "isEnabled";

    }

    public interface fetchGrp {

    }

    public interface updateGrp {
    }

    public interface insertGrp {
    }

    public static class Builder {

        private MetadataEntity metadataEntity = new MetadataEntity();

        public MetadataEntity build() {
            return this.metadataEntity;
        }

        public Builder institutionId(String institutionId) {
            this.metadataEntity.setInstitutionId(institutionId);
            return this;
        }

        public Builder id(ObjectId id) {
            this.metadataEntity.setId(id);
            return this;
        }

        public Builder product(String product) {
            this.metadataEntity.setProduct(product);
            return this;
        }


        public Builder isEnabled(Boolean isEnabled) {
            this.metadataEntity.setEnabled(isEnabled);
            return this;
        }

        public Builder type(String type) {
            this.metadataEntity.setType(type);
            return this;
        }

        public Builder transformedValue(Collection<String> transformedValue) {
            this.metadataEntity.setTransformedValue(transformedValue);
            return this;
        }

        public Builder values(Collection<String> values) {
            this.metadataEntity.setValues(values);
            return this;
        }

    }
}
