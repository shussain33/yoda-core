package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class FraudCheck {

    // Check if device is rooted
    @JsonProperty("rootedDeviceAdvanced")
    private Boolean  rootedDeviceAdvanced ;

    // Check if IMEI number is masked or tampered with
    @JsonProperty("imeiMasked")
    private String  imeiMasked ;

    // Check if Ithe location reading from device is masked
    @JsonProperty("gpsLocationMasked")
    private String  gpsLocationMasked ;

    @JsonProperty("highSimChanger")
    private String  highSimChanger ;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}