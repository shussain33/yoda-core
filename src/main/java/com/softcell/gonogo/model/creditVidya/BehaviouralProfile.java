package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BehaviouralProfile {

    @JsonProperty("interactionProfile")
    private InteractionProfile interactionProfile;

    @JsonProperty("connectionProfile")
    private ConnectionProfile connectionProfile;

    @JsonProperty("travelProfile")
    private TravelProfile travelProfile;
}
