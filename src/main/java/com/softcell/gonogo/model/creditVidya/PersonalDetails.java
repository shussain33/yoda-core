package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class PersonalDetails {

    @JsonProperty("metadata")
    private PersonalMetaData metadata;

    @JsonProperty("income")
    private Income income;

    @JsonProperty("bankAccount")
    private BankAccountInfo bankAccountInfo;

    @JsonProperty("creditCardAccount")
    private CreditCardAccountInfo creditCardAccountInfo;

    @JsonProperty("loanAccount")
    private LoanAccountInfo loanAccountInfo;

    @JsonProperty("locationProfile")
    private LocationProfile locationProfile;

    @JsonProperty("contactInfo")
    private ContactInfo contactInfo;

    @JsonProperty("creditScore")
    private CreditScore creditScore;

    @JsonProperty("fraudCheck")
    private FraudCheck fraudCheck;

    @JsonProperty("utilityAccount")
    private UtilityAccountInfo utilityAccountInfo;

    /*@JsonProperty("investmentProfile")
    private InvestmentProfile investmentProfile;*/

    /*@JsonProperty("expenseProfile")
    private ExpenseProfile expenseProfile;
*/
    @JsonProperty("appographicProfile")
    private AppographicProfile appographicProfile;

    /*@JsonProperty("behaviouralProfile")
    private BehaviouralProfile behaviouralProfile;
*/
    @JsonProperty("auxiliaryInfo")
    private AuxiliaryInfo auxiliaryInfo;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
