package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AggregatePayment {

    @JsonProperty("averageInternetBillAmountLast3Mon")
    private BigDecimal averageInternetBillAmountLast3Mon;

    @JsonProperty("averageLandlineBillAmountLast3Mon")
    private BigDecimal averageLandlineBillAmountLast3Mon;

    @JsonProperty("averageElectricityBillAmountLast3Mon")
    private BigDecimal averageElectricityBillAmountLast3Mon;

    @JsonProperty("prepaidPostpaidFlag")
    private Boolean prepaidPostpaidFlag;

    @JsonProperty("averageMobileBillAmountLast3Mon")
    private BigDecimal averageMobileBillAmountLast3Mon;

    @JsonProperty("averageMobileRechargeAmountLast3Mon")
    private BigDecimal averageMobileRechargeAmountLast3Mon;

    @JsonProperty("avgGrossUtilitySpendLast3Mon")
    private BigDecimal avgGrossUtilitySpendLast3Mon;
}
