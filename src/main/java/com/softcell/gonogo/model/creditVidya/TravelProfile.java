package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TravelProfile {

    @JsonProperty("distinctRoamingCirclesListMonth0")
    private String distinctRoamingCirclesListMonth0;

    @JsonProperty("distinctRoamingCirclesListMonth1")
    private String distinctRoamingCirclesListMonth1;

    @JsonProperty("distinctRoamingCirclesListMonth2")
    private String distinctRoamingCirclesListMonth2;

    @JsonProperty("frequentRoamingCircle")
    private String frequentRoamingCircle;

    @JsonProperty("countTravelAir")
    private Integer countTravelAir;

    @JsonProperty("countTravelRoad")
    private Integer countTravelRoad;

    @JsonProperty("countTravelTrain")
    private Integer countTravelTrain;
}
