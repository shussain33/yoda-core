package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExpenseProfile {
    @JsonProperty("monthsExpenseDataAvailable")
    private BigDecimal monthsExpenseDataAvailable;

    @JsonProperty("expenseDataSufficiencyFlag")
    private Boolean expenseDataSufficiencyFlag;

    @JsonProperty("averageExpenseLast3Mon")
    private BigDecimal averageExpenseLast3Mon;

    @JsonProperty("cashExpenseLevel")
    private BigDecimal cashExpenseLevel;

    @JsonProperty("favExpenseChannel")
    private String favExpenseChannel;

    @JsonProperty("cashExpenseToGrossExpense")
    private BigDecimal cashExpenseToGrossExpense;

    @JsonProperty("expenseChannel")
    private ExpenseChannel expenseChannel;


}
