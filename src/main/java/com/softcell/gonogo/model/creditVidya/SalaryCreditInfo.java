package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class SalaryCreditInfo {
    @JsonProperty("salaryWithKeywordMonth0")
    private BigDecimal salaryWithKeywordMonth0;

    @JsonProperty("salaryWithKeywordMonth1")
    private BigDecimal salaryWithKeywordMonth1;

    @JsonProperty("salaryWithKeywordMonth2")
    private BigDecimal salaryWithKeywordMonth2;

    // Median date of month for salary credit, e.g. 3
    @JsonProperty("salaryCreditDate")
    private Integer salaryCreditDate;

    @JsonProperty("salaryCreditAccount")
    private String salaryCreditAccount;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}