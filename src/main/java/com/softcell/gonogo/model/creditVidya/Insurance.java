package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Insurance {
    // TODO : ?? boolean ??
    @JsonProperty("monthsInsuranceDataAvailable")
    private BigDecimal monthsInsuranceDataAvailable;

    @JsonProperty("countInsuranceAccount")
    private Integer countInsuranceAccount;

    // TODO : ?? array ??
    @JsonProperty("insuranceSchemes")
    private String insuranceSchemes;
}
