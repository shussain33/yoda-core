package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 27/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class CreditCard {

    // Name of the issuer of credit card
    @JsonProperty("issuerName")
    private String issuerName;

    // Credit limit on the credit card
    @JsonProperty("creditLimit")
    private BigDecimal creditLimit;

    // Maximum credit utilized in last 3 months
    @JsonProperty("maxCreditUtilization")
    private BigDecimal maxCreditUtilization;

    // Number of times the credit card payment has been overdue in last 3 months
    @JsonProperty("countOverdueLast3Months")
    private Integer countOverdueLast3Months;

    // Number of times credit card payment has been overdue
    @JsonProperty("countOverdueEver")
    private Integer countOverdueEver;

    // Latest credit card payment due date e.g. 22/09/2017
    @JsonProperty("latestCCDueDate")
    private String latestCCDueDate;

    // Total spend in month 0
    @JsonProperty("totalCCTraxnAmountMonthYear0")
    private BigDecimal totalCCTraxnAmountMonthYear0;

    // Total spend in month 1
    @JsonProperty("totalCCTraxnAmountMonthYear1")
    private BigDecimal totalCCTraxnAmountMonthYear1;

    // Total spend in month 2
    @JsonProperty("totalCCTraxnAmountMonthYear2")
    private BigDecimal totalCCTraxnAmountMonthYear2;

    // Credit card bill amount paid in month 0
    @JsonProperty("totalCCPaymentMonthYear0")
    private BigDecimal totalCCPaymentMonthYear0;

    // Credit card bill amount paid in month 1
    @JsonProperty("totalCCPaymentMonthYear1")
    private BigDecimal totalCCPaymentMonthYear1;

    // Credit card bill amount paid in month 2
    @JsonProperty("totalCCPaymentMonthYear2")
    private BigDecimal totalCCPaymentMonthYear2;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
