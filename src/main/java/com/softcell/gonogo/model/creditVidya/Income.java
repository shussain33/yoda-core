package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class Income {

    // Check if data is sufficient to estimate income
    @JsonProperty("incomePredictiondataSufficiencyFlag")
    private Boolean incomePredictiondataSufficiencyFlag;

    // Estimated income of the individual
    @JsonProperty("predictedIncome")
    private BigDecimal predictedIncome;

    // Accuracy level of estimated income - High, Medium, Low
    @JsonProperty("predictedIncomeConfidenceLevel")
    private String predictedIncomeConfidenceLevel;

    @JsonProperty("salaryCredit")
    private SalaryCreditInfo salaryCredit;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
