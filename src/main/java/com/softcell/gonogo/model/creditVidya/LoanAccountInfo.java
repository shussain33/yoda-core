package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class LoanAccountInfo {

    // No of months for which loan data is available
    @JsonProperty("monthsLoanDataAvailable")
    private Integer  monthsLoanDataAvailable;

    // Check if data is sufficient to calculate the EMI variables
    @JsonProperty("loanDataSufficiencyFlag")
    private Boolean loanDataSufficiencyFlag;

    // No of loan accounts found excluding credit card
    @JsonProperty("countLoanAccounts")
    private Integer countLoanAccounts;

    // No of secured loan accounts
    @JsonProperty("countSecuredLoanAccounts")
    private Integer countSecuredLoanAccounts;

    // No of unsecured loan accounts
    @JsonProperty("countUnsecuredLoanAccounts")
    private Integer countUnsecuredLoanAccounts;

    @JsonProperty("loans")
    private List<Loan> loans;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
