package com.softcell.gonogo.model.creditVidya;


import com.fasterxml.jackson.annotation.*;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class ConnectionProfile {

    // Most frequently contacted person
    @JsonProperty("freqContact1")
    private String freqContact1;

    // Most frequently contacted person
    @JsonProperty("freqContact2")
    private String freqContact2;

    // Most frequently contacted person
    @JsonProperty("freqContact3")
    private String freqContact3;

    // Most frequently family person e.g. Mummy - 9609876543
    @JsonProperty("freqFamilyContact")
    private String freqFamilyContact;

    // Most regularly contacted  person
    @JsonProperty("regularlyCalledContact1")
    private String regularlyCalledContact1;

    // Most regularly contacted  person
    @JsonProperty("regularlyCalledContact2")
    private String regularlyCalledContact2;

    // Most regularly contacted  person
    @JsonProperty("regularlyCalledContact3")
    private String regularlyCalledContact3;

    /*@JsonProperty("totalContacts")
    private Integer totalContacts;

    @JsonProperty("contactsWithMobile")
    private Integer contactsWithMobile;

    @JsonProperty("contactsWithEmail")
    private Integer contactsWithEmail;

    @JsonProperty("contactsWithInternationalNumber")
    private Integer contactsWithInternationalNumber;

    @JsonProperty("contactsWithSmartphone")
    private Integer contactsWithSmartphone;

    @JsonProperty("countLandlineContacts")
    private Integer countLandlineContacts;

    @JsonProperty("countFamilyContacts")
    private Integer countFamilyContacts;*/

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
