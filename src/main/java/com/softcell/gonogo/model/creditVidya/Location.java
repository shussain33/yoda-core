package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Location {
    @JsonProperty("pointOfInstallationAddress")
    private String installationAddress;

    @JsonProperty("pointOfInstallationAddressAccuracy")
    private String pointOfInstallationAddressAccuracy;

    @JsonProperty("daysLocationMonitored")
    private Integer daysLocationMonitored;

    @JsonProperty("countLocationReadingCollected")
    private Integer countLocationReadingCollected;

    @JsonProperty("afternoonPlaceWeekDay")
    private String afternoonPlaceWeekDay;

    @JsonProperty("afternoonPlaceWeekEnd")
    private String afternoonPlaceWeekEnd;

    @JsonProperty("eveningPlaceWeekDay")
    private String eveningPlaceWeekDay;

    @JsonProperty("eveningPlaceWeekEnd")
    private Integer eveningPlaceWeekEnd;

    @JsonProperty("morningPlaceWeekDay")
    private Integer morningPlaceWeekDay;

    @JsonProperty("morningPlaceWeekEnd")
    private String morningPlaceWeekEnd;

    @JsonProperty("nightPlaceWeekDay")
    private Integer nightPlaceWeekDay;

    @JsonProperty("nightPlaceWeekEnd")
    private Integer nightPlaceWeekEnd;

    @JsonProperty("prominentLocation")
    private ProminentLocation prominentLocation;

}
