package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 27/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class Account {

    // Name of the bank
    @JsonProperty("bankName")
    private String bankName;

    // Latest balance in the account
    @JsonProperty("latestBalance")
    private BigDecimal latestBalance;

    // Average monthly balance in the account in month 0
    @JsonProperty("ambMonth0")
    private BigDecimal ambMonth0;

    // Average monthly balance in the account in month 1

    @JsonProperty("ambMonth1")
    private BigDecimal ambMonth1;

    // Average monthly balance in the account in month 2
    @JsonProperty("ambMonth2")
    private BigDecimal ambMonth2;

    // Total credit in the account in month 0
    @JsonProperty("creditMonth0")
    private BigDecimal creditMonth0;

    // Total credit in the account in month 1
    @JsonProperty("creditMonth1")
    private BigDecimal creditMonth1;

    // Total credit in the account in month 2
    @JsonProperty("creditMonth2")
    private BigDecimal creditMonth2;

    // Total debit in the account in month 0
    @JsonProperty("debitMonth0")
    private BigDecimal debitMonth0;

    // Total debit in the account in month 1
    @JsonProperty("debitMonth1")
    private BigDecimal debitMonth1;

    // Total debit in the account in month 2
    @JsonProperty("debitMonth2")
    private BigDecimal debitMonth2;

    // Standard deviation of average monthly balance in last 3 months
    @JsonProperty("volatilityBalance")
    private BigDecimal volatilityBalance;

    // Standard deviation of average monthly debit in last 3 months
    @JsonProperty("volatilityDebit")
    private BigDecimal volatilityDebit;

    // Standard deviation of average monthly credit in last 3 months
    @JsonProperty("volatilityCredit")
    private BigDecimal volatilityCredit;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
