package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UtilityAccountInfo {

    @JsonProperty("monthsUtilityBillDataAvailableElectricity")
    private Integer monthsUtilityBillDataAvailableElectricity;

    @JsonProperty("monthsUtilityBillDataAvailableTelecom")
    private Integer monthsUtilityBillDataAvailableTelecom;

    @JsonProperty("utilityBillDataTelcoSufficiencyFlag")
    private Boolean utilityBillDataTelcoSufficiencyFlag;

    @JsonProperty("utilityBillDataNonTelcoSufficiencyFlag")
    private Boolean utilityBillDataNonTelcoSufficiencyFlag;

    @JsonProperty("countUtilityAccount")
    private Integer countUtilityAccount;

    @JsonProperty("aggregatePayment")
    private AggregatePayment aggregatePayment;

    @JsonProperty("riskEvents")
    private UtilityRiskEvents riskEvents;

}
