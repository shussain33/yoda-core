package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InteractionProfile {

    @JsonProperty("answerRate")
    private BigDecimal answerRate;

    @JsonProperty("avgCallsPerDay")
    private BigDecimal avgCallsPerDay;

    @JsonProperty("avgDurationPerDay")
    private BigDecimal avgDurationPerDay;

    @JsonProperty("avgIncomingCallsPerDay")
    private BigDecimal avgIncomingCallsPerDay;

    @JsonProperty("avgIncomingDurationPerDay")
    private BigDecimal avgIncomingDurationPerDay;

    @JsonProperty("avgOutgoingCallsPerDay")
    private BigDecimal avgOutgoingCallsPerDay;

    @JsonProperty("avgOutgoingDurationPerDay")
    private BigDecimal avgOutgoingDurationPerDay;

    @JsonProperty("highestAnswerRateDay")
    private String highestAnswerRateDay;

    @JsonProperty("numberOfUniqueContactsForIncomingCalls")
    private Integer numberOfUniqueContactsForIncomingCalls;

    @JsonProperty("numberOfUniqueContactsForOutgoingCalls")
    private Integer numberOfUniqueContactsForOutgoingCalls;

    @JsonProperty("hourOfDayMostLikelyToAnswerCall")
    private Integer hourOfDayMostLikelyToAnswerCall;

    @JsonProperty("hourOfDayMostLikelyToMissCall")
    private Integer hourOfDayMostLikelyToMissCall;


}
