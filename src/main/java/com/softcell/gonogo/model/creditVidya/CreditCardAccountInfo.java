package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class CreditCardAccountInfo {

    // No of months for which credit card data is available
    @JsonProperty("monthsCCDataAvailable")
    private Integer  monthsCCDataAvailable;

    // Check if data is sufficient to calculate the transaction variables
    @JsonProperty("ccDataSufficiencyFlag")
    private Boolean ccDataSufficiencyFlag;

    // Total number of credit cards found
    @JsonProperty("countTotalCreditCard")
    private Integer countTotalCreditCard;

    // Total number of credit cards for which transactions found in last 3 months
    @JsonProperty("countActiveCreditCards")
    private Integer countActiveCreditCards;

    // Average monthly credit card spend in last 3 months
    @JsonProperty("avgCCTraxnAmountLast3Mon")
    private BigDecimal avgCCTraxnAmountLast3Mon;

    @JsonProperty("cards")
    private List<CreditCard> creditCards;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
