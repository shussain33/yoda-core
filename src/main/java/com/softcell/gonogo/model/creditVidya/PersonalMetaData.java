package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class PersonalMetaData {

    @JsonProperty("uniqueId")
    private String  uniqueId;

    @JsonProperty("requestTime")
    private String requestTime;

    @JsonProperty("installTime")
    private String installTime;

    @JsonProperty("lastPingTime")
    private String lastPingTime;

    // Package name of the app in which sdk is embedded e.g. com.trustscore.app
    @JsonProperty("sourceApp")
    private String sourceApp;

    @JsonProperty("permissionGiven")
    private Permissions permissionGiven;

    @JsonProperty("dataThickness")
    private DataVolume dataThickness;
// ---------------------
    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
