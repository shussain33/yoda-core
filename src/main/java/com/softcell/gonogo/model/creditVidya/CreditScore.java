package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class CreditScore {

    // Check if data is sufficient to calculate the risk score for the customer
    @JsonProperty("scoringSufficiency")
    private Boolean scoringSufficiency;

    // Risk score calculated on the basis of alternate data - Ranges from 0 to 1
    @JsonProperty("alternateRiskScore")
    private Double  alternateRiskScore;

    // Code associated with data availability & sufficiency scenarios associated with score calculation
    /*@JsonProperty("reasonCodes")
    private String reasonCodes;*/

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
