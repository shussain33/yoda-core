package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AggregateCreditInfo {

    @JsonProperty("totalCreditMonth0")
    private BigDecimal totalCreditMonth0;

    @JsonProperty("totalCreditMonth1")
    private BigDecimal totalCreditMonth1;

    @JsonProperty("totalCreditMonth2")
    private BigDecimal totalCreditMonth2;

    @JsonProperty("totalCashCreditMonth0")
    private BigDecimal totalCashCreditMonth0;

    @JsonProperty("totalCashCreditMonth1")
    private BigDecimal totalCashCreditMonth1;

    @JsonProperty("totalCashCreditMonth2")
    private BigDecimal totalCashCreditMonth2;

    @JsonProperty("totalChequeCreditMonth0")
    private BigDecimal totalChequeCreditMonth0;

    @JsonProperty("totalChequeCreditMonth1")
    private BigDecimal totalChequeCreditMonth1;

    @JsonProperty("totalChequeCreditMonth2")
    private BigDecimal totalChequeCreditMonth2;

    @JsonProperty("totalTransferInAmountMonth0")
    private BigDecimal totalTransferInAmountMonth0;

    @JsonProperty("totalTransferInAmountMonth1")
    private BigDecimal totalTransferInAmountMonth1;

    @JsonProperty("totalTransferInAmountMonth2")
    private BigDecimal totalTransferInAmountMonth2;

    @JsonProperty("totalOtherCreditMonth0")
    private BigDecimal totalOtherCreditMonth0;

    @JsonProperty("totalOtherCreditMonth1")
    private BigDecimal totalOtherCreditMonth1;

    @JsonProperty("totalOtherCreditMonth2")
    private BigDecimal totalOtherCreditMonth2;

    @JsonProperty("avgCashCreditAmtLast3Mon")
    private BigDecimal avgCashCreditAmtLast3Mon;

    @JsonProperty("avgChequeCreditAmtLast3Mon")
    private BigDecimal avgChequeCreditAmtLast3Mon;

    @JsonProperty("avgCreditOtherAmtLast3Mon")
    private BigDecimal avgCreditOtherAmtLast3Mon;

    @JsonProperty("avgTransferInAmtLast3Mon")
    private BigDecimal avgTransferInAmtLast3Mon;

    @JsonProperty("maxCashCreditAmtLast3Mon")
    private BigDecimal maxCashCreditAmtLast3Mon;

    @JsonProperty("maxChequeCreditAmtLast3Mon")
    private BigDecimal maxChequeCreditAmtLast3Mon;

    @JsonProperty("maxTransferInAmtLast3Mon")
    private BigDecimal maxTransferInAmtLast3Mon;

    @JsonProperty("maxOtherCreditAmtLast3Mon")
    private BigDecimal maxOtherCreditAmtLast3Mon;
}
