package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExpenseChannel {
    @JsonProperty("netBankingExpense")
    private BigDecimal netBankingExpense;

    @JsonProperty("debitCardExpense")
    private BigDecimal debitCardExpense;

    @JsonProperty("creditCardExpense")
    private BigDecimal creditCardExpense;

    @JsonProperty("cashExpenseaverageEcommerceExpenseLast3Mon")
    private BigDecimal cashExpenseaverageEcommerceExpenseLast3Mon;
}
