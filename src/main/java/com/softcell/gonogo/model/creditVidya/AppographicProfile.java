package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppographicProfile {

    @JsonProperty("countOfapps")
    private Integer countOfapps;

    @JsonProperty("countOfDistinctAppCategories")
    private Integer countOfDistinctAppCategories;

    @JsonProperty("countOfSocialApps")
    private Integer countOfSocialApps;

    @JsonProperty("countOfFinancialApps")
    private Integer countOfFinancialApps;

    @JsonProperty("countOfFitnessApps")
    private Integer countOfFitnessApps;

    @JsonProperty("countOfShoppingApps")
    private Integer countOfShoppingApps;

    @JsonProperty("countOfWalletApps")
    private Integer countOfWalletApps;
}
