package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 27/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class Loan {

    // Name of the lender
    @JsonProperty("lenderName")
    private String  lenderName;

    // Type of the loan e.g. Car Loan
    @JsonProperty("loanType")
    private String  loanType;

    // EMI paid in month 0
    @JsonProperty("emiAmountMonth0")
    private BigDecimal emiAmountMonth0;

    // EMI paid in month 1
    @JsonProperty("emiAmountMonth1")
    private BigDecimal  emiAmountMonth1;

    // EMI paid in month 2
    @JsonProperty("emiAmountMonth2")
    private BigDecimal  emiAmountMonth2;

    // Date of month when the EMI is due e.g. 2
    @JsonProperty("emiDueDate")
    private Integer  emiDueDate;

    // No of overdues found ever across loans
    @JsonProperty("countOverdueEmiEver")
    private Integer  countOverdueEmiEver;

    // No of overdues found in last 3 months across loans
    @JsonProperty("countOverdueEmiLast3Months")
    private Integer  countOverdueEmiLast3Months;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
