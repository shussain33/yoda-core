package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
/**
 * POJO telling no. of counts found in the device
 */
public class DataVolume {

    @JsonProperty("smsCount")
    private Integer  smsCount;

    @JsonProperty("callCount")
    private Integer callCount;

    @JsonProperty("contactCount")
    private Integer contactCount;

    @JsonProperty("imageCount")
    private Integer imageCount;

    @JsonProperty("appCount")
    private Integer appCount;

    @JsonProperty("accountCount")
    private Integer accountCount;

    @JsonProperty("locationCount")
    private Integer locationCount;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}

