package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProminentLocation {
    @JsonProperty("prominentPlace1")
    private String prominentPlace1;

    @JsonProperty("accuracyProminentPlace1")
    private String accuracyProminentPlace1;

    @JsonProperty("prominentPlace2")
    private Integer prominentPlace2;

    @JsonProperty("accuracyProminentPlace2")
    private Integer accuracyProminentPlace2;

    @JsonProperty("prominentPlace3")
    private String prominentPlace3;

    @JsonProperty("accuracyProminentPlace3")
    private String accuracyProminentPlace3;

}
