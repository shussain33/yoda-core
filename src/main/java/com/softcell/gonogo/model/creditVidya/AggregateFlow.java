package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class AggregateFlow {

    // Total debit across all accounts in month 0
    @JsonProperty("totalDebitMonth0")
    private BigDecimal totalDebitMonth0;

    // Total debit across all accounts in month 1
    @JsonProperty("totalDebitMonth1")
    private BigDecimal totalDebitMonth1;

    // Total debit across all accounts in month 2
    @JsonProperty("totalDebitMonth2")
    private BigDecimal totalDebitMonth2;

    // Total credit across all accounts in month 0
    @JsonProperty("totalCreditMonth0")
    private BigDecimal totalCreditMonth0;

    // Total credit across all accounts in month 1
    @JsonProperty("totalCreditMonth1")
    private BigDecimal totalCreditMonth1;

    // Total credit across all accounts in month 2
    @JsonProperty("totalCreditMonth2")
    private BigDecimal totalCreditMonth2;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }


}
