package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UtilityRiskEvents {

    // No of times internet bill was overdue in last 3 months
    @JsonProperty("billOverdueBroadband")
    private Integer billOverdueBroadband;

    // No of times landline bill was overdue in last 3 months
    @JsonProperty("billOverdueLandline")
    private Integer billOverdueLandline;

    // No of times postpaid bill was overdue in last 3 months
    @JsonProperty("billOverduePostpaid")
    private Integer billOverduePostpaid;

    // No of times electricity bill was overdue in last 3 months
    @JsonProperty("billOverdueElectricity")
    private Integer billOverdueElectricity;

    // No of time internet connection was suspended in last 3 months
    @JsonProperty("connectionSuspendedBroadband")
    private Integer connectionSuspendedBroadband;

    // No of time landline connection was suspended in last 3 months
    @JsonProperty("connectionSuspendedLandline")
    private Integer connectionSuspendedLandline;

    // No of time postpaid connection was suspended in last 3 months
    @JsonProperty("connectionSuspendedPostpaid")
    private Integer connectionSuspendedPostpaid;

    // Total no of utility bill account penalties in last 3 months
    @JsonProperty("countOfPenaltiesInUtilityBills")
    private Integer countOfPenaltiesInUtilityBills;
}
