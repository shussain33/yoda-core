package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankRiskEvents {

    @JsonProperty("countChequeBounceInward")
    private Integer countChequeBounceInward;

    @JsonProperty("countChequeBounceOutward")
    private Integer countChequeBounceOutward;

    @JsonProperty("countECSBounce")
    private Integer countECSBounce;

    @JsonProperty("countInsufficientBal")
    private Integer countInsufficientBal;
}
