package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AggregateDebitInfo {

    @JsonProperty("totalDebitMonth0")
    private BigDecimal totalDebitMonth0;

    @JsonProperty("totalDebitMonth1")
    private BigDecimal totalDebitMonth1;

    @JsonProperty("totalDebitMonth2")
    private BigDecimal totalDebitMonth2;

    @JsonProperty("totalAtmWithdrawalAmountMonth0")
    private BigDecimal totalAtmWithdrawalAmountMonth0;

    @JsonProperty("totalAtmWithdrawalAmountMonth1")
    private BigDecimal totalAtmWithdrawalAmountMonth1;

    @JsonProperty("totalAtmWithdrawalAmountMonth2")
    private BigDecimal totalAtmWithdrawalAmountMonth2;

    @JsonProperty("totalChequeDebitAmountMonth0")
    private BigDecimal totalChequeDebitAmountMonth0;

    @JsonProperty("totalChequeDebitAmountMonth1")
    private BigDecimal totalChequeDebitAmountMonth1;

    @JsonProperty("totalChequeDebitAmountMonth2")
    private BigDecimal totalChequeDebitAmountMonth2;

    @JsonProperty("totalTransferOutAmountMonth0")
    private BigDecimal totalTransferOutAmountMonth0;

    @JsonProperty("totalTransferOutAmountMonth1")
    private BigDecimal totalTransferOutAmountMonth1;

    @JsonProperty("totalTransferOutAmountMonth2")
    private BigDecimal totalTransferOutAmountMonth2;

    @JsonProperty("totalInternetBankingDebitAmtMonth0")
    private BigDecimal totalInternetBankingDebitAmtMonth0;

    @JsonProperty("totalInternetBankingDebitAmtMonth1")
    private BigDecimal totalInternetBankingDebitAmtMonth1;

    @JsonProperty("totalInternetBankingDebitAmtMonth2")
    private BigDecimal totalInternetBankingDebitAmtMonth2;

    @JsonProperty("totalOtherDebitMonth0")
    private BigDecimal totalOtherDebitMonth0;

    @JsonProperty("totalOtherDebitMonth1")
    private BigDecimal totalOtherDebitMonth1;

    @JsonProperty("totalOtherDebitMonth2")
    private BigDecimal totalOtherDebitMonth2;

    @JsonProperty("avgAtmWithdrawalAmtLast3Mon")
    private BigDecimal avgAtmWithdrawalAmtLast3Mon;

    @JsonProperty("avgChequeDebitAmtLast3Mon")
    private BigDecimal avgChequeDebitAmtLast3Mon;

    @JsonProperty("avgInternetBankingDebitAmtLast3Mon")
    private BigDecimal avgInternetBankingDebitAmtLast3Mon;

    @JsonProperty("avgTransferOutAmtLast3Mon")
    private BigDecimal avgTransferOutAmtLast3Mon;

    @JsonProperty("avgDebitOtherAmtLast3Mon")
    private BigDecimal avgDebitOtherAmtLast3Mon;

    @JsonProperty("maxAtmWithdrawalAmountLast3Mon")
    private BigDecimal maxAtmWithdrawalAmountLast3Mon;

    @JsonProperty("maxChequeOutAmountLast3Mon")
    private BigDecimal maxChequeOutAmountLast3Mon;

    @JsonProperty("maxInternetBankingDebitLast3Mon")
    private BigDecimal maxInternetBankingDebitLast3Mon;

    @JsonProperty("maxTransferOutAmountLast3Mon")
    private BigDecimal maxTransferOutAmountLast3Mon;

    @JsonProperty("maxDebitOthersLast3Mon")
    private BigDecimal maxDebitOthersLast3Mon;

}
