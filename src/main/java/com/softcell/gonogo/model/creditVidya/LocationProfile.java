package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 22/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class LocationProfile {

    // Install time/SDK initialize time location
    @JsonProperty("pointOfInstallationAddress")
    private String installationAddress;

    // Accuracy of the location in meters
    @JsonProperty("pointOfInstallationAddressAccuracy")
    private Integer pointOfInstallationAddressAccuracy;

    // No of days for which location data is read
    @JsonProperty("daysLocationMonitored")
    private Integer daysLocationMonitored;

    // No of location readings taken
    @JsonProperty("countLocationReadingCollected")
    private Integer countLocationReadingCollected;

    // Prominent location identified in accessment window
    @JsonProperty("prominentPlace1")
    private String prominentPlace1;

    // Accuracy of the location in meters
    @JsonProperty("accuracyProminentPlace1")
    private Integer accuracyProminentPlace1;

    // Prominent location identified in accessment window
    @JsonProperty("prominentPlace2")
    private String prominentPlace2;

    // Accuracy of the location in meters
    @JsonProperty("accuracyProminentPlace2")
    private Integer accuracyProminentPlace2;

    @JsonProperty("prominentPlace3")
    private String prominentPlace3;

    //Accuracy of the location in meters
    @JsonProperty("accuracyProminentPlace3")
    private Integer accuracyProminentPlace3;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
