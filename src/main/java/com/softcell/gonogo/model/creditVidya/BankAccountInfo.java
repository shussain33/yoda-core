package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class BankAccountInfo {

    // No of months for which bank data is available e.g. 4.5
    @JsonProperty("monthsBankDataAvailable")
    private Double  monthsBankDataAvailable;

    // Check if data is sufficient to calculate the credit, debit and balance variables
    @JsonProperty("bankDataSufficiencyFlag")
    private Boolean bankDataSufficiencyFlag;

    // Total number of bank accounts found
    @JsonProperty("countAccounts")
    private Integer countAccounts;

    // No of bank accounts for which transactions seen in last 3 months
    @JsonProperty("countActiveAccounts")
    private Integer countActiveAccounts;

    // Bank's name of primary account
    @JsonProperty("primaryAccount")
    private String primaryAccount;

    // Bank's name of primary accounts for cash withdrawal.
    @JsonProperty("primaryAccountCashWithdwl")
    private List<String> primaryAccountCashWithdwl;

    @JsonProperty("accounts")
    private List<Account> accounts;

    @JsonProperty("aggregateFlow")
    private AggregateFlow aggregateFlow;

    /*@JsonProperty("aggregateDebit")
    private AggregateDebitInfo aggregateDebit;

    @JsonProperty("aggregateCredit")
    private AggregateCreditInfo aggregateCredit;
*/
    @JsonProperty("riskEvents")
    private BankRiskEvents riskEvents;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
