package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 27/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class Permissions {

    // SMS permission given to read SMS
    @JsonProperty("sms")
    private Boolean sms;

    // Account permission given to read accounts and contacts
    @JsonProperty("account")
    private Boolean account;

    // Phone permission given to read calls, imei and sim information
    @JsonProperty("phone")
    private Boolean phone;

    // Location permission given to read gps & wifi location
    @JsonProperty("location")
    private Boolean location;

    // Calendar permission given to read calendar events
    @JsonProperty("calendar")
    private Boolean calendar;

    // Storage permission given to read metadata of media files
    @JsonProperty("storage")
    private Boolean storage;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
