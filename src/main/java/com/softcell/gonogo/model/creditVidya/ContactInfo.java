package com.softcell.gonogo.model.creditVidya;

import com.fasterxml.jackson.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("other")
public class ContactInfo {

    @JsonProperty("emailIds")
    private List<EmailInfo> emailInfoList;

    // Mobile number associated with SIM on device
    @JsonProperty("mobileNos")
    private List<String> mobileNos;

    @JsonProperty("alternateContact")
    private ConnectionProfile alternateContact;

    /*@JsonProperty("location")
    private Location location;*/

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();


    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}