package com.softcell.gonogo.model.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.stringtemplate.v4.ST;

/**
 * Created by ssguser on 28/12/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MifinAddress {

    @JsonProperty("sMifinAddrId")
    private String mifinAddrId;

    @JsonProperty("sAddrType")
    private String addrType;

    public void setMifinAddrId(String mifinAddrId) {
        this.mifinAddrId = mifinAddrId;
    }

    public String getMifinAddrId() { return mifinAddrId; }

    public void setAddrType(String addrType){
        this.addrType = addrType;
    }

    public String getAddrType() {return addrType; }

}
