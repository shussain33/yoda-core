package com.softcell.gonogo.model.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author kishorp This class extends 'Address', It is related with Customer
 *         Address.
 */
public class CustomerAddress extends Address implements Serializable {

    @JsonProperty("sAccm")
    private String accommodation;

    @JsonProperty("iTimeAtAddr")
    private int timeAtAddress;

    @JsonProperty("sAddrType")
    private String addressType;

    @JsonProperty("sResAddrType")
    private String residenceAddressType;

    @JsonProperty("sOfficeType")
    private String officeType;

    @JsonProperty("bSameAbove")
    private boolean sameAsAbove;

    @JsonProperty("iMonthAtCity")
    private int monthAtCity;

    @JsonProperty("iMonthAtAddr")
    private int monthAtAddress;

    @JsonProperty("dRentAmt")
    private double rentAmount;

    @JsonProperty("sFlatNo")
    private String flatNo;

    @JsonProperty("sStreet")
    private String street;

    @JsonProperty("sLocality")
    private String locality;

    /**
     * Newly Added attributes of hdfs
     */
    @JsonProperty("iYearAtCity")
    private int yearAtCity;

    /**
     * newly added field to find Negative Area purpose.
     */
    @JsonProperty("sNegativeArea")
    private String negativeArea;

    @JsonProperty("sNegativeAreaReason")
    private String negativeAreaReason;

    @JsonProperty("bNegativeAreaNotApplicableFlag")
    private boolean negativeAreaNotApplicableFlag;

    @JsonProperty("dLatitude")
    private double latitude;

    @JsonProperty("dLongitude")
    private double longitude;

    @JsonProperty("sLandLine")
    private String landLine;

    @JsonProperty("bSameResi")
    private boolean sameResi;

    @JsonProperty("bSameOffice")
    private boolean sameOffice;

    @JsonProperty("bSamePermanent")
    private boolean samePermanent;

    @JsonProperty("bDestinationAddress")
    private boolean destinationAddress;

    @JsonProperty("bMailingAddress")
    private boolean mailingAddress;

    @JsonProperty("sAddrCompanyName")
    private String addrCompanyName;

    @JsonProperty("sTopSevenCity")
    private String topSevenCity;

    @JsonProperty("sServiceablePin")
    private String serviceablePin;

    public String getServiceablePin() { return serviceablePin; }

    public void setServiceablePin(String serviceablePin) { this.serviceablePin = serviceablePin; }

    public String getTopSevenCity() {
        return topSevenCity;
    }

    public void setTopSevenCity(String topSevenCity) {
        this.topSevenCity = topSevenCity;
    }

    public void setDestinationAddress(boolean destinationAddress) { this.destinationAddress = destinationAddress; }

    public boolean getDestinationAddress() { return destinationAddress; }

    public void setMailingAddress(boolean mailingAddress ) { this.mailingAddress = mailingAddress; }

    public boolean getMailingAddress() { return mailingAddress;}

    public  void setAddrCompanyName(String addrCompanyName) { this.addrCompanyName =addrCompanyName; }

    public String getAddrCompanyName() { return addrCompanyName;}


    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public String getNegativeAreaReason() {
        return negativeAreaReason;
    }

    public void setNegativeAreaReason(String negativeAreaReason) {
        this.negativeAreaReason = negativeAreaReason;
    }

    public String getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(String accommodation) {
        this.accommodation = accommodation;
    }

    public int getTimeAtAddress() {
        return timeAtAddress;
    }

    public void setTimeAtAddress(int timeAtAddress) {
        this.timeAtAddress = timeAtAddress;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        if(StringUtils.isNotEmpty(addressType)){
        this.addressType = addressType.toUpperCase();
        }
    }

    public String getResidenceAddressType() {
        return residenceAddressType;
    }

    public void setResidenceAddressType(String residenceAddressType) {
        this.residenceAddressType = residenceAddressType;
    }

    public boolean isSameAsAbove() {
        return sameAsAbove;
    }

    public void setSameAsAbove(boolean sameAsAbove) {
        this.sameAsAbove = sameAsAbove;
    }

    public String getOfficeType() {
        return officeType;
    }

    public void setOfficeType(String officeType) {
        this.officeType = officeType;
    }

    public int getMonthAtCity() {
        return monthAtCity;
    }

    public void setMonthAtCity(int monthAtCity) {
        this.monthAtCity = monthAtCity;
    }

    public int getMonthAtAddress() {
        return monthAtAddress;
    }

    public void setMonthAtAddress(int monthAtAddress) {
        this.monthAtAddress = monthAtAddress;
    }

    public double getRentAmount() {
        return rentAmount;
    }

    public void setRentAmount(double rentAmount) {
        this.rentAmount = rentAmount;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public int getYearAtCity() {
        return yearAtCity;
    }

    public void setYearAtCity(int yearAtCity) {
        this.yearAtCity = yearAtCity;
    }

    public String getNegativeArea() {
        return negativeArea;
    }
    public void setNegativeArea(String negativeArea) {
        this.negativeArea = negativeArea;
    }

    public boolean isNegativeAreaNotApplicableFlag() {
        return negativeAreaNotApplicableFlag;
    }

    public void setNegativeAreaNotApplicableFlag(boolean negativeAreaNotApplicableFlag) {
        this.negativeAreaNotApplicableFlag = negativeAreaNotApplicableFlag;
    }

    public double getLatitude() { return latitude; }

    public void setLatitude(double latitude) { this.latitude = latitude; }

    public double getLongitude() { return longitude; }

    public void setLongitude(double longitude) { this.longitude = longitude; }

    public boolean isSameResi() { return sameResi; }

    public void setSameResi(boolean sameResi) { this.sameResi = sameResi; }

    public boolean isSameOffice() { return sameOffice; }

    public void setSameOffice(boolean sameOffice) { this.sameOffice = sameOffice; }

    public boolean isSamePermanent() { return samePermanent; }

    public void setSamePermanent(boolean samePermanent) { this.samePermanent = samePermanent; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CustomerAddress{");
        sb.append("accommodation='").append(accommodation).append('\'');
        sb.append(", timeAtAddress=").append(timeAtAddress);
        sb.append(", addressType='").append(addressType).append('\'');
        sb.append(", residenceAddressType='").append(residenceAddressType).append('\'');
        sb.append(", officeType='").append(officeType).append('\'');
        sb.append(", monthAtCity=").append(monthAtCity);
        sb.append(", monthAtAddress=").append(monthAtAddress);
        sb.append(", rentAmount=").append(rentAmount);
        sb.append(", flatNo='").append(flatNo).append('\'');
        sb.append(", street='").append(street).append('\'');
        sb.append(", locality='").append(locality).append('\'');
        sb.append(", yearAtCity=").append(yearAtCity);
        sb.append(", negativeArea='").append(negativeArea).append('\'');
        sb.append(", negativeAreaReason='").append(negativeAreaReason).append('\'');
        sb.append(", negativeAreaNotApplicableFlag=").append(negativeAreaNotApplicableFlag).append('\'');
        sb.append(", latitude='").append(latitude).append('\'');
        sb.append(", longitude=").append(longitude);
        sb.append('}');
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CustomerAddress that = (CustomerAddress) o;

        if (timeAtAddress != that.timeAtAddress) return false;
        if (monthAtCity != that.monthAtCity) return false;
        if (monthAtAddress != that.monthAtAddress) return false;
        if (Double.compare(that.rentAmount, rentAmount) != 0) return false;
        if (yearAtCity != that.yearAtCity) return false;
        if (negativeAreaNotApplicableFlag != that.negativeAreaNotApplicableFlag) return false;
        if (accommodation != null ? !accommodation.equals(that.accommodation) : that.accommodation != null)
            return false;
        if (addressType != null ? !addressType.equals(that.addressType) : that.addressType != null) return false;
        if (residenceAddressType != null ? !residenceAddressType.equals(that.residenceAddressType) : that.residenceAddressType != null)
            return false;
        if (officeType != null ? !officeType.equals(that.officeType) : that.officeType != null) return false;
        if (flatNo != null ? !flatNo.equals(that.flatNo) : that.flatNo != null) return false;
        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        if (locality != null ? !locality.equals(that.locality) : that.locality != null) return false;
        if (negativeArea != null ? !negativeArea.equals(that.negativeArea) : that.negativeArea != null) return false;
        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        return !(negativeAreaReason != null ? !negativeAreaReason.equals(that.negativeAreaReason) : that.negativeAreaReason != null);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + (accommodation != null ? accommodation.hashCode() : 0);
        result = 31 * result + timeAtAddress;
        result = 31 * result + (addressType != null ? addressType.hashCode() : 0);
        result = 31 * result + (residenceAddressType != null ? residenceAddressType.hashCode() : 0);
        result = 31 * result + (officeType != null ? officeType.hashCode() : 0);
        result = 31 * result + monthAtCity;
        result = 31 * result + monthAtAddress;
        temp = Double.doubleToLongBits(rentAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (flatNo != null ? flatNo.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (locality != null ? locality.hashCode() : 0);
        result = 31 * result + yearAtCity;
        result = 31 * result + (negativeArea != null ? negativeArea.hashCode() : 0);
        result = 31 * result + (negativeAreaReason != null ? negativeAreaReason.hashCode() : 0);
        result = 31 * result + (negativeAreaNotApplicableFlag ? 1 : 0);
        return result;
    }

    public interface FetchGrp{

    }
}
