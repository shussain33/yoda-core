    package com.softcell.gonogo.model.address;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author kishorp
 *         The basic residential details of customer. like city, pin state country,state.
 *         Line 1, Line 2, City, Pin code,  state and country are mandatory  fields.
 */
public class Address implements Serializable {

    @JsonProperty("sLine1")
    private String addressLine1;

    @JsonProperty("sLine2")
    private String addressLine2;

    @JsonProperty("sCity")
    private String city;

    @JsonProperty("iPinCode")
    private long pin;

    @JsonProperty("sState")
    private String state;

    @JsonProperty("sCountry")
    private String country;

    /**
     * Newly Added attributes of DMI
     */
    @JsonProperty("sLandLoard")
    private String landLoard;

    /**
     * Newly Added attributes of hdfs
     */
    @JsonProperty("sLine3")
    private String line3;

    @JsonProperty("sLine4")
    private String line4;

    @JsonProperty("sVillage")
    private String village;

    @JsonProperty("sTaluka")
    private String taluka;

    @JsonProperty("sDistrict")
    private String district;

    @JsonProperty("fDistFrom")
    private float distanceFrom;

    @JsonProperty("sLandMark")
    private String landMark;

    @JsonProperty("sOutOfGeoLimit")
    private String outOfGeoLimit;

    /**
     +     * Newly Added attributes of Sbfc-Pl
     +     */

    @JsonProperty("sLandLine")
    private String landLineNumber;

    @JsonProperty("sMifinAddressId")
    private String mifinAddressId;

    @JsonProperty("sMifinAddrMessage")
    private String mifinAddrMessage;

    @JsonProperty("sCopiedApplicantAddress")
    private String copiedApplicantAddress;

    @JsonProperty("bSameAsApplicant")
    private boolean sameAsApplicant;

            public String getLandLineNumber() {
                return landLineNumber;
            }

            public void setLandLineNumber(String landLineNumber) {
                this.landLineNumber = landLineNumber;
            }


    public String getLandLoard() {
        return landLoard;
    }

    public void setLandLoard(String landLoard) {
        this.landLoard = landLoard;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getOutOfGeoLimit() {
        return outOfGeoLimit;
    }

    public void setOutOfGeoLimit(String outOfGeoLimit) {
        this.outOfGeoLimit = outOfGeoLimit;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getPin() {
        return pin;
    }

    public void setPin(long pin) {
        this.pin = pin;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLine3() {
        return line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    public String getLine4() {
        return line4;
    }

    public void setLine4(String line4) {
        this.line4 = line4;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public float getDistanceFrom() {
        return distanceFrom;
    }

    public void setDistanceFrom(float distanceFrom) {
        this.distanceFrom = distanceFrom;
    }

    public String getLandMark() {
        return landMark;
    }

    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }

    public String getMifinAddressId() { return mifinAddressId; }

    public void setMifinAddressId(String mifinAddressId) { this.mifinAddressId = mifinAddressId; }

    public void setMifinAddrMessage(String mifinAddrMessage) { this.mifinAddrMessage = mifinAddrMessage; }

    public String getMifinAddrMessage() { return mifinAddrMessage; }

    public boolean isSameAsApplicant() { return sameAsApplicant; }

    public void setSameAsApplicant(boolean sameAsApplicant) { this.sameAsApplicant = sameAsApplicant; }

    public String getCopiedApplicantAddress() { return copiedApplicantAddress; }

    public void setCopiedApplicantAddress(String copiedApplicantAddress) { this.copiedApplicantAddress = copiedApplicantAddress; }

    @Override
    public String toString() {
        return "Address [addressLine1=" + addressLine1 + ", addressLine2="
                + addressLine2 + ", city=" + city + ", pin=" + pin + ", state="
                + state + ", country=" + country + ", line3=" + line3
                + ", line4=" + line4 + ", village=" + village + ", district="
                + district + ", distanceFrom=" + distanceFrom + ", landMark="
                + landMark + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((addressLine1 == null) ? 0 : addressLine1.hashCode());
        result = prime * result
                + ((addressLine2 == null) ? 0 : addressLine2.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + Float.floatToIntBits(distanceFrom);
        result = prime * result
                + ((district == null) ? 0 : district.hashCode());
        result = prime * result
                + ((landLoard == null) ? 0 : landLoard.hashCode());
        result = prime * result
                + ((landMark == null) ? 0 : landMark.hashCode());
        result = prime * result + ((line3 == null) ? 0 : line3.hashCode());
        result = prime * result + ((line4 == null) ? 0 : line4.hashCode());
        result = prime * result + (int) (pin ^ (pin >>> 32));
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result + ((village == null) ? 0 : village.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Address other = (Address) obj;
        if (addressLine1 == null) {
            if (other.addressLine1 != null)
                return false;
        } else if (!addressLine1.equals(other.addressLine1))
            return false;
        if (addressLine2 == null) {
            if (other.addressLine2 != null)
                return false;
        } else if (!addressLine2.equals(other.addressLine2))
            return false;
        if (city == null) {
            if (other.city != null)
                return false;
        } else if (!city.equals(other.city))
            return false;
        if (country == null) {
            if (other.country != null)
                return false;
        } else if (!country.equals(other.country))
            return false;
        if (Float.floatToIntBits(distanceFrom) != Float
                .floatToIntBits(other.distanceFrom))
            return false;
        if (district == null) {
            if (other.district != null)
                return false;
        } else if (!district.equals(other.district))
            return false;
        if (landLoard == null) {
            if (other.landLoard != null)
                return false;
        } else if (!landLoard.equals(other.landLoard))
            return false;
        if (landMark == null) {
            if (other.landMark != null)
                return false;
        } else if (!landMark.equals(other.landMark))
            return false;
        if (line3 == null) {
            if (other.line3 != null)
                return false;
        } else if (!line3.equals(other.line3))
            return false;
        if (line4 == null) {
            if (other.line4 != null)
                return false;
        } else if (!line4.equals(other.line4))
            return false;
        if (pin != other.pin)
            return false;
        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;
        if (village == null) {
            if (other.village != null)
                return false;
        } else if (!village.equals(other.village))
            return false;
        return true;
    }


}
