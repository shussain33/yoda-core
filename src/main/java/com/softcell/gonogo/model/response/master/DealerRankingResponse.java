package com.softcell.gonogo.model.response.master;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class DealerRankingResponse {

    @JsonProperty("iDealerRanking")
    private double dealerRanking;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResponseMsg")
    private String responseMsg;

    public double getDealerRanking() {
        return dealerRanking;
    }

    public void setDealerRanking(double dealerRanking) {
        this.dealerRanking = dealerRanking;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DealerRankingResponse [dealerRanking=");
        builder.append(dealerRanking);
        builder.append(", status=");
        builder.append(status);
        builder.append(", responseMsg=");
        builder.append(responseMsg);
        builder.append("]");
        return builder.toString();
    }

}
