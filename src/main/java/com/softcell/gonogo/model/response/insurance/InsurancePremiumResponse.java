package com.softcell.gonogo.model.response.insurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.InsurancePremiumMaster;

/**
 * Created by mahesh on 29/5/17.
 */
public class InsurancePremiumResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResMessage")
    private String resMessage;

    @JsonProperty("sSrNoOrImeiNo")
    private String serialOrImeiNumber;

    @JsonProperty("oInsurancePremiumMaster")
    private InsurancePremiumMaster insurancePremiumMaster;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResMessage() {
        return resMessage;
    }

    public void setResMessage(String resMessage) {
        this.resMessage = resMessage;
    }

    public InsurancePremiumMaster getInsurancePremiumMaster() {
        return insurancePremiumMaster;
    }

    public void setInsurancePremiumMaster(InsurancePremiumMaster insurancePremiumMaster) {
        this.insurancePremiumMaster = insurancePremiumMaster;
    }

    public String getSerialOrImeiNumber() {
        return serialOrImeiNumber;
    }

    public void setSerialOrImeiNumber(String serialOrImeiNumber) {
        this.serialOrImeiNumber = serialOrImeiNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InsurancePremiumResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", resMessage='").append(resMessage).append('\'');
        sb.append(", serialOrImeiNumber='").append(serialOrImeiNumber).append('\'');
        sb.append(", insurancePremiumMaster=").append(insurancePremiumMaster);
        sb.append('}');
        return sb.toString();
    }
}
