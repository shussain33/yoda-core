package com.softcell.gonogo.model.response.AmbitMifinResponse.SavePersonalInsurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SavePersonalInsuranceBaseResponse {
    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("MESSAGE")
    private String message;

    @JsonProperty("INSURANCE_DETAILS")
    private List<InsuranceDetails> insuranceDetailsList;
}
