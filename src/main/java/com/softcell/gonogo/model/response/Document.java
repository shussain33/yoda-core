package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Document {
    @JsonProperty("sDocID")
    private String docID;
    @JsonProperty("sStat")
    private String status;
    @JsonProperty("sDocName")
    private String docName;
    @JsonProperty("sByteCode")
    private String byteCode;
    @JsonProperty("sHtmlContent")
    private String htmlContent;

    public String getHtmlContent() {    return htmlContent; }

    public void setHtmlContent(String htmlContent) {    this.htmlContent = htmlContent; }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getByteCode() {
        return byteCode;
    }

    public void setByteCode(String byteCode) {
        this.byteCode = byteCode;
    }

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Document [docID=" + docID + ", status=" + status + ", docName="
                + docName + ", byteCode=" + byteCode + "]";
    }


}
