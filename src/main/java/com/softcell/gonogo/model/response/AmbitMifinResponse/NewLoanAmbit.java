package com.softcell.gonogo.model.response.AmbitMifinResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NewLoanAmbit {

    @JsonProperty("bodyStr")
    private String bodystrAmbit;

    @JsonProperty("body")
    private BodyAmbit bodyAmbit;
}