package com.softcell.gonogo.model.response.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class AckHeader {
    @JsonProperty("sAppID")
    private String applicationId;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("sResType")
    private String responseType;

    @JsonProperty("sReqRcvdTM")
    private Date requestReceivedTime;

    @JsonProperty("sResDT")
    private Date responseDateTime;

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }

    public Date getRequestReceivedTime() {
        return requestReceivedTime;
    }

    public void setRequestReceivedTime(Date requestReceivedTime) {
        this.requestReceivedTime = requestReceivedTime;
    }

    public Date getResponseDateTime() {
        return responseDateTime;
    }

    public void setResponseDateTime(Date responseDateTime) {
        this.responseDateTime = responseDateTime;
    }

    @Override
    public String toString() {
        return "AckHeader [applicationId=" + applicationId + ", institutionId="
                + institutionId + ", responseType=" + responseType
                + ", requestReceivedTime=" + requestReceivedTime
                + ", responseDateTime=" + responseDateTime + "]";
    }


}
