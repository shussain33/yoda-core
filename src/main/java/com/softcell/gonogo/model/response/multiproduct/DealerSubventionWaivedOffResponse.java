package com.softcell.gonogo.model.response.multiproduct;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 */
public class DealerSubventionWaivedOffResponse {
    @JsonProperty("bDlrSbvnWaivedOff")
    private boolean dealerSubventionWaivedOff;

    public DealerSubventionWaivedOffResponse() {
        super();
    }

    public DealerSubventionWaivedOffResponse(boolean dealerSubventionWaivedOff) {
        super();
        this.dealerSubventionWaivedOff = dealerSubventionWaivedOff;
    }

    public boolean isDealerSubventionWaivedOff() {
        return dealerSubventionWaivedOff;
    }

    public void setDealerSubventionWaivedOff(boolean dealerSubventionWaivedOff) {
        this.dealerSubventionWaivedOff = dealerSubventionWaivedOff;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DealerSubventionWaivedOffResponse [dealerSubventionWaivedOff=");
        builder.append(dealerSubventionWaivedOff);
        builder.append("]");
        return builder.toString();
    }

}
