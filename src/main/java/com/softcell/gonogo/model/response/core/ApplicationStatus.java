/**
 * kishorp3:25:09 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.response.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author kishorp
 *
 */
public class ApplicationStatus {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sAppID")
    private String appId;

    @JsonProperty("sInstID")
    private String instId;

    @JsonProperty("dtStart")
    private Date startTime;

    @JsonProperty("sStart")
    private String start;

    @JsonProperty("sPan")
    private String pan;

    @JsonProperty("sMb")
    private String mb;

    @JsonProperty("sDedupe")
    private String dedupe;

    @JsonProperty("dtDedupeInt")
    private Date dedupeInitiatedDt= new Date();

    @JsonProperty("sSaathi")
    private String saathi;

    @JsonProperty("sCreditVidya")
    private String creditVidya;

    @JsonProperty("sVerScore")
    private String verScore;

    @JsonProperty("sAppScore")
    private String appsScore;

    @JsonProperty("sCroStat")
    private String croStatus;

    @JsonProperty("sOtpStat")
    private String otpStatus;

    @JsonProperty("sEmailStat")
    private String emailStatus;

    @JsonProperty("dtETime")
    private Date endTime;

    @JsonProperty("sOffAddScore")
    private String officeAddress;

    @JsonProperty("sResiAddrScore")
    private String residanceAddScore;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getInstId() {
        return instId;
    }

    public void setInstId(String instId) {
        this.instId = instId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getMb() {
        return mb;
    }

    public void setMb(String mb) {
        this.mb = mb;
    }

    public String getDedupe() {
        return dedupe;
    }

    public void setDedupe(String dedupe) {
        this.dedupe = dedupe;
    }

    public String getVerScore() {
        return verScore;
    }

    public void setVerScore(String verScore) {
        this.verScore = verScore;
    }

    public String getAppsScore() {
        return appsScore;
    }

    public void setAppsScore(String appsScore) {
        this.appsScore = appsScore;
    }

    public String getCroStatus() {
        return croStatus;
    }

    public void setCroStatus(String croStatus) {
        this.croStatus = croStatus;
    }

    public String getOtpStatus() {
        return otpStatus;
    }

    public void setOtpStatus(String otpStatus) {
        this.otpStatus = otpStatus;
    }

    public String getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getResidanceAddScore() {
        return residanceAddScore;
    }

    public void setResidanceAddScore(String residanceAddScore) {
        this.residanceAddScore = residanceAddScore;
    }

    public String getSaathi() {
        return saathi;
    }

    public void setSaathi(String saathi) {
        this.saathi = saathi;
    }

    public String getCreditVidya() {
        return creditVidya;
    }

    public void setCreditVidya(String creditVidya) {
        this.creditVidya = creditVidya;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public String toString() {
        return "ApplicationStatus [refId=" + refId + ", appId=" + appId
                + ", instId=" + instId + ", startTime=" + startTime
                + ", start=" + start + ", pan=" + pan + ", mb=" + mb
                + ", dedupe=" + dedupe
                + ", saathi=" + saathi + ", creditVidya=" + creditVidya
                + ", verScore=" + verScore
                + ", appsScore=" + appsScore + ", croStatus=" + croStatus
                + ", otpStatus=" + otpStatus + ", emailStatus=" + emailStatus
                + ", endTime=" + endTime + ", officeAddress=" + officeAddress
                + ", residanceAddScore=" + residanceAddScore
                + ", additionalProperties=" + additionalProperties + "]";
    }
}
