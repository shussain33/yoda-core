package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "header",
        "documents"
})
public class KycImages {
    /**
     * contains document owner information
     */
    @JsonProperty("header")
    private KycImageHeader header;

    /**
     * contains all kyc documents
     */
    @JsonProperty("documents")
    private List<Document> documents = new ArrayList<Document>();

    public KycImageHeader getHeader() {
        return header;
    }

    public void setHeader(KycImageHeader header) {
        this.header = header;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "KycImages [header=" + header + ", documents=" + documents + "]";
    }


}
