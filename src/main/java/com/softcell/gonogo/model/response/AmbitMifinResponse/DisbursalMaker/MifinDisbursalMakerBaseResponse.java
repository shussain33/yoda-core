package com.softcell.gonogo.model.response.AmbitMifinResponse.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MifinDisbursalMakerBaseResponse {

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("MESSAGE")
    private String message;

    @JsonProperty("DISBURAL_DETAILS")
    private List<DisbursalDetailResponse> disbursalDetailResponses;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
