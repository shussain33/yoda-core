package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 23/3/17.
 */
public class NegativeAreaWithReason {

    @JsonProperty("sAreaName")
    private String areaName;

    @JsonProperty("sReason")
    private String reason;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NegativeAreaWithReason{");
        sb.append("areaName='").append(areaName).append('\'');
        sb.append(", reason='").append(reason).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
