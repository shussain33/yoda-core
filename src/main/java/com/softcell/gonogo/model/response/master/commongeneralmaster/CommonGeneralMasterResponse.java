package com.softcell.gonogo.model.response.master.commongeneralmaster;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 15/5/17.
 */
public class CommonGeneralMasterResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sMessage")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CommonGeneralMasterResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
