package com.softcell.gonogo.model.response.AmbitMifinResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MifinBaseAmbitResponse {

    @JsonProperty("newLoan")
    private String newLoan;

    @JsonProperty("saveApplicantDetails")
    private String saveApplicantDetails;

    @JsonProperty("saveApplicantAddress")
    private String saveApplicantAddress;

    @JsonProperty("saveFinancialInfo")
    private String saveFinancialInfo;

    @JsonProperty("panCardSearch")
    private String panCardSearch;

    @JsonProperty("updateLoanDetails")
    private String updateLoanDetails;

    @JsonProperty("saveVerification")
    private String saveVerification;

}