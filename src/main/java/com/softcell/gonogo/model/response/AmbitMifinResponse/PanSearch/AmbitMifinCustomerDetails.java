package com.softcell.gonogo.model.response.AmbitMifinResponse.PanSearch;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AmbitMifinCustomerDetails implements Serializable {

    @JsonProperty("PROSPECTCODE")
    private String prospectCode;

    @JsonProperty("PROSPECTID")
    private String prospectId;

    @JsonProperty("APPLICANTID")
    private String applicantId;

    @JsonProperty("APPLICANTCODE")
    private String applicantCode;

    @JsonProperty("CUSTOMERNAME")
    private String customerName;
}
