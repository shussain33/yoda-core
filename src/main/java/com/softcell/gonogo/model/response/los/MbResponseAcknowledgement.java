package com.softcell.gonogo.model.response.los;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by yogeshb on 8/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MbResponseAcknowledgement {

    private InnerMbResponseAcknowledgement multiBureauResponseAck;

    public InnerMbResponseAcknowledgement getMultiBureauResponseAck() {
        return multiBureauResponseAck;
    }

    public void setMultiBureauResponseAck(InnerMbResponseAcknowledgement multiBureauResponseAck) {
        this.multiBureauResponseAck = multiBureauResponseAck;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MbResponseAcknowledgement{");
        sb.append("multiBureauResponseAck=").append(multiBureauResponseAck);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MbResponseAcknowledgement that = (MbResponseAcknowledgement) o;

        return multiBureauResponseAck != null ? multiBureauResponseAck.equals(that.multiBureauResponseAck) : that.multiBureauResponseAck == null;
    }

    @Override
    public int hashCode() {
        return multiBureauResponseAck != null ? multiBureauResponseAck.hashCode() : 0;
    }
}
