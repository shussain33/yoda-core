package com.softcell.gonogo.model.response.AmbitMifinResponse.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BankingIdResponse {
    @JsonProperty("ACC_DTL_ID")
    private String accDtlId;

    @JsonProperty("BANKDETAILSID")
    private String bankDetailsId;
}

