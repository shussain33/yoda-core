package com.softcell.gonogo.model.response.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.masters.BankDetailsMaster;
import com.softcell.gonogo.model.masters.BankMaster;
import com.softcell.gonogo.model.masters.EmployerMaster;
import com.softcell.gonogo.model.masters.PinCodeMaster;
import com.softcell.gonogo.model.request.ApplicationRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0268 on 17/9/19.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpadateResponse {

    @JsonProperty("aRefId")
    private String refId;

    @JsonProperty("sEncryptedPassword")
    private String encyptedpassword;

    @JsonProperty("oApplicationRequest")
    private List<ApplicationRequest> applicationRequest;

    @JsonProperty("oBankDetailMaster")
    BankDetailsMaster bankDetailsMaster;

    @JsonProperty("sMsg")
    String message;

    @JsonProperty("oPincodeMaster")
    private PinCodeMaster pinCodeMaster;

    @JsonProperty("oCompnyMaster")
    EmployerMaster employerMaster;

    @JsonProperty("oBankMaster")
    BankMaster bankMaster;

    @JsonProperty("aBankMaster")
    List<BankMaster> bankMasterList;

    @JsonProperty("aApplicant")
    List<Applicant> applicant;
}
