package com.softcell.gonogo.model.response.los;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by yogeshb on 8/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MbEndOfTransactionAcknowledgement {

    private InnerMbEndOfTransactionAcknowledgement multiBureauEoTAck;

    public InnerMbEndOfTransactionAcknowledgement getMultiBureauEoTAck() {
        return multiBureauEoTAck;
    }

    public void setMultiBureauEoTAck(InnerMbEndOfTransactionAcknowledgement multiBureauEoTAck) {
        this.multiBureauEoTAck = multiBureauEoTAck;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MbEndOfTransactionAcknowledgement{");
        sb.append("multiBureauEoTAck=").append(multiBureauEoTAck);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MbEndOfTransactionAcknowledgement that = (MbEndOfTransactionAcknowledgement) o;

        return multiBureauEoTAck != null ? multiBureauEoTAck.equals(that.multiBureauEoTAck) : that.multiBureauEoTAck == null;
    }

    @Override
    public int hashCode() {
        return multiBureauEoTAck != null ? multiBureauEoTAck.hashCode() : 0;
    }
}
