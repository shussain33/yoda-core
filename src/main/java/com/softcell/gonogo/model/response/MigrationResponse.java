package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ssg0268 on 8/10/19.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MigrationResponse {
    @JsonProperty("aCountAcordingToSecId")
    private HashMap<String,Long> secIdCount;

    @JsonProperty("aUsertdPrimaryIdRecord")
    private HashMap<String,Integer> UpdateSecId ;

}
