package com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicantMatchedAddress {

    @JsonProperty("ADDRESSTYPE")
    private String addretype;

    @JsonProperty("CELL_ORIG")
    private String cellOrig;

    @JsonProperty("STATE")
    private String state;

    @JsonProperty("ISD_ORIG")
    private String isoOrig;

    @JsonProperty("LANDLINE1_ORIG")
    private String landline1Orig;

    @JsonProperty("LANDLINE2")
    private String landline2;

    @JsonProperty("LANDLINE1")
    private String landline1;


    @JsonProperty("FLAT")
    private String flat;

    @JsonProperty("STREET")
    private String street;

    @JsonProperty("LOCALITY")
    private String locality;

    @JsonProperty("COMPLETEADDRESS_SP")
    private String completeaddresSp;

    @JsonProperty("ADDRESS2")
    private String address2;

    @JsonProperty("ADDRESS3")
    private String address3;

    @JsonProperty("COMPLETEADDRESS")
    private String completeAddress;

    @JsonProperty("BUILDINGNAME")
    private String buildingname;

    @JsonProperty("ADDRESS1")
    private String address1;

    @JsonProperty("ZIP")
    private String zip;

    @JsonProperty("CELL")
    private String cell;

    @JsonProperty("S_L_O")
    private String slo;

    @JsonProperty("RECORDID")
    private String resordid;

    @JsonProperty("CITY_ORIG")
    private String cityOrig;

    @JsonProperty("ADDRESS1_ORIG")
    private String address1Orig;

    @JsonProperty("COUNTRY")
    private String country;

    @JsonProperty("LANDLINE2_ORIG")
    private String landline2Orig;

    @JsonProperty("CUSTID")
    private String custId;

    @JsonProperty("ADDRESS4_ORIG")
    private String address4Orig;

    @JsonProperty("ADDRESS4")
    private String address4;

    @JsonProperty("ADDRESS2_ORIG")
    private String address2orig;

    @JsonProperty("ISD")
    private String isd;

    @JsonProperty("ADDRESS3_ORIG")
    private String address3orig;

    @JsonProperty("CITY")
    private String city;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }


}
