/**
 *
 */
package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.softcell.gonogo.model.core.kyc.response.aadhar.AadharMainResponse;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponse;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.creditVidya.UserProfileResponse;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.ntc.NTCResponse;
import com.softcell.gonogo.model.scoring.AddressScoringResult;
import com.softcell.gonogo.model.saathi.SaathiResponse;
import java.util.Date;
import java.util.List;

/**
 * @author kishorp
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ComponentResponse {

    private String referenceId;

    private String applicationId;

    private String applicantId;

    private Date finishedDate;

    /**
     * MB Response
     */
    private ResponseMultiJsonDomain multiBureauJsonRespose;

    /**
     * MBCorp Response
     */
    private OutputAckDomain mbCorpJsonResponse;
    /**
     * Kyc pan service response
     */
    private PanResponse panServiceResponse;

    /**
     * posidex service response
     */
    private PosidexResponse posidexResponse;

    /**
     * Adhar service Response
     */
    private AadharMainResponse adharServiceResponse;

    /**
     * Scoring service
     */
    private ScoringResponse scoringServiceResponse;
    /**
     * All address Scoring
     */
    private List<AddressScoringResult> addressScoringResult;

    /**
     * For NTC response based on cibil score less then zero
     */
    private NTCResponse ntcResponse;

    // Response from CreditVidya API
    private UserProfileResponse creditVidyaResponse;

    // Response from TVS CD API
    private SaathiResponse saathiResponse;


    public List<AddressScoringResult> getAddressScoringResult() {
        return addressScoringResult;
    }

    public void setAddressScoringResult(
            List<AddressScoringResult> addressScoringResult) {
        this.addressScoringResult = addressScoringResult;
    }

    /**
     * @return the referenceId
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * @param referenceId the referenceId to set
     */
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    /**
     * @return the applicationId
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * @param applicationId the applicationId to set
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * @return the applicantId
     */
    public String getApplicantId() {
        return applicantId;
    }

    /**
     * @param applicantId the applicantId to set
     */
    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    /**
     * @return the finishedDate
     */
    public Date getFinishedDate() {
        return finishedDate;
    }

    /**
     * @param finishedDate the finishedDate to set
     */
    public void setFinishedDate(Date finishedDate) {
        this.finishedDate = finishedDate;
    }

    /**
     * @return the multiBureauJsonRespose
     */
    public ResponseMultiJsonDomain getMultiBureauJsonRespose() {
        return multiBureauJsonRespose;
    }

    /**
     * @param multiBureauJsonRespose the multiBureauJsonRespose to set
     */
    public void setMultiBureauJsonRespose(
            ResponseMultiJsonDomain multiBureauJsonRespose) {
        this.multiBureauJsonRespose = multiBureauJsonRespose;
    }

    /**
     * @return the panServiceResponse
     */
    public PanResponse getPanServiceResponse() {
        return panServiceResponse;
    }

    /**
     * @param panServiceResponse the panServiceResponse to set
     */
    public void setPanServiceResponse(PanResponse panServiceResponse) {
        this.panServiceResponse = panServiceResponse;
    }

    /**
     * @return the adharServiceResponse
     */
    public AadharMainResponse getAdharServiceResponse() {
        return adharServiceResponse;
    }

    /**
     * @param adharServiceResponse the adharServiceResponse to set
     */
    public void setAdharServiceResponse(AadharMainResponse adharServiceResponse) {
        this.adharServiceResponse = adharServiceResponse;
    }

    /**
     * @return the scoringServiceResponse
     */
    public ScoringResponse getScoringServiceResponse() {
        return scoringServiceResponse;
    }

    /**
     * @param scoringServiceResponse the scoringServiceResponse to set
     */
    public void setScoringServiceResponse(ScoringResponse scoringServiceResponse) {
        this.scoringServiceResponse = scoringServiceResponse;
    }

    /**
     * @return the ntcResponse
     */
    public NTCResponse getNtcResponse() {
        return ntcResponse;
    }

    /**
     * @param ntcResponse the ntcResponse to set
     */
    public void setNtcResponse(NTCResponse ntcResponse) {
        this.ntcResponse = ntcResponse;
    }


    public PosidexResponse getPosidexResponse() {
        return posidexResponse;
    }

    public void setPosidexResponse(PosidexResponse posidexResponse) {
        this.posidexResponse = posidexResponse;
    }

    public UserProfileResponse getCreditVidyaResponse() {
        return creditVidyaResponse;
    }

    public void setCreditVidyaResponse(UserProfileResponse creditVidyaResponse) {
        this.creditVidyaResponse = creditVidyaResponse;
    }

    public SaathiResponse getSaathiResponse() {
        return saathiResponse;
    }

    public void setSaathiResponse(SaathiResponse saathiResponse) {
        this.saathiResponse = saathiResponse;
    }

    public OutputAckDomain getMbCorpJsonResponse() {
        return mbCorpJsonResponse;
    }

    public void setMbCorpJsonResponse(OutputAckDomain mbCorpJsonResponse) {
        this.mbCorpJsonResponse = mbCorpJsonResponse;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComponentResponse{");
        sb.append("referenceId='").append(referenceId).append('\'');
        sb.append(", applicationId='").append(applicationId).append('\'');
        sb.append(", applicantId='").append(applicantId).append('\'');
        sb.append(", finishedDate=").append(finishedDate);
        sb.append(", multiBureauJsonRespose=").append(multiBureauJsonRespose);
        sb.append(", panServiceResponse=").append(panServiceResponse);
        sb.append(", posidexResponse=").append(posidexResponse);
        sb.append(", adharServiceResponse=").append(adharServiceResponse);
        sb.append(", scoringServiceResponse=").append(scoringServiceResponse);
        sb.append(", addressScoringResult=").append(addressScoringResult);
        sb.append(", ntcResponse=").append(ntcResponse);
        sb.append(", creditVidyaResponse=").append(creditVidyaResponse);
        sb.append(", saathiGetCdResponse=").append(saathiResponse);
        sb.append(", mbCorpJsonResponse=").append(mbCorpJsonResponse);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ComponentResponse that = (ComponentResponse) o;

        if (referenceId != null ? !referenceId.equals(that.referenceId) : that.referenceId != null) return false;
        if (applicationId != null ? !applicationId.equals(that.applicationId) : that.applicationId != null)
            return false;
        if (applicantId != null ? !applicantId.equals(that.applicantId) : that.applicantId != null) return false;
        if (finishedDate != null ? !finishedDate.equals(that.finishedDate) : that.finishedDate != null) return false;
        if (multiBureauJsonRespose != null ? !multiBureauJsonRespose.equals(that.multiBureauJsonRespose) : that.multiBureauJsonRespose != null)
            return false;
        if (panServiceResponse != null ? !panServiceResponse.equals(that.panServiceResponse) : that.panServiceResponse != null)
            return false;
        if (posidexResponse != null ? !posidexResponse.equals(that.posidexResponse) : that.posidexResponse != null)
            return false;
        if (adharServiceResponse != null ? !adharServiceResponse.equals(that.adharServiceResponse) : that.adharServiceResponse != null)
            return false;
        if (scoringServiceResponse != null ? !scoringServiceResponse.equals(that.scoringServiceResponse) : that.scoringServiceResponse != null)
            return false;
        if (addressScoringResult != null ? !addressScoringResult.equals(that.addressScoringResult) : that.addressScoringResult != null)
            return false;
        if( ntcResponse != null ? !ntcResponse.equals(that.ntcResponse) : that.ntcResponse != null)
            return false;
        if( creditVidyaResponse != null ? !creditVidyaResponse.equals(that.creditVidyaResponse) : that.creditVidyaResponse != null)
            return false;
        return saathiResponse != null ? saathiResponse.equals(that.saathiResponse) : that.saathiResponse == null;
    }

    @Override
    public int hashCode() {
        int result = referenceId != null ? referenceId.hashCode() : 0;
        result = 31 * result + (applicationId != null ? applicationId.hashCode() : 0);
        result = 31 * result + (applicantId != null ? applicantId.hashCode() : 0);
        result = 31 * result + (finishedDate != null ? finishedDate.hashCode() : 0);
        result = 31 * result + (multiBureauJsonRespose != null ? multiBureauJsonRespose.hashCode() : 0);
        result = 31 * result + (panServiceResponse != null ? panServiceResponse.hashCode() : 0);
        result = 31 * result + (posidexResponse != null ? posidexResponse.hashCode() : 0);
        result = 31 * result + (adharServiceResponse != null ? adharServiceResponse.hashCode() : 0);
        result = 31 * result + (scoringServiceResponse != null ? scoringServiceResponse.hashCode() : 0);
        result = 31 * result + (addressScoringResult != null ? addressScoringResult.hashCode() : 0);
        result = 31 * result + (ntcResponse != null ? ntcResponse.hashCode() : 0);
        result = 31 * result + (creditVidyaResponse != null ? creditVidyaResponse.hashCode() : 0);
        result = 31 * result + (saathiResponse != null ? saathiResponse.hashCode() : 0);

        return result;
    }
}
