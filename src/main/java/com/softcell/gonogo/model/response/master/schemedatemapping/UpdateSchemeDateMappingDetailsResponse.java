package com.softcell.gonogo.model.response.master.schemedatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class UpdateSchemeDateMappingDetailsResponse {

    @JsonProperty("sResMessage")
    private String resMessage;

    @JsonProperty("sStatus")
    private String status;

    public String getResMessage() {
        return resMessage;
    }

    public void setResMessage(String resMessage) {
        this.resMessage = resMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UpdateSchemeDetailsResponse [resMessage=");
        builder.append(resMessage);
        builder.append(", status=");
        builder.append(status);
        builder.append("]");
        return builder.toString();
    }


}
