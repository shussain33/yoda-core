package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.CroJustification;
import com.softcell.gonogo.model.core.user.CreditRiskOfficer;
import com.softcell.gonogo.model.response.core.*;
import com.softcell.gonogo.model.surrogate.Surrogate;

import java.util.Date;
import java.util.List;
import java.util.Vector;

public class ApplicationResponse implements Comparable<ApplicationResponse> {
    @JsonProperty("oHeader")
    private AckHeader header;

    @JsonProperty("sRefID")
    private String gonogoRefId;

    @JsonProperty("sAppStat")
    private String applicationStatus;

    @JsonProperty("aCroDec")
    private List<CroDecision> croDecisions;

    @JsonProperty("oIntrmStat")
    private InterimStatus interimStatus;

    @JsonProperty("aCustAttr")
    private List<CustomAttributes> customAttributesList;

    @JsonProperty("oCreRskOff")
    private CreditRiskOfficer creditRiskOfficer;

    @JsonProperty("aAppScoRslt")
    private Vector<ModuleOutcome> moduleOutcomes;

    @JsonProperty("oAllMdlStat")
    private ApplicationStatus allModuleStatus = new ApplicationStatus();

    @JsonProperty("aCroJustification")
    private List<CroJustification> croJustification;

    @JsonProperty("dAppliedAmt")
    private double appliedAmount;

    @JsonProperty("oSurrogate")
    private Surrogate surrogate;

    @JsonProperty("sReApprsReas")
    private String reAppraiseReason;

    @JsonProperty("sReAppraiseRmk")
    private String reAppraiseRemark;

    @JsonIgnore
    private Date submitDate;

    @JsonProperty("bQdeDecision")
    private boolean qdeDecision;

    /**
     * @return the header
     */
    public AckHeader getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(AckHeader header) {
        this.header = header;
    }

    /**
     * @return the gonogoRefId
     */
    public String getGonogoRefId() {
        return gonogoRefId;
    }

    /**
     * @param gonogoRefId the gonogoRefId to set
     */
    public void setGonogoRefId(String gonogoRefId) {
        this.gonogoRefId = gonogoRefId;
    }

    /**
     * @return the applicationStatus
     */
    public String getApplicationStatus() {
        return applicationStatus;
    }

    /**
     * @param applicationStatus the applicationStatus to set
     */
    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    /**
     * @return the croDecisions
     */
    public List<CroDecision> getCroDecisions() {
        return croDecisions;
    }

    /**
     * @param croDecisions the croDecisions to set
     */
    public void setCroDecisions(List<CroDecision> croDecisions) {
        this.croDecisions = croDecisions;
    }

    /**
     * @return the interimStatus
     */
    public InterimStatus getInterimStatus() {
        return interimStatus;
    }

    /**
     * @param interimStatus the interimStatus to set
     */
    public void setInterimStatus(InterimStatus interimStatus) {
        this.interimStatus = interimStatus;
    }

    /**
     * @return the customAttributesList
     */
    public List<CustomAttributes> getCustomAttributesList() {
        return customAttributesList;
    }

    /**
     * @param customAttributesList the customAttributesList to set
     */
    public void setCustomAttributesList(List<CustomAttributes> customAttributesList) {
        this.customAttributesList = customAttributesList;
    }

    /**
     * @return the creditRiskOfficer
     */
    public CreditRiskOfficer getCreditRiskOfficer() {
        return creditRiskOfficer;
    }

    /**
     * @param creditRiskOfficer the creditRiskOfficer to set
     */
    public void setCreditRiskOfficer(CreditRiskOfficer creditRiskOfficer) {
        this.creditRiskOfficer = creditRiskOfficer;
    }

    /**
     * @return the moduleOutcomes
     */
    public Vector<ModuleOutcome> getModuleOutcomes() {
        return moduleOutcomes;
    }

    /**
     * @param moduleOutcomes the moduleOutcomes to set
     */
    public void setModuleOutcomes(
            Vector<ModuleOutcome> moduleOutcomes) {
        this.moduleOutcomes = moduleOutcomes;
    }

    /**
     * @return the allModuleStatus
     */
    public ApplicationStatus getAllModuleStatus() {
        return allModuleStatus;
    }

    /**
     * @param allModuleStatus the allModuleStatus to set
     */
    public void setAllModuleStatus(ApplicationStatus allModuleStatus) {
        this.allModuleStatus = allModuleStatus;
    }

    /**
     * @return the croJustification
     */
    public List<CroJustification> getCroJustification() {
        return croJustification;
    }

    /**
     * @param croJustification the croJustification to set
     */
    public void setCroJustification(List<CroJustification> croJustification) {
        this.croJustification = croJustification;
    }

    /**
     * @return the appliedAmount
     */
    public double getAppliedAmount() {
        return appliedAmount;
    }


    /**
     * @param appliedAmount the appliedAmount to set
     */
    public void setAppliedAmount(double appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    /**
     * @return the surrogate
     */
    public Surrogate getSurrogate() {
        return surrogate;
    }

    /**
     * @param surrogate the surrogate to set
     */
    public void setSurrogate(Surrogate surrogate) {
        this.surrogate = surrogate;
    }

    /**
     * @return the reAppraiseReason
     */
    public String getReAppraiseReason() {
        return reAppraiseReason;
    }

    /**
     * @param reAppraiseReason the reAppraiseReason to set
     */
    public void setReAppraiseReason(String reAppraiseReason) {
        this.reAppraiseReason = reAppraiseReason;
    }

    /**
     * @return the reAppraiseRemark
     */
    public String getReAppraiseRemark() {
        return reAppraiseRemark;
    }

    /**
     * @param reAppraiseRemark the reAppraiseRemark to set
     */
    public void setReAppraiseRemark(String reAppraiseRemark) {
        this.reAppraiseRemark = reAppraiseRemark;
    }

    /**
     * @return the submitDate
     */
    public Date getSubmitDate() {
        return submitDate;
    }

    /**
     * @param submitDate the submitDate to set
     */
    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public boolean isQdeDecision() {
        return qdeDecision;
    }

    public void setQdeDecision(boolean qdeDecision) {
        this.qdeDecision = qdeDecision;
    }



    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ApplicationResponse [header=");
        builder.append(header);
        builder.append(", gonogoRefId=");
        builder.append(gonogoRefId);
        builder.append(", applicationStatus=");
        builder.append(applicationStatus);
        builder.append(", croDecisions=");
        builder.append(croDecisions);
        builder.append(", interimStatus=");
        builder.append(interimStatus);
        builder.append(", customAttributesList=");
        builder.append(customAttributesList);
        builder.append(", creditRiskOfficer=");
        builder.append(creditRiskOfficer);
        builder.append(", moduleOutcomes=");
        builder.append(moduleOutcomes);
        builder.append(", allModuleStatus=");
        builder.append(allModuleStatus);
        builder.append(", croJustification=");
        builder.append(croJustification);
        builder.append(", appliedAmount=");
        builder.append(appliedAmount);
        builder.append(", surrogate=");
        builder.append(surrogate);
        builder.append(", reAppraiseReason=");
        builder.append(reAppraiseReason);
        builder.append(", reAppraiseRemark=");
        builder.append(reAppraiseRemark);
        builder.append(", submitDate=");
        builder.append(submitDate);
        builder.append("]");
        return builder.toString();
    }


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((allModuleStatus == null) ? 0 : allModuleStatus.hashCode());
        result = prime
                * result
                + ((moduleOutcomes == null) ? 0
                : moduleOutcomes.hashCode());
        result = prime
                * result
                + ((applicationStatus == null) ? 0 : applicationStatus
                .hashCode());
        long temp;
        temp = Double.doubleToLongBits(appliedAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime
                * result
                + ((creditRiskOfficer == null) ? 0 : creditRiskOfficer
                .hashCode());
        result = prime * result
                + ((croDecisions == null) ? 0 : croDecisions.hashCode());
        result = prime
                * result
                + ((croJustification == null) ? 0 : croJustification.hashCode());
        result = prime
                * result
                + ((customAttributesList == null) ? 0 : customAttributesList
                .hashCode());
        result = prime * result
                + ((gonogoRefId == null) ? 0 : gonogoRefId.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result
                + ((interimStatus == null) ? 0 : interimStatus.hashCode());
        result = prime
                * result
                + ((reAppraiseReason == null) ? 0 : reAppraiseReason.hashCode());
        result = prime
                * result
                + ((reAppraiseRemark == null) ? 0 : reAppraiseRemark.hashCode());
        result = prime * result
                + ((submitDate == null) ? 0 : submitDate.hashCode());
        result = prime * result
                + ((surrogate == null) ? 0 : surrogate.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApplicationResponse other = (ApplicationResponse) obj;
        if (allModuleStatus == null) {
            if (other.allModuleStatus != null)
                return false;
        } else if (!allModuleStatus.equals(other.allModuleStatus))
            return false;
        if (moduleOutcomes == null) {
            if (other.moduleOutcomes != null)
                return false;
        } else if (!moduleOutcomes
                .equals(other.moduleOutcomes))
            return false;
        if (applicationStatus == null) {
            if (other.applicationStatus != null)
                return false;
        } else if (!applicationStatus.equals(other.applicationStatus))
            return false;
        if (Double.doubleToLongBits(appliedAmount) != Double
                .doubleToLongBits(other.appliedAmount))
            return false;
        if (creditRiskOfficer == null) {
            if (other.creditRiskOfficer != null)
                return false;
        } else if (!creditRiskOfficer.equals(other.creditRiskOfficer))
            return false;
        if (croDecisions == null) {
            if (other.croDecisions != null)
                return false;
        } else if (!croDecisions.equals(other.croDecisions))
            return false;
        if (croJustification == null) {
            if (other.croJustification != null)
                return false;
        } else if (!croJustification.equals(other.croJustification))
            return false;
        if (customAttributesList == null) {
            if (other.customAttributesList != null)
                return false;
        } else if (!customAttributesList.equals(other.customAttributesList))
            return false;
        if (gonogoRefId == null) {
            if (other.gonogoRefId != null)
                return false;
        } else if (!gonogoRefId.equals(other.gonogoRefId))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (interimStatus == null) {
            if (other.interimStatus != null)
                return false;
        } else if (!interimStatus.equals(other.interimStatus))
            return false;
        if (reAppraiseReason == null) {
            if (other.reAppraiseReason != null)
                return false;
        } else if (!reAppraiseReason.equals(other.reAppraiseReason))
            return false;
        if (reAppraiseRemark == null) {
            if (other.reAppraiseRemark != null)
                return false;
        } else if (!reAppraiseRemark.equals(other.reAppraiseRemark))
            return false;
        if (submitDate == null) {
            if (other.submitDate != null)
                return false;
        } else if (!submitDate.equals(other.submitDate))
            return false;
        if (surrogate == null) {
            if (other.surrogate != null)
                return false;
        } else if (!surrogate.equals(other.surrogate))
            return false;
        return true;
    }

    @Override
    public int compareTo(ApplicationResponse appResponse) {
        return this.submitDate.compareTo(appResponse.submitDate);
    }
}
