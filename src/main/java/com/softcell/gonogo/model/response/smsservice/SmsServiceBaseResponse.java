package com.softcell.gonogo.model.response.smsservice;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

/**
 * @author vinodk
 */
public class SmsServiceBaseResponse {

    @JsonProperty("aErrors")
    private Error[] errors;

    @JsonProperty("aSmsStatus")
    private SmsStatus[] smsStatus;

    /**
     * @return the errors
     */
    public Error[] getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(Error[] errors) {
        this.errors = errors;
    }

    /**
     * @return the smsStatus
     */
    public SmsStatus[] getSmsStatus() {
        return smsStatus;
    }

    /**
     * @param smsStatus the smsStatus to set
     */
    public void setSmsStatus(SmsStatus[] smsStatus) {
        this.smsStatus = smsStatus;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsServiceBaseResponse [errors=");
        builder.append(Arrays.toString(errors));
        builder.append(", smsStatus=");
        builder.append(Arrays.toString(smsStatus));
        builder.append("]");
        return builder.toString();
    }
}