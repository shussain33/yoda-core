package com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerPeralInfo {
    @JsonProperty("APPLICANT_DETAILS")
    private List<ApplicantDetails> applicantDetailsList;

    @JsonProperty("APPLICANT_ADDRESS")
    private List<ApplicantMatchedAddress> applicantMatchedAddressesList;

    @JsonProperty("APPLICANT_BANKACCOUNT")
    private List<ApplicantBranch> applicantBranches;

}
