package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dv-stl-07 on 13/4/17.
 */
public class ApplicationStatusResponse {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sApplStatus")
    private String applicationStatus;

    @JsonProperty("sApplStage")
    private String applicationStage;

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public String getApplicationStage() {
        return applicationStage;
    }

    public void setApplicationStage(String applicationStage) {
        this.applicationStage = applicationStage;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApplicationStatusResponse{");
        sb.append("refID='").append(refID).append('\'');
        sb.append(", applicationStatus='").append(applicationStatus).append('\'');
        sb.append(", applicationStage='").append(applicationStage).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
