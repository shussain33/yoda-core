package com.softcell.gonogo.model.response.AmbitMifinResponse.SaveApplicantAddress;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveApplicantAddressBody {

    @JsonProperty("RECEIVE")
    public MiFinSaveApplicantAddressBaseResponse miFinSaveApplicantAddressBaseResponse;
}