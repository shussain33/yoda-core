package com.softcell.gonogo.model.response.master.schemedatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class AssetCostValidationResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResMsg")
    private String resMsg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AssetCostValidationResponse [status=");
        builder.append(status);
        builder.append(", resMsg=");
        builder.append(resMsg);
        builder.append("]");
        return builder.toString();
    }


}
