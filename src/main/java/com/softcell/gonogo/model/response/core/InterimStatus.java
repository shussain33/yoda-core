package com.softcell.gonogo.model.response.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.ntc.NTCGnGStatus;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author kishorp
 */
public class InterimStatus {

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sAppID")
    private String appId;

    @JsonProperty("sInstID")
    private String instId;

    @JsonProperty("dtStart")
    private Date startTime = new Date();

    @JsonProperty("dtETime")
    private Date endTime;

    @JsonProperty("sAppStart")
    private String appStart = Status.DEFAULT.toString();

    @JsonProperty("sDedupe")
    private String dedupe = Status.DEFAULT.toString();

    @JsonProperty("sPosidexDedupeStat")
    private String posidexDedupeStatus = Status.DEFAULT.toString();

    @JsonProperty("sEmailStat")
    private String emailStatus = Status.DEFAULT.toString();

    @JsonProperty("sOtpStat")
    private String otpStatus = Status.DEFAULT.toString();

    @JsonProperty("sAppStat")
    private String appsStatus = Status.DEFAULT.toString();

    @JsonProperty("sPanStat")
    private String panStatus = Status.DEFAULT.toString();

    @JsonProperty("sAadharStat")
    private String aadharStatus = Status.DEFAULT.toString();

    @JsonProperty("sMbStat")
    private String mbStatus = Status.DEFAULT.toString();

    @JsonProperty("sCreditVidyaStat")
    private String creditVidyaStatus = Status.DEFAULT.toString();

    @JsonProperty("sSaathiPullStat")
    private String saathiPullStatus = Status.DEFAULT.toString();

    @JsonProperty("sVarScoreStat")
    private String varScoreStatus = Status.DEFAULT.toString();

    @JsonProperty("sScoreStat")
    private String scoreStatus = Status.DEFAULT.toString();

    @JsonProperty("sCblScore")
    private String cibilScore = Status.DEFAULT.toString();

    @JsonProperty("sExperianStat")
    private String experianStatus = Status.DEFAULT.toString();

    @JsonProperty("sHighmarkStat")
    private String highmarkStatus = Status.DEFAULT.toString();

    @JsonProperty("sCroStat")
    private String croStatus = Status.DEFAULT.toString();

    @JsonIgnore
    private String awsS3Status = Status.DEFAULT.toString();

    @JsonProperty("sNtcStatus")
    private String ntcStatus = Status.DEFAULT.toString();

    @JsonProperty("oNTCResult")
    private NTCGnGStatus ntcGngStatus;

    @JsonProperty("oPanResult")
    private ModuleOutcome panModuleResult;

    @JsonProperty("oCibilResult")
    private ModuleOutcome cibilModuleResult;

    @JsonProperty("oResAddressResult")
    private ModuleOutcome residenceAddressResult;

    @JsonProperty("oOffAddressResult")
    private ModuleOutcome officeModuleResult;

    @JsonProperty("oScoringResult")
    private ModuleOutcome scoringModuleResult;

    @JsonProperty("oAadharResult")
    private ModuleOutcome aadharModuleResult;

    @JsonProperty("oExperianResult")
    private ModuleOutcome experianModuleResult;

    @JsonProperty("oEquifaxResult")
    private ModuleOutcome equifaxModuleResult;

    @JsonProperty("oCHMResult")
    private ModuleOutcome chmModuleResult;

    @JsonProperty("oMbResult")
    private ModuleOutcome mbModuleResult;

    @JsonProperty("oCrediVidyaResult")
    private ModuleOutcome creditVidyaModuleResult;

    @JsonProperty("oSaathiResult")
    private ModuleOutcome saathiModuleResult;

    @JsonProperty("oPosidexResult")
    private ModuleOutcome posidexModuleResult;

    @JsonProperty("oInternalDedupeResult")
    private ModuleOutcome internalDedupeResult;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }

    /**
     * @return the appId
     */
    public String getAppId() {
        return appId;
    }

    /**
     * @param appId the appId to set
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /**
     * @return the instId
     */
    public String getInstId() {
        return instId;
    }

    /**
     * @param instId the instId to set
     */
    public void setInstId(String instId) {
        this.instId = instId;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the appStart
     */
    public String getAppStart() {
        return appStart;
    }

    /**
     * @param appStart the appStart to set
     */
    public void setAppStart(String appStart) {
        this.appStart = appStart;
    }

    /**
     * @return the dedupe
     */
    public String getDedupe() {
        return dedupe;
    }

    /**
     * @param dedupe the dedupe to set
     */
    public void setDedupe(String dedupe) {
        this.dedupe = dedupe;
    }

    /**
     * @return the emailStatus
     */
    public String getEmailStatus() {
        return emailStatus;
    }

    /**
     * @param emailStatus the emailStatus to set
     */
    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }

    /**
     * @return the otpStatus
     */
    public String getOtpStatus() {
        return otpStatus;
    }

    /**
     * @param otpStatus the otpStatus to set
     */
    public void setOtpStatus(String otpStatus) {
        this.otpStatus = otpStatus;
    }

    /**
     * @return the appsStatus
     */
    public String getAppsStatus() {
        return appsStatus;
    }

    /**
     * @param appsStatus the appsStatus to set
     */
    public void setAppsStatus(String appsStatus) {
        this.appsStatus = appsStatus;
    }

    /**
     * @return the panStatus
     */
    public String getPanStatus() {
        return panStatus;
    }

    /**
     * @param panStatus the panStatus to set
     */
    public void setPanStatus(String panStatus) {
        this.panStatus = panStatus;
    }

    /**
     * @return the aadharStatus
     */
    public String getAadharStatus() {
        return aadharStatus;
    }

    /**
     * @param aadharStatus the aadharStatus to set
     */
    public void setAadharStatus(String aadharStatus) {
        this.aadharStatus = aadharStatus;
    }

    /**
     * @return the mbStatus
     */
    public String getMbStatus() {
        return mbStatus;
    }

    /**
     * @param mbStatus the mbStatus to set
     */
    public void setMbStatus(String mbStatus) {
        this.mbStatus = mbStatus;
    }

    /**
     * @return the varScoreStatus
     */
    public String getVarScoreStatus() {
        return varScoreStatus;
    }

    /**
     * @param varScoreStatus the varScoreStatus to set
     */
    public void setVarScoreStatus(String varScoreStatus) {
        this.varScoreStatus = varScoreStatus;
    }

    /**
     * @return the scoreStatus
     */
    public String getScoreStatus() {
        return scoreStatus;
    }

    /**
     * @param scoreStatus the scoreStatus to set
     */
    public void setScoreStatus(String scoreStatus) {
        this.scoreStatus = scoreStatus;
    }

    /**
     * @return the cibilScore
     */
    public String getCibilScore() {
        return cibilScore;
    }

    /**
     * @param cibilScore the cibilScore to set
     */
    public void setCibilScore(String cibilScore) {
        this.cibilScore = cibilScore;
    }

    public String getExperianStatus() {
        return experianStatus;
    }

    public void setExperianStatus(String experianStatus) {
        this.experianStatus = experianStatus;
    }

    /**
     * @return the awsS3Status
     */
    public String getAwsS3Status() {
        return awsS3Status;
    }

    /**
     * @param awsS3Status the awsS3Status to set
     */
    public void setAwsS3Status(String awsS3Status) {
        this.awsS3Status = awsS3Status;
    }

    /**
     * @return the croStatus
     */
    public String getCroStatus() {
        return croStatus;
    }

    /**
     * @param croStatus the croStatus to set
     */
    public void setCroStatus(String croStatus) {
        this.croStatus = croStatus;
    }

    /**
     * @return the panModuleResult
     */
    public ModuleOutcome getPanModuleResult() {
        return panModuleResult;
    }

    /**
     * @param panModuleResult the panModuleResult to set
     */
    public void setPanModuleResult(ModuleOutcome panModuleResult) {
        this.panModuleResult = panModuleResult;
    }

    /**
     * @return the cibilModuleResult
     */
    public ModuleOutcome getCibilModuleResult() {
        return cibilModuleResult;
    }

    /**
     * @param cibilModuleResult the cibilModuleResult to set
     */
    public void setCibilModuleResult(ModuleOutcome cibilModuleResult) {
        this.cibilModuleResult = cibilModuleResult;
    }

    /**
     * @return the residenceAddressResult
     */
    public ModuleOutcome getResidenceAddressResult() {
        return residenceAddressResult;
    }

    /**
     * @param residenceAddressResult the residenceAddressResult to set
     */
    public void setResidenceAddressResult(
            ModuleOutcome residenceAddressResult) {
        this.residenceAddressResult = residenceAddressResult;
    }

    /**
     * @return the officeModuleResult
     */
    public ModuleOutcome getOfficeModuleResult() {
        return officeModuleResult;
    }

    /**
     * @param officeModuleResult the officeModuleResult to set
     */
    public void setOfficeModuleResult(ModuleOutcome officeModuleResult) {
        this.officeModuleResult = officeModuleResult;
    }

    /**
     * @return the scoringModuleResult
     */
    public ModuleOutcome getScoringModuleResult() {
        return scoringModuleResult;
    }

    /**
     * @param scoringModuleResult the scoringModuleResult to set
     */
    public void setScoringModuleResult(ModuleOutcome scoringModuleResult) {
        this.scoringModuleResult = scoringModuleResult;
    }

    /**
     * @return the aadharModuleResult
     */
    public ModuleOutcome getAadharModuleResult() {
        return aadharModuleResult;
    }

    /**
     * @param aadharModuleResult the aadharModuleResult to set
     */
    public void setAadharModuleResult(ModuleOutcome aadharModuleResult) {
        this.aadharModuleResult = aadharModuleResult;
    }

    /**
     * @return the experianModuleResult
     */
    public ModuleOutcome getExperianModuleResult() {
        return experianModuleResult;
    }

    /**
     * @param experianModuleResult the experianModuleResult to set
     */
    public void setExperianModuleResult(
            ModuleOutcome experianModuleResult) {
        this.experianModuleResult = experianModuleResult;
    }

    /**
     * @return the equifaxModuleResult
     */
    public ModuleOutcome getEquifaxModuleResult() {
        return equifaxModuleResult;
    }

    /**
     * @param equifaxModuleResult the equifaxModuleResult to set
     */
    public void setEquifaxModuleResult(ModuleOutcome equifaxModuleResult) {
        this.equifaxModuleResult = equifaxModuleResult;
    }

    /**
     * @return the chmModuleResult
     */
    public ModuleOutcome getChmModuleResult() {
        return chmModuleResult;
    }

    /**
     * @param chmModuleResult the chmModuleResult to set
     */
    public void setChmModuleResult(ModuleOutcome chmModuleResult) {
        this.chmModuleResult = chmModuleResult;
    }

        /**
     * @return the mbModuleResult
     */
    public ModuleOutcome getMbModuleResult() {
        return mbModuleResult;
    }

    /**
     * @param mbModuleResult the mbModuleResult to set
     */
    public void setMbModuleResult(ModuleOutcome mbModuleResult) {
        this.mbModuleResult = mbModuleResult;
    }

    /**
     * @return the additionalProperties
     */
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    /**
     * @param additionalProperties the additionalProperties to set
     */
    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getNtcStatus() {
        return ntcStatus;
    }

    public void setNtcStatus(String ntcStatus) {
        this.ntcStatus = ntcStatus;
    }

    /**
     * @return the ntcGngStatus
     */
    public NTCGnGStatus getNtcGngStatus() {
        return ntcGngStatus;
    }

    /**
     * @param ntcGngStatus the ntcGngStatus to set
     */
    public void setNtcGngStatus(NTCGnGStatus ntcGngStatus) {
        this.ntcGngStatus = ntcGngStatus;
    }

    public String getPosidexDedupeStatus() {
        return posidexDedupeStatus;
    }

    public void setPosidexDedupeStatus(String posidexDedupeStatus) {
        this.posidexDedupeStatus = posidexDedupeStatus;
    }

    public String getCreditVidyaStatus() {
        return creditVidyaStatus;
    }

    public void setCreditVidyaStatus(String creditVidyaStatus) {
        this.creditVidyaStatus = creditVidyaStatus;
    }

    public String getSaathiPullStatus() {
        return saathiPullStatus;
    }

    public void setSaathiPullStatus(String saathiPullStatus) {
        this.saathiPullStatus = saathiPullStatus;
    }

    public void setCreditVidyaModuleResult(ModuleOutcome creditVidyaModuleResult) {
        this.creditVidyaModuleResult = creditVidyaModuleResult;
    }

    public ModuleOutcome getCreditVidyaModuleResult() {
        return this.creditVidyaModuleResult;
    }

    public void setSaathiModuleResult(ModuleOutcome saathiModuleResult) {
        this.saathiModuleResult = saathiModuleResult;
    }

    public ModuleOutcome getSaathiModuleResult() {
        return this.saathiModuleResult;
    }

    public ModuleOutcome getPosidexModuleResult() {
        return posidexModuleResult;
    }

    public void setPosidexModuleResult(ModuleOutcome posidexModuleResult) {
        this.posidexModuleResult = posidexModuleResult;
    }

    public ModuleOutcome getInternalDedupeResult() {
        return internalDedupeResult;
    }

    public void setInternalDedupeResult(ModuleOutcome internalDedupeResult) {
        this.internalDedupeResult = internalDedupeResult;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InterimStatus{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", appId='").append(appId).append('\'');
        sb.append(", instId='").append(instId).append('\'');
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", appStart='").append(appStart).append('\'');
        sb.append(", dedupe='").append(dedupe).append('\'');
        sb.append(", posidexDedupeStatus='").append(posidexDedupeStatus).append('\'');
        sb.append(", emailStatus='").append(emailStatus).append('\'');
        sb.append(", otpStatus='").append(otpStatus).append('\'');
        sb.append(", appsStatus='").append(appsStatus).append('\'');
        sb.append(", panStatus='").append(panStatus).append('\'');
        sb.append(", aadharStatus='").append(aadharStatus).append('\'');
        sb.append(", mbStatus='").append(mbStatus).append('\'');
        sb.append(", creditVidyaStatus='").append(creditVidyaStatus).append('\'');
        sb.append(", saathiPullStatus='").append(saathiPullStatus).append('\'');
        sb.append(", varScoreStatus='").append(varScoreStatus).append('\'');
        sb.append(", scoreStatus='").append(scoreStatus).append('\'');
        sb.append(", cibilScore='").append(cibilScore).append('\'');
        sb.append(", experianStatus='").append(experianStatus).append('\'');
        sb.append(", croStatus='").append(croStatus).append('\'');
        sb.append(", awsS3Status='").append(awsS3Status).append('\'');
        sb.append(", ntcStatus='").append(ntcStatus).append('\'');
        sb.append(", ntcGngStatus=").append(ntcGngStatus);
        sb.append(", panModuleResult=").append(panModuleResult);
        sb.append(", cibilModuleResult=").append(cibilModuleResult);
        sb.append(", residenceAddressResult=").append(residenceAddressResult);
        sb.append(", officeModuleResult=").append(officeModuleResult);
        sb.append(", scoringModuleResult=").append(scoringModuleResult);
        sb.append(", aadharModuleResult=").append(aadharModuleResult);
        sb.append(", experianModuleResult=").append(experianModuleResult);
        sb.append(", equifaxModuleResult=").append(equifaxModuleResult);
        sb.append(", chmModuleResult=").append(chmModuleResult);
        sb.append(", mbModuleResult=").append(mbModuleResult);
        sb.append(", creditVidyaModuleResult=").append(creditVidyaModuleResult);
        sb.append(", saathiModuleResult=").append(saathiModuleResult);
        sb.append(", posidexModuleResult=").append(posidexModuleResult);
        sb.append(", internalDedupeResult=").append(internalDedupeResult);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = refId != null ? refId.hashCode() : 0;
        result = 31 * result + (appId != null ? appId.hashCode() : 0);
        result = 31 * result + (instId != null ? instId.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (appStart != null ? appStart.hashCode() : 0);
        result = 31 * result + (dedupe != null ? dedupe.hashCode() : 0);
        result = 31 * result + (posidexDedupeStatus != null ? posidexDedupeStatus.hashCode() : 0);
        result = 31 * result + (emailStatus != null ? emailStatus.hashCode() : 0);
        result = 31 * result + (otpStatus != null ? otpStatus.hashCode() : 0);
        result = 31 * result + (appsStatus != null ? appsStatus.hashCode() : 0);
        result = 31 * result + (panStatus != null ? panStatus.hashCode() : 0);
        result = 31 * result + (aadharStatus != null ? aadharStatus.hashCode() : 0);
        result = 31 * result + (mbStatus != null ? mbStatus.hashCode() : 0);
        result = 31 * result + (creditVidyaStatus != null ? creditVidyaStatus.hashCode() : 0);
        result = 31 * result + (saathiPullStatus != null ? saathiPullStatus.hashCode() : 0);
        result = 31 * result + (varScoreStatus != null ? varScoreStatus.hashCode() : 0);
        result = 31 * result + (scoreStatus != null ? scoreStatus.hashCode() : 0);
        result = 31 * result + (cibilScore != null ? cibilScore.hashCode() : 0);
        result = 31 * result + (experianStatus != null ? experianStatus.hashCode() : 0);
        result = 31 * result + (croStatus != null ? croStatus.hashCode() : 0);
        result = 31 * result + (awsS3Status != null ? awsS3Status.hashCode() : 0);
        result = 31 * result + (ntcStatus != null ? ntcStatus.hashCode() : 0);
        result = 31 * result + (ntcGngStatus != null ? ntcGngStatus.hashCode() : 0);
        result = 31 * result + (panModuleResult != null ? panModuleResult.hashCode() : 0);
        result = 31 * result + (cibilModuleResult != null ? cibilModuleResult.hashCode() : 0);
        result = 31 * result + (residenceAddressResult != null ? residenceAddressResult.hashCode() : 0);
        result = 31 * result + (officeModuleResult != null ? officeModuleResult.hashCode() : 0);
        result = 31 * result + (scoringModuleResult != null ? scoringModuleResult.hashCode() : 0);
        result = 31 * result + (aadharModuleResult != null ? aadharModuleResult.hashCode() : 0);
        result = 31 * result + (experianModuleResult != null ? experianModuleResult.hashCode() : 0);
        result = 31 * result + (equifaxModuleResult != null ? equifaxModuleResult.hashCode() : 0);
        result = 31 * result + (chmModuleResult != null ? chmModuleResult.hashCode() : 0);
        result = 31 * result + (mbModuleResult != null ? mbModuleResult.hashCode() : 0);
        result = 31 * result + (creditVidyaModuleResult != null ? creditVidyaModuleResult.hashCode() : 0);
        result = 31 * result + (saathiModuleResult != null ? saathiModuleResult.hashCode() : 0);
        result = 31 * result + (posidexModuleResult != null ? posidexModuleResult.hashCode() : 0);
        result = 31 * result + (internalDedupeResult != null ? internalDedupeResult.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InterimStatus other = (InterimStatus) obj;
        if (aadharModuleResult == null) {
            if (other.aadharModuleResult != null)
                return false;
        } else if (!aadharModuleResult.equals(other.aadharModuleResult))
            return false;
        if (aadharStatus == null) {
            if (other.aadharStatus != null)
                return false;
        } else if (!aadharStatus.equals(other.aadharStatus))
            return false;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (appId == null) {
            if (other.appId != null)
                return false;
        } else if (!appId.equals(other.appId))
            return false;
        if (appStart == null) {
            if (other.appStart != null)
                return false;
        } else if (!appStart.equals(other.appStart))
            return false;
        if (appsStatus == null) {
            if (other.appsStatus != null)
                return false;
        } else if (!appsStatus.equals(other.appsStatus))
            return false;
        if (awsS3Status == null) {
            if (other.awsS3Status != null)
                return false;
        } else if (!awsS3Status.equals(other.awsS3Status))
            return false;
        if (chmModuleResult == null) {
            if (other.chmModuleResult != null)
                return false;
        } else if (!chmModuleResult.equals(other.chmModuleResult))
        if (saathiModuleResult != null ? !saathiModuleResult.equals(other.saathiModuleResult) : other.saathiModuleResult != null)
            return false;
        if (posidexModuleResult != null ? !posidexModuleResult.equals(other.posidexModuleResult) : other.posidexModuleResult != null)
            return false;
        if (cibilModuleResult == null) {
            if (other.cibilModuleResult != null)
                return false;
        } else if (!cibilModuleResult.equals(other.cibilModuleResult))
            return false;
        if (cibilScore == null) {
            if (other.cibilScore != null)
                return false;
        } else if (!cibilScore.equals(other.cibilScore))
            return false;
        if (croStatus == null) {
            if (other.croStatus != null)
                return false;
        } else if (!croStatus.equals(other.croStatus))
            return false;
        if (dedupe == null) {
            if (other.dedupe != null)
                return false;
        } else if (!dedupe.equals(other.dedupe))
            return false;
        if (emailStatus == null) {
            if (other.emailStatus != null)
                return false;
        } else if (!emailStatus.equals(other.emailStatus))
            return false;
        if (endTime == null) {
            if (other.endTime != null)
                return false;
        } else if (!endTime.equals(other.endTime))
            return false;
        if (equifaxModuleResult == null) {
            if (other.equifaxModuleResult != null)
                return false;
        } else if (!equifaxModuleResult.equals(other.equifaxModuleResult))
            return false;
        if (experianModuleResult == null) {
            if (other.experianModuleResult != null)
                return false;
        } else if (!experianModuleResult.equals(other.experianModuleResult))
            return false;
        if (instId == null) {
            if (other.instId != null)
                return false;
        } else if (!instId.equals(other.instId))
            return false;
        if (mbModuleResult == null) {
            if (other.mbModuleResult != null)
                return false;
        } else if (!mbModuleResult.equals(other.mbModuleResult))
            return false;
        if (mbStatus == null) {
            if (other.mbStatus != null)
                return false;
        } else if (!mbStatus.equals(other.mbStatus))
            return false;
        if (ntcStatus == null) {
            if (other.ntcStatus != null)
                return false;
        } else if (!ntcStatus.equals(other.ntcStatus))
            return false;
        if (officeModuleResult == null) {
            if (other.officeModuleResult != null)
                return false;
        } else if (!officeModuleResult.equals(other.officeModuleResult))
            return false;
        if (otpStatus == null) {
            if (other.otpStatus != null)
                return false;
        } else if (!otpStatus.equals(other.otpStatus))
            return false;
        if (panModuleResult == null) {
            if (other.panModuleResult != null)
                return false;
        } else if (!panModuleResult.equals(other.panModuleResult))
            return false;
        if (panStatus == null) {
            if (other.panStatus != null)
                return false;
        } else if (!panStatus.equals(other.panStatus))
            return false;
        if (refId == null) {
            if (other.refId != null)
                return false;
        } else if (!refId.equals(other.refId))
            return false;
        if (residenceAddressResult == null) {
            if (other.residenceAddressResult != null)
                return false;
        } else if (!residenceAddressResult.equals(other.residenceAddressResult))
            return false;
        if (scoreStatus == null) {
            if (other.scoreStatus != null)
                return false;
        } else if (!scoreStatus.equals(other.scoreStatus))
            return false;
        if (scoringModuleResult == null) {
            if (other.scoringModuleResult != null)
                return false;
        } else if (!scoringModuleResult.equals(other.scoringModuleResult))
            return false;
        if (startTime == null) {
            if (other.startTime != null)
                return false;
        } else if (!startTime.equals(other.startTime))
            return false;
        if (varScoreStatus == null) {
            if (other.varScoreStatus != null)
                return false;
        } else if (!varScoreStatus.equals(other.varScoreStatus))
            return false;
        if (creditVidyaStatus == null) {
            if (other.creditVidyaStatus != null)
                return false;
        } else if (!creditVidyaStatus.equals(other.creditVidyaStatus))
            return false;
        if (saathiPullStatus == null) {
            if (other.saathiPullStatus != null)
                return false;
        } else if (!saathiPullStatus.equals(other.saathiPullStatus))
            return false;


        return true;
    }

    public String getHighmarkStatus() {
        return highmarkStatus;
    }

    public void setHighmarkStatus(String highmarkStatus) {
        this.highmarkStatus = highmarkStatus;
    }
}
