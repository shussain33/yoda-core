package com.softcell.gonogo.model.response.dooperation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahesh on 30/8/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DeliveryOrderOperationResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sDeliveryOrderStatus")
    private String deliveryOrderStatus;

    @JsonProperty("sResponseMsg")
    private String responseMsg;

}
