package com.softcell.gonogo.model.response.AmbitMifinResponse.SavePersonalInsurance;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceDetails {
    @JsonProperty("PERSONAL_INS_ID")
    private String personalInsId;

    @JsonProperty("PROSPECTCODE")
    private String prospectCode;

    @JsonProperty("PROSPECTID")
    private String prospectId;

    @JsonProperty("APPLICATIONCODE")
    private String applicantCode;
}
