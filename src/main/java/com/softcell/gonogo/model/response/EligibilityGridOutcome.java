/**
 * yogeshb6:39:02 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author yogeshb
 *
 */
public class EligibilityGridOutcome {
    @JsonProperty("dElgAmt")
    private double amount;

    @JsonProperty("sDec")
    private String decision;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    @Override
    public String toString() {
        return "EligibilityGridOutCome [amount=" + amount + ", decision="
                + decision + "]";
    }


}
