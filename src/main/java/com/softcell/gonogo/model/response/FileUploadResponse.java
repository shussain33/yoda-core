package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.response.core.ErrorMessage;

public class FileUploadResponse {
    @JsonProperty("oHeader")
    private FileHeader header;

    @JsonProperty("sRefID")
    private String gonogoRefId;

    @JsonProperty("sFileID")
    private String fileId;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("oError")
    private ErrorMessage errors;

    @JsonProperty("sUrl")
    private String url;

    public FileHeader getHeader() {
        return header;
    }

    public void setHeader(FileHeader header) {
        this.header = header;
    }

    public String getGonogoRefId() {
        return gonogoRefId;
    }

    public void setGonogoRefId(String gonogoRefId) {
        this.gonogoRefId = gonogoRefId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ErrorMessage getErrors() {
        return errors;
    }

    public void setErrors(ErrorMessage errors) {
        this.errors = errors;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
