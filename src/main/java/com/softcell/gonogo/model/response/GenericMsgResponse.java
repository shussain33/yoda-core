package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by anupamad on 22/8/17.
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenericMsgResponse {
    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sStatus")
    private String status;
}
