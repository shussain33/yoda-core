package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.response.core.AckHeader;
import com.softcell.gonogo.model.response.core.ErrorMessage;
import com.softcell.gonogo.model.response.core.Warnings;

public class Acknowledgement {

    @JsonProperty("oHeader")
    private AckHeader header;

    @JsonProperty("sRefID")
    private String gonogoRefId;

    @JsonProperty("sStat")
    private String status;

    @JsonProperty("oWrngs")
    private Warnings warnings;

    @JsonProperty("oError")
    private ErrorMessage errors;

    public AckHeader getHeader() {
        return header;
    }

    public void setHeader(AckHeader header) {
        this.header = header;
    }

    public String getGonogoRefId() {
        return gonogoRefId;
    }

    public void setGonogoRefId(String gonogoRefId) {
        this.gonogoRefId = gonogoRefId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Warnings getWarnings() {
        return warnings;
    }

    public void setWarnings(Warnings warnings) {
        this.warnings = warnings;
    }

    public ErrorMessage getErrors() {
        return errors;
    }

    public void setErrors(ErrorMessage errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "Acknowledgement [header=" + header + ", gonogoRefId="
                + gonogoRefId + ", status=" + status + ", warnings=" + warnings
                + ", errors=" + errors + "]";
    }


}
