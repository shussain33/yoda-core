package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

/**
 * @author kishor Generic response is used to send Request status of any
 *         asynchronous request.
 */
public class GenericResponse {

    @JsonProperty("status")
    private String status;

    public static Builder getbuilder() {
        return new Builder();
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GenericResponse)) return false;
        GenericResponse that = (GenericResponse) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("status", status)
                .toString();
    }

    public static class Builder {
        GenericResponse genericResponse = new GenericResponse();

        public GenericResponse build() {
            return this.genericResponse;
        }

        public Builder status(String status) {
            this.genericResponse.status = status;
            return this;
        }

    }

}
