package com.softcell.gonogo.model.response.dmz;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.ActionName;
import com.softcell.gonogo.model.request.DmzRequest;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by bhuvneshk on 17/2/17.
 */
public class DmzServiceCheckRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotEmpty(groups = {DmzServiceCheckRequest.FetchGrp.class})
    private String gonogoRefId;

    @JsonProperty("sActionName")
    @NotNull(groups = {DmzServiceCheckRequest.FetchGrp.class})
    private ActionName actionName;

    public Header getHeader() {
        return header;
    }
    public void setHeader(Header header) {
        this.header = header;
    }
    public String getGonogoRefId() {
        return gonogoRefId;
    }
    public void setGonogoRefId(String gonogoRefId) {
        this.gonogoRefId = gonogoRefId;
    }

    public ActionName getActionName() {
        return actionName;
    }
    public void setActionName(ActionName actionName) {
        this.actionName = actionName;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("DmzServiceCheckRequest [header=");
        builder.append(header);
        builder.append(", gonogoRefId=");
        builder.append(gonogoRefId);
        builder.append(", actionName=");
        builder.append(actionName);
        builder.append("]");
        return builder.toString();
    }


    public interface FetchGrp {
    }
}
