package com.softcell.gonogo.model.response.AmbitMifinResponse.SearchExistingApplicant;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExistingApplicantAddress {

    @JsonProperty("STATE")
    private String state;

    @JsonProperty("CREATEDDATETIME")
    private String createdDateTime;

    @JsonProperty("MAILINGADDRESS")
    private String mailingAddress;

    @JsonProperty("ACCOMMODATIONTYPE")
    private String accomodationType;

    @JsonProperty("COMPANY_NAME")
    private String companyName;

    @JsonProperty("FLOORNO")
    private String floorNo;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("OWNERSHIP")
    private String ownership;

    @JsonProperty("LOCALITY")
    private String locality;

    @JsonProperty("PHONE1")
    private String phone1;

    @JsonProperty("BUILDINGNAME")
    private String buildingName;

    @JsonProperty("CREATEDBY")
    private String createdBy;

    @JsonProperty("ADDRESSID")
    private String addressId;

    @JsonProperty("GSTIN_NO")
    private String gstinNo;

    @JsonProperty("STATENAME")
    private String stateName;

    @JsonProperty("PINCODE")
    private String pincode;

    @JsonProperty("EMAIL")
    private String email;

    @JsonProperty("CITYNAME")
    private String cityName;

    @JsonProperty("FAX")
    private String fax;

    @JsonProperty("NOOFMONTHS")
    private String noOfMonths;

    @JsonProperty("LANDMARK")
    private String landmark;

    @JsonProperty("GST_APPLICABLE")
    private String gstApplicable;

    @JsonProperty("APPLICANTADDRESSNAME")
    private String applicantAddName;

    @JsonProperty("APPLICANTTYPE")
    private String applicantType;

    @JsonProperty("NOOFYEARS")
    private String noOfYears;

    @JsonProperty("ZIPCODE")
    private String zipCode;

    @JsonProperty("STD_ISD")
    private String stdIsd;

    @JsonProperty("ADDRESSTYPE")
    private String addressType;

    @JsonProperty("MOBILE")
    private String mobile;

    @JsonProperty("APPLICANTID")
    private String applicantId;

    @JsonProperty("FLATNO")
    private String flatNo;

    @JsonProperty("CITY")
    private String city;

    @JsonProperty("EXT1")
    private String ext1;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
