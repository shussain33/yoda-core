package com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerMatchInfo {
    @JsonProperty("APPLICANT_MATCHED_DETAILS")
    private List<ApplicantMatchedInfo> applicantMatchedInfoList;

    @JsonProperty("APPLICANT_MATCHED_ADDRESS")
    private List<ApplicantMatchedAddress> applicantMatchedAddressesList;

    @JsonProperty("APPLICANT_MATCHED_BANKACCOUNT")
    private List<ApplicantMatchedBankAccount> applicantMatchedBankAccountList;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }
}
