package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.DedupeMatchDetail;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.mifin.crm.CrmResponse;
import com.softcell.gonogo.model.mifin.topup.TopUpDedupeResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0302 on 18/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginDataResponse {

    @JsonProperty("aDedupeMatchDetails")
    private List<DedupeMatchDetail> dedupeMatchDetails;///In Case Dedup is found in internal system then this field will be used for response

    @JsonProperty("oMifinDedupeResponse")
    TopUpDedupeResponse mifinDedupeResponse; /// In case Dedupe is found in Mififn then this field will be used for response

    @JsonProperty("oGoNoGoCustomerApplication")
    GoNoGoCustomerApplication goNoGoCustomerApplication; /// When GoNoGoCustomer saved successfully, this field be used for response

    @JsonProperty("oEligibility")
    Eligibility eligibility; /// When eligibility of GoNoGoCustomer saved successfully, this field be used for response

    @JsonProperty("oCrmResponse")
    CrmResponse crmResponse;
}
