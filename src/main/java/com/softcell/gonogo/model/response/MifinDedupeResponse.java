package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.MiFinCallLog;
import com.softcell.gonogo.model.mifin.topup.Applicant;
import com.softcell.gonogo.model.mifin.topup.DedupeInfo;
import com.softcell.gonogo.model.request.ApplicationRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class MifinDedupeResponse {

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("oApplicationRequest")
    private ApplicationRequest applicationRequest;

    @JsonProperty("APPLICANTLIST")
    private List<Applicant> applicantList ;

}
