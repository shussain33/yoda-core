package com.softcell.gonogo.model.response.multiproduct;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MultiProductResponse {
    @JsonProperty("sNewRefID")
    private String refID;

    @JsonProperty("dUnUtilizedAmt")
    private double unUtilizedAmt;

    @JsonProperty("iProductNumber")
    private int productNumber;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sError")
    private MultiProductError error;

    public double getUnUtilizedAmt() {
        return unUtilizedAmt;
    }

    public void setUnUtilizedAmt(double unUtilizedAmt) {
        this.unUtilizedAmt = unUtilizedAmt;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public MultiProductError getError() {
        return error;
    }

    public void setError(MultiProductError error) {
        this.error = error;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(int productNumber) {
        this.productNumber = productNumber;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("MultiProductResponse [refID=");
        builder.append(refID);
        builder.append(", unUtilizedAmt=");
        builder.append(unUtilizedAmt);
        builder.append(", productNumber=");
        builder.append(productNumber);
        builder.append(", status=");
        builder.append(status);
        builder.append(", error=");
        builder.append(error);
        builder.append("]");
        return builder.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((error == null) ? 0 : error.hashCode());
        result = prime * result + productNumber;
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        long temp;
        temp = Double.doubleToLongBits(unUtilizedAmt);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MultiProductResponse other = (MultiProductResponse) obj;
        if (error == null) {
            if (other.error != null)
                return false;
        } else if (!error.equals(other.error))
            return false;
        if (productNumber != other.productNumber)
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (Double.doubleToLongBits(unUtilizedAmt) != Double
                .doubleToLongBits(other.unUtilizedAmt))
            return false;
        return true;
    }
}
