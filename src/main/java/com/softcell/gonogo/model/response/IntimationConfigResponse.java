package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.configuration.admin.InstitutionConfig;
import com.softcell.gonogo.model.request.TemplateDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by amit on 3/9/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IntimationConfigResponse {
    @JsonProperty("oInstConfig")
    InstitutionConfig institutionConfig;

    @JsonProperty("aActions")
    List<String> actions;

    @JsonProperty("aTemplates")
    List<TemplateDetails> templateDetails;
}
