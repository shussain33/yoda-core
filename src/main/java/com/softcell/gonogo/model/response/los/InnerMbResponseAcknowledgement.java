package com.softcell.gonogo.model.response.los;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by yogeshb on 8/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InnerMbResponseAcknowledgement {

    @JsonProperty("acknowledgementidspecified")
    private boolean acknowledgementIdspecified;

    @JsonProperty("acknowledgementid")
    private String acknowledgementId;

    @JsonProperty("statusspecified")
    private boolean statusSpecified;

    @JsonProperty("status")
    private String status;

    @JsonProperty("soaFillersSpecified")
    private boolean soaFillersSpecified;

    @JsonProperty("soaFillers")
    private Object soaFillers;

    public boolean isAcknowledgementIdspecified() {
        return acknowledgementIdspecified;
    }

    public void setAcknowledgementIdspecified(boolean acknowledgementIdspecified) {
        this.acknowledgementIdspecified = acknowledgementIdspecified;
    }

    public String getAcknowledgementId() {
        return acknowledgementId;
    }

    public void setAcknowledgementId(String acknowledgementId) {
        this.acknowledgementId = acknowledgementId;
    }

    public boolean isStatusSpecified() {
        return statusSpecified;
    }

    public void setStatusSpecified(boolean statusSpecified) {
        this.statusSpecified = statusSpecified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSoaFillersSpecified() {
        return soaFillersSpecified;
    }

    public void setSoaFillersSpecified(boolean soaFillersSpecified) {
        this.soaFillersSpecified = soaFillersSpecified;
    }

    public Object getSoaFillers() {
        return soaFillers;
    }

    public void setSoaFillers(Object soaFillers) {
        this.soaFillers = soaFillers;
    }
}
