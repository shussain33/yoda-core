package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.response.smsservice.Error;
import com.softcell.gonogo.model.response.smsservice.SmsStatus;

import java.util.Arrays;

/**
 * @author vinodk
 */
public class SmsResponse {

    @JsonProperty("aSmsStatus")
    private SmsStatus[] smsStatus;

    @JsonProperty("bSuccess")
    private boolean success;

    @JsonProperty("aErrors")
    private Error[] errors;

    @JsonProperty("sOtp")
    private String otp;

    @JsonProperty("sUuid")
    private String uuid;

    @JsonProperty("sIdentifier")
    private String identifier;

    /**
     * @return the smsStatus
     */
    public SmsStatus[] getSmsStatus() {
        return smsStatus;
    }

    /**
     * @param smsStatus the smsStatus to set
     */
    public void setSmsStatus(SmsStatus[] smsStatus) {
        this.smsStatus = smsStatus;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * @return the errors
     */
    public Error[] getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(Error[] errors) {
        this.errors = errors;
    }

    /**
     * @return the otp
     */
    public String getOtp() {
        return otp;
    }

    /**
     * @param otp the otp to set
     */
    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsResponse [smsStatus=");
        builder.append(Arrays.toString(smsStatus));
        builder.append(", success=");
        builder.append(success);
        builder.append(", errors=");
        builder.append(Arrays.toString(errors));
        builder.append(", otp=");
        builder.append(otp);
        builder.append("]");
        return builder.toString();
    }
}
