package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.error.Error;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoNoGoReportResponse {

    @JsonProperty("sFormat")
    private String format;

    @JsonProperty("sExcelName")
    private String excelName;

    @JsonProperty("aByte")
    private List<byte[]> fileContent;

    @JsonProperty("aError")
    private Error error;
}
