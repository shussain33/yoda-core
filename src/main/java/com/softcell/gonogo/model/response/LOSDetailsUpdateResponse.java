package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by yogeshb on 24/5/17.
 */
public class LOSDetailsUpdateResponse {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sLosID")
    private String losID;

    @JsonProperty("sAck")
    private String acknowledgement;

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getLosID() {
        return losID;
    }

    public void setLosID(String losID) {
        this.losID = losID;
    }

    public String getAcknowledgement() {
        return acknowledgement;
    }

    public void setAcknowledgement(String acknowledgement) {
        this.acknowledgement = acknowledgement;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LOSDetailsUpdateResponse{");
        sb.append("refID='").append(refID).append('\'');
        sb.append(", losID='").append(losID).append('\'');
        sb.append(", acknowledgement='").append(acknowledgement).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LOSDetailsUpdateResponse that = (LOSDetailsUpdateResponse) o;

        if (refID != null ? !refID.equals(that.refID) : that.refID != null) return false;
        if (losID != null ? !losID.equals(that.losID) : that.losID != null) return false;
        return acknowledgement != null ? acknowledgement.equals(that.acknowledgement) : that.acknowledgement == null;
    }

    @Override
    public int hashCode() {
        int result = refID != null ? refID.hashCode() : 0;
        result = 31 * result + (losID != null ? losID.hashCode() : 0);
        result = 31 * result + (acknowledgement != null ? acknowledgement.hashCode() : 0);
        return result;
    }
}
