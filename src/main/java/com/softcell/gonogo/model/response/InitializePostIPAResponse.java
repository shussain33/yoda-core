package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anupamad on 28/8/17.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InitializePostIPAResponse {
    @JsonProperty("oSchemeMasters")
    private List<SchemeMasterData> schemeMasters = new ArrayList<SchemeMasterData>();

    @JsonProperty("oAdditionalFields")
    private Map<String, Object> additionalFields = new HashMap<>();
}
