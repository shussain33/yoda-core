package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.error.Error;

import java.util.Set;

/**
 * @author vinodk
 */
public class ModelVariantMasterResponse {

    @JsonProperty("aManufacturers")
    private Set<String> manufacturers;

    @JsonProperty("aModels")
    private Set<String> models;

    @JsonProperty("aVariants")
    private Set<String> variants;

    @JsonProperty("oError")
    private Error error;

    /**
     * @return the manufacturers
     */
    public Set<String> getManufacturers() {
        return manufacturers;
    }

    /**
     * @param manufacturers the manufacturers to set
     */
    public void setManufacturers(Set<String> manufacturers) {
        this.manufacturers = manufacturers;
    }

    /**
     * @return the models
     */
    public Set<String> getModels() {
        return models;
    }

    /**
     * @param models the models to set
     */
    public void setModels(Set<String> models) {
        this.models = models;
    }

    /**
     * @return the variants
     */
    public Set<String> getVariants() {
        return variants;
    }

    /**
     * @param variants the variants to set
     */
    public void setVariants(Set<String> variants) {
        this.variants = variants;
    }

    /**
     * @return the error
     */
    public Error getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(Error error) {
        this.error = error;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ModelVariantMasterResponse [manufacturers=");
        builder.append(manufacturers);
        builder.append(", models=");
        builder.append(models);
        builder.append(", variants=");
        builder.append(variants);
        builder.append("]");
        return builder.toString();
    }
}
