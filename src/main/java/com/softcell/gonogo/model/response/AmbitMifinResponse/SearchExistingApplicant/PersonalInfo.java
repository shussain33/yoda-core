package com.softcell.gonogo.model.response.AmbitMifinResponse.SearchExistingApplicant;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonalInfo {

    @JsonProperty("NO_OF_OTHER_DEPENDENTS")
    private String noOfOtherDependents;

    @JsonProperty("FNAME")
    private String fname;

    @JsonProperty("CONSTITUTION")
    private String costitution;

    @JsonProperty("MARTIALSTATUS")
    private String maritalStatus;

    @JsonProperty("EDUQUALIFICATION")
    private String education;

    @JsonProperty("LNAME")
    private String lname;

    @JsonProperty("AADHAAR_AS_KYC")
    private String aadharaskyc;

    @JsonProperty("APPLICANT_AGE")
    private String aplicantAge;

    @JsonProperty("NOOFDEPENDENTS")
    private String noofDependents;

    @JsonProperty("SALES_MANAGER_ID")
    private String salesManagerId;

    @JsonProperty("INDUSTRYNAME")
    private String IndustryName;

    @JsonProperty("APPLICANTCODE")
    private String applicantCode;

    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("INDUSTRYTYPE")
    private String industryType;

    @JsonProperty("PANNO")
    private String panNo;

    @JsonProperty("KEY_CONTACT_PERSON")
    private String keyContactPerson;

    @JsonProperty("TINNO")
    private String tinNo;

    @JsonProperty("FATHERLNAME")
    private String fatherLname;

    @JsonProperty("CREATEDBY")
    private String createdBy;

    @JsonProperty("CLUSTER_DESCRIPTION")
    private String clusterDesc;

    @JsonProperty("IS_PAN_AVAILABLE")
    private String isPanAvailable;

    @JsonProperty("CUSTOMER_CREDIT_INFO")
    private String custCreditInfo;

    @JsonProperty("TENOR")
    private String tenor;

    @JsonProperty("EXISTING_CUSTOMER")
    private String existingCustomer;

    @JsonProperty("NATUREOFBUSINESS")
    private String natureOfBusiness;

    @JsonProperty("CREATED_DATETIME")
    private String createdDatetime;

    @JsonProperty("HIGHESTQUALIFICATION")
    private String HighestQualification;

    @JsonProperty("AGE_MONTHS")
    private String ageMonths;

    @JsonProperty("LEGAL_FORM_TYPE")
    private String legalFornType;

    @JsonProperty("COMPANY_ID")
    private String custId;

    @JsonProperty("FATHERFNAME")
    private String fatherFname;

    @JsonProperty("AUTHORIZED_CAPITAL")
    private String authorizedCapital;

    @JsonProperty("TYPE_OF_BUSINESS_DESC")
    private String typeOfBusinessDesc;

    @JsonProperty("NAME_AS_PER_AADHAAR")
    private String nameAsAadhar;

    @JsonProperty("APPLICANTTYPE")
    private String applicantype;

    @JsonProperty("CUSTOMER_ENTITYTYPE")
    private String custEntityType;

    @JsonProperty("NICK_NAME")
    private String nickName;

    @JsonProperty("CKYC_ID")
    private String ckycId;

    @JsonProperty("CLUSTER_NAME_DESC")
    private String customerNameDesc;

    @JsonProperty("APPLICANTTYPENAME")
    private String applicanttypeName;

    @JsonProperty("ISSUED_CAPITAL")
    private String issueCapital;

    @JsonProperty("GENDER")
    private String gender;

    @JsonProperty("IS_EXISTING_CUSTOMER")
    private String isExistingCust;

    @JsonProperty("APPLICANTID")
    private String applicantId;

    @JsonProperty("RBI_REGISTRATION_NO")
    private String rbiNo;

    @JsonProperty("CASTE_CATEGORY")
    private String casteCategory;

    @JsonProperty("PERSONAL_COMPLETED")
    private String personalCompleted;

    @JsonProperty("CLUSTER_VAL")
    private String clusterVal;

    @JsonProperty("TITLE")
    private String title;

    @JsonProperty("SALES_MANAGER")
    private String salesManager;

    @JsonProperty("DOB")
    private String dob;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }




}
