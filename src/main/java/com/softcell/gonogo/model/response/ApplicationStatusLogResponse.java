package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.CroJustification;

/**
 * Created by mahesh on 22/3/17.
 */
public class ApplicationStatusLogResponse {

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("oCroJustification")
    private CroJustification croJustification;

    @JsonProperty("oCroDecision")
    private CroDecision croDecision;

    public CroDecision getCroDecision() {
        return croDecision;
    }

    public void setCroDecision(CroDecision croDecision) {
        this.croDecision = croDecision;
    }

    public CroJustification getCroJustification() {
        return croJustification;
    }

    public void setCroJustification(CroJustification croJustification) {
        this.croJustification = croJustification;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ApplicationStatusLogResponse{");
        sb.append("croDecision=").append(croDecision);
        sb.append(", refId='").append(refId).append('\'');
        sb.append(", croJustification=").append(croJustification);
        sb.append('}');
        return sb.toString();
    }
}
