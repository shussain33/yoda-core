package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.kyc.response.ErrorDesc;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 22/10/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreditVidyaResponse {

    @JsonProperty("sOriginalResp")
    String originalResp;

    @JsonProperty("oError")
    ErrorDesc error;
}
