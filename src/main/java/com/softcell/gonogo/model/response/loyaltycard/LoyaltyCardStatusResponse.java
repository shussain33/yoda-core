package com.softcell.gonogo.model.response.loyaltycard;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 2/5/17.
 */
public class LoyaltyCardStatusResponse {

    @JsonProperty("sLoyaltyCardNo")
    private String loyaltyCardNo;

    @JsonProperty("sLoyaltyCardStatus")
    private String loyaltyCardStatus;

    @JsonProperty("sLoyaltyCardPrice")
    private double loyaltyCardPrice ;

    @JsonProperty("sStatus")
    private String status;

    public String getLoyaltyCardNo() {
        return loyaltyCardNo;
    }

    public void setLoyaltyCardNo(String loyaltyCardNo) {
        this.loyaltyCardNo = loyaltyCardNo;
    }

    public String getLoyaltyCardStatus() {
        return loyaltyCardStatus;
    }

    public void setLoyaltyCardStatus(String loyaltyCardStatus) {
        this.loyaltyCardStatus = loyaltyCardStatus;
    }

    public double getLoyaltyCardPrice() {
        return loyaltyCardPrice;
    }

    public void setLoyaltyCardPrice(double loyaltyCardPrice) {
        this.loyaltyCardPrice = loyaltyCardPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LoyaltyCardStatusResponse{");
        sb.append("loyaltyCardNo='").append(loyaltyCardNo).append('\'');
        sb.append(", loyaltyCardStatus='").append(loyaltyCardStatus).append('\'');
        sb.append(", loyaltyCardPrice=").append(loyaltyCardPrice);
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
