package com.softcell.gonogo.model.response.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

/**
 * Created by archana on 16/11/17.
 */
@Data
public class SchedulerInfoResponse {

    @JsonProperty("lIdleTimeInSec")
    private long idleTimeInSec;

    @JsonProperty("lOfflineTimeSec")
    private long offlineTimeSec;

    @JsonProperty("dtLastSchedulerRun")
    private Date lastSchedulerRun;

    @JsonProperty("lSchedulerFrequency")
    private long schedulerFrequency;

}
