package com.softcell.gonogo.model.response.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Warnings {
    @JsonProperty("sCode")
    private String code;

    @JsonProperty("sDescr")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Warnings [code=" + code + ", description=" + description + "]";
    }

}
