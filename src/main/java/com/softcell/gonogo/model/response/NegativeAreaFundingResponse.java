package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.masters.NegativeAreaGeoLimitMaster;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mahesh on 7/2/17.
 */
public class NegativeAreaFundingResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("aoAreaWithReason")
    private List<NegativeAreaWithReason> negativeAreaWithReason;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<NegativeAreaWithReason> getNegativeAreaWithReason() {
        return negativeAreaWithReason;
    }

    public void setNegativeAreaWithReason(List<NegativeAreaWithReason> negativeAreaWithReason) {
        this.negativeAreaWithReason = negativeAreaWithReason;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NegativeAreaFundingResponse{");
        sb.append("negativeAreaWithReason=").append(negativeAreaWithReason);
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }

    /**
     * This method is used to populate NegativeAreaFundingResponse using negativeAreaFundingMaster List.
     * @param negativeAreaGeoLimitMasterList
     * @return
     */
    public NegativeAreaFundingResponse populateResponse(List<NegativeAreaGeoLimitMaster> negativeAreaGeoLimitMasterList) {
        NegativeAreaFundingResponse negativeAreaFundingResponse = new NegativeAreaFundingResponse();

        List<NegativeAreaWithReason> negativeAreaWithReasonList = negativeAreaGeoLimitMasterList.parallelStream().map(negativeAreaFundingMaster -> {
            NegativeAreaWithReason negativeAreaWithReason = new NegativeAreaWithReason();
            negativeAreaWithReason.setReason(negativeAreaFundingMaster.getReason());
            negativeAreaWithReason.setAreaName(negativeAreaFundingMaster.getArea());

            return negativeAreaWithReason;
        }).collect(Collectors.toList());


        negativeAreaFundingResponse.setStatus(Status.SUCCESS.name());
        negativeAreaFundingResponse.setNegativeAreaWithReason(negativeAreaWithReasonList);

        return negativeAreaFundingResponse;
    }
}
