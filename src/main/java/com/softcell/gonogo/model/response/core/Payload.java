package com.softcell.gonogo.model.response.core;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.Assert;

/**
 * @param <T>
 * @author kishorp
 */
public class Payload<T> {

    @JsonProperty("payLoad")
    private T t;

    public Payload(T body) {
        Assert.notNull(body,"Payload must not be null");
        this.t = body;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ResponsePayload [t=");
        builder.append(t);
        builder.append(", getT()=");
        builder.append(getT());
        builder.append(", getClass()=");
        builder.append(getClass());
        builder.append(", hashCode()=");
        builder.append(hashCode());
        builder.append(", toString()=");
        builder.append(super.toString());
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((t == null) ? 0 : t.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Payload other = (Payload) obj;
        if (t == null) {
            if (other.t != null)
                return false;
        } else if (!t.equals(other.t))
            return false;
        return true;
    }
}
