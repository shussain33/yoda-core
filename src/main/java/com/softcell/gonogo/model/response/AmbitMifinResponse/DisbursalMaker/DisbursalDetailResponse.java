package com.softcell.gonogo.model.response.AmbitMifinResponse.DisbursalMaker;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssguser on 29/12/20.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DisbursalDetailResponse {

    @JsonProperty("PROSPECT_ID")
    private String prospectId;

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

    @JsonProperty("LOANCHARGEID")
    private String loanChargeId;

    @JsonProperty("DISBURSAL_ID")
    private String disbursalId;

    @JsonProperty("CHARGE_ID")
    private String chargeId;

    @JsonProperty("CHARGENAME")
    private String chargeName;

    @JsonProperty("CHARGE_AMOUNT")
    private String chargeAmt;

}
