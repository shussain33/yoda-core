package com.softcell.gonogo.model.response.master.schemedatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class SchemeInformationResponse {

    @JsonProperty("sSchID")
    private String schemeID;

    @JsonProperty("sSchDes")
    private String schemeDesc;

    @JsonProperty("sCid")
    private String ccid;

    public String getSchemeID() {
        return schemeID;
    }

    public void setSchemeID(String schemeID) {
        this.schemeID = schemeID;
    }

    public String getSchemeDesc() {
        return schemeDesc;
    }

    public void setSchemeDesc(String schemeDesc) {
        this.schemeDesc = schemeDesc;
    }

    public String getCcid() {
        return ccid;
    }

    public void setCcid(String ccid) {
        this.ccid = ccid;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SchemeIdResponse [schemeID=");
        builder.append(schemeID);
        builder.append(", schemeDesc=");
        builder.append(schemeDesc);
        builder.append(", ccid=");
        builder.append(ccid);
        builder.append("]");
        return builder.toString();
    }

}
