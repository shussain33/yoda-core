package com.softcell.gonogo.model.response;

import com.softcell.gonogo.model.posidex.DedupeEnquiryResponse;

import java.io.Serializable;

/**
 * Created by yogeshb on 23/8/17.
 */
public class PosidexResponse implements Serializable {

    private DedupeEnquiryResponse DedupeDoEnquiryResponse;

    private DedupeEnquiryResponse dedupeEnquiryStatusResponse;

    public DedupeEnquiryResponse getDedupeDoEnquiryResponse() {
        return DedupeDoEnquiryResponse;
    }

    public void setDedupeDoEnquiryResponse(DedupeEnquiryResponse dedupeDoEnquiryResponse) {
        DedupeDoEnquiryResponse = dedupeDoEnquiryResponse;
    }

    public DedupeEnquiryResponse getDedupeEnquiryStatusResponse() {
        return dedupeEnquiryStatusResponse;
    }

    public void setDedupeEnquiryStatusResponse(DedupeEnquiryResponse dedupeEnquiryStatusResponse) {
        this.dedupeEnquiryStatusResponse = dedupeEnquiryStatusResponse;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PosidexResponse{");
        sb.append("DedupeDoEnquiryResponse=").append(DedupeDoEnquiryResponse);
        sb.append(", dedupeEnquiryStatusResponse=").append(dedupeEnquiryStatusResponse);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PosidexResponse that = (PosidexResponse) o;

        if (DedupeDoEnquiryResponse != null ? !DedupeDoEnquiryResponse.equals(that.DedupeDoEnquiryResponse) : that.DedupeDoEnquiryResponse != null)
            return false;
        return dedupeEnquiryStatusResponse != null ? dedupeEnquiryStatusResponse.equals(that.dedupeEnquiryStatusResponse) : that.dedupeEnquiryStatusResponse == null;
    }

    @Override
    public int hashCode() {
        int result = DedupeDoEnquiryResponse != null ? DedupeDoEnquiryResponse.hashCode() : 0;
        result = 31 * result + (dedupeEnquiryStatusResponse != null ? dedupeEnquiryStatusResponse.hashCode() : 0);
        return result;
    }
}
