package com.softcell.gonogo.model.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 16/1/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MCPAggregatorResponse {

    @JsonProperty("sApplicantName")
    private String applicantName;

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sBucket")
    private String bucket;

    @JsonProperty("sCibilHitResult")
    private String cibilHitResult;

    @JsonProperty("sApplicationStatus")
    private String applicationStatus;

    @JsonProperty("sRemarks")
    private String remarks;
}
