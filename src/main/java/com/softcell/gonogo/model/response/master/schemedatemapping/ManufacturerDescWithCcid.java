package com.softcell.gonogo.model.response.master.schemedatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class ManufacturerDescWithCcid {

    @JsonProperty("sManfcDesc")
    private String manufactureDesc;

    @JsonProperty("sManfcId")
    private String manufactureId;

    public String getManufactureDesc() {
        return manufactureDesc;
    }

    public void setManufactureDesc(String manufactureDesc) {
        this.manufactureDesc = manufactureDesc;
    }

    public String getManufactureId() {
        return manufactureId;
    }

    public void setManufactureId(String manufactureId) {
        this.manufactureId = manufactureId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ManufacturerWithCcid [manufactureDesc=");
        builder.append(manufactureDesc);
        builder.append(", manufactureId=");
        builder.append(manufactureId);
        builder.append("]");
        return builder.toString();
    }

}
