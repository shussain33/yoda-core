package com.softcell.gonogo.model.response.extendedwarranty;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.EWPremiumMaster;

/**
 * Created by mahesh on 25/5/17.
 */
public class ExtendedWarrantyPremiumResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResMessage")
    private String resMessage;

    @JsonProperty("dMnfcrWarranty")
    private double manufacturerWarranty;

    @JsonProperty("sSrNoOrImeiNo")
    private String serialOrImeiNumber;

    @JsonProperty("oEwPremiumMaster")
    private EWPremiumMaster ewPremiumMaster;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResMessage() {
        return resMessage;
    }

    public void setResMessage(String resMessage) {
        this.resMessage = resMessage;
    }

    public EWPremiumMaster getEwPremiumMaster() {
        return ewPremiumMaster;
    }

    public void setEwPremiumMaster(EWPremiumMaster ewPremiumMaster) {
        this.ewPremiumMaster = ewPremiumMaster;
    }

    public double getManufacturerWarranty() {
        return manufacturerWarranty;
    }

    public void setManufacturerWarranty(double manufacturerWarranty) {
        this.manufacturerWarranty = manufacturerWarranty;
    }

    public String getSerialOrImeiNumber() {
        return serialOrImeiNumber;
    }

    public void setSerialOrImeiNumber(String serialOrImeiNumber) {
        this.serialOrImeiNumber = serialOrImeiNumber;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExtendedWarrantyPremiumResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", resMessage='").append(resMessage).append('\'');
        sb.append(", manufacturerWarranty=").append(manufacturerWarranty);
        sb.append(", serialOrImeiNumber='").append(serialOrImeiNumber).append('\'');
        sb.append(", ewPremiumMaster=").append(ewPremiumMaster);
        sb.append('}');
        return sb.toString();
    }
}
