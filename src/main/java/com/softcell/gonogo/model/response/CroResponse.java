package com.softcell.gonogo.model.response;

import java.util.List;

public class CroResponse {
    private List<CroData> croDataList;

    public List<CroData> getCroDataList() {
        return croDataList;
    }

    public void setCroDataList(List<CroData> croDataList) {
        this.croDataList = croDataList;
    }

    @Override
    public String toString() {
        return "CroResponse [croDataList=" + croDataList + "]";
    }
}
