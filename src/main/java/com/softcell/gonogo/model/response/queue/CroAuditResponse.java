package com.softcell.gonogo.model.response.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.queueMgmt.CroAudit;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * Created by archana on 19/9/17.
 */
public class CroAuditResponse {
    @JsonProperty("sUserId")
    @NotNull
    private String userId;

    @JsonProperty("sInstitutionId")
    @NotNull
    private String institutionId;

    private Map<String, List<CroAudit>> auditList;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, List<CroAudit>> getAuditList() {
        return auditList;
    }

    public void setAuditList(Map<String, List<CroAudit>> auditList) {
        this.auditList = auditList;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }
}
