package com.softcell.gonogo.model.response.AmbitMifinResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.mifin.MiFinBaseResponse;
import lombok.Data;

@Data
public class BodyAmbit {

    @JsonProperty("RECEIVE")
    public MiFinBaseResponse miFinBaseResponse;
}