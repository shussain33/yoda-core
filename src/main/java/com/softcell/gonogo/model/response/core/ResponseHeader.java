package com.softcell.gonogo.model.response.core;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ResponseHeader {

    @JsonProperty("sContentType")
    private String contentType;

    @JsonProperty("sApiName")
    private String apiName;

    @JsonProperty("sApiVersion")
    private String apiVersion;

    @JsonProperty("dtRequestTime")
    private Date apiRequestTime;

    @JsonProperty("dtResponseTime")
    private Date apiResponseTime;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getApiName() {
        return apiName;
    }

    public void setApiName(String apiName) {
        this.apiName = apiName;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public Date getApiRequestTime() {
        return apiRequestTime;
    }

    public void setApiRequestTime(Date apiRequestTime) {
        this.apiRequestTime = apiRequestTime;
    }

    public Date getApiResponseTime() {
        return apiResponseTime;
    }

    public void setApiResponseTime(Date apiResponseTime) {
        this.apiResponseTime = apiResponseTime;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ResponseHeader [contentType=");
        builder.append(contentType);
        builder.append(", apiName=");
        builder.append(apiName);
        builder.append(", apiVersion=");
        builder.append(apiVersion);
        builder.append(", apiRequestTime=");
        builder.append(apiRequestTime);
        builder.append(", apiResponseTime=");
        builder.append(apiResponseTime);
        builder.append("]");
        return builder.toString();
    }


}
