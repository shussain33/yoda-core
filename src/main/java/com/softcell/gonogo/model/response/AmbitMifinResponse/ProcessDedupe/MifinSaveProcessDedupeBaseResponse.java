package com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MifinSaveProcessDedupeBaseResponse {
    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("MESSAGE")
    private String mesage;

    @JsonProperty("ENTITYTYPE")
    private String entityType;

    @JsonProperty("CUSTOMER_PERSONAL_INFO")
    private List<CustomerPeralInfo> customerPersonalInfo;

    @JsonProperty("CUSTOMER_MATCHED_INFO")
    private List<CustomerMatchInfo> customermatchInfo;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
