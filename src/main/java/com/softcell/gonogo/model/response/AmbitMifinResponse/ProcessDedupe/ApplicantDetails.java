package com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicantDetails {
    @JsonProperty("PAN")
    private String pan;

    @JsonProperty("ACTUAL_CUSTID")
    private String actualCustId;

    @JsonProperty("SEARCHTIME")
    private String searchTime;

    @JsonProperty("FNAME")
    private String fname;

    @JsonProperty("EMAIL_USER")
    private String emailUser;

    @JsonProperty("BATCHID")
    private String batchId;

    @JsonProperty("DOB")
    private String dob;

    @JsonProperty("SCORE")
    private String score;

    @JsonProperty("FATHERNAME")
    private String fatherName;

    @JsonProperty("COMPLETE")
    private String complete;

    @JsonProperty("CUSTID")
    private String custId;

    @JsonProperty("FNAME_THREE_CHAR")
    private String fnameThreeChar;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("VOTERIDCARDNO")
    private String votercardId;

    @JsonProperty("MOTHERNAME")
    private String mothername;

    @JsonProperty("GENDER")
    private String gender;

    @JsonProperty("STRONG_MATCH")
    private String strongmatch;

    @JsonProperty("FNAME_ORIG")
    private String fnameOrig;

    @JsonProperty("EMAIL")
    private String email;

    @JsonProperty("TITLE")
    private String title;

    @JsonProperty("POTENTIAL_MATCH")
    private String potentialMatch;

    @JsonProperty("LNAME_FIRSTCHAR")
    private String lnameFirstChar;

    @JsonProperty("CUSTTYPE")
    private String custtype;

    @JsonProperty("SEARCHID")
    private String searchid;

    @JsonProperty("LNAME_ORIG")
    private String lanemOrig;

    @JsonProperty("LNAME_THREE_CHAR")
    private String lnameThreeChar;

    @JsonProperty("LNAME")
    private String lname;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
