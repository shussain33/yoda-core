package com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicantMatchedInfo {
    @JsonProperty("PAN")
    private String pan;

    @JsonProperty("ACTUAL_CUSTID")
    private String actualCustid;

    @JsonProperty("MATCHED_RULES")
    private String matchedRules;

    @JsonProperty("FNAME")
    private String fname;

    @JsonProperty("COMPLETENAME")
    private String completeName;

    @JsonProperty("EMAIL_USER")
    private String emailUser;

    @JsonProperty("LNAME_FIRSTCHAR")
    private String lnameFirstchar;


    @JsonProperty("LNAME")
    private String lanme;

    @JsonProperty("LNAME_ORIG")
    private String lnameOrig;


    @JsonProperty("LNAME_THREE_CHAR")
    private String lnameThreeChar;

    @JsonProperty("MNAME_ORIG")
    private String mnameOrig;

    @JsonProperty("RESIMAIL")
    private String resiEmail;

    @JsonProperty("SCORE")
    private String score;

    @JsonProperty("COAPPLCAINT")
    private String coapplicant;

    @JsonProperty("DL")
    private String dl;

    @JsonProperty("DESCRIPTION")
    private String description;

    @JsonProperty("DOB")
    private String dob;

    @JsonProperty("FATHERNAME")
    private String fatherName;

    @JsonProperty("CUSTID")
    private String custId;

    @JsonProperty("MNAME_THREE_CHAR")
    private String mnameThreeChar;

    @JsonProperty("FNAME_THREE_CHAR")
    private String fnamethreeChar;

    @JsonProperty("PRODUCT")
    private String product;

    @JsonProperty("MOTHERNAME")
    private String mothername;

    @JsonProperty("SOURCE")
    private String source;

    @JsonProperty("VOTERIDCARDNO")
    private String voteridcardNo;

    @JsonProperty("NEWCUST_CUSTID")
    private String newcustcustid;

    @JsonProperty("COMPLETENAME_SP")
    private String completesp;

    @JsonProperty("MNAME_FIRSTCHAR")
    private String mnameFirstchar;

    @JsonProperty("MNAME")
    private String mname;

    @JsonProperty("FNAME_ORIG")
    private String fnameOrig;

    @JsonProperty("EMAIL")
    private String email;

    @JsonProperty("CUSTTYPE")
    private String custType;

    @JsonProperty("REASON")
    private String reason;

    @JsonProperty("REMARKS")
    private String remarks;

    @JsonProperty("CUSTOMER_DEDUPE_STATUS")
    private String customerDedupeStatus;

    @JsonProperty("DEDUPE_DECISION")
    private String dedupeDecision;

    @JsonProperty("FLAG")
    private boolean flag;

    @JsonProperty("INDEX")
    private int index;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }

}
