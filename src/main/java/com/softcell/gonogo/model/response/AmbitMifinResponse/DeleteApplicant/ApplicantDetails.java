package com.softcell.gonogo.model.response.AmbitMifinResponse.DeleteApplicant;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicantDetails implements Serializable {

    @JsonProperty("PROSPECT_CODE")
    private String prospectCode;

    @JsonProperty("PROSPECT_ID")
    private String prospectId;
}
