package com.softcell.gonogo.model.response.master.commongeneralmaster;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.SourcingDetailMaster;

import java.util.List;

/**
 * Created by mahesh on 17/5/17.
 */
public class SourcingDetails {

    @JsonProperty("oS1")
    private SourcingDetailMaster s1;

    @JsonProperty("aS2")
    private List<SourcingDetailMaster> s2;

    @JsonProperty("oS3")
    private SourcingDetailMaster s3;

    @JsonProperty("oS4")
    private SourcingDetailMaster s4;

    public SourcingDetailMaster getS1() {
        return s1;
    }

    public void setS1(SourcingDetailMaster s1) {
        this.s1 = s1;
    }

    public List<SourcingDetailMaster> getS2() {
        return s2;
    }

    public void setS2(List<SourcingDetailMaster> s2) {
        this.s2 = s2;
    }

    public SourcingDetailMaster getS3() {
        return s3;
    }

    public void setS3(SourcingDetailMaster s3) {
        this.s3 = s3;
    }

    public SourcingDetailMaster getS4() {
        return s4;
    }

    public void setS4(SourcingDetailMaster s4) {
        this.s4 = s4;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SourcingDetails{");
        sb.append("s1=").append(s1);
        sb.append(", s2=").append(s2);
        sb.append(", s3=").append(s3);
        sb.append(", s4=").append(s4);
        sb.append('}');
        return sb.toString();
    }
}
