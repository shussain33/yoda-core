package com.softcell.gonogo.model.response.AmbitMifinResponse.ProcessDedupe;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.HashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApplicantMatchedBankAccount {

    @JsonProperty("ACCOUNTNUMBER")
    private String accountNumber;

    @JsonProperty("CREATEDON")
    private String createDon;

    @JsonProperty("RECORDID")
    private String recordId;

    @JsonProperty("CUSTID")
    private String custId;

    @JsonProperty("ACCOUNTTYPE")
    private String accountType;

    @JsonProperty("BANKBRANCH")
    private String bankBranch;

    @JsonProperty("BANKADDRESS")
    private String bankAddress;

    @JsonProperty("BANKNAME")
    private String bankName;

    // and then "other" stuff:
    private Map<String,Object> other = new HashMap<String,Object>();

    @JsonAnySetter
    public void set(String name, Object value) {
        if( other == null ) other = new HashMap<String,Object>();
        other.put(name, value);
    }

    // "any getter" needed for serialization
    @JsonAnyGetter
    public Map<String,Object> any() {
        return other;
    }


}
