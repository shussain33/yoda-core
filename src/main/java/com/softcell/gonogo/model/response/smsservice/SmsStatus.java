package com.softcell.gonogo.model.response.smsservice;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author vinodk
 */
public class SmsStatus {

    @JsonProperty("sPhoneNumber")
    private String phoneNumber;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("bDelivered")
    private boolean delivered;

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the delivered
     */
    public boolean isDelivered() {
        return delivered;
    }

    /**
     * @param delivered the delivered to set
     */
    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SmsStatus [phoneNumber=");
        builder.append(phoneNumber);
        builder.append(", status=");
        builder.append(status);
        builder.append(", delivered=");
        builder.append(delivered);
        builder.append("]");
        return builder.toString();
    }
}
