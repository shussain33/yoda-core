package com.softcell.gonogo.model.response.geolimit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by mahesh on 9/12/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeoLimitResponse {

    @JsonProperty("sStatus")
    private String status;

    // can be error message or success message depending on above status.
    @JsonProperty("sMessage")
    private String message;

}
