/**
 * kishorp10:40:34 AM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Name;

import java.util.Date;

/**
 * @author kishorp
 *
 */
public class CroQueue {

    @JsonProperty("sDsaName")
    private String dsaName;

    @JsonProperty("sLoc")
    private String location;

    @JsonProperty("dloanAmt")
    private double loanAmmount;

    @JsonProperty("dtDateTime")
    private Date dateTime;

    @JsonProperty("sAppId")
    private String applicationId;

    @JsonProperty("sRefID")
    private String refId;

    @JsonProperty("sStat")
    private String status;

    @JsonProperty("sStage")
    private String stage;

    @JsonProperty("sProcBy")
    private String processedBy;

    @JsonProperty("oCustName")
    private Name name;

    @JsonProperty("sMobileNumber")
    private String mobileNumber;

    @JsonProperty("sRqTyp")
    private String requestType;

    @JsonProperty("bTvrDecision")
    private boolean tvrDecision;

    public boolean isTvrDecision() {
        return tvrDecision;
    }

    public void setTvrDecision(boolean tvrDecision) {
        this.tvrDecision = tvrDecision;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getDsaName() {
        return dsaName;
    }

    public void setDsaName(String dsaName) {
        this.dsaName = dsaName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getLoanAmmount() {
        return loanAmmount;
    }

    public void setLoanAmmount(double loanAmmount) {
        this.loanAmmount = loanAmmount;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProcessedBy() {
        return processedBy;
    }

    public void setProcessedBy(String processedBy) {
        this.processedBy = processedBy;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * @param requestType the requestType to set
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */

    @Override
    public String toString() {
        return "CroQueue [dsaName=" + dsaName + ", location=" + location
                + ", loanAmmount=" + loanAmmount + ", dateTime=" + dateTime
                + ", applicationId=" + applicationId + ", refId=" + refId
                + ", status=" + status + ", stage=" + stage + ", processedBy="
                + processedBy + ", name=" + name + ", mobileNumber="
                + mobileNumber + ", requestType=" + requestType + "]";
    }


}
