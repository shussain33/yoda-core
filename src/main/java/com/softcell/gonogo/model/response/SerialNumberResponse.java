package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */

public class SerialNumberResponse {

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sStatus")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
