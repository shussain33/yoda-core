package com.softcell.gonogo.model.response.core;

import com.softcell.gonogo.model.multibureau.MultiBureauResponse;
import com.softcell.gonogo.model.scoring.ScoreResponse;

public class Finished {
    private long applicantId;
    private String status;
    private ErrorMessage errors;
    private ScoreResponse scoreResponse;
    private MultiBureauResponse multiBureauResponse;

    public long getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(long applicantId) {
        this.applicantId = applicantId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ErrorMessage getErrors() {
        return errors;
    }

    public void setErrors(ErrorMessage errors) {
        this.errors = errors;
    }

    public ScoreResponse getScoreResponse() {
        return scoreResponse;
    }

    public void setScoreResponse(ScoreResponse scoreResponse) {
        this.scoreResponse = scoreResponse;
    }

    public MultiBureauResponse getMultiBureauResponse() {
        return multiBureauResponse;
    }

    public void setMultiBureauResponse(MultiBureauResponse multiBureauResponse) {
        this.multiBureauResponse = multiBureauResponse;
    }


}
