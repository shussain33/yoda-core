package com.softcell.gonogo.model.response.los;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by yogeshb on 8/6/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "mbPushResponseLog")
public class MbPushResponse {

    private String refId;
    private MbResponseAcknowledgement mbResponseAck;
    private MbEndOfTransactionAcknowledgement mbEoTAck;

    public MbResponseAcknowledgement getMbResponseAck() {
        return mbResponseAck;
    }

    public void setMbResponseAck(MbResponseAcknowledgement mbResponseAck) {
        this.mbResponseAck = mbResponseAck;
    }

    public MbEndOfTransactionAcknowledgement getMbEoTAck() {
        return mbEoTAck;
    }

    public void setMbEoTAck(MbEndOfTransactionAcknowledgement mbEoTAck) {
        this.mbEoTAck = mbEoTAck;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MbPushResponse{");
        sb.append("refId='").append(refId).append('\'');
        sb.append(", mbResponseAck=").append(mbResponseAck);
        sb.append(", mbEoTAck=").append(mbEoTAck);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MbPushResponse that = (MbPushResponse) o;

        if (refId != null ? !refId.equals(that.refId) : that.refId != null) return false;
        if (mbResponseAck != null ? !mbResponseAck.equals(that.mbResponseAck) : that.mbResponseAck != null)
            return false;
        return mbEoTAck != null ? mbEoTAck.equals(that.mbEoTAck) : that.mbEoTAck == null;
    }

    @Override
    public int hashCode() {
        int result = refId != null ? refId.hashCode() : 0;
        result = 31 * result + (mbResponseAck != null ? mbResponseAck.hashCode() : 0);
        result = 31 * result + (mbEoTAck != null ? mbEoTAck.hashCode() : 0);
        return result;
    }
}
