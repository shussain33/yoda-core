package com.softcell.gonogo.model.response.smsservice;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author vinodk
 */
public class Error {

    @JsonProperty("sErrorMsg")
    private String errorMsg;

    @JsonProperty("sErrorCode")
    private String errorCode;

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Error [errorMsg=");
        builder.append(errorMsg);
        builder.append(", errorCode=");
        builder.append(errorCode);
        builder.append("]");
        return builder.toString();
    }
}