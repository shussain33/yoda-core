package com.softcell.gonogo.model.response.AmbitMifinResponse.SaveFinancialInfo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveFinancialInfoBody {

    @JsonProperty("RECEIVE")
    public MiFinSaveFinancialInfoBaseResponse miFinSaveFinancialInfoBaseResponse;
}