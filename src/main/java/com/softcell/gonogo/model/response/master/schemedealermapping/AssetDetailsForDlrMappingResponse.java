package com.softcell.gonogo.model.response.master.schemedealermapping;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

/**
 * @author mahesh
 */
public class AssetDetailsForDlrMappingResponse {

    @JsonProperty("aMfrDscr")
    private Set<String> manufacturerDesc;

    @JsonProperty("aCatDsc")
    private Set<String> catgDesc;

    @JsonProperty("aMdlNo")
    private Set<String> modelNo;

    public Set<String> getManufacturerDesc() {
        return manufacturerDesc;
    }

    public void setManufacturerDesc(Set<String> manufacturerDesc) {
        this.manufacturerDesc = manufacturerDesc;
    }

    public Set<String> getCatgDesc() {
        return catgDesc;
    }

    public void setCatgDesc(Set<String> catgDesc) {
        this.catgDesc = catgDesc;
    }

    public Set<String> getModelNo() {
        return modelNo;
    }

    public void setModelNo(Set<String> modelNo) {
        this.modelNo = modelNo;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AssetDetailsForDlrMappingResponse [manufacturerDesc=");
        builder.append(manufacturerDesc);
        builder.append(", catgDesc=");
        builder.append(catgDesc);
        builder.append(", modelNo=");
        builder.append(modelNo);
        builder.append("]");
        return builder.toString();
    }


}
