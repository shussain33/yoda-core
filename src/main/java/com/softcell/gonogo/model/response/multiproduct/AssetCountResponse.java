package com.softcell.gonogo.model.response.multiproduct;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AssetCountResponse {

    @JsonProperty("iProductAllowed")
    private int productAllowed;

    @JsonProperty("iProductRemaining")
    private int productRemaining;

    @JsonProperty("iProductNumber")
    private int productNumber;

    @JsonProperty("dUnUtilizedAmt")
    private double unUtilizedAmt;

    @JsonProperty("dApprovedAmt")
    private double approvedAmount;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sError")
    private MultiProductError error;

    public int getProductAllowed() {
        return productAllowed;
    }

    public void setProductAllowed(int productAllowed) {
        this.productAllowed = productAllowed;
    }

    public int getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(int productNumber) {
        this.productNumber = productNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MultiProductError getError() {
        return error;
    }

    public void setError(MultiProductError error) {
        this.error = error;
    }

    public int getProductRemaining() {
        return productRemaining;
    }

    public void setProductRemaining(int productRemaining) {
        this.productRemaining = productRemaining;
    }

    public double getUnUtilizedAmt() {
        return unUtilizedAmt;
    }

    public void setUnUtilizedAmt(double unUtilizedAmt) {
        this.unUtilizedAmt = unUtilizedAmt;
    }



    public double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AssetCountResponse that = (AssetCountResponse) o;

        if (productAllowed != that.productAllowed) return false;
        if (productRemaining != that.productRemaining) return false;
        if (productNumber != that.productNumber) return false;
        if (Double.compare(that.unUtilizedAmt, unUtilizedAmt) != 0) return false;
        if (Double.compare(that.approvedAmount, approvedAmount) != 0) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        return error != null ? error.equals(that.error) : that.error == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = productAllowed;
        result = 31 * result + productRemaining;
        result = 31 * result + productNumber;
        temp = Double.doubleToLongBits(unUtilizedAmt);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(approvedAmount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (error != null ? error.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AssetCountResponse{");
        sb.append("productAllowed=").append(productAllowed);
        sb.append(", productRemaining=").append(productRemaining);
        sb.append(", productNumber=").append(productNumber);
        sb.append(", unUtilizedAmt=").append(unUtilizedAmt);
        sb.append(", approvedAmount=").append(approvedAmount);
        sb.append(", status='").append(status).append('\'');
        sb.append(", error=").append(error);
        sb.append('}');
        return sb.toString();
    }
}
