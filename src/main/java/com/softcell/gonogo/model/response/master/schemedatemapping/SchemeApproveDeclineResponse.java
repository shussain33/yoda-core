package com.softcell.gonogo.model.response.master.schemedatemapping;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class SchemeApproveDeclineResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sMessage")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
