package com.softcell.gonogo.model.response;

import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.request.ApplicationRequest;

public class CroData {
    private ApplicationRequest applicationRequest;
    private ResponseMultiJsonDomain responseMultiJsonDomain;
    private ScoringResponse scoringResponse;

    public ApplicationRequest getApplicationRequest() {
        return applicationRequest;
    }

    public void setApplicationRequest(ApplicationRequest applicationRequest) {
        this.applicationRequest = applicationRequest;
    }

    public ResponseMultiJsonDomain getResponseMultiJsonDomain() {
        return responseMultiJsonDomain;
    }

    public void setResponseMultiJsonDomain(
            ResponseMultiJsonDomain responseMultiJsonDomain) {
        this.responseMultiJsonDomain = responseMultiJsonDomain;
    }

    public ScoringResponse getScoringResponse() {
        return scoringResponse;
    }

    public void setScoringResponse(ScoringResponse scoringResponse) {
        this.scoringResponse = scoringResponse;
    }

    @Override
    public String toString() {
        return "CroData [applicationRequest=" + applicationRequest
                + ", responseMultiJsonDomain=" + responseMultiJsonDomain
                + ", scoringResponse=" + scoringResponse + "]";
    }


}
