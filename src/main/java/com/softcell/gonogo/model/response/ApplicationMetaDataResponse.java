/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 4, 2016 9:20:55 PM
 */
package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.masters.ApplicationMetaData;
import com.softcell.gonogo.model.response.core.AckHeader;
import com.softcell.gonogo.model.response.core.ErrorMessage;
import com.softcell.gonogo.model.response.core.Warnings;

/**
 * @author kishorp
 *
 */
public class ApplicationMetaDataResponse {
    @JsonProperty("oHeader")
    private AckHeader header;
    @JsonProperty("oWrngs")
    private Warnings warnings;
    @JsonProperty("oError")
    private ErrorMessage errors;
    @JsonProperty("oAppMtDt")
    private ApplicationMetaData applicationMetaData;

    /**
     * @return the applicationMetaData
     */
    public ApplicationMetaData getApplicationMetaData() {
        return applicationMetaData;
    }

    /**
     * @param applicationMetaData the applicationMetaData to set
     */
    public void setApplicationMetaData(ApplicationMetaData applicationMetaData) {
        this.applicationMetaData = applicationMetaData;
    }

    /**
     * @return the header
     */
    public AckHeader getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(AckHeader header) {
        this.header = header;
    }

    /**
     * @return the warnings
     */
    public Warnings getWarnings() {
        return warnings;
    }

    /**
     * @param warnings the warnings to set
     */
    public void setWarnings(Warnings warnings) {
        this.warnings = warnings;
    }

    /**
     * @return the errors
     */
    public ErrorMessage getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(ErrorMessage errors) {
        this.errors = errors;
    }

}
