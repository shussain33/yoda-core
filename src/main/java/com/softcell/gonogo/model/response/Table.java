package com.softcell.gonogo.model.response;

import java.util.List;

/**
 * Created by piyushg on 15/2/17.
 */
public class Table<T> {

    private long totalCount;
    private List<?> data;

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Table table = (Table) o;

        if (totalCount != table.totalCount) return false;
        return data != null ? data.equals(table.data) : table.data == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (totalCount ^ (totalCount >>> 32));
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Table{" +
                "totalCount=" + totalCount +
                ", data=" + data +
                '}';
    }
}
