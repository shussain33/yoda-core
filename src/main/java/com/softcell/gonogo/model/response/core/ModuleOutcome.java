/**
 * kishorp9:27:49 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.model.response.core;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

/**
 * @author kishorp
 */
public class ModuleOutcome {

    @JsonProperty("sCustID")
    private String customerId;

    @JsonProperty("sFldName")
    private String fieldName;

    @JsonProperty("iOrder")
    private int order;

    @JsonProperty("sFldVal")
    private String fieldValue;

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("iAddrStblty")
    private int addStability;

    @JsonProperty("fNameScore")
    private String nameScore;

    public static Builder builder() {
        return new Builder();
    }

    public String getNameScore() {
        return nameScore;
    }

    public void setNameScore(String nameScore) {
        this.nameScore = nameScore;
    }

    public int getAddStability() {
        return addStability;
    }

    public void setAddStability(int addStability) {
        this.addStability = addStability;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ModuleOutcome{");
        sb.append("customerId='").append(customerId).append('\'');
        sb.append(", fieldName='").append(fieldName).append('\'');
        sb.append(", order=").append(order);
        sb.append(", fieldValue='").append(fieldValue).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", addStability=").append(addStability);
        sb.append(", nameScore='").append(nameScore).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ModuleOutcome)) return false;
        ModuleOutcome that = (ModuleOutcome) o;
        return Objects.equal(getOrder(), that.getOrder()) &&
                Objects.equal(getAddStability(), that.getAddStability()) &&
                Objects.equal(getCustomerId(), that.getCustomerId()) &&
                Objects.equal(getFieldName(), that.getFieldName()) &&
                Objects.equal(getFieldValue(), that.getFieldValue()) &&
                Objects.equal(getMessage(), that.getMessage()) &&
                Objects.equal(getNameScore(), that.getNameScore());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getCustomerId(), getFieldName(), getOrder(), getFieldValue(), getMessage(), getAddStability(), getNameScore());
    }

    public static class Builder {
        private ModuleOutcome moduleOutcome = new ModuleOutcome();

        public ModuleOutcome build() {
            return moduleOutcome;
        }

        public Builder customerId(String customerId) {
            this.moduleOutcome.customerId = customerId;
            return this;
        }

        public Builder fieldName(String fieldName) {
            this.moduleOutcome.fieldName = fieldName;
            return this;
        }

        public Builder order(int order) {
            this.moduleOutcome.order = order;
            return this;
        }

        public Builder fieldValue(String fieldValue) {
            this.moduleOutcome.fieldValue = fieldValue;
            return this;
        }

        public Builder message(String message) {
            this.moduleOutcome.message = message;
            return this;
        }

        public Builder addStability(int addStability) {
            this.moduleOutcome.addStability = addStability;
            return this;
        }

        public Builder nameScore(String nameScore) {
            this.moduleOutcome.nameScore = nameScore;
            return this;
        }


    }
}
