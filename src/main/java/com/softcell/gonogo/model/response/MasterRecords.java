package com.softcell.gonogo.model.response;

import com.softcell.gonogo.model.core.error.FileUploadStatus;

import java.util.List;

/**
 * @author yogeshb
 */
public class MasterRecords {
    private FileUploadStatus fileUploadStatus;
    private List<?> masterList;

    /**
     * @return the fileUploadStatus
     */
    public FileUploadStatus getFileUploadStatus() {
        return fileUploadStatus;
    }

    /**
     * @param fileUploadStatus the fileUploadStatus to set
     */
    public void setFileUploadStatus(FileUploadStatus fileUploadStatus) {
        this.fileUploadStatus = fileUploadStatus;
    }

    /**
     * @return the masterList
     */
    public List<?> getMasterList() {
        return masterList;
    }

    /**
     * @param masterList the masterList to set
     */
    public void setMasterList(List<?> masterList) {
        this.masterList = masterList;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MasterRecords [fileUploadStatus=" + fileUploadStatus
                + ", masterList=" + masterList + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((fileUploadStatus == null) ? 0 : fileUploadStatus.hashCode());
        result = prime * result
                + ((masterList == null) ? 0 : masterList.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof MasterRecords))
            return false;
        MasterRecords other = (MasterRecords) obj;
        if (fileUploadStatus == null) {
            if (other.fileUploadStatus != null)
                return false;
        } else if (!fileUploadStatus.equals(other.fileUploadStatus))
            return false;
        if (masterList == null) {
            if (other.masterList != null)
                return false;
        } else if (!masterList.equals(other.masterList))
            return false;
        return true;
    }
}
