package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 26/3/20.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanChargesRepaymentResponse {

    @JsonProperty("oLoanCharges")
    private LoanCharges loanCharges;

    @JsonProperty("oRepayment")
    private Repayment repayment;

    @JsonProperty("sRefId")
    public String refId;
}
