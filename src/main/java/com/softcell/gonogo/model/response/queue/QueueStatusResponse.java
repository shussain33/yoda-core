package com.softcell.gonogo.model.response.queue;

import java.util.List;
import java.util.Map;

/**
 * Created by archana on 18/9/17.
 */

public class QueueStatusResponse {

    private List<UserStatus> onlineUsers;

    private List<UserStatus> idleUsers;

    private List<UserStatus> offlineUsers;

    // This is map reflecting db where operator field is set
    private Map<String, List<String>> operatorIdMap;

    public List<UserStatus> getOnlineUsers() {
        return onlineUsers;
    }

    public void setOnlineUsers(List<UserStatus> onlineUsers) {
        this.onlineUsers = onlineUsers;
    }

    public List<UserStatus> getIdleUsers() {
        return idleUsers;
    }

    public void setIdleUsers(List<UserStatus> idleUsers) {
        this.idleUsers = idleUsers;
    }

    public List<UserStatus> getOfflineUsers() {
        return offlineUsers;
    }

    public void setOfflineUsers(List<UserStatus> offlineUsers) {
        this.offlineUsers = offlineUsers;
    }

    public Map<String, List<String>> getOperatorIdMap() {
        return operatorIdMap;
    }

    public void setOperatorIdMap(Map<String, List<String>> operatorIdMap) {
        this.operatorIdMap = operatorIdMap;
    }
}
