package com.softcell.gonogo.model.response.AmbitMifinResponse.DeleteApplicant;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeleteApplicantBaseResponse implements Serializable {
    @JsonProperty("STATUS")
    private String status;

    @JsonProperty("MESSAGE")
    private String message;

    @JsonProperty("APPLICANT_DETAILS")
    private List<ApplicantDetails> applicantDetailsList;
}
