package com.softcell.gonogo.model.response.manufacturer.samsung;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author kishorp
 *         Vendor will be use for to filter API call
 *         of Manufacturer. if and serial number validation  is applicable.
 *         else it return null or empty.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SerialNumberResponse {

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sVendor")
    private String vendor;

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public static Builder builder() {
        return new Builder();
    }


    public static class Builder {
        private SerialNumberResponse serialNumberResponse = new SerialNumberResponse();

        public SerialNumberResponse build() {
            return serialNumberResponse;
        }

        public Builder message(String message) {
            this.serialNumberResponse.message = message;
            return this;
        }

        public Builder status(String status) {
            this.serialNumberResponse.status = status;
            return this;
        }

        public Builder vendor(String vendor) {
            this.serialNumberResponse.vendor = vendor;
            return this;
        }

    }

}
