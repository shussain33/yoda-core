package com.softcell.gonogo.model.response.manufacturer.samsung;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mahesh
 */
public class UpdatedSRNumberDetailsResponse {

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sStatus")
    private String status;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
