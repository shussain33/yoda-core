package com.softcell.gonogo.model.response.AmbitMifinResponse;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MifinAmbitResponse {

    @JsonProperty("id")
    private String id;

    @JsonProperty("updated")
    private String updated;

    @JsonProperty("product")
    private String product;

    @JsonProperty("vendor")
    private String vedndor;

    @JsonProperty("referenceId")
    private String referenceId;

    @JsonProperty("institutionId")
    private String institutionId;

    @JsonProperty("version")
    private String version;

    @JsonProperty("mifin")
    private MifinBaseAmbitResponse mifinBaseAmbitResponse;

    @JsonProperty("oError")
    private Error error;

    @JsonProperty("created")
    private String created;

    @JsonProperty("type")
    private String type;
}