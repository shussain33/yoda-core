package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "referenceId",
        "applicantId"
})
public class KycImageHeader {
    /**
     * Application reference Number
     */
    @JsonProperty("referenceId")
    private String referenceId;

    /**
     * applicantId
     */
    @JsonProperty("applicantId")
    private String applicantId;

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    @Override
    public String toString() {
        return "KycImageHeader [referenceId=" + referenceId + ", applicantId="
                + applicantId + "]";
    }
}
