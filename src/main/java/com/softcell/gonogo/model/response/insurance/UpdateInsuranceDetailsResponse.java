package com.softcell.gonogo.model.response.insurance;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 29/5/17.
 */
public class UpdateInsuranceDetailsResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResMessage")
    private String resMessage;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResMessage() {
        return resMessage;
    }

    public void setResMessage(String resMessage) {
        this.resMessage = resMessage;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdateInsuranceDetailsResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", resMessage='").append(resMessage).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
