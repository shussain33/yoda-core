package com.softcell.gonogo.model.response.queue;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.queue.management.RealtimeUserStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by archana on 18/9/17.
 */
public class UserStatus {

    @JsonProperty("sLoginId")
    private String userId;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("sRole")
    private String role;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sAssignedCases")
    private List<String> assignedCaseIds = new ArrayList<>();

    @JsonProperty("sWorkingOnCase")
    private String workingCaseId;

    @JsonProperty("dLastSignal")
    private Date lastSignalReceived;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInstitutionId() {         return institutionId;    }

    public void setInstitutionId(String institutionId) {        this.institutionId  = institutionId;    }

    public List<String> getAssignedCaseIds() {
        return assignedCaseIds;
    }

    public void setAssignedCaseIds(List<String> assignedCaseIds) {
        this.assignedCaseIds = assignedCaseIds;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {   this.role = role;    }

    public Date getLastSignalReceived() {
        return lastSignalReceived;
    }

    public void setLastSignalReceived(Date lastSignalReceived) {
        this.lastSignalReceived = lastSignalReceived;
    }

    public String getWorkingCaseId() {
        return workingCaseId;
    }

    public void setWorkingCaseId(String workingCaseId) {
        this.workingCaseId = workingCaseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RealtimeUserStatus realtimeUserStatus = (RealtimeUserStatus) o;

        if ( institutionId.equalsIgnoreCase(realtimeUserStatus.getInstitutionId())
                &&  userId.equals(realtimeUserStatus.getUserId()) ) return true;

        return false;

    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + institutionId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserStatus{");
        sb.append("userId='").append(userId).append('\'');
        sb.append(", insttId ='").append(institutionId).append('\'');
        sb.append(", role ='").append(role).append('\'');
        sb.append(", assignedCaseIds =").append(assignedCaseIds);
        sb.append(", workingCaseId ='").append(workingCaseId).append('\'');
        sb.append(", lastSignalReceived = ").append(lastSignalReceived);
        sb.append("}\n");
        return sb.toString();
    }
}
