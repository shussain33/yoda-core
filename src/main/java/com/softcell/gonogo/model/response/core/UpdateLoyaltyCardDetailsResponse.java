package com.softcell.gonogo.model.response.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mahesh on 28/2/17.
 */
public class UpdateLoyaltyCardDetailsResponse {

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sResponseMsg")
    private String responseMsg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UpdateLoyaltyCardDetailsResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", responseMsg='").append(responseMsg).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
