package com.softcell.gonogo.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.user.TvrDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Softcell on 24/07/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TvrStatusResponse
{
    @JsonProperty("oTvrDetails")
    private TvrDetails tvrDetails;
}
