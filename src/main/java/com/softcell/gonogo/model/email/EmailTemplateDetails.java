package com.softcell.gonogo.model.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.Date;

/**
 * @author yogeshb
 */

@Document(collection = "SendMailLog")
public class EmailTemplateDetails extends AuditEntity {

    private String institutionId;
    private Date dateTime = new Date();

    @JsonProperty("sRefID")
    private String refId;
    private double loanAmount;
    private String subject;
    private String[] to;
    private String[] cc;
    private String[] bcc;
    private String template;
    private boolean sent;
    private String productType;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String[] getTo() {
        return to;
    }

    public void setTo(String[] to) {
        this.to = to;
    }

    public String[] getCc() {
        return cc;
    }

    public void setCc(String[] cc) {
        this.cc = cc;
    }

    public String[] getBcc() {
        return bcc;
    }

    public void setBcc(String[] bcc) {
        this.bcc = bcc;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("EmailTemplateDetails [institutionId=");
        builder.append(institutionId);
        builder.append(", dateTime=");
        builder.append(dateTime);
        builder.append(", refId=");
        builder.append(refId);
        builder.append(", loanAmount=");
        builder.append(loanAmount);
        builder.append(", subject=");
        builder.append(subject);
        builder.append(", to=");
        builder.append(Arrays.toString(to));
        builder.append(", cc=");
        builder.append(Arrays.toString(cc));
        builder.append(", bcc=");
        builder.append(Arrays.toString(bcc));
        builder.append(", template=");
        builder.append(template);
        builder.append(", sent=");
        builder.append(sent);
        builder.append(", productType=");
        builder.append(productType);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(bcc);
        result = prime * result + Arrays.hashCode(cc);
        result = prime * result
                + ((dateTime == null) ? 0 : dateTime.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        long temp;
        temp = Double.doubleToLongBits(loanAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((productType == null) ? 0 : productType.hashCode());
        result = prime * result + ((refId == null) ? 0 : refId.hashCode());
        result = prime * result + (sent ? 1231 : 1237);
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        result = prime * result
                + ((template == null) ? 0 : template.hashCode());
        result = prime * result + Arrays.hashCode(to);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof EmailTemplateDetails))
            return false;
        EmailTemplateDetails other = (EmailTemplateDetails) obj;
        if (!Arrays.equals(bcc, other.bcc))
            return false;
        if (!Arrays.equals(cc, other.cc))
            return false;
        if (dateTime == null) {
            if (other.dateTime != null)
                return false;
        } else if (!dateTime.equals(other.dateTime))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (Double.doubleToLongBits(loanAmount) != Double
                .doubleToLongBits(other.loanAmount))
            return false;
        if (productType == null) {
            if (other.productType != null)
                return false;
        } else if (!productType.equals(other.productType))
            return false;
        if (refId == null) {
            if (other.refId != null)
                return false;
        } else if (!refId.equals(other.refId))
            return false;
        if (sent != other.sent)
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        if (template == null) {
            if (other.template != null)
                return false;
        } else if (!template.equals(other.template))
            return false;
        if (!Arrays.equals(to, other.to))
            return false;
        return true;
    }

}
