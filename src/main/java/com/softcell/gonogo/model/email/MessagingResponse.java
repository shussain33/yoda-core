package com.softcell.gonogo.model.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.kyc.response.ErrorDesc;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by yogesh on 25/10/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessagingResponse {

    @JsonProperty("status")
    private String status;

    @JsonProperty("oError")
    private ErrorDesc error;

    @JsonProperty("sOtp")
    private String otp;

}
