package com.softcell.gonogo.model.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Attachment;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by bhavishya on 25/10/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GenericMailRequest {
    @JsonProperty("oHeader")
    private Header header;

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sAction")
    private String action;

    @JsonProperty("aTo")
    private String[] to;

    @JsonProperty("sSub")
    private String subject;

    @JsonProperty("sText")
    private String content;

    @JsonProperty("aCC")
    private String[] cc;

}
