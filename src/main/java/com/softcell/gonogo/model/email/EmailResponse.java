package com.softcell.gonogo.model.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogesh on 24/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailResponse {

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("sOtp")
    private String otp;

    @JsonProperty("sUuid")
    private String uuid;

    @JsonProperty("sIdentifier")
    private String identifier;
}
