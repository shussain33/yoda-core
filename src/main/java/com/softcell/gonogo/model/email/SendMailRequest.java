package com.softcell.gonogo.model.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Created by yogesh on 24/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SendMailRequest {
    @JsonProperty("requestParams")
    private Map<String, String> requestParams ;

    @Override
    public String toString() {
        return "SendMailRequest{" +
                "requestParams=" + requestParams +
                '}';
    }
}
