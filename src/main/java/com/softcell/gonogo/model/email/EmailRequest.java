package com.softcell.gonogo.model.email;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogesh on 24/2/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailRequest {

    @JsonProperty("oHeader")
    @Valid
    @NotNull(groups = {Header.InsertGroup.class})
    private Header header;

    @NotNull
    @JsonProperty("sEmail")
    private String emailId;

    @NotNull
    @JsonProperty("sRefId")
    private String refId;

    @JsonProperty("sIdentifier")
    private String identifier;

}
