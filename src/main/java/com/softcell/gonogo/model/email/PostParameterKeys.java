package com.softcell.gonogo.model.email;

/**
 * Created by yogesh on 24/2/18.
 */
public class PostParameterKeys {
    public static final String RECIPIENTS = "recipients";
    public static final String CONTENT = "content";
    public static final String SUBJECT = "subject";
}
