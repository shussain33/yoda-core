package com.softcell.gonogo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by yogesh on 5/12/17.
 */
public class DisbursementDetails {

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("dtDisbDate")
    private Date disbDate;

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public Date getDisbDate() {
        return disbDate;
    }

    public void setDisbDate(Date disbDate) {
        this.disbDate = disbDate;
    }
}
