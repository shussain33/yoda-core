package com.softcell.gonogo.queue.management;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * @author amit.botre
 */
@Document(collection = "queueGroupConfiguration")
public class QueueGroupConfiguration {
    private String institutionId; //
    private String groupName; // Name for the group
    private List<String> products; // Type of product (eg: CDL, DPL etc.)
   // TODO : add if require configuration for sub-products
   // private List<String> subProducts; // Names of sub-products (Optional)
    private String groupType; // User group (eg: Sales, CRO, OPS etc)
    private String hierarchyType; // Hierarchy type (Organizational/Geographic)
    private String hierarchyLevel; // only if hierarchyType = Geographic
    private List<String> hierarchyValues; // only if hierarchyType = Geographic
    private List<String> userIds; // UserIds for whome configuration is applied
    private String nextGroupName; // Name of the next group which needs to be call after

    public String getInstitutionId() {  return institutionId; }

    public void setInstitutionId(String institutionId) {  this.institutionId = institutionId; }

    public String getGroupName() {   return groupName;    }

    public List<String> getProducts() {        return products;    }

    //public List<String> getSubProducts() {        return subProducts;    }

    public String getGroupType() {        return groupType;    }

    public String getHierarchyType() {        return hierarchyType;    }

    public String getHierarchyLevel() {        return hierarchyLevel;    }

    public List<String> getHierarchyValues() {        return hierarchyValues;    }

    public List<String> getUserIds() {        return userIds;   }

    public String getNextGroupName() {  return nextGroupName; }

    public void setNextGroupName(String nextGroupName) {  this.nextGroupName = nextGroupName; }

    public void setGroupName(String groupName) {        this.groupName = groupName;    }

    public void setProducts(List<String> products) {        this.products = products;    }

    //public void setSubProducts(List<String> subProducts) {        this.subProducts = subProducts;    }

    public void setGroupType(String groupType) {        this.groupType = groupType;    }

    public void setHierarchyType(String hierarchyType) {        this.hierarchyType = hierarchyType;    }

    public void setHierarchyLevel(String hierarchyLevel) {        this.hierarchyLevel = hierarchyLevel;    }

    public void setHierarchyValues(List<String> hierarchyValues) {        this.hierarchyValues = hierarchyValues;    }

    public void setUserIds(List<String> userIds) {        this.userIds = userIds;    }

    @Override
    public String toString() {
      return "QueueGroupConfiguration{" +
              "institutionId='" + institutionId + '\'' +
              ", groupName='" + groupName + '\'' +
              ", products=" + products +
              ", groupType='" + groupType + '\'' +
              ", hierarchyType='" + hierarchyType + '\'' +
              ", hierarchyLevel='" + hierarchyLevel + '\'' +
              ", hierarchyValues=" + hierarchyValues +
              ", userIds=" + userIds +
              ", nextGroupName='" + nextGroupName + '\'' +
              '}';
    }
}
