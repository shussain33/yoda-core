package com.softcell.gonogo.queue.management;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.queue.management.QueueRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by archana on 11/9/17.
 */
@Component
public class QueueActivityHelper {
    // CroQueTracking fieldnames
    public static final String FIELD_lastSignalReceived = "lastSignalReceived";
    public static final String FIELD_assignedCaseIds = "assignedCaseIds";
    public static final String FIELD_lastCaseAssignment = "lastCaseAssignment";
    public static final String FIELD_availableFlag = "availableFlag";


    @Autowired
    private QueueRepository queueRepository;

    public void logActivity(String userId, String instituteId, List<String> refIds, GNGWorkflowConstant action,
                            String customMessage) throws Exception {
        saveActivity(createLogs(userId, instituteId, refIds, action, customMessage));
    }

    /**
     * Creates activitylogs for refIds assigned to a user for an action.
     *
     * @param realtimeUserStatus User to which refIds are assigned.
     * @param refIds             List of refIds assigned
     * @param action             Assignement happened as a result of this action
     * @param customMessage      Message
     * @return
     * @throws Exception
     */
    public List<ActivityLogs> createLogs(RealtimeUserStatus realtimeUserStatus, List<String> refIds,
                                         GNGWorkflowConstant action, String customMessage) throws Exception {
        return createLogs(realtimeUserStatus.getUserId(), realtimeUserStatus.getInstitutionId(), refIds,
                action, customMessage);
    }

    public List<ActivityLogs> createLogs(String userId, String instituteId, List<String> refIds,
                                         GNGWorkflowConstant action, String customMessage) throws Exception {
        List<ActivityLogs> logsToInsert = new ArrayList<ActivityLogs>();
        ActivityLogs activityLogs;
        for (String refId : refIds) {
            activityLogs = new ActivityLogs();
            activityLogs.setUserName(userId);
            activityLogs.setInstitutionId(instituteId);
            activityLogs.setRefId(refId);
            activityLogs.setAction(action.toFaceValue());
            activityLogs.setCustomMsg(customMessage);
            activityLogs.setActionDate(new Date());

            logsToInsert.add(activityLogs);
        }
        return logsToInsert;
    }

    /**
     * Saves a list of activitylogs to database
     *
     * @param logsToInsert List of activityLogs to be saved in database.
     * @throws Exception
     */
    public void saveActivity(List<ActivityLogs> logsToInsert) throws Exception {
        queueRepository.saveActivityLog(logsToInsert);

    }

    /**
     * Logs activity of distributing a list of refIds among list of users.
     * Finds out which refId has been assigned to which user and logs for each refId.
     *
     * @param realtimeUserStatusList List of users to which refIds are assigned
     * @param caseIds                List of refIds assigned among list of users
     * @param action                 Action under which these assignments have been done.
     * @param customMessage
     * @throws Exception
     */
    public void logActivity(List<RealtimeUserStatus> realtimeUserStatusList, List<String> caseIds,
                            GNGWorkflowConstant action, String customMessage) throws Exception {
        List<String> copy;
        String caseId;
        List<ActivityLogs> logsToInsert = new ArrayList<>();
        for (RealtimeUserStatus realtimeUserStatus : realtimeUserStatusList) {
            copy = new ArrayList<>(realtimeUserStatus.getAssignedCaseIds());
            copy.retainAll(caseIds);

            logsToInsert.addAll(createLogs(realtimeUserStatus, copy, action, customMessage));

            // Update CroTracking
            Map<String, Object> croTrackingFields = new HashMap<>();
            croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, copy);
            updateCroQueueTracking(realtimeUserStatus.getUserId(), realtimeUserStatus.getInstitutionId(), croTrackingFields);
        }
        saveActivity(logsToInsert);
    }

    public void updateCroQueueTracking(String userId, String institutionId, Map<String, Object> updateValues) {
        queueRepository.updateCroQueueTracking(userId, institutionId, updateValues);
    }

    public void updateCroQueueTracking(List<RealtimeUserStatus> distributedTo) {
        List<Map<String, Object>> fieldValueMapList = new ArrayList<>();
        for (RealtimeUserStatus realtimeUserStatus : distributedTo) {
            Map<String, Object> croTrackingFields = new HashMap<>();
            croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, realtimeUserStatus.getAssignedCaseIds());
            croTrackingFields.put(QueueActivityHelper.FIELD_lastCaseAssignment, new Date());
            fieldValueMapList.add(croTrackingFields);
        }
        queueRepository.updateCroQueueTracking(distributedTo, fieldValueMapList);
    }
}
