package com.softcell.gonogo.queue.management;

import java.util.List;

/**
 * Created by archana on 13/7/17.
 */
public interface CaseDistributor {

    void setCasesPerUser(int caseCount);

    void assignCases(RealtimeUserStatus realtimeUserStatus, String action) throws Exception;

    /**
     * @param realtimeUserStatus Assigns cases to user represented by RealtimeUserStatus
     */
    void assignCases(RealtimeUserStatus realtimeUserStatus, String action,
                     int skip) throws Exception;

    /**
     * @param caseIds
     * @return Reassigns the cases of an idle/offline user to the online users.
     */
    List<RealtimeUserStatus> redistributeCases(List<RealtimeUserStatus> onlineUsers, List<String> caseIds, String institutionId) throws Exception;

    /**
     * Updates operator for the list of newly assigned cases
     *
     * @param userId
     * @param toBeAssignedIds
     */
    void updateOperator(String userId, List<String> toBeAssignedIds);

    /**
     * Fills in the 'quota' of cases assigned to a user, so that user's plate is full.
     *
     * @param realtimeUserStatusList List of users
     * @param fetchCount             Total number of cases to be fetched and distributed amongst the active users.
     */
    void assignCases(List<RealtimeUserStatus> realtimeUserStatusList, int fetchCount) throws Exception;
}

