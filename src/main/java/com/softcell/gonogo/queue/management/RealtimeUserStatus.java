package com.softcell.gonogo.queue.management;

import com.softcell.gonogo.model.request.core.RequestCriteria;
import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;

import java.io.Serializable;
import java.util.*;

/**
 * Created by archana on 12/7/17.
 */
@REntity
public class RealtimeUserStatus implements Serializable {

    public enum Status {
        ONLINE, OFFLINE, IDLE;
    };


    // username
    // zone
    // InsttId
    // array of assigned cases List<GoNoGoCustomerApplication.refId>

    @RId
    private String userId;
    private String userName;
    private String institutionId;
    private String role;
    private String zone;
    private Status status;
    private String statusName; // Added for Redisson
    private List<String> assignedCaseIds = new ArrayList<>();
    private Date lastSignalReceived;
    private String workingCaseId;
    private Map<String,Long> todaysCroStatistics = new HashMap<>();

    private RequestCriteria criteria;

    public String getStatusName() {
        return statusName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
        statusName = status.name();
    }

    public void setStatusName(Status status) {
        statusName = status.name();
    }
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInstitutionId() {         return institutionId;    }

    public void setInstitutionId(String institutionId) {        this.institutionId  = institutionId;    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public List<String> getAssignedCaseIds() {
        return assignedCaseIds;
    }

    public void setAssignedCaseIds(List<String> assignedCaseIds) {
        this.assignedCaseIds = assignedCaseIds;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {   this.role = role;    }

    public Date getLastSignalReceived() {
        return lastSignalReceived;
    }

    public void setLastSignalReceived(Date lastSignalReceived) {
        this.lastSignalReceived = lastSignalReceived;
    }

    public String getWorkingCaseId() {
        return workingCaseId;
    }

    public void setWorkingCaseId(String workingCaseId) {
        this.workingCaseId = workingCaseId;
    }

    public Map<String,Long> getTodaysCroStatistics() {
        return todaysCroStatistics;
    }
    public void  setTodaysCroStatistics(Map<String,Long> todaysCroStatistics) {
        this.todaysCroStatistics =  todaysCroStatistics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RealtimeUserStatus realtimeUserStatus = (RealtimeUserStatus) o;

        if ( institutionId.equalsIgnoreCase(realtimeUserStatus.getInstitutionId())
              &&  userId.equals(realtimeUserStatus.getUserId()) ) return true;

        return false;

    }

    @Override
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + institutionId.hashCode();
        return result;
    }

    public RequestCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(RequestCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RealtimeUserStatus{");
        sb.append("userId='").append(userId).append('\'');
        sb.append(", insttId ='").append(institutionId).append('\'');
        sb.append(", role ='").append(role).append('\'');
        sb.append(", zone ='").append(zone).append('\'');
        sb.append(", status = '").append(status).append('\'');
        sb.append(", assignedCaseIds =").append(assignedCaseIds);
        sb.append(", workingCaseId ='").append(workingCaseId).append('\'');
        sb.append(", todaysCroStats ='").append(todaysCroStatistics.entrySet()).append('\'');
        sb.append(", lastSignalReceived = ").append(lastSignalReceived);
        sb.append("}\n");
        return sb.toString();
    }


}
