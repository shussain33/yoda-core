package com.softcell.gonogo.queue.management;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.service.DataEntryManager;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by archana on 20/7/17.
 * Scheduler to load balance cases against online users.
 * Also checks for the users whether they have gone idle/offline.
 */
@Component
@PropertySource(value = "classpath:queue-management.properties")
public class CaseLoadBalancer {

    private static final Logger logger = LoggerFactory.getLogger(CaseLoadBalancer.class);

    private static Date lastSchedulerRun;

    @Value("${MAX_IDLE_TIME_IN_MILLIS}")
    private long MAX_IDLE_TIME_IN_MILLIS;

    @Value("${OFFLINE_TIME_IN_MILLIS}")
    private long OFFLINE_TIME_IN_MILLIS;

    @Value("${DELAY_IN_MILLIS}")
    private long DELAY_IN_MILLIS;

    private static Date LAST_SCHEDULED_DATE;

    private QueueEventHandler queueHandler;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    AppConfigurationRepository appConfigurationRepository;

    @Autowired
    private DataEntryManager dataEntryManager;


    public CaseLoadBalancer(QueueEventHandler queueEventHandler) {
        this.queueHandler = queueEventHandler;
    }

    public static Date getLastSchedulerRun() {
        return lastSchedulerRun;
    }

    public long getMAX_IDLE_TIME_IN_MILLIS() {
        return MAX_IDLE_TIME_IN_MILLIS;
    }

    public long getOFFLINE_TIME_IN_MILLIS() {
        return OFFLINE_TIME_IN_MILLIS;
    }

    public long getDELAY_IN_MILLIS() {
        return DELAY_IN_MILLIS;
    }


    /*
    ~ Auto user status tracking.
         + Online - Some user activity in the last 5 minutes.
         + Idle - No user activity in the last 5 minutes.
         + Offline - No user activity in the last 20 minutes.

     */
    /* Cron expression to run scheduler between 9am to 11 pm
    *                           every 5 mins
             0 0/5 9-22 ? * * *

             @Scheduled(cron = "${cron.expression}"
    */
    //@Scheduled(fixedDelayString = "${DELAY_IN_MILLIS}")
    //@Scheduled(cron = "${SCHEDULER_FREQUENCY}")
    public void scheduleCaseLoadBalancing() {

        logger.info("LoadBalancer triggered with values MAX_IDLE_TIME_IN_MILLIS {}," +
                        " OFFLINE_TIME_IN_MILLIS {}",
                MAX_IDLE_TIME_IN_MILLIS, OFFLINE_TIME_IN_MILLIS);

        lastSchedulerRun = new Date();

        /* Check online users from RealtimeCache for their last signal received
            If last signal received is far before the signal delay specified
                then    mark the user as idle
                        Reassign his cases ( except workFlag set ) to other online users
                        // ?? What to do if no online user other than this one is present ??
        */
        markOffline();
        markIdle();
        queueHandler.assignCases();
    }

 @Scheduled(cron = "0 0 7 * * *")
 /* @Scheduled(cron = "0 0/1 * * * *")*/
    public void aadharMaskingSchedular(){
        logger.debug("Masking Schedular called!");
        try{
            maskApplications();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public void maskApplications(){
        Date startDate = LAST_SCHEDULED_DATE;
        Date endDate = new Date();
        Calendar c=new GregorianCalendar();
        c.add(Calendar.DATE, -60);
        endDate = c.getTime();

        List<GoNoGoCustomerApplication> applications = applicationRepository.fetchAllCasesBeforeDefinedTime(startDate, endDate);
        logger.debug("Application needs to mask between StartDate : " + startDate + " EndDate : " + endDate);
       applications.forEach(application -> {
           boolean isApplicantUpdated = false;
           boolean isCoApplicantUpdated = false;
            Request request = application.getApplicationRequest().getRequest();
            if(request != null){
                if(request.getApplicant() != null && CollectionUtils.isNotEmpty(request.getApplicant().getKyc())){
                    for(Kyc kyc : request.getApplicant().getKyc()) {
                        if(StringUtils.isNotEmpty(kyc.getKycNumber()) &&
                                StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.AADHAAR.name()) &&
                                kyc.getKycNumber().length() == 12){
                            logger.info("Aadhaar masking in applicant");
                            kyc.setKycNumber(kyc.getKycNumber().replace(
                                    kyc.getKycNumber().substring(3, kyc.getKycNumber().length() - 3), "000000"));
                            isApplicantUpdated = true;
                        }
                    }
                }
                if(CollectionUtils.isNotEmpty(request.getCoApplicant())){
                    for(CoApplicant coApplicant : request.getCoApplicant()) {
                        if (CollectionUtils.isNotEmpty(coApplicant.getKyc())) {
                            for(Kyc kyc : coApplicant.getKyc()) {
                                if (StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.AADHAAR.name()) &&
                                        StringUtils.isNotEmpty(kyc.getKycNumber()) &&
                                kyc.getKycNumber().length() == 12) {
                                    kyc.setKycNumber(kyc.getKycNumber().replace(
                                            kyc.getKycNumber().substring(3, kyc.getKycNumber().length() - 3), "000000"));
                                    isCoApplicantUpdated = true;
                                }
                            }
                        }
                    }
                }
            }
           application.getApplicationRequest().setRequest(request);
           if(isApplicantUpdated || isCoApplicantUpdated)
                applicationRepository.updateKycDetails(application, isApplicantUpdated, isCoApplicantUpdated);
        });
        logger.debug("Addhar masking Done");
        LAST_SCHEDULED_DATE = endDate;
    }

    private void markIdle() {
        List<RealtimeUserStatus> userStatuses = queueHandler.getOnlineUsers();
        long timeElapsed;
        for (RealtimeUserStatus userStatus : userStatuses) {
            timeElapsed = System.currentTimeMillis() - userStatus.getLastSignalReceived().getTime();
            logger.debug("time elapsed {} secs for user {} of institution {} since he was active",
                    timeElapsed / 1000, userStatus.getUserId(), userStatus.getInstitutionId());

            if (timeElapsed > (MAX_IDLE_TIME_IN_MILLIS)) { // Signal not received
                // rebalance
                try {
                    logger.info("Setting user {} of institution {} IDLE, last heartbeat received at {}",
                            userStatus.getUserId(), userStatus.getInstitutionId(), userStatus.getLastSignalReceived());
                    queueHandler.registerUserIdle(userStatus.getUserId(), userStatus.getInstitutionId());
                } catch (Exception e) {
                    logger.error("Error in markIdle() - Stacktrace as below ");
                    logger.error("{}",e.getStackTrace());
                    logger.error("-------------------------------------------");
                }
            }
        }
    }

    private void markOffline() {
        List<RealtimeUserStatus> userStatuses = queueHandler.getIdleUsers();
        long timeElapsed;
        for (RealtimeUserStatus userStatus : userStatuses) {
            timeElapsed = System.currentTimeMillis() - userStatus.getLastSignalReceived().getTime();
            logger.debug("time elapsed {} secs for user {} of institution {} since he was active",
                    timeElapsed / 1000, userStatus.getUserId(), userStatus.getInstitutionId());

            if (timeElapsed > (OFFLINE_TIME_IN_MILLIS)) { // Signal not received
                // rebalance
                try {
                    logger.info("Setting user {} of institution {} OFFLINE, last heartbeat received at {}",
                            userStatus.getUserId(), userStatus.getInstitutionId(), userStatus.getLastSignalReceived());
                    queueHandler.registerUserOffline(userStatus.getUserId(), userStatus.getInstitutionId());
                } catch (Exception e) {
                    logger.error("Error in markOffline() - Stacktrace as below ");
                    logger.error("{}",e.getStackTrace());
                    logger.error("-------------------------------------------");
                }
            }
        }
    }

    //@Scheduled(cron = "0 0 2 * * *")
    public void scheduleMigration(){
        dataEntryManager.getCoApplicantCibilScore("4021", null);
        dataEntryManager.migrateCoApplicantPLBSData("4021", null);
    }
}
