package com.softcell.gonogo.queue.management;

import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.request.queue.HeartbeatRequest;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by archana on 12/7/17.
 */
public final class RealtimeUserCache {

    private static Map<String, RealtimeUserStatus> userCache = new HashMap<>();

    public static String getKey(String userId, String instituteId){
        return userId + "-" + instituteId;
    }


    public static void setOffline(String userId, String institutionId) {
        synchronized(userCache) {
            userCache.remove(RealtimeUserCache.getKey(userId, institutionId));
        }
    }

    public static void setOnline(RealtimeUserStatus realtimeUserStatus) {
        String key = RealtimeUserCache.getKey(realtimeUserStatus.getUserId(), realtimeUserStatus.getInstitutionId());

        if( userCache.get(key) == null ){
            realtimeUserStatus.setStatus(RealtimeUserStatus.Status.ONLINE);
            userCache.put(key, realtimeUserStatus);
        }else{
            userCache.get(key).setStatus(RealtimeUserStatus.Status.ONLINE);
        }
    }

    public static void setToIdle(String userId, String institutionId ) {
        String key = RealtimeUserCache.getKey(userId, institutionId);
        synchronized (userCache) {
            if (userCache.get(key) != null) {
                userCache.get(key).setStatus(RealtimeUserStatus.Status.IDLE);
            } else {
                // Error : user not found in cache
                // TODO : what??
            }
        }
    }

    public static List<RealtimeUserStatus> getIdleUsers(String instituteId){
        return userCache.values().stream()
                .filter(realtimeUserStatus ->
                        realtimeUserStatus.getStatus() == RealtimeUserStatus.Status.IDLE
                                && StringUtils.equalsIgnoreCase( realtimeUserStatus.getInstitutionId(), instituteId) )
                .collect(Collectors.toList());
    }

    public static List<RealtimeUserStatus> getOnlineUsers(String instituteId){
        // filter on status ONLINE
        return userCache.values().stream()
                .filter(realtimeUserStatus -> 
                        realtimeUserStatus.getStatus() == RealtimeUserStatus.Status.ONLINE
                                && StringUtils.equalsIgnoreCase( realtimeUserStatus.getInstitutionId(), instituteId) )
                .collect(Collectors.toList());
    }

    public static List<RealtimeUserStatus> getIdleUsers(){
        return userCache.values().stream()
                .filter(realtimeUserStatus ->
                        realtimeUserStatus.getStatus() == RealtimeUserStatus.Status.IDLE )
                .collect(Collectors.toList());
    }
    public static List<RealtimeUserStatus> getOnlineUsers(){
        // filter on status ONLINE
        return userCache.values().stream()
                .filter(realtimeUserStatus ->
                        realtimeUserStatus.getStatus() == RealtimeUserStatus.Status.ONLINE )
                .collect(Collectors.toList());
    }

    public static boolean updateHeartbeat(HeartbeatRequest heartbeat) throws Exception{
        boolean statusChanged = false;
        String userId = heartbeat.getHeader().getLoggedInUserId();
        String instituteId = heartbeat.getHeader().getInstitutionId();
        RealtimeUserStatus userstatus =  userCache.get(RealtimeUserCache.getKey(userId, instituteId));
        if( userstatus == null || userstatus.getStatus() == RealtimeUserStatus.Status.OFFLINE ){
            throw new Exception(String.format("User %s not logged in", userId) );
        }
        if( userstatus.getStatus() == RealtimeUserStatus.Status.IDLE){
            userstatus.setStatus(RealtimeUserStatus.Status.ONLINE);
            statusChanged = true;
        }
        userstatus.setLastSignalReceived(new Date());
        return statusChanged;
    }


    // Thisis singleton
    private RealtimeUserCache() {
    }

    public static RealtimeUserStatus getUserRealtimeStatus(String userId, String instituteId) throws GoNoGoException {
        RealtimeUserStatus userstatus =  userCache.get(RealtimeUserCache.getKey(userId, instituteId));
        if( userstatus == null  ){
            throw new GoNoGoException(String.format("User %s not logged in ", userId));
        }else{
            return userstatus;
        }
    }


    public static String log(){
        StringBuilder sb = new StringBuilder("\nRealtimeUserCache has following entries :\n");
        sb.append(userCache);
        sb.append("\n------\n");
        return sb.toString();
    }

    public static boolean isUserLoggedIn(String userId, String institutionId) {
        boolean loggedIn = true;
        try {
            getUserRealtimeStatus(userId, institutionId);
        } catch (GoNoGoException gne) {
            loggedIn = false;
        }
        return loggedIn;
    }
}
