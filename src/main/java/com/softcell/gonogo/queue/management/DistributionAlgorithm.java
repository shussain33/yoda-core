package com.softcell.gonogo.queue.management;

import java.util.List;

/**
 * Created by archana on 19/7/17.
 */
public interface DistributionAlgorithm {
    /**
     * Returns list of userStatus which are assigned.
     *
     * @param caseIds
     * @param onlineUsers
     * @param casesPerUser
     * @return
     */
    List<RealtimeUserStatus> distribute(List<String> caseIds, List<RealtimeUserStatus> onlineUsers, int casesPerUser);
}
