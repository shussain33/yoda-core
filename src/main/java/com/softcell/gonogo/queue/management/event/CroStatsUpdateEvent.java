package com.softcell.gonogo.queue.management.event;

import java.util.Map;

/**
 * Created by archana on 29/11/17.
 */
public class CroStatsUpdateEvent {
    private String userId;
    private String institutionId;
    private Map<String, Long> croStats;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Map<String, Long> getCroStats() {
        return croStats;
    }

    public void setCroStats(Map<String, Long> croStats) {
        this.croStats = croStats;
    }
}
