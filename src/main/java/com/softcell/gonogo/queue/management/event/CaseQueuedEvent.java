package com.softcell.gonogo.queue.management.event;

/**
 * Created by archana on 15/11/17.
 */
public class CaseQueuedEvent {

    private String institutionId;
    private String caseId;
    private String role;

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
