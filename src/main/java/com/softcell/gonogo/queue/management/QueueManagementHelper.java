package com.softcell.gonogo.queue.management;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.gonogo.queue.management.event.CroStatsUpdateEvent;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by archana on 2/11/17.
 */
public class QueueManagementHelper {

    public static final String STAT_KEY_FOR_QUEUED_COUNT = GNGWorkflowConstant.QUEUED.toFaceValue();

    public static final Boolean CASES_SORT_BY_LATEST = Boolean.FALSE;

    public static final Comparator<RealtimeUserStatus> sortByCaseCount =
            (RealtimeUserStatus o1, RealtimeUserStatus o2)
                    -> Integer.valueOf(o1.getAssignedCaseIds().size())
                    .compareTo(Integer.valueOf(o2.getAssignedCaseIds().size()));

    public static void updateCroStats(RealtimeUserStatus realtimeUserStatus) {
        // Update CRO stats
        Map<String, Long> croStats = realtimeUserStatus.getTodaysCroStatistics();
        croStats.put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                Long.valueOf(realtimeUserStatus.getAssignedCaseIds().size()));
        updateCroStatistics(realtimeUserStatus.getUserId(), realtimeUserStatus.getInstitutionId(), croStats);
    }

    public static void updateCroStatistics(String userId, String institutionId, Map<String, Long> croStats) {
        CroStatsUpdateEvent croStatsUpdateEvent = new CroStatsUpdateEvent();
        croStatsUpdateEvent.setUserId(userId);
        croStatsUpdateEvent.setInstitutionId(institutionId);
        croStatsUpdateEvent.setCroStats(croStats);
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(croStatsUpdateEvent);
    }


    static List<String> getHalfOfMostAgedCases(RealtimeUserStatus userStatus) throws Exception {
        List<String> casesToRedistribue = new ArrayList<>();

        List<String> assignedCaseIds = userStatus.getAssignedCaseIds();
        String workingCaseId = userStatus.getWorkingCaseId();
        // Reassign his cases to others -> 50% of most aged cases to be re-balanced when the users become idle.
        if (CollectionUtils.isNotEmpty(assignedCaseIds)) {
            int listSize = assignedCaseIds.size();

            int reallocateCount = 0;
            if (listSize >= 1) {
                if (listSize % 2 != 0) {
                    reallocateCount++;
                } // odd list

                reallocateCount = reallocateCount + listSize / 2;
            }

            // Currently working case
            if (workingCaseId != null && listSize > 1) {
                assignedCaseIds.remove(workingCaseId);
                --listSize;
            }
            if (listSize >= 1) {
                List<String> retainedCases;
                /*  NOTE : The subList becomes invalid when the underlying original list changes.
                            Make a copy of it.
                 */
                if (CASES_SORT_BY_LATEST) {
                    // Redistribute last ones in the list
                    retainedCases = new ArrayList<String>(assignedCaseIds.subList(0, (listSize - reallocateCount)));
                    casesToRedistribue = new ArrayList<String>(assignedCaseIds.subList((listSize - reallocateCount), listSize));

                } else {
                    // Redistribute first ones in the list
                    casesToRedistribue = new ArrayList<String>(assignedCaseIds.subList(0, reallocateCount));
                    retainedCases = new ArrayList<String>(assignedCaseIds.subList(reallocateCount, listSize));
                }
                userStatus.setAssignedCaseIds(retainedCases);
            }
        }
        // Put the working case id back in the assignedCases list
        if (workingCaseId != null) {
            userStatus.getAssignedCaseIds().add(workingCaseId);
        }

        // Update CRO stats
        Map<String, Long> croStats = userStatus.getTodaysCroStatistics();
        croStats.put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                Long.valueOf(userStatus.getAssignedCaseIds().size()));
        updateCroStatistics(userStatus.getUserId(), userStatus.getInstitutionId(), croStats);

        return casesToRedistribue;
    }
}
