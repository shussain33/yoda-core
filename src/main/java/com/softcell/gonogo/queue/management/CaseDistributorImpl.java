package com.softcell.gonogo.queue.management;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.queue.management.QueueRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by archana on 13/7/17.
 */
@Component
@PropertySource(value = "classpath:queue-management.properties")
public class CaseDistributorImpl implements CaseDistributor {

    private static final Logger logger = LoggerFactory.getLogger(CaseDistributorImpl.class);

    @Autowired
    private QueueRepository queueRepository;

    @Value("${CASES_PER_USER}")
    private int casesPerUser;

    @Autowired
    private DistributionAlgorithm distributionAlgorithm;

    @Autowired
    private QueueActivityHelper activityHelper;

    public int getCasesPerUser() {
        return casesPerUser;
    }

    public void setCasesPerUser(int casesPerUser) {
        this.casesPerUser = casesPerUser;
    }

    @Override
    public synchronized void assignCases(RealtimeUserStatus realtimeUserStatus, String action) throws Exception {
        assignCases(realtimeUserStatus, action, 0);
    }

    @Override
    public synchronized void assignCases(RealtimeUserStatus realtimeUserStatus, String action, int skip) throws Exception {
        /*
        Case POJO fields to be added :
            operatorId
            operationTime
        List<String> queueRepository.fetchQueuedCases(boolean assigned);
            Fetch cases in stage CR_Q where operatorId = null ( unassigned cases => assigned= false)
            Fetch cases in stage CR_Q where operatorId != null ( assigned cases => assigned= true)
        List<String> queueRepository.fetchQueuedCases(Time criterion); // All cases
            Fetch cases in stage CR_Q where createdTime falls within the period mentioned
        List<String> queueRepository.fetchQueuedCases(String userId);
            Fetch cases in stage CR_Q where operatorId = userId

        TODO : All above queries have a default timeperiod criterion applied and order by timestamp
         */
        List<String> refIds;
        int listSize = realtimeUserStatus.getAssignedCaseIds().size();
        // call repository
        int fetchCount = casesPerUser - listSize;
        refIds = queueRepository.fetchQueuedCaseIds(fetchCount,
                realtimeUserStatus.getInstitutionId(), realtimeUserStatus.getRole(), realtimeUserStatus.getCriteria(), skip);

        logger.debug("Fertched {} cases for user {} action '{}'; Fetched cases are {}", fetchCount, realtimeUserStatus.getUserId(),
                action, refIds);

        // Add ref ids
        realtimeUserStatus.getAssignedCaseIds().addAll(listSize, refIds);
        // update operator for these cases
        updateOperator(realtimeUserStatus.getUserId(), refIds);
        // Activity log
        activityHelper.logActivity(realtimeUserStatus.getUserId(), realtimeUserStatus.getInstitutionId(), refIds,
                GNGWorkflowConstant.CASE_ASSIGNED, action);

    }

    @Override
    public synchronized List<RealtimeUserStatus> redistributeCases(List<RealtimeUserStatus> onlineUsers, List<String> caseIds, String institutionId) throws Exception {
        List<RealtimeUserStatus> distributedTo = distributionAlgorithm.distribute(caseIds, onlineUsers, casesPerUser);
        // Activity log
        activityHelper.logActivity(distributedTo, caseIds, GNGWorkflowConstant.CASE_ASSIGNED,
                GNGWorkflowConstant.CASES_REDISTRIBUTION.toFaceValue());
        // Update CroQueueTracking
        activityHelper.updateCroQueueTracking(distributedTo);
        return onlineUsers;
    }

    @Override
    public void updateOperator(String userId, List<String> assignedIds) {
        // Update DB
        queueRepository.updateOperator(userId, assignedIds);
    }

    @Override
    public synchronized void assignCases(List<RealtimeUserStatus> realtimeUserStatusList, int fetchCount) throws Exception {

        // Get case ids from database
        List<String> refIds = queueRepository.fetchQueuedCaseIds(fetchCount,
                realtimeUserStatusList.get(0).getInstitutionId(), realtimeUserStatusList.get(0).getRole(),
                realtimeUserStatusList.get(0).getCriteria(), 0);
//        List<String> refIds = getForTest(fetchCount);

        // check whether there are queued cases
        if (CollectionUtils.isNotEmpty(refIds)) {
            // Distribute teh refIds among the userstatusList
            List<String> assignedList;
            List<ActivityLogs> activityLogs = new ArrayList<>();

            // Sort the user list based on cases in their queue
            // So that user having less number of cases will get cases first
            realtimeUserStatusList.sort(QueueManagementHelper.sortByCaseCount);

            for (RealtimeUserStatus realtimeUserStatus : realtimeUserStatusList) {
                assignedList = realtimeUserStatus.getAssignedCaseIds();
                int assignCount = casesPerUser - assignedList.size();
            /*  NOTE : The subList becomes invalid when the underlying original list changes.
                    Make a copy of it.
            */
                List<String> toBeAssignedCases = new ArrayList<String>(refIds.subList(0, assignCount));

                assignedList.addAll(toBeAssignedCases);
                // update remaining caseId list
                refIds = refIds.subList(assignCount, refIds.size());

                // update operator for assigned cases
                updateOperator(realtimeUserStatus.getUserId(), toBeAssignedCases);

                // Update CROStat
                QueueManagementHelper.updateCroStats(realtimeUserStatus);

                // Update CroQueueTracking
                Map<String, Object> croTrackingFields = new HashMap<>();
                croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, assignedList);
                croTrackingFields.put(QueueActivityHelper.FIELD_lastCaseAssignment, new Date());
                activityHelper.updateCroQueueTracking(realtimeUserStatus.getUserId(), realtimeUserStatus.getInstitutionId(),
                                                        croTrackingFields);

                activityLogs.addAll(activityHelper.createLogs(realtimeUserStatus, toBeAssignedCases, GNGWorkflowConstant.CASE_ASSIGNED,
                        GNGWorkflowConstant.CASE_ASSIGNED.toFaceValue()));
            }
            activityHelper.saveActivity(activityLogs);
        }
    }

    private List<String> getForTest(int fetchCount) {
        List<String> ids = new ArrayList<String>();
        for (int i = 0; i < fetchCount; i++) {
            ids.add(Integer.toString(i * 2));
        }
        return ids;
    }


}
