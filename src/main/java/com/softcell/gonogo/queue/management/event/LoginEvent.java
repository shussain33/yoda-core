package com.softcell.gonogo.queue.management.event;

import com.softcell.gonogo.model.security.v2.Hierarchy;
import com.softcell.gonogo.model.security.v2.Product;

import java.util.Date;
import java.util.Set;

/**
 * Created by archana on 12/7/17.
 */
public class LoginEvent {
    private String userId;

    private String role;
    private String institutionId;
    private Date loggedInTime;

    private Set<Product> products;
    private Hierarchy hierarchy;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Date getLoggedInTime() {
        return loggedInTime;
    }

    public void setLoggedInTime(Date loggedInTime) {
        this.loggedInTime = loggedInTime;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Hierarchy getHierarchy() {
        return hierarchy;
    }

    public void setHierarchy(Hierarchy hierarchy) {
        this.hierarchy = hierarchy;
    }

}
