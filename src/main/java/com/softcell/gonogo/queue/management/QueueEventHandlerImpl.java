package com.softcell.gonogo.queue.management;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Product;
import com.softcell.dao.mongodb.repository.queue.management.QueueRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.gonogo.cache.distributed.RealtimeUserStatusCache;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.queueMgmt.CroAudit;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.gonogo.model.request.queue.CaseAllocationRequest;
import com.softcell.gonogo.model.request.queue.CroAuditRequest;
import com.softcell.gonogo.model.request.queue.HeartbeatRequest;
import com.softcell.gonogo.queue.management.event.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

/**
 * Created by archana on 12/7/17.
 * <p>
 * ~ Auto case re-balancing when the users turn idle/offline/online.
 * + 50% of most aged cases to be re-balanced when the users become idle.
 * + All cases to be re-balanced when the users become offline.
 * + Cases to be re-balanced when users become online.
 */
@Component
public class QueueEventHandlerImpl implements QueueEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(QueueEventHandlerImpl.class);

    @Autowired
    private CaseDistributor caseDistributor;

    @Autowired
    private QueueRepository queueRepository;

    @Autowired
    private ConfigurationRepository configurationRepository;

    @Autowired
    private QueueActivityHelper queueActivityHelper;

    @Autowired
    private RealtimeUserStatusCache userStatusCache;


    @Value("${CASES_PER_USER}")
    private int casesPerUser;

    public int getCasesPerUser() {
        return casesPerUser;
    }

    public void setCasesPerUser(int casesPerUser) {
        this.casesPerUser = casesPerUser;
    }

    @Override
    public void addQueueUnassignedCasesInCache(List<QueueUnassignedCases> QueueUnassignedCasess){

       QueueUnassignedCasess.forEach(QueueUnassignedCases -> {
            // add new object in cache
            userStatusCache.setInstituteUnassignedCases(QueueUnassignedCases);
        });
    }

    @Override
    public List<QueueUnassignedCases> getQueueUnassignedCases(List<String> ids){
        List<QueueUnassignedCases> list = new ArrayList<>();
        ids.forEach(id -> {
            list.add(userStatusCache.getQueueUnassignedCases(id));
        });
        return list;
    }

    @Async
    @EventListener
    @Override
    public void registerUserLogin(LoginEvent loginEvent) throws Exception {
        RealtimeUserStatus realtimeUserStatus = new RealtimeUserStatus();
        realtimeUserStatus.setUserId(loginEvent.getUserId());
        realtimeUserStatus.setRole(loginEvent.getRole());
        realtimeUserStatus.setInstitutionId(loginEvent.getInstitutionId());
        realtimeUserStatus.setStatus(RealtimeUserStatus.Status.ONLINE);
        realtimeUserStatus.setStatusName(RealtimeUserStatus.Status.ONLINE);
        realtimeUserStatus.setLastSignalReceived(loginEvent.getLoggedInTime());
        realtimeUserStatus.setCriteria(new RequestCriteria());
        realtimeUserStatus.getCriteria().setHierarchy(loginEvent.getHierarchy());
        Set<Product> products = new HashSet<>();
        realtimeUserStatus.getCriteria().setProducts(products);

        Set<String> productAbbrs = loginEvent.getProducts().stream()
                .map(product -> product.getAbbreviation()).collect(toSet());
        for (String productAbbr : productAbbrs) {
            products.add(Product.getProductFromName(productAbbr));
        }

        // Assign cases to him
        caseDistributor.assignCases(realtimeUserStatus, GNGWorkflowConstant.LOGIN.toFaceValue());

        /*  Setup statistics data for today :
            Get the approved, declined, onhold cases from db.
            Reason : User can log in more than once during day then the cases he worked on today
            would need to be fetched from DB.
         */
        loadCroStatistics(realtimeUserStatus);

        // Put in Cache
        userStatusCache.setOnline(realtimeUserStatus);

        // Update CroQueueTracking
        Map<String, Object> croTrackingFields = new HashMap<>();
        croTrackingFields.put(QueueActivityHelper.FIELD_lastSignalReceived, new Date());
        croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, realtimeUserStatus.getAssignedCaseIds());
        croTrackingFields.put(QueueActivityHelper.FIELD_lastCaseAssignment, new Date());
        queueActivityHelper.updateCroQueueTracking(loginEvent.getUserId(), loginEvent.getInstitutionId(), croTrackingFields);

        logger.debug("LOGIN of {}", realtimeUserStatus);
    }

    private void loadCroStatistics(RealtimeUserStatus realtimeUserStatus) throws Exception {
        // Load count for Approved, Declined , onhold cases done today
        Map<String, Long> croStatistics = queueRepository.fetchCroStatistics(realtimeUserStatus.getUserId(),
                realtimeUserStatus.getInstitutionId());
        if (croStatistics == null) {
            croStatistics = new HashMap<>();
        }
        // Add entry for pending count
        croStatistics.put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                new Long(realtimeUserStatus.getAssignedCaseIds().size()));

        realtimeUserStatus.setTodaysCroStatistics(croStatistics);
    }

    @Async
    @EventListener
    @Override
    public void registerUserLogout(LogoutEvent logoutEvent) {
        String userId = logoutEvent.getUserId();
        String instituteId = logoutEvent.getInstituteId();
        setCacheOffline(userId, instituteId);
    }

    private void setCacheOffline(String userId, String instituteId) {
        try {
            RealtimeUserStatus realtimeUserStatus = userStatusCache.getRealtimeUserStatus(userId, instituteId, false);
            // The Cache Object may be from global cache hence when user gets offline we cannot get the assigned cases.
            // So keep copy of values which we are going to deal with later in the code
            // sublist -  fromIndex, inclusive, and toIndex, exclusive.
            /*  NOTE : The subList becomes invalid when the underlying original list changes.
                        Make a copy of it if original is going to be changed.
            */
            List<String> assignedCaseIds = realtimeUserStatus.getAssignedCaseIds();
            assignedCaseIds = new ArrayList<String>(assignedCaseIds.subList(0, assignedCaseIds.size()));

            // Mark the user offline in the cache
            userStatusCache.setOffline(userId, instituteId);

            // Audit activity
            queueActivityHelper.logActivity(userId, instituteId, assignedCaseIds, GNGWorkflowConstant.CASE_UNASSIGNED,
                    GNGWorkflowConstant.LOGOUT.toFaceValue());

            // Reassign his cases to others
            List<RealtimeUserStatus> onlineUsers = userStatusCache.getOnlineUsers(instituteId);
            if (CollectionUtils.isNotEmpty(assignedCaseIds)) {
                // There should be online users available for redistribution
                if (onlineUsers.size() > 0) {
                    caseDistributor.redistributeCases(onlineUsers, assignedCaseIds, instituteId);
                } else { // No user available
                    // clear operator to null
                    caseDistributor.updateOperator(null, assignedCaseIds);
                }
            }

            // Update CroQueueTracking for this user
            Map<String, Object> croTrackingFields = new HashMap<>();
            croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, new ArrayList<>());
            croTrackingFields.put(QueueActivityHelper.FIELD_lastCaseAssignment, new Date());
            queueActivityHelper.updateCroQueueTracking(userId, instituteId, croTrackingFields);
        } catch (Exception e) {
            // TODO
        }
    }

    @Override
    public void registerUserOffline(String userId, String instituteId) {
        setCacheOffline(userId, instituteId);
    }

    @Override
    public void registerUserIdle(String userId, String instituteId) throws Exception {

        RealtimeUserStatus realtimeUserStatus = userStatusCache.getRealtimeUserStatus(userId, instituteId, false);
        List<String> casesToRedistribute = getCasesToRedistribute(realtimeUserStatus);

        userStatusCache.setIdle(userId, instituteId);

        // Audit activity - unassignement from idle user;
        queueActivityHelper.logActivity(userId, instituteId, casesToRedistribute, GNGWorkflowConstant.CASE_UNASSIGNED,
                GNGWorkflowConstant.USER_STATUS_CHANGES_TO_IDLE.toFaceValue());

        // Distribute the cases
        List<RealtimeUserStatus> onlineUsers = userStatusCache.getOnlineUsers(instituteId);
        if (CollectionUtils.isNotEmpty(casesToRedistribute)) {
            // There has to be any user available to distribute the cases
            if (onlineUsers.size() > 0) {
                caseDistributor.redistributeCases(userStatusCache.getOnlineUsers(instituteId), casesToRedistribute, instituteId);
            } else { // No user available - cases are not distributed
                // clear operator to null
                caseDistributor.updateOperator(null, casesToRedistribute);
            }
        }

        // Update CroQueueTracking
        Map<String, Object> croTrackingFields = new HashMap<>();
        croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, realtimeUserStatus.getAssignedCaseIds());
        croTrackingFields.put(QueueActivityHelper.FIELD_lastCaseAssignment, new Date().getTime());
        queueActivityHelper.updateCroQueueTracking(userId, instituteId, croTrackingFields);
    }

    @Async
    @EventListener
    @Override
    public void registerHeartbeat(HeartbeatRequest heartbeat) throws Exception {
        Map<String, Object> croTrackingFields = new HashMap<>();
        boolean userStatusChanged;
        String userId = heartbeat.getHeader().getLoggedInUserId();
        String institutionId = heartbeat.getHeader().getInstitutionId();

        logger.debug("Received heartbeat for user {} of institution {}", userId, institutionId);

        userStatusChanged = userStatusCache.updateHeartbeat(userId, institutionId);
        croTrackingFields.put(QueueActivityHelper.FIELD_lastSignalReceived, new Date());

        if (userStatusChanged) {
            RealtimeUserStatus realtimeUserStatus = userStatusCache.getRealtimeUserStatus(userId, institutionId, false);
            // Assign cases
            caseDistributor.assignCases(realtimeUserStatus,
                    GNGWorkflowConstant.USER_STATUS_CHANGES_TO_ACTIVE.toFaceValue());
            // Update cro stats
            realtimeUserStatus.getTodaysCroStatistics().put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                    new Long(realtimeUserStatus.getAssignedCaseIds().size()));
            userStatusCache.updateCroStats(userId, institutionId, realtimeUserStatus.getTodaysCroStatistics());

            // Activity audit
            croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, realtimeUserStatus.getAssignedCaseIds());
        }

        // Update Cro Queue Tracking
        queueActivityHelper.updateCroQueueTracking(heartbeat.getHeader().getLoggedInUserId(),
                heartbeat.getHeader().getInstitutionId(), croTrackingFields);

        logger.debug("Cache update complete for heartbeat for user {} of institution {}", userId, institutionId);
    }

    @Async
    @EventListener
    @Override
    public void caseSubmission(CaseQueuedEvent caseQueuedEvent) {
        /*
            Get the online users for the institution.
            Check whether anyone is having lessno. of cases.
            Assign the case to him.
            Update operator in DB
         */

        // Sort in asc order  and get the first one only
        List<RealtimeUserStatus> users = RealtimeUserCache.getOnlineUsers(caseQueuedEvent.getInstitutionId()).stream()
                .filter(realtimeUserStatus ->
                        StringUtils.equalsIgnoreCase(realtimeUserStatus.getRole(), caseQueuedEvent.getRole())
                                && realtimeUserStatus.getAssignedCaseIds().size() < casesPerUser
                )
                .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(users)) {
            users.sort(QueueManagementHelper.sortByCaseCount);
            String caseId = caseQueuedEvent.getCaseId();
            RealtimeUserStatus user = users.get(0);
            user.getAssignedCaseIds().add(caseId);
            // Update operator in DB
            caseDistributor.updateOperator(user.getUserId(), new ArrayList<String>() {{
                add(caseId);
            }});
            // Update cro stats
            user.getTodaysCroStatistics().put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                    user.getTodaysCroStatistics().get(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT) + 1);

            // Update CroQueueTracking
            Map<String, Object> croTrackingFields = new HashMap<>();
            croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, user.getAssignedCaseIds());
            croTrackingFields.put(QueueActivityHelper.FIELD_lastCaseAssignment, new Date());
            queueActivityHelper.updateCroQueueTracking(user.getUserId(), user.getInstitutionId(), croTrackingFields);
        }
    }

    /**
     * CRO's actions on a case are as follows :
     * Opening of a case
     * Approving it
     * Declining it
     *
     * @param croActivity
     */
    @Async
    @EventListener
    @Override
    public void actionOnCase(CroActivityEvent croActivity) {
        // Remove case from the queue if user has done and set/unset working caseid for the user
        ActivityLogs activityLog = new ActivityLogs();
        try {
            RealtimeUserStatus userStatus = userStatusCache.getRealtimeUserStatus(croActivity.getUserId(),
                    croActivity.getInstitutionId(), false);

            if (croActivity.isDone()) {
            /* Approval or declining are the decisions for the case.
                Decisioning happens after cro opens the case.  When decision taken, cro is done with case
                and he is not now working with teh case.
                So clear working case id and remove the case id from his list.
                Also update cro stats
            */
                userStatus.setWorkingCaseId(null);
                userStatus.getAssignedCaseIds().remove(croActivity.getCaseId());
                caseDistributor.updateOperator(null,
                        new ArrayList<String>() {{
                            add(croActivity.getCaseId());
                        }});
                // Add this in cro-audit document(table)
                saveAuditData(croActivity, userStatus);

                // Update CRO stats
                Map<String, Long> croStats = userStatus.getTodaysCroStatistics();
                // Queued count
                croStats.put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                        Long.valueOf(userStatus.getAssignedCaseIds().size()));
                // based on decision
                if (croStats.get(croActivity.getDecision()) == null) { // This is his first decision of the day !!
                    croStats.put(croActivity.getDecision(), 1L);
                } else {
                    croStats.put(croActivity.getDecision(),
                            croStats.get(croActivity.getDecision()) + 1);
                }
                userStatusCache.updateCroStats(userStatus.getUserId(), userStatus.getInstitutionId(), croStats);

                logger.debug("User {} finished with case {}; Stats are {} ", croActivity.getUserId(), croActivity.getCaseId(),
                        userStatus.getTodaysCroStatistics());

            } else {
                if (croActivity.isWorking()) { // CRO has opened the case
                    userStatus.setWorkingCaseId(croActivity.getCaseId());

                    queueActivityHelper.logActivity(userStatus.getUserId(), userStatus.getInstitutionId(), new ArrayList<String>() {{
                                add(croActivity.getCaseId());
                            }},
                            GNGWorkflowConstant.CASE_OPENED, GNGWorkflowConstant.CASE_OPENED.toFaceValue());

                    logger.debug("User {} working with case {} ; case working set {}",
                            croActivity.getUserId(), croActivity.getCaseId(), userStatus.getWorkingCaseId());
                } else {
                    // Will this code ever reach ?? No, but just to make foolproof.
                    userStatus.setWorkingCaseId(null);
                }
            }
        } catch (Exception e) {
            logger.error("User {} not found in active cache", croActivity.getUserId());
        }
    }

    @Override
    public List<String> getCases(String loggedInUserId, String institutionId) throws Exception {
        return userStatusCache.getRealtimeUserStatus(loggedInUserId, institutionId, false).getAssignedCaseIds();
    }

    @Override
    public List<String> getNextCases(String loggedInUserId, String institutionId, int skip) throws Exception {
        // Get next batch of cases
        RealtimeUserStatus realtimeUserStatus = RealtimeUserCache.getUserRealtimeStatus(loggedInUserId, institutionId);
        List<String> assignedCases = realtimeUserStatus.getAssignedCaseIds();
        List<String> assignedCasesCopy = new ArrayList<>(assignedCases);

        // Clear currently assigned list
        assignedCases.clear();

        // Assign new cases to him
        caseDistributor.assignCases(realtimeUserStatus, GNGWorkflowConstant.REQUEST_NEXT_BATCH_OF_CASES.toFaceValue(), skip);
        assignedCases = realtimeUserStatus.getAssignedCaseIds();
        if (assignedCases.size() > 0) {
            // Update operator to null for these cases
            caseDistributor.updateOperator(null, assignedCasesCopy);
            // Activity log for unassignment
            queueActivityHelper.logActivity(realtimeUserStatus.getUserId(), realtimeUserStatus.getInstitutionId(),
                    assignedCasesCopy, GNGWorkflowConstant.CASE_UNASSIGNED,
                    GNGWorkflowConstant.REQUEST_NEXT_BATCH_OF_CASES.toFaceValue());
        }
        // Update stat data
        realtimeUserStatus.getTodaysCroStatistics().put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                new Long(assignedCases.size()));
        logger.debug("=========== Cache AFTER usr requesting next batch ( {} )==================", loggedInUserId);
        logger.debug(RealtimeUserCache.log());
        logger.debug("========================================");

        return assignedCases;
    }

    @Override
    public boolean isAssignedRefId(String refId, String userId, String institutionId) throws Exception {
        boolean isAssigned = false;
        try {
            //isAssigned = RealtimeUserCache.getUserRealtimeStatus(userId, institutionId).getAssignedCaseIds().contains(refId);
            isAssigned = userStatusCache.getRealtimeUserStatus(userId, institutionId, false).getAssignedCaseIds().contains(refId);
        } catch (GoNoGoException ge) {
            logger.error(ge.getMessage());
        }
        logger.debug("RefId {} assigned to user {} of institution {} : {}", refId, userId, institutionId, isAssigned);
        return isAssigned;
    }

    private void saveAuditData(CroActivityEvent croActivity, RealtimeUserStatus userStatus) {
        CroAudit croAudit = new CroAudit();
        croAudit.setInstitutionId(userStatus.getInstitutionId());
        croAudit.setUserId(croActivity.getUserId());
        croAudit.setRole(userStatus.getRole().toString());
        croAudit.setRefID(croActivity.getCaseId());
        croAudit.setDecision(croActivity.getDecision());

        queueRepository.saveCroAudit(croAudit);
    }

    private List<String> getCasesToRedistribute(RealtimeUserStatus realtimeUserStatus) throws Exception {
        return QueueManagementHelper.getHalfOfMostAgedCases(realtimeUserStatus);
    }

    /**
     * Assigns cases to users when the assigned cases count is below the set count for per user.
     *
     * @throws Exception
     */
    @Override
    public void assignCases() {
        //  instituteId - role are the variations to be catered for retrieving
        // TODO : Keep the InsttId set in the cache
        List<String> instituteIds = configurationRepository.fetchActiveInstitutionIds();
        Iterator iterator = instituteIds.iterator();

        List<String> roles = Roles.getCroRoles();

        // Iterate over institute id
        while (iterator.hasNext()) {
            final String instituteId = (String) iterator.next();

            // Iterate over roles
            for (final String role : roles) {
                logger.debug("Assigning cases for inst {} and role {} -", instituteId, role);
                /* Get the users of specified insttId and role where case count is less than
                    cases per user. */
                List<RealtimeUserStatus> users = userStatusCache.getOnlineUsers(instituteId).stream()
                        .filter(realtimeUserStatus ->
                                StringUtils.equalsIgnoreCase(realtimeUserStatus.getRole(), role)
                                        && realtimeUserStatus.getAssignedCaseIds().size() < casesPerUser
                        )
                        .collect(Collectors.toList());

                logger.debug("Current assignment - plate not full for users: {}", users);

                if (CollectionUtils.isNotEmpty(users)) {
                    //  Determine count of to be fetched cases = sum of ( casesperUser - assignedCases )
                    int fetchCount = users.stream().mapToInt(p -> (casesPerUser - p.getAssignedCaseIds().size())).sum();
                    try {
                        // Get the no. of cases
                        caseDistributor.assignCases(users, fetchCount);
                    } catch (Exception e) {
                        logger.error("Error while assigning cases to users of role {} of instituteId {} : {}"
                                , role, instituteId, e.getMessage());
                    }
                }
            }  // Loop for Role
        }// Loop for InstId
    }

    @Override
    public void assignCases(CaseAllocationRequest caseAllocationRequest) throws Exception {
        /*
            Case allocation scenarios
            Scenario 1 : Allocate from user1 to user 2
            Scenario 2 : Deallocate from user 1
            Scenario 3 : Allocate to user 2
         */
        String userToAllocate = caseAllocationRequest.getAllocateToUserId();
        String userFromAllocate = caseAllocationRequest.getAllocateFromUserId();
        String institutionId = caseAllocationRequest.getHeader().getInstitutionId();
        List<String> caseIds = caseAllocationRequest.getCaseIds();

        RealtimeUserStatus userStatus = null;
        // Scenario 1, 2 : Deallocate from user 1
        if (userFromAllocate != null) {
            userStatus = userStatusCache.getRealtimeUserStatus(userFromAllocate, institutionId, false);
            userStatus.getAssignedCaseIds().removeAll(caseIds);
            // Update cro stats
            userStatus.getTodaysCroStatistics().put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                    new Long(userStatus.getAssignedCaseIds().size()));
            userStatusCache.updateCroStats(userFromAllocate, institutionId, userStatus.getTodaysCroStatistics());
        }
        // Scenario 1, 3 : Allocate to user 2
        if (userToAllocate != null) {
            userStatus = userStatusCache.getRealtimeUserStatus(userToAllocate, institutionId, false);
            userStatus.getAssignedCaseIds().addAll(caseIds);
            // Update cro stats
            userStatus.getTodaysCroStatistics().put(QueueManagementHelper.STAT_KEY_FOR_QUEUED_COUNT,
                    new Long(userStatus.getAssignedCaseIds().size()));
            userStatusCache.updateCroStats(userFromAllocate, institutionId, userStatus.getTodaysCroStatistics());

            // Update CroQueueTracking
            Map<String, Object> croTrackingFields = new HashMap<>();
            croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, userStatus.getAssignedCaseIds());
            croTrackingFields.put(QueueActivityHelper.FIELD_lastSignalReceived, new Date());
            queueActivityHelper.updateCroQueueTracking(userStatus.getUserId(), userStatus.getInstitutionId(), croTrackingFields);
        }

        // UserToAllocate is null in case of deallocation
        caseDistributor.updateOperator(userToAllocate, caseIds);
    }

    @Override
    @EventListener
    public void setCroStatistics(CroStatsUpdateEvent croStatsUpdateEvent) {
        String userId = croStatsUpdateEvent.getUserId();
        String institutionId = croStatsUpdateEvent.getInstitutionId();
        Map<String, Long> statistics = croStatsUpdateEvent.getCroStats();
        try {
            userStatusCache.updateCroStats(userId, institutionId, statistics);
        } catch (Exception gne) {
            logger.error("Cannot set CRO-statistics for {} of institution {} for following reason \n{}",
                    userId, institutionId, gne.getMessage());
        }
    }

    @Override
    public Map<String, Long> getCroStatistics(CroAuditRequest croAuditRequest) {
        String userId = croAuditRequest.getUserIdToAudit();
        String institutionId = croAuditRequest.getHeader().getInstitutionId();
        try {
            RealtimeUserStatus userStatus = userStatusCache.getRealtimeUserStatus(userId, institutionId, false);
            return userStatus.getTodaysCroStatistics();
        } catch (GoNoGoException gne) {
            logger.error("Cannot get CRO-statistics for {} of institution {} for following reason \n{}",
                    userId, institutionId, gne.getMessage());
            //throw gne;
        }
        return null;
    }

    @Override
    public List<RealtimeUserStatus> getOnlineUsers() {
        List<String> institutionIds = configurationRepository.fetchActiveInstitutionIds();
        List<RealtimeUserStatus> onlineUsers = new ArrayList<>();
        for (String institutionId : institutionIds) {
            onlineUsers.addAll(userStatusCache.getOnlineUsers(institutionId));
        }
        return onlineUsers;
    }

    @Override
    public List<RealtimeUserStatus> getIdleUsers() {
        List<String> institutionIds = configurationRepository.fetchActiveInstitutionIds();
        List<RealtimeUserStatus> idleUsers = new ArrayList<>();
        for (String institutionId : institutionIds) {
            idleUsers.addAll(userStatusCache.getIdleUsers(institutionId));
        }
        return idleUsers;
    }

    @Override
    public List<RealtimeUserStatus> getOnlineUsers(String institutionId, boolean getCopy) {
        return userStatusCache.getOnlineUsers(institutionId, getCopy);
    }

    @Override
    public List<RealtimeUserStatus> getIdleUsers(String institutionId, boolean getCopy) {
        return userStatusCache.getIdleUsers(institutionId, getCopy);
    }

    @Override
    public boolean isUserLoggedIn(String userId, String institutionId) {
        return userStatusCache.isUserLoggedIn(userId, institutionId);
    }

    @Override
    public RealtimeUserStatus getRealtimeUserStatus(String userId, String institutionId) throws GoNoGoException {
        return userStatusCache.getRealtimeUserStatus(userId, institutionId, false);
    }

    @Override
    public void changeAvailability(String userId, String institutionId, boolean availabilityFlag) {
        // TODO : Revisit this logic once requirement is finalized
        // Change his status in db

        // Update CroQueueTracking for this user
        Map<String, Object> croTrackingFields = new HashMap<>();
        croTrackingFields.put(QueueActivityHelper.FIELD_assignedCaseIds, new ArrayList<>());
        croTrackingFields.put(QueueActivityHelper.FIELD_availableFlag, Boolean.valueOf(availabilityFlag));
        queueActivityHelper.updateCroQueueTracking(userId, institutionId, croTrackingFields);

        // If user is now offline nothing to be done
        // For online user , if he is flaged as unavailable then ressign his cases to others
        // TODO : Mark him offline from cache?
        if (availabilityFlag) {
            // User status changed to available - do nothing
        } else { // user status changed to unavailable
            // check whether user is online
            if (isUserLoggedIn(userId, institutionId)) {
                // TODO : should it be made offline??
                registerUserOffline(userId, institutionId);
            } else {
                // user offline - do nothing
            }
        }
    }

    @Override
    public void clearWorkingCaseId(String userId, String institutionId) {
        try {
            RealtimeUserStatus userStatus = userStatusCache.getRealtimeUserStatus(userId, institutionId, false);
            if (userStatus.getWorkingCaseId() != null) {
                userStatus.setWorkingCaseId(null);
            }
        } catch (GoNoGoException ge) {
            logger.error(ge.getMessage());
        }
    }
}
