package com.softcell.gonogo.queue.management;

import org.redisson.api.annotation.REntity;
import org.redisson.api.annotation.RId;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amit.Botre
 * Class to maintain all unassigned application ids against instituteId_roleId
 */
@REntity
public class QueueUnassignedCases {

    @RId
    private String instituteGroupId; //  PENDING_CASES_<institutionId>_<groupName>
    private List<String> unassignedCaseIds = new ArrayList<>();

    public String getInstituteGroupId() {       return instituteGroupId;    }

    public void setUnassignedCaseIds(List<String> assignedCaseIds) {
        this.unassignedCaseIds = assignedCaseIds;
    }

    public void setInstituteGroupId(String instituteGroupId) {
        this.instituteGroupId = instituteGroupId;
    }

    public List<String> getUnassignedCaseIds() {        return unassignedCaseIds;    }

    @Override
    public String toString() {
        return "QueueUnassignedCases{" +
                "instituteGroupId='" + instituteGroupId + '\'' +
                ", unassignedCaseIds=" + unassignedCaseIds +
                '}';
    }
}
