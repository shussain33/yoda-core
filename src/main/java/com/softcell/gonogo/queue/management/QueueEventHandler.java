package com.softcell.gonogo.queue.management;

import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.request.queue.CaseAllocationRequest;
import com.softcell.gonogo.model.request.queue.CroAuditRequest;
import com.softcell.gonogo.model.request.queue.HeartbeatRequest;
import com.softcell.gonogo.queue.management.event.*;

import java.util.List;
import java.util.Map;

/**
 * Created by archana on 12/7/17.
 */
public interface QueueEventHandler {

    void addQueueUnassignedCasesInCache(List<QueueUnassignedCases> QueueUnassignedCasess);

    List<QueueUnassignedCases> getQueueUnassignedCases(List<String> ids);

    // User related events
    void registerUserLogin(LoginEvent loginEvent) throws Exception;

    void registerUserLogout(LogoutEvent logoutEvent);

    void registerUserOffline(String userId, String instituteId);

    void registerUserIdle(String userId, String instituteId) throws Exception;

    void registerHeartbeat(HeartbeatRequest heartbeat) throws Exception;

    // Case assignements

    void caseSubmission(CaseQueuedEvent caseQueuedEvent);

    void actionOnCase(CroActivityEvent croActivity);

    List<String> getCases(String loggedInUserId, String institutionId) throws Exception;

    List<String> getNextCases(String loggedInUserId, String institutionId, int skip) throws Exception;

    boolean isAssignedRefId(String refId, String userId, String institutionId) throws Exception;

    void assignCases();

    void assignCases(CaseAllocationRequest caseAllocationRequest) throws Exception;

    // ---- CroStats
    void setCroStatistics(CroStatsUpdateEvent croStatsUpdateEvent);

    Map<String, Long> getCroStatistics(CroAuditRequest croAuditRequest) throws Exception;


    // -------- Get users list based on status
    List<RealtimeUserStatus> getOnlineUsers();

    List<RealtimeUserStatus> getIdleUsers();

    List<RealtimeUserStatus> getOnlineUsers(String institutionId, boolean getCopy);

    List<RealtimeUserStatus> getIdleUsers(String institutionId, boolean getCopy);

    boolean isUserLoggedIn(String userId, String institutionId);

    RealtimeUserStatus getRealtimeUserStatus(String userId, String institutionId) throws GoNoGoException;

    void changeAvailability(String userId, String institutionId, boolean availabilityFlag);

    void clearWorkingCaseId(String userId, String institutionId);
}