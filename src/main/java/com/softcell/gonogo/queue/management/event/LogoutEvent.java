package com.softcell.gonogo.queue.management.event;

/**
 * Created by archana on 12/7/17.
 */
public class LogoutEvent {
    private String userId;
    private String instituteId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setInstituteId(String instituteId) {
        this.instituteId = instituteId;
    }

    public String getInstituteId() {
        return instituteId;
    }
}
