package com.softcell.gonogo.queue.management;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Has information about the roles which are supposed to
 * work on queued cases. Exposes methods to access those roles.
 * Created by archana on 31/8/17.
 */
public class Roles {

    public enum Role {
        DSA, CRO1, CRO2, CRO3, CRO4,
        CRO, OPS, Sales, QUEUE_ADMIN,
        FOS, CPA, CREDIT_MANAGER;
    };

    private static ArrayList<String> croRoles = new ArrayList<String>()
    {{  add(Role.CRO.name()); add(Role.OPS.name());  add(Role.Sales.name());
        add(Role.CRO1.name()); add(Role.CRO2.name());  add(Role.CRO3.name()); add(Role.CRO4.name());  }};

    public static List<String> getCroRoles() {
        return (List)croRoles.clone();
    }

    public static boolean isCroRole(String role){
        return croRoles.contains(role);
    }

    public static boolean hasCroRole(Set<String> roles){
        // Returns true if the two specified collections have no elements in common.
        return !Collections.disjoint(roles, croRoles);
    }

    public static void retainCroRoles(Set<String> roles){
        roles.retainAll(croRoles);
    }

}
