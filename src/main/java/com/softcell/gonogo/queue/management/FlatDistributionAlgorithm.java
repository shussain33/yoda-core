package com.softcell.gonogo.queue.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Distributes the cases to users.
 * The users are sorted based cases they have. A user having the least number of cases is assigned with the cases to
 * fill his plate count.  * Then the rest of the cases to the remaining users ...
 * <p>
 * Created by archana on 19/7/17.
 */
@Component
public class FlatDistributionAlgorithm implements DistributionAlgorithm {

    @Autowired
    private CaseDistributor caseDistributor;

    @Override
    public List<RealtimeUserStatus> distribute(List<String> caseIds, List<RealtimeUserStatus> onlineUsers, int casesPerUser) {
        List<RealtimeUserStatus> distributedTo = new ArrayList<>();
        List<String> distributionList = new ArrayList<>();
        distributionList.addAll(caseIds);

        onlineUsers.sort(QueueManagementHelper.sortByCaseCount);
        for (RealtimeUserStatus realtimeUserStatus : onlineUsers) {
            int assignedCaseCount = realtimeUserStatus.getAssignedCaseIds().size();
            if (assignedCaseCount < casesPerUser) {
                // allocate no. cases equal to difference between  assignedCaseCount and userCaseCount
                int unassignedCaseCount = casesPerUser - assignedCaseCount;
                int toIndex = unassignedCaseCount;
                if (distributionList.size() > unassignedCaseCount) {

                    // assign to the
                    List<String> assignedIds = realtimeUserStatus.getAssignedCaseIds();
                    // sublist -  fromIndex, inclusive, and toIndex, exclusive.
                    /*  NOTE : The subList becomes invalid when the underlying original list changes.
                                Make a copy of it if original is going to be changed.
                    */
                    List<String> toBeAssignedIds = new ArrayList<String>(distributionList.subList(0, toIndex));
                    assignedIds.addAll(toBeAssignedIds);
                    // Update CROStat
                    QueueManagementHelper.updateCroStats(realtimeUserStatus);
                    // update DB
                    caseDistributor.updateOperator(realtimeUserStatus.getUserId(), toBeAssignedIds);
                    // Remove those from the list
                    distributionList = distributionList.subList(toIndex, distributionList.size());
                    // Add user to distributedTo list
                    distributedTo.add(realtimeUserStatus);
                } else {
                    // Assign all cases to the user
                    realtimeUserStatus.getAssignedCaseIds().addAll(distributionList);
                    // Update CROStat
                    QueueManagementHelper.updateCroStats(realtimeUserStatus);
                    // update DB
                    caseDistributor.updateOperator(realtimeUserStatus.getUserId(), distributionList);
                    // clear list and exit since all ids have been assigned now
                    distributionList.clear();
                    // Add user to distributedTo list
                    distributedTo.add(realtimeUserStatus);
                    break;
                }
            }
        }
        // Remove operator for the cases which are left as unassigned
        if (distributionList.size() != 0) {
            caseDistributor.updateOperator(null, distributionList);
        }
        // Distribution list contains unassigned Ids.
        // Removing those values from caseIds gives us the cases reassigned.
        caseIds.removeAll(distributionList);

        return distributedTo;
    }
}
