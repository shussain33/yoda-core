package com.softcell.gonogo.exceptions;

/**
 * this class is a wrapper over exception
 *
 * @author prateek
 */
public class ReportConfigNotInDb extends Exception {

    public ReportConfigNotInDb(String message) {
        super(message);
    }

}
