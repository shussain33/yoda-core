package com.softcell.gonogo.exceptions;

/**
 * Created by prateek on 19/2/17.
 */
public interface ErrorCode {

    int getCode();
}
