package com.softcell.gonogo.exceptions;

/**
 * Created by anupamad on 11/10/17.
 */
public class TcLosException extends Exception {
    public TcLosException(){
        super();
    }
    public TcLosException(String message){
        super(message);
    }
}

