package com.softcell.gonogo.exceptions.category;

/**
 * Created by archana on 14/8/17.
 */
public class GoNoGoException extends Exception {
    public GoNoGoException(){
        super();
    }
    public GoNoGoException(String message){
        super(message);
    }
}
