package com.softcell.gonogo.exceptions.category;

import com.softcell.gonogo.exceptions.ErrorCode;

/**
 * Created by prateek on 19/2/17.
 */
public enum UserNotFound implements ErrorCode{

    USER_NOT_REGISTERED(1001);

    private final int number ;

    private UserNotFound(int number){
        this.number = number;
    }

    @Override
    public int getCode() {
        return number;
    }
}
