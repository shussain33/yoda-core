package com.softcell.gonogo.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

/*_ USAGES
* create enum implementing ErrorCode
* public enum PaymentCode implements ErrorCode{
*   SERVICE_TIMEOUT(101),
*   AMOUNT_TOO_HIGH(102),
*   INSUFFICIENT_FUND(103);
* }
* throw new SystemException(PaymentCode.SERVICE_TIMEOUT);
* // Dynamic field to exception {for validation}
*  throw new SystemException(ValidationCode.VALUE_TOO_SHORT) .set("field", field) .set("value", value) .set("min-length", MIN_LENGTH);
*/

/**
 * class to handle exception throughout application based on error codes
 *
 * Created by prateek on 19/2/17.
 */
public class SystemException extends RuntimeException{

    private ErrorCode errorCode;

    private final Map<String,Object> properties = new TreeMap<String,Object>();



    public SystemException(ErrorCode errorCode){
        this.errorCode = errorCode;
    }

    public SystemException (String message){
        super(message);
    }

    public SystemException(String message , ErrorCode errorCode){
        super(message);
        this.errorCode =errorCode ;
    }

    public SystemException(Throwable throwable, ErrorCode errorCode){
        super(throwable);
        this.errorCode = errorCode;
    }

    public SystemException(String message, Throwable cause, ErrorCode errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode(){
        return errorCode;
    }

    public SystemException setErrorCode(ErrorCode errorCode){
        this.errorCode = errorCode;
        return this;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public <T> T get(String name){
        return  (T)properties.get(name);
    }

    public SystemException set(String name , Object value){
        properties.put(name, value);
        return this;
    }

    public void printStackTrace(PrintStream printStream){
        synchronized (printStream){
            printStackTrace(new PrintWriter(printStream));
        }
    }

    public void printStackTrace(PrintWriter printWriter){
        synchronized (printWriter){

            printWriter.println(this);

            printWriter.println("\t-------------------------------");

            if (errorCode != null) {
                printWriter.println("\t" + errorCode + ":" + errorCode.getClass().getName());
            }

            for (String key : properties.keySet()) {
                printWriter.println("\t" + key + "=[" + properties.get(key) + "]");
            }

            printWriter.println("\t-------------------------------");

            StackTraceElement[] trace = getStackTrace();

            for (int i=0; i < trace.length; i++)
                printWriter.println("\tat " + trace[i]);

            Throwable ourCause = getCause();
            if (ourCause != null) {
                ourCause.printStackTrace(printWriter);
            }
            printWriter.flush();
        }
    }

    public static  SystemException wrap(Throwable exception, ErrorCode errorCode){
        if(exception instanceof  SystemException){
            SystemException systemException = (SystemException)exception;

            if(null != errorCode && errorCode != systemException.getErrorCode()){
                return new SystemException(exception.getMessage(), exception, errorCode);
            }

            return systemException;

        }else{
            return new SystemException(exception.getMessage(), exception, errorCode);
        }
    }

    public static SystemException wrap(Throwable exception) {
        return wrap(exception, null);
    }


}
