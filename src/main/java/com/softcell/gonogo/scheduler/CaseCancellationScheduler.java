package com.softcell.gonogo.scheduler;

import com.softcell.constants.CacheName;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.core.CancelDoNotRaisedApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.service.AdminManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/**
 * Created by mahesh on 7/12/17.
 * <p>
 * This class is used to run scheduled job every day on specific time which is provided in the configuration
 * to cancel cases in the application.
 */

@Component
@EnableScheduling
public class CaseCancellationScheduler {

    private static final Logger logger = LoggerFactory.getLogger(CaseCancellationScheduler.class);

    @Autowired
    private AdminManager adminManager;

    @Autowired
    private LookupService lookupService;

    @Value("${gonogo.institution.hdbfs}")
    private String institutionId;


    //@Scheduled(cron = "${gonogo.institution.hdbfs.cron}")
    public void runCaseCancellationJob() {

        logger.info("Inside Run Case cancellation Job");

        try {
            CancelDoNotRaisedApplicationRequest caseCancellationRequest = getCaseCancellationRequest();

            if (null != caseCancellationRequest) {
                adminManager.cancelDoNotRaisedApplication(caseCancellationRequest);
            }
        } catch (Exception e) {
            logger.error("problem occur at the time of running CaseCancellation job for {} institution  with probable cause {}", institutionId, e.getMessage());
            logger.error("{}",e.getStackTrace());
        }

    }

    private CancelDoNotRaisedApplicationRequest getCaseCancellationRequest() {

        // get case cancellation job configuration from database against the institution Id
        CaseCancellationJobConfig caseCancellationJobConfig = lookupService.getCaseCancellationJobConfig(this.institutionId, CacheName.CASE_CANCEL_CONFIG.name());

        if (null != caseCancellationJobConfig) {
            logger.info("CaseCancellation Job Configuration found successfully for {} institution", institutionId);
            return buildCaseCancellationRequest(caseCancellationJobConfig);
        } else {
            logger.error("CaseCancellation Job Configuration not found for {} institution", institutionId);
            return null;
        }

    }

    private CancelDoNotRaisedApplicationRequest buildCaseCancellationRequest(CaseCancellationJobConfig caseCancellationJobConfig) {

        CancelDoNotRaisedApplicationRequest cancelCancellationRequest = new CancelDoNotRaisedApplicationRequest();

        // build header
        Header header = new Header();
        header.setInstitutionId(caseCancellationJobConfig.getInstitutionId());

        // build case cancellation request
        cancelCancellationRequest.setHeader(header);
        cancelCancellationRequest.setAppStatus(caseCancellationJobConfig.getAppStatus());
        cancelCancellationRequest.setAppStage(caseCancellationJobConfig.getAppStage());
        cancelCancellationRequest.setPastNoOfDays(caseCancellationJobConfig.getPastNoOfDays());
        cancelCancellationRequest.setStartDate(caseCancellationJobConfig.getStartDate());
        cancelCancellationRequest.setEndDate(caseCancellationJobConfig.getEndDate());
        cancelCancellationRequest.setSkip(caseCancellationJobConfig.getSkip());
        cancelCancellationRequest.setLimit(caseCancellationJobConfig.getLimit());
        cancelCancellationRequest.setBatchCount(caseCancellationJobConfig.getBatchCount());

        return cancelCancellationRequest;

    }

}
