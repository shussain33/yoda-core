/**
 * yogeshb6:05:33 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.scheduler;

/**
 * @author yogeshb
 *
 */
public interface ISchedulerService {
    public void executeDailyTask() throws Exception;

    public void executeWeeklyTask();

    public void executeMonthlyTask();
}
