/**
 * yogeshb6:19:18 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yogeshb
 */
public class RunMonthlyTask {

    private static final Logger logger = LoggerFactory.getLogger(RunMonthlyTask.class);

    public void sendMailWithCreditReport() {
        logger.info("MailWithMonthyCreditReport");
    }
}
