/**
 * yogeshb6:21:48 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.scheduler;

/**
 * @author yogeshb
 *
 */
public class SchedulerService implements ISchedulerService {

    private RunDailyTask runDailyTask;
    private RunWeeklyTask runWeeklyTask;
    private RunMonthlyTask runMonthlyTask;

    public RunDailyTask getRunDailyTask() {
        return runDailyTask;
    }

    public void setRunDailyTask(RunDailyTask runDailyTask) {
        this.runDailyTask = runDailyTask;
    }

    public RunWeeklyTask getRunWeeklyTask() {
        return runWeeklyTask;
    }

    public void setRunWeeklyTask(RunWeeklyTask runWeeklyTask) {
        this.runWeeklyTask = runWeeklyTask;
    }

    public RunMonthlyTask getRunMonthlyTask() {
        return runMonthlyTask;
    }

    public void setRunMonthlyTask(RunMonthlyTask runMonthlyTask) {
        this.runMonthlyTask = runMonthlyTask;
    }


    /* (non-Javadoc)
     * @see com.softcell.gonogo.scheduler.ISchedulareService#executeDailyTask()
     */
    @Override
    public void executeDailyTask() throws Exception {
        getRunDailyTask().sendMailWithCreditReport();

    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.scheduler.ISchedulareService#executeWeeklyTask()
     */
    @Override
    public void executeWeeklyTask() {
        getRunWeeklyTask().sendMailWithCreditReport();
    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.scheduler.ISchedulareService#executeMonthlyTask()
     */
    @Override
    public void executeMonthlyTask() {
        getRunMonthlyTask().sendMailWithCreditReport();
    }

}
