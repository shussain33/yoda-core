/**
 * yogeshb6:02:17 pm  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yogeshb
 */
public class RunWeeklyTask {

    private static final Logger logger = LoggerFactory.getLogger(RunWeeklyTask.class);

    public void sendMailWithCreditReport() {
        logger.info("MailWithWeeklyCreditReport");
    }
}
