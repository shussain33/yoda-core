package com.softcell.gonogo.scheduler;


import com.softcell.config.EmailServiceConfiguration;
import com.softcell.config.ReportEmailConfiguration;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.AttachementDOMailRequest;
import com.softcell.gonogo.model.core.scoring.response.Header;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.security.MessagingServiceResponse;
import com.softcell.reporting.report.HDBFSReport;
import com.softcell.reporting.report.SalesReport;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author yogeshb
 */
@Component("runDailyTask")
public class RunDailyTask {

    private static final Logger logger = LoggerFactory.getLogger(RunDailyTask.class);

    private static HDBFSReport hdbfsReport;

    static {
        hdbfsReport = new HDBFSReport();
    }

    /**
     * This method call Quartz scheduler to generate daily report.
     */
    public void sendMailWithCreditReport() throws Exception {

        logger.info("Sending credit report in mail job begun....");

        List<ReportingModuleConfiguration> reportingModuleConfigurationList = hdbfsReport
                .getReportingModuleConfiguration();

        try {

            if (reportingModuleConfigurationList != null
                    && !reportingModuleConfigurationList.isEmpty()) {

                for (ReportingModuleConfiguration reportingModuleConfiguration : reportingModuleConfigurationList) {

                    byte[] zipBytes = null;

                    Date[] fromToDate = GngDateUtil
                            .getFromToDate(reportingModuleConfiguration);

                    Query gonogoCustomerQuery = QueryBuilder
                            .buildGoNoGoApplicationQuery(
                                    reportingModuleConfiguration
                                            .getInstitutionID(), fromToDate[0],
                                    fromToDate[1], reportingModuleConfiguration
                                            .getProductType());
                    Query applicationRequestQuery = QueryBuilder
                            .applicationRequestQuery(
                                    reportingModuleConfiguration
                                            .getInstitutionID(), fromToDate[0],
                                    fromToDate[1], reportingModuleConfiguration
                                            .getProductType());
                    boolean status = false;

                    if (StringUtils.endsWithIgnoreCase("Credit_Report",
                            reportingModuleConfiguration.getReportType())) {


                        zipBytes = hdbfsReport.getCreditReport(
                                reportingModuleConfiguration);


                        status = sendMail(
                                reportingModuleConfiguration.getReportType(),
                                reportingModuleConfiguration.getProductType(),
                                reportingModuleConfiguration.getInstitutionID(),
                                reportingModuleConfiguration.getFormat(),
                                zipBytes);
                    }

                    if (StringUtils.endsWithIgnoreCase("Sales_Report",
                            reportingModuleConfiguration.getReportType())) {
                        SalesReport salesReport = new SalesReport();


                        zipBytes = salesReport.getSaleReport(
                                gonogoCustomerQuery, applicationRequestQuery);

                        status = sendMail(
                                reportingModuleConfiguration.getReportType(),
                                reportingModuleConfiguration.getProductType(),
                                reportingModuleConfiguration.getInstitutionID(),
                                reportingModuleConfiguration.getFormat(),
                                zipBytes);
                    }

                    if (status) {
                        logger.info("Daily Report Send Successfully.");
                    } else {
                        logger.error("Daily Report Send Failed.");
                    }
                }

            } else {
                logger.info("Configuration not found");
            }
        } catch (IOException e) {
            logger.error("Error while executing DailyCreditReport ", e);
            throw new SystemException(e.getMessage());
        }catch(Exception e ){
            logger.error("Error while executing DailyCreditReport ", e);
            throw new SystemException(e.getMessage());
        }finally{
            logger.info("DailyCreditReport job has finished ..");
        }
    }

    /**
     * This method set pojo data for mail service.
     *
     * @param reportType
     * @param institutionID
     * @param format
     * @param zipBytes
     * @return
     */
    private boolean sendMail(String reportType, String productType,
                             String institutionID, String format, byte[] zipBytes) throws Exception {
        Set<String> recipentList = new HashSet<>();
        Set<String> ccList = new HashSet<>();
        /**
         * This method fetch email app from database.
         */
        List<ReportEmailConfiguration> reportEmailConfigurationList = hdbfsReport
                .getReportEmailConfiguration(reportType, productType,
                        institutionID);

        if (reportEmailConfigurationList != null) {
            for (ReportEmailConfiguration reportEmailConfiguration : reportEmailConfigurationList) {
                if ("TO".equalsIgnoreCase(reportEmailConfiguration
                        .getRecipientType())) {
                    recipentList.add(reportEmailConfiguration.getEmail());
                } else if ("CC".equalsIgnoreCase(reportEmailConfiguration
                        .getRecipientType())) {
                    ccList.add(reportEmailConfiguration.getEmail());
                } else {
                    logger.warn("Unknown recipient or email ID not valid ==> "
                            + reportEmailConfiguration.getEmail());
                }
            }
        } else {
            logger.warn("Not available in email list");
            return false;
        }

        AttachementDOMailRequest attachementDOMailRequest = new AttachementDOMailRequest();
        Header header = new Header();
        header.setInstitutionId(institutionID);
        attachementDOMailRequest.setHeader(header);
        attachementDOMailRequest.setAttachedDoPdf(zipBytes);

        attachementDOMailRequest.setContentType("application/zip");

        String to[] = recipentList.toArray(new String[recipentList.size()]);
        attachementDOMailRequest.setRecipients(to);

        String cc[] = ccList.toArray(new String[ccList.size()]);
        attachementDOMailRequest.setMailToCC(cc);

        attachementDOMailRequest.setRefID("Credit_Report_"
                + GngDateUtil.getDdMmYyyy(new DateTime()) + ".zip");
        attachementDOMailRequest.setSubject("Credit Report");
        attachementDOMailRequest
                .setContent("Dear All,\n Please find Attachement ");

        EmailServiceConfiguration config = Cache.URL_CONFIGURATION.getEmailServiceConfiguration().get(header.getInstitutionId());

        boolean status = false;

        MessagingServiceResponse smsServiceResponse = (MessagingServiceResponse) TransportUtils.
                postJsonRequest(attachementDOMailRequest, config.getEmailUrl(),
                        MessagingServiceResponse.class);

        if(smsServiceResponse != null && smsServiceResponse.getStatus().equals("OK")){
            status = true;
        }

        return status;

    }
}
