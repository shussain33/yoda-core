package com.softcell.gonogo.scheduler;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author yogeshb
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class RunCreditReportJob extends QuartzJobBean {

    private static final Logger logger = LoggerFactory.getLogger(RunCreditReportJob.class);

    @Autowired
    private RunDailyTask runDailyTask;

    /**
     *
     * @param context
     * @throws JobExecutionException
     */
    @Override
    protected void executeInternal(JobExecutionContext context)
            throws JobExecutionException {

        logger.info("Job ** {} ** fired @ {}",context.getJobDetail().getKey().getName(), context.getFireTime());

        try {
            runDailyTask.sendMailWithCreditReport();
        } catch (Exception e) {
           throw new JobExecutionException(e.getMessage());
        }

        logger.info("Next Job scheduled @ {}", context.getNextFireTime());

    }


    public void setRunDailyTask(RunDailyTask runDailyTask) {
        this.runDailyTask = runDailyTask;
    }

}
