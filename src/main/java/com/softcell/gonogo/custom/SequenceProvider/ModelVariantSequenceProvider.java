package com.softcell.gonogo.custom.SequenceProvider;

import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.master.ModelVariantMasterRequest;
import org.apache.commons.codec.binary.StringUtils;
import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinod on 5/4/17.
 */
public class ModelVariantSequenceProvider implements DefaultGroupSequenceProvider<ModelVariantMasterRequest>{
    @Override
    public List<Class<?>> getValidationGroups(ModelVariantMasterRequest mvmr) {
        List<Class<?>> sequence = new ArrayList<>();

        sequence.add(Header.FetchGrp.class);
        sequence.add(ModelVariantMasterRequest.class);

        if (null != mvmr) {
            if (StringUtils.equals(ModelVariantMasterRequest.RequestType.MODEL.name(), mvmr.getRequestType())) {
                sequence.add(ModelVariantMasterRequest.ModFetchGrp.class);
            }

            if (StringUtils.equals(ModelVariantMasterRequest.RequestType.VARIANT.name(), mvmr.getRequestType())) {
                sequence.add(ModelVariantMasterRequest.VarFetchGrp.class);
            }
        }
        return sequence;
    }
}
