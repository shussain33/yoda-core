package com.softcell.gonogo.cache;

/*
 * This Class Is created to set token which comes in
 * request Header and Used any were throught
 * out the application
 */
public final class TokenCache {

    private TokenCache(){}
    public static final InheritableThreadLocal<String> INHERITABLE_THREAD_LOCAL_CACHE = new InheritableThreadLocal<>();
}
