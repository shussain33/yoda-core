/**
 *
 */
package com.softcell.gonogo.cache;

import com.itextpdf.text.Image;
import com.softcell.config.*;
import com.softcell.config.casehistory.StageConfiguration;
import com.softcell.config.casehistory.StageProperties;
import com.softcell.config.casehistory.StepProperties;
import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.sms.GngSmsServiceConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.config.templates.TemplateName;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.ConfigCache;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ComponentConfigurationMongoRepository;
import com.softcell.dao.mongodb.repository.ComponentConfigurationRepository;
import com.softcell.dao.mongodb.utils.MongoRepositoryUtil;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.Stages;
import com.softcell.gonogo.model.MasterSchedulerConfiguration;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.admin.InstitutionConfig;
import com.softcell.gonogo.model.configuration.admin.IntimationConfig;
import com.softcell.gonogo.model.configuration.admin.ProductConfig;
import com.softcell.gonogo.model.configuration.admin.RoleConfig;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.LegalVerification;
import com.softcell.gonogo.model.core.PersonalDiscussion;
import com.softcell.gonogo.model.core.UploadFileDetails;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.verification.PropertyVisit;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.logger.EmailRequestRegistry;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMaster;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.queuemanager.config.EsConnectionConfig;
import com.softcell.queuemanager.connection.pool.ESConnectionPool;
import com.softcell.queuemanager.connection.pool.ElasticSearchConnectionPool;
import com.softcell.utils.aadharpidblock.AuthAUADataCreator;
import com.softcell.utils.aadharpidblock.Encrypter;
import com.softcell.workflow.component.ComponentSetting;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author kishorp
 *         <p/>
 *         <pre>
 *                        A <em> Cache </em> This will maintained all application level
 *                        cache and meta data cache.
 *                 </pre>
 */
@Component
public class Cache {
    /**
     * Logger
     */
    public static final Logger logger = LoggerFactory.getLogger(Cache.class);

    private static final Map<String,String> SERIAL_NUMBER_VENDOR_CODE_BY_INSTITUTION = new HashMap<>();


    public static AuthAUADataCreator auaDataCreator = null;

    public static Map<String, StageConfiguration> stageConfigurationMap = new HashMap<String, StageConfiguration>();

    public static Map<String, FileUploadRequest> LOGO_FILE_CONFIG_REQUEST = new HashMap<String, FileUploadRequest>();

    /**
     * A <em> LOGOIPA</em> will used to generate DO LOGO of HDBFS. Will read
     * from source folder of
     */
    public static Image LOGOIPA;

    /**
     * <em>NUMBER_OF_RE_TRY<em> will  defined number of re-try for re-initiate
     * and reappraisal. it's intitution level flag.
     */
    public static HashMap<String, ReAppraisalConfig> NUMBER_OF_RE_TRY = new HashMap<>();

    /**
     * <pre>
     * <em> WEB_RESPONSE_INCLUDE_FIELD_MAP</em> this web  field map will use to generate
     * dynamics query to get projection fields. This will be output of query  result
     * </pre>
     */
    public static Map<String, List<String>> WEB_RESPONSE_INCLUDE_FIELD_MAP = new HashMap<String, List<String>>();

    public static Map<String, EmailRequestRegistry> EMAIL_REGISTRY = Collections
            .synchronizedMap(new HashMap<>());

    /**
     * Component Setting map is used to store component settings
     */
    public static Map<String, ComponentSetting> COMPONENT_SETTING_MAP = new HashMap<String, ComponentSetting>();
    //public static Map<String, MultiBreComponentConfiguration> MULTIBRE_COMPONENT_SETTING_MAP = new HashMap<String, MultiBreComponentConfiguration>();

    public static Map<String,Map<String,MultiBreComponentConfiguration>> MULTIBRE_COMPONENT_SETTING_MAP=new HashMap<>();
    /**
     * <pre>
     * Work flow will be defined and an initialized for application cache.
     * </pre>
     */
    public static Map<CacheConstant, ApplicationContext> COMPONENT_FACTORY_BEAN = new HashMap<CacheConstant, ApplicationContext>();

    public static Map<Integer, ModuleOutcome> APPLICATION_SCORING_BUCKET = new HashMap<Integer, ModuleOutcome>();

    /**
     * <pre>
     * URL_CONFIGURATION is used to configured all sub module URL for connectivity.
     * </pre>
     */
    public static URLConfiguration URL_CONFIGURATION;

    /**
     * <pre>
     * A <em > NEGATIVE_PINCODESET </em> is used to validate pincode cam.
     * </pre>
     */
    public static Map<String, HashSet<String>> NEGATIVE_PINCODESET = new TreeMap<String, HashSet<String>>();

    public static Set<String> DEDUPE_PRODUCT_SET = new HashSet<String>();

    public static Set<String> NTC_EXCLUDED_PRODUCT_SET = new HashSet<String>();

    public static ConfigCache<String, List<ComponentConfiguration>> configCache = new ConfigCache<String, List<ComponentConfiguration>>(
            50);

    private static MongoTemplate mongoTemplate = MongoConfig.getMongoTemplate();

    /**
     * <pre>
     * A <em>MONGO_TAMPLATE</em> will  used to connect data base.
     * </pre>
     */
    private static Map<String, EMailContentConfiguration> EMAIL_CONTENT_CONFIGURATION = new HashMap<>();

    private static Map<String, Map<String, Map<ActionName, ActionConfiguration>>> ACTION_CONFIGURATION_MAP = null;

    /**
     * TEMPLATE_CONFIGURATION_MAP use for check particular digitize document has
     * access to institution.
     */
    private static Map<String, Map<String, Map<TemplateName, TemplateConfiguration>>> TEMPLATE_CONFIGURATION_MAP = null;

    /**
     * EMAIL_CONFIGURATION_MAP use to check email app against institution and product.
     */
    private static Map<String, Map<String, EmailConfiguration>> EMAIL_CONFIGURATION_MAP = null;

    /**
     * TEMPLATE_CONFIGURATION_MAP use for check particular digitize document has
     * access to institution.
     */
    private static Map<String, Map<String, String>> SMS_CONFIGURATION_MAP = new HashMap<String, Map<String, String>>();

    private static ElasticSearchConnectionPool<Client> ELASTIC_SEARCH_CONNECTION_POOL;

    private static ApplicationContext urlConfiguration;

    private static Map<String, String> SERIAL_NUMBER_VENDOR_BY_INSTITUTION = new HashMap<String, String>();

    private static Set<String> serialNumberManufacturerNames = new HashSet<>();

    /**
     * for digital product vendor selections.
     */
    private static Set<String> dplManufacturerNames = new HashSet<>();

    /**
     * used to initialize product name (for digital product purpose)
     */
    private static Map<String , Map<String,String>> PRODUCT_CONFIGURATION_MAP = null;

    /**
     * used to store secured loan product.
     */
    public static Set<String> securedLoanProduct = new HashSet<>();

    public static Map<String ,Double> LOYALTY_CARD_PRICE_MAP = null;

    /**
     * used to store images type for dms push purpose.
     */
    public static Set<String> SOURCING_IMAGES_SET = null;

    public static Set<String> UNDERWRITER_IMAGES_SET = null;

    public static Set<String> DISBURSAL_IMAGES_SET = null;

    public static Map<String, String> institutionNameMap = null;

    // set Queue management unable disable flag for InstitutionId
    public static Map<String, Boolean> INSTITUTION_QUEUE_CONFIG = null;

    public static Map<String, List<String>> ROLE_ENABLED_STAGES_MAP = null;

    public static Map<String, Map<String,  Map<String, List<String>>>> INST_PROD_ROLE_CONFIG = null;

    public static Map<String, List<Object>> VERIFICATION_ROLE_URL_MAP;

    public static Map<String, String> CRITERIA_FIELD_MAPPING_MAP;

    public static Map<String, Map<String, IntimationConfig>> INSTITUTION_PRODUCT_WISE_INTIMATION_MAP;

    public static Map<String, Map<String, Map<String, Branch>>> BRANCH_ID_OBJECT_MAP;

    public static Map<String, Map<String, Map<String, Branch>>> BRANCH_NAME_OBJECT_MAP;

    public static Map<String, Map<String, String>> INSTITUTION_WISE_USER_ID_NAME_MAP;

    public static Map<String,MasterSchedulerConfiguration>  masterSchedularConfigurationMap = new HashMap();

    public static Map<String, Boolean> INST_SECONDARY_DB = null;

    public static Map<String, Boolean> INST_SENT_OTP;

    /**
     * Initialize all setting of cache service
     */
    static {
        init();
    }

    /**
     * @return
     */
    public static boolean serviceCallForRefreshCache() throws Exception {

        logger.debug("serviceCallForRefreshCache is started");

        try {

            init();

            return true;

        } catch (Exception e) {

            logger.error("Cache Refresh Error : {}",e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    /**
     *
     */
    public static void initMetaConfig() {
        /*initActions();
        initTemplateConfiguration();
        initEmailConfiguration();*/
    }

    /**
     *
     */
    private static void init() {

        logger.debug("Cache.init is started");

        initComponentSetting();
        initMultiBreComponentSetting();
        initBeanFactory();
        initIncludeField();
        initApplicationScoringBucket();
        initUrlConfiguration();
        initNegativePinCode();
        initBureauStateMap();
        initProductSet();
        initReAppraisalConfig();
        //initSerialNumberManufacturerNames();
        //initDplManufacturerNames();
        initCaseHistoryStageConfigurationMap();
        initVendorByInstitution();
        initVendorCodeByInstitution();
        initLogoConfig();
        /**
         * It will deprecated after db app.
         */
        initEmailContentConfig();
        //initActions();
        //initTemplateConfiguration();
        //initEmailConfiguration();
        initSmsConfigurations();
        //initProductConfiguration();
        initAadharCertificate();
        initSecuredLoanProduct();
        //initLoyaltyCardConfiguration();
        initSourcingImagesSet();
        initUnderWritingImagesSet();
        initDisbursalImageSet();
        initInstitutionNameMap();
        initInstitutionQueueConfigMap();
        initRoleEnabledStagMap();
        initVerificationRoleUrlMap();
        initCriteriaFieldMappingMap();
        initOtpStatus();
        //initInstitutionProductWiseIntimationMap();
        //initBranchIdObjectMap();
        //initBranchNameObjectMap();
        //initInstitutionWiseUserIdNameMap();
        initMasterSchedulingConfig();
        initSecondaryDbEnabledMap();
    }


    private static void initMasterSchedulingConfig() {

        Query query = new Query();

        query.addCriteria(Criteria.where("active").is(true).and("masterName").is("DEFAULT"));


        List<MasterSchedulerConfiguration> masterSchedulerConfigurations = mongoTemplate.find(query, MasterSchedulerConfiguration.class);

        masterSchedulerConfigurations.forEach(masterSchedularConfiguration -> {

            masterSchedularConfigurationMap.put(masterSchedularConfiguration.getInstitutionId(),masterSchedularConfiguration);

        });
    }

    public static String getUserName(String institutionId, String userId){
        if(INSTITUTION_WISE_USER_ID_NAME_MAP == null){
            Cache.initInstitutionWiseUserIdNameMap();
        } else if(INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId) == null ||
                INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId).containsKey(userId)) {
            Cache.initInstitutionWiseUserIdNameMap(institutionId, userId);
        }
        if(INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId) != null &&
                INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId).containsKey(userId))
            return INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId).get(userId);
        return userId;
    }


    public static void initInstitutionWiseUserIdNameMap() {
        INSTITUTION_WISE_USER_ID_NAME_MAP = new HashMap<>();
        List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List =
                MongoRepositoryUtil.getOrganisationalHierarchyMaster(mongoTemplate);
        createUserIdNameMap(organizationalHierarchyMasterV2List);
    }

    public static void initInstitutionWiseUserIdNameMap(String institutionId, String userId) {
        if(INSTITUTION_WISE_USER_ID_NAME_MAP == null || INSTITUTION_WISE_USER_ID_NAME_MAP.isEmpty())
            INSTITUTION_WISE_USER_ID_NAME_MAP = new HashMap<>();
        OrganizationalHierarchyMasterV2 organisationalHierarchyMaster =
                MongoRepositoryUtil.getOrganisationalHierarchyMaster(mongoTemplate, institutionId, userId);

        if(organisationalHierarchyMaster != null) {
            List<OrganizationalHierarchyMasterV2> tempList = new ArrayList<>();
            tempList.add(organisationalHierarchyMaster);
            createUserIdNameMap(tempList);
        }
    }

    private static void createUserIdNameMap(List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List) {
        organizationalHierarchyMasterV2List.forEach(master -> {
            String institutionId = master.getInstitutionId();
            if(!INSTITUTION_WISE_USER_ID_NAME_MAP.containsKey(institutionId))
                INSTITUTION_WISE_USER_ID_NAME_MAP.put(institutionId, new HashMap<>());
            master.getRoleWiseUsersList().forEach(roleWiseUsers -> {
                roleWiseUsers.getUsers().forEach(user -> {
                    INSTITUTION_WISE_USER_ID_NAME_MAP.get(institutionId).put(user.getUserId(), user.getUserName());
                });
            });
        });
    }

    public static void initBranchIdObjectMap(){
        BRANCH_ID_OBJECT_MAP = new HashMap<>();
        List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List =
           MongoRepositoryUtil.getOrganisationalHierarchyMaster(mongoTemplate, true);

        organizationalHierarchyMasterV2List.forEach(master -> {
            String institutionId = master.getInstitutionId();
            String product = master.getProduct();
            if(!BRANCH_ID_OBJECT_MAP.containsKey(institutionId))
                BRANCH_ID_OBJECT_MAP.put(institutionId, new HashMap<>());
            if(!BRANCH_ID_OBJECT_MAP.get(institutionId).containsKey(product))
                BRANCH_ID_OBJECT_MAP.get(institutionId).put(product, new HashMap<>());
            if(master.getBranch() != null)
                BRANCH_ID_OBJECT_MAP.get(institutionId).get(product).
                        put(master.getBranch().getBranchId().toString(), master.getBranch());
        });
    }

    public static void initBranchNameObjectMap(){
        BRANCH_NAME_OBJECT_MAP = new HashMap<>();
        List<OrganizationalHierarchyMasterV2> organizationalHierarchyMasterV2List =
                MongoRepositoryUtil.getOrganisationalHierarchyMaster(mongoTemplate, true);

        organizationalHierarchyMasterV2List.forEach(master -> {
            String institutionId = master.getInstitutionId();
            String product = master.getProduct();
            if(!BRANCH_NAME_OBJECT_MAP.containsKey(institutionId))
                BRANCH_NAME_OBJECT_MAP.put(institutionId, new HashMap<>());
            if(!BRANCH_NAME_OBJECT_MAP.get(institutionId).containsKey(product))
                BRANCH_NAME_OBJECT_MAP.get(institutionId).put(product, new HashMap<>());
            if(master.getBranch() != null)
                BRANCH_NAME_OBJECT_MAP.get(institutionId).get(product).
                        put(master.getBranch().getBranchName(), master.getBranch());
        });
    }

    /*public static void initInstitutionProductWiseIntimationMap() {
        INSTITUTION_PRODUCT_WISE_INTIMATION_MAP = new HashMap<>();
        Map<String, IntimationConfig> productWiseIntimationConfigMap;
        List<InstitutionConfig> institutionConfigList = MongoRepositoryUtil.getInstitutionConfig(mongoTemplate);
        for (InstitutionConfig institutionConfig : institutionConfigList) {
            String institutionId = institutionConfig.getInstitutionId();
            productWiseIntimationConfigMap = new HashMap<>();
            INSTITUTION_PRODUCT_WISE_INTIMATION_MAP.put(institutionId, productWiseIntimationConfigMap);
            for(ProductConfig productConfig : institutionConfig.getProductConfigs()){
                String productName = productConfig.getProductName();
                if(productConfig.getIntimationConfig() != null){
                    productWiseIntimationConfigMap.put(productName, productConfig.getIntimationConfig());
                }
            }
        }
    }*/

    private static void initCriteriaFieldMappingMap() {
        CRITERIA_FIELD_MAPPING_MAP = new HashMap<>();
        List<CriteriaMapping> criteriaMappings = MongoRepositoryUtil.getAllCriteriaMapping(mongoTemplate);
        if(!CollectionUtils.isEmpty(criteriaMappings)) {
            for(CriteriaMapping criteriaMapping : criteriaMappings) {
                CRITERIA_FIELD_MAPPING_MAP.put(criteriaMapping.getHierarchyLevel(), criteriaMapping.getFieldMapping());
            }
        }
    }

    private static  void initVerificationRoleUrlMap(){

        VERIFICATION_ROLE_URL_MAP = new HashMap<>();
        List<Object> objList = new ArrayList<>();
        objList.add(VerificationDetails.class);
        objList.add("residenceVerification.agencyCode");
        VERIFICATION_ROLE_URL_MAP.put(Roles.Role.RESI_VERIFIER.name(), objList);

        objList = new ArrayList<>();
        objList.add(VerificationDetails.class);
        objList.add("officeVerification.agencyCode");
        VERIFICATION_ROLE_URL_MAP.put(Roles.Role.OFC_VERIFIER.name(), objList);

        objList = new ArrayList<>();
        objList.add(VerificationDetails.class);
        objList.add("itrVerification.agencyCode");
        VERIFICATION_ROLE_URL_MAP.put(Roles.Role.ITR_VERIFIER.name(), objList);

        objList = new ArrayList<>();
        objList.add(VerificationDetails.class);
        objList.add("bankingVerification.agencyCode");
        VERIFICATION_ROLE_URL_MAP.put(Roles.Role.BANK_VERIFIER.name(), objList);

        objList = new ArrayList<>();
        objList.add(LegalVerification.class);
        objList.add("legalVerificationDetailsList.agencyCode");
        VERIFICATION_ROLE_URL_MAP.put(Roles.Role.LEGAL_VERIFIER.name(), objList);

        objList = new ArrayList<>();
        objList.add(Valuation.class);
        objList.add("valuationDetailsList.agencyCode");
        VERIFICATION_ROLE_URL_MAP.put(Roles.Role.PROPERTY_VERIFIER.name(), objList);
    }

    public static void initRoleEnabledStagMap() {
        ROLE_ENABLED_STAGES_MAP = new HashMap<>();
        List<String> stages = new ArrayList<>();
        stages.add(GNGWorkflowConstant.DE.name());
        stages.add(GNGWorkflowConstant.CR_Q.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.FOS.name(), stages);
        stages = new ArrayList<>();
        stages.add(GNGWorkflowConstant.DDE.name());
        stages.add(GNGWorkflowConstant.CR_Q.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.CPA.name(), stages);
        stages = new ArrayList<>();
        stages.add(GNGWorkflowConstant.CRDT.name());
        stages.add(GNGWorkflowConstant.CR_H.name());
        stages.add(GNGWorkflowConstant.APRV.name());
        stages.add(GNGWorkflowConstant.SUBJ_APRV.name());
        stages.add(GNGWorkflowConstant.DCLN.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.CREDIT.name(), stages);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.ACM.name(), stages);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.RCM.name(), stages);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.ZCM.name(), stages);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.NCM.name(), stages);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.CRO.name(), stages);
        List<String> stages1 = new ArrayList<>();
        stages1.addAll(stages);
        stages1.add(GNGWorkflowConstant.DDE.name());
        stages1.add(GNGWorkflowConstant.CR_Q.name());
        stages1.add(GNGWorkflowConstant.BOPS.name());
        stages1.add(GNGWorkflowConstant.TRANCH_BOPS.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.BANK_VERIFIER.name(), stages1);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.RESI_VERIFIER.name(), stages1);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.OFC_VERIFIER.name(), stages1);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.LEGAL_VERIFIER.name(), stages1);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.ITR_VERIFIER.name(), stages1);
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.PROPERTY_VERIFIER.name(), stages1);
        stages = new ArrayList<>();
        stages.add(GNGWorkflowConstant.BOPS.name());
        stages.add(GNGWorkflowConstant.SUBJ_APRV.name());
        stages.add(GNGWorkflowConstant.TRANCH_BOPS.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.BOPS.name(), stages);
        stages = new ArrayList<>();
        stages.add(GNGWorkflowConstant.HOPS.name());
        stages.add(GNGWorkflowConstant.TRANCH_HOPS.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.HOPS.name(), stages);
        stages = new ArrayList<>();
        stages.add(Stages.Stage.EXTERNAL_CRDT.name());
        stages.add(Stages.Stage.EXT_APRV.name());
        stages.add(Stages.Stage.EXT_DCLN.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.EXTERNAL_CREDIT.name(), stages);
        stages = new ArrayList<>();
        stages.add(Stages.Stage.EXTERNAL_OPS.name());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.EXTERNAL_OPS.name(), stages);

        //ViewAll and View_Email
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.VIEWALL.name(), new ArrayList<>());
        ROLE_ENABLED_STAGES_MAP.put(Roles.Role.VIEWALL_EMAIL.name(), new ArrayList<>());

    }

    //TODO Amit this configuration get from DB (InstProdConfig collection)
    private static void initInstitutionQueueConfigMap(){
        INSTITUTION_QUEUE_CONFIG = new HashMap<>();
        INSTITUTION_QUEUE_CONFIG.put("4075", false); // SBFC
        INSTITUTION_QUEUE_CONFIG.put("4021", false); //SBFC PROD
        INSTITUTION_QUEUE_CONFIG.put("4045", false); // Softcell for demo purpose
        INSTITUTION_QUEUE_CONFIG.put("4084", false); // JFVS
        INSTITUTION_QUEUE_CONFIG.put("4025", false); // JFVS PROD
        INSTITUTION_QUEUE_CONFIG.put("4072", false); // Volition Credit
        INSTITUTION_QUEUE_CONFIG.put("4032", false); // ABFL
        INSTITUTION_QUEUE_CONFIG.put("4080", false); // Five Star
        INSTITUTION_QUEUE_CONFIG.put("4170", false); //TRIC
        INSTITUTION_QUEUE_CONFIG.put("4071", false); //TRIC PROD
        INSTITUTION_QUEUE_CONFIG.put("4160", false); //AMBIT
        INSTITUTION_QUEUE_CONFIG.put("4074", false); //AMBIT PROD
    }

    private static void initInstitutionNameMap() {
        institutionNameMap = new HashMap<>();
        institutionNameMap.put("4019", "HDB");
        institutionNameMap.put("4059", "TVS");
        //added TVS production configuration purpose.i.e different InstId in production (4016)
        institutionNameMap.put("4016", "TVS");
    }

    private static void initSecondaryDbEnabledMap() {
        INST_SECONDARY_DB = new HashMap<>();
        INST_SECONDARY_DB.put("4021", true);
    }

    private static void initDisbursalImageSet() {

        DISBURSAL_IMAGES_SET =new TreeSet<>();

        DISBURSAL_IMAGES_SET.add(GNGWorkflowConstant.INVOICE_IMAGE.toFaceValue());
        DISBURSAL_IMAGES_SET.add(GNGWorkflowConstant.DELIVERY_ORDER.toFaceValue());
        DISBURSAL_IMAGES_SET.add(GNGWorkflowConstant.AGREEMENT_FORM.toFaceValue());
    }

    private static void initUnderWritingImagesSet() {
        UNDERWRITER_IMAGES_SET = new TreeSet<>();
        UNDERWRITER_IMAGES_SET.add(GNGWorkflowConstant.CIBIL_REPORT.toFaceValue());
    }

    private static void initLoyaltyCardConfiguration(){
        LOYALTY_CARD_PRICE_MAP =new HashMap<>();
        LOYALTY_CARD_PRICE_MAP.put("4019",300.00);
    }

    private static void initSourcingImagesSet(){

        SOURCING_IMAGES_SET = new TreeSet<>();

        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.BANK_STATEMENT.name());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.DEBIT_CARD.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.PAN.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.AADHAAR.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.DRIVING_LICENSE.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.APPLICANT_PHOTO.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.PASSPORT.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.VOTER_ID.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.APPLICATION_FORM.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.INCOME_PROOF1.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.CUSTOMER_PHOTO.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.ADDITIONAL_KYC.toFaceValue());
        SOURCING_IMAGES_SET.add(GNGWorkflowConstant.BANK_PASSBOOK.toFaceValue());

    }

    private static void initVendorCodeByInstitution() {
        SERIAL_NUMBER_VENDOR_CODE_BY_INSTITUTION.put("4019", "F");
    }

    public static String getVendorCodeByInstitution(String institute){
        return SERIAL_NUMBER_VENDOR_CODE_BY_INSTITUTION.get(institute);
    }

    public static double getLoyaltyCardPrice(String institutionId){
        return LOYALTY_CARD_PRICE_MAP.get(institutionId);
    }

    private static void initAadharCertificate() {
        auaDataCreator = new AuthAUADataCreator(new Encrypter(new ClassPathResource("uidai_auth_encryp_certificate.cer")), false);
    }

    /**
     *
     */
    private static void initEmailContentConfig() {
        EMailContentConfiguration eMailContentConfiguration = new EMailContentConfiguration();
        eMailContentConfiguration.setInsertDate(new Date());
        eMailContentConfiguration.setActive(true);
        eMailContentConfiguration.setProduct("Consumer Durables");
        eMailContentConfiguration.setSubject("HDB Financial Services - ");
        eMailContentConfiguration
                .setDisclaimer("Disclaimer: The information contained in this e-mail and any of its attachments are intended for the exclusive use of the addressee(s) and may contain proprietary, confidential and privileged information of HDB Financial Services Limited ('HDB'). \n If you are not an intended recipient(s), you should not disseminate, distribute and copy this e-mail. Further, notify the sender of this e-mail immediately and destroy all copies and contents of this e-mail and any of its attachments. \n HDB shall not be responsible for any damage(s) caused by a virus and/or alteration of the e-mail and any of its attachments by a third party or otherwise. The contents of this e-mail and any of its attachments do not represent the views and/or policies of HDB. \n Any direct and/or indirect loss of profits and/or goodwill/reputation of HDB, due to any misuse and/or alteration of the contents of this e-mail and any of its attachments, shall have to be indemnified to HDB by the defaulter/miscreant. This is without prejudice to any of other legal rights of HDB.");
        eMailContentConfiguration.setInstitutionId("4019");

        EMAIL_CONTENT_CONFIGURATION.put(
                eMailContentConfiguration.getInstitutionId(),
                eMailContentConfiguration);
        eMailContentConfiguration = new EMailContentConfiguration();
        eMailContentConfiguration.setInsertDate(new Date());
        eMailContentConfiguration.setActive(true);
        eMailContentConfiguration.setProduct("Consumer Durables");
        eMailContentConfiguration.setSubject("Softcell Financial Services - ");
        eMailContentConfiguration
                .setDisclaimer("Disclaimer: The information contained in this e-mail and any of its attachments are intended for the exclusive use of the addressee(s) and may contain proprietary, confidential and privileged information of Softcell Financial Services Limited ('Softcell'). \n If you are not an intended recipient(s), you should not disseminate, distribute and copy this e-mail. Further, notify the sender of this e-mail immediately and destroy all copies and contents of this e-mail and any of its attachments. \n Softcell shall not be responsible for any damage(s) caused by a virus and/or alteration of the e-mail and any of its attachments by a third party or otherwise. The contents of this e-mail and any of its attachments do not represent the views and/or policies of Softcell. \n Any direct and/or indirect loss of profits and/or goodwill/reputation of Softcell, due to any misuse and/or alteration of the contents of this e-mail and any of its attachments, shall have to be indemnified to Softcell by the defaulter/miscreant. This is without prejudice to any of other legal rights of Softcell.");
        eMailContentConfiguration.setInstitutionId("4045");

        EMAIL_CONTENT_CONFIGURATION.put(
                eMailContentConfiguration.getInstitutionId(),
                eMailContentConfiguration);
    }

    /**
     * @param InstitutionId
     * @return
     */
    public static String getSubject(String InstitutionId) {
        if (null != EMAIL_CONTENT_CONFIGURATION.get(InstitutionId))
            return EMAIL_CONTENT_CONFIGURATION.get(InstitutionId).getSubject();
        else {
            return "";
        }
    }

    /**
     * @param InstitutionId
     * @return
     */
    public static String getDisclaimer(String InstitutionId) {
        if (null != EMAIL_CONTENT_CONFIGURATION.get(InstitutionId))
            return EMAIL_CONTENT_CONFIGURATION.get(InstitutionId)
                    .getDisclaimer();
        else {
            return "";
        }

    }

    /***
     * @param actionName    unique id for action for application
     * @param institutionId unique Id for Institution
     * @param productId     product Id
     * @return true if action is allowed else false.
     */
    public static boolean checkActionsAccess(ActionName actionName,
                                             String institutionId, String productId) {
        try {
            ActionConfiguration actionConfiguration = ACTION_CONFIGURATION_MAP
                    .get(institutionId).get(productId).get(actionName);
            if (null != actionConfiguration) {
                return actionConfiguration.isEnable();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }

    /**
     * @param institutionId
     * @param productId
     * @param templateName
     * @return
     */

    public static TemplateConfiguration getTemplate(String institutionId,
                                                    String productId, TemplateName templateName) {
        try {
            return TEMPLATE_CONFIGURATION_MAP.get(institutionId).get(productId)
                    .get(templateName);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;
    }

    /**
     *
     */
    private static void initActions() {

        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true));

        List<ActionConfiguration> actionConfigurations = mongoTemplate.find(query, ActionConfiguration.class);

        ACTION_CONFIGURATION_MAP = new HashMap<String, Map<String, Map<ActionName, ActionConfiguration>>>();

        for (ActionConfiguration actionConfiguration : actionConfigurations) {

            String institutionId = actionConfiguration.getInstitutionId();
            String productId = actionConfiguration.getProductId();
            ActionName actionName = actionConfiguration.getActionName();

            if (ACTION_CONFIGURATION_MAP.containsKey(institutionId)) {

                if(ACTION_CONFIGURATION_MAP.get(institutionId).containsKey(productId)) {
                    addActions(ACTION_CONFIGURATION_MAP.get(institutionId), productId, actionName, actionConfiguration);

                }else{
                    Map<ActionName, ActionConfiguration> actionMap = new TreeMap<>();
                    ACTION_CONFIGURATION_MAP.get(institutionId).put(productId, actionMap);
                    addActions(ACTION_CONFIGURATION_MAP.get(institutionId), productId, actionName, actionConfiguration);

                }
            } else {

                ACTION_CONFIGURATION_MAP.put(institutionId, getProductMap(productId, actionName, actionConfiguration));

            }
        }
    }

    /**
     *
     */
    private static void initTemplateConfiguration() {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true));
        List<TemplateConfiguration> templateConfigurations = mongoTemplate
                .find(query, TemplateConfiguration.class);
        TEMPLATE_CONFIGURATION_MAP = new HashMap<String, Map<String, Map<TemplateName, TemplateConfiguration>>>();

        for (TemplateConfiguration templateConfiguration : templateConfigurations) {

            String institutionId = templateConfiguration.getInstitutionId();
            String productId = templateConfiguration.getProductId();
            TemplateName templateName = templateConfiguration.getTemplateName();

            if (TEMPLATE_CONFIGURATION_MAP.containsKey(institutionId)) {

                if(TEMPLATE_CONFIGURATION_MAP.get(institutionId).containsKey(productId)) {

                    addTemplates(TEMPLATE_CONFIGURATION_MAP.get(institutionId), productId, templateName, templateConfiguration);

                }else{
                    Map<TemplateName, TemplateConfiguration> templateMap = new TreeMap<>();

                    TEMPLATE_CONFIGURATION_MAP.get(institutionId).put(productId, templateMap);
                    addTemplates(TEMPLATE_CONFIGURATION_MAP.get(institutionId), productId, templateName, templateConfiguration);


                }
            } else {

                TEMPLATE_CONFIGURATION_MAP.put(institutionId, getTemplateMap(productId, templateName, templateConfiguration));
            }

        }

    }

    /**
     *
     */
    private static void initSmsConfigurations() {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true));
        List<GngSmsServiceConfiguration> smsConfigurations = mongoTemplate
                .find(query, GngSmsServiceConfiguration.class);
        for (GngSmsServiceConfiguration smsConfiguration : smsConfigurations) {
            String institutionId = smsConfiguration.getInstitutionId();
            String productId = smsConfiguration.getProductId();
            String content = smsConfiguration.getContent();
            if (StringUtils.isNotBlank(institutionId)
                    && StringUtils.isNotBlank(productId)
                    && StringUtils.isNotBlank(content)) {
                if (SMS_CONFIGURATION_MAP.containsKey(institutionId)) {
                    SMS_CONFIGURATION_MAP.get(institutionId).put(productId,
                            content);
                } else {
                    SMS_CONFIGURATION_MAP.put(
                            institutionId,
                            getSmsContentMap(productId, content));
                }
            }
        }
    }

    /**
     * this method will add product configuration against the institutionId in the PRODUCT_CONFIGURATION_MAP. ie product_name ,alias name
     */
    private static void initProductConfiguration(){

        List<InstitutionProductConfiguration> institutionProductConfigList =  mongoTemplate.findAll(InstitutionProductConfiguration.class);

        Map<String, String> map = new HashMap<>();

        if (null != institutionProductConfigList && !institutionProductConfigList.isEmpty()) {

            PRODUCT_CONFIGURATION_MAP = new HashMap<String, Map<String, String>>();

            for (InstitutionProductConfiguration institutionProductConfiguration : institutionProductConfigList) {

                String productName = institutionProductConfiguration.getProductName();
                String aliasName = institutionProductConfiguration.getAliasName();
                String institutionId = institutionProductConfiguration.getInstitutionID();

                if (StringUtils.isNotBlank(productName) && StringUtils.isNotBlank(aliasName) && StringUtils.isNotBlank(institutionId)) {

                    if (PRODUCT_CONFIGURATION_MAP.containsKey(institutionId)) {
                        PRODUCT_CONFIGURATION_MAP.get(institutionId).put(productName, aliasName);
                    } else {
                        map.put(productName, aliasName);
                        PRODUCT_CONFIGURATION_MAP.put(institutionId, map);
                    }

                }

            }
        }
    }

    /**
     * return alias name against the institution id and product name from map.
     * @param productName
     * @param institutionId
     * @return
     */
    public static String getAliasNameByProductNameAndInstitutionId(String productName ,String institutionId) {

        if(null != PRODUCT_CONFIGURATION_MAP && !PRODUCT_CONFIGURATION_MAP.isEmpty()){

            return PRODUCT_CONFIGURATION_MAP.get(institutionId).get(productName);
        }else{
            return null;
        }

    }

    /**
     *
     */
    private static void initEmailConfiguration() {
        Query query = new Query();
        query.addCriteria(Criteria.where("enable").is(true));
        List<EmailConfiguration> emailConfigurations = mongoTemplate.find(query, EmailConfiguration.class);
        EMAIL_CONFIGURATION_MAP = new HashMap<String, Map<String, EmailConfiguration>>();

        for (EmailConfiguration emailConfiguration : emailConfigurations) {

            String institutionId = emailConfiguration.getInstitutionId();
            String productId = emailConfiguration.getProductId();

            if (EMAIL_CONFIGURATION_MAP.containsKey(institutionId)) {

                if (!EMAIL_CONFIGURATION_MAP.get(institutionId).containsKey(productId)) {

                    EMAIL_CONFIGURATION_MAP.get(institutionId).put(productId,emailConfiguration);

                }
            } else {

                EMAIL_CONFIGURATION_MAP.put(institutionId, getEmailMap(productId, emailConfiguration));

            }

        }
    }

    /**
     * @param institutionId
     * @param productId
     * @return
     */
    public static EmailConfiguration getEmailConfiguration(String institutionId, String productId) {
        try {
            return EMAIL_CONFIGURATION_MAP.get(institutionId).get(productId);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;
    }

    /**
     * @param productId
     * @param emailConfiguration
     * @return
     */
    private static Map<String, EmailConfiguration> getEmailMap(String productId,
                                                               EmailConfiguration emailConfiguration) {
        Map<String, EmailConfiguration> emailMap = new HashMap<>();
        emailMap.put(productId, emailConfiguration);
        return emailMap;
    }

    /**
     * @param emailMap
     * @param productId
     * @param emailConfiguration
     */
    private static void addEmailMap(Map<String, EmailConfiguration> emailMap, String productId,
                                    EmailConfiguration emailConfiguration) {
        if (!emailMap.containsKey(productId)) {
            getEmailMap(productId, emailConfiguration);
        }
    }

    /**
     * @param productMap
     * @param productId
     * @param actionName
     * @param actionConfiguration
     */
    private static void addActions(Map<String, Map<ActionName, ActionConfiguration>> productMap,
                                   String productId, ActionName actionName, ActionConfiguration actionConfiguration) {


        if (productMap.get(productId).containsKey(actionName)) {

            logger.warn("Duplicate action found for Product Id : {} Action Name {}",productId, actionName);

        } else {

            productMap.get(productId).put(actionName, actionConfiguration);
        }

    }

    /**
     * @param templateMap
     * @param productId
     * @param templateName
     * @param templateConfiguration
     */
    private static void addTemplates(
            Map<String, Map<TemplateName, TemplateConfiguration>> templateMap,
            String productId, TemplateName templateName,
            TemplateConfiguration templateConfiguration) {

        if (templateMap.get(productId).containsKey(templateName)) {

            logger.warn("Duplicate template found for Product Id : {} Template Name {}",productId , templateName);

        } else {

            templateMap.get(productId).put(templateName, templateConfiguration);

        }

    }

    /**
     * @param productId
     * @param actionName
     * @param actionConfiguration
     * @return
     */
    private static Map<String, Map<ActionName, ActionConfiguration>> getProductMap(
            String productId, ActionName actionName,
            ActionConfiguration actionConfiguration) {
        Map<String, Map<ActionName, ActionConfiguration>> productMap = new HashMap<>();
        Map<ActionName, ActionConfiguration> actionMap = new TreeMap<>();
        actionMap.put(actionName, actionConfiguration);
        productMap.put(productId, actionMap);
        return productMap;
    }

    /**
     * @param productId
     * @param templateName
     * @param templateConfiguration
     * @return
     */
    private static Map<String, Map<TemplateName, TemplateConfiguration>> getTemplateMap(
            String productId, TemplateName templateName,
            TemplateConfiguration templateConfiguration) {
        Map<String, Map<TemplateName, TemplateConfiguration>> productMap = new HashMap<>();
        Map<TemplateName, TemplateConfiguration> templateMap = new TreeMap<>();
        templateMap.put(templateName, templateConfiguration);
        productMap.put(productId, templateMap);
        return productMap;
    }

    /**
     * @param productId
     * @param content
     * @return
     */
    private static Map<String, String> getSmsContentMap(String productId,
                                                        String content) {
        Map<String, String> productContentMap = new HashMap<>();
        productContentMap.put(productId, content);
        return productContentMap;
    }

    /**
     *
     */
    private static void initLogoConfig() {

        FileHeader fileHeader = new FileHeader();
        fileHeader.setInstitutionId("4019");

        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileName("hdbfs.png");
        uploadFileDetails.setFileType("png");

        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId("HDBFS-DO-LOGO-01");
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);

        LOGO_FILE_CONFIG_REQUEST.put("4019", fileUploadRequest);

        fileHeader = new FileHeader();
        fileHeader.setInstitutionId("4045");
        uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileName("softcell_logo.jpg");
        uploadFileDetails.setFileType("jpg");
        fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId("SOFTCELL-LOGO-01");
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);
        LOGO_FILE_CONFIG_REQUEST.put("4045", fileUploadRequest);

        fileHeader = new FileHeader();
        fileHeader.setInstitutionId("4059");
        uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileName("TVS-LOGO.png");
        uploadFileDetails.setFileType("png");
        fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId("TVS-LOGO");
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);
        LOGO_FILE_CONFIG_REQUEST.put("4059", fileUploadRequest);

    }

    /**
     * @param institutionId
     * @param product
     * @return
     */
    public static String getSmsContent(String institutionId, String product) {
        String content = null;
        try {
            content = SMS_CONFIGURATION_MAP
                    .get(institutionId).get(product);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return content;
    }

    /**
     *
     */
    private static void initCaseHistoryStageConfigurationMap() {
        StageConfiguration stageConfiguration = new StageConfiguration();

        stageConfiguration.setInstitutionId("4019");
        stageConfiguration.setProductName("CDL");
        stageConfiguration.setStageProperties(getStagesConfig());
        stageConfigurationMap.put("4019", stageConfiguration);

    }

    /**
     * @return
     */
    private static Map<String, Map<String, StageProperties>> getStagesConfig() {
        Map<String, Map<String, StageProperties>> stageStandartization = new HashMap<String, Map<String, StageProperties>>();

        Map<String, StageProperties> cdlStages = new HashMap<String, StageProperties>();

        StageProperties stageProperties = new StageProperties();
        stageProperties.setDisplayName("DE");
        stageProperties.setStageId("1");
        stageProperties.setSystemStageName("GoNoGo");

        Map<String, StepProperties> stepProperties = new HashMap<String, StepProperties>();
        StepProperties stepDE = new StepProperties();
        stepDE.setDisplayName("DE");

        stepProperties.put("101", stepDE);
        stageProperties.setStepProperties(stepProperties);
        cdlStages.put("DE", stageProperties);

        stageStandartization.put("CDL", cdlStages);
        return null;
    }

    /**
     * @return
     */
    public static Set<String> getSerialNumberManufacturerNames() {
        return serialNumberManufacturerNames;
    }


    /**
     *
     * @return
     */
    public static Set<String> getDplManufacturerNames() {
        return dplManufacturerNames;
    }

    private static void initDplManufacturerNames() {
        dplManufacturerNames.add(GNGWorkflowConstant.APPLE.toFaceValue());
        dplManufacturerNames.add(GNGWorkflowConstant.SAMSUNG.toFaceValue());
    }

    /**
     *
     */
    private static void initSerialNumberManufacturerNames() {
        serialNumberManufacturerNames.add(GNGWorkflowConstant.LG.toFaceValue());
        serialNumberManufacturerNames.add(GNGWorkflowConstant.SAMSUNG.toFaceValue());
        serialNumberManufacturerNames.add(GNGWorkflowConstant.SONY.toFaceValue());
        serialNumberManufacturerNames.add(GNGWorkflowConstant.INTEX.toFaceValue());
        serialNumberManufacturerNames.add(GNGWorkflowConstant.APPLE.toFaceValue());

    }

    private static void initSecuredLoanProduct(){

        securedLoanProduct.add(Product.GL.name());
        securedLoanProduct.add(Product.LAP.name());
        securedLoanProduct.add(Product.LAS.name());
        securedLoanProduct.add(Product.CEL.name());
    }

    /**
     *
     */
    private static void initVendorByInstitution() {
        SERIAL_NUMBER_VENDOR_BY_INSTITUTION.put("4019", "HDBFS");
        SERIAL_NUMBER_VENDOR_BY_INSTITUTION.put("4059", "TVSC");
        //added TVS production configuration purpose.i.e different InstId in production (4016)
        SERIAL_NUMBER_VENDOR_BY_INSTITUTION.put("4016", "TVSC");
    }

    /**
     * @param institutionID
     * @return
     */
    public static String getVendorByInstitution(String institutionID) {
        return SERIAL_NUMBER_VENDOR_BY_INSTITUTION.get(institutionID);
    }

    /**
     *
     */
    private static void initReAppraisalConfig() {
        ReAppraisalConfig hdbfs = new ReAppraisalConfig();
        hdbfs.setAllowedReTry(2);
        hdbfs.setInstitutionId("4019");
        hdbfs.setProductType("Consumer Durables");
        NUMBER_OF_RE_TRY.put("4019", hdbfs);
    }

    /**
     *
     */
    private static void initProductSet() {
        DEDUPE_PRODUCT_SET.add(Product.CDL.name());
        DEDUPE_PRODUCT_SET.add(Product.DPL.name());
        NTC_EXCLUDED_PRODUCT_SET.add(Product.CDL.name());
        NTC_EXCLUDED_PRODUCT_SET.add(Product.CCBT.name());
        NTC_EXCLUDED_PRODUCT_SET.add(Product.DPL.name());
    }

    /**
     * method to populate state map depends on inititutionId and bureau_type
     * (cibil,hightmark,equifax,experian)
     */
    private static void initBureauStateMap() {

        // call to mongo to fetch data and populate states
        // BUREAU_STATES.put(key, value);

    }

    /**
     *
     */
    private static void initNegativePinCode() {
        HashSet<String> value = new HashSet<String>();
        NEGATIVE_PINCODESET.put("4019", value);
    }

    /**
     *
     */
    private static void initUrlConfiguration() {
        urlConfiguration = new ClassPathXmlApplicationContext(
                "url-configuration.xml");
        URL_CONFIGURATION = (URLConfiguration) urlConfiguration.getBean("urlConfiguration");
        initConnectionPool();
    }

    /**
     *
     */
    private static void initConnectionPool() {
        EsConnectionConfig elasticSearchConfig = URL_CONFIGURATION
                .getEsConnectionConfig().get("DEFAULT");
        ELASTIC_SEARCH_CONNECTION_POOL = new ElasticSearchConnectionPool<>();
        ESConnectionPool connectionPool = new ESConnectionPool(
                elasticSearchConfig);
        for (int conn = 0; conn < 5; conn++) {
            ELASTIC_SEARCH_CONNECTION_POOL.add(connectionPool
                    .getClusterClient());
        }
    }

    /**
     * @return
     */
    public static Client getClient() {
        if (null == ELASTIC_SEARCH_CONNECTION_POOL
                || 5 > ELASTIC_SEARCH_CONNECTION_POOL.size()) {
            initConnectionPool();
        }
        return ELASTIC_SEARCH_CONNECTION_POOL.peek();
    }

    /**
     *
     */
    private static void initApplicationScoringBucket() {
        ModuleOutcome moduleOutcome = new ModuleOutcome();
        moduleOutcome
                .setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
        moduleOutcome
                .setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);
        moduleOutcome.setFieldValue("0.0");
        moduleOutcome.setMessage("-");
        APPLICATION_SCORING_BUCKET.put(moduleOutcome.getOrder(),
                moduleOutcome);

        moduleOutcome = new ModuleOutcome();
        moduleOutcome
                .setFieldName(ScoringDisplayName.OFFICE_ADDRESS);
        moduleOutcome
                .setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);
        moduleOutcome.setFieldValue("0.0");
        moduleOutcome.setMessage("-");
        APPLICATION_SCORING_BUCKET.put(moduleOutcome.getOrder(),
                moduleOutcome);

        moduleOutcome = new ModuleOutcome();
        moduleOutcome.setFieldName(ScoringDisplayName.CIBIL_SCORE);
        moduleOutcome.setOrder(ScoringDisplayName.CIBIL_SCORE_ORDER);
        moduleOutcome.setFieldValue("0.0");
        moduleOutcome.setMessage("-");
        APPLICATION_SCORING_BUCKET.put(moduleOutcome.getOrder(),
                moduleOutcome);

        moduleOutcome = new ModuleOutcome();
        moduleOutcome.setFieldName(ScoringDisplayName.PAN_RESULT);
        moduleOutcome.setOrder(ScoringDisplayName.PAN_RESULT_ORDER);
        moduleOutcome.setFieldValue("0.0");
        moduleOutcome.setMessage("-");

        moduleOutcome = new ModuleOutcome();
        moduleOutcome
                .setFieldName(ScoringDisplayName.APPLICATION_SCORE);
        moduleOutcome
                .setOrder(ScoringDisplayName.APPLICATION_SCORE_ORDERE);
        moduleOutcome.setFieldValue("0.0");
        moduleOutcome.setMessage("-");
        APPLICATION_SCORING_BUCKET.put(moduleOutcome.getOrder(),
                moduleOutcome);

        moduleOutcome = new ModuleOutcome();
        moduleOutcome
                .setFieldName(ScoringDisplayName.EXPERIAN_SCORE);
        moduleOutcome
                .setOrder(ScoringDisplayName.EXPERIAN_SCORE_ORDER);
        moduleOutcome.setFieldValue("0.0");
        moduleOutcome.setMessage("-");
        APPLICATION_SCORING_BUCKET.put(moduleOutcome.getOrder(),
                moduleOutcome);

    }

    /**
     *
     */
    private static void initIncludeField() {
        List<String> fieldsList = new ArrayList<String>();
        fieldsList.add("application.croID");
        fieldsList.add("statusFlag");
        fieldsList.add("applicationRequest");
        fieldsList
                .add("applicantComponentResponse.multiBureauJsonRespose");
        // MB Corporate response
        fieldsList
                .add("applicantComponentResponse.mbCorpJsonResponse");

        fieldsList.add("applicantComponentResponse.scoringServiceResponse");

        // now we are sending whole scoring response no need of that
        /*fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.scoreData.scoreValue");
        fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.scoreData.status");
        fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.scoreData.scoreDetails");
        fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.scoreTree");
        fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.eligibilityResponse");
        fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.decisionResponse");
        fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.scoreAndDecisionDomains");
        fieldsList
                .add("applicantComponentResponse.scoringServiceResponse.scoringApplicantResponse");*/
        // Coapplicants' Bre responses
        fieldsList
                .add("applicantComponentResponseList");

        /*fieldsList.add("intrimStatus");*/
        /*fieldsList.add("applScoreVector");*/
        fieldsList.add("croDecisions");
        fieldsList.add("tvrDetails");
        fieldsList.add("croJustification");
        fieldsList.add("applicationRequest.currentStageId");
        fieldsList.add("dedupedApplications");
        fieldsList.add("losDetails");
        fieldsList.add("invoiceDetails");
        fieldsList.add("reInitiateCount");
        fieldsList.add("applicationStatus");
        fieldsList.add("applicationBucket");
        fieldsList.add("reappraiseReq");
        fieldsList.add("_id");
        fieldsList.add("parentID");
        fieldsList.add("rootID");
        fieldsList.add("allocationInfo");
        fieldsList.add("completed");
        fieldsList.add("dateTime");
        fieldsList.add("lastUpdatedDate");
        fieldsList.add("actualDisbDate");
        WEB_RESPONSE_INCLUDE_FIELD_MAP.put(CacheConstant.WEB.toString(),
                fieldsList);

        fieldsList = new ArrayList<String>();
        /*fieldsList.add("intrimStatus");*/
        fieldsList.add("applicationStatus");
        fieldsList.add("creditRiskOfficer");
        fieldsList.add("customAttributesList");
        fieldsList.add("croDecisions");
        /*fieldsList.add("applScoreVector");*/
        fieldsList.add("applicationRequest.header.croId");
        fieldsList.add("croJustification");
        fieldsList.add("applicationRequest.currentStageId");
        fieldsList.add("applicationRequest.header.dateTime");
        fieldsList.add("applicationRequest.request.applicant");
        fieldsList.add("applicationRequest.request.application.loanAmount");
        fieldsList.add("applicationRequest.reAppraiseDetails");
        fieldsList.add("applicationRequest.header.dateTime");
        fieldsList.add("applicationRequest.qdeDecision");
        fieldsList
                .add("applicantComponentResponse.saathiResponse.casedetails.status");
        fieldsList
                .add("applicantComponentResponse.creditVidyaResponse.status");
        WEB_RESPONSE_INCLUDE_FIELD_MAP.put(
                CacheConstant.STATUS_INCLUDE_FIELD_TAB.toString(), fieldsList);

    }

    /**
     *
     */
    private static void initBeanFactory() {
        loadComponentBean();
    }

    /**
     *
     */
    private static void loadComponentBean() {
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext(
                    "workFlowBean.xml");
            COMPONENT_FACTORY_BEAN.put(CacheConstant.BEAN_FACTORY, context);
        }catch (Exception e){
            logger.error("Error while loading workflow beans " + ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     *
     */
    private static void initComponentSetting() {

        ComponentConfigurationRepository componentConfigurationRepository = new ComponentConfigurationMongoRepository(mongoTemplate);

        List<ComponentConfiguration> componentConfigurations = componentConfigurationRepository
                .getSystemComponentConfiguration(ComponentConfigurationType.BASIC);


        if (null != componentConfigurations) {

            for (ComponentConfiguration componentConfiguration : componentConfigurations) {

                if (null != componentConfiguration.getInstitutionId() && !COMPONENT_SETTING_MAP.containsKey(componentConfiguration.getInstitutionId())) {

                    COMPONENT_SETTING_MAP.put(componentConfiguration.getInstitutionId(),componentConfiguration.getComponentSettings());

                    logger.info(">>> InstitutionId = "+ componentConfiguration.getInstitutionId()+ " >>> Workflow Type = "+ componentConfiguration.getComponentConfigType());

                    printComponentSetting(componentConfiguration.getComponentSettings());
                }
            }
        }

        logger.info(">>>>>>>> Cache Loaded for = " + COMPONENT_SETTING_MAP.size() + " Institutions >>>>>>>>>>>>");
    }

    private static void initMultiBreComponentSetting(){
        ComponentConfigurationRepository componentConfigurationRepository = new ComponentConfigurationMongoRepository(mongoTemplate);

        List<MultiBreComponentConfiguration> componentConfigurations = componentConfigurationRepository
                .getMultiBreComponentConfiguration(ComponentConfigurationType.BASIC);

// TODO Bhavishya add comment
        if (CollectionUtils.isNotEmpty(componentConfigurations)) {

            for (MultiBreComponentConfiguration componentConfiguration : componentConfigurations) {

                if (componentConfiguration.getInstitutionId() != null && componentConfiguration.getProductName() != null) {

                    Map<String,MultiBreComponentConfiguration> PRODUCT_SPECIFIC_COMPONENT_MAP = MULTIBRE_COMPONENT_SETTING_MAP.
                            get(componentConfiguration.getInstitutionId());
                    if(PRODUCT_SPECIFIC_COMPONENT_MAP == null){
                        PRODUCT_SPECIFIC_COMPONENT_MAP = new HashMap<>();
                        MULTIBRE_COMPONENT_SETTING_MAP.put(componentConfiguration.getInstitutionId(), PRODUCT_SPECIFIC_COMPONENT_MAP);
                    }
                    PRODUCT_SPECIFIC_COMPONENT_MAP.put(componentConfiguration.getProductName(), componentConfiguration);


                    logger.info(">>> MULTIBRE :: InstitutionId = {}, productId = {}  Workflow Type = {} ",
                            componentConfiguration.getInstitutionId(),componentConfiguration.getProductId(), componentConfiguration.getComponentConfigType());

                }
            }
        }
    }

    private static void initOtpStatus() {
        INST_SENT_OTP = new HashMap<>();
        INST_SENT_OTP.put("4021", true);
    }

    private static void printComponentSetting(ComponentSetting componentSettings) {

        if(componentSettings != null &&
                componentSettings.getComponentMap() != null
                && !componentSettings.getComponentMap().isEmpty()) {

            Set<Integer> componentMapKeys = componentSettings.getComponentMap().keySet();

            Map<Integer, List<com.softcell.workflow.component.Component>> componentMap = componentSettings.getComponentMap();

            if(componentMapKeys != null && !componentMapKeys.isEmpty()){

                for (Integer key : componentMapKeys){

                    List<com.softcell.workflow.component.Component> componentList = componentMap.get(key);

                    if(componentList != null && !componentList.isEmpty()){

                        logger.info("Reading Component info.........");

                        for(com.softcell.workflow.component.Component component :componentList){

                            logger.info("componentId : {} componentName : {}",component.getComponentId(),component.getComponentName());

                            Set<String> moduleKeys = component.getModuleSettingMap().keySet();

                            Map<String, ModuleSetting> moduleMap = component.getModuleSettingMap();

                            if(moduleKeys != null && !moduleKeys.isEmpty()) {

                                for (String moduleKey : moduleKeys) {

                                    ModuleSetting module = moduleMap.get(moduleKey);

                                    logger.info("module info moduleId : {} moduleName : {} moduleActive : {}",
                                            module.getModuleId(), module.getModuleName(), module.isActive());
                                }

                            }else{

                                logger.error("Modules not loaded ");

                            }

                        }
                    }else{
                        logger.error("Components not found for key {} is empty",key);
                    }
                }

            }else{
                logger.error("component not loaded ");
            }
        }
    }

    /**
     * @param institutionId
     * @param product
     * @param actionId
     * @return
     */
    public boolean checkAction(String institutionId, String product,
                               ActionName actionId) {
        ActionConfiguration actionConfiguration = ACTION_CONFIGURATION_MAP
                .get(institutionId).get(product).get(actionId);
        if (null != actionConfiguration) {
            return actionConfiguration.isEnable();
        }

        return false;
    }

    public static boolean isSecondaryDbEnable(String institutionId){
        boolean isEnable = false;
        if(StringUtils.isNotEmpty(institutionId) && Objects.nonNull(INST_SECONDARY_DB) && INST_SECONDARY_DB.containsKey(institutionId)){
            return INST_SECONDARY_DB.get(institutionId);
        }
        return isEnable;
    }

    public static boolean isSentOtpDisable(String institutionId){
        boolean isDisable = false;
        if(StringUtils.isNotEmpty(institutionId) && Objects.nonNull(INST_SENT_OTP) && INST_SENT_OTP.containsKey(institutionId)){
            return INST_SENT_OTP.get(institutionId);
        }
        return isDisable;
    }

}


