package com.softcell.gonogo.cache;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by yogeshb on 11/5/17.
 */
@Component
public class CacheManager {

    public static Map<String, Object> CACHE = new ConcurrentHashMap<>();

    private CallBack callBack;

    public static boolean refreshCache(String key) {
        return false;
    }

    public static boolean rebuildCache(String key, CallBack callback) {

        boolean b = dropCache(key);

        callback.loadDbDataToCache(key);

        return false;
    }

    public static boolean dropCache(String key) {

        if (CACHE.containsKey(key)) {
            Object o = CACHE.get(key);
            CACHE.put(key, null);
            return true;
        }

        return false;
    }

    interface CallBack {
        void loadDbDataToCache(String key);
    }

    class CallBackImpl implements CallBack {
        public void loadDbDataToCache(String key) {

        }
    }

}
