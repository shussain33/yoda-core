package com.softcell.gonogo.cache.distributed;

import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.queue.management.QueueUnassignedCases;
import com.softcell.gonogo.queue.management.RealtimeUserStatus;

import java.util.List;
import java.util.Map;

/**
 * Created by archana on 27/11/17.
 */
public abstract class RealtimeUserStatusCache {

    //TODO Amit remove
    public abstract List<QueueUnassignedCases> getAllInstituteUnassignedCases(String id);

    public enum ChangeableFields {
        STATUS, ASSIGNED_CASES_IDS, LAST_SIGNAL_RECEIVED, WORKING_CASE_ID, CRO_STATS;
    }

    public abstract void setOnline(RealtimeUserStatus realtimeUserStatus);

    public abstract void setIdle(String userId, String instituteId);

    public abstract void setOffline(String userId, String institutionId);

    public abstract List<RealtimeUserStatus> getOnlineUsers(String institutionId);
    public abstract List<RealtimeUserStatus> getOnlineUsers(String institutionId, boolean getCopy);

    public abstract List<RealtimeUserStatus> getIdleUsers(String institutionId);
    public abstract List<RealtimeUserStatus> getIdleUsers(String institutionId, boolean getCopy);

    public abstract RealtimeUserStatus getRealtimeUserStatus(String userId, String institutionId, boolean getCopy) throws GoNoGoException;

    public abstract boolean updateHeartbeat(String userId, String institutionId) throws Exception;

    public abstract void updateCroStats(String userId, String institutionId, Map<String, Long> stats) throws Exception;

    public abstract void updateAssignedCases(String userId, String institutionId, List<String> assignedCases) throws Exception;

    public abstract void setWorkingCaseId(String userId, String institutionId, String workingCaseId) throws Exception;

    public abstract boolean isUserLoggedIn(String userId, String institutionId);

    public abstract void updateRealtimeUserStatus(RealtimeUserStatus changed, ChangeableFields ...fieldNames);

    public abstract void setInstituteUnassignedCases(QueueUnassignedCases QueueUnassignedCases);

    public abstract QueueUnassignedCases getQueueUnassignedCases(String id);
}
