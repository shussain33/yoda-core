package com.softcell.gonogo.cache.distributed;

/**
 * Exposes methods to deal with distributed cache tool.
 * Encapsulates nested classes that represent cache for specific bean/pojos.
 * These nested classes expose methods to deal with the specific functionality the pojo is offering.
 * Created by archana on 21/11/17.
 */
public abstract class DistributedCacheHelper {


}
