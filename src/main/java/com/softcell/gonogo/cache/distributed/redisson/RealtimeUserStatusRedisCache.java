package com.softcell.gonogo.cache.distributed.redisson;

import com.softcell.constants.FieldSeparator;
import com.softcell.gonogo.cache.distributed.RealtimeUserStatusCache;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.queue.management.QueueUnassignedCases;
import com.softcell.gonogo.queue.management.RealtimeUserCache;
import com.softcell.gonogo.queue.management.RealtimeUserStatus;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by archana on 27/11/17.
 */
@Component
public class RealtimeUserStatusRedisCache extends RealtimeUserStatusCache {

    private static final Logger logger = LoggerFactory.getLogger(RealtimeUserStatusRedisCache.class);

    /**
     * Add to cache
     *
     * @param realtimeUserStatus
     */
    @Override
    public void setOnline(RealtimeUserStatus realtimeUserStatus) {
        liveObjectToGlobalCache(realtimeUserStatus);
    }


    @Override
    public void setIdle(String userId, String instituteId) {
        RealtimeUserStatus userStatus = getFromCache(userId, instituteId);
        userStatus.setStatus(RealtimeUserStatus.Status.IDLE);
        // Need to set as string as the Redisson handles everything as String
        userStatus.setStatusName(RealtimeUserStatus.Status.IDLE);
    }

    @Override
    public void setOffline(String userId, String institutionId) {
        RealtimeUserStatus realtimeUserStatus = getFromCache(userId, institutionId);

        // Remove Assigned Cases
        // Setting the expire time so that the key will be removed.
        ((RList<String>) realtimeUserStatus.getAssignedCaseIds()).expire(1L, TimeUnit.SECONDS);

        // Remove CRO Stats
        String statCollectionKeyName = StringUtils.join(RedissonCacheHelper.CollectionNames.CRO_STATS.name(), FieldSeparator.UNDER_SQURE
                , RealtimeUserCache.getKey(userId, institutionId));
        RKeys keys = RedissonCacheHelper.client.getKeys();
        keys.delete(statCollectionKeyName);

        // Remove key for user
        String collectionKeyName = StringUtils.join(RedissonCacheHelper.CollectionNames.USER_STATUS.name(), FieldSeparator.UNDER_SQURE
                , institutionId);
        RSet<String> set = RedissonCacheHelper.client.getSet(collectionKeyName);
        set.remove(userId);

        try {
            RLiveObjectService service = RedissonCacheHelper.client.getLiveObjectService();
            service.delete(RealtimeUserStatus.class, userId);
        } catch (NullPointerException npe) {
            logger.error("{} user of institute {} not available in global cache", userId, institutionId);
        }

    }

    @Override
    public List<RealtimeUserStatus> getOnlineUsers(String institutionId) {
        return getUsers(institutionId, RealtimeUserStatus.Status.ONLINE, false);
    }

    //@Override
    public List<RealtimeUserStatus> getOnlineUsers(String institutionId, boolean getCopy) {
        return getUsers(institutionId, RealtimeUserStatus.Status.ONLINE, getCopy);
    }

    @Override
    public List<RealtimeUserStatus> getIdleUsers(String institutionId) {
        return getUsers(institutionId, RealtimeUserStatus.Status.IDLE, false);
    }

    //@Override
    public List<RealtimeUserStatus> getIdleUsers(String institutionId, boolean getCopy) {
        return getUsers(institutionId, RealtimeUserStatus.Status.IDLE, getCopy);
    }

    @Override
    public RealtimeUserStatus getRealtimeUserStatus(String userId, String institutionId, boolean getCopy) throws GoNoGoException {
        RealtimeUserStatus userFromCache = getFromCache(userId, institutionId);
        if (getCopy) {
            RealtimeUserStatus userStatus = setValuesFromCache(userFromCache);
            return userStatus;
        } else {
            return userFromCache;
        }

    }

    @Override
    public boolean updateHeartbeat(String userId, String institutionId) throws Exception {

        boolean statusChanged = false;
        RealtimeUserStatus userstatus = getFromCache(userId, institutionId);
        if (userstatus == null || RealtimeUserStatus.Status.valueOf(userstatus.getStatusName())
                == RealtimeUserStatus.Status.OFFLINE) {
            throw new Exception(String.format("User %s not logged in", userId));
        }
        if (RealtimeUserStatus.Status.valueOf(userstatus.getStatusName()) == RealtimeUserStatus.Status.IDLE) {
            userstatus.setStatus(RealtimeUserStatus.Status.ONLINE);
            userstatus.setStatusName(RealtimeUserStatus.Status.ONLINE);
            statusChanged = true;
        }
        userstatus.setLastSignalReceived(new Date());
        logger.debug("Heartbeat time {} for user {} of institution {}", new Date(), userId, institutionId);
        return statusChanged;
    }

    @Override
    public void updateCroStats(String userId, String institutionId, Map<String, Long> stats) throws Exception {
        RealtimeUserStatus userStatus = getFromCache(userId, institutionId);
        persistCroStats(userId, institutionId, userStatus, stats);
    }

    @Override
    public void updateAssignedCases(String userId, String institutionId, List<String> assignedCases) throws Exception {
        RealtimeUserStatus userFromCache = getFromCache(userId, institutionId);
        userFromCache.setAssignedCaseIds(assignedCases);
    }

    @Override
    public void setWorkingCaseId(String userId, String institutionId, String workingCaseId) throws Exception {
        RealtimeUserStatus userFromCache = getFromCache(userId, institutionId);
        userFromCache.setWorkingCaseId(workingCaseId);
    }

    @Override
    public boolean isUserLoggedIn(String userId, String institutionId) {
        String collectionKeyName = StringUtils.join(RedissonCacheHelper.CollectionNames.USER_STATUS.name(),
                FieldSeparator.UNDER_SQURE, institutionId);
        RSet<String> set = RedissonCacheHelper.client.getSet(collectionKeyName);
        return set.contains(userId);
    }

    @Override
    public void updateRealtimeUserStatus(RealtimeUserStatus changed, ChangeableFields... fieldNames) {
        RealtimeUserStatus userFromCache = getFromCache(changed.getUserId(), changed.getInstitutionId());
        for (ChangeableFields fieldName : fieldNames) {
            switch (fieldName) {
                case STATUS:
                    userFromCache.setStatus(changed.getStatus());
                    userFromCache.setStatusName(changed.getStatus());
                    break;
                case LAST_SIGNAL_RECEIVED:
                    userFromCache.setLastSignalReceived(new Date());
                    break;
                case WORKING_CASE_ID:
                    userFromCache.setWorkingCaseId(changed.getWorkingCaseId());
                    break;
                case ASSIGNED_CASES_IDS:
                    userFromCache.setAssignedCaseIds(changed.getAssignedCaseIds());
                    break;
                case CRO_STATS:
                    persistCroStats(changed.getUserId(), changed.getInstitutionId(), userFromCache, changed.getTodaysCroStatistics());
                    break;
            }
        }
    }

    @Override
    public void setInstituteUnassignedCases(QueueUnassignedCases QueueUnassignedCases) {
        RLiveObjectService service = RedissonCacheHelper.client.getLiveObjectService();
        //delete already cached object first and then add new object
        service.delete(com.softcell.gonogo.queue.management.QueueUnassignedCases.class, QueueUnassignedCases.getInstituteGroupId());
        service.persist(QueueUnassignedCases);
    }

    @Override
    public QueueUnassignedCases getQueueUnassignedCases(String id) {
        //get object from cache
        RLiveObjectService service = RedissonCacheHelper.client.getLiveObjectService();
        QueueUnassignedCases cachedObject = service.get(QueueUnassignedCases.class, id);
        // object received from cache is just reference object(proxy), so fetch its data and create local object
        QueueUnassignedCases unassignedCases = new QueueUnassignedCases();
        if(cachedObject != null){
            unassignedCases.setInstituteGroupId(cachedObject.getInstituteGroupId());
            unassignedCases.setUnassignedCaseIds(cachedObject.getUnassignedCaseIds());
        }
        return unassignedCases;
    }

    @Override
    public List<QueueUnassignedCases> getAllInstituteUnassignedCases(String id) {
        //get object from cache
        RedissonClient client = RedissonCacheHelper.client;
        RLiveObjectService service = client.getLiveObjectService();
        QueueUnassignedCases cachedObject = service.get(QueueUnassignedCases.class, id);
        // object received from cache is just reference object(proxy), so fetch its data and create local object
        QueueUnassignedCases unassignedCases = new QueueUnassignedCases();
        unassignedCases.setInstituteGroupId(cachedObject.getInstituteGroupId());
        unassignedCases.setUnassignedCaseIds(cachedObject.getUnassignedCaseIds());
        return null;
    }

    private void liveObjectToGlobalCache(RealtimeUserStatus realtimeUserStatus) {
        // TODO when userstatus changes to ONLINE from IDLE then we need to check first the existence
        String institutionId = realtimeUserStatus.getInstitutionId();
        String userId = realtimeUserStatus.getUserId();
        RLiveObjectService service = RedissonCacheHelper.client.getLiveObjectService();

        System.out.println("To persist = " + realtimeUserStatus.toString());
        RealtimeUserStatus persisted = service.persist(realtimeUserStatus);

        persistCroStats(userId, institutionId, persisted, realtimeUserStatus.getTodaysCroStatistics());


        // Add the user entry in the logged in set
        String collectionKeyName = StringUtils.join(RedissonCacheHelper.CollectionNames.USER_STATUS.name(), FieldSeparator.UNDER_SQURE
                , institutionId);

        RSet<String> set = RedissonCacheHelper.client.getSet(collectionKeyName);
        set.add(userId);

    }

    private void persistCroStats(String userId, String institutionId, RealtimeUserStatus persisted,
                                 Map<String, Long> todaysCroStatistics) {
        // Persist CRO-Stats
        String statCollectionKeyName = StringUtils.join(RedissonCacheHelper.CollectionNames.CRO_STATS.name(), FieldSeparator.UNDER_SQURE
                , RealtimeUserCache.getKey(userId, institutionId));

        RMap<String, Long> map = RedissonCacheHelper.client.<String, Long>getMap(statCollectionKeyName);
        persisted.setTodaysCroStatistics(map);
        map.putAll(todaysCroStatistics);
    }

    private List<RealtimeUserStatus> getUsers(String institutionId, RealtimeUserStatus.Status userStatus, boolean getCopy)
           {

        RLiveObjectService service = RedissonCacheHelper.client.getLiveObjectService();
        List<RealtimeUserStatus> userStatusList = new ArrayList<>();
        String collectionKeyName = StringUtils.join(RedissonCacheHelper.CollectionNames.USER_STATUS.name(), FieldSeparator.UNDER_SQURE
                , institutionId);
        RSet<String> userIds = RedissonCacheHelper.client.getSet(collectionKeyName);

        // Iterate set to get the userId and retrieve liveobject using this key
        Iterator<String> itr = userIds.iterator();
        String userId;
        RealtimeUserStatus realtimeUserStatus, retrieved;
        String statCollectionKeyName;

        while (itr.hasNext()) {
            userId = itr.next();
            retrieved = service.get(RealtimeUserStatus.class, userId);
            if (StringUtils.equalsIgnoreCase(retrieved.getStatusName(), userStatus.name())) {
                if (getCopy) {
                    realtimeUserStatus = setValuesFromCache(retrieved);
                    userStatusList.add(realtimeUserStatus);
                } else {
                    userStatusList.add(retrieved);
                }
            }
        }
        return userStatusList;
    }

    private RealtimeUserStatus setValuesFromCache(RealtimeUserStatus retrieved)  {
        RealtimeUserStatus realtimeUserStatus = new RealtimeUserStatus();

       /* try {
            BeanUtils.copyProperties(realtimeUserStatus, retrieved);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error(String.format("Unable to copy from cache : error %s", e.getMessage()) );
        }*/
        realtimeUserStatus.setUserId(retrieved.getUserId());
        realtimeUserStatus.setUserName(retrieved.getUserName());
        realtimeUserStatus.setInstitutionId(retrieved.getInstitutionId());
        realtimeUserStatus.setStatus(RealtimeUserStatus.Status.valueOf(retrieved.getStatusName()));
        realtimeUserStatus.setRole(retrieved.getRole());
        realtimeUserStatus.setLastSignalReceived(retrieved.getLastSignalReceived());
        realtimeUserStatus.setWorkingCaseId(retrieved.getWorkingCaseId());

        realtimeUserStatus.setAssignedCaseIds(retrieved.getAssignedCaseIds());
        realtimeUserStatus.setTodaysCroStatistics(retrieved.getTodaysCroStatistics());

        logger.debug("Retrived {} and set {} ", retrieved.getWorkingCaseId(), realtimeUserStatus.getWorkingCaseId());
        return realtimeUserStatus;
    }

    private RealtimeUserStatus getFromCache(String userId, String institutionId) {
        RLiveObjectService service = RedissonCacheHelper.client.getLiveObjectService();
        // TODO check what s
        return service.get(RealtimeUserStatus.class, userId);

    }
}
