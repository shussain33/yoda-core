package com.softcell.gonogo.cache.distributed.redisson;

import com.softcell.gonogo.cache.distributed.DistributedCacheHelper;
import com.softcell.gonogo.queue.management.QueueUnassignedCases;
import com.softcell.gonogo.queue.management.RealtimeUserStatus;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RKeys;
import org.redisson.api.RLiveObjectService;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Iterator;

/**
 * Offers concrete functionality to interact with the distributed cache used.
 * Created by archana on 21/11/17.
 */
@Component
@PropertySource(value = "classpath:redis.properties")
public class RedissonCacheHelper extends DistributedCacheHelper {

    enum RedisServerMode {
        SINGLE, CLUSTER, MASTER_SLAVE, REPLICATED, SENTINEL;
    }

    @Value("${redis-server}")
    private String REDIS_SERVER_ADDRESS;

    @Value("${server-mode}")
    private String REDIS_SERVER_MODE;

    private Config config;
    static RedissonClient client;

    private static final Logger logger = LoggerFactory.getLogger(RedissonCacheHelper.class);

    // TODO : Commented for a time being
    //@PostConstruct
    private void init()
    {
        config = new Config();
        logger.info("REDIS_SERVER_ADDRESS = {} , REDIS_SERVER_MODE = {} ", REDIS_SERVER_ADDRESS, REDIS_SERVER_MODE);
        if ( ! StringUtils.equalsIgnoreCase(REDIS_SERVER_MODE, RedisServerMode.SINGLE.name())) {
            logger.error("Redis Server mode {} not yet supported in Gonogo", REDIS_SERVER_MODE);
            logger.info("Using SingleServer mode for Redis");
        }

        config.useSingleServer().setAddress(REDIS_SERVER_ADDRESS);
        client = Redisson.create(config);
        registerClasses();
    }


    private void registerClasses() {
        // Register custom classes which will be in Redis Cache
        RLiveObjectService service = client.getLiveObjectService();
        service.registerClass(RealtimeUserStatus.class);
        // Class to maintain unassigned application ids in cache
        service.registerClass(QueueUnassignedCases.class);
    }

    public enum CollectionNames {
        USER_STATUS, ASSIGNED_CASES, CRO_STATS, INSTITUTE_UNASSIGNED_CASES
    }

    // ------ Helper methods --------
    public void getFromCache() {
        RKeys keys = client.getKeys();
        Iterable<String> itr = keys.getKeys();
        Iterator itt = itr.iterator();
        while (itt.hasNext()) {
            Object o = itt.next();

            System.out.println(o.getClass() + " = " + o);
        }
    }

    public void emptyCache() {
        RKeys keys = client.getKeys();

        Iterable<String> itr = keys.getKeys();
        Iterator itt = itr.iterator();
        while (itt.hasNext()) {
            String keyfromcache = (String) itt.next();
         /*   RMap<String, Object> map = client.getMap(keyfromcache);
            Set<String> allKeys = map.readAllKeySet();
            map.delete();*/
            keys.delete(keyfromcache);
        }
    }

    public void emptyCache(String key) {
        RKeys keys = client.getKeys();
        Iterable<String> itr = keys.getKeys();
        Iterator itt = itr.iterator();
        while (itt.hasNext()) {
            String keyfromcache = (String) itt.next();
            if(keyfromcache.equalsIgnoreCase(key)) {
                keys.delete(keyfromcache);
                break;
            }
        }
    }

    public void forJunit() {
        init();
    }

    public void setREDIS_SERVER_ADDRESS(String REDIS_SERVER_ADDRESS) {
        this.REDIS_SERVER_ADDRESS = REDIS_SERVER_ADDRESS;
    }

    public void setREDIS_SERVER_MODE(String REDIS_SERVER_MODE) {
        this.REDIS_SERVER_MODE = REDIS_SERVER_MODE;
    }

    @PreDestroy
    public void cleanUp(){
        // Close client
        logger.info("Shutting down Redis client...");
        client.shutdown();
    }
}
