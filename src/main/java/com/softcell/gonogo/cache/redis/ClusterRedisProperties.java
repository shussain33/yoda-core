package com.softcell.gonogo.cache.redis;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class ClusterRedisProperties {

    @Value("${redis.cluster.nodes}")
    private String nodes;

    @Value("${redis.cluster.active}")
    private boolean active = false;

    @Value("${redis.cluster.cache-expire}")
    private Long defaultCacheExpiry;

    @Value("${redis.cluster.adaptiveRefreshTriggersTimeout}")
    private int adaptiveRefreshTriggersTimeout;
}
