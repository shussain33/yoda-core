package com.softcell.gonogo.cache.redis;

import com.softcell.constants.Constant;
import com.softcell.constants.FieldSeparator;
import com.softcell.gonogo.cache.services.RedisServiceImpl;
import com.softcell.utils.GngUtils;
import io.lettuce.core.ClientOptions;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.ClusterTopologyRefreshOptions;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.async.RedisAdvancedClusterAsyncCommands;
import io.lettuce.core.cluster.api.sync.RedisAdvancedClusterCommands;
import io.lettuce.core.codec.CompressionCodec;
import io.lettuce.core.resource.DefaultClientResources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static io.lettuce.core.codec.CompressionCodec.CompressionType.DEFLATE;

@Configuration
public class RedisConfig {

    private static final Logger log = LoggerFactory.getLogger(RedisConfig.class);

    static RedisAdvancedClusterAsyncCommands<String, Object> redisAdvancedClusterAsyncCommands = null;
    static RedisCommands<String, Object> redisCommands = null;
    static RedisAdvancedClusterCommands<String, Object> redisAdvancedClusterCommands = null;
    static RedisAsyncCommands<String, Object> redisAsyncCommands = null;

    //Below 2 members are used to store data in cache without using compression algorithm
    static RedisAsyncCommands<String, String> stringRedisAsyncCommands = null;
    static RedisAdvancedClusterAsyncCommands<String, String> stringRedisAdvancedClusterAsyncCommands = null;

    static HashMap<String, Object> applicationPropertyDetails = GngUtils.getApplicationPropertyDetails();


    @Autowired(required = false)
    static ClusterRedisProperties clusterRedisProperties;

    @Autowired(required = false)
    static StandaloneRedisProperties standaloneRedisProperties;

    public RedisConfig() {
        log.warn("Initialising stand alone server redis properties");
        if (applicationPropertyDetails.containsKey(Constant.STANDALONE_SERVER)) {
            standaloneRedisProperties = (StandaloneRedisProperties) applicationPropertyDetails.get(Constant.STANDALONE_SERVER);
            log.warn("Initialised stand alone server redis properties {}", standaloneRedisProperties);
        }
        log.warn("Initialising cluster server redis properties");
        if (applicationPropertyDetails.containsKey(Constant.CLUSTER_SERVER)) {
            clusterRedisProperties = (ClusterRedisProperties) applicationPropertyDetails.get(Constant.CLUSTER_SERVER);
            log.warn("Initialised cluster server redis properties {}", clusterRedisProperties);
        }
    }

    @Bean
    public  RedisAsyncCommands<String, Object> redisAsyncConnection() {
        log.warn("Initialising redis async connection");
        if (null==redisAsyncCommands && !clusterRedisProperties.isActive()) {
            RedisClient redisClient = getRedisClient();
            redisAsyncCommands = redisClient.connect(CompressionCodec.valueCompressor(new SerializedObjectCodec(), DEFLATE)).async();
            log.warn("Initialised redis async connection {}", redisAsyncCommands);
        }
        return redisAsyncCommands;
    }

    @Bean
    public RedisAdvancedClusterAsyncCommands<String, Object> redisAdvancedClusterAsyncConnection() {
        log.warn("Initialising redis advanced cluster async connection");
        if (null==redisAdvancedClusterAsyncCommands && clusterRedisProperties.isActive()) {
            RedisClusterClient clusterClient = getRedisClusterClient();
            redisAdvancedClusterAsyncCommands = clusterClient.connect(CompressionCodec.valueCompressor(new SerializedObjectCodec(), DEFLATE)).async();
            log.warn("Initialised redis advanced cluster async connection {}", redisAdvancedClusterAsyncCommands);
        }
        return redisAdvancedClusterAsyncCommands;
    }

    @Bean
    public  RedisCommands<String, Object> redisSyncConnection() {
        log.warn("Initialising redis sync connection");
        if (null==redisCommands && !clusterRedisProperties.isActive()) {
            RedisClient redisClient = getRedisClient();
            redisCommands = redisClient.connect(CompressionCodec.valueCompressor(new SerializedObjectCodec(), DEFLATE)).sync();
            log.warn("Initialised redis sync connection {}", redisCommands);
        }
        return redisCommands;
    }

    @Bean
    public  RedisAdvancedClusterCommands<String, Object> redisAdvancedClusterSyncConnection() {
        log.warn("Initialising redis advanced cluster sync connection");
        log.warn("Initialising redis advanced cluster sync connection");
        if (null==redisAdvancedClusterCommands && clusterRedisProperties.isActive()) {
            RedisClusterClient clusterClient = getRedisClusterClient();
            redisAdvancedClusterCommands = clusterClient.connect(CompressionCodec.valueCompressor(new SerializedObjectCodec(), DEFLATE)).sync();
            log.warn("Initialised redis advanced cluster sync connection {}", redisAdvancedClusterCommands);
        }
        return redisAdvancedClusterCommands;
    }

    private  RedisClient getRedisClient() {
        log.warn("Redis standalone activation in progress");
        RedisServiceImpl.defaultExpireTime =standaloneRedisProperties.getDefaultCacheExpiry();
        RedisURI redisURI = new RedisURI(standaloneRedisProperties.getRedisHost(), standaloneRedisProperties.getRedisPort(), standaloneRedisProperties.getAdaptiveRefreshTriggersTimeout(), TimeUnit.MINUTES);

        int threadPoolSize = Runtime.getRuntime().availableProcessors();
        threadPoolSize *= 2;
        log.warn("Redis cluster threadPoolSize[{}]", threadPoolSize);

        RedisClient redisClient = RedisClient.create(
                DefaultClientResources.builder()
                        .ioThreadPoolSize(threadPoolSize)
                        .computationThreadPoolSize(threadPoolSize)
                        .build(), redisURI);
        redisClient.setOptions(ClientOptions.builder().autoReconnect(true).pingBeforeActivateConnection(false).build());
        log.warn("Redis standalone activated {}", redisClient);
        return redisClient;
    }

    private  RedisClusterClient getRedisClusterClient() {
        log.warn("Redis cluster activation in progress");
        RedisServiceImpl.defaultExpireTime =clusterRedisProperties.getDefaultCacheExpiry();
        String[] serverArray = clusterRedisProperties.getNodes().split(",");
        List<RedisURI> redisURIS = new ArrayList<>();
        for (String server : serverArray) {
            String[] ipPortPair = server.split(":");
            redisURIS.add(RedisURI.Builder.redis(ipPortPair[0].trim(), Integer.valueOf(ipPortPair[1].trim())).build());
        }

        int threadPoolSize = Runtime.getRuntime().availableProcessors();
        threadPoolSize *= 2;
        log.warn("Redis cluster threadPoolSize[{}]", threadPoolSize);
        RedisClusterClient clusterClient = RedisClusterClient.create(DefaultClientResources.builder().ioThreadPoolSize(threadPoolSize).computationThreadPoolSize(threadPoolSize).build(), redisURIS);

        clusterClient.setOptions(ClusterClientOptions.builder()
                .topologyRefreshOptions(ClusterTopologyRefreshOptions.builder()
                        .enableAdaptiveRefreshTrigger(ClusterTopologyRefreshOptions.RefreshTrigger.MOVED_REDIRECT, ClusterTopologyRefreshOptions.RefreshTrigger.PERSISTENT_RECONNECTS)
                        .adaptiveRefreshTriggersTimeout(Duration.ofMinutes(clusterRedisProperties.getAdaptiveRefreshTriggersTimeout()))
                        .build()).autoReconnect(true).pingBeforeActivateConnection(false).build());
        log.warn("Redis cluster activated {}", clusterClient);
        return clusterClient;
    }

    //Below method are used to store data in cache without using compression algorithm
    @Bean(name = "redis")
    public RedisAsyncCommands<String, String> standaloneConnectionRedis() {
        log.warn("Redis stand alone connection activation in progress");
        if (null==stringRedisAsyncCommands && !clusterRedisProperties.isActive()) {
            RedisURI redisURI = new RedisURI(standaloneRedisProperties.getRedisHost(), standaloneRedisProperties.getRedisPort(), standaloneRedisProperties.getAdaptiveRefreshTriggersTimeout(), TimeUnit.MINUTES);

            int threadPoolSize = Runtime.getRuntime().availableProcessors();
            threadPoolSize *= 2;
            log.warn("Redis cluster threadPoolSize[{}]",threadPoolSize);

            RedisClient redisClient = RedisClient.create(
                    DefaultClientResources.builder()
                            .ioThreadPoolSize(threadPoolSize)
                            .computationThreadPoolSize(threadPoolSize)
                            .build(), redisURI);

            redisClient.setOptions(ClientOptions.builder().autoReconnect(true)
                    .pingBeforeActivateConnection(true)
                    .build());

            stringRedisAsyncCommands= redisClient.connect().async();
            try {
                log.warn("Redis Standalone activation, ping [{}],pong [{}]", stringRedisAsyncCommands.set("lettuceTest", "application start time:" + new Date()).get(), stringRedisAsyncCommands.get("lettuceTest").get());
            } catch (InterruptedException | ExecutionException e) {
                log.error(e.toString());
            }
            log.warn("Redis stand alone connection activated {}", stringRedisAsyncCommands);
        }
        return stringRedisAsyncCommands;
    }

    //Below method are used to store data in cache without using compression algorithm
    @Bean(name = "redisCluster")
    public RedisAdvancedClusterAsyncCommands<String, String> ConnectionRedisCluster() {
        log.warn("Redis cluster activation in progress");
        if (null==stringRedisAdvancedClusterAsyncCommands && clusterRedisProperties.isActive()) {
            String[] serverArray = clusterRedisProperties.getNodes().split(FieldSeparator.COMMA_STR);
            List<RedisURI> redisURIS = new ArrayList<>();
            for (String server : serverArray) {
                String[] ipPortPair = server.split(FieldSeparator.COLON);
                redisURIS.add(RedisURI.Builder.redis(ipPortPair[0].trim(), Integer.valueOf(ipPortPair[1].trim())).build());
            }

            int threadPoolSize = Runtime.getRuntime().availableProcessors();
            threadPoolSize *= 2;
            log.warn("Redis cluster threadPoolSize[{}]",threadPoolSize);
            RedisClusterClient clusterClient = RedisClusterClient.create(
                    DefaultClientResources.builder()
                            .ioThreadPoolSize(threadPoolSize)
                            .computationThreadPoolSize(threadPoolSize)
                            .build(), redisURIS);

            clusterClient.setOptions(ClusterClientOptions.builder()
                    .topologyRefreshOptions(ClusterTopologyRefreshOptions.builder()
                            .enableAdaptiveRefreshTrigger(ClusterTopologyRefreshOptions.RefreshTrigger.MOVED_REDIRECT, ClusterTopologyRefreshOptions.RefreshTrigger.PERSISTENT_RECONNECTS)
                            .adaptiveRefreshTriggersTimeout(Duration.ofMinutes(clusterRedisProperties.getAdaptiveRefreshTriggersTimeout()))
                            .build())

                    .autoReconnect(true)
                    .pingBeforeActivateConnection(true)
                    .build());

            stringRedisAdvancedClusterAsyncCommands= clusterClient.connect().async();

            try {
                log.warn("Redis cluster activation, ping [{}],pong [{}]", stringRedisAdvancedClusterAsyncCommands.set("lettuceTest", "application start time:" + new Date()).get(), stringRedisAdvancedClusterAsyncCommands.get("lettuceTest").get());
            } catch (InterruptedException | ExecutionException e) {
                log.error("Exception occured while creating redis bean:{} ",e);
            }
        }
        log.warn("Redis cluster activated {}", stringRedisAdvancedClusterAsyncCommands);
        return stringRedisAdvancedClusterAsyncCommands;
    }
}
