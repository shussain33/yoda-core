package com.softcell.gonogo.cache.redis;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class StandaloneRedisProperties {

    @Value("${redis.enabled}")
    private boolean active = false;

    @Value("${redis.host}")
    private String redisHost = "localhost";

    @Value("${redis.port}")
    private int redisPort = 6379;

    @Value("${redis.cache-expire}")
    private Long defaultCacheExpiry;

    @Value("${redis.adaptiveRefreshTriggersTimeout}")
    private int adaptiveRefreshTriggersTimeout;
}
