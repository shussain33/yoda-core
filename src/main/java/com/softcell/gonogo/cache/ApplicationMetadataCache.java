/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 4, 2016 6:02:36 PM
 */
package com.softcell.gonogo.cache;

import com.softcell.constants.CacheConstant;
import org.apache.commons.lang3.text.WordUtils;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author kishorp
 *         <p/>
 *         <pre>
 *         A <em> ApplicationMetadataCache</em> is used to  initialized PL meta data, which used by web services frequently.
 *         </pre>
 */
public class ApplicationMetadataCache {
    /**
     * All drop down value of hdfcb application, Gender, Address type
     */
    public static Map<String, Map<String, Set<String>>> DROP_DOWN_ENUMERATION = new TreeMap<String, Map<String, Set<String>>>();

    static {
        init();
    }

    /**
     *
     */
    private static void init() {
        loadGender();
        loadAddressType();
        loadConstitutions();
        loadEmployType();
        loadkycDocumentName();
    }

    /**
     *
     */
    private static void loadkycDocumentName() {
        Set<String> kycDocumetationSet = new HashSet<String>();
        kycDocumetationSet.add(WordUtils.capitalizeFully("PAN"));
        Map<String, Set<String>> institutionEnumeration = DROP_DOWN_ENUMERATION
                .get("1500");
        institutionEnumeration.put(CacheConstant.KYC_DOCUMENTS.toString(),
                kycDocumetationSet);
        DROP_DOWN_ENUMERATION.put("1500", institutionEnumeration);
    }

    /**
     *
     */
    private static void loadEmployType() {
        Set<String> loadEmploymentTypeSet = new HashSet<String>();

        loadEmploymentTypeSet.add(WordUtils.capitalizeFully("Self Employed"));
        loadEmploymentTypeSet.add(WordUtils.capitalizeFully("Others"));
        loadEmploymentTypeSet.add(WordUtils.capitalizeFully("Professional"));
        loadEmploymentTypeSet.add(WordUtils.capitalizeFully("Retired"));
        loadEmploymentTypeSet.add(WordUtils.capitalizeFully("Salaried"));
        Map<String, Set<String>> institutionEnumeration = DROP_DOWN_ENUMERATION
                .get("1500");
        institutionEnumeration.put(CacheConstant.EMPLOY_TYPE.toString(),
                loadEmploymentTypeSet);
        DROP_DOWN_ENUMERATION.put("1500", institutionEnumeration);
    }

    /**
     *
     */
    private static void loadConstitutions() {
        Set<String> constitutionsSet = new HashSet<String>();
        constitutionsSet.add(WordUtils.capitalizeFully("TRUST"));
        constitutionsSet.add(WordUtils.capitalizeFully("SELF - EMPLOYED"));
        constitutionsSet.add(WordUtils.capitalizeFully("SALARIED"));
        constitutionsSet.add(WordUtils.capitalizeFully("PARTNERSHIP"));
        constitutionsSet.add(WordUtils
                .capitalizeFully("PRIVATE LIMITED COMPANY"));

        Map<String, Set<String>> institutionEnumeration = DROP_DOWN_ENUMERATION
                .get("1500");
        institutionEnumeration.put(CacheConstant.CONSTITUTION.toString(),
                constitutionsSet);
        DROP_DOWN_ENUMERATION.put("1500", institutionEnumeration);
    }

    /**
     *
     */
    private static void loadAddressType() {
        Set<String> addressTypeSet = new HashSet<String>();
        addressTypeSet.add(WordUtils.capitalizeFully("ADDRESS TYPE"));
        addressTypeSet.add(WordUtils
                .capitalizeFully("SIGNATORY'S (AUTHORIZED) RESIDENCE"));
        addressTypeSet.add(WordUtils.capitalizeFully("CURRENT RESIDENCE"));
        addressTypeSet.add(WordUtils.capitalizeFully("HEAD OFFICE"));
        addressTypeSet.add(WordUtils.capitalizeFully("INVOICE ADDRESS"));
        addressTypeSet.add(WordUtils.capitalizeFully("OFFICE ADDRESS"));
        addressTypeSet.add(WordUtils.capitalizeFully("PERMANENT ADDRESS"));
        addressTypeSet.add(WordUtils.capitalizeFully("PERMENANT ADDRESS"));
        addressTypeSet.add(WordUtils.capitalizeFully("PREVIOUS OFFICE"));
        addressTypeSet.add(WordUtils.capitalizeFully("PREVIOUS RESIDENCE"));
        addressTypeSet.add(WordUtils.capitalizeFully("REFERENCE ADD 1"));
        addressTypeSet.add(WordUtils.capitalizeFully("REFERENCE ADD 2"));

        Map<String, Set<String>> institutionEnumeration = DROP_DOWN_ENUMERATION
                .get("1500");
        institutionEnumeration.put(CacheConstant.ADDRESS_TYPE.toString(),
                addressTypeSet);
        DROP_DOWN_ENUMERATION.put("1500", institutionEnumeration);
    }

    /**
     *
     */
    private static void loadGender() {
        Set<String> genderSet = new HashSet<String>();
        genderSet.add(WordUtils.capitalizeFully("Female"));
        genderSet.add(WordUtils.capitalizeFully("Male"));
        Map<String, Set<String>> institutionEnumeration = new TreeMap<String, Set<String>>();
        institutionEnumeration.put(CacheConstant.GENDER.toString(), genderSet);
        DROP_DOWN_ENUMERATION.put("1500", institutionEnumeration);
    }

}
