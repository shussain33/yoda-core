package com.softcell.gonogo.cache.services;

import com.softcell.gonogo.model.configuration.AuthSkipConfiguration;
import com.softcell.gonogo.model.security.AuthTokenPayLoad;

import java.util.List;
import java.util.Map;

public interface RedisService {

    void addToHashMapCache(String hSetName, String key, Object val);

    void addToSetCache(String key, Object val);

    boolean addToSetCache(String key, Object val, Long aLong);

    Object getHashMapCacheValues(String domain, String subKey);

    Object getSetCacheValues(String subKey);

    boolean removeKeysFromCache(String... subKey);

    // TODO: 22/7/20 This method are disabled in production
    //  boolean flushCache();

    boolean removeKeysFromCacheBasedPattern(String pattern);

    void masterUploadWithTransaction(Map<String, Map<String, Object>> mapHashMap, boolean isMasterUploaded);

    void setAuthConfig(AuthSkipConfiguration authSkipConfiguration);

    AuthSkipConfiguration getAuthConfig(String institutionID);

    boolean checkTokenIntoRedis(String authToken);

    void setToken(String authToken, long tokenExpTime, AuthTokenPayLoad authTokenPayLoad);

    List<String> getCacheKeys();

    void addToHashMapCacheAndTTL(String pixar_idfy, String imageName, Object kycRedisDetails, long expTime);

    List<Object> getAllHashMapValues(String hashMapName);
}
