package com.softcell.gonogo.cache.services;

import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationMongoRepository;
import com.softcell.gonogo.cache.redis.RedisConfig;
import com.softcell.gonogo.model.configuration.AuthSkipConfiguration;
import com.softcell.gonogo.model.security.AuthTokenPayLoad;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.JsonUtil;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.cluster.api.async.RedisAdvancedClusterAsyncCommands;
import io.lettuce.core.cluster.api.sync.RedisAdvancedClusterCommands;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import static com.softcell.constants.Constant.ALL_MATCH;

@Repository
public class RedisServiceImpl implements RedisService {

    private static final Logger log = LoggerFactory.getLogger(RedisServiceImpl.class);

    public static Long defaultExpireTime;
    public static boolean redisEnabled = false;
    private static boolean checkFromDB = false;
    public static int criticalThreshold=100;
    public static int warningThreshold=10;
    @Autowired
    private RedisAdvancedClusterAsyncCommands<String, Object> redisAdvancedClusterAsyncCommands;
    @Autowired
    private RedisAsyncCommands<String, Object> redisAsyncCommands;
    @Autowired
    private RedisAdvancedClusterCommands<String, Object> redisAdvancedClusterCommands;
    @Autowired
    private RedisCommands<String, Object> redisCommands;

    @Autowired
    private AppConfigurationMongoRepository configurationRepository;

    /**
     * stringRedisAsyncCommands and stringClusterAsyncCommands this redis
     * configuration use for OSOL functionality only
     */

    @Autowired
    @Qualifier("redis")
    private RedisAsyncCommands<String, String> stringRedisAsyncCommands;

    @Autowired
    @Qualifier("redisCluster")
    private RedisAdvancedClusterAsyncCommands<String, String> stringClusterAsyncCommands;


    public RedisServiceImpl() {
        log.warn("Inside RedisServiceImpl constructor");
        if (null != configurationRepository) {
            configurationRepository = new AppConfigurationMongoRepository();
            redisEnabled=configurationRepository.checkCacheEnable();
        }
        if (null== redisAdvancedClusterAsyncCommands){
            redisAdvancedClusterAsyncCommands = new RedisConfig().redisAdvancedClusterAsyncConnection();
        }
        if (null == redisAdvancedClusterCommands){
            redisAdvancedClusterCommands = new RedisConfig().redisAdvancedClusterSyncConnection();
        }
        if (null == redisAsyncCommands){
            redisAsyncCommands = new RedisConfig().redisAsyncConnection();
        }
        if (null == redisCommands){
            redisCommands = new RedisConfig().redisSyncConnection();
        }
        if (null == stringRedisAsyncCommands){
            stringRedisAsyncCommands = new RedisConfig().standaloneConnectionRedis();
        }
        if (null == stringClusterAsyncCommands){
            stringClusterAsyncCommands = new RedisConfig().ConnectionRedisCluster();
        }
    }


    @Override
    public void addToHashMapCache(String hashMapName, String hashMapSubKey, Object val) {
        log.warn("Inside addToHashMapCache method");
        if (checkCacheEnabled()) {
            if (StringUtils.isNotBlank(hashMapName) && StringUtils.isNotBlank(hashMapSubKey) && val != null) {
                log.warn("redis add: addToHashMapCache method invoke,domainKey [{}], key [{}]", hashMapName, hashMapSubKey);
                try {
                    Instant start = Instant.now();
                    RedisFuture<Boolean> hset = null;
                    if (redisAsyncCommands != null) {
                        hset = redisAsyncCommands.hset(hashMapName, hashMapSubKey, val);
                    } else if (redisAdvancedClusterAsyncCommands != null) {
                        hset = redisAdvancedClusterAsyncCommands.hset(hashMapName, hashMapSubKey, val);
                    }
                    if (hset.get().booleanValue()) {
                        log.warn("{}, addToHashMapCache:domainKey[{}], key [{}]", getCacheLog(start), hashMapName, hashMapSubKey);
                    } else {
                        log.warn("redis add: addToHashMapCache failed to add, hashMapName [{}], key [{}]", hashMapName, hashMapSubKey);
                    }
                } catch (Throwable ex) {
                    log.error("redis add server with key[{}] and subKey [{}] in method addToHashMapCache error: [{}] ", hashMapName, hashMapSubKey, ex);

                }
            } else {
                log.warn("redis add: provided key empty domainKey [{}], subKey [{}]", hashMapName, hashMapSubKey);
            }
        } else {
            log.warn("redis enabled: false");
        }
    }

    public void addToSetCache(String key, Object val) {
        addToSetCache(key, val, defaultExpireTime);
    }

    @Override
    public boolean addToSetCache(String key, Object val, Long aLong) {
        log.warn("Inside addToSetCache method for key and value {}, {}", key, val);
        boolean status = false;
        if (checkCacheEnabled()) {
            if (StringUtils.isNotBlank(key) && val != null) {
                try {
                    Instant start = Instant.now();
                    RedisFuture<String> setex = null;
                    if (redisAsyncCommands != null) {
                        setex = redisAsyncCommands.setex(key, aLong, val);
                    } else if (redisAdvancedClusterAsyncCommands != null) {
                        setex = redisAdvancedClusterAsyncCommands.setex(key, aLong, val);
                    }
                    if (setex != null && setex.get() != null && !setex.get().isEmpty()) {
                        status = true;
                        log.warn("{}, addToSetCache:Key[{}]", getCacheLog(start), key);
                    } else {
                        log.warn("redis add: failed to add, key [{}]", key);
                    }
                } catch (Throwable ex) {
                    log.error("redis add: server error: [{}] with key[{}]  in method addToSetCache", ex, key);

                }
            } else {
                log.warn("redis add: provided key against value empty [{}]", key);
            }
        } else {
            log.warn("redis enabled: false");
        }
        return status;
    }

    @Override
    public Object getHashMapCacheValues(String hashMapName, String hashMapSubKey) {
        log.warn("Inside getHashMapCacheValues method for map key {} & {}", hashMapName, hashMapSubKey);
        Object obj = null;
        if (checkCacheEnabled()) {
            try {
                Instant start = Instant.now();
                if (StringUtils.isNotBlank(hashMapSubKey) && StringUtils.isNotBlank(hashMapSubKey)) {
                    if (redisAdvancedClusterCommands != null) {
                        obj = redisAdvancedClusterCommands.hget(hashMapName, hashMapSubKey);
                    } else if (redisCommands != null) {
                        obj = redisCommands.hget(hashMapName, hashMapSubKey);
                    }
                } else {
                    log.warn("redis hget:  provided key empty domainKey [{}], subKey [{}]", hashMapName, hashMapSubKey);
                }
                log.warn("{}, getHashMapCacheValues:domainKey:[{}], subKey: [{}]", getCacheLog(start), hashMapName, hashMapSubKey);
            } catch (NullPointerException ex) {
                log.warn("redis hget: key not found: domainKey [{}], match [{}] in method getHashMapCacheValues", hashMapName, hashMapSubKey);

            } catch (Throwable ex) {
                log.error("redis hget: server error: [{}] with key[{}] and subKey [{}]", ex, hashMapName, hashMapSubKey);

            }
        } else {
            log.warn("redis enabled: false");
        }
        return obj;
    }

    @Override
    public Object getSetCacheValues(String key) {
        log.warn("Inside getSetCacheValues method for key {}", key);
        Object obj = null;
        if (checkCacheEnabled()) {
            try {
                Instant start = Instant.now();
                if (StringUtils.isNotBlank(key)) {
                    if (redisAdvancedClusterCommands != null) {
                        obj = redisAdvancedClusterCommands.get(key);
                    } else if (redisCommands != null) {
                        obj = redisCommands.get(key);
                    }
                } else {
                    log.warn("redis get: provided key: [{}] empty", key);
                }
                log.warn("{}, getSetCacheValues:key[{}]", getCacheLog(start), key);
            } catch (NullPointerException ex) {
                log.warn("redis get: key not found: [{}] in method getSetCacheValues", key);

            } catch (Throwable ex) {
                log.error("redis get: server error: [{}] with key[{}]", ex, key);

            }
        } else {
            log.warn("redis enabled: false");
        }

        return obj;
    }

    @Override
    public boolean removeKeysFromCache(String... keys) {
        log.warn("Inside removeKeysFromCache method");
        boolean status = false;
        if (checkCacheEnabled()) {
            try {
                Instant start = Instant.now();
                if (redisAsyncCommands != null) {
                    redisAsyncCommands.del(keys);
                } else if (redisAdvancedClusterAsyncCommands != null) {
                    redisAdvancedClusterAsyncCommands.del(keys);
                }
                log.warn("{}, removeKeysFromCache:key[{}]", getCacheLog(start), keys);
                status = true;
            } catch (Throwable ex) {
                log.error("redis server error: [{}]  in method masterUploadWithTransaction", ex);

            }
        } else {
            log.warn("redis enabled: false");
        }

        return status;
    }

    // TODO: 22/7/20 This method are disabled in production
/*    @Override
    @Trace(metricName = "flushCache", dispatcher = true)
    public boolean flushCache() {
        boolean status = false;
        if (checkCacheEnabled()) {
            RedisFuture<String> redisFuture = null;
            Instant start = Instant.now();
            try {
                if (redisAsyncCommands != null) {
                    redisFuture = redisAsyncCommands.flushall();
                } else if (redisAdvancedClusterAsyncCommands != null) {
                    redisFuture = redisAdvancedClusterAsyncCommands.flushall();
                }
                if (redisFuture != null) {
                    String s = redisFuture.get();
                    log.info("Flush status [{}]", s);
                    status = true;
                }

                log.warn("redis Time taken: [{}] Millis", Duration.between(start, Instant.now()).toMillis());
            } catch (Throwable ex) {
                log.error("redis server error: [{}]  in method flushCache", ex);

            }
        } else {
            log.info("redis enabled: false");
        }
        return status;
    }*/

    @Override
    public boolean removeKeysFromCacheBasedPattern(String pattern) {
        log.warn("Inside removeKeysFromCacheBasedPattern with pattern {}", pattern);

        boolean status = false;
        if (checkCacheEnabled()) {
            try {
                Instant start = Instant.now();
                List<String> strings;
                String[] keys = new String[0];
                RedisFuture<List<String>> redisFuture = null;
                if (redisAsyncCommands != null) {
                    redisFuture = redisAsyncCommands.keys(pattern);
                    strings = redisFuture.get();

                    log.warn("Inside removeKeysFromCacheBasedPattern with List of Keys {}", strings);

                    if (!strings.isEmpty()) {
                        keys = strings.toArray(new String[0]);

                        log.warn("Inside removeKeysFromCacheBasedPattern with Keys {}", keys);
                        redisAsyncCommands.del(keys);
                        status = true;

                        log.warn("Inside removeKeysFromCacheBasedPattern with SUCCESS");
                    }
                } else if (redisAdvancedClusterAsyncCommands != null) {
                    redisFuture = redisAdvancedClusterAsyncCommands.keys(pattern);
                    strings = redisFuture.get();
                    if (!strings.isEmpty()) {
                        keys = strings.toArray(new String[0]);

                        log.warn("Inside removeKeysFromCacheBasedPattern with Keys {}", keys);
                        redisAdvancedClusterAsyncCommands.del(keys);
                        status = true;

                        log.warn("Inside removeKeysFromCacheBasedPattern with SUCCESS.");
                    }
                }
                log.warn("{}, removeKeysFromCacheBasedPattern:key[{}]", getCacheLog(start), keys);
            } catch (Throwable ex) {
                log.error("redis server error: [{}]  in method masterUploadWithTransaction", ex);

            }
        } else {
            log.warn("redis enabled: false");
        }
        return status;
    }

    @Override
    public void masterUploadWithTransaction(Map<String, Map<String, Object>> mapHashMap, boolean isMasterUploaded) {
        log.warn("Inside masterUploadWithTransaction method");
        Instant start = Instant.now();
        if (checkCacheEnabled()) {
            try {
                if (isMasterUploaded) {
                    for (String key : mapHashMap.keySet()) {
                        if (redisCommands != null) {
                            redisCommands.del(key);
                        } else if (redisAdvancedClusterCommands != null) {
                            redisAdvancedClusterCommands.del(key);
                        }
                        log.warn("redis bulk upload: deleted hashName:[{}]", key);
                    }
                }
                if (mapHashMap != null && mapHashMap.size() != 0) {
                    for (Map.Entry<String, Map<String, Object>> entry : mapHashMap.entrySet()) {
                        String domainKey = entry.getKey();
                        Map<String, Object> subKeyValueMap = entry.getValue();
                        if (subKeyValueMap != null && subKeyValueMap.size() != 0) {
                            if (redisAsyncCommands != null) {
                                redisAsyncCommands.hmset(domainKey, subKeyValueMap);
                            } else if (redisAdvancedClusterAsyncCommands != null) {
                                redisAdvancedClusterAsyncCommands.hmset(domainKey, subKeyValueMap);
                            }
                            try {
                                log.warn("redis bulk upload: upload hashName key:[{}], size:[{}]", domainKey, subKeyValueMap.size());
                            } catch (Exception e) {
                                log.error("redis bulk upload: server error: [{}]", e.toString());
                            }
                        } else {
                            log.info("empty value of key [{}] unable to cached", domainKey);
                        }
                    }

                } else {
                    log.warn("empty list unable to cached");
                }
                log.warn("{} masterUploadWithTransaction:keys[{}]", getCacheLog(start),mapHashMap.keySet());
            } catch (Throwable ex) {
                log.error("redis bulk upload: server error: [{}]  in method masterUploadWithTransaction", ex);

            }
        } else {
            log.warn("redis enabled: false");
        }
    }

    @Override
    public void setAuthConfig(AuthSkipConfiguration authSkipConfiguration) {
        log.warn("Inside setAuthConfig method");
        if (checkCacheEnabled()) {
            RedisFuture<String> redisFuture = null;
            try {
                Instant start = Instant.now();

                if (stringRedisAsyncCommands != null) {

                    redisFuture = stringRedisAsyncCommands.set(authSkipConfiguration.getInstitutionId(), JsonUtil.ObjectToString(authSkipConfiguration));
                } else if (stringClusterAsyncCommands != null) {
                    redisFuture = stringClusterAsyncCommands.set(authSkipConfiguration.getInstitutionId(), JsonUtil.ObjectToString(authSkipConfiguration));
                }
                assert redisFuture != null;

                log.warn("{}, setAuth:key[{}]",getCacheLog(start),authSkipConfiguration.getInstitutionId());
            } catch (Exception ex) {
                log.error("Exception occured while setting auth config into redis:{}", ex);
            }
        } else {
            log.warn("redis enabled: false");
        }
    }

    @Override
    public AuthSkipConfiguration getAuthConfig(String institutionID) {
        log.warn("Inside getAuthConfig method for inst id {}", institutionID);
        AuthSkipConfiguration authSkipConfiguration = null;
        RedisFuture<String> redisFuture = null;
        if (checkCacheEnabled()) {
            try {
                Instant start = Instant.now();

                if (stringRedisAsyncCommands != null) {

                    redisFuture = stringRedisAsyncCommands.get(institutionID);
                } else if (stringClusterAsyncCommands != null) {
                    redisFuture = stringClusterAsyncCommands.get(institutionID);
                }
                if (redisFuture != null && StringUtils.isBlank(redisFuture.getError()) && StringUtils.isNotBlank(redisFuture.get())) {
                    String config = redisFuture.get();
                    authSkipConfiguration = JsonUtil.StringToObject(config, AuthSkipConfiguration.class);
                }

                log.warn("{}, getAuth:key[{}]",getCacheLog(start),institutionID);
            } catch (Exception ex) {
                log.error("Exception occured while fetching auth config from redis:{}", ex);
            }
        } else {
            log.warn("redis enabled: false");
        }

        return authSkipConfiguration;
    }

    @Override
    public boolean checkTokenIntoRedis(String authToken) {
        log.warn("Inside checkTokenIntoRedis method");
        boolean response = false;
        if (checkCacheEnabled()) {
            try {
                RedisFuture<String> redisFuture = null;
                Instant start = Instant.now();

                if (stringRedisAsyncCommands != null) {

                    redisFuture = stringRedisAsyncCommands.get(authToken);
                } else if (stringClusterAsyncCommands != null) {
                    redisFuture = stringClusterAsyncCommands.get(authToken);
                }
                if (redisFuture != null && StringUtils.isNotBlank(redisFuture.get())) {
                    response = true;
                }
                log.warn("{}, checkTokenIntoRedis:key[{}]",getCacheLog(start),authToken);
            } catch (Exception ex) {
                log.error("Exception occured while checking token into redis:{}", ex);
            }
        } else {
            log.warn("redis enabled: false");
        }
        return response;
    }


    public void setToken(String authToken, long exp, AuthTokenPayLoad authTokenPayLoad) {
        log.warn("Inside setToken method");
        if (checkCacheEnabled()) {
            try {
                Instant start = Instant.now();

                DateTime date = new DateTime(exp*1000);
                long dateSecond = GngDateUtil.getDateSecond(new DateTime(), date);
                if (stringRedisAsyncCommands != null) {
                    stringRedisAsyncCommands.setex(authToken,dateSecond, GNGWorkflowConstant.YES.name());
                }else if (stringClusterAsyncCommands != null) {
                    stringClusterAsyncCommands.setex(authToken,dateSecond, GNGWorkflowConstant.YES.name());
                }
                log.warn("{}, setToken:key{} with expiry ={}",getCacheLog(start),authToken,exp);
            } catch (Exception ex) {
                log.error("Exception occured while set token into redis:{}",ex);
            }
        } else {
            log.warn("redis enabled: false");
        }
    }

    @Override
    public List<String> getCacheKeys() {
        log.warn("Inside getCacheKeys method");
        List<String> keys = null;
        if (checkCacheEnabled()) {
            if (redisCommands != null) {
                keys = redisCommands.keys(ALL_MATCH);
            } else if (redisAdvancedClusterCommands != null) {
                keys = redisAdvancedClusterCommands.keys(ALL_MATCH);
            }
        } else {
            log.warn("redis enabled: false");
        }
        return keys;
    }

    private boolean checkCacheEnabled() {
        if (!checkFromDB) {
            redisEnabled=configurationRepository.checkCacheEnable();
            checkFromDB = true;
        }
        return redisEnabled;
    }
    private String getCacheLog(Instant start) {
        String logs=null;
        long toMillis = Duration.between(start, Instant.now()).toMillis();
        if (toMillis > warningThreshold && toMillis <= criticalThreshold) {
            logs = String.format("[%s] redis Time taken: [%s]ms", Constant.WARNING, toMillis);
        } else if (toMillis > criticalThreshold) {
            logs = String.format("[%s] redis Time taken: [%s]ms", Constant.CRITICAL, toMillis);
        } else {
            logs = String.format("[%s] redis Time taken: [%s]ms", Constant.NORMAL, toMillis);
        }
        return logs;
    }

    @Override
    public void addToHashMapCacheAndTTL(String hashMapName, String hashMapSubKey, Object val, long expTime) {
        log.warn("Inside addToHashMapCacheAndTTL method with map name, key & value {}, {}, {}", hashMapName, hashMapSubKey, val);
        if (StringUtils.isNotBlank(hashMapName) && StringUtils.isNotBlank(hashMapSubKey)) {
            log.warn("redis add: addToHashMapCacheAndTTL method invoke,domainKey [{}], key [{}]", hashMapName, hashMapSubKey);
            try {
                Instant start = Instant.now();

                RedisFuture<Boolean> redisFuture = null;
                if (redisAsyncCommands != null) {
                    redisFuture = redisAsyncCommands.hset(hashMapName, hashMapSubKey, val);
                    redisAsyncCommands.expire(hashMapName,expTime);

                } else if (redisAdvancedClusterAsyncCommands != null) {
                    redisFuture = redisAdvancedClusterAsyncCommands.hset(hashMapName, hashMapSubKey, val);
                    redisAdvancedClusterAsyncCommands.expire(hashMapName,expTime);
                }

                assert redisFuture != null;
                log.warn("redis add time [{}] Millis, domainKey[{}], key [{}], status[{}]", Duration.between(start, Instant.now()).toMillis(), hashMapName, hashMapSubKey, redisFuture.get());

            } catch (Exception ex) {
                log.error("redis add server error: [{}] with key[{}] and subKey [{}] in method addToHashMapCacheAndTTL", ex.toString(), hashMapName, hashMapSubKey);
            }
        } else {
            log.warn("redis add: provided key empty domainKey [{}], subKey [{}]", hashMapName, hashMapSubKey);
        }
    }

    @Override
    public List<Object> getAllHashMapValues(String hashMapName) {
        log.warn("Inside getAllHashMapValues method with value {}", hashMapName);
        List<Object> collect = null;
        try {
            if (StringUtils.isNotBlank(hashMapName)) {
                Instant start = Instant.now();

                if (redisAsyncCommands != null) {
                    collect = redisAsyncCommands.hvals(hashMapName).get();
                } else if (redisAdvancedClusterAsyncCommands != null) {
                    collect = redisAdvancedClusterAsyncCommands.hvals(hashMapName).get();
                }

                if (CollectionUtils.isEmpty(collect)) {
                    log.warn("redis hvals: keys not found, hashMapName[{}], time:[{}] Millis", hashMapName, Duration.between(start, Instant.now()).toMillis());
                } else {
                    log.warn("redis hvals: keys found, hashMapKey:[{}], time:[{}] Millis,", hashMapName, Duration.between(start, Instant.now()).toMillis());
                }
            } else {
                log.warn("redis hvals: sub keys empty domainKey[{}]", hashMapName);
            }
        } catch (Throwable ex) {
            log.error("redis hvals: server error: [{}] with key[{}]", ex.toString(), hashMapName);
        }
        return collect;
    }
}
