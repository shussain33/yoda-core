/**
 * yogeshb12:23:23 pm  Copyright Softcell Technology
 **/
package com.softcell.gonogo.service;

import java.io.IOException;

/**
 * @author yogeshb
 *
 */
public interface AuthServiceCaller {

    /**
     * This service will be used to login, authentication, reset and change password,
     * It will be taking input json which further be converted into map and post to traget service
     * @param inputJson
     * @param url
     * @return
     */
     String authService(String inputJson, String url) throws IOException;

    /**
     * It will be used to send json request to dmz servers
     *
     * @param inputJson
     * @param url
     * @return
     * @throws Exception
     */
    String postJsonRequest(String inputJson, String url) throws Exception;
}
