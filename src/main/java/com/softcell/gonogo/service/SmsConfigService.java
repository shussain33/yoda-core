package com.softcell.gonogo.service;

import com.softcell.config.sms.GngSmsServiceConfiguration;
import com.softcell.gonogo.model.response.core.BaseResponse;

import java.util.List;

/**
 * Serivces will be use to perform SMSConfig CRUD operations
 *
 * @author bhuvneshk
 */

public interface SmsConfigService {

    List<BaseResponse> insertSmsConfiguration(List<GngSmsServiceConfiguration> smsConfigList);

    List<BaseResponse> updateSmsConfiguration(List<GngSmsServiceConfiguration> smsConfigList);

    List<BaseResponse> deleteSmsConfiguration(List<GngSmsServiceConfiguration> smsConfigList);

    BaseResponse readSmsConfigByInstitution(String institutionId);

    BaseResponse readActiveSmsConfigByInstitution(String institutionId);
}
