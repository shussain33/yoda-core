package com.softcell.gonogo.service.metadata.impl;

import com.softcell.config.ComponentConfiguration;
import com.softcell.dao.mongodb.repository.metadata.ComponentConfigRepository;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Status;
import com.softcell.gonogo.service.metadata.ComponentConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ComponentConfigServiceImpl implements ComponentConfigService {

    @Autowired
    ComponentConfigRepository configRepository;

    BaseResponse fail;

    BaseResponse success;

    public ComponentConfigServiceImpl() {

        fail = new BaseResponse();
        Status statusFail = new Status();
        statusFail.setStatusCode(HttpStatus.BAD_REQUEST.value());
        statusFail.setStatusValue(HttpStatus.BAD_REQUEST.name());
        fail.setStatus(statusFail);

        success = new BaseResponse();
        Status statusSuccess = new Status();
        statusSuccess.setStatusCode(HttpStatus.OK.value());
        statusSuccess.setStatusValue(HttpStatus.OK.name());
        success.setStatus(statusSuccess);
    }

    @Override
    public List<BaseResponse> insertComponentConfiguration(
            List<ComponentConfiguration> componentConfigurations) {
        List<BaseResponse> baseResponse = new ArrayList<BaseResponse>();
        for (ComponentConfiguration componentConfiguration : componentConfigurations) {
            if (vaildateRequest(componentConfiguration)
                    && configRepository
                    .insertComponentConfiguration(componentConfiguration)) {
                baseResponse.add(success);
            } else {
                baseResponse.add(fail);
            }
        }
        return baseResponse;
    }

    @Override
    public List<BaseResponse> updateComponentConfiguration(
            List<ComponentConfiguration> componentConfigurations) {

        return null;
    }

    @Override
    public List<BaseResponse> deleteComponentConfiguration(
            List<ComponentConfiguration> componentConfigurations) {
        return null;
    }

    @Override
    public BaseResponse readComponentConfigByInstitution(String institutionId) {
        return null;
    }

    @Override
    public BaseResponse readActiveComponentConfigByInstitution(
            String institutionId) {
        return null;
    }

    private boolean vaildateRequest(ComponentConfiguration componentConfig) {
        if (StringUtils.isNotBlank(componentConfig.getInstitutionId())
                && componentConfig.getComponentSettings() != null
                && componentConfig.getComponentSettings().getComponentMap() != null
                && !componentConfig.getComponentSettings().getComponentMap()
                .isEmpty()) {
            return true;
        }
        return false;
    }
}
