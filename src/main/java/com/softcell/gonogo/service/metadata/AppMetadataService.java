package com.softcell.gonogo.service.metadata;

import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * This Service will be use to do all CRUD operation specific to Application app
 * Like action , email , sms service app based on
 * Created by bhuvneshk on 16/1/17.
 */
public interface AppMetadataService {


    BaseResponse insertActionConfiguration(ActionConfiguration actionConfig);

    BaseResponse updateActionConfiguration(ActionConfiguration actionConfig);

    BaseResponse deleteActionConfiguration(ActionConfiguration actionConfig);

    BaseResponse readActionByInstitution(String institutionId);

    BaseResponse readActiveActionByInstitution(String institutionId);

    BaseResponse findAllActionConfiguration(String institutionId, String productId ,boolean active);
}