package com.softcell.gonogo.service.metadata;

import com.softcell.config.ComponentConfiguration;
import com.softcell.gonogo.model.response.core.BaseResponse;

import java.util.List;

/**
 * workflow related component will be use to perform SMSConfig CRUD operations
 *
 * @author bhuvneshk
 */
public interface ComponentConfigService {

    List<BaseResponse> insertComponentConfiguration(List<ComponentConfiguration> componentConfigurations);

    List<BaseResponse> updateComponentConfiguration(List<ComponentConfiguration> componentConfigurations);

    List<BaseResponse> deleteComponentConfiguration(List<ComponentConfiguration> componentConfigurations);

    BaseResponse readComponentConfigByInstitution(String institutionId);

    BaseResponse readActiveComponentConfigByInstitution(String institutionId);
}
