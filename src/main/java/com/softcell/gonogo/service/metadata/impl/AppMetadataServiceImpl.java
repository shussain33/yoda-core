package com.softcell.gonogo.service.metadata.impl;


import com.softcell.constants.CacheName;
import com.softcell.dao.mongodb.lookup.LookupDao;
import com.softcell.dao.mongodb.repository.metadata.AppMetadataRepository;
import com.softcell.gonogo.cache.UpdatedCache;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.gonogo.model.response.core.Status;
import com.softcell.gonogo.service.metadata.AppMetadataService;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by bhuvneshk on 16/1/17.
 */

@Service
public class AppMetadataServiceImpl implements AppMetadataService {

    @Autowired
    AppMetadataRepository appMetadataRepository;
    BaseResponse fail;
    BaseResponse success;
    @Autowired
    private UpdatedCache updatedCache;
    @Autowired
    private LookupDao lookupDao;

    public AppMetadataServiceImpl() {

        fail = new BaseResponse();
        Status statusFail = new Status();
        statusFail.setStatusCode(HttpStatus.BAD_REQUEST.value());
        statusFail.setStatusValue(HttpStatus.BAD_REQUEST.name());
        fail.setStatus(statusFail);

        success = new BaseResponse();
        Status statusSuccess = new Status();
        statusSuccess.setStatusCode(HttpStatus.OK.value());
        statusSuccess.setStatusValue(HttpStatus.OK.name());
        success.setStatus(statusSuccess);
    }

    @Override
    public BaseResponse insertActionConfiguration(ActionConfiguration actionConfig) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.ACTION.name(),
                actionConfig.getInstitutionId(),
                actionConfig.getProductId(),
                actionConfig.getActionName().name());

        if (actionConfig != null) {

            if (vaildateRequest(actionConfig)
                    && !appMetadataRepository
                    .isActionConfigExist(actionConfig)) {

                appMetadataRepository
                        .insertActionConfiguration(actionConfig);

                updatedCache.CACHE.put(lookupKey, lookupDao.actionConfigurationReferrer(
                        actionConfig.getInstitutionId(),
                        actionConfig.getProductId(),
                        actionConfig.getActionName()));

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, actionConfig);

            } else {

                baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, actionConfig);
            }
        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, actionConfig);
        }

        return baseResponse;

    }

    @Override
    public BaseResponse updateActionConfiguration(ActionConfiguration actionConfig) {

        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.ACTION.name(),
                actionConfig.getInstitutionId(),
                actionConfig.getProductId(),
                actionConfig.getActionName().name());

        if (actionConfig != null) {

            if (vaildateRequest(actionConfig)
                    && appMetadataRepository
                    .isActionConfigExist(actionConfig)
                    && appMetadataRepository
                    .updateActionConfiguration(actionConfig)) {

                List<ActionConfiguration> actionConfigurations = lookupDao.actionConfigurationReferrer(
                        actionConfig.getInstitutionId(),
                        actionConfig.getProductId(),
                        actionConfig.getActionName());

                if (!CollectionUtils.isEmpty(actionConfigurations)) {
                    updatedCache.CACHE.put(lookupKey, actionConfigurations);
                }

                baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, actionConfig);

            } else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, actionConfig);

            }

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, actionConfig);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse deleteActionConfiguration(ActionConfiguration actionConfig) {
        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.ACTION.name(),
                actionConfig.getInstitutionId(),
                actionConfig.getProductId(),
                actionConfig.getActionName().name());

        if (vaildateRequest(actionConfig)
                && appMetadataRepository
                .deleteActionConfiguration(actionConfig)) {

            updatedCache.CACHE.remove(lookupKey);

            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, actionConfig);

        } else {

            baseResponse = GngUtils.getBaseResponse(HttpStatus.NOT_MODIFIED, actionConfig);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse readActionByInstitution(String institutionId) {
        BaseResponse baseResponse = fail;
        if (institutionId != null
                && StringUtils.isNotBlank(institutionId)
                && appMetadataRepository
                .readActionByInstitution(institutionId) != null
                && !appMetadataRepository.readActionByInstitution(
                institutionId).isEmpty()) {
            Payload<List<String>> payload = new Payload<List<String>>(
                    appMetadataRepository
                            .readActionByInstitution(institutionId));
            success.setPayload(payload);
            baseResponse = success;
        }
        return baseResponse;
    }

    @Override
    public BaseResponse readActiveActionByInstitution(String institutionId) {
        BaseResponse baseResponse = fail;
        if (institutionId != null
                && StringUtils.isNotBlank(institutionId)
                && appMetadataRepository
                .readActionByInstitution(institutionId) != null
                && !appMetadataRepository.readActionByInstitution(
                institutionId).isEmpty()) {
            Payload<List<String>> payload = new Payload<List<String>>(
                    appMetadataRepository
                            .readActiveActionByInstitution(institutionId));
            success.setPayload(payload);
            baseResponse = success;
        }
        return baseResponse;
    }

    /**
     * It is util method to validate request for mandatoryFields
     *
     * @param actionConfig
     * @return
     */
    private boolean vaildateRequest(
            ActionConfiguration actionConfig) {
        if (StringUtils.isNotBlank(actionConfig
                .getInstitutionId())
                && StringUtils.isNotBlank(actionConfig
                .getProductId())) {
            return true;
        }
        return false;
    }

    @Override
    public BaseResponse findAllActionConfiguration(String institutionId, String productId ,boolean active) {

        BaseResponse baseResponse;
        List<ActionConfiguration> allActionConfiguration = appMetadataRepository.getAllActionConfiguration(institutionId, productId ,active);

        if (!CollectionUtils.isEmpty(allActionConfiguration)) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, allActionConfiguration);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, GngUtils.getNoContentErrorList());
        }
        return baseResponse;

    }
}