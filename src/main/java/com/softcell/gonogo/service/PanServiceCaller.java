package com.softcell.gonogo.service;

import com.softcell.config.PanCommunicationDomain;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponse;

/**
 * @author yogeshb
 */
public interface PanServiceCaller {

    PanResponse callPanService(PanRequest panRequest,
                                      PanCommunicationDomain panComunicationDomain);

}
