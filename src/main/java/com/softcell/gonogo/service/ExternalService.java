package com.softcell.gonogo.service;

import java.io.IOException;
import java.util.Map;

/**
 * Created by prateek on 1/3/17.
 */
public interface ExternalService {

    /**
     *
     * @param url
     * @param postData
     * @return
     * @throws IOException
     */
    String postRequest(String url, Map<String, String> postData) throws IOException ;

}
