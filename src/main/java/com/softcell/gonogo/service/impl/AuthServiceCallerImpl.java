package com.softcell.gonogo.service.impl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.softcell.gonogo.service.AuthServiceCaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yogeshb
 */
@Service
public class AuthServiceCallerImpl implements AuthServiceCaller {

    private static final Logger logger = LoggerFactory.getLogger(AuthServiceCallerImpl.class);



    /**
     * @param inputJson
     * @param url
     * @return
     */
    @Override
    public String authService(String inputJson, String url) throws IOException {

        logger.debug("authService started ");

        try {

            Map<String, String> postMap = new HashMap<String, String>() {{
                put("request", inputJson);
            }};

            String response = new HttpTransportationService().postRequest(url, postMap);

            logger.debug("auth service successfully completed");

            return response;

        } catch (IOException e) {

            logger.error(e.getMessage());

            throw  new IOException(e.getMessage());
        }
    }


    @Override
    public String postJsonRequest(String inputJson, String url) throws Exception {

        logger.debug("postJsonRequest started ");

        try {

            String response = new HttpTransportationService().postRequest(url, inputJson, MediaType.APPLICATION_JSON_VALUE);

            logger.debug("postJsonRequest successfully completed ");

            return response;

        } catch (JsonParseException e) {

            logger.error(e.getMessage());

            throw new Exception(e.getMessage());

        } catch (JsonMappingException e) {

            logger.error(e.getMessage());

            throw new Exception(e.getMessage());

        } catch (IOException e) {

            logger.error(e.getMessage());

            throw new Exception(e.getMessage());

        } catch (Exception e) {

            logger.error("AuthServiceCallerImpl.postJsonRequest exception "+e.getMessage());

            throw new Exception("AuthServiceCallerImpl.postJsonRequest exception "+e.getMessage());
        }

    }
}
