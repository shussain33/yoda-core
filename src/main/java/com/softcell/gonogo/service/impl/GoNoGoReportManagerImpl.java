package com.softcell.gonogo.service.impl;

import com.mongodb.BasicDBObject;
import com.softcell.constants.Institute;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.gonogo.model.masters.DropdownMaster;
import com.softcell.gonogo.model.reports.request.GoNoGoReportRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.GoNoGoReportResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.reporting.report.GoNoGoReportHelper;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.GoNoGoReportManager;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class GoNoGoReportManagerImpl implements GoNoGoReportManager {

    private static final Logger logger = LoggerFactory.getLogger(GoNoGoReportManagerImpl.class);

    public static final String CSV_SEPARATOR = ",";
    public static final String END_OF_LINE = "\n";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy.hh.mm.ss");

    @Autowired
    GoNoGoReportHelper goNoGoReportHelper;

    @Autowired
    MasterRepository masterRepository;

    @Override
    public BaseResponse downloadReportsDump(GoNoGoReportRequest goNoGoReportRequest) {
        logger.debug("downloadReportsDump started");
        BaseResponse baseResponse = null;
        GoNoGoReportResponse goNoGoReportResponse = new GoNoGoReportResponse();
        boolean allEmpty = true;
        try {
            List<List<LinkedHashMap<String, String>>> result = null;
            List<byte[]> data = null;
            Date startDate = DateUtils.addMinutes(goNoGoReportRequest.getValueDateFrom(), 330);
            Date endDate = DateUtils.addMinutes(goNoGoReportRequest.getValueDateTo(), 1769);
            Header header = goNoGoReportRequest.getHeader();
            String fileName = goNoGoReportRequest.getReportName();
            List<List<BasicDBObject>> multipleReports = null;

            multipleReports = goNoGoReportHelper.findRecordsBetweenDates(startDate, endDate, header,fileName);
            if(multipleReports != null){
                for(List<BasicDBObject> list:multipleReports){
                    if(list != null && !list.isEmpty()){
                        allEmpty = false;
                        break;
                    }
                }
            }

            if(CollectionUtils.isNotEmpty(multipleReports) && !allEmpty){
                logger.debug("multipleReports count {}",multipleReports.size());
                result = goNoGoReportHelper.createReportByName(multipleReports,fileName);

                if(CollectionUtils.isNotEmpty(result)){
                    logger.debug("result count {}",result.size());
                    data = createCSV(result);

                    if(CollectionUtils.isNotEmpty(data)){
                        logger.debug("data count {}",data.size());
                        goNoGoReportResponse.setFormat("CSV");
                        goNoGoReportResponse.setExcelName(fileName + " " + dateFormat.format(new Date().getTime()));
                        goNoGoReportResponse.setFileContent(data);
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, goNoGoReportResponse);
                    }else{
                        baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "NO RECORD FOUND");
                    }
                }else {
                    baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "NO RECORD FOUND");
                }
            }else {
                baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT, "NO RECORD FOUND");
            }
        }catch (Exception ex){
            logger.error("Error While Fetching Reports {}", ExceptionUtils.getStackTrace(ex));
            baseResponse = GngUtils.getBaseResponse(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.debug("downloadReportsDump ended");
        return baseResponse;
    }

    @Override
    public BaseResponse getAllReportsName(GoNoGoReportRequest goNoGoReportRequest) {
        BaseResponse baseResponse = null;
        List<String> reportsName = new ArrayList<>();
        if(goNoGoReportRequest.getHeader() !=null && Institute.isInstitute(goNoGoReportRequest.getHeader().getInstitutionId(), Institute.SBFC)){
            String product = goNoGoReportRequest.getHeader().getProduct().name();
            String institutionId = goNoGoReportRequest.getHeader().getInstitutionId();

            List<DropdownMaster> dropdownMasterList = masterRepository.fetchDropDownMaster(
                    institutionId, GoNoGoReportHelper.GONOGO_REPORT_DROPDOWN_TYPE,
                    EndPointReferrer.ONE, product);

            for(DropdownMaster dropdownMaster:dropdownMasterList){
                for(Map<String,String> map:dropdownMaster.getDropDownValueDetailsList()){
                    if(StringUtils.equalsIgnoreCase(product,map.get("Product"))){
                        reportsName.add(map.get("reportName"));
                    }
                }
            }
        }
        baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, reportsName);
        return baseResponse;
    }

    private List<byte[]> createCSV(List<List<LinkedHashMap<String, String>>> multipleReport) {
        logger.debug("createCSV started");

        List<byte[]> output = new ArrayList<>();

        for(List<LinkedHashMap<String, String>> report:multipleReport){
            if(CollectionUtils.isNotEmpty(report)){
                try {
                    StringBuilder header = new StringBuilder();
                    LinkedHashMap<String, String> headers = report.get(0);
                    for (Map.Entry<String, String> entry : headers.entrySet()) {
                        header.append(entry.getKey()).append(CSV_SEPARATOR);
                    }
                    StringBuilder builder = new StringBuilder();
                    builder.append(header);
                    for (LinkedHashMap<String, String> temp : report) {
                        builder.append(END_OF_LINE);
                        for (Map.Entry<String, String> entry : temp.entrySet()) {
                            builder.append(entry.getValue());
                            builder.append(CSV_SEPARATOR);
                        }
                    }
                    String data = builder.toString();
                    output.add(data.getBytes());
                }catch (Exception ex){
                    logger.debug("Error while converting into CSV {}",ex.getMessage());
                    ex.printStackTrace();
                }
            }else{
                output.add(new byte[0]);
            }
        }
        logger.debug("createCSV ended");
        return output;
    }
}
