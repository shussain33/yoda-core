package com.softcell.gonogo.service.impl;

import com.softcell.config.NTCConfiguration;
import com.softcell.constants.FieldSeparator;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.ntc.NTCResponse;
import com.softcell.gonogo.model.ntc.RequestDomain;
import com.softcell.gonogo.service.NTCService;
import com.softcell.gonogo.service.factory.NTCEntityBuilder;
import com.softcell.gonogo.service.factory.impl.NTCEntityBuilderImpl;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;


/**
 * Implements NTC service methods
 *
 * @author bhuvneshk
 */

@Service
public class NTCServiceImpl implements NTCService {

    private static final Logger logger = LoggerFactory.getLogger(NTCServiceImpl.class);

    @Autowired
    private NTCEntityBuilder ntcEntityBuilder;


     public NTCServiceImpl(){

         if(ntcEntityBuilder == null){
             ntcEntityBuilder = new NTCEntityBuilderImpl();
         }

     }



    @Override
    public NTCResponse callNTC(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception {

        RequestDomain request = ntcEntityBuilder.build(goNoGoCustomerApplication);

        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        NTCResponse ntcResponse = callNTCService(request, institutionId);

        return ntcResponse;
    }


    private NTCResponse callNTCService(RequestDomain request, String institutionId) throws Exception {

        NTCResponse ntcResponse = null;

        NTCConfiguration ntcConfiguration = Cache.URL_CONFIGURATION.getNtcConfiguration().get(institutionId);

        if(null == ntcConfiguration){

            logger.warn(" communication domain for NTC is not registered with institutionId {} ", institutionId);

            return null;

        }



        String ntcJsonRequest = JsonUtil.ObjectToString(request);

        try {

            String url = ntcConfiguration.getNtcUrl();

            String tempResponse = new HttpTransportationService().postRequest(url, new HashMap<String, String>() {{
                put("INSTITUTION_ID", ntcConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", ntcConfiguration.getAggregatorId());
                put("MEMBER_ID", ntcConfiguration.getMemberId());
                put("PASSWORD", ntcConfiguration.getPassword());
                put("inputJson_", ntcJsonRequest);
            }});

            if (StringUtils.isNotBlank(tempResponse)) {

                ntcResponse = JsonUtil.StringToObject(tempResponse, NTCResponse.class);
            }

            return ntcResponse;

        } catch (Exception e) {

            e.printStackTrace();

            logger.error("Exception Occurred while send request to ntc service by cause {}", e.getMessage());

            throw new Exception(String.format("error occurred while invoking ntc service with probable cause [{}]", e.getMessage()));
        }

    }

}
