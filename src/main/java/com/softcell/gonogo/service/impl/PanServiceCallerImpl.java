package com.softcell.gonogo.service.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.PanCommunicationDomain;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponse;
import com.softcell.gonogo.service.PanServiceCaller;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


/**
 * @author yogeshb
 */
@Service
public class PanServiceCallerImpl implements PanServiceCaller {
    private Logger logger = LoggerFactory.getLogger(PanServiceCallerImpl.class);

    @Override
    public PanResponse callPanService(PanRequest panRequest,
                                      PanCommunicationDomain panComunicationDomain) {
        String panJsonRequest = getRequestJson(panRequest);
        String panUrl = panComunicationDomain.getPanUrl();
        ArrayList<NameValuePair> postParameters = getPostParameter(
                panComunicationDomain, panJsonRequest);

        if (panJsonRequest != null && panUrl != null && postParameters != null) {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = null;
            HttpResponse response = null;
            HttpEntity entity = null;
            InputStream content = null;
            try {
                httpPost = new HttpPost(panUrl);
                httpPost.setEntity(new UrlEncodedFormEntity(postParameters));
                response = client.execute(httpPost);
                entity = response.getEntity();
                content = entity.getContent();
                return getAadharObject(getResponseJson(new BufferedReader(
                        new InputStreamReader(content))));
            } catch (IOException e) {
                logger.error("Error in IOException: " + e);
                return null;
            } finally {
                if (null != content) {
                    try {
                        content.close();
                    } catch (IOException e) {
                        logger.error("Error in IOException: " + e);
                    }
                }
                if (null != httpPost) {
                    httpPost.releaseConnection();
                }
            }
        }
        return null;
    }

    private PanResponse getAadharObject(String responseJson) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(responseJson, PanResponse.class);
        } catch (JsonParseException e) {
            logger.error("Error in JsonParseException: " + e);
            return null;
        } catch (JsonMappingException e) {
            logger.error("Error in JsonMappingException: " + e);
            return null;
        } catch (IOException e) {
            logger.error("Error in IOException: " + e);
            return null;
        }
    }

    private ArrayList<NameValuePair> getPostParameter(
            PanCommunicationDomain panComunicationDomain, String panJsonRequest) {
        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("INSTITUTION_ID",
                panComunicationDomain.getInstitutionId()));
        postParameters.add(new BasicNameValuePair("AGGREGATOR_ID",
                panComunicationDomain.getAggregatorId()));
        postParameters.add(new BasicNameValuePair("MEMBER_ID",
                panComunicationDomain.getMemberId()));
        postParameters.add(new BasicNameValuePair("PASSWORD",
                panComunicationDomain.getPassword()));
        postParameters
                .add(new BasicNameValuePair("inputJson_", panJsonRequest));
        return postParameters;
    }

    private String getRequestJson(PanRequest panRequest) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(panRequest);
        } catch (JsonGenerationException e1) {
            logger.error("Error in JsonGenerationException: " + e1);
            return null;
        } catch (JsonMappingException e1) {
            logger.error("Error in JsonMappingException: " + e1);
            return null;
        } catch (IOException e1) {
            logger.error("Error in IOException: " + e1);
            return null;
        }
    }

    /**
     * @param rd
     * @return This method returns Response json from Buffered Reader.
     */
    private String getResponseJson(BufferedReader rd) {
        StringBuilder tempResponse = new StringBuilder();
        String line;
        try {
            while ((line = rd.readLine()) != null) {
                if (StringUtils.isBlank(tempResponse)) {
                    tempResponse.append(line);
                } else {
                    tempResponse.append(tempResponse).append(" ").append(line);
                }
            }
            return tempResponse.toString();
        } catch (IOException e) {
            logger.error("Error in IOException: " + e);
            return null;
        }
    }

}
