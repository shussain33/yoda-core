package com.softcell.gonogo.service.impl;

import com.softcell.config.sms.GngSmsServiceConfiguration;
import com.softcell.dao.mongodb.repository.metadata.SmsConfigRepository;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Payload;
import com.softcell.gonogo.model.response.core.Status;
import com.softcell.gonogo.service.SmsConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SmsConfigServiceImpl implements SmsConfigService {

    @Autowired
    SmsConfigRepository smsConfigRepository;

    BaseResponse fail;

    BaseResponse success;

    public SmsConfigServiceImpl() {

        fail = new BaseResponse();
        Status statusFail = new Status();
        statusFail.setStatusCode(HttpStatus.BAD_REQUEST.value());
        statusFail.setStatusValue(HttpStatus.BAD_REQUEST.name());
        fail.setStatus(statusFail);

        success = new BaseResponse();
        Status statusSuccess = new Status();
        statusSuccess.setStatusCode(HttpStatus.OK.value());
        statusSuccess.setStatusValue(HttpStatus.OK.name());
        success.setStatus(statusSuccess);
    }

    @Override
    public List<BaseResponse> insertSmsConfiguration(
            List<GngSmsServiceConfiguration> smsConfigList) {

        List<BaseResponse> baseResponse = new ArrayList<BaseResponse>();

        if (smsConfigList != null && !smsConfigList.isEmpty()) {
            for (GngSmsServiceConfiguration gngSmsServiceConfiguration : smsConfigList) {
                if (vaildateRequest(gngSmsServiceConfiguration)
                        && !smsConfigRepository
                        .isSmsConfigExist(gngSmsServiceConfiguration)
                        && smsConfigRepository
                        .insertSmsConfiguration(gngSmsServiceConfiguration)) {
                    baseResponse.add(success);
                } else {
                    baseResponse.add(fail);
                }
            }
        } else {

            baseResponse.add(fail);
        }

        return baseResponse;
    }

    @Override
    public List<BaseResponse> updateSmsConfiguration(
            List<GngSmsServiceConfiguration> smsConfigList) {

        List<BaseResponse> baseResponse = new ArrayList<BaseResponse>();

        if (smsConfigList != null && !smsConfigList.isEmpty()) {
            for (GngSmsServiceConfiguration gngSmsServiceConfiguration : smsConfigList) {
                if (vaildateRequest(gngSmsServiceConfiguration)
                        && smsConfigRepository
                        .isSmsConfigExist(gngSmsServiceConfiguration)
                        && smsConfigRepository
                        .updateSmsConfiguration(gngSmsServiceConfiguration)) {
                    baseResponse.add(success);
                } else {
                    baseResponse.add(fail);
                }
            }
        } else {

            baseResponse.add(fail);
        }

        return baseResponse;
    }

    @Override
    public List<BaseResponse> deleteSmsConfiguration(
            List<GngSmsServiceConfiguration> smsConfigList) {
        List<BaseResponse> baseResponse = new ArrayList<BaseResponse>();

        if (smsConfigList != null && !smsConfigList.isEmpty()) {
            for (GngSmsServiceConfiguration gngSmsServiceConfiguration : smsConfigList) {
                if (vaildateRequest(gngSmsServiceConfiguration)
                        && smsConfigRepository
                        .isSmsConfigExist(gngSmsServiceConfiguration)
                        && smsConfigRepository
                        .deleteSmsConfiguration(gngSmsServiceConfiguration)) {
                    baseResponse.add(success);
                } else {
                    baseResponse.add(fail);
                }
            }
        } else {

            baseResponse.add(fail);
        }

        return baseResponse;
    }

    @Override
    public BaseResponse readSmsConfigByInstitution(String institutionId) {
        BaseResponse baseResponse = fail;
        if (institutionId != null
                && StringUtils.isNotBlank(institutionId)
                && smsConfigRepository
                .readSmsConfigByInstitution(institutionId) != null
                && !smsConfigRepository.readSmsConfigByInstitution(
                institutionId).isEmpty()) {
            Payload<List<GngSmsServiceConfiguration>> payload = new Payload<List<GngSmsServiceConfiguration>>(
                    smsConfigRepository
                            .readSmsConfigByInstitution(institutionId));
            success.setPayload(payload);
            baseResponse = success;
        }
        return baseResponse;
    }

    @Override
    public BaseResponse readActiveSmsConfigByInstitution(String institutionId) {
        BaseResponse baseResponse = fail;
        if (institutionId != null
                && StringUtils.isNotBlank(institutionId)
                && smsConfigRepository
                .readSmsConfigByInstitution(institutionId) != null
                && !smsConfigRepository.readSmsConfigByInstitution(
                institutionId).isEmpty()) {
            Payload<List<GngSmsServiceConfiguration>> payload = new Payload<List<GngSmsServiceConfiguration>>(
                    smsConfigRepository
                            .readActiveSmsConfigByInstitution(institutionId));
            success.setPayload(payload);
            baseResponse = success;
        }
        return baseResponse;
    }

    /**
     * It is util method to validate request for mandatoryFields
     *
     * @param gngSmsServiceConfiguration
     * @return
     */
    private boolean vaildateRequest(
            GngSmsServiceConfiguration gngSmsServiceConfiguration) {
        if (StringUtils.isNotBlank(gngSmsServiceConfiguration
                .getInstitutionId())
                && StringUtils.isNotBlank(gngSmsServiceConfiguration
                .getProductId())
                && StringUtils.isNotBlank(gngSmsServiceConfiguration
                .getVendorName())) {
            return true;
        }
        return false;
    }
}
