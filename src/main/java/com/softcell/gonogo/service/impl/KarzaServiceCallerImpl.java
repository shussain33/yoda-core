package com.softcell.gonogo.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.config.KarzaConfiguration;
import com.softcell.config.KarzaProcessingInfo;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Institute;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationMongoRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAPIRequest;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAuthenticationRequest;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaRequestData;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaRequestKeyHeader;
import com.softcell.gonogo.model.core.kyc.response.karza.*;
import com.softcell.gonogo.model.core.kyc.response.karza.kscan.DistrictCourt;
import com.softcell.gonogo.model.core.kyc.response.karza.kscan.LitigationsSearchResponseDetails;
import com.softcell.gonogo.model.core.perfios.BankData;
import com.softcell.gonogo.model.core.perfios.PerfiosData;
import com.softcell.gonogo.model.core.request.ThirdPartyRequest;
import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.ExternalServiceCalls;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.KarzaServiceCaller;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;

import com.softcell.service.impl.KarzaRequestHelper;
import com.softcell.service.impl.KarzaResponseHelper;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by yogesh Khandare on 7/10/18.
 */
@Service
public class KarzaServiceCallerImpl implements KarzaServiceCaller {

    private static final Logger logger = LoggerFactory.getLogger(KarzaServiceCallerImpl.class);

    @Autowired
    private HttpTransportationService httpTransportationService;

    @Autowired
    private ConfigurationRepository configurationRepository;

    @Autowired
    private ExternalAPILogRepository externalAPILogRepository;

    @Autowired
    private KarzaResponseHelper karzaResponseHelper;

    @Autowired
    private KarzaRequestHelper karzaRequestHelper;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    private Map<Applicant, List<KarzaProcessingInfo>> applicantkarzaProcessingInfoList;


    public KarzaServiceCallerImpl(MongoTemplate mongoTemplate) {

        if(httpTransportationService == null) {
            httpTransportationService = new HttpTransportationService();
        }

        if(configurationRepository == null) {
            configurationRepository = new ConfigurationMongoRepository(mongoTemplate);
        }

        if(externalAPILogRepository == null) {
            externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(mongoTemplate);
        }

        if(karzaResponseHelper == null) {
            karzaResponseHelper = new KarzaResponseHelper();
        }

        if(karzaRequestHelper == null) {
            karzaRequestHelper = new KarzaRequestHelper();
        }
    }

    public VoterIdResponse callvoterIdKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception {

        VoterIdResponse voterIdResponse=null;

        String voterIdKarzaJsonRequest = JsonUtil.ObjectToString(karzaAuthenticationRequest);

        logger.info("VoterId Karza JsonRequest{} ",voterIdKarzaJsonRequest);

        try {

            if (null == karzaConfiguration) {
                logger.warn(" communication domain for Karza not registered with institution ");
                return null;
            }
            HashMap<String, String> headerParams = getHeader(karzaConfiguration, refID);

            String url=karzaConfiguration.getKarzaUrl();
            String tempResponse = httpTransportationService.postRequestSSL(url,voterIdKarzaJsonRequest,headerParams, MediaType.APPLICATION_JSON);

            logger.info("VoterId Karza Raw Response {}" , tempResponse);

            if (StringUtils.isNotBlank(tempResponse)) {

                voterIdResponse = JsonUtil.StringToObject(tempResponse, VoterIdResponse.class);
            }

            logger.info("Voter ID Karza Response {}" , voterIdResponse);

            return voterIdResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to karza for Voter Id service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }


    }


    public PanResponse callPanKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception {

        PanResponse panResponse=null;

        String panKarzaJsonRequest = JsonUtil.ObjectToString(karzaAuthenticationRequest);

        logger.info("PAN Karza JsonRequest{} ",panKarzaJsonRequest);

        try {

            if (null == karzaConfiguration) {
                logger.warn(" communication domain for Karza not registered with institution ");
                return null;
            }
            HashMap<String, String> headerParams = getHeader(karzaConfiguration, refID);

            String url=karzaConfiguration.getKarzaUrl();
            String tempResponse = httpTransportationService.postRequestSSL(url,panKarzaJsonRequest,headerParams, MediaType.APPLICATION_JSON);

            logger.info("PAN Karza Raw Response {}" , tempResponse);

            if (StringUtils.isNotBlank(tempResponse)) {

                panResponse = JsonUtil.StringToObject(tempResponse, PanResponse.class);
            }

            logger.info("PAN Karza Response {}" , panResponse);

            return panResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to karza for PAN service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }


    }

    public DrivingLicenceResponse callDrivingLicenceKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception {

        DrivingLicenceResponse drivingLicenceResponse=null;

        String drivingLicenceKarzaJsonRequest = JsonUtil.ObjectToString(karzaAuthenticationRequest);

        logger.info("Driving Licence Karza JsonRequest{} ",drivingLicenceKarzaJsonRequest);

        try {

            if (null == karzaConfiguration) {
                logger.warn(" communication domain for Karza not registered with institution ");
                return null;
            }
            HashMap<String, String> headerParams = getHeader(karzaConfiguration, refID);

            String url=karzaConfiguration.getKarzaUrl();

            String tempResponse = httpTransportationService.postRequestSSL(url,drivingLicenceKarzaJsonRequest,headerParams, MediaType.APPLICATION_JSON);

            logger.info("Driving Licence Karza Raw Response {}" , tempResponse);

            if (StringUtils.isNotBlank(tempResponse)) {

                drivingLicenceResponse = JsonUtil.StringToObject(tempResponse, DrivingLicenceResponse.class);
            }

            logger.info("Driving Licence Karza Response {}" , drivingLicenceResponse);

            return drivingLicenceResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to karza for Driving Licence service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }


    }

    public ElectricityResponse callElectricityKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception {

        ElectricityResponse electricityResponse=null;

        String electricityKarzaJsonRequest = JsonUtil.ObjectToString(karzaAuthenticationRequest);

        logger.info("Electricity Karza JsonRequest{} ",electricityKarzaJsonRequest);

        try {

            if (null == karzaConfiguration) {
                logger.warn(" communication domain for Karza not registered with institution ");
                return null;
            }
            HashMap<String, String> headerParams = getHeader(karzaConfiguration, refID);

            String url=karzaConfiguration.getKarzaUrl();

            String tempResponse = httpTransportationService.postRequestSSL(url,electricityKarzaJsonRequest,headerParams, MediaType.APPLICATION_JSON);

            logger.info("Electricity Karza Raw Response {}" , tempResponse);

            if (StringUtils.isNotBlank(tempResponse)) {

                electricityResponse = JsonUtil.StringToObject(tempResponse, ElectricityResponse.class);
            }

            logger.info("Electricity Karza Response {}" , electricityResponse);

            return electricityResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to karza for Electricity service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }


    }

    /**
     * Method to build the List karza Authentication Requests based on the KYC type document.
     *
     * @param applicationRequest
     * @return
     * @throws Exception
     */
    @Override
    public Map<Applicant, List<KarzaAuthenticationRequest>> buildKarzaAuthenticationRequest(final ApplicationRequest applicationRequest,
                                                                                            final List<String> kycConfig, final boolean isExecuteForCoApplicant)
            throws Exception {
        final String institutionId = applicationRequest.getHeader().getInstitutionId();
        final String productName = applicationRequest.getHeader().getProduct().name();
        final List<Kyc> kycList = applicationRequest.getRequest().getApplicant().getKyc();
        final KarzaConfiguration karzaConfiguration = getKarzaConfiguration(institutionId, productName);
        if (institutionId != null && productName != null && CollectionUtils.isNotEmpty(kycList)
                && karzaConfiguration != null) {
            return getKarzaAuthenticationRequests(applicationRequest, karzaConfiguration,
                    kycConfig, isExecuteForCoApplicant);
        } else {
            logger.info("ApplicationRequest/KarzaConfiguration is empty");
            throw new GoNoGoException("Precondition failed");
        }
    }

    /**
     * Method to generate Authentication request for the list of kycs, institution id and product.
     * @param applicationRequest
     * @return
     * @throws GoNoGoException
     */
    public Map<Applicant, List<KarzaAuthenticationRequest>> getKarzaAuthenticationRequests(final ApplicationRequest applicationRequest,
                                                                                           final KarzaConfiguration karzaConfiguration, final List<String> kycConfig,
                                                                                           final boolean isExecuteForCoApplicant) throws GoNoGoException {


        final String successRequestMessage = "Created karzaAuthenticationRequest for ";
        final String failRequestMessage = "Failed to create karzaAuthenticationRequest for ";
        Map<Applicant, List<Kyc>> kycFilteredMap = getKycFilteredMap(applicationRequest, kycConfig, isExecuteForCoApplicant);
        Map<Applicant, List<KarzaAuthenticationRequest>> karzaAuthenticationRequestMap = new HashMap<>(kycFilteredMap.size());
        KarzaAuthenticationRequest karzaAuthenticationRequest;
        applicantkarzaProcessingInfoList = new HashMap<>();
        for (Map.Entry<Applicant, List<Kyc>> applicantKycList : kycFilteredMap.entrySet()) {
            List<KarzaAuthenticationRequest> karzaAuthenticationRequestList = new ArrayList<>(applicantKycList.getValue().size());
            final List<KarzaProcessingInfo> karzaProcessingInfoList = new ArrayList<>();
            for (Kyc kyc : applicantKycList.getValue()) {
                String kycType = kyc.getKycRequestType();
                KarzaProcessingInfo existingKarzaProcessingInfo = externalAPILogRepository
                        .fetchKarzaProcessingInfo(applicationRequest.getHeader().getInstitutionId(), kyc.getKycNumber(),
                                applicationRequest.getRefID(), applicantKycList.getKey().getApplicantId(), kycType);
                if(existingKarzaProcessingInfo != null) {
                    /**
                     * provide karza result as a response.
                     */
                    karzaProcessingInfoList.add(existingKarzaProcessingInfo);
                    continue;
                }
                switch (kycType) {
                    case KarzaHelper.PAN_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addPANRequest(kyc, karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);

                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.AADHAR_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addAadharRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.DL_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addDLRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.VOTER_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addVoterIdRequest(kyc, karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.NREGA_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addNregaResponse(kyc, karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.PASSPORT_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addPassportRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.TAN_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addTanAuthRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.TAN_DETAIL_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addTanDetailRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.MCA_SIGNATORIES_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addMcaSignatoriesRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.COMPANY_SEARCH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addCompanySearchRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.IEC_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addIec(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.IEC_DETAILED_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addIecDetailed(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.COMPANY_AND_LLP_CIN_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addCompanySearchRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.COMPANY_AND_LLP_CIN_ID_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addCinLookup(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.UAN_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addUAN(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGSTIdentification(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_DETAILED_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGST(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GSP_GSTIN_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGST(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GSP_GST_RETURN_FILLING_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGST(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;


                    case KarzaHelper.GST_SEARCH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addPANRequest(kyc, karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.SHOP_ESTABLISHMENT_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addShopEstablishment(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.FSSAI_LICENSE_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addFssaiLicenseAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.FDA_LICENSE_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addFdaLicenseAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.CA_MEMBERSHIP_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addCaMembershipAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ICSI_MEMBERSHIP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addIcsiMembershipAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ICWAI_MEMBERSHIP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addCaMembershipAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ICWAI_FIRM_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addFssaiLicenseAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.MCI_MEMBERSHIP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addMciMembershipAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.PNG_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addPngMembershipAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ELECTRICITY_BILL_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addElectricityBill(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.TELEPHONE_BILL_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addTeleAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.MOBILE_OTP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addMobileOtpAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.MOBILE_DETAILS_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addMobileAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.LPG_ID_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addLpgAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.EPF_OTP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addEpfOtpAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.EPF_PASSBOOK_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addMobileAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.EPF_UAN_LOOKUP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addEpfUanAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.EMPLOYER_LOOKUP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addEmployerLookup(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ESIC_ID_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addEsicIdLookup(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.FORM_16_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addForm16(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.FORM_16_QUATERLY_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addForm16Quarterly(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ITR_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addItr(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ADDRESS_MATCHING_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addAddressMatching(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.EMAIL_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addEmail(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.NAME_SIMILARITY_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addNameSimilarity(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.IFSC_CODE_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addIfsc(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.BANK_ACCOUNT_VERIFICATION_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addBankVerification(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.HSN_CODE_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addHsnCode(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.WEBSITE_DOMAIN_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addWebDomain(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.VEHICLE_RC_AUTH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addVehicleRCAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.VEHICLE_RC_SEARCH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addVehicleRCSearch(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_TRANS_API_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGSTTranAPI(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_TRRN_OTP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGSTTranOTP(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_TRRN_VERIFY_OTP_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addMobileAuth(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_CREDENTIAL_LINK_GENERATION_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addVehicleRCSearch(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_CREDENTIAL_LINK_SEND_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addVehicleRCSearch(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_SEARCH_BY_PAN_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addPANRequest(kyc, karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.ENTITY_SEARCH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addCompanySearchRequest(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;
                    case KarzaHelper.DETAILED_PROFILE_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addDetailedProfile(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_FILING_STATUS_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGST(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.GST_PROFILE_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addGST(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.IBBI_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addIbbi(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.BIFR_CHECK_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addDetailedProfile(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.DRT_CHECK_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addDetailedProfile(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.SUIT_FILED_SEARCH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addSuitFilled(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.LITIGATIONS_SEARCH_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addLitigationSearch(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    case KarzaHelper.LITIGATIONS_DETAILS_DISTRICT_COURTS_REQUEST_TYPE:
                        karzaAuthenticationRequest = karzaRequestHelper
                                .addLitigationDetails(kyc, applicantKycList.getKey(), karzaConfiguration);
                        if (null != karzaAuthenticationRequest) {
                            karzaAuthenticationRequestList.add(karzaAuthenticationRequest);
                            logger.info(successRequestMessage + kycType);
                        } else {
                            logger.info(failRequestMessage + kycType);
                        }
                        break;

                    default:
                        throw new GoNoGoException("kycType " + kycType + " is not configured in the system");
                }
                karzaAuthenticationRequestMap.put(applicantKycList.getKey(), karzaAuthenticationRequestList);
            }
            if (! karzaProcessingInfoList.isEmpty()) {
                applicantkarzaProcessingInfoList.put(applicantKycList.getKey(), karzaProcessingInfoList);
            }
        }
        return karzaAuthenticationRequestMap;
    }

    /**
     * Methos which filters out the kyc types from the applicant request
     * based on the allowed kyc types for the application institution id.
     * @param applicationRequest
     * @param kycConfig
     * @param isExecuteForCoApplicant
     * @return Map{applicantId, List{ApplicationRequest}}
     */
    private Map<Applicant, List<Kyc>> getKycFilteredMap(final ApplicationRequest applicationRequest,
                                                        final List<String> kycConfig, final boolean isExecuteForCoApplicant) {

        Map<Applicant, List<Kyc>> karzaRequestTypeMap = new HashMap<>();

        // Add applicant
        karzaRequestTypeMap.put(applicationRequest.getRequest().getApplicant(),
                applicationRequest.getRequest().getApplicant().getKyc().stream()
                        .filter(kyc -> kycConfig.contains(kyc.getKycRequestType()))
                        .collect(Collectors.toList()));

        // Add coApplicants if exist
        List<CoApplicant> coApplicantList = applicationRequest.getRequest().getCoApplicant();
        if (isExecuteForCoApplicant && CollectionUtils.isNotEmpty(coApplicantList)) {
            for (CoApplicant coApplicant: coApplicantList) {
                if (CollectionUtils.isNotEmpty(coApplicant.getKyc())){
                    karzaRequestTypeMap.put(coApplicant, getKycList(coApplicant, kycConfig));
                }
            }
        }
        return karzaRequestTypeMap;
    }

    public List<Kyc> getKycList(final CoApplicant coApplicant, final List<String> kycConfig) {
        return coApplicant.getKyc().stream().filter(kyc -> kycConfig.contains(kyc.getKycRequestType())).collect(Collectors.toList());
    }
    /**
     * Method to get KarzaConfiguration
     * @param institutionId
     * @param productName
     * @return
     */

    public KarzaConfiguration getKarzaConfiguration(final String institutionId, final String productName) {

        return configurationRepository.findKarzaConfiguration(institutionId,productName);
    }

    /**
     * Method to build the consolidated Karza response for all the provided requests.
     *
     * @param institutionId
     * @param applicationId
     * @param productName
     * @param karzaAuthenticationRequestMap
     * @param kycConfig
     * @param isExecuteForCoApplicant
     * @return KarzaClientResponse
     * @throws Exception
     */
    @Override
    public KarzaClientResponse buildKarzaResponse(final String institutionId, final String applicationId, final String productName,
                                                  final Map<Applicant, List<KarzaAuthenticationRequest>> karzaAuthenticationRequestMap,
                                                  final List<String> kycConfig, final boolean isExecuteForCoApplicant, final String loggedInUser) throws Exception {

        KarzaClientResponse karzaClientResponse = new KarzaClientResponse();
        final KarzaConfiguration karzaConfiguration = getKarzaConfiguration(institutionId, productName);
        try {

            if (null == karzaConfiguration) {
                logger.warn(" communication domain for Karza not registered with institution ");
                return null;
            }

            for (Map.Entry<Applicant, List<KarzaAuthenticationRequest>> applicantKarzaRequest : karzaAuthenticationRequestMap.entrySet()) {
                final List<KarzaProcessingInfo> karzaProcessingInfoList = new ArrayList<>(applicantKarzaRequest.getValue().size());
                final Applicant applicant = applicantKarzaRequest.getKey();
                for (final KarzaAuthenticationRequest karzaAuthenticationRequest : applicantKarzaRequest.getValue()) {
                    final String kycType = karzaAuthenticationRequest.getRequestType();
                    final String jsonRequest = JsonUtil.ObjectToString(karzaAuthenticationRequest);
                    final KarzaProcessingInfo karzaProcessingInfo = new KarzaProcessingInfo();
                    karzaProcessingInfo.setInstitutionId(institutionId);
                    karzaProcessingInfo.setProductName(productName);
                    karzaProcessingInfo.setRefId(applicationId);
                    karzaProcessingInfo.setApplicantId(applicantKarzaRequest.getKey().getApplicantId());
                    karzaProcessingInfo.setAuthor(loggedInUser);
                    karzaProcessingInfo.setKycName(kycType);
                    karzaProcessingInfo.setRequest(jsonRequest);
                    karzaProcessingInfo.setInsertDate(DateUtils.getDateyyyyMMdd());
                    karzaProcessingInfo.setStatus(KarzaHelper.NOT_VERIFIED);
                    String tempResponse = getKarzaClientResponse(applicationId, karzaConfiguration, jsonRequest);
                    // Added this as sometimes response from karza is null.
                    if(tempResponse == null) {
                        tempResponse = getKarzaClientResponse(applicationId, karzaConfiguration, jsonRequest);
                    }
                    if (StringUtils.isNotBlank(tempResponse)) {
                        karzaClientResponse.setRefId(applicationId);
                        karzaClientResponse.setDocumentType(kycType);
                        karzaClientResponse = getKarzaKycResponse(karzaClientResponse,
                                karzaAuthenticationRequest, kycType, karzaProcessingInfo, tempResponse, institutionId);

                        if(StringUtils.isNotEmpty(karzaProcessingInfo.getResponse()) && !karzaProcessingInfo.getResponse().equals("null")) {
                            karzaProcessingInfo.setStatus(KarzaHelper.VERIFIED);
                            final Kyc currentKyc = applicant.getKyc().stream().filter(kyc -> kyc.getKycRequestType().equalsIgnoreCase(kycType))
                                    .collect(Collectors.toList()).get(0);
                            currentKyc.setKycApplied(true);
                            currentKyc.setKycStatus(true);
                        }
                    } else {
                        karzaProcessingInfo.setErrorMessage("Karza response is null");
                    }
                    karzaProcessingInfo.setActive(true);

                    /**
                     * Save the karza information in DB for each applicants kyc request.
                     */
                    externalAPILogRepository.saveKarzaProcessingInfo(karzaProcessingInfo);
                    karzaProcessingInfoList.add(karzaProcessingInfo);
                }
                /**
                 * provide karza result as a response.
                 */
                if (applicantkarzaProcessingInfoList.size() == 0
                        || applicantkarzaProcessingInfoList.get(applicantKarzaRequest.getKey()) == null
                        || applicantkarzaProcessingInfoList.get(applicantKarzaRequest.getKey()).size() == 0) {
                    applicantkarzaProcessingInfoList.put(applicantKarzaRequest.getKey(), karzaProcessingInfoList);
                    applicant.setKarzaProcessingInfoList(karzaProcessingInfoList);
                } else {
                    List<KarzaProcessingInfo> karzaProcessingInfoListOld = applicantkarzaProcessingInfoList.get(applicantKarzaRequest.getKey());
                    karzaProcessingInfoListOld.addAll(karzaProcessingInfoList);
                    applicantkarzaProcessingInfoList.put(applicantKarzaRequest.getKey(), karzaProcessingInfoListOld);
                    applicant.setKarzaProcessingInfoList(karzaProcessingInfoListOld);
                }
            }
        } catch (final Exception e) {
            logger.error("Exception Occurred while send request to karza service by cause {}", e.getMessage());
            throw new Exception(e.getMessage());
        }
        return karzaClientResponse;
    }

    private KarzaClientResponse getKarzaKycResponse(KarzaClientResponse karzaClientResponse,
                                                    KarzaAuthenticationRequest karzaAuthenticationRequest, String kycType,
                                                    KarzaProcessingInfo karzaProcessingInfo, String tempResponse, String institutionId)
            throws JsonProcessingException, GoNoGoException {
        switch (kycType) {
            case KarzaHelper.AADHAR_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getAadhaarid());
                karzaClientResponse = karzaResponseHelper.addAadharResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getAadharNumberResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getAadharNumberResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.PAN_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getPan());
                karzaClientResponse = karzaResponseHelper.addPANResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getPanAuthResponse()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getPanAuthResponse());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case  KarzaHelper.PAN_STATUS_CHECK:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getPan());
                karzaClientResponse = karzaResponseHelper.addPANStatus(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getPanAuthenticationResponse()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getPanAuthenticationResponse());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));
                break;

            case KarzaHelper.DL_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getDl_no());
                karzaClientResponse = karzaResponseHelper.addDLResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getDrivingLicenceAuthResponse()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getDrivingLicenceAuthResponse());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.VOTER_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEpic_no());
                karzaClientResponse = karzaResponseHelper.addVoterIdResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getVoterIdAuthResponse()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getVoterIdAuthResponse());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.NREGA_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getJobcardid());
                karzaClientResponse = karzaResponseHelper.addNregaResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getNregaResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getNregaResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.PASSPORT_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getPassportNo());
                karzaClientResponse = karzaResponseHelper.addPassportResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getKPassportResult()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getKPassportResult());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.TAN_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getTanNo());
                karzaClientResponse = karzaResponseHelper.addTANAuthResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getKTanAuthResult()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getKTanAuthResult());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.TAN_DETAIL_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getTanNo());
                karzaClientResponse = karzaResponseHelper.addTANDetailResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getKTanDetailResult()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getKTanDetailResult());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.IEC_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getIec());
                karzaClientResponse = karzaResponseHelper.addIecResultResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getIecResultResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getIecResultResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.IEC_DETAILED_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getIec());
                karzaClientResponse = karzaResponseHelper.addIecDetailedProfileResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getIecDetailedProfileResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getIecDetailedProfileResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.COMPANY_SEARCH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getCompany_name());
                karzaClientResponse = karzaResponseHelper.addCompanySearchResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getCompanySearchByNameResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getCompanySearchByNameResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.EMPLOYMENT_VERIFICATION_ADVANCED:
                karzaProcessingInfo.setKycNumber(StringUtils.join(karzaAuthenticationRequest.getData().getEmployerName(), FieldSeparator.COLON, karzaAuthenticationRequest.getData().getEmployeeName())) ;
                karzaClientResponse = karzaResponseHelper.addEmploymentVerificationAdvancedResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getEmploymentVerificationAdvancedResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getEmploymentVerificationAdvancedResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.COMPANY_AND_LLP_CIN_ID_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getCin());
                karzaClientResponse = karzaResponseHelper.addCompanyIdentificationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getCompanyIdentificationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getCompanyIdentificationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            /*case KarzaHelper.COMPANY_AND_LLP_CIN_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getIec());
                karzaClientResponse = karzaResponseHelper.addCompanySearchResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getCompanySearchByNameResponseDetails()));
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getAadharNumberResponseDetails()));
                break;*/

            case KarzaHelper.UAN_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getUan());
                karzaClientResponse = karzaResponseHelper.addUdyogAadharNumberResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getUdyogAadharNumberResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getUdyogAadharNumberResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstIdentificationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstIdentificationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstIdentificationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_DETAILED_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_SEARCH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstSearchBasisPanResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstSearchBasisPanResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstSearchBasisPanResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GSP_GSTIN_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGspGstinAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGspGstinAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGspGstinAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GSP_GST_RETURN_FILLING_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGspGstReturnFillingResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGspGstReturnFillingResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGspGstReturnFillingResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.SHOP_ESTABLISHMENT_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getReg_no());
                karzaClientResponse = karzaResponseHelper.addShopAndEstablishmentResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getShopAndEstablishmentResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getShopAndEstablishmentResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.FSSAI_LICENSE_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getReg_no());
                karzaClientResponse = karzaResponseHelper.addFssaiLicenseResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getFssaiLicenseResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getFssaiLicenseResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.FDA_LICENSE_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getLicence_no());
                karzaClientResponse = karzaResponseHelper.addFdaLicenseAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getFdaLicenseAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getFdaLicenseAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.CA_MEMBERSHIP_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getMembership_no());
                karzaClientResponse = karzaResponseHelper.addCaMembershipAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getCaMembershipAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getCaMembershipAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.ICSI_MEMBERSHIP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getMembership_no());
                karzaClientResponse = karzaResponseHelper.addIcsiMembershipAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getIcsiMembershipAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getIcsiMembershipAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.ICWAI_MEMBERSHIP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getMembership_no());
                karzaClientResponse = karzaResponseHelper.addIcwaiMembershipAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getIcwaiMembershipAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getIcwaiMembershipAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.ICWAI_FIRM_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getReg_no());
                karzaClientResponse = karzaResponseHelper.addIcwaiFirmAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getIcwaiFirmAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getIcwaiFirmAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.MCI_MEMBERSHIP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getRegistration_no());
                karzaClientResponse = karzaResponseHelper.addMciMembershipAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getMciMembershipAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getMciMembershipAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.PNG_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getConsumer_id());
                karzaClientResponse = karzaResponseHelper.addPngAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getPngAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getPngAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;


            case KarzaHelper.ELECTRICITY_BILL_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getConsumer_id());
                karzaClientResponse = karzaResponseHelper.addElectricityResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getElectricityAuthResponse()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getElectricityAuthResponse());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.TELEPHONE_BILL_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getTel_no());
                karzaClientResponse = karzaResponseHelper.addTelephoneBillAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getTelephoneBillAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getTelephoneBillAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.MOBILE_OTP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getMobile());
                karzaClientResponse = karzaResponseHelper.addMobileOtpResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getMobileOtpResponseDetails()));
                karzaProcessingInfo.setAcknowledgementId(karzaClientResponse.getAcknowledgementId());
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getMobileOtpResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.MOBILE_DETAILS_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getOtp());
                karzaClientResponse = karzaResponseHelper.addMobileDetailsResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getMobileDetailsResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getMobileDetailsResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.LPG_ID_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getLpg_id());
                karzaClientResponse = karzaResponseHelper.addLpgIdAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getLpgIdAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getLpgIdAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.EPF_OTP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getUan());
                karzaClientResponse = karzaResponseHelper.addOtpResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getOtpResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getOtpResponseDetails());
                karzaProcessingInfo.setAcknowledgementId(karzaClientResponse.getAcknowledgementId());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.EPF_PASSBOOK_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getOtp());
                karzaClientResponse = karzaResponseHelper.addPassbookResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getPassbookResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getPassbookResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.EPF_UAN_LOOKUP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getUan());
                karzaClientResponse = karzaResponseHelper.addEpfUanLookupResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getEpfUanLookupResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getEpfUanLookupResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.EMPLOYER_LOOKUP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getUan());
                karzaClientResponse = karzaResponseHelper.addEmployerLookupResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getEmployerLookupResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getEmployerLookupResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.ESIC_ID_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEsic_id());
                karzaClientResponse = karzaResponseHelper.addEsicIdAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getEsicIdAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getEsicIdAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.FORM_16_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getTan());
                karzaClientResponse = karzaResponseHelper.addForm16AuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getForm16AuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getForm16AuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.FORM_16_QUATERLY_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getTan());
                karzaClientResponse = karzaResponseHelper.addQuarterlyRecordsCountForNextFiscalResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getQuarterlyRecordsCountForNextFiscal()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getQuarterlyRecordsCountForNextFiscal());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.ITR_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getPan());
                karzaClientResponse = karzaResponseHelper.addItrAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getItrAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getItrAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.ADDRESS_MATCHING_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getAddress1());
                karzaClientResponse = karzaResponseHelper.addAddressMatchingResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getAddressMatchingResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getAddressMatchingResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.NAME_SIMILARITY_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getName1());
                karzaClientResponse = karzaResponseHelper.addNameSimilarityResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getNameSimilarityResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getNameSimilarityResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.IFSC_CODE_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getIfsc());
                karzaClientResponse = karzaResponseHelper.addIfscCodeCheckResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getIfscCodeCheckResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getIfscCodeCheckResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.BANK_ACCOUNT_VERIFICATION_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getIfsc());
                karzaClientResponse = karzaResponseHelper.addBankAccountVerificationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getBankAccountVerificationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getBankAccountVerificationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.HSN_CODE_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getHsCode());
                karzaClientResponse = karzaResponseHelper.addHsnCodeCheckResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getHsnCodeCheckResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getHsnCodeCheckResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.WEBSITE_DOMAIN_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getDomain());
                karzaClientResponse = karzaResponseHelper.addWebsiteDomainAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getWebsiteDomainAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getWebsiteDomainAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.VEHICLE_RC_AUTH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getReg_no());
                karzaClientResponse = karzaResponseHelper.addVehicleRCAuthenticationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getAadharNumberResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getAadharNumberResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.VEHICLE_RC_SEARCH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEngine_no());
                karzaClientResponse = karzaResponseHelper.addVehicleRCAuthenticationSearchResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getVehicleRCAuthenticationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getVehicleRCAuthenticationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));
                break;

            case KarzaHelper.GST_TRANS_API_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstTransAPIResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstTransactionApiResponse()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstTransactionApiResponse());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_TRRN_VERIFY_OTP_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getOtp());
                karzaClientResponse = karzaResponseHelper.addGstTrrnVerifyOtpResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstTransactionApiVerifyOtpResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstTransactionApiVerifyOtpResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_CREDENTIAL_LINK_GENERATION_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstCredentialLinkGenerationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstCredentialLinkGenerationResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstCredentialLinkGenerationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_CREDENTIAL_LINK_SEND_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstCredentialLinkSendResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstCredentialLinkSendResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstCredentialLinkSendResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.ENTITY_SEARCH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getCompany_name());
                karzaClientResponse = karzaResponseHelper.addEntitySearchResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getEntitySearchResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getEntitySearchResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;
            case KarzaHelper.DETAILED_PROFILE_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEntityId());
                karzaClientResponse = karzaResponseHelper.addDetailedprofileResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getDetailedProfileResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getDetailedProfileResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_FILING_STATUS_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstFilingStatusResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstFilingStatusResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstFilingStatusResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.GST_PROFILE_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getGstin());
                karzaClientResponse = karzaResponseHelper.addGstProfileResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getGstProfileResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getGstProfileResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.IBBI_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEntityId());
                karzaClientResponse = karzaResponseHelper.addIBBIResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getIbbiResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getIbbiResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.BIFR_CHECK_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEntityId());
                karzaClientResponse = karzaResponseHelper.addBIFRResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getBifrResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getBifrResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.DRT_CHECK_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEntityId());
                karzaClientResponse = karzaResponseHelper.addDrtCheckResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getDrtCheckResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getDrtCheckResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));

                break;

            case KarzaHelper.SUIT_FILED_SEARCH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getName());
                karzaClientResponse = karzaResponseHelper.addSuitFiledSearchResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getSuitFiledSearchResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getSuitFiledSearchResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));
                break;

            case KarzaHelper.LITIGATIONS_SEARCH_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getName());
                karzaClientResponse = karzaResponseHelper.addLitigationsSearchResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getLitigationsSearchResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getLitigationsSearchResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));
                break;

            case KarzaHelper.LITIGATIONS_DETAILS_DISTRICT_COURTS_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getCin());
                karzaClientResponse = karzaResponseHelper.addLitigationDetailsDistrictCourtResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getLitigationDetailsDistrictCourtResponseDetails()));
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getLitigationDetailsDistrictCourtResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));
                break;

            case KarzaHelper.EMAIL_VERIFICATION_REQUEST_TYPE:
                karzaProcessingInfo.setKycNumber(karzaAuthenticationRequest.getData().getEmail());
                karzaClientResponse =  karzaResponseHelper.addEmailVerificationResponse(karzaClientResponse, tempResponse);
                karzaProcessingInfo.setResponse(JsonUtil.ObjectToString(karzaClientResponse.getEmailVerificationResponseDetails()));
                karzaProcessingInfo.setAcknowledgementId(karzaClientResponse.getAcknowledgementId());
                karzaProcessingInfo.setResponseObj(karzaClientResponse.getEmailVerificationResponseDetails());
                karzaProcessingInfo.setErrorMessage(karzaClientResponse.getError());
                karzaProcessingInfo.setErrors(JsonUtil.ObjectToString(karzaClientResponse.getErrorList()));
                break;

            default:
                throw new GoNoGoException("kycType " + kycType + " is not configured in the system");
        }
        return karzaClientResponse;
    }

    private void autoCallingLitigationDetails(LitigationsSearchResponseDetails litigationsSearchResponseDetails) {
        if(litigationsSearchResponseDetails.getRecords() != null){
            List<DistrictCourt> districtCourts = litigationsSearchResponseDetails.getRecords().getDistrictCourts();
            if(CollectionUtils.isNotEmpty(districtCourts)){
                for (DistrictCourt court : districtCourts) {
                    KarzaAuthenticationRequest karzaRequestData = KarzaAuthenticationRequest.builder()
                            .data(KarzaRequestData.builder().cin(court.getCino()).build())
                            .requestType(com.softcell.gonogo.service.impl.KarzaHelper.LITIGATIONS_DETAILS_DISTRICT_COURTS_REQUEST_TYPE)
                            .build();

                    KarzaAPIRequest karzaAPIRequest = new KarzaAPIRequest();
                    karzaAPIRequest.setKarzaAuthenticationRequest(karzaRequestData);
                    try {
                        getKarzaAPIVerification(karzaAPIRequest, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * API Call to Karza for a specific request and get the respective response.DI
     *
     * @param applicationId
     * @param karzaConfiguration
     * @param jsonRequest
     * @return
     * @throws IOException
     */
    public String getKarzaClientResponse(final String applicationId, final KarzaConfiguration karzaConfiguration,
                                         final String jsonRequest) throws IOException {
        final HashMap<String, String> headerParams = getHeader(karzaConfiguration, applicationId);

        final String url = karzaConfiguration.getKarzaUrl();
        return httpTransportationService.postRequestSSL(url, jsonRequest, headerParams, MediaType.APPLICATION_JSON);
    }

    /**
     * Method to get Header for Karza Request.
     *
     * @param karzaConfiguration
     * @param applicationId
     * @return
     */
    private HashMap<String, String> getHeader(final KarzaConfiguration karzaConfiguration, final String applicationId) {
        final HashMap<String, String> headerParams = new HashMap<>();
        headerParams.put(KarzaRequestKeyHeader.LOGIN_ID, karzaConfiguration.getLoginId());
        headerParams.put(KarzaRequestKeyHeader.PASSWORD, karzaConfiguration.getPassword());
        headerParams.put(KarzaRequestKeyHeader.INSTITUTION_NAME, karzaConfiguration.getInstitutionName());
        headerParams.put(KarzaRequestKeyHeader.APPLICATION_ID, applicationId);
        headerParams.put(KarzaRequestKeyHeader.PRODUCT, karzaConfiguration.getProductName());
        return headerParams;
    }

    @Override
    public BaseResponse getKarzaAPIVerification(KarzaAPIRequest karzaAPIRequest, HttpServletRequest httpRequest) throws Exception {
        BaseResponse baseResponse;
        final String institutionId = karzaAPIRequest.getHeader().getInstitutionId();
        final String productName = karzaAPIRequest.getHeader().getProduct().name();
        final String jsonRequest = JsonUtil.ObjectToString(karzaAPIRequest.getKarzaAuthenticationRequest());
        KarzaClientResponse karzaClientResponse = new KarzaClientResponse();
        KarzaProcessingInfo existingKarzaProcessingInfo = getKarzaProcessingInfo(karzaAPIRequest);
        if (null == existingKarzaProcessingInfo) {
            final KarzaConfiguration karzaConfiguration = getKarzaConfiguration(institutionId, productName);
            if (null == karzaConfiguration) {
                logger.warn(" communication domain for Karza not registered with institution ");
                return null;
            }
            String tempResponse = getKarzaClientResponse(karzaAPIRequest.getRefId(), karzaConfiguration, jsonRequest);
            // Added this as sometimes response from karza is null.
            if (tempResponse == null) {
                tempResponse = getKarzaClientResponse(karzaAPIRequest.getRefId(), karzaConfiguration, jsonRequest);
            }

            final KarzaProcessingInfo karzaProcessingInfo = new KarzaProcessingInfo();
            karzaProcessingInfo.setInstitutionId(institutionId);
            karzaProcessingInfo.setProductName(productName);
            karzaProcessingInfo.setRefId(karzaAPIRequest.getRefId());
            karzaProcessingInfo.setAuthor(karzaAPIRequest.getHeader().getLoggedInUserId());
            karzaProcessingInfo.setKycName(karzaAPIRequest.getKarzaAuthenticationRequest().getRequestType());
            karzaProcessingInfo.setRequest(jsonRequest);
            karzaProcessingInfo.setInsertDate(DateUtils.getDateyyyyMMdd());
            karzaProcessingInfo.setStatus(KarzaHelper.NOT_VERIFIED);

            karzaClientResponse = getKarzaKycResponse(karzaClientResponse, karzaAPIRequest.getKarzaAuthenticationRequest(),
                    karzaAPIRequest.getKarzaAuthenticationRequest().getRequestType(), karzaProcessingInfo, tempResponse, institutionId);

            if (StringUtils.isNotEmpty(karzaProcessingInfo.getResponse()) && !karzaProcessingInfo.getResponse().equals("null")) {
                karzaProcessingInfo.setStatus(KarzaHelper.VERIFIED);
            }
            /**
             * Save the karza information in DB for each applicants kyc request.
             */
            externalAPILogRepository.saveKarzaProcessingInfo(karzaProcessingInfo);
            karzaClientResponse.setKarzaProcessingInfo(karzaProcessingInfo);
        }else
            karzaClientResponse.setKarzaProcessingInfo(existingKarzaProcessingInfo);

        if (karzaClientResponse != null) {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.OK, karzaClientResponse);
        } else {
            baseResponse = GngUtils.getBaseResponse(HttpStatus.NO_CONTENT);
        }
        return baseResponse;
    }

    private KarzaProcessingInfo getKarzaProcessingInfo(KarzaAPIRequest karzaAPIRequest){
        KarzaProcessingInfo existingKarzaProcessingInfo = null;
        KarzaAuthenticationRequest karzaAuthenticationRequest = karzaAPIRequest.getKarzaAuthenticationRequest();
        String kycType =  karzaAuthenticationRequest.getRequestType();
        String kycValue = "";
        switch(kycType){
            case KarzaHelper.PAN_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getPan();
                break;
            /*  As of now SBFC do not want rehit to karza in case of PAN.
                For below commented services, always new hit will be generated to karza.
            case KarzaHelper.AADHAR_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getAadhaarid();
                break;
            case KarzaHelper.DL_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getDl_no();
                break;
            case KarzaHelper.VOTER_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getEpic_no();
                break;
            case KarzaHelper.PASSPORT_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getPassportNo();
                break;
            case KarzaHelper.UAN_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getAadhar();
                break;
            case KarzaHelper.ELECTRICITY_BILL_AUTH_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getConsumer_id();
                break;
            case KarzaHelper.TELEPHONE_BILL_AUTH_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getTel_no();
                break;
            case KarzaHelper.EPF_UAN_LOOKUP_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getMobile();
                break;
            case KarzaHelper.EMAIL_VERIFICATION_REQUEST_TYPE:
                kycValue = karzaAuthenticationRequest.getData().getEmail();
                break;*/
            default:
                kycValue =null;
                break;
        }
        if(StringUtils.isNotEmpty(kycValue)) {
            existingKarzaProcessingInfo = externalAPILogRepository
                    .fetchKarzaProcessingInfo(karzaAPIRequest.getHeader().getInstitutionId(), kycValue,
                            karzaAPIRequest.getRefId(), "", kycType);
        }
        return existingKarzaProcessingInfo;
    }



    private HashMap<String, String> getSSLHeader(WFJobCommDomain wfJobCommDomain, String refId) {
        HashMap<String, String> headerParam = new HashMap<>();
        if(wfJobCommDomain != null){
            headerParam.put(KarzaRequestKeyHeader.LOGIN_ID, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), KarzaRequestKeyHeader.LOGIN_ID));
            headerParam.put(KarzaRequestKeyHeader.PASSWORD, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), KarzaRequestKeyHeader.PASSWORD));
            headerParam.put(KarzaRequestKeyHeader.INSTITUTION_NAME, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), KarzaRequestKeyHeader.INSTITUTION_NAME));
            headerParam.put(KarzaRequestKeyHeader.PRODUCT, GngUtils.getValueForKey(wfJobCommDomain.getKeys(), KarzaRequestKeyHeader.PRODUCT));
            headerParam.put(KarzaRequestKeyHeader.APPLICATION_ID, refId);
        }
        return  headerParam;
    }

    public static String getValueForKey(List<HeaderKey> headerKeyList, String keyName){
        String value = null;
        for (HeaderKey headerKeyValue : headerKeyList) {
            if (headerKeyValue.getKeyName().equals(keyName))
                value = headerKeyValue.getKeyValue();
        }
        return value;
    }

}
