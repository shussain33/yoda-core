package com.softcell.gonogo.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.gonogo.model.request.smsservice.SmsServiceBaseRequest;
import com.softcell.gonogo.model.response.smsservice.SmsServiceBaseResponse;
import com.softcell.gonogo.service.SmsServiceCaller;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Service
public class SmsServiceCallerImpl implements SmsServiceCaller {

    private static Logger logger = LoggerFactory.getLogger(SmsServiceCallerImpl.class);

    @Override
    public SmsServiceBaseResponse callSmsService(
            SmsServiceBaseRequest smsServiceBaseRequest, String smsServiceUrl)
            throws IOException {

        SmsServiceBaseResponse smsServiceBaseResponse = null;
        try (CloseableHttpClient httpClient = HttpClientBuilder.create()
                .build()) {
            HttpPost httpPost = new HttpPost(smsServiceUrl);
            String jsonString = new ObjectMapper()
                    .writeValueAsString(smsServiceBaseRequest);

            StringEntity entity = new StringEntity(jsonString);
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,
                    "application/json"));

            httpPost.setEntity(entity);
            HttpResponse httpResponse = httpClient.execute(httpPost);

            if (httpResponse != null && httpResponse.getEntity() != null) {

                try (InputStream is = httpResponse.getEntity().getContent();
                     BufferedReader br = new BufferedReader(
                             new InputStreamReader(is))) {

                    String line = br.readLine();
                    StringBuilder builder = new StringBuilder();
                    while (line != null) {
                        builder.append(line);
                        line = br.readLine();
                    }
                    String responseString = builder.toString();
                    if (StringUtils.isEmpty(responseString)) {
                        logger.error("Sms service returned null or empty response");
                        return smsServiceBaseResponse;
                    } else {
                        smsServiceBaseResponse = new ObjectMapper().readValue(
                                responseString, SmsServiceBaseResponse.class);
                    }
                }
            }
            return smsServiceBaseResponse;
        }
    }
}
