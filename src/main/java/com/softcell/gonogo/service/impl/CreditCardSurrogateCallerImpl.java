package com.softcell.gonogo.service.impl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.CreditCardSurrogateConfig;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateRequest;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateResponse;
import com.softcell.gonogo.service.CreditCardSurrogateCaller;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 */
@Service
public class CreditCardSurrogateCallerImpl implements CreditCardSurrogateCaller {

    private static final Logger logger = LoggerFactory.getLogger(CreditCardSurrogateCallerImpl.class);

    @Override
    public CreditCardSurrogateResponse callToCreditCardSurrogateApi(
            CreditCardSurrogateRequest creditCardSurrogateRequest,
            CreditCardSurrogateConfig creditCardSurrogateConfig) {

        ArrayList<NameValuePair> postParameters = getPostParameter(
                creditCardSurrogateRequest, creditCardSurrogateConfig);

        if (null != postParameters && null != creditCardSurrogateConfig
                && null != creditCardSurrogateConfig.getUrl()) {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost httpPost = null;
            HttpResponse response = null;
            InputStream content = null;
            try {
                httpPost = new HttpPost(creditCardSurrogateConfig.getUrl());
                httpPost.setEntity(new UrlEncodedFormEntity(postParameters));
                response = client.execute(httpPost);
                content = response.getEntity().getContent();
                return getCreditCardSurrogateObject(getResponseJson(new BufferedReader(
                        new InputStreamReader(content))));
            } catch (IOException e) {
                logger.error("Error in IOException: " + e);
                return null;
            } finally {
                if (null != content) {
                    try {
                        content.close();
                    } catch (IOException e) {
                        logger.error("Error in IOException: " + e);
                    }
                }
                if (null != httpPost) {
                    httpPost.releaseConnection();
                }
            }
        }
        return null;
    }

    private CreditCardSurrogateResponse getCreditCardSurrogateObject(
            String responseJson) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(responseJson,
                    CreditCardSurrogateResponse.class);
        } catch (JsonParseException e) {
            logger.error("Error in JsonParseException: " + e);
            return null;
        } catch (JsonMappingException e) {
            logger.error("Error in JsonMappingException: " + e);
            return null;
        } catch (IOException e) {
            logger.error("Error in IOException: " + e);
            return null;
        }
    }

    private ArrayList<NameValuePair> getPostParameter(
            CreditCardSurrogateRequest creditCardSurrogateRequest,
            CreditCardSurrogateConfig creditCardSurrogateConfig) {
        ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("bin",
                creditCardSurrogateRequest.getCreditCardNumber()));
        postParameters.add(new BasicNameValuePair("action",
                creditCardSurrogateConfig.getAction()));
        postParameters.add(new BasicNameValuePair("target",
                creditCardSurrogateConfig.getTarget()));
        return postParameters;
    }

    /**
     * @param rd
     * @return This method returns Response json from Buffered Reader.
     */
    private String getResponseJson(BufferedReader rd) {
        StringBuilder tempResponse = new StringBuilder();
        String line;
        try {
            while ((line = rd.readLine()) != null) {
                if (StringUtils.isBlank(tempResponse)) {
                    tempResponse.append(line);
                } else {
                    tempResponse.append(tempResponse).append(" ").append(line);
                }
            }
            return tempResponse.toString();
        } catch (IOException e) {
            logger.error("Error in IOException: " + e);
            return null;
        }
    }

}
