package com.softcell.gonogo.service.impl;

import com.softcell.config.AadharConfiguration;
import com.softcell.constants.ConfigurationVersion;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.EKycServiceException;
import com.softcell.gonogo.model.core.kyc.DefaultBaseResponse;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequestNew;
import com.softcell.gonogo.model.core.kyc.response.aadhar.AadharMainResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.AadharServiceCaller;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;


/**
 * @author yogeshb
 */
@Service
public class AadharServiceCallerImpl implements AadharServiceCaller {

    private static final Logger logger = LoggerFactory.getLogger(AadharServiceCallerImpl.class);


    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private HttpTransportationService httpTransportationService;

    /**
     * @param aadhaarRequest
     * @param institutionId
     * @return
     */
    @Override
    public AadharMainResponse callAadharOtpService(final AadhaarRequest aadhaarRequest, String institutionId) throws Exception {

        try {

            AadharMainResponse aadharResponse = null;


            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration().get(institutionId);

            if (aadharConfiguration == null) {


                logger.warn(" communication domain for AADHAR is not registered with institutionId {} ", institutionId);

                return null;

            }

            String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);


            String tempResponse = httpTransportationService.postRequest(aadharConfiguration.getAadharOtpServiceUrl(), new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});


            if (StringUtils.isNotBlank(tempResponse)) {

                aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
            }

            return aadharResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to aadhaar otp service with probable cause [{}]", e.getMessage());

            throw new Exception(String.format("Exception Occurred while send request to aadhaar otp service with probable cause [%s]", e.getMessage()));
        }

    }

    /**
     * @param aadhaarRequest
     * @param institutionId
     * @return
     * @throws Exception
     */
    @Override
    public AadharMainResponse callAadharEkycService(AadhaarRequest aadhaarRequest, String institutionId) throws Exception {

        AadharMainResponse aadharResponse = null;

        String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);

        try {
            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration().get(institutionId);

            if (null == aadharConfiguration) {
                logger.warn(" communication domain for aadhaar kyc is not registered with institution {} ", institutionId);
                return null;
            }

            String tempResponse = httpTransportationService.postRequest(aadharConfiguration.getAadharEkyServiceUrl(), new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});

            if (StringUtils.isNotBlank(tempResponse)) {

                aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
            }

            return aadharResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to aadhar kyc service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }

    @Override

    public AadharMainResponse callAadharBiometricService(AadhaarRequest aadhaarRequest, String institutionId) throws Exception {

        AadharMainResponse aadharResponse = null;

        String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);

        try {
            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION
                    .getAadharConfiguration().get(institutionId.concat(ConfigurationVersion.v2_1.name()));

            if (null == aadharConfiguration) {
                logger.warn(" communication domain for aadhaar kyc is not registered with institution {} ", institutionId);
                return null;
            }

            String tempResponse = httpTransportationService.postRequest(aadharConfiguration.getAadharEkyServiceUrl(), new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});

            if (StringUtils.isNotBlank(tempResponse)) {

                aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
            }

            return aadharResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to aadhar kyc service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }

    public BaseResponse  callAadharEkycServiceV2_1_1(AadhaarRequest aadhaarRequest, String institutionId) throws Exception {
        BaseResponse baseResponse = null;
        DefaultBaseResponse defaultBaseResponse;
        String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);
        try {
            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                    .get(institutionId.concat(ConfigurationVersion.v2_1.name()));
            if (null == aadharConfiguration) {
                logger.warn(" communication domain for aadhaar kyc version 2.1 is not registered with institution {} ", institutionId);
                return null;
            }
            String tempResponse = httpTransportationService.postRequest_1(aadharConfiguration.getAadharEkyServiceUrl(), new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});

            if (StringUtils.isNotBlank(tempResponse)) {
                AadharMainResponse aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
                baseResponse = GngUtils.getBaseResponse(org.springframework.http.HttpStatus.OK, aadharResponse);
            }
        }catch (EKycServiceException e) {
            logger.error("Exception Occurred while send request to aadhar kyc version 2.1 service by cause {}", e.getMessage());
            defaultBaseResponse = DefaultBaseResponse.builder().resMessage(e.getMessage()).build();
            baseResponse=  GngUtils.getBaseResponse(org.springframework.http.HttpStatus.FAILED_DEPENDENCY, defaultBaseResponse);

        }
        catch (Exception e) {
            logger.error("Exception Occurred while send request to aadhar kyc version 2.1 service by cause {}", e.getMessage());
            defaultBaseResponse = DefaultBaseResponse.builder().resMessage(e.getMessage()).build();
            baseResponse=  GngUtils.getBaseResponse(org.springframework.http.HttpStatus.FAILED_DEPENDENCY, defaultBaseResponse);
            // throw new Exception(e.getMessage());
        }
        return baseResponse;
    }

    public AadharMainResponse callAadharEkycServiceV2_1(AadhaarRequest aadhaarRequest, String institutionId) throws Exception {

        AadharMainResponse aadharResponse = null;

        String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);

        try {

            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                    .get(institutionId.concat(ConfigurationVersion.v2_1.name()));

            if (null == aadharConfiguration) {
                logger.warn(" communication domain for aadhaar kyc version 2.1 is not registered with institution {} ", institutionId);
                return null;
            }

            String tempResponse = httpTransportationService.postRequest(aadharConfiguration.getAadharEkyServiceUrl(), new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});

            if (StringUtils.isNotBlank(tempResponse)) {

                aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
            }

            return aadharResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to aadhar kyc version 2.1 service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }


    public AadharMainResponse callAadharEkycServiceV2_2_NEW(AadhaarRequest aadhaarRequest, String institutionId) throws Exception {

        AadharMainResponse aadharResponse = null;

        String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);

        try {

            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                    .get(institutionId.concat(ConfigurationVersion.v2_2.name()));

            if (null == aadharConfiguration) {
                logger.warn(" communication domain for aadhaar kyc version 2.2 is not registered with institution {} ", institutionId);
                return null;
            }


            String tempResponse = httpTransportationService.postRequest( aadharConfiguration.getAadharEkyServiceUrl(),new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});

            if (StringUtils.isNotBlank(tempResponse)) {

                aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
            }

            return aadharResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to aadhar kyc version 2.1 service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }

    @Override
    public AadharMainResponse callAadharEkycServiceV2_2(AadhaarRequestNew aadhaarRequest, String institutionId) throws Exception {

        AadharMainResponse aadharResponse = null;

        String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);

        try {

            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                    .get(institutionId.concat(ConfigurationVersion.v2_2.name()));

            if (null == aadharConfiguration) {
                logger.warn(" communication domain for aadhaar kyc version 2.2 is not registered with institution {} ", institutionId);
                return null;
            }

            String tempResponse = httpTransportationService.postRequest(aadharConfiguration.getAadharOtpServiceUrl(), new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});

            if (StringUtils.isNotBlank(tempResponse)) {

                aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
            }

            return aadharResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to aadhar kyc version 2.1 service by cause {}", e.getMessage());

            throw new Exception(e.getMessage());
        }

    }

    @Override
    public AadharMainResponse callAadharOtpServiceV2_1(AadhaarRequest aadhaarRequest, String institutionId) throws Exception {
        try {

            AadharMainResponse aadharResponse = null;


            AadharConfiguration aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                    .get(institutionId.concat(ConfigurationVersion.v2_1.name()));

            if (aadharConfiguration == null) {


                logger.warn(" communication domain for AADHAR is not registered with institutionId {} ", institutionId);

                return null;

            }

            String aadhaarJsonRequest = JsonUtil.ObjectToString(aadhaarRequest);


            String tempResponse = httpTransportationService.postRequest(aadharConfiguration.getAadharOtpServiceUrl(), new HashMap<String, String>() {{
                put("INSTITUTION_ID", aadharConfiguration.getInstitutionId());
                put("AGGREGATOR_ID", aadharConfiguration.getAggregatorId());
                put("MEMBER_ID", aadharConfiguration.getMemberId());
                put("PASSWORD", aadharConfiguration.getPassword());
                put("inputJson_", aadhaarJsonRequest);
            }});


            if (StringUtils.isNotBlank(tempResponse)) {

                aadharResponse = JsonUtil.StringToObject(tempResponse, AadharMainResponse.class);
            }

            return aadharResponse;

        } catch (Exception e) {

            logger.error("Exception Occurred while send request to aadhaar otp service with probable cause [{}]", e.getMessage());

            throw new Exception(String.format("Exception Occurred while send request to aadhaar otp service with probable cause [%s]", e.getMessage()));
        }
    }
}
