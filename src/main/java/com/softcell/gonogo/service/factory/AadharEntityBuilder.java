/**
 * @date Mar 2, 2016 11:14:05 AM
 */
package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.aadhar.ClientAadhaarRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;

/**
 * @author yogeshb
 */
public interface AadharEntityBuilder {

    /**
     * @param aadharRequest
     * @return
     */
    AadhaarRequest buildEkycWithBIOSRequest(ClientAadhaarRequest aadharRequest);

    /**
     * @param clientAadhaarRequest
     * @return
     */
    AadhaarRequest buildOtpRequest(ClientAadhaarRequest clientAadhaarRequest);

    /**
     * @param clientAadhaarRequest
     * @return
     */
    AadhaarRequest buildEkycUsingOtpRequestV2(ClientAadhaarRequest clientAadhaarRequest);

    /**
     * @param clientAadhaarRequest
     * @return
     */
    AadhaarRequest buildEkycUsingOtpRequestV2_1(ClientAadhaarRequest clientAadhaarRequest);

    /**
     * @param clientAadhaarRequest
     * @return
     */
    AadhaarRequest buildIrisRequest(ClientAadhaarRequest clientAadhaarRequest);

    /**
     * @param application
     * @return
     */
    AadhaarRequest buildAuthenticationRequest(ApplicationRequest application);


    AadhaarRequest buildOtpRequestV2_1(ClientAadhaarRequest aadharOtpRequest);

    AadhaarRequest buildIrisRequestV2_1(ClientAadhaarRequest clientAadhaarRequest);

    AadhaarRequest buildBiometricRequest(ClientAadhaarRequest clientAadhaarRequest);

    String createAadhaarXml(AadhaarRequest aadhaarRequest, String institutionId);

    String createOTPXml(AadhaarRequest aadhaarRequest, String institutionId);
}
