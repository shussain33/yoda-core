package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.KYC_TYPES;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.qde.ExistingCustomerServiceRequest;
import com.softcell.gonogo.model.qde.SolicitedCustomerServiceRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.service.factory.CustomerDataBuilder;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by archana on 27/7/17.
 */
@Service
public class CustomerDataBuilderImpl implements CustomerDataBuilder {
    @Override
    public SolicitedCustomerServiceRequest buildSolicitedServiceRequest(ApplicationRequest customerDataRequest) {
        // Set teh fields

        return SolicitedCustomerServiceRequest.builder()
                .mobileNumber(customerDataRequest.getRequest().getApplicant().getPhone().get(0).getPhoneNumber())
                .requestId(customerDataRequest.getCouponCode())
                .build();
    }

    @Override
    public ExistingCustomerServiceRequest buildExistingServiceRequest(ApplicationRequest applicationRequest) {

        ExistingCustomerServiceRequest request = ExistingCustomerServiceRequest.builder().build();
        // Date of birth
        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();
        if (dob != null ) {
            request.setDateOfBirth(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat(GngDateUtil.ddMMyyyy,dob),
                    GngDateUtil.yyyy_MM_dd));
        }
        // Mobile
        List<Phone> phoneList = applicationRequest.getRequest().getApplicant().getPhone();
        if( phoneList != null && ! phoneList.isEmpty()
                && StringUtils.isNotBlank(phoneList.get(0).getPhoneNumber()) ) {
            request.setMobileNumber(phoneList.get(0).getPhoneNumber());
        }
        // Kyc document
        List<Kyc> kycList = applicationRequest.getRequest().getApplicant().getKyc();
        if( kycList != null && ! kycList.isEmpty()  ) {
            Kyc kycDoc = (Kyc) kycList.get(0);
            String kycName = kycDoc.getKycName();
            String kycNumber = kycDoc.getKycNumber();

            if (StringUtils.isNotBlank(kycName) && StringUtils.isNotBlank(kycNumber)) {
                if (KYC_TYPES.PAN.toString().equalsIgnoreCase(kycName)) {
                    request.setPanNumber(kycNumber);
                } else if (KYC_TYPES.AADHAR.toString().equalsIgnoreCase(kycName)) {
                    request.setAadharNumber(kycNumber);
                } else if (KYC_TYPES.VOTER_ID.toString().equalsIgnoreCase(kycName)) {
                    request.setVoterId(kycNumber);
                } else if (KYC_TYPES.DRIVING_LICENSE.toString().equalsIgnoreCase(kycName)) {
                    request.setDrivingLicence(kycNumber);
                }
            }
        }
        return request;
    }
}
