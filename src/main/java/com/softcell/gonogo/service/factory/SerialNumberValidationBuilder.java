package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.DisbursementRequest;
import com.softcell.gonogo.serialnumbervalidation.DisbursementResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.apple.AppleRequest;
import com.softcell.gonogo.serialnumbervalidation.apple.AppleResponse;
import com.softcell.gonogo.serialnumbervalidation.apple.RollbackRequestServiceDetails;
import com.softcell.gonogo.serialnumbervalidation.gionee.GioneeRequest;
import com.softcell.gonogo.serialnumbervalidation.gionee.GioneeResponse;
import com.softcell.gonogo.serialnumbervalidation.google.GoogleRequest;
import com.softcell.gonogo.serialnumbervalidation.google.GoogleResponse;
import com.softcell.gonogo.serialnumbervalidation.intex.IntexRequest;
import com.softcell.gonogo.serialnumbervalidation.intex.IntexResponse;
import com.softcell.gonogo.serialnumbervalidation.kent.KentRequest;
import com.softcell.gonogo.serialnumbervalidation.kent.KentResponse;
import com.softcell.gonogo.serialnumbervalidation.oppo.OppoRequest;
import com.softcell.gonogo.serialnumbervalidation.oppo.OppoResponse;
import com.softcell.gonogo.serialnumbervalidation.panasonic.PanasonicRequest;
import com.softcell.gonogo.serialnumbervalidation.panasonic.PanasonicResponse;
import com.softcell.gonogo.serialnumbervalidation.samsung.SamsungRequest;
import com.softcell.gonogo.serialnumbervalidation.samsung.SamsungResponse;
import com.softcell.gonogo.serialnumbervalidation.sony.RollBackResponse;
import com.softcell.gonogo.serialnumbervalidation.sony.SonyRequest;
import com.softcell.gonogo.serialnumbervalidation.sony.SonyResponse;
import com.softcell.gonogo.serialnumbervalidation.videocon.VideoconRequest;
import com.softcell.gonogo.serialnumbervalidation.videocon.VideoconResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by yogeshb on 10/3/17.
 */
public interface SerialNumberValidationBuilder {
    /**
     *
     * @param serialSaleConfirmationRequest
     * @param sonyCavConfig
     * @return
     */
    SonyRequest buildSonyMobileRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain sonyCavConfig);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param sonyCavConfig
     * @return
     */
    SonyRequest buildSonyCAVRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain sonyCavConfig);

    /**
     *
     * @param sonyResponse
     * @return
     */
    SerialNumberResponse buildSonyResponse(SonyResponse sonyResponse);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param intexConfig
     * @return
     */
    IntexRequest buildIntexSerialNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain intexConfig);

    /**
     *
     * @param intexResponse
     * @return
     */
    SerialNumberResponse buildIntexResponse(IntexResponse intexResponse);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param panasonicConfig
     * @return
     */
    PanasonicRequest buildPanasonicSerialNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain panasonicConfig);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param panasonicConfig
     * @return
     */
    PanasonicRequest buildPanasonicImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain panasonicConfig);


    /**
     *
     * @param panasonicResponse
     * @return
     */
    SerialNumberResponse buildPanasonicSerialResponse(PanasonicResponse panasonicResponse);

    /**
     *
     * @param panasonicResponse
     * @return
     */
    SerialNumberResponse buildPanasonicImeiResponse(PanasonicResponse panasonicResponse);

    /**
     *
     * @param samsungResponse
     * @return
     */
    SerialNumberResponse buildSamsungImeiResponse(SamsungResponse samsungResponse);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param appleConfig
     * @return
     */
    AppleRequest buildAppleDPLRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain appleConfig);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param appleConfig
     * @return
     */
    AppleRequest buildAppleCDRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain appleConfig);


    /**
     *
     * @param appleResponse
     * @return
     */
    SerialNumberResponse buildAppleResponse(AppleResponse appleResponse);

    /**
     *
     * @param serialsaleconfirmationrequest
     * @param samsungConfig
     * @return
     */
    SamsungRequest buildSamsungRequest(SerialSaleConfirmationRequest serialsaleconfirmationrequest, WFJobCommDomain samsungConfig);

    /**
     *
     * @param rollbackRequest
     * @param appleConfig
     * @return
     */
    AppleRequest buildAppleImeiRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain appleConfig);

    /**
     *
     * @param rollBackResponse
     * @return
     */
    SerialNumberResponse buildAppleRollbackResponse(RollBackResponse rollBackResponse);

    /**
     *
     * @param rollbackRequest
     * @param appleConfig
     * @return
     */
    AppleRequest buildAppleSerialNumberRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain appleConfig);

    /**
     *
     * @param serialNumberInfo
     * @return
     */
    RollbackRequestServiceDetails buildAppleRollbackPreRequisiteServiceResponse(SerialNumberInfo serialNumberInfo);

    /**
     *
     * @param serialsaleconfirmationrequest
     * @param samsungConfig
     * @return
     */
    SamsungRequest buildSamsungImeiRequest(SerialSaleConfirmationRequest serialsaleconfirmationrequest, WFJobCommDomain samsungConfig);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param gioneeConfig
     * @return
     */
    GioneeRequest buildGioneeImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain gioneeConfig);

    /**
     *
     * @param gioneeResponse
     * @return
     */
    SerialNumberResponse buildGioneeImeiResponse(GioneeResponse gioneeResponse);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    VideoconRequest buildVideoconRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param videoconResponse
     * @return
     */
    SerialNumberResponse buildVideoconResponse(VideoconResponse videoconResponse);


    /**
     *
     * @param rollBackResponse
     * @return
     */
    SerialNumberResponse buildVideoconSerialRollbackResponse(RollBackResponse rollBackResponse);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    VideoconRequest buildVideoconSerialNumberRollbackRequest(RollbackRequest rollbackRequest);

    /**
     *
     * @param disbursementRequest
     * @return
     */
    VideoconRequest buildVideoconSerialNumberDisbursementRequest(DisbursementRequest disbursementRequest);

    /**
     *
     * @param disbursementResponse
     * @return
     */
    SerialNumberResponse buildVideoconSerialDisbursementResponse(DisbursementResponse disbursementResponse);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    KentRequest buildKentRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain kentConfig);

    /**
     *
     * @param kentResponse
     * @return
     */
    SerialNumberResponse buildKentResponse(KentResponse kentResponse);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    KentRequest buildKentSerialRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain kentConfig);

    /**
     *
     * @param disbursementRequest
     * @return
     */
    KentRequest buildKentSerialDisburseRequest(DisbursementRequest disbursementRequest, WFJobCommDomain kentConfig);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param oppoConfig
     * @return
     */
    OppoRequest buildOppoImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain oppoConfig);

    /**
     *
     * @param oppoResponse
     * @return
     */
    SerialNumberResponse buildOppoImeiResponse(OppoResponse oppoResponse);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    GoogleRequest buildGoogleImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param googleResponse
     * @return
     */
    SerialNumberResponse buildGoogleImeiResponse(GoogleResponse googleResponse);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    GoogleRequest buildGoogleImeiRollbackRequest(RollbackRequest rollbackRequest);

    /**
     *
     * @param googleResponse
     * @return
     */
    SerialNumberResponse buildGoogleImeiRollbackResponse(GoogleResponse googleResponse);


}