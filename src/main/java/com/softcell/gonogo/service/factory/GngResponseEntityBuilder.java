package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.Acknowledgement;
import com.softcell.gonogo.model.response.core.AckHeader;
import com.softcell.gonogo.model.response.core.ErrorMessage;
import com.softcell.gonogo.model.response.core.Warnings;

import java.util.Date;

/**
 * @author kishor
 */
public class GngResponseEntityBuilder {
    /**
     * @param refId
     * @param errMessage
     * @return
     */
    public static Acknowledgement getFailureAcknowledgement(String refId,
                                                            String errMessage) {
        Acknowledgement ack = new Acknowledgement();
        ack.setGonogoRefId(refId);
        ErrorMessage errMsg = new ErrorMessage();
        errMsg.setDescription(errMessage);
        ack.setErrors(errMsg);
        return ack;
    }

    /**
     * @param refId
     * @return
     */
    public static Acknowledgement getSuccesAcknowledgement(String refId) {
        Acknowledgement ack = new Acknowledgement();
        ack.setGonogoRefId(refId);
        ack.setStatus("SUCCESS");
        return ack;
    }

    /**
     * @param applicationRequest data of application and customer
     * @return custom message of application is failed to process.
     */
    public Acknowledgement getFailureAcknowledgement(ApplicationRequest applicationRequest) {
        Acknowledgement ack = new Acknowledgement();
        AckHeader ackHeader = new AckHeader();
        ackHeader.setApplicationId(applicationRequest.getHeader().getApplicationId());
        ackHeader.setInstitutionId(applicationRequest.getHeader().getInstitutionId());
        ackHeader.setRequestReceivedTime(new Date());
        ackHeader.setResponseDateTime(new Date());
        ackHeader.setResponseType("");
        ack.setHeader(ackHeader);
        ack.setGonogoRefId(applicationRequest.getRefID());
        ack.setStatus("ERROR");
        Warnings warnings = new Warnings();
        warnings.setCode("");
        warnings.setDescription("");
        ack.setWarnings(warnings);
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode("");
        errorMessage.setDescription("");
        ack.setErrors(errorMessage);
        return ack;
    }

    public Acknowledgement getInvalidRequestResponse() {
        Acknowledgement ack = new Acknowledgement();
        AckHeader ackHeader = new AckHeader();
        ackHeader.setRequestReceivedTime(new Date());
        ackHeader.setResponseDateTime(new Date());
        ackHeader.setResponseType("");
        ack.setHeader(ackHeader);
        ack.setStatus("ERROR");
        Warnings warnings = new Warnings();
        warnings.setCode("");
        warnings.setDescription("");
        ack.setWarnings(warnings);
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode("400");
        errorMessage.setDescription("Bad Request");
        ack.setErrors(errorMessage);
        return ack;
    }

    /**
     * @param applicationRequest data of application and customer.
     * @return Successful message if application start normally.
     */
    public Acknowledgement getSuccesAcknowledgement(
            ApplicationRequest applicationRequest) {
        Acknowledgement ack = new Acknowledgement();
        AckHeader ackHeader = new AckHeader();
        ackHeader.setApplicationId(applicationRequest.getHeader().getApplicationId());
        ackHeader.setInstitutionId(applicationRequest.getHeader().getInstitutionId());
        ackHeader.setRequestReceivedTime(new Date());
        ackHeader.setResponseDateTime(new Date());
        ackHeader.setResponseType("");
        ack.setHeader(ackHeader);
        ack.setGonogoRefId(applicationRequest.getRefID());
        ack.setStatus("SUCCESS");
        Warnings warnings = new Warnings();
        warnings.setCode("");
        warnings.setDescription("");
        ack.setWarnings(warnings);
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode("");
        errorMessage.setDescription("");
        ack.setErrors(errorMessage);
        return ack;
    }

    /**
     * @param applicationRequest ApplicationRequest of the GNG application
     * @param errorMessage       Error occurred while processing.
     * @return acknowledgement.
     */
    public Acknowledgement getFailureAcknowledgement(
            ApplicationRequest applicationRequest, ErrorMessage errorMessage) {
        Acknowledgement ack = new Acknowledgement();
        AckHeader ackHeader = new AckHeader();
        ackHeader.setApplicationId(null != applicationRequest && null != applicationRequest.getHeader() ?
                applicationRequest.getHeader().getApplicationId() : null);
        ackHeader.setInstitutionId(null != applicationRequest && null != applicationRequest.getHeader() ? applicationRequest.getHeader()
                .getInstitutionId() : null);
        ackHeader.setRequestReceivedTime(new Date());
        ackHeader.setResponseDateTime(new Date());
        ackHeader.setResponseType("");
        ack.setHeader(ackHeader);
        ack.setGonogoRefId(null != applicationRequest ? applicationRequest.getRefID() : null);
        ack.setStatus("ERROR");
        ack.setErrors(errorMessage);
        return ack;
    }
}
