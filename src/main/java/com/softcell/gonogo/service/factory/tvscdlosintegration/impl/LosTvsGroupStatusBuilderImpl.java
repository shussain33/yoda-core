package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.google.gson.Gson;
import com.softcell.constants.LosTvsGroupStatusLogEnums;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosTvsGroupStatusBuilder;
import org.springframework.stereotype.Service;

@Service
public class LosTvsGroupStatusBuilderImpl implements LosTvsGroupStatusBuilder {
    Gson gson = new Gson();
    @Override
    public TvsGroupServiceStatus setDefaultGroupStatus() {

        return TvsGroupServiceStatus.builder()
                .TVSGroup1Status(LosTvsGroupStatusLogEnums.N.name())
                .TVSGroup2Status(LosTvsGroupStatusLogEnums.N.name())
                .TVSGroup3Status(LosTvsGroupStatusLogEnums.N.name())
                .TVSGroup4Status(LosTvsGroupStatusLogEnums.N.name())
                .TVSGroup5Status(LosTvsGroupStatusLogEnums.N.name())
                .build();
    }

    @Override
    public TVSCdLosGroupResponse createGroupThreeResponse(String groupName, String action, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordGroupThree) {
        return TVSCdLosGroupResponse.builder()
                .groupName(groupName)
                .groupAction(action)
                .statusCode(statusCode)
                .statusMessage(statusMessage)
                .jsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupThree))
                .build();
    }

    @Override
    public TVSCdLosGroupResponse createGroupOneResponse(String groupName, String action, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneRequestJson) {
        return TVSCdLosGroupResponse.builder()
                .groupName(groupName)
                .groupAction(action)
                .statusCode(statusCode)
                .statusMessage(statusMessage)
                .jsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupOneRequestJson))
                .build();
    }

    @Override
    public TVSCdLosGroupResponse createGroupTwoResponse(String groupName, String action, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoRequestJson) {
        return TVSCdLosGroupResponse.builder()
                .groupName(groupName)
                .groupAction(action)
                .statusCode(statusCode)
                .statusMessage(statusMessage)
                .jsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupTwoRequestJson))
                .build();
    }

    @Override
    public TVSCdLosGroupResponse createGroupFourResponse(String groupName, String action, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupFour insertOrUpdateTvsRecordGroupFourRequestJson) {
        return TVSCdLosGroupResponse.builder()
                .groupName(groupName)
                .groupAction(action)
                .statusCode(statusCode)
                .statusMessage(statusMessage)
                .jsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupFourRequestJson))
                .build();
    }

    @Override
    public TVSCdLosGroupResponse createGroupFiveResponse(String groupName, String action, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveRequestJson) {
        return TVSCdLosGroupResponse.builder()
                .groupName(groupName)
                .groupAction(action)
                .statusCode(statusCode)
                .statusMessage(statusMessage)
                .jsonRequest(gson.toJson(insertOrUpdateTvsRecordGroupFiveRequestJson))
                .build();
    }
}
