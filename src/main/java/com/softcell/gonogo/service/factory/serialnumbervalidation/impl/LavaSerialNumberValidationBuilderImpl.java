package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.constants.ResponseConstants;
import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.lava.LavaRequest;
import com.softcell.gonogo.serialnumbervalidation.lava.LavaResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.LavaSerialNumberValidationBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.GngUtils;
import org.springframework.stereotype.Service;

/**
 * @author prasenjit wadmare
 * @date 02 Jan 2018
 */

@Service
public class LavaSerialNumberValidationBuilderImpl implements LavaSerialNumberValidationBuilder {


    @Override
    public LavaRequest buildLavaImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain lavaConfig, PostIpaRequest postIpaRequest) {
        return LavaRequest.builder()
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .modelNumber(serialSaleConfirmationRequest.getSkuCode())
                .brandName(serialSaleConfirmationRequest.getVendor())
                .retailerCode(serialSaleConfirmationRequest.getStoreCode())
                .price(Integer.toString((int)postIpaRequest.getPostIPA().getTotalAssetCost()))
                .partnerCode(lavaConfig.getMemberId())
                .key(lavaConfig.getLicenseKey())
                .subject(lavaConfig.getMemberId())
                .tokenName(lavaConfig.getKeys().get(0).getKeyValue())
                .build();
    }

    @Override
    public SerialNumberResponse buildLavaImeiNumberResponse(LavaResponse lavaResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.LAVA.toFaceValue())
                .status((lavaResponse.isVerifiedFlag() && 0 == lavaResponse.getErrorCode()) ? Status.VALID.name() : Status.INVALID.name())
                .message((lavaResponse.isVerifiedFlag() && 0 == lavaResponse.getErrorCode()) ? ResponseConstants.VALID_IMEI
                            :GngUtils.convertLavaResponseMessage(lavaResponse))
                .build();
    }
}
