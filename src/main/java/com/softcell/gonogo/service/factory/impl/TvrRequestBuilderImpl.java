package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.PhoneJsonKeys;
import com.softcell.constants.Status;
import com.softcell.constants.SurrogateType;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.los.tvr.TvrRequestDTO;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.surrogate.*;
import com.softcell.gonogo.service.factory.TvrRequestBuilder;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by bhuvneshk on 9/8/17.
 */
@Component
public class TvrRequestBuilderImpl implements TvrRequestBuilder {

    @Override
    public TvrRequestDTO buildTvrRequest(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        Header header = null;
        Applicant applicant = null;
        Name applicantName = null;
        com.softcell.gonogo.model.core.Application application = null;
        CreditCardSurrogate creditCardSurrogate = null;
        long creditCardNumber = 0;
        CarSurrogate carSurrogate = null;
        OwnHouseSurrogate ownHouseSurrogate = null;
        int netTakeHomeSurrSal= 0;
        Employment employment = null;
        String address1 = null;
        String address2 = null;
        String address3 = null;
        String oAddress1 = null;
        String oAddress2 = null;
        String oAddress3 = null;
        String state = null;
        String oState = null;
        int oZipCode = 0;
        int zipCode = 0;
        String oCity = null;
        String city = null;
        long oPhoneNumber = 0;
        int oAreaCode = 0;
        String oCountryCode;
        String oExtension = null;
        String countryCode;
        long pPhoneNumber = 0;
        long pAreaCode = 0;
        String pCountryCode;
        String pExtension;
        long rPhoneNumber = 0;
        int rAreaCode = 0;
        String rCountryCode;
        String rExtension = null;
        int rZipCode = 0;
        String rAddress1 = null;
        String rAddress2 = null;
        String rAddress3 = null;
        String rState = null;
        String rCity = null;
        String rResType = null;
        String cibilScore = null;
        String pEmailAddress = null;
        String oEmailAddress = null;
        String rEmailAddress = null;
        int applicationScore = 0;
        int amtApproved = 0;
        int eligibleAmount = 0;
        String panStatus = null;
        boolean negativePinFlag = false;
        String assetCtg = null;
        String assetMake = null;
        String modelNo = null;
        long aadhaarNumber = 0;
        String panNumber = null;
        String passportNumber = null;
        String voterId = null;
        String drivingLicense = null;
        String otherId = null;
        String panName = null;
        String surrogate = null;
        int traderYearInBusiness = 0;
        String bussinessProof = null;
        String houseType = null;
        int dealerId = 0;

        if (null != applicationRequest) {
            header = applicationRequest.getHeader();
            if (null != applicationRequest.getRequest()) {
                applicant = applicationRequest.getRequest().getApplicant();
                application = applicationRequest.getRequest().getApplication();
                if (null != applicant) {
                    applicantName = applicant.getApplicantName();
                }
            }
        }

        if (null != header) {
            try {
                dealerId = Integer.parseInt(header.getDealerId().trim());
            } catch (Exception e) {
                //ignore
            }
        }


        if (!CollectionUtils.isEmpty(goNoGoCustomerApplication.getCroDecisions())
                && null != goNoGoCustomerApplication.getCroDecisions().get(0)) {
            CroDecision croDecision = goNoGoCustomerApplication.getCroDecisions().get(0);
            amtApproved = (int) croDecision.getAmtApproved();
            eligibleAmount = (int) croDecision.getEligibleAmt();

        }

        if (null != goNoGoCustomerApplication.getIntrimStatus()) {
            if (null != goNoGoCustomerApplication.getIntrimStatus().getCibilModuleResult()) {
                cibilScore = goNoGoCustomerApplication.getIntrimStatus().getCibilModuleResult().getFieldValue();
            }
            if (null != goNoGoCustomerApplication.getIntrimStatus().getScoringModuleResult()) {
                String scoringScore = goNoGoCustomerApplication.getIntrimStatus().getScoringModuleResult().getFieldValue();
                if (StringUtils.isNotBlank(scoringScore)) {
                    try {
                        applicationScore = Integer.parseInt(scoringScore);
                    } catch (Exception e) {//ignore}
                    }
                }
            }
            if (null != goNoGoCustomerApplication.getIntrimStatus().getPanModuleResult()) {
                panStatus = goNoGoCustomerApplication.getIntrimStatus().getPanModuleResult().getMessage();
                panName = goNoGoCustomerApplication.getIntrimStatus().getPanModuleResult().getFieldValue();
            }
        }

        if (null != applicant) {
            if (null != applicant.getSurrogate()) {
                Surrogate surrogateSelected = applicant.getSurrogate();
                if (surrogateSelected.getSelectedSurrogate() != null) {

                    surrogate = surrogateSelected.getSelectedSurrogate().getTvrSurrogate();
                    if (StringUtils.equals(surrogateSelected.getSelectedSurrogate().getGngSurrogate(),
                            SurrogateType.CREDIT_SURROGATE.getGngSurrogate())) {
                        try {
                            creditCardSurrogate = surrogateSelected.getCreditCardSurrogate() != null ? surrogateSelected.getCreditCardSurrogate().get(0) : null;
                            if(null != creditCardSurrogate)
                            creditCardNumber = Long.parseLong(creditCardSurrogate.getCardNumber());
                        } catch (Exception e) {
                            //ignore
                        }
                    } else if (StringUtils.equals(surrogateSelected.getSelectedSurrogate().getGngSurrogate(),
                            SurrogateType.HOUSE_SURROGATE.getGngSurrogate())) {
                        ownHouseSurrogate = surrogateSelected.getHouseSurrogate() != null ? surrogateSelected.getHouseSurrogate().get(0) : null;
                        if (null != ownHouseSurrogate) {
                            houseType = ownHouseSurrogate.getHouseType();
                        }
                    } else if (StringUtils.equals(surrogateSelected.getSelectedSurrogate().getGngSurrogate(),
                            SurrogateType.BUSINESS_SURROGATE.getGngSurrogate())) {
                        TraderSurrogate traderSurrogate = surrogateSelected.getTraderSurrogate() != null ? surrogateSelected.getTraderSurrogate().get(0) : null;
                        if (null != traderSurrogate) {
                            traderYearInBusiness = traderSurrogate.getYearInBussines();
                            bussinessProof = traderSurrogate.getBussinesProof();
                        }
                    } else if (surrogateSelected.getSelectedSurrogate() != null
                            && StringUtils.equals(surrogateSelected.getSelectedSurrogate().getGngSurrogate(),
                            SurrogateType.CAR_SURROGATE.getGngSurrogate())) {
                          carSurrogate = surrogateSelected.getCarSurrogate() != null ? surrogateSelected.getCarSurrogate().get(0) : null;
                    }
                    else if (StringUtils.equals(surrogateSelected.getSelectedSurrogate().getGngSurrogate(),
                            SurrogateType.SALARIED_NET_TAKE_HOME.getGngSurrogate())) {
                        SalariedSurrogate salariedSurrogate = surrogateSelected.getSalariedSurrogate() != null ? surrogateSelected.getSalariedSurrogate().get(0) : null;
                        if(null != salariedSurrogate)
                            try {
                                netTakeHomeSurrSal = (int)salariedSurrogate.getNetTakeHome();
                            }catch (Exception e){
                            //Log unparseable salary.
                            }

                    }
                }
            }
            if (!CollectionUtils.isEmpty(applicant.getEmployment())) {
                employment = applicant.getEmployment().get(0);
            }

            if (!CollectionUtils.isEmpty(applicant.getAddress())) {

                for (com.softcell.gonogo.model.address.CustomerAddress address : applicant.getAddress()) {
                    switch (address.getAddressType()) {
                        case "PERMANENT": {
                            address1 = address.getAddressLine1();
                            address2 = address.getAddressLine2();
                            address3 = address.getLine3();
                            state = address.getState();
                            zipCode = (int) address.getPin();
                            city = address.getCity();
                            negativePinFlag = address.isNegativeAreaNotApplicableFlag();
                        }
                        break;
                        case "OFFICE": {
                            oZipCode = (int) address.getPin();
                            oAddress1 = address.getAddressLine1();
                            oAddress2 = address.getAddressLine2();
                            oAddress3 = address.getLine3();
                            oState = address.getState();
                            oCity = address.getCity();
                        }
                        break;
                        case "RESIDENCE": {
                            rZipCode = (int) address.getPin();
                            rAddress1 = address.getAddressLine1();
                            rAddress2 = address.getAddressLine2();
                            rAddress3 = address.getLine3();
                            rState = address.getState();
                            rCity = address.getCity();
                            rResType = address.getResidenceAddressType();
                        }
                        break;
                    }
                }
            }

            if (!CollectionUtils.isEmpty(applicant.getPhone())) {
                for (Phone phone : applicant.getPhone()) {
                    switch (phone.getPhoneType()) {
                        case PhoneJsonKeys.PERSONAL_MOBILE_TYPE: {
                            try {
                                pPhoneNumber = Long.parseLong(phone.getPhoneNumber());
                            } catch (Exception e) {
                                //ignore
                            }
                            try {
                                pAreaCode = Long.parseLong(phone.getAreaCode());
                            } catch (Exception e) {
                                //ignore
                            }
                            pCountryCode = phone.getCountryCode();
                            pExtension = phone.getExtension();
                        }
                        break;
                        case PhoneJsonKeys.OFFICE_MOBILE: {

                            try {
                                if (oAreaCode == 0) {
                                    try {
                                        oAreaCode = Integer.parseInt(phone.getAreaCode());
                                    } catch (Exception e) {
                                        //ignore
                                    }
                                }
                                oPhoneNumber = Long.parseLong(phone.getPhoneNumber());
                            } catch (Exception e) {
                                //ignore
                            }
                            oCountryCode = phone.getCountryCode();
                            oExtension = phone.getExtension();
                        }
                        break;
                        case PhoneJsonKeys.OFFICE_TYPE: {
                            if (oAreaCode == 0) {
                                try {
                                    oAreaCode = Integer.parseInt(phone.getAreaCode());
                                } catch (Exception e) {
                                    //ignore
                                }
                            }
                        }
                        break;
                        case PhoneJsonKeys.RESIDENCE_MOBILE: {
                            try {
                                rPhoneNumber = Long.parseLong(phone.getPhoneNumber());
                            } catch (Exception e) {
                                //ignore
                            }
                            if (rAreaCode == 0) {
                                try {
                                    rAreaCode = Integer.parseInt(phone.getAreaCode());
                                } catch (Exception e) {
                                    //ignore
                                }
                            }

                            rCountryCode = phone.getCountryCode();
                            rExtension = phone.getExtension();
                        }
                        break;
                        case PhoneJsonKeys.PERSONAL_PHONE_TYPE: {
                            if (rAreaCode == 0) {
                                try {
                                    rAreaCode = Integer.parseInt(phone.getAreaCode());
                                } catch (Exception e) {
                                    //ignore
                                }
                            }

                        }
                        break;
                    }
                }
            }

            if (!CollectionUtils.isEmpty(applicant.getEmail())) {
                List<Email> emails = applicant.getEmail();
                for (Email email : emails) {
                    switch (email.getEmailType()) {
                        case "PERSONAL": {
                            pEmailAddress = email.getEmailAddress();
                        }
                        break;
                        case "WORK": {
                            oEmailAddress = email.getEmailAddress();
                        }
                        break;
                        case "PERMANENT": {
                            rEmailAddress = email.getEmailAddress();
                        }
                        break;
                    }
                }
            }

            if (!CollectionUtils.isEmpty(applicant.getKyc())) {
                List<Kyc> kycs = applicant.getKyc();
                for (Kyc kyc : kycs) {
                    switch (kyc.getKycName()) {
                        case "AADHAAR": {
                            if (StringUtils.isNotBlank(kyc.getKycNumber())) {
                                try {
                                    aadhaarNumber = Long.parseLong(kyc.getKycNumber());
                                } catch (Exception ex) {
                                    //ignore
                                }
                            }
                        }
                        break;
                        case "PAN": {
                            panNumber = kyc.getKycNumber();
                        }
                        break;
                        case "PASSPORT": {
                            passportNumber = kyc.getKycNumber();
                        }
                        break;
                        case "VOTERID": {
                            voterId = kyc.getKycNumber();
                        }
                        break;
                        case "DRIVING-LICENSE": {
                            drivingLicense = kyc.getKycNumber();
                        }
                        break;
                        case "OTHER": {
                            otherId = kyc.getKycNumber();
                        }
                        break;
                    }
                }
            }
        }

        if (null != application) {
            if (!CollectionUtils.isEmpty(application.getAsset())) {
                AssetDetails assetDetails = application.getAsset().get(0);
                if (null != assetDetails) {
                    assetCtg = assetDetails.getAssetCtg();
                    assetMake = assetDetails.getAssetModelMake();
                    modelNo = assetDetails.getModelNo();
                }
            }
        }


        return TvrRequestDTO.builder()
                .applicationId(goNoGoCustomerApplication.getGngRefId())
                .requestDate(new Date())
                .dealerId(dealerId)
                .dsaId(header.getDsaId())
                .appliedAmount(null != application ? (int) application.getLoanAmount() : 0)
                .firstName(applicantName.getFirstName())
                .middleName(applicantName.getMiddleName())
                .lastName(applicantName.getLastName())
                .status(goNoGoCustomerApplication.getApplicationStatus())
                .gender(applicant.getGender())
                .maritalStatus(applicant.getMaritalStatus())
                .education(applicant.getEducation())
                .creditCardNumber(creditCardNumber)
                .creditCardsCategory(null != creditCardSurrogate ? creditCardSurrogate.getCardCategory() : null)
                .employerName(null != employment ? employment.getEmploymentName() : null)
                .employConstitution(null != employment ? employment.getConstitution() : null)
                .carCategory(null != carSurrogate ? carSurrogate.getCategory() : null)
                .carManufacturerYear(null != carSurrogate ? carSurrogate.getManufactureYear() : null)
                .carModelName(null != carSurrogate ? carSurrogate.getModelName() : null)
                .carRegistrationNumber(null != carSurrogate ? carSurrogate.getRegistrationNumber() : null)
                .addressLine1(rAddress1)
                .addressLine2(rAddress2)
                .addressLine3(rAddress3)
                .state(rState)
                .zipCode(rZipCode)
                .city(rCity)
                .oAddressLine1(oAddress1)
                .oAddressLine2(oAddress2)
                .oAddressLine3(oAddress3)
                .oState(oState)
                .oZipCode(oZipCode)
                .oCity(oCity)
                .oPhoneAreaCode(oAreaCode)
                .oPhoneNumber(oPhoneNumber)
                .oPhoneType(GNGWorkflowConstant.OFFICE_MOBILE.toFaceValue())
                .oPhoneExt(oExtension)
                .oEmailAddress(oEmailAddress)
                .oEmailType(GNGWorkflowConstant.WORK.toFaceValue())
                .pAddressLine1(address1)
                .pAddressLine2(address2)
                .pAddressLine3(address3)
                .pCity(city)
                .pZipCode(zipCode)
                .pState(state)
                .pPhoneNumber(pPhoneNumber)
                .pPhoneAreaCode(pAreaCode)
                .pPhoneType(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue())
                .cibilScore(cibilScore)
                .applicationScore(applicationScore)
                .rHomeAreaCode(rAreaCode)
                .rHomeExt(rExtension)
                .residenceType(rResType)
                .rHomeNumber(rPhoneNumber)
                .rEmailAddress(pEmailAddress)
                .rEmailType(GNGWorkflowConstant.PERSONAL.toFaceValue())
                .approvedAmountByCro(amtApproved)
                .eligibleAmount(eligibleAmount)
                .dedupeStatus(CollectionUtils.isEmpty(goNoGoCustomerApplication.getDedupedApplications()) ? GNGWorkflowConstant.NO_MATCH.toFaceValue() : GNGWorkflowConstant.MATCH.toFaceValue())
                .houseSurrogateDocumentType(null != ownHouseSurrogate ? ownHouseSurrogate.getDocumentType() : null)
                .ownHouseType(houseType)
                .kyc1Pan(panNumber)
                .kyc2VoterId(voterId)
                .kyc3Passport(passportNumber)
                .kyc4DrivingLicense(drivingLicense)
                .kyc5AadhaarNumber(aadhaarNumber)
                .kyc6Other(otherId)
                .panStatus(panStatus)
                .panName(panName)
                .queueId(StringUtils.equals(Status.STP.name(), header.getCroId()) ? Status.STP.name() : Status.DEFAULT.name())
                .primaryAssetCtg(assetCtg)
                .primaryAssetMake(assetMake)
                .primaryAssetModelNo(modelNo)
                .loginDate(header.getDateTime())
                .timeStamp(GngDateUtil.getTimeStamp(header.getDateTime()))
                .dob(Integer.parseInt(GngDateUtil.getDateInYyyyMMddFormat(applicant.getDateOfBirth())))
                .approvedDate(GngDateUtil.getDateInYyyyMMddFormat(goNoGoCustomerApplication.getDateTime()))
                .traderYearInBusiness((short) traderYearInBusiness)
                .traderBusinessProof(bussinessProof)
                .netTakeHomeSalary(netTakeHomeSurrSal)
                .rHomeTyrE("")
                .surrogate(surrogate)
                .negativePinFlag(negativePinFlag ? Status.YES.name() : Status.NO.name())
                .build();
    }
}
