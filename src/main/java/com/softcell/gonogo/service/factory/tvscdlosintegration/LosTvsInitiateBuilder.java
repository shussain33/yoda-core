package com.softcell.gonogo.service.factory.tvscdlosintegration;


import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.nextgen.domain.WFJobCommDomain;

public interface LosTvsInitiateBuilder {

    InitiateRequest buildLosTvsInitiateRequest(WFJobCommDomain lostvsconfig, TvsIntiateData tvsIntiateData);

    TVSGroupOneRequest buildLosTvsGroupOneRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneResponse);

    TVSGroupTwoRequest buildLosTvsGroupTwoRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoResponse);

    TVSGroupThreeRequest buildLosTvsGroupThreeRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordResponse);

    TVSGroupFourRequest buildLosTvsGroupFourRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupFour groupFourReturn);

    TVSGroupFiveRequest buildLosTvsGroupFiveRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveResponse);

    TvsIntiateData buildLosTvsInitiateInputRequest(LosTvsRequest losTvsRequest);

    TvsIntiateData buildLosTvsInitiateInputRequestForCaseId(String caseId);
}
