package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.model.kyc.request.*;
import com.softcell.gonogo.model.kyc.request.gstRequests.KGstIdentityRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.KGstRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstAuthDetailRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstIdentyRequest;
import com.softcell.gonogo.service.factory.TotalKycBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import org.springframework.stereotype.Service;

/**
 * Created by anupamad on 14/7/17.
 */

@Service
public class TotalKycBuilderImpl implements TotalKycBuilder{

    @Override
    public KLpgIdRequest buildLpgDetailRequest(TotalKycLpgDetailRequest totalKycLpgDtlRequest, WFJobCommDomain lpgConfig) {
        return KLpgIdRequest.builder()
                .key(lpgConfig.getLicenseKey())
                .mobileNumber(totalKycLpgDtlRequest.getMobileNumber())
                .build();
    }

    @Override
    public KDrivLicRequest buildDlDetailRequest(TotalKycDlDetailRequest totalKycDlDtlRequest, WFJobCommDomain lpgConfig){
        return KDrivLicRequest.builder()
                .key(lpgConfig.getLicenseKey())
                .dlNo(totalKycDlDtlRequest.getDlNo())
                .dob(totalKycDlDtlRequest.getDob())
                .build();

    }

    @Override
    public KElecRequest buildElecDetailRequest(TotalKycElecDtlRequest totalKycElecDtlRequest, WFJobCommDomain totalKycConfig) {
        return KElecRequest.builder()
                .key(totalKycConfig.getLicenseKey())
                .consumerId(totalKycElecDtlRequest.getConsumerId())
                .serviceProvider(totalKycElecDtlRequest.getServiceProvider())
                .build();
    }

    @Override
    public KVoterIdRequest buildVoterDetailRequest(TotalKycVoterDetailRequest totalKycVoterDetailRequest, WFJobCommDomain voterConfig) {
        return KVoterIdRequest.builder()
                .key(voterConfig.getLicenseKey())
                .epicNo(totalKycVoterDetailRequest.getEpicNo())
                .build();

    }

    @Override
    public KLpgIdRequestV2 buildLpgDetailRequestV2(TotalKycLpgDetailRequestV2 totalKycLpgV2DtlRequest)
    {
        return KLpgIdRequestV2.builder()
                .consent(totalKycLpgV2DtlRequest.getConsent())
                .mobileNumber(totalKycLpgV2DtlRequest.getMobileNumber())
                .build();
    }

    @Override
    public KDrivLicRequestV2 buildDlDetailRequestV2(TotalKycDlDetailRequestV2 totalKycDlDetailRequestV2)
    {
        return KDrivLicRequestV2.builder()
                .dlNo(totalKycDlDetailRequestV2.getDlNo())
                .dob(totalKycDlDetailRequestV2.getDob())
                .consent(totalKycDlDetailRequestV2.getConsent())
                .build();
    }

    @Override
    public KVoterIdRequestV2 buildVoterDetailRequestV2(TotalKycVoterDetailRequestV2 totalKycVoterDetailRequestV2)
    {
        return KVoterIdRequestV2.builder()
                .epicNo(totalKycVoterDetailRequestV2.getEpicNo())
                .consent(totalKycVoterDetailRequestV2.getConsent())
                .build();

    }

    @Override
    public KElecRequestV2 buildElecDetailRequestV2(TotalKycElecDetailRequestV2 totalKycElecDetailRequestV2)
    {
        return KElecRequestV2.builder()
                .consumerId(totalKycElecDetailRequestV2.getConsumerId())
                .serviceProvider(totalKycElecDetailRequestV2.getServiceProvider())
                .consent(totalKycElecDetailRequestV2.getConsent())
                .build();
    }

    @Override
    public KGstRequest buildGstAuthRequest(TotalKycGstAuthDetailRequest totalKycGstAuthDetailRequest)
    {
        return KGstRequest.builder()
                .gstin(totalKycGstAuthDetailRequest.getGstin())
                .consent(totalKycGstAuthDetailRequest.getConsent())
                .build();

    }

    @Override
    public KGstIdentityRequest buildGstIdentityRequest(TotalKycGstIdentyRequest totalKycGstIdentyRequest) {
        //prepare request for dmzConnector
        return KGstIdentityRequest.builder()
                .gstin(totalKycGstIdentyRequest.getGstin())
                .kycNumber(totalKycGstIdentyRequest.getKycNumber())
                .state(totalKycGstIdentyRequest.getState())
                .consent(totalKycGstIdentyRequest.getConsent())
                .gstinAvailable(totalKycGstIdentyRequest.isGstinAvailable())
                .build();
    }

    @Override
    public KPanRequest buildPanAuthRequest(TotalKycPanAuthDetailRequest totalKycPanAuthDetailRequest)
    {
        return KPanRequest.builder()
                .pan(totalKycPanAuthDetailRequest.getPan())
                .consent(totalKycPanAuthDetailRequest.getConsent())
                .build();

    }
}
