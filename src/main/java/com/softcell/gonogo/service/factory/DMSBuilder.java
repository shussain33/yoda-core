package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.dms.addfolder.AddFolderDmsOriginalResponse;
import com.softcell.gonogo.model.dms.addfolder.AddFolderRequest;
import com.softcell.gonogo.model.dms.additionaldoc.DmsAdditionalInformation;
import com.softcell.gonogo.model.dms.collection.DMSCreatedFolderInformation;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;
import com.softcell.gonogo.model.dms.connectcabinet.ConnectCabinetRequest;
import com.softcell.gonogo.model.dms.postdocument.PostDocumentRequest;
import com.softcell.gonogo.model.dms.postdocument.PostDocumentResponse;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.GetFileRequest;

import java.io.IOException;
import java.util.Date;
import java.util.Set;

/**
 * Created by mahesh on 24/6/17.
 */
public interface DMSBuilder {
    /**
     * @param losId
     * @param refId
     * @param institutionId
     * @param folderName
     * @param parentFolderId
     * @return
     */
    AddFolderRequest buildAddFolderRequest(String losId, String refId, String institutionId, String folderName, long parentFolderId, int userDbId);

    /**
     * @param refId
     * @param institutionId
     */
    CheckApplicationStatus buildGetApplicationImagesRequest(String refId, String institutionId);

    /**
     * @param refId
     * @param institutionId
     * @param addFolderDmsOriginalResponse
     * @return
     */
    DMSCreatedFolderInformation buildCreatedFolderInformationObject(String refId, String institutionId, AddFolderDmsOriginalResponse addFolderDmsOriginalResponse);

    /**
     * @param imageId
     * @param institutionId
     * @return
     */
    GetFileRequest buildGetDocumentBase64ImageRequest(String imageId, String institutionId);

    /**
     * @param institutionId
     * @param losId
     * @param byteCode
     * @param folderId
     * @return
     */
    PostDocumentRequest buildDocumentPushRequest(ImagesDetails imagesDetails, String institutionId, String losId, String byteCode, long folderId, int userDbId, Set<DmsAdditionalInformation> dmsAdditionalInformations) throws IOException;

    /**
     * @param postDocumentResponse
     * @param refId
     * @param institutionId
     * @param documentObjectId
     * @return
     */
    DMSPushDocumentInformation buildPushDocumentInformationSuccessObject(PostDocumentResponse postDocumentResponse, String refId, String institutionId, String documentObjectId ,String folderName ,Date createDate);

    /**
     * @param losId
     * @param institutionId
     * @return
     */
    ConnectCabinetRequest buildConnectCabinetObject(String losId, String institutionId, String requestType);

    /**
     * @param losId
     * @param institutionId
     * @param name
     * @param userDbId
     * @return
     */
    ConnectCabinetRequest buildDisConnectCabinetObject(String losId, String institutionId, String name, int userDbId);

    /**
     *
     * @param imagesDetails
     * @param refId
     * @param institutionId
     * @return
     */
    DMSPushDocumentInformation buildPushDocumentInformationFailedObject(ImagesDetails imagesDetails, String refId, String institutionId ,Date createDate);

    /**
     *
     * @param pushDMSDocumentsRequest
     * @param errorMessage
     * @param errorCode
     * @return
     */
    PushDMSDocumentsRequest buildDmsErrorLogObject(PushDMSDocumentsRequest pushDMSDocumentsRequest, String errorMessage ,String errorCode);
}
