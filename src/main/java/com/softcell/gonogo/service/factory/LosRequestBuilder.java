package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.LosMbData;
import com.softcell.gonogo.model.request.PostIpaRequest;

import javax.xml.bind.JAXBException;


/**
 * LosRequestBuilder build request for gonogo specific and
 * multibureau specific
 *
 * @author bhuvneshk
 */
public interface LosRequestBuilder {


    /**
     * This method will build los request xml based on gng object
     *
     * @param goNoGoCustomerApplication
     * @param postIpaRequest
     * @return
     * @throws JAXBException
     */
     String buildLosCustomerRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, PostIpaRequest postIpaRequest,LosMbData losMbData) throws JAXBException, Exception;

}
