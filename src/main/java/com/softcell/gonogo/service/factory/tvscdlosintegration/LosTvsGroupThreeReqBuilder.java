package com.softcell.gonogo.service.factory.tvscdlosintegration;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.InsertOrUpdateTvsRecordGroupThree;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;

public interface LosTvsGroupThreeReqBuilder {

    public InsertOrUpdateTvsRecordGroupThree buildLosTvsGroupThreeRequest(GoNoGoCustomerApplication gonogoCustomerApplication, PostIpaRequest postIpaRequest, SerialNumberInfo serialInfo, InsurancePremiumDetails insurancePremiumDetails, SchemeMasterData schemeMasterData, ExtendedWarrantyDetails extendedWarrantyDetails);
}
