package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaRequest;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.NokiaSerialNumberValidationBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

@Service
public class NokiaSerialNumberValidationBuilderImpl implements NokiaSerialNumberValidationBuilder {

    @Override
    public NokiaRequest buildNokiaImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain nokiaConfig) {
        return NokiaRequest.builder()
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .modelNumber(serialSaleConfirmationRequest.getSkuCode())
                .requestId(serialSaleConfirmationRequest.getReferenceID())
                .clientId(Cache.getVendorByInstitution(serialSaleConfirmationRequest.getHeader().getInstitutionId()))
                .statusValue((short) 1)
                .build();

    }

    @Override
    public SerialNumberResponse buildNokiaImeiNumberResponse(NokiaResponse nokiaResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.NOKIA.toFaceValue())
                .message(GngUtils.convertNokiaResponseCodeToMessage(Integer.parseInt(nokiaResponse.getResponseCode())))
                .status((StringUtils.equals("0",nokiaResponse.getResponseCode()) ? Status.VALID.name() : Status.INVALID.name()))
                .build();
    }

    @Override
    public NokiaRequest buildNokiaImeiRollbackRequest(RollbackRequest rollbackRequest) {
        return NokiaRequest.builder()
                .imeiNumber(rollbackRequest.getImeiNumber())
                .modelNumber(rollbackRequest.getSkuCode())
                .requestId(rollbackRequest.getReferenceID())
                .clientId(Cache.getVendorByInstitution(rollbackRequest.getHeader().getInstitutionId()))
                .statusValue((short) 2)
                .build();

    }

    @Override
    public SerialNumberResponse buildNokiaImeiRollbackResponse(NokiaResponse nokiaResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.NOKIA.toFaceValue())
                .message(GngUtils.convertNokiaRollbackResponseCodeToMessage(Integer.parseInt(nokiaResponse.getResponseCode())))
                .status((StringUtils.equals("0", nokiaResponse.getResponseCode()) ? Status.SUCCESS.name() : Status.FAILED.name()))
                .build();
    }

}
