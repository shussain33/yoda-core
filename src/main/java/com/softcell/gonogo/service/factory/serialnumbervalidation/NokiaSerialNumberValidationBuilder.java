package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaRequest;
import com.softcell.gonogo.serialnumbervalidation.nokia.NokiaResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

public interface NokiaSerialNumberValidationBuilder {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param nokiaConfig
     * @return
     */
    NokiaRequest buildNokiaImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain nokiaConfig);

    /**
     *
     * @param nokiaResponse
     * @return
     */
    SerialNumberResponse buildNokiaImeiNumberResponse(NokiaResponse nokiaResponse);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    NokiaRequest buildNokiaImeiRollbackRequest(RollbackRequest rollbackRequest);

    /**
     *
     * @param nokiaResponse
     * @return
     */
    SerialNumberResponse buildNokiaImeiRollbackResponse(NokiaResponse nokiaResponse);




}
