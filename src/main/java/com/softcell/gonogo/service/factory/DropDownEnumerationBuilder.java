/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 4, 2016 9:31:48 PM
 */
package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.masters.ApplicationMetaData;

/**
 * @author kishorp
 *
 */
public interface DropDownEnumerationBuilder {
    /**
     *
     * @param institutionId
     * @return
     */
    public ApplicationMetaData build(String institutionId);
}
