package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.AmbitMifin.AccomodationTypeEnum;
import com.softcell.constants.AmbitMifin.ConstitutionEnum;
import com.softcell.constants.AmbitMifin.EducationTypeEnum;
import com.softcell.constants.AmbitMifin.ProductEnum;
import com.softcell.constants.AmbitMifin.ChargesEnum;
import com.softcell.constants.AmbitMifin.AccountTypeEnum;
import com.softcell.constants.AmbitMifin.RepaymentTypeEnum;
import com.softcell.constants.AmbitMifin.AddressTypeEnum;
import com.softcell.constants.AmbitMifin.SchemeEnum;
import com.softcell.constants.ApplicantType;
import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.KYC_TYPES;
import com.softcell.constants.MifinConstants;
import com.softcell.constants.Product;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Constant;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import com.softcell.gonogo.model.core.cam.RTRTransaction;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.core.verification.PropertyVerificationInput;
import com.softcell.gonogo.model.core.verification.PropertyVerificationOutput;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.address.MifinAddress;
import com.softcell.gonogo.model.core.AmountForDate;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.Application;
import com.softcell.gonogo.model.core.BalanceSheet;
import com.softcell.gonogo.model.core.BankingStatement;
import com.softcell.gonogo.model.core.BankingTransaction;
import com.softcell.gonogo.model.core.InsurancePolicy;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.PLAndBSAnalysis;
import com.softcell.gonogo.model.core.ProfitAndLoss;
import com.softcell.gonogo.model.core.StatementFieldDetails;
import com.softcell.gonogo.model.core.PersonalDiscussion;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import com.softcell.gonogo.model.core.cam.RTRTransaction;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.verification.PropertyVerification;
import com.softcell.gonogo.model.core.verification.PropertyVerificationInput;
import com.softcell.gonogo.model.core.verification.PropertyVerificationOutput;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.core.ProfessionIncomeDetails;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.CroJustification;
import com.softcell.gonogo.model.masters.PinCodeMaster;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.RepaymentDetails;
import com.softcell.gonogo.model.ops.DMInput;
import com.softcell.gonogo.model.ops.LoanDetails;
import com.softcell.gonogo.model.request.AmbitMifinRequest.AuthenticationDetails;
import com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision.CRBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision.CrDecisionRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision.CreditDecisionRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision.sanctionTermsrequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant.ApplicantInfoRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant.DeleteApplicantBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant.DeleteApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker.*;
import com.softcell.gonogo.model.request.AmbitMifinRequest.NewLoan.NewLoanBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.NewLoan.NewLoanCreationRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantAddress.SaveAddressBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantAddress.SaveApplicantAddressRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.PanSearch.PanSearchBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.PanSearch.PanSearchRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.ProcessDedupe.ProcessDedupeBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.ProcessDedupe.ProcessDedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails.AuthorisedSign;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails.SaveApplicantBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails.SaveApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.BankDetails;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.Financial;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.Itr;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.OtherLoansDet;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.SaveFinancialInfoBankingDetails;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.SaveFinancialInfoBasicInfoDetails;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.SaveFinancialInfoRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.Viability;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SavePersonalInsurance.SavePersonalInsuranceBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SavePersonalInsurance.SavePersonalInsuranceRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification.SaveVerificationBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification.SaveVerificationRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification.VerificationQuesAns;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateLoanDetails.LoanDetailsBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateLoanDetails.UpdateLoanDetailsRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SearchExistingApplicantSearch.SearchApplicantBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SearchExistingApplicantSearch.SearchExistingApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.DedupeUpdate;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.ApplicantdedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.UpdateDedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateStatusDedupeBasicInfo;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateStatusDedupeResquest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.InsuranceData;
import com.softcell.gonogo.model.response.AmbitMifinResponse.PanSearch.MiFinPanSearchBaseResponse;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.service.factory.AmbitMifinRequestBuilder;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.thirdparty.utils.ThirdPartyFieldConstant;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Component
public class AmbitMifinRequestBuilderImpl implements AmbitMifinRequestBuilder {
    @Autowired
    private ApplicationRepository applicationRepository;


    @Override
    public PanSearchRequest buildPanSearchRequest(ApplicationRequest applicationRequest, Applicant applicant, String userId) throws Exception {
        return PanSearchRequest.builder()
                .authentication(buildAuthDetails(userId))
                .panSearchBasicInfo(buildPanSearchBasicInfo(applicationRequest,applicant))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_PAN_SEARCH)
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .build();
    }

    public PanSearchBasicInfo buildPanSearchBasicInfo(ApplicationRequest applicationRequest, Applicant applicant) throws Exception {

        return PanSearchBasicInfo.builder()
                .pan(applicationRequest.getRequest().getPanSearchNo())
                .entityType(ApplicantType.isIndividual(applicant.getApplicantType()) ?
                        ApplicantType.INDIVIDUAL.toString(): ApplicantType.NON_INDIVIDUAL.value())
                .build();
    }
    public NewLoanCreationRequest buildLoanCreationRequest(ApplicationRequest applicationRequest, String userId) throws Exception {

        return NewLoanCreationRequest.builder()
                .authentication(buildAuthDetails(userId))
                .newLoanBasicInfo(buildCreateLoanBasicInfo(applicationRequest))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_CREATE_LOAN)
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .p_form(StringUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getImageform60())?applicationRequest.getRequest().getApplicant().getImageform60():"")
                .build();
    }



    public SaveApplicantRequest buildSaveApplicantDetailsRequest(ApplicationRequest applicationRequest,String appId, String userId) throws Exception {

        return SaveApplicantRequest.builder()
                .authentication(buildAuthDetails(userId))
                .saveApplicantBasicInfo(buildSaveApplicantBasicInfo(applicationRequest,appId))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_SAVE_APPLICANT_DETAILS)
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .p_form(getform60(applicationRequest,appId))
                .build();
    }

    @Override
    public ProcessDedupeRequest buildProcessDedupeRequest (ApplicationRequest applicationRequest,Applicant applicant, String userId) throws Exception {
        return ProcessDedupeRequest.builder()
                .authentication(buildAuthDetails(userId))
                .processDedupeBasicInfo(buildProcessDedupeBasicInfo(applicationRequest,applicant))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_PROCESS_DEDUPE)
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .build();
    }
    private ProcessDedupeBasicInfo buildProcessDedupeBasicInfo(ApplicationRequest applicationRequest, Applicant applicant) {
        return ProcessDedupeBasicInfo.builder()
                .prospectCode(applicationRequest.getMiFinProspectCode())
                .applicantCode(null != applicant.getAmbitMifinData() ? applicant.getAmbitMifinData().getMiFinApplicantCode():Constant.BLANK)
                .build();
    }

    private NewLoanBasicInfo buildCreateLoanBasicInfo(ApplicationRequest applicationRequest) {
        NewLoanBasicInfo newLoanBasicInfo = new NewLoanBasicInfo();

        if (null != applicationRequest.getRequest() && null != applicationRequest.getRequest().getApplicant()) {
            Applicant applicant = applicationRequest.getRequest().getApplicant();

            newLoanBasicInfo.setProduct(ProductEnum.getMifinProductNameFromGonogoName(applicationRequest.getHeader().getProduct().toProductName()));

            if(ApplicantType.INDIVIDUAL.value().equals(applicant.getApplicantType())) {

                if (null != applicant.getApplicantName()) {
                    newLoanBasicInfo.setFname(GngUtils.setValueForNull(applicant.getApplicantName().getFirstName()).toUpperCase());
                    newLoanBasicInfo.setMname(GngUtils.setValueForNull(applicant.getApplicantName().getMiddleName()).toUpperCase());
                    newLoanBasicInfo.setLname(GngUtils.setValueForNull(applicant.getApplicantName().getLastName()).toUpperCase());
                }
                if (applicant.getFatherName() != null) {
                    newLoanBasicInfo.setFatherFName(GngUtils.setValueForNull(applicant.getFatherName().getFirstName()));
                    newLoanBasicInfo.setFatherMName(GngUtils.setValueForNull(applicant.getFatherName().getMiddleName()));
                    newLoanBasicInfo.setFatherLName(GngUtils.setValueForNull(applicant.getFatherName().getLastName()));
                }
                else{
                    newLoanBasicInfo.setFatherFName("testFatherFname");
                    newLoanBasicInfo.setFatherLName("testFatherLname");
                }
            }
            if (StringUtils.isNotEmpty(applicant.getDateOfBirth())) {
                if (!(StringUtils.equalsIgnoreCase(MifinConstants.INVALID_DATE, applicant.getDateOfBirth()))) {

                    newLoanBasicInfo.setDateOfBirth(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                    (GngDateUtil.ddMMyyyy,
                                            applicant.getDateOfBirth()),
                            GngDateUtil.ddMMMyyyy_WITH_HYPHEN));

                } else {
                    newLoanBasicInfo.setDateOfBirth("13-MAR-1998");
                }
            } else {
                newLoanBasicInfo.setDateOfBirth("13-MAR-1998");
            }
            newLoanBasicInfo.setGender(null != applicant.getGender()?
                    (StringUtils.equalsIgnoreCase(applicant.getGender(),GNGWorkflowConstant.TRANSGENDER.toFaceValue()) ?
                            GNGWorkflowConstant.OTHER.toFaceValue() : applicant.getGender().toUpperCase())
                    :"");
            newLoanBasicInfo.setNoOfDepChild(String.valueOf(applicant.getiNoOfChildren()));
            newLoanBasicInfo.setAffordEMI(Constant.BLANK);
            newLoanBasicInfo.setResiPhone(StringUtils.isNotEmpty(applicant.getPersonalLandline()) ? applicant.getPersonalLandline():"05942-233567");
            newLoanBasicInfo.setResiPhone2(StringUtils.isNotEmpty(applicant.getPersonalLandline()) ? applicant.getPersonalLandline():"05942-233567");
            newLoanBasicInfo.setIspanavailable("N");
            newLoanBasicInfo.setAdhaar("N");
            newLoanBasicInfo.setCin("FDDD222");

            List<Kyc> kycDetailsArray = applicant.getKyc();
            if(CollectionUtils.isNotEmpty(kycDetailsArray)) {
                for (Kyc kyc : kycDetailsArray) {
                    if (StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.PAN.toFaceValue())) {
                        newLoanBasicInfo.setPanNo(kyc.getKycNumber());
                        newLoanBasicInfo.setIspanavailable("Y");
                        if (!(applicant.getApplicantType().equals(ApplicantType.INDIVIDUAL.value()))) {
                            newLoanBasicInfo.setCompanyPan(kyc.getKycNumber());
                        } else
                            newLoanBasicInfo.setCompanyPan("SSSSS3222S");

                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.DRIVING_LICENSE.toString())) {
                        newLoanBasicInfo.setDrivingLicNo(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.PASSPORT.toString())) {
                        newLoanBasicInfo.setPassportNo(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.VOTER_ID.toString())) {
                        newLoanBasicInfo.setVoter_id(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), MifinConstants.LPG_BILL)) {
                        newLoanBasicInfo.setOther_name(MifinConstants.LPG_BILL);
                        newLoanBasicInfo.setOthers_id_no(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), MifinConstants.PNG_BILL)) {
                        newLoanBasicInfo.setOther_name(MifinConstants.PNG_BILL);
                        newLoanBasicInfo.setOthers_id_no(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), MifinConstants.ELECTRICITY_BILL)) {
                        newLoanBasicInfo.setOther_name(MifinConstants.ELECTRICITY_BILL);
                        newLoanBasicInfo.setOthers_id_no(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.AADHAAR.toFaceValue())) {
                        newLoanBasicInfo.setAdhaar(GNGWorkflowConstant.YES.toFaceValue());
                    }

                }
            }

            if(StringUtils.isNotEmpty(applicant.getImageform60())){
                newLoanBasicInfo.setForm_name(StringUtils.isNotEmpty(applicant.getForm60Name()) ? applicant.getForm60Name() : "Form60Cell");
            }
            else
                newLoanBasicInfo.setForm_name(Constant.BLANK);

            List<CustomerAddress> address = applicant.getAddress();
            if (CollectionUtils.isNotEmpty(address)) {
                address.forEach(obj -> {
                    if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.OFFICE.toFaceValue())) {
                        newLoanBasicInfo.setOffHouseNo(obj.getAddressLine1());
                        PinCodeMaster pinCodeMaster=applicationRepository.fetchPinCodeDetails(applicationRequest.getHeader().getInstitutionId(), Long.toString(obj.getPin()));

                        if(null!=pinCodeMaster) {
                            newLoanBasicInfo.setOffState(StringUtils.isEmpty(pinCodeMaster.getStateCode()) ? Constant.BLANK : pinCodeMaster.getStateCode());

                            newLoanBasicInfo.setOffCity(StringUtils.isEmpty(pinCodeMaster.getCityCode()) ? Constant.BLANK : pinCodeMaster.getCityCode());

                        }newLoanBasicInfo.setOffPincode(String.valueOf(obj.getPin()));
                        newLoanBasicInfo.setMonthsAtOffice("0");

                        newLoanBasicInfo.setOffAccomodationType(AccomodationTypeEnum.getMifinAccomodationTypeFromGonogoType(obj.getResidenceAddressType()));
                        if(obj.getMailingAddress()) newLoanBasicInfo.setMailingAddress("O");

                        if(obj.getDestinationAddress())  newLoanBasicInfo.setDestinationAddress("O");

                        newLoanBasicInfo.setOffStreetNo(obj.getAddressLine2());
                        newLoanBasicInfo.setOffLandmark(obj.getLandMark()=="" ? "OFFNDMARK":obj.getLandMark());
                        newLoanBasicInfo.setOffDistrict(obj.getCity());
                        newLoanBasicInfo.setOffAreaName("OADDR3");
                        newLoanBasicInfo.setSocialCategory(StringUtils.isNotEmpty(applicant.getSocialCategory())? applicant.getSocialCategory() : "1000000001");
                        newLoanBasicInfo.setYearsAtOffice(String.valueOf(obj.getTimeAtAddress()));
                        newLoanBasicInfo.setOffYrOfStayAtCurrentArea(String.valueOf(obj.getTimeAtAddress()));
                        newLoanBasicInfo.setOffCompanyName(obj.getAddrCompanyName());
                    }
                    if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())) {
                        newLoanBasicInfo.setResiHouseNo(obj.getAddressLine1());
                        PinCodeMaster pinCodeMaster=applicationRepository.fetchPinCodeDetails(applicationRequest.getHeader().getInstitutionId(), Long.toString(obj.getPin()));
                        if(null!=pinCodeMaster) {
                            newLoanBasicInfo.setResiState(StringUtils.isEmpty(pinCodeMaster.getStateCode()) ? Constant.BLANK : pinCodeMaster.getStateCode());
                            newLoanBasicInfo.setResiCity(StringUtils.isEmpty(pinCodeMaster.getCityCode()) ? Constant.BLANK : pinCodeMaster.getCityCode());
                        }newLoanBasicInfo.setResiStreetNo(obj.getAddressLine2());
                        if(obj.getMailingAddress()) newLoanBasicInfo.setMailingAddress("R");
                        if(obj.getDestinationAddress()) newLoanBasicInfo.setDestinationAddress("R");

                        newLoanBasicInfo.setResiLandmark(obj.getLandMark()=="" ? "RESILANDMARK":obj.getLandMark());
                        newLoanBasicInfo.setResiAreaName("RESAREA");
                        newLoanBasicInfo.setResiDistrict(obj.getCity());
                        newLoanBasicInfo.setResiPincode(String.valueOf(obj.getPin()));
                        newLoanBasicInfo.setMonthAtResi("0");
                        newLoanBasicInfo.setResYrOfStayAtCurrentArea(String.valueOf(obj.getTimeAtAddress()));
                        newLoanBasicInfo.setResiAccomodationType(AccomodationTypeEnum.getMifinAccomodationTypeFromGonogoType(obj.getResidenceAddressType()));

                        newLoanBasicInfo.setYearAtResi(String.valueOf(obj.getTimeAtAddress()));
                    }

                    if (StringUtils.equals(obj.getAddressType(), GNGWorkflowConstant.PERMANENT.toFaceValue())) {
                        newLoanBasicInfo.setPermHouseNo(obj.getAddressLine1());
                        PinCodeMaster pinCodeMaster=applicationRepository.fetchPinCodeDetails(applicationRequest.getHeader().getInstitutionId(), Long.toString(obj.getPin()));
                        if(null!=pinCodeMaster) {
                            newLoanBasicInfo.setPermState((StringUtils.isEmpty(pinCodeMaster.getStateCode()) ? Constant.BLANK : pinCodeMaster.getStateCode()));
                            newLoanBasicInfo.setPermCity(StringUtils.isEmpty(pinCodeMaster.getCityCode()) ? Constant.BLANK : pinCodeMaster.getCityCode());
                        }newLoanBasicInfo.setPermStreetNo(obj.getAddressLine2());
                        newLoanBasicInfo.setPermAreaName("PERMAREA");
                        if(obj.getMailingAddress()) newLoanBasicInfo.setMailingAddress("P");
                        if(obj.getDestinationAddress()) newLoanBasicInfo.setDestinationAddress("P");

                        newLoanBasicInfo.setPermLandmark(obj.getLandMark()=="" ? "PERMLANDMARK":obj.getLandMark());
                        newLoanBasicInfo.setPermDistrict(obj.getCity());
                        newLoanBasicInfo.setPermPincode(String.valueOf(obj.getPin()));
                        newLoanBasicInfo.setMonthsAtPerm("1");
                        newLoanBasicInfo.setYearsAtPerm(String.valueOf(obj.getTimeAtAddress()));
                        newLoanBasicInfo.setPermAccomodationType(AccomodationTypeEnum.getMifinAccomodationTypeFromGonogoType(obj.getResidenceAddressType()));

                    }
                });
            }
            if (CollectionUtils.isNotEmpty(applicant.getPhone())) {
                newLoanBasicInfo.setResiMobile(applicant.getPhone().get(0).getPhoneNumber());
                newLoanBasicInfo.setPermMobile(applicant.getPhone().get(0).getPhoneNumber());
                newLoanBasicInfo.setOffMobile(applicant.getPhone().get(0).getPhoneNumber());
                newLoanBasicInfo.setOffPhone(StringUtils.isNotEmpty(applicant.getPersonalLandline()) ? applicant.getPersonalLandline():"05942-233567");
            }

            newLoanBasicInfo.setFormNo(applicationRequest.getRefID());

            newLoanBasicInfo.setTypeOfLoan(applicationRequest.getRequest().getApplication().getLoanPurpose());
            newLoanBasicInfo.setUserId("1000000023");

            newLoanBasicInfo.setResGSTINNo(StringUtils.isNotEmpty(applicant.getGstNo()) && applicant.isGstStatus() ? applicant.getGstNo(): Constant.BLANK);
            newLoanBasicInfo.setOffGSTINNo(StringUtils.isNotEmpty(applicant.getGstNo()) && applicant.isGstStatus() ? applicant.getGstNo(): Constant.BLANK);
            newLoanBasicInfo.setPermGSTINNo(StringUtils.isNotEmpty(applicant.getGstNo()) && applicant.isGstStatus() ? applicant.getGstNo(): Constant.BLANK);

            newLoanBasicInfo.setResGstApplicable(applicant.isGstStatus() ? GNGWorkflowConstant.YES.toFaceValue() : GNGWorkflowConstant.NO.toFaceValue());
            newLoanBasicInfo.setOffGstApplicable(applicant.isGstStatus() ? GNGWorkflowConstant.YES.toFaceValue() : GNGWorkflowConstant.NO.toFaceValue());
            newLoanBasicInfo.setPermGstApplicable(applicant.isGstStatus() ? GNGWorkflowConstant.YES.toFaceValue() : GNGWorkflowConstant.NO.toFaceValue());

            newLoanBasicInfo.setTypeOfBusiness("AGRICULTURE AND ALLIED ACTIVITIES:AGRICULTURAL INFRASTRUCTURE");
            newLoanBasicInfo.setSegment("AGRICULTURE AND ALLIED ACTIVITIES");
            newLoanBasicInfo.setClusterVal("AGRICULTURAL INFRASTRUCTURE:MILLING PRODUCTS");

            if (StringUtils.equalsIgnoreCase(applicationRequest.getHeader().getProduct().toProductName(), Product.BL.toProductName())) {
                newLoanBasicInfo.setScheme(newLoanBasicInfo.getProduct().concat(":").concat(MifinConstants.AMBIT_UDYAM));
            } else {
                newLoanBasicInfo.setScheme(newLoanBasicInfo.getProduct().concat(":").concat(MifinConstants.AMBIT_VYAPAR));
            }

            if(null != applicationRequest.getRequest().getApplication()){
                newLoanBasicInfo.setTenor(applicationRequest.getRequest().getApplication().getTenorRequested());
                newLoanBasicInfo.setLoanAmount(String.valueOf(applicationRequest.getRequest().getApplication().getLoanAmount()));
            }

            if(null!=applicant.getAmbitMifinData() && StringUtils.isNotEmpty(applicant.getAmbitMifinData().getMiFinApplicantCode())) {
                newLoanBasicInfo.setCustCategory(ApplicantType.EXISTING.value());
                newLoanBasicInfo.setIs_existing(GNGWorkflowConstant.YES.toFaceValue());
                newLoanBasicInfo.setApplicantCode(applicant.getAmbitMifinData().getMiFinApplicantCode());
                applicationRequest.setApplicantType(ApplicantType.EXISTING.value());
            }
            else {
                newLoanBasicInfo.setCustCategory(MifinConstants.NEW);
                newLoanBasicInfo.setIs_existing(GNGWorkflowConstant.NO.toFaceValue());
            }

            if (CollectionUtils.isNotEmpty(applicant.getEmployment()) && StringUtils.isNotEmpty(applicant.getEmployment().get(0).getDateOfJoining())
                    && !(StringUtils.containsIgnoreCase(MifinConstants.INVALID_DATE,applicant.getEmployment().get(0).getDateOfJoining()))){
                newLoanBasicInfo.setDateOfIncorporation(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                (GngDateUtil.ddMMyyyy,
                                        applicant.getEmployment().get(0).getDateOfJoining()),
                        GngDateUtil.ddMMMyyyy_WITH_HYPHEN));
            } else {
                newLoanBasicInfo.setDateOfIncorporation("13-MAR-1998");
            }

            if(ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicant.getApplicantType())
                    && CollectionUtils.isNotEmpty(applicant.getEmployment())){
                newLoanBasicInfo.setCustEntityType(ApplicantType.INDIVIDUAL.name());
                newLoanBasicInfo.setConstitution(newLoanBasicInfo.getCustEntityType() +
                        ConstitutionEnum.getMifinConstitutionFromGonogoType(applicant.getEmployment().get(0).getEmploymentType()));
                newLoanBasicInfo.setSaluation(getSalutation(applicant.getGender(),applicant.getMaritalStatus()).toUpperCase());
            }
            else {
                newLoanBasicInfo.setCustEntityType(ApplicantType.NON_INDIVIDUAL.value());
                newLoanBasicInfo.setConstitution(newLoanBasicInfo.getCustEntityType() +
                        ConstitutionEnum.getMifinConstitutionFromGonogoType(applicant.getApplicantType()));

            }

            newLoanBasicInfo.setNoOfOtherDependents(String.valueOf(applicant.getNoOfDependents()));
            newLoanBasicInfo.setAlias(Constant.BLANK);
            newLoanBasicInfo.setKycId(Constant.BLANK);
            newLoanBasicInfo.setCustomerCreditInfo("1000000001");
            newLoanBasicInfo.setResiMobile2("9205603722");
            newLoanBasicInfo.setRegistrationNo("WE3232");
            newLoanBasicInfo.setKeyContactPerson(Constant.BLANK);
            newLoanBasicInfo.setAuthorizedCapital(Constant.BLANK);

            newLoanBasicInfo.setTinNo(StringUtils.isNotEmpty(applicant.getTinNo()) ? applicant.getTinNo(): Constant.BLANK);
            newLoanBasicInfo.setGroupName(Constant.BLANK);
            newLoanBasicInfo.setPermPhone(StringUtils.isNotEmpty(applicant.getPersonalLandline()) ? applicant.getPersonalLandline():"05942-233567");

            if(!(ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicant.getApplicantType()))) {
                newLoanBasicInfo.setCompanyName(applicant.getApplicantName().getFirstName());
                newLoanBasicInfo.setOffCompanyName(applicant.getApplicantName().getFirstName());
                newLoanBasicInfo.setKeyContactPerson(applicant.getContactPerson());
                newLoanBasicInfo.setFatherFName("");
                newLoanBasicInfo.setFatherLName("");
            }

            newLoanBasicInfo.setNameAsPerAadhaar(applicant.getApplicantName().getFullName());
            newLoanBasicInfo.setClusterDescription(Constant.BLANK);
            if(CollectionUtils.isNotEmpty(applicant.getEmail())
                    && StringUtils.isNotEmpty(applicant.getEmail().get(0).getEmailAddress())) {
                newLoanBasicInfo.setIndividualPermemail(applicant.getEmail().get(0).getEmailAddress());
                newLoanBasicInfo.setResiEmail(applicant.getEmail().get(0).getEmailAddress());
                newLoanBasicInfo.setOffMail(applicant.getEmail().get(0).getEmailAddress());
            }
            else {
                newLoanBasicInfo.setIndividualPermemail("test@gmail.com");
                newLoanBasicInfo.setResiEmail("test@gmail.com");
                newLoanBasicInfo.setOffMail("test@gmail.com");
            }
            newLoanBasicInfo.setMaritalStatus(applicant.getMaritalStatus().toUpperCase());
            newLoanBasicInfo.setOffFax("423423");
            newLoanBasicInfo.setOffExt("3333");
            newLoanBasicInfo.setPromotionScheme(Constant.BLANK);

            newLoanBasicInfo.setEducationalQualification(StringUtils.isNotEmpty(applicant.getEducation()) ?
                    EducationTypeEnum.getMifinEducationTypeFromGonogoType(applicant.getEducation()): "Graduate");

            if (StringUtils.equalsIgnoreCase(applicationRequest.getHeader().getProduct().toProductName(), Product.BL.toProductName())) {
                newLoanBasicInfo.setPurposesubscheme(MifinConstants.AMBIT_UDYAM+":"+applicationRequest.getRequest().getApplication().getLoanPurpose());
            }
            else{
                newLoanBasicInfo.setPurposesubscheme(MifinConstants.AMBIT_VYAPAR+":"+applicationRequest.getRequest().getApplication().getLoanPurpose());
            }
        }
        newLoanBasicInfo.setPromoschemecustomertype(Constant.BLANK);

        Application application = applicationRequest.getRequest().getApplication();
        Branch branch =applicationRequest.getAppMetaData().getBranchV2();
        if(null != branch && StringUtils.isNotEmpty(branch.getMifinBranchId())) {
            newLoanBasicInfo.setBranch(branch.getMifinBranchId());
            newLoanBasicInfo.setRelationManager(StringUtils.isNotEmpty(branch.getMifinBranchManagerId()) ? branch.getMifinBranchManagerId() : Constant.BLANK);
            if(null != application && StringUtils.isNotEmpty(application.getChannel())
                    && StringUtils.equalsIgnoreCase(application.getChannel(), "DSA")) {
                newLoanBasicInfo.setSourcingChannelType(MifinConstants.DSA);
                newLoanBasicInfo.setSecsourcingchanneltype(MifinConstants.DSA);
                if(StringUtils.isNotEmpty(application.getMifinAgencyId())) {
                    newLoanBasicInfo.setSourcingChannel(application.getMifinAgencyId());
                    newLoanBasicInfo.setSecsourcingchannel(application.getMifinAgencyId());
                    if (StringUtils.isNotEmpty(application.getMifinAgentId())) {
                        newLoanBasicInfo.setSourcingAgent(application.getMifinAgentId());
                        newLoanBasicInfo.setSecsourcingagent(application.getMifinAgentId());
                    }
                }
            }
        }
        return newLoanBasicInfo;
    }

    private SaveApplicantBasicInfo buildSaveApplicantBasicInfo(ApplicationRequest applicationRequest, String appId) throws Exception {
        SaveApplicantBasicInfo.SaveApplicantBasicInfoBuilder saveApplicantBasicInfo = SaveApplicantBasicInfo.builder().
                prospectCode(applicationRequest.getMiFinProspectCode());

        Applicant applicant = null;
        if(Integer.parseInt(appId) > 0 ){
            applicant= applicationRequest.getRequest().getCoApplicant().stream().filter(coApplicant -> StringUtils.equalsIgnoreCase(
                    coApplicant.getApplicantId(),appId)).findFirst().orElse(null);
            CoApplicant coApplicantObj=applicationRequest.getRequest().getCoApplicant().stream().filter(coApplicant -> StringUtils.equalsIgnoreCase(
                    coApplicant.getApplicantId(),appId)).findFirst().orElse(null);
            saveApplicantBasicInfo.applicantType(MifinConstants.CO_APPLICANT);
            if (StringUtils.equalsIgnoreCase( ApplicantType.INDIVIDUAL.value(),applicant.getApplicantType())){
                saveApplicantBasicInfo.relation(coApplicantObj.getRelationWithApplicant());
            }
        }
        else if (appId.equals("0")){
            applicant = applicationRequest.getRequest().getApplicant();
            saveApplicantBasicInfo.applicantType(MifinConstants.APPLICANT);
        }

        if(null!=applicant.getAmbitMifinData()){
            if(applicant.getAmbitMifinData().isExistingMifinApplicant()){
                saveApplicantBasicInfo.custCategory(ApplicantType.EXISTING.value()).isExisting("Y");
            }
            else {
                saveApplicantBasicInfo.custCategory(MifinConstants.NEW).isExisting("N");
            }
        }


        saveApplicantBasicInfo.
                applicantCategory("1000000001");

        if(null != applicant) {

            if (null != applicant.getAmbitMifinData()) {

                saveApplicantBasicInfo.applicantCode(null == applicant.getAmbitMifinData().getMiFinApplicantCode() ? Constant.BLANK : applicant.getAmbitMifinData().getMiFinApplicantCode());
            }
            if (ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicant.getApplicantType())) {
                saveApplicantBasicInfo.custEntityType(ApplicantType.INDIVIDUAL.name());

                saveApplicantBasicInfo.constitution(saveApplicantBasicInfo.build().getCustEntityType() +
                        ConstitutionEnum.getMifinConstitutionFromGonogoType(applicant.getEmployment().get(0).getEmploymentType()));

            } else {
                saveApplicantBasicInfo.custEntityType(ApplicantType.NON_INDIVIDUAL.value());

                saveApplicantBasicInfo.constitution(saveApplicantBasicInfo.build().getCustEntityType() +
                        ConstitutionEnum.getMifinConstitutionFromGonogoType(applicant.getApplicantType()));
            }


            if(ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicant.getApplicantType())) {
                if (null != applicant.getApplicantName()) {
                    saveApplicantBasicInfo.fname(GngUtils.setValueForNull(applicant.getApplicantName().getFirstName()))
                            .mname(GngUtils.setValueForNull(applicant.getApplicantName().getMiddleName()))
                            .lname(GngUtils.setValueForNull(applicant.getApplicantName().getLastName()))
                            .salutation(getSalutation(applicant.getGender(),applicant.getMaritalStatus()).toUpperCase());

                }

                saveApplicantBasicInfo.noOfDependents(String.valueOf(applicant.getNoOfDependents()));
                if (null != applicant.getFatherName()) {
                    saveApplicantBasicInfo.fatherFname(GngUtils.setValueForNull(applicant.getFatherName().getFirstName()))
                            .fatherMname(GngUtils.setValueForNull(applicant.getFatherName().getMiddleName()))
                            .fatherLname(GngUtils.setValueForNull(applicant.getFatherName().getLastName()));
                } else {
                    saveApplicantBasicInfo.fatherFname("TESTFATHERFNAME").fatherLname("TESTFATHERLNAME");
                }
                if (null != applicant.getSpouseName()) {
                    saveApplicantBasicInfo.spouseFname(GngUtils.setValueForNull(applicant.getSpouseName().getFirstName()))
                            .spouseMname(GngUtils.setValueForNull(applicant.getSpouseName().getMiddleName()))
                            .spouseLname(GngUtils.setValueForNull(applicant.getSpouseName().getLastName()));
                }
                saveApplicantBasicInfo.companyName("TESTCOMPANY")
                        .keyContactPerson(Constant.BLANK);

                List<CustomerAddress> addressList = applicant.getAddress();
                if(CollectionUtils.isNotEmpty(addressList)) {
                    Optional<CustomerAddress> address1 = addressList.stream().filter(address -> StringUtils.isNotEmpty(address.getAddrCompanyName())).findFirst();
                    address1.ifPresent(value -> saveApplicantBasicInfo.companyName(value.getAddrCompanyName()));
                }
            }
            else{
                saveApplicantBasicInfo.companyName(applicant.getApplicantName().getFirstName())
                        .keyContactPerson(applicant.getContactPerson()).fatherFname("").fatherLname("");
            }

            if(StringUtils.isNotEmpty(applicant.getImageform60())){
                saveApplicantBasicInfo.form60Name(StringUtils.isNotEmpty(applicant.getForm60Name()) ? applicant.getForm60Name() : "Form60Cell");
            }
            else
                saveApplicantBasicInfo.form60Name(Constant.BLANK);

            saveApplicantBasicInfo.maritalStatus(null != applicant.getMaritalStatus() ?
                    applicant.getMaritalStatus().toUpperCase() : applicant.getMaritalStatus());

            if (StringUtils.isNotEmpty(applicant.getDateOfBirth())) {
                if (!(StringUtils.equalsIgnoreCase(MifinConstants.INVALID_DATE, applicant.getDateOfBirth()))) {
                    saveApplicantBasicInfo.dob(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                    (GngDateUtil.ddMMyyyy,
                                            applicant.getDateOfBirth()),
                            GngDateUtil.ddMMMyyyy_WITH_HYPHEN));
                } else {
                    saveApplicantBasicInfo.dob("13-MAR-1998");
                }
            } else {
                saveApplicantBasicInfo.dob("13-MAR-1998");
            }

            saveApplicantBasicInfo.gender(null != applicant.getGender()?
                    (StringUtils.equalsIgnoreCase(applicant.getGender(),GNGWorkflowConstant.TRANSGENDER.toFaceValue()) ?
                            GNGWorkflowConstant.OTHER.toFaceValue() : applicant.getGender().toUpperCase())
                    :Constant.BLANK);

            saveApplicantBasicInfo.isPanAvailable("N");
            saveApplicantBasicInfo
                    .religion(Constant.BLANK)
                    .psl(null != applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails() &&
                            applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().isPsl()? GNGWorkflowConstant.YES.toFaceValue() : "")
                    .socialCategory(StringUtils.isNotEmpty(applicant.getSocialCategory())? applicant.getSocialCategory() : "1000000001" )
                    .dnsRequired(Constant.BLANK)
                    .dnsReason(Constant.BLANK)
                    .prefTimeToCall(Constant.BLANK)
                    .prefCommunicationMode(Constant.BLANK)
                    .alias(Constant.BLANK)
                    .aadharAsKyc("N")
                    .nameAsPerAadhaar(applicant.getApplicantName().getFullName())
                    .noOfDependentChildren(String.valueOf(applicant.getiNoOfChildren()))
                    .cin(Constant.BLANK)
                    .educationalQualification(StringUtils.isNotEmpty(applicant.getEducation()) ?
                            EducationTypeEnum.getMifinEducationTypeFromGonogoType(applicant.getEducation()): "Graduate");

            List<Kyc> kycDetailsArray = applicant.getKyc();
            if (CollectionUtils.isNotEmpty(kycDetailsArray)) {
                for (Kyc kyc : kycDetailsArray) {
                    if (StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.PAN.toFaceValue())) {
                        saveApplicantBasicInfo.panNo(kyc.getKycNumber()).isPanAvailable("Y");

                    }else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.DRIVING_LICENSE.toString())) {
                        saveApplicantBasicInfo.drivingLicenceNo(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.PASSPORT.toString())) {
                        saveApplicantBasicInfo.passportNo(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), KYC_TYPES.VOTER_ID.toString())) {
                        saveApplicantBasicInfo.voterId(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), MifinConstants.LPG_BILL)) {
                        saveApplicantBasicInfo.otherName(MifinConstants.LPG_BILL)
                                .othersIdNumber(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), MifinConstants.PNG_BILL)) {
                        saveApplicantBasicInfo.otherName(MifinConstants.PNG_BILL)
                                .othersIdNumber(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), MifinConstants.ELECTRICITY_BILL)) {
                        saveApplicantBasicInfo.otherName(MifinConstants.ELECTRICITY_BILL)
                                .othersIdNumber(kyc.getKycNumber());
                    } else if (StringUtils.equalsIgnoreCase(kyc.getKycName(), GNGWorkflowConstant.AADHAAR.toFaceValue())) {
                        saveApplicantBasicInfo.aadharAsKyc(GNGWorkflowConstant.YES.toFaceValue());
                    }

                }
            }


            saveApplicantBasicInfo.tanNo(StringUtils.isNotEmpty(applicant.getTanNo()) ? applicant.getTanNo() : Constant.BLANK)
                    .tinNo(StringUtils.isNotEmpty(applicant.getTinNo()) ? applicant.getTinNo() : Constant.BLANK)
                    .segment("AGRICULTURE AND ALLIED ACTIVITIES")
                    .typeOfBusiness("AGRICULTURE AND ALLIED ACTIVITIES:AGRICULTURAL INFRASTRUCTURE")
                    .cluster("AGRICULTURAL INFRASTRUCTURE:MILLING PRODUCTS")
                    .clusterDescription(Constant.BLANK)
                    .ckycId(Constant.BLANK)
                    .customerCreditInfo(Constant.BLANK);

            if (CollectionUtils.isNotEmpty(applicant.getEmployment())
                    && StringUtils.isNotEmpty(applicant.getEmployment().get(0).getDateOfJoining())) {

                if (!(StringUtils.equalsIgnoreCase(applicant.getEmployment().get(0).getDateOfJoining(), MifinConstants.INVALID_DATE))) {
                    saveApplicantBasicInfo.dateOfIncorporation(GngDateUtil.changeDateFormat(GngDateUtil.getDateFromSpecificFormat
                                    (GngDateUtil.ddMMyyyy,
                                            applicant.getEmployment().get(0).getDateOfJoining()),
                            GngDateUtil.ddMMMyyyy_WITH_HYPHEN));
                } else {
                    saveApplicantBasicInfo.dateOfIncorporation("13-MAR-1998");
                }

            } else {
                saveApplicantBasicInfo.dateOfIncorporation("13-MAR-1998");
            }

            saveApplicantBasicInfo.regNumber(Constant.BLANK)
                    .authorizedCapital(Constant.BLANK)
                    .groupName(Constant.BLANK);

            saveApplicantBasicInfo.authorisedSignList(new ArrayList<AuthorisedSign>() {
                {
                    add(AuthorisedSign.builder()
                            .firstNameAuthorisedSign(Constant.BLANK)
                            .middleNameAuthorisedSign(Constant.BLANK)
                            .lastNameAuthorisedSign(Constant.BLANK)
                            .delegationAuthorisedSign(Constant.BLANK)
                            .dinnoAuthorisedSign(Constant.BLANK)
                            .emailAuthorisedSign(Constant.BLANK)
                            .contactnoAuthorisedSign(Constant.BLANK)
                            .designationAuthorisedSign(Constant.BLANK).build()
                    );
                }
            });



            PersonalDiscussion personalDiscussion = applicationRequest.getRequest().getPersonalDiscussion();
            if(null!= applicationRequest.getRequest().getPersonalDiscussion() && StringUtils.isNotEmpty(personalDiscussion.getSector()) &&
                    StringUtils.isNotEmpty(personalDiscussion.getBusinessType()) &&  StringUtils.isNotEmpty(personalDiscussion.getIndustry())) {

                saveApplicantBasicInfo.segment(personalDiscussion.getSector())
                        .typeOfBusiness(personalDiscussion.getSector()+":"+personalDiscussion.getBusinessType())
                        .cluster(personalDiscussion.getBusinessType()+":"+personalDiscussion.getIndustry());
            }
        }

        return saveApplicantBasicInfo.build();
    }

    private String getform60(ApplicationRequest applicationRequest,String appId){
        Applicant applicant = null;
        String image= Constant.BLANK;
        if(Integer.parseInt(appId) > 0 ){
            applicant= applicationRequest.getRequest().getCoApplicant().stream().filter(coApplicant -> StringUtils.equalsIgnoreCase(
                    coApplicant.getApplicantId(),appId)).findFirst().orElse(null);
            image=StringUtils.isNotEmpty(applicant.getImageform60())?applicant.getImageform60():Constant.BLANK;

        }
        else if (appId.equals("0")){
            applicant = applicationRequest.getRequest().getApplicant();
            image=StringUtils.isNotEmpty(applicant.getImageform60())?applicant.getImageform60():Constant.BLANK;
        }
        return image;
    }

    public SaveApplicantAddressRequest buildSaveApplicantAddressRequest(ApplicationRequest applicationRequest, String appId, String addressType, String userId) throws Exception {
        log.debug("Inside buildSaveApplicantAddressRequest()");
        return SaveApplicantAddressRequest.builder()
                .authentication(buildAuthDetails(userId))
                .saveAddressBasicInfo(buildSaveAddressBasicInfo(applicationRequest, appId, addressType))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_SAVE_APPLICANT_ADDRESS)
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .p_form(Constant.BLANK)
                .build();
    }

    private SaveAddressBasicInfo buildSaveAddressBasicInfo(ApplicationRequest applicationRequest, String appId, String addressType) {
        Applicant applicant = null;
        SaveAddressBasicInfo.SaveAddressBasicInfoBuilder saveAddressBasicInfo = SaveAddressBasicInfo.builder()
                .prospectCode(applicationRequest.getMiFinProspectCode());

        if (applicationRequest.getRequest() != null && applicationRequest.getRequest().getApplicant() != null ) {

            if(Integer.parseInt(appId) > 0 && CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())) {
                applicant = applicationRequest.getRequest().getCoApplicant().stream().filter(coApplicant -> StringUtils.equalsIgnoreCase(
                        coApplicant.getApplicantId(), appId)).findFirst().orElse(null);
            } else if (StringUtils.equals(appId, "0")) {
                applicant = applicationRequest.getRequest().getApplicant();
            }

            if (null != applicant) {
                saveAddressBasicInfo.applicantCode(null != applicant.getAmbitMifinData() && null != applicant.getAmbitMifinData().getMiFinApplicantCode() ?
                        applicant.getAmbitMifinData().getMiFinApplicantCode() : Constant.BLANK);
                if(CollectionUtils.isNotEmpty(applicant.getAddress())) {
                    for(CustomerAddress address1 : applicant.getAddress()) {
                        if(StringUtils.equals(addressType, address1.getAddressType())) {
                            saveAddressBasicInfo
                                    .addressType(AddressTypeEnum.getMifinAddressTypeFromGonogoAddressType(address1.getAddressType()))
                                    .applicantAddressId(StringUtils.isNotEmpty(address1.getMifinAddressId()) ? address1.getMifinAddressId() : Constant.BLANK)
                                    .address1(null != address1.getAddressLine1() ? address1.getAddressLine1() : Constant.BLANK)
                                    .address2(null != address1.getAddressLine2() ? address1.getAddressLine2() : Constant.BLANK)
                                    .address3(null != address1.getLine3() ? address1.getLine3() : Constant.BLANK)
                                    .pincode(String.valueOf(address1.getPin()))
                                    .landmark(null != address1.getLandMark() ? address1.getLandMark() : Constant.BLANK)
                                    .district(null != address1.getCity() ? address1.getCity() : Constant.BLANK)
                                    .yearOfStayCurrentArea(String.valueOf(address1.getTimeAtAddress()))
                                    .occupanyStatus(null != address1.getResidenceAddressType() ?
                                            AccomodationTypeEnum.getMifinAccomodationTypeFromGonogoType(address1.getResidenceAddressType()) : AccomodationTypeEnum.OWNED.getMifinAccomodationType())
                                    .destinationAddress(address1.getDestinationAddress() ? GNGWorkflowConstant.YES.toFaceValue() : GNGWorkflowConstant.NO.toFaceValue())
                                    .email(null != applicant.getEmail() && null != applicant.getEmail().get(0) ?
                                            applicant.getEmail().get(0).getEmailAddress() : Constant.BLANK)
                                    .mailingAddress(address1.getMailingAddress() ? GNGWorkflowConstant.YES.toFaceValue() : GNGWorkflowConstant.NO.toFaceValue())
                                    .companyName(StringUtils.isNotEmpty(address1.getAddrCompanyName()) ? address1.getAddrCompanyName() : Constant.BLANK)
                                    .monthsOfStayArea("0")
                                    .gstNo(StringUtils.isNotEmpty(applicant.getGstNo()) && applicant.isGstStatus() ? applicant.getGstNo() : Constant.BLANK)
                                    .gstApplicable(applicant.isGstStatus() ? GNGWorkflowConstant.YES.toFaceValue() : GNGWorkflowConstant.NO.toFaceValue())
                                    .fax(Constant.BLANK)
                                    .createdBy(Constant.BLANK)
                                    .updatedBy("1000000024");
                            PinCodeMaster pinCodeMaster = applicationRepository.fetchPinCodeDetails(applicationRequest.getHeader().getInstitutionId(), Long.toString(address1.getPin()));
                            if(null != pinCodeMaster) {
                                saveAddressBasicInfo.city(StringUtils.isNotEmpty(pinCodeMaster.getCityCode()) ? pinCodeMaster.getCityCode() : Constant.BLANK)
                                        .state(StringUtils.isNotEmpty(pinCodeMaster.getStateCode()) ? pinCodeMaster.getStateCode() : Constant.BLANK);
                            }
                        }
                    }
                }
                if(CollectionUtils.isNotEmpty(applicant.getPhone())) {
                    saveAddressBasicInfo
                            .mobile1(applicant.getPhone().get(0).getPhoneNumber())
                            .mobile2(Constant.BLANK)
                            .phone1(null != applicant.getPersonalLandline() ? applicant.getPersonalLandline() : Constant.BLANK)
                            .phone2(Constant.BLANK);
                }
            }
        }
        return saveAddressBasicInfo.build();
    }

    public SaveFinancialInfoRequest buildSaveFinancialInfoRequest(ApplicationRequest applicationRequest, String userId) {
        log.debug("Save Financial Info Started");

        return SaveFinancialInfoRequest.builder()
                .authenticationDetails(buildAuthDetails(userId))
                .saveFinancialInfoBankingDetailsList(getSaveFinancialInfoBankingDetails(applicationRequest))
                .saveFinancialInfoBasicInfoDetails(getSaveFinancialInfoBasicInfoDetails(applicationRequest))
                .financialList(getFinancial(applicationRequest))
                .itrList(getItr())
                .otherLoansDetList(getOtherLoansDet(applicationRequest))
                .viability(getViability())
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_SAVE_FINANCIAL_INFO)
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .build();

    }

    @Override
    public SaveVerificationRequest buildSaveVerificationBasicInfoRequest(ApplicationRequest applicationRequest, String userId) {
        log.debug("Inside buildSaveVerificationBasicInfoRequest()");
        return SaveVerificationRequest.builder()
                .authentication(buildAuthDetails(userId))
                .verificationQuesAnsList(getVerificationQuesAns())
                .saveVerificationBasicInfoList(buildSaveVerificationBasicInfo(applicationRequest))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .type(ThirdPartyFieldConstant.MIFIN_SAVE_VERIFICATION)
                .build();
    }

    @Override
    public List<SavePersonalInsuranceRequest> buildSavePersonalInsuranceRequest(ApplicationRequest applicationRequest, String userId) {
        List<SavePersonalInsuranceRequest> savePersonalInsuranceRequestList = null;
        log.debug("Inside buildSavePersonalInsuranceRequest()");
        if(null != buildSavePersonalInsuranceBasicInfo(applicationRequest) ){
            for(SavePersonalInsuranceBasicInfo savePersonalInsuranceBasicInfo : buildSavePersonalInsuranceBasicInfo(applicationRequest)){
                SavePersonalInsuranceRequest savePersonalInsuranceRequest = SavePersonalInsuranceRequest.builder()
                        .authenticationDetails(buildAuthDetails(userId))
                        .savePersonalInsuranceBasicInfo(savePersonalInsuranceBasicInfo)
                        .institutionId(applicationRequest.getHeader().getInstitutionId())
                        .referenceId(applicationRequest.getRefID())
                        .type(ThirdPartyFieldConstant.MIFIN_SAVE_PERSONAL_INSURANCE)
                        .product(applicationRequest.getHeader().getProduct().toProductName())
                        .build();

                if(null == savePersonalInsuranceRequestList){
                    savePersonalInsuranceRequestList = new ArrayList<>();
                }
                savePersonalInsuranceRequestList.add(savePersonalInsuranceRequest);
            }
        }
        return savePersonalInsuranceRequestList;
    }

    private List<SavePersonalInsuranceBasicInfo> buildSavePersonalInsuranceBasicInfo(ApplicationRequest applicationRequest) {
        List<SavePersonalInsuranceBasicInfo> savePersonalInsuranceBasicInfoList = null;
        Optional<InsuranceData> insuranceData = Optional.ofNullable(applicationRepository.fetchInsuranceData(applicationRequest.getRefID()));
        if(insuranceData.isPresent()){
            Optional<List<Applicant>> applicantList = Optional.ofNullable(insuranceData.get().getApplicantList());
            if(applicantList.isPresent()){
                for(Applicant applicant : applicantList.get()){
                    Optional<List<InsurancePolicy>> insurancePolicyList = Optional.ofNullable(applicant.getInsurancePolicyList());
                    if(insurancePolicyList.isPresent()){
                        for(InsurancePolicy insurancePolicy: insurancePolicyList.get()){
                            SavePersonalInsuranceBasicInfo savePersonalInsuranceBasicInfo = SavePersonalInsuranceBasicInfo.builder()
                                    .prospectCode(applicationRequest.getMiFinProspectCode())
                                    .applicantCode(null != applicant.getAmbitMifinData() ? applicant.getAmbitMifinData().getMiFinApplicantCode():Constant.BLANK)
                                    .personalInsuranceId(null != insurancePolicy.getMifinInsuranceId() ? insurancePolicy.getMifinInsuranceId():Constant.BLANK)
                                    .insuranceType(null != insurancePolicy.getProduct() ? insurancePolicy.getProduct().toUpperCase() :Constant.BLANK)
                                    .companyName(null != insurancePolicy.getProduct() && null !=insurancePolicy.getCompany() ?
                                            insurancePolicy.getProduct().toUpperCase()+":"+ insurancePolicy.getCompany().toUpperCase(): Constant.BLANK)
                                    .policyName(null != insurancePolicy.getProduct() && null !=insurancePolicy.getCompany() && null != insurancePolicy.getScheme() ?
                                            insurancePolicy.getProduct().toUpperCase()+":"+ insurancePolicy.getCompany().toUpperCase()+":"
                                                    +insurancePolicy.getScheme().toUpperCase() : Constant.BLANK)
                                    .policyNumber(insurancePolicy.getPolicyNumber())
                                    .insuredAmount(insurancePolicy.getInsuredAmt().toString())
                                    .paymentOption("COMPANY FINANCED")
                                    .premiumAmount(insurancePolicy.getPremium().toString())
                                    .stOnPremiumAmount("0")
                                    .policyStartDate(GngDateUtil.changeDateFormat(insurancePolicy.getGeneratedDate(), GngDateUtil.ddMMMyyyy_WITH_HYPHEN))
                                    .nomineeName(null != insurancePolicy.getNomineeInfo()?  insurancePolicy.getNomineeInfo().getName().getFullName() : Constant.BLANK)
                                    .nomineeRelationship(null != insurancePolicy.getNomineeInfo() ? insurancePolicy.getNomineeInfo().getRelationship().toUpperCase(): Constant.BLANK)
                                    .build();

                            if(null == savePersonalInsuranceBasicInfoList ){
                                savePersonalInsuranceBasicInfoList = new ArrayList<>();
                            }
                            savePersonalInsuranceBasicInfoList.add(savePersonalInsuranceBasicInfo);
                        }
                    }
                }
            }
        }
        return savePersonalInsuranceBasicInfoList;
    }

    @Override
    public UpdateLoanDetailsRequest buildLoanDeatilsBasicInfoRequest(ApplicationRequest applicationRequest, String userId) throws Exception{
        log.debug("Inside buildLoanDeatilsBasicInfoRequest()");
        return UpdateLoanDetailsRequest.builder()
                .authentication(buildAuthDetails(userId))
                .loanDetailsBasicInfo(buildLoanDetailBasicInfo(applicationRequest))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_LOAN_DETAILS)
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .build();
    }

    @Override
    public CrDecisionRequest buildCrdecisionRequest(ApplicationRequest applicationRequest, String userId) {
        log.debug("Inside buildCrdecisionRequest()");
        return CrDecisionRequest.builder()
                .authentication(buildAuthDetails(userId))
                .crBasicInfo(buildBasicInfoCr(applicationRequest))
                .sanctionTermsrequest(buildSanctionTermRequest())
                .creditDecisionRequest(buildCreditDecisionRequest(applicationRequest))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .type(ThirdPartyFieldConstant.MIFIN_SAVE_CR_DECISION)
                .product(applicationRequest.getHeader().getProduct().toString())
                .build();
    }

    @Override
    public SaveDisbursalMakerRequest buildSaveDisbursalMaker(ApplicationRequest applicationRequest, String userId) throws Exception {
        log.debug("Inside buildSaveDisbursalMaker()");
        return SaveDisbursalMakerRequest.builder()
                .authentication(buildAuthDetails(userId))
                .disbursalMakerBasicInfo(buildDisbursalMakerBasicInfo(applicationRequest))
                .disbursalScheduleList(buildDisbursalSchedule(applicationRequest))
                .interestRateScheduleList(buildInterestRateSchedule(applicationRequest))
                .chargesList(buildCharges(applicationRequest))
                .trancheDisbursalSchedule(buildTrancheDisbursalSchedule(applicationRequest))
                .disbursalDetail(buildDisbursalDetail(applicationRequest))
                .disbursalDecisionInfoList(buildDisbursalDecisionInfo(applicationRequest))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .type(ThirdPartyFieldConstant.MIFIN_DISBURSAL_MAKER)
                .build();
    }

    @Override
    public DeleteApplicantRequest buildDeleteApplicantRequest(ApplicationRequest applicationRequest, List<String> coApplicantIds, String userId) throws Exception {
        DeleteApplicantRequest deleteApplicantRequest = null;
        log.debug("Inside buildDeleteApplicantRequest()");
        if(null != applicationRequest.getRequest() && null != applicationRequest.getRequest().getCoApplicant() ){
            for(String coApplicantId : coApplicantIds) {
                Applicant applicant = applicationRequest.getRequest().getCoApplicant().stream().filter(coApplicant -> StringUtils.equalsIgnoreCase(
                        coApplicant.getApplicantId(), coApplicantId)).findFirst().orElse(null);

                if(null != applicant && null != applicant.getAmbitMifinData() && StringUtils.isNotEmpty(applicant.getAmbitMifinData().getMiFinApplicantCode())) {
                    ApplicantInfoRequest applicantInfoRequest = ApplicantInfoRequest.builder()
                            .applicantCode(applicant.getAmbitMifinData().getMiFinApplicantCode())
                            .applicantType(ThirdPartyFieldConstant.CO_APPLICANT)
                            .flag("D")
                            .build();
                    List<ApplicantInfoRequest> applicantInfoRequestList = new ArrayList<ApplicantInfoRequest>(){{
                        add(applicantInfoRequest);
                    }};
                    deleteApplicantRequest = DeleteApplicantRequest.builder()
                            .authenticationDetails(buildAuthDetails(userId))
                            .deleteApplicantBasicInfo(DeleteApplicantBasicInfo.builder()
                                    .prospectCode(applicationRequest.getMiFinProspectCode())
                                    .build())
                            .applicantInfoRequestList(applicantInfoRequestList)
                            .institutionId(applicationRequest.getHeader().getInstitutionId())
                            .referenceId(applicationRequest.getRefID())
                            .type(ThirdPartyFieldConstant.MIFIN_DELETE_APPLICANT)
                            .product(applicationRequest.getHeader().getProduct().toString())
                            .build();
                }
            }
        }
        return deleteApplicantRequest;
    }

    private CRBasicInfo buildBasicInfoCr(ApplicationRequest applicationRequest){
        return CRBasicInfo.builder()
                .prospectCode(applicationRequest.getMiFinProspectCode())
                .build();
    }

    private List<CreditDecisionRequest> buildCreditDecisionRequest(ApplicationRequest applicationRequest){
        EligibilityDetails eligibility = applicationRepository.fetchEligibilityDetails(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
        CamDetails camDetails = applicationRepository.fetchCamDetails(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId(), EndPointReferrer.CAM_SUMMARY);
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(applicationRequest.getRefID() , applicationRequest.getHeader().getInstitutionId());
        GoNoGoCustomerApplication goNoGoCustomerApplication= applicationRepository.getGoNoGoCustomerApplicationByRefId(applicationRequest.getRefID());
        List<CroJustification> croJustificationList = goNoGoCustomerApplication.getCroJustification();

        CreditDecisionRequest creditDecisionRequest = CreditDecisionRequest.builder()
                .action("APPROVE")
                .rejectType(Constant.BLANK)
                .rejectCode(Constant.BLANK)
                .decisionDate("14-Aug-2020").build();

        if(Optional.ofNullable(eligibility).isPresent()) {
            creditDecisionRequest.setRecommadedAmt(String.valueOf(Math.round(eligibility.getAprvLoan())));
            creditDecisionRequest.setRecommendedTenor(String.valueOf(eligibility.getTenor()));
        }
        if(null != loanCharges) {
            creditDecisionRequest.setRecommecdedRoi(null != loanCharges.getInterestDetails() ?
                    String.valueOf(loanCharges.getInterestDetails().getInterestRate()) : Constant.BLANK);
        }
        if(null != camDetails && null != camDetails.getSummary()) {
            if (StringUtils.isNotEmpty(camDetails.getSummary().getRiskProfile())) {
                creditDecisionRequest.setRiskCategory(camDetails.getSummary().getRiskProfile().toUpperCase());
            }
        }
        creditDecisionRequest.setRemarks(CollectionUtils.isNotEmpty(croJustificationList) ?
                croJustificationList.get(croJustificationList.size()-1).getRemark() : GNGWorkflowConstant.APPROVED.toFaceValue());
        creditDecisionRequest.setCrLevel("1");
        creditDecisionRequest.setCreditOfficer("1000000023");
        return new ArrayList<CreditDecisionRequest>() {
            {
                add(creditDecisionRequest);
            }
        };
    }
    @Override
    public UpdateStatusDedupeResquest buildUpdateProcessDedupeRequest(UpdateDedupeRequest updateDedupeRequest, ApplicantdedupeRequest applicantdedupeRequest, String userId) throws Exception {
        return UpdateStatusDedupeResquest.builder()
                .authenticationDetails(buildAuthDetails(userId))
                .updateStatusDedupeBasicInfo(buildUpdateProcessDedupeBasicInfo(updateDedupeRequest,applicantdedupeRequest))
                .dedupeUpdate(buildDedupeUpdate(applicantdedupeRequest))
                .institutionId(updateDedupeRequest.getInstId())
                .referenceId(updateDedupeRequest.getRefId())
                .type(ThirdPartyFieldConstant.MIFIN_UPDATE_DEDUPE_STATUS)
                .product(updateDedupeRequest.getProduct().toProductName())
                .build();
    }

    private DedupeUpdate buildDedupeUpdate(ApplicantdedupeRequest applicantdedupeRequest) {

        return DedupeUpdate.builder()
                .mathcedApplications(null!=applicantdedupeRequest.getUpdateDedupeBaseRequest()
                        && null != applicantdedupeRequest.getUpdateDedupeBaseRequest().getApplicantMatchedInfoList() ?
                        applicantdedupeRequest.getUpdateDedupeBaseRequest().getApplicantMatchedInfoList().getActualCustid()
                        :Constant.BLANK)
                .build();

    }

    private sanctionTermsrequest buildSanctionTermRequest(){
        return sanctionTermsrequest.builder()
                .sanctionTermsConditions("INTERNAL")
                .sanctionTermsConditionsRemarks("OKAY")
                .build();
    }


    private List<OtherLoansDet> getOtherLoansDet(ApplicationRequest applicationRequest) {
        List<OtherLoansDet> otherLoansDetList = null;
        CamDetails camDetails = applicationRepository.fetchCamDetails(applicationRequest.getRefID() , applicationRequest.getHeader().getInstitutionId() , EndPointReferrer.CAM_RTR_DETAILS);

        if(null != camDetails && CollectionUtils.isNotEmpty(camDetails.getRtrDetailList())){
            for(RTRDetail rtrDetail : camDetails.getRtrDetailList()){
                if (null != rtrDetail.getRtrTransactions() && rtrDetail.getRtrTransactions().size()>0) {
                    for (RTRTransaction rtrTransaction : rtrDetail.getRtrTransactions()) {
                        OtherLoansDet otherLoansDet = OtherLoansDet.builder()
                                .otherLoansDetlId(StringUtils.isNotEmpty(rtrDetail.getMifinOtherLoanDetailsId()) ? rtrDetail.getMifinOtherLoanDetailsId() : Constant.BLANK)
                                .institutionFrom(null != rtrTransaction.getMifinBankCode() ? rtrTransaction.getMifinBankCode() : Constant.BLANK)
                                .accountNumber(Constant.BLANK)
                                .facilityType(rtrTransaction.getLoanType())
                                .emiStartDate(null != rtrTransaction.getSanctionOrEmiStartDate() ?
                                        GngDateUtil.changeDateFormat(rtrTransaction.getSanctionOrEmiStartDate(), GngDateUtil.ddMMMyyyy_WITH_HYPHEN) : Constant.BLANK)
                                .tenorOfLoan(rtrTransaction.getTenure())
                                .originalLoanAmount(String.format("%.0f",rtrTransaction.getLoanAmount()))
                                .totalOutstanding(String.format("%.0f",rtrDetail.getTotalOutstandingAmount()))
                                .otherLoanStatus(Constant.BLANK)
                                .emiAmt(String.valueOf(rtrTransaction.getEmi()))
                                .numOfEmiPaid(Constant.BLANK)
                                .numOfBouncedEmi(String.valueOf(rtrTransaction.getNumberOfBounces()))
                                .remarks("DONE")
                                .considerForOblFlg("Y")
                                .lastEmiPaidDate(Constant.BLANK)
                                .emiBouncedTimesOverDrawn(Constant.BLANK)
                                .mob(Constant.BLANK)
                                .averageDelay(Constant.BLANK)
                                .peakDelay(Constant.BLANK)
                                .rtrStatus(Constant.BLANK)
                                .analysisBasedOn(Constant.BLANK)
                                .btAmt(Constant.BLANK)
                                .regOwnerSoaName(Constant.BLANK)
                                .regnNum(Constant.BLANK).build();

                        if (null == otherLoansDetList) {
                            otherLoansDetList = new ArrayList<>();
                        }
                        otherLoansDetList.add(otherLoansDet);
                    }
                } else {
                    RTRTransaction rtrTransaction = null;
                    OtherLoansDet otherLoansDet = OtherLoansDet.builder()
                            .otherLoansDetlId(StringUtils.isNotEmpty(rtrDetail.getMifinOtherLoanDetailsId()) ? rtrDetail.getMifinOtherLoanDetailsId() : Constant.BLANK)
                            .institutionFrom(Constant.BLANK)
                            .accountNumber(Constant.BLANK)
                            .facilityType(Constant.BLANK)
                            .emiStartDate(Constant.BLANK)
                            .tenorOfLoan(Constant.BLANK)
                            .originalLoanAmount(Constant.BLANK)
                            .totalOutstanding(Double.toString(rtrDetail.getTotalOutstandingAmount()))
                            .otherLoanStatus(Constant.BLANK)
                            .emiAmt(Constant.BLANK)
                            .numOfEmiPaid(Constant.BLANK)
                            .numOfBouncedEmi(Constant.BLANK)
                            .remarks("DONE")
                            .considerForOblFlg("Y")
                            .lastEmiPaidDate(Constant.BLANK)
                            .emiBouncedTimesOverDrawn(Constant.BLANK)
                            .mob(Constant.BLANK)
                            .averageDelay(Constant.BLANK)
                            .peakDelay(Constant.BLANK)
                            .rtrStatus(Constant.BLANK)
                            .analysisBasedOn(Constant.BLANK)
                            .btAmt(Constant.BLANK)
                            .regOwnerSoaName(Constant.BLANK)
                            .regnNum(Constant.BLANK).build();

                    if (null == otherLoansDetList) {
                        otherLoansDetList = new ArrayList<>();
                    }
                    otherLoansDetList.add(otherLoansDet);
                }
            }
        }

        return  otherLoansDetList;
    }

    private List<Itr> getItr() {

        return new ArrayList<Itr>(){{
            add(Itr.builder()
                    .itrId(Constant.BLANK)
                    .memberId(Constant.BLANK)
                    .assessmentYear(Constant.BLANK)
                    .inc(Constant.BLANK)
                    .tax(Constant.BLANK)
                    .deleteFlag("N").build());
        }};
    }

    private List<Financial> getFinancial(ApplicationRequest applicationRequest) {

        List<Financial> financialList = new ArrayList<>();

        CamDetails camDetails = applicationRepository.fetchCamDetails(applicationRequest.getRefID() , applicationRequest.getHeader().getInstitutionId() , EndPointReferrer.CAM_PL_BS_DETAILS);

        if(null != camDetails &&CollectionUtils.isNotEmpty(camDetails.getPlAndBSAnalysis())){
            for(PLAndBSAnalysis plAndBSAnalysis : camDetails.getPlAndBSAnalysis()){
                String currentYear = null;
                if(null != plAndBSAnalysis.getProfitAndLoss() && StringUtils.isNotEmpty(plAndBSAnalysis.getProfitAndLoss().getCurrentYear())){
                    currentYear = plAndBSAnalysis.getProfitAndLoss().getCurrentYear();
                }else if (null != plAndBSAnalysis.getBalanceSheet()){
                    currentYear = plAndBSAnalysis.getBalanceSheet().getCurrentYear();
                }

                Financial financial = Financial.builder()
                        .financialId(StringUtils.isNotEmpty(plAndBSAnalysis.getMifinFinancialTabId()) ? plAndBSAnalysis.getMifinFinancialTabId() :Constant.BLANK)
                        .financialYear((Integer.parseInt(currentYear)-1)+"-"+ currentYear).build();

                if(null != plAndBSAnalysis.getBalanceSheet() && StringUtils.equalsIgnoreCase(plAndBSAnalysis.getBalanceSheet().getCurrentYear() , currentYear)){
                    BalanceSheet balanceSheet = plAndBSAnalysis.getBalanceSheet();
                    if(null != balanceSheet) {
                        financial.setShareCapital(getStatementFieldDetailsAmount(balanceSheet.getShareCapital()));
                        financial.setReservesAndSurplus(getStatementFieldDetailsAmount(balanceSheet.getReservesAndSurplus()));
                        financial.setShortTermBankBorrowing(getStatementFieldDetailsAmount(balanceSheet.getShortTermBorrowing()));
                        financial.setLongTermDebt(getStatementFieldDetailsAmount(balanceSheet.getLongTermDebt()));
                        financial.setUnsecuredLoans(getStatementFieldDetailsAmount(balanceSheet.getUnsecuredDebtPersonal()));
                        financial.setCurrentLiabilities(getStatementFieldDetailsAmount(balanceSheet.getCurrentLiabilities()));
                        financial.setProvision(getStatementFieldDetailsAmount(balanceSheet.getProvisions()));
                        financial.setDefferedTaxLiability(getStatementFieldDetailsAmount(balanceSheet.getDefferedTaxLiabilities()));
                        financial.setContingentLiabilities(getStatementFieldDetailsAmount(balanceSheet.getContingentLiabilities()));
                        financial.setNetFixedAssets(getStatementFieldDetailsAmount(balanceSheet.getNetFixedAssets()));
                        financial.setInvestments(getStatementFieldDetailsAmount(balanceSheet.getInvestments()));
                        financial.setLoansAndAdvances(getStatementFieldDetailsAmount(balanceSheet.getLoansAndAdvancess()));
                        financial.setBookDebtLessthanSixmonths(getStatementFieldDetailsAmount(balanceSheet.getDebtorsLessthanSixMonths()));
                        financial.setBookDebtGreaterOrEqualthanSixmonths(getStatementFieldDetailsAmount(balanceSheet.getDebtorsExceedsSixMonths()));
                        financial.setInventory(getStatementFieldDetailsAmount(balanceSheet.getInventory()));
                        financial.setCashAndBankBalances(getStatementFieldDetailsAmount(balanceSheet.getCashAndBank()));
                        financial.setOtherCurrentAssets(getStatementFieldDetailsAmount(balanceSheet.getOtherCurrentAssets()));
                        financial.setDefferedTaxAssets(getStatementFieldDetailsAmount(balanceSheet.getDefferedTaxAssets()));
                    }

                }
                if(null != plAndBSAnalysis.getProfitAndLoss() && StringUtils.equalsIgnoreCase(plAndBSAnalysis.getProfitAndLoss().getCurrentYear() , currentYear)){
                    ProfitAndLoss profitAndLoss = plAndBSAnalysis.getProfitAndLoss();
                    if(null != profitAndLoss) {
                        financial.setNetSales(getStatementFieldDetailsAmount(profitAndLoss.getSales()));
                        financial.setOtherIncome(getStatementFieldDetailsAmount(profitAndLoss.getOtherIncome()));
                        financial.setTotalExpense(getStatementFieldDetailsAmount(profitAndLoss.getFinanceExpenses()));
                        financial.setTotalInterest(getStatementFieldDetailsAmount(profitAndLoss.getTotalInterestPaid()));
                        financial.setDepriciation(getStatementFieldDetailsAmount(profitAndLoss.getDepreciation()));
                        financial.setTaxPaid(getStatementFieldDetailsAmount(profitAndLoss.getTaxPaid()));
                        financial.setProfitAfterTaxPaid(getStatementFieldDetailsAmount(profitAndLoss.getPostTaxCashProfitsWithSalAndPartnerInterest()));
                        financial.setIncomeFromOtherSources(getStatementFieldDetailsAmount(profitAndLoss.getOtherIncome()));
                        financial.setInterestPaidToPartners(getStatementFieldDetailsAmount(profitAndLoss.getPartnersInterest()));
                        financial.setPartnersSalary(getStatementFieldDetailsAmount(profitAndLoss.getPartnerSalary()));
                    }
                }

                financial.setMiscOutflowAmount(Constant.BLANK);
                financialList.add(financial);
            }
        }

        if(financialList.isEmpty()){
            financialList.add(Financial.builder()
                    .financialId(Constant.BLANK)
                    .financialYear(Constant.BLANK)
                    .auditedFlag(Constant.BLANK)
                    .considerInFinAsmntFlg(Constant.BLANK)
                    .shareCapital(Constant.BLANK)
                    .reservesAndSurplus(Constant.BLANK)
                    .shortTermBankBorrowing(Constant.BLANK)
                    .longTermDebt(Constant.BLANK)
                    .unsecuredLoans(Constant.BLANK)
                    .currentLiabilities(Constant.BLANK)
                    .provision(Constant.BLANK)
                    .defferedTaxLiability(Constant.BLANK)
                    .contingentLiabilities(Constant.BLANK)
                    .netFixedAssets(Constant.BLANK)
                    .investments(Constant.BLANK)
                    .loansAndAdvances(Constant.BLANK)
                    .bookDebtLessthanSixmonths(Constant.BLANK)
                    .bookDebtGreaterOrEqualthanSixmonths(Constant.BLANK)
                    .inventory(Constant.BLANK)
                    .cashAndBankBalances(Constant.BLANK)
                    .otherCurrentAssets(Constant.BLANK)
                    .defferedTaxAssets(Constant.BLANK)
                    .netSales(Constant.BLANK)
                    .otherIncome(Constant.BLANK)
                    .totalExpense(Constant.BLANK)
                    .totalInterest(Constant.BLANK)
                    .depriciation(Constant.BLANK)
                    .taxPaid(Constant.BLANK)
                    .profitAfterTaxPaid(Constant.BLANK)
                    .incomeFromOtherSources(Constant.BLANK)
                    .interestPaidToPartners(Constant.BLANK)
                    .partnersSalary(Constant.BLANK)
                    .miscOutflowAmount(Constant.BLANK)
                    .build());
        }
        return financialList;
    }

    private List<SaveVerificationBasicInfo> buildSaveVerificationBasicInfo(ApplicationRequest applicationRequest) {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        List<SaveVerificationBasicInfo> saveVerificationBasicInfoList = new ArrayList<>();
        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, institutionId);
        if(null != verificationDetails) {
            List<PropertyVerification> propertyVerificationList =  verificationDetails.getResidenceVerification();
            if(CollectionUtils.isNotEmpty(propertyVerificationList)) {
                saveVerificationBasicInfoList.addAll(getVerification(applicationRequest, propertyVerificationList));
            }
            propertyVerificationList =  verificationDetails.getOfficeVerification();
            if(CollectionUtils.isNotEmpty(propertyVerificationList)) {
                saveVerificationBasicInfoList.addAll(getVerification(applicationRequest, propertyVerificationList));
            }
        }
        return saveVerificationBasicInfoList;
    }

    private List<SaveVerificationBasicInfo> getVerification(ApplicationRequest applicationRequest, List<PropertyVerification> propertyVerificationList) {
        List<SaveVerificationBasicInfo> verificationBasicInfoList = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(propertyVerificationList)) {
            for(PropertyVerification propertyVerification : propertyVerificationList) {
                PropertyVerificationInput input = propertyVerification.getInput();
                PropertyVerificationOutput output = propertyVerification.getOutput();
                String appId = propertyVerification.getApplicantId();

                SaveVerificationBasicInfo saveVerificationBasicInfo = SaveVerificationBasicInfo.builder()
                        .prospectCode(applicationRequest.getMiFinProspectCode())
                        .mobNo(CollectionUtils.isNotEmpty(input.getPhone()) && null != input.getPhone().get(0).getPhoneNumber() ?
                                input.getPhone().get(0).getPhoneNumber() : Constant.BLANK)
                        .remarks(null != input.getRemarks() ? input.getRemarks().getComment() : Constant.BLANK)
                        .supervisor(null != output ? output.getCpvReviewedBy(): Constant.BLANK)
                        .personContacted(null != output && null != output.getContactedTo() ? output.getContactedTo() : Constant.BLANK)
                        .verificationResult(null != output && null != output.getStatus() ? output.getStatus().toUpperCase() : Constant.BLANK)
                        .initiatedDate(null != output && null != output.getInitiateDate() ?
                                GngDateUtil.changeDateFormat(output.getInitiateDate(), GngDateUtil.ddMMMyyyy_WITH_HYPHEN) : Constant.BLANK)
                        .completionDate(null != output && null != output.getCompletionDate() ?
                                GngDateUtil.changeDateFormat(output.getCompletionDate(), GngDateUtil.ddMMMyyyy_WITH_HYPHEN) : Constant.BLANK)
                        .verificationId(StringUtils.isNotEmpty(propertyVerification.getMifinVerificationId()) ?
                                propertyVerification.getMifinVerificationId(): Constant.BLANK)
                        .verificationStatus(StringUtils.equalsIgnoreCase(MifinConstants.VERIFIED, propertyVerification.getStatus()) ?
                                MifinConstants.COMPLETED : propertyVerification.getStatus().toUpperCase())
                        .agencyName(Constant.BLANK)
                        .agentName(Constant.BLANK)
                        .contactPerson(Constant.BLANK)
                        .noOfAttempts(Constant.BLANK)
                        .verificationType(MifinConstants.PERSONAL_VERIFICATION)
                        .build();
                if(null != applicationRequest.getRequest().getApplicant().getAmbitMifinData()) {
                    if (StringUtils.equals(appId, "0")) {
                        saveVerificationBasicInfo.setApplicantCode(applicationRequest.getRequest().getApplicant().getAmbitMifinData().getMiFinApplicantCode());
                    } else if(CollectionUtils.isNotEmpty( applicationRequest.getRequest().getCoApplicant())){
                        for (Applicant applicant : applicationRequest.getRequest().getCoApplicant()) {
                            if (StringUtils.equals(appId, applicant.getApplicantId())) {
                                saveVerificationBasicInfo.setApplicantCode(applicant.getAmbitMifinData().getMiFinApplicantCode());
                            }
                        }
                    }
                }
                if (null != input && null != input.getAddress()) {
                    CustomerAddress address = input.getAddress();
                    saveVerificationBasicInfo.setBusinessAddress(StringUtils.isNotEmpty(address.getAddressLine1()) ?
                            address.getAddressLine1().concat(" ").concat(address.getAddressLine2()) : Constant.BLANK);
                    saveVerificationBasicInfo.setVerificationRCUStatus(StringUtils.isNotEmpty(address.getAddressType()) ?
                            MifinConstants.PERSONAL_VERIFICATION +":"+ address.getAddressType().toUpperCase() : Constant.BLANK);
                }
                verificationBasicInfoList.add(saveVerificationBasicInfo);
            }
        }
        return verificationBasicInfoList;
    }

    private Viability getViability () {

        return Viability.builder()
                .segment(Constant.BLANK)
                .manufacturer(Constant.BLANK)
                .makeNModel(Constant.BLANK)
                .variant(Constant.BLANK)
                .productType(Constant.BLANK)
                .mfgYear(Constant.BLANK)
                .proposedEmi(Constant.BLANK)
                .loadCapacityInTonKlG(Constant.BLANK)
                .route(Constant.BLANK)
                .natureOfGoodsG(Constant.BLANK)
                .distanceInKmPerTripG(Constant.BLANK)
                .noOfTripsPerMonthG(Constant.BLANK)
                .avgLoadInTonKlG(Constant.BLANK)
                .fuelAvgKmPerLitre(Constant.BLANK)
                .costPerLitreOfFuel(Constant.BLANK)
                .noOfTyres(Constant.BLANK)
                .costOfOneTyre(Constant.BLANK)
                .lifeNewTyresInKm(Constant.BLANK)
                .driversSalaryNAllowances(Constant.BLANK)
                .cleanersSalaryNAllowances(Constant.BLANK)
                .permitCost(Constant.BLANK)
                .fcCharges(Constant.BLANK)
                .tollTaxPaid(Constant.BLANK)
                .taxes(Constant.BLANK)
                .maintenance(Constant.BLANK)
                .miscellaneous(Constant.BLANK)
                .insuranceExpenses(Constant.BLANK)
                .remarks(Constant.BLANK)
                .natureOfServicep(Constant.BLANK)
                .distanceInKmBothSidep(Constant.BLANK)
                .noOfTripsInDayp(Constant.BLANK)
                .noOfTripsInMonthp(Constant.BLANK)
                .seatingCapacityp(Constant.BLANK)
                .sleeperCapacityp(Constant.BLANK)
                .avgOccupancyInPercentp(Constant.BLANK)
                .seatingRatep(Constant.BLANK)
                .sleepingRatep(Constant.BLANK)
                .build();
    }

    private List<VerificationQuesAns> getVerificationQuesAns() {
        return new ArrayList<VerificationQuesAns>(){{
            add(
                    VerificationQuesAns.builder()
                            .questionId(Constant.BLANK)
                            .answer(Constant.BLANK)
                            .build()
            );
        }};
    }

    private LoanDetailsBasicInfo buildLoanDetailBasicInfo(ApplicationRequest applicationRequest1) {
        String refId = applicationRequest1.getRefID();
        String institutionId = applicationRequest1.getHeader().getInstitutionId();
        ApplicationRequest applicationRequest = applicationRepository.getApplicationRequest(refId, institutionId);
        Application application = applicationRequest.getRequest().getApplication();
        Repayment repayment = applicationRepository.fetchRepaymentDetails(refId, institutionId);
        EligibilityDetails eligibility = applicationRepository.fetchEligibilityDetails(refId, institutionId);
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId , institutionId);

        LoanDetailsBasicInfo.LoanDetailsBasicInfoBuilder loanDetailsBasicInfo = LoanDetailsBasicInfo.builder()
                .prospectCode(applicationRequest1.getMiFinProspectCode())
                .promoSchemeCustomerType(Constant.BLANK)
                .appFormNumber(applicationRequest.getRefID())
                .repaymentMode(null != repayment && null != repayment.getRepaymentDetailsList().get(0).getRepaymentType() ?
                        RepaymentTypeEnum.getMifinRepayementTypeFromRepaymentType(repayment.getRepaymentDetailsList().get(0).getRepaymentType())
                        : RepaymentTypeEnum.CASH.getMifinRepayemntType() )
                .incomeMethod("BUSINESS")
                .repaymentFrequency("MONTHLY")
                .installmentType("EQUAL STRUCTURED INSTALLMENTS")
                .targetIRR("0")
                .dueDay("5")
                .addToLoanAmt(GNGWorkflowConstant.NO.toFaceValue())
                .maturityDate(Constant.BLANK)
                .tenorDays(Constant.BLANK);

        ProfessionIncomeDetails professionIncomeDetails = applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails();
        if(null != application && null != professionIncomeDetails) {
            loanDetailsBasicInfo.loanType(StringUtils.isNotEmpty(application.getLoanPurpose()) ? application.getLoanPurpose() : "")
                    .purposeSubScheme(StringUtils.isNotEmpty(application.getLoanPurpose()) ?
                        SchemeEnum.getGoNoGoSchemeFromMifinSchemeCode(professionIncomeDetails.getLoanScheme())+":"+application.getLoanPurpose() :"")
                    .scheme(ProductEnum.BL.getMifinProductName() + ":" + SchemeEnum.getGoNoGoSchemeFromMifinSchemeCode(professionIncomeDetails.getLoanScheme()))
                    .loanAmt(String.valueOf((int)application.getLoanAmount()));
        }
        if( null != loanCharges) {
            loanDetailsBasicInfo.advEmi(String.valueOf((int)loanCharges.getAdvanceEMI()))
                    .instRate(null != loanCharges.getInterestDetails() ?
                            String.valueOf(loanCharges.getInterestDetails().getInterestRate()) : "");
        }
        if(null != eligibility) {
            loanDetailsBasicInfo.maxLtv(String.valueOf(eligibility.getFoirDetail().getLtv()))
                    .tenorMonths(String.valueOf(eligibility.getTenor()))
                    .instlNumber(String.valueOf(eligibility.getTenor()))
                    .sanctionAmt(String.valueOf((int)eligibility.getAprvLoan()));
        }
        Branch branch = applicationRequest.getAppMetaData().getBranchV2();
        loanDetailsBasicInfo.realtionshipManager(null != branch
                && StringUtils.isNotEmpty(branch.getMifinBranchManagerId()) ? branch.getMifinBranchManagerId() : Constant.BLANK);
        if(null != application && StringUtils.isNotEmpty(application.getChannel())
                && StringUtils.equalsIgnoreCase(application.getChannel(), "DSA")) {
            loanDetailsBasicInfo.secSourcingChannelType(MifinConstants.DSA)
                    .channelType(MifinConstants.DSA);
                if(StringUtils.isNotEmpty(application.getMifinAgencyId())) {
                    loanDetailsBasicInfo.channel(application.getMifinAgencyId())
                            .secSourcingChannel(application.getMifinAgencyId());
                    if (StringUtils.isNotEmpty(application.getMifinAgentId())) {
                        loanDetailsBasicInfo.agent(application.getMifinAgentId())
                                .secSourcingAgent(application.getMifinAgentId());
                    }
                }
            }
        return loanDetailsBasicInfo.build();
    }

    private String getStatementFieldDetailsAmount(StatementFieldDetails statementFieldDetails){
        return null != statementFieldDetails && statementFieldDetails.getChangeInAmount()> 0 ?
                String.valueOf(Math.round(statementFieldDetails.getChangeInAmount())): "";
    }

    private String getYear(String date){
        String year = "";
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(date);
        if(m.find()) {
            year= m.group(0);
        }
        return year;
    }

    private List<SaveFinancialInfoBankingDetails> getSaveFinancialInfoBankingDetails(ApplicationRequest applicationRequest) {
        List<SaveFinancialInfoBankingDetails> saveFinancialInfoBankingDetailsList= null;

        CamDetails camDetails = applicationRepository.fetchCamDetails(applicationRequest.getRefID() , applicationRequest.getHeader().getInstitutionId() , EndPointReferrer.CAM_BANKING_DETAILS);

        if(null != camDetails && CollectionUtils.isNotEmpty(camDetails.getBankingStatements())){

            for(BankingStatement bankingStatement : camDetails.getBankingStatements()) {

                if(null != bankingStatement.getAccountNumber() && null != bankingStatement.getAccountHolder()) {
                    SaveFinancialInfoBankingDetails saveFinancialInfoBankingDetails = SaveFinancialInfoBankingDetails.builder()
                            .bankingId(StringUtils.isNotEmpty(bankingStatement.getMifinBankingId()) ? bankingStatement.getMifinBankingId() : Constant.BLANK)
                            .accountType(null != bankingStatement.getAccountType() ? AccountTypeEnum.getMifinAccountTypeFromRepaymentType(bankingStatement.getAccountType()) : Constant.BLANK)
                            .beneficiaryName( bankingStatement.getAccountHolder().toUpperCase())
                            .account(bankingStatement.getAccountNumber())
                            .ifscCode(null != bankingStatement.getIfscCode() ? bankingStatement.getIfscCode().toUpperCase() : Constant.BLANK)
                            .bankingSinceMonths("90")
                            .computationFlag(Constant.BLANK)
                            .existingEmis(Constant.BLANK)
                            .avgCashCreditInterest(String.valueOf((int) bankingStatement.getAverageCreditSum()))
                            .proposedLoanEmi(Constant.BLANK)
                            .abbCalculation("5")
                            .deleteFlag("N").build();
                    if (CollectionUtils.isNotEmpty(bankingStatement.getBankingTransactions())) {
                        saveFinancialInfoBankingDetails.setBankDetailsList(getBankDetails(bankingStatement.getBankingTransactions()));
                    }

                    if (null == saveFinancialInfoBankingDetailsList) {
                        saveFinancialInfoBankingDetailsList = new ArrayList<>();
                    }
                    saveFinancialInfoBankingDetailsList.add(saveFinancialInfoBankingDetails);
                }
            }
        }
        return  saveFinancialInfoBankingDetailsList;
    }

    private List<BankDetails> getBankDetails(List<BankingTransaction> bankingTransactionList) {

        List<BankDetails> bankDetailsList = null;
        for(BankingTransaction bankingTransaction : bankingTransactionList){
            BankDetails bankDetails = BankDetails.builder()
                    .accDtlId(StringUtils.isNotEmpty(bankingTransaction.getMifinAccountDetailsId())? bankingTransaction.getMifinAccountDetailsId(): Constant.BLANK)
                    .cashCredits(String.valueOf((int)bankingTransaction.getCashCredit()))
                    .chequeCredits(String.valueOf((int)(bankingTransaction.getCreditTotal() - bankingTransaction.getCashCredit())))
                    .noOfCashCredits(Constant.BLANK)
                    .noOfChequeCredits(Constant.BLANK)
                    .noOfChequeIssued(Constant.BLANK)
                    .noOfInwardChequeReturn(String.valueOf((int) bankingTransaction.getNumberOfIW()))
                    .noOfOutwardChequeReturn(String.valueOf((int) bankingTransaction.getNumberOfOW()))
                    .momSales(Constant.BLANK)
                    .deleteFlag("N").build();

            String date = bankingTransaction.getDate();
            if(null != date){
                bankDetails.setYear(getYear(date));
                date = date.replace(bankDetails.getYear(),Constant.BLANK);
                bankDetails.setMonth(date.replace("-",Constant.BLANK).trim().toUpperCase());
            }

            List<AmountForDate> inMonthBalance = bankingTransaction.getInMonthBalance();
            if(CollectionUtils.isNotEmpty(inMonthBalance)) {
                for (AmountForDate amountForDate : inMonthBalance) {
                    if (amountForDate.getMonthDate() == 2) {
                        bankDetails.setBalanceAsOn2nd(String.valueOf((int) amountForDate.getBalanceAmt()));
                    } else if (amountForDate.getMonthDate() == 8) {
                        bankDetails.setBalanceAsOn8th(String.valueOf((int)amountForDate.getBalanceAmt()));
                    } else if (amountForDate.getMonthDate() == 15) {
                        bankDetails.setBalanceAsOn15th(String.valueOf((int)amountForDate.getBalanceAmt()));
                    } else if (amountForDate.getMonthDate() == 22) {
                        bankDetails.setBalanceAsOn22nd(String.valueOf((int)amountForDate.getBalanceAmt()));
                    } else if (amountForDate.getMonthDate() == 27) {
                        bankDetails.setBalanceAsOn27th(String.valueOf((int)amountForDate.getBalanceAmt()));
                    }
                }
            }
            if(null == bankDetailsList){
                bankDetailsList = new ArrayList<>();
            }
            bankDetailsList.add(bankDetails);
        }

        return bankDetailsList;
    }

    private SaveFinancialInfoBasicInfoDetails getSaveFinancialInfoBasicInfoDetails(ApplicationRequest applicationRequest) {
        SaveFinancialInfoBasicInfoDetails saveFinancialInfoBasicInfoDetails = null;
        Applicant applicant = applicationRequest.getRequest().getApplicant();

        if(null != applicant && null != applicant.getProfessionIncomeDetails()){
            saveFinancialInfoBasicInfoDetails = SaveFinancialInfoBasicInfoDetails.builder()
                    .sector("salaried type")
                    .prospectCode(applicationRequest.getMiFinProspectCode())
                    .applicantCode(null != applicant.getAmbitMifinData() ? applicant.getAmbitMifinData().getMiFinApplicantCode():Constant.BLANK)
                    .industryClassifaction(null != applicant.getProfessionIncomeDetails() && StringUtils.isNotEmpty(applicant.getProfessionIncomeDetails().getClassification()) ?
                            applicant.getProfessionIncomeDetails().getClassification().toUpperCase() : "SMALL")
                    .yearsInService(Constant.BLANK)
                    .totalEmployee(Constant.BLANK)
                    .expInExistingLineOfBusiness(Constant.BLANK)
                    .purchase(Constant.BLANK)
                    .valueOfPlant("300000")
                    .employmentDetailsId(null != applicant.getAmbitMifinData()?applicant.getAmbitMifinData().getMifinEmploymentDetailsId():Constant.BLANK)
                    .build();

        }
        return saveFinancialInfoBasicInfoDetails;
    }

    private DisbursalMakerBasicInfo buildDisbursalMakerBasicInfo(ApplicationRequest applicationRequest) {
        return DisbursalMakerBasicInfo.builder()
                .prospectCode(StringUtils.isNotEmpty(applicationRequest.getMiFinProspectCode()) ? applicationRequest.getMiFinProspectCode():"")
                .build();
    }

    private List<DisbursalSchedule> buildDisbursalSchedule(ApplicationRequest applicationRequest) {
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
        EligibilityDetails eligibility = applicationRepository.fetchEligibilityDetails(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
        return new ArrayList<DisbursalSchedule>(){{
            add(
                    DisbursalSchedule.builder()
                            .disbAmount(null != eligibility ? String.valueOf((int)eligibility.getAprvLoan()):"0")
                            .disbDate(GngDateUtil.changeDateFormat(loanCharges.getDisbursedDate(), GngDateUtil.ddMMMyyyy_WITH_HYPHEN))
                            .disbStage(Constant.BLANK)
                            .build()
            );
        }};
    }

    private List<InterestRateSchedule> buildInterestRateSchedule(ApplicationRequest applicationRequest) {
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
        return new ArrayList<InterestRateSchedule>(){{
            add(InterestRateSchedule.builder()
                    .startDate(GngDateUtil.changeDateFormat(loanCharges.getDisbursedDate(), GngDateUtil.ddMMMyyyy_WITH_HYPHEN))
                    .endDate("")
                    .interestRate(null != loanCharges && null != loanCharges.getInterestDetails() ?
                            String.valueOf(loanCharges.getInterestDetails().getDisbInterestRate()) : Constant.BLANK)
                    .build()
            );
        }};
    }

    private List<Charges> buildCharges(ApplicationRequest applicationRequest) {
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
        List<LoanDetails> chargesList = loanCharges.getLoanChargesDetails();
        List<Charges> disbChargesList = new ArrayList<>();

        if(CollectionUtils.isNotEmpty(chargesList)) {
            for(LoanDetails charges: chargesList) {
                if( !StringUtils.equalsIgnoreCase(charges.getChargeName(), ChargesEnum.PREEMI.getGonogoCharge())
                        && !StringUtils.equalsIgnoreCase(charges.getChargeName(), ChargesEnum.ADVEMIINTEREST.getGonogoCharge())
                        && charges.isConsiderForDeduction()) {
                    disbChargesList.add(
                            Charges.builder()
                                    .status("PRE_D")
                                    .loanChargeId(Constant.BLANK)
                                    .chargeName(ChargesEnum.getMifinChargeFromGonogoCharge(charges.getChargeName()))
                                    .chargeDate(Constant.BLANK)
                                    .chargeAmtCalculation(Constant.BLANK)
                                    .chargePercentage(Constant.BLANK)
                                    .chargeAmount(null != charges.getChargeAmount() ? String.valueOf(charges.getNetChargeAmount()) : Constant.BLANK)
                                    .build()
                    );
                }
            }
        } else {
            disbChargesList. add(Charges.builder()
                    .status(Constant.BLANK)
                    .loanChargeId(Constant.BLANK)
                    .chargeName(Constant.BLANK)
                    .chargeDate(Constant.BLANK)
                    .chargeAmtCalculation(Constant.BLANK)
                    .chargePercentage(Constant.BLANK)
                    .chargeAmount(Constant.BLANK)
                    .build());
        }
        return disbChargesList;
    }

    private TrancheDisbursalSchedule buildTrancheDisbursalSchedule(ApplicationRequest applicationRequest) {
        EligibilityDetails eligibility = applicationRepository.fetchEligibilityDetails(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());
        LoanDetails charge = loanCharges.getLoanChargesDetails().stream().filter(charges -> StringUtils.equalsIgnoreCase(
                charges.getChargeName(), ChargesEnum.PREEMI.getGonogoCharge()) && charges.isConsiderForDeduction()).findFirst().orElse(null);
        TrancheDisbursalSchedule trancheDisbursalSchedule = TrancheDisbursalSchedule.builder()
                .preEmiAsCharge(null != charge ? GNGWorkflowConstant.YES.toFaceValue() : GNGWorkflowConstant.NO.toFaceValue())
                .disbStage(Constant.BLANK)
                .disbAmount(null != eligibility ? String.valueOf((int)eligibility.getAprvLoan()):"0")
                .build();
        return trancheDisbursalSchedule;
    }

    private DisbursalDetail buildDisbursalDetail(ApplicationRequest applicationRequest) {
        return DisbursalDetail.builder()
                .sendToAuthor("Y")
                .fileAckDate("07-OCT-2019")
                .fileAckHours("13")
                .fileAckMin("15")
                .fileAckFlag("Y")
                .finalDisbursal("N")
                .build();
    }

    private List<DisbursalDecisionInfo> buildDisbursalDecisionInfo(ApplicationRequest applicationRequest) {
        List<DisbursalDecisionInfo> disbursalDecisionInfoList = new ArrayList<>();
        DisbursementMemo dmDetails = applicationRepository.fetchDMDetails(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId());

        if(null != dmDetails && CollectionUtils.isNotEmpty(dmDetails.getDmInputList())) {
            for(DMInput dmInput: dmDetails.getDmInputList()) {
                DisbursalDecisionInfo disbursalDecisionInfo = DisbursalDecisionInfo.builder()
                        .applicantId(Constant.BLANK)
                        .entityType("OTHERS")
                        .entityName(null != dmDetails.getCamSummary() && null != dmDetails.getCamSummary().getApplicantName() ?
                                dmDetails.getCamSummary().getApplicantName().getFullName() : "OTHER")
                        .disbAmount(dmInput.getDmAmt())
                        .bankId(Constant.BLANK)
                        .instrumentType(RepaymentTypeEnum.getMifinRepayementTypeFromRepaymentType(dmInput.getDmPaymentMode()))
                        .build();
                Repayment repayment = dmDetails.getRepayment();
                if (null != repayment && CollectionUtils.isNotEmpty(repayment.getRepaymentDetailsList())) {
                    RepaymentDetails repaymentDetails = repayment.getRepaymentDetailsList().get(0);
                    disbursalDecisionInfo.setAccountNo(null != repaymentDetails ? repaymentDetails.getAccountNumber() : Constant.BLANK);
                    disbursalDecisionInfo.setAccountType(null != repaymentDetails && null != repaymentDetails.getAccountType() ?
                            AccountTypeEnum.getMifinAccountTypeFromRepaymentType(repaymentDetails.getAccountType()) : AccountTypeEnum.Saving.getMifinAccountType());
                    disbursalDecisionInfo.setBeneficiaryName(null != repaymentDetails ? repaymentDetails.getAccountHolderName().getFullName() : Constant.BLANK);
                    disbursalDecisionInfo.setIfscCode(null != repaymentDetails ? repaymentDetails.getIfscCode() : Constant.BLANK);
                }
                disbursalDecisionInfoList.add(disbursalDecisionInfo);
            }
        }
        return disbursalDecisionInfoList;
    }
    private UpdateStatusDedupeBasicInfo buildUpdateProcessDedupeBasicInfo(UpdateDedupeRequest updateDedupeRequest, ApplicantdedupeRequest applicantdedupeRequest)  throws Exception
    {
        return UpdateStatusDedupeBasicInfo.builder()
                .applicantCode(applicantdedupeRequest.getApplicantCode())
                .prospectCode(applicantdedupeRequest.getProspectCode())
                .customerDedupeStatus(StringUtils.isNotEmpty(applicantdedupeRequest.getCuomerDedupeStatus()) ? applicantdedupeRequest.getCuomerDedupeStatus().toUpperCase() : "NO MATCH FOUND")
                .dedupeDecision(updateDedupeRequest.getMifinDedupeDecision().toUpperCase())
                .reason(updateDedupeRequest.getMifinDedupeDecision().toUpperCase()+":"+updateDedupeRequest.getMifinReason().toUpperCase())
                .remarks(StringUtils.isNotEmpty(applicantdedupeRequest.getRemarks()) ? applicantdedupeRequest.getRemarks() : "SOFTCELL")
                .build();
    }
    @Override
    public SearchExistingApplicantRequest buildSearchExistingApplicantRequest(ApplicationRequest applicationRequest, MiFinPanSearchBaseResponse miFinPanSearchBaseResponse,boolean hit_again,String userId)
    {
        return SearchExistingApplicantRequest.builder()
                .authenticationDetails(buildAuthDetails(userId))
                .searchApplicantBasicInfo(buildSearchExistingBasicInfo(applicationRequest,miFinPanSearchBaseResponse,hit_again))
                .institutionId(applicationRequest.getHeader().getInstitutionId())
                .referenceId(applicationRequest.getRefID())
                .product(applicationRequest.getHeader().getProduct().toProductName())
                .type(ThirdPartyFieldConstant.MIFIN_SEARCH_EXISTING_APPLICANT)
                .build();
    }

    private SearchApplicantBasicInfo buildSearchExistingBasicInfo(ApplicationRequest applicationRequest,MiFinPanSearchBaseResponse miFinPanSearchBaseResponse,boolean hit_again) {

        String applicantCode=Constant.BLANK;
        String parameter=Constant.BLANK;
        if(CollectionUtils.isNotEmpty(miFinPanSearchBaseResponse.getAmbitMifinCustomerDetailsList()) && StringUtils.isNotEmpty(miFinPanSearchBaseResponse.getAmbitMifinCustomerDetailsList().get(0).getApplicantCode())){
                applicantCode=miFinPanSearchBaseResponse.getAmbitMifinCustomerDetailsList().get(0).getApplicantCode();
                parameter=MifinConstants.APPLICANTCODE;
        }else if(hit_again){
            parameter=MifinConstants.PAN;
        }
        else {
            parameter=MifinConstants.PAN;
        }

        return SearchApplicantBasicInfo.builder()
                .prospectCode(Constant.BLANK)
                .applicantCode(parameter.equalsIgnoreCase(MifinConstants.APPLICANTCODE) ? applicantCode:Constant.BLANK)
                .companyName(Constant.BLANK)
                .dobOfIncorp(Constant.BLANK)
                .email(Constant.BLANK)
                .voterId(Constant.BLANK)
                .gstNo(Constant.BLANK)
                .lastName(Constant.BLANK)
                .passportNo(Constant.BLANK)
                .mobileNo(Constant.BLANK)
                .panNo(parameter.equalsIgnoreCase(MifinConstants.PAN) ? applicationRequest.getRequest().getPanSearchNo():Constant.BLANK)
                .build();

    }

    private AuthenticationDetails buildAuthDetails(String userId) {
        return AuthenticationDetails.builder()
                .appName("SOFTCELL")
                .appPass("PASS@123")
                .ipAddress("10.1.6.219")
                .deviceId("CRM360")
                .latitude("28.6127356")
                .longitude("77.3877269")
                .uniqueRequestId(String.valueOf(GngUtils.generateUniqueId()))
                .userId(StringUtils.isNotEmpty(userId) ? userId : Constant.BLANK)
                .appVersion(Constant.BLANK).build();
    }

    public String getSalutation(String gender, String maritalStatus) {

        if (org.apache.commons.lang3.StringUtils.equalsIgnoreCase(gender, GNGWorkflowConstant.MALE.toFaceValue())) {
            return GNGWorkflowConstant.MR.toFaceValue();
        }
        else if (org.apache.commons.lang3.StringUtils.equalsIgnoreCase(gender, GNGWorkflowConstant.FEMALE.toFaceValue())
                && org.apache.commons.lang3.StringUtils.equalsIgnoreCase(maritalStatus, GNGWorkflowConstant.SINGLE.toFaceValue())) {
            return GNGWorkflowConstant.MS.toFaceValue();
        } else {
            return GNGWorkflowConstant.MRS.toFaceValue();
        }
    }
}
