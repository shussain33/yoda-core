package com.softcell.gonogo.service.factory.tvscdlosintegration;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.InsertOrUpdateTvsRecordGroupTwo;

public interface LosTvsGroupTwoReqBuilder {

    InsertOrUpdateTvsRecordGroupTwo buildLosTvsGroupTwoRequest(GoNoGoCustomerApplication gonogoCustomerApplication);

}
