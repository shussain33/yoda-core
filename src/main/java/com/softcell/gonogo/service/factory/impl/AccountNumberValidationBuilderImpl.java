package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.*;
import com.softcell.gonogo.model.core.BankingDetails;
import com.softcell.gonogo.model.imps.*;
import com.softcell.gonogo.model.request.imps.IMPSRequest;
import com.softcell.gonogo.model.request.imps.IMPSResponse;
import com.softcell.gonogo.service.factory.AccountNumberValidationBuilder;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by sampat on 3/11/17.
 */

@Service
public class AccountNumberValidationBuilderImpl implements AccountNumberValidationBuilder{


    @Override
    public AccountNumberValidationRequest buildAccountNumberValidationRequest(IMPSRequest impsRequest, IMPSConfigDomain impsConfig) {

        return AccountNumberValidationRequest.builder()
                .instrumentIdentifier(impsRequest.getAccountNumber())
                .instrumentIFSC(impsRequest.getIFSCCode())
                .instrumentExpiryMonth(FieldSeparator.BLANK)
                .instrumentExpiryYear(FieldSeparator.BLANK)
                .merchantTxnPGMID(impsConfig.getMerchantTxnPGMID())
                .merchantIdentifier(impsConfig.getMerchantIdentifier())
                .channelType(impsConfig.getTxnChannelType())
                .channelName(impsConfig.getTxnChannelName())
                .txnAmount(impsConfig.getTxnAmount())
                .txnType(impsConfig.getTxnType())
                .txnSubtype(impsConfig.getTxnSubtype())
                .refundAmount(impsConfig.getRefundAmount())
                .instrumentDetails(impsConfig.getInstrumentDetails())
                .instrumentType(impsConfig.getInstrumentType())
                .instrumentToken(FieldSeparator.BLANK)
                .txnRequestType(impsConfig.getTxnRequestType())
                .txnDescription(impsConfig.getTxnDescription())
                .emailSending(impsConfig.getEmailSending())
                .smsSending(impsConfig.getSmsSending())
                .build();
    }

    @Override
    public IMPSResponse buildIMPSResponse(AccountNumberValidationResponse accountNumberValidationResponse) {

        String txnErrorCode = accountNumberValidationResponse.getTxnErrorCode();

        return IMPSResponse.builder()
                .accountHolderName(accountNumberValidationResponse.getAccountHolderName())
                .message((StringUtils.equals(IMPSConstants.PNY_SUCCESS_CODE,txnErrorCode) || StringUtils.equals(IMPSConstants.PNY_DEDUP_CODE,txnErrorCode) ? ResponseConstants.IMPS_SUCCESS : ResponseConstants.IMPS_FAILED))
                .status((StringUtils.equals(IMPSConstants.PNY_SUCCESS_CODE,txnErrorCode) || StringUtils.equals(IMPSConstants.PNY_DEDUP_CODE,txnErrorCode) ? IMPSStatus.VALID.name() : IMPSStatus.INVALID.name()))
                .build();
    }

    @Override
    public AccountValidationAttemptResponse buildAccountValidationAttemptResponse(int validationAttempt, long validationCount) {

        long remainingAttempt = validationAttempt-validationCount;

        return AccountValidationAttemptResponse.builder()
                .remainingAttempt( remainingAttempt < 0 ? 0 : remainingAttempt )
                .status(Status.SUCCESS.name())
                .build();
    }

    @Override
    public AccountNumberInfo.AccountNumberInfoBuilder buildAccountNumberInfo(IMPSRequest impsRequest) {

        AccountNumberInfo.AccountNumberInfoBuilder accountNumberInfo = AccountNumberInfo.builder();
        accountNumberInfo.institutionId(impsRequest.getHeader().getInstitutionId());
        accountNumberInfo.refID(impsRequest.getReferenceID());
        accountNumberInfo.dateTime(new Date());
        accountNumberInfo.product(impsRequest.getHeader().getProduct());
        accountNumberInfo.bankName(impsRequest.getBankName());
        accountNumberInfo.bankBranch(impsRequest.getBankBranch());
        accountNumberInfo.accountHolderFname(impsRequest.getAccountHolderFName());
        accountNumberInfo.accountHolderMname(impsRequest.getAccountHolderMName());
        accountNumberInfo.accountHolderLname(impsRequest.getAccountHolderLName());
        accountNumberInfo.accountNumber(impsRequest.getAccountNumber());
        accountNumberInfo.accountType(impsRequest.getAccountType());
        accountNumberInfo.ifscCode(impsRequest.getIFSCCode());
        accountNumberInfo.accountIdentifier(StringUtils.join(impsRequest.getAccountNumber(),impsRequest.getIFSCCode()));
        accountNumberInfo.yearsHeld(impsRequest.getYearsHeld());
        accountNumberInfo.bankingType(impsRequest.getBankingType());
        accountNumberInfo.status(Status.FAILED.toString());
        accountNumberInfo.validFlag(false);
        return accountNumberInfo;
    }

    @Override
    public ValidatedBanksResponse buildValidatedBanksResponse(AccountNumberInfo accountNumberInfo) {

        return ValidatedBanksResponse.builder()
                .bankName(accountNumberInfo.getBankName())
                .bankBranch(accountNumberInfo.getBankBranch())
                .ifscCode(accountNumberInfo.getIfscCode())
                .accountNumber(accountNumberInfo.getAccountNumber())
                .accountHolderFname(accountNumberInfo.getAccountHolderFname())
                .accountHolderMname(accountNumberInfo.getAccountHolderMname())
                .accountHolderLname(accountNumberInfo.getAccountHolderLname())
                .accountType(accountNumberInfo.getAccountType())
                .yearsHeld(accountNumberInfo.getYearsHeld())
                .bankingType(accountNumberInfo.getBankingType())
                .validFlag(accountNumberInfo.isValidFlag())
                .status(StringUtils.endsWithIgnoreCase(accountNumberInfo.getStatus(),Status.SUCCESS.name()) ? Status.VALID.name() : Status.INVALID.name())
                .build();
    }

    @Override
    public BankingDetails buildBankingDetails(IMPSRequest impsRequest) {
        BankingDetails bankingDetails = new BankingDetails();

        bankingDetails.setAccountHolderName(GngUtils.convertFirstMiddleLastToName(impsRequest.getAccountHolderFName(),
                impsRequest.getAccountHolderMName(),impsRequest.getAccountHolderLName()));
        bankingDetails.setBankName(impsRequest.getBankName());
        bankingDetails.setBranchName(impsRequest.getBankBranch());
        bankingDetails.setAccountType(impsRequest.getAccountType());
        bankingDetails.setAccountNumber(impsRequest.getAccountNumber());
        bankingDetails.setIfscCode(impsRequest.getIFSCCode());
        bankingDetails.setYearHeld(impsRequest.getYearsHeld());
        bankingDetails.setBankingType(impsRequest.getBankingType());

        bankingDetails.setSelected(false);
        bankingDetails.setDateTime(new Date());
        return bankingDetails;
    }
}
