package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.AssetDetails;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.tkil.TKILRequest;
import com.softcell.gonogo.service.factory.TkilRequestBuilder;
import com.softcell.utils.GngUtils;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by sampat on 24/8/17.
 */
@Service
public class TkilRequestBuilerImpl implements TkilRequestBuilder{

    @Override
    public TKILRequest buildTkilRequest(PostIpaRequest postIpaRequest, ApplicationRequest appRequest) throws Exception {

       TKILRequest tkilRequest = new TKILRequest();

        if (postIpaRequest != null && appRequest != null && postIpaRequest.getHeader() != null && postIpaRequest.getPostIPA() != null) {

            String refId = postIpaRequest.getRefID();

            PostIPA postIPA = postIpaRequest.getPostIPA();

            tkilRequest.setDoNumber(refId);
            tkilRequest.setNetLoanAmount(GngUtils.getString(postIPA.getNetFundingAmount()));
            tkilRequest.setFinanceAmount(GngUtils.getString(postIPA.getFinanceAmount()));
            tkilRequest.setProcessingFees(GngUtils.getString(postIPA.getProcessingFees()));
            tkilRequest.setCustomerAmount(GngUtils.getString(postIPA.getEmi()));
            tkilRequest.setDealersubvention(GngUtils.getString(postIPA.getDealerSubvention()));
            tkilRequest.setDisbursementAmount(GngUtils.getString(postIPA.getNetDisbursalAmount()));
            tkilRequest.setAdvanceEMI(GngUtils.getString(postIPA.getAdvanceEmi()));
            tkilRequest.setProductCost(GngUtils.getString(postIPA.getTotalAssetCost()));
            tkilRequest.setAdvanceFees("");
            tkilRequest.setSchemeCodeEMI(postIPA.getScheme());

            String deduction = GngUtils.getString(postIPA
                    .getProcessingFees()
                    + postIPA.getAdvanceEmi()
                    + postIPA.getManufSubBorneByDealer()
                    + postIPA.getDealerSubvention()
                    + postIPA.getOtherChargesIfAny());

            tkilRequest.setDeductionbyHDBFS(deduction);
            tkilRequest.setManufacturerSubvention(GngUtils.getString(postIPA.getManufSubBorneByDealer()));
            tkilRequest.setMarginMoney(GngUtils.getString(postIPA.getMarginMoney()));
            tkilRequest.setOtherCharges(GngUtils.getString(postIPA.getOtherChargesIfAny()));
            tkilRequest.setSumDeduction(deduction);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = postIpaRequest.getDate();
            String dateString = sdf.format(date);
            tkilRequest.setDoDate(dateString);

            List<AssetDetails> assetList = postIPA.getAssetDetails();

            setAssetValue(assetList, tkilRequest);
            /**
             * Below fields we are populating from Application Request Object
             * which we fetched using reference id from PostIpaRequest
             */
            if (appRequest != null) {
                poupulateApplicantDetails(tkilRequest, appRequest);
            }

        } else {
            throw new SystemException("Post Ipa and Application request object must not be null !!");
        }
        return tkilRequest;
    }


    private String calculateDbd(PostIPA postIPA) {
        double dealerSubvention = postIPA.getDealerSubvention();
        double financeAmount = postIPA.getFinanceAmount();
        double dbd = 0d;

        if (financeAmount > 0) {
            dbd = (dealerSubvention * 100) / financeAmount;
        }

        DecimalFormat df = new DecimalFormat("#.##");
        dbd = Double.valueOf(df.format(dbd));

        return GngUtils.getString(dbd);
    }

    private void poupulateApplicantDetails(TKILRequest tkilRequest, ApplicationRequest appRequest) {

        Header header = appRequest.getHeader();
        tkilRequest.setHeader(header);
        tkilRequest.setRefID(appRequest.getRefID());

        Request userRequest = appRequest.getRequest();
        if (userRequest != null) {

            if (appRequest.getHeader() != null) {
                tkilRequest.setStoreCode(appRequest.getHeader().getDealerId());
            } else {
                tkilRequest.setStoreCode("");
            }

            Applicant applicant = userRequest.getApplicant();

            if (applicant != null) {
                tkilRequest.setCustomerName(GngUtils.convertNameToFullName(applicant.getApplicantName()));
                List<CustomerAddress> address = applicant.getAddress();
                setAddress(address, tkilRequest);
            } else {
                tkilRequest.setCustomerName("");
                tkilRequest.setCustomerAddress("");
            }
        }
    }

    private void setAddress(List<CustomerAddress> addressList, TKILRequest tkilRequest) {

        if (addressList != null && addressList.size() > 0) {
            CustomerAddress address = addressList.get(0);
            StringBuilder sb = new StringBuilder();
            sb.append(address.getAddressLine1()).append(", ");
            sb.append(address.getAddressLine2()).append(", ");
            sb.append(address.getLandMark()).append(", ");
            sb.append(address.getCity()).append(", ");
            sb.append(address.getState()).append(", ");
            sb.append(address.getPin());

            String addressString = sb.toString();
            if (addressString != null && addressString.length() > 0) {
                if (addressString.length() > 39) {
                    addressString = addressString.substring(0, 39);
                } else {
                    addressString = addressString.substring(0, addressString.length() - 1);
                }
            }
            tkilRequest.setCustomerAddress(addressString);
        } else {
            tkilRequest.setCustomerAddress("");
        }

    }

    private void setAssetValue(List<AssetDetails> assetList, TKILRequest tkilRequest) {
        if (assetList != null) {
            for (AssetDetails asset : assetList) {
                tkilRequest.setProductBrand(asset.getAssetMake());
                tkilRequest.setProductCatgMake(asset.getAssetCtg());
                tkilRequest.setProductModel(asset.getModelNo());
                break;
            }
        }

    }
}
