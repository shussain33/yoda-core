package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.softcell.constants.LosTvsInitiateConstants;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosTvsInitiateBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import org.springframework.stereotype.Service;

@Service
public class LosTvsInitiateBuilderImpl implements LosTvsInitiateBuilder{
    @Override
    public InitiateRequest buildLosTvsInitiateRequest(WFJobCommDomain lostvsconfig, TvsIntiateData tvsIntiateData) {

        return InitiateRequest.builder()
                .name(lostvsconfig.getMemberId())
                .key(lostvsconfig.getLicenseKey())
                .tvcInitiateData(tvsIntiateData)
                .build();
    }

    @Override
    public TVSGroupOneRequest buildLosTvsGroupOneRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneResponse) {
        return TVSGroupOneRequest.builder()
                .name(lostvsconfig.getMemberId())
                .key(lostvsconfig.getLicenseKey())
                .insertOrUpdateTvsRecordGroupOne(insertOrUpdateTvsRecordGroupOneResponse)
                .build();
    }

    @Override
    public TVSGroupTwoRequest buildLosTvsGroupTwoRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoResponse) {
        return TVSGroupTwoRequest.builder()
                .name(lostvsconfig.getMemberId())
                .key(lostvsconfig.getLicenseKey())
                .insertOrUpdateTvsRecordGroupTwo(insertOrUpdateTvsRecordGroupTwoResponse).build();

    }

    @Override
    public TVSGroupThreeRequest buildLosTvsGroupThreeRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordResponse) {
        return TVSGroupThreeRequest.builder()
                .name(lostvsconfig.getMemberId())
                .key(lostvsconfig.getLicenseKey())
                .insertOrUpdateTvsRecordForGroupThree(insertOrUpdateTvsRecordResponse)
                .build();
    }

    @Override
    public TVSGroupFourRequest buildLosTvsGroupFourRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupFour groupFourReturn) {
        return TVSGroupFourRequest.builder()
                .name(lostvsconfig.getMemberId())
                .key(lostvsconfig.getLicenseKey())
                .insertOrUpdateTvsRecordForGroupFour(groupFourReturn)
                .build();
    }
    @Override
    public TVSGroupFiveRequest buildLosTvsGroupFiveRequest(WFJobCommDomain lostvsconfig, InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveResponse) {
        return TVSGroupFiveRequest.builder()
                .name(lostvsconfig.getMemberId())
                .key(lostvsconfig.getLicenseKey())
                .insertOrUpdateTvsRecordGroupFive(insertOrUpdateTvsRecordGroupFiveResponse)
                .build();
    }

    @Override
    public TvsIntiateData buildLosTvsInitiateInputRequest(LosTvsRequest losTvsRequest) {
        return TvsIntiateData.builder()
                .stage(losTvsRequest.getStage())
                .vendorid(losTvsRequest.getVendorid())
                .prospectid(losTvsRequest.getRefID())
                .referenceno("")
                .build();
    }

    @Override
    public TvsIntiateData buildLosTvsInitiateInputRequestForCaseId(String caseId) {
        return TvsIntiateData.builder()
                .stage(LosTvsInitiateConstants.DISB)
                .vendorid(LosTvsInitiateConstants.SOFTCELL)
                .prospectid(caseId)
                .referenceno("")
                .build();
    }

}
