package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.serialnumbervalidation.voltas.ServiceCodeRegResponse;
import com.softcell.gonogo.serialnumbervalidation.voltas.VoltasRequest;
import com.softcell.gonogo.serialnumbervalidation.voltas.VoltasResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 *  created by prasenjit wadmare on 18/12/2017
 */


public interface VoltasRequestNumberValidationBuilder {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param voltasConfig
     * @return
     */
    VoltasRequest buildVoltasRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain voltasConfig);

    /**
     *
     * @param voltasResponse
     * @return
     */
    ServiceCodeRegResponse buildVoltasResponse(VoltasResponse voltasResponse);

    /**
     *
     * @param voltasResponse
     * @return
     */
    SerialNumberInfo.Builder buildVoltasServiceCodeLogs(VoltasResponse voltasResponse, SerialNumberInfo.Builder serialNumberInfoBuilder);




}
