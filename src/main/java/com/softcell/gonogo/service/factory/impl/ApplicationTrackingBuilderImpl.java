package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.StageConstants;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.service.factory.ApplicationTrackingBuilder;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Implements ApplicationTracking builder
 *
 * @author bhuvneshk
 */
@Component
public class ApplicationTrackingBuilderImpl implements ApplicationTrackingBuilder {

    public static void setStepId(ApplicationTracking applicationTracking,
                                 String moduleId) {
        switch (moduleId) {
            case "APRV":
                applicationTracking.setStepId("203");
                break;

            case "DCLN":
                applicationTracking.setStepId("204");
                break;
            case "CR_Q":
                applicationTracking.setStepId("205");
                break;
            case "PD_DE":
                applicationTracking.setStepId("206");
                break;
            case "DE":
                applicationTracking.setStepId("101");
                break;
            case "DE2":
                applicationTracking.setStepId("102");
                break;
            case "DE3":
                applicationTracking.setStepId("103");
                break;
            case "BRE":
                applicationTracking.setStepId("104");
                break;
            case "DO":
                applicationTracking.setStepId("301");
                break;
            case "LOS_DCLN":
                applicationTracking.setStepId("304");
                break;
            case "LOS_DISB":
                applicationTracking.setStepId("305");
                break;
            case "LOS_APRV":
                applicationTracking.setStepId("306");
                break;
            case "LOS_QDE":
                applicationTracking.setStepId("307");
                break;
            default:
                applicationTracking.setStepId("unknown");

        }

    }

    @Override
    public void buildAppTracking(ApplicationRequest applicationRequest,
                                 ApplicationTracking applicationTracking) {
        applicationTracking.setInstitutionId(applicationRequest.getHeader()
                .getInstitutionId());
        applicationTracking.setGngRefId(applicationRequest.getRefID());
        applicationTracking.setApplicationStatus("");
        applicationTracking.setCurrentStageId(applicationRequest
                .getCurrentStageId());

        if (applicationRequest.getHeader() != null) {
            applicationTracking.setDsaId(applicationRequest.getHeader()
                    .getDsaId());
        }
        pouplateDates(applicationTracking);
        customMessages(null, applicationTracking);
    }

    @Override
    public void buildAppTracking(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            ApplicationTracking applicationTracking) {
        buildAppTracking(goNoGoCustomerApplication.getApplicationRequest(),
                applicationTracking);
        setApplicationStatus(applicationTracking, goNoGoCustomerApplication);
        customMessages(null, applicationTracking);
    }

    @Override
    public void buildAppTracking(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            ModuleSetting moduleSetting, ApplicationTracking applicationTracking) {
        buildAppTracking(goNoGoCustomerApplication.getApplicationRequest(),
                applicationTracking);
        setApplicationStatus(applicationTracking, goNoGoCustomerApplication);
        applicationTracking.setModuleSetting(moduleSetting);
        customMessages(moduleSetting, applicationTracking);
    }

    ;

    @Override
    public void buildAppTracking(String status,
                                 ApplicationTracking applicationTracking) {
        applicationTracking.setCurrentStageId(status);
        pouplateDates(applicationTracking);
        customMessages(null, applicationTracking);
    }

    /**
     * It is use to convert system codes to readable messages
     *
     * @param moduleSetting
     * @param applicationTracking
     */
    public void customMessages(ModuleSetting moduleSetting,
                               ApplicationTracking applicationTracking) {
        if (moduleSetting != null) {
            String moduleId = moduleSetting.getModuleId();

            if (StageConstants.mapConstant.containsKey(moduleId)) {
                applicationTracking
                        .setNodeDescription(StageConstants.mapConstant
                                .get(moduleId));
                applicationTracking.setStepId(setworkStepId(moduleId));
            } else {
                applicationTracking.setNodeDescription(moduleId);
                applicationTracking.setStepId(setworkStepId(moduleId));
            }
        } else {
            String currnetStageId = applicationTracking.getCurrentStageId();
            if (StageConstants.mapConstant.containsKey(currnetStageId)) {
                applicationTracking
                        .setNodeDescription(StageConstants.mapConstant
                                .get(currnetStageId));
                setStepId(applicationTracking, currnetStageId);
            } else {
                applicationTracking.setNodeDescription(currnetStageId);
                setStepId(applicationTracking, currnetStageId);
            }
        }
    }

    private String setworkStepId(String moduleId) {
        switch (moduleId) {
            case "NegativeDedupe":
                return "601";

            case "multiBureauExecutor":
                return "602";

            case "panVarificationExecuter":
                return "603";
            case "scoringExecutor":
                return "604";

            case "verificationScoring":
                return "605";

            case "aadharExecutor":
                return "606";

            case "ntcExecutor":
                return "607";

            case "salesforceExecutor":
                return "608";
            case "amazonS3ServiceExecutor":
                return "609";
            default:
                return "699";
        }
    }

    private void pouplateDates(ApplicationTracking applicationTracking) {
        Date currentDate = new Date();
        if (applicationTracking.getStartDate() == null) {
            applicationTracking.setStartDate(currentDate);
        } else {
            applicationTracking.setEndDate(currentDate);
            long timeDiffrence = Math.abs((applicationTracking.getEndDate()
                    .getTime() - applicationTracking.getStartDate().getTime()));
            applicationTracking.setTimeDiffrence(timeDiffrence);
        }
    }

    /**
     * This method is to handle STA application status So that CRO can know
     * whether application is approved by system or CRO
     *
     * @param applicationTracking
     * @param goNoGoCustomerApplication
     */
    private void setApplicationStatus(ApplicationTracking applicationTracking,
                                      GoNoGoCustomerApplication goNoGoCustomerApplication) {

        String status = goNoGoCustomerApplication.getApplicationStatus();
        if (goNoGoCustomerApplication.getApplicantComponentResponse() != null && status != null) {

            ScoringResponse scoringResponse = goNoGoCustomerApplication
                    .getApplicantComponentResponse()
                    .getScoringServiceResponse();

            if (scoringResponse != null) {
                if (scoringResponse.getEligibilityResponse() != null || scoringResponse.getDecisionResponse() != null) {

                    if (StringUtils.equalsIgnoreCase(status.toUpperCase(),Status.APPROVED.toString())) {
                        status = "System Approved";
                        applicationTracking.setStepId("202");
                    } else if (StringUtils.equalsIgnoreCase(status.toUpperCase(), Status.DECLINED.toString())) {
                        status = "System Declined";
                        applicationTracking.setStepId("201");
                    }
                }
            }
        }

        applicationTracking.setApplicationStatus(status);

        applicationTracking.setCurrentStageId(status);

    }
}
