package com.softcell.gonogo.service.factory.impl;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rits.cloning.Cloner;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.master.MasterDataViewMongoRepository;
import com.softcell.gonogo.ModuleHelper;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import com.softcell.gonogo.model.core.cam.RTRTransaction;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.request.scoring.*;
import com.softcell.gonogo.model.core.request.scoring.Application;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationDetails;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.creditVidya.UserProfileResponse;
import com.softcell.gonogo.model.masters.AssetModelMaster;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.FinishedInprocessObject;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.experian.InProfileResponse;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.ntc.NTCResponse;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.saathi.SaathiResponse;
import com.softcell.gonogo.service.factory.ScoringJsonBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author kishorp This will use to build scoring service request. Its also set
 *         work-flow fields which will use for scoring purpose.
 */
@Service
public class ScoringJsonBuilderImpl implements ScoringJsonBuilder {

    private static final Logger logger = LoggerFactory.getLogger(ScoringJsonBuilderImpl.class);

    LookupService lookupService = new LookupServiceHandler();

    @Autowired
    ApplicationMongoRepository applicationMongoRepository;

    public ScoringJsonBuilderImpl(){
        if(null == applicationMongoRepository){
            applicationMongoRepository = new ApplicationMongoRepository();
        }
    }


    private static String getDateFromLong(long date, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return sdf.format(new Date(date));
        } catch (Exception e) {
            logger.info("ERROR MESSAGE  " + e.getMessage());
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    @Override
    public ScoringApplicationRequest buildScoringJSon(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception {

        ComponentResponse componentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();

        ResponseMultiJsonDomain multiJsonDomain = componentResponse.getMultiBureauJsonRespose();

        NTCResponse ntcResponse = componentResponse.getNtcResponse();

        ScoringApplicationRequest scoringApplicationRequest = new ScoringApplicationRequest();

        Cloner cloner = new Cloner();
        ApplicationRequest applicationRequest = cloner.deepClone(goNoGoCustomerApplication.getApplicationRequest());
        scoringApplicationRequest.setRequest(applicationRequest);

        /**
         * Below code block transforms the Product enum name to productName,
         * because currently policies are defined on loanType
         */
        if (null != scoringApplicationRequest.getRequest()
                && null != scoringApplicationRequest.getRequest().getRequest()
                && null != scoringApplicationRequest.getRequest().getRequest().getApplication()
                && StringUtils.isNotBlank(scoringApplicationRequest.getRequest().getRequest().getApplication().getLoanType())) {

            String loanType = scoringApplicationRequest.getRequest().getRequest().getApplication().getLoanType();
            String product = Product.getProductNameFromEnumName(loanType);
            scoringApplicationRequest.getRequest().getRequest().getApplication().setLoanType(product);
        }


        ScoringHeader header = new ScoringHeader();

        header.setApplicationId(goNoGoCustomerApplication.getGngRefId());

        header.setCustomerId(goNoGoCustomerApplication.getGngRefId());

        header.setInstitutionId(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());

        header.setAppType("01");

        header.setRequestTime(new Date().toString());

        scoringApplicationRequest.setHeader(header);

        if (null != multiJsonDomain) {

            List<Finished> individualBureauList = multiJsonDomain.getFinishedList();

            if (individualBureauList != null)
                for (Finished finished : individualBureauList) {
                    String bureauName = finished.getBureau();
                    switch (bureauName) {
                        case "CIBIL": {
                            Object cibilResponseObject = finished
                                    .getResponseJsonObject();
                            scoringApplicationRequest.setCibil(cibilResponseObject);
                        }
                        break;

                        case "EQUIFAX": {
                            Object equifaxResponseObject = finished
                                    .getResponseJsonObject();
                            scoringApplicationRequest.setEquifax(equifaxResponseObject);
                        }
                        break;
                        case "EXPERIAN": {

                            try {
                                InProfileResponse experianResponse = null;
                                String responseJsonObject = GngUtils.getBureauResponse(finished.getResponseJsonObject());

                                ObjectMapper objectMapper = new ObjectMapper();
                                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

                                experianResponse = objectMapper.readValue(responseJsonObject, InProfileResponse.class);


                                scoringApplicationRequest.setExperian(experianResponse);
                            } catch (Exception e) {
                                logger.error("Error parsing experian response in scoring executor cause {}", e.getMessage());
                            }

                        }
                        break;
                        case "HIGHMARK": {
                            Object highmarkResponseObject = finished
                                    .getResponseJsonObject();
                            scoringApplicationRequest
                                    .setHighmark(highmarkResponseObject);
                        }

                        break;
                        default:
                            break;
                    }
                }
        }

        // Get sathi and credit vidya responses
        setSaathiGetCdResponse(goNoGoCustomerApplication, scoringApplicationRequest);
        setCreditVidyaResponse(goNoGoCustomerApplication, scoringApplicationRequest);


        WorkflowFields workflowFields = new WorkflowFields();
        Date appSubmitDate = goNoGoCustomerApplication.getApplicationRequest()
                .getHeader().getDateTime();
        if (appSubmitDate != null) {
            workflowFields.setSubmitDate(getDateFromLong(
                    appSubmitDate.getTime(), "ddMMyyyy"));
        }
        if (goNoGoCustomerApplication.getDedupedApplications() != null) {
            workflowFields.setDedupeCustomerFlag("Y");
            if (goNoGoCustomerApplication.getDedupedApplications().isEmpty()) {
                workflowFields.setDedupeCustomerStatus("N");
            } else {
                workflowFields.setDedupeCustomerStatus("Y");
            }
        } else {
            workflowFields.setDedupeCustomerFlag("N");
            workflowFields.setDedupeCustomerStatus("N");
        }
        if (goNoGoCustomerApplication.isNegativePincode())
            workflowFields.setNegPincodeCheck("Y");
        else {
            workflowFields.setNegPincodeCheck("N");
        }
        if (ntcResponse != null) {
            workflowFields.setNtcScore(ntcResponse.getNtcScore());
        }

        //set dedupe application details
        setDedupeApplicationDetails(workflowFields, goNoGoCustomerApplication, scoringApplicationRequest);
        //set gonogo master fields
        setGngMasterField(goNoGoCustomerApplication, scoringApplicationRequest);

        setScoring(workflowFields, goNoGoCustomerApplication);
        scoringApplicationRequest.setWorkflowFields(workflowFields);
        return scoringApplicationRequest;
    }

    @Override
    public ScoringApplicantRequest buildApplicantScoringJSon(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception {

        ComponentResponse componentResponse;
        // Consumer bureau report
        ResponseMultiJsonDomain multiJsonDomain;
        // Commercial bureau report
        OutputAckDomain mbCorpJsonResponse;

        Cloner cloner = new Cloner();
        ApplicationRequest applicationRequest = cloner.deepClone(goNoGoCustomerApplication.getApplicationRequest());

        ScoringHeader header = buildHeader(goNoGoCustomerApplication);
        Application application = buildApplicationForRequest(applicationRequest);

        List<ApplicantRequest> applicantRequests = new ArrayList<>();
        ApplicantRequest applicantRequest;
        Applicant applicant;

        // Primary Applicant
        //Applicant is either consumer or commercial
        applicantRequest = populateApplicantRequest(goNoGoCustomerApplication, applicationRequest.getRequest().getApplicant().getApplicantId(),true);

        componentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();
        multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
        mbCorpJsonResponse = componentResponse.getMbCorpJsonResponse();
        setCibilResponse(multiJsonDomain , applicantRequest);
        //adding
        applicantRequests.add(applicantRequest);

        // For coapplicants
        List<ComponentResponse> componentResponseList = goNoGoCustomerApplication.getApplicantComponentResponseList();
        List<CoApplicant> coApplicants = applicationRequest.getRequest().getCoApplicant();

       // adding all co-applicants
        if (CollectionUtils.isNotEmpty(coApplicants)) {
            logger.info("Number of coApplicants " + coApplicants.size());
            for (CoApplicant coapplicant : coApplicants) {
                applicantRequest = populateApplicantRequest(goNoGoCustomerApplication, coapplicant.getApplicantId(),false);
                        new ApplicantRequest();
                applicantRequest.setRequest(coapplicant);

                List<ComponentResponse> coApplicantResponseList = componentResponseList.stream()
                        .filter(coAppResponse -> StringUtils.equalsIgnoreCase(coAppResponse.getApplicantId(),
                                coapplicant.getApplicantId())
                        ).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(coApplicantResponseList)) {
                    componentResponse = coApplicantResponseList.get(0);
                    if (componentResponse != null) {
                        //It is possible that Bureau report is not available for some of the coapplicants.

                        multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
                        mbCorpJsonResponse = componentResponse.getMbCorpJsonResponse();
                        setBureauResponse(multiJsonDomain, mbCorpJsonResponse, applicantRequest);
                        setCibilResponse(multiJsonDomain , applicantRequest);
                    }
                }
                applicantRequests.add(applicantRequest);
            }
        }

        return ScoringApplicantRequest.builder()
                .gngRefId(goNoGoCustomerApplication.getGngRefId())
                .header(header)
                .applicants(applicantRequests)
                .application(application)
                .build();
    }

    @Override
    public ScoringApplicantRequestV2 buildApplicantScoringJSonV2(GoNoGoCustomerApplication goNoGoCustomerApplication, ScoringApplicantRequestV2 scoringApplicantRequest) throws Exception {

        ComponentResponse componentResponse;
        // Consumer bureau report
        ResponseMultiJsonDomain multiJsonDomain;
        // Commercial bureau report
        OutputAckDomain mbCorpJsonResponse;

        // FIX for age ; sometimes age is not set , set now to avoid error
        ModuleHelper.fixAge(goNoGoCustomerApplication.getApplicationRequest());

        Cloner cloner = new Cloner();
        ApplicationRequest applicationRequest = cloner.deepClone(goNoGoCustomerApplication.getApplicationRequest());

        ScoringHeader header = buildHeader(goNoGoCustomerApplication);
        Application application = buildApplicationForRequest(applicationRequest);

        List<ApplicantRequest> applicantRequests = new ArrayList<>();
        ApplicantRequest applicantRequest;
        Applicant applicant;

        // Primary Applicant
        //Applicant is either consumer or commercial
        applicantRequest = populateApplicantRequest(goNoGoCustomerApplication, applicationRequest.getRequest().getApplicant().getApplicantId(),true);
        applicantRequest.setMergedResponse(null);
        componentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();
        multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
        mbCorpJsonResponse = componentResponse.getMbCorpJsonResponse();
        setCibilResponse(multiJsonDomain , applicantRequest);
        //adding
        applicantRequests.add(applicantRequest);

        // For coapplicants
        List<ComponentResponse> componentResponseList = goNoGoCustomerApplication.getApplicantComponentResponseList();
        List<CoApplicant> coApplicants = applicationRequest.getRequest().getCoApplicant();

        // adding all co-applicants
        if (CollectionUtils.isNotEmpty(coApplicants)) {
            logger.info("Number of coApplicants " + coApplicants.size());
            for (CoApplicant coapplicant : coApplicants) {
                applicantRequest = populateApplicantRequest(goNoGoCustomerApplication, coapplicant.getApplicantId(),false);
                new ApplicantRequest();
                applicantRequest.setRequest(coapplicant);
                if( CollectionUtils.isNotEmpty(componentResponseList) ) {
                    List<ComponentResponse> coApplicantResponseList = componentResponseList.stream()
                            .filter(coAppResponse -> StringUtils.equalsIgnoreCase(coAppResponse.getApplicantId(),
                                    coapplicant.getApplicantId())
                            ).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(coApplicantResponseList)) {
                        componentResponse = coApplicantResponseList.get(0);
                        if (componentResponse != null) {
                            //It is possible that Bureau report is not available for some of the coapplicants.

                            multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
                            mbCorpJsonResponse = componentResponse.getMbCorpJsonResponse();
                            setBureauResponse(multiJsonDomain, mbCorpJsonResponse, applicantRequest);
                            applicantRequest.setMergedResponse(null);
                            setCibilResponse(multiJsonDomain, applicantRequest);
                        }
                    }
                }
                applicantRequests.add(applicantRequest);
            }
        }
        CamDetails camDetails=scoringApplicantRequest.getCamDetails();
        Valuation valuation=scoringApplicantRequest.getValuation();
        FinancialData financialData = getFinancialDetails(applicationRequest, camDetails, valuation);
        VerificationDetails verificationRequest=scoringApplicantRequest.getVerification();
        DedupeMatch dedupe=scoringApplicantRequest.getDedupeMatch();
        LoanCharges loanCharges=scoringApplicantRequest.getLoanCharges();
        EligibilityDetails eligibility=scoringApplicantRequest.getEligibility();
        return ScoringApplicantRequestV2.builder()
                .gngRefId(goNoGoCustomerApplication.getGngRefId())
                .header(header)
                .applicants(applicantRequests)
                .application(application)
                .finData(financialData)
                .camDetails(camDetails)
                .valuation(valuation)
                .verification(verificationRequest)
                .dedupeMatch(dedupe)
                .loanCharges(loanCharges)
                .eligibility(eligibility)
                .build();
    }

    private FinancialData getFinancialDetails(ApplicationRequest applicationRequest, CamDetails camDetails, Valuation valuation) throws Exception {

        FinancialData financialData = null;
        try {
            if (camDetails != null) {
                GoNoGoCustomerApplication gngAppObj = applicationMongoRepository.getGngApplication(applicationRequest.getRefID());
                // ROI, //inward and out chqreturn, //emi bounce
                financialData = new FinancialData();
                if (CollectionUtils.isNotEmpty(camDetails.getBankingStatements())) {
                    financialData.setBankingStatements(camDetails.getBankingStatements());
                }
                if (CollectionUtils.isNotEmpty(camDetails.getRtrDetailList())) {
                    financialData.setRtrDetailList(camDetails.getRtrDetailList());
                }
                if (CollectionUtils.isNotEmpty(camDetails.getPlAndBSAnalysis())) {
                    financialData.setPlAndBSAnalysis(camDetails.getPlAndBSAnalysis());
                }
                if (camDetails.getSummary() != null) {
                    CamSummary camSummary = camDetails.getSummary();
                    // ROI
                    financialData.setRoiOffered(Double.parseDouble(
                            camSummary.getRoiOffered() != null ?
                                    camDetails.getSummary().getRoiOffered().trim() : "0"));
                    if (Institute.isInstitute(applicationRequest.getHeader().getInstitutionId(), Institute.AMBIT)) {
                        String caseLoginDate = GngDateUtil.convertDateIntoStringFormat(gngAppObj.getIntrimStatus().getStartTime());
                        camSummary.setCaseloginDate(caseLoginDate);
                        financialData.setSummary(camSummary);
                    }

                }

                if (Institute.isInstitute(applicationRequest.getHeader().getInstitutionId(), Institute.AMBIT)) {
                    if (camDetails.getGstDetails() != null) {
                        List<GstDetailsList> gstDetails = camDetails.getGstDetails();
                        financialData.setGstDetails(gstDetails);
                    }
                }

                // Inward, outward cheqs
                if (CollectionUtils.isNotEmpty(camDetails.getBankingStatements())) {
                    int inwardCount = 0, outwardCount = 0;
                    for (BankingStatement stmt : camDetails.getBankingStatements()) {
                        inwardCount += stmt.getTotalNoOfInwardTransaction();
                        outwardCount += stmt.getTotalNoOfOutwardTransaction();
                    }
                    financialData.setInwardCheques(inwardCount);
                    financialData.setOutwardCheques(outwardCount);
                }
                // Emi bounces
                if (CollectionUtils.isNotEmpty(camDetails.getRtrDetailList())) {
                    int bounceCount = 0;
                    for (RTRDetail rtr : camDetails.getRtrDetailList()) {
                        if (CollectionUtils.isNotEmpty(rtr.getRtrTransactions())) {
                            for (RTRTransaction rtrTransaction : rtr.getRtrTransactions()) {
                                bounceCount += rtrTransaction.getNumberOfBounces();
                            }
                        }
                    }
                }
                // PnL Analysis related fields
                if (CollectionUtils.isNotEmpty(camDetails.getPlAndBSAnalysis())) {
                    PLAndBSAnalysis plAndBSAnalysis = camDetails.getPlAndBSAnalysis().get(0);
                    if (plAndBSAnalysis != null) {
                        // From ProfitAndLoss
                        if (plAndBSAnalysis.getProfitAndLoss() != null) {
                            // - Gross YearData
                            if (plAndBSAnalysis.getProfitAndLoss().getGrossProfits() != null) {
                                financialData.setGrossProft(fillYearsData(plAndBSAnalysis.getProfitAndLoss().getGrossProfits()));
                            }
                            // - Cash YearData
                            if (plAndBSAnalysis.getProfitAndLoss().getPostTaxCashProfitsWithSalAndPartnerInterest() != null) {
                                financialData.setCashProft(fillYearsData(plAndBSAnalysis.getProfitAndLoss()
                                        .getPostTaxCashProfitsWithSalAndPartnerInterest()));
                            }
                            // Set YearData before Tax, Tax Paid, Sales, Other Income
                            setFromPnL(plAndBSAnalysis.getProfitAndLoss(), financialData);
                        }

                        // From BalanceSheet
                        setFromBalanceSheet(plAndBSAnalysis.getBalanceSheet(), financialData);
                    } // PnL
                }
            } // Camdetails

            //setting fields from valuation.
            if (valuation != null) {
                List<DoubleValue> collateralValues = new ArrayList<>();
                List<StringValue> collateralOwnership = new ArrayList<>();
                StringValue stringValue;
                List<Collateral> collateralList = applicationRequest.getRequest().getApplication().getCollateral();
                if (CollectionUtils.isNotEmpty(valuation.getValuationDetailsList())) {
                    List<ValuationDetails> valuationDetailsList = valuation.getValuationDetailsList();
                    DoubleValue doubleValue;
                    for (ValuationDetails valuationDtls : valuationDetailsList) {
                        List<String> ownershipType = collateralList.stream().filter(col -> StringUtils.equalsIgnoreCase(col.getCollateralId(),
                                valuationDtls.getCollateralId()))
                                .map(col -> col.getOwnerShip())
                                .collect(Collectors.toList());
                        if (CollectionUtils.isNotEmpty(ownershipType)) {
                            stringValue = new StringValue(ownershipType.get(0));
                            collateralOwnership.add(stringValue);
                        }
                        if (valuationDtls.getValuationOutput() != null) {
                            doubleValue = new DoubleValue();
                            doubleValue.setValue(Double.parseDouble(valuationDtls.getValuationOutput().getCurrentMarketValue() != null ?
                                    valuationDtls.getValuationOutput().getCurrentMarketValue().trim() : "0")
                            );
                            collateralValues.add(doubleValue);
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(collateralValues)) {
                    financialData.setCollateralValues(collateralValues);
                }
                if (CollectionUtils.isNotEmpty(collateralOwnership)) {
                    financialData.setCollateralOwnershipType(collateralOwnership);
                }
            }
            if (Institute.isInstitute(camDetails.getInstitutionId(), Institute.AMBIT)) {
                for (RTRDetail RTR : camDetails.getRtrDetailList()) {
                    RTR.getRtrTransactions().stream()
                            .map(transaction -> {
                                String disburseDate = GngDateUtil.convertDateIntoStringFormat(transaction.getDisburseDate());
                                String emiStartDate = GngDateUtil.convertDateIntoStringFormat(transaction.getSanctionOrEmiStartDate());
                                transaction.setDisbursalDate(disburseDate);
                                transaction.setEmiStartDate(emiStartDate);
                                return transaction;
                            }).collect(Collectors.toList());
                }
                financialData.setRtrDetailList(camDetails.getRtrDetailList());
            }
        }
        catch (Exception e){
            logger.error("{} error occured at the time of setting Financial Data {}", applicationRequest.getRefID(), ExceptionUtils.getStackTrace(e));
        }
        return financialData;
    }

    private Application buildApplicationForRequest(ApplicationRequest applicationRequest) {
        Application application = new Application();
        application.setLoanTenorInMonths(applicationRequest.getRequest().getApplication().getLoanTenor());
        application.setAggregation("MAX");
        application.setLoanType(applicationRequest.getRequest().getApplication().getLoanType());
        application.setLoanAmt(applicationRequest.getRequest().getApplication().getLoanAmount());
        application.setPriority("QAD");

        if(applicationRequest.getAppMetaData() != null){
            application.setBranchName(applicationRequest.getAppMetaData().getBranchV2().getBranchName());
        }
        String promoCode = applicationRequest.getRequest().getApplication().getPromoCode() != null ?
                applicationRequest.getRequest().getApplication().getPromoCode() :  "" ;
        application.setPromoCode(promoCode);
        if(Institute.isInstitute(applicationRequest.getHeader().getInstitutionId(), Institute.AMBIT))
            application.setProfessionIncomeDetails(applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails());

        if(applicationRequest.getHeader().getProduct()== Product.PL){
            if(org.apache.commons.lang3.StringUtils.isNotEmpty(applicationRequest.getRequest().getApplication().getCity())){
                logger.info("{} PL product City {}",applicationRequest.getRefID(), applicationRequest.getRequest().getApplication().getCity());
                application.setCity(applicationRequest.getRequest().getApplication().getCity().toUpperCase());
            }else{
                logger.info("{} PL product City Not Found",applicationRequest.getRefID());
                application.setCity(Constant.NULL);
            }
        }

        return application;
    }

    private ScoringHeader buildHeader(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        ScoringHeader header = new ScoringHeader();
        header.setInstitutionId(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
        header.setApplicationId(goNoGoCustomerApplication.getGngRefId());
        header.setClientId(goNoGoCustomerApplication.getGngRefId());
        header.setRequestTime(new Date().toString());
        header.setAppType("01");
        header.setRequestType("REQUEST");
        return header;
    }

    private void setBureauResponse(ResponseMultiJsonDomain multiJsonDomain, OutputAckDomain mbCorpJsonResponse, ApplicantRequest applicantRequest) {
        if (null != multiJsonDomain) {
            if (multiJsonDomain.getMergedResponse() != null) {
                applicantRequest.setMergedResponse(multiJsonDomain.getMergedResponse());
            }
            //setCibilResponse(multiJsonDomain, applicantRequest);
        } else if (mbCorpJsonResponse != null) {
            if (mbCorpJsonResponse.getMergedResponse() != null) {
                applicantRequest.setMergedResponse(mbCorpJsonResponse.getMergedResponse());
            }
            //setCibilResponse(mbCorpJsonResponse, applicantRequest);
        }
    }

    private void setCibilResponse(ResponseMultiJsonDomain multiJsonDomain, ApplicantRequest applicantRequest) {
        if( multiJsonDomain != null) {
            List<Finished> individualBureauList = multiJsonDomain.getFinishedList();
            if (CollectionUtils.isNotEmpty(individualBureauList)) {
                for (Finished finished : individualBureauList) {
                    String bureauName = finished.getBureau();
                    switch (bureauName) {
                        case "CIBIL": {
                            Object cibilResponseObject = finished
                                    .getResponseJsonObject();
                            applicantRequest.setCibilRespopnse(cibilResponseObject);
                        }
                        break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    private void setCibilResponse(OutputAckDomain multiJsonDomain, ApplicantRequest applicantRequest) {
        List<FinishedInprocessObject> individualBureauList = multiJsonDomain.getFinishedList();
        if (individualBureauList != null) {
            for (FinishedInprocessObject finished : individualBureauList) {
                String bureauName = finished.getBureauName();
                switch (bureauName) {
                    case "CIBIL": {
                        Object cibilResponseObject = finished.getJsonObject();
                        applicantRequest.setCibilRespopnse(cibilResponseObject);
                    }
                    break;
                    default:
                        break;
                }
            }
        }
    }

    private void setGngMasterField(GoNoGoCustomerApplication goNoGoCustomerApplication, ScoringApplicationRequest scoringApplicationRequest) throws Exception {

        MasterDataViewMongoRepository masterDataViewMongoRepository = new MasterDataViewMongoRepository(MongoConfig.getMongoTemplate());
        ApplicationMongoRepository applicationMongoRepository = new ApplicationMongoRepository();

        String institutionId = null;
        String assetCatId = null;
        String productName = null;
        GngMasterFields gngMasterFields = null;

        if (null != goNoGoCustomerApplication.getApplicationRequest()) {

            ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

            gngMasterFields = new GngMasterFields();

            institutionId = applicationRequest.getHeader().getInstitutionId();
            productName = applicationRequest.getHeader().getProduct().name();

            // get dealerEmailMaster  based on institutionId and dealerId
            DealerEmailMaster dealerEmailMaster = applicationMongoRepository.getDealerEmailMaster(applicationRequest.getHeader());

            if (null != dealerEmailMaster) {
                // set dealer state id in gngMasterFields
                gngMasterFields.setDealerStateId(dealerEmailMaster.getStateId());
                gngMasterFields.setDealerCityId(dealerEmailMaster.getCityId());
                logger.debug("State Id {} and city Id {} added scoringRequest successfully against the dealer {}", dealerEmailMaster.getStateId(),
                        dealerEmailMaster.getCityId(), dealerEmailMaster.getDealerID());
            }

            if (null != applicationRequest.getRequest()
                    && null != applicationRequest.getRequest().getApplication()
                    && !CollectionUtils.isEmpty(applicationRequest.getRequest().getApplication().getAsset())) {

                AssetDetails assetDetails = applicationRequest.getRequest().getApplication().getAsset().get(0);

                // get assetCatId from asset category master  based on institutionId and assetCategory
                assetCatId = masterDataViewMongoRepository.getAssetCatIdFromAssetCatMaster(institutionId, assetDetails.getAssetCtg());

                // set asset category id in gngMasterFields
                if (StringUtils.isNotBlank(assetCatId)) {
                    gngMasterFields.setAssetCatId(assetCatId);
                    logger.debug("Asset category Id {} added in the scoringRequest successfully ", assetCatId);
                }

                String productAliasName = lookupService.getAliasNameByInstitutionIdAndProductName(institutionId, productName);

                // get manufacturer id from asset model master
                AssetModelMaster assetModelMaster = masterDataViewMongoRepository.getAssetModelMaster(assetDetails.getModelNo(), assetDetails.getAssetCtg(), assetDetails.getAssetMake(), assetDetails.getAssetModelMake(), productAliasName, institutionId);

                if (null != assetModelMaster) {
                    gngMasterFields.setAssetManctrId(assetModelMaster.getManufacturerId());
                    logger.debug("Asset manufacturer Id {} added in the scoringRequest successfully ", assetModelMaster.getManufacturerId());
                }

            }
        }

        // Set gng master fields in scoring applicationRequest
        scoringApplicationRequest.setGngMasterFields(gngMasterFields);
    }


    /**
     * @param workflowFields
     * @param goNoGoCustomerApplication
     */
    private void setScoring(WorkflowFields workflowFields,
                            GoNoGoCustomerApplication goNoGoCustomerApplication) {

        if (goNoGoCustomerApplication.getApplicationRequest().getRequest()
                .getApplicant().isMobileVerified())
            workflowFields.setOtpStatus("Y");
        else {
            workflowFields.setOtpStatus("N");
        }
        if (goNoGoCustomerApplication.getIntrimStatus().getPanModuleResult() != null) {
            if (Status.ERROR.toString().equals(
                    goNoGoCustomerApplication.getIntrimStatus().getPanStatus())) {
                workflowFields.setPanVerifyStatus(goNoGoCustomerApplication
                        .getIntrimStatus().getPanModuleResult().getMessage());
                workflowFields.setPanNameVerifyScore(goNoGoCustomerApplication
                        .getIntrimStatus().getPanModuleResult().getNameScore());
                workflowFields.setPanGoNoGoVerifyStatus("N");
            } else {
                workflowFields.setPanVerifyStatus(goNoGoCustomerApplication
                        .getIntrimStatus().getPanModuleResult().getMessage());
                workflowFields.setPanNameVerifyScore(goNoGoCustomerApplication
                        .getIntrimStatus().getPanModuleResult().getNameScore());
            }
        } else {
            workflowFields.setPanVerifyStatus("PAN_NOT_SUBMITED");
            workflowFields.setPanNameVerifyScore("0.0");
            workflowFields.setPanGoNoGoVerifyStatus("N");
        }
        /**
         * Set Aadhar response in Scoring request
         */
        setAadharStatus(workflowFields, goNoGoCustomerApplication);

        /**
         * end Aadhar response in Scoring request
         */

        if (goNoGoCustomerApplication.getIntrimStatus()
                .getResidenceAddressResult() != null) {
            workflowFields.setResiVerifyScore(goNoGoCustomerApplication
                    .getIntrimStatus().getResidenceAddressResult()
                    .getFieldValue());
            workflowFields
                    .setResiVerifyScoreStatus(goNoGoCustomerApplication
                            .getIntrimStatus().getResidenceAddressResult()
                            .getMessage());
            workflowFields
                    .setResiVerifyScoreStability(goNoGoCustomerApplication
                            .getIntrimStatus().getResidenceAddressResult()
                            .getAddStability());
        }

        if (goNoGoCustomerApplication.getIntrimStatus().getOfficeModuleResult() != null) {
            workflowFields.setAddrVerifyScore(goNoGoCustomerApplication
                    .getIntrimStatus().getOfficeModuleResult().getFieldValue());
            workflowFields.setAddrVerifyScoreStatus(goNoGoCustomerApplication
                    .getIntrimStatus().getOfficeModuleResult().getMessage());
            workflowFields
                    .setAddrVerifyScoreStability(goNoGoCustomerApplication
                            .getIntrimStatus().getOfficeModuleResult()
                            .getAddStability());
        }
    }

    private void setAadharStatus(WorkflowFields workflowFields,
                                 GoNoGoCustomerApplication goNoGoCustomerApplication) {


        if (null != goNoGoCustomerApplication.getApplicationRequest()
                && null != goNoGoCustomerApplication.getApplicationRequest().getRequest()
                && null != goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()) {

            Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();

            workflowFields.setAadhaarVerifyStatus(String.valueOf(applicant.isAadhaarVerified()));
        }


        if (goNoGoCustomerApplication.getIntrimStatus().getAadharModuleResult() != null) {
            workflowFields
                    .setAadhaarGoNoGoVerifyStatus(goNoGoCustomerApplication
                            .getIntrimStatus().getAadharModuleResult()
                            .getFieldValue());
            logger.debug("setting aadhar status in workflow fields for scoring");
        } else {
            workflowFields.setAadhaarVerifyStatus("AADHAR_NOT_SUBMITED");
            workflowFields.setAadhaarGoNoGoVerifyStatus("n");
            logger.error("setting aadhar status in workflow fields for scoring");
        }
    }

    @Override
    public List<ScoringApplicationRequest> buildScoringJsonForCoApplicant(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {

        List<ComponentResponse> componentResponseList = goNoGoCustomerApplication
                .getApplicantComponentResponseList();
        List<CoApplicant> coApplicants = goNoGoCustomerApplication
                .getApplicationRequest().getRequest().getCoApplicant();

        ScoringHeader header = new ScoringHeader();
        header.setApplicationId(goNoGoCustomerApplication.getGngRefId());
        header.setCustomerId(goNoGoCustomerApplication.getGngRefId());
        header.setInstitutionId(goNoGoCustomerApplication
                .getApplicationRequest().getHeader().getInstitutionId());
        header.setAppType("01");
        header.setRequestTime(new Date().toString());

        List<ScoringApplicationRequest> scoringApplicationRequestsList = new ArrayList<ScoringApplicationRequest>();

        if (!CollectionUtils.isEmpty(coApplicants)) {
            for (int coApplicantIndex = 0; coApplicantIndex < coApplicants.size(); coApplicantIndex++) {

                ResponseMultiJsonDomain multiJsonDomain = null;
                NTCResponse ntcResponse = null;
                if (null != componentResponseList) {
                    ComponentResponse componentResponse = componentResponseList.get(coApplicantIndex);
                    if (null != componentResponse) {
                        multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
                        ntcResponse = componentResponse.getNtcResponse();
                    }
                }

                ScoringApplicationRequest scoringApplicationRequest = new ScoringApplicationRequest();
                scoringApplicationRequest.setHeader(header);

                Applicant applicant = coApplicants.get(coApplicantIndex);


                Cloner cloner = new Cloner();
                ApplicationRequest coApplicantrequest = cloner.deepClone(goNoGoCustomerApplication.getApplicationRequest());

                coApplicantrequest.getRequest().setApplicant(applicant);
                scoringApplicationRequest.setRequest(coApplicantrequest);


                /**
                 * Below code block transforms the Product enum name to productName,
                 * because currently policies are defined on loanType
                 */
                if (null != scoringApplicationRequest.getRequest()
                        && null != scoringApplicationRequest.getRequest().getRequest()
                        && null != scoringApplicationRequest.getRequest().getRequest().getApplication()
                        && StringUtils.isNotBlank(scoringApplicationRequest.getRequest().getRequest().getApplication().getLoanType())) {

                    String loanType = scoringApplicationRequest.getRequest().getRequest().getApplication().getLoanType();
                    String product = Product.getProductNameFromEnumName(loanType);
                    scoringApplicationRequest.getRequest().getRequest().getApplication().setLoanType(product);
                }

                if (null != multiJsonDomain) {

                    List<Finished> individualBureauList = multiJsonDomain
                            .getFinishedList();
                    if (individualBureauList != null)
                        for (Finished finished : individualBureauList) {
                            String bureauName = finished.getBureau();
                            switch (bureauName) {
                                case "CIBIL": {
                                    Object cibilResponseObject = finished
                                            .getResponseJsonObject();
                                    scoringApplicationRequest.setCibil(cibilResponseObject);
                                }
                                break;

                                case "EQUIFAX": {
                                    Object equifaxResponseObject = finished
                                            .getResponseJsonObject();
                                    scoringApplicationRequest
                                            .setEquifax(equifaxResponseObject);
                                }
                                break;
                                case "EXPERIAN": {
                                    Object experianResponseObject = finished
                                            .getResponseJsonObject();
                                    scoringApplicationRequest
                                            .setExperian(experianResponseObject);
                                }
                                break;
                                case "HIGHMARK": {
                                    Object highmarkResponseObject = finished
                                            .getResponseJsonObject();
                                    scoringApplicationRequest
                                            .setHighmark(highmarkResponseObject);
                                }

                                break;
                                default:
                                    break;
                            }
                        }
                }

                WorkflowFields workflowFields = new WorkflowFields();
                Date appSubmitDate = goNoGoCustomerApplication
                        .getApplicationRequest().getHeader().getDateTime();

                if (appSubmitDate != null) {
                    workflowFields.setSubmitDate(getDateFromLong(
                            appSubmitDate.getTime(), "ddMMyyyy"));
                }
                workflowFields.setAadhaarVerifyStatus(goNoGoCustomerApplication
                        .getIntrimStatus().getAadharStatus());

                if (goNoGoCustomerApplication.getDedupedApplications() != null) {
                    workflowFields.setDedupeCustomerFlag("Y");
                    if (goNoGoCustomerApplication.getDedupedApplications()
                            .isEmpty()) {
                        workflowFields.setDedupeCustomerStatus("N");
                    } else {
                        workflowFields.setDedupeCustomerStatus("Y");
                    }
                } else {
                    workflowFields.setDedupeCustomerFlag("N");
                    workflowFields.setDedupeCustomerStatus("N");
                }
                if (goNoGoCustomerApplication.isNegativePincode())
                    workflowFields.setNegPincodeCheck("Y");
                else {
                    workflowFields.setNegPincodeCheck("N");
                }
                if (ntcResponse != null) {
                    workflowFields.setNtcScore(ntcResponse.getNtcScore());
                }
                setScoring(workflowFields, goNoGoCustomerApplication);
                scoringApplicationRequest.setWorkflowFields(workflowFields);
                scoringApplicationRequestsList.add(scoringApplicationRequest);
            }
        }
        return scoringApplicationRequestsList;
    }

    private void setCreditVidyaResponse(GoNoGoCustomerApplication goNoGoCustomerApplication,
                                        ScoringApplicationRequest scoringApplicationRequest) {
        UserProfileResponse response = goNoGoCustomerApplication.getApplicantComponentResponse().getCreditVidyaResponse();
        if (response != null && response.getPersonalDetails() != null) {
            Cloner cloner = new Cloner();
            UserProfileResponse clonedResponse = cloner.deepClone(response);
            String dateTime = clonedResponse.getPersonalDetails().getMetadata().getInstallTime();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(dateTime)) {
                clonedResponse.getPersonalDetails().getMetadata().setInstallTime(
                        GngDateUtil.changeDateformatWithtimezoneOffset(dateTime,
                                GngDateUtil.ddMMyyyy));
            }

            dateTime = clonedResponse.getPersonalDetails().getMetadata().getRequestTime();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(dateTime)) {
                clonedResponse.getPersonalDetails().getMetadata().setRequestTime(
                        GngDateUtil.changeDateformatWithtimezoneOffset(dateTime,
                                GngDateUtil.ddMMyyyy));
            }

            dateTime = clonedResponse.getPersonalDetails().getMetadata().getLastPingTime();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(dateTime)) {
                clonedResponse.getPersonalDetails().getMetadata().setLastPingTime(
                        GngDateUtil.changeDateformatWithtimezoneOffset(dateTime,
                                GngDateUtil.ddMMyyyy));
            }
            scoringApplicationRequest.setCreditVidyaResponse(clonedResponse);
        }
    }

    private void setSaathiGetCdResponse(GoNoGoCustomerApplication goNoGoCustomerApplication,
                                        ScoringApplicationRequest scoringApplicationRequest) {
        // Get Saathi response for mobile number + refId
        SaathiResponse response = goNoGoCustomerApplication.getApplicantComponentResponse().getSaathiResponse();
        if (response != null && response.getCasedetails() != null) {
            Cloner cloner = new Cloner();
            SaathiResponse clonedResponse = cloner.deepClone(response);

            String datetime = clonedResponse.getCasedetails().getCreatedDate();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(datetime)) {
                clonedResponse.getCasedetails().setCreatedDate(
                        GngDateUtil.changeDateFormat(
                                GngDateUtil.getDateFromSpecificFormat(GngDateUtil.dd_MM_yyyy_hh_mm_a, datetime),
                                GngDateUtil.ddMMyyyy));
            }

            datetime = clonedResponse.getCasedetails().getUpdatedDate();
            if (org.apache.commons.lang3.StringUtils.isNotBlank(datetime)) {
                clonedResponse.getCasedetails().setUpdatedDate(
                        GngDateUtil.changeDateFormat(
                                GngDateUtil.getDateFromSpecificFormat(GngDateUtil.dd_MM_yyyy_hh_mm_a, datetime),
                                GngDateUtil.ddMMyyyy));
            }
            scoringApplicationRequest.setSaathiResponse(clonedResponse);
        }
    }

    private void setDedupeApplicationDetails(WorkflowFields workflowFields, GoNoGoCustomerApplication goNoGoCustomerApplication, ScoringApplicationRequest scoringApplicationRequest) {

        if (null != goNoGoCustomerApplication.getApplicantComponentResponse() &&
                null != goNoGoCustomerApplication.getApplicantComponentResponse().getPosidexResponse() &&
                null != goNoGoCustomerApplication.getApplicantComponentResponse().getPosidexResponse().getDedupeEnquiryStatusResponse() &&
                !CollectionUtils.isEmpty(goNoGoCustomerApplication.getApplicantComponentResponse().getPosidexResponse().getDedupeEnquiryStatusResponse().getMatchDetails())) {
            workflowFields.setPosidexDedupeStatus(Status.DEDUPE_FOUND.name());
        } else {

            workflowFields.setPosidexDedupeStatus(Status.NOT_FOUND.name());
        }

        ApplicationStatusLogRequest applicationStatusLogRequest = null;

        FinalApplicationMongoRepository finalApplicationMongoRepository = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());


        try {
            boolean actionFlag = lookupService.checkActionsAccess(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId(),
                    ActionName.SEND_DEDUPE_INFO_TO_SOBRE);

            //check action configuration i.e SEND_DEDUPE_INFO_TO_SOBRE
            if (actionFlag) {

                //build the request to call dedupeApplicationDetails Service
                if (null != goNoGoCustomerApplication.getGngRefId() && null != goNoGoCustomerApplication.getApplicationRequest()
                        && null != goNoGoCustomerApplication.getApplicationRequest().getHeader()) {

                    applicationStatusLogRequest = new ApplicationStatusLogRequest();

                    applicationStatusLogRequest.setRefId(goNoGoCustomerApplication.getGngRefId());
                    applicationStatusLogRequest.setHeader(goNoGoCustomerApplication.getApplicationRequest().getHeader());
                }

                if (null != applicationStatusLogRequest) {

                    BaseResponse dedupeApplicationDetails = finalApplicationMongoRepository.getDedupeApplicationDetails(applicationStatusLogRequest, true);

                    if (null != dedupeApplicationDetails
                            && StringUtils.equals(dedupeApplicationDetails.getStatus().getStatusValue(), Status.OK.name())
                            && null != dedupeApplicationDetails.getPayload()) {

                        List<DedupeApplicationDetails> dedupeApplicationDetailsList = (List<DedupeApplicationDetails>) dedupeApplicationDetails.getPayload().getT();
                        scoringApplicationRequest.setDedupeApplicationDetails(dedupeApplicationDetailsList);

                        logger.debug("Dedupe Application Details Successfully Stored in scoringApplicationRequest Object");
                    }
                } else {
                    logger.error("error occur at the time building GetDedupeApplicationDetails Request");
                }
            }


        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("error occur at the time of getting DedupeApplication Details with probable cause {}", e.getMessage());
        }

    }

    public ScoringApplicantRequest buildApplicantRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, String applicantId, boolean primary) {
        ScoringApplicantRequest scoringApplicantRequest = new ScoringApplicantRequest();
        scoringApplicantRequest.setGngRefId(goNoGoCustomerApplication.getGngRefId());
        ScoringHeader header = buildHeader(goNoGoCustomerApplication);
        scoringApplicantRequest.setHeader(header);
        Application application = buildApplicationForRequest(goNoGoCustomerApplication.getApplicationRequest());
        scoringApplicantRequest.setApplication(application);

        List<ApplicantRequest> applicantRequests = new ArrayList<>();
        applicantRequests.add(populateApplicantRequest(goNoGoCustomerApplication, applicantId, primary));



        scoringApplicantRequest.setApplicants(applicantRequests);
        return scoringApplicantRequest;
    }

    private ApplicantRequest populateApplicantRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, String applicantId, boolean primary) {
        ComponentResponse componentResponse;
        Cloner cloner = new Cloner();
        ApplicationRequest applicationRequest = cloner.deepClone(goNoGoCustomerApplication.getApplicationRequest());
        ApplicantRequest applicantRequest = new ApplicantRequest();

        if (primary) {
            Applicant applicant = applicationRequest.getRequest().getApplicant();
            componentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();
            applicantRequest.setRequest(applicant);
            setBureauResponse(componentResponse, applicantRequest);
        } else {
            List<ComponentResponse> componentResponseList = goNoGoCustomerApplication.getApplicantComponentResponseList();
            List<CoApplicant> coApplicants = applicationRequest.getRequest().getCoApplicant()
                                            .stream()
                                            .filter(coApp -> StringUtils.equalsIgnoreCase(coApp.getApplicantId(), applicantId))
                                            .collect(Collectors.toList());
            if( CollectionUtils.isNotEmpty(coApplicants) ) {
                CoApplicant  coapplicant = coApplicants.get(0);
                applicantRequest.setRequest(coapplicant);
                if( CollectionUtils.isNotEmpty(componentResponseList) ) {
                    List<ComponentResponse> coApplicantResponseList = componentResponseList.stream()
                            .filter(coAppResponse -> StringUtils.equalsIgnoreCase(coAppResponse.getApplicantId(),
                                    coapplicant.getApplicantId())
                            ).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(coApplicantResponseList)) {
                        componentResponse = coApplicantResponseList.get(0);
                        if (componentResponse != null) {
                            setBureauResponse(componentResponse, applicantRequest);
                        }
                    }
                }
            }
        }
        return applicantRequest;
    }

    private void setBureauResponse(ComponentResponse componentResponse, ApplicantRequest applicantRequest) {
        // Consumer bureau report
        ResponseMultiJsonDomain multiJsonDomain;
        // Commercial bureau report
        OutputAckDomain mbCorpJsonResponse;
        // It is possible that Bureau report is not available for some of the coapplicants.
        multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
        mbCorpJsonResponse = componentResponse.getMbCorpJsonResponse();
        setBureauResponse(multiJsonDomain, mbCorpJsonResponse, applicantRequest);
    }

    private YearData fillYearsData(StatementFieldDetails statementFieldDetails) {
        YearData yearData = new YearData();
        List<YearValue> yearValueList = statementFieldDetails.getYears();
        if( CollectionUtils.isNotEmpty(yearValueList)) {
            int size = yearValueList.size();
            // The yearlist is in chronological order.
            for( int i = size-1; i >= 0; i--) {
                if( i == size -1 ) { // last element
                    yearData.setCurrentYearValue(yearValueList.get(i).getValue());
                } else if( i == size -2) {
                    yearData.setPreviousYearValue(yearValueList.get(i).getValue());
                } else if( i == size -3) {
                    yearData.setCurrentMinusTwoYearValue(yearValueList.get(i).getValue());
                }else if( i == size -4) {
                    yearData.setCurrentMinusThreeYearValue(yearValueList.get(i).getValue());
                }else if( i == size -5) {
                    yearData.setCurrentMinusFourYearValue(yearValueList.get(i).getValue());
                } else {
                    break;
                }
            }
        }
        return yearData;
    }

    private void setFromPnL(ProfitAndLoss profitAndLoss, FinancialData financialData) {
        if( profitAndLoss != null ) {
            YearData values ;
            DoubleValue doubleValue;
            int size;
            // YearData before Tax
            if( profitAndLoss.getPBT() != null
                    && CollectionUtils.isNotEmpty(profitAndLoss.getPBT().getYears())) {
                values = fillYearsData(profitAndLoss.getPBT());
                financialData.setProfitBeforeTax(values);
            }
            // Tax Paid
            if( profitAndLoss.getTaxPaid() != null
                    && CollectionUtils.isNotEmpty(profitAndLoss.getTaxPaid().getYears())) {
                values = fillYearsData(profitAndLoss.getTaxPaid());
                financialData.setTaxPaid(values);
            }
            // Sales
            if( profitAndLoss.getSales() != null
                    && CollectionUtils.isNotEmpty(profitAndLoss.getSales().getYears())) {
                values = fillYearsData(profitAndLoss.getSales());
                financialData.setSales(values);
            }
            // Other Income
            if( profitAndLoss.getOtherIncome() != null
                    && CollectionUtils.isNotEmpty(profitAndLoss.getOtherIncome().getYears())) {
                values = fillYearsData(profitAndLoss.getOtherIncome());
                financialData.setOtherIncome(values);
            }
        }
    }

    private void setFromBalanceSheet(BalanceSheet balanceSheet, FinancialData financialData) {
        if( balanceSheet != null ) {
            YearData values ;
            DoubleValue doubleValue;
            int size;
            // TotalEquityFund
            if( balanceSheet.getTotalEquityFund() != null
                    && CollectionUtils.isNotEmpty(balanceSheet.getTotalEquityFund().getYears())) {
                values = fillYearsData(balanceSheet.getTotalEquityFund());
                financialData.setTotalEqutyFund(values);
            }

            // Current Liabilities/Total Equity Fund
            if( balanceSheet.getCurrentLiabilities() != null
                    && CollectionUtils.isNotEmpty(balanceSheet.getCurrentLiabilities().getYears())) {
                values = fillYearsData(balanceSheet.getCurrentLiabilities());
                financialData.setCurrentLiabilities(values);
            }

            // Misc. Exp. Not written off
            if( balanceSheet.getMiscExpNotWrittenOff() != null
                    && CollectionUtils.isNotEmpty(balanceSheet.getMiscExpNotWrittenOff().getYears())) {
                values = fillYearsData(balanceSheet.getMiscExpNotWrittenOff());
                financialData.setMiscExpNotWrittenOff(values);
            }
        }
    }

    @Override
    public ScoringApplicantRequest buildApplicantScoringJSonVersion2(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception {

        ComponentResponse componentResponse;
        // Consumer bureau report
        ResponseMultiJsonDomain multiJsonDomain;
        // Commercial bureau report
        OutputAckDomain mbCorpJsonResponse;

        Cloner cloner = new Cloner();
        ApplicationRequest applicationRequest = cloner.deepClone(goNoGoCustomerApplication.getApplicationRequest());

        ScoringHeader header = buildHeader(goNoGoCustomerApplication);
        Application application = buildApplicationForRequestVersion2(applicationRequest);

        List<ApplicantRequest> applicantRequests = new ArrayList<>();
        ApplicantRequest applicantRequest;
        Applicant applicant;

        // Primary Applicant
        //Applicant is either consumer or commercial
        applicantRequest = populateApplicantRequest(goNoGoCustomerApplication, applicationRequest.getRequest().getApplicant().getApplicantId(),true);

        componentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();
        multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
        mbCorpJsonResponse = componentResponse.getMbCorpJsonResponse();
        setCibilResponse(multiJsonDomain , applicantRequest);
        //adding
        applicantRequests.add(applicantRequest);

        // For coapplicants
        List<ComponentResponse> componentResponseList = goNoGoCustomerApplication.getApplicantComponentResponseList();
        List<CoApplicant> coApplicants = applicationRequest.getRequest().getCoApplicant();

        // adding all co-applicants
        if (CollectionUtils.isNotEmpty(coApplicants)) {
            logger.info("Number of coApplicants " + coApplicants.size());
            for (CoApplicant coapplicant : coApplicants) {
                applicantRequest = populateApplicantRequest(goNoGoCustomerApplication, coapplicant.getApplicantId(),false);
                new ApplicantRequest();
                applicantRequest.setRequest(coapplicant);

                List<ComponentResponse> coApplicantResponseList = componentResponseList.stream()
                        .filter(coAppResponse -> StringUtils.equalsIgnoreCase(coAppResponse.getApplicantId(),
                                coapplicant.getApplicantId())
                        ).collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(coApplicantResponseList)) {
                    componentResponse = coApplicantResponseList.get(0);
                    if (componentResponse != null) {
                        //It is possible that Bureau report is not available for some of the coapplicants.

                        multiJsonDomain = componentResponse.getMultiBureauJsonRespose();
                        mbCorpJsonResponse = componentResponse.getMbCorpJsonResponse();
                        setBureauResponse(multiJsonDomain, mbCorpJsonResponse, applicantRequest);
                        setCibilResponse(multiJsonDomain , applicantRequest);
                    }
                }
                applicantRequests.add(applicantRequest);
            }
        }

        return ScoringApplicantRequest.builder()
                .gngRefId(goNoGoCustomerApplication.getGngRefId())
                .header(header)
                .applicants(applicantRequests)
                .application(application)
                .build();
    }

    private Application buildApplicationForRequestVersion2(ApplicationRequest applicationRequest) {
        Application application = new Application();
        application.setLoanTenorInMonths(Integer.valueOf(applicationRequest.getRequest().getApplication().getTenorRequested()));
        application.setAggregation("MAX");
        application.setLoanType(applicationRequest.getRequest().getApplication().getLoanType());
        application.setLoanAmt(applicationRequest.getRequest().getApplication().getLoanAmount());
        application.setPriority("QAD");
        application.setChannel(applicationRequest.getRequest().getApplication().getChannel());
        //if(applicationRequest.getHeader().getProduct()== Product.PL || applicationRequest.getHeader().getProduct()== Product.DIGI_PL){
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(applicationRequest.getRequest().getApplication().getCity())){
            logger.info("{} PL product City {}",applicationRequest.getRefID(), applicationRequest.getRequest().getApplication().getCity());
            application.setCity(applicationRequest.getRequest().getApplication().getCity().toUpperCase());
        }else{
            logger.info("{} PL product City Not Found",applicationRequest.getRefID());
            application.setCity(Constant.NULL);
        }
        // }

        return application;
    }

}
