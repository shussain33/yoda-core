package com.softcell.gonogo.service.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.queuemanager.search.request.IndexingRequest;

/**
 * @author kishorp
 */
public interface ESEntityBuilder {
    /**
     * @param customerApplication
     * @return
     * @throws JsonProcessingException
     */
    IndexingRequest buildNotificationIndexRequest(GoNoGoCustomerApplication customerApplication) throws JsonProcessingException;


}
