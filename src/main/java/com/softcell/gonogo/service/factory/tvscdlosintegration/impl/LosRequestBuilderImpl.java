package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.softcell.constants.LosTvsInitiateConstants;
import com.softcell.gonogo.model.los.tvs.LosTvsRequest;
import com.softcell.gonogo.model.request.AppDisbursalRequest;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosRequestBuilder;
import org.springframework.stereotype.Service;

@Service
public class LosRequestBuilderImpl implements LosRequestBuilder {
    @Override
    public LosTvsRequest buildLosDisbursalRequest(AppDisbursalRequest appDisbursalRequest) {
        return LosTvsRequest.builder()
                .header(appDisbursalRequest.getHeader())
                .refID(appDisbursalRequest.getRefID())
                .stage(LosTvsInitiateConstants.DISB)
                .vendorid(LosTvsInitiateConstants.SOFTCELL)
                .build();
    }
}
