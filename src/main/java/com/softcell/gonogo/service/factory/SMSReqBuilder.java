package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.SmsRequest;
import com.softcell.gonogo.model.request.core.Header;

/**
 * Created by anupamad on 10/8/17.
 */
public interface SMSReqBuilder {
    CheckApplicationStatus buildCheckApplicationStatus(Header header, String refId);
    SmsRequest buildSmsRequest(Header header, String mobileNum, String smsType);
}
