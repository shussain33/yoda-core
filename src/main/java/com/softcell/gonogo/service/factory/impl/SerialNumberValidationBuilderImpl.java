package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.*;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.Credential;
import com.softcell.gonogo.serialnumbervalidation.DisbursementRequest;
import com.softcell.gonogo.serialnumbervalidation.DisbursementResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.apple.AppleRequest;
import com.softcell.gonogo.serialnumbervalidation.apple.AppleResponse;
import com.softcell.gonogo.serialnumbervalidation.apple.RollbackRequestServiceDetails;
import com.softcell.gonogo.serialnumbervalidation.gionee.GioneeRequest;
import com.softcell.gonogo.serialnumbervalidation.gionee.GioneeResponse;
import com.softcell.gonogo.serialnumbervalidation.google.GoogleRequest;
import com.softcell.gonogo.serialnumbervalidation.google.GoogleResponse;
import com.softcell.gonogo.serialnumbervalidation.intex.IntexRequest;
import com.softcell.gonogo.serialnumbervalidation.intex.IntexResponse;
import com.softcell.gonogo.serialnumbervalidation.kent.Authentication;
import com.softcell.gonogo.serialnumbervalidation.kent.KentRequest;
import com.softcell.gonogo.serialnumbervalidation.kent.KentResponse;
import com.softcell.gonogo.serialnumbervalidation.kent.SerlInfo;
import com.softcell.gonogo.serialnumbervalidation.oppo.OppoRequest;
import com.softcell.gonogo.serialnumbervalidation.oppo.OppoResponse;
import com.softcell.gonogo.serialnumbervalidation.panasonic.PanasonicRequest;
import com.softcell.gonogo.serialnumbervalidation.panasonic.PanasonicResponse;
import com.softcell.gonogo.serialnumbervalidation.samsung.SamsungRequest;
import com.softcell.gonogo.serialnumbervalidation.samsung.SamsungResponse;
import com.softcell.gonogo.serialnumbervalidation.sony.RollBackResponse;
import com.softcell.gonogo.serialnumbervalidation.sony.SonyRequest;
import com.softcell.gonogo.serialnumbervalidation.sony.SonyResponse;
import com.softcell.gonogo.serialnumbervalidation.videocon.VideoconRequest;
import com.softcell.gonogo.serialnumbervalidation.videocon.VideoconResponse;
import com.softcell.gonogo.service.factory.SerialNumberValidationBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by yogeshb on 10/3/17.
 */
@Service
public class SerialNumberValidationBuilderImpl implements SerialNumberValidationBuilder {

    @Override
    public SonyRequest buildSonyMobileRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain sonyMobConfig) {
        return SonyRequest.builder()
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .materialName(serialSaleConfirmationRequest.getMaterialName())
                .storeCode(serialSaleConfirmationRequest.getStoreCode())
                .secureKey(sonyMobConfig.getLicenseKey())
                .build();
    }

    @Override
    public SonyRequest buildSonyCAVRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain sonyCavConfig) {
        return SonyRequest.builder()
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .productCode(serialSaleConfirmationRequest.getProductCode())
                .storeCode(serialSaleConfirmationRequest.getStoreCode())
                .materialName(serialSaleConfirmationRequest.getMaterialName())
                .secureKey(sonyCavConfig.getLicenseKey())
                .build();
    }

    @Override
    public SerialNumberResponse buildSonyResponse(SonyResponse sonyResponse) {

        return SerialNumberResponse.builder()
                .message(sonyResponse.getComments())
                .status((StringUtils.equalsIgnoreCase(ReqResHelperConstants.Approved.name(),
                        sonyResponse.getComments())) ? Status.VALID.name() : Status.INVALID.name())
                .vendor(GNGWorkflowConstant.SONY.toFaceValue())
                .build();
    }

    @Override
    public IntexRequest buildIntexSerialNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain intexConfig) {
        return IntexRequest.builder()
                .loginId(intexConfig.getLicenseKey())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .materialCode(serialSaleConfirmationRequest.getMaterialCode())
                .build();
    }

    @Override
    public PanasonicRequest buildPanasonicSerialNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain panasonicConfig) {

        return PanasonicRequest.builder()
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .userName(panasonicConfig.getLicenseKey())
                .password(panasonicConfig.getPassword())
                .agreementId(serialSaleConfirmationRequest.getReferenceID())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .build();
    }

    @Override
    public PanasonicRequest buildPanasonicImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain panasonicConfig) {

        return PanasonicRequest.builder()
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .serialNumber(serialSaleConfirmationRequest.getImeiNumber())
                .userName(panasonicConfig.getLicenseKey())
                .password(panasonicConfig.getPassword())
                .agreementId(serialSaleConfirmationRequest.getReferenceID())
                .dealerId(serialSaleConfirmationRequest.getHeader().getDealerId())
                .build();
    }

    @Override
    public SerialNumberResponse buildIntexResponse(IntexResponse intexResponse) {

        return SerialNumberResponse.builder()
                .vendor(GNGWorkflowConstant.INTEX.toFaceValue())
                .message(GngUtils.convertIntexResponseCodeToMessage(intexResponse.getResponseCode()))
                .status((intexResponse.getResponseCode() == 3) ? Status.VALID.name() : Status.INVALID.name())
                .build();
    }

    @Override
    public SerialNumberResponse buildPanasonicSerialResponse(PanasonicResponse panasonicResponse) {
        return SerialNumberResponse.builder()
                .vendor(GNGWorkflowConstant.PANASONIC.toFaceValue())
                .message(panasonicResponse.getValidateDataResponse())
                .status((StringUtils.equals("Success",panasonicResponse.getValidateDataResponse()) ? Status.VALID.name() : Status.INVALID.name()))
                .build();
    }

    @Override
    public SerialNumberResponse buildPanasonicImeiResponse(PanasonicResponse panasonicResponse) {
        return SerialNumberResponse.builder()
                .vendor(GNGWorkflowConstant.PANASONIC.toFaceValue())
                .message(GngUtils.convertPanasonicMessageForIMEI(panasonicResponse.getValidateDataResponse()))
                .status((StringUtils.equals("Success",panasonicResponse.getValidateDataResponse()) ? Status.VALID.name() : Status.INVALID.name()))
                .build();
    }

    @Override
    public SerialNumberResponse buildSamsungImeiResponse(SamsungResponse samsungResponse) {
        return SerialNumberResponse.builder()
                .vendor(GNGWorkflowConstant.SAMSUNG.toFaceValue())
                .message(GngUtils.convertSamsungImeiResponseCodeToMessage(samsungResponse.getOriginalResponse()))
                .status((StringUtils.equals("0",samsungResponse.getOriginalResponse()) ? Status.VALID.name() : Status.INVALID.name()))
                .build();
    }

    @Override
    public AppleRequest buildAppleDPLRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain appleConfig) {
        return AppleRequest.builder()
                .imei(serialSaleConfirmationRequest.getImeiNumber())
                .mpn(serialSaleConfirmationRequest.getMpn())
                .bankName(serialSaleConfirmationRequest.getBankName())
                .paymentMethod(2)
                .scheme(serialSaleConfirmationRequest.getScheme())
                .userId(appleConfig.getMemberId())
                .password(appleConfig.getLicenseKey())
                .storeAppleId(serialSaleConfirmationRequest.getStoreAppleId())
                .vendor(Cache.getVendorByInstitution(serialSaleConfirmationRequest.getHeader().getInstitutionId()))
                .build();
    }

    @Override
    public AppleRequest buildAppleCDRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain appleConfig) {
        return AppleRequest.builder()
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .mpn(serialSaleConfirmationRequest.getMpn())
                .bankName(serialSaleConfirmationRequest.getBankName())
                .paymentMethod(2)
                .scheme(serialSaleConfirmationRequest.getScheme())
                .userId(appleConfig.getMemberId())
                .password(appleConfig.getLicenseKey())
                .storeAppleId(serialSaleConfirmationRequest.getStoreAppleId())
                .vendor(Cache.getVendorByInstitution(serialSaleConfirmationRequest.getHeader().getInstitutionId()))
                .build();
    }

    @Override
    public SerialNumberResponse buildAppleResponse(AppleResponse appleResponse) {

        String statusFlag = Status.INVALID.name();
        String message = null;
        if (null != appleResponse.getQueryResponse()
                && null != appleResponse.getRegisteredResponse()
                && null != appleResponse.getReportResponse()) {
            if (appleResponse.getQueryResponse().isSaleEligible()
                    && appleResponse.getRegisteredResponse().isSaleRegistered()
                    && appleResponse.getReportResponse().isSaleReported()) {
                statusFlag = Status.VALID.name();
                message = appleResponse.getReportResponse().getStatus();
            }
        } else if (null != appleResponse.getError()) {
            statusFlag = Status.INVALID.name();
            message = appleResponse.getError().getMessage();
        }


        return SerialNumberResponse.builder()
                .vendor(GNGWorkflowConstant.APPLE.toFaceValue())
                .message(message)
                .status(statusFlag)
                .build();
    }

    @Override
    public SamsungRequest buildSamsungRequest(SerialSaleConfirmationRequest serialsaleconfirmationrequest, WFJobCommDomain samsungConfig) {
        return SamsungRequest.builder()
                .referenceID(serialsaleconfirmationrequest.getReferenceID())
                .serialNumber(serialsaleconfirmationrequest.getSerialNumber())
                .partnerCode(serialsaleconfirmationrequest.getPartnerCode())
                .productType(serialsaleconfirmationrequest.getProductType())
                .skuCode(serialsaleconfirmationrequest.getSkuCode())
                .saleType(serialsaleconfirmationrequest.getSaleType())
                .saleDate(serialsaleconfirmationrequest.getSaleDate())
                .vendor(Cache.getVendorByInstitution(serialsaleconfirmationrequest.getHeader().getInstitutionId()))
                .credential(Credential.builder()
                        .userId(samsungConfig.getMemberId())
                        .password(samsungConfig.getPassword())
                        .build())
                .build();
    }

    @Override
    public SamsungRequest buildSamsungImeiRequest(SerialSaleConfirmationRequest serialsaleconfirmationrequest, WFJobCommDomain samsungConfig) {
        return SamsungRequest.builder()
                .imeiNumber(serialsaleconfirmationrequest.getImeiNumber())
                .saleDate(serialsaleconfirmationrequest.getSaleDate())
                .skuCode(serialsaleconfirmationrequest.getSkuCode())
                .vendor(Cache.getVendorCodeByInstitution(serialsaleconfirmationrequest.getHeader().getInstitutionId()))
                .referenceID(serialsaleconfirmationrequest.getReferenceID())
                .channelCode(serialsaleconfirmationrequest.getChannelCode())
                .status("1")
                .credential(Credential.builder()
                        .userId(samsungConfig.getMemberId())
                        .password(samsungConfig.getPassword())
                        .build())
                .build();
    }

    @Override
    public GioneeRequest buildGioneeImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain gioneeConfig) {
        return GioneeRequest.builder()
                .imei(serialSaleConfirmationRequest.getImeiNumber())
                .modelCode(serialSaleConfirmationRequest.getSkuCode())
                .requestDate(GngDateUtil.getGioneeDate(new DateTime()))
                .retailerCode(serialSaleConfirmationRequest.getStoreCode())
                .requestType("1")
                .password(gioneeConfig.getPassword())
                .userName(gioneeConfig.getMemberId())
                .accessKey(gioneeConfig.getLicenseKey())
                .build();
    }

    @Override
    public SerialNumberResponse buildGioneeImeiResponse(GioneeResponse gioneeResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.GIONEE.toFaceValue())
                .message(gioneeResponse.getStatusMsg())
                .status(StringUtils.equals("0",gioneeResponse.getStatusCode())?Status.VALID.name():Status.INVALID.name())
                .build();
    }

    @Override
    public VideoconRequest buildVideoconRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        return VideoconRequest.builder()
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .fType(Constant.DO)
                .institutionID(serialSaleConfirmationRequest.getHeader().getInstitutionId())
                .build();
    }

    @Override
    public SerialNumberResponse buildVideoconResponse(VideoconResponse videoconResponse) {
         return SerialNumberResponse.builder()
                .vendor(Vendor.VIDEOCON.toFaceValue())
                .message(GngUtils.convertVideoconResponseCodeToMessage(videoconResponse.getResponseCode()))
                .status((videoconResponse.getResponseCode() == 1) ? Status.VALID.name() : Status.INVALID.name())
                .build();
    }

    @Override
    public AppleRequest buildAppleImeiRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain appleConfig) {
        return AppleRequest.builder()
                .imei(rollbackRequest.getImeiNumber())
                .userId(appleConfig.getMemberId())
                .password(appleConfig.getLicenseKey())
                .build();
    }

    @Override
    public AppleRequest buildAppleSerialNumberRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain appleConfig) {
        return AppleRequest.builder()
                .serialNumber(rollbackRequest.getSerialNumber())
                .userId(appleConfig.getMemberId())
                .password(appleConfig.getLicenseKey())
                .build();
    }

    @Override
    public VideoconRequest buildVideoconSerialNumberRollbackRequest(RollbackRequest rollbackRequest) {
        return VideoconRequest.builder()
                .serialNumber(rollbackRequest.getSerialNumber())
                .skuCode(rollbackRequest.getSkuCode())
                .institutionID(rollbackRequest.getHeader().getInstitutionId())
                .build();
    }

    @Override
    public VideoconRequest buildVideoconSerialNumberDisbursementRequest(DisbursementRequest disbursementRequest) {
        return VideoconRequest.builder()
                .serialNumber(disbursementRequest.getSerialNumber())
                .skuCode(disbursementRequest.getSkuCode())
                .institutionID(disbursementRequest.getHeader().getInstitutionId())
                .build();
    }

    @Override
    public RollbackRequestServiceDetails buildAppleRollbackPreRequisiteServiceResponse(SerialNumberInfo serialNumberInfo) {
        return RollbackRequestServiceDetails.builder()
                .serialNumber(serialNumberInfo.getSerialNumber())
                .skuCode(serialNumberInfo.getSkuCode())
                .imeiNumber(serialNumberInfo.getImeiNumber())
                .vendor(serialNumberInfo.getVendor())
                .build();
    }

    @Override
    public SerialNumberResponse buildAppleRollbackResponse(RollBackResponse rollBackResponse) {
        String statusFlag;
        String message;

        if (rollBackResponse.isSaleRollback()) {
            statusFlag = Status.SUCCESS.name();
            message = rollBackResponse.getStatus();
        } else {
            statusFlag = Status.FAILED.name();
            message = rollBackResponse.getErrorMessage();
        }


        return SerialNumberResponse.builder()
                .vendor(GNGWorkflowConstant.APPLE.toFaceValue())
                .message(message)
                .status(statusFlag)
                .build();

    }

    @Override
    public SerialNumberResponse buildVideoconSerialRollbackResponse(RollBackResponse rollBackResponse) {
          return SerialNumberResponse.builder()
                .vendor(Vendor.VIDEOCON.toFaceValue())
                .message(GngUtils.convertVideoconResponseCodeToMessage(rollBackResponse.getResponseCode()))
                .status((rollBackResponse.getResponseCode() == 5) ? Status.VALID.name() : Status.INVALID.name())
                .build();

    }

    @Override
    public SerialNumberResponse buildVideoconSerialDisbursementResponse(DisbursementResponse disbursementResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.VIDEOCON.toFaceValue())
                .message(GngUtils.convertVideoconResponseCodeToMessage(disbursementResponse.getResponseCode()))
                .status((disbursementResponse.getResponseCode() == 1) ? Status.VALID.name() : Status.INVALID.name())
                .build();

    }

    @Override
    public KentRequest buildKentRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain kentConfig) {
        return KentRequest.builder()
                .serlInfo(SerlInfo.builder()
                        .serialNo(serialSaleConfirmationRequest.getSerialNumber())
                        .modelID(serialSaleConfirmationRequest.getSkuCode())
                        .storeId(serialSaleConfirmationRequest.getStoreCode())
                        .schemeId(serialSaleConfirmationRequest.getScheme())
                        .build())
                .Authentication(Authentication.builder()
                        .username(kentConfig.getMemberId())
                        .password(kentConfig.getPassword())
                        .servicename(EndPointReferrer.KENT_SERIAL_NUMBER)
                        .date(new Date().toString())
                        .build())
                .build();

    }

    @Override
    public SerialNumberResponse buildKentResponse(KentResponse kentResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.KENT.toFaceValue())
                .status(kentResponse.getStatus())
                .message(kentResponse.getStatusMsg())
                .build();
    }

    @Override
    public KentRequest buildKentSerialRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain kentConfig) {
        return KentRequest.builder()
                .serlInfo(SerlInfo.builder()
                        .serialNo(rollbackRequest.getSerialNumber())
                        .modelID(rollbackRequest.getSkuCode())
                        .storeId(rollbackRequest.getStoreCode())
                        .schemeId(rollbackRequest.getSchemeCode())
                        .build())
                .Authentication(Authentication.builder()
                        .username(kentConfig.getMemberId())
                        .password(kentConfig.getPassword())
                        .servicename(EndPointReferrer.KENT_SERIAL_ROLLBACK)
                        .date(new Date().toString())
                        .build())
                .build();
    }

    @Override
    public KentRequest buildKentSerialDisburseRequest(DisbursementRequest disbursementRequest, WFJobCommDomain kentConfig) {
        return KentRequest.builder()
                .serlInfo(SerlInfo.builder()
                        .serialNo(disbursementRequest.getSerialNumber())
                        .modelID(disbursementRequest.getSkuCode())
                        .storeId(disbursementRequest.getStoreCode())
                        .schemeId(disbursementRequest.getSchemeCode())
                        .build())
                .Authentication(Authentication.builder()
                        .username(kentConfig.getMemberId())
                        .password(kentConfig.getPassword())
                        .servicename(EndPointReferrer.KENT_SERIAL_DISBURSED)
                        .date(new Date().toString())
                        .build())
                .build();
    }

    @Override
    public OppoRequest buildOppoImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain oppoConfig) {
        return OppoRequest.builder()
                .imei(serialSaleConfirmationRequest.getImeiNumber())
                .model(serialSaleConfirmationRequest.getSkuCode())
                .oppoDealerId(serialSaleConfirmationRequest.getDealerId())
                .oppoAgent(serialSaleConfirmationRequest.getChannelCode())
                .accessCode(oppoConfig.getLicenseKey())
                .build();
    }

    @Override
    public SerialNumberResponse buildOppoImeiResponse(OppoResponse oppoResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.OPPO.toFaceValue())
                .message(StringUtils.equalsIgnoreCase(oppoResponse.getErrorMsg(), "true") ? GngUtils.convertOppoResponseCodeToMessage(Integer.parseInt(oppoResponse.getErrorCode())) :  oppoResponse.getErrorMsg())
                .status(StringUtils.equals("0",oppoResponse.getErrorCode())?Status.VALID.name():Status.INVALID.name())
                .build();
    }

    @Override
    public GoogleRequest buildGoogleImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {

        DateTime dateTime = new DateTime();

        return GoogleRequest.builder()
                .rdsCode(serialSaleConfirmationRequest.getRdsCode())
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .blockStatus(Constant.BLOCKSTATUS)
                .dateAndTime(dateTime.toString())
                .customerCode(Cache.getVendorByInstitution(serialSaleConfirmationRequest.getHeader().getInstitutionId()))
                .build();
    }

    @Override
    public SerialNumberResponse buildGoogleImeiResponse(GoogleResponse googleResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.GOOGLE.toFaceValue())
                .message(GngUtils.convertGoogleResponseCodeToMessage(Integer.parseInt(googleResponse.getStatusCode())))
                .status((StringUtils.equals("0", googleResponse.getStatusCode()) ? Status.VALID.name() : Status.INVALID.name()))
                .build();
    }


    @Override
    public GoogleRequest buildGoogleImeiRollbackRequest(RollbackRequest rollbackRequest) {
        DateTime dateTime = new DateTime();

        return GoogleRequest.builder()
                .blockStatus(Constant.ROLLBACKSTATUS)
                .dateAndTime(dateTime.toString())
                .imeiNumber(rollbackRequest.getImeiNumber())
                .customerCode(Cache.getVendorByInstitution(rollbackRequest.getHeader().getInstitutionId()))
                .build();
    }

    @Override
    public SerialNumberResponse buildGoogleImeiRollbackResponse(GoogleResponse googleResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.GOOGLE.toFaceValue())
                .message(GngUtils.convertGoogleRollbackResponseCodeToMessage(Integer.parseInt(googleResponse.getStatusCode())))
                .status((StringUtils.equals("0", googleResponse.getStatusCode()) ? Status.SUCCESS.name() : Status.FAILED.name()))
                .build();
    }


    }