package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.lava.LavaRequest;
import com.softcell.gonogo.serialnumbervalidation.lava.LavaResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * @author prasenjit wadmare
 * @date 02 Jan 2018
 */

public interface LavaSerialNumberValidationBuilder {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param lavaConfig
     * @return
     */
    LavaRequest buildLavaImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain lavaConfig, PostIpaRequest postIpaRequest);

    /**
     *
     * @param lavaResponse
     * @return
     */
    SerialNumberResponse buildLavaImeiNumberResponse(LavaResponse lavaResponse);


}
