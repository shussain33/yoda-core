package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.Status;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.dms.PushDMSDocumentsRequest;
import com.softcell.gonogo.model.dms.addfolder.AddFolderDmsOriginalResponse;
import com.softcell.gonogo.model.dms.addfolder.AddFolderRequest;
import com.softcell.gonogo.model.dms.additionaldoc.DmsAdditionalInformation;
import com.softcell.gonogo.model.dms.collection.DMSCreatedFolderInformation;
import com.softcell.gonogo.model.dms.collection.DMSPushDocumentInformation;
import com.softcell.gonogo.model.dms.connectcabinet.ConnectCabinetRequest;
import com.softcell.gonogo.model.dms.postdocument.DmsOriginalResponse;
import com.softcell.gonogo.model.dms.postdocument.DocumentInformation;
import com.softcell.gonogo.model.dms.postdocument.PostDocumentRequest;
import com.softcell.gonogo.model.dms.postdocument.PostDocumentResponse;
import com.softcell.gonogo.model.dms.searchfolder.FolderInformation;
import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.GetFileRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.sms.Error;
import com.softcell.gonogo.service.factory.DMSBuilder;
import com.softcell.utils.DMSUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by mahesh on 24/6/17.
 */
@Service
public class DMSBuilderImpl implements DMSBuilder {

    @Override
    public AddFolderRequest buildAddFolderRequest(String losId, String refId, String institutionId, String folderName, long parentFolderId, int userDbId) {

        AddFolderRequest addFolderRequest = new AddFolderRequest();

        addFolderRequest.setAccessType(Status.I.name());
        addFolderRequest.setFolderName(folderName);
        addFolderRequest.setFolderType(Status.G.name());
        addFolderRequest.setInstitutionId(institutionId);
        addFolderRequest.setLosId(losId);
        addFolderRequest.setParentFolderIndex(parentFolderId);
        addFolderRequest.setUserDbId(String.valueOf(userDbId));

        return addFolderRequest;
    }

    @Override
    public CheckApplicationStatus buildGetApplicationImagesRequest(String refId, String institutionId) {

        CheckApplicationStatus checkApplicationStatus = new CheckApplicationStatus();
        Header header = new Header();

        header.setInstitutionId(institutionId);
        checkApplicationStatus.setGonogoRefId(refId);
        checkApplicationStatus.setHeader(header);

        return checkApplicationStatus;
    }

    @Override
    public DMSCreatedFolderInformation buildCreatedFolderInformationObject(String refId, String institutionId, AddFolderDmsOriginalResponse addFolderDmsOriginalResponse) {

        DMSCreatedFolderInformation dmsCreatedFolderInformation = new DMSCreatedFolderInformation();
        List<FolderInformation> folderInformationList = new ArrayList<>();
        FolderInformation folderInformation = new FolderInformation();

        dmsCreatedFolderInformation.setRefId(refId);
        dmsCreatedFolderInformation.setInstitutionId(institutionId);

        folderInformation.setParentFolderIndex(addFolderDmsOriginalResponse.getParentFolderIndex());
        folderInformation.setFolderName(addFolderDmsOriginalResponse.getFolderName());
        folderInformation.setFolderIndex(addFolderDmsOriginalResponse.getFolderIndex());
        folderInformation.setFolderType(addFolderDmsOriginalResponse.getFolderType());
        folderInformation.setUpdateDate(new Date());
        folderInformation.setOwner(addFolderDmsOriginalResponse.getOwner());
        folderInformation.setOwnerIndex(addFolderDmsOriginalResponse.getOwnerIndex());

        folderInformationList.add(folderInformation);

        dmsCreatedFolderInformation.setFolderInformation(folderInformationList);


        return dmsCreatedFolderInformation;
    }

    @Override
    public GetFileRequest buildGetDocumentBase64ImageRequest(String imageId, String institutionId) {
        GetFileRequest getImageRequest = new GetFileRequest();

        Header header = new Header();

        getImageRequest.setImageFileID(imageId);
        header.setInstitutionId(institutionId);
        getImageRequest.setHeader(header);

        return getImageRequest;
    }

    @Override
    public PostDocumentRequest buildDocumentPushRequest(ImagesDetails imagesDetails, String institutionId, String losId, String byteCode, long folderId, int userDbId, Set<DmsAdditionalInformation> dmsAdditionalInformations) throws IOException {

        PostDocumentRequest postDocumentRequest = new PostDocumentRequest();

        postDocumentRequest.setInstitutionId(institutionId);
        postDocumentRequest.setBase64Document(byteCode);
        postDocumentRequest.setLosId(losId);
        postDocumentRequest.setFolderIndex((int) folderId);
        postDocumentRequest.setDocumentName(DMSUtils.getDocumentNameWithExtension(imagesDetails.getImageType(),imagesDetails.getImageName()));
        postDocumentRequest.setDocumentType(DMSUtils.getDocumentExtension(imagesDetails.getImageType()));
        postDocumentRequest.setUserDbId(String.valueOf(userDbId));
        postDocumentRequest.setVolumeId(1);
        postDocumentRequest.setDocumentSize(String.valueOf(DMSUtils.getDocumentSize(byteCode)));
        postDocumentRequest.setNoOfPages(DMSUtils.getNoOfPagesInDocument(imagesDetails.getImageType(), byteCode, imagesDetails.getImageName()));
        postDocumentRequest.setDmsAdditionalInformations(dmsAdditionalInformations);

        return postDocumentRequest;
    }

    @Override
    public DMSPushDocumentInformation buildPushDocumentInformationSuccessObject(PostDocumentResponse postDocumentResponse, String refId, String institutionId, String objectId ,String folderName ,Date createDate) {

        DMSPushDocumentInformation dmsPushDocumentInformation = null;
        DocumentInformation successDocumentInformation = new DocumentInformation();
        List<DocumentInformation> successDocumentInformationList = new ArrayList<>();

        if (null != postDocumentResponse
                && StringUtils.equals(postDocumentResponse.getStatus(), Status.SUCCESS.name())
                && null != postDocumentResponse.getDmsOriginalResponse()) {

            dmsPushDocumentInformation = new DMSPushDocumentInformation();

            DmsOriginalResponse dmsOriginalResponse = postDocumentResponse.getDmsOriginalResponse();


            successDocumentInformation.setDocumentName(dmsOriginalResponse.getDocumentName());
            successDocumentInformation.setDocumentType(dmsOriginalResponse.getDocumentType());
            successDocumentInformation.setDocumentId(dmsOriginalResponse.getDocumentIndex());
            successDocumentInformation.setDocPushStatusCode(String.valueOf(dmsOriginalResponse.getStatusCode()));
            successDocumentInformation.setParentFolderId(dmsOriginalResponse.getParentFolderIndex());
            successDocumentInformation.setParentFolderName(folderName);
            successDocumentInformation.setDocumentSize(dmsOriginalResponse.getDocumentSize());
            successDocumentInformation.setDocPushStatus(Status.SUCCESS.name());

            successDocumentInformation.setDocObjectId(objectId);
            // time and date before service call
            successDocumentInformation.setCreateDate(createDate);
            successDocumentInformation.setUpdateDate(new Date());

            successDocumentInformationList.add(successDocumentInformation);

            dmsPushDocumentInformation.setInstitutionId(institutionId);
            dmsPushDocumentInformation.setRefId(refId);

            dmsPushDocumentInformation.setSuccessDocumentInformation(successDocumentInformationList);
        }

        return dmsPushDocumentInformation;
    }

    @Override
    public ConnectCabinetRequest buildConnectCabinetObject(String losId, String institutionId, String requestType) {

        ConnectCabinetRequest connectCabinetRequest = new ConnectCabinetRequest();
        connectCabinetRequest.setInstitutionId(institutionId);
        connectCabinetRequest.setLosId(losId);
        connectCabinetRequest.setRequestType(requestType);

        return connectCabinetRequest;

    }


    @Override
    public ConnectCabinetRequest buildDisConnectCabinetObject(String losId, String institutionId, String requestType, int userDbId) {

        ConnectCabinetRequest disConnectCabinetObject = new ConnectCabinetRequest();

        disConnectCabinetObject.setInstitutionId(institutionId);
        if (null != losId) {
            disConnectCabinetObject.setLosId(losId);
        }
        disConnectCabinetObject.setRequestType(requestType);
        disConnectCabinetObject.setUserDbId(String.valueOf(userDbId));

        return disConnectCabinetObject;
    }


    @Override
    public DMSPushDocumentInformation buildPushDocumentInformationFailedObject(ImagesDetails imagesDetails, String refId, String institutionId ,Date createDate) {

        DMSPushDocumentInformation  failedDocumentInformation = new DMSPushDocumentInformation();
        DocumentInformation failedDocument = new DocumentInformation();
        List<DocumentInformation> failedDocumentInformationList = new ArrayList<>();

        failedDocument.setDocumentName(imagesDetails.getImageName());
        failedDocument.setDocumentType(imagesDetails.getImageType());
        failedDocument.setFailedReason(imagesDetails.getReason());
        failedDocument.setDocPushStatus(imagesDetails.getStatus());
        failedDocument.setDocPushStatusCode(imagesDetails.getBlockName());

        failedDocument.setDocObjectId(imagesDetails.getImageId());
        // the time when service call
        failedDocument.setCreateDate(createDate);
        failedDocument.setUpdateDate(new Date());

        failedDocumentInformationList.add(failedDocument);

        failedDocumentInformation.setInstitutionId(institutionId);
        failedDocumentInformation.setRefId(refId);

        failedDocumentInformation.setFailedDocumentInformation(failedDocumentInformationList);


        return failedDocumentInformation;
    }

    @Override
    public PushDMSDocumentsRequest buildDmsErrorLogObject(PushDMSDocumentsRequest pushDMSDocumentsRequest, String errorMessage ,String errorCode) {

        Error error = new Error();
        error.setErrorMsg(errorMessage);
        error.setErrorCode(errorCode);

        pushDMSDocumentsRequest.setError(error);

        return pushDMSDocumentsRequest;
    }
}
