package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.ApplicantType;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Institute;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Application;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.lms.*;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.service.factory.LMSBuilder;
import com.softcell.utils.GngDateUtil;
import net.logstash.logback.encoder.org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.stringtemplate.v4.ST;

/**
 * Created by kumar on 23/7/18.
 */
@Service
public class LMSBuilderImpl implements LMSBuilder {

    private static final Logger logger = LoggerFactory.getLogger(LMSBuilderImpl.class);


    @Override
    public SBFCLMSIntegrationRequest buildRetrieveClientRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap) {

        sbfclmsIntegrationRequest.setRetrieveClientRequest(getRetrieveClientRequest(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc(), lmsMasterDataMap));

        return sbfclmsIntegrationRequest;
    }

    private RetrieveClientRequest getRetrieveClientRequest(List<Kyc> kycList, Map lmsMasterDataMap) {

        for (Kyc kycdoc: kycList) {

            if (StringUtils.isNotBlank(kycdoc.getKycNumber()) &&
                    StringUtils.isNotBlank(kycdoc.getKycName()) &&
                    lmsMasterDataMap.containsKey(kycdoc.getKycName())){

                return RetrieveClientRequest.builder()
                        .documentKey(kycdoc.getKycNumber())
                        .documentType(String.valueOf(lmsMasterDataMap.get(kycdoc.getKycName())))
                        .build();

            }
        }

        return null;
    }

    @Override
    public SBFCLMSIntegrationRequest buildClientCreationRequestForEntity(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap, CamDetails camDetails, LoanCharges loanCharges) {
        sbfclmsIntegrationRequest.setClientCreationRequest(ClientCreationRequest.builder()
                .clientIdentifiers(getClientIdentifiers(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc(), lmsMasterDataMap))
                .address(getAddress(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress(), lmsMasterDataMap))
                .familyMembers(new ArrayList<>())
                .officeId(String.valueOf(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2().getLmsBranchId()))
                .staffId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.STAFF_ID)))
                .legalFormId(getLegalFormId(goNoGoCustomerApplication, lmsMasterDataMap))
                .fullname(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName().getFirstName())
                .mobileNo(getMobileNo(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getPhone()))
                .genderId(StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getGender(), LOSLMSIntegrationConstants.MALE) ? "25" : "26")
                .clientTypeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.CLIENT_TYPE_ID)))
                .clientClassificationId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.CLIENT_CLASSIFICATION_ID)))
                .active(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.CLIENTACTIVE)))
                .locale(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOCALE)))
                .dateFormat(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.DATE_FORMAT)))
                .activationDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .submittedOnDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .dateOfBirth(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getEmployment().get(0).getDateOfJoining())
                .savingsProductId(null)
                .commAddressTypeId(getCommAddressTypeId(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress(), lmsMasterDataMap))
                .build());
        return sbfclmsIntegrationRequest;
    }

    @Override
    public SBFCLMSIntegrationRequest buildClientCreationRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap,
                                                                CamDetails camDetails, LoanCharges loanCharges) {

        sbfclmsIntegrationRequest.setClientCreationRequest(ClientCreationRequest.builder()
                .clientIdentifiers(getClientIdentifiers(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc(), lmsMasterDataMap))
                .address(getAddress(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress(), lmsMasterDataMap))
                .familyMembers(new ArrayList<>())
                .officeId(String.valueOf(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2().getLmsBranchId()))
                .staffId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.STAFF_ID)))
                .legalFormId(getLegalFormId(goNoGoCustomerApplication, lmsMasterDataMap))
                .firstname(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName().getFirstName())
                .lastname(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName().getLastName())
                .mobileNo(getMobileNo(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getPhone()))
//                .emailId(getEmailId(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getEmail()))
                .genderId(StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getGender(), LOSLMSIntegrationConstants.MALE) ? "25" : "26")
                .clientTypeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.CLIENT_TYPE_ID)))
                .clientClassificationId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.CLIENT_CLASSIFICATION_ID)))
                .active(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.CLIENTACTIVE)))
                .locale(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOCALE)))
                .dateFormat(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.DATE_FORMAT)))
                .activationDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .submittedOnDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .dateOfBirth(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getDateOfBirth())
                .savingsProductId(null)
                .commAddressTypeId(getCommAddressTypeId(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress(), lmsMasterDataMap))
                .build());

        return sbfclmsIntegrationRequest;
    }
    private String getCommAddressTypeId(List<CustomerAddress> customerAddresses, Map lmsMasterDataMap) {

        String id = null;

        for (CustomerAddress customerAddress: customerAddresses) {

            if (StringUtils.isNotBlank(customerAddress.getAddressType()) &&
                    StringUtils.isNotBlank(customerAddress.getAddressLine1()) &&
                    StringUtils.isNotBlank(customerAddress.getState()) &&
                    lmsMasterDataMap.containsKey(customerAddress.getAddressType()) &&
                    lmsMasterDataMap.containsKey(customerAddress.getState())){

                id = String.valueOf(lmsMasterDataMap.get(customerAddress.getAddressType()));
                break;
            }
        }

        return id;
    }

    private String getMobileNo(List<Phone> phoneList) {

        for (Phone phone:phoneList) {

            if (StringUtils.isNotBlank(phone.getPhoneNumber())){

                return phone.getPhoneNumber();
            }
        }

        return null;
    }

    private List<Address> getAddress(List<CustomerAddress> customerAddressList, Map lmsMasterDataMap) {

        List<Address> addressList = new ArrayList<>();

        for (CustomerAddress customerAddress: customerAddressList) {

            if (StringUtils.isNotBlank(customerAddress.getAddressType()) &&
                    StringUtils.isNotBlank(customerAddress.getAddressLine1()) &&
                    StringUtils.isNotBlank(customerAddress.getState()) &&
                    lmsMasterDataMap.containsKey(customerAddress.getAddressType()) &&
                    lmsMasterDataMap.containsKey(customerAddress.getState())){

                addressList.add(Address.builder()
                        .addressTypeId(String.valueOf(lmsMasterDataMap.get(customerAddress.getAddressType())))
                        .street(customerAddress.getAddressLine1())
                        .city(customerAddress.getCity())
                        .countryId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.INDIA)))
                        .stateProvinceId(String.valueOf(lmsMasterDataMap.get(customerAddress.getState())))
                        .postalCode(String.valueOf(customerAddress.getPin()))
                        .isActive(LOSLMSIntegrationConstants.IS_ACTIVE_PARAMETER)
                        .build());
            }
        }

        return addressList;
    }

    private List<ClientIdentifiers> getClientIdentifiers(List<Kyc> kycList, Map lmsMasterDataMap) {

        List<ClientIdentifiers> clientIdentifiersList = new ArrayList<>();

        for (Kyc kycdoc: kycList) {

            if (StringUtils.isNotBlank(kycdoc.getKycNumber()) &&
                    StringUtils.isNotBlank(kycdoc.getKycName()) &&
                    lmsMasterDataMap.containsKey(kycdoc.getKycName())){

                clientIdentifiersList.add(ClientIdentifiers.builder()
                        .documentTypeId(String.valueOf(lmsMasterDataMap.get(kycdoc.getKycName())))
                        .status(LOSLMSIntegrationConstants.STATUS)
                        .documentKey(kycdoc.getKycNumber())
                        .description(kycdoc.getKycName())
                        .build());
            }
        }

        return clientIdentifiersList;
    }

    private List<Charges> getCharges(LoanCharges loanCharges, Map lmsMasterDataMap, GoNoGoCustomerApplication goNoGoCustomerApplication) {
        List<Charges> chargeList = new ArrayList<>();

        if (Institute.isInstitute(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), Institute.TRIC)) {
            if (loanCharges.getLoanDetails().getApprovedPf() > 0) {
                double amount = 0;
                amount = (loanCharges.getProFeesAmt() / loanCharges.getLoanDetails().getFinalDisbAmt()) * 100;
                chargeList.add(Charges.builder()
                        .chargeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.PROCESSING_FEE)))
                        .amount(String.valueOf(amount))
                        .build());
            }
            if (loanCharges.getPreEMIDeductAmt() > 0) {
                chargeList.add(Charges.builder()
                        .chargeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.Pre_EMI)))
                        .amount(String.valueOf(loanCharges.getPreEMIDeductAmt())).build());
            }
            if(loanCharges.getLoanDetails().getDocumentCharges() > 0) {
                chargeList.add(Charges.builder()
                        .chargeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.Document_Charges)))
                        .amount(String.valueOf(loanCharges.getLoanDetails().getDocumentCharges())).build());
            }
            if(loanCharges.getLoanDetails().getPdcAmount() > 0) {
                chargeList.add(Charges.builder()
                        .chargeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.PDC_Amount)))
                        .amount(String.valueOf(loanCharges.getLoanDetails().getPdcAmount())).build());
            }
        }

        /*else if (Institute.isInstitute(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), Institute.SELFIN)) {

            String state = getState(goNoGoCustomerApplication);

            if (StringUtils.equalsIgnoreCase(state, LOSLMSIntegrationConstants.MAHARASHTRA)) {
                if (loanCharges.getLoanDetails().getApprovedPf() > 0) {
                    double amount = 0;
                    amount = (loanCharges.getProFeesAmt() / loanCharges.getLoanDetails().getFinalDisbAmt()) * 100;
                    chargeList.add(Charges.builder()
                            .chargeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.PF_INTRA)))
                            .amount(String.valueOf(amount))
                            .build());
                }
            } else {
                if (loanCharges.getLoanDetails().getApprovedPf() > 0) {
                    double amount = 0;
                    amount = (loanCharges.getProFeesAmt() / loanCharges.getLoanDetails().getFinalDisbAmt()) * 100;
                    chargeList.add(Charges.builder()
                            .chargeId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.PF_INTER)))
                            .amount(String.valueOf(amount))
                            .build());
                }
            }
        } */
        return chargeList;
    }

    private String getLoanProductId(Application application, Map lmsMasterDataMap){

        if(StringUtils.isNotBlank(application.getLoanType()) &&
                lmsMasterDataMap.containsKey(application.getLoanType())){
            String loanProductId = (String.valueOf(lmsMasterDataMap.get(application.getLoanType())));
            return loanProductId;
        }
        return null;
    }

    private String getState(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        String state = null;
        try {
            state = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress().stream()
                    .filter(customerAddress -> (StringUtils.equalsIgnoreCase(customerAddress.getAddressType(), GNGWorkflowConstant.RESIDENCE.toString())))
                    .map(CustomerAddress::getState)
                    .findFirst().orElse(null);
        } catch (Exception e) {
            logger.info("Exception while getting state", ExceptionUtils.getStackTrace(e));
        }
        return state;
    }

    private String getLegalFormId (GoNoGoCustomerApplication goNoGoCustomerApplication, Map lmsMasterDataMap) {
        String legalFormId = null;
        if (Institute.isInstitute(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), Institute.TRIC)) {
            String applicantType = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantType();
            if (StringUtils.equalsIgnoreCase(applicantType, ApplicantType.INDIVIDUAL.value())) {
                legalFormId = String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LEGAL_FORM_ID_PERSON));
            } else {
                legalFormId = String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LEGAL_FORM_ID_ENTITY));
            }
        } else {
            legalFormId = String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LEGAL_FORM_ID_PERSON));
        }
        return legalFormId;
    }

    private String getProductId(GoNoGoCustomerApplication goNoGoCustomerApplication, Map lmsMasterDataMap) {
        String productId = null;

        //if institutionId = 4157
        if (Institute.isInstitute(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), Institute.TRIC)) {
            if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getLoanType()) &&
                    lmsMasterDataMap.containsKey(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getLoanType())) {
                productId = (String.valueOf(lmsMasterDataMap.get(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getLoanType())));
            }
        }
        /*else if (Institute.isInstitute(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), Institute.SELFIN)) {
            String state = getState(goNoGoCustomerApplication);
            if (StringUtils.equalsIgnoreCase(state, LOSLMSIntegrationConstants.MAHARASHTRA)) {
                productId = String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.BL_INTRA));
            } else {
                productId = String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.BL_INTER));
            }
        }*/
        return productId;
    }

    @Override
    public SBFCLMSIntegrationRequest buildCreateLoanRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, String clientId, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap, LoanCharges loanCharges) {

        sbfclmsIntegrationRequest.setCreateLoanRequest(CreateLoanRequest.builder()
                .monthlyRepaymentType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.MONTHLY_REPAYMENT_TYPE)))
                .monthlyInterestRepaymentType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.MONTHLY_INTEREST_REPAYMENT_TYPE)))
                .monthlyPrincipalRepaymentType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.MONTHLY_PRINCIPAL_REPAYMENT_TYPE)))
                .clientId(String.valueOf(clientId))
                .externalId(goNoGoCustomerApplication.getGngRefId())
                .productId(getProductId(goNoGoCustomerApplication, lmsMasterDataMap))
                .disbursementData(new ArrayList<>())
                .fundId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.FUND_ID)))
                .isCoborrower(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.IS_COBORROWER)))
                .principal(String.valueOf(loanCharges.getLoanDetails().getFinalDisbAmt()))
                .loanTermFrequency(String.valueOf(loanCharges.getTenureDetails().getTenureMonths()))
                .loanTermFrequencyType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOAN_TERM_FREQ_TYPE)))
                .numberOfRepayments(String.valueOf(loanCharges.getTenureDetails().getTenureMonths()))
                .repaymentEvery(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.REPAYMENT_EVERY)))
                .repaymentFrequencyType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.REPAYMENT_FREQ_TYPE)))
                .numberOfInterestRepayments(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.NUMBER_OF_INTEREST_REPAYMENTS)))
                .interestRepaymentEvery(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.INTEREST_REPAYMENT_EVERY)))
                .interestRepaymentFrequencyType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.INTEREST_REPAYMENT_FREQ_TYPE)))
                .numberOfPrincipalRepayments(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.NUMBER_OF_PRINCIPAL_REPAYMENTS)))
                .principalRepaymentEvery(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.PRINCIPAL_REPAYMENT_EVERY)))
                .principalRepaymentFrequencyType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.PRINCIPAL_REPAYMENT_FREQ_TYPE)))
                .interestRatePerPeriod(String.valueOf(loanCharges.getInterestDetails().getInterestRate()))
                .amortizationType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.AMORTIZATION_TYPE)))
                .isEqualAmortization(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.IS_EQUAL_AMORTIZATION)))
//                .interestType(String.valueOf(loanCharges.getInterestDetails().getDisbInterestRate()))
                .interestType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.INTEREST_TYPE)))
                .interestCalculationPeriodType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.INTEREST_CALCULATION_PERIOD_TYPE)))
                .allowPartialPeriodInterestCalcualtion(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.PARTIAL_INTEREST_CALCULATION)))
                .transactionProcessingStrategyId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.REPAYMENT_STRATEGY)))
//                .loanOfficerId(goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getEmpId())
                .loanOfficerId(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOAN_OFFICER_ID)))
                .charges(getCharges(loanCharges, lmsMasterDataMap, goNoGoCustomerApplication))
                .locale(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOCALE)))
                .dateFormat(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.DATE_FORMAT)))
                .loanType(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOAN_TYPE)))
                .expectedDisbursementDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .repaymentsStartingFromDate(getRepaymentStartDate(loanCharges.getDisbursedDate()))
//                .repaymentsStartingFromDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getEmiStartDate()))
                .submittedOnDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
//                .submittedOnDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate())) --only for institution Id 4045
                .isTopup(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.IS_TOPUP)))
                .build());

        return sbfclmsIntegrationRequest;
    }

    @Override
    public SBFCLMSIntegrationRequest buildApproveRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, LoanCharges loanCharges, String loanId, Map lmsMasterDataMap) {

        sbfclmsIntegrationRequest.setApproveCoreRequest(ApproveCoreRequest.builder()
                .approvedLoanAmount(String.valueOf(loanCharges.getLoanDetails().getFinalDisbAmt()))
 //               .approvedLoanAmount(String.valueOf(loanCharges.getNetDisbAmt()))
                .approvedOnDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .dateFormat(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.DATE_FORMAT)))
                .disbursementData(new ArrayList<>())
                .expectedDisbursementDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .loanId(loanId)
                .locale(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOCALE)))
                .build());

        return sbfclmsIntegrationRequest;
    }

    @Override
    public SBFCLMSIntegrationRequest buildDisburseRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, LoanCharges loanCharges, String loanId, DisbursementMemo disbursementMemo, Map lmsMasterDataMap) {

        sbfclmsIntegrationRequest.setDisburseCoreRequest(DisburseCoreRequest.builder()
                .accountNumber(disbursementMemo.getRepayment().getRepaymentDetailsList().get(0).getAccountNumber())
//                .accountNumber("")
                .actualDisbursementDate(GngDateUtil.getDDMMYYYYFormat(loanCharges.getDisbursedDate()))
                .bankNumber(disbursementMemo.getRepayment().getRepaymentDetailsList().get(0).getIfscCode())
//                .bankNumber("")
//                .checkNumber(StringUtils.isBlank(disbursementMemo.getDmInputList().get(0).getInstrumentNo()) ? "" :
//                disbursementMemo.getDmInputList().get(0).getInstrumentNo())
                .checkNumber((disbursementMemo.getRepayment().getRepaymentDetailsList().get(0).getChqNumber()))
                .dateFormat(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.DATE_FORMAT)))
                .loanId(loanId)
                .locale(String.valueOf(lmsMasterDataMap.get(LOSLMSIntegrationConstants.LOCALE)))
                .note("")
                .paymentTypeId(StringUtils.equalsIgnoreCase(disbursementMemo.getDmInputList().get(0).getDmPaymentMode(), LOSLMSIntegrationConstants.CHEQUE)? "1" : "")
//                .receiptNumber(disbursementMemo.getRepayment().getRepaymentDetailsList().get(0).getReceipt())
                .receiptNumber("")
                .routingCode("")
                .transactionAmount(String.valueOf(loanCharges.getLoanDetails().getFinalDisbAmt()))
                .build());

        return sbfclmsIntegrationRequest;
    }

    private String getRepaymentStartDate(Date date) {

        String currentDate = GngDateUtil.getDDMMYYYYFormat(date);

        int currentDay = Integer.parseInt(currentDate.substring(0,2));
        int currentMonth = Integer.parseInt(currentDate.substring(2,4));
        int currentYear = Integer.parseInt(currentDate.substring(4));

        int startDay = 5;
        int startMonth = currentMonth;
        int startYear = currentYear;

        if(currentDay <= 20){
            startMonth = currentMonth + 1;
        } else {
            startMonth = currentMonth + 2;
        }

        if (startMonth > 12) {
            startMonth -= 12;
            startYear += 1;
        }

        String day = String.valueOf(startDay);
        String month = String.valueOf(startMonth);

        if (startDay < 10)
            day = "0" + startDay;

        if (startMonth < 10)
            month = "0" + startMonth;

        String repaymentStartDate = day + month + startYear;

        return repaymentStartDate;
    }

}