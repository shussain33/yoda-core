package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.yuho.YuhoRequest;
import com.softcell.gonogo.serialnumbervalidation.yuho.YuhoResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by prasenjit wadmare on 19/11/17.
 */

public interface YuhoSerialNumberValidationBuilder {


    /**
     *
     * @param serialSaleConfirmationRequest
     * @param yuhoConfig
     * @return
     */
    YuhoRequest buildYuhoImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain yuhoConfig);

    /**
     *
     * @param yuhoResponse
     * @return
     */
    SerialNumberResponse buildYuhoImeiNumberResponse(YuhoResponse yuhoResponse);



}
