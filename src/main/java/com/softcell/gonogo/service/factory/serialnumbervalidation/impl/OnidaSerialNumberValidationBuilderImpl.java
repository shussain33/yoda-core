package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.onida.OnidaRequest;
import com.softcell.gonogo.serialnumbervalidation.onida.OnidaResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.OnidaSerialNumberValidationBuilder;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Created by ibrar on 12/12/17.
 */

@Service
public class OnidaSerialNumberValidationBuilderImpl implements OnidaSerialNumberValidationBuilder{

    @Override
    public OnidaRequest buildOnidaSerialNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        return OnidaRequest.builder()
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .build();
    }

    @Override
    public SerialNumberResponse buildOnidaSerialNumberResponse(OnidaResponse onidaResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.ONIDA.toFaceValue())
                .message(GngUtils.convertOnidaResponseCodeToMessage(Integer.parseInt(onidaResponse.getResponseCode())))
                .status((StringUtils.equals("1",onidaResponse.getResponseCode()) ? Status.VALID.name() : Status.INVALID.name()))
                .build();
    }
}
