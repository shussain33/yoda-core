package com.softcell.gonogo.service.factory.impl;


import com.sforce.ws.util.Base64;
import com.softcell.config.AadharConfiguration;
import com.softcell.constants.ConfigurationVersion;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ReqResHelperConstants;
import com.softcell.constants.ValidationPattern;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.kyc.KycHeader;
import com.softcell.gonogo.model.core.kyc.request.aadhar.*;
import com.softcell.gonogo.model.core.kyc.response.aadhar.KycResp;
import com.softcell.gonogo.model.core.kyc.response.aadhar.Resp;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.service.factory.AadharEntityBuilder;
import com.softcell.service.AadharRequestCreater;
import com.softcell.utils.DigitallySignRequest;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.aadharpidblock.AadharResponseReader;
import com.softcell.utils.aadharpidblock.HelperClassAdhar;
import com.softcell.utils.aadharpidblock.v2.AuthAUADataCreatorV2;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @author yogeshb
 */
@Service
public class AadharEntityBuilderImpl implements AadharEntityBuilder {

    private static final Logger logger = LoggerFactory.getLogger(AadharEntityBuilderImpl.class);

    private AadharConfiguration aadharConfiguration;


    @Override
    public AadhaarRequest buildEkycWithBIOSRequest(
            ClientAadhaarRequest aadharRequest) {
        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                .get(aadharRequest.getHeader().getInstitutionId());

        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for this institution: "
                    + aadharRequest.getHeader().getInstitutionId());
            return null;
        }

        /**
         * AadhaarRequest Main Object
         */
        AadhaarRequest aadharMainRequest = new AadhaarRequest();

        /**
         * KycHeader Object
         */
        KycHeader header = new KycHeader();
        header.setApplicationId("GoNoGo_" + aadharRequest.getAadharNumber());
        header.setRequestTime(new Date().toString());
        header.setRequestType("request");
        aadharMainRequest.setHeader(header);
        /**
         * KycRequest object
         */
        KycRequest kycRequest = new KycRequest();

        /**
         * Fix hard coded data
         */

        Metadata metadata = new Metadata();
        metadata.setFingerPrintDeviceCode("NC");
        metadata.setIrisDeviceCode("NA");
        metadata.setPublicIpAddress("NA");
        metadata.setLocationType("P");
        metadata.setLocationValue("411005");
        metadata.setUniqueHostOrTerminalDeviceCode("0201061508");

        PersonalIdentityData personalIdentityData = new PersonalIdentityData();
        personalIdentityData.setTimeStamp(new Date().toString());
        personalIdentityData.setVer("1.0");

        Uses uses = new Uses();
        uses.setBio("y");
        uses.setOtp("n");
        uses.setPa("n");
        uses.setPfa("n");
        uses.setPi("n");
        uses.setPin("n");

        List<BiometricType> biometricTypeList = aadharRequest.getBios();
        Set<String> btSet = new HashSet<>();
        if (biometricTypeList != null) {
            for (BiometricType biometricType : biometricTypeList) {
                btSet.add(biometricType.getType());
            }
            uses.setBt(StringUtils.join(btSet, '|'));
            personalIdentityData.setBios(biometricTypeList);
        } else {
            return null;
        }
        AadharHolderDetails aadharHolderDetails = new AadharHolderDetails();
        /**
         * For eKyc use kyc keyword
         */
        aadharHolderDetails.setKycType("kyc");
        if (StringUtils.isNotBlank(aadharConfiguration.getPdfReportFlag())) {
            aadharHolderDetails.setPdfReport(aadharConfiguration.getPdfReportFlag());
        } else {
            aadharHolderDetails.setPdfReport("N");
        }
        aadharHolderDetails.setVersion("1.6");
        aadharHolderDetails.setTerminalId("public");
        aadharHolderDetails.setTransactionIdentifier("UKC:public:"
                + aadharRequest.getAadharNumber());
        aadharHolderDetails.setAuaCode(aadharConfiguration.getAuaCode());
        aadharHolderDetails.setSubAuaCode(aadharConfiguration.getSubAuaCode());
        aadharHolderDetails.setLicenseKey(aadharConfiguration.getLicenseKey());
        aadharHolderDetails.setPersonalIdentityData(personalIdentityData);
        aadharHolderDetails.setUses(uses);
        aadharHolderDetails.setMetaData(metadata);
        aadharHolderDetails.setAadharNumber(aadharRequest.getAadharNumber());
        kycRequest.setAadharHolderDetails(aadharHolderDetails);

        aadharMainRequest.setHeader(header);
        aadharMainRequest.setKycRequest(kycRequest);
        return aadharMainRequest;
    }

    @Override
    public AadhaarRequest buildOtpRequest(ClientAadhaarRequest clientAadhaarRequest) {
        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                .get(clientAadhaarRequest.getHeader().getInstitutionId());
        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for this institution: "
                    + clientAadhaarRequest.getHeader().getInstitutionId());
            return null;
        }
        String uuid=GngUtils.generateUUID();
        return AadhaarRequest.builder()
                .header(KycHeader.builder()
                        .applicationId("GoNoGo_" + uuid)
                        .requestTime(new Date().toString())
                        .requestType(ReqResHelperConstants.request.name()).build())
                .kycRequest(KycRequest.builder()
                        .otpDetails(OtpDetails.builder()
                                .aadharNumber(clientAadhaarRequest.getAadharNumber())
                                .auaCode(aadharConfiguration.getAuaCode())
                                .subAuaCode(aadharConfiguration.getSubAuaCode())
                                .licenseKey(aadharConfiguration.getLicenseKey())
                                .terminalId("public")
                                .transactionIdentifier(generateTracnsactionIdentifier(uuid))
                                .version("1.6")
                                .otpOption(OtpOption.builder()
                                        .otpChannel("01").build())
                                .build())
                        .build())
                .build();
    }

    @Override
    public AadhaarRequest buildEkycUsingOtpRequestV2(ClientAadhaarRequest clientAadhaarRequest) {

        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                .get(clientAadhaarRequest.getHeader().getInstitutionId());

        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for this institution: "
                    + clientAadhaarRequest.getHeader().getInstitutionId());
            return null;
        }

        String timeStamp = GngDateUtil.toAadharTimeStamp(new Date());

        HelperClassAdhar prepareAUAData  = Cache.auaDataCreator.prepareAUAData(createOtpWithEkycXmlRequest(clientAadhaarRequest.getOtp(), "1.0", timeStamp));

        return AadhaarRequest.builder()
                .header(KycHeader.builder()
                        .applicationId("GoNoGo_" + clientAadhaarRequest.getAadharNumber())
                        .requestTime(new Date().toString())
                        .requestType(ReqResHelperConstants.request.name())
                        .build())
                .kycRequest(KycRequest.builder()
                        .aadharHolderDetails(AadharHolderDetails.builder()
                                .kycType(ReqResHelperConstants.kyc.name())
                                .pdfReport(StringUtils.isNotBlank(aadharConfiguration.getPdfReportFlag()) ? aadharConfiguration.getPdfReportFlag() : "N")
                                .version("1.6")
                                .terminalId("public")
                                .transactionIdentifier("UKC:public:" + clientAadhaarRequest.getAadharNumber() + clientAadhaarRequest.getOtp())
                                .auaCode(aadharConfiguration.getAuaCode())
                                .subAuaCode(aadharConfiguration.getSubAuaCode())
                                .licenseKey(aadharConfiguration.getLicenseKey())
                                .sessionKey(SessionKey.builder()
                                        .certificateID(prepareAUAData.getCertificateIdentifier())
                                        .value(new String(Base64.encode(prepareAUAData.getSessionKeyDetails().getSkeyValue()))).build())
                                .hMac(new String(Base64.encode(prepareAUAData.getEncryptedHmacBytes())))
                                .uses(Uses.builder()
                                        .bio(ReqResHelperConstants.n.name())
                                        .bt(ReqResHelperConstants.n.name())
                                        .otp(ReqResHelperConstants.y.name())
                                        .pa(ReqResHelperConstants.n.name())
                                        .pfa(ReqResHelperConstants.n.name())
                                        .pi(ReqResHelperConstants.n.name())
                                        .pin(ReqResHelperConstants.n.name()).build())
                                .metaData(Metadata.builder()
                                        .fingerPrintDeviceCode(ReqResHelperConstants.NC.name())
                                        .irisDeviceCode(ReqResHelperConstants.NA.name())
                                        .publicIpAddress(ReqResHelperConstants.NA.name())
                                        .locationType(ReqResHelperConstants.P.name())
                                        .locationValue("411005")
                                        .uniqueHostOrTerminalDeviceCode("0201061508").build())
                                .aadharNumber(clientAadhaarRequest.getAadharNumber())
                                .personalIdentityData(PersonalIdentityData.builder()
                                        .timeStamp(timeStamp)
                                        .ver("1.0")
                                        .encryptedPIDBlock(new String(Base64.encode(prepareAUAData.getEncXMLPIDData())))
                                        .build())
                                .build())
                        .build())
                .build();

    }

    @Override
    public AadhaarRequest buildIrisRequest(ClientAadhaarRequest clientAadhaarRequest) {


        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                .get(clientAadhaarRequest.getHeader().getInstitutionId());


        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for  institution: "
                    + clientAadhaarRequest.getHeader().getInstitutionId());
            return null;
        }

        return AadhaarRequest.builder()
                .header(KycHeader.builder()
                        .applicationId("GoNoGo_" + clientAadhaarRequest.getAadharNumber())
                        .requestTime(new Date().toString())
                        .requestType(ReqResHelperConstants.request.name())
                        .build())
                .kycRequest(KycRequest.builder()
                        .aadharHolderDetails(AadharHolderDetails.builder()
                                .version("1.6")
                                .terminalId("public")
                                .transactionIdentifier("UKC:public:" + System.currentTimeMillis())
                                .auaCode(aadharConfiguration.getAuaCode())
                                .subAuaCode(aadharConfiguration.getSubAuaCode())
                                .licenseKey(aadharConfiguration.getLicenseKey())
                                .aadharNumber(clientAadhaarRequest.getAadharNumber())
                                .hMac(clientAadhaarRequest.gethMac())
                                .sessionKey(clientAadhaarRequest.getSessionKey())
                                .kycType(ReqResHelperConstants.kyc.name())
                                .pdfReport(StringUtils.isNotBlank(aadharConfiguration.getPdfReportFlag()) ? aadharConfiguration.getPdfReportFlag() : ReqResHelperConstants.N.name())
                                .uses(Uses.builder()
                                        .bio(ReqResHelperConstants.y.name())
                                        .bt(ReqResHelperConstants.IIR.name())
                                        .otp(ReqResHelperConstants.n.name())
                                        .pa(ReqResHelperConstants.n.name())
                                        .pfa(ReqResHelperConstants.n.name())
                                        .pi(ReqResHelperConstants.n.name())
                                        .pin(ReqResHelperConstants.n.name()).build())
                                .metaData(Metadata.builder()
                                        .fingerPrintDeviceCode(ReqResHelperConstants.NA.name())
                                        .irisDeviceCode(ReqResHelperConstants.NC.name())
                                        .publicIpAddress("127.0.0.1")
                                        .locationType(ReqResHelperConstants.P.name())
                                        .locationValue("411005")
                                        .uniqueHostOrTerminalDeviceCode("11112222").build())
                                .personalIdentityData(PersonalIdentityData.builder()
                                        .timeStamp(GngDateUtil.toAadharTimeStamp(clientAadhaarRequest.getTimeStamp()))
                                        .ver("1.0")
                                        .encryptedPIDBlock(clientAadhaarRequest.getEncryptedPIDBlock())
                                        .build())
                                .build())
                        .build())
                .build();
    }

    @Override
    public AadhaarRequest buildAuthenticationRequest(ApplicationRequest application) {
        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration().get(application.getHeader().getInstitutionId());

        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for this institution: {}", application.getHeader().getInstitutionId());
            return null;
        }

        Applicant applicant = application.getRequest().getApplicant();

        return AadhaarRequest.builder()
                .header(KycHeader.builder()
                        .applicationId(application.getRefID())
                        .requestTime(new Date().toString())
                        .requestType(ReqResHelperConstants.request.name()).build())
                .gngRefId(application.getRefID())
                .kycRequest(KycRequest.builder()
                        .aadharHolderDetails(AadharHolderDetails.builder()
                                .aadharNumber(GngUtils.getKYC(GNGWorkflowConstant.AADHAAR.toFaceValue(), applicant.getKyc(), ValidationPattern.AADHAAR_CARD))
                                .metaData(Metadata.builder()
                                        .fingerPrintDeviceCode(ReqResHelperConstants.NC.name())
                                        .irisDeviceCode(ReqResHelperConstants.NA.name())
                                        .publicIpAddress(ReqResHelperConstants.NA.name())
                                        .locationType(ReqResHelperConstants.P.name())
                                        .locationValue("411005")
                                        .uniqueHostOrTerminalDeviceCode("0201061508")
                                        .build())
                                .uses(Uses.builder()
                                        .bio(ReqResHelperConstants.n.name())
                                        .bt(ReqResHelperConstants.n.name())
                                        .otp(ReqResHelperConstants.n.name())
                                        .pa(ReqResHelperConstants.n.name())
                                        .pfa(ReqResHelperConstants.n.name())
                                        .pi(ReqResHelperConstants.y.name())
                                        .pin(ReqResHelperConstants.n.name())
                                        .build())
                                .personalIdentityData(PersonalIdentityData.builder()
                                        .timeStamp(new Date().toString())
                                        .ver("1.0")
                                        .demo(DemographicDetails.builder()
                                                .personalIdentity(PersonalIdentity.builder()
                                                        .gender((aadharConfiguration.isGenderValidation() && StringUtils.isNotBlank(applicant.getGender())) ?
                                                                GngUtils.getGenderInAadhaarRequestFormat(applicant.getGender()) : null)
                                                        .dateOfBirth((aadharConfiguration.isDobValidation() && StringUtils.isNotBlank(applicant.getGender())) ?
                                                                GngDateUtil.transformDateToAadharFormat(applicant.getDateOfBirth()) : null)
                                                        .age((aadharConfiguration.isAgeValidation() && applicant.getAge() != 0) ?
                                                                String.valueOf(applicant.getAge()) : null)
                                                        .email((aadharConfiguration.isEmailValidation() && applicant.getEmail() != null) ?
                                                                GngUtils.getEmailId(GNGWorkflowConstant.PERSONAL.toFaceValue(), applicant.getEmail()) : null)
                                                        .name((aadharConfiguration.isNameValidation() && applicant.getApplicantName() != null) ?
                                                                GngUtils.convertNameToFullName(applicant.getApplicantName()) : null)
                                                        .matchStrategy((aadharConfiguration.isNameValidation() && applicant.getApplicantName() != null) ?
                                                                ReqResHelperConstants.P.name() : null)
                                                        .matchValue((aadharConfiguration.isNameValidation() && applicant.getApplicantName() != null) ?
                                                                "80" : null)
                                                        .phone((aadharConfiguration.isPhoneValidation() && applicant.getPhone() != null) ?
                                                                GngUtils.getPhoneNumber(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue(), applicant.getPhone()) : null)
                                                        .build())
                                                .build())
                                        .build())
                                .kycType(ReqResHelperConstants.auth.name())
                                .version("1.6")
                                .auaCode(aadharConfiguration.getAuaCode())
                                .subAuaCode(aadharConfiguration.getSubAuaCode())
                                .licenseKey(aadharConfiguration.getLicenseKey())
                                .terminalId("public")
                                .transactionIdentifier("UKC:public:20130315092700201")
                                .build())
                        .build())
                .build();
    }

    @Override
    public AadhaarRequest buildEkycUsingOtpRequestV2_1(ClientAadhaarRequest clientAadhaarRequest) {

        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                .get(clientAadhaarRequest.getHeader().getInstitutionId().concat(ConfigurationVersion.v2_1.name()));

        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for this institution: "
                    + clientAadhaarRequest.getHeader().getInstitutionId());
            return null;
        }

        String timeStamp = GngDateUtil.toAadharTimeStamp(new Date());

        HelperClassAdhar prepareAUAData  = new AuthAUADataCreatorV2()
                .prepareAUADataV2(createOtpWithEkycXmlRequest(clientAadhaarRequest.getOtp(), aadharConfiguration.getAuthApiVersion(), timeStamp));
        String uuid=GngUtils.generateUUID();

        return AadhaarRequest.builder()
                .header(KycHeader.builder()
                        .applicationId("GoNoGo_" + clientAadhaarRequest.getTransactionId().replaceAll("GNG:",""))
                        .requestTime(new Date().toString())
                        .requestType(ReqResHelperConstants.request.name())
                        .build())
                .kycRequest(KycRequest.builder()
                        .aadharHolderDetails(AadharHolderDetails.builder()
                                .kycType(ReqResHelperConstants.kyc.name())
                                .pdfReport(StringUtils.isNotBlank(aadharConfiguration.getPdfReportFlag()) ? aadharConfiguration.getPdfReportFlag() : "N")
                                .version("2.1")
                                .terminalId("public")
                                .transactionIdentifier("UKC:"+clientAadhaarRequest.getTransactionId())
                                .auaCode(aadharConfiguration.getAuaCode())
                                .subAuaCode(aadharConfiguration.getSubAuaCode())
                                .licenseKey(aadharConfiguration.getLicenseKey())
                                .sessionKey(SessionKey.builder()
                                        .certificateID(prepareAUAData.getCertificateIdentifier())
                                        .value(new String(Base64.encode(prepareAUAData.getSessionKeyDetails().getSkeyValue()))).build())
                                .hMac(new String(Base64.encode(prepareAUAData.getEncryptedHmacBytes())))
                                .uses(Uses.builder()
                                        .bio(ReqResHelperConstants.n.name())
                                        .bt(ReqResHelperConstants.n.name())
                                        .otp(ReqResHelperConstants.y.name())
                                        .pa(ReqResHelperConstants.n.name())
                                        .pfa(ReqResHelperConstants.n.name())
                                        .pi(ReqResHelperConstants.n.name())
                                        .pin(ReqResHelperConstants.n.name()).build())
                                .metaData(Metadata.builder()
                                        .fingerPrintDeviceCode(ReqResHelperConstants.NC.name())
                                        .irisDeviceCode(ReqResHelperConstants.NA.name())
                                        .publicIpAddress(ReqResHelperConstants.NA.name())
                                        .locationType(ReqResHelperConstants.P.name())
                                        .locationValue("411005")
                                        .uniqueHostOrTerminalDeviceCode("0201061508").build())
                                .aadharNumber(clientAadhaarRequest.getAadharNumber())
                                .personalIdentityData(PersonalIdentityData.builder()
                                        .timeStamp(timeStamp)
                                        .ver(aadharConfiguration.getAuthApiVersion())
                                        .encryptedPIDBlock(new String(Base64.encode(prepareAUAData.getEncXMLPIDData())))
                                        .build())
                                .build())
                        .build())
                .build();

    }


    @Override
    public String createOTPXml(AadhaarRequest aadhaarRequest, String institutionId) {
        KycRequest kycRequest=aadhaarRequest.getKycRequest();
        OtpDetails otpDetails=kycRequest.getOtpDetails();
        StringBuffer request = new StringBuffer(GNGWorkflowConstant.requestInitializationString);
        request.append("<Otp ");
        request.append(nodeWithAttrCreator("uid",otpDetails.getAadharNumber()));
        request.append(nodeWithAttrCreator("tid",otpDetails.getTerminalId()));
        request.append(nodeWithAttrCreator("ac",otpDetails.getAuaCode()));
        request.append(nodeWithAttrCreator("sa",otpDetails.getSubAuaCode()));
        request.append(nodeWithAttrCreator("ver",otpDetails.getVersion()));
        request.append(nodeWithAttrCreator("txn",otpDetails.getTransactionIdentifier()));
        request.append(nodeWithAttrCreator("lk",otpDetails.getLicenseKey()));

        if(StringUtils.equalsIgnoreCase(otpDetails.getVersion(), "1.6")){
            request.append(nodeWithAttrCreator("ts",new SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss").format(new Date())));
        }

        /*if(StringUtils.isNotBlank(otpDetails.getType())){
            request.append(nodeWithAttrCreator("type",otpDetails.getType()));
        }*/

        request.append(">");
        OtpOption otpOpt = otpDetails.getOtpOption()!=null?otpDetails.getOtpOption():null;
        if(otpOpt != null){
            request.append("<Opts");
            request.append(nodeWithAttrCreator(" ch",otpOpt.getOtpChannel()));
            request.append("/>");
        }

//			if(otpDetails.getSignature() != null)
//			request.append("<Signature>"+otpDetails.getSignature()+"</Signature>");
        request.append("</Otp>");

        return new String(request);
    }
    @Override
    public String createAadhaarXml(AadhaarRequest aadhaarRequest, String institutionId) {
        AadhaarRequestXML aadhaarRequestXml=new AadhaarRequestXML();
        KycRequest kycRequest=aadhaarRequest.getKycRequest();
        AadharHolderDetails aadharJsonDomain=kycRequest.getAadharHolderDetails();

        fetchModelForXMLCreation(aadhaarRequestXml, aadharJsonDomain);
        //object for xml created
        String aadhaarXMLReq=kycXmlRequest_(aadhaarRequestXml);


        return aadhaarXMLReq;
    }

    private void fetchModelForXMLCreation(AadhaarRequestXML aadhaarRequestXml, AadharHolderDetails aadharJsonDomain) {
        if(aadharJsonDomain != null){
            aadhaarRequestXml.setUid(aadharJsonDomain.getAadharNumber());
            aadhaarRequestXml.setAc(aadharJsonDomain.getAuaCode());
            aadhaarRequestXml.setHmac(aadharJsonDomain.gethMac());
            aadhaarRequestXml.setLk(aadharJsonDomain.getLicenseKey());
            aadhaarRequestXml.setSa(aadharJsonDomain.getSubAuaCode());
            /*aadhaarRequestXml.setSignature(aadharJsonDomain.getSignature());*/
            aadhaarRequestXml.setTid(aadharJsonDomain.getTerminalId());
            aadhaarRequestXml.setTxn(aadharJsonDomain.getTransactionIdentifier());
            aadhaarRequestXml.setVer(aadharJsonDomain.getVersion());
            /*aadhaarRequestXml.setPfr(aadharJsonDomain.getPfr());*/

            SKey skey = null;
            if(aadharJsonDomain.getSessionKey() != null){
                skey = new SKey();
                skey.setCi(aadharJsonDomain.getSessionKey().getCertificateID());
                skey.setSkeyValue(aadharJsonDomain.getSessionKey().getValue());
                aadhaarRequestXml.setSkey(skey);
            }

            ResidentToken resToken = null;
            if(aadharJsonDomain.getToken() != null){
                resToken = new ResidentToken();
                resToken.setType(aadharJsonDomain.getToken().getType());
                resToken.setValue(aadharJsonDomain.getToken().getValue());
                aadhaarRequestXml.setTkn(resToken);
            }
            Metadata metaJson = aadharJsonDomain.getMetaData();
            MetadataForDeviceAndTxn meta = null;
            if(metaJson != null){
                meta = new MetadataForDeviceAndTxn();
                meta.setUdc(metaJson.getUniqueHostOrTerminalDeviceCode());
                meta.setFdc(metaJson.getFingerPrintDeviceCode());
                meta.setIdc(metaJson.getIrisDeviceCode());
                meta.setLot(metaJson.getLocationType());
                meta.setLov(metaJson.getLocationValue());
                meta.setPip(metaJson.getPublicIpAddress());
              /*  meta.setCdc(metaJson.getCameradeviceCode);
                meta.setFpmi(metaJson.());
                meta.setFpmc(metaJson);
                meta.setIrmi(metaJson.getIrisDeviceC());
                meta.setIrmc(metaJson.getI());
                meta.setFdmi(metaJson.getFa());
                meta.setFdmc(metaJson.getFa());
*/
                meta.setDpId(metaJson.getDpId());
                meta.setRdsId(metaJson.getRdsId());
                meta.setRdsVer(metaJson.getRdsVer());
                meta.setDc(metaJson.getDc());
                meta.setMc(metaJson.getMc());
                meta.setMi(metaJson.getMi());
                aadhaarRequestXml.setMeta(meta);
            }

            Uses usesJson = aadharJsonDomain.getUses();
            RequestUses uses = null;
            if(usesJson != null){
                uses =new RequestUses();
                uses.setBio(usesJson.getBio());
                uses.setBt(usesJson.getBt());
                uses.setOtp(usesJson.getOtp());
                uses.setPa(usesJson.getPa());
                uses.setPfa(usesJson.getPfa());
                uses.setPi(usesJson.getPi());
                uses.setPin(usesJson.getPin());

                aadhaarRequestXml.setUses(uses);
            }

            PersonalIdData piData = null;

            DemoGraphicData demoData = null;
            UidData uiData = null;

            if(aadharJsonDomain.getPersonalIdentityData()!=null){
                uiData = new UidData();
                piData = new PersonalIdData();
                piData.setTs(aadharJsonDomain.getPersonalIdentityData().getTimeStamp());
                piData.setVer(aadharJsonDomain.getPersonalIdentityData().getVer());
                piData.setEncPidBlock(aadharJsonDomain.getPersonalIdentityData().getEncryptedPIDBlock());

                DemographicDetails demo=aadharJsonDomain.getPersonalIdentityData().getDemo();

                if(demo != null){

                    demoData = new DemoGraphicData();
                    PersonalIdentity pi = null;
                    if(demo.getPersonalIdentity()!=null){
                        pi = new PersonalIdentity();
                        pi.setName(demo.getPersonalIdentity().getName());
                        pi.setLname(demo.getPersonalIdentity().getLname());
                        pi.setGender(demo.getPersonalIdentity().getGender());
                        pi.setAge(demo.getPersonalIdentity().getAge());
                        pi.setDateOfBirth(demo.getPersonalIdentity().getDateOfBirth());
                        pi.setPhone(demo.getPersonalIdentity().getPhone());
                        pi.setEmail(demo.getPersonalIdentity().getEmail());
                        pi.setMatchStrategy(demo.getPersonalIdentity().getMatchStrategy());
                        pi.setMatchValue(demo.getPersonalIdentity().getMatchValue());
                        pi.setDateOfBirthType(demo.getPersonalIdentity().getDateOfBirthType());
                        pi.setLmv(demo.getPersonalIdentity().getLmv());
                        demoData.setPi(pi);
                    }

                    PersonalAddress pAddress = null;
                    if(demo.getPersonalAddress()!=null){

                        pAddress = new PersonalAddress();
                        pAddress.setDist(demo.getPersonalAddress().getDist());
                        pAddress.setHouseNo(demo.getPersonalAddress().getHouseNo());
                        pAddress.setState(demo.getPersonalAddress().getState());
                        pAddress.setStreet(demo.getPersonalAddress().getStreet());
                        pAddress.setVillageTownCity(demo.getPersonalAddress().getVillageTownCity());
                        pAddress.setPostalCode(demo.getPersonalAddress().getPostalCode());
                        pAddress.setCareOf(demo.getPersonalAddress().getCareOf());
                        pAddress.setLandMark(demo.getPersonalAddress().getLandMark());
                        pAddress.setLocation(demo.getPersonalAddress().getLocation());
                        pAddress.setMatchStrategy(demo.getPersonalAddress().getMatchStrategy());
                        pAddress.setPostOffice(demo.getPersonalAddress().getPostOffice());
                        pAddress.setSubDist(demo.getPersonalAddress().getSubDist());
                        demoData.setPa(pAddress);
                    }

                    PersonalFullAddress personalFullAddress= null;
                    if(demo.getPersonalFullAddress() != null){
                        personalFullAddress = new PersonalFullAddress();
                        personalFullAddress.setAddressValue(demo.getPersonalFullAddress().getAddressValue());
                        personalFullAddress.setLav(demo.getPersonalFullAddress().getLav());
                        personalFullAddress.setLmv(demo.getPersonalFullAddress().getLmv());
                        personalFullAddress.setMatchStrategy(demo.getPersonalFullAddress().getMatchStrategy());
                        personalFullAddress.setMatchValue(demo.getPersonalFullAddress().getMatchValue());
                        demoData.setPfa(personalFullAddress);
                    }
                    demoData.setLang(demo.getLang() != null?demo.getLang():null);
                    piData.setDemo(demoData);
                }

                List<BiometricType> bioJson = aadharJsonDomain.getPersonalIdentityData().getBios();

                if(bioJson != null && bioJson.size()>0){
                    BioMetric bios = new BioMetric();
                    List<BioMetricRecord> bioRecordList = new ArrayList<>();
                    for(BiometricType bioMetricRecord : bioJson){
                        BioMetricRecord bioRecord = new BioMetricRecord();
                        bioRecord.setPosh(bioMetricRecord.getPosh());
                        bioRecord.setType(bioMetricRecord.getType());
                        bioRecord.setValue(bioMetricRecord.getValue());
                        /*bioRecord.setBs(bioMetricRecord.get());*/
                        bioRecordList.add(bioRecord);
                    }
                    bios.setBio(bioRecordList);
                    piData.setBios(bios);
                }

                PinValue pinValueJson = aadharJsonDomain.getPersonalIdentityData().getPinValue();
                if(pinValueJson != null){
                    piData.setPv(pinValueJson);
                }

                uiData.setPid(piData);

            }
            aadhaarRequestXml.setData(uiData);
        }
    }

    private String kycXmlRequest_(AadhaarRequestXML kycReq) {

        PersonalIdData pid = kycReq.getData().getPid();
        String timeStamp = new SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss").format(new Date());

        //for direct pid
        if(StringUtils.isNotBlank(pid.getEncPidBlock())){
            timeStamp = pid.getTs();
        }
        //end direct pid

        try{
            if(kycReq != null){

                kycReq.setVer("2.0");
                String aadharRequest = new AadharRequestCreater().aadharXmlRequest(kycReq, timeStamp);
                if(aadharRequest==null||aadharRequest.equalsIgnoreCase("failure")){
                    return "failure";
                }else{
                    try {
                        logger.debug("signing started");
                        aadharRequest = DigitallySignRequest.signRequestStringWithHsm(aadharRequest, true);
                        logger.debug("signing completed");
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        logger.error("{}",e.getStackTrace());
                    }
                }
                logger.debug("signed aadharRequest : "+aadharRequest);
                StringBuffer request = new StringBuffer(GNGWorkflowConstant.requestInitializationString);
//			request.append("<Kyc xmlns=\"http://www.uidai.gov.in/kyc/uid-kyc-request/1.0\"");
                request.append("<Kyc");
                request.append(nodeWithAttrCreator(" ver", "2.1"));
//			request.append(nodeWithAttrCreator("ts",timeStamp));
                request.append(nodeWithAttrCreator("ra", getResidentAutenticationType(kycReq)));
                request.append(nodeWithAttrCreator("rc", "Y"));
                request.append(nodeWithAttrCreator("lr", "N"));
                request.append(nodeWithAttrCreator("de", "N"));
                //request.append(nodeWithAttrCreator("mec", "Y"));

                if(StringUtils.isNotBlank(kycReq.getPfr())){
                    request.append(nodeWithAttrCreator("pfr", kycReq.getPfr()));
                }


                request.append(">");
                request.append("<Rad>");

                request.append(new String(Base64.encode(aadharRequest.getBytes())));
                request.append("</Rad>");

                request.append("</Kyc>");
                String finalTemp=new String(request);
                return finalTemp;
            }
        }catch(Exception e){logger.error("{}",e.getStackTrace());}

        return null;
    }



    private  Resp kycRespElementReader_(String response) {

        JAXBContext jaxbContext;
        javax.xml.bind.Unmarshaller jaxbUnMarshaller;
        Resp responseDomain = null;
        StringReader stringReader = new StringReader(response);

        try {
            jaxbContext = JAXBContext.newInstance(Resp.class);
            jaxbUnMarshaller = jaxbContext.createUnmarshaller();
            responseDomain = (Resp) jaxbUnMarshaller.unmarshal(stringReader);
            return responseDomain;
        } catch (JAXBException e) {
            logger.error("{}",e.getStackTrace());
            return null;
        }
        catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            return null;
        }

    }

    private  KycResp kycResponseReader_(String response) {

        JAXBContext jaxbContext;
        javax.xml.bind.Unmarshaller jaxbUnMarshaller;
        KycResp responseDomain = null;
        StringReader stringReader = new StringReader(response);

        try {
				/*jaxbContext = JAXBContext.newInstance(KycResp.class);
				jaxbUnMarshaller = jaxbContext.createUnmarshaller();
				responseDomain = (KycResp) jaxbUnMarshaller.unmarshal(stringReader);*/

            AadharResponseReader obj = new AadharResponseReader();
            responseDomain=(KycResp)obj.parseKycResponse(KycResp.class, response);
            return responseDomain;
        } catch (JAXBException e) {
            logger.error("{}",e.getStackTrace());
            return null;
        }
        catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            return null;
        }

    }

    private static StringBuffer nodeWithAttrCreator(String attrName , String attrValue){
        StringBuffer buffer = new StringBuffer();

        if(StringUtils.isNotBlank(attrValue)){
            if(StringUtils.isNotBlank(attrName)){
                buffer.append(attrName+"=\""+attrValue+"\" ");
                return buffer;
            }
        }else {
            buffer.append("");
//				buffer.append("</"+attrName+">");
        }
        return buffer;
    }

    private String getResidentAutenticationType(AadhaarRequestXML kycReq){
        String ra=null;
        try{
            if(kycReq != null){
                RequestUses uses = kycReq.getUses();
                if(uses != null){
                    if(StringUtils.equalsIgnoreCase(uses.getBio(), "y")){
                        if(uses.getBt().contains("FMR") || uses.getBt().contains("FIR")){
                            ra="F";
                        }if(uses.getBt().contains("IIR")){
                            if(StringUtils.isBlank(ra)){
                                ra="I";
                            }else{
                                ra=ra+"I";
                            }
                        }
                        //ra="F";
                    }
                    if(StringUtils.equalsIgnoreCase(uses.getPi(), "y") || StringUtils.equalsIgnoreCase(uses.getPa(), "y")
                            || StringUtils.equalsIgnoreCase(uses.getPfa(), "y")){
                        if(StringUtils.isBlank(ra)){
                            ra="I";
                        }else{
                            ra=ra+"I";
                        }
                    }
                    if(StringUtils.equalsIgnoreCase(uses.getOtp(), "y")){
                        if(StringUtils.isBlank(ra)){
                            ra="O";
                        }else{
                            ra=ra+"O";
                        }
                    }

                }
            }
        }catch(Exception e){
            logger.error("{}",e.getStackTrace());
        }
        return ra;
    }

    @Override
    public AadhaarRequest buildOtpRequestV2_1(ClientAadhaarRequest clientAadhaarRequest) {
        {
            aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                    .get(clientAadhaarRequest.getHeader().getInstitutionId().concat(ConfigurationVersion.v2_1.name()));
            if (null == aadharConfiguration) {
                logger.error("Configuration Not Found for this institution: "
                        + clientAadhaarRequest.getHeader().getInstitutionId());
                return null;
            }
            String uuid=GngUtils.generateUUID();

            return AadhaarRequest.builder()
                    .header(KycHeader.builder()
                            .applicationId("GoNoGo_" + uuid)
                            .requestTime(new Date().toString())
                            .requestType(ReqResHelperConstants.request.name()).build())
                    .kycRequest(KycRequest.builder()
                            .otpDetails(OtpDetails.builder()
                                    .aadharNumber(clientAadhaarRequest.getAadharNumber())
                                    .auaCode(aadharConfiguration.getAuaCode())
                                    .subAuaCode(aadharConfiguration.getSubAuaCode())
                                    .licenseKey(aadharConfiguration.getLicenseKey())
                                    .terminalId("public")
                                    .transactionIdentifier(generateTracnsactionIdentifier(uuid))
                                    .version("1.6")
                                    .otpOption(OtpOption.builder()
                                            .otpChannel("01").build())
                                    .build())
                            .build())
                    .build();
        }
    }

    @Override
    public AadhaarRequest buildIrisRequestV2_1(ClientAadhaarRequest clientAadhaarRequest) {


        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                .get(clientAadhaarRequest.getHeader().getInstitutionId().concat(ConfigurationVersion.v2_1.name()));

        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for  institution: "
                    + clientAadhaarRequest.getHeader().getInstitutionId());
            return null;
        }
        String uuid=GngUtils.generateUUID();
        return AadhaarRequest.builder()
                .header(KycHeader.builder()
                        .applicationId("GoNoGo_" + uuid)
                        .requestTime(new Date().toString())
                        .requestType(ReqResHelperConstants.request.name())
                        .build())
                .kycRequest(KycRequest.builder()
                        .aadharHolderDetails(AadharHolderDetails.builder()
                                .version("1.6")
                                .terminalId("public")
                                .transactionIdentifier("UKC:"+generateTracnsactionIdentifier(uuid))
                                .auaCode(aadharConfiguration.getAuaCode())
                                .subAuaCode(aadharConfiguration.getSubAuaCode())
                                .licenseKey(aadharConfiguration.getLicenseKey())
                                .aadharNumber(clientAadhaarRequest.getAadharNumber())
                                .hMac(clientAadhaarRequest.gethMac())
                                .sessionKey(clientAadhaarRequest.getSessionKey())
                                .kycType(ReqResHelperConstants.kyc.name())
                                .pdfReport(StringUtils.isNotBlank(aadharConfiguration.getPdfReportFlag()) ? aadharConfiguration.getPdfReportFlag() : ReqResHelperConstants.N.name())
                                .uses(Uses.builder()
                                        .bio(ReqResHelperConstants.y.name())
                                        .bt(ReqResHelperConstants.IIR.name())
                                        .otp(ReqResHelperConstants.n.name())
                                        .pa(ReqResHelperConstants.n.name())
                                        .pfa(ReqResHelperConstants.n.name())
                                        .pi(ReqResHelperConstants.n.name())
                                        .pin(ReqResHelperConstants.n.name()).build())
                                .metaData(Metadata.builder()
                                        .fingerPrintDeviceCode(ReqResHelperConstants.NA.name())
                                        .irisDeviceCode(ReqResHelperConstants.NC.name())
                                        .publicIpAddress("127.0.0.1")
                                        .locationType(ReqResHelperConstants.P.name())
                                        .locationValue("411005")
                                        .uniqueHostOrTerminalDeviceCode("11112222")
                                        .dpId(clientAadhaarRequest.getDpId())
                                        .dc(clientAadhaarRequest.getDc())
                                        .mc(clientAadhaarRequest.getMc())
                                        .mi(clientAadhaarRequest.getMi())
                                        .rdsId(clientAadhaarRequest.getRdsId())
                                        .rdsVer(clientAadhaarRequest.getRdsVer()).build())
                                .personalIdentityData(PersonalIdentityData.builder()
                                        .timeStamp(GngDateUtil.toAadharTimeStamp(clientAadhaarRequest.getTimeStamp()))
                                        .ver(aadharConfiguration.getAuthApiVersion())
                                        .encryptedPIDBlock(clientAadhaarRequest.getEncryptedPIDBlock())
                                        .build())
                                .build())
                        .build())
                .build();

    }

    @Override
    public AadhaarRequest buildBiometricRequest(ClientAadhaarRequest clientAadhaarRequest) {

        aadharConfiguration = Cache.URL_CONFIGURATION.getAadharConfiguration()
                .get(clientAadhaarRequest.getHeader().getInstitutionId().concat(ConfigurationVersion.v2_1.name()));

        if (null == aadharConfiguration) {
            logger.error("Configuration Not Found for  institution: "
                    + clientAadhaarRequest.getHeader().getInstitutionId());
            return null;
        }
        String uuid=GngUtils.generateUUID();
        return AadhaarRequest.builder()
                .header(KycHeader.builder()
                        .applicationId("GoNoGo_" + uuid)
                        .requestTime(new Date().toString())
                        .requestType(ReqResHelperConstants.request.name())
                        .build())
                .kycRequest(KycRequest.builder()
                        .aadharHolderDetails(AadharHolderDetails.builder()
                 .version("2.0")
                .terminalId("registered")
                .transactionIdentifier("UKC:" + generateTracnsactionIdentifier(uuid))
                .auaCode(aadharConfiguration.getAuaCode())
                .subAuaCode(aadharConfiguration.getSubAuaCode())
                .licenseKey(aadharConfiguration.getLicenseKey())
                .aadharNumber(clientAadhaarRequest.getAadharNumber())
                .hMac(clientAadhaarRequest.gethMac())
                .sessionKey(clientAadhaarRequest.getSessionKey())
                .kycType(ReqResHelperConstants.kyc.name())
                .pdfReport(StringUtils.isNotBlank(aadharConfiguration.getPdfReportFlag()) ? aadharConfiguration.getPdfReportFlag() : ReqResHelperConstants.N.name())
                .uses(Uses.builder()
                 .bio(ReqResHelperConstants.y.name())
                 .bt(ReqResHelperConstants.FMR.name())
                .otp(ReqResHelperConstants.n.name())
                .pa(ReqResHelperConstants.n.name())
                .pfa(ReqResHelperConstants.n.name())
                .pi(ReqResHelperConstants.n.name())
                .pin(ReqResHelperConstants.n.name()).build())
                .metaData(Metadata.builder()
                .fingerPrintDeviceCode(ReqResHelperConstants.NC.name())
                .irisDeviceCode(ReqResHelperConstants.NA.name())
                .publicIpAddress("127.0.0.1")
                .locationType(ReqResHelperConstants.P.name())
                .locationValue("411005")
                .uniqueHostOrTerminalDeviceCode("11112222")
                .dpId(clientAadhaarRequest.getDpId())
                .rdsId(clientAadhaarRequest.getRdsId())
                .rdsVer(clientAadhaarRequest.getRdsVer())
                .dc(clientAadhaarRequest.getDc())
                .mi(clientAadhaarRequest.getMi())
                .mc(clientAadhaarRequest.getMc()).build())
                                .personalIdentityData(PersonalIdentityData.builder()
                .timeStamp(GngDateUtil.toAadharTimeStamp(clientAadhaarRequest.getTimeStamp()))
                .ver(aadharConfiguration.getAuthApiVersion())
                .encryptedPIDBlock(clientAadhaarRequest.getEncryptedPIDBlock())
                .build())
                                .build())
                        .build())
                .build();
    }


    private String createOtpWithEkycXmlRequest(String otp, String version, String timeStamp) {
        StringBuilder otpXml = new StringBuilder();
        otpXml.append("<Pid ts=\"" + timeStamp + "\" ver=\"" + version + "\">");
        otpXml.append("<Pv otp=\"" + otp + "\"/>");
        otpXml.append("</Pid>");
        return otpXml.toString();
    }

    private String generateTracnsactionIdentifier(String uuid) {

        /*String a1 = aadharNumber.substring(0, (aadharNumber.length()/2));*/
        return "GNG:"+uuid;
    }

}
