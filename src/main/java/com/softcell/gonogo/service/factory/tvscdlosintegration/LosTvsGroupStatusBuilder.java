package com.softcell.gonogo.service.factory.tvscdlosintegration;

import com.softcell.gonogo.model.los.tvs.*;

public interface LosTvsGroupStatusBuilder {
    TvsGroupServiceStatus setDefaultGroupStatus();

    TVSCdLosGroupResponse createGroupThreeResponse(String groupName, String action, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupThree insertOrUpdateTvsRecordGroupThree);

    TVSCdLosGroupResponse createGroupOneResponse(String name, String name1, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupOne insertOrUpdateTvsRecordGroupOneRequestJson);

    TVSCdLosGroupResponse createGroupTwoResponse(String name, String name1, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwoRequestJson);

    TVSCdLosGroupResponse createGroupFourResponse(String name, String name1, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupFour insertOrUpdateTvsRecordGroupFourRequestJson);

    TVSCdLosGroupResponse createGroupFiveResponse(String name, String name1, String statusCode, String statusMessage, InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFiveRequestJson);
}
