package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.dooperation.DoOperationLog;
import com.softcell.gonogo.service.factory.DoOperationBuilder;
import com.softcell.utils.GngUtils;
import org.springframework.stereotype.Service;

/**
 * Created by mahesh on 30/8/17.
 */
@Service
public class DoOperationBuilderImpl implements DoOperationBuilder {


    @Override
    public PostIpaRequest buildPostIpaRequest(Header header, String refID) {

        PostIpaRequest postIpaRequest = new PostIpaRequest();
        postIpaRequest.setRefID(refID);
        postIpaRequest.setHeader(GngUtils.getFileHeader(header));
        return postIpaRequest;
    }

    @Override
    public DoOperationLog buildDoOperationLog(String refID, FileHeader header, String doStatus) {

        DoOperationLog doOperationLog = new DoOperationLog();
        doOperationLog.setRefID(refID);
        doOperationLog.setDeliveryOrderStatus(doStatus);
        doOperationLog.setHeader(header);

        return doOperationLog;
    }
}
