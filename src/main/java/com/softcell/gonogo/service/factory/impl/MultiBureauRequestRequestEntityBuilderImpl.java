package com.softcell.gonogo.service.factory.impl;

import com.rits.cloning.Cloner;
import com.softcell.config.MultiBureauConfig;
import com.softcell.constants.*;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.address.PropertyAddress;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.Application;
import com.softcell.gonogo.model.core.Property;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.AddressDomain;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.CommercialRequestResponseDomain;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.IDDomain;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.PhoneDomain;
import com.softcell.gonogo.model.multibureau.pickup.*;
import com.softcell.gonogo.model.multibureau.pickup.Name;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.service.factory.MultiBureauRequestEntityBuilder;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yogeshb
 */
@Service
public class MultiBureauRequestRequestEntityBuilderImpl implements MultiBureauRequestEntityBuilder {

    @Override
    public RequestJsonDomain build(ApplicationRequest applicationRequest) {

        Request request = applicationRequest.getRequest();
        // Applicant Data
        Applicant applicant = request.getApplicant();
        com.softcell.gonogo.model.request.core.Header gonogoHeader = applicationRequest
                .getHeader();
        RequestJsonDomain requestJsonDomain = new RequestJsonDomain();

        Header header = new Header();
        header.setApplicationID(applicationRequest.getRefID());// REVIEW
        header.setRequestType("REQUEST");// review
        header.setConsumerID(applicationRequest.getRefID());
        header.setRequestDate(String.valueOf(new Date()));
        requestJsonDomain.setHeader(header);

        RequestDomain requestDomain = new RequestDomain();
        requestDomain.setInstitutionId(gonogoHeader.getInstitutionId());
        requestDomain.setInquiryStage("PRE-SCREEN");
        requestDomain.setPriority("LOW_PRIORITY");
        requestDomain.setBereauRegion("QA/UAT");
        requestDomain.setJointInd("INDV");
        requestDomain.setSourceSystemName("GONOGO");
        requestDomain.setEnquirySubmittedBy("GoNoGoCore");

        requestDomain.setGender(applicant.getGender());
        requestDomain.setDob(applicant.getDateOfBirth());
        requestDomain.setMaritalStatus(applicant.getMaritalStatus());
        requestDomain.setAge(applicant.getAge());
        requestDomain.setNoOfDependents(String.valueOf(applicant
                .getNoOfDependents()));
        requestDomain.setNoEarningMember(String.valueOf(applicant
                .getNoOfEarningMembers()));
        requestDomain.setNoFamilyMember(String.valueOf(applicant
                .getNoOfFamilyMembers()));

        applicant.getApplicantId();

        com.softcell.gonogo.model.core.Name applicantName = applicant
                .getApplicantName();
        Name name = new Name();

        if (StringUtils.isNotBlank(applicantName.getFirstName())) {
            name.setCustName1(applicantName.getFirstName());
        }
        if (StringUtils.isNotBlank(applicantName.getMiddleName())) {
            name.setCustName2(applicantName.getMiddleName());
        }
        if (StringUtils.isNotBlank(applicantName.getLastName())) {
            name.setCustName3(applicantName.getLastName());
        }
        if (StringUtils.isNotBlank(applicantName.getPrefix())) {
            name.setCustName4(applicantName.getPrefix());
        }
        if (StringUtils.isNotBlank(applicantName.getSuffix())) {
            name.setCustName5(applicantName.getSuffix());
        }
        requestDomain.setName(name);

        List<Employment> employmentList = applicant.getEmployment();
        int employmentCount = 0;
        if (employmentList != null)
            for (Employment employment : employmentList) {
                switch (employmentCount) {
                    case 0: {
                        requestDomain.setTimeWithEmploy(Long.valueOf(employment
                                .getTimeWithEmployer()));
                        requestDomain.setMonthlyIncome(String.valueOf(Math.round(employment
                                .getMonthlySalary())));
                        requestDomain.setEmploymentType(employment
                                .getEmploymentType());
                        requestDomain.setEmployer(employment.getEmploymentName());
                        // requestDomain.setGross_Annual(String.valueOf(employment.getGrossSalary()));
                        employment.getDateOfJoining();
                        employment.getDateOfLeaving();
                        employment.getDesignation();
                        employment.getEmployerBranch();
                        employment.getEmployerCode();
                        employment.getGrossSalary();

                        List<LastMonthIncome> lastMonthIncome = employment
                                .getLastMonthIncome();
                    }
                    break;
                    case 1: {

                    }
                    break;
                    default: {

                    }
                    break;
                }
                employmentCount++;
            }

        List<Kyc> kycList = applicant.getKyc();
        ID id = new ID();
        if (kycList != null)
            for (Kyc kyc : kycList) {
                String kycDocName = kyc.getKycName();
                if (StringUtils.equalsIgnoreCase(kycDocName, "PAN")) {
                    id.setPanNo(kyc.getKycNumber());
                } else if (StringUtils.equalsIgnoreCase(kycDocName, "AADHAAR")) {
                    id.setUidNo(kyc.getKycNumber());
                } else if (StringUtils.equalsIgnoreCase(kycDocName, "PASSPORT")) {
                    id.setPassportNo(kyc.getKycNumber());
                } else if (StringUtils.equalsIgnoreCase(kycDocName, "VOTERID")) {
                    id.setVoterId(kyc.getKycNumber());
                } else if (StringUtils.equalsIgnoreCase(kycDocName, "OTHER")) {
                    id.setOtherId(kyc.getKycNumber());
                }
            }
        requestDomain.setId(id);

        List<Email> emailList = applicant.getEmail();

        int emailCount = 0;
        if (emailList != null)
            for (Email email : emailList) {
                switch (emailCount) {
                    case 0: {
                        requestDomain.setEmailId1(email.getEmailAddress());
                    }
                    break;
                    case 1: {
                        requestDomain.setEmailId2(email.getEmailAddress());
                    }
                    break;
                    default: {

                    }
                    break;
                }
                emailCount++;
            }

        // Address List populate operation
        List<CustomerAddress> addressList = applicant.getAddress();
        List<Address> requestDomainAddressList = new ArrayList<Address>();
        if (addressList != null)
            for (CustomerAddress customerAddress : addressList) {
                Address address = new Address();
                address.setAddress(customerAddress.getAddressLine1() + " , "
                        + customerAddress.getAddressLine2());
                address.setAddressPin(String.valueOf(customerAddress.getPin()));
                address.setAddressState(customerAddress.getState());
                address.setAddressType1(customerAddress.getAddressType());
                address.setCity(customerAddress.getCity());
                requestDomainAddressList.add(address);
            }
        requestDomain.setAddressList(requestDomainAddressList);

        List<Phone> phoneList = applicant.getPhone();
        List<com.softcell.gonogo.model.multibureau.pickup.Phone> mbphoneList = new ArrayList<com.softcell.gonogo.model.multibureau.pickup.Phone>();
        if (phoneList != null)
            for (Phone phoneObj : phoneList) {
                com.softcell.gonogo.model.multibureau.pickup.Phone phone = new com.softcell.gonogo.model.multibureau.pickup.Phone();
                phone.setPhoneNumber(String.valueOf(phoneObj.getPhoneNumber()));
                phone.setPhoneType(phoneObj.getPhoneType());
                phone.setStdCode(phoneObj.getAreaCode());
                mbphoneList.add(phone);
            }
        requestDomain.setPhoneList(mbphoneList);

        Application application = request.getApplication();
        if (application != null) {
            requestDomain.setApplicationId(application.getApplicationID());

            String loanType = Product.getProductNameFromEnumName(application.getLoanType());
            if (StringUtils.isNotBlank(loanType)) {
                requestDomain.setLoanType(loanType);
            }else{
                requestDomain.setLoanType(application.getLoanType());
            }
            requestDomain.setTenure(Long.valueOf(application.getLoanTenor()));
            requestDomain.setEnqAmt((long) application.getLoanAmount());

            requestDomain.setLoanPurposeDesc(application.getAppliedfor());

            Property property = application.getProperty();
            if (property != null) {
                requestDomain.setLocation(property.getLocation());
                requestDomain.setPropertyName(property.getPropertyName());
                requestDomain.setPropertyStatus(property.getStatus());
                requestDomain.setPropertyType(property.getPropertyType());
                requestDomain.setPropertyValue(property.getPropertyValue());
                PropertyAddress propertyAddress = property.getPropertyAddress();
                if (propertyAddress != null) {
                    propertyAddress.getAddressLine1();
                    propertyAddress.getAddressLine2();
                    requestDomain.setPropertyAddress(propertyAddress
                            .getAddressLine1()
                            + " , "
                            + propertyAddress.getAddressLine2()
                            + " , "
                            + propertyAddress.getCity()
                            + " , "
                            + propertyAddress.getCountry()
                            + " , "
                            + propertyAddress.getPin()
                            + " , "
                            + propertyAddress.getState());
                }
            }
            requestJsonDomain.setRequest(requestDomain);
        }
        return requestJsonDomain;
    }

    @Override
    public IssueJsonDomain buildIssueRequest(AcknowledgmentDomain ackJsonDomain) {
        String resFor = "02_04_11_03_12";

        String[] formatArray = StringUtils.splitPreserveAllTokens(resFor, "_");

        return IssueJsonDomain.builder().
                header(
                        Header.builder().
                                applicationID(ackJsonDomain.getHeader().getApplicationID())
                                .consumerID(ackJsonDomain.getHeader().getConsumerID())
                                .requestType(ReqResHelperConstants.ISSUE.name())
                                .dateOfRequest(ackJsonDomain.getHeader().getDateOfRequest())
                                .build())
                .acknowledgmentId(ackJsonDomain.getAcknowledgmentId())
                .responseType(formatArray).build();
    }

    @Override
    public IssueJsonDomain buildCorpIssueRequest(OutputAckDomain outputAckDomain) {
        final String resFor = "02_04_11_03_12";

        String[] formatArray = StringUtils.splitPreserveAllTokens(resFor, "_");

        return IssueJsonDomain.builder().
                header(
                        Header.builder().
                                applicationID(outputAckDomain.getOutputHeader().getApplicationId())
                                .consumerID(outputAckDomain.getOutputHeader().getCustId())
                                .requestType(ReqResHelperConstants.ISSUE.name())
                                .dateOfRequest(outputAckDomain.getOutputHeader().getTimestamp())
                                .build())
                .acknowledgmentId(outputAckDomain.getAcknowledgementId())
                .responseType(formatArray).build();

    }

    @Override
    public RequestJsonDomain buildInitialRequest(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        ApplicationRequest applicationRequest = new Cloner().deepClone(goNoGoCustomerApplication.getApplicationRequest());

        Request request = applicationRequest.getRequest();
        Applicant applicant = request.getApplicant();
        Long branchId = null;

        if (null != applicationRequest.getRequest()
                && null != applicationRequest.getRequest().getApplication()
                && org.apache.commons.lang.StringUtils.isNotBlank(applicationRequest.getRequest().getApplication().getLoanType())) {

            String loanType = applicationRequest.getRequest().getApplication().getLoanType();
            String product = Product.getProductNameFromEnumName(loanType);
            applicationRequest.getRequest().getApplication().setLoanType(product);
        }


        Long timeWithEmployer = 0L;
        String monthlyIncome = null;
        String employmentType = null;
        String employerName = null;

        List<Employment> employmentList = applicant.getEmployment();
        int employmentCount = 0;
        if (employmentList != null && !employmentList.isEmpty()) {
            for (Employment employment : employmentList) {
                switch (employmentCount) {
                    case 0: {
                        timeWithEmployer = Long.valueOf(employment
                                .getTimeWithEmployer());
                        monthlyIncome = String.valueOf(Math.round(employment
                                .getMonthlySalary()));
                        employmentType = employment
                                .getEmploymentType();
                        employerName = employment.getEmploymentName();

                    }
                    break;
                    case 1: {

                    }
                    break;
                    default: {

                    }
                    break;
                }
                employmentCount++;
            }

        }

        String panNumber = null;
        String uidNo = null;
        String passportNo = null;
        String voterId = null;
        String otherId = null;

        List<Kyc> kycList = applicant.getKyc();

        if (kycList != null && !kycList.isEmpty())
            for (Kyc kyc : kycList) {
                String kycDocName = kyc.getKycName();
                if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.PAN.toFaceValue())) {
                    panNumber = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.AADHAAR.toFaceValue())) {
                    uidNo = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.PASSPORT.toFaceValue())) {
                    passportNo = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.VOTER_ID.toFaceValue())) {
                    voterId = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.OTHER.toFaceValue())) {
                    otherId = kyc.getKycNumber();
                }
            }

        String emailId1 = null;
        String emailId2 = null;
        List<Email> emailList = applicant.getEmail();
        int emailCount = 0;
        if (emailList != null && !emailList.isEmpty())
            for (Email email : emailList) {
                switch (emailCount) {
                    case 0: {
                        emailId1 = email.getEmailAddress();
                    }
                    break;
                    case 1: {
                        emailId2 = email.getEmailAddress();
                    }
                    break;
                    default: {

                    }
                    break;
                }
                emailCount++;
            }

        List<Phone> phoneList = applicant.getPhone();

        List<com.softcell.gonogo.model.multibureau.pickup.Phone> phones = phoneList.parallelStream().map(phoneObj -> com.softcell.gonogo.model.multibureau.pickup.Phone.builder()
                .phoneNumber(String.valueOf(phoneObj.getPhoneNumber()))
                .phoneType(phoneObj.getPhoneType())
                .stdCode(phoneObj.getAreaCode()).build()).collect(Collectors.toList());


        List<Address> requestDomainAddressList = applicant.getAddress().parallelStream().map(customerAddress -> Address.builder()
                .address(customerAddress.getAddressLine1() + FieldSeparator.COMMA_SPACE + customerAddress.getAddressLine2())
                .addressPin(String.valueOf(customerAddress.getPin()))
                .addressState(customerAddress.getState())
                .addressType1(customerAddress.getAddressType())
                .city(customerAddress.getCity()).build()).collect(Collectors.toList());


        String applicationId = null;
        String loanType = null;
        long tenure = 0;
        long enqAmt = 0;
        String loanPurposeDesc = null;
        String location = null;
        String propertyName = null;
        String propertyStatus = null;
        String propertyType = null;
        String propertyValue = null;
        String propertyAddress = null;

        Application application = request.getApplication();
        if (application != null) {
            applicationId = application.getApplicationID();
            loanType = application.getLoanType();
            tenure = Long.valueOf(application.getLoanTenor());
            enqAmt = (long) application.getLoanAmount();
            loanPurposeDesc = application.getAppliedfor();

            Property property = application.getProperty();
            if (property != null) {
                location = property.getLocation();
                propertyName = property.getPropertyName();
                propertyStatus = property.getStatus();
                propertyType = property.getPropertyType();
                propertyValue = property.getPropertyValue();
                PropertyAddress propertyAddressObj = property.getPropertyAddress();
                if (propertyAddress != null) {
                    propertyAddress = propertyAddressObj.getAddressLine1()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getAddressLine2()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getCity()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getCountry()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getPin()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getState();
                }
            }
        }

        if(applicationRequest != null && applicationRequest.getAppMetaData() != null
                && applicationRequest.getAppMetaData().getBranchV2() != null
                && applicationRequest.getAppMetaData().getBranchV2().getBranchId() !=null ){

                branchId = new Long(applicationRequest.getAppMetaData().getBranchV2().getBranchId());
        }

        //source system will  set by using urlConfiguaration
        String sourceSystem = MultiBureauConfig.getMbCommunicationDomain(applicationRequest.getHeader().getInstitutionId()).getSourceSystem();

        return RequestJsonDomain.builder()
                .header(Header.builder()
                        .applicationID(goNoGoCustomerApplication.getApplicationRequest().getRefID())
                        .requestType(ReqResHelperConstants.REQUEST.name())
                        .consumerID(goNoGoCustomerApplication.getApplicationRequest().getRefID())
                        .requestDate(String.valueOf(new Date())).build())
                .gngRefId(goNoGoCustomerApplication.getGngRefId())
                .request(RequestDomain.builder()
                        .institutionId(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId())
                        .inquiryStage("PRE-SCREEN")
                        .priority(ReqResHelperConstants.LOW_PRIORITY.name())
                        .bereauRegion("QA/UAT")
                        .jointInd(ReqResHelperConstants.INDV.name())
                        .sourceSystemName(sourceSystem)
                        .enquirySubmittedBy(ReqResHelperConstants.GoNoGoCore.name())
                        .gender(applicant.getGender())
                        .dob(applicant.getDateOfBirth())
                        .maritalStatus(applicant.getMaritalStatus())
                        .age(applicant.getAge())
                        .noOfDependents(String.valueOf(applicant.getNoOfDependents()))
                        .noEarningMember(String.valueOf(applicant.getNoOfEarningMembers()))
                        .noFamilyMember(String.valueOf(applicant.getNoOfFamilyMembers()))
                        .name(Name.builder()
                                .custName1(applicant.getApplicantName().getFirstName())
                                .custName2(applicant.getApplicantName().getMiddleName())
                                .custName3(applicant.getApplicantName().getLastName())
                                .custName4(applicant.getApplicantName().getPrefix())
                                .custName5(applicant.getApplicantName().getSuffix()).build())
                        .timeWithEmploy(timeWithEmployer)
                        .monthlyIncome(monthlyIncome)
                        .employmentType(employmentType)
                        .employer(employerName)
                        .id(ID.builder()
                                .panNo(panNumber)
                                .passportNo(passportNo)
                                .voterId(voterId)
                                .uidNo(uidNo)
                                .OtherId(otherId)
                                .build())
                        .emailId1(emailId1)
                        .emailId2(emailId2)
                        .phoneList(phones)
                        .addressList(requestDomainAddressList)
                        .applicationId(applicationId)
                        .loanType(loanType)
                        .tenure(tenure)
                        .enqAmt(enqAmt)
                        .loanPurposeDesc(loanPurposeDesc)
                        .location(location)
                        .propertyName(propertyName)
                        .propertyStatus(propertyStatus)
                        .propertyType(propertyType)
                        .propertyValue(propertyValue)
                        .propertyAddress(propertyAddress)
                        .originBranch(branchId)
                        .applicantType(applicationRequest.getApplicantType()).build())
                .build();

    }

    @Override
    public RequestJsonDomain buildCoApplicantInitialRequest(ApplicationRequest applicationRequest, Applicant applicant) {

        applicationRequest = new Cloner().deepClone(applicationRequest);
        Request request = applicationRequest.getRequest();

        /*
         *   Transformation applied for loantype need to add it for CoaApplicant As well.
         */
        if (null != applicationRequest.getRequest()
                && null != applicationRequest.getRequest().getApplication()
                && org.apache.commons.lang.StringUtils.isNotBlank(applicationRequest.getRequest().getApplication().getLoanType())) {

            String loanType = applicationRequest.getRequest().getApplication().getLoanType();
            String product = Product.getProductNameFromEnumName(loanType);
            applicationRequest.getRequest().getApplication().setLoanType(product);
        }
        Long timeWithEmployer = 0L;
        String monthlyIncome = null;
        String employmentType = null;
        String employerName = null;

        List<Employment> employmentList = applicant.getEmployment();
        int employmentCount = 0;
        if (employmentList != null && !employmentList.isEmpty()) {
            for (Employment employment : employmentList) {
                switch (employmentCount) {
                    case 0: {
                        timeWithEmployer = Long.valueOf(employment
                                .getTimeWithEmployer());
                        monthlyIncome = String.valueOf(Math.round(employment
                                .getMonthlySalary()));
                        employmentType = employment
                                .getEmploymentType();
                        employerName = employment.getEmploymentName();

                    }
                    break;
                    case 1: {

                    }
                    break;
                    default: {

                    }
                    break;
                }
                employmentCount++;
            }

        }

        String panNumber = null;
        String uidNo = null;
        String passportNo = null;
        String voterId = null;
        String otherId = null;

        List<Kyc> kycList = applicant.getKyc();
        if (kycList != null && !kycList.isEmpty())
            for (Kyc kyc : kycList) {
                String kycDocName = kyc.getKycName();
                if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.PAN.toFaceValue())) {
                    panNumber = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.AADHAAR.toFaceValue())) {
                    uidNo = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.PASSPORT.toFaceValue())) {
                    passportNo = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.VOTER_ID.toFaceValue())) {
                    voterId = kyc.getKycNumber();
                } else if (StringUtils.equalsIgnoreCase(kycDocName, GNGWorkflowConstant.OTHER.toFaceValue())) {
                    otherId = kyc.getKycNumber();
                }
            }

        String emailId1 = null;
        String emailId2 = null;
        List<Email> emailList = applicant.getEmail();
        int emailCount = 0;
        if (emailList != null && !emailList.isEmpty())
            for (Email email : emailList) {
                switch (emailCount) {
                    case 0: {
                        emailId1 = email.getEmailAddress();
                    }
                    break;
                    case 1: {
                        emailId2 = email.getEmailAddress();
                    }
                    break;
                    default: {

                    }
                    break;
                }
                emailCount++;
            }

        List<Phone> phoneList = applicant.getPhone();

        List<com.softcell.gonogo.model.multibureau.pickup.Phone> phones = new ArrayList<>();
        if (phoneList != null && !phoneList.isEmpty())
            for (Phone phoneObj : phoneList) {
                com.softcell.gonogo.model.multibureau.pickup.Phone phone = com.softcell.gonogo.model.multibureau.pickup.Phone.builder()
                        .phoneNumber(String.valueOf(phoneObj.getPhoneNumber()))
                        .phoneType(phoneObj.getPhoneType())
                        .stdCode(phoneObj.getAreaCode()).build();
                phones.add(phone);

            }

        List<CustomerAddress> addressList = applicant.getAddress();
        List<Address> requestDomainAddressList = new ArrayList<>();
        if (addressList != null && !addressList.isEmpty()) {
            for (CustomerAddress customerAddress : addressList) {
                Address address = Address.builder()
                        .address(customerAddress.getAddressLine1() + FieldSeparator.COMMA_SPACE + customerAddress.getAddressLine2())
                        .addressPin(String.valueOf(customerAddress.getPin()))
                        .addressState(customerAddress.getState())
                        .addressType1(customerAddress.getAddressType())
                        .city(customerAddress.getCity()).build();
                requestDomainAddressList.add(address);
            }
        }

        String applicationId = null;
        String loanType = null;
        long tenure = 0;
        long enqAmt = 0;
        String loanPurposeDesc = null;
        String location = null;
        String propertyName = null;
        String propertyStatus = null;
        String propertyType = null;
        String propertyValue = null;
        String propertyAddress = null;

        Application application = applicationRequest.getRequest().getApplication();

        if (application != null) {

            applicationId = application.getApplicationID();

            loanType = application.getLoanType();

            tenure = Long.valueOf(application.getLoanTenor());

            enqAmt = (long) application.getLoanAmount();

            loanPurposeDesc = application.getAppliedfor();

            Property property = application.getProperty();
            if (property != null) {
                location = property.getLocation();
                propertyName = property.getPropertyName();
                propertyStatus = property.getStatus();
                propertyType = property.getPropertyType();
                propertyValue = property.getPropertyValue();
                PropertyAddress propertyAddressObj = property.getPropertyAddress();
                if (propertyAddress != null) {
                    propertyAddress = propertyAddressObj.getAddressLine1()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getAddressLine2()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getCity()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getCountry()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getPin()
                            + FieldSeparator.COMMA_SPACE
                            + propertyAddressObj.getState();
                }
            }
        }

        //source system will  set by using urlConfiguaration
        String sourceSystem = MultiBureauConfig.getMbCommunicationDomain(applicationRequest.getHeader().getInstitutionId()).getSourceSystem();

        return RequestJsonDomain.builder()
                .header(Header.builder()
                        .applicationID(applicationRequest.getRefID())
                        .requestType(ReqResHelperConstants.REQUEST.name())
                        .consumerID(applicationRequest.getRefID())
                        .requestDate(String.valueOf(new Date())).build())
                .gngRefId(applicationRequest.getRefID())
                .coApplicantId(applicant.getApplicantId())
                .request(RequestDomain.builder()
                        .institutionId(applicationRequest.getHeader().getInstitutionId())
                        .inquiryStage("PRE-SCREEN")
                        .priority(ReqResHelperConstants.LOW_PRIORITY.name())
                        .bereauRegion("QA/UAT")
                        .jointInd(ReqResHelperConstants.INDV.name())
                        .sourceSystemName(sourceSystem)
                        .enquirySubmittedBy(ReqResHelperConstants.GoNoGoCore.name())
                        .gender(applicant.getGender())
                        .dob(applicant.getDateOfBirth())
                        .maritalStatus(applicant.getMaritalStatus())
                        .age(applicant.getAge())
                        .noOfDependents(String.valueOf(applicant.getNoOfDependents()))
                        .noEarningMember(String.valueOf(applicant.getNoOfEarningMembers()))
                        .noFamilyMember(String.valueOf(applicant.getNoOfFamilyMembers()))
                        .name(Name.builder()
                                .custName1(applicant.getApplicantName().getFirstName())
                                .custName2(applicant.getApplicantName().getMiddleName())
                                .custName3(applicant.getApplicantName().getLastName())
                                .custName4(applicant.getApplicantName().getPrefix())
                                .custName5(applicant.getApplicantName().getSuffix()).build())
                        .timeWithEmploy(timeWithEmployer)
                        .monthlyIncome(monthlyIncome)
                        .employmentType(employmentType)
                        .employer(employerName)
                        .id(ID.builder()
                                .panNo(panNumber)
                                .passportNo(passportNo)
                                .voterId(voterId)
                                .uidNo(uidNo)
                                .OtherId(otherId)
                                .build())
                        .emailId1(emailId1)
                        .emailId2(emailId2)
                        .phoneList(phones)
                        .addressList(requestDomainAddressList)
                        .applicationId(applicationId)
                        .loanType(loanType)
                        .tenure(tenure)
                        .enqAmt(enqAmt)
                        .loanPurposeDesc(loanPurposeDesc)
                        .location(location)
                        .propertyName(propertyName)
                        .propertyStatus(propertyStatus)
                        .propertyType(propertyType)
                        .propertyValue(propertyValue)
                        .propertyAddress(propertyAddress).build())
                .build();
    }

    @Override
    public CommercialRequestResponseDomain buildCorpRequest(ApplicationRequest applicationRequest, Applicant applicant) throws SystemException {

        String institutionId = applicationRequest.getHeader().getInstitutionId();

        CommercialRequestResponseDomain commercialRequestResponseDomain = new CommercialRequestResponseDomain();
        com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.Header  header = new com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.Header();
        header.setApplicationId(applicationRequest.getRefID());
        header.setRequestType("REQUEST");
        header.setCustomerId(applicant.getApplicantId());
        header.setRequestTime(GngDateUtil.toAadhaarRequest(new Date().getTime()));
        commercialRequestResponseDomain.setHeader(header);

        com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.Request request = new com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.Request();
        request.setLoanAmount(String.valueOf((int)applicationRequest.getRequest().getApplication().getLoanAmount()));
        request.setLoanType(applicationRequest.getRequest().getApplication().getLoanType());
        request.setLoanPurposeDesc(applicationRequest.getRequest().getApplication().getLoanPurpose());
        // This is temporary fix; will be moved to configuration module
        switch (institutionId ) {
            case "4084" : // JFVS
                request.setSourceSystemName("COMM");
                break;
            default :
                request.setSourceSystemName("COMMGNG"); // for SBFC
                break;
        }
        request.setPriority("LOW_PRIORITY");
        request.setBureauRegion("QA/UAT");
        request.setBureauProductType("CIR");
        request.setProductType("CCIR");
        request.setName(GngUtils.convertNameToFullName(applicant.getApplicantName()));
        // TODO : EmployerType has not yet been captured on UI
        if(CollectionUtils.isNotEmpty(applicant.getEmployment())){
            request.setCompanyCategory(applicant.getEmployment().get(0).getEmployerType());
        }
        request.setCompanyCategory("PUBLIC LIMITED");
        // TODO : capture from UI
        request.setNatureOfBusiness("MANUFACTURE OF DOMESTIC APPLIANCES");
        //TODO
        request.setLegalConstitution("");
        request.setClassOfActivity1("ALL OTHER LOANS NOT CLASSIFIED ELSEWHERE OR ACTIVITIES NOT ADEQUATELY DESCRIBED");
        request.setClassOfActivity2("");
        request.setClassOfActivity3("");

        if(CollectionUtils.isNotEmpty(applicant.getAddress())){
            com.softcell.gonogo.model.address.Address address = applicant.getAddress().get(0);
            AddressDomain addressDomain = new AddressDomain();
            addressDomain.setAddress(address.getAddressLine1()+" "+address.getAddressLine2());
            addressDomain.setAddressCity(address.getCity());
            addressDomain.setAddressState(address.getState());
            addressDomain.setAddressPin(String.valueOf(address.getPin()));
            addressDomain.setAddressResidenceCode("");
            addressDomain.setAddressTan("");
            addressDomain.setAddressType("OTHERS");
            List<AddressDomain> addressDomainList =new ArrayList<>();
            addressDomainList.add(addressDomain);
            request.setAddress(addressDomainList);
        }
        if(CollectionUtils.isNotEmpty(applicant.getKyc())) {
            for (Kyc kyc:applicant.getKyc()) {
                if(kyc.getKycName().equalsIgnoreCase(KYC_TYPES.PAN.name())) {
                    IDDomain idDomain = new IDDomain();
                    idDomain.setPan(kyc.getKycNumber());
                    request.setId(idDomain);
                }
                //TODO yogeshd check parameters.
                /*idDomain.setCin("");
                idDomain.setDuns("");
                idDomain.setIdType1("");
                idDomain.setIdType1Value("");
                idDomain.setIdType2("");
                idDomain.setIdType2Value("");*/

            }
        }
        if(CollectionUtils.isNotEmpty(applicant.getPhone())){
            Phone phone =applicant.getPhone().get(0);
            if(phone!= null) {
                PhoneDomain phoneDomain = new PhoneDomain();
                phoneDomain.setPhoneType(phone.getPhoneType());
                phoneDomain.setPhoneNumber(phone.getPhoneNumber());
                phoneDomain.setPhoneExtn(phone.getExtension());
                phoneDomain.setStdCode(phone.getAreaCode());
                request.setPhone(phoneDomain);
            }
        }
        commercialRequestResponseDomain.setRequest(request);
        return commercialRequestResponseDomain;
    }
}
