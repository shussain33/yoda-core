package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by ibrar on 16/11/17.
 */
public interface ComioSerialNumberValidationBuilder {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @param oppoConfig
     * @return
     */
    ComioRequest buildComioImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain oppoConfig);

    /**
     *
     * @param comioResponse
     * @return
     */
    SerialNumberResponse buildComioImeiResponse(ComioResponse comioResponse);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    ComioRequest buildComioImeiRollbackRequest(RollbackRequest rollbackRequest,WFJobCommDomain comioConfig);

    /**
     *
     * @param comioResponse
     * @return
     */
    SerialNumberResponse buildComioImeiRollbackResponse(ComioResponse comioResponse);





}