package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.ApplicationTracking;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.workflow.component.module.ModuleSetting;

/**
 * Application Tracking builder It is use to build application tracking
 * based on passing object i.e Application Request , GonogoCustomerApplication,
 * Executors like (panservice, mb, cibil, sobre etc), Dedupe , Credit Card
 * surogate , Los or Invoice update
 *
 * @author bhuvneshk
 */

public interface ApplicationTrackingBuilder {

    /**
     * Building ApplicationTracking object based on  Application request object
     *
     * @param applicationRequest
     * @return
     */
    public void buildAppTracking(ApplicationRequest applicationRequest, ApplicationTracking applicationTracking);

    /**
     * This is to keep the track of cro decision
     *
     * @param status
     */
    public void buildAppTracking(String status, ApplicationTracking applicationTracking);

    public void buildAppTracking(GoNoGoCustomerApplication goNoGoCustomerApplication, ModuleSetting moduleSetting, ApplicationTracking applicationTracking);

    /**
     * Building ApplicationTracking object based on  Application response object
     * @param goNoGoCustomerApplication
     * @param applicationTracking
     * @return
     */
    public void buildAppTracking(GoNoGoCustomerApplication goNoGoCustomerApplication, ApplicationTracking applicationTracking);
}
