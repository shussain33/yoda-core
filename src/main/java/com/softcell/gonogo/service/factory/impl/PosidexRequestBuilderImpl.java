package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ReqResHelperConstants;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.posidex.*;
import com.softcell.gonogo.service.factory.PosidexRequestBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yogeshb on 21/8/17.
 */
@Service
public class PosidexRequestBuilderImpl implements PosidexRequestBuilder {

    @Override
    public DedupeEnquiryRequest buildInitialRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, WFJobCommDomain posidexConfig) {
        List<AddressList> addressList = null;
        List<ContactList> contactList = null;

        List<CustomerAddress> address = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress();
        List<Phone> phoneList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getPhone();

        if (!CollectionUtils.isEmpty(phoneList)) {
            contactList = new ArrayList<>();
            for (Phone phone : phoneList) {
                ContactList contact = ContactList.builder()
                        .contactNumber(phone.getPhoneNumber())
                        .contactType(GngUtils.convertToPosidexContactCode(phone.getPhoneType()))
                        .build();
                if (null != contact.getContactType()) {
                    contactList.add(contact);
                }

            }
        }

        if (!CollectionUtils.isEmpty(address)) {
            addressList = new ArrayList<>();
            for (CustomerAddress customerAddress : address) {
                AddressList addressObj = AddressList.builder()
                        .addressType(GngUtils.convertToPosidexAddressCode(customerAddress.getAddressType()))
                        .address(StringUtils.join(customerAddress.getAddressLine1(), customerAddress.getAddressLine2(), customerAddress.getLine3()))
                        .country(customerAddress.getCountry())
                        .pincode(String.valueOf(customerAddress.getPin()))
                        .state(customerAddress.getState())
                        .build();
                if (null != addressObj.getAddressType()) {
                    addressList.add(addressObj);
                }
            }
        }

        DedupeEnquiryRequest dedupeEnquiryRequest = DedupeEnquiryRequest.builder()
                .enquiryData(EnquiryData.builder()
                        .firstName(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName().getFirstName())
                        .middleName(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName().getMiddleName())
                        .lastName(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantName().getLastName())
                        .dob(GngDateUtil.transformDateToPosidexFormat(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getDateOfBirth()))
                        .fatherName(GngUtils.convertNameToFullName(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getFatherName()))
                        .gender(GngUtils.getPosidexTranformGender(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getGender()))
                        .aadhaarNumber(GngUtils.getKYCNumber(GNGWorkflowConstant.AADHAAR.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc()))
                        .drivingLicenceNumber(GngUtils.getKYCNumber(GNGWorkflowConstant.DRIVING_LICENSE.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc()))
                        .panCardNumber(GngUtils.getKYCNumber(GNGWorkflowConstant.PAN.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc()))
                        .passportNumber(GngUtils.getKYCNumber(GNGWorkflowConstant.PASSPORT.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc()))
                        .rationCardNumber(GngUtils.getKYCNumber(GNGWorkflowConstant.RATION_CARD.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc()))
                        .addressList(addressList)
                        .contactList(contactList)
                        .applicantId("1")
                        .applicantType(ReqResHelperConstants.BORROWER.name())
                        .registrationNumber("")//not available
                        .product("CD")
                        .enquiryNumber(goNoGoCustomerApplication.getGngRefId())
                        .build())
                .build();

        dedupeEnquiryRequest.setAction(ReqResHelperConstants.I.name());
        dedupeEnquiryRequest.setKey(posidexConfig.getLicenseKey());
        dedupeEnquiryRequest.setName(ReqResHelperConstants.doenquiry.name());

        return dedupeEnquiryRequest;

    }

    @Override
    public GetEnquiryRequest buildIssueRequest(DedupeEnquiryRequest dedupeEnquiryRequest, WFJobCommDomain posidexConfig) {
        GetEnquiryRequest getEnquiryRequest = GetEnquiryRequest.builder()
                .enquiryStatus(EnquiryStatus.builder()
                        .applicantId(dedupeEnquiryRequest.getEnquiryData().getApplicantId())
                        .applicantType(dedupeEnquiryRequest.getEnquiryData().getApplicantType())
                        .enquiryNumber(dedupeEnquiryRequest.getEnquiryData().getEnquiryNumber())
                        .build())
                .build();
        getEnquiryRequest.setAction(ReqResHelperConstants.U.name());
        getEnquiryRequest.setKey(posidexConfig.getLicenseKey());
        getEnquiryRequest.setName(ReqResHelperConstants.getenquirystatus.name());
        return getEnquiryRequest;
    }
}
