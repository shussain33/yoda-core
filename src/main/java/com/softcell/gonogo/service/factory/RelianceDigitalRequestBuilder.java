package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.reliance.RelianceDigitalRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;

/**
 * This is used to build request based on POSTIPA request
 *
 * @author bhuvneshk
 */
public interface RelianceDigitalRequestBuilder {

    /**
     * Build reliance request object based on postIpaRequest
     *
     * @param postIpaRequest
     * @return
     */
    public RelianceDigitalRequest buildRelianceDigitalRequest(PostIpaRequest postIpaRequest, ApplicationRequest appRequest) throws Exception;

    /**
     * Test for connectivity
     * Need to remove when done
     *
     * @param postIpaRequest
     * @return
     */
    public RelianceDigitalRequest buildRelianceDigitalRequestDummyNew(PostIpaRequest postIpaRequest);
}