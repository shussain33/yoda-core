package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.Status;
import com.softcell.gonogo.model.masters.SchemeDetailsDateMapping;
import com.softcell.gonogo.model.request.master.ApplicableSchemeReportDetails;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.ApplicableSchemeDetails;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.UpdateSchemeDateMappingDetailsRequest;
import com.softcell.gonogo.service.factory.MasterDataViewBuilder;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mahesh on 27/3/17.
 */
@Service
public class MasterDataViewBuilderImpl implements MasterDataViewBuilder {

    @Override
    public List<ApplicableSchemeReportDetails> buildApplicableSchemeReportResponse(List<ApplicableSchemeDetails> allApplicableScheme) {

        String date_Format = "dd/MM/yyyy";

        List<ApplicableSchemeReportDetails> applicableSchemeReportDetailsList = new ArrayList<>();

        for (ApplicableSchemeDetails applicableSchemeDetails : allApplicableScheme) {

            String dealerName;
            String cityName;
            String stateName;
            String excludedDate = null;
            String includedDate = null;

            if (!applicableSchemeDetails.isAllStateCityDealer()) {

                dealerName = GngUtils.convertSetToString(applicableSchemeDetails.getDealerName());
                cityName = GngUtils.convertSetToString(applicableSchemeDetails.getCity());
                stateName = GngUtils.convertSetToString(applicableSchemeDetails.getState());

            } else {
                dealerName = Status.ALL.name();
                cityName = Status.ALL.name();
                stateName = Status.ALL.name();

            }
            if (!applicableSchemeDetails.getExcludedDate().isEmpty()) {
                excludedDate = GngDateUtil.convertDateSetToString(applicableSchemeDetails
                        .getExcludedDate());
            }
            if (!applicableSchemeDetails.getIncludedDate().isEmpty()) {
                includedDate = GngDateUtil.convertDateSetToString(applicableSchemeDetails
                        .getIncludedDate());
            }
            applicableSchemeReportDetailsList
                    .add(ApplicableSchemeReportDetails.builder().schemeID(applicableSchemeDetails.getSchemeID())
                            .schemeDesc(applicableSchemeDetails.getSchemeDesc()).schemeStatus(applicableSchemeDetails.getSchemeStatus())
                            .assetCategory(GngUtils.convertSetToString(applicableSchemeDetails.getAssetCategory()))
                            .assetMake(GngUtils.convertSetToString(applicableSchemeDetails.getAssetMake()))
                            .manufacturer(GngUtils.convertSetToString(applicableSchemeDetails.getMaufacturer()))
                            .modelNo(GngUtils.convertSetToString(applicableSchemeDetails
                                    .getModelNo()))
                            .dealerName(dealerName).cityName(cityName).stateName(stateName)
                            .excludedDate(excludedDate)
                            .includedDate(includedDate)
                            .validFromDate(GngDateUtil.changeDateFormat(applicableSchemeDetails.getValidFromDate(), date_Format))
                            .validToDate(GngDateUtil.changeDateFormat(applicableSchemeDetails.getValidToDate(), date_Format))
                            .makeDate(GngDateUtil.changeDateFormat(applicableSchemeDetails.getMakeDate(), date_Format))
                            .makerId(applicableSchemeDetails.getMakerId())
                            .checkerId(applicableSchemeDetails.getCheckerId())
                            .checkDate(GngDateUtil.changeDateFormat(applicableSchemeDetails.getCheckDate(), date_Format))
                            .productFlag(applicableSchemeDetails.getProductFlag())

                            .build());

        }
        return applicableSchemeReportDetailsList;
    }

    @Override
    public SchemeDetailsDateMapping buildSchemeDetailsDateMapping(UpdateSchemeDateMappingDetailsRequest updateSchemeDateMappingDetailsRequest,String schmeDesc , String schemeId, String productAliasName) {

      return  SchemeDetailsDateMapping.builder().schemeID(schemeId).schemeDesc(schmeDesc)
                .excludedDate(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getExcludedDate())
                .includedDate(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getIncludedDate())
                .validFromDate(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getValidFromDate())
                .validToDate(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getValidToDate())
                .makerID(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getMakerID())
                .makeDate(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getMakeDate())
                .schemeStatus(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getSchemeStatus())
                .updateStatus(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().isUpdateStatus())
                .institutionID(updateSchemeDateMappingDetailsRequest.getSchemeDetailsDateMapping().getInstitutionID())
                .productFlag(productAliasName)
                .build();
    }

}
