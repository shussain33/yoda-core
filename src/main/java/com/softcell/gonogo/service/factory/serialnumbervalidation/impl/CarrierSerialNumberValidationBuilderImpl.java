package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierRequest;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.CarrierSerialNumberValidationBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.GngUtils;
import org.springframework.stereotype.Service;

/**
 * Created by ibrar on 28/12/17.
 */

@Service
public class CarrierSerialNumberValidationBuilderImpl implements CarrierSerialNumberValidationBuilder{

    @Override
    public CarrierRequest buildCarrierRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain carrierConfig) {
        return CarrierRequest.builder()
                .serialNo(serialSaleConfirmationRequest.getSerialNumber())
                //make use as a oracle code in carrier.
                .make(serialSaleConfirmationRequest.getMake())
                .serviceUserId(carrierConfig.getLicenseKey())
                .build();
    }

    @Override
    public SerialNumberResponse buildCarrierResponse(CarrierResponse carrierResponse){
        return SerialNumberResponse.builder()
                .vendor(Vendor.CARRIER.toFaceValue())
                .message(GngUtils.convertCarrierResponseCodeToMessage(carrierResponse.getStatusCode()))
                .status((carrierResponse.getStatusCode() == 0 )? Status.VALID.name(): Status.INVALID.name())
                .build();
    }

    @Override
    public CarrierRequest buildCarrierSerialRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain carrierConfig) {
        return CarrierRequest.builder()
                .serialNo(rollbackRequest.getSerialNumber())
                //make use as a oracle code in carrier.
                .make(rollbackRequest.getMake())
                //setStatus 0 for rollback serial number
                .setStatus(0)
                .serviceUserId(carrierConfig.getLicenseKey())
                .build();
    }

    @Override
    public SerialNumberResponse buildCarrierSerialRollbackResponse(CarrierResponse carrierResponse){
        return SerialNumberResponse.builder()
                .vendor(Vendor.CARRIER.toFaceValue())
                .message(GngUtils.convertCarrierRollbackResponseCodeToMessage(carrierResponse.getStatusCode()))
                .status((carrierResponse.getStatusCode() == 0) ? Status.SUCCESS.name() : Status.FAILED.name())
                .build();
    }

}
