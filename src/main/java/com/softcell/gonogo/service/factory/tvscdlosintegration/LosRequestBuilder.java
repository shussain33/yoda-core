package com.softcell.gonogo.service.factory.tvscdlosintegration;

import com.softcell.gonogo.model.los.tvs.LosTvsRequest;
import com.softcell.gonogo.model.request.AppDisbursalRequest;

public interface LosRequestBuilder {
    LosTvsRequest buildLosDisbursalRequest(AppDisbursalRequest appDisbursalRequest);
}
