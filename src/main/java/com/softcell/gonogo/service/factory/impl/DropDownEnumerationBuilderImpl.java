/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 4, 2016 9:33:45 PM
 */
package com.softcell.gonogo.service.factory.impl;


import com.softcell.constants.CacheConstant;
import com.softcell.gonogo.cache.ApplicationMetadataCache;
import com.softcell.gonogo.model.masters.ApplicationMetaData;
import com.softcell.gonogo.model.masters.DropDownEnumeration;
import com.softcell.gonogo.service.factory.DropDownEnumerationBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author kishorp
 */
public class DropDownEnumerationBuilderImpl implements DropDownEnumerationBuilder {

    /* (non-Javadoc)
     * @see com.softcell.gonogo.service.factory.DropDownEnumerationBuilder#build(java.lang.String)
     */
    @Override
    public ApplicationMetaData build(String institutionId) {
        Map<String, Set<String>> enumCache = ApplicationMetadataCache.DROP_DOWN_ENUMERATION.get(institutionId);

        if (enumCache != null) {
            ApplicationMetaData applicationMetaData = new ApplicationMetaData();
            List<DropDownEnumeration> dropDownEnumerationList = new ArrayList<DropDownEnumeration>();
            applicationMetaData.setEnumeration(dropDownEnumerationList);

            DropDownEnumeration dropDownEnumeration;
            if (enumCache.containsKey(CacheConstant.GENDER.toString())) {
                dropDownEnumeration = new DropDownEnumeration();
                dropDownEnumeration
                        .setEnumName(CacheConstant.GENDER.toString());
                dropDownEnumeration.setEnumValues(enumCache
                        .get(CacheConstant.GENDER.toString()));
                dropDownEnumerationList.add(dropDownEnumeration);
            }

            if (enumCache.containsKey(CacheConstant.ADDRESS_TYPE.toString())) {
                dropDownEnumeration = new DropDownEnumeration();
                dropDownEnumeration.setEnumName(CacheConstant.ADDRESS_TYPE
                        .toString());
                dropDownEnumeration.setEnumValues(enumCache
                        .get(CacheConstant.ADDRESS_TYPE.toString()));
                dropDownEnumerationList.add(dropDownEnumeration);
            }
            if (enumCache.containsKey(CacheConstant.KYC_DOCUMENTS.toString())) {
                dropDownEnumeration = new DropDownEnumeration();
                dropDownEnumeration.setEnumName(CacheConstant.KYC_DOCUMENTS
                        .toString());
                dropDownEnumeration.setEnumValues(enumCache
                        .get(CacheConstant.KYC_DOCUMENTS.toString()));
                dropDownEnumerationList.add(dropDownEnumeration);
            }
            if (enumCache.containsKey(CacheConstant.EMPLOY_TYPE.toString())) {
                dropDownEnumeration = new DropDownEnumeration();
                dropDownEnumeration.setEnumName(CacheConstant.EMPLOY_TYPE
                        .toString());
                dropDownEnumeration.setEnumValues(enumCache
                        .get(CacheConstant.EMPLOY_TYPE.toString()));
                dropDownEnumerationList.add(dropDownEnumeration);
            }
            if (enumCache.containsKey(CacheConstant.CONSTITUTION.toString())) {
                dropDownEnumeration = new DropDownEnumeration();
                dropDownEnumeration.setEnumName(CacheConstant.CONSTITUTION
                        .toString());
                dropDownEnumeration.setEnumValues(enumCache
                        .get(CacheConstant.CONSTITUTION.toString()));
                dropDownEnumerationList.add(dropDownEnumeration);
            }
            return applicationMetaData;
        }
        return null;
    }

}
