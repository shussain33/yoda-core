package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioRequest;
import com.softcell.gonogo.serialnumbervalidation.comio.ComioResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.ComioSerialNumberValidationBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Created by ibrar on 16/11/17.
 */
@Service
public class ComioSerialNumberValidationBuilderImpl implements ComioSerialNumberValidationBuilder {

    @Override
    public ComioRequest buildComioImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain comioConfig) {
        return ComioRequest.builder()
                .userName(comioConfig.getMemberId())
                .password(comioConfig.getPassword())
                .imei(serialSaleConfirmationRequest.getImeiNumber())
                .accessKey(comioConfig.getLicenseKey())
                .retailerCode(serialSaleConfirmationRequest.getStoreCode())
                .modelCode(serialSaleConfirmationRequest.getModel())
                //requestType 1 is use for block
                .requestType(String.valueOf(1))
                .build();
    }

    @Override
    public SerialNumberResponse buildComioImeiResponse(ComioResponse comioResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.COMIO.toFaceValue())
                .message(GngUtils.convertComioResponseCodeToMessage(Integer.parseInt(comioResponse.getStatusCode())))
                .status(StringUtils.equals("0",comioResponse.getStatusCode())?Status.VALID.name(): Status.INVALID.name())
                .build();
    }

    @Override
    public ComioRequest buildComioImeiRollbackRequest(RollbackRequest rollbackRequest,WFJobCommDomain comioConfig) {
        return ComioRequest.builder()
                .userName(comioConfig.getMemberId())
                .password(comioConfig.getPassword())
                .accessKey(comioConfig.getLicenseKey())
                .imei(rollbackRequest.getImeiNumber())
                .retailerCode(rollbackRequest.getStoreCode())
                .modelCode(rollbackRequest.getSkuCode())
                //requestType 2 is use for rollback
                .requestType(String.valueOf(2))
                .build();
    }

    @Override
    public SerialNumberResponse buildComioImeiRollbackResponse(ComioResponse comioResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.COMIO.toFaceValue())
                .message(GngUtils.convertComioRollbackResponseCodeToMessage(Integer.parseInt(comioResponse.getStatusCode())))
                .status(StringUtils.equals("0", comioResponse.getStatusCode()) ? Status.SUCCESS.name() : Status.FAILED.name())
                .build();
    }

}