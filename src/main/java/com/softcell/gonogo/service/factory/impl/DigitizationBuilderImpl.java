package com.softcell.gonogo.service.factory.impl;

import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.constants.DoStatusEnum;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Product;
import com.softcell.dao.mongodb.repository.FinalApplicationRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.dao.mongodb.repository.digitization.DigitizationRepository;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.digitization.AchMandateForm;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.InvoiceDetailsRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.service.factory.DigitizationBuilder;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;

/**
 * Created by mahesh on 6/7/17.
 */
@Service
public class DigitizationBuilderImpl implements DigitizationBuilder {

    @Autowired
    private DigitizationRepository digitizationRepository;

    @Autowired
    private FinalApplicationRepository finalApplicationRepository;

    @Autowired
    private UploadFileRepository uploadFileRepository;


    @Override
    public DeliveryOrderReport buildDeliveryOrderReport(PostIpaRequest postIpaRequest ,String fileOperationType) {

        DeliveryOrderReport deliveryOrderReport = new DeliveryOrderReport();

        GoNoGoCustomerApplication goNoGoCustomerApplication = finalApplicationRepository
                .getGoNoGoApplication(postIpaRequest.getRefID());

        if (null != goNoGoCustomerApplication &&
                null != goNoGoCustomerApplication.getApplicationRequest()
                && null != goNoGoCustomerApplication.getApplicationRequest().getRequest()
                && null != goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()) {

            Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();

            String fullName = GngUtils.convertNameToFullName(applicant.getApplicantName());

            deliveryOrderReport.setCustomerName(fullName);

            java.util.List<Phone> phone = applicant.getPhone();
            String phoneNumber = GngUtils.getPhoneNumber(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue(), phone);
            deliveryOrderReport.setMobileNumber(StringUtils.isNotBlank(phoneNumber) ? phoneNumber : FieldSeparator.BLANK);

            java.util.List<CustomerAddress> addresses = applicant.getAddress();

            if (!CollectionUtils.isEmpty(addresses)) {
                for (CustomerAddress address : addresses) {

                    StringBuffer addressBuffer = new StringBuffer();
                    if (StringUtils.isNotBlank(address.getAddressLine1())) {
                        addressBuffer.append(address.getAddressLine1()).append(',')
                                .append(' ');
                    }

                    if (StringUtils.isNotBlank(address.getAddressLine2())) {
                        addressBuffer.append(address.getAddressLine2()).append(',')
                                .append(' ');
                    }

                    if (StringUtils.isNotBlank(address.getLine3())) {
                        addressBuffer.append(address.getLine3()).append(',')
                                .append(' ');
                    }

                    if (StringUtils.isNotBlank(address.getCity())) {
                        addressBuffer.append(address.getCity()).append(',')
                                .append(' ');
                    }

                    addressBuffer.append(" - ").append(address.getPin())
                            .append(',').append(' ');
                    deliveryOrderReport
                            .setCustomerAddress(addressBuffer.toString());

                    break;
                }
            }

        }

        if (postIpaRequest.getPostIPA() != null) {
            PostIPA postIpa = postIpaRequest.getPostIPA();
            deliveryOrderReport.setSumExtWrntAmtInsuranceAmt(postIpa.getSumExtWrntAmtInsuranceAmt());
            deliveryOrderReport.setEmi(postIpa.getEmi());
            deliveryOrderReport.setRefID(postIpaRequest.getRefID());
            deliveryOrderReport.setProductCost(postIpa.getTotalAssetCost());// Pending
            deliveryOrderReport.setProcessingFees(postIpa
                    .getProcessingFees());
            deliveryOrderReport.setMarginMoney(postIpa.getMarginMoney());
            deliveryOrderReport.setAdvanceEMI(postIpa.getAdvanceEmi());
            deliveryOrderReport.setSchemeCode(postIpa.getScheme());
            deliveryOrderReport.setSchemeDscr(postIpa.getSchemeDscr());
            deliveryOrderReport.setSchemeType("("
                    + postIpa.getTenor() + "/"
                    + postIpa.getAdvanceEMITenor() + ")");
            deliveryOrderReport.setDealerSubvention(postIpa
                    .getDealerSubvention());
            deliveryOrderReport.setManufSubBorneByDealer(postIpa
                    .getManufSubBorneByDealer());
            deliveryOrderReport.setManufSubventionMbd(postIpa
                    .getManufSubventionMbd());
            deliveryOrderReport.setFinanceAmount(deliveryOrderReport
                    .getProductCost()
                    - deliveryOrderReport.getMarginMoney());

            //deduction by HDBFS
            deliveryOrderReport.setDeductionsByHDBFS(postIpa
                    .getProcessingFees()
                    + postIpa.getAdvanceEmi()
                    + postIpa.getManufSubBorneByDealer()
                    + postIpa.getDealerSubvention()
                    + postIpa.getOtherChargesIfAny());
            //deduction by Tvsc
            deliveryOrderReport.setDeductionByTvsc(postIpa.getProcessingFees()//1
                    + postIpa.getAdvanceEmi()//2
                    + postIpa.getDealerSubvention()//3
                    + postIpa.getOtherChargesIfAny()//5
                    + postIpa.getSumExtWrntAmtInsuranceAmt());//6

            // sum of deduction for hdbfs
            deliveryOrderReport.setSumOfDeductions(postIpa
                    .getProcessingFees()
                    + postIpa.getAdvanceEmi()
                    + postIpa.getManufSubBorneByDealer()
                    + postIpa.getDealerSubvention()
                    + postIpa.getOtherChargesIfAny());

            // sum of deduction for TVSC
            deliveryOrderReport.setSumOfDeductionForTvsc(postIpa.getProcessingFees()//1
                    + postIpa.getAdvanceEmi()//2
                    + postIpa.getDealerSubvention()//3
                    + postIpa.getOtherChargesIfAny()//5
                    + postIpa.getSumExtWrntAmtInsuranceAmt());//6

            deliveryOrderReport
                    .setFinalDisbursementAmount(deliveryOrderReport
                            .getFinanceAmount()
                            - deliveryOrderReport.getDeductionsByHDBFS());

            deliveryOrderReport.setTotalAmtCollectedFromCustomer(postIpa.getProcessingFees()//1
                    + postIpa.getAdvanceEmi()//2
                    + postIpa.getOtherChargesIfAny()//5
                    +postIpa.getSumExtWrntAmtInsuranceAmt()//6
                    +postIpa.getMarginMoney() );// 1+2+5+6+B+ as per
            // kanhaiya

            deliveryOrderReport.setNetFundingAmount(deliveryOrderReport.getFinanceAmount() - postIpaRequest.getPostIPA().getAdvanceEmi());
            deliveryOrderReport.setInstID(postIpaRequest.getHeader()
                    .getInstitutionId());// added for hdbfs logo purpose.
            //other charges for TVSC purpose
            deliveryOrderReport.setOtherChargesIfAny(postIpa.getOtherChargesIfAny());

            java.util.List<AssetDetails> assetList = postIpaRequest.getPostIPA()
                    .getAssetDetails();
            if (!CollectionUtils.isEmpty(assetList)) {
                for (AssetDetails asset : assetList) {
                    deliveryOrderReport.setProductMake(asset.getAssetModelMake());
                    deliveryOrderReport.setProductBrand(asset.getAssetMake());
                    deliveryOrderReport.setProductCategory(asset.getAssetCtg());
                    deliveryOrderReport.setProductModel(asset.getModelNo());
                    deliveryOrderReport.setDealerName(asset.getDlrName());
                    break;
                }
            }

            if (null != postIpaRequest.getHeader()) {
                if (null != postIpaRequest.getHeader().getProduct()) {
                    setImeiNumberDetails(postIpaRequest.getHeader().getProduct(), deliveryOrderReport, postIpaRequest.getRefID());

                }
            }
            String deliveryOrderName = GngUtils.formDeliveryOrderName(postIpaRequest.getRefID());


            // if file operationType is RESTORED OR CANCELLED ,then we have to preserve the Delivery Order "CREATED" date on pdf.
            // if file operation type is "CREATED" then use current post ipa date
             Date deliveryOrderCreateDate = null;
            if (StringUtils.equals(fileOperationType, DoStatusEnum.RESTORED.name())
                    || StringUtils.equals(fileOperationType, DoStatusEnum.CANCELLED.name())) {

                deliveryOrderCreateDate = uploadFileRepository.getDeliveryOrderCreateDate(postIpaRequest.getRefID(), deliveryOrderName);

            } else {
                deliveryOrderCreateDate = postIpaRequest.getHeader().getDateTime();
            }

            deliveryOrderReport.setDeliveryOrderDate(deliveryOrderCreateDate);


        }

        return deliveryOrderReport;

    }

    private void setImeiNumberDetails(Product product, DeliveryOrderReport deliveryOrderReport, String refID) {

        deliveryOrderReport.setProduct(product);
        if (StringUtils.equals(product.name(), Product.DPL.name())) {
            SerialNumberInfo imeiNumberDetails = digitizationRepository.getImeiNumberDetails(refID);
            if (null != imeiNumberDetails) {
                if (StringUtils.isNotBlank(imeiNumberDetails.getImeiNumber())) {
                    deliveryOrderReport.setImeiNumber(imeiNumberDetails.getImeiNumber());
                }

            }
        }
    }


    public FileUploadRequest buildDeliveryOrderUploadRequest(PostIpaRequest postIpaRequest, String modelNo) {

        UploadFileDetails uploadFileDetails = new UploadFileDetails();
        uploadFileDetails.setFileId(postIpaRequest.getRefID());
        uploadFileDetails.setFileType("application/pdf");
        uploadFileDetails.setFileName("DO_"
                + postIpaRequest.getRefID()
                + "_"
                + modelNo);

        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setGonogoReferanceId(postIpaRequest.getRefID());
        fileUploadRequest.setFileHeader(GngUtils.getFileHeader(postIpaRequest.getHeader()));
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);

        return fileUploadRequest;
    }

    @Override
    public FileUploadRequest buildDigitizationFormUploadRequest(InvoiceDetailsRequest invoiceDetailsRequest, String fileName) {
        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        UploadFileDetails uploadFileDetails = new UploadFileDetails();

        FileHeader fileHeader = GngUtils.getFileHeader(invoiceDetailsRequest.getHeader());

        uploadFileDetails.setFileId(invoiceDetailsRequest.getRefID());
        uploadFileDetails.setFileType("application/pdf");
        uploadFileDetails.setFileName(fileName);

        fileUploadRequest.setGonogoReferanceId(invoiceDetailsRequest.getRefID());
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setUploadFileDetails(uploadFileDetails);


        return fileUploadRequest;
    }

    @Override
    public AchMandateForm buildAchMandateForm(GoNoGoCroApplicationResponse application, PostIPA postIpa, TemplateConfiguration templateConfiguration) throws Exception{
        AchMandateForm achMandateForm = new AchMandateForm();
        //set logo
        achMandateForm.setHdbfsLogo(digitizationRepository.getBase64Image(templateConfiguration.getLogoId()));

        achMandateForm.setTickMark(GngUtils.convertImageToBase64Code("images/tickMark.png"));

        achMandateForm.setScissor(GngUtils.convertImageToBase64Code("images/scissors.png"));
        achMandateForm.setCrossBox(GngUtils.convertImageToBase64Code("images/crossBox.png"));

        achMandateForm.setIndianRupee(GngUtils.convertImageToBase64Code("images/indianCurrency.png"));

        achMandateForm.setApplicationDate(GngDateUtil.getDDMMYYYYFormat(application.getDateTime()));

        if (null != postIpa) {
            achMandateForm.setRevisedEmi(String.format("%.2f", postIpa.getRevisedEmi()));
            achMandateForm.setFinanceAmount(String.format("%.2f", postIpa.getFinanceAmount()));

        }

        if (null != application.getApplicationRequest().getRequest()) {
            Request request = application.getApplicationRequest().getRequest();
            if (null != request.getApplicant()) {
                Applicant applicant = request.getApplicant();
               // achMandateForm.setApplicantName(GngUtils.convertNameToFullName(applicant.getApplicantName()));

                if (!CollectionUtils.isEmpty(applicant.getPhone())) {
                    achMandateForm.setMobileNo(GngUtils.getPhoneNumber(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue(), applicant.getPhone()));
                }
                if (!CollectionUtils.isEmpty(applicant.getEmail())) {
                    achMandateForm.setEmailId(GngUtils.getEmailId(GNGWorkflowConstant.PERSONAL.toFaceValue(), applicant.getEmail()));
                }
                if (!CollectionUtils.isEmpty(applicant.getBankingDetails())) {

                    BankingDetails bankingDetails = applicant.getBankingDetails().get(0);
                    achMandateForm.setAccHolderName(GngUtils.convertNameToFullName(bankingDetails.getAccountHolderName()));
                    achMandateForm.setBankName(bankingDetails.getBankName());
                    achMandateForm.setBanckAccNo(bankingDetails.getAccountNumber());
                    achMandateForm.setIfscCode(bankingDetails.getIfscCode());
                }
            }

        }
        return achMandateForm;
    }
}
