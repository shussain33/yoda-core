package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.softcell.constants.*;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosTvsGroupOneReqBuilder;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LosTvsGroupOneReqBuilderImpl implements LosTvsGroupOneReqBuilder {
    @Override
    public InsertOrUpdateTvsRecordGroupOne buildLosTvsGroupOneRequest(GoNoGoCustomerApplication gonogoCustomerApplication,
                                                                      SerialNumberInfo serialNumberInfo) {


        Logger logger = LoggerFactory.getLogger(LosTvsGroupOneReqBuilderImpl.class);
        PersonalDetails personalDetails = new PersonalDetails();
        AddressResidence addressResidence = new AddressResidence();
        AddressOffice addressOffice = new AddressOffice();
        AadharDetails aadharDetails = new AadharDetails();
        Professionaldetails professionaldetails = new Professionaldetails();
        ContactDetails contactDetails = new ContactDetails();
        CDLos CDLos = new CDLos();
        PanDetails panDetails = new PanDetails();
        Decision decision = new Decision();
        AddressPermanent addressPermanent = new AddressPermanent();
        List<CreditLoanDetails> creditLoanDetailsListReturn = new ArrayList<CreditLoanDetails>();
        try {

            if(gonogoCustomerApplication != null ){

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {

                        if (gonogoCustomerApplication.getApplicationRequest() != null) {
                            if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                                if (gonogoCustomerApplication.getApplicationRequest().getRequest()
                                        .getApplicant() != null) {
                                    if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                            .getAddress())) {

                                        for (int i = 0; i < gonogoCustomerApplication.getApplicationRequest()
                                                .getRequest().getApplicant().getAddress().size(); i++) {
                                            CustomerAddress custAddress = gonogoCustomerApplication
                                                    .getApplicationRequest().getRequest().getApplicant().getAddress()
                                                    .get(i);

                                            if (custAddress.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.AADHAR_ADDRESS.name())) {
                                                aadharDetails.setDistrict(custAddress.getDistrict());
                                                aadharDetails.setLandmark(custAddress.getLandMark());
                                                aadharDetails.setLocality(custAddress.getLocality());

                                            }

                                        }
                                    }
                                }
                            }
                        }

                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc()))
                        {
                            for(int i=0;i<gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc().size();i++) {

                                if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getKyc().get(i).getKycName().equalsIgnoreCase(LosTvsConstants.AADHAR)) {

                                    String getaadharNo = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                            .getApplicant().getKyc().get(i).getKycNumber();
                                    logger.debug("get addhaar number:" + getaadharNo);
                                    if (StringUtils.isNotBlank(getaadharNo)) {

                                        aadharDetails.setAadharnumber(getaadharNo);

                                    }
                                }
                            }
                        }

                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getDateOfBirth())) {
                            String DateOfBirth = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getDateOfBirth();
                            logger.debug(DateOfBirth);
                            if (StringUtils.isNotBlank(DateOfBirth)) {
                                DateTime jodaTime1 = GngDateUtil.convertStringToFormatedDate(DateOfBirth, LosTvsConstants.ddMMyyyy);
                                DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/yyyy");
                                String str = dtfOut.print(jodaTime1);
                                aadharDetails.setDateofbirth(str);
                            }

                        }
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getPhone()
                        )) {
                            String mobileNUmber = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getPhone().get(0).getPhoneNumber();
                           if(StringUtils.isNotBlank(mobileNUmber)){
                                aadharDetails.setMobilenumber(mobileNUmber);
                           }
                        }
                    }
                }
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getFatherName()!=null) {

                            String firstName = "";
                            String middleName = "";
                            String lastName = "";
                            if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getFatherName().getFirstName() != null) {

                                firstName = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                        .getApplicant().getFatherName().getFirstName();
                            }
                            if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getFatherName().getMiddleName() != null) {
                                middleName = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                        .getApplicant().getFatherName().getMiddleName();
                            }
                            if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getFatherName().getLastName() != null) {
                                lastName = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                        .getApplicant().getFatherName().getLastName();
                            }
                            aadharDetails.setFatherspousename(firstName + " " + middleName + " " + lastName);
                        }
                    }
                }
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getBankingDetails())) {
                            if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getBankingDetails().get(0).getAccountHolderName() != null) {
                                aadharDetails.setName(
                                        gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                                .getBankingDetails().get(0).getAccountHolderName().getFirstName());
                            }
                        }
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getAddress())) {
                            for(int i=0;i<gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getAddress().size();i++) {

                                if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getAddress().get(i).getAddressType().equalsIgnoreCase(LosTvsConstants.AADHAR_ADDRESS)) {
                                    aadharDetails.setStreetname(gonogoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getAddress().get(i).getStreet());
                                    aadharDetails.setSubdistrict("");
                                    aadharDetails.setHouseidentifier(gonogoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getAddress().get(i).getLandMark());
                                    aadharDetails.setVillagecity(gonogoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getAddress().get(i).getVillage());
                                    aadharDetails.setDistrict(gonogoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getAddress().get(i).getDistrict());
                                    aadharDetails.setLocality(gonogoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getAddress().get(i).getLocality());
                                    aadharDetails.setLandmark(gonogoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getAddress().get(i).getLandMark());
                                }
                            }
                        }

                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getPhone()
                        )) {
                            contactDetails.setLandline("");

                            if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getPhone())) {

                                for (int i = 0; i < gonogoCustomerApplication.getApplicationRequest().getRequest()
                                        .getApplicant().getPhone().size(); i++) {
                                    Phone phone = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                            .getApplicant().getPhone().get(i);

                                    if (phone != null) {
                                        if (phone.getPhoneType().equalsIgnoreCase(LosTvsPhoneTypeEnum.PERSONAL_MOBILE.name())) {
                                            contactDetails.setMobilenumber(phone.getPhoneNumber());
                                        }
                                        if (phone.getPhoneType().equalsIgnoreCase(LosTvsPhoneTypeEnum.PERSONAL_PHONE.name())) {
                                            contactDetails.setLpgmobilenumber(phone.getPhoneNumber());

                                        }
                                        if (phone.getPhoneType().equalsIgnoreCase(LosTvsPhoneTypeEnum.RESIDENCE_MOBILE.name())) {

                                        }
                                        if (phone.getPhoneType().equalsIgnoreCase(LosTvsPhoneTypeEnum.RESIDENCE_PHONE.name())) {

                                        }
                                        if (phone.getPhoneType().equalsIgnoreCase(LosTvsPhoneTypeEnum.OFFICE_PHONE.name())) {
                                            contactDetails.setOfficenumber(phone.getPhoneNumber());//

                                        }
                                        if (phone.getPhoneType().equalsIgnoreCase(LosTvsPhoneTypeEnum.OFFICE_MOBILE.name())) {

                                        }

                                    }

                                }
                            }

                            String getSTDcode = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getPhone().get(0).getAreaCode();
                            if (StringUtils.isNotBlank(getSTDcode)) {
                                int STDcode = Integer.parseInt(getSTDcode);
                                contactDetails.setStdcode(STDcode);
                            }

                        }

                    }
                }
            }

            if (gonogoCustomerApplication.getApplicantComponentResponse() != null) {
                if (gonogoCustomerApplication.getApplicantComponentResponse().getPanServiceResponse() != null) {
                    if (gonogoCustomerApplication.getApplicantComponentResponse().getPanServiceResponse()
                            .getKycResponse() != null) {
                        if (gonogoCustomerApplication.getApplicantComponentResponse().getPanServiceResponse()
                                .getKycResponse().getPanResponseDetails() != null) {
                            if (gonogoCustomerApplication.getApplicantComponentResponse().getPanServiceResponse()
                                    .getKycResponse().getPanResponseDetails().getPanInfo() != null) {
                                String FirstName = gonogoCustomerApplication.getApplicantComponentResponse()
                                        .getPanServiceResponse().getKycResponse().getPanResponseDetails().getPanInfo()
                                        .getFirstName();
                                if (StringUtils.isNotBlank(FirstName)) {
                                    panDetails.setFirstname(FirstName);
                                }
                                String LastName = gonogoCustomerApplication.getApplicantComponentResponse()
                                        .getPanServiceResponse().getKycResponse().getPanResponseDetails().getPanInfo()
                                        .getLastName();
                                if (StringUtils.isNotBlank(LastName)) {
                                    panDetails.setLastname(LastName);
                                }
                                String UpdatedDate = gonogoCustomerApplication.getApplicantComponentResponse()
                                        .getPanServiceResponse().getKycResponse().getPanResponseDetails().getPanInfo()
                                        .getLastUpdateDate();
                                if (StringUtils.isNotBlank(UpdatedDate)) {
                                    panDetails.setLastupdatedate(UpdatedDate);
                                }

                                panDetails.setPaninfo("");
                                String PanNumber = gonogoCustomerApplication.getApplicantComponentResponse()
                                        .getPanServiceResponse().getKycResponse().getPanResponseDetails().getPanInfo()
                                        .getPanNumber();
                                if (StringUtils.isNotBlank(PanNumber)) {
                                    panDetails.setPannumber(PanNumber);
                                }
                                String PanStatus = gonogoCustomerApplication.getApplicantComponentResponse()
                                        .getPanServiceResponse().getKycResponse().getPanResponseDetails().getPanInfo()
                                        .getPanStatus();
                                if (StringUtils.isNotBlank(PanStatus)) {
                                    panDetails.setPanstatus(PanStatus);
                                }
                                String PanTitle = gonogoCustomerApplication.getApplicantComponentResponse()
                                        .getPanServiceResponse().getKycResponse().getPanResponseDetails().getPanInfo()
                                        .getPanTitle();
                                if (StringUtils.isNotBlank(PanTitle)) {
                                    panDetails.setPantitle(PanTitle);
                                }
                            }
                        }
                    }
                }
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        String getCreditCard = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().getCreditCardLnAccNumbers().get(0);

                        //String str = "";
                        //logger.debug("getCreditCard:::" + getCreditCard);
                        CreditLoanDetails creditLoanDetails = new CreditLoanDetails();

                        if (StringUtils.isNotBlank(getCreditCard)) {
                            int CreditCard = Integer.parseInt(getCreditCard);
                            logger.debug("CreditCardLnAccNumbers String::" + getCreditCard);
                            //creditLoanDetails.setCreditcard(str);CreditCard
                            creditLoanDetails.setCreditcard(CreditCard);
                            creditLoanDetailsListReturn.add(creditLoanDetails);
//                            if (CollectionUtils.isNotEmpty(creditLoanDetailsListReturn)) {
//                                CreditLoanDetails[] creditLoanDetailsArr = creditLoanDetailsList
//                                        .toArray(new CreditLoanDetails[creditLoanDetailsList.size()]);
//                                insertOrUpdateTvsRecordGroupOneReturn.setCreditLoanDetails(creditLoanDetailsArr);
//
//                            }
                        }
                        if (serialNumberInfo != null) {
                            logger.debug("serialNumber in gr001 serviceImpl::" + serialNumberInfo.getSerialNumber());
                            if (StringUtils.isNotBlank(serialNumberInfo.getSerialNumber())) {
                                creditLoanDetails.setSno(serialNumberInfo.getSerialNumber());

                            }
                        }


                    }
                }
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getAddress())) {

                            for (int i = 0; i < gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getAddress().size(); i++) {

                                CustomerAddress custAdd = gonogoCustomerApplication.getApplicationRequest()
                                        .getRequest().getApplicant().getAddress().get(i);

                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.PERMANENT.name())) {

                                    if (StringUtils.isNotBlank(custAdd.getAddressLine1())) {
                                        String AddressLine1 = GngUtils.trimString(custAdd.getAddressLine1());
                                        if (StringUtils.isNotBlank(AddressLine1)) {
                                            addressPermanent.setAddressline1(AddressLine1);
                                        }
                                    }

                                    if (StringUtils.isNotBlank(custAdd.getAddressLine2())) {
                                        String AddressLine2 = GngUtils.trimString(custAdd.getAddressLine2());
                                        if (StringUtils.isNotBlank(AddressLine2)) {
                                            addressPermanent.setAddressline2(AddressLine2);
                                        }
                                    }

                                    if (StringUtils.isNotBlank(custAdd.getCity())) {
                                        addressPermanent.setCity(custAdd.getCity());
                                    }
                                    if (custAdd.getMonthAtAddress() != 0) {
                                        addressPermanent.setMonthataddress(custAdd.getMonthAtAddress());
                                    }

                                    long getPin = custAdd.getPin();
                                    if (getPin != 0) {
                                        int PIN = (int) getPin;
                                        addressPermanent.setPin(PIN);
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getState())) {
                                        addressPermanent.setState(custAdd.getState());
                                    }
                                }

                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.RESIDENCE.name())) {

                                    if (StringUtils.isNotBlank(custAdd.getAddressLine2())) {
                                        String AddressLine2 = GngUtils.trimString(custAdd.getAddressLine2());
                                        if (AddressLine2 != null && AddressLine2 != "") {
                                            addressResidence.setAddressline2(AddressLine2);
                                        }

                                    }

                                }

                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.OFFICE.name())) {
                                    if (StringUtils.isNotBlank(custAdd.getAddressLine2())) {
                                        String AddressLine2 = GngUtils.trimString(custAdd.getAddressLine2());
                                        if (StringUtils.isNotBlank(AddressLine2)) {
                                            addressOffice.setAddressline2(AddressLine2);
                                        }
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getLine3())) {
                                        String AddressLine3 = GngUtils.trimString(custAdd.getLine3());
                                        if (StringUtils.isNotBlank(AddressLine3)) {
                                            addressOffice.setAddressline3(AddressLine3);
                                        }
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getLandMark())) {
                                        addressOffice.setLandmark(custAdd.getLandMark());
                                    }

                                }

                            }
                        }
                    }
                }
            }


                decision.setCrostatus("STP APPROVED");
                decision.setCrouserid("STP APPROVED");


            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getBankingDetails())) {
                            personalDetails.setAadharauthentication(LosTvsFlagEnum.TRUE.name());
                            personalDetails.setBankaccountsince(gonogoCustomerApplication.getApplicationRequest()
                                    .getRequest().getApplicant().getBankingDetails().get(0).getYearHeld());
                        }
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getEmail()
                        )) {
                            personalDetails.setEmail(gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getEmail().get(0).getEmailAddress());
                        }
                        if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getGender()
                                .equalsIgnoreCase(LosTvsPersonalDetailsEnum.male.name())) {
                            personalDetails.setGender(LosTvsPersonalDetailsEnum.male.toValue());
                        } else if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getGender().equalsIgnoreCase(LosTvsPersonalDetailsEnum.female.name())) {
                            personalDetails.setGender(LosTvsPersonalDetailsEnum.female.toValue());
                        } else {
                            personalDetails.setGender(LosTvsPersonalDetailsEnum.other.toValue());
                        }

                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getHasBankAccount())) {
                            if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getHasBankAccount().equalsIgnoreCase(LosTvsHasBankAccountEnums.yes.name())) {
                                personalDetails.setIsbankaccount(LosTvsHasBankAccountEnums.yes.toValue());
                            } else {
                                personalDetails.setIsbankaccount(LosTvsHasBankAccountEnums.no.toValue());
                            }
                        }
                        boolean getCurrentAddress = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().isResidenceAddSameAsAbove();
                        String CurrentAddress = String.valueOf(getCurrentAddress);
                        if (StringUtils.isNotBlank(CurrentAddress)) {
                            if (CurrentAddress.equalsIgnoreCase(LosTvsFlagEnum.TRUE.name())) {
                                personalDetails.setIscurrentaddress(LosTvsIsCurrentAddressEnums.Y.name());
                            } else {
                                personalDetails.setIscurrentaddress(LosTvsIsCurrentAddressEnums.N.name());
                            }
                        }
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getAddress())) {
                            double getLat = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getAddress().get(0).getLatitude();
                            //if (getLat != 0) {
                            String Latitude = Double.toString(getLat);
                            logger.debug("Latitude::"+Latitude);
                            personalDetails.setLat(Latitude);
                            //}
                            double getLongitude = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getAddress().get(0).getLongitude();
                            //if (getLongitude != 0) {
                            String Longitude = Double.toString(getLongitude);
                            logger.debug("Longitude::"+Longitude);
                            personalDetails.setLng(Longitude);
                            //}
                        }

                        if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getApplicantName() != null) {
                            personalDetails.setMiddlename(gonogoCustomerApplication.getApplicationRequest()
                                    .getRequest().getApplicant().getApplicantName().getMiddleName());
                        }
                        personalDetails.setNoofdependant(gonogoCustomerApplication.getApplicationRequest()
                                .getRequest().getApplicant().getNoOfDependents());
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getEmployment())) {
                            personalDetails.setProfile(gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getEmployment().get(0).getConstitution());
                        }
                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getReligion())) {
                            personalDetails.setReligion(gonogoCustomerApplication.getApplicationRequest()
                                    .getRequest().getApplicant().getReligion());
                        }
                        if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getSpouseName() != null) {

                            Name spouseName = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getSpouseName();

                            String firstName = "";
                            String middleName = "";
                            String lastName = "";
                            if (StringUtils.isNotBlank(spouseName.getFirstName())) {

                                firstName = spouseName.getFirstName();
                            }
                            if (StringUtils.isNotBlank(spouseName.getMiddleName())) {
                                middleName = spouseName.getMiddleName();
                            }
                            if (StringUtils.isNotBlank(spouseName.getLastName())) {
                                lastName = spouseName.getLastName();
                            }
                            personalDetails.setSpousename(firstName + " " + middleName + " " + lastName);
                        }

                    }
                }

                personalDetails.setStage(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId());
                personalDetails.setStatus(gonogoCustomerApplication.getApplicationStatus());

                if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                    if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getEmployment()
                    )) {
                        double getTotalearn = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().getEmployment().get(0).getGrossSalary();
                        if (getTotalearn != 0) {
                            int TotalEArning = (int) getTotalearn;
                            personalDetails.setTotalearning(TotalEArning);
                        }
                    }
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null)
                        personalDetails.setTotalfamilymember(gonogoCustomerApplication.getApplicationRequest()
                                .getRequest().getApplicant().getNoOfFamilyMembers());
                }
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getIncomeDetails() != null) {
                            double getMonthlyIncome = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getIncomeDetails().getOtherSrcMnthlyIncmAmt();
                            if (getMonthlyIncome != 0) {
                                int MonthlyIncome = (int) getMonthlyIncome;
                                professionaldetails.setAdditionalmonthlyincome(MonthlyIncome);
                            }
                        }
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getEmail()
                        )) {
                            String emailAdd = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getEmail().get(0).getEmailAddress();
                            if (StringUtils.isNotBlank(emailAdd)) {
                                professionaldetails.setEmail(gonogoCustomerApplication.getApplicationRequest()
                                        .getRequest().getApplicant().getEmail().get(0).getEmailAddress());
                            }

                        }

                    }
                }
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getExternalServiceCalls() != null) {
                    boolean getSaathiDownloaded = gonogoCustomerApplication.getApplicationRequest()
                            .getExternalServiceCalls().isCallSaathi();
                    String SaathiDownloaded = String.valueOf(getSaathiDownloaded);
                    if (SaathiDownloaded.equalsIgnoreCase(LosTvsFlagEnum.TRUE.name())) {
                        professionaldetails.setSaathidowloaded(LosTvsFlagEnum.Y.name());//
                    } else {
                        professionaldetails.setSaathidowloaded(LosTvsFlagEnum.N.name());
                    }
                    logger.debug("setsathi" + professionaldetails.getSaathidowloaded());
                }
            }
            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getIncomeDetails() != null) {
                            double getTotalIncome = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getIncomeDetails().getTotFmlyMnthlyIncmAmt();
                            if (getTotalIncome != 0) {
                                int TotalIncome = (int) getTotalIncome;
                                professionaldetails.setTotalfamilyincome(TotalIncome);
                            }
                        }
                    }
                }
            }
            //////////////////// MANDATORY /////////////////////////////////
            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getAddress())) {

                            for (int i = 0; i < gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getAddress().size(); i++) {
                                CustomerAddress custAdd = gonogoCustomerApplication.getApplicationRequest()
                                        .getRequest().getApplicant().getAddress().get(i);


                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.OFFICE.name())) {

                                    String officeAdd = custAdd.getAddressLine1();
                                    if (StringUtils.isNotBlank(officeAdd)) {
                                        String AddressLine1 = GngUtils.trimString(officeAdd);
                                        if (StringUtils.isNotBlank(AddressLine1)) {
                                            addressOffice.setAddressline1(AddressLine1);
                                        }

                                    }

                                    addressOffice.setCity(custAdd.getCity());
                                    long getPIN = custAdd.getPin();
                                    if (getPIN != 0) {
                                        int pin = (int) getPIN;
                                        addressOffice.setPin(pin);
                                    }
                                    addressOffice.setState(custAdd.getState());

                                }
                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.RESIDENCE.name())) {
                                    if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                            .getAddress().get(0).getAddressLine1())) {

                                        String custResiAdd = custAdd.getAddressLine1();
                                        if (StringUtils.isNotBlank(custResiAdd)) {

                                            String AddressLine1 = GngUtils.trimString(custAdd.getAddressLine1());
                                            if (StringUtils.isNotBlank(AddressLine1)) {
                                                addressResidence.setAddressline1(AddressLine1);
                                            }

                                        }

                                    }

                                    addressResidence.setCity(custAdd.getCity());
                                    long getPin = custAdd.getPin();
                                    if (getPin != 0) {
                                        int PIN = (int) getPin;
                                        addressResidence.setPin(PIN);
                                    }
                                    addressResidence.setState(custAdd.getState());
                                    addressResidence.setMonthataddress(custAdd.getMonthAtAddress());
                                    addressResidence.setResidenceaddresstype(custAdd.getResidenceAddressType());

                                }

                            }

                        }
                    }
                }
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplication() != null) {

                        double getappliedAmm = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplication().getLoanAmount();
                        if (getappliedAmm != 0) {
                            int Croappliedamount = (int) getappliedAmm;
                            decision.setCroappliedamount(Croappliedamount);
                        }
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getCroDecisions())) {
                double getCroapprovedamount = gonogoCustomerApplication.getCroDecisions().get(0).getAmtApproved();
                if (getCroapprovedamount != 0) {
                    int Croapprovedamount = (int) getCroapprovedamount;
                    decision.setCroapprovedamount(Croapprovedamount);
                }
                decision.setCrotenor(gonogoCustomerApplication.getCroDecisions().get(0).getTenor());
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {

                        personalDetails.setAge(gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().getAge());
                    }
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplication() != null) {
                        double getAppliedAmm = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplication().getLoanAmount();
                        if (getAppliedAmm != 0) {
                            int AppliedAmmount = (int) getAppliedAmm;
                            personalDetails.setAppliedamount(AppliedAmmount);
                        }
                    }

                }
                if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                    if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                            .getDateOfBirth())) {
                        {
                            String getDateOfBirth = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getDateOfBirth();

                            if (StringUtils.isNotBlank(getDateOfBirth)) {
                                DateTime jodaTime1 = GngDateUtil.convertStringToFormatedDate(getDateOfBirth,
                                        "ddMMyyyy");
                                DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/yyyy");
                                // Printing the date
                                //String str ="";
                                String str = dtfOut.print(jodaTime1);
                                logger.debug(str);
                                //String str = getDateOfBirth.replace("-","/");
                                personalDetails.setDateofbirth(str);
                            }
                        }
                    }
                }

                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getEmployment())) {
                            personalDetails.setDesignation(gonogoCustomerApplication.getApplicationRequest()
                                    .getRequest().getApplicant().getEmployment().get(0).getDesignation());
                        }
                        personalDetails.setEducation(gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().getEducation());

                        String firstName = "";
                        String middleName = "";
                        String lastName = "";
                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getFatherName().getFirstName())) {

                            firstName = gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getFatherName().getFirstName();
                        }
                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getFatherName().getMiddleName())) {
                            middleName = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getFatherName().getMiddleName();
                        }
                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getFatherName().getLastName())) {
                            lastName = gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getFatherName().getLastName();
                        }
                        personalDetails.setFathername(firstName + " " + middleName + " " + lastName);
                    }
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                            .getApplicantName() != null) {

                        personalDetails.setFirstname(gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().getApplicantName().getFirstName());

                        personalDetails.setLastname(gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().getApplicantName().getLastName());
                    }

                    personalDetails.setMaritalstatus(gonogoCustomerApplication.getApplicationRequest().getRequest()
                            .getApplicant().getMaritalStatus());
                }
            }
            if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplication() != null) {
                personalDetails.setTenor(gonogoCustomerApplication.getApplicationRequest().getRequest()
                        .getApplication().getLoanTenor());
            }

            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (gonogoCustomerApplication.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getEmployment())) {
                            professionaldetails.setYrswithcurrentemployer(
                                    gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                            .getEmployment().get(0).getTimeWithEmployer() / 12);

                            professionaldetails.setMnthswithcurrrentemployer(
                                    gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                            .getEmployment().get(0).getTimeWithEmployer() % 12);

                            if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getEmployment().get(0).getEmploymentName() != null) {
                                String getEmployerName = GngUtils
                                        .trimString(gonogoCustomerApplication.getApplicationRequest().getRequest()
                                                .getApplicant().getEmployment().get(0).getEmploymentName());

                                if (StringUtils.isNotBlank(getEmployerName)) {
                                    professionaldetails.setEmployername(getEmployerName);

                                }
                            }
                            professionaldetails.setEmploymenttype(gonogoCustomerApplication.getApplicationRequest()
                                    .getRequest().getApplicant().getEmployment().get(0).getEmploymentType());
                        }
                        if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                .getIncomeDetails() != null) {
                            double getGross = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                    .getApplicant().getIncomeDetails().getGrossMonthlyAmt();
                            if (getGross != 0) {
                                int Grossmntrhlyincome = (int) getGross;
                                professionaldetails.setGrossmnthlyincome(Grossmntrhlyincome);
                            }
                        }
                    }

                    if (gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                            .getIncomeDetails() != null) {
                        double getNetmonthlyincome = gonogoCustomerApplication.getApplicationRequest().getRequest()
                                .getApplicant().getIncomeDetails().getNetMonthlyAmt();
                        if (getNetmonthlyincome != 0) {
                            int Netmonthlyincome = (int) getNetmonthlyincome;
                            professionaldetails.setNetmonthlyincome(Netmonthlyincome);
                        }
                    }
                }
            }
            CDLos.setAction(LosTvsActionEnums.I.name());
            if (StringUtils.isNotBlank(gonogoCustomerApplication.getGngRefId())) {
                CDLos.setProspectid(gonogoCustomerApplication.getGngRefId());// M
            }
            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                    CDLos.setStage(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId());// M
                }
            }

//            if (CollectionUtils.isNotEmpty(creditLoanDetailsListReturn)) {
//                CreditLoanDetails[] creditLoanDetailsArr = creditLoanDetailsListReturn
//                        .toArray(new CreditLoanDetails[creditLoanDetailsListReturn.size()]);
//                insertOrUpdateTvsRecordGroupOneReturn.setCreditLoanDetails(creditLoanDetailsArr);
//
//            }

            CDLos.setRemarks("");
            CDLos.setVendorid("");
            }

            return InsertOrUpdateTvsRecordGroupOne.builder()
                    .CDLos(CDLos)
                    .addressResidence(addressResidence)
                    .decision(decision)
                    .professionaldetails(professionaldetails)
                    .personalDetails(personalDetails)
                    .aadharDetails(aadharDetails)
                    .addressOffice(addressOffice)
                    .addressPermanent(addressPermanent)
                    .contactDetails(contactDetails)
                    .panDetails(panDetails)
                    .creditLoanDetails(CollectionUtils.isNotEmpty(creditLoanDetailsListReturn)?(creditLoanDetailsListReturn
                            .toArray(new CreditLoanDetails[creditLoanDetailsListReturn.size()])): new CreditLoanDetails[10])
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occured in InsertOrUpdateTvsRecordGroupOne serviceIMPL:",e);
            return null;
        }
    }
}
