package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.*;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.LosRepository;
import com.softcell.dao.mongodb.repository.MasterDataRepository;
import com.softcell.dao.mongodb.repository.digitization.DigitizationRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.ConfigurationRepository;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.kyc.response.pan.PanInfo;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponse;
import com.softcell.gonogo.model.los.LosChargeConfig;
import com.softcell.gonogo.model.los.LosMbData;
import com.softcell.gonogo.model.masters.AssetModelMaster;
import com.softcell.gonogo.model.masters.SupplierLocationMaster;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.InterimStatus;
import com.softcell.gonogo.service.factory.LosRequestBuilder;
import com.softcell.soap.mb.nucleus.ws.los.types.Application;
import com.softcell.soap.mb.nucleus.ws.los.types.Application.Customers;
import com.softcell.soap.mb.nucleus.ws.los.types.Application.Fillers;
import com.softcell.soap.mb.nucleus.ws.los.types.*;
import com.softcell.soap.mb.nucleus.ws.los.types.Customer.CustomerAddresses;
import com.softcell.soap.mb.nucleus.ws.los.types.Customer.CustomerIncomes;
import com.softcell.soap.mb.nucleus.ws.los.types.Customer.CustomerPan;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;

@Scope("prototype")
@Component
public class LosGnGRequestBuilderImplV2 implements LosRequestBuilder {

    Logger logger = LoggerFactory.getLogger(LosGnGRequestBuilderImplV2.class);

    ObjectFactory objectFactory;

    @Autowired
    private LosRepository losMongoRepo;

    @Autowired
    private DigitizationRepository digitizationRepository;

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Autowired
    private ConfigurationRepository configurationRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Override
    public String buildLosCustomerRequest(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            PostIpaRequest postIpaRequest,LosMbData losMbData) throws Exception {

        Header header = null;
        ApplicationRequest appRequest = null;

        if (goNoGoCustomerApplication != null) {

            appRequest = goNoGoCustomerApplication.getApplicationRequest();

            if (appRequest != null) {
                header = appRequest.getHeader();

                logger.debug("checking for dealer access to los push ");

                if (header != null && !losMongoRepo.isValidDealerToPushLos(header.getDealerId(), header.getInstitutionId())) {

                    logger.debug("{} dealer dont have access to los push ", header.getDealerId());


                    throw new Exception("Dealer is not allowed to push data to los");
                }
                logger.debug("Dealer Has access");
            }
        }

        objectFactory = new ObjectFactory();
        LosCustomerRequest request = objectFactory.createLosCustomerRequest();

        Application a = objectFactory.createApplication();

        try {
            setApplicationData(a, goNoGoCustomerApplication, postIpaRequest, losMbData);
        } catch (Exception e) {
            logger.debug("Exception while setting LOS data" + e);
        }

        logger.info("Application is:- " + a.toString());
        request.setApplication(a);
        logger.info("LOS cut Request is:-" + request.toString());
        String requestXml = generateRequestXml(request);
        logger.info("Request XML is:-" + requestXml);
        return requestXml;
    }


    private String generateRequestXml(LosCustomerRequest request)
            throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(LosCustomerRequest.class);
        Marshaller jaxbMarshaller = context.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(request, sw);
        return sw.toString();
    }

    private void setApplicationData(Application app, GoNoGoCustomerApplication goNoGoCustomerApplication , PostIpaRequest postIpaRequest,LosMbData losMbData) {

        float ltvCal = 0;
        app.setTmpApplicationId(goNoGoCustomerApplication.getGngRefId());

        app.setSrcSystem("GONOGO");

        List<CroJustification> croJustifications = goNoGoCustomerApplication.getCroJustification();
        ApplicationRequest appRequest = goNoGoCustomerApplication.getApplicationRequest();
        Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
        Header header = goNoGoCustomerApplication.getApplicationRequest().getHeader();
        PostIPA postIpa = postIpaRequest.getPostIPA();

        if (croJustifications != null && !croJustifications.isEmpty()) {
            CroJustification croJustification = croJustifications.get(0);
            if (croJustification != null) {
                app.setDecisionReason(croJustification.getRemark());
                if (GNGWorkflowConstant.APPROVED.toFaceValue()
                        .equalsIgnoreCase(croJustification.getDecisionCase())) {
                    app.setDecision("A");
                } else if (GNGWorkflowConstant.DECLINED.toFaceValue()
                        .equalsIgnoreCase(croJustification.getDecisionCase())) {
                    app.setDecision("R");
                }
            }
        }

        app.setDecision("");
        app.setDecisionReason("");
        app.setDeviations(null);
        app.setMitigants(null);
        app.setMbInterface("Y");
        //set in losMb data as well may need while pushing the data to MB
        if(null != losMbData)
        losMbData.setLosMbInterfaceFlag("Y");

        app.setPosidexInterface("N");
        app.setFiExecuted("N");
        app.setAppFormNo(goNoGoCustomerApplication.getGngRefId());

        if(header.getProduct().name() == Product.DPL.name()) {
            app.setProduct("DPL");
        }else{
            app.setProduct("CD");
        }


        String promotionScheme = losMongoRepo.getPromotionSchemeBySurrogateAndProduct(GngUtils
                .getSurrogateName(applicant.getSurrogate()), app.getProduct());

        if (StringUtils.isNotBlank(promotionScheme)){
            app.setPromotionalScheme(promotionScheme);
        }else{
            app.setPromotionalScheme("XXXX");
        }
        app.setLoanPurpose("CDP");
        app.setRelationshipManager("");

        if (postIpa != null) {
            //FIXME : changes stage to DDE when deployed production
            float advEmiTenor = postIpa.getAdvanceEMITenor();
            float tenor = postIpa.getTenor();
            app.setLosStage("BDE");
            app.setScheme(postIpa.getScheme());
            //This need to remove before production release
            //app.setScheme("100206");
            app.setLoanAmt(GngUtils.getString(postIpa.getFinanceAmount()));
            app.setTenure(GngUtils.getString(postIpa.getTenor()));
            app.setNoOfAdvEmi(GngUtils.getString(postIpa.getAdvanceEMITenor()));

            if (tenor > 0) {
                ltvCal = advEmiTenor / tenor;
                ltvCal = ltvCal * 100;
            }
            app.setProposedEmi(GngUtils.getString(postIpa.getEmi()));
            app.setMarginMoney(GngUtils.getString(postIpa.getMarginMoney()));
            app.setAssetCost(GngUtils.getString(postIpa
                    .getTotalAssetCost()));
            // get dealer id from dealer email master
            List<AssetDetails> assetDetails = postIpa.getAssetDetails();
            if (assetDetails != null && !assetDetails.isEmpty()) {
                // Taking one as we process only a asset
                AssetDetails asset = assetDetails.get(0);
                app.setSupplier(losMongoRepo.getSupplierIdFromDesc(asset
                        .getDlrName(), header.getInstitutionId()));
                String category = asset.getAssetCtg();
                String modelNo = asset.getModelNo();
                String manufacture = asset.getAssetMake();
                String make = asset.getAssetModelMake();
                AssetModelMaster assetModelDetails = losMongoRepo
                        .getAssetDetails(make, modelNo, manufacture, category, header.getInstitutionId());
                app.setAssetCatg(losMongoRepo
                        .getAssetCatgIdbyCatgDesc(category, header.getInstitutionId()));
                app.setAssetMake(losMongoRepo.getMakeIdbyMakeDesc(make,
                        manufacture, header.getInstitutionId()));
                if (assetModelDetails != null) {
                    app.setAssetModel(assetModelDetails.getModelId());
                    app.setManufacturer(assetModelDetails.getManufacturerId());
                }
            }

            if (StringUtils.isNotBlank(app.getScheme())) {
                app.setCreditProgram(losMongoRepo.getCreditPromotionIdByScheme(app
                        .getScheme(), header.getInstitutionId()));
            } else {
                app.setCreditProgram("503088");
            }
            //If case is STP
            if (StringUtils.isBlank(app.getDecision())) {
                app.setDecision("A");
                //app.setDecisionReason("A");
            }
        } else {
            //If case is not disbursed
            app.setLosStage("CAN");
            com.softcell.gonogo.model.core.Application application = goNoGoCustomerApplication.getApplicationRequest()
                    .getRequest().getApplication();

            app.setLoanAmt(GngUtils.getString(application.getLoanAmount()));
            app.setTenure(GngUtils.getString(application.getLoanTenor()));
            app.setNoOfAdvEmi("0");
            app.setProposedEmi("100");
            app.setMarginMoney("100");
            //FIXME: Im getting error from Asset Cost cannot be less than Loan Amount
            app.setAssetCost("7000");
            app.setSupplier(goNoGoCustomerApplication.getApplicationRequest().getHeader()
                    .getDealerId());
            List<AssetDetails> assetDetails = application.getAsset();
            if (assetDetails != null && !assetDetails.isEmpty()) {
                // Taking one as we process only a asset
                AssetDetails asset = assetDetails.get(0);
                app.setSupplier(losMongoRepo.getSupplierIdFromDesc(asset
                        .getDlrName(), header.getInstitutionId()));
                String category = asset.getAssetCtg();
                String modelNo = asset.getModelNo();
                String manufacture = asset.getAssetMake();
                String make = asset.getAssetModelMake();
                AssetModelMaster assetModelDetails = losMongoRepo
                        .getAssetDetails(make, modelNo, manufacture, category, header.getInstitutionId());
                app.setAssetCatg(losMongoRepo
                        .getAssetCatgIdbyCatgDesc(category, header.getInstitutionId()));
                app.setAssetMake(losMongoRepo.getMakeIdbyMakeDesc(make,
                        manufacture, header.getInstitutionId()));
                app.setAssetModel(assetModelDetails.getModelId());
                app.setManufacturer(assetModelDetails.getManufacturerId());
                app.setScheme("100206");
                app.setCreditProgram("503088");
                //If case is STP
                if (StringUtils.isBlank(app.getDecision())) {
                    app.setDecision("");
                    app.setDecisionReason("DECLINED");
                }
            }
        }

        app.setAssetType("NEW");


        if (StringUtils.isNotBlank(app.getSupplier())) {

            String branchId = losMongoRepo.getBranchByDealerId(app.getSupplier(),
                    app.getProduct(), header.getInstitutionId());
            //Get supplier location master record based on supplierid and branch id.
            SupplierLocationMaster supplierLocationMaster = masterDataRepository.getSupplierLocationMaster(header.getDealerId(), branchId);

            app.setBranch(branchId);

            if (null != supplierLocationMaster)
                app.setSupplierLocation(supplierLocationMaster.getSupplierLocationCode());

            List<String> dsaIds = losMongoRepo.getDSAByBranchId(app.getBranch(), header.getInstitutionId());

            if (dsaIds != null && !dsaIds.isEmpty()) {

                for (String dsa : dsaIds) {

                    String dsaId = losMongoRepo.getDSAByProdcutId(dsa, app.getProduct(), header.getInstitutionId());

                    if (StringUtils.isNotBlank(dsaId)) {

                        app.setDsa(dsaId);
                        break;

                    }
                }

            }
        }


        app.setLtv(GngUtils.getDecimalValue(ltvCal));
        app.setDti("0");

        if (appRequest.getAppMetaData() != null && appRequest.getAppMetaData().getSourcingDetails() != null) {

            app.setFirstSource(appRequest.getAppMetaData().getSourcingDetails().getS1() != null ?
                    appRequest.getAppMetaData().getSourcingDetails().getS1().getValue() : "");
            app.setSecondSource(appRequest.getAppMetaData().getSourcingDetails().getS2() != null
                    && !appRequest.getAppMetaData().getSourcingDetails().getS2().isEmpty()
                    && appRequest.getAppMetaData().getSourcingDetails().getS2().stream().findFirst().isPresent() ?
                    appRequest.getAppMetaData().getSourcingDetails().getS2().stream().findFirst().get().getValue() : "");
            app.setThirdSource(appRequest.getAppMetaData().getSourcingDetails().getS3() != null ?
                    appRequest.getAppMetaData().getSourcingDetails().getS3().getValue() : "");
            app.setFourthSource(appRequest.getAppMetaData().getSourcingDetails().getS4() != null ?
                    appRequest.getAppMetaData().getSourcingDetails().getS4().getValue() : "");
        } else {
            app.setFirstSource("");
            app.setSecondSource("");
            app.setThirdSource("");
            app.setFourthSource("");
        }

        if (appRequest != null && appRequest.getAppMetaData() != null && appRequest.getAppMetaData().getBranchV2() != null) {

            app.setSourceState(losMongoRepo.getStateIdByBranchName(appRequest.getAppMetaData().getBranchV2().getBranchName()));

        }

        if (goNoGoCustomerApplication.getInvoiceDetails() != null) {

            app.setInvoiceNumber(GngUtils.getTruncatedString(goNoGoCustomerApplication.getInvoiceDetails().getInvoiceNumber(), 15));
            app.setInvoiceValue(GngUtils.getString(postIpa
                    .getTotalAssetCost()));
            app.setInvoiceDate(GngDateUtil.getGregorianDate(goNoGoCustomerApplication.getInvoiceDetails().getInvoiceDate()));

            //FIXME : Added for while as HDBFS want to map for CDL from invoice serial number
            //app.setAssetSerialNo(StringUtils.isNotBlank(gng.getInvoiceDetails().getSerialOrImeiNumber())?gng.getInvoiceDetails().getSerialOrImeiNumber() : "");

        } else {
            app.setInvoiceNumber("");
            app.setInvoiceDate(null);
            app.setInvoiceValue("");
        }

        //NEED to update
        SerialNumberInfo imeiNumberDetails = digitizationRepository.getImeiNumberDetails(goNoGoCustomerApplication.getGngRefId());

        if(imeiNumberDetails != null) {

            app.setImei1(StringUtils.isNotBlank(imeiNumberDetails.getImeiNumber())?imeiNumberDetails.getImeiNumber() : "");

            //FIXME : commented for while as HDBFS want to map for CDL from invoice serial number
            app.setAssetSerialNo(StringUtils.isNotBlank(imeiNumberDetails.getSerialNumber()) ? imeiNumberDetails.getSerialNumber() : "");

        }

        app.setAppCreationDate(GngDateUtil.getGregorianDate(goNoGoCustomerApplication.getDateTime()));

        if (goNoGoCustomerApplication.getCroDecisions() != null && !goNoGoCustomerApplication.getCroDecisions().isEmpty()) {

            app.setAppDecisionDate(GngDateUtil.getGregorianDate(goNoGoCustomerApplication.getCroDecisions().get(0).getDecisionUpdateDate()));

        }

        app.setReferences(populateReference(applicant, header));

        app.setPanValidation("N");

        app.setCustomers(setCustomerData(applicant, goNoGoCustomerApplication, appRequest, header, app, losMbData));

        //default value provided by HDB
        app.setLoanPurposeDescription("TEST");

        logger.info("Starting to fill the fillers");
        app.setFillers(setFillerData(goNoGoCustomerApplication, postIpaRequest));
        logger.info("All fillers are filled");

        app.setCharges(populateCharge(goNoGoCustomerApplication));


    }

    private Application.References populateReference(Applicant applicant, Header header) {

        Application.References applicationReferences = objectFactory.createApplicationReferences();

        List<Reference> references = applicationReferences.getReference();

        if (applicant != null && applicant.getApplicantReferences() != null && !applicant.getApplicantReferences().isEmpty()) {

            List<ApplicantReference> applicantReferences = applicant.getApplicantReferences();

            for (ApplicantReference applicantReference : applicantReferences) {

                Reference reference = objectFactory.createReference();

                reference.setReferenceName(GngUtils.convertNameToFullName(applicantReference.getName()));
                reference.setReferenceRelation(losMongoRepo
                        .getValueFromCGPMOnKey1AndDescription("REFRELT", applicantReference.getRelationType()));


                if (applicantReference.getAddress() != null) {

                    reference.setAddress1(applicantReference.getAddress().getAddressLine1());
                    reference.setAddress2(applicantReference.getAddress().getAddressLine2());

                    if(StringUtils.isNotBlank(applicantReference.getAddress().getCity())) {

                        reference.setCity(losMongoRepo
                                .getCityIdByCity(applicantReference.getAddress().getCity(), header.getInstitutionId()));

                    }
                }

                Optional<Phone> phone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_PHONE_TYPE, new ArrayList<>(applicantReference.getPhones()));

                Optional<Phone> mobile = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE, new ArrayList<>(applicantReference.getPhones()));

                if (mobile.isPresent()) {

                    reference.setMobile(mobile.get().getPhoneNumber());

                }



                if (phone.isPresent()) {

                    reference.setStdIsd(phone.get().getAreaCode());

                    if(StringUtils.isNotBlank(phone.get().getPhoneNumber())){
                        reference.setPhone1(phone.get().getPhoneNumber());
                    }else {

                        //Default value to be set mobile if phone1 is not present
                        reference.setPhone1(reference.getMobile());

                    }

                }

                references.add(reference);

            }
        } else {

            references.add(objectFactory.createReference());

        }

        return applicationReferences;
    }

    private Fillers setFillerData(GoNoGoCustomerApplication gng, PostIpaRequest postIpaRequest) {
        logger.info("Started filling fillers");
        Fillers fillers = objectFactory.createApplicationFillers();

        try {
            PostIPA postIpa = postIpaRequest.getPostIPA();
            Header header = gng.getApplicationRequest().getHeader();

            LoyaltyCardDetails loyaltyCardDetails = losMongoRepo.getLoyaltyCardsDetails(
                    gng.getGngRefId(), header.getInstitutionId());
            if (null != loyaltyCardDetails) {
                fillers.setFiller1(GngUtils.getTruncatedString(loyaltyCardDetails.getLoyaltyCardNo(), 50));
            }
            fillers.setFiller4(GngDateUtil.getFormattedDate(postIpaRequest.getHeader().getDateTime()));

            List<CroJustification> croJustificationList = gng.getCroJustification();

            if (CollectionUtils.isNotEmpty(croJustificationList)) {
                Collections.sort(croJustificationList);
                fillers.setFiller2(GngUtils.getTruncatedString(croJustificationList.get(croJustificationList.size() - 1).getSubjectTo(), 50));//subject to
            }

            if (gng != null) {
                if (gng.getIntrimStatus() != null) {
                    InterimStatus interimStatus = gng.getIntrimStatus();

                    if (interimStatus.getScoringModuleResult() != null) {
                        fillers.setFiller6(GngUtils.getTruncatedString(interimStatus.getScoringModuleResult()
                                .getFieldValue(), 50));
                    }
                    if (interimStatus.getPanModuleResult() != null) {
                        fillers.setFiller11(GngUtils.getTruncatedString(interimStatus.getPanModuleResult()
                                .getFieldValue(), 50));
                    }
                    if (interimStatus.getOfficeModuleResult() != null) {
                        fillers.setFiller8(GngUtils.getTruncatedString(interimStatus.getOfficeModuleResult()
                                .getFieldValue(), 50));
                        fillers.setFiller12(GngUtils.getTruncatedString(GngUtils.getString(interimStatus
                                .getOfficeModuleResult().getAddStability()), 50));
                    }
                    if (interimStatus.getResidenceAddressResult() != null) {
                        fillers.setFiller7(GngUtils.getTruncatedString(interimStatus
                                .getResidenceAddressResult().getFieldValue(), 50));
                        fillers.setFiller13(GngUtils.getTruncatedString(GngUtils.getString(interimStatus
                                .getResidenceAddressResult().getAddStability()), 50));
                    }

                    fillers.setFiller14(interimStatus.getPanStatus());
                }

                if (gng.getCroJustification() != null && !gng.getCroJustification().isEmpty()) {
                    CroJustification croJustification = gng.getCroJustification()
                            .get(0);
                    fillers.setFiller9(GngUtils.getTruncatedString(croJustification.getCroID(), 50));
                    fillers.setFiller10(GngUtils.getTruncatedString(croJustification.getRemark(), 50));
                }

                if (gng.getApplicantComponentResponse() != null && gng.getApplicantComponentResponse().getScoringServiceResponse() != null &&
                        gng.getApplicantComponentResponse().getScoringServiceResponse().getEligibilityResponse() != null) {
                    fillers.setFiller15(GngUtils.getTruncatedString(GngUtils.getString(gng
                            .getApplicantComponentResponse()
                            .getScoringServiceResponse().getEligibilityResponse()
                            .getEligibilityAmount()), 50));
                    fillers.setFiller16(GngUtils.getTruncatedString(gng.getApplicantComponentResponse()
                            .getScoringServiceResponse().getEligibilityResponse()
                            .getDecision(), 50));
                }
            }

            if (postIpa != null) {
                fillers.setFiller17(GngUtils.getTruncatedString(GngUtils.getString(postIpa.getProcessingFees()), 50));
                fillers.setFiller18(GngUtils.getTruncatedString(GngUtils.getString(postIpa.getTenor()), 50));
                fillers.setFiller19(GngUtils.getTruncatedString(GngUtils.getString(postIpa.getNetFundingAmount()), 50));
                fillers.setFiller20(GngUtils.getTruncatedString(GngUtils.getString(postIpa.getNetDisbursalAmount()), 50));
                fillers.setFiller21(GngUtils.getTruncatedString(GngUtils.getString(postIpa.getOtherChargesIfAny()), 50));
                fillers.setFiller22(GngUtils.getTruncatedString(GngUtils.getString(postIpa.getDealerSubvention()), 50));
                fillers.setFiller26(gng.getGngRefId());
                fillers.setFiller28(GngUtils.getString(postIpa.getManufacturingProcessingFee()));
                fillers.setFiller27(GngDateUtil.getDDMMYYFormat(postIpaRequest.getDate()));
            }


            if (header != null) {
                fillers.setFiller23(header.getDsaId());
                fillers.setFiller24("");
                Date d = header.getDateTime();
                SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
                if (d != null) {
                    fillers.setFiller25(sdf.format(d));
                }

                //Serial no added for devices
                fillers.setFiller30(losMongoRepo.getProductSerialNo(gng.getGngRefId()));
            }
        }catch (Exception e){
            logger.error("Exception caught while filling fillers of LOS " + e);
        }
        return fillers;
    }

    public Customers setCustomerData(Applicant applicant, GoNoGoCustomerApplication gng, ApplicationRequest appRequest, Header header, Application app,LosMbData losMbData) {

        Customers customers = objectFactory.createApplicationCustomers();
        List<Customer> customerList = customers.getCustomer();
        Customer customer = objectFactory.createCustomer();
        String rentAmount = "";
        String residenceAddressType = "";

        customer.setApplicantType("P");
        customer.setExistingLosCustomer("N");
        customer.setOldApplicationId("");
        customer.setRelationship("");
        customer.setConsentToCall(applicant.isConsentToCall() ? Status.YES.name() : Status.NO.name());
        if (!CollectionUtils.isEmpty(applicant.getEmployment())) {
            customer.setYearsAtEmployment(String.valueOf(applicant.getEmployment().get(0).getTimeWithEmployer() / 12));
        }

        if (null != losMbData)
            customer.setMbReqFile(losMbData.getLosMbFileName());
        else {
            long mbPushSequenceNumber = losMongoRepo.getMbPushSequenceNumber(header.getInstitutionId());
            customer.setMbReqFile(GngUtils.getMbFileName(appRequest, header, mbPushSequenceNumber));
        }

        customer.setPosidexReqFile("N");

        ComponentResponse applicantComponentResponse = gng.getApplicantComponentResponse();

        if (null != applicantComponentResponse && null != applicantComponentResponse.getMultiBureauJsonRespose()) {
            customer.setMbAckId(String.valueOf(applicantComponentResponse.getMultiBureauJsonRespose().getAcknowledgmentId()));
        }

        if (null != appRequest && null != appRequest.getRequest() && null != appRequest.getRequest().getGstDetails()) {
            customer.setGstNo(appRequest.getRequest().getGstDetails().getGstNumber());
            customer.setGstinExists(appRequest.getRequest().getGstDetails().isGstDetailsEntered() ? Status.Y.name() : Status.N.name());
            customer.setGstinFromDt(GngDateUtil.getGregorianDate(appRequest.getRequest().getGstDetails().getFromDate()));
            customer.setGstinToDt(GngDateUtil.getGregorianDate(appRequest.getRequest().getGstDetails().getToDate()));
        }

        customer.setCustomerLoyaltyCards(setLoyaltyCardDetails(appRequest));

        if (applicant != null) {

            Name applicantName = applicant.getApplicantName();

            if (applicantName != null) {

                customer.setFirstName(GngUtils.getTruncatedString(applicantName.getFirstName(), 15));

                if (StringUtils.isNotBlank(applicantName.getMiddleName())) {
                    customer.setMiddleName(GngUtils.getTruncatedString(applicantName.getMiddleName(), 15));
                } else {
                    // FIXME:middlename is not mandatory so passing blank as suggested by HDBFS
                    customer.setMiddleName(" ");
                }
                customer.setLastName(GngUtils.getTruncatedString(applicantName.getLastName(), 15));
            }

            customer.setFatherName(applicant.getFatherName() != null ?
                    GngUtils.getTruncatedString(GngUtils.convertNameToFullName(applicant.getFatherName()), 35) : "");


            customer.setMothersMaidenName(applicant.getMotherName() != null ? (GngUtils.getTruncatedString(GngUtils
                    .convertNameToFullName(applicant.getMotherName()), 15)) : "");

            if (applicant.getAge() > 0)
                customer.setAge(GngUtils.getTruncatedString(GngUtils.getString(applicant.getAge()), 2));

            customer.setDateOfBirth(GngDateUtil.getGregorianDate(applicant.getDateOfBirth()));

            List<Kyc> kycList = applicant.getKyc();

            if (kycList != null && !kycList.isEmpty()) {

                customer.setPassportNo(GngUtils.getKYC(GNGWorkflowConstant.PASSPORT.toFaceValue(), kycList, ValidationPattern.PASSPORT));

                customer.setPanNo(GngUtils.getKYC(GNGWorkflowConstant.PAN.toFaceValue(), kycList, ValidationPattern.PAN));

                customer.setDrivingLicense(GngUtils.getKYC(GNGWorkflowConstant.DRIVING_LICENSE.toFaceValue(), kycList, ValidationPattern.DRIVING_LICENSE));

                customer.setVoterId(GngUtils.getKYC(GNGWorkflowConstant.VOTER_ID.toFaceValue(), kycList, ValidationPattern.VOTER_ID));

                customer.setAadharCard(GngUtils.getKYC(GNGWorkflowConstant.AADHAAR.toFaceValue(), kycList, ValidationPattern.AADHAAR_CARD));

                //FIXME: AADHAR constant value varies
                if (StringUtils.isBlank(customer.getAadharCard())) {
                    customer.setAadharCard(GngUtils.getKYC(GNGWorkflowConstant.AADHAAR.toFaceValue(), kycList, ValidationPattern.AADHAAR_CARD));
                }
                if (StringUtils.isBlank(customer.getVoterId())) {
                    customer.setVoterId(GngUtils.getKYC(GNGWorkflowConstant.VOTER_ID.toFaceValue(), kycList, ValidationPattern.VOTER_ID));
                }

            }

            String maritalStatus = applicant.getMaritalStatus();
            if ("SINGLE".equalsIgnoreCase(maritalStatus)) {
                customer.setMaritalStatus("S");
            } else {
                customer.setMaritalStatus("M");
            }

            customer.setCitizenship("1");

            List<com.softcell.gonogo.model.address.CustomerAddress> address = applicant.getAddress();


            if (address != null && !address.isEmpty()) {
                com.softcell.gonogo.model.address.CustomerAddress custAddress = GngUtils.getAddressObject(GNGWorkflowConstant.RESIDENCE.toFaceValue(), address);
                if (custAddress != null) {
                    rentAmount = GngUtils.getString(custAddress.getRentAmount());
                    if (StringUtils.equals(custAddress.getResidenceAddressType(), "OWNED-FLAT")) {
                        residenceAddressType = "FL";
                    } else if (StringUtils.equals(custAddress.getResidenceAddressType(), "OWNED-CHAWL")) {
                        residenceAddressType = "CH";
                    } else {
                        residenceAddressType = losMongoRepo.getResidenceByResidenceDesc(custAddress.getResidenceAddressType());
                    }
                }
            }

            customer.setResidenceType(residenceAddressType);
            customer.setRentPerMonth(rentAmount);

            String gender = applicant.getGender();
            if ("MALE".equalsIgnoreCase(gender)) {
                customer.setGender("M");
                // FIXME:It was discussed with HDBFS we will be sending MR. in title
                customer.setTitle("MR.");
                //customer.setTitle("1");
            } else {
                customer.setGender("F");
                customer.setTitle("MS.");
                //customer.setTitle("2");
            }

            List<Employment> empList = applicant.getEmployment();
            Employment employment = null;
            if (empList != null && !empList.isEmpty()) {
                for (Employment employmentObj : empList) {
                    employment = employmentObj;
                    break;
                }
            }

            int timeWithEmployer = 0;
            float timeWithEmployerF = 0;
            if (employment != null) {
                String constitutionType = employment.getConstitution();
                String empType = employment.getEmploymentType();

                if ("SALARIED".equalsIgnoreCase(constitutionType)) {
                    customer.setConstitution("16");
                    customer.setDesignation("OFF");
                } else if ("SELF-EMPLOYED".equalsIgnoreCase(constitutionType)) {
                    customer.setConstitution("15");
                    customer.setDesignation("PROP");
                } else {
                    customer.setDesignation("EXECUT");
                }

                if("TRUST".equalsIgnoreCase(constitutionType)){
                    customer.setConstitution("21");
                }else if("PARTNERSHIP".equalsIgnoreCase(constitutionType)){
                    customer.setConstitution("19");
                }else if("PRIVATE LIMITED COMPANY".equalsIgnoreCase(constitutionType)){
                    customer.setConstitution("20");
                }else if("PROPRIETORSHIP CONCERN".equalsIgnoreCase(constitutionType)){
                    customer.setConstitution("23");
                }

                customer.setCustomerCatg("11");

                if ("SELF-EMPLOYED".equalsIgnoreCase(empType)) {
                    customer.setEmploymentType("E");
                } else if ("RETIRED".equalsIgnoreCase(empType)) {
                    customer.setEmploymentType("R");
                } else if ("SALARIED".equalsIgnoreCase(empType)) {
                    customer.setEmploymentType("S");
                } else {
                    customer.setEmploymentType("O");
                }

                // FIXME:need to get value
                String employmentId = losMongoRepo
                        .getEmployerIdByName(employment.getEmploymentName(), header.getInstitutionId());
                if (StringUtils.isNotBlank(employmentId)) {
                    customer.setEmployerName(employmentId);
                } else {

                    customer.setEmployerNameOthers(employment.getEmploymentName());
                }

                timeWithEmployerF = employment.getTimeWithEmployer();

                if (timeWithEmployerF > 6) {
                    timeWithEmployer = Math.round(timeWithEmployerF / 12);
                }
                customer.setYearsAtEmployment(GngUtils
                        .getString(timeWithEmployer));
                customer.setBusinessName(employment.getBusinessName());
                customer.setNaureOfBusiness("OTHERS");
                customer.setIndustry(employment.getIndustryType());
            }

            customer.setConsentToCall("Y");

            customer.setEducationalQualification(losMongoRepo.getEducationByEducationDesc(applicant.getEducation()));

            if (StringUtils.equalsIgnoreCase("Professional", applicant.getEducation())) {
                customer.setPersonalQualification("OTH");
            } else {
                customer.setPersonalQualification("");
            }

            customer.setNoOfDependents(GngUtils.getString(applicant
                    .getNoOfDependents()));
            // FIXME:need to update. PSEIOTH will be send in profession as it dont accept blank or OTHERS
            //customer.setProfession("");
            customer.setProfession("PSEIOTH");
            customer.setTotalExperience(GngUtils
                    .getString(timeWithEmployer));
            customer.setIncomeDetails("Y");
            // It it marked N as for product CD
            customer.setLiabilityDetails("N");
            customer.setCustomerIncomes(populateCustomerIncomes(applicant));
            customer.setCustomerAddresses(populateCustomerAddress(applicant, appRequest, header));

            CustomerPan customerPan = populateCustomerPan(gng, app);

            if ( customerPan != null) {
                customer.setCustomerPan(populateCustomerPan(gng, app));
            }


            customer.setBankingDetails("Y");
            customer.setCustomerBankings(populateCustomerBankingDetails(applicant));

            //Value provided by deepak gupta
            customer.setDepartment("OTHERS");
            customer.setCustomerLoyaltyCards(populateLoyaltyCardDetails(gng, appRequest));

            customerList.add(customer);
        }
        return customers;
    }

    private Customer.CustomerLoyaltyCards populateLoyaltyCardDetails(GoNoGoCustomerApplication gng, ApplicationRequest appRequest) {

        Customer.CustomerLoyaltyCards customerCustomerLoyaltyCards = objectFactory.createCustomerCustomerLoyaltyCards();

        List<CustomerLoyaltyCard> customerLoyaltyCards = customerCustomerLoyaltyCards.getCustomerLoyaltyCard();

        LoyaltyCardDetails loyaltyCardsDetails = losMongoRepo.getLoyaltyCardsDetails(gng.getGngRefId(), appRequest.getHeader().getInstitutionId());

        if (loyaltyCardsDetails != null) {

            CustomerLoyaltyCard customerLoyaltyCard = objectFactory.createCustomerLoyaltyCard();

            customerLoyaltyCard.setEmiCardNo(loyaltyCardsDetails.getLoyaltyCardNo());
            customerLoyaltyCard.setLosId("");
            customerLoyaltyCard.setCardExpDate(GngDateUtil.getGregorianDate("01012050"));
            customerLoyaltyCard.setIssuanceDate(GngDateUtil.getGregorianDate(new Date()));
            customerLoyaltyCard.setApprovalLimit("0");

            customerLoyaltyCards.add(customerLoyaltyCard);

        }

        return customerCustomerLoyaltyCards;
    }

    private Customer.CustomerBankings populateCustomerBankingDetails(Applicant applicant) {

        Customer.CustomerBankings customerCustomerBankings = objectFactory.createCustomerCustomerBankings();

        List<CustomerBanking> customerBankingList = customerCustomerBankings.getCustomerBanking();

        List<BankingDetails> bankingDetails = applicant.getBankingDetails();

        if (bankingDetails != null && !bankingDetails.isEmpty()) {

            for (BankingDetails bankingDetail : bankingDetails) {

                CustomerBanking customerBanking = objectFactory.createCustomerBanking();

                customerBanking.setBankName(losMongoRepo.getBankIdFromBankName(bankingDetail.getBankName()));
                customerBanking.setBankBranch(bankingDetail.getBranchName());

                if (StringUtils.equalsIgnoreCase(bankingDetail.getBankingType(), "Primary")) {
                    customerBanking.setBanking("PRIMARY");
                } else {
                    customerBanking.setBanking("ADDITION");
                }

                customerBanking.setAccountType(bankingDetail.getAccountType());
                customerBanking.setAccountNo(bankingDetail.getAccountNumber());
                customerBanking.setAcHolderName(GngUtils.getTruncatedString(GngUtils.convertNameToFullName(bankingDetail.getAccountHolderName()), 50));
                customerBanking.setNoOfYearsHeld(GngUtils.getString(bankingDetail.getYearHeld()));

                //These values made non mandatory
                /*customerBanking.setAvgBalance12M("0");
                customerBanking.setCreditTxn12M("0");
                customerBanking.setCreditTxn12MAmt("0");
                customerBanking.setDebitTxn12M("0");
                customerBanking.setDebitTxn12MAmt("0");
                customerBanking.setInwardReturns12M("0");
                customerBanking.setOutwardReturns12M("0");
                customerBanking.setMedian("0");
                customerBanking.setLimit("0");
                customerBanking.setRemarks("0");*/

                customerBankingList.add(customerBanking);


            }
        }


        return customerCustomerBankings;
    }

    private Customer.CustomerLoyaltyCards setLoyaltyCardDetails(ApplicationRequest appRequest) {
        LoyaltyCardDetails loyaltyCardDetails = losMongoRepo.getLoyaltyCardsDetails(
                appRequest.getRefID(), appRequest.getHeader().getInstitutionId());
        Customer.CustomerLoyaltyCards customerLoyaltyCardsObj = null;
        if (null != loyaltyCardDetails) {
            CustomerLoyaltyCard customerLoyaltyCard = new CustomerLoyaltyCard();
            customerLoyaltyCard.setEmiCardNo(loyaltyCardDetails.getLoyaltyCardNo());
            customerLoyaltyCardsObj = new Customer.CustomerLoyaltyCards();
            List<CustomerLoyaltyCard> customerLoyaltyCards = customerLoyaltyCardsObj.getCustomerLoyaltyCard();
            customerLoyaltyCards.add(customerLoyaltyCard);
        }
        return customerLoyaltyCardsObj;
    }

    private CustomerIncomes populateCustomerIncomes(Applicant applicant) {
        CustomerIncomes customerIncomes = objectFactory
                .createCustomerCustomerIncomes();
        List<CustomerIncome> customerIncomeList = customerIncomes
                .getCustomerIncome();
        List<Employment> employments = applicant.getEmployment();
        if (employments != null && !employments.isEmpty()) {
            CustomerIncome customerIncome = objectFactory
                    .createCustomerIncome();
            for (Employment employment : employments) {
                if (employment != null) {
                    customerIncome.setAmtCurrent(GngUtils
                            .getString(employment.getMonthlySalary()));
                    customerIncome.setAmtPrevious(GngUtils
                            .getString(employment.getMonthlySalary()));
                    customerIncome.setFrequency("12");

                    String empType = employment.getEmploymentType();

                    if ("SALARIED".equalsIgnoreCase(empType)) {
                        // FIXME:Head is not according to default value
                        customerIncome.setHead("BS");
                    } else {
                        customerIncome.setHead("BP");
                    }
                    customerIncome.setHead("INCOS");
                }

                customerIncomeList.add(customerIncome);
                break;
            }
        }
        return customerIncomes;
    }

    public CustomerAddresses populateCustomerAddress(Applicant applicant, ApplicationRequest appRequest, Header header) {
        CustomerAddresses customerAddresses = objectFactory
                .createCustomerCustomerAddresses();
        List<CustomerAddress> customerList = customerAddresses
                .getCustomerAddress();
        List<com.softcell.gonogo.model.address.CustomerAddress> addresss = applicant
                .getAddress();
        List<Phone> phones = applicant.getPhone();
        List<Email> emails = applicant.getEmail();
        String gstAddressType = null;
        if (null != appRequest.getRequest().getGstDetails() && null != appRequest.getRequest().getGstDetails().getAddress()) {
            gstAddressType = appRequest.getRequest().getGstDetails().getAddress().getAddressType();
        }

        for (int i = 0; i < addresss.size(); i++) {
            com.softcell.gonogo.model.address.CustomerAddress gngCustomerAddress = addresss
                    .get(i);
            if (gngCustomerAddress != null) {
                //FIXME:this check is for not populating blank address details
                if (StringUtils.isBlank(gngCustomerAddress.getAddressLine1())) {
                    continue;
                }

                CustomerAddress customerAddress = objectFactory
                        .createCustomerAddress();

                String gstAddressFlag = GNGWorkflowConstant.NO.toFaceValue();

                if (GNGWorkflowConstant.OFFICE.toFaceValue()
                        .equalsIgnoreCase(gngCustomerAddress.getAddressType())) {
                    customerAddress.setAddressType("OFFICE");
                    gstAddressFlag = StringUtils.equalsIgnoreCase(GNGWorkflowConstant.OFFICE.toFaceValue(), gstAddressType)
                            ? GNGWorkflowConstant.YES.toFaceValue()
                            : GNGWorkflowConstant.NO.toFaceValue();
                } else if (GNGWorkflowConstant.RESIDENCE.toFaceValue()
                        .equalsIgnoreCase(gngCustomerAddress.getAddressType())) {
                    gstAddressFlag = StringUtils.equalsIgnoreCase(GNGWorkflowConstant.RESIDENCE.toFaceValue(), gstAddressType)
                            ? GNGWorkflowConstant.YES.toFaceValue()
                            : GNGWorkflowConstant.NO.toFaceValue();
                    customerAddress.setAddressType("CURRES");
                    customerAddress.setYrsAtCity(String.valueOf(gngCustomerAddress.getMonthAtCity() / 12));
                    customerAddress.setYrsAtCurrAdd(String.valueOf(gngCustomerAddress.getMonthAtAddress() / 12));
                } else if (GNGWorkflowConstant.PERMANENT.toFaceValue()
                        .equalsIgnoreCase(gngCustomerAddress.getAddressType())) {
                    customerAddress.setAddressType("PERMNENT");
                    gstAddressFlag = StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERMANENT.toFaceValue(), gstAddressType)
                            ? GNGWorkflowConstant.YES.toFaceValue()
                            : GNGWorkflowConstant.NO.toFaceValue();
                } else {
                    continue;
                }

                if (StringUtils
                        .isNotBlank(gngCustomerAddress.getAddressLine1())) {
                    customerAddress.setAddressLine1(gngCustomerAddress
                            .getAddressLine1());
                } else {
                    customerAddress.setAddressLine1(" ");
                }
                customerAddress.setGstAddressFlag(gstAddressFlag);
                customerAddress.setAddressLine2(gngCustomerAddress
                        .getAddressLine2());
                customerAddress.setAddressLine3(gngCustomerAddress.getLine3());
                // 1 is india country code
                customerAddress.setCountry("1");
                customerAddress.setState(losMongoRepo
                        .getStateIdByState(gngCustomerAddress.getState(), header.getInstitutionId()));
                customerAddress.setCity(losMongoRepo
                        .getCityIdByCity(gngCustomerAddress.getCity(), header.getInstitutionId()));
                customerAddress.setPincode(GngUtils
                        .getString(gngCustomerAddress.getPin()));
                customerAddress.setLandmark(gngCustomerAddress.getLandMark());

                if (emails != null && i < emails.size()) {
                    Email email = emails.get(i);
                    if (StringUtils.isNotBlank(email.getEmailAddress())) {
                        customerAddress.setEmailId(email.getEmailAddress());
                    }
                }

                if (GNGWorkflowConstant.RESIDENCE.toFaceValue().equals(gngCustomerAddress
                        .getAddressType())) {
                    customerAddress.setMailingFlag("Y");
                } else if (GNGWorkflowConstant.OFFICE.toFaceValue()
                        .equals(gngCustomerAddress.getAddressType())) {
                    customerAddress.setMailingFlag("N");
                } else {
                    customerAddress.setMailingFlag("N");
                }

                if (GNGWorkflowConstant.OFFICE.toFaceValue()
                        .equalsIgnoreCase(gngCustomerAddress.getAddressType()) && CollectionUtils.isNotEmpty(applicant.getEmployment())) {

                    List<Employment> employmentList = applicant.getEmployment();
                    if(employmentList.size() > 0){
                        Collections.sort(employmentList);
                    }

                    Employment employment = employmentList.get(employmentList.size() - 1);
                    int timeWithEmployer = employment.getTimeWithEmployer();

                    int yearsWithEmployer = timeWithEmployer / 12;
                    int monthsWithEmployer = timeWithEmployer % 12;

                    customerAddress.setYrsAtCurrAdd(GngUtils
                            .getString(yearsWithEmployer));

                    customerAddress.setYrsAtCity(GngUtils
                            .getString(yearsWithEmployer));

                    customerAddress.setMonthsAtCity(GngUtils.getString(monthsWithEmployer));
                    customerAddress.setMonthsAtCurAdd(GngUtils.getString(monthsWithEmployer));

                    customerAddress.setDistanceInKm("1");

                } else {

                    int monthsAtAddress = gngCustomerAddress.getMonthAtAddress();

                    int yearAtAddress = 0;
                    if (monthsAtAddress > 0) {
                        yearAtAddress = monthsAtAddress / 12;
                    }

                    int monthsAtCity = gngCustomerAddress.getMonthAtCity();
                    int yearAtCity = 0;
                    if (monthsAtCity > 0) {
                        yearAtCity = monthsAtCity / 12;
                    }
                    customerAddress.setYrsAtCurrAdd(GngUtils
                            .getString(yearAtAddress));

                    customerAddress.setYrsAtCity(GngUtils
                            .getString(yearAtCity));

                    int monthAtCity = monthsAtCity % 12;
                    int monthAtAdd = monthsAtAddress % 12;
                    customerAddress.setMonthsAtCity(GngUtils.getString(monthAtCity));
                    customerAddress.setMonthsAtCurAdd(GngUtils.getString(monthAtAdd));

                    customerAddress.setDistanceInKm("1");
                }
                customerList.add(customerAddress);
            }
        }


        for (CustomerAddress customerAddress : customerList) {
            if (customerAddress != null) {
                //Setting phone and email as we store it in different object not same as hdbfs address object

                if (StringUtils.contains(customerAddress.getAddressType(), "OFFICE")) {

                    Phone phone = GngUtils.getPhone(PhoneJsonKeys.OFFICE_TYPE, phones).orElse(null);
                    Phone mobile = GngUtils.getPhone(PhoneJsonKeys.OFFICE_MOBILE, phones).orElse(null);

                    if (phone != null && StringUtils.isNotBlank(phone.getAreaCode())) {
                        customerAddress.setAreacode(phone.getAreaCode());
                        customerAddress.setPhone1(phone.getPhoneNumber());
                    }
                    if (mobile != null && StringUtils.isNotBlank(mobile.getPhoneNumber())) {
                        customerAddress.setExtension("91");
                        customerAddress.setMobile(mobile.getPhoneNumber());
                    }

                    if (StringUtils.isBlank(customerAddress.getPhone1())) {
                        customerAddress.setPhone1(customerAddress.getMobile());
                    }
                    if (StringUtils.isBlank(customerAddress.getAreacode())) {
                        customerAddress.setAreacode("91");
                    }

                    Email officeEmail = GngUtils.getEmail(GNGWorkflowConstant.WORK.toFaceValue(), emails);

                    if (officeEmail == null) {
                        officeEmail = GngUtils.getEmail(GNGWorkflowConstant.OFFICE_EMAIL.toFaceValue(), emails);
                    }

                    if (officeEmail != null) {
                        customerAddress.setEmailId(officeEmail.getEmailAddress());
                    }

                } else if (StringUtils.contains(customerAddress.getAddressType(), "CURRES")) {
                    Phone phone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_PHONE_TYPE, phones).orElse(null);
                    Phone mobile = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE, phones).orElse(null);
                    if (phone != null && StringUtils.isNotBlank(phone.getAreaCode())) {
                        customerAddress.setAreacode(phone.getAreaCode());
                        customerAddress.setPhone1(phone.getPhoneNumber());
                    }
                    if (mobile != null && StringUtils.isNotBlank(mobile.getPhoneNumber())) {
                        customerAddress.setExtension("91");
                        customerAddress.setMobile(mobile.getPhoneNumber());
                    }
                    if (StringUtils.isBlank(customerAddress.getPhone1())) {
                        customerAddress.setPhone1(customerAddress.getMobile());
                    }
                    if (StringUtils.isBlank(customerAddress.getAreacode())) {
                        customerAddress.setAreacode("91");
                    }

                    Email personalEmail = GngUtils.getEmail(GNGWorkflowConstant.PERSONAL_EMAIL.toFaceValue(), emails);

                    if (personalEmail == null) {
                        personalEmail = GngUtils.getEmail(GNGWorkflowConstant.PERSONAL_EMAIL.toFaceValue(), emails);
                    }

                    if (personalEmail != null) {
                        customerAddress.setEmailId(personalEmail.getEmailAddress());
                    }
                } else if (StringUtils.contains(customerAddress.getAddressType(), "PERMNENT")) {
                    Phone phone = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_PHONE_TYPE, phones).orElse(null);
                    Phone mobile = GngUtils.getPhone(PhoneJsonKeys.PERSONAL_MOBILE_TYPE, phones).orElse(null);
                    if (phone != null && StringUtils.isNotBlank(phone.getAreaCode())) {
                        customerAddress.setAreacode(phone.getAreaCode());
                        customerAddress.setPhone1(phone.getPhoneNumber());
                    }
                    if (mobile != null && StringUtils.isNotBlank(mobile.getPhoneNumber())) {
                        customerAddress.setExtension("91");
                        customerAddress.setMobile(mobile.getPhoneNumber());
                    }
                    if (StringUtils.isBlank(customerAddress.getPhone1())) {
                        customerAddress.setPhone1(customerAddress.getMobile());
                    }
                    if (StringUtils.isBlank(customerAddress.getAreacode())) {
                        customerAddress.setAreacode("91");
                    }

                    Email personalEmail = GngUtils.getEmail(GNGWorkflowConstant.PERMANENT.toFaceValue(), emails);

                    if (personalEmail != null) {
                        customerAddress.setEmailId(personalEmail.getEmailAddress());
                    }
                }
            }
        }
        return customerAddresses;
    }

    public CustomerPan populateCustomerPan(GoNoGoCustomerApplication gng, Application app) {
        CustomerPan customerPan = null;
        if (gng != null
                && gng.getApplicantComponentResponse() != null
                && gng.getApplicantComponentResponse().getPanServiceResponse() != null) {
            if (StringUtils.equals("SUCCESS", gng
                    .getApplicantComponentResponse().getPanServiceResponse()
                    .getTxnStatus())) {
                PanResponse panServiceResponse = gng
                        .getApplicantComponentResponse()
                        .getPanServiceResponse();

                if (panServiceResponse.getKycResponse() != null
                        && panServiceResponse.getKycResponse()
                        .getPanResponseDetails() != null
                        && panServiceResponse.getKycResponse()
                        .getPanResponseDetails().getPanInfo() != null && StringUtils.equalsIgnoreCase(panServiceResponse.getKycResponse()
                        .getPanResponseDetails().getPanInfo().getPanStatus(), "E")) {

                    app.setPanValidation("Y");

                    customerPan = objectFactory.createCustomerCustomerPan();
                    PanInfo panInfo = panServiceResponse.getKycResponse()
                            .getPanResponseDetails().getPanInfo();
                    customerPan.setPan(panInfo.getPanNumber());
                    customerPan.setPanStatus(panInfo.getPanStatus());


                    if (StringUtils.isNotBlank(panInfo.getFirstName())) {
                        customerPan.setFirstName(panInfo.getFirstName());
                    } else {
                        customerPan.setFirstName(" ");
                    }

                    if (StringUtils.isNotBlank(panInfo.getMiddleName())) {
                        customerPan.setMiddleName(panInfo.getMiddleName());
                    } else {
                        customerPan.setMiddleName(" ");
                    }

                    if (StringUtils.isNotBlank(panInfo.getLastName())) {
                        customerPan.setLastName(panInfo.getLastName());
                    } else {
                        customerPan.setLastName(" ");
                    }

                    customerPan.setPanTitle(panInfo.getPanTitle());
                    String lastUpdateDate = panInfo.getLastUpdateDate();
                    customerPan.setDateLastUpdated(GngDateUtil
                            .getPanVerificationDate(lastUpdateDate));
                }
            }
        }

        return customerPan;
    }

    private Application.Charges populateCharge(GoNoGoCustomerApplication gngCustApp) {

        Application.Charges charges = objectFactory.createApplicationCharges();
        List<Charge> chargeList = charges.getCharge();

        for (LosChargeConfig.LosChargeType losChargeType : LosChargeConfig.LosChargeType.values()) {
            try {
                switch (losChargeType) {

                    case LOYALTY_CARD:
                        Charge loyaltyCardCharge = getLoyaltyCardCharge(gngCustApp);
                        if (null != loyaltyCardCharge) {
                            chargeList.add(loyaltyCardCharge);
                        }
                        break;

                    case EXTENDED_WARRANTY:
                        Charge extendedWarrantyCharge = getExtendedWarrantyCharge(gngCustApp);
                        if (null != extendedWarrantyCharge) {
                            chargeList.add(extendedWarrantyCharge);
                        }
                        break;
                }
            }catch (Exception e){
                logger.error("Exception caught while populating charges for LOS application "+e);
            }
        }
        return CollectionUtils.isNotEmpty(chargeList) ? charges : null;
    }

    private Charge getExtendedWarrantyCharge(GoNoGoCustomerApplication gngCustApp) {
        Charge charge = null;
        Header header = gngCustApp.getApplicationRequest().getHeader();
        LosChargeConfig losChargeConfiguration = configurationRepository.getLosChargeConfiguration(header.getInstitutionId(), LosChargeConfig.LosChargeType.EXTENDED_WARRANTY, header.getProduct().toProductId());
        ExtendedWarrantyDetails extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(gngCustApp.getGngRefId(), header.getInstitutionId());
        if (losChargeConfiguration != null && extendedWarrantyDetails != null) {
            charge = new Charge();
            charge.setChargeCode(losChargeConfiguration.getChargeCode());
            charge.setChargeAmount(String.valueOf(extendedWarrantyDetails.getEwWarrantyPremium()));
        }
        return charge;
    }


    private Charge getLoyaltyCardCharge(GoNoGoCustomerApplication gngCustApp) {
        Charge charge = null;
        Header header = gngCustApp.getApplicationRequest().getHeader();
        LosChargeConfig losChargeConfiguration = configurationRepository.getLosChargeConfiguration(header.getInstitutionId(), LosChargeConfig.LosChargeType.LOYALTY_CARD, header.getProduct().toProductId());
        LoyaltyCardDetails loyaltyCardDetails = losMongoRepo.getLoyaltyCardsDetails(
                gngCustApp.getGngRefId(), header.getInstitutionId());

        if (losChargeConfiguration != null && loyaltyCardDetails != null) {
            charge = new Charge();
            charge.setChargeCode(losChargeConfiguration.getChargeCode());
            charge.setChargeAmount(String.valueOf(loyaltyCardDetails.getLoyaltyCardPrice()));
        }
        return charge;
    }
}
