package com.softcell.gonogo.service.factory.tvscdlosintegration;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.InsertOrUpdateTvsRecordGroupOne;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;

public interface LosTvsGroupOneReqBuilder {
    InsertOrUpdateTvsRecordGroupOne buildLosTvsGroupOneRequest(GoNoGoCustomerApplication gonogoCustomerApplication, SerialNumberInfo serialNumberInfo);
}
