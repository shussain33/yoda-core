package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.ntc.RequestDomain;

/**
 * NTC request builder
 * It is use to build request for NTC from Cibil request.
 *
 * @author bhuvneshk
 */
public interface NTCEntityBuilder {

    /**
     * This method build request for NTC builder based on CIBIL request domain
     *
     * @param goNoGoCustomerApplication
     * @return
     */
    RequestDomain build(GoNoGoCustomerApplication goNoGoCustomerApplication);

}
