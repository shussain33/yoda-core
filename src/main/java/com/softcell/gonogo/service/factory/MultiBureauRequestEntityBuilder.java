package com.softcell.gonogo.service.factory;


import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.CommercialRequestResponseDomain;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.pickup.AcknowledgmentDomain;
import com.softcell.gonogo.model.multibureau.pickup.IssueJsonDomain;
import com.softcell.gonogo.model.multibureau.pickup.RequestJsonDomain;
import com.softcell.gonogo.model.request.ApplicationRequest;

/**
 * @author kishorp
 */

public interface MultiBureauRequestEntityBuilder {
    /**
     * @param application
     * @return
     */
    RequestJsonDomain build(ApplicationRequest application);


    /**
     *
     * @param ackJsonDomain
     * @return
     */
    IssueJsonDomain buildIssueRequest(final AcknowledgmentDomain ackJsonDomain);

    /**
     *
     * @param outputAckDomain
     * @return
     */
    IssueJsonDomain buildCorpIssueRequest(final OutputAckDomain outputAckDomain);

    /**
     *
     * @param goNoGoCustomerApplication
     * @return
     */
    RequestJsonDomain buildInitialRequest(final GoNoGoCustomerApplication goNoGoCustomerApplication) throws SystemException;

    /**
     * @param applicationRequest
     * @param applicant
     * @return
     */
    RequestJsonDomain buildCoApplicantInitialRequest(ApplicationRequest applicationRequest, Applicant applicant) throws SystemException;

    CommercialRequestResponseDomain buildCorpRequest(ApplicationRequest applicationRequest, Applicant applicant) throws SystemException;

}
