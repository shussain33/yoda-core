package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.Header;
import com.softcell.gonogo.model.multibureau.pickup.RequestJsonDomain;
import com.softcell.gonogo.model.ntc.NTCHeader;
import com.softcell.gonogo.model.ntc.NTCRequest;
import com.softcell.gonogo.model.ntc.RequestDomain;
import com.softcell.gonogo.service.factory.MultiBureauRequestEntityBuilder;
import com.softcell.gonogo.service.factory.NTCEntityBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * NTCEnititBuilder interface implementation
 *
 * @author bhuvneshk
 */
@Service
public class NTCEntityBuilderImpl implements NTCEntityBuilder {

    @Override
    public RequestDomain build(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        RequestDomain ntcRequest = new RequestDomain();

        MultiBureauRequestEntityBuilder bureauEntityBuilder = new MultiBureauRequestRequestEntityBuilderImpl();
        RequestJsonDomain mbRequest = bureauEntityBuilder
                .build(goNoGoCustomerApplication.getApplicationRequest());

        String cibilBureauString = getCibilBureauString(goNoGoCustomerApplication);

        NTCHeader header = populateHeader(mbRequest.getHeader());
        NTCRequest request = populateRequest(mbRequest.getRequest(), cibilBureauString);


        ntcRequest.setHeader(header);
        ntcRequest.setRequest(request);

        return ntcRequest;
    }

    /**
     * To get cibil response string from cibil list
     *
     * @param goNoGoCustomerApplication
     * @return
     */
    private String getCibilBureauString(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {
        String cibilBureauString = null;
        if (goNoGoCustomerApplication.getApplicantComponentResponse() != null &&
                goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose() != null &&
                goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList() != null &&
                !goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList().isEmpty()) {

            List<Finished> finishedList = goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getFinishedList();

            for (Finished finished : finishedList) {
                if (finished.getBureau().equals("CIBIL")) {
                    cibilBureauString = finished.getBureauString();
                }
            }
        }
        return cibilBureauString;
    }

    private NTCHeader populateHeader(Header header) {
        NTCHeader ntcHeader = new NTCHeader();
        ntcHeader.setApplicationID(header.getApplicationID());
        ntcHeader.setConsumerID(header.getConsumerID());
        ntcHeader.setDateOfRequest(header.getDateOfRequest());
        ntcHeader.setRequestDate(header.getRequestDate());
        ntcHeader.setRequestType("NTC-REQUEST");
        ntcHeader.setResponseType(header.getResponseType());
        return ntcHeader;
    }

    private NTCRequest populateRequest(com.softcell.gonogo.model.multibureau.pickup.RequestDomain requestDomain, String cibilBureauString) {
        NTCRequest request = new NTCRequest();
        request.setLoanType(requestDomain.getLoanType());
        request.setSourceSystemName(requestDomain.getSourceSystemName());
        request.setFiller8(cibilBureauString);
        return request;
    }


}