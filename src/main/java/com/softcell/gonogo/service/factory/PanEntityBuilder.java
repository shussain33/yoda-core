/**
 * @date Mar 2, 2016 11:16:14 AM
 */
package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;

import java.util.List;

/**
 * @author kishorp
 *
 */
public interface PanEntityBuilder {

    /**
     *
     * @param coApplicantList
     * @return List<PanRequest>
     */
    List<PanRequest> build(List<CoApplicant> coApplicantList);

    /**
     *
     * @param applicant
     * @return
     */
    PanRequest buildPanRequest(GoNoGoCustomerApplication applicant);

}
