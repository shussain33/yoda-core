package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.masters.SchemeDetailsDateMapping;
import com.softcell.gonogo.model.request.master.ApplicableSchemeReportDetails;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.ApplicableSchemeDetails;
import com.softcell.gonogo.model.request.master.schememaster.schemdatemapping.UpdateSchemeDateMappingDetailsRequest;

import java.util.List;

/**
 * Created by mahesh on 27/3/17.
 */
public interface MasterDataViewBuilder {

    /**
     *
     * @param allApplicableScheme
     * @return
     */
    List<ApplicableSchemeReportDetails> buildApplicableSchemeReportResponse(List<ApplicableSchemeDetails> allApplicableScheme);

    /**
     *
     * @param updateSchemeDateMappingDetailsRequest
     * @param schemeId
     * @param productAliasName
     * @return
     */
    SchemeDetailsDateMapping buildSchemeDetailsDateMapping(UpdateSchemeDateMappingDetailsRequest updateSchemeDateMappingDetailsRequest,String schmeDesc, String schemeId, String productAliasName);

}
