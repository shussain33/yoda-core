package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.LosMbData;
import com.softcell.gonogo.model.los.LosMbPostRequest;

import javax.xml.bind.JAXBException;
import java.util.List;

public interface LosMBRequestBuilder {

    /**
     * This methods builds LOS mb request using MB object
     * It will create object for cibil bureau and End of transaction object
     *
     * @param goNoGoCustomerApplication
     * @return
     * @throws JAXBException
     */
    public List<LosMbPostRequest> buildLosMbRequest(GoNoGoCustomerApplication goNoGoCustomerApplication,LosMbData losMbData) throws JAXBException;
}
