package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.qde.ExistingCustomerServiceRequest;
import com.softcell.gonogo.model.qde.SolicitedCustomerServiceRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;

/**
 * Created by archana on 22/8/17.
 */
public interface CustomerDataBuilder {
    SolicitedCustomerServiceRequest buildSolicitedServiceRequest(ApplicationRequest customerDataRequest) ;
    ExistingCustomerServiceRequest buildExistingServiceRequest(ApplicationRequest applicationRequest) ;

}
