package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.email.EmailTemplateDetails;

/**
 * @author kishorp
 */
public interface EmailTemplateBuilder {
    String buildEmailTemplate(EmailTemplateDetails emailTemplateDetails);

}
