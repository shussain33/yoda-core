package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.kyc.request.*;
import com.softcell.gonogo.model.kyc.request.gstRequests.KGstIdentityRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.KGstRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstAuthDetailRequest;
import com.softcell.gonogo.model.kyc.request.gstRequests.TotalKycGstIdentyRequest;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by anupamad on 14/7/17.
 */
public interface TotalKycBuilder {
    public KLpgIdRequest buildLpgDetailRequest(TotalKycLpgDetailRequest totalKycLpgDtlRequest, WFJobCommDomain totalKycConfig);
    public KDrivLicRequest buildDlDetailRequest(TotalKycDlDetailRequest totalKycDlDtlRequest, WFJobCommDomain totalKycConfig);
    public KElecRequest buildElecDetailRequest(TotalKycElecDtlRequest totalKycElecDtlRequest, WFJobCommDomain totalKycConfig);
    public KVoterIdRequest buildVoterDetailRequest(TotalKycVoterDetailRequest totalKycVoterDetailRequest, WFJobCommDomain voterConfig);
    public KLpgIdRequestV2 buildLpgDetailRequestV2(TotalKycLpgDetailRequestV2 totalKycLpgV2DtlRequest);
    public KDrivLicRequestV2 buildDlDetailRequestV2(TotalKycDlDetailRequestV2 totalKycDlDetailRequestV2);
    public KVoterIdRequestV2 buildVoterDetailRequestV2(TotalKycVoterDetailRequestV2 totalKycVoterDetailRequestV2);
    public KElecRequestV2 buildElecDetailRequestV2(TotalKycElecDetailRequestV2 totalKycElecDetailRequestV2);
    public KGstRequest buildGstAuthRequest(TotalKycGstAuthDetailRequest totalKycGstAuthDetailRequest);

    public KGstIdentityRequest buildGstIdentityRequest(TotalKycGstIdentyRequest totalKycGstIdentyRequest);

    public KPanRequest buildPanAuthRequest(TotalKycPanAuthDetailRequest totalKycPanAuthDetailRequest);
}