package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.request.AmbitMifinRequest.CrDecision.CrDecisionRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DeleteApplicant.DeleteApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.DisbursalMaker.SaveDisbursalMakerRequest;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.request.AmbitMifinRequest.NewLoan.NewLoanCreationRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantAddress.SaveApplicantAddressRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.PanSearch.PanSearchRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.ProcessDedupe.ProcessDedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveApplicantDetails.SaveApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveFinancialInfo.SaveFinancialInfoRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SavePersonalInsurance.SavePersonalInsuranceRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SaveVerification.SaveVerificationRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateLoanDetails.UpdateLoanDetailsRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.SearchExistingApplicantSearch.SearchExistingApplicantRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.ApplicantdedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateDedupeControllerRequest.UpdateDedupeRequest;
import com.softcell.gonogo.model.request.AmbitMifinRequest.UpdateProcessDedupe.UpdateStatusDedupeResquest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.AmbitMifinResponse.PanSearch.MiFinPanSearchBaseResponse;

import java.util.List;

public interface AmbitMifinRequestBuilder {
    /**
     * @param applicationRequest,applicant
     * @return
     */
    PanSearchRequest buildPanSearchRequest(ApplicationRequest applicationRequest, Applicant applicant, String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */

    NewLoanCreationRequest buildLoanCreationRequest(ApplicationRequest applicationRequest, String userId) throws Exception;
    /**
     * @param applicationRequest
     * @return
     */

    SaveApplicantRequest buildSaveApplicantDetailsRequest(ApplicationRequest applicationRequest, String appId, String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */
    SaveApplicantAddressRequest buildSaveApplicantAddressRequest(ApplicationRequest applicationRequest, String appId, String addressType, String userId) throws Exception ;

    /**
     * @param applicationRequest
     * @return
     */
    DeleteApplicantRequest buildDeleteApplicantRequest(ApplicationRequest applicationRequest, List<String> coApplicantIds, String userId) throws Exception;


    /**
     * @param applicationRequest
     * @return
     */
    CrDecisionRequest buildCrdecisionRequest(ApplicationRequest applicationRequest, String userId);


    /**
     * @param applicationRequest
     * @return
     */
    UpdateLoanDetailsRequest buildLoanDeatilsBasicInfoRequest(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */
    SaveVerificationRequest buildSaveVerificationBasicInfoRequest(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */
    List<SavePersonalInsuranceRequest> buildSavePersonalInsuranceRequest(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */
    SaveDisbursalMakerRequest buildSaveDisbursalMaker(ApplicationRequest applicationRequest, String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */


    ProcessDedupeRequest buildProcessDedupeRequest(ApplicationRequest applicationRequest, Applicant applicant, String userId) throws Exception;

    /**
     * @param updateDedupeRequest
     * @return
     */
    UpdateStatusDedupeResquest buildUpdateProcessDedupeRequest(UpdateDedupeRequest updateDedupeRequest, ApplicantdedupeRequest applicantdedupeRequest, String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */

    SearchExistingApplicantRequest buildSearchExistingApplicantRequest(ApplicationRequest applicationRequest, MiFinPanSearchBaseResponse miFinPanSearchBaseResponse,boolean hit_again ,String userId) throws Exception;

    /**
     * @param applicationRequest
     * @return
     */
    SaveFinancialInfoRequest buildSaveFinancialInfoRequest(ApplicationRequest applicationRequest, String userId) throws Exception;

}
