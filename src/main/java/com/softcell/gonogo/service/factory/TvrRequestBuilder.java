package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvr.TvrRequestDTO;

/**
 * Created by bhuvneshk on 9/8/17.
 */
public interface TvrRequestBuilder {

    /**
     *
     * @param goNoGoCustomerApplication
     * @return
     */
    TvrRequestDTO buildTvrRequest(GoNoGoCustomerApplication goNoGoCustomerApplication);
}
