package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.RollbackRequest;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierRequest;
import com.softcell.gonogo.serialnumbervalidation.carrier.CarrierResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by ibrar on 28/12/17.
 */

public interface CarrierSerialNumberValidationBuilder {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    CarrierRequest buildCarrierRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain carrierConfig);

    /**
     *
     * @param carrierResponse
     * @return
     */
    SerialNumberResponse buildCarrierResponse(CarrierResponse carrierResponse);

    /**
     *
     * @param rollbackRequest
     * @return
     */
    CarrierRequest buildCarrierSerialRollbackRequest(RollbackRequest rollbackRequest, WFJobCommDomain carrierConfig);

    /**
     *
     * @param carrierResponse
     * @return
     */
    SerialNumberResponse buildCarrierSerialRollbackResponse(CarrierResponse carrierResponse);
}
