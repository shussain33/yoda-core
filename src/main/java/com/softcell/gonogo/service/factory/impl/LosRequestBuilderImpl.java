package com.softcell.gonogo.service.factory.impl;


public class LosRequestBuilderImpl {/*

	ObjectFactory objectFactory ;
	private ApplicationRequest appRequest;
	private Header header;
	private Request applicantReq;
	private com.softcell.gonogo.model.core.Application application;
	private Applicant applicant;
	private PostIPA postIPA;
	

	
	public LosCustomerRequest buildLosCustomerRequest(GoNoGoCustomerApplication goNoGoCustomerApplication) {
		objectFactory = new ObjectFactory();
		LosCustomerRequest request = objectFactory.createLosCustomerRequest();
    	Application a = objectFactory.createApplication();
    	appRequest = goNoGoCustomerApplication.getApplicationRequest();
    	header = appRequest.getHeader();
    	applicantReq = appRequest.getRequest();
    	applicant = applicantReq.getApplicant();
    	application = applicantReq.getApplication();
    	Application.Customers c =  objectFactory.createApplicationCustomers();
		return request;
	}
	
	@Override
	public LosCustomerRequest buildLosCustomerRequest(ApplicationRequest appRequest,
			PostIpaRequest postIpaRequest) {
		objectFactory = new ObjectFactory();
		LosCustomerRequest request = objectFactory.createLosCustomerRequest();
    	Application a = objectFactory.createApplication();
    	this.appRequest = appRequest;
    	header = appRequest.getHeader();
    	applicantReq = appRequest.getRequest();
    	applicant = applicantReq.getApplicant();
    	application = applicantReq.getApplication();
    	postIPA = postIpaRequest.getPostIPA();
    	Application.Customers c =  objectFactory.createApplicationCustomers();
    	populateCustomerApplicationData(a);
    	request.setApplication(a);
		return request;
	}
	
	public static void main(String[] args) throws JAXBException {
		LosRequestBuilderImpl impl = new LosRequestBuilderImpl();

		ApplicationMongoRepository applicationRepository = new ApplicationMongoRepository(MongoConfig.getMongoTemplate());
		
		ApplicationRequest appRequest1 = applicationRepository.getApplicationRequestByRefId("161108TK0TK4", "4011");
		LosCustomerRequest request = impl.buildLosCustomerRequest(appRequest1 , null);
		
		JAXBContext context = JAXBContext.newInstance(LosCustomerRequest.class);
		Marshaller jaxbMarshaller = context.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		 StringWriter sw = new StringWriter();
		 jaxbMarshaller.marshal(request, sw);
	}

	private void populateCustomerApplicationData(Application a){
		List<JAXBElement<?>> content = a.getContent();
		content.add(objectFactory.createApplicationTmpApplicationId(appRequest.getRefID()));
		content.add(objectFactory.createApplicationSrcSystem("GONOGO"));
		content.add(objectFactory.createApplicationLosStage("HDBFS to confirm"));
		content.add(objectFactory.createApplicationDecision(""));
		content.add(objectFactory.createApplicationDecisionReason(""));
		content.add(objectFactory.createApplicationDeviation(""));
		content.add(objectFactory.createApplicationMitigant(""));
		content.add(objectFactory.createApplicationMbInterface("Y"));
		content.add(objectFactory.createApplicationPosidexInterface("N"));
		content.add(objectFactory.createApplicationPanValidation("Y"));
		content.add(objectFactory.createApplicationFiExecuted("N"));
		content.add(objectFactory.createApplicationBranch(""));
		content.add(objectFactory.createApplicationAppFormNo(""));
		content.add(objectFactory.createApplicationProduct(""));
		content.add(objectFactory.createApplicationScheme(postIPA.getScheme()));
		content.add(objectFactory.createApplicationLoanPurpose("CD_Purchase"));
		content.add(objectFactory.createApplicationLoanAmt(GNGStringUtil.getString(application.getLoanAmount())));
		content.add(objectFactory.createApplicationTenure(GNGStringUtil.getString(postIPA.getTenor())));
		content.add(objectFactory.createApplicationDsa(header.getDsaId()));
		content.add(objectFactory.createApplicationCreditProgram(""));
		content.add(objectFactory.createApplicationPromotionalScheme("XXX-NoScheme"));
		content.add(objectFactory.createApplicationRelationshipManager(""));
		content.add(objectFactory.createApplicationNoOfAdvEmi(GNGStringUtil.getString(postIPA.getAdvanceEmi())));
		content.add(objectFactory.createApplicationProposedEmi(GNGStringUtil.getString(postIPA.getEmi())));
		content.add(objectFactory.createApplicationMarginMoney(GNGStringUtil.getString(postIPA.getMarginMoney())));
		List<AssetDetails> assetDetails =postIPA.getAssetDetails();
		if(assetDetails != null && assetDetails.size() > 0){
			AssetDetails  assetDetail =assetDetails.get(0);
		content.add(objectFactory.createApplicationAssetCost(assetDetail.getPrice()));
		content.add(objectFactory.createApplicationAssetCatg(assetDetail.getAssetCtg()));
		content.add(objectFactory.createApplicationAssetMake(assetDetail.getAssetModelMake()));
		content.add(objectFactory.createApplicationManufacturer(assetDetail.getAssetMake()));
		content.add(objectFactory.createApplicationAssetType("CONSUMER DURABLE"));
		}
		
		List<String> assetModels = postIPA.getAssetModel();
		if(assetModels != null && assetModels.size() > 0){
			String assetModel = assetModels.get(0);
			content.add(objectFactory.createApplicationAssetModel(assetModel));
		}		
		content.add(objectFactory.createApplicationSupplier(header.getDealerId()));
		content.add(objectFactory.createApplicationLtv(""));
		content.add(objectFactory.createApplicationDti(""));
		content.add(populateCustomer());
		content.add(populateDocument());
	}
	
	private JAXBElement<Customers> populateCustomer(){
		Customers customers = objectFactory.createApplicationCustomers();
		List<Customer> customerList = customers.getCustomer();
		Customer customer = objectFactory.createCustomer();
		customer.setApplicantType("P");
		customer.setExistingLosCustomer("N");
		customer.setOldApplicationId("");
		customer.setRelationship("");
		Name applicantName = applicant.getApplicantName();
		if(applicantName != null){
			customer.setFirstName(applicantName.getFirstName());
			customer.setMiddleName(applicantName.getMiddleName());
			customer.setLastName(applicantName.getLastName());
		}
		customer.setDateOfBirth(null);

		List<Kyc> kycList = applicant.getKyc();
		customer.setPassportNo(GNGRequestResponseUtil.getKYC(KycJsonKeys.PASSPORT_NAME, kycList));
		customer.setPanNo(GNGRequestResponseUtil.getKYC(KycJsonKeys.PAN_NAME, kycList));
		customer.setDrivingLicense(GNGRequestResponseUtil.getKYC(KycJsonKeys.DRIVING_LICENSE_NAME, kycList));
		customer.setVoterId(GNGRequestResponseUtil.getKYC(KycJsonKeys.VOTERID_NAME, kycList));
		customer.setAadharCard(GNGRequestResponseUtil.getKYC(KycJsonKeys.AADHAR_NAME, kycList));
		customer.setMaritalStatus(applicant.getMaritalStatus());
		customer.setCitizenship("1");
		customer.setResidenceType("");
		customer.setRentPerMonth("");
		customer.setGender(applicant.getGender());
		
		List<Employment> empList = applicant.getEmployment();
		Employment employment = null;
		if(empList !=null && !empList.isEmpty()){
			for (Employment employmentObj : empList) {
				 	employment = employmentObj;
			}
		}
		customer.setConstitution(employment.getConstitution());
		customer.setCustomerCatg("");
		
		customer.setEmploymentType(employment.getEmploymentType());
		customer.setEmployerName(employment.getEmploymentName());
		customer.setYearsAtEmployment(GNGStringUtil.getString(employment.getTimeWithEmployer()));
		customer.setBusinessName(employment.getBusinessName());
		customer.setNaureOfBusiness("");
		customer.setIndustry(employment.getIndustryType());
		customer.setConsentToCall("Y");
		customer.setEducationalQualification(applicant.getEducation());
		customer.setPersonalQualification("");
		customer.setNoOfDependents(GNGStringUtil.getString(applicant.getNoOfDependents()));
		customer.setProfession("");
		customer.setTitle("");
		customer.setTotalExperience(employment.getWorkExps());
		customer.setIncomeDetails("");
		customer.setLiabilityDetails("");
		customer.setBankingDetails("");
		customer.setCustomerIncomes(populateCustomerIncomes());
		customer.setCustomerLiabilities(populateCustomerLiabilities());
		customer.setCustomerBankings(populateCustomerBankings());
		customer.setCustomerAddresses(populateCustomerAddress());
		customer.setCustomerPan(populateCustomerPan());
		customerList.add(customer);

    	JAXBElement<Customers> customersJaxb = objectFactory.createApplicationCustomers(customers);
		return customersJaxb;
	}

	private CustomerIncomes populateCustomerIncomes(){
		CustomerIncomes customerIncomes = objectFactory.createCustomerCustomerIncomes();
		List<CustomerIncome> customerIncomeList = customerIncomes.getCustomerIncome();
		IncomeDetails incomeDetail = applicant.getIncomeDetails();
		CustomerIncome customerIncome = objectFactory.createCustomerIncome();
		customerIncome.setAmtCurrent("");
		customerIncome.setAmtPrevious("");
		customerIncome.setFrequency("");
		customerIncome.setHead("");
		customerIncomeList.add(customerIncome);
		return customerIncomes;
	}
	
	private CustomerLiabilities populateCustomerLiabilities(){
		CustomerLiabilities customerLiabilities = objectFactory.createCustomerCustomerLiabilities();
		List<CustomerLiability> customerLiabilityList = customerLiabilities.getCustomerLiability();
		List<LoanDetails> loanDetails = applicant.getLoanDetails();
		for (LoanDetails loanDetail : loanDetails) {
			CustomerLiability customerLiability = objectFactory.createCustomerLiability();
			customerLiability.setProduct("");
			customerLiability.setFinName("");
			customerLiability.setLoanAmt(GNGStringUtil.getString(loanDetail.getLoanAmt()));
			customerLiability.setCurrPos("");
			customerLiability.setEmiStartDt(null);
			customerLiability.setEmiEndDt(null);
			customerLiability.setBounce("");
			customerLiability.setLast3MBounce("");
			customerLiability.setDpd30("");
			customerLiability.setEmiAmt(GNGStringUtil.getString(loanDetail.getEmiAmt()));
			customerLiability.setConsiderObligation("");
			customerLiability.setMultiplier("");
			customerLiability.setVerifySource("");
			customerLiability.setDocumentedMnths("");
			customerLiabilityList.add(customerLiability);
		}
		
		return customerLiabilities;
	}
	
	private CustomerBankings populateCustomerBankings() {

		CustomerBankings customerBankings = objectFactory.createCustomerCustomerBankings();
		List<CustomerBanking> customerBankingList = customerBankings.getCustomerBanking();
		List<BankingDetails> bankingDetails = applicant.getBankingDetails();
		
		for (BankingDetails bankingDetail : bankingDetails) {
			CustomerBanking customerBanking = objectFactory.createCustomerBanking();
			customerBanking.setBankName(bankingDetail.getBankName());
			customerBanking.setBankBranch(bankingDetail.getBranchName());
			customerBanking.setBanking("");
			customerBanking.setAccountType(bankingDetail.getAccountType());
			customerBanking.setAccountNo(bankingDetail.getAccountNumber());
			Name accntHolderName = bankingDetail.getAccountHolderName();
			customerBanking.setAcHolderName(GNGRequestResponseUtil.convertNameToFullNameString(accntHolderName));
			customerBanking.setNoOfYearsHeld("");
			customerBanking.setAvgBalance12M("");
			customerBanking.setCreditTxn12M("");
			customerBanking.setDebitTxn12M("");
			customerBanking.setDebitTxn12MAmt("");
			customerBanking.setInwardReturns12M("");
			customerBanking.setOutwardReturns12M("");
			customerBanking.setMedian("");
			customerBanking.setLimit("");
			customerBanking.setRemarks("");
			customerBankingList.add(customerBanking);
		}
		return customerBankings;
	}

	private CustomerAddresses populateCustomerAddress(){
		CustomerAddresses customerAddresses = objectFactory.createCustomerCustomerAddresses();
		List<CustomerAddress> customerList = customerAddresses.getCustomerAddress();
		List<com.softcell.gonogo.model.address.CustomerAddress> address = applicant.getAddress();
		for (com.softcell.gonogo.model.address.CustomerAddress gngCustomerAddress : address) {
			CustomerAddress customerAddress = objectFactory.createCustomerAddress();
			customerAddress.setAddressType(gngCustomerAddress.getAddressType());
			customerAddress.setAddressLine1(gngCustomerAddress.getAddressLine1());
			customerAddress.setAddressLine2(gngCustomerAddress.getAddressLine2());
			customerAddress.setAddressLine3("");
			customerAddress.setCountry(gngCustomerAddress.getCountry());
			customerAddress.setState(gngCustomerAddress.getState());
			customerAddress.setCity(gngCustomerAddress.getCity());
			customerAddress.setPincode(GNGStringUtil.getString(gngCustomerAddress.getPin()));
			customerAddress.setLandmark(gngCustomerAddress.getLandMark());
			Phone phone = new Phone();
			customerAddress.setAreacode(phone.getAreaCode());
			customerAddress.setPhone1(phone.getPhoneNumber());
			customerAddress.setExtension(phone.getExtension());
			customerAddress.setMobile(phone.getPhoneNumber());
			customerAddress.setEmailId("");
			customerAddress.setMailingFlag("");
			customerAddress.setYrsAtCurrAdd(GNGStringUtil.getString(gngCustomerAddress.getTimeAtAddress()));	
			customerAddress.setYrsAtCity(GNGStringUtil.getString(gngCustomerAddress.getYearAtCity()));
			customerAddress.setDistanceInKm("1");
			customerAddress.setPosidexReqFile("");
			customerAddress.setMbReqFile("");
			customerList.add(customerAddress);
		}
		
		return customerAddresses;
	}
	
	private CustomerPan populateCustomerPan(){
		CustomerPan customerPan = objectFactory.createCustomerCustomerPan();
		List<Kyc> kycList = applicant.getKyc();
		Kyc pandetails = GNGRequestResponseUtil.getKYCObject(KycJsonKeys.PAN_NAME, kycList);
		customerPan.setPan(pandetails.getKycNumber());
		customerPan.setPanStatus(pandetails.getKycStatus());
		Name name = applicant.getApplicantName();
		customerPan.setFirstName(name.getFirstName());
		customerPan.setMiddleName(name.getMiddleName());
		customerPan.setLastName(name.getLastName());
		customerPan.setPanTitle(pandetails.getKycName());
		customerPan.setDateLastUpdated(null);
		return customerPan;
	}
	
	private JAXBElement<Documents> populateDocument(){
		Documents documents = objectFactory.createApplicationDocuments();
		List<Document> documentList = documents.getDocument();
		Document document = objectFactory.createDocument();
		document.setDocId("k");
		document.setDocStatus("");
		document.setTargetDate(null);
		document.setReason("");
		document.setRemarks("");
		document.setOriginal("");
		documentList.add(document);
		JAXBElement<Documents> documentJaxb = objectFactory.createApplicationDocuments(documents);
		return documentJaxb;
	}


	
	*/
}
