package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.serialnumbervalidation.lg.LgRequest;

/**
 * @author prasenjit wadmare
 * @date 09 Jan 2018
 */

public interface LgSerialNumberValidationBuilder {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    LgRequest buildLgSerialRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    LgRequest buildLgImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest);


}
