package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.tkil.TKILRequest;

/**
 * Created by sampat on 23/8/17.
 */
public interface TkilRequestBuilder {

    /**
     *
     * @param postIpaRequest
     * @param appRequest
     * @return
     */
    TKILRequest buildTkilRequest(PostIpaRequest postIpaRequest, ApplicationRequest appRequest)  throws Exception;
}
