package com.softcell.gonogo.service.factory.serialnumbervalidation;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.onida.OnidaRequest;
import com.softcell.gonogo.serialnumbervalidation.onida.OnidaResponse;

/**
 * Created by ibrar on 12/12/17.
 */

public interface OnidaSerialNumberValidationBuilder {

    /**
     *
     * @param serialSaleConfirmationRequest
     * @return
     */
    OnidaRequest buildOnidaSerialNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest);

    /**
     *
     * @param onidaResponse
     * @return
     */
    SerialNumberResponse buildOnidaSerialNumberResponse(OnidaResponse onidaResponse);
}


