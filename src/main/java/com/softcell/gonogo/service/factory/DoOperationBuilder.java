package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.dooperation.DoOperationLog;

/**
 * Created by mahesh on 30/8/17.
 */
public interface DoOperationBuilder {

    /**
     * @param header
     * @param refID
     * @return
     */
    PostIpaRequest buildPostIpaRequest(Header header, String refID);

    /**
     * @param refID
     * @param header
     * @param doStatus
     * @return
     */
    DoOperationLog buildDoOperationLog(String refID, FileHeader header, String doStatus);
}
