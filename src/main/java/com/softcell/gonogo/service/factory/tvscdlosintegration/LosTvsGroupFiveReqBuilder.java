package com.softcell.gonogo.service.factory.tvscdlosintegration;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.DocumentListForGroupFive;
import com.softcell.gonogo.model.los.tvs.InsertOrUpdateTvsRecordGroupFive;

public interface LosTvsGroupFiveReqBuilder {
    InsertOrUpdateTvsRecordGroupFive buildTvsLosGroupFiveRequest(GoNoGoCustomerApplication gonogoCustomerApplicationFive, DocumentListForGroupFive uploadFileDetails);
}
