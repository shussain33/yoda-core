package com.softcell.gonogo.service.factory;

import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.gonogo.model.core.DeliveryOrderReport;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.core.digitization.AchMandateForm;
import com.softcell.gonogo.model.request.FileUploadRequest;
import com.softcell.gonogo.model.request.InvoiceDetailsRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;

/**
 * Created by mahesh on 6/7/17.
 */
public interface DigitizationBuilder {


    /**
     * @param postIpaRequest
     * @return
     */
    public DeliveryOrderReport buildDeliveryOrderReport(PostIpaRequest postIpaRequest ,String fileOperationType);

    /**
     *
     * @param postIpaRequest
     * @return
     */
    public FileUploadRequest buildDeliveryOrderUploadRequest(PostIpaRequest postIpaRequest ,String modelNo);

    /**
     *
     * @param invoiceDetailsRequest
     * @param fileName
     * @return
     */
    FileUploadRequest buildDigitizationFormUploadRequest(InvoiceDetailsRequest invoiceDetailsRequest, String fileName);

    /**
     *
     * @param application
     * @param postIpa
     * @param templateConfiguration
     * @return
     */
    AchMandateForm buildAchMandateForm(GoNoGoCroApplicationResponse application, PostIPA postIpa, TemplateConfiguration templateConfiguration) throws Exception;

}
