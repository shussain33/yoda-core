package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.softcell.constants.LosTvsActionEnums;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosTvsGroupTwoReqBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LosTvsGroupTwoReqBuilderImpl implements LosTvsGroupTwoReqBuilder {
    @Override
    public InsertOrUpdateTvsRecordGroupTwo buildLosTvsGroupTwoRequest(GoNoGoCustomerApplication gonogoCustomerApplication) {
        com.softcell.gonogo.model.los.tvs.Application application = new com.softcell.gonogo.model.los.tvs.Application();
        com.softcell.gonogo.model.los.tvs.CreditVidya creditVidya = new com.softcell.gonogo.model.los.tvs.CreditVidya();

        com.softcell.gonogo.model.los.tvs.CreditCardAccount creditCardAccount = new com.softcell.gonogo.model.los.tvs.CreditCardAccount();
        com.softcell.gonogo.model.los.tvs.AuxiliaryInfo auxiliaryInfo = new com.softcell.gonogo.model.los.tvs.AuxiliaryInfo();
        com.softcell.gonogo.model.los.tvs.AppographicProfile appographicProfile = new com.softcell.gonogo.model.los.tvs.AppographicProfile();
        com.softcell.gonogo.model.los.tvs.Cards cards = new com.softcell.gonogo.model.los.tvs.Cards();
        com.softcell.gonogo.model.los.tvs.CreditScore creditScore = new com.softcell.gonogo.model.los.tvs.CreditScore();
        com.softcell.gonogo.model.los.tvs.FraudCheck fraudCheck = new com.softcell.gonogo.model.los.tvs.FraudCheck();

        com.softcell.gonogo.model.los.tvs.Income income = new com.softcell.gonogo.model.los.tvs.Income();
        com.softcell.gonogo.model.los.tvs.SalaryCredit salaryCredit = new com.softcell.gonogo.model.los.tvs.SalaryCredit();

        com.softcell.gonogo.model.los.tvs.LoanAccount loanAccount = new com.softcell.gonogo.model.los.tvs.LoanAccount();
        com.softcell.gonogo.model.los.tvs.Loans loans = new com.softcell.gonogo.model.los.tvs.Loans();

        com.softcell.gonogo.model.los.tvs.LocationProfile locationProfile = new com.softcell.gonogo.model.los.tvs.LocationProfile();

        com.softcell.gonogo.model.los.tvs.UtilityAccount utilityAccount = new com.softcell.gonogo.model.los.tvs.UtilityAccount();
        com.softcell.gonogo.model.los.tvs.AggregatePayment aggregatePayment = new com.softcell.gonogo.model.los.tvs.AggregatePayment();

        com.softcell.gonogo.model.los.tvs.LosDto.RiskEvents riskEvents = new com.softcell.gonogo.model.los.tvs.LosDto.RiskEvents();
        com.softcell.gonogo.model.los.tvs.LosDto.RiskEvents riskEventsLosDto = new com.softcell.gonogo.model.los.tvs.LosDto.RiskEvents();

        com.softcell.gonogo.model.los.tvs.RiskEvents riskEventsDto = new com.softcell.gonogo.model.los.tvs.RiskEvents();

        com.softcell.gonogo.model.los.tvs.ContactInfo contactInfo = new com.softcell.gonogo.model.los.tvs.ContactInfo();
        com.softcell.gonogo.model.los.tvs.AlternateContact alternateContact = new com.softcell.gonogo.model.los.tvs.AlternateContact();


        com.softcell.gonogo.model.los.tvs.Metadata metadata = new com.softcell.gonogo.model.los.tvs.Metadata();
        com.softcell.gonogo.model.los.tvs.DataThickness dataThickness = new com.softcell.gonogo.model.los.tvs.DataThickness();
        com.softcell.gonogo.model.los.tvs.PermissionGiven permissionGiven = new com.softcell.gonogo.model.los.tvs.PermissionGiven();

        com.softcell.gonogo.model.los.tvs.BankAccount bankAccount = new com.softcell.gonogo.model.los.tvs.BankAccount();
        com.softcell.gonogo.model.los.tvs.Accounts accounts = new com.softcell.gonogo.model.los.tvs.Accounts();
        com.softcell.gonogo.model.los.tvs.AggregateFlow aggregateFlow = new com.softcell.gonogo.model.los.tvs.AggregateFlow();

        com.softcell.gonogo.model.los.tvs.Cro cro = new com.softcell.gonogo.model.los.tvs.Cro();
        com.softcell.gonogo.model.los.tvs.Posidex posidex = new com.softcell.gonogo.model.los.tvs.Posidex();
        com.softcell.gonogo.model.los.tvs.WelcomeScreen welcomeScreen = new com.softcell.gonogo.model.los.tvs.WelcomeScreen();
        com.softcell.gonogo.model.los.tvs.DoStages doStages = new com.softcell.gonogo.model.los.tvs.DoStages();
        com.softcell.gonogo.model.los.tvs.KarzaElectricityBill karzaElectricityBill = new com.softcell.gonogo.model.los.tvs.KarzaElectricityBill();
        com.softcell.gonogo.model.los.tvs.KarzaVoterID karzaVoterID = new com.softcell.gonogo.model.los.tvs.KarzaVoterID();
        com.softcell.gonogo.model.los.tvs.Scoring scoring = new com.softcell.gonogo.model.los.tvs.Scoring();
        com.softcell.gonogo.model.los.tvs.ScoreList scoreList = new com.softcell.gonogo.model.los.tvs.ScoreList();


        com.softcell.gonogo.model.los.tvs.Surrogate surrogate = new com.softcell.gonogo.model.los.tvs.Surrogate();
        com.softcell.gonogo.model.los.tvs.Business business = new com.softcell.gonogo.model.los.tvs.Business();
        com.softcell.gonogo.model.los.tvs.CreditCard creditCard = new com.softcell.gonogo.model.los.tvs.CreditCard();
        com.softcell.gonogo.model.los.tvs.DebitCard debitCard = new com.softcell.gonogo.model.los.tvs.DebitCard();
        com.softcell.gonogo.model.los.tvs.OwnedCar ownedCar = new com.softcell.gonogo.model.los.tvs.OwnedCar();
        com.softcell.gonogo.model.los.tvs.OwnedHouse ownedHouse = new com.softcell.gonogo.model.los.tvs.OwnedHouse();
        com.softcell.gonogo.model.los.tvs.SalaryHome salaryHome = new com.softcell.gonogo.model.los.tvs.SalaryHome();

        com.softcell.gonogo.model.los.tvs.EmailIds emailIds = new com.softcell.gonogo.model.los.tvs.EmailIds();
        com.softcell.gonogo.model.los.tvs.MobileNos mobileNos = new com.softcell.gonogo.model.los.tvs.MobileNos();
        com.softcell.gonogo.model.los.tvs.KarzaLPG karzaLPG = new com.softcell.gonogo.model.los.tvs.KarzaLPG();
        com.softcell.gonogo.model.los.tvs.Saathi saathi = new com.softcell.gonogo.model.los.tvs.Saathi();
        com.softcell.gonogo.model.los.tvs.KarzaDrivingLicense karzaDrivingLicense = new com.softcell.gonogo.model.los.tvs.KarzaDrivingLicense();
        com.softcell.gonogo.model.los.tvs.CDLos CDLos = new com.softcell.gonogo.model.los.tvs.CDLos();

        MultibureauCIBIL multiBureauCIBIL = new MultibureauCIBIL();
        AddressList addressList = new AddressList();
        EnquiryList enquiryList = new EnquiryList();
        AccountList accountList = new AccountList();
        PhoneList phoneList = new PhoneList();
        com.softcell.gonogo.model.los.tvs.Name name = new com.softcell.gonogo.model.los.tvs.Name();

        SecondaryMatches secondaryMatches = new SecondaryMatches();
        SecIdList secIdList = new SecIdList();

        SecName secName = new SecName();
        SecAddressList secAddressList = new SecAddressList();
        SecPhoneList secPhoneList = new SecPhoneList();

        IdList idList = new IdList();

        EmploymentList employmentList = new EmploymentList();
        com.softcell.gonogo.model.los.tvs.Header header =  new com.softcell.gonogo.model.los.tvs.Header();


        MultibureauCRIF multibureauCRIF = new MultibureauCRIF();
        INDVReports INDVReports = new INDVReports();
        INDVReport INDVReport = new INDVReport();

        IndvResponses indvResponses = new IndvResponses();
        Summary summary =  new Summary();
        SecSummary secSummary = new SecSummary();
        PrimarySummary primarySummary = new PrimarySummary();
        Alerts alerts = new Alerts();
        Alert alert = new Alert();

        Request request = new Request();

        Ids ids = new Ids();
        Id id =  new Id();
        Phones phones = new Phones();
        com.softcell.gonogo.model.los.tvs.Phone phone = new com.softcell.gonogo.model.los.tvs.Phone();
        Addresses addresses = new Addresses();
        Address address = new Address();

        PrintableReport printableReport = new PrintableReport();

        AccountSummary accountSummary = new AccountSummary();
        DrivedAttributes drivedAttributes = new DrivedAttributes();
        SecondaryAccountSummary secondaryAccountSummary = new SecondaryAccountSummary();
        PrimaryAccountsSummary primaryAccountsSummary = new PrimaryAccountsSummary();

        com.softcell.gonogo.model.los.tvs.PersonalInfoVariation personalInfoVariation = new com.softcell.gonogo.model.los.tvs.PersonalInfoVariation();
        StatusDetails statusDetails = new StatusDetails();

        InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordForGroup002 = new InsertOrUpdateTvsRecordGroupTwo();

        doStages.setAgreementno("");
        doStages.setDisbursmentdate("");
        doStages.setDisbursmentuserid("");
        doStages.setIsagreementgenerated("");
        doStages.setIsapplicationgenerated("");
        doStages.setIsdogenerated("");
        doStages.setIsemailsent("");

        try {
            if (gonogoCustomerApplication != null) {

                if (StringUtils.isNotBlank(gonogoCustomerApplication.getAgreementNum())) {
                    application.setAggrementNumber(gonogoCustomerApplication.getAgreementNum());
                }
                if (gonogoCustomerApplication.getApplicationRequest() != null
                        && gonogoCustomerApplication.getApplicationRequest().getHeader() != null) {

                    if (StringUtils.isNotBlank(
                            gonogoCustomerApplication.getApplicationRequest().getHeader().getApplicationId())) {
                        application.setAppId(
                                gonogoCustomerApplication.getApplicationRequest().getHeader().getApplicationId());
                    }
                    if (StringUtils.isNotBlank(
                            gonogoCustomerApplication.getApplicationRequest().getHeader().getApplicationSource())) {
                        application.setApplicationSource(
                                gonogoCustomerApplication.getApplicationRequest().getHeader().getApplicationSource());
                    }
                    if (StringUtils
                            .isNotBlank(gonogoCustomerApplication.getApplicationRequest().getHeader().getBranchCode())) {
                        application.setBranchCode(
                                gonogoCustomerApplication.getApplicationRequest().getHeader().getBranchCode());
                    }
                    if (StringUtils
                            .isNotBlank(gonogoCustomerApplication.getApplicationRequest().getHeader().getCroId())) {
                        application.setCroId(gonogoCustomerApplication.getApplicationRequest().getHeader().getCroId());
                    }
                    if (StringUtils
                            .isNotBlank(gonogoCustomerApplication.getApplicationRequest().getHeader().getDealerId())) {
                        application.setDealerId(
                                gonogoCustomerApplication.getApplicationRequest().getHeader().getDealerId());
                    }
                    if (StringUtils
                            .isNotBlank(gonogoCustomerApplication.getApplicationRequest().getHeader().getDsaId())) {
                        application.setDsald(gonogoCustomerApplication.getApplicationRequest().getHeader().getDsaId());
                    }

                    application.setDtSubmit("");

                    if (StringUtils.isNotBlank(
                            gonogoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId())) {
                        application.setInstitutionId(
                                gonogoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                    }

                    application.setProduct("");

                    if (StringUtils
                            .isNotBlank(gonogoCustomerApplication.getApplicationRequest().getHeader().getSourceId())) {
                        application.setSourceId(
                                gonogoCustomerApplication.getApplicationRequest().getHeader().getSourceId());
                    }

                }
            }
            if (gonogoCustomerApplication != null) {
                if (gonogoCustomerApplication.getApplicantComponentResponse() != null &&
                        gonogoCustomerApplication.getApplicantComponentResponse().getCreditVidyaResponse() != null
                        && gonogoCustomerApplication.getApplicantComponentResponse().getCreditVidyaResponse().getPersonalDetails() != null) {

                    com.softcell.gonogo.model.creditVidya.PersonalDetails personalDetails = gonogoCustomerApplication.getApplicantComponentResponse().getCreditVidyaResponse().getPersonalDetails();

                    if (personalDetails.getAuxiliaryInfo() != null) {
                        auxiliaryInfo.setDeviceName(personalDetails.getAuxiliaryInfo().getDeviceName());
                        auxiliaryInfo.setDevicePrice(personalDetails.getAuxiliaryInfo().getDevicePrice().toString());

                        creditCardAccount.setAuxiliaryInfo(auxiliaryInfo);
                    }

                    if (personalDetails.getAppographicProfile() != null) {
                        com.softcell.gonogo.model.creditVidya.AppographicProfile appographicProfileRef = personalDetails.getAppographicProfile();

                        appographicProfile.setCountOfapps(appographicProfileRef.getCountOfapps().toString());
                        appographicProfile.setCountOfDistinctAppCategories(appographicProfileRef.getCountOfDistinctAppCategories().toString());
                        appographicProfile.setCountOfFinancialApps(appographicProfileRef.getCountOfFinancialApps().toString());
                        appographicProfile.setCountOfFitnessApps(appographicProfileRef.getCountOfFitnessApps().toString());
                        appographicProfile.setCountOfShoppingApps(appographicProfileRef.getCountOfShoppingApps().toString());
                        appographicProfile.setCountOfSocialApps(appographicProfileRef.getCountOfSocialApps().toString());
                        appographicProfile.setCountOfWalletApps(appographicProfileRef.getCountOfWalletApps().toString());

                        creditCardAccount.setAppographicProfile(appographicProfile);


                        //creditCardAccount.setContactInfo(contactInfo);

                        if (personalDetails.getCreditCardAccountInfo() != null) {
                            creditCardAccount.setAvgCCTraxnAmountLast3Mon(personalDetails.getCreditCardAccountInfo().getAvgCCTraxnAmountLast3Mon().toString());
                            creditCardAccount.setCcDataSufficiencyFlag(personalDetails.getCreditCardAccountInfo().getCcDataSufficiencyFlag().toString());
                        }
                        if (CollectionUtils.isNotEmpty(personalDetails.getCreditCardAccountInfo().getCreditCards())) ;
                        {
                            List<Cards> cardsList = new ArrayList<Cards>();
                            cards.setCountOverdueEver(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getCountOverdueEver().toString());
                            cards.setCountOverdueLast3Months(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getCountOverdueLast3Months().toString());
                            cards.setCreditLimit(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getCreditLimit().toString());
                            cards.setIssuerName(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getIssuerName());
                            cards.setLatestCCDueDate(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getLatestCCDueDate());
                            cards.setMaxCreditUtilization(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getMaxCreditUtilization().toString());
                            cards.setTotalCCPaymentMonthYear0(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getTotalCCPaymentMonthYear0().toString());
                            cards.setTotalCCPaymentMonthYear1(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getTotalCCPaymentMonthYear1().toString());
                            cards.setTotalCCPaymentMonthYear2(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getTotalCCPaymentMonthYear2().toString());
                            cards.setTotalCCTraxnAmountMonthYear0(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getTotalCCTraxnAmountMonthYear0().toString());
                            cards.setTotalCCTraxnAmountMonthYear1(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getTotalCCTraxnAmountMonthYear1().toString());
                            cards.setTotalCCTraxnAmountMonthYear2(personalDetails.getCreditCardAccountInfo().getCreditCards().get(0).getTotalCCTraxnAmountMonthYear2().toString());
                            cards.setSno("");
                            cardsList.add(cards);
                            com.softcell.gonogo.model.los.tvs.Cards[] cardsArr = cardsList.toArray(new com.softcell.gonogo.model.los.tvs.Cards[cardsList.size()]);
                            creditCardAccount.setCards(cardsArr);
                        }
                    }
                    if (personalDetails.getContactInfo() != null && personalDetails.getContactInfo().getAlternateContact() != null) {
                        alternateContact.setFreqContact1(personalDetails.getContactInfo().getAlternateContact().getFreqContact1());
                        alternateContact.setFreqContact2(personalDetails.getContactInfo().getAlternateContact().getFreqContact2());
                        alternateContact.setFreqContact3(personalDetails.getContactInfo().getAlternateContact().getFreqContact3());
                        alternateContact.setFreqFamilyContact(personalDetails.getContactInfo().getAlternateContact().getFreqFamilyContact());
                        alternateContact.setRegularlyCalledContact1(personalDetails.getContactInfo().getAlternateContact().getRegularlyCalledContact1());
                        alternateContact.setRegularlyCalledContact2(personalDetails.getContactInfo().getAlternateContact().getRegularlyCalledContact2());
                        alternateContact.setRegularlyCalledContact3(personalDetails.getContactInfo().getAlternateContact().getRegularlyCalledContact3());

                        contactInfo.setAlternateContact(alternateContact);

                    }
                    if (personalDetails.getContactInfo() != null && CollectionUtils.isNotEmpty(personalDetails.getContactInfo().getEmailInfoList())) {
                        InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwo = new InsertOrUpdateTvsRecordGroupTwo();
                        com.softcell.gonogo.model.los.tvs.CreditVidya CV = new com.softcell.gonogo.model.los.tvs.CreditVidya();
                        com.softcell.gonogo.model.los.tvs.CreditCardAccount CCAccount = new com.softcell.gonogo.model.los.tvs.CreditCardAccount();
                        com.softcell.gonogo.model.los.tvs.ContactInfo contInfo = new com.softcell.gonogo.model.los.tvs.ContactInfo();
                        List<EmailIds> emailIdsList = new ArrayList<EmailIds>();
                        emailIds.setCompanyCategory(personalDetails.getContactInfo().getEmailInfoList().get(0).getCompanyCategory());
                        emailIds.setCompanyName(personalDetails.getContactInfo().getEmailInfoList().get(0).getCompanyName());
                        emailIds.setCorporateFlag(personalDetails.getContactInfo().getEmailInfoList().get(0).getCorporateFlag().toString());
                        emailIds.setEmailID(personalDetails.getContactInfo().getEmailInfoList().get(0).getEmailID());
                        emailIds.setSno("");
                        emailIdsList.add(emailIds);
                        com.softcell.gonogo.model.los.tvs.EmailIds[] emailIdsArr = emailIdsList.toArray(new com.softcell.gonogo.model.los.tvs.EmailIds[emailIdsList.size()]);
                        contInfo.setEmailIds(emailIdsArr);
                        CCAccount.setContactInfo(contInfo);
                        CV.setCreditCardAccount(CCAccount);
                        insertOrUpdateTvsRecordGroupTwo.setCreditVidya(CV);

                    }
                    if (personalDetails.getContactInfo() != null)////////MOBILENO is a list///////////
                    {
                        InsertOrUpdateTvsRecordGroupTwo insertOrUpdateTvsRecordGroupTwo = new InsertOrUpdateTvsRecordGroupTwo();
                        com.softcell.gonogo.model.los.tvs.CreditVidya CV = new com.softcell.gonogo.model.los.tvs.CreditVidya();
                        com.softcell.gonogo.model.los.tvs.CreditCardAccount CCAccount = new com.softcell.gonogo.model.los.tvs.CreditCardAccount();
                        com.softcell.gonogo.model.los.tvs.ContactInfo contInfo = new com.softcell.gonogo.model.los.tvs.ContactInfo();
                        List<MobileNos> mobileNosList = new ArrayList<MobileNos>();
                        mobileNos.setNumber(personalDetails.getContactInfo().getMobileNos().get(0));
                        mobileNos.setSno("");
                        mobileNosList.add(mobileNos);
                        com.softcell.gonogo.model.los.tvs.MobileNos[] mobileNosArr = mobileNosList.toArray(new com.softcell.gonogo.model.los.tvs.MobileNos[mobileNosList.size()]);

                        contInfo.setMobileNos(mobileNosArr);
                        CCAccount.setContactInfo(contInfo);
                        CV.setCreditCardAccount(CCAccount);
                        insertOrUpdateTvsRecordGroupTwo.setCreditVidya(CV);
                    }

                    if (personalDetails.getCreditCardAccountInfo() != null) {
                        creditCardAccount.setCountActiveCreditCards(personalDetails.getCreditCardAccountInfo().getCountActiveCreditCards().toString());
                        creditCardAccount.setCountTotalCreditCard(personalDetails.getCreditCardAccountInfo().getCountTotalCreditCard().toString());

                    }
                    if (personalDetails.getCreditScore() != null) {
                        creditScore.setAlternateRiskScore(personalDetails.getCreditScore().getAlternateRiskScore().toString());
                        creditScore.setScoringSufficiency(personalDetails.getCreditScore().getScoringSufficiency().toString());

                        creditCardAccount.setCreditScore(creditScore);


                    }
                    if (personalDetails.getFraudCheck() != null) {
                        fraudCheck.setGpsLocationMasked(personalDetails.getFraudCheck().getGpsLocationMasked());
                        fraudCheck.setHighSimChanger(personalDetails.getFraudCheck().getHighSimChanger());
                        fraudCheck.setImeiMasked(personalDetails.getFraudCheck().getImeiMasked());
                        fraudCheck.setRootedDeviceAdvanced(personalDetails.getFraudCheck().getRootedDeviceAdvanced().toString());

                        creditCardAccount.setFraudCheck(fraudCheck);
                    }
                    if (personalDetails.getIncome() != null) {
                        income.setIncomePredictiondataSufficiencyFlag(personalDetails.getIncome().getIncomePredictiondataSufficiencyFlag().toString());
                        income.setPredictedIncome(personalDetails.getIncome().getPredictedIncome().toString());
                        income.setPredictedIncomeConfidenceLevel(personalDetails.getIncome().getPredictedIncomeConfidenceLevel());
                        if (personalDetails.getIncome().getSalaryCredit() != null) {
                            salaryCredit.setSalaryCreditAccount(personalDetails.getIncome().getSalaryCredit().getSalaryCreditAccount());
                            salaryCredit.setSalaryCreditDate(personalDetails.getIncome().getSalaryCredit().getSalaryCreditDate().toString());
                            salaryCredit.setSalaryWithKeywordMonth0(personalDetails.getIncome().getSalaryCredit().getSalaryWithKeywordMonth0().toString());
                            salaryCredit.setSalaryWithKeywordMonth1(personalDetails.getIncome().getSalaryCredit().getSalaryWithKeywordMonth1().toString());
                            salaryCredit.setSalaryWithKeywordMonth2(personalDetails.getIncome().getSalaryCredit().getSalaryWithKeywordMonth2().toString());
                        }
                        income.setSalaryCredit(salaryCredit);
                        creditCardAccount.setIncome(income);
                    }
                    if (personalDetails.getLoanAccountInfo() != null) {
                        loanAccount.setCountLoanAccounts(personalDetails.getLoanAccountInfo().getCountLoanAccounts().toString());
                        loanAccount.setCountSecuredLoanAccounts(personalDetails.getLoanAccountInfo().getCountSecuredLoanAccounts().toString());
                        loanAccount.setCountUnsecuredLoanAccounts(personalDetails.getLoanAccountInfo().getCountUnsecuredLoanAccounts().toString());

                        loanAccount.setLoanDataSufficiencyFlag(personalDetails.getLoanAccountInfo().getLoanDataSufficiencyFlag().toString());

                        loanAccount.setMonthsLoanDataAvailable(personalDetails.getLoanAccountInfo().getMonthsLoanDataAvailable().toString());

                        if (CollectionUtils.isNotEmpty(personalDetails.getLoanAccountInfo().getLoans())) {
                            loans.setCountOverdueEmiEver(personalDetails.getLoanAccountInfo().getLoans().get(0).getCountOverdueEmiEver().toString());
                            loans.setCountOverdueEmiLast3Months(personalDetails.getLoanAccountInfo().getLoans().get(0).getCountOverdueEmiLast3Months().toString());
                            loans.setEmiAmountMonth0(personalDetails.getLoanAccountInfo().getLoans().get(0).getEmiAmountMonth0().toString());
                            loans.setEmiAmountMonth1(personalDetails.getLoanAccountInfo().getLoans().get(0).getEmiAmountMonth1().toString());
                            loans.setEmiAmountMonth2(personalDetails.getLoanAccountInfo().getLoans().get(0).getEmiAmountMonth2().toString());
                            loans.setEmiDueDate(personalDetails.getLoanAccountInfo().getLoans().get(0).getEmiDueDate().toString());
                            loans.setLenderName(personalDetails.getLoanAccountInfo().getLoans().get(0).getLenderName());
                            loans.setLoanType(personalDetails.getLoanAccountInfo().getLoans().get(0).getLoanType());
                            loans.setLoanNo("");
                        }
                        List<Loans> LoansList = new ArrayList<Loans>();
                        LoansList.add(loans);
                        com.softcell.gonogo.model.los.tvs.Loans[] loansArray = LoansList.toArray(new com.softcell.gonogo.model.los.tvs.Loans[LoansList.size()]);

                        loanAccount.setLoans(loansArray);
                        creditCardAccount.setLoanAccount(loanAccount);

                    }
                    if (personalDetails.getLocationProfile() != null) {
                        locationProfile.setAccuracyProminentPlace1(personalDetails.getLocationProfile().getProminentPlace1());
                        locationProfile.setAccuracyProminentPlace2(personalDetails.getLocationProfile().getProminentPlace2());
                        locationProfile.setAccuracyProminentPlace3(personalDetails.getLocationProfile().getProminentPlace3());
                        locationProfile.setCountLocationReadingCollected(personalDetails.getLocationProfile().getCountLocationReadingCollected().toString());
                        locationProfile.setDaysLocationMonitored(personalDetails.getLocationProfile().getDaysLocationMonitored().toString());
                        locationProfile.setPointOfInstallationAddress(personalDetails.getLocationProfile().getInstallationAddress());
                        locationProfile.setPointOfInstallationAddressAccuracy(personalDetails.getLocationProfile().getPointOfInstallationAddressAccuracy().toString());
                        locationProfile.setProminentPlace1(personalDetails.getLocationProfile().getProminentPlace1());
                        locationProfile.setProminentPlace2(personalDetails.getLocationProfile().getProminentPlace2());
                        locationProfile.setProminentPlace3(personalDetails.getLocationProfile().getProminentPlace3());

                        creditCardAccount.setLocationProfile(locationProfile);

                    }
                    if (personalDetails.getCreditCardAccountInfo() != null) {

                        creditCardAccount.setMonthsCCDataAvailable(personalDetails.getCreditCardAccountInfo().getMonthsCCDataAvailable().toString());

                        if (personalDetails.getUtilityAccountInfo().getAggregatePayment() != null) {
                            aggregatePayment.setAverageElectricityBillAmountLast3Mon(personalDetails.getUtilityAccountInfo().getAggregatePayment().getAverageElectricityBillAmountLast3Mon().toString());
                            aggregatePayment.setAverageInternetBillAmountLast3Mon(personalDetails.getUtilityAccountInfo().getAggregatePayment().getAverageInternetBillAmountLast3Mon().toString());
                            aggregatePayment.setAverageLandlineBillAmountLast3Mon(personalDetails.getUtilityAccountInfo().getAggregatePayment().getAverageLandlineBillAmountLast3Mon().toString());
                            aggregatePayment.setAverageMobileBillAmountLast3Mon(personalDetails.getUtilityAccountInfo().getAggregatePayment().getAverageMobileBillAmountLast3Mon().toString());
                            aggregatePayment.setAverageMobileRechargeAmountLast3Mon(personalDetails.getUtilityAccountInfo().getAggregatePayment().getAverageMobileRechargeAmountLast3Mon().toString());
                            aggregatePayment.setAvgGrossUtilitySpendLast3Mon(personalDetails.getUtilityAccountInfo().getAggregatePayment().getAvgGrossUtilitySpendLast3Mon().toString());
                            aggregatePayment.setPrepaidPostpaidFlag(personalDetails.getUtilityAccountInfo().getAggregatePayment().getPrepaidPostpaidFlag().toString());

                            utilityAccount.setAggregatePayment(aggregatePayment);

                            utilityAccount.setCountUtilityAccount(personalDetails.getUtilityAccountInfo().getCountUtilityAccount().toString());
                            utilityAccount.setMonthsUtilityBillDataAvailableElectricity(personalDetails.getUtilityAccountInfo().getMonthsUtilityBillDataAvailableElectricity().toString());
                            utilityAccount.setMonthsUtilityBillDataAvailableTelecom(personalDetails.getUtilityAccountInfo().getMonthsUtilityBillDataAvailableTelecom().toString());

                            riskEventsDto.setBillOverdueBroadband(personalDetails.getUtilityAccountInfo().getRiskEvents().getBillOverdueBroadband().toString());
                            riskEventsDto.setBillOverdueElectricity(personalDetails.getUtilityAccountInfo().getRiskEvents().getBillOverdueElectricity().toString());
                            riskEventsDto.setBillOverdueLandline(personalDetails.getUtilityAccountInfo().getRiskEvents().getBillOverdueLandline().toString());
                            riskEventsDto.setBillOverduePostpaid(personalDetails.getUtilityAccountInfo().getRiskEvents().getBillOverduePostpaid().toString());
                            riskEventsDto.setConnectionSuspendedBroadband(personalDetails.getUtilityAccountInfo().getRiskEvents().getConnectionSuspendedBroadband().toString());
                            riskEventsDto.setConnectionSuspendedLandline(personalDetails.getUtilityAccountInfo().getRiskEvents().getConnectionSuspendedLandline().toString());
                            riskEventsDto.setConnectionSuspendedPostpaid(personalDetails.getUtilityAccountInfo().getRiskEvents().getConnectionSuspendedPostpaid().toString());
                            riskEventsDto.setCountOfPenaltiesInUtilityBills(personalDetails.getUtilityAccountInfo().getRiskEvents().getCountOfPenaltiesInUtilityBills().toString());

                            utilityAccount.setUtilityBillDataNonTelcoSufficiencyFlag(personalDetails.getUtilityAccountInfo().getUtilityBillDataNonTelcoSufficiencyFlag().toString());
                            utilityAccount.setUtilityBillDataTelcoSufficiencyFlag(personalDetails.getUtilityAccountInfo().getUtilityBillDataTelcoSufficiencyFlag().toString());
                            utilityAccount.setRiskEvents(riskEventsDto);

                            creditCardAccount.setUtilityAccount(utilityAccount);
                        }
                    }
                    if (personalDetails.getBankAccountInfo() != null) {
                        if (CollectionUtils.isNotEmpty(personalDetails.getBankAccountInfo().getAccounts())) {
                            accounts.setAmbMonth0(personalDetails.getBankAccountInfo().getAccounts().get(0).getAmbMonth0().toString());
                            accounts.setAmbMonth1(personalDetails.getBankAccountInfo().getAccounts().get(0).getAmbMonth1().toString());
                            accounts.setAmbMonth2(personalDetails.getBankAccountInfo().getAccounts().get(0).getAmbMonth2().toString());
                            accounts.setBankName(personalDetails.getBankAccountInfo().getAccounts().get(0).getBankName());
                            accounts.setCreditMonth0(personalDetails.getBankAccountInfo().getAccounts().get(0).getCreditMonth0().toString());
                            accounts.setCreditMonth1(personalDetails.getBankAccountInfo().getAccounts().get(0).getCreditMonth1().toString());
                            accounts.setCreditMonth2(personalDetails.getBankAccountInfo().getAccounts().get(0).getCreditMonth2().toString());
                            accounts.setDebitMonth0(personalDetails.getBankAccountInfo().getAccounts().get(0).getDebitMonth0().toString());
                            accounts.setDebitMonth1(personalDetails.getBankAccountInfo().getAccounts().get(0).getDebitMonth1().toString());
                            accounts.setDebitMonth2(personalDetails.getBankAccountInfo().getAccounts().get(0).getDebitMonth2().toString());
                            accounts.setLatestBalance(personalDetails.getBankAccountInfo().getAccounts().get(0).getLatestBalance().toString());
                            accounts.setSno("");
                            accounts.setVolatilityBalance(personalDetails.getBankAccountInfo().getAccounts().get(0).getVolatilityBalance().toString());
                            accounts.setVolatilityCredit(personalDetails.getBankAccountInfo().getAccounts().get(0).getVolatilityCredit().toString());
                            accounts.setVolatilityDebit(personalDetails.getBankAccountInfo().getAccounts().get(0).getVolatilityDebit().toString());
                            accounts.setAccount_cnt("");
                            bankAccount.setAccounts(accounts);
                        }
                        if (personalDetails.getBankAccountInfo().getAggregateFlow() != null) {
                            aggregateFlow.setTotalCreditMonth0(personalDetails.getBankAccountInfo().getAggregateFlow().getTotalCreditMonth0().toString());
                            aggregateFlow.setTotalCreditMonth1(personalDetails.getBankAccountInfo().getAggregateFlow().getTotalCreditMonth1().toString());
                            aggregateFlow.setTotalCreditMonth2(personalDetails.getBankAccountInfo().getAggregateFlow().getTotalCreditMonth2().toString());
                            aggregateFlow.setTotalDebitMonth0(personalDetails.getBankAccountInfo().getAggregateFlow().getTotalDebitMonth0().toString());
                            aggregateFlow.setTotalDebitMonth1(personalDetails.getBankAccountInfo().getAggregateFlow().getTotalDebitMonth1().toString());
                            aggregateFlow.setTotalDebitMonth2(personalDetails.getBankAccountInfo().getAggregateFlow().getTotalDebitMonth2().toString());


                            bankAccount.setAggregateFlow(aggregateFlow);

                        }

                        bankAccount.setBankDataSufficiencyFlag(personalDetails.getBankAccountInfo().getBankDataSufficiencyFlag().toString());
                        bankAccount.setCountAccounts(personalDetails.getBankAccountInfo().getCountAccounts().toString());
                        bankAccount.setCountActiveAccounts(personalDetails.getBankAccountInfo().getCountActiveAccounts().toString());
                        bankAccount.setMonthsBankDataAvailable(personalDetails.getBankAccountInfo().getMonthsBankDataAvailable().toString());
                        bankAccount.setPrimaryAccount(personalDetails.getBankAccountInfo().getPrimaryAccount());
                        bankAccount.setPrimaryAccountCashWithdwl(personalDetails.getBankAccountInfo().getPrimaryAccountCashWithdwl().toString());

                        if (personalDetails.getBankAccountInfo().getRiskEvents() != null) {

                            riskEventsLosDto.setCountChequeBounceInward(personalDetails.getBankAccountInfo().getRiskEvents().getCountChequeBounceInward().toString());
                            riskEventsLosDto.setCountChequeBounceOutward(personalDetails.getBankAccountInfo().getRiskEvents().getCountChequeBounceOutward().toString());
                            riskEventsLosDto.setCountECSBounce(personalDetails.getBankAccountInfo().getRiskEvents().getCountECSBounce().toString());
                            riskEventsLosDto.setCountInsufficientBal(personalDetails.getBankAccountInfo().getRiskEvents().getCountInsufficientBal().toString());

                            bankAccount.setRiskEvents(riskEventsLosDto);

                        }

                    }

                    if (personalDetails.getMetadata() != null) {
                        if (personalDetails.getMetadata().getDataThickness() != null) {
                            dataThickness.setAccountCount(personalDetails.getMetadata().getDataThickness().getAccountCount().toString());
                            dataThickness.setAppCount(personalDetails.getMetadata().getDataThickness().getAppCount().toString());
                            dataThickness.setCallCount(personalDetails.getMetadata().getDataThickness().getCallCount().toString());
                            dataThickness.setContactCount(personalDetails.getMetadata().getDataThickness().getContactCount().toString());
                            dataThickness.setImageCount(personalDetails.getMetadata().getDataThickness().getImageCount().toString());
                            dataThickness.setLocationCount(personalDetails.getMetadata().getDataThickness().getLocationCount().toString());
                            dataThickness.setSmsCount(personalDetails.getMetadata().getDataThickness().getSmsCount().toString());

                            metadata.setDataThickness(dataThickness);
                        }

                        metadata.setInstallTime(personalDetails.getMetadata().getInstallTime());
                        metadata.setLastPingTime(personalDetails.getMetadata().getLastPingTime());
                        metadata.setRequestTime(personalDetails.getMetadata().getRequestTime());
                        metadata.setSourceApp(personalDetails.getMetadata().getSourceApp());
                        metadata.setUniqueId(personalDetails.getMetadata().getUniqueId());

                        permissionGiven.setAccount(personalDetails.getMetadata().getPermissionGiven().getAccount().toString());
                        permissionGiven.setCalendar(personalDetails.getMetadata().getPermissionGiven().getCalendar().toString());
                        permissionGiven.setLocation(personalDetails.getMetadata().getPermissionGiven().getLocation().toString());
                        permissionGiven.setPhone(personalDetails.getMetadata().getPermissionGiven().getPhone().toString());
                        permissionGiven.setSms(personalDetails.getMetadata().getPermissionGiven().getSms().toString());
                        permissionGiven.setStorage(personalDetails.getMetadata().getPermissionGiven().getStorage().toString());


                        metadata.setPermissionGiven(permissionGiven);

                    }


                }
                if (gonogoCustomerApplication.getApplicantComponentResponse().getCreditVidyaResponse() != null) {
                    creditVidya.setErrorMessage(gonogoCustomerApplication.getApplicantComponentResponse().getCreditVidyaResponse().getErrorMessage());
                }

                creditVidya.setBankAccount(bankAccount);
                creditVidya.setCreditCardAccount(creditCardAccount);
                creditVidya.setMetadata(metadata);
            }


            if (CollectionUtils.isNotEmpty(gonogoCustomerApplication.getCroDecisions())) {

                cro.setDecisionupdatedate(gonogoCustomerApplication.getCroDecisions().get(0).getDecisionUpdateDate().toString());
                double Dpay = gonogoCustomerApplication.getCroDecisions().get(0).getDownPayment();
                String DownPayment = Double.toString(Dpay);
                cro.setDpay(DownPayment);
                double Emi = gonogoCustomerApplication.getCroDecisions().get(0).getEmi();
                String emi = Double.toString(Emi);
                cro.setEmi(emi);
                double eligibleAmm = gonogoCustomerApplication.getCroDecisions().get(0).getEligibleAmt();
                String EligibleAmmount = Double.toString(eligibleAmm);
                cro.setEligibleamt(EligibleAmmount);
                double Ittrt = gonogoCustomerApplication.getCroDecisions().get(0).getInterestRate();
                String InterestRate = Double.toString(Ittrt);
                cro.setItrrt(InterestRate);
                double Ltv = gonogoCustomerApplication.getCroDecisions().get(0).getLtv();
                String LTV = Double.toString(Ltv);
                cro.setLtv(LTV);
                double Mtappr = gonogoCustomerApplication.getCroDecisions().get(0).getAmtApproved();
                String AmmApproved = Double.toString(Mtappr);
                cro.setMtappr(AmmApproved);
                double Tenor = gonogoCustomerApplication.getCroDecisions().get(0).getTenor();
                String tenor = Double.toString(Tenor);
                cro.setTenor(tenor);
                double UnutilizeAmm = gonogoCustomerApplication.getCroDecisions().get(0).getUnUtilizedAmt();
                String UnUtilizedAmm = Double.toString(UnutilizeAmm);
                cro.setUnutilizedamt(UnUtilizedAmm);
                double Utilizedamm = gonogoCustomerApplication.getCroDecisions().get(0).getUtilizedAmt();
                String UtilizedAmm = Double.toString(Utilizedamm);
                cro.setUtilizedamt(UtilizedAmm);
            }

            if (gonogoCustomerApplication.getIntrimStatus() != null && gonogoCustomerApplication.getIntrimStatus().getPosidexModuleResult() != null) {
                posidex.setAddrstblty(String.valueOf(gonogoCustomerApplication.getIntrimStatus().getPosidexModuleResult().getAddStability()));
                posidex.setDedupeuserid("");
                posidex.setFldval(gonogoCustomerApplication.getIntrimStatus().getPosidexModuleResult().getFieldValue());
                posidex.setMsg(gonogoCustomerApplication.getIntrimStatus().getPosidexModuleResult().getMessage());
                posidex.setOrdernumber(String.valueOf(gonogoCustomerApplication.getIntrimStatus().getPosidexModuleResult().getOrder()));
                posidex.setPosidexdedupesta("");
                posidex.setPosidexid("");
                posidex.setTenorpaid("");
                posidex.setTenoravailable("");
            }


            if (gonogoCustomerApplication.getTvrDetails() != null) {

                welcomeScreen.setTvrdecision(gonogoCustomerApplication.getTvrDetails().getStatus());
                welcomeScreen.setTvrremarks(gonogoCustomerApplication.getTvrDetails().getRemarks());
                welcomeScreen.setTvrstatus(gonogoCustomerApplication.getTvrDetails().getStatus());
                welcomeScreen.setWelcomecalluserid("");//
            }


            karzaElectricityBill.setAddress("");
            karzaElectricityBill.setAmountpayable("");
            karzaElectricityBill.setBillamount("");
            karzaElectricityBill.setBillduedate("");
            karzaElectricityBill.setBillissuedate("");
            karzaElectricityBill.setBillno("");
            karzaElectricityBill.setConsumerid("");
            karzaElectricityBill.setConsumername("");
            karzaElectricityBill.setEmail("");
            karzaElectricityBill.setIscallmode("");
            karzaElectricityBill.setMobilenumber("");
            karzaElectricityBill.setServiceprovider("");
            karzaElectricityBill.setStatus("");
            karzaElectricityBill.setTotalamount("");


            karzaVoterID.setAcname("");
            karzaVoterID.setAcno("");
            karzaVoterID.setAge("");
            karzaVoterID.setApplicationid("");
            karzaVoterID.setApplicationsource("");
            karzaVoterID.setCrold("");
            karzaVoterID.setDealerid("");
            karzaVoterID.setDistrict("");
            karzaVoterID.setDob("");
            karzaVoterID.setDsald("");
            karzaVoterID.setEpicno("");
            karzaVoterID.setGender("");
            karzaVoterID.setHouseno("");
            karzaVoterID.setId("");
            karzaVoterID.setInstitutionid("");
            karzaVoterID.setIscallmade("");
            karzaVoterID.setLast_update("");
            karzaVoterID.setName("");
            karzaVoterID.setPartname("");
            karzaVoterID.setPcname("");
            karzaVoterID.setProduct("");
            karzaVoterID.setPs_lat("");
            karzaVoterID.setPs_lng("");
            karzaVoterID.setPsname("");
            karzaVoterID.setRequesttype("");
            karzaVoterID.setRlnname("");
            karzaVoterID.setRlntype("");
            karzaVoterID.setS_action("");
            karzaVoterID.setS_kycid("");
            karzaVoterID.setSectionno("");
            karzaVoterID.setSlnoinpart("");
            karzaVoterID.setSourceid("");
            karzaVoterID.setStatus("");
            karzaVoterID.setState("");
            karzaVoterID.setStcode("");
            karzaVoterID.setVoteridno("");


            if (gonogoCustomerApplication.getIntrimStatus() != null && gonogoCustomerApplication.getIntrimStatus().getScoringModuleResult() != null) {
                scoring.setApplicationscore(gonogoCustomerApplication.getIntrimStatus().getScoringModuleResult().getFieldValue());
                scoring.setCibilscore(gonogoCustomerApplication.getIntrimStatus().getScoringModuleResult().getFieldValue());
                scoring.setCrifscore(gonogoCustomerApplication.getIntrimStatus().getScoringModuleResult().getFieldValue());
                scoring.setOfficeaddrscore(gonogoCustomerApplication.getIntrimStatus().getScoringModuleResult().getFieldValue());
                scoring.setResidenceaddrscore(gonogoCustomerApplication.getIntrimStatus().getScoringModuleResult().getFieldValue());
            }

            if (gonogoCustomerApplication.getApplicantComponentResponse() != null && CollectionUtils.isNotEmpty(gonogoCustomerApplication.getApplicantComponentResponse().getAddressScoringResult())) {
                List<ScoreList> scorelistsList = new ArrayList<ScoreList>();
                scoreList.setAdd1(gonogoCustomerApplication.getApplicantComponentResponse().getAddressScoringResult().get(0).getAdd1());
                scoreList.setAdd2(gonogoCustomerApplication.getApplicantComponentResponse().getAddressScoringResult().get(0).getAdd2());
                scoreList.setAddtype(gonogoCustomerApplication.getApplicantComponentResponse().getAddressScoringResult().get(0).getAddType());
                scoreList.setCibiladdsubdatetodatedays(String.valueOf(gonogoCustomerApplication.getApplicantComponentResponse().getAddressScoringResult().get(0).getCibilAddSubdateToDateDays()));
                scoreList.setGngrefno(gonogoCustomerApplication.getApplicantComponentResponse().getAddressScoringResult().get(0).getGngRefNo());
                scoreList.setScore(String.valueOf(gonogoCustomerApplication.getApplicantComponentResponse().getAddressScoringResult().get(0).getScore()));
                scorelistsList.add(scoreList);
                com.softcell.gonogo.model.los.tvs.ScoreList[] scoreListsArray = scorelistsList.toArray(new com.softcell.gonogo.model.los.tvs.ScoreList[scorelistsList.size()]);

                scoring.setScoreList(scoreListsArray);
                insertOrUpdateTvsRecordForGroup002.setScoring(scoring);

            }

            com.softcell.gonogo.model.surrogate.Surrogate surrogateRef = gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate();
            if (surrogateRef != null) {
                if (CollectionUtils.isNotEmpty(surrogateRef.getTraderSurrogate())) {
                    business.setBusinessproof(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getTraderSurrogate().get(0).getBussinesProof());
                    business.setYearinbusiness(String.valueOf(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getTraderSurrogate().get(0).getYearInBussines()));
                }

                if (CollectionUtils.isNotEmpty(surrogateRef.getCreditCardSurrogate())) {
                    creditCard.setNumber(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getCreditCardSurrogate().get(0).getCardNumber());
                    creditCard.setCategory(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getCreditCardSurrogate().get(0).getCardCategory());
                }

                if (CollectionUtils.isNotEmpty(surrogateRef.getDebitCardSurrogate())) {
                    debitCard.setNumber(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getDebitCardSurrogate().get(0).getCardNumber());
                }

                if (CollectionUtils.isNotEmpty(surrogateRef.getCarSurrogate())) {
                    ownedCar.setCarage("");
                    ownedCar.setCategory(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getCarSurrogate().get(0).getCategory());
                    ownedCar.setManufactureyear(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getCarSurrogate().get(0).getManufactureYear().toString());
                    ownedCar.setModelname(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getCarSurrogate().get(0).getModelName());
                    ownedCar.setRegno(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getCarSurrogate().get(0).getRegistrationNumber());
                }
                if (CollectionUtils.isNotEmpty(surrogateRef.getHouseSurrogate())) {
                    ownedHouse.setDocumenttype(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getHouseSurrogate().get(0).getDocumentType());
                    ownedHouse.setHometype(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getHouseSurrogate().get(0).getHouseType());
                }
                if (CollectionUtils.isNotEmpty(surrogateRef.getSalariedSurrogate())) {
                    salaryHome.setNethometake(String.valueOf(gonogoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getSurrogate().getSalariedSurrogate().get(0).getNetTakeHome()));
                }
                surrogate.setBusiness(business);
                surrogate.setCreditCard(creditCard);
                surrogate.setDebitCard(debitCard);
                surrogate.setOwnedCar(ownedCar);
                surrogate.setOwnedHouse(ownedHouse);
                surrogate.setSalaryHome(salaryHome);
            }


            karzaLPG.setAadhaarnumber("");
            karzaLPG.setAddressline1("");
            karzaLPG.setAddressline2("");
            karzaLPG.setBankaccountno("");
            karzaLPG.setBankname("");
            karzaLPG.setCity("");
            karzaLPG.setConsumeraddress("");
            karzaLPG.setConsumercontact("");
            karzaLPG.setConsumeremail("");
            karzaLPG.setConsumername("");
            karzaLPG.setConsumerno("");
            karzaLPG.setDistributoraddress("");
            karzaLPG.setDistributorcode("");
            karzaLPG.setDistributorname("");
            karzaLPG.setGivenupsubsidy("");
            karzaLPG.setIfsccode("");
            karzaLPG.setIscallmode("");
            karzaLPG.setLpg_id("");
            karzaLPG.setLpgstatus("");
            karzaLPG.setPincode("");
            karzaLPG.setRefillconsumed("");
            karzaLPG.setStatus("");
            karzaLPG.setSubsidyavailed("");
            karzaLPG.setTotalrefillconsumed("");
            karzaLPG.setLng("");
            karzaLPG.setLat("");

            if (gonogoCustomerApplication.getApplicantComponentResponse() != null && gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse() != null
                    && gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse().getCasedetails() != null) {
                saathi.setBranch(gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse().getCasedetails().getBranch());
                saathi.setCustomername(gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse().getCasedetails().getCustomerName());
                saathi.setDealername(gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse().getCasedetails().getDealerName());
                saathi.setMobileno(gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse().getCasedetails().getMobileNo());
                saathi.setStatus(gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse().getCasedetails().getStatus());
                saathi.setUpdateddate(gonogoCustomerApplication.getApplicantComponentResponse().getSaathiResponse().getCasedetails().getUpdatedDate());
                saathi.setCalldate("");
            }


            karzaDrivingLicense.setAddress("");
            karzaDrivingLicense.setCovdetails("");
            karzaDrivingLicense.setDateofbirth("");
            karzaDrivingLicense.setDl_number("");
            karzaDrivingLicense.setFather_husband_name("");
            karzaDrivingLicense.setIscallmode("");
            karzaDrivingLicense.setIssuedate("");
            karzaDrivingLicense.setName("");
            karzaDrivingLicense.setS_addrtype("");
            karzaDrivingLicense.setS_kycnumber("");
            karzaDrivingLicense.setStatus("");
            karzaDrivingLicense.setValidity("");

            List<com.softcell.gonogo.model.los.tvs.LosDto.ScoreList> scorelist = new ArrayList<com.softcell.gonogo.model.los.tvs.LosDto.ScoreList>();
            com.softcell.gonogo.model.los.tvs.LosDto.ScoreList scoreListCIBIL = new com.softcell.gonogo.model.los.tvs.LosDto.ScoreList();

            scoreListCIBIL.setReasonCode1("");
            scoreListCIBIL.setReasonCode2("");
            scoreListCIBIL.setScore("");
            scoreListCIBIL.setScoreCardName("");
            scoreListCIBIL.setScoreCardVersion("");
            scoreListCIBIL.setScoreDate("");
            scoreListCIBIL.setScoreName("");
            scoreListCIBIL.setSno("");

            scorelist.add(scoreListCIBIL);
            com.softcell.gonogo.model.los.tvs.LosDto.ScoreList[] scoreListArr = scorelist.toArray(new com.softcell.gonogo.model.los.tvs.LosDto.ScoreList[scorelist.size()]);
            multiBureauCIBIL.setScoreList(scoreListArr);

            addressList.setAddressCategory("");
            addressList.setAddressLine1("");
            addressList.setAddressLine2("");
            addressList.setDateReported("");
            addressList.setEnrichedThroughtEnquiry("");
            addressList.setPinCode("");
            addressList.setSno("");
            addressList.setStateCode("");
            List<AddressList> AddresslistsList = new ArrayList<AddressList>();
            AddresslistsList.add(addressList);
            AddressList[] addressListArr = AddresslistsList.toArray(new AddressList[AddresslistsList.size()]);
            multiBureauCIBIL.setAddressList(addressListArr);


            enquiryList.setDateReported("");
            enquiryList.setEnquiryAmount("");
            enquiryList.setEnquiryPurpose("");
            enquiryList.setReportingMemberShortName("");
            enquiryList.setSno("");
            List<EnquiryList> enquirylistsList = new ArrayList<EnquiryList>();
            enquirylistsList.add(enquiryList);
            EnquiryList[] enquiryListArr = enquirylistsList.toArray(new EnquiryList[enquirylistsList.size()]);
            multiBureauCIBIL.setEnquiryList(enquiryListArr);


            accountList.setAccountType("");
            accountList.setCurrentBalance("");
            accountList.setDateClosed("");
            accountList.setDateOfLastPayment("");
            accountList.setDateOpenedOrDisbursed("");
            accountList.setDateReportedAndCertified("");
            accountList.setHighCreditOrSanctionedAmount("");
            accountList.setOwnershipIndicator("");
            accountList.setPaymentHistory1("");
            accountList.setPaymentHistory2("");
            accountList.setPaymentHistoryEndDate("");
            accountList.setPaymentHistoryStartDate("");
            accountList.setReportingMemberShortName("");
            accountList.setSno("");
            List<AccountList> AccountlistsList = new ArrayList<AccountList>();
            AccountlistsList.add(accountList);
            AccountList[] accountListArr = AccountlistsList.toArray(new AccountList[AccountlistsList.size()]);
            multiBureauCIBIL.setAccountList(accountListArr);


            phoneList.setEnrichEnquiryForPhone("");
            phoneList.setSno("");
            phoneList.setTelephoneNumber("");
            phoneList.setTelephoneType("");
            List<PhoneList> phonelistsList = new ArrayList<PhoneList>();
            phonelistsList.add(phoneList);
            PhoneList[] phoneListArr = phonelistsList.toArray(new PhoneList[phonelistsList.size()]);
            multiBureauCIBIL.setPhoneList(phoneListArr);

            name.setDob("");
            name.setGender("");
            name.setName1("");
            name.setName2("");
            name.setName3("");
            name.setName4("");
            name.setName5("");
            multiBureauCIBIL.setName(name);


            secondaryMatches.setSno("");
            List<SecondaryMatches> secondaryMatchesList = new ArrayList<SecondaryMatches>();
            secondaryMatchesList.add(secondaryMatches);
            SecondaryMatches[] secondaryMatchesArr = secondaryMatchesList.toArray(new SecondaryMatches[secondaryMatchesList.size()]);
            multiBureauCIBIL.setSecondaryMatches(secondaryMatchesArr);

            secIdList.setEnrichedThroughtEnquiry("");
            secIdList.setIdType("");
            secIdList.setIdValue("");
            secIdList.setSno("");
            List<SecIdList> secidlistsList = new ArrayList<SecIdList>();
            secidlistsList.add(secIdList);
            SecIdList[] secIdListArr = secidlistsList.toArray(new SecIdList[secidlistsList.size()]);
            secondaryMatches.setSecIdList(secIdListArr);

            secName.setDob("");
            secName.setGender("");
            secName.setName1("");
            secondaryMatches.setSecName(secName);

            secAddressList.setAddressCategory("");
            secAddressList.setAddressLine1("");
            secAddressList.setAddressLine2("");
            secAddressList.setAddressLine3("");
            secAddressList.setDateReported("");
            secAddressList.setEnrichedThroughtEnquiry("");
            secAddressList.setPinCode("");
            secAddressList.setSno("");
            secAddressList.setStateCode("");
            List<SecAddressList> secAddresslistsList = new ArrayList<SecAddressList>();
            secAddresslistsList.add(secAddressList);
            SecAddressList[] secAddressListArr = secAddresslistsList.toArray(new SecAddressList[secAddresslistsList.size()]);
            secondaryMatches.setSecAddressList(secAddressListArr);//list


            secPhoneList.setEnrichEnquiryForPhone("");
            secPhoneList.setSno("");
            secPhoneList.setTelephoneNumber("");
            secPhoneList.setTelephoneType("");
            List<SecPhoneList> secphonelistsList = new ArrayList<SecPhoneList>();
            secphonelistsList.add(secPhoneList);
            SecPhoneList[] secPhoneListArr = secphonelistsList.toArray(new SecPhoneList[secphonelistsList.size()]);
            secondaryMatches.setSecPhoneList(secPhoneListArr);


            idList.setIdType("");
            idList.setIdValue("");
            idList.setIssueDate("");
            idList.setSno("");
            List<IdList> idlistsList = new ArrayList<IdList>();
            idlistsList.add(idList);
            IdList[] idListArr = idlistsList.toArray(new IdList[idlistsList.size()]);
            multiBureauCIBIL.setIdList(idListArr);


            employmentList.setAccountType("");
            employmentList.setDateReported("");
            employmentList.setOccupationCode("");
            employmentList.setSno("");
            List<EmploymentList> employmentlistsList = new ArrayList<EmploymentList>();
            employmentlistsList.add(employmentList);
            EmploymentList[] employmentListArr = employmentlistsList.toArray(new EmploymentList[employmentlistsList.size()]);
            multiBureauCIBIL.setEmploymentList(employmentListArr);


            header.setBatchId("");
            header.setDateOfIssue("");
            header.setDateOfRequest("");
            header.setDateProceed("");
            header.setEnquiryControlNumber("");
            header.setEnquiryMemberUserID("");
            header.setMemberReferenceNumber("");
            header.setPreparedFor("");
            header.setPreparedForId("");
            header.setReportId("");
            header.setSubjectReturnCode("");
            header.setTimeProceed("");
            header.setVersion("");
            multiBureauCIBIL.setHeader(header);


            INDVReport.setAccountSummary(accountSummary);
            INDVReport.setIndvResponses(indvResponses);
            INDVReport.setPersonalInfoVariation(personalInfoVariation);
            INDVReport.setRequest(request);
            INDVReport.setStatusDetails(statusDetails);
            INDVReports.setINDVReport(INDVReport);
            multibureauCRIF.setINDVReports(INDVReports);


            summary.setNoOfActiveAccounts("");
            summary.setNoOfClosedAccounts("");
            summary.setNoOfDefaultAccounts("");
            summary.setNoOfOtherMfis("");
            summary.setNoOfOwnMfi("");
            summary.setStatus("");
            summary.setTotalResponses("");
            indvResponses.setSummary(summary);

            secSummary.setNoOfActiveAccounts("");
            secSummary.setNoOfClosedAccounts("");
            secSummary.setNoOfDefaultAccounts("");
            secSummary.setNoOfOtherMfis("");
            secSummary.setNoOfOwnMfi("");
            secSummary.setTotalResponses("");
            indvResponses.setSecSummary(secSummary);

            primarySummary.setNoOfActiveAccounts("");
            primarySummary.setNoOfClosedAccounts("");
            primarySummary.setNoOfDefaultAccounts("");
            primarySummary.setNoOfOtherMfis("");
            primarySummary.setNoOfOwnMfi("");
            primarySummary.setTotalResponses("");
            indvResponses.setPrimarySummary(primarySummary);


            alert.setAlertDescription("");
            alerts.setAlert(alert);
            INDVReport.setAlerts(alerts);

            request.setAgeAsOn("");
            request.setAka("");
            request.setBranch("");
            request.setCnsind("");
            request.setCnsscore("");
            request.setCreditInquiryPurposeType("");
            request.setCreditInquiryPurposeTypeDescription("");
            request.setCreditInquiryStage("");
            request.setCreditReportTransectionDatetime("");
            request.setCreditRequestType("");
            request.setDob("");
            request.setEmail1("");
            request.setGender("");
            request.setLoanAmount("");
            request.setMemberId("");
            request.setLosApplicationId("");
            request.setMfigroup("");
            request.setMfiInd("");
            request.setName("");
            request.setMfiscore("");


            id.setSno("");
            id.setType("");
            id.setValue("");
            List<Id> idLists = new ArrayList<Id>();
            idLists.add(id);
            Id[] idArr = idLists.toArray(new Id[idLists.size()]);
            ids.setId(idArr);
            request.setIds(ids);


            address.setAddress("");
            address.setSno("");
            List<Address> addressLists = new ArrayList<Address>();
            addressLists.add(address);
            Address[] addressArr = addressLists.toArray(new Address[addressLists.size()]);
            addresses.setAddress(addressArr);
            request.setAddresses(addresses);


            phone.setPhone("");
            phone.setSno("");
            List<Phone> phoneLists = new ArrayList<Phone>();
            phoneLists.add(phone);
            com.softcell.gonogo.model.los.tvs.Phone[] phoneArr = phoneLists.toArray(new com.softcell.gonogo.model.los.tvs.Phone[phoneLists.size()]);
            phones.setPhone(phoneArr);
            request.setPhones(phones);

            printableReport.setContent("");
            printableReport.setFileName("");
            printableReport.setType("");
            INDVReport.setPrintableReport(printableReport);

            header.setBatchId("");
            header.setDateOfIssue("");
            header.setDateOfRequest("");
            header.setDateProceed("");
            header.setEnquiryControlNumber("");
            header.setEnquiryMemberUserID("");
            header.setMemberReferenceNumber("");
            header.setPreparedFor("");
            header.setPreparedForId("");
            header.setReportId("");
            header.setSubjectReturnCode("");
            header.setTimeProceed("");
            header.setVersion("");
            INDVReport.setHeader(header);


            drivedAttributes.setAverageAccountAgeMonth("");
            drivedAttributes.setAverageAccountAgeYear("");
            drivedAttributes.setInquiriesInLastSixMonth("");
            drivedAttributes.setLengthOfCreditHistoryMonth("");
            drivedAttributes.setLengthOfCreditHistoryYear("");
            drivedAttributes.setNewAccountInLastSixMonths("");
            drivedAttributes.setNewDlinqAccountInLastSixMonths("");
            accountSummary.setDrivedAttributes(drivedAttributes);

            primaryAccountsSummary.setPrimaryActiveNumberOfAccounts("");
            primaryAccountsSummary.setPrimaryCurrentBalance("");
            primaryAccountsSummary.setPrimaryDisbursedAmount("");
            primaryAccountsSummary.setPrimaryNumberOfAccounts("");
            primaryAccountsSummary.setPrimaryOverdueNumberOfAccounts("");
            primaryAccountsSummary.setPrimarySanctionedAmount("");
            primaryAccountsSummary.setPrimarySecuredNumberOfAccounts("");
            primaryAccountsSummary.setPrimaryUnsecuredNumberOfAccounts("");
            primaryAccountsSummary.setPrimaryUntaggedNumberOfAccounts("");
            accountSummary.setPrimaryAccountsSummary(primaryAccountsSummary);

            secondaryAccountSummary.setSecondaryActiveNumberOfAccounts("");
            secondaryAccountSummary.setSecondaryCurrentBalance("");
            secondaryAccountSummary.setSecondaryDisbursedAmount("");
            secondaryAccountSummary.setSecondaryNumberOfAccounts("");
            secondaryAccountSummary.setSecondaryOverdueNumberOfAccounts("");
            secondaryAccountSummary.setSecondarySanctionedAmount("");
            secondaryAccountSummary.setSecondarySecuredNumberOfAccounts("");
            secondaryAccountSummary.setSecondaryUnsecuredNumberOfAccounts("");
            secondaryAccountSummary.setSecondaryUntaggedNumberOfAccounts("");
            accountSummary.setSecondaryAccountSummary(secondaryAccountSummary);


            CDLos.setAction(LosTvsActionEnums.I.name());
            if (StringUtils.isNotBlank(gonogoCustomerApplication.getGngRefId())) {
                CDLos.setProspectid(gonogoCustomerApplication.getGngRefId());// M
            }
            if (gonogoCustomerApplication.getApplicationRequest() != null) {
                if (StringUtils.isNotBlank(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                    CDLos.setStage(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId());// M
                }
            }
            CDLos.setRemarks("");
            CDLos.setVendorid("");

            return InsertOrUpdateTvsRecordGroupTwo.builder()
                    .CDLos(CDLos)
                    .application(application)
                    .creditVidya(creditVidya)
                    .cro(cro)
                    .doStages(doStages)
                    .karzaDrivingLicense(karzaDrivingLicense)
                    .karzaElectricityBill(karzaElectricityBill)
                    .karzaLPG(karzaLPG)
                    .karzaVoterID(karzaVoterID)
                    .posidex(posidex)
                    .surrogate(surrogate)
                    .scoring(scoring)
                    .saathi(saathi)
                    .welcomeScreen(welcomeScreen)
                    .multibureauCIBIL(multiBureauCIBIL)
                    .multibureauCRIF(multibureauCRIF)
                    .build();
        }catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }


}
