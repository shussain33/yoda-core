package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.model.request.smsservice.SmsRequest;
import com.softcell.gonogo.service.factory.MessagingServicesBuilder;
import org.springframework.stereotype.Service;

/**
 * Created by yogeshb on 24/2/17.
 */
@Service
public class MessagingServicesBuilderImpl implements MessagingServicesBuilder {

    @Override
    public SmsRequest buildSmsRequest(String mobileNumber, String otp) {
        return SmsRequest.builder()
                .mobileNumber(mobileNumber)
                .message(String.format("Dear Customer, OTP %s to Submit your Web Inquiry.", otp))
                .build();
    }
}
