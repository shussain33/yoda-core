package com.softcell.gonogo.service.factory.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.*;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.Header;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.multibureau.pickup.WarningAndError;
import com.softcell.gonogo.service.factory.LosMBRequestBuilder;
import com.softcell.soap.mb.MultiBureauResponseServiceStub;
import com.softcell.soap.mb.hdfcbank.xsd.multibureau.*;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


public class LosMBRequestBuilderImpl implements LosMBRequestBuilder {

    private static Logger logger = LoggerFactory.getLogger(LosMBRequestBuilder.class);

    public static void main(String[] args) throws Exception {
        ApplicationMongoRepository mongoObj = new ApplicationMongoRepository();
        GoNoGoCustomerApplication goNoGoCustomerApp = mongoObj.getGoNoGoCustomerApplicationByRefId("25213000934");
   //     GoNoGoCustomerApplication goNoGoCustomerApp = new ObjectMapper().readValue(new File("/tmp/MBDAtaPushJson.json"), GoNoGoCustomerApplication.class);
        List<LosMbPostRequest> buildLosMbRequest = new LosMBRequestBuilderImpl().buildLosMbRequest(goNoGoCustomerApp,null);
        for (LosMbPostRequest losMbPostRequest : buildLosMbRequest) {
            byte[] requestData = losMbPostRequest.getRequestData();
            ByteArrayInputStream bais = new ByteArrayInputStream(requestData);
            GZIPInputStream gzipIn = new GZIPInputStream(bais);
            ObjectInputStream objectIn = new ObjectInputStream(gzipIn);
            Object myObj1 = (Object) objectIn.readObject();
            String request = myObj1.toString();
            boolean checkMbResponseType = checkMbResponseType(request);

            MultiBureauResponseServiceStub stub = new MultiBureauResponseServiceStub();
            Gson gson = new Gson();
            if (checkMbResponseType) {
                try {
                    MBIssueResponseType mbIssueResponseType = gson.fromJson(request, MBIssueResponseType.class);
                    request = null;
                    if (mbIssueResponseType != null) {
                        MBIssueResponseType fromJson = new GsonBuilder().create().fromJson(request, MBIssueResponseType.class);
                        MultiBureauResponse multiBureauResponse4 = new MultiBureauResponse();
                        multiBureauResponse4.setMultiBureauResponse(fromJson);
                        MultiBureauResponseAck postMBResponse = stub.postMBResponse(multiBureauResponse4);
                        System.out.println(postMBResponse.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (!checkMbResponseType) {
                MBEoTRequestType mbEoTRequestType = gson.fromJson(request, MBEoTRequestType.class);
                if (mbEoTRequestType != null) {
                    try {
                        MultiBureauEoTRequest multiBureauEoTRequest6 = new MultiBureauEoTRequest();
                        multiBureauEoTRequest6.setMultiBureauEoTRequest(mbEoTRequestType);
                        MultiBureauEoTAck postEndOfTransaction = stub.postEndOfTransaction(multiBureauEoTRequest6);
                        System.out.println("postEndOfTransaction :" + postEndOfTransaction);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


        }

    }

    private static Boolean checkMbResponseType(String request) {
        try {
            MBIssueResponseType fromJson = new GsonBuilder().create().fromJson(request, MBIssueResponseType.class);
            if (fromJson != null && fromJson.getRESPONSE() != null) {
                return true;
            }
//			MBIssueResponseType mbIssueResponseType = new Gson().fromJson(request, MBIssueResponseType.class);
//			return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            MBEoTRequestType fromJson = new GsonBuilder().create().fromJson(request, MBEoTRequestType.class);
            if (fromJson != null && fromJson.getHEADER() != null) {
                return false;
            }
//			MBEoTRequestType mbEoTRequestType = new Gson().fromJson(request, MBEoTRequestType.class);


        } catch (Exception e) {
        }


        return null;
    }

    @Override
    public List<LosMbPostRequest> buildLosMbRequest(GoNoGoCustomerApplication goNoGoCustomerApplication,LosMbData losMbData) throws JAXBException {
        if (goNoGoCustomerApplication != null
                && goNoGoCustomerApplication.getApplicantComponentResponse() != null
                && goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose() != null
                && StringUtils.equals("COMPLETED",goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getStatus())) {
            ResponseMultiJsonDomain multiJson = goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose();
            try {
                String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
                List<LosMbPostRequest> losMbRequests = new ArrayList<LosMbPostRequest>();
                MBIssueResponseType mbIssueResponse = (MBIssueResponseType) populateSoapResponseObject(multiJson,goNoGoCustomerApplication,losMbData);
                losMbRequests.add(prepareLosFeedRequest(mbIssueResponse, institutionId));

                MBEoTRequestType mbEotResponse = (MBEoTRequestType) populateEndOfTxnObject(multiJson,goNoGoCustomerApplication);
                losMbRequests.add(prepareLosFeedRequest(mbEotResponse, institutionId));
                return losMbRequests;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return null;
    }

    private LosMbPostRequest prepareLosFeedRequest(Object object, String institutionId) throws IOException {

        Gson gson = new Gson();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream gzipOut = new GZIPOutputStream(baos);
        ObjectOutputStream objectOut = new ObjectOutputStream(gzipOut);
        String json = null;
        if (object instanceof MBEoTRequestType) {
            MBEoTRequestType eotObject = (MBEoTRequestType) object;
            json = gson.toJson(eotObject);
            logger.info("LOS MB EOT request :-" + json);
        } else if (object instanceof MBIssueResponseType) {
            MBIssueResponseType mbIssueObject = (MBIssueResponseType) object;
            json = gson.toJson(mbIssueObject);
            logger.info("LOS MB issue response request :-" + json);
        }

        objectOut.writeObject(json);
        objectOut.close();
        byte[] byteArray = baos.toByteArray();

        LosMbPostRequest losMbPostRequest = new LosMbPostRequest();
        losMbPostRequest.setInstitutionId(institutionId);
        losMbPostRequest.setRequestData(byteArray);
        return losMbPostRequest;
    }

    private Object populateEndOfTxnObject(ResponseMultiJsonDomain multiJson,GoNoGoCustomerApplication gngCustApp) throws Exception {
        EndOfTxn endOfTxnObj = new EndOfTxn();
        Object mbResponse = null;

        ResponseHeader headerObj = new ResponseHeader();
        Header header = multiJson.getHeader();
        if (header != null) {
            headerObj.setApplicationId(header.getApplicationID());
            headerObj.setCustId(header.getConsumerID());
            headerObj.setReqRcvdTime(GngDateUtil.getFormattedDate(new Date(), "ddMMyyyy HH:mm:ss"));
            headerObj.setResponseType(header.getResponseType());
            headerObj.setSourceSystem("GNG");
        }
        endOfTxnObj.setHeaderObj(headerObj);
        endOfTxnObj.setStatus(multiJson.getStatus());
        endOfTxnObj.setAckId(GngUtils.getString(multiJson.getAcknowledgmentId()));
        endOfTxnObj.setSentToCibil("Y");
        endOfTxnObj.setSentToChm("N");
        endOfTxnObj.setSentToEquifax("N");
        endOfTxnObj.setSentToExperian("N");
        endOfTxnObj.setFiller1("N");
        endOfTxnObj.setFiller2("N");
        PostSoaResponseToService postHdfcResponseToService = new PostSoaResponseToService();
        mbResponse = postHdfcResponseToService.convert(endOfTxnObj,null);
        return mbResponse;
    }

    private Object populateSoapResponseObject(ResponseMultiJsonDomain multiJson,GoNoGoCustomerApplication gngCustApp,LosMbData losMbData) throws Exception {

        IssueResponseMB issueResponseMB = new IssueResponseMB();
        Object mbResponse = null;
        Header header = multiJson.getHeader();
        List<Finished> finishedList = multiJson.getFinishedList();
        List<Finished> rejectList = multiJson.getReject();

        ArrayList<FinishedDomain> finishedObjectList = null;
        ArrayList<RejectDomain> rejectObjectList = null;

        if (finishedList != null && !finishedList.isEmpty()) {
            finishedObjectList = populateFinishedObjectList(finishedList);
        }

        if (rejectList != null && !rejectList.isEmpty()) {
            rejectObjectList = populateRejectObjectList(rejectList);
        }

        ResponseHeader headerObj = new ResponseHeader();
        if (header != null) {
            headerObj.setApplicationId(header.getApplicationID());
            headerObj.setCustId(header.getConsumerID());
            headerObj.setReqRcvdTime(header.getDateOfRequest());
            headerObj.setResponseType(header.getResponseType());
            headerObj.setSourceSystem("GNG");
        }
        issueResponseMB.setHeaderObj(headerObj);
        issueResponseMB.setAckId(multiJson.getAcknowledgmentId());
        issueResponseMB.setStatus(multiJson.getStatus());

        if (finishedObjectList != null && !finishedObjectList.isEmpty()) {
            issueResponseMB.setFinishedObject(finishedObjectList);
        }
        if (rejectObjectList != null && !rejectObjectList.isEmpty()) {
            issueResponseMB.setRejectObject(rejectObjectList);
        }

        PostSoaResponseToService postHdfcResponseToService = new PostSoaResponseToService();
        mbResponse = postHdfcResponseToService.convert(issueResponseMB,losMbData);
        return mbResponse;
    }

    private ArrayList<RejectDomain> populateRejectObjectList(List<Finished> rejectList) {
        ArrayList<RejectDomain> rejectDomainList = new ArrayList<RejectDomain>();
        if (rejectList != null && !rejectList.isEmpty()) {
            for (Finished finished : rejectList) {
                if (finished.getBureau().equals(GNGWorkflowConstant.CIBIL.name())) {
                    RejectDomain rejectDomain = new RejectDomain();
                    rejectDomain.setBureau(finished.getBureau());
                    rejectDomain.setProduct(finished.getProduct());
                    rejectDomain.setTrackingId(finished.getTrackingId());
                    rejectDomain.setStatus(finished.getStatus());
                    List<Errors> errorObject = new ArrayList<Errors>();
                    List<WarningAndError> errorList = finished.getErrorList();

                    if (errorList != null && !errorList.isEmpty()) {
                        for (WarningAndError warningAndError : errorList) {
                            Errors errors = new Errors();
                            errors.setCode(warningAndError.getCode());
                            errors.setDescription(warningAndError.getDescription());
                            errorObject.add(errors);
                        }

                    }

                    if (!errorObject.isEmpty()) {
                        rejectDomain.setErrorObject(errorObject);
                    }
                    break;
                }
            }
        }
        return rejectDomainList;
    }

    public ArrayList<FinishedDomain> populateFinishedObjectList(List<Finished> finishedList) {

        ArrayList<FinishedDomain> finishedDomainList = new ArrayList<FinishedDomain>();
        for (Finished finished : finishedList) {

            FinishedDomain finishedObj = new FinishedDomain();
            finishedObj.setTrackingId(finished.getTrackingId());
            finishedObj.setBureau(finished.getBureau());
            finishedObj.setProduct(finished.getProduct());
            finishedObj.setStatus(finished.getStatus());
            finishedObj.setHtmlReport(finished.getHtmlReport());
            finishedObj.setPdfReport(finished.getPdfReport());
            finishedObj.setBureauString(finished.getBureauString());
            finishedObj.setResponseJsonObject(finished.getResponseJsonObject());
            finishedObj.setXmlResponseObject(finished.getBureauRrpObject() != null ? "" : finished.getBureauRrpObject().toString());

            finishedDomainList.add(finishedObj);
            break;

        }
        return finishedDomainList;
    }

}
