package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.softcell.constants.*;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosTvsGroupFourReqBuilder;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class LosTvsGroupFourReqBuilderImpl implements LosTvsGroupFourReqBuilder{
    @Override
    public InsertOrUpdateTvsRecordGroupFour buildLosTvsGroupFourRequest(ExtendedWarrantyDetails extendedWarrantyDetails,
                                                                        List<ESignedLog> eSignedLogList,
                                                                        GoNoGoCustomerApplication goNoGoCustomerApplication,
                                                                        InsurancePremiumDetails insurancePremiumDetails,
                                                                        SerialNumberInfo serialNumber) {

        Logger logger = LoggerFactory.getLogger(LosTvsGroupFourReqBuilderImpl.class);
        CDLos cdLos = new CDLos();
        BankDetails bankDetailsreturn = new BankDetails();
        SerialNumber serialNumberReturn = new SerialNumber();
        ExtendedWarranty extendedWarrantyReturn = new ExtendedWarranty();
        EMudra emudraReturn = new EMudra();
        Enach enachReturn = new Enach();
        InsuranceDetails insuranceDetailsReturn = new InsuranceDetails();
        try {
            if (goNoGoCustomerApplication != null) {

                if (goNoGoCustomerApplication.getApplicationRequest() != null) {
                    if (goNoGoCustomerApplication.getApplicationRequest().getRequest() != null)
                        if (goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant() != null)
                            if (CollectionUtils.isNotEmpty(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                    .getBankingDetails())) {
                                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getBankingDetails().get(0).getAccountType()))
                                {
                                    bankDetailsreturn.setAccountingtype(
                                            goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                                    .getApplicant().getBankingDetails().get(0).getAccountType());
                                }
                                if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()//////
                                        .getBankingDetails().get(0).getAccountHolderName()!=null)
                                {
                                    if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()//////
                                            .getBankingDetails().get(0).getAccountHolderName().getFirstName()))
                                    {

                                        bankDetailsreturn.setFirstname(goNoGoCustomerApplication.getApplicationRequest()
                                                .getRequest().getApplicant().getBankingDetails().get(0)
                                                .getAccountHolderName().getFirstName());
                                    }

                                    if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                            .getBankingDetails().get(0).getAccountHolderName().getLastName()))
                                    {
                                        bankDetailsreturn.setLastname(goNoGoCustomerApplication.getApplicationRequest()
                                                .getRequest().getApplicant().getBankingDetails().get(0)
                                                .getAccountHolderName().getLastName());
                                    }
                                }
                                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getBankingDetails().get(0).getAccountNumber()))
                                {
                                    bankDetailsreturn.setAccountnumber(
                                            goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                                    .getApplicant().getBankingDetails().get(0).getAccountNumber());
                                }
                                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getBankingDetails().get(0).getBranchName()))
                                {
                                    bankDetailsreturn.setBankbranch(goNoGoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getBankingDetails().get(0).getBranchName());
                                }
                                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getBankingDetails().get(0).getBankName())) {
                                    String bankDetail = GngUtils.trimString(
                                            goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                                    .getApplicant().getBankingDetails().get(0).getBankName());
                                    if (StringUtils.isNotBlank(bankDetail)) {
                                        bankDetailsreturn.setBankname(bankDetail);
                                    }
                                }
                                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getBankingDetails().get(0).getmICRCode()))
                                {
                                    bankDetailsreturn.setMicrcode(goNoGoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getBankingDetails().get(0).getmICRCode());
                                }
                                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getBankingDetails().get(0).getIfscCode()))
                                {
                                    bankDetailsreturn.setIfsccode(goNoGoCustomerApplication.getApplicationRequest()
                                            .getRequest().getApplicant().getBankingDetails().get(0).getIfscCode());
                                }
                                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant()
                                        .getBankingDetails().get(0).getRepaymentType()))
                                {

                                    if(goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                            .getApplicant().getBankingDetails().get(0).getRepaymentType().equalsIgnoreCase(LosTvsConstants.GONOGO_REPAYMENT_TYPE_ECS)){

                                        bankDetailsreturn.setRepaymenttype(LosTvsConstants.TVS_REPAYMENT_TYPE_ECS);
                                    }else

                                    if(goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                            .getApplicant().getBankingDetails().get(0).getRepaymentType().equalsIgnoreCase(LosTvsConstants.GONOGO_REPAYMENT_TYPE_ADM)){

                                        bankDetailsreturn.setRepaymenttype(LosTvsConstants.TVS_REPAYMENT_TYPE_ADM);
                                    }else

                                    if(goNoGoCustomerApplication.getApplicationRequest().getRequest()
                                            .getApplicant().getBankingDetails().get(0).getRepaymentType().equalsIgnoreCase(LosTvsConstants.GONOGO_REPAYMENT_TYPE_ECS)){

                                        bankDetailsreturn.setRepaymenttype(LosTvsConstants.TVS_REPAYMENT_TYPE_ACH);
                                    }



                                }

                            }
                }

                cdLos.setAction(LosTvsActionEnums.I.name());

                if (StringUtils.isNotBlank(goNoGoCustomerApplication.getGngRefId())) {
                    cdLos.setProspectid(goNoGoCustomerApplication.getGngRefId());
                }

                if (goNoGoCustomerApplication.getApplicationRequest() != null) {
                    if (StringUtils.isNotBlank(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                        cdLos.setStage(LosTvsInitiateConstants.DISB);
                    }
                }
                cdLos.setRemarks("");
                cdLos.setVendorid("");

            }else{
                logger.debug("goNogoApplicationCustomer not found in group four");
            }



            // for insurance
            if (insurancePremiumDetails != null) {

                double insuranceDPAm = insurancePremiumDetails.getInsuranceDPAmt();
                if (insuranceDPAm != 0) {
                    String insuranceDPAmt = Double.toString(insuranceDPAm);
                    insuranceDetailsReturn.setInsurancedownpayment(insuranceDPAmt);
                }
                double insuranceDPAminsuranceprem = insurancePremiumDetails.getInsuranceEmi();
                if (insuranceDPAminsuranceprem != 0) {
                    String insurancepremiumemi = Double.toString(insuranceDPAminsuranceprem);
                    insuranceDetailsReturn.setInsurancepremiumemi(insurancepremiumemi);
                }
                boolean sinsuranced = insurancePremiumDetails.isActiveFlag();
                String sinsurancedetails = String.valueOf(sinsuranced);
                if (StringUtils.isNotBlank(sinsurancedetails)) {
                    if (sinsurancedetails.equalsIgnoreCase(LosTvsFlagEnum.TRUE.name())) {
                        insuranceDetailsReturn.setIsinsurancedetails(LosTvsFlagEnum.T.name());
                    } else {
                        insuranceDetailsReturn.setIsinsurancedetails(LosTvsFlagEnum.F.name());
                    }
                }

                insuranceDetailsReturn
                        .setInsurancepremiumtype(insurancePremiumDetails.getInsuranceType().name());
                insuranceDetailsReturn.setInsuranceprovider(insurancePremiumDetails.getInsuranceType().name());
            }else{
                logger.debug("insurancePremiumDetails not found in group four");
            }

            // serial number

            if (serialNumber != null) {
                if (StringUtils.isNotBlank(serialNumber.getVendor())) {
                    serialNumberReturn.setAssetvendor(serialNumber.getVendor());
                }
                if (StringUtils.isNotBlank(serialNumber.getImeiNumber())) {
                    serialNumberReturn.setImeinumber(serialNumber.getImeiNumber());
                }
                if (StringUtils.isNotBlank(serialNumber.getDistributorId())) {
                    serialNumberReturn.setDealerid(serialNumber.getDistributorId());
                }
                if (StringUtils.isNotBlank(serialNumber.getProductCode())) {
                    serialNumberReturn.setProducttype(serialNumber.getProductCode());
                }
                if (StringUtils.isNotBlank(serialNumber.getScheme())) {
                    serialNumberReturn.setScheme(serialNumber.getScheme());
                }
                if (StringUtils.isNotBlank(serialNumber.getSerialNumber())) {
                    serialNumberReturn.setSerialnumber(serialNumber.getSerialNumber());
                }
                if (StringUtils.isNotBlank(serialNumber.getSkuCode())) {
                    serialNumberReturn.setSkucode(serialNumber.getSkuCode());
                }
                if (StringUtils.isNotBlank(serialNumber.getStatus())) {
                    if (serialNumber.getStatus().equalsIgnoreCase(LosTvsFlagEnum.TRUE.name()))
                        serialNumberReturn.setSnoValidationStatus(LosTvsFlagEnum.T.name());
                    else
                        serialNumberReturn.setSnoValidationStatus(LosTvsFlagEnum.F.name());
                }
            }else{
                logger.debug("serialNumber not found in group four");
            }

            if (extendedWarrantyDetails != null) {
                if (StringUtils.isNotBlank(extendedWarrantyDetails.geteWAssetCategory())) {
                    extendedWarrantyReturn.setExtendedwarrantycategory(extendedWarrantyDetails.geteWAssetCategory());
                }

                double extendedwarrantydownpay = extendedWarrantyDetails.getEwDPAmt();
                String extendedwarrantydownpayment = Double.toString(extendedwarrantydownpay);
                if (StringUtils.isNotBlank(extendedwarrantydownpayment))
                    extendedWarrantyReturn.setExtendedwarrantydownpayment(extendedwarrantydownpayment);

                double revisedTotalEmi = extendedWarrantyDetails.getRevisedTotalEmi();
                if (revisedTotalEmi != 0) {
                    String extendedwarrantyemi = Double.toString(revisedTotalEmi);
                    if (StringUtils.isNotBlank(extendedwarrantyemi))
                        extendedWarrantyReturn.setExtendedwarrantyemi(extendedwarrantyemi);

                    extendedWarrantyReturn.setExtendedwarrantypremiumtype(
                            extendedWarrantyDetails.getPaymentMethod().name());
                }
                double ewWarrantyTenure = extendedWarrantyDetails.getEwWarrantyTenure();
                if (ewWarrantyTenure != 0) {
                    String extendedwarrantytenor = Double.toString(ewWarrantyTenure);
                    if (StringUtils.isNotBlank(extendedwarrantytenor))
                        extendedWarrantyReturn.setExtendedwarrantytenor(extendedwarrantytenor);
                }
                double oemWarranty = extendedWarrantyDetails.getOemWarranty();
                if (oemWarranty != 0) {
                    String manufacturerwarranty = Double.toString(oemWarranty);
                    if (StringUtils.isNotBlank(manufacturerwarranty))
                        extendedWarrantyReturn.setManufacturerwarranty(manufacturerwarranty);
                }
                double ewWarrantyPremium = extendedWarrantyDetails.getEwWarrantyPremium();
                String premium = Double.toString(ewWarrantyPremium);
                if (StringUtils.isNotBlank(premium))
                    extendedWarrantyReturn.setPremium(premium);

                boolean Isextendedwarranty = extendedWarrantyDetails.isActiveFlag();
                String setIsextendedwarranty = String.valueOf(Isextendedwarranty);
                if (StringUtils.isNotBlank(setIsextendedwarranty)) {
                    if (setIsextendedwarranty.equalsIgnoreCase(LosTvsFlagEnum.TRUE.name()))
                        extendedWarrantyReturn.setIsextendedwarranty(LosTvsFlagEnum.T.name());
                    else
                        extendedWarrantyReturn.setIsextendedwarranty(LosTvsFlagEnum.F.name());
                }

            }else{
                logger.debug("extendedWarrantyDetails not found in group four");
            }

            if (CollectionUtils.isNotEmpty(eSignedLogList)) {
                for (int i=0;i<eSignedLogList.size();i++){
                    ESignedLog eSignedLog = eSignedLogList.get(i);

                    if(eSignedLog.getRequestType().equalsIgnoreCase(LosTvsGroupFourReqTypeEnum.ESIGN.name())){

                        if (StringUtils.isNotBlank(eSignedLog.getAadhaarNumber())) {
                            emudraReturn.setAadharnumber(eSignedLog.getAadhaarNumber());
                        }
                        if (StringUtils.isNotBlank(eSignedLog.getAuthMode())) {
                            emudraReturn.setAuthmode(eSignedLog.getAuthMode());
                        }
                        if (StringUtils.isNotBlank(eSignedLog.getStatus())) {
                            emudraReturn.setStatus(eSignedLog.getStatus());

                        }
                        if (StringUtils.isNotBlank(eSignedLog.getFileType())) {
                            emudraReturn.setFiletype(eSignedLog.getFileType());

                        }
                        if (StringUtils.isNotBlank(eSignedLog.getTransactionId())) {
                            emudraReturn.setTransactionid(eSignedLog.getTransactionId());
                        }

                        if (StringUtils.isNotBlank(eSignedLog.getInstitutionId())) {
                            emudraReturn.setInstitutionid(eSignedLog.getInstitutionId());
                        }
                        String setNumberofdocsend = null;

                        int getNumberOfDocSend = eSignedLog.getNumberOfDocSend();
                        if (getNumberOfDocSend != 0) {
                            setNumberofdocsend = String.valueOf(getNumberOfDocSend);
                            if (StringUtils.isNotBlank(setNumberofdocsend)) {
                                emudraReturn.setNumberofdocsend(setNumberofdocsend);
                            }
                        }
                        String setNumberofdoc = null;
                        int getNumberOfDocSigne = eSignedLog.getNumberOfDocSigned();
                        if (getNumberOfDocSigne != 0) {
                            setNumberofdoc = String.valueOf(getNumberOfDocSigne);
                            if (StringUtils.isNotBlank(setNumberofdoc)) {
                                emudraReturn.setNumberofdocsend(setNumberofdoc);
                            }
                        }

                        boolean isConsentToESigned = eSignedLog.isConsentToESigned();
                        String Consenttoesigned = String.valueOf(isConsentToESigned);
                        if (StringUtils.isNotBlank(Consenttoesigned)) {
                            if (Consenttoesigned.equalsIgnoreCase(LosTvsFlagEnum.TRUE.name()))
                                emudraReturn.setConsenttoesigned(LosTvsFlagEnum.T.name());
                            else
                                emudraReturn.setConsenttoesigned(LosTvsFlagEnum.F.name());
                        }
                        boolean ConsentToESigned = eSignedLog.isConsentToESigned();
                        String setConsenttosigned = String.valueOf(ConsentToESigned);
                        if (StringUtils.isNotBlank(setConsenttosigned)) {
                            if (setConsenttosigned.equalsIgnoreCase(LosTvsFlagEnum.TRUE.name()))
                                emudraReturn.setConsenttosigned(LosTvsFlagEnum.T.name());
                            else
                                emudraReturn.setConsenttosigned(LosTvsFlagEnum.F.name());
                        }

                        if (StringUtils.isNotBlank(eSignedLog.getUpdateBy())) {
                            emudraReturn.setUpdateby(eSignedLog.getUpdateBy());
                        }
                        Long version = eSignedLog.getVersion();
                        String varsion = String.valueOf(version);
                        if (StringUtils.isNotBlank(varsion)) {
                            emudraReturn.setVersion(varsion);
                        }

                        DateTime date1 = eSignedLog.getUpdatedDate();
                        if (date1 != null) {
                            String str = GngDateUtil.getDdMmYyyyFormatDate(date1);
                            logger.debug("str:::"+str);
                            emudraReturn.setUpdateddate(str);
                        }

                        Date date = eSignedLog.getDate();
                        if (date != null) {
                            String eSignLoagDate = GngDateUtil.convertDateIntoStringDateFormat(date);
                            emudraReturn.setEmudradate(eSignLoagDate);
                        }

                    }



                    // ============
                    if ( eSignedLog.getRequestType().equalsIgnoreCase(LosTvsGroupFourReqTypeEnum.ENACH.name())) {
                        if (StringUtils.isNotBlank(eSignedLog.getAadhaarNumber())) {
                            enachReturn.setAadhaarnumber(eSignedLog.getAadhaarNumber());
                        }
                        if (StringUtils.isNotBlank(eSignedLog.getAuthMode())) {
                            enachReturn.setAuthmode(eSignedLog.getAuthMode());
                        }
                        if (StringUtils.isNotBlank(eSignedLog.getStatus())) {
                            String status = eSignedLog.getStatus();
                            if(status.equalsIgnoreCase(LosTvsFlagEnum.SUCCESS.name()))
                            {
                                enachReturn.setStatus(LosTvsFlagEnum.T.name());
                            }else
                            {
                                enachReturn.setStatus(LosTvsFlagEnum.F.name());
                            }


                        }
                        if (StringUtils.isNotBlank(eSignedLog.getFileType())) {
                            enachReturn.setFiletype(eSignedLog.getFileType());

                        }
                        if (StringUtils.isNotBlank(eSignedLog.getTransactionId())) {
                            enachReturn.setTransactionid(eSignedLog.getTransactionId());
                        }
                        if (StringUtils.isNotBlank(eSignedLog.getFileType())) {
                            enachReturn.setFiletype(eSignedLog.getFileType());

                        }
                        if (StringUtils.isNotBlank(eSignedLog.getRefId())) {
                            enachReturn.setRefid(eSignedLog.getRefId());

                        }

                        boolean isConsentToES = eSignedLog.isConsentToESigned();
                        String setConsenttoesig = String.valueOf(isConsentToES);
                        if (StringUtils.isNotBlank(setConsenttoesig)) {
                            if (setConsenttoesig.equalsIgnoreCase(LosTvsFlagEnum.TRUE.name()))

                                enachReturn.setConsenttoesigned(LosTvsFlagEnum.T.name());
                            else
                                enachReturn.setConsenttoesigned(LosTvsFlagEnum.F.name());
                        }
                    }
                }

            }

            return InsertOrUpdateTvsRecordGroupFour.builder()
                    .bankDetails(bankDetailsreturn)
                    .CDLos(cdLos)
                    .EMudra(emudraReturn)
                    .enach(enachReturn)
                    .extendedWarranty(extendedWarrantyReturn)
                    .insuranceDetails(insuranceDetailsReturn)
                    .serialNumber(serialNumberReturn)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occured in InsertOrUpdateTvsRecordForGroup004 serviceIMPL",e);
            return null;
        }
    }


    public boolean doesObjectContainField(Object object, String fieldName) {
        return Arrays.stream(object.getClass().getFields())
                .anyMatch(f -> f.getName().equals(fieldName));
    }
}
