package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.model.response.manufacturer.samsung.SerialNumberResponse;
import com.softcell.gonogo.serialnumbervalidation.yuho.YuhoRequest;
import com.softcell.gonogo.serialnumbervalidation.yuho.YuhoResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.YuhoSerialNumberValidationBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Created by prasenjit wadmare on 19/11/17.
 */
@Service
public class YuhoSerialNumberValidationBuilderImpl implements YuhoSerialNumberValidationBuilder{

    @Override
    public YuhoRequest buildYuhoImeiNumberRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain yuhoConfig) {
        return YuhoRequest.builder()
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .modelCode(serialSaleConfirmationRequest.getSkuCode())
                .retailerCode(serialSaleConfirmationRequest.getPartnerCode())
                .authHeader(yuhoConfig.getLicenseKey())
                .source(Cache.getVendorByInstitution(serialSaleConfirmationRequest.getHeader().getInstitutionId()))
                .build();
    }

    @Override
    public SerialNumberResponse buildYuhoImeiNumberResponse(YuhoResponse yuhoResponse) {
        return SerialNumberResponse.builder()
                .vendor(Vendor.YUHO.toFaceValue())
                .message(yuhoResponse.getMessage())
                .status((StringUtils.equals("0",yuhoResponse.getStatusCode()) ? Status.VALID.name() : Status.INVALID.name()))
                .build();
    }

}
