package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.softcell.constants.LosTvsActionEnums;
import com.softcell.constants.LosTvsAddressTypeEnum;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.*;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosTvsGroupFiveReqBuilder;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LosTvsGroupFiveReqBuilderImpl implements LosTvsGroupFiveReqBuilder {
    @Override
    public InsertOrUpdateTvsRecordGroupFive buildTvsLosGroupFiveRequest(GoNoGoCustomerApplication gonogoCustomerApplicationFive,
                                                                        DocumentListForGroupFive uploadFileDetails) {

        Logger logger = LoggerFactory.getLogger(LosTvsGroupFiveReqBuilderImpl.class);
        ShippingAddressResidence shippingAddressResidence = new ShippingAddressResidence();
        ShippingAddressPermanent shippingAddressPermanent = new ShippingAddressPermanent();
        ShippingAddressOffice shippingAddressOffice = new ShippingAddressOffice();
        Invoice invoice = new Invoice();
        ShippingAddressOther shippingAddressOther = new ShippingAddressOther();
        CDLos CDLos = new CDLos();
        InsertOrUpdateTvsRecordGroupFive insertOrUpdateTvsRecordGroupFive = new InsertOrUpdateTvsRecordGroupFive();
        try { // non mandatory fields Invoice
            if(gonogoCustomerApplicationFive!=null){
            if (gonogoCustomerApplicationFive.getInvoiceDetails() != null) {
                if (gonogoCustomerApplicationFive.getInvoiceDetails().getInvoiceDate() != null) {

                    Date getinvoiceDate = gonogoCustomerApplicationFive.getInvoiceDetails().getInvoiceDate();
                    if (getinvoiceDate != null) {
                        String InvoiceDate = GngDateUtil.convertDateIntoStringDateFormat(getinvoiceDate);
                        if (StringUtils.isNotBlank(InvoiceDate)) {
                            invoice.setInvoicedate(InvoiceDate);

                        }

                    }

                }
                if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getInvoiceDetails().getInvoiceNumber())) {
                    invoice.setInvoicenumber(gonogoCustomerApplicationFive.getInvoiceDetails().getInvoiceNumber());
                }
                if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getInvoiceDetails().getSerialOrImeiNumber())) {
                    invoice.setInvoiceserialnumber(
                            gonogoCustomerApplicationFive.getInvoiceDetails().getSerialOrImeiNumber());
                }
                if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getInvoiceDetails().getInvoiceType())) {
                    invoice.setInvoicetype(gonogoCustomerApplicationFive.getInvoiceDetails().getInvoiceType());
                }

                // non mandatory field shippingAddressOther
                if (gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress() != null) {
                    if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress()
                            .getAddressLine1())) {

                        String AddressLine = GngUtils.trimString(gonogoCustomerApplicationFive.getInvoiceDetails()
                                .getDeliveryAddress().getAddressLine1());
                        if (StringUtils.isNotBlank(AddressLine)) {
                            shippingAddressOther.setAddressline1(AddressLine);
                        }
                    }
                    if (gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress()
                            .getAddressLine2() != null) {

                        String AddressLine = GngUtils.trimString(gonogoCustomerApplicationFive.getInvoiceDetails()
                                .getDeliveryAddress().getAddressLine2());
                        if (StringUtils.isNotBlank(AddressLine)) {
                            shippingAddressOther.setAddressline2(AddressLine);
                        }

                    }
                    if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress().getCity())) {
                        shippingAddressOther.setCity(
                                gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress().getCity());
                    }
                    if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress()
                            .getLandMark())) {
                        shippingAddressOther.setLandmark(
                                gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress().getLandMark());
                    }

                    if (gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress()
                            .getMonthAtAddress() != 0) {
                        shippingAddressOther.setMonthataddress(gonogoCustomerApplicationFive.getInvoiceDetails()
                                .getDeliveryAddress().getMonthAtAddress());
                    }
                    if (gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress().getPin() != 0)///// -------null
                    ///// /////
                    ///// problem
                    {
                        long getPin = gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress().getPin();
                        int Pin = (int) getPin;
                        shippingAddressOther.setPin(Pin);
                    }
                    if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress().getState())) {
                        shippingAddressOther.setState(
                                gonogoCustomerApplicationFive.getInvoiceDetails().getDeliveryAddress().getState());
                    }
                }
            }

            //////////////////////////// MANDATORY////////////////////////////////

            if (gonogoCustomerApplicationFive.getApplicationRequest() != null) {
                if (gonogoCustomerApplicationFive.getApplicationRequest().getRequest() != null) {
                    if (gonogoCustomerApplicationFive.getApplicationRequest().getRequest().getApplicant() != null) {
                        if (CollectionUtils.isNotEmpty(gonogoCustomerApplicationFive.getApplicationRequest().getRequest().getApplicant()
                                .getAddress())) {

                            for (int i = 0; i < gonogoCustomerApplicationFive.getApplicationRequest().getRequest()
                                    .getApplicant().getAddress().size(); i++) {
                                //// mandatory field shippingAddressResidence
                                CustomerAddress custAdd = gonogoCustomerApplicationFive.getApplicationRequest()
                                        .getRequest().getApplicant().getAddress().get(i);

                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.RESIDENCE.name())) {

                                    if (StringUtils.isNotBlank(custAdd.getAddressLine1())) {
                                        String AddressLine = GngUtils.trimString(custAdd.getAddressLine1());
                                        if (StringUtils.isNotBlank(AddressLine)) {
                                            shippingAddressResidence.setAddressline1(AddressLine);
                                        }
                                    }

                                    String AddressLine = GngUtils.trimString(custAdd.getAddressLine2());
                                    if (StringUtils.isNotBlank(AddressLine)) {
                                        shippingAddressResidence.setAddressline2(AddressLine);
                                    }

                                    if (StringUtils.isNotBlank(custAdd.getCity())) {
                                        shippingAddressResidence.setCity(custAdd.getCity());
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getLandMark())) {
                                        shippingAddressResidence.setLandmark(custAdd.getLandMark());
                                    }

                                    if (custAdd.getPin() != 0) {
                                        long getPin = custAdd.getPin();
                                        int Pin = (int) getPin;
                                        shippingAddressResidence.setPin(Pin);
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getResidenceAddressType())) {
                                        shippingAddressResidence
                                                .setResidenceaddresstype(custAdd.getResidenceAddressType());
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getState())) {
                                        shippingAddressResidence.setState(custAdd.getState());
                                    }
                                }

                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.PERMANENT.name())) {

                                    String AddressLine = GngUtils.trimString(custAdd.getAddressLine1());
                                    if (StringUtils.isNotBlank(AddressLine)) {
                                        shippingAddressPermanent.setAddressline1(AddressLine);
                                    }

                                    String AddressLinePerm = GngUtils.trimString(custAdd.getAddressLine2());
                                    if (StringUtils.isNotBlank(AddressLinePerm)) {
                                        shippingAddressPermanent.setAddressline2(AddressLinePerm);
                                    }

                                    if (StringUtils.isNotBlank(custAdd.getCity())) {
                                        shippingAddressPermanent.setCity(custAdd.getCity());
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getLandMark())) {
                                        shippingAddressPermanent.setLandmark(custAdd.getLandMark());
                                    }

                                    if (custAdd.getPin() != 0) {
                                        long getPin = custAdd.getPin();
                                        int Pin = (int) getPin;
                                        shippingAddressPermanent.setPin(Pin);
                                    }
                                    if (custAdd.getMonthAtAddress() != 0) {
                                        shippingAddressPermanent.setMonthataddress(custAdd.getMonthAtAddress());
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getState())) {
                                        shippingAddressPermanent.setState(custAdd.getState());
                                    }

                                }

                                if (custAdd.getAddressType().equalsIgnoreCase(LosTvsAddressTypeEnum.OFFICE.name())) {
                                    String AddressLine = GngUtils.trimString(custAdd.getAddressLine1());
                                    if (StringUtils.isNotBlank(AddressLine)) {
                                        shippingAddressOffice.setAddressline1(AddressLine);
                                    }

                                    String AddressLineOfc = GngUtils.trimString(custAdd.getAddressLine2());
                                    if (StringUtils.isNotBlank(AddressLineOfc)) {
                                        shippingAddressOffice.setAddressline2(AddressLineOfc);
                                    }

                                    if (StringUtils.isNotBlank(custAdd.getCity())) {
                                        shippingAddressOffice.setCity(custAdd.getCity());
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getLandMark())) {
                                        shippingAddressOffice.setLandmark(custAdd.getLandMark());
                                    }

                                    if (custAdd.getPin() != 0) {
                                        long getPin = custAdd.getPin();
                                        int Pin = (int) getPin;
                                        shippingAddressOffice.setPin(Pin);
                                    }
                                    if (custAdd.getMonthAtAddress() != 0) {
                                        shippingAddressOffice.setMonthataddress(custAdd.getMonthAtAddress());
                                    }
                                    if (StringUtils.isNotBlank(custAdd.getState())) {
                                        shippingAddressOffice.setState(custAdd.getState());
                                    }

                                }

                            }

                        }
                    }
                }
            }
            CDLos.setAction(LosTvsActionEnums.I.name());
            if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getGngRefId()))
            {
                CDLos.setProspectid(gonogoCustomerApplicationFive.getGngRefId());// M
            }
            if (gonogoCustomerApplicationFive.getApplicationRequest() != null) {
                if (StringUtils.isNotBlank(gonogoCustomerApplicationFive.getApplicationRequest().getCurrentStageId())) {
                    CDLos.setStage(gonogoCustomerApplicationFive.getApplicationRequest().getCurrentStageId());// M
                }
            }
            CDLos.setRemarks("");
            CDLos.setVendorid("");

            if (CollectionUtils.isNotEmpty(uploadFileDetails.getDmsDocList())) {
                DMSDocuments[] dmsDocNewArr = uploadFileDetails.getDmsDocList()
                        .toArray(new DMSDocuments[uploadFileDetails.getDmsDocList().size()]);
                for (int i = 0; i < dmsDocNewArr.length; i++) {
                    logger.debug("file1:::" + dmsDocNewArr[i]);
                }
                insertOrUpdateTvsRecordGroupFive.setDMSDocuments(dmsDocNewArr);

                Pdfs[] pdfDocNewArr = uploadFileDetails.getPdfsDocList()
                        .toArray(new Pdfs[uploadFileDetails.getPdfsDocList().size()]);
                for (int i = 0; i < pdfDocNewArr.length; i++) {
                    logger.debug("file2:::" + pdfDocNewArr[i]);
                }
                insertOrUpdateTvsRecordGroupFive.setPdfs(pdfDocNewArr);

            }
            }

            return InsertOrUpdateTvsRecordGroupFive.builder()
                                                    .CDLos(CDLos)
                                                    .invoice(invoice)
                                                    .shippingAddressOffice(shippingAddressOffice)
                                                    .shippingAddressOther(shippingAddressOther)
                                                    .shippingAddressPermanent(shippingAddressPermanent)
                                                    .shippingAddressResidence(shippingAddressResidence)
                                                    .DMSDocuments(insertOrUpdateTvsRecordGroupFive.getDMSDocuments())
                                                    .pdfs(insertOrUpdateTvsRecordGroupFive.getPdfs())
                                                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error occured in  InsertOrUpdateTvsRecordGroupFive serviceIMPL",e);
            return null;
        }
    }
}
