package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.posidex.DedupeEnquiryRequest;
import com.softcell.gonogo.model.posidex.GetEnquiryRequest;
import com.softcell.nextgen.domain.WFJobCommDomain;

/**
 * Created by yogeshb on 21/8/17.
 */
public interface PosidexRequestBuilder {
    /**
     * @param goNoGoCustomerApplication
     * @param posidexConfig
     * @return
     */
    DedupeEnquiryRequest buildInitialRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, WFJobCommDomain posidexConfig);

    /**
     * @param dedupeEnquiryResponse
     * @param posidexConfig
     * @return
     */
    GetEnquiryRequest buildIssueRequest(DedupeEnquiryRequest dedupeEnquiryResponse, WFJobCommDomain posidexConfig);
}
