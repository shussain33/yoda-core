package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.constants.Status;
import com.softcell.constants.Vendor;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.serialnumbervalidation.voltas.ServiceCodeRegResponse;
import com.softcell.gonogo.serialnumbervalidation.voltas.VoltasRequest;
import com.softcell.gonogo.serialnumbervalidation.voltas.VoltasResponse;
import com.softcell.gonogo.service.factory.serialnumbervalidation.VoltasRequestNumberValidationBuilder;
import com.softcell.nextgen.domain.WFJobCommDomain;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 *  created by prasenjit wadmare on 18/12/2017
 */


@Service
public class VoltasRequestNumberValidationBuilderImpl implements VoltasRequestNumberValidationBuilder {

    @Override
    public VoltasRequest buildVoltasRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest, WFJobCommDomain voltasConfig) {
        return VoltasRequest.builder()
                .serviceRequestNumber(serialSaleConfirmationRequest.getServiceRequestNumber())
                .userName(voltasConfig.getMemberId())
                .password(voltasConfig.getPassword())
                .sessionType(voltasConfig.getSessionType())
                .build();
    }

    @Override
    public ServiceCodeRegResponse buildVoltasResponse(VoltasResponse voltasResponse) {
        return ServiceCodeRegResponse.builder()
                .status(StringUtils.equalsIgnoreCase(Status.VALID.name(), voltasResponse.getOutput()) ? Status.VALID.name()
                        : StringUtils.equalsIgnoreCase(Status.INVALID.name(), voltasResponse.getOutput()) ? Status.INVALID.name()
                        : StringUtils.equalsIgnoreCase(Status.DUPLICATE.name(), voltasResponse.getOutput()) ? Status.DUPLICATE.name()
                        : Status.INVALID.name())
                .message(voltasResponse.getOutput())
                .vendor(Vendor.VOLTAS.toFaceValue())
                .customerName(voltasResponse.getCustomerName())
                .productCategory(voltasResponse.getProductCategory())
                .helpDeskNumber(voltasResponse.getHelpDeskNumber())
                .serviceRequestNumber(voltasResponse.getServiceRequestNumber())
                .build();
    }

    @Override
    public SerialNumberInfo.Builder buildVoltasServiceCodeLogs(VoltasResponse voltasResponse, SerialNumberInfo.Builder serialNumberInfoBuilder) {

        serialNumberInfoBuilder
                .productCategory(voltasResponse.getProductCategory())
                .unitStatus(voltasResponse.getUnitStatus())
                .purchaseDate(voltasResponse.getPurchaseDate())
                .alternateContact(voltasResponse.getAlternateContact())
                .customerEmail(voltasResponse.getCustomerEmail())
                .area(voltasResponse.getArea())
                .branchName(voltasResponse.getBranchName())
                .callType(voltasResponse.getCallType())
                .closedDate(voltasResponse.getClosedDate())
                .customerName(voltasResponse.getCustomerName())
                .franchisee(voltasResponse.getFranchisee())
                .lotNumber(voltasResponse.getLotNumber())
                .openDate(voltasResponse.getOpenDate())
                .serviceRequestNumber(voltasResponse.getServiceRequestNumber())
                .serviceRequestStatus(voltasResponse.getServiceRequestStatus())
                .postalCode(voltasResponse.getPostalCode())
                .output(voltasResponse.getOutput())
                .originalResponse(voltasResponse.getOutput());

        return serialNumberInfoBuilder;
    }


}
