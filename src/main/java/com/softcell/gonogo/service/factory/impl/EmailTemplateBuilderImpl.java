package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.model.email.EmailTemplateDetails;
import com.softcell.gonogo.service.factory.EmailTemplateBuilder;
import com.softcell.gonogo.utils.factory.TemplateFactory;
import com.softcell.utils.GngNumUtil;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;

/**
 * @author kishorp
 */
@Service
public class EmailTemplateBuilderImpl implements EmailTemplateBuilder {

    @Override
    public String buildEmailTemplate(EmailTemplateDetails emailTemplateDetails) {

        StringWriter writer = null;
        try {
            Template template;
            VelocityContext context = new VelocityContext();
            writer = new StringWriter();
            /**
             * add commas in loan amount
             */
            context.put("loanAmount", GngNumUtil
                    .toLoanAmountFormat(emailTemplateDetails.getLoanAmount()));
            final String templatePath = "templates/"
                    + emailTemplateDetails.getTemplate();
            TemplateFactory templateFactory = new TemplateFactory();
            template = templateFactory.fetchTemplate(templatePath);
            template.merge(context, writer);
            return writer.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            writer.flush();
            try {
                if (null != writer)
                    writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
