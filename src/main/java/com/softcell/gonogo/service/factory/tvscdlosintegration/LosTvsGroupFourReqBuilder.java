package com.softcell.gonogo.service.factory.tvscdlosintegration;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.InsertOrUpdateTvsRecordGroupFour;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;

import java.util.List;

public interface LosTvsGroupFourReqBuilder {
    InsertOrUpdateTvsRecordGroupFour buildLosTvsGroupFourRequest(ExtendedWarrantyDetails extendedWarrantyDetails, List<ESignedLog> eSignedLog, GoNoGoCustomerApplication goNoGoCustomerApplication, InsurancePremiumDetails insurancePremiumDetails, SerialNumberInfo serialNumber);
}
