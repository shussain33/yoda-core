package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.creditVidya.Loan;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationRequest;
import com.softcell.gonogo.model.ops.DisbursementMemo;
import com.softcell.gonogo.model.ops.LoanCharges;

import java.util.Map;

/**
 * Created by kumar on 23/7/18.
 */
public interface LMSBuilder {

    /**
     *
     * @param goNoGoCustomerApplication
     * @param sbfclmsIntegrationRequest
     * @param lmsMasterDataMap
     * @return
     */
    SBFCLMSIntegrationRequest buildRetrieveClientRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap);

    /**
     *
     * @param goNoGoCustomerApplication
     * @param sbfclmsIntegrationRequest
     * @param lmsMasterDataMap
     * @return
     */
    SBFCLMSIntegrationRequest buildClientCreationRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap, CamDetails camDetails, LoanCharges loanCharges);

    /**
     *
     * @param goNoGoCustomerApplication
     * @param clientId
     * @param sbfclmsIntegrationRequest
     * @param lmsMasterDataMap
     * @param loanCharges
     * @return
     */
    SBFCLMSIntegrationRequest buildCreateLoanRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, String clientId, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap, LoanCharges loanCharges);

    /**
     *
     * @param goNoGoCustomerApplication
     * @param sbfclmsIntegrationRequest
     * @param loanCharges
     * @param loanId
     * @return
     */
    SBFCLMSIntegrationRequest buildApproveRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, LoanCharges loanCharges, String loanId, Map lmsMasterDataMap);

    /**
     *  @param goNoGoCustomerApplication
     * @param sbfclmsIntegrationRequest
     * @param loanCharges
     * @param loanId
     * @param disbursementMemo
     * @return
     */
    SBFCLMSIntegrationRequest buildDisburseRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, LoanCharges loanCharges, String loanId, DisbursementMemo disbursementMemo, Map lmsMasterDataMap);

    /**
     *
     * @param goNoGoCustomerApplication
     * @param sbfclmsIntegrationRequest
     * @param lmsMasterDataMap
     * @return
     */
    SBFCLMSIntegrationRequest buildClientCreationRequestForEntity(GoNoGoCustomerApplication goNoGoCustomerApplication, SBFCLMSIntegrationRequest sbfclmsIntegrationRequest, Map lmsMasterDataMap, CamDetails camDetails, LoanCharges loanCharges);

}