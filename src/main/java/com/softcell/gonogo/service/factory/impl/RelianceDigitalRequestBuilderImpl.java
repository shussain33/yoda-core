package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.AssetDetails;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.reliance.RelianceDigitalRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.service.factory.RelianceDigitalRequestBuilder;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class RelianceDigitalRequestBuilderImpl implements RelianceDigitalRequestBuilder {

    @Override
    public RelianceDigitalRequest buildRelianceDigitalRequest(PostIpaRequest postIpaRequest, ApplicationRequest appRequest) throws Exception {
        RelianceDigitalRequest request = new RelianceDigitalRequest();

        if (postIpaRequest != null && appRequest != null && postIpaRequest.getHeader() != null && postIpaRequest.getPostIPA() != null) {

            String refId = postIpaRequest.getRefID();

            PostIPA postIPA = postIpaRequest.getPostIPA();
            /**
             * Below Fields are marked compulsory from reliance
             */
            // These values is hardcoded as per reliance requirments
            request.setMopID("68");
            request.setGuid("5821a4c9-5007-4367-a4a1-071a2cfa1b27");
            request.setDoStatus("New");
            request.setDoNumber(refId);
            request.setNetLoanAmount(getString(postIPA.getNetFundingAmount()));
            request.setGrossLoanAmount(getString(postIPA.getFinanceAmount()));
            request.setProcessingFee(getString(postIPA.getProcessingFees()));
            request.setCustomerDownPayment(getString(postIPA.getEmi()));
            request.setSubvention(getString(postIPA.getDealerSubvention()));
            request.setNetDisbursement(getString(postIPA.getNetDisbursalAmount()));
            request.setAdvanceEMI(getString(postIPA.getAdvanceEmi()));
            request.setCostOfProduct(getString(postIPA.getTotalAssetCost()));
            request.setDealID(refId);
            request.setSchemeName(postIPA.getSchemeDscr());

            String dbd = calculateDbd(postIPA);
            request.setDbdPercentage(dbd);

            request.setTenure(postIPA.getTenor() + "");
            request.setNumAdvTenure(postIPA.getAdvanceEMITenor() + "");

            //This field we are making blank as HDBFS don't want to send it
            request.setMfrSubvention("");
            request.setMarginMoney(getString(postIPA.getMarginMoney()));

            //Other Charges is marked 0 as we are not using it. Asset quantity will always be 1
            request.setOtherCharges(getString(postIPA.getOtherChargesIfAny()));
            request.setAssetQuantity("1");

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date date = postIpaRequest.getDate();
            String dateString = sdf.format(date);
            request.setCreatedDate(dateString);

            java.util.List<AssetDetails> assetList = postIPA.getAssetDetails();

            setAssetValue(assetList, request);
            /**
             * Below fields we are populating from Application Request Object
             * which we fetched using reference id from PostIpaRequest
             */
            if (appRequest != null) {
                poupulateApplicantDetails(request, appRequest);
            }

        } else {
            throw new Exception("Post Ipa and Application request object must not be null !!");
        }
        return request;
    }


    private String calculateDbd(PostIPA postIPA) {
        Double dealerSubvention = postIPA.getDealerSubvention();
        Double financeAmount = postIPA.getFinanceAmount();
        Double dbd = 0d;

        if (financeAmount > 0) {
            dbd = (dealerSubvention * 100) / financeAmount;
        }

        DecimalFormat df = new DecimalFormat("#.##");
        dbd = Double.valueOf(df.format(dbd));

        return getString(dbd);
    }

    private void poupulateApplicantDetails(RelianceDigitalRequest request, ApplicationRequest appRequest) {

        Header header = appRequest.getHeader();
        request.setHeader(header);
        request.setRefID(appRequest.getRefID());

        Request userRequest = appRequest.getRequest();
        if (userRequest != null) {

            if (appRequest.getHeader() != null) {
                request.setDealerCode(appRequest.getHeader().getDealerId());
            } else {
                request.setDealerCode("");
            }


            Applicant applicant = userRequest.getApplicant();

            if (applicant != null) {
                request.setCustomerName(getName(applicant.getApplicantName()));
                if (applicant.getEmail() != null && applicant.getEmail().size() > 0) {
                    request.setEmailId(applicant.getEmail().get(0).getEmailAddress());
                }
                if (applicant.getPhone() != null && applicant.getPhone().size() > 0) {
                    request.setMobileNo(applicant.getPhone().get(0).getPhoneNumber());
                }

                List<CustomerAddress> address = applicant.getAddress();
                setAddress(address, request);
            } else {
                request.setCustomerName("");
                request.setEmailId("");
                request.setMobileNo("");
            }


        }

    }

    private void setAddress(List<CustomerAddress> addressList, RelianceDigitalRequest request) {
        if (addressList != null && addressList.size() > 0) {
            CustomerAddress address = addressList.get(0);
            request.setPlotNo("");
            request.setHouseNo("");
            request.setFloorNo("");
            request.setBuildingName("");
            request.setWingNo("");
            request.setSocietyName("");
            request.setArea("");
            request.setStreet("");
            request.setSector("");
            request.setLandmark(address.getLandMark());
            request.setCity(address.getCity());
            request.setPincode(address.getPin() + "");
            request.setState(address.getState());

            String addressString = address.getAddressLine1() + " " + address.getAddressLine2() + " " + address.getCity() + " " + address.getState();

            if (addressString != null && addressString.length() > 0) {
                if (addressString.length() > 39) {
                    addressString = addressString.substring(0, 39);
                } else {
                    addressString = addressString.substring(0, addressString.length() - 1);
                }
            }

            //FIXME:address string not accepting
            request.setResiAddress1(addressString);
            request.setResiAddress2("");
            request.setResiAddress3("");
        } else {
            request.setPlotNo("");
            request.setHouseNo("");
            request.setFloorNo("");
            request.setBuildingName("");
            request.setSocietyName("");
            request.setArea("");
            request.setStreet("");
            request.setSector("");
            request.setLandmark("");
            request.setCity("");
            request.setPincode("");
            request.setState("");

            request.setResiAddress1("");
            request.setResiAddress2("");
            request.setResiAddress3("");
        }

    }


    private void setAssetValue(List<AssetDetails> assetList, RelianceDigitalRequest request) {
        if (assetList != null) {
            for (AssetDetails asset : assetList) {
                request.setBrand(asset.getAssetMake());
                request.setAssetCategory(asset.getAssetCtg());
                request.setProduct(asset.getModelNo());
                request.setProductType(asset.getAssetModelMake());
                break;
            }
        }

    }


    /**
     * To get Name String value from Name Object
     *
     * @param applicantName
     * @return
     */
    private String getName(Name applicantName) {
        String name = "";
        if (applicantName != null) {
            String temp = applicantName.getFirstName();
            if (StringUtils.isNotBlank(temp)) {
                name += temp;
            }
            temp = applicantName.getMiddleName();
            if (StringUtils.isNotBlank(temp)) {
                name += " " + temp;
            }
            temp = applicantName.getLastName();
            if (StringUtils.isNotBlank(temp)) {
                name += " " + temp;
            }
        }
        return name;
    }


    /**
     * Converting Double to String
     *
     * @param value
     * @return
     */
    private String getString(Double value) {
        String string = null;
        if (value != null) {
            string = value.toString();
        } else {
            return "0";
        }
        return string;
    }


    public RelianceDigitalRequest buildRelianceDigitalRequestDummyNew(PostIpaRequest postIpaRequest) {
        RelianceDigitalRequest rdr = new RelianceDigitalRequest();
        rdr.setMopID("68");
        rdr.setGuid("5821a4c9-5007-4367-a4a1-071a2cfa1b27");
        rdr.setDoNumber("12");
        rdr.setCustomerName("1");
        rdr.setBrand("philips");
        rdr.setAssetCategory("1");
        rdr.setAssetQuantity("1");
        rdr.setProductType("1");
        rdr.setCostOfProduct("1");
        rdr.setNetLoanAmount("1.50");
        rdr.setProcessingFee("1");
        rdr.setDoStatus("New");
        rdr.setCreatedDate("10-06-2016");
        rdr.setDealID("1");
        rdr.setSchemeName("1");
        rdr.setProduct("1");
        rdr.setTenure("1");
        rdr.setEmailId("ss@ss.com");
        rdr.setMobileNo("9999999999");
        rdr.setDealerCode("1");
        rdr.setCustomerDownPayment("1");
        rdr.setSubvention("1");
        rdr.setMfrSubvention("1");
        rdr.setOtherCharges("1");
        rdr.setNetDisbursement("1");
        rdr.setMarginMoney("1");
        rdr.setGrossLoanAmount("1");
        rdr.setAdvanceEMI("1");
        rdr.setDbdPercentage("10-10-2013");
        rdr.setNumAdvTenure("1");
        //rdr.setResiAddress1("?");
        rdr.setResiAddress2("?");
        rdr.setResiAddress3("?");
        rdr.setLandmark("?");
        rdr.setState("?");
        rdr.setCity("?");
        rdr.setPincode("?");
        rdr.setHouseNo("?");
        rdr.setFloorNo("?");
        rdr.setWingNo("?");
        rdr.setBuildingName("?");
        rdr.setSocietyName("?");
        rdr.setPlotNo("?");
        rdr.setStreet("?");
        rdr.setSector("?");
        rdr.setArea("?");
        Header header = new Header();
        header.setApplicationId("12");
        header.setApplicationSource("12");
        header.setCroId("12");
        header.setDateTime(new Date());
        header.setInstitutionId("4019");
        header.setRequestType("requestType");
        header.setSourceId("sourceId");
        rdr.setHeader(header);
        return rdr;
    }
}
