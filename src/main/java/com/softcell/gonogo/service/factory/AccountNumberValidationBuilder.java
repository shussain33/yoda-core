package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.BankingDetails;
import com.softcell.gonogo.model.imps.*;
import com.softcell.gonogo.model.request.imps.IMPSRequest;
import com.softcell.gonogo.model.request.imps.IMPSResponse;

/**
 * Created by sampat on 3/11/17.
 */

public interface AccountNumberValidationBuilder {

    /**
     *
     * @param impsRequest
     * @param impsConfig
     * @return
     */
    AccountNumberValidationRequest buildAccountNumberValidationRequest(IMPSRequest impsRequest, IMPSConfigDomain impsConfig);

    /**
     *
     * @param accountNumberValidationResponse
     * @return
     */
    IMPSResponse buildIMPSResponse(AccountNumberValidationResponse accountNumberValidationResponse);

    /**
     *
     * @param validationAttempt
     * @param validationCount
     * @return
     */
    AccountValidationAttemptResponse buildAccountValidationAttemptResponse(int validationAttempt,long validationCount);

    /**
     *
     * @param impsRequest
     * @return
     */
    AccountNumberInfo.AccountNumberInfoBuilder buildAccountNumberInfo(IMPSRequest impsRequest);

    /**
     * @param accountNumberInfo
     * @return
     */
    ValidatedBanksResponse buildValidatedBanksResponse(AccountNumberInfo accountNumberInfo);

    /**
     * @param impsRequest
     * @return
     */
    BankingDetails buildBankingDetails(IMPSRequest impsRequest);
}
