package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequestV2;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;

import java.util.List;

/**
 * @author kishorp This Interface is Use to build scoring inputs of gonogo.
 */
public interface ScoringJsonBuilder {
    /**
     * @return return Scoring input Jsons
     */
    public ScoringApplicationRequest buildScoringJSon(
            GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception;

    public ScoringApplicantRequest buildApplicantScoringJSon(
            GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception;

    public ScoringApplicantRequestV2 buildApplicantScoringJSonV2(GoNoGoCustomerApplication goNoGoCustomerApplication, ScoringApplicantRequestV2 scoringApplicantRequest) throws Exception;

    /**
     * @param goNoGoCustomerApplication
     * @return
     */
    public List<ScoringApplicationRequest> buildScoringJsonForCoApplicant(
            GoNoGoCustomerApplication goNoGoCustomerApplication);

    public ScoringApplicantRequest buildApplicantRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, String applicantId, boolean primary);

    public ScoringApplicantRequest buildApplicantScoringJSonVersion2(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception;

}
