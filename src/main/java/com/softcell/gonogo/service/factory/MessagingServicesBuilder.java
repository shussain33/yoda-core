package com.softcell.gonogo.service.factory;

import com.softcell.gonogo.model.request.smsservice.SmsRequest;

/**
 * Created by yogeshb on 24/2/17.
 */
public interface MessagingServicesBuilder {

    SmsRequest buildSmsRequest(String mobileNumber,String otp);
}
