package com.softcell.gonogo.service.factory.tvscdlosintegration.impl;

import com.softcell.constants.LosTvsActionEnums;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.tvs.CDLos;
import com.softcell.gonogo.model.los.tvs.InsertOrUpdateTvsRecordGroupThree;
import com.softcell.gonogo.model.los.tvs.PostIPA;
import com.softcell.gonogo.model.los.tvs.ProductInformation;
import com.softcell.gonogo.model.masters.SchemeMasterData;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.service.factory.tvscdlosintegration.LosTvsGroupThreeReqBuilder;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LosTvsGroupThreeReqBuilderImpl implements LosTvsGroupThreeReqBuilder {
    @Override
    public InsertOrUpdateTvsRecordGroupThree buildLosTvsGroupThreeRequest(GoNoGoCustomerApplication gonogoCustomerApplication,
                                                                          PostIpaRequest postIpaRequest, SerialNumberInfo serialInfo,
                                                                          InsurancePremiumDetails insurancePremiumDetails,
                                                                          SchemeMasterData schemeMasterData,
                                                                          ExtendedWarrantyDetails extendedWarrantyDetails) {

        Logger logger = LoggerFactory.getLogger(LosTvsGroupThreeReqBuilderImpl.class);
        PostIPA postIpa = new PostIPA();
        ProductInformation productInfo = new ProductInformation();
        CDLos cdLos = new CDLos();

        logger.debug("in populateTvsCdLos serviceIMPL");

        try {

            if (postIpaRequest != null) {
                if (postIpaRequest.getPostIPA() != null) {

                    double getAdvanceEmi = postIpaRequest.getPostIPA().getAdvanceEmi();
                    int AdvanceEmi = (int) getAdvanceEmi;
                    postIpa.setAdvemiamount(AdvanceEmi);

                    if (CollectionUtils.isNotEmpty(postIpaRequest.getPostIPA().getAssetDetails())) {
                        double initialhire = postIpaRequest.getPostIPA().getTotalAssetCost();
                        logger.debug("in populateTvsCdLos serviceIMPL initialhire::"+initialhire);
                        int price = (int) initialhire;
                        //postIpa.setAmountA(price);
                        logger.debug("in populateTvsCdLos serviceIMPL assetCost::"+postIpaRequest.getPostIPA().getAssetDetails().get(0).getPrice());
                        if(StringUtils.isNotBlank(postIpaRequest.getPostIPA().getAssetDetails().get(0).getPrice())){
                            postIpa.setAssetcost(Integer.parseInt(postIpaRequest.getPostIPA().getAssetDetails().get(0).getPrice()));
                        }
                        postIpa.setInitialhire(price);

                    }

                    postIpa.setAmountA((int)postIpaRequest.getPostIPA().getTotalRecDwnAmt());
                    logger.debug("MarginMoney serviceIMPL::"+(int)postIpaRequest.getPostIPA().getTotalRecDwnAmt());

                    postIpa.setAmountB((int)postIpaRequest.getPostIPA().getLoanDisbursalAmt());
                    logger.debug("MarginMoney serviceIMPL::"+(int)postIpaRequest.getPostIPA().getLoanDisbursalAmt());

                    postIpa.setAmountC((int)postIpaRequest.getPostIPA().getLoanAccAgrAmt());
                    logger.debug("MarginMoney serviceIMPL::"+(int)postIpaRequest.getPostIPA().getLoanAccAgrAmt());

                    double financeAmount = postIpaRequest.getPostIPA().getFinanceAmount();
                    if (financeAmount != 0) {
                        int financedAmount = (int) financeAmount;
                        postIpa.setAmountfinanced(financedAmount);
                    }

                    if (insurancePremiumDetails != null) {

                        double getInsuranceEmi = insurancePremiumDetails.getInsuranceEmi();
                        if (getInsuranceEmi != 0) {
                            int InsuranceEmi = (int) getInsuranceEmi;
                            postIpa.setCreditshieldemi(InsuranceEmi);
                        }
                        double getInsurancePremium = insurancePremiumDetails.getInsurancePremium();
                        if (getInsurancePremium != 0) {
                            int InsurancePremium = (int) getInsurancePremium;
                            postIpa.setCreditshieldupfront(InsurancePremium);
                        }
                    }


                    double getDealerSubvention = postIpaRequest.getPostIPA().getDealerSubvention();
                    if (getDealerSubvention != 0) {
                        int DealerSubvention = (int) getDealerSubvention;
                        postIpa.setDlrsubventioncalculatedamt(DealerSubvention);

                        String DealerSubventionType = Double.toString(DealerSubvention);
                        postIpa.setDlrsubventiontype(DealerSubventionType);
                    }

                    if (schemeMasterData != null) {
                        if (StringUtils.isNotBlank(schemeMasterData.getDealerSubMax())) {

                            postIpa.setDlrsubventionmasteramt(schemeMasterData.getDealerSubMax()); //
                        }

                        if (StringUtils.isNotBlank(schemeMasterData.getManuSubMax())) {

                            postIpa.setMfrsubventionmasteramt(schemeMasterData.getManuSubMax());
                        }
                        if (StringUtils.isNotBlank(schemeMasterData.getSubventionType())) {
                            postIpa.setMfrsubventiontype(schemeMasterData.getSubventionType());
                        }
                    }

                    logger.debug("ltvMax:::"+postIpaRequest.getPostIPA().getLtv());
                    postIpa.setLtvmax(Double.toString(postIpaRequest.getPostIPA().getLtv()));

                    double getDownPaymentAmt = postIpaRequest.getPostIPA().getDownPaymentAmt();
                    if (getDownPaymentAmt != 0) {
                        int DownPaymentAmt = (int) getDownPaymentAmt;
                        logger.debug("DownPaymentAmt::"+DownPaymentAmt);
                        postIpa.setDownpaymentamount(DownPaymentAmt);
                    }

                    Date downPaymentDate = postIpaRequest.getPostIPA().getDownPaymentDate();
                    if (downPaymentDate != null) {
                        String convertedDate = GngDateUtil.convertDateIntoStringDateFormat(downPaymentDate);
                        postIpa.setDownpaymentdate(convertedDate);
                    }

                    if (StringUtils.isNotBlank(postIpaRequest.getPostIPA().getDownPaymentReceiptNo())) {
                        postIpa.setDownpaymentreceiptno(
                                Integer.parseInt(postIpaRequest.getPostIPA().getDownPaymentReceiptNo()));
                    }

                    double emi = postIpaRequest.getPostIPA().getEmi();
                    if (emi != 0) {
                        int emiAmount = (int) emi;
                        logger.debug("emiAmount::"+emiAmount);
                        postIpa.setEmi(emiAmount);
                    }

                    if (postIpaRequest.getPostIPA().getAdvanceEMITenor() != 0) {
                        postIpa.setAdvemitenor(postIpaRequest.getPostIPA().getAdvanceEMITenor());
                    }

                    Date emiEndDate = postIpaRequest.getPostIPA().getEmiEndDate();
                    if (emiEndDate != null) {
                        String emiEndDt = GngDateUtil.convertDateIntoStringDateFormat(emiEndDate);
                        postIpa.setEmienddate(emiEndDt);
                        logger.debug("emiEndDt::" + emiEndDt);
                    }

                    Date emiStartDate = postIpaRequest.getPostIPA().getEmiStartDate();
                    if (emiStartDate != null) {
                        String emiStartDt = GngDateUtil.convertDateIntoStringDateFormat(emiStartDate);
                        postIpa.setEmistartdate(emiStartDt);
                        logger.debug("emiStartDt::" + emiStartDt);
                    }

                    double margineMoney = postIpaRequest.getPostIPA().getMarginMoney();
                    if (margineMoney != 0) {
                        int margineMoneyMoney = (int) margineMoney;
                        logger.debug("margineMoney dd::"+margineMoney);
                        postIpa.setMarginmoney(margineMoneyMoney);
                    }

                    double getManufSubventionMbd = postIpaRequest.getPostIPA().getManufSubventionMbd();
                    if (getManufSubventionMbd != 0) {
                        int ManufSubventionMbd = (int) getManufSubventionMbd;
                        postIpa.setMfrsubventioncalculatedamt(ManufSubventionMbd);
                        logger.debug("ManufSubventionMbd::"+ManufSubventionMbd);
                    }

                    if (StringUtils.isNotBlank(postIpaRequest.getPostIPA().getAssetDetails().get(0).getModelNo())) {
                        postIpa.setModelnumber(postIpaRequest.getPostIPA().getAssetDetails().get(0).getModelNo());
                    }

                    double netDisbursalAmount = postIpaRequest.getPostIPA().getNetDisbursalAmount();
                    if (netDisbursalAmount != 0) {
                        int netDisbursalAmt = (int) netDisbursalAmount;
                        postIpa.setNetdisbursalamount(netDisbursalAmt);
                        logger.debug("netDisbursalAmt::"+netDisbursalAmt);
                    }

                    double getOtherChargesIfAny = postIpaRequest.getPostIPA().getOtherChargesIfAny();
                    if (getOtherChargesIfAny != 0) {
                        int OtherChargesIfAny = (int) getOtherChargesIfAny;
                        postIpa.setOthercharges(OtherChargesIfAny);

                    }

                    double getProcessingFees = postIpaRequest.getPostIPA().getProcessingFees();
                    if (getProcessingFees != 0) {
                        int ProcessingFees = (int) getProcessingFees;
                        postIpa.setProcessingfee(ProcessingFees);

                    }


                    double EwEmi;
                    double InsEMI;
                    double EwDpAmt;
                    double InsDpAmt;
                    if(extendedWarrantyDetails!=null)
                    {
                        EwEmi = extendedWarrantyDetails.getRevisedTotalEmi();
                        EwDpAmt = extendedWarrantyDetails.getEwDPAmt();
                        logger.debug("getting value for EwEmi:: "+EwEmi);
                        logger.debug("getting value for EwDpAmt:: "+EwDpAmt);
                    }else
                    {
                        EwEmi = 0.0;
                        EwDpAmt = 0.0;
                        logger.debug("not getting value for EwEmi & EwDpAmt by default it is 0:: "+EwEmi);
                    }
                    if(insurancePremiumDetails!=null)
                    {
                        InsEMI = insurancePremiumDetails.getInsuranceEmi();
                        InsDpAmt = insurancePremiumDetails.getInsuranceDPAmt();
                        logger.debug("getting value for InsEMI:: "+InsEMI);
                        logger.debug("getting value for InsDpAmt:: "+InsDpAmt);
                    }else
                    {
                        InsEMI = 0.0;
                        InsDpAmt = 0.0;
                        logger.debug("not getting value for InsEMI by default it is 0:: "+InsEMI);
                        logger.debug("not getting value for InsDpAmt by default it is 0:: "+InsDpAmt);
                    }

                    double NRMLEMI =  postIpaRequest.getPostIPA().getEmi();
                    double AdvTenor = postIpaRequest.getPostIPA().getAdvanceEMITenor();

                    logger.debug("EwEMI:::"+EwEmi);
                    logger.debug("InsEMI:::"+InsEMI);
                    logger.debug("NRMLEMI:::"+NRMLEMI);
                    logger.debug("AdvTenor:::"+AdvTenor);


                    double revisedEmiCalculation = (EwEmi + InsEMI +  NRMLEMI);
                    logger.debug("revisedEmiCalculation:::"+revisedEmiCalculation);
                    logger.debug("AdvTenor:::"+AdvTenor);

                    int revisedAdvanceEmiResult = (int)(revisedEmiCalculation * AdvTenor);
                    logger.debug("revisedAdvanceEmiResult:::"+revisedAdvanceEmiResult);
                    postIpa.setRevisedadvanceemi(revisedAdvanceEmiResult);

                    double financeAmt = postIpaRequest.getPostIPA().getFinanceAmount();

                    int revisedLoanAmounts = (int) (financeAmt + EwDpAmt + InsDpAmt);
                    logger.debug("revisedLoanAmounts:::"+revisedLoanAmounts);

                    int getRevisedLoanAmounts = (int) revisedLoanAmounts;
                    postIpa.setRevisedloanamount(getRevisedLoanAmounts);

                    double getRevisedEmi = postIpaRequest.getPostIPA().getRevisedEmi();
                    if (getRevisedEmi != 0) {
                        int RevisedEmi = (int) getRevisedEmi;
                        postIpa.setRevisedemi(RevisedEmi);

                    }
                    double getRevisedLoanAmount = postIpaRequest.getPostIPA().getRevisedLoanAmount();
                    if (getRevisedLoanAmount != 0) {
                        int RevisedLoanAmount = (int) getRevisedLoanAmount;
                        postIpa.setRevisedfinanceamount(RevisedLoanAmount);

                    }


                    double getRoi = postIpaRequest.getPostIPA().getRoi();
                    if (getRoi != 0) {
                        int Roi = (int) getRoi;
                        postIpa.setRoi(Roi);
                        logger.debug("Roi::" + Roi);
                    }

                    if (StringUtils.isNotBlank(postIpaRequest.getPostIPA().getSchemeDscr())) {
                        postIpa.setScheme(postIpaRequest.getPostIPA().getSchemeDscr());
                    }

                    if (StringUtils.isNotBlank(postIpaRequest.getPostIPA().getScheme())) {
                        postIpa.setSchemecode(postIpaRequest.getPostIPA().getScheme());
                    }
                    double getFinanceAmountForCalculation = postIpaRequest.getPostIPA().getFinanceAmount();
                    double dRoi = postIpaRequest.getPostIPA().getRoi();

                    if (schemeMasterData != null) {
                        String maxTenure = schemeMasterData.getMaxTenure();
                        if (StringUtils.isNotBlank(maxTenure)) {
                            logger.debug("dRoi::" + dRoi);
                            int maxTenureVal = Integer.parseInt(maxTenure);
                            int stockOnHireResult = (int) (getFinanceAmountForCalculation+((getFinanceAmountForCalculation * (dRoi / 100))/12
                                    * maxTenureVal));
                            logger.debug("stockOnHireResult:::" +
                                    stockOnHireResult);
                            postIpa.setStockonhire(stockOnHireResult); // calculation
                        }
                    }
                    postIpa.setTenor(postIpaRequest.getPostIPA().getTenor());

                    double getTotalRecvAmt = postIpaRequest.getPostIPA().getTotalRecvAmt();
                    int TotalRecvAmt = (int) getTotalRecvAmt;
                    postIpa.setTotalreceivable(TotalRecvAmt);

                    double getTotalRecDwnAmt = postIpaRequest.getPostIPA().getTotalRecDwnAmt();
                    int TotalRecDwnAmt = (int) getTotalRecDwnAmt;
                    postIpa.setTotalreceivabledownpay(TotalRecDwnAmt);

                    double getUmfc = postIpaRequest.getPostIPA().getUmfc();
                    int Umfc = (int) getUmfc;
                    postIpa.setUmfc(Umfc);

                    productInfo.setAssetctg(postIpaRequest.getPostIPA().getAssetDetails().get(0).getAssetCtg());// M
                    productInfo.setAssetmake(postIpaRequest.getPostIPA().getAssetDetails().get(0).getAssetMake());// M
                    productInfo.setAssetmodelmake(
                            postIpaRequest.getPostIPA().getAssetDetails().get(0).getAssetModelMake());// M
                    productInfo.setModelnumber(postIpaRequest.getPostIPA().getAssetDetails().get(0).getModelNo());// M

                    if(gonogoCustomerApplication!=null){
                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getGngRefId())) {
                            cdLos.setProspectid(gonogoCustomerApplication.getGngRefId());// M
                        }

                        cdLos.setAction(LosTvsActionEnums.I.name());// M

                        if (StringUtils.isNotBlank(gonogoCustomerApplication.getGngRefId())) {
                            cdLos.setProspectid(gonogoCustomerApplication.getGngRefId());// M
                        }
                        if (gonogoCustomerApplication.getApplicationRequest() != null) {

                            if (StringUtils
                                    .isNotBlank(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                                cdLos.setStage(gonogoCustomerApplication.getApplicationRequest().getCurrentStageId());// M
                            }
                        }

                        if(gonogoCustomerApplication.getDisbursementDetails()!=null){
                            if(StringUtils.isNotBlank(gonogoCustomerApplication.getDisbursementDetails().getDealerId())){
                                postIpa.setDisbursementuserid(gonogoCustomerApplication.getDisbursementDetails().getDealerId());
                            }
                        }
                        cdLos.setRemarks("");
                        cdLos.setVendorid("");
                    }
                }
            }
            return InsertOrUpdateTvsRecordGroupThree.builder()
                    .CDLos(cdLos)
                    .postIPA(postIpa)
                    .productInformation(productInfo)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("InsertOrUpdateTvsRecordForGroup003 serviceIMPL",e);
            return null;
        }

    }
}
