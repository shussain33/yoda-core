package com.softcell.gonogo.service.factory.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Product;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.Application;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.service.factory.ESEntityBuilder;
import com.softcell.queuemanager.search.Notification;
import com.softcell.queuemanager.search.request.IndexingRequest;
import com.softcell.utils.DataCleaner;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EsEntityBuilderImpl implements ESEntityBuilder {

    private static final Logger logger = LoggerFactory.getLogger(EsEntityBuilderImpl.class);

    @Override
    public IndexingRequest buildNotificationIndexRequest(GoNoGoCustomerApplication customerApplication) throws JsonProcessingException {

        IndexingRequest indexingRequest = new IndexingRequest();

        logger.debug(" building request for elasticsearch ");

        if(null != customerApplication){
            indexingRequest.setId(customerApplication.getGngRefId());

            Notification notification = new Notification();

            notification.setRefId(customerApplication.getGngRefId());
            notification.setStatus(customerApplication.getApplicationStatus());

            if(null != customerApplication.getDateTime()) {
                indexingRequest.setInsertDate(new DateTime(customerApplication.getDateTime()));
            }

            ApplicationRequest applicationRequest = customerApplication.getApplicationRequest();

            if(null != applicationRequest){

                Header header = applicationRequest.getHeader();

                Request request = applicationRequest.getRequest();

                notification.setStage(DataCleaner.cleanInvisible(applicationRequest.getCurrentStageId()));

                if(null != header){

                    notification.setDsaName(DataCleaner.cleanInvisible(header.getDsaId()));
                    indexingRequest.setInstitutionId(header.getInstitutionId());

                    Product product = header.getProduct();

                    if(null != product){

                        notification.setProducts(new String[]{product.name()});
                        indexingRequest.setProduct(product.name());

                    }
                }

                if(null != request){

                    Application application = request.getApplication();
                    Applicant applicant = request.getApplicant();

                    if(null != applicant){
                        notification.setLocation(DataCleaner.cleanInvisible(getAddress(applicant.getAddress())));

                        Name applicantName = applicant.getApplicantName();
                        if(null != applicantName) {
                            notification.setCustomerName(DataCleaner.cleanInvisible(
                                    GngUtils.stringToFirstMidlleLastName(
                                            Arrays.asList(applicantName.getFirstName(), applicantName.getMiddleName(), applicantName.getLastName())
                                    ))
                            );
                        }
                        notification.setMobileNumber(DataCleaner.cleanInvisible(getMobileNumber(applicant.getPhone())));
                    }

                    if(null != application){
                        notification.setLoanAmmount(application.getLoanAmount());
                    }
                }
            }

            String document =  JsonUtil.ObjectToString(notification);

            indexingRequest.setDocument(document);
            logger.debug("Record with refID [{}] indexed in elastic search",customerApplication.getGngRefId());
        }

        return indexingRequest;
    }

    private String getMobileNumber(List<Phone> phones) {

        String result = null;
        if(!CollectionUtils.isEmpty(phones)) {
            result = phones.parallelStream()
                    .filter(phone -> Objects.nonNull(phone))
                    .map(phone -> Arrays.asList(phone.getAreaCode(), phone.getPhoneNumber()).parallelStream()
                        .filter(StringUtils::isNotBlank)
                        .collect(Collectors.joining(FieldSeparator.SPACE_STR))).collect(Collectors.joining(FieldSeparator.SPACE_STR));
        }

        return result;

    }

    private String getAddress(List<CustomerAddress> applications) {

        String str = null;

        if(!CollectionUtils.isEmpty(applications)){

            str = applications.parallelStream().filter(address -> Objects.nonNull(address)).map(address -> Arrays.asList(address.getCity(), address.getDistrict(), address.getAddressLine1()).parallelStream()
                    .filter(str1 -> StringUtils.isNotBlank(str1) || !StringUtils.isEmpty(str1))
                    .collect(Collectors.joining(FieldSeparator.SPACE_STR))).collect(Collectors.joining(FieldSeparator.SPACE_STR));
        }

        return str;
    }

}
