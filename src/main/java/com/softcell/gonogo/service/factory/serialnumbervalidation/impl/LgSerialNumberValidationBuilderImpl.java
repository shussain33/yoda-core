package com.softcell.gonogo.service.factory.serialnumbervalidation.impl;

import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialSaleConfirmationRequest;
import com.softcell.gonogo.serialnumbervalidation.lg.LgRequest;
import com.softcell.gonogo.service.factory.serialnumbervalidation.LgSerialNumberValidationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author prasenjit wadmare
 * @date 09 Jan 2018
 */

@Service
public class LgSerialNumberValidationBuilderImpl implements LgSerialNumberValidationBuilder {

    private static final Logger logger = LoggerFactory.getLogger(LgSerialNumberValidationBuilderImpl.class);


    @Override
    public LgRequest buildLgSerialRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        return LgRequest.builder()
                .vendor(Cache.getVendorByInstitution(serialSaleConfirmationRequest.getHeader().getInstitutionId()))
                .serialNumber(serialSaleConfirmationRequest.getSerialNumber())
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .schemeId(serialSaleConfirmationRequest.getScheme())
                .dealId(serialSaleConfirmationRequest.getReferenceID())
                .flag(1)
                .build();
    }

    @Override
    public LgRequest buildLgImeiRequest(SerialSaleConfirmationRequest serialSaleConfirmationRequest) {
        return LgRequest.builder()
                .vendor(Cache.getVendorByInstitution(serialSaleConfirmationRequest.getHeader().getInstitutionId()))
                .imeiNumber(serialSaleConfirmationRequest.getImeiNumber())
                .skuCode(serialSaleConfirmationRequest.getSkuCode())
                .schemeId(serialSaleConfirmationRequest.getScheme())
                .dealId(serialSaleConfirmationRequest.getReferenceID())
                .flag(1)
                .build();
    }


}
