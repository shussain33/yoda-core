package com.softcell.gonogo.service.factory.impl;

import com.softcell.gonogo.model.request.CheckApplicationStatus;
import com.softcell.gonogo.model.request.SmsRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.service.factory.SMSReqBuilder;
import org.springframework.stereotype.Service;

/**
 * Created by anupamad on 10/8/17.
 */
@Service
public class SMSReqBuilderImpl implements SMSReqBuilder {
    @Override
    public CheckApplicationStatus buildCheckApplicationStatus(Header header, String refId) {
        return CheckApplicationStatus.builder()
                .header(header)
                .gonogoRefId(refId).build();
    }

    @Override
    public SmsRequest buildSmsRequest(Header header, String mobileNum, String msgContent){
        return SmsRequest.builder()
                .header(header)
                .mobileNumber(new String[]{mobileNum})
                .messageContent(msgContent)
                .otpSms(false)
                .build();
    }
}
