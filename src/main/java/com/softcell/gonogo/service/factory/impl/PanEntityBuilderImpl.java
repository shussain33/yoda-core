package com.softcell.gonogo.service.factory.impl;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ReqResHelperConstants;
import com.softcell.constants.ValidationPattern;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.gonogo.model.core.kyc.KycHeader;
import com.softcell.gonogo.model.core.kyc.request.pan.KycRequest;
import com.softcell.gonogo.model.core.kyc.request.pan.PanDetails;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.service.factory.PanEntityBuilder;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author yogeshb
 */
@Service
public class PanEntityBuilderImpl implements PanEntityBuilder {

    private static final Logger logger = LoggerFactory.getLogger(PanEntityBuilderImpl.class);

    @Override
    public PanRequest buildPanRequest(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();

        PanRequest panRequest = PanRequest.builder()
                .header(KycHeader.builder()
                        .requestType(ReqResHelperConstants.REQUEST.name())
                        .requestTime(LocalDateTime.now().toString())
                        .applicationId(applicant.getApplicantId()).build()
                ).kycRequest(KycRequest.builder()
                        .panDetails(
                                PanDetails.builder()
                                        .panNumber(GngUtils.getKYC(GNGWorkflowConstant.PAN.toFaceValue(), applicant.getKyc(), ValidationPattern.PAN))
                                        .build()
                        ).build()
                ).gngRefId(goNoGoCustomerApplication.getGngRefId()).build();

        if(StringUtils.isNotBlank(panRequest.getKycRequest().getPanDetails().getPanNumber())){

            logger.trace("pan request created successfully with properties {} ",panRequest );

            return panRequest;

        } else {

            logger.debug(" We can't find your pan as valid so we are not able to prepare your request ");

            return null;

        }
    }

    /**
     * @param kycNumber
     * @return
     */
    private boolean isValidPan(String kycNumber) {
        Pattern pattern = Pattern.compile(ValidationPattern.PAN);

        if(StringUtils.isNotBlank(kycNumber)){
             return pattern.matcher(kycNumber.trim()).matches();
        }else{
            return false;
        }

    }

    @Override
    public List<PanRequest> build(List<CoApplicant> coapplicantList) {
        List<PanRequest> panRequestList = new ArrayList<PanRequest>();
        for (CoApplicant coApplicant : coapplicantList) {
            List<Kyc> kycList = coApplicant.getKyc();
            PanRequest panRequest = null;
            for (Kyc kyc : kycList) {
                String kycName = StringUtils.trim(kyc.getKycName());
                if (StringUtils.equalsIgnoreCase(kycName, "PAN")) {
                    panRequest = new PanRequest();
                    KycHeader header = new KycHeader();
                    header.setApplicationId(coApplicant.getApplicantId());
                    header.setRequestTime(new Date().toString());
                    header.setRequestType("REQUEST");
                    panRequest.setHeader(header);

                    KycRequest kycRequest = new KycRequest();
                    PanDetails panDetails = new PanDetails();
                    panDetails.setPanNumber(StringUtils.trim(kyc.getKycNumber()));
                    kycRequest.setPanDetails(panDetails);
                    panRequest.setKycRequest(kycRequest);
                    panRequestList.add(panRequest);
                    break;
                }
            }
        }
        return panRequestList;
    }


}
