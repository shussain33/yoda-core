package com.softcell.gonogo.service;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.ntc.NTCResponse;

/**
 * @author bhuvneshk
 *         This interface soul propose is to connect to NTC services
 *         <p/>
 *         This interface provides a method callNTC()
 */

public interface NTCService {

    /**
     * Method is use to connect to NTC service
     *
     * @return
     * @throws Exception
     */
     NTCResponse callNTC(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception;
}
