package com.softcell.gonogo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.config.KarzaConfiguration;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAPIRequest;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAuthenticationRequest;
import com.softcell.gonogo.model.core.kyc.response.karza.*;
import com.softcell.gonogo.model.core.request.ThirdPartyRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by yogesh Khandare on 7/10/18.
 */
public interface KarzaServiceCaller {

    VoterIdResponse callvoterIdKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception;

    PanResponse callPanKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception;

    DrivingLicenceResponse callDrivingLicenceKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception;

    ElectricityResponse callElectricityKarza(KarzaAuthenticationRequest karzaAuthenticationRequest, KarzaConfiguration karzaConfiguration, String refID)throws Exception;

    Map<Applicant, List<KarzaAuthenticationRequest>> buildKarzaAuthenticationRequest(ApplicationRequest applicationRequest, List<String> kycConfig, boolean isExecuteForCoApplicant) throws Exception;

    KarzaClientResponse buildKarzaResponse(String institutionId, String applicationId, String productName,
                                           Map<Applicant, List<KarzaAuthenticationRequest>> karzaAuthenticationRequestMap,
                                           List<String> kycConfig, boolean isExecuteForCoApplicant, String loggedInUser) throws Exception;

    BaseResponse getKarzaAPIVerification(KarzaAPIRequest karzaAPIRequest, HttpServletRequest httpRequest) throws Exception;

}

