package com.softcell.gonogo.service.lookup;

import com.softcell.config.AadharVersionConfiguration;
import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.config.templates.TemplateName;
import com.softcell.constants.ActionName;
import com.softcell.constants.CacheName;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.constants.aadhar.KycRequestVersion;
import com.softcell.dao.mongodb.lookup.LookupDao;
import com.softcell.dao.mongodb.lookup.LookupDaoHandler;
import com.softcell.gonogo.cache.UpdatedCache;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.configuration.InstitutionProductConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.configuration.loyaltyCard.LoyaltyCardConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;
import com.softcell.gonogo.model.masters.ApiRoleAuthorisationMaster;
import com.softcell.gonogo.model.response.core.BaseResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * Created by yogeshb on 10/5/17.
 */
@Component
public class LookupServiceHandler implements LookupService {

    private static final Logger logger = LoggerFactory.getLogger(LookupServiceHandler.class);

    @Autowired
    private LookupDao lookupDao;

    @Autowired
    private UpdatedCache updatedCache;

    public LookupServiceHandler() {
        if (null == lookupDao) {
            lookupDao = new LookupDaoHandler();
        }
        if (null == updatedCache) {
            updatedCache = new UpdatedCache();
        }
    }


    public boolean checkActionsAccess(String institutionId, String productId, ActionName actionName) {

        String lookupKey = generateKey(CacheName.ACTION.name(), institutionId, productId, actionName.name());

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);

            updatedCache.CACHE.put(lookupKey, lookupDao.actionConfigurationReferrer(institutionId, productId, actionName));

            return isActionEnabled(lookupKey, actionName);

        } else {

            logger.debug("Cache hit occur for Cache Name [{}]: for lookup key [{}]", CacheName.ACTION.name(), lookupKey);

            return isActionEnabled(lookupKey, actionName);

        }

    }


    private Boolean isActionEnabled(String lookupKey, ActionName actionName) {

        List<ActionConfiguration> actionConfigurations = (List<ActionConfiguration>) updatedCache.CACHE.get(lookupKey);

        Optional<Boolean> first = actionConfigurations.stream()
                .filter(obj -> obj.getActionName() == actionName)
                .map(obj -> obj.isEnable())
                .findFirst();

        return first.isPresent();

    }


    @Override
    public TemplateConfiguration getTemplate(String institutionId, String productId, TemplateName templateName) {

        TemplateConfiguration templateConfiguration;

        String lookupKey = generateKey(CacheName.TEMPLATE.name(), institutionId, productId, templateName.name());

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.TEMPLATE.name(), lookupKey);

            updatedCache.CACHE.put(lookupKey, lookupDao.templateConfigurationReferrer(institutionId, productId, templateName));

            templateConfiguration = getTemplateObject(lookupKey, templateName);

        } else {

            logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.TEMPLATE.name(), lookupKey);

            templateConfiguration = getTemplateObject(lookupKey, templateName);

        }

        return templateConfiguration;
    }

    private TemplateConfiguration getTemplateObject(String lookupKey, TemplateName templateName) {

        List<TemplateConfiguration> templateConfigurations = (List<TemplateConfiguration>) updatedCache.CACHE.get(lookupKey);

        return templateConfigurations.stream()
                .filter(obj -> obj.getTemplateName() == templateName)
                .map(obj -> obj)
                .findFirst().get();

    }

    @Override
    public double getLoyaltyCardPrice(String institutionId, String productId ,LoyaltyCardType loyaltyCardType) {

        LoyaltyCardConfiguration loyaltyCardConfiguration = null;

        List<LoyaltyCardConfiguration> loyaltyCardConfigurations;

        String lookupKey = generateKey(CacheName.LOYALTY_CARD.name(),loyaltyCardType.name(), institutionId, productId);

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            updatedCache.CACHE.put(lookupKey, lookupDao.loyaltyCardConfigurationReferral(institutionId, productId,loyaltyCardType));

            loyaltyCardConfigurations = (List<LoyaltyCardConfiguration>) updatedCache.CACHE.get(lookupKey);

            if (null != loyaltyCardConfigurations && !loyaltyCardConfigurations.isEmpty()) {

                loyaltyCardConfiguration = loyaltyCardConfigurations.stream().map(obj -> obj).findFirst().get();

                logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.LOYALTY_CARD.name(), lookupKey);

            }
        } else {

            loyaltyCardConfigurations = (List<LoyaltyCardConfiguration>) updatedCache.CACHE.get(lookupKey);

            if (null != loyaltyCardConfigurations && !loyaltyCardConfigurations.isEmpty()) {

                loyaltyCardConfiguration = loyaltyCardConfigurations.stream().map(obj -> obj).findFirst().get();

                logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.LOYALTY_CARD.name(), lookupKey);

            }
        }

        if (null != loyaltyCardConfiguration) {

            return loyaltyCardConfiguration.getLoyaltyCardPrice();

        } else {
            return 0;
        }

    }

    @Override
    public EmailConfiguration getEmailConfiguration(String institutionId, String productId) {

        EmailConfiguration emailConfiguration = null;

        List<EmailConfiguration> emailConfigurations;

        String lookupKey = generateKey(CacheName.EMAIL.name(), institutionId, productId);

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            updatedCache.CACHE.put(lookupKey, lookupDao.emailConfigurationReferrer(institutionId, productId));

            emailConfigurations = (List<EmailConfiguration>) updatedCache.CACHE.get(lookupKey);

            if (null != emailConfigurations && !emailConfigurations.isEmpty()) {

                emailConfiguration = emailConfigurations.stream().map(obj -> obj).findFirst().get();

                logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.EMAIL.name(), lookupKey);

            }
        } else {

            emailConfigurations = (List<EmailConfiguration>) updatedCache.CACHE.get(lookupKey);

            if (null != emailConfigurations && !emailConfigurations.isEmpty()) {

                emailConfiguration = emailConfigurations.stream().map(obj -> obj).findFirst().get();

                logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.EMAIL.name(), lookupKey);

            }
        }

        return emailConfiguration;
    }


    @Override
    public Set<String> getApplicableVendors(String institutionId, String productId) {

        Set<String> vendors;

        String lookupKey = generateKey(CacheName.APPLICABLE_VENDOR.name(), institutionId, productId);

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.APPLICABLE_VENDOR.name(), lookupKey);

            updatedCache.CACHE.put(lookupKey, lookupDao.applicableVendorsConfigurationReferrer(institutionId, productId));

            vendors = (Set<String>) updatedCache.CACHE.get(lookupKey);

        } else {

            logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.APPLICABLE_VENDOR.name(), lookupKey);

            vendors = (Set<String>) updatedCache.CACHE.get(lookupKey);
        }

        return vendors;
    }

    @Override
    public String getAliasNameByInstitutionIdAndProductName(String institutionId, String productName) {

        String alias = null;

        String lookupKey = generateKey(CacheName.ALIAS_NAME.name(), institutionId, productName);

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            List<InstitutionProductConfiguration> institutionProductConfigurations = lookupDao.aliasNameByProductNameAndInstitutionIdReferrer(institutionId, productName);

            if (null != institutionProductConfigurations && !institutionProductConfigurations.isEmpty()) {

                updatedCache.CACHE.put(lookupKey, institutionProductConfigurations);

                alias = ((List<InstitutionProductConfiguration>) updatedCache.CACHE.get(lookupKey)).stream().map(obj -> obj.getAliasName()).findFirst().get();

                logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ALIAS_NAME.name(), lookupKey);

            }

        } else {

            logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.ALIAS_NAME.name(), lookupKey);

            List<InstitutionProductConfiguration> institutionProductConfigurations = (List<InstitutionProductConfiguration>) updatedCache.CACHE.get(lookupKey);

            alias = institutionProductConfigurations.stream().map(obj -> obj.getAliasName()).findFirst().get();

        }

        return alias;
    }

    @Override
    public SmsTemplateConfiguration getSmsTemplateConfiguration(String institutionId, String productId, String smsType) {
        SmsTemplateConfiguration smsTemplateConfiguration = null;
        List<SmsTemplateConfiguration> smsTemplateConfigurations;
        String lookupKey = generateKey(CacheName.SMS_TEMPLATE.name(), institutionId, productId, smsType);

        if (!updatedCache.CACHE.containsKey(lookupKey)) {
            updatedCache.CACHE.put(lookupKey, lookupDao.getSmsTemplateConfiguration(
                    SmsTemplateConfiguration.builder().institutionId(institutionId).productId(productId).smsType(smsType).build()));
            smsTemplateConfiguration = getSmsTemplateConf(lookupKey);
            logger.warn("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.SMS_TEMPLATE.name(), lookupKey);
        } else {
            smsTemplateConfiguration = getSmsTemplateConf(lookupKey);
            logger.warn("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.SMS_TEMPLATE.name(), lookupKey);
        }
        return smsTemplateConfiguration;
    }

    private SmsTemplateConfiguration  getSmsTemplateConf(String lookupKey) {
        List <SmsTemplateConfiguration> smsTemplateConfigurations =
                (List<SmsTemplateConfiguration>) updatedCache.CACHE.get(lookupKey);
            if (!CollectionUtils.isEmpty(smsTemplateConfigurations)) {
                return smsTemplateConfigurations.stream().findFirst().get();
            }
        return null;
    }

    private String generateKey(String... args) {
        return StringUtils.join(args);
    }


    @Override
    public List<DmsFolderConfiguration> getDmsIndexInformation(String institutionId) {

        String lookupKey = generateKey(CacheName.DMS_FOLDER_CONFIG.name(), institutionId);

        List<DmsFolderConfiguration> dmsFolderConfigurations = null;

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            updatedCache.CACHE.put(lookupKey, lookupDao.getDmsFolderInformation(institutionId));

            if (null != updatedCache.CACHE.get(lookupKey)) {

                dmsFolderConfigurations = (List<DmsFolderConfiguration>) updatedCache.CACHE.get(lookupKey);
            }

        } else {

            dmsFolderConfigurations = (List<DmsFolderConfiguration>) updatedCache.CACHE.get(lookupKey);
        }

        return dmsFolderConfigurations;

    }

    @Override
    public KycRequestVersion getAadharVersionConfiguration(String institutionId, String kycRequestType) {

        AadharVersionConfiguration aadharVersionConfiguration = lookupDao.getAadharVersionConfiguration(institutionId, kycRequestType);

        if (null != aadharVersionConfiguration) {
            return aadharVersionConfiguration.getKycReqVersion();
        } else {
            return null;
        }
    }

    @Override
    public CaseCancellationJobConfig getCaseCancellationJobConfig(String institutionId, String caseCancelType) {

        String lookupKey = generateKey(caseCancelType, institutionId);

        CaseCancellationJobConfig caseCancellationJobConfig = null;

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            CaseCancellationJobConfig caseCancellationJobConfig1 = lookupDao.getCaseCancellationJobConfig(institutionId);
            if (null != caseCancellationJobConfig1) {

                updatedCache.CACHE.put(lookupKey, caseCancellationJobConfig1);

                if (null != updatedCache.CACHE.get(lookupKey)) {

                    caseCancellationJobConfig = (CaseCancellationJobConfig) updatedCache.CACHE.get(lookupKey);
                }
            }


        } else {

            caseCancellationJobConfig = (CaseCancellationJobConfig) updatedCache.CACHE.get(lookupKey);
        }

        return caseCancellationJobConfig;
    }


    @Override
    public IMPSConfigDomain getIMPSDomainConfig(String institutionId) {


        BaseResponse baseResponse;

        String lookupKey = StringUtils.join(CacheName.IMPS.name(), institutionId);

        IMPSConfigDomain impsConfiguration = null;

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            impsConfiguration = lookupDao.getImpsConfiguration(institutionId);
            if (null != impsConfiguration) {

                updatedCache.CACHE.put(lookupKey, impsConfiguration);

                if (null != updatedCache.CACHE.get(lookupKey)) {

                    impsConfiguration = (IMPSConfigDomain) updatedCache.CACHE.get(lookupKey);
                }
            }


        } else {

            impsConfiguration = (IMPSConfigDomain) updatedCache.CACHE.get(lookupKey);
        }

        return impsConfiguration;
    }

    @Override
    public TemplateConfiguration getTemplateById(String institutionId, String productId, TemplateName templateName, String templateId) {
        TemplateConfiguration templateConfiguration;

        String lookupKey = generateKey(CacheName.TEMPLATE.name(), institutionId, productId, templateName.name(), templateId);

        if (!updatedCache.CACHE.containsKey(lookupKey)) {

            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.TEMPLATE.name(), lookupKey);

            updatedCache.CACHE.put(lookupKey, lookupDao.templateConfigurationReferrerById(institutionId, productId, templateName, templateId));

            templateConfiguration = getTemplateObjectById(lookupKey, templateName, templateId);

        } else {

            logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.TEMPLATE.name(), lookupKey);

            templateConfiguration = getTemplateObjectById(lookupKey, templateName, templateId);

        }

        return templateConfiguration;
    }

    /** TO get the configuration values from LookupMaster.
     * for e.g MB_PULL_ON_DAYS_BASIS = 30 Days.
     *  */
    public String getMasterValue(String institutionId, ActionName actionName) {
        String value = null;
        String lookupKey = generateKey(CacheName.ACTION.name(), institutionId, actionName.name());
        try {
            if (!updatedCache.CACHE.containsKey(lookupKey)) {
                logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
                value = lookupDao.getLookupMasterValue(institutionId, actionName);
                if(null != value )
                    updatedCache.CACHE.put(lookupKey,value);
            } else {

                if (null != updatedCache.CACHE.get(lookupKey))
                    value = (String) updatedCache.CACHE.get(lookupKey);

                logger.debug("Cache hit occur for Cache Name [{}]: for lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            }
        }catch(Exception e){
            logger.error("Error occurred while fetching Master Value Exception {} ",  ExceptionUtils.getStackTrace(e));
        }
        return value;
    }

    private TemplateConfiguration getTemplateObjectById(String lookupKey, TemplateName templateName, String templateId) {
        TemplateConfiguration templateConfiguration = null;
        List<TemplateConfiguration> templateConfigurations = (List<TemplateConfiguration>) updatedCache.CACHE.get(lookupKey);
        for (TemplateConfiguration temp : templateConfigurations) {
            if(StringUtils.equalsIgnoreCase(temp.getTemplateName().name(),templateName.name())
                    && StringUtils.equalsIgnoreCase(temp.getTemplateId(),templateId)){
               templateConfiguration = temp;
               logger.info("returning templateConfiguration templateConfiguration{}", templateConfiguration);
               return templateConfiguration;
            }

        }

        return null;
    }

    @Override
    public ActionConfiguration getActionConfiguration(String institutionId, String productId, ActionName actionName) {
        logger.info("Fetching Action Configuration for InstitutionId {}, ProductID {}, ActionName {}",
                institutionId, productId, actionName);

        ActionConfiguration actionConfiguration = null;
        List<ActionConfiguration> actionConfigurationList = new ArrayList<>();
        String lookupKey = generateKey(CacheName.ACTION.name(), institutionId, productId, actionName.name());
        if(!updatedCache.CACHE.containsKey(lookupKey)) {
            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);

            actionConfiguration = lookupDao.getActionConfigurationReferrer(institutionId, productId, actionName);
            if(actionConfiguration != null) {
                actionConfigurationList.add(actionConfiguration);
                updatedCache.CACHE.put(lookupKey, actionConfigurationList);
            }
        } else {
            logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            List<ActionConfiguration> actionConfigurations = (List<ActionConfiguration>) updatedCache.CACHE.get(lookupKey);

            if(!CollectionUtils.isEmpty(actionConfigurations)) {
                actionConfiguration = actionConfigurations.get(0);
            }
        }


        return actionConfiguration;
    }

    @Override
    public ActionConfiguration getActionConfigurations(String institutionId, String sourceId,ActionName actionName) {

        ActionConfiguration actionConfiguration = null;
        List<ActionConfiguration> actionConfigurationList = new ArrayList<>();
        String lookupKey = generateKey(CacheName.ACTION.name(), institutionId, sourceId,actionName.name());
        if(!updatedCache.CACHE.containsKey(lookupKey)) {
            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);

            actionConfiguration = lookupDao.getActionsConfigurationReferrer(institutionId, sourceId,actionName);
            if(actionConfiguration != null) {
                actionConfigurationList.add(actionConfiguration);
                updatedCache.CACHE.put(lookupKey, actionConfigurationList);
            }
        } else {
            logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            List<ActionConfiguration> actionConfigurations = (List<ActionConfiguration>) updatedCache.CACHE.get(lookupKey);

            if(!CollectionUtils.isEmpty(actionConfigurations)) {
                actionConfiguration = actionConfigurations.get(0);
            }
        }

        return actionConfiguration;
    }

    @Override
    public boolean checkActionsAccess(String institutionId, ActionName actionName) {
        logger.info("Check Action Access started for Institution Id {}",institutionId);
        String lookupKey = generateKey(CacheName.ACTION.name(), institutionId, actionName.name());
        if (!updatedCache.CACHE.containsKey(lookupKey)) {
            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            updatedCache.CACHE.put(lookupKey, lookupDao.actionConfigurationReferrer(institutionId, actionName));
            return isActionEnabled(lookupKey, actionName);
        } else {
            logger.debug("Cache hit occur for Cache Name [{}]: for lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            return isActionEnabled(lookupKey, actionName);
        }
    }

    @Override
    public boolean checkActionAccess(String institutionId, String sourceId, ActionName actionName) {
        logger.warn("Check Action Access started for sourceId {} and institutionId:{}",sourceId,institutionId);
        String lookupKey = generateKey(CacheName.ACTION.name(), institutionId, sourceId, actionName.name());
        if (!updatedCache.CACHE.containsKey(lookupKey)) {
            logger.warn("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            updatedCache.CACHE.put(lookupKey, lookupDao.actionsConfigurationReferrer(institutionId, sourceId, actionName));
            return isActionEnabled(lookupKey, actionName);
        } else {
            logger.warn("Cache hit occur for Cache Name [{}]: for lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            return isActionEnabled(lookupKey, actionName);
        }
    }

    @Override
    public boolean checkApiRoleAccess(String institutionId, String sourceId,String apiName, String role,String methodType) {
        String lookupKey = generateKey(CacheName.API_ROLE_AUTH.name(),institutionId,sourceId,apiName);
        if (!updatedCache.CACHE.containsKey(lookupKey)){
            if(lookupDao.apiRoleAccessCheck(institutionId, sourceId,apiName,role) != null) {
                updatedCache.CACHE.put(lookupKey, lookupDao.apiRoleAccessCheck(institutionId, sourceId,apiName, role));
            }else{
                logger.info("Role Authorization skipped for institutionId {} for api {}:",institutionId,apiName);
                return true;
            }
        }
        return isApiRoleAccessEnabled(lookupKey,role,methodType);
    }

    private Boolean isApiRoleAccessEnabled(String lookupKey,String role,String methodType) {
        ApiRoleAuthorisationMaster apiRoleAuthorisation = (ApiRoleAuthorisationMaster) updatedCache.CACHE.get(lookupKey);
        JSONObject apiRole =new JSONObject(apiRoleAuthorisation);
        List<String> rolesList = Arrays.asList(apiRole.get("rolesList").toString().replaceAll(FieldSeparator.BACKWARD_SLASH_DOUBLE_QUOTE,FieldSeparator.BLANK)
                .replace(FieldSeparator.OPENING_SQUARE_BRACKET,FieldSeparator.BLANK)
                .replace(FieldSeparator.CLOSING_SQUARE_BRACKET,FieldSeparator.BLANK).split(","));
        if(apiRole != null && role != null && rolesList.contains(role) && StringUtils.equalsIgnoreCase(apiRoleAuthorisation.getMethodType(),methodType)){
            return true;
        }

        return false;
    }

    public ActionConfiguration getActionConfig(String institutionId, ActionName actionName) {
        ActionConfiguration actionConfiguration = null;
        List<ActionConfiguration> actionConfigurationList;
        String lookupKey = generateKey(CacheName.ACTION.name(), institutionId, actionName.name());
        if(!updatedCache.CACHE.containsKey(lookupKey)) {
            logger.debug("Cache miss occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            actionConfigurationList = lookupDao.actionConfigurationReferrer(institutionId,actionName);
            if(actionConfigurationList != null && !CollectionUtils.isEmpty(actionConfigurationList)) {
                updatedCache.CACHE.put(lookupKey, actionConfigurationList);
                actionConfiguration = actionConfigurationList.get(0);
            }
        } else {
            logger.debug("Cache hit occur for Cache Name: [{}] lookup key [{}]", CacheName.ACTION.name(), lookupKey);
            List<ActionConfiguration> actionConfigurations = (List<ActionConfiguration>) updatedCache.CACHE.get(lookupKey);
            if(!CollectionUtils.isEmpty(actionConfigurations)) {
                actionConfiguration = actionConfigurations.get(0);
            }
        }
        return actionConfiguration;
    }

}
