package com.softcell.gonogo.service.lookup;

import com.softcell.config.email.EmailConfiguration;
import com.softcell.config.templates.TemplateConfiguration;
import com.softcell.config.templates.TemplateName;
import com.softcell.constants.ActionName;
import com.softcell.constants.LoyaltyCardType;
import com.softcell.constants.aadhar.KycRequestVersion;
import com.softcell.gonogo.model.configuration.ActionConfiguration;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.casecancel.CaseCancellationJobConfig;
import com.softcell.gonogo.model.configuration.dms.DmsFolderConfiguration;
import com.softcell.gonogo.model.imps.IMPSConfigDomain;

import java.util.List;
import java.util.Set;

/**
 * Created by yogeshb on 10/5/17.
 */
public interface LookupService {

    /**
     *
     * @param institutionId
     * @param productId
     * @param actionName
     * @return
     */
    boolean checkActionsAccess(String institutionId, String productId , ActionName actionName);

    /**
     *
     * @param institutionId
     * @param productId
     * @param templateName
     * @return
     */
    TemplateConfiguration getTemplate(String institutionId, String productId, TemplateName templateName);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    EmailConfiguration getEmailConfiguration(String institutionId, String productId);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    Set<String> getApplicableVendors(String institutionId, String productId);

    /**
     *
     * @param institutionId
     * @param productName
     * @return
     */
    String getAliasNameByInstitutionIdAndProductName(String institutionId, String productName);

    /**
     *
     * @param institutionId
     * @param productId
     * @return
     */
    double getLoyaltyCardPrice(String institutionId ,String productId ,LoyaltyCardType loyaltyCardType);

    /**

     * Get Sms Template for institute and product
     * @param institutionId
     * @param productId
     * @return
     */
    SmsTemplateConfiguration getSmsTemplateConfiguration(String institutionId, String productId, String smsType);

    /**
     *
     * @param institutionId
     * @return
     */
    List<DmsFolderConfiguration> getDmsIndexInformation(String institutionId);

    /**
     *
     * @param institutionId
     * @param kycRequestType
     * @return
     */
    KycRequestVersion getAadharVersionConfiguration(String institutionId, String kycRequestType);

    /**
     *
     * @param institutionId
     * @return
     */
    CaseCancellationJobConfig getCaseCancellationJobConfig(String institutionId ,String caseCancelType);

    IMPSConfigDomain getIMPSDomainConfig(String institutionId);

    TemplateConfiguration getTemplateById(String institutionId, String productId, TemplateName sanctionLetter, String templateId);

    String getMasterValue(String institutionId, ActionName actionName);

    ActionConfiguration getActionConfiguration(String institutionId, String productId, ActionName actionName);

    ActionConfiguration getActionConfigurations(String institutionId, String sourceId,ActionName actionName);

    boolean checkActionsAccess(String institutionId, ActionName actionName);

    boolean checkActionAccess(String institutionId, String sourceId , ActionName actionName);

    boolean checkApiRoleAccess(String institutionId,String sourceId,String apiName,String role,String methodType);

    ActionConfiguration getActionConfig(String institutionId,ActionName actionName);
}
