package com.softcell.gonogo.service;

import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequestNew;
import com.softcell.gonogo.model.core.kyc.response.aadhar.AadharMainResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;

/**
 * @author yogeshb
 */
public interface AadharServiceCaller {
    /**
     * @param aadhaarRequest
     * @param institutionId
     * @return
     */
    public AadharMainResponse callAadharOtpService(AadhaarRequest aadhaarRequest, String institutionId) throws Exception;

    /**
     * @param aadhaarRequest
     * @param institutionId
     * @return
     */
    public AadharMainResponse callAadharEkycService(AadhaarRequest aadhaarRequest, String institutionId) throws Exception;

    /**
     * @param aadhaarRequest
     * @param institutionId
     * @return
     */
    public AadharMainResponse callAadharEkycServiceV2_1(AadhaarRequest aadhaarRequest, String institutionId) throws Exception;

    public BaseResponse callAadharEkycServiceV2_1_1(AadhaarRequest aadhaarRequest, String institutionId) throws Exception;


    public AadharMainResponse callAadharEkycServiceV2_2_NEW(AadhaarRequest aadhaarRequest, String institutionId) throws Exception;

    AadharMainResponse callAadharOtpServiceV2_1(AadhaarRequest aadhaarRequest, String institutionId) throws Exception;

    AadharMainResponse callAadharBiometricService(AadhaarRequest aadhaarRequest, String institutionId) throws Exception;

    public AadharMainResponse callAadharEkycServiceV2_2(AadhaarRequestNew aadhaarRequestNew, String institutionId) throws Exception;

}
