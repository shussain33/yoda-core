package com.softcell.gonogo.service;

import com.softcell.gonogo.model.request.smsservice.SmsServiceBaseRequest;
import com.softcell.gonogo.model.response.smsservice.SmsServiceBaseResponse;

import java.io.IOException;

/**
 * @author vinodk
 */
public interface SmsServiceCaller {

    /**
     * @param smsServiceBaseRequest
     * @return
     */
    public SmsServiceBaseResponse callSmsService(
            SmsServiceBaseRequest smsServiceBaseRequest, String smsServiceUrl) throws IOException;

}
