package com.softcell.gonogo.service;

import com.softcell.config.CreditCardSurrogateConfig;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateRequest;
import com.softcell.gonogo.model.surrogate.creditcard.CreditCardSurrogateResponse;

public interface CreditCardSurrogateCaller {

    CreditCardSurrogateResponse callToCreditCardSurrogateApi(
            CreditCardSurrogateRequest creditCardSurrogateRequest, CreditCardSurrogateConfig creditCardSurrogateConfig);

}
