package com.softcell.gonogo.service;


import com.softcell.gonogo.exceptions.TcLosException;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.TCLosData;

import java.util.List;
/**
 * Created by Softcell on 21/09/17.
 */
public interface TCLosManager {
    void buildTCLosData(GoNoGoCroApplicationResponse goNoGoCroApplicationResponse
            , boolean generateCSV
            , String path
            , String branchCodeVal
    ) throws TcLosException;

}
