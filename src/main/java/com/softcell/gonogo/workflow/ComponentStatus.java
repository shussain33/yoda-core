/**
 * kishorp11:34:22 PM  Copyright Softcell Technolgy
 **/
package com.softcell.gonogo.workflow;

/**
 * @author kishorp
 *
 */
public class ComponentStatus {
    private boolean dedupe;
    private boolean pan;
    private boolean aadhar;
    private boolean variScoring;
    private boolean scoring;
    private boolean mb;
    private int flowId;

    public boolean isDedupe() {
        return dedupe;
    }

    public void setDedupe(boolean dedupe) {
        this.dedupe = dedupe;
    }

    public boolean isPan() {
        return pan;
    }

    public void setPan(boolean pan) {
        this.pan = pan;
    }

    public boolean isAadhar() {
        return aadhar;
    }

    public void setAadhar(boolean aadhar) {
        this.aadhar = aadhar;
    }

    public boolean isVariScoring() {
        return variScoring;
    }

    public void setVariScoring(boolean variScoring) {
        this.variScoring = variScoring;
    }

    public boolean isScoring() {
        return scoring;
    }

    public void setScoring(boolean scoring) {
        this.scoring = scoring;
    }

    public boolean isMb() {
        return mb;
    }

    public void setMb(boolean mb) {
        this.mb = mb;
    }

    public int getFlowId() {
        return flowId;
    }

    public void setFlowId(int flowId) {
        this.flowId = flowId;
    }

}
