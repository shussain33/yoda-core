package com.softcell.gonogo.workflow.actions;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.workflow.component.Component;

public interface ComponentActionInterface extends Runnable {
    /**
     * @param goNoGoCustomerApplication
     * @param component
     * @return
     */
    String process(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            Component component);

    /**
     * @return the goNoGoCustomerApplication
     */
    GoNoGoCustomerApplication getGoNoGoCustomerApplication();

    /**
     * @param goNoGoCustomerApplication the goNoGoCustomerApplication to set
     */
    void setGoNoGoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication);

    /**
     * @return the component
     */
    Component getComponent();

    /**
     * @param component
     */
    void setComponent(Component component);
}

