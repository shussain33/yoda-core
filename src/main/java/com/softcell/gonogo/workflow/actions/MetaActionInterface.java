package com.softcell.gonogo.workflow.actions;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.workflow.component.module.ModuleSetting;

public interface MetaActionInterface extends Runnable {

    String process(Object goNoGoCustomerApplication, ModuleSetting moduleSetting);

    /**
     * @return status of process
     */
    String finishProcess();

    /**
     * @param bean
     * @return
     */
    String process(Object bean);

    /**
     * @return the goNoGoCustomerApplication
     */
    public GoNoGoCustomerApplication getGoNoGoCustomerApplication();

    /**
     * @param goNoGoCustomerApplication the goNoGoCustomerApplication to set
     */
    public void setGoNoGoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication);

    /**
     * @return the component
     */
    public ModuleSetting getModuleSetting();

    public void setModuleSetting(ModuleSetting moduleSetting);
}
