/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 2, 2016 4:53:34 PM
 */
package com.softcell.gonogo.workflow.actions;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.executors.sobre.ScoringExecutor;
import com.softcell.workflow.processors.sobre.ScoringProcessor;

/**
 * @author kishorp
 *         <p/>
 *         <pre>
 *         A <em> ComponentAction</em>  is abstract class which  will execute all workflow process to execute the modules of each component.
 *         </pre>
 * @see com.softcell.workflow.component.finished.FinalGonoGoProcessor
 * @see ScoringExecutor
 * {@link ScoringProcessor}
 */
public abstract class ComponentAction implements ComponentActionInterface {

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private Component component;

    /**
     * @param goNoGoCustomerApplication
     * @param component
     * @return
     */
    public abstract String process(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            Component component);

    /**
     * @return the goNoGoCustomerApplication
     */
    public GoNoGoCustomerApplication getGoNoGoCustomerApplication() {
        return goNoGoCustomerApplication;
    }

    /**
     * @param goNoGoCustomerApplication the goNoGoCustomerApplication to set
     */
    public void setGoNoGoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
    }

    /**
     * @return the component
     */
    public Component getComponent() {
        return component;
    }

    /**
     * @param component the component to set
     */
    public void setComponent(Component component) {
        this.component = component;
    }


}
