package com.softcell.gonogo.workflow.actions;

import com.softcell.constants.Constant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.service.AppConfigurationHelper;
import com.softcell.workflow.component.ComponentCondition;
import com.softcell.workflow.component.Condition;
import com.softcell.workflow.component.manager.ComponentManager;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.collections.CollectionUtils;
import org.stringtemplate.v4.ST;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kishorp
 *         This class is used to  define MetaAction  of each work flow action runtime.
 */
public abstract class MetaAction implements MetaActionInterface {

    protected GoNoGoCustomerApplication goNoGoCustomerApplication;

    protected final static  String APPLICANT = "APPLICANT";

    protected final static  String APPLICATION = "APPLICATION";

    protected String policyName = Constant.DEFAULT;

    protected boolean executeForCoApplicant;

    protected String executionBase = APPLICATION;

    private ModuleSetting moduleSetting;

    protected ComponentManager componentManager;

    protected String loggedInUserRole;

    protected int actionId;

    protected List<String> kycConfig;

    public String getLoggedInUserRole() {
        return loggedInUserRole;
    }

    public void setLoggedInUserRole(String loggedInUserRole) {
        this.loggedInUserRole = loggedInUserRole;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int id) {
        this.actionId = id;
    }

    public void setComponentManager(ComponentManager componentManager){
        this.componentManager = componentManager;
        this.goNoGoCustomerApplication = componentManager.getGoNoGoCustomerApplication();
    }

    public ComponentManager getComponentManager(){
        return this.componentManager;
    }

    /**
     * This method will take care of all bean to do their works.
     * Each child class of MetaAction should implement their own process.
     */
    public abstract String process(Object goNoGoCustomerApplication, ModuleSetting moduleSetting);

    public abstract void run();

    /**
     * @return status of process
     */
    public abstract String finishProcess();


    /**
     *
     * @param bean
     * @return
     */
    public String process(Object bean) {
        return null;
    }

    /**
     * @return the goNoGoCustomerApplication
     */
    public GoNoGoCustomerApplication getGoNoGoCustomerApplication() {
        return goNoGoCustomerApplication;
    }

    /**
     * @param goNoGoCustomerApplication the goNoGoCustomerApplication to set
     */
    public void setGoNoGoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
    }

    /**
     * @return the component
     */
    public ModuleSetting getModuleSetting() {
        return moduleSetting;
    }

    /**
     *
     * @param moduleSetting
     */
    public void setModuleSetting(ModuleSetting moduleSetting) {
        this.moduleSetting = moduleSetting;
    }

    public void setExecuteForCoApplicant(boolean executeForCoApplicant) {
        this.executeForCoApplicant = executeForCoApplicant;
    }

    public boolean isExecuteForCoApplicant() {
        return executeForCoApplicant;
    }

    public static String getAPPLICANT() {
        return APPLICANT;
    }

    public static String getAPPLICATION() {
        return APPLICATION;
    }

    public String getExecutionBase() {
        return executionBase;
    }

    public void setExecutionBase(String executionBase) {
        this.executionBase = executionBase;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public List<String> getKycConfig() {
        return kycConfig;
    }

    public void setKycConfig(List<String> kycConfig) {
        this.kycConfig = kycConfig;
    }

    public boolean checkConditions(ComponentCondition componentCondition){
        boolean result = false;
        List<Boolean> bValues = new ArrayList();
        try {
            if( null != componentCondition) {
                if(CollectionUtils.isNotEmpty(componentCondition.getComponentConditions())){
                    for(ComponentCondition componentCondition1:componentCondition.getComponentConditions()){
                        bValues.add(checkConditions(componentCondition1));
                    }
                }
                if(CollectionUtils.isNotEmpty(componentCondition.getConditions())) {
                    for (Condition condition : componentCondition.getConditions()) {
                        String className = condition.getClassName();
                        String key = condition.getKey();
                        String operator = condition.getOperator();
                        String value = condition.getValue().equalsIgnoreCase("null") ? null : condition.getValue();

                        String objectValue = AppConfigurationHelper.getRequestStringValue(goNoGoCustomerApplication, key, Class.forName(className));
                        String keyType = AppConfigurationHelper.getFieldType(key, Class.forName(className));
                        condition.setEvalValue(evaluateOperations(operator, objectValue, value, keyType));
                        bValues.add(condition.isEvalValue());
                    }
                }
                String conditionalOperator = componentCondition.getConditionalOperator();

                componentCondition.setResult(setConditionalResult(conditionalOperator, bValues));
                if(componentCondition.isResult()) result = true;
            }else
                result = true;// default result set to true
        }catch(Exception e){
            result = true; // default result set to true
            // logger.debug("Exceptions while execution conditions {} ", this.componentCondition.toString());
        }
        return result;
    }

    private boolean setConditionalResult(String operator, List<Boolean> values){
        String ocase = operator.toUpperCase();
        boolean result = true;
        switch(ocase){
            case "AND":
                for(boolean res: values){
                    if(!res) {
                        result = false;
                        break;
                    }
                }
                break;
            case "OR":
                for(boolean res: values){
                    if(res) {
                        result = true;
                        break;
                    }
                }
                break;
        }
        return  result;
    }

    private boolean evaluateOperations(String operator, String dbValue, String specifiedValue, String type){
        String ocase = operator.toUpperCase();
        Boolean result;
        switch(ocase){
            case ("EQUALS") :
            case ("==") :{
                if (type.equals("int"))
                    result = (Integer.valueOf(dbValue) == Integer.valueOf(specifiedValue));
                else if (type.equals("double"))
                    result = (Double.valueOf(dbValue).doubleValue() == Double.valueOf(specifiedValue).doubleValue());
                else
                    result = dbValue.equals(specifiedValue);

                break;
            }
            case "NOT-EQUALS" :
            case ("!=") :{
                if (type.equals("int"))
                    result = (Integer.valueOf(dbValue) != Integer.valueOf(specifiedValue));
                else if (type.equals("double"))
                    result = (Double.valueOf(dbValue).doubleValue() != Double.valueOf(specifiedValue).doubleValue());
                else
                    result = !dbValue.equals(specifiedValue);

                break;
            }
            case "EQUALS-IGNORE-CASE" :
            case ("EQUALSIGNORECASE") : {
                result = dbValue.equalsIgnoreCase(specifiedValue);
                break;
            }
            case "NOT-EQUALS-IGNORE-CASE" :
            case ("NOTEQUALSIGNORECASE") :{
                result = !dbValue.equalsIgnoreCase(specifiedValue);
                break;
            }
            case "GREATER-THAN":
            case (">") :{
                if (type.equals("double"))
                    result = (Double.valueOf(dbValue).doubleValue() > Double.valueOf(specifiedValue).doubleValue());
                else
                    result = (Integer.valueOf(dbValue) > Integer.valueOf(specifiedValue));

                break;
            }
            case "LESS-THAN":
            case ("<") :{
                if (type.equals("double"))
                    result = (Double.valueOf(dbValue).doubleValue() < Double.valueOf(specifiedValue).doubleValue());
                else
                    result = (Integer.valueOf(dbValue) < Integer.valueOf(specifiedValue));

                break;
            }
            case "GREATER-THAN-EQUALS":
            case (">=") :{
                if (type.equals("double"))
                    result = (Double.valueOf(dbValue).doubleValue() >= Double.valueOf(specifiedValue).doubleValue());
                else
                    result = (Integer.valueOf(dbValue) >= Integer.valueOf(specifiedValue));

                break;
            }
            case "LESS-THAN-EQUALS":
            case ("<=") :{
                if (type.equals("double"))
                    result = (Double.valueOf(dbValue).doubleValue() <= Double.valueOf(specifiedValue).doubleValue());
                else
                    result = (Integer.valueOf(dbValue) <= Integer.valueOf(specifiedValue));

                break;
            }
            default: {
                result = true;
            }
        }
        return result;
    }
}
