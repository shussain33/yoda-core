package com.softcell.gonogo;

import com.softcell.config.MultiBreComponentConfiguration;
import com.softcell.constants.*;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.master.MasterDataViewRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.configuration.admin.ExternalInterfaceConfig;
import com.softcell.gonogo.model.configuration.admin.WorkflowMaster;
import com.softcell.gonogo.model.configuration.admin.WorkflowNode;
import com.softcell.gonogo.model.core.AllocationInfo;
import com.softcell.gonogo.model.core.Application;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.masters.RoiSchemeMaster;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.ops.CustomerCreditDocs;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.SanctionConditions;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AppConfigurationHelper;
import com.softcell.service.AppConfigurationManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.workflow.component.manager.ComponentManager;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by archana on 29/1/18.
 */
@Component
public class ModuleManager {

    private static Logger logger = LoggerFactory.getLogger(ModuleManager.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ModuleHelper moduleHelper;

    @Autowired
    AppConfigurationManager appConfigurationManager;

    @Autowired
    AppConfigurationHelper appConfigurationHelper;

    @Autowired
    WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    MasterDataViewRepository masterDataViewRepository;

    private static final String STAGE_CHANGE_VALIDATION_OPS = "Sanctioned conditions/Deviations/Docs verification are pending.";

    private static final String STAGE_CHANGE_VALIDATION_CRDT = "Deviations are pending.";

    public void executeBre(GoNoGoCustomerApplication goNoGoCustomerApplication, String executionPoint, String loggedInUserRole,
                           Header header, String nodeType) {
        String productName = header.getProduct().name();
        WorkflowMaster workflowMaster = appConfigurationManager.getActiveWorkFlow(header.getInstitutionId(),
                productName);

        if (workflowMaster != null) {
            callExternalInterfacesAsPerWorkflow(goNoGoCustomerApplication, executionPoint, loggedInUserRole, header, nodeType, workflowMaster);
        } else {
            callExternalInterfacesAsPerConfiguration(goNoGoCustomerApplication, executionPoint, loggedInUserRole, productName);
        }
    }

    private void callExternalInterfacesAsPerConfiguration(GoNoGoCustomerApplication goNoGoCustomerApplication,
                       String executionPoint, String loggedInUserRole, String productName) {
        // Add role based check ( Hint : check ACTION setting )
        if (hasBrePermission(goNoGoCustomerApplication.getApplicationRequest().getHeader(), executionPoint)) {
            String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
            Map<String, MultiBreComponentConfiguration> productMultiBreConfigMap = Cache.MULTIBRE_COMPONENT_SETTING_MAP.get(institutionId);
            if (productMultiBreConfigMap != null){
                MultiBreComponentConfiguration multiBreConfig = productMultiBreConfigMap.get(productName);

            // Check whether there are BRE components set for this execution point
            if (multiBreConfig != null && multiBreConfig.getMultiBreComponentMap() != null) {
                if (multiBreConfig.getMultiBreComponentMap().get(executionPoint) != null) {
                    List<MultiBreComponentConfiguration.BreSetting> breComponents
                            = multiBreConfig.getMultiBreComponentMap().get(executionPoint);

                    // Check whether there are compoments to be run at this edge of executionPoint
                    if (CollectionUtils.isNotEmpty(breComponents)) {
                    /* Iterate the elements of the list breComponents; the list contains a single entry since there are
                        only 2 values of the flag executionPointStart.*/
                        for (MultiBreComponentConfiguration.BreSetting breSetting : breComponents) {
                            boolean mainThreadWait = breSetting.isMainThreadWait();
                            scheduleComponantManager(goNoGoCustomerApplication, executionPoint, loggedInUserRole,
                                    breSetting.getComponentMap(), mainThreadWait);
                        }
                    }
                }
            }
          }
        }
    }

    private void callExternalInterfacesAsPerWorkflow(GoNoGoCustomerApplication goNoGoCustomerApplication, String executionPoint, String loggedInUserRole, Header header, String nodeType, WorkflowMaster workflowMaster) {
        WorkflowNode node = appConfigurationHelper.getWorkflowNode(executionPoint, nodeType,
                null, workflowMaster);

        // call external interfaces if any configured in workflow (eg. MiFin, Perfios etc)
        if (node != null && node.getExternalInterfacesConfig() != null) {
            ExternalInterfaceConfig externalInterfaceConfig = node.getExternalInterfacesConfig();
            boolean mainThreadWait = externalInterfaceConfig.isMainThreadWait();
            // call internal executors (eg. MB, SOBRE, Dedupe etc)
            if (externalInterfaceConfig.getComponentMap() != null) {
                scheduleComponantManager(goNoGoCustomerApplication, executionPoint, loggedInUserRole,
                        externalInterfaceConfig.getComponentMap(), mainThreadWait);
            }

            // call external interfaces (eg. MiFin, Perfios, LMS etc)
            if (CollectionUtils.isNotEmpty(externalInterfaceConfig.getInterfaces())) {
                moduleHelper.callExternalInterfaces(externalInterfaceConfig.getInterfaces(), goNoGoCustomerApplication,
                        loggedInUserRole, header.getLoggedInUserId(), executionPoint);

            }
        }
    }

    private void scheduleComponantManager(GoNoGoCustomerApplication goNoGoCustomerApplication, String executionPoint,
                                          String loggedInUserRole, Map<Integer, com.softcell.workflow.component.Component> componantMap, boolean mainThreadWait) {
        ComponentManager componentManager = new ComponentManager(goNoGoCustomerApplication,
                componantMap, loggedInUserRole);
        logger.debug("ModuleManager stating component for refId {} at {} with threadwait {}",
                goNoGoCustomerApplication.getApplicationRequest().getRefID(), executionPoint, mainThreadWait);

        componentManager.start();

        if (mainThreadWait) {
            logger.debug("At {} waiting for components to finish for refId {}", executionPoint,
                    goNoGoCustomerApplication.getApplicationRequest().getRefID());
            try {
                componentManager.join();
                logger.debug("At {} - components finished for refId {}", executionPoint,
                        goNoGoCustomerApplication.getApplicationRequest().getRefID());
            } catch (InterruptedException e) {
                logger.error("{}",e.getStackTrace());
            }
        } else {
            logger.debug("At {} no wait for components started for refId {}", executionPoint,
                    goNoGoCustomerApplication.getApplicationRequest().getRefID());
        }
    }

    private boolean hasBrePermission(Header header, String executionPoint) {
        boolean permission = true;
        switch (header.getInstitutionId()) {
            case "4075":
            case "4021":    //SBFC PROD instId
                permission = checkBrePermissionsForSBFC(header, executionPoint);
                break;
            default:
                break;
        }
        return permission;
    }

    private boolean checkBrePermissionsForSBFC(Header header, String executionPoint) {
        boolean permitted = true;
        /*if( Roles.Role.CPA == Roles.Role.valueOf(header.getLoggedInUserRole()) ) {
            switch (executionPoint){
                case EndPointReferrer.STEP_REGISTRATION :
                case EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS :
                case EndPointReferrer.STEP_PROFESSION_INCOME_DETAILS :
                        permitted = false;
                        break;
                default : break;
            }
        }*/
        return permitted;
    }

    public boolean checkApplicationIsEditable(String refId, String userId, String loggedInUserRole, String currentStageId, String institutionId,
                                              AllocationInfo allocationInfo, boolean isCredit) throws Exception {
        boolean isEditable = false;
        if (Cache.ROLE_ENABLED_STAGES_MAP.get(loggedInUserRole) != null) {
            // Check WRITE access for the role for the application in current stage
            if (Cache.ROLE_ENABLED_STAGES_MAP.get(loggedInUserRole).contains(currentStageId)) {
                // if institution has no QMS configured then check applications operator
                if (!Cache.INSTITUTION_QUEUE_CONFIG.get(institutionId)) {
                    // Check WRITE access for the User for the application
                    isEditable = moduleHelper.checkApplicationIsEditableForOperator(userId, loggedInUserRole, refId, institutionId, allocationInfo, isCredit);
                }
            }
        }
        if(Institute.isInstitute(institutionId,Institute.SBFC) && isCredit){
            isEditable = moduleHelper.checkApplicationIsEditableForOperator(userId, loggedInUserRole, refId, institutionId, allocationInfo, isCredit);
        }
        return isEditable;
    }

    public void preSave(ApplicationRequest applicationRequest, String execPoint) {

        // Update current stage of applicationRequest same as GoNoGoCustomerApplication
        moduleHelper.updateApplicationRequestStage(applicationRequest);

        switch (execPoint) {
            case EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS:
                // copy address to RV,OV
                moduleHelper.savePropertyVerificationDetails(applicationRequest);
                recheckLoanCharges(applicationRequest);

                if (StringUtils.equals(applicationRequest.getHeader().getProduct().toProductName(), Product.DIGI_PL.name())) {
                    moduleHelper.preSaveCamSummary(applicationRequest, "Only For DIGIPL"); //ToDo : remove second parameter "product" when preSaveCamSummary will be available for All product.
                }
                moduleHelper.preSaveRTRDetails(applicationRequest);

                moduleHelper.updateApplicantNameOfPropertyVisit(applicationRequest);
                moduleHelper.updateApplicantNameInPropertyValuation(applicationRequest);
                moduleHelper.updateInsurance(applicationRequest);
                break;
            case EndPointReferrer.STEP_COLLATERAL_DETAILS:
                // copy address into PropertyVisit, Valuation
                moduleHelper.updateAddressOfPropertyVisit(applicationRequest);
                moduleHelper.updateAddressOfValuation(applicationRequest);
                moduleHelper.updateAddressOfLegalVerification(applicationRequest);
                break;
        }
    }

    private void recheckLoanCharges(ApplicationRequest applicationRequest) {
        String refId = applicationRequest.getRefID();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        // Check whether any consent flag related to loancharges have been changed.
        // Check creditvidya consent flag
        LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, institutionId);
        if( loanCharges != null ) {
            boolean toSave = recheckCreditVidyaCharges(applicationRequest, loanCharges);
            if (toSave) {
                applicationRepository.saveLoanChargesDetails(refId, institutionId, loanCharges);
            }
        }
    }

    private boolean recheckCreditVidyaCharges(ApplicationRequest applicationRequest, LoanCharges loanCharges) {
        boolean toSave = false;
        boolean creditVidyaConsent = applicationRequest.getRequest().getApplicant().isCreditVidyaFlag();
        // TODO Remove hardcoded value
        int creditVidyaCharges = creditVidyaConsent ? 1200 :0;
        if( CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant() ) ){
            int coAppCreditVidyaConsent = (int)applicationRequest.getRequest().getCoApplicant()
                    .stream()
                    .filter(coApplicant -> coApplicant.isCreditVidyaFlag() == true)
                    .count();
            creditVidyaCharges += (1200 * coAppCreditVidyaConsent);
        }
        String newCreditVidyaCharges = Integer.toString(creditVidyaCharges);
        // get loan charges
        String dbCreditVidyaCharges = loanCharges.getCreditVidyaCharge();
        if( StringUtils.isEmpty(dbCreditVidyaCharges)){
            if( creditVidyaCharges > 0) {
                loanCharges.setCreditVidyaCharge(newCreditVidyaCharges);
                toSave = true;
            }
        } else {
            if( ! StringUtils.equalsIgnoreCase(newCreditVidyaCharges, dbCreditVidyaCharges)){
                loanCharges.setCreditVidyaCharge(newCreditVidyaCharges);
                toSave = true;
            }
        }
        return toSave;
    }

    public void validateStageChange(String refID, Header header, String currentStage) throws GoNoGoException {
        String insttId = header.getInstitutionId();
        String userId = header.getLoggedInUserId();
        String role = header.getLoggedInUserRole();
        boolean isCredit = false;
        if(Institute.isInstitute(header.getInstitutionId(),Institute.SBFC)) {
            if(header.getCredit() != null) {
                isCredit = header.getCredit();
            }
        }

        if (StringUtils.equalsIgnoreCase(role,Roles.Role.HOPS.name())) {
            /*  Stage change is valid only if all following 3 conditions are satisfied
                    All sanction conditions have to be met
                    All deviations have been closed
                    All documents received
            */
            validateSanctionCondition(refID, insttId);
            validateDeviations(refID, insttId);
            validateCreditDocsReceived(refID, insttId);
        } else if (Roles.isCreditRole(role) || StringUtils.equalsIgnoreCase(role, Roles.Role.CREDIT.name()) || isCredit) {
            try {
                validateDeviations(refID, insttId);
            } catch (GoNoGoException e) {
                throw new GoNoGoException(STAGE_CHANGE_VALIDATION_CRDT);
            }
        }
    }

    private void validateCreditDocsReceived(String refID, String insttId) throws GoNoGoException {
        // Documents received
        CustomerCreditDocs documents = applicationRepository.fetchCustomerCreditDocs(refID, insttId);
        // check whther all sanctioned conditions are in OK status
        if (documents != null) {
            if (CollectionUtils.isNotEmpty(documents.getCustomerCreditDetails())) {
                // Throw exception if any condition is pending
                if (documents.getCustomerCreditDetails().stream().filter(document ->
                        StringUtils.equalsIgnoreCase(document.getOpsStatus(), Status.PENDING.name()))
                        .collect(Collectors.toList()).size() > 0) {
                    throw new GoNoGoException(STAGE_CHANGE_VALIDATION_OPS);
                }
            }
        }
    }

    public void validateDeviations(String refID, String insttId) throws GoNoGoException {
        // Deviation conditions met
        DeviationDetails deviationDetails = applicationRepository.fetchDeviationDetails(refID, insttId);
        // check whther all sanctioned conditions are in OK status
        if (deviationDetails != null) {
            if (CollectionUtils.isNotEmpty(deviationDetails.getDeviationList())) {
                // Throw exception if any condition is pending
                if (deviationDetails.getDeviationList().stream().filter(deviation ->
                        StringUtils.equalsIgnoreCase(deviation.getStatus(), Status.PENDING.name()))
                        .collect(Collectors.toList()).size() > 0) {
                    throw new GoNoGoException(STAGE_CHANGE_VALIDATION_OPS);
                }
            }
        }
    }

    private void validateSanctionCondition(String refID, String insttId) throws GoNoGoException {
        // Sanctioned conditions met
        SanctionConditions sanctionConditions = applicationRepository.fetchSanctionConditions(refID, insttId);
        // check whther all sanctioned conditions are in OK status
        if (sanctionConditions != null) {
            if (CollectionUtils.isNotEmpty(sanctionConditions.getScDetailsList())) {
                // Throw exception if any condition is pending
                if (sanctionConditions.getScDetailsList().stream().filter(conditionDetails ->
                        StringUtils.equalsIgnoreCase(conditionDetails.getStatus(), Status.PENDING.name()))
                        .collect(Collectors.toList()).size() > 0) {
                    throw new GoNoGoException(STAGE_CHANGE_VALIDATION_OPS);
                }
            }
        }
    }

    public void afterSave(GoNoGoCustomerApplication goNoGoCustomerApplication, String stepId) {
        if (StringUtils.equalsIgnoreCase(EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS, stepId)) {
            removeInvalidComponantResponse(goNoGoCustomerApplication);
        }
    }

    public void removeInvalidComponantResponse(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        List<CoApplicant> coApplicants = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();
        List<ComponentResponse> coApplicantComponentResponseList = new ArrayList<>();
        ;
        List<ComponentResponse> coApplicantCompRspList =
                goNoGoCustomerApplication.getApplicantComponentResponseList();
        if (CollectionUtils.isNotEmpty(coApplicantCompRspList)) {
            // filter list for all co-applicant
            Map<String, ComponentResponse> tempMap = new HashMap<>();
            // Filter DB objects for unique records for applicantID
            coApplicantCompRspList.forEach(componentResponse -> {
                if(null != componentResponse) tempMap.put(componentResponse.getApplicantId(), componentResponse);
            });
            //Gel all CompononantResponse objects for current coApplicants
            for (CoApplicant applicant : coApplicants) {
                if (tempMap.get(applicant.getApplicantId()) != null)
                    coApplicantComponentResponseList.add(tempMap.get(applicant.getApplicantId()));
            }
            //Set filtered list in goNoGoCustomerApplication
            if (CollectionUtils.isNotEmpty(coApplicantComponentResponseList)) {
                goNoGoCustomerApplication.setApplicantComponentResponseList(coApplicantComponentResponseList);
            } else {
                goNoGoCustomerApplication.setApplicantComponentResponseList(null);
            }
        }
    }

    public void setStage( String nodeValue, String nodeType, String event, GoNoGoCustomerApplication gngApplication) throws Exception {
        ApplicationRequest applicationRequest = gngApplication.getApplicationRequest();
        Header header = applicationRequest.getHeader();
        String insttId = header.getInstitutionId();
        String product = header.getProduct().name();

        WorkflowMaster workflowMaster = appConfigurationManager.getActiveWorkFlow(insttId, product);
        String action = null;
        boolean updateStage;
        if (workflowMaster != null){
            updateStage = setStage(nodeValue, nodeType, event, gngApplication, workflowMaster);

        } else {
            // TODO This falls to default flow
            updateStage = false;
        }
        if (updateStage) {
            // update stage in db
            updateStageId(applicationRequest, null);
        }

        // Remove allocation

        // Intimate users


        // fetch updated doc
    }


    private boolean setStage(String nodeValue, String nodeType, String event,
                             GoNoGoCustomerApplication gngApplication,  WorkflowMaster workflowMaster)  throws Exception{
        boolean updateStage = true;
        ApplicationRequest applicationRequest = gngApplication.getApplicationRequest();
        Header header = applicationRequest.getHeader();
        String refId = applicationRequest.getRefID();
        String insttId = header.getInstitutionId();
        String userId = header.getLoggedInUserId();
        String loggedInRole = header.getLoggedInUserRole();
        String currentStage = applicationRequest.getCurrentStageId();
        String changedStage = null;

        WorkflowNode node = null;
        if( StringUtils.isNotEmpty(nodeValue) ) {
            node = appConfigurationHelper.getWorkflowNode(nodeValue, nodeType, event, workflowMaster);
        } else {
            node = appConfigurationHelper.getWorkflowNode(currentStage, nodeType, event, workflowMaster);
        }
        if (node != null) {
            Application application = gngApplication.getApplicationRequest().getRequest().getApplication();
            String flowType = ConfigurationConstants.APPLICATION_FLOW_TYPE_NORMAL;
            if(application.isCoOrigination()){
                flowType = ConfigurationConstants.APPLICATION_FLOW_TYPE_COORIGINATION;
            }
            String nextStage = appConfigurationHelper.getNextStage(node, currentStage, gngApplication.getApplicationStatus(), loggedInRole, flowType);
            if( nextStage != null) {
                applicationRequest.setCurrentStageId(nextStage);
                changedStage = nextStage;
                String stepId = "submit-by-" + loggedInRole;
                applicationRepository.updateCompletedInfo(gngApplication, stepId, userId, loggedInRole);
                // After-submit module check

                this.executeBre(gngApplication, currentStage, loggedInRole, applicationRequest.getHeader(), ConfigurationConstants.WF_NODE_TYPE_STAGE);
                if (!StringUtils.equalsIgnoreCase(nextStage, GNGWorkflowConstant.STAGE_CHANGE_VIA_CODE.toFaceValue())) {
                    gngApplication.getApplicationRequest().setCurrentStageId(nextStage);
                }
            } else {
                // No next stage hence no update required
                updateStage = false;
            }
        } else {
            updateStage = false;
            applicationRequest.setCurrentStageId(currentStage);
        }
        return updateStage;
    }


    public boolean updateStageId(ApplicationRequest applicationRequest, String applicationStatus) throws Exception {
        return applicationRepository.updateStageId(applicationRequest, applicationStatus);
    }

    public boolean isMBSuccessful(ComponentResponse componentResponse){
        boolean success = false;
        /*  Scenarios :
                component may be having consumer or commercial
                If has neither consumer nor commercial -> success = false => Default condition
                If either of the present then check for status; ( "applicantComponentResponse.multiBureauJsonRespose.status":"REJECTED" )
                In the finished list check status
        * */
        if( componentResponse.getMultiBureauJsonRespose() != null ) {
            ResponseMultiJsonDomain mbResponse = componentResponse.getMultiBureauJsonRespose();
            if( ! StringUtils.equalsIgnoreCase(mbResponse.getStatus(), Status.REJECTED.name() ) ){
                // Check for finished list status
                if( CollectionUtils.isNotEmpty(mbResponse.getFinishedList()) ){
                    if( mbResponse.getFinishedList().size() == 1){
                        if( StringUtils.equalsIgnoreCase(mbResponse.getFinishedList().get(0).getStatus(),
                                Status.SUCCESS.name() ) ){
                            success = true;
                        }
                    } else {
                        // TODO : get response of particular bureau - need to add bureau name in method param
                        //mbResponse.getFinishedList().stream().filter(finished -> StringUtils.equalsIgnoreCase(finished.getBureau(), )
                    }
                }
            }

        } else if ( componentResponse.getMbCorpJsonResponse() != null){
            // TODO : later
        }
        return success;
    }

    public WFJobCommDomain getServiceConfiguration(String institutionId, String product, String serviceId, String urlType) throws GoNoGoException {
        WFJobCommDomain serviceConfig = workFlowCommunicationManager.getWfCommDomainJob(
                institutionId, product,  serviceId,  urlType);
        if (null == serviceConfig) {
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType));
        }
        return serviceConfig;
    }

    public void checkForCoOrigination(ApplicationRequest applicationRequest) {
        Header header = applicationRequest.getHeader();
        Application application = applicationRequest.getRequest().getApplication();
        if(application != null){
            if(application.isCoOrigination()){
                if(application.getSchemeName() != null && application.getCompanyName() != null){
                    RoiSchemeMaster roiSchemeMaster = masterDataViewRepository.getSchemeCompanySpecificData(application.getCompanyName(), application.getSchemeName());
                    if(roiSchemeMaster != null){
                        // pre saving Coorigination specific details
                        moduleHelper.updateCoOriginationDetails(applicationRequest, roiSchemeMaster);
                    }
                }
            }
        }
    }
}
