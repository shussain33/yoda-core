package com.softcell.gonogo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by amit on 30/3/18.
 */
public class Stages {

    public enum Stage {
        DE, DDE, CRDT, CR_H, APRV, DCLN, SUBJ_APRV, BOPS, HOPS, TRANCH_BOPS, TRANCH_HOPS, EXTERNAL_CRDT, EXTERNAL_OPS, EXT_APRV, EXT_DCLN, CR_Q
    };

    private static ArrayList<String> creditStages = new ArrayList<String>()
    {{  add(Stages.Stage.CRDT.name()); add(Stages.Stage.CR_H.name()); add(Stages.Stage.APRV.name());
        add(Stages.Stage.DCLN.name()); add(Stages.Stage.SUBJ_APRV.name()); add(Stage.EXTERNAL_CRDT.name());
        add(Stages.Stage.EXT_APRV.name()); add(Stage.EXT_DCLN.name());
    }};

    private static ArrayList<String> opsStages = new ArrayList<String>()
    {{  add(Stages.Stage.BOPS.name());  add(Stages.Stage.HOPS.name()); add(Stage.TRANCH_BOPS.name()); add(Stage.TRANCH_HOPS.name());
        add(Stage.EXTERNAL_OPS.name());
    }};

    private static ArrayList<String> cpaStages = new ArrayList<String>()
    {{  add(Stages.Stage.DDE.name());
        add(Stages.Stage.CR_Q.name());
    }};

    private static ArrayList<String> tranchStages = new ArrayList<String>()
    {{  add(Stage.TRANCH_BOPS.name());
        add(Stage.TRANCH_HOPS.name());
    }};

    private static ArrayList<String> bopsStages = new ArrayList<String>()
    {{  add(Stage.TRANCH_BOPS.name());
        add(Stage.BOPS.name());
    }};


    public static List<String> getCreditStages() {
        return (List)creditStages.clone();
    }

    public static List<String> getOpsStages() {
        return (List)opsStages.clone();
    }

    public static List<String> getCpaStages() {
        return (List)cpaStages.clone();
    }

    public static boolean isCreditStage(String stage){        return creditStages.contains(stage);    }

    public static boolean isOpsStage(String stage){        return opsStages.contains(stage);    }

    public static boolean isCpaStage(String stage){        return cpaStages.contains(stage);    }

    public static boolean isTranchStage(String stage){  return tranchStages.contains(stage);    }

    public static boolean isBopsStage(String stage){  return bopsStages.contains(stage);    }

}
