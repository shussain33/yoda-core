package com.softcell.gonogo;

import com.softcell.config.actions.Role;
import com.softcell.config.actions.User;
import com.softcell.constants.*;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.Remark;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.adroit.AdroitCallLog;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.cam.CamSummary;
import com.softcell.gonogo.model.core.cam.RTRDetail;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.eligibility.IncomeDetail;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.core.ocr.OCRDetails;
import com.softcell.gonogo.model.core.ocr.OCRDetailsObj;
import com.softcell.gonogo.model.core.ocr.OCRRequest;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationDetails;
import com.softcell.gonogo.model.core.valuation.ValuationInput;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.core.verification.*;
import com.softcell.gonogo.model.lms.DisburseResponse;
import com.softcell.gonogo.model.lms.SBFCLMSIntegrationRequest;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.RoiSchemeMaster;
import com.softcell.gonogo.model.ops.*;
import com.softcell.gonogo.model.request.*;
import com.softcell.gonogo.model.request.core.FileHeader;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.core.ThirdPartyVerification;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DataEntryManager;
import com.softcell.service.ExternalApiManager;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.impl.LMSIntegrationManager;
import com.softcell.service.thirdparty.utils.IciciHelper;
import com.softcell.service.thirdparty.utils.PolicyHelper;
import com.softcell.service.thirdparty.utils.ReligareHelper;
import com.softcell.service.thirdparty.utils.ThirdPartyIntegrationHelper;
import com.softcell.service.utils.MiFinHelper;
import com.softcell.service.utils.OriginationHelper;
import com.softcell.utils.GngCalculationUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.executors.extapi.ThirdPartyExecutor;
import com.softcell.workflow.executors.quickcheck.QuickCheckExecutor;
import jodd.util.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by amit on 14/3/18.
 */
@Component
public class ModuleHelper {

    private static Logger logger = LoggerFactory.getLogger(ModuleManager.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private DataEntryManager dataEntryManager;

    @Autowired
    private ExternalApiManager externalApiManager;

    @Autowired
    private ThirdPartyIntegrationHelper thirdPartyHelper;

    @Autowired
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Autowired
    private OriginationHelper originationHelper;

    @Autowired
    private PolicyHelper policyHelper ;

    @Autowired
    private ModuleManager moduleManager;

    @Autowired
    private LMSIntegrationManager lmsIntegrationManager;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private MasterRepository masterRepository;

    public boolean checkApplicationIsEditableForOperator(String userId, String role, String refId, String institutionId,
                                                         AllocationInfo allocationInfo,boolean isCredit) throws Exception {
        boolean isEditable = true;
        boolean update = false;
        // Release previous working application with which userId is associated as operator
        applicationRepository.releaseAllocatedApplications(userId, institutionId);
        // Check that currently no user is editing the application

        if( allocationInfo == null){
            // Set User as new Operator for the application
            allocationInfo = new AllocationInfo();
            allocationInfo.setOperator(userId);
            allocationInfo.setRole(role);
            update = true;
        } else {
            /// if logged in login userId is not equal to assigned operator then make case non editable
            if( !userId.equals(allocationInfo.getOperator()) && allocationInfo.isLocked()){
                //for sbfc credit-limit, check if role is credit then set isEditable to true
                if(Institute.isInstitute(institutionId,Institute.SBFC) && isCredit){
                    isEditable = true;
                }else{
                    isEditable = false;
                }
            } else {
                /*  Do nothing when allocation is not null and logged in userId is same as assignd operator
                        because user is already assigned to the case , so no need of updating allocaton Info.
                 */
            }
        }

        if(update){
            applicationRepository.updateGoNoGoCustomerApplication(refId, allocationInfo, institutionId);
        }
        return isEditable;
    }

    public void updateAddressOfValuation(ApplicationRequest applicationRequest) {
        ValuationRequest valuationRequest = new ValuationRequest();
        valuationRequest.setHeader(applicationRequest.getHeader());
        valuationRequest.setRefId(applicationRequest.getRefID());
        Name applicantName = applicationRequest.getRequest().getApplicant().getApplicantName();

        Valuation valuation = applicationRepository.fetchValuationDetails(valuationRequest);
        List<ValuationDetails> valuationDetailsList = null;
        ValuationDetails valuationDetails = null;
        boolean addAll = true;

        if (valuation == null) {  // Valuation not yet saved in DB
            valuation = new Valuation();
            valuation.setRefId(applicationRequest.getRefID());
            valuation.setInstitutionId(applicationRequest.getHeader().getInstitutionId());
            valuationDetailsList = new ArrayList<>();
            valuation.setValuationDetailsList(valuationDetailsList);
        } else {
            valuationDetailsList = valuation.getValuationDetailsList();
            if (CollectionUtils.isEmpty(valuationDetailsList)) {
                valuationDetailsList = new ArrayList<>();
                valuation.setValuationDetailsList(valuationDetailsList);
            } else {
                addAll = false;
            }
        }
        if (addAll) {
            List<Collateral> collateralList = applicationRequest.getRequest().getApplication().getCollateral();
            for (Collateral colletral : collateralList) {
                valuationDetails = new ValuationDetails();
                valuationDetails.setValuationInput(new ValuationInput());
                valuationDetails.getValuationInput().setPropertyAddress(colletral.getAddress());
                valuationDetails.getValuationInput().setPropertyOwnerNames(colletral.getOwnerNames());
                valuationDetails.getValuationInput().setPropertyType(colletral.getType());
                valuationDetails.setCollateralId(colletral.getCollateralId());
                valuationDetails.getValuationInput().setApplicantName(applicantName);
                valuationDetails.setStatus(ThirdPartyVerification.Status.PENDING.name());
                valuationDetailsList.add(valuationDetails);
            }
        } else {
            for (Collateral colletralObj : applicationRequest.getRequest().getApplication().getCollateral()) {
                boolean addNewObject = true;
                for (ValuationDetails dbObj : valuationDetailsList) {
                    if (StringUtils.equals(colletralObj.getCollateralId(), dbObj.getCollateralId())) {
                        if (!StringUtils.equalsIgnoreCase(dbObj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                            dbObj.getValuationInput().setPropertyAddress(colletralObj.getAddress());
                            dbObj.getValuationInput().setPropertyOwnerNames(colletralObj.getOwnerNames());
                            dbObj.getValuationInput().setPropertyType(colletralObj.getType());
                            dbObj.getValuationInput().setApplicantName(applicantName);
                            if (dbObj.getStatus() == null)
                                dbObj.setStatus(ThirdPartyVerification.Status.PENDING.name());
                        }
                        addNewObject = false;
                        break;
                    }
                }
                if (addNewObject) {
                    ValuationDetails newObj = new ValuationDetails();
                    newObj.setCollateralId(colletralObj.getCollateralId());
                    newObj.setValuationInput(new ValuationInput());
                    newObj.getValuationInput().setPropertyAddress(colletralObj.getAddress());
                    newObj.getValuationInput().setPropertyOwnerNames(colletralObj.getOwnerNames());
                    newObj.getValuationInput().setPropertyType(colletralObj.getType());
                    newObj.getValuationInput().setApplicantName(applicantName);
                    newObj.setStatus(ThirdPartyVerification.Status.PENDING.name());
                    valuationDetailsList.add(newObj);
                }
            }
        }

        valuationRequest.setValuation(valuation);
        Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
        Map<String, Valuation> triggerToAgencies = new HashMap<>();
        applicationRepository.saveValuationDetails(valuationRequest, verificationStatusDealersMap, triggerToAgencies);
        /*appConfigurationHelper.intimateUsers(valuationRequest.getValuation().getRefId(),
                IntimationConstants.VERIFICATION_TRIGGER, IntimationConstants.VER_TYPE_VALUATION ,verificationStatusDealersMap);*/
        // Send message to valuation agency
        triggerToAgencies(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId(),
                                                    triggerToAgencies);
    }

    /**
     * Sends trigger to third party valuation agencies configured for the institution and product
     *
     * @param refID
     * @param institutionId
     * @param triggerToAgencies
     */
    public void triggerToAgencies(String refID, String institutionId, Map<String, Valuation> triggerToAgencies) {
        /* Get whether agencies are configured for this instituion.
            If yes then get the communication details and send proper request
        */
        logger.debug("{} : triggerToAgencies ", refID, triggerToAgencies);

        try {
            ApplicationRequest applicationRequest =
                    applicationRepository.getApplicationRequest(refID, institutionId);
            String agencyCode ;
            Valuation valuation;
            for( Map.Entry<String, Valuation> entry :triggerToAgencies.entrySet()) {
                agencyCode = entry.getKey();
                valuation = entry.getValue();

                //Read the configuration.
                WFJobCommDomain agencyConfig = workFlowCommunicationManager.getWfCommDomainJobByMemberId(
                        institutionId, applicationRequest.getHeader().getProduct().toProductName(), agencyCode, ThirdPartyIntegrationHelper.VALUATION_INITIATE);
                if( agencyConfig != null ){
                    ThirdPartyExecutor executor = new ThirdPartyExecutor();
                    executor.init(refID, institutionId, agencyConfig, applicationRequest, valuation);
                    Thread threePThread = new Thread(executor);
                    logger.info("{} : Valuation trigger for agency {} starting...", refID, agencyConfig.getServiceId());
                    threePThread.start();
                }
            }
        } catch (Exception e){
            logger.error("{}",e.getStackTrace());
            logger.error("Exception occurred while triggering valuation to agencies for refId {} : {}", refID, e.getStackTrace());
        }
    }

    public Valuation getAgencyValuationReport(ValuationRequest valuationRequest, Valuation valuation){
        String refID = valuationRequest.getRefId();
        String institutionId = valuationRequest.getHeader().getInstitutionId();
        String product = valuationRequest.getHeader().getProduct().toProductName();

        /*
                Check for valuation status if either of the SUBMIITED agencies is configured
                i.e. intersection of submitted agencies and configured agencies is empty no need to check
         */
        List<ValuationDetails> valDetailList = valuation.getValuationDetailsList();
        setIndex(valDetailList);
        // Get configured agencies for this instt
        List<WFJobCommDomain> serviceConfigList = workFlowCommunicationManager.getServices(institutionId,
                                                                    ThirdPartyIntegrationHelper.VALUATION_CHECK, product);
        // Represents map of  Member id(agency code) vs agency name - Useful since code flow works with agency Name
        Map<String, String> agencyServiceIdCodes = serviceConfigList.stream()
                                                .collect(Collectors.toMap(config -> config.getMemberId(),
                                                                            config -> config.getServiceId()) );

        // List of memberIds (agency codes) - This is required since valuation has agency code
        List<String> agencyCodes = new ArrayList<>(agencyServiceIdCodes.keySet());

        logger.info("{} Got agencies from db {} ", refID, agencyServiceIdCodes);

        // Filter valuation inputs on SUBMIITED status and configured agency codes
        List<ValuationDetails> submittedList = valDetailList.stream()
                .filter(valDtl -> agencyCodes.contains( valDtl.getAgencyCode())
                                && StringUtils.equals(valDtl.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name()) )
                .collect(Collectors.toList());
        logger.info("{} submittedList size {} ", refID, submittedList.size());

        /* Get count of VERIFIED valuations so that after call to agencies, we shall come to know
            whether more valuations has been VERIFIED.
        */
        // Verified list is required to know, so that newly verified valuations can be filtered on the basis of exclusion
        List<ValuationDetails> verifiedList = valDetailList.stream()
                .filter(valDtl -> StringUtils.equals(valDtl.getStatus(), ThirdPartyVerification.Status.VERIFIED.name()) )
                .collect(Collectors.toList());
        int verifiedCount = verifiedList.size();
        logger.info("{} already verified count {}", refID, verifiedCount);

        if( CollectionUtils.isNotEmpty(submittedList) ){
            boolean agencyReport = false;
            WFJobCommDomain serviceConfig = null;

            // From the submitted list iterate for each agency code and check valation for each agency
            for( String agencyCode : agencyCodes ){
                // serviceConfig from serviceConfigList
                serviceConfig = serviceConfigList.stream()
                        .filter(config -> StringUtil.endsWithIgnoreCase(agencyCode, config.getMemberId()))
                        .collect(Collectors.toList()).get(0);
                List<ValuationDetails> agencySubmittedList = submittedList.stream()
                                                                .filter(valDtl -> StringUtils.equals( valDtl.getAgencyCode(), agencyCode))
                                                                .collect(Collectors.toList());
                // Get ackId from DB based on the service
                // Check for ADROIT
                if( StringUtils.equalsIgnoreCase(agencyServiceIdCodes.get(agencyCode),
                                                ThirdPartyIntegrationHelper.ThirdParty.ADROIT.name())  ) {
                    List<AdroitCallLog> ackIds = thirdPartyHelper.getAckIds(agencyServiceIdCodes.get(agencyCode), refID,
                                                                            institutionId, agencySubmittedList);
                    logger.debug("{} AckIds fetched size {}", refID, ackIds.size());

                    if (CollectionUtils.isNotEmpty(ackIds)) {
                        // Call 3pExecutor for each submitted
                        logger.info("{} : Fetching report from agency {}", refID, agencyCode);
                        call3piExecutor(refID, ackIds, serviceConfig, valuation);

                    } else {
                        logger.info("{} : Valuation not triggered for agency {}", refID, agencyCode);
                    }
                } else {
                    // Code will never reach here since any other agency not yet configured
                    // But for foolproof log following statement
                    logger.error("{} System not configured for valuation agency {}" , refID, agencyCode);
                }
            } // Agency code loop

            /* Now check count of VERIFIED VALUATIONS .
                        If the count is > verifiedCount => at least a single valuation has been VERIFIED
                            set agencuyReport flag.
                    */
            List<ValuationDetails> newVerifiedList = valDetailList.stream()
                    .filter(valDtl -> StringUtils.equals(valDtl.getStatus(), ThirdPartyVerification.Status.VERIFIED.name()) )
                    .collect(Collectors.toList());
            int newVerifiedCount = newVerifiedList.size();
            logger.info("{} newVerifiedCount {}", refID, newVerifiedCount);
            if( newVerifiedCount > verifiedCount )   agencyReport = true;

            if( agencyReport ) {
                logger.info("{} Saving valuation since there is report fetched for {} valuations", refID,
                                                                        (newVerifiedCount - verifiedCount));
                saveValuationDetails(valuationRequest, valuation);
                // There is imagelist for the valuations from Adroit , need to download and save the images
                // Send the list of verified valuations which have been verfied in this call.
                // For that get disjunction of verifiedList and latestVerified list
                if( verifiedCount == 0 ) {
                    saveImages(refID, institutionId, product, valuation, newVerifiedList);
                } else {
                    // Get latest verified valuation
                    List<ValuationDetails> latestVerifiedList = newVerifiedList.stream()
                                                            .filter(lv -> ! verifiedList.contains(lv))
                                                            .collect(Collectors.toList());
                    saveImages(refID, institutionId, product, valuation, latestVerifiedList);
                }
            }
        } else {
            // Do nothing
            logger.info("{} No valuation submitted so nothing to fetch", valuationRequest.getRefId());
        }
        return valuation;
    }

    private void setIndex(List<ValuationDetails> valDetailList) {
        int i = 0;
        for( ValuationDetails dtl : valDetailList){
            dtl.setIndex(i);
            i++;
        }
    }

    private void saveImages(String refID, String institutionId, String product, Valuation valuation,
                            List<ValuationDetails> latestVerifiedList) {
        /* Filter valuation output on the basis of imagelist
            Iterate over the filtered o/p
                Get agency code
                Get config
                call 3piExecutor
        * */

        Map<String, WFJobCommDomain> agencyServices = workFlowCommunicationManager.getServices(institutionId,
                ThirdPartyIntegrationHelper.VALUATION_GET_IMAGE, product)
                .stream()
                .collect(Collectors.toMap(config -> config.getMemberId(), config -> config) );

        // Check whether there is any imagelist available
        List<ValuationDetails> valDtlsWithImages = latestVerifiedList
                                                    .stream()
                                                    .filter(valDtl ->
                                                            CollectionUtils.isNotEmpty(valDtl.getValuationOutput().getImageList() ))
                                                    .collect(Collectors.toList());

        WFJobCommDomain agencyConfig = null;
        ExecutorService es = Executors.newCachedThreadPool();
        for( ValuationDetails valDtl : valDtlsWithImages){
            logger.info("{} Fetching images for collateral {} agency {}", refID, valDtl.getCollateralId(), valDtl.getAgencyCode());
            agencyConfig = agencyServices.get(valDtl.getAgencyCode());
            // Create executor and run it
            ThirdPartyExecutor executor = new ThirdPartyExecutor();
            executor.init(refID, valuation.getInstitutionId(), agencyConfig, valDtl, null );
            es.execute(executor);
        }
        es.shutdown();
        // Wait till all images are downloaded
        try {
            boolean finshed = es.awaitTermination(2, TimeUnit.MINUTES);
        } catch (InterruptedException ie){
            logger.error("{}",ie.getStackTrace());
            logger.error("Error while running 3piExecutors {}", ie.getStackTrace());
        }
    }

    private void saveValuationDetails(ValuationRequest valuationRequest, Valuation valuation) {
        valuationRequest.setValuation(valuation);
        Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
        Map<String, Valuation> triggerToAgencies = new HashMap<>(); // not needed here
        boolean save = applicationRepository.saveValuationDetails(valuationRequest, verificationStatusDealersMap, triggerToAgencies);
        /*if (save) {
            appConfigurationHelper.intimateUsers(valuationRequest.getRefId(),
                    IntimationConstants.VERIFICATION_DONE, IntimationConstants.VER_TYPE_VALUATION, verificationStatusDealersMap);
        }*/
    }
    public void updateAddressOfPropertyVisit(ApplicationRequest applicationRequest) {
        PropertyVisitRequest propertyVisitRequest = new PropertyVisitRequest();
        propertyVisitRequest.setHeader(applicationRequest.getHeader());
        propertyVisitRequest.setRefId(applicationRequest.getRefID());
        Name applicantName = applicationRequest.getRequest().getApplicant().getApplicantName();

        PropertyVisit propertyVisit = applicationRepository.fetchPropertyVisitDetailsByRefId(propertyVisitRequest);
        List<PropertyVisitDetails> propertyVisitDetails = new ArrayList<>();
        if (propertyVisit == null) {
            propertyVisit = new PropertyVisit();
            propertyVisit.setRefId(applicationRequest.getRefID());
            propertyVisit.setInstitutionId(applicationRequest.getHeader().getInstitutionId());
        } else {
            propertyVisitDetails = propertyVisit.getPropertyVisitDetailsList();
        }
        if (propertyVisitDetails == null)
            propertyVisitDetails = new ArrayList<>();

        for (Collateral colletralObj : applicationRequest.getRequest().getApplication().getCollateral()) {
            boolean addNewObject = true;
            for (PropertyVisitDetails dbObj : propertyVisitDetails) {
                if (StringUtils.equals(colletralObj.getCollateralId(), dbObj.getCollateralId())) {
                    dbObj.setAddress(colletralObj.getAddress());
                    dbObj.setApplicantName(applicantName);
                    dbObj.setPropertyOwnerNames(colletralObj.getOwnerNames());
                    dbObj.setCollateralStatus(colletralObj.getStatus());
                    dbObj.setCollateralUsage(colletralObj.getUsage());
                    addNewObject = false;
                    break;
                }
            }
            if (addNewObject) {
                PropertyVisitDetails newObj = new PropertyVisitDetails();
                newObj.setCollateralId(colletralObj.getCollateralId());
                newObj.setAddress(colletralObj.getAddress());
                newObj.setApplicantName(applicantName);
                newObj.setPropertyOwnerNames(colletralObj.getOwnerNames());
                newObj.setCollateralUsage(colletralObj.getUsage());
                newObj.setCollateralStatus(colletralObj.getStatus());
                propertyVisitDetails.add(newObj);
            }
        }
        propertyVisit.setPropertyVisitDetailsList(propertyVisitDetails);
        propertyVisitRequest.setPropertyVisit(propertyVisit);

        applicationRepository.savePropertyVisitDetails(propertyVisitRequest);
    }

    public void updateAddressOfLegalVerification(ApplicationRequest applicationRequest) {
        LegalVerificationRequest legalVerificationRequest = new LegalVerificationRequest();
        legalVerificationRequest.setHeader(applicationRequest.getHeader());
        legalVerificationRequest.setRefId(applicationRequest.getRefID());
        Name applicantName = applicationRequest.getRequest().getApplicant().getApplicantName();
        LegalVerification legalVerification = new LegalVerification();

        legalVerification = applicationRepository.fetchLegalVerificatioDetailsByRefId(legalVerificationRequest);
        if (legalVerification != null && legalVerification.getLegalVerificationDetailsList() == null) {
            LegalVerificationDetails legalVerificatioDetailsByRefId = applicationRepository.fetchOldLegalVerificatioDetailsByRefId(legalVerificationRequest);
            if (legalVerificatioDetailsByRefId != null) {
                legalVerificatioDetailsByRefId.setCollateralId("0");
                List<LegalVerificationDetails> legalVerificationDetailsList = new ArrayList<>();
                legalVerificationDetailsList.add(legalVerificatioDetailsByRefId);
                legalVerification.setLegalVerificationDetailsList(legalVerificationDetailsList);
            }
        }
        List<LegalVerificationDetails> legalVerificationDetails = new ArrayList<>();

        if (legalVerification == null) {
            legalVerification = new LegalVerification();
            legalVerification.setRefId(applicationRequest.getRefID());
            legalVerification.setInstitutionId(applicationRequest.getHeader().getInstitutionId());
        } else {
            legalVerificationDetails = legalVerification.getLegalVerificationDetailsList();
        }
        if (legalVerificationDetails == null)
            legalVerificationDetails = new ArrayList<>();

        for (Collateral colletralObj : applicationRequest.getRequest().getApplication().getCollateral()) {
            boolean addNewObject = true;
            for (LegalVerificationDetails dbObj : legalVerificationDetails) {
                if (StringUtils.equals(colletralObj.getCollateralId(), dbObj.getCollateralId())) {
                    if (!StringUtils.equalsIgnoreCase(dbObj.getStatus(), ThirdPartyVerification.Status.VERIFIED.name())) {
                        dbObj.getLegalVerificationInput().setPropertyAddress(colletralObj.getAddress());
                        dbObj.getLegalVerificationInput().setPropOwnerName(colletralObj.getOwnerNames());
                        dbObj.getLegalVerificationInput().setPropType(colletralObj.getType());
                        dbObj.getLegalVerificationInput().setPropUsage(colletralObj.getUsage());
                        dbObj.setLegalVerificationOutput(new LegalVerificationOutput());
                        dbObj.getLegalVerificationOutput().setOwnerNameOnPapers(colletralObj.getOwnerNames());
                        dbObj.setApplicantName(applicantName);
                        if (dbObj.getStatus() == null)
                            dbObj.setStatus(ThirdPartyVerification.Status.PENDING.name());
                    }
                    addNewObject = false;
                    break;
                }
            }
            if (addNewObject) {
                LegalVerificationDetails newObj = new LegalVerificationDetails();
                newObj.setCollateralId(colletralObj.getCollateralId());
                newObj.setLegalVerificationInput(new LegalVerificationInput());
                newObj.setLegalVerificationOutput(new LegalVerificationOutput());
                newObj.getLegalVerificationInput().setPropertyAddress(colletralObj.getAddress());
                newObj.getLegalVerificationInput().setPropOwnerName(colletralObj.getOwnerNames());
                newObj.getLegalVerificationInput().setPropType(colletralObj.getType());
                newObj.getLegalVerificationInput().setPropUsage(colletralObj.getUsage());
                newObj.getLegalVerificationOutput().setOwnerNameOnPapers(colletralObj.getOwnerNames());
                newObj.setApplicantName(applicantName);
                newObj.setStatus(ThirdPartyVerification.Status.PENDING.name());
                legalVerificationDetails.add(newObj);
            }
        }

        legalVerification.setLegalVerificationDetailsList(legalVerificationDetails);
        legalVerificationRequest.setLegalVerification(legalVerification);
        Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
        applicationRepository.saveLegalVerificationMultiDetails(legalVerificationRequest, verificationStatusDealersMap);
        /*appConfigurationHelper.intimateUsers(legalVerificationRequest.getRefId(),
                IntimationConstants.VERIFICATION_TRIGGER, IntimationConstants.VER_TYPE_LEGAL ,verificationStatusDealersMap);*/
    }

    public void updateApplicationRequestStage(ApplicationRequest applicationRequest) {
        applicationRequest.setCurrentStageId(applicationRepository.getApplicationStage(
                applicationRequest.getHeader().getInstitutionId(), applicationRequest.getRefID()));
    }

    /*public void updateApplicantNameOfPropertyVisit(ApplicationRequest applicationRequest) {
        PropertyVisitRequest propertyVisitRequest = new PropertyVisitRequest();
        propertyVisitRequest.setHeader(applicationRequest.getHeader());
        propertyVisitRequest.setRefId(applicationRequest.getRefID());

        PropertyVisit propertyVisit = applicationRepository.fetchPropertyVisitDetailsByRefId(propertyVisitRequest);

        if(propertyVisit == null){
            propertyVisit = new PropertyVisit();
            propertyVisit.setRefId(applicationRequest.getRefID());
            propertyVisit.setInstitutionId(applicationRequest.getHeader().getInstitutionId());
        }
        propertyVisit.setApplicantName(applicationRequest.getRequest().getApplicant().getApplicantName());
        propertyVisitRequest.setPropertyVisit(propertyVisit);

        applicationRepository.savePropertyVisitDetails(propertyVisitRequest);
    }*/

    public void savePropertyVerificationDetails(ApplicationRequest applicationRequest) {
        VerificationRequest verificationRequest = new VerificationRequest();
        verificationRequest.setRefId(applicationRequest.getRefID());
        verificationRequest.setHeader(applicationRequest.getHeader());
        VerificationDetails verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(verificationRequest);

        List<PropertyVerification> residenceVerification;
        List<PropertyVerification> officeVerification;

        if (verificationDetails == null) {
            verificationDetails = new VerificationDetails();
            residenceVerification = new ArrayList<>();
            officeVerification = new ArrayList<>();
        } else {
            residenceVerification = verificationDetails.getResidenceVerification();
            officeVerification = verificationDetails.getOfficeVerification();

            if (residenceVerification == null) {
                residenceVerification = new ArrayList<>();
            }
            if (officeVerification == null) {
                officeVerification = new ArrayList<>();
            }
        }
        copyAddresses(applicationRequest, residenceVerification, officeVerification);

        for(PropertyVerification propertyVerification : residenceVerification ){
            setApplicantName(propertyVerification, applicationRequest);

        }
        if(CollectionUtils.isNotEmpty(officeVerification)){
            for(PropertyVerification propertyVerification : officeVerification ){
                setApplicantName(propertyVerification, applicationRequest);
            }
        }
        // Save
        verificationDetails.setResidenceVerification(residenceVerification);
        verificationDetails.setOfficeVerification(officeVerification);
        verificationRequest.setVerificationDetails(verificationDetails);

        Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_TRIGGERED, new HashSet<>());
        verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());
        applicationRepository.saveVerificationDetails(applicationRequest.getRefID(),verificationRequest.getHeader(), EndPointReferrer.RESIDENCE_VERIFICATION,verificationRequest.getVerificationDetails() ,verificationStatusDealersMap,applicationRequest);
        applicationRepository.saveVerificationDetails(applicationRequest.getRefID(),verificationRequest.getHeader(), EndPointReferrer.OFFICE_VERIFICATION,verificationRequest.getVerificationDetails() ,verificationStatusDealersMap,applicationRequest);
    }

    private void setApplicantName(PropertyVerification propertyVerification, ApplicationRequest applicationRequest) {
        if(StringUtils.equalsIgnoreCase(propertyVerification.getStatus(),ThirdPartyVerification.Status.PENDING.name())){
            if (StringUtils.equalsIgnoreCase(applicationRequest.getRequest().getApplicant().getApplicantId(),propertyVerification.getApplicantId())){
                propertyVerification.setApplicantName(applicationRequest.getRequest().getApplicant().getApplicantName());
            }else{
                for(CoApplicant coApplicant  : applicationRequest.getRequest().getCoApplicant()){
                    if (StringUtils.equalsIgnoreCase(coApplicant.getApplicantId(),propertyVerification.getApplicantId())){
                        propertyVerification.setApplicantName(coApplicant.getApplicantName());
                    }
                }
            }

        }
    }

    private void copyAddresses(ApplicationRequest applicationRequest, List<PropertyVerification> residenceVerification,
                               List<PropertyVerification> officeVerification) {
        List<CustomerAddress> addressList;
        String productName = applicationRequest.getHeader().getProduct().toProductName();
        Applicant applicant = applicationRequest.getRequest().getApplicant();

        // For applicant's addresses
        addressList = applicationRequest.getRequest().getApplicant().getAddress();
        copyAddresses(applicant, addressList, productName, residenceVerification, officeVerification);

        // For Coapplicants' addresses
        List<CoApplicant> coApplicantList = applicationRequest.getRequest().getCoApplicant();
        if (coApplicantList != null) {
            for (CoApplicant coApplicant : coApplicantList) {
                addressList = coApplicant.getAddress();
                if (CollectionUtils.isNotEmpty(addressList)) {
                    copyAddresses(coApplicant, addressList, productName, residenceVerification, officeVerification);
                }
            }
        }
    }

    private void copyAddresses(Applicant applicant, List<CustomerAddress> addressList, String productName,
                               List<PropertyVerification> residenceVerification,
                               List<PropertyVerification> officeVerification) {
        /* Copy the address which are newly filled in; But do not copy for VERIFIED addresses
                     Fetch all the address office and RV from db
                     Exclude VERIFIED addresses and add/overwrite other addresses, based on applicant name
            */
        //Code to fix remove existing data of rv/ov in case of change
        if(CollectionUtils.isNotEmpty(residenceVerification)){
            if(CollectionUtils.isEmpty(addressList.stream().filter(addr -> addr.getAddressType().equals(EndPointReferrer.ADDRESS_TYPE_RESIDENCE))
                    .collect(Collectors.toList()))){
                PropertyVerification verficationObj = null;
                for(PropertyVerification obj : residenceVerification){
                    if(StringUtils.equalsIgnoreCase(obj.getApplicantId(), applicant.getApplicantId())){
                        verficationObj = obj;
                        break;
                    }

                }
                if(verficationObj != null){
                    residenceVerification.remove(verficationObj);
                }
            }
        }
        if(CollectionUtils.isNotEmpty(officeVerification)){
            if(CollectionUtils.isEmpty(addressList.stream().filter(addr -> addr.getAddressType().equals(EndPointReferrer.ADDRESS_TYPE_OFFICE))
                    .collect(Collectors.toList()))){
                PropertyVerification verficationObj = null;
                for(PropertyVerification obj : officeVerification){
                    if(StringUtils.equalsIgnoreCase(obj.getApplicantId(), applicant.getApplicantId())){
                        verficationObj = obj;
                        break;
                    }

                }
                if(verficationObj != null){
                    officeVerification.remove(verficationObj);
                }
            }
        }

        boolean emptyOfficeList = CollectionUtils.isEmpty(officeVerification);
        boolean emptyResiList = CollectionUtils.isEmpty(residenceVerification);
        List<PropertyVerification> thisApplcntResi = new ArrayList<>();
        List<PropertyVerification> thisApplcntOffices = new ArrayList<>();
        if (!emptyResiList) {
            thisApplcntResi = residenceVerification.stream()
                    .filter(property -> StringUtils.equalsIgnoreCase(property.getApplicantId(),
                            applicant.getApplicantId())
                    ).collect(Collectors.toList());
        }

        if (!emptyOfficeList) {
            thisApplcntOffices = officeVerification.stream()
                    .filter(property -> StringUtils.equalsIgnoreCase(property.getApplicantId(),
                            applicant.getApplicantId())
                    ).collect(Collectors.toList());
        }

        for (CustomerAddress customerAddress : addressList) {
            if (customerAddress.getAddressType().equals(EndPointReferrer.ADDRESS_TYPE_RESIDENCE)) {
                if (emptyResiList) {
                    residenceVerification.add(copyAddresses(applicant, customerAddress, productName));
                } else {
                    if (CollectionUtils.isEmpty(thisApplcntResi)) {
                        residenceVerification.add(copyAddresses(applicant, customerAddress, productName));
                    } else {
                        //replace on;y if status is nonverified; so check status and then replace
                        if (!StringUtils.equalsIgnoreCase(thisApplcntResi.get(0).getStatus(),
                                ThirdPartyVerification.Status.VERIFIED.name())) {
                            thisApplcntResi.get(0).getInput().setAddress(customerAddress);
                            thisApplcntResi.get(0).getInput().setPhone(applicant.getPhone());
                            thisApplcntResi.get(0).getInput().setProduct(productName);
                        }
                    }
                }
            } else if (customerAddress.getAddressType().equals(EndPointReferrer.ADDRESS_TYPE_OFFICE)) {
                if (emptyOfficeList) {
                    officeVerification.add(copyAddresses(applicant, customerAddress, productName));
                } else {
                    if (CollectionUtils.isEmpty(thisApplcntOffices)) {
                        officeVerification.add(copyAddresses(applicant, customerAddress, productName));
                    } else {//replace on;y if status is nonverified; so check status and then replace
                        if (!StringUtils.equalsIgnoreCase(thisApplcntOffices.get(0).getStatus(),
                                ThirdPartyVerification.Status.VERIFIED.name())) {
                            thisApplcntOffices.get(0).getInput().setAddress(customerAddress);
                            thisApplcntOffices.get(0).getInput().setPhone(applicant.getPhone());
                            thisApplcntOffices.get(0).getInput().setProduct(productName);
                        }
                    }
                }
            }
        }
    }

    private PropertyVerification copyAddresses(Applicant applicant, CustomerAddress address, String productName) {
        PropertyVerification property = new PropertyVerification();
        PropertyVerificationInput input = new PropertyVerificationInput();

        property.setApplicantId(applicant.getApplicantId());
        property.setApplicantName(applicant.getApplicantName());

        input.setAddress(address);
        input.setPhone(applicant.getPhone());
        input.setProduct(productName);
        property.setInput(input);
        property.setStatus(ThirdPartyVerification.Status.PENDING.name());
        return property;
    }

    public void presaveEligibilityDetails(ApplicationRequest applicationRequest, String execPoint) {
        EligibilityRequest eligibilityRequest = new EligibilityRequest();
        eligibilityRequest.setRefId(applicationRequest.getRefID());
        eligibilityRequest.setHeader(applicationRequest.getHeader());
        EligibilityDetails eligibilityDetails = applicationRepository.fetchEligibilityDetails(applicationRequest.getRefID(),
                applicationRequest.getHeader().getInstitutionId());
        if(eligibilityDetails == null)
            eligibilityDetails = new EligibilityDetails();
        eligibilityDetails.setAppliedTenor(applicationRequest.getRequest().getApplication().getTenorRequested());
        eligibilityDetails.setAppliedLoan(applicationRequest.getRequest().getApplication().getLoanAmount());
        for(Employment obj : applicationRequest.getRequest().getApplicant().getEmployment()) {
            eligibilityDetails.setMonthlySalary(obj.getMonthlySalary());
        }

        switch (execPoint) {

            case EndPointReferrer.STEP_DEMOGRAPHIC_DETAILS:
                List<IncomeDetail> incomeDetailList = new ArrayList<>();
                if (applicationRequest.getRequest().getApplicant() != null) {
                    IncomeDetail incomeDetail = new IncomeDetail();
                    incomeDetail.setApplicantType(applicationRequest.getRequest().getApplicant().getApplicantType());
                    incomeDetail.setIncomeConsidered(applicationRequest.getRequest().getApplicant().isConsentToCall());
                    incomeDetailList.add(incomeDetail);
                }
                if (applicationRequest.getRequest().getCoApplicant() != null) {
                    IncomeDetail incomeDetail = new IncomeDetail();
                    incomeDetail.setApplicantType(applicationRequest.getRequest().getApplicant().getApplicantType());
                    incomeDetail.setIncomeConsidered(applicationRequest.getRequest().getApplicant().isConsentToCall());
                    incomeDetailList.add(incomeDetail);
                }
                eligibilityDetails.setIncomeDetailList(incomeDetailList);
                break;

            case EndPointReferrer.STEP_PROFESSION_INCOME_DETAILS:
                List<IncomeDetail> incomeDetailList1 = new ArrayList<>();
                if (applicationRequest.getRequest().getApplicant() != null) {
                    IncomeDetail incomeDetail = new IncomeDetail();
                    incomeDetail.setApplicantType(applicationRequest.getRequest().getApplicant().getApplicantType());
                    incomeDetail.setIncomeConsidered(applicationRequest.getRequest().getApplicant().isConsentToCall());
                    incomeDetailList1.add(incomeDetail);
                }
                if (applicationRequest.getRequest().getCoApplicant() != null) {
                    IncomeDetail incomeDetail = new IncomeDetail();
                    incomeDetail.setApplicantType(applicationRequest.getRequest().getApplicant().getApplicantType());
                    incomeDetail.setIncomeConsidered(applicationRequest.getRequest().getApplicant().isConsentToCall());
                    incomeDetailList1.add(incomeDetail);
                }
                eligibilityDetails.setIncomeDetailList(incomeDetailList1);
                break;
        }

        eligibilityRequest.setEligibilityDetails(eligibilityDetails);
        applicationRepository.saveEligibilityDetails(eligibilityRequest);
    }

    public void preSaveRepaymentDetails(ApplicationRequest applicationRequest) {
        RepaymentRequest repaymentRequest = new RepaymentRequest();
        repaymentRequest.setRefId(applicationRequest.getRefID());
        repaymentRequest.setHeader(applicationRequest.getHeader());
        Repayment repayment = new Repayment();
        List<RepaymentDetails> repaymentDetailsList = new ArrayList<>();
        RepaymentDetails repaymentDetails = new RepaymentDetails();
        applicationRequest.getRequest().getApplicant().getBankingDetails().forEach(obj -> {
            repaymentDetails.setBankName(obj.getBankName());
            repaymentDetails.setAccountType(obj.getAccountType());
            repaymentDetails.setIfscCode(obj.getIfscCode());
            repaymentDetails.setAccountNumber(obj.getAccountNumber());
            repaymentDetails.setAccountHolderName(obj.getAccountHolderName());
            repaymentDetails.setBranchaddress(obj.getBankAddress());
            repaymentDetailsList.add(repaymentDetails);

        });
        repayment.setRepaymentDetailsList(repaymentDetailsList);
        repayment.setRefId(repaymentRequest.getRefId());
        repayment.setInstitutionId(repaymentRequest.getHeader().getInstitutionId());
        repaymentRequest.setRepayment(repayment);
        applicationRepository.saveRepaymentDetails(repayment);

    }

    public void preSaveCamSummary(ApplicationRequest applicationRequest) {
        try {
            List<CustomerAddress> customerAddressesList = applicationRequest.getRequest().getApplicant().getAddress();
            int noOfYearAtResidence = 0;
            for (CustomerAddress obj : customerAddressesList) {
                noOfYearAtResidence = obj.getTimeAtAddress();
            }
            applicationRepository.saveCamSummary(applicationRequest, noOfYearAtResidence);
        } catch (Exception e) {
            logger.error("Exception caught while saving camDetails in preSAveCamSummary " + e.getStackTrace());
            logger.error("{}",e.getStackTrace());
        }

    }

    public void preSaveCamSummary(ApplicationRequest applicationRequest, String product) {
        logger.info("inside preSave Cam Summary method");
        CamDetails camDetails = null;
        CamSummary camSummary = null;
        try {
            List<CustomerAddress> customerAddressesList = applicationRequest.getRequest().getApplicant().getAddress();
            int noOfYearAtResidence = 0;
            String dtJoinDate = null ;
            for (CustomerAddress obj : customerAddressesList) {
                noOfYearAtResidence = obj.getTimeAtAddress();
            }
            List<Employment> employments = applicationRequest.getRequest().getApplicant().getEmployment();
            if(CollectionUtils.isNotEmpty(employments)){
                for (Employment obj : employments) {
                    dtJoinDate = obj.getDateOfJoining();
                }
            }
            camDetails = applicationRepository.fetchCamDetails(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId(),EndPointReferrer.CAM_SUMMARY);
            if(camDetails == null){
                camDetails = new CamDetails();
                camSummary = new CamSummary();
                if(null != applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails()) {
                    camSummary.setScheme(applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme());
                    camSummary.setIndustry(applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getBusiness());
                }
                camSummary.setDateOfJoining(dtJoinDate);
                camSummary.setNoOfYearAtResidence(noOfYearAtResidence);
            }else {
                camSummary = camDetails.getSummary();
                if(camSummary == null){
                    camSummary = new CamSummary();
                }
                if(null != applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails()) {
                    camSummary.setScheme(applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getLoanScheme());
                    camSummary.setIndustry(applicationRequest.getRequest().getApplicant().getProfessionIncomeDetails().getBusiness());
                }
                camSummary.setDateOfJoining(dtJoinDate);
                camSummary.setNoOfYearAtResidence(noOfYearAtResidence);
            }
            camDetails.setSummary(camSummary);
            applicationRepository.saveCamSummaryDetails(applicationRequest, camDetails);
        } catch (Exception e) {
            logger.error("refId:{} Exception caught while saving camDetails in preSAveCamSummary: msg:{} stacktrace:{} "
                    , applicationRequest.getRefID(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }

    }

    public void preSaveRTRDetails(ApplicationRequest applicationRequest){
        try {
            CamDetails camDetails = applicationRepository.fetchCamDetails(applicationRequest.getRefID(), applicationRequest.getHeader().getInstitutionId(), EndPointReferrer.CAM_RTR_DETAILS);
            if (camDetails == null) camDetails = new CamDetails();

            Applicant applicant = applicationRequest.getRequest().getApplicant();
            List<CoApplicant> coApplicantList = applicationRequest.getRequest().getCoApplicant();
            if(CollectionUtils.isEmpty(coApplicantList))
                coApplicantList = new ArrayList<>();

            if (CollectionUtils.isEmpty(camDetails.getRtrDetailList())) {
                List<RTRDetail> rtrDetailList = new ArrayList<>();

                //New Applicant
                rtrDetailList.add(addRTRDetail(applicant));

                //New CoApplicant
                for (CoApplicant coApplicant : coApplicantList) {
                    rtrDetailList.add(addRTRDetail(coApplicant));
                }
                camDetails.setRtrDetailList(rtrDetailList);
            } else {
                //Applicant and CoApplicant appIds from GoNoGo
                Set<String> appIds = new HashSet<>();
                appIds.add(applicant.getApplicantId());
                coApplicantList.forEach(obj -> {
                    appIds.add(obj.getApplicantId());
                });

                List<RTRDetail> removedRTR = new ArrayList<>();
                for (RTRDetail rtrDetail : camDetails.getRtrDetailList()) {
                    if (StringUtils.isNotEmpty(rtrDetail.getApplicantId())) {

                        // Preparing for delete which are not available as Applicant/CoApplicant
                        if (!appIds.contains(rtrDetail.getApplicantId())) {
                            removedRTR.add(rtrDetail);
                            continue;
                        }

                        if (StringUtils.equalsIgnoreCase(applicant.getApplicantId(), rtrDetail.getApplicantId()))
                            rtrDetail.setApplicantName(applicationRequest.getRequest().getApplicant().getApplicantName()); //Updating Applicant Name
                        else {
                            for (CoApplicant coApplicant : coApplicantList) {
                                if (StringUtils.equalsIgnoreCase(coApplicant.getApplicantId(), rtrDetail.getApplicantId()))
                                    rtrDetail.setApplicantName(coApplicant.getApplicantName()); //Updating CoApplicantName
                            }
                        }
                    }
                }

                if (CollectionUtils.isNotEmpty(removedRTR))
                    camDetails.getRtrDetailList().removeAll(removedRTR);

                //appIds from RTRDetailsList.
                Set<String> rtrAppIds = new HashSet<>();
                camDetails.getRtrDetailList().forEach(obj -> {
                    rtrAppIds.add(obj.getApplicantId());
                });

                //To add extra Added Co-Applicants
                for (CoApplicant coApplicant : coApplicantList) {
                    if (!rtrAppIds.contains(coApplicant.getApplicantId()))
                        camDetails.getRtrDetailList().add(addRTRDetail(coApplicant));
                }
            }

            CamDetailsRequest camDetailsRequest = new CamDetailsRequest();
            camDetailsRequest.setRefId(applicationRequest.getRefID());
            camDetailsRequest.setHeader(applicationRequest.getHeader());
            camDetailsRequest.setCamDetails(camDetails);
            applicationRepository.saveCamDetails(camDetailsRequest, EndPointReferrer.CAM_RTR_DETAILS);
        } catch (Exception e) {
            logger.error("refId:{} Exception caught while saving camDetails in preSaveRTRDetails: msg:{} stacktrace:{} "
                    , applicationRequest.getRefID(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
    }

    private RTRDetail addRTRDetail(Applicant applicant){
        RTRDetail rtrDetail = new RTRDetail();
        rtrDetail.setApplicantName(applicant.getApplicantName());
        rtrDetail.setApplicantId(applicant.getApplicantId());
        return  rtrDetail;
    }

    public void callExternalInterfaces(List<String> interfaces, GoNoGoCustomerApplication goNoGoCustomerApplication,
                                       String loggedInUserRole, String loggedInUserId, String executionPoint) {
        try {
            for (String extInterface : interfaces) {
                if (ConfigurationConstants.EXTERNAL_SERVICE_MIFIN.equals(extInterface)) {
                    goNoGoCustomerApplication = callMiFin(goNoGoCustomerApplication, loggedInUserRole, loggedInUserId);
                    applicationRepository.updateGoNoGoCustomerApplication(goNoGoCustomerApplication.getApplicationRequest());
                }else if (ConfigurationConstants.EXTERNAL_SERVIC_EMI_CAL.equals(extInterface)) {
                    try{
                        if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().isCoOrigination()){
                            originationHelper.populateMisReleatedData(goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                        }
                    }catch(Exception ex){
                        logger.error("{} error while populatatin Data {} ", ExceptionUtils.getStackTrace(ex));
                    }
                }else if(StringUtils.equalsIgnoreCase(extInterface,ConfigurationConstants.EXTERNAL_SERVICE_PREMIUM_CALCULATOR)){
                    InsuranceRequest insuranceRequest = new InsuranceRequest();
                    insuranceRequest.setRefId(goNoGoCustomerApplication.getApplicationRequest().getRefID());
                    insuranceRequest.setHeader(goNoGoCustomerApplication.getApplicationRequest().getHeader());
                    insuranceRequest.setInsuranceData(new InsuranceData());
                    insuranceRequest.getInsuranceData().setCurrentCnt(0);
                    populateApplicantList(insuranceRequest,goNoGoCustomerApplication.getApplicationRequest());
                    if(CollectionUtils.isNotEmpty(insuranceRequest.getInsuranceData().getApplicantList())){
                        dataEntryManager.calculatePremiumAmount(insuranceRequest);
                    }
                }else if (ConfigurationConstants.EXTERNAL_SERVICE_LMS.equals(extInterface)) {
                    logger.info("Inside EXTERNAL_SERVICE_LMS");
                    DisburseResponse disburseResponse = new DisburseResponse();
                    SBFCLMSIntegrationRequest sbfclmsIntegrationRequest = new SBFCLMSIntegrationRequest();
                    sbfclmsIntegrationRequest.setRefID(goNoGoCustomerApplication.getApplicationRequest().getRefID());
                    sbfclmsIntegrationRequest.setOHeader(goNoGoCustomerApplication.getApplicationRequest().getHeader());
                    disburseResponse = lmsIntegrationManager.dataPush(sbfclmsIntegrationRequest);
                    logger.info("DisburseResponse: ", disburseResponse);
                } else if(StringUtils.equalsIgnoreCase(extInterface,ConfigurationConstants.EXTERNAL_SERVICE_ICICI_CORPORATE_API)){
                    if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().isCoOrigination()) {
                        logger.info("ICICI Corporate API invoked for refId {}",goNoGoCustomerApplication.getApplicationRequest().getRefID());
                        originationHelper.pushCorporateAPIDetails(goNoGoCustomerApplication.getApplicationRequest().getRefID());
                    }
                }else if(ConfigurationConstants.QUICK_CHECK_API.equals(extInterface)){
                    if(StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductName(), Product.DIGI_PL.toProductName())
                            && (StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getChannel(), "Online"))
                            && (Stages.isTranchStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId()))
                            && Buckets.isSTPBucket(goNoGoCustomerApplication.getApplicationBucket())){
                        QuickCheckExecutor quickCheckExecutor = new QuickCheckExecutor();
                        quickCheckExecutor.process(goNoGoCustomerApplication);
                        quickCheckExecutor.start();
                    }
                } else if (ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_APPLICANTS.equals(extInterface)) {
                    try {
                        externalApiManager.callToMiFin(goNoGoCustomerApplication.getApplicationRequest(), loggedInUserRole, loggedInUserId);
                    } catch (Exception e) {
                        logger.error("Error while saving details to mifin [{}]", e.getMessage());
                    }
                }else if (ConfigurationConstants.EXTERNAL_SERVICE_AMBIT_MIFIN_SAVE_DATA.equals(extInterface)){
                    try {
                        externalApiManager.callToMiFinSaveData(goNoGoCustomerApplication, loggedInUserRole, loggedInUserId);
                    } catch (Exception e) {
                        logger.error("Error while saving details to mifin [{}]", e.getMessage());
                    }
                }else if(ConfigurationConstants.EXTERNAL_SERVICE_SBFC_MIFIN_IMD.equals(extInterface)){
                    if(StringUtils.isNotEmpty(goNoGoCustomerApplication.getApplicationRequest().getMifinImd2Message())){
                        goNoGoCustomerApplication.getApplicationRequest().setMifinImd2Message(null);
                    }
                    List<InitialMoneyDeposit> imdList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getImd2();
                    for (InitialMoneyDeposit imd2 : imdList) {
                        if (StringUtils.equalsIgnoreCase("Cleared", imd2.getStatus()) && imd2.isCleared()) {
                            try {
                                externalApiManager.pushIMDDataToMifin(goNoGoCustomerApplication, imd2);
                            } catch (Exception e) {
                                goNoGoCustomerApplication.getApplicationRequest().setMifinImd2Status(MiFinHelper.STATUS_FAILED);
                                goNoGoCustomerApplication.getApplicationRequest().setMifinImd2Message(StringUtils.isNotEmpty(e.getMessage())
                                        ? e.getMessage() : "Error while calling IMD_DETAIL_API");
                                imd2.setStatus("Pending");
                                imd2.setCleared(false);
                                logger.error("Error while calling IMD_DETAIL_API call for refId {}, error {}",goNoGoCustomerApplication.getGngRefId(), e.getMessage());
                            }
                        }
                    }
                    applicationRepository.updateGoNoGoCustomerApplication(goNoGoCustomerApplication.getApplicationRequest());
                }
            }
        } catch (Exception e) {
            logger.error("Error occoured while calling External API {} for RefID {} on ({}) execution - {}",
                    interfaces, goNoGoCustomerApplication.getGngRefId(), executionPoint, e.getStackTrace());
            logger.error("{}",e.getStackTrace());
        }
    }

    private void populateApplicantList(InsuranceRequest insuranceRequest, ApplicationRequest applicationRequest) {
        List <Applicant> applicantList = new ArrayList<>();
        List<InsurancePolicy> insurancePolicyList = null;
        InsurancePolicy insurancePolicy = null;
        if(applicationRequest.getRequest().getApplicant() != null &&
          ApplicantType.isIndividual(applicationRequest.getRequest().getApplicant().getApplicantType())){
            insurancePolicyList = new ArrayList<>();
            insurancePolicy = new InsurancePolicy();
            insurancePolicy = policyHelper.populateInsuranceData(insurancePolicy,ReligareHelper.RELIGARE_LOAN_PROTECTION);
            insurancePolicyList.add(insurancePolicy);
            applicationRequest.getRequest().getApplicant().setInsurancePolicyList(insurancePolicyList);
            applicantList.add(applicationRequest.getRequest().getApplicant());
        }
        if(CollectionUtils.isNotEmpty(applicationRequest.getRequest().getCoApplicant())){
            for(Applicant applicant : applicationRequest.getRequest().getCoApplicant()){
                if (ApplicantType.isIndividual(applicant.getApplicantType())) {
                    insurancePolicyList = new ArrayList<>();
                   /* insurancePolicy = new InsurancePolicy();
                    insurancePolicy = policyHelper.populateInsuranceData(insurancePolicy,IciciHelper.ICICI_COMPANY_NAME);
                    insurancePolicyList.add(insurancePolicy);*/
                    applicant.setInsurancePolicyList(insurancePolicyList);
                    applicantList.add(applicant);
                }
            }
        }
        if(insuranceRequest.getInsuranceData() == null){
            insuranceRequest.setInsuranceData(new InsuranceData()) ;
        }
        insuranceRequest.getInsuranceData().setApplicantList(applicantList);
    }


    private GoNoGoCustomerApplication callMiFin(GoNoGoCustomerApplication goNoGoCustomerApplication, String loggedInUserRole, String loggedInUserId) throws Exception {
        /* HOPS submits DISB case to miFin.
                        DISB is last stage of the application.
                    */
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        String currentStage = applicationRequest.getCurrentStageId();
        String refId = goNoGoCustomerApplication.getGngRefId();
        String institutionId = applicationRequest.getHeader().getInstitutionId();
        String applicationStatus = goNoGoCustomerApplication.getApplicationStatus();
        String executionPoint;
        if (currentStage.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue())) {
           // applicationRequest.setCurrentStageId(GNGWorkflowConstant.PUSH_TO_LMS.name());
            // update DM details
            dataEntryManager.updateDM(applicationRequest);
            // update step id
            executionPoint = "submit-by-" + loggedInUserRole;
            applicationRepository.updateCompletedInfo(refId, institutionId, executionPoint, loggedInUserId, loggedInUserRole);
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            // Case has reached to end of processing; push data to LMS system.
            dataEntryManager.externalAPICall(goNoGoCustomerApplication, applicationRequest);
            //reverting to previous stage if Mifin push failed.
            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(),
                    goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                logger.info("Attention {} ...Revering to stage HOPS and status Approved!!", applicationRequest.getRefID());
                applicationStatus = GNGWorkflowConstant.APPROVED.toFaceValue();
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            }
            // update application status to Disbursed if pushed to LMS
            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DISB.toFaceValue(),
                    goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                applicationStatus = GNGWorkflowConstant.DISBURSED.toFaceValue();
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            }
            dataEntryManager.updateStageId(applicationRequest, applicationStatus);
        }else if(StringUtils.equalsIgnoreCase(applicationRequest.getHeader().getProduct().toProductName(), Product.DIGI_PL.toProductName())
                && ((StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DE.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())
                && Buckets.isSTPBucket(goNoGoCustomerApplication.getApplicationBucket()))
                || StringUtils.equalsIgnoreCase(GNGWorkflowConstant.TRANCH_HOPS.toFaceValue(),
                goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId()))){
            // applicationRequest.setCurrentStageId(GNGWorkflowConstant.PUSH_TO_LMS.name());
            // update DM details
            dataEntryManager.updateDM(applicationRequest);
            // update step id
            executionPoint = "submit-by-" + loggedInUserRole;
            applicationRepository.updateCompletedInfo(refId, institutionId, executionPoint, loggedInUserId, loggedInUserRole);
            goNoGoCustomerApplication = applicationRepository.getGoNoGoCustomerApplicationByRefId(refId);
            goNoGoCustomerApplication.getApplicationRequest().setFinalTranchCancelled(applicationRequest.isFinalTranchCancelled());
            goNoGoCustomerApplication.getApplicationRequest().setFinalTranchCancelledRemarks(applicationRequest.getFinalTranchCancelledRemarks());
            // Case has reached to end of processing; push data to LMS system.
            dataEntryManager.externalAPICall(goNoGoCustomerApplication, applicationRequest);
            //reverting to previous stage if Mifin push failed.
            if(Stages.isTranchStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())){
                applicationStatus = GNGWorkflowConstant.TRANCH_DISB.toFaceValue();
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            }
            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.HOPS.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())
                    || StringUtils.equalsIgnoreCase(GNGWorkflowConstant.BOPS.toFaceValue(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                logger.info("Attention {} ...Revering to stage HOPS and status Approved!!", applicationRequest.getRefID());
                applicationStatus = GNGWorkflowConstant.APPROVED.toFaceValue();
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            }
            // update application status to Disbursed if pushed to LMS
            if (StringUtils.equalsIgnoreCase(GNGWorkflowConstant.DISB.toFaceValue(),
                    goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId())) {
                applicationStatus = GNGWorkflowConstant.DISBURSED.toFaceValue();
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            }
            dataEntryManager.updateStageId(applicationRequest, applicationStatus);
        } else {
            throw new GoNoGoException("Submitted at wrong stage");
        }
        // update step id
        applicationRepository.updateCompletedInfo(refId, institutionId, executionPoint, loggedInUserId, loggedInUserRole);
        GoNoGoCustomerApplication gng = goNoGoCustomerApplication;
        gng.setApplicationRequest(applicationRequest);
        return gng;
        
    }
    /**
     * Copies city from applicant's address into application
     * */
    public void setcity(ApplicationRequest applicationRequest) {
        if( StringUtils.isEmpty(applicationRequest.getRequest().getApplication().getCity() ) ) {
            if (CollectionUtils.isNotEmpty(applicationRequest.getRequest().getApplicant().getAddress())) {
                applicationRequest.getRequest().getApplication().setCity(
                        applicationRequest.getRequest().getApplicant().getAddress().get(0).getCity());
            }
        }
    }


private void call3piExecutor(String refID, List<AdroitCallLog> ackIds, WFJobCommDomain serviceConfig, Valuation valuation) {
        /*      Iterate through ackIds
                    Find out the valuation detail
                    Create an executor with valuation detail
                Wait till all threads end
                Iterate thr ackIds
                    Check the valuation detail list for the collateral from ackvalue
                    if output is filled then change the statust
        * */
        List<ValuationDetails> valDetailList = valuation.getValuationDetailsList();
        String agencyCode = serviceConfig.getMemberId();
        ExecutorService es = Executors.newCachedThreadPool();
        for( AdroitCallLog adroitCallLog : ackIds ){
            ValuationDetails valuationDetails = valDetailList.stream()
                    .filter(valDtl -> StringUtils.equalsIgnoreCase( valDtl.getAgencyCode(), agencyCode)
                            &&  StringUtils.equalsIgnoreCase( valDtl.getCollateralId(), adroitCallLog.getCollateralId())
                            && StringUtils.equals(valDtl.getStatus(), ThirdPartyVerification.Status.SUBMITTED.name()) )
                    .collect(Collectors.toList()).get(0);

            // Set acknowledgmentId so that it will be useful later.
            valuationDetails.setAcknowledgmentId( adroitCallLog.getAcknowledgementId());

            // Create executor and run it
            ThirdPartyExecutor executor = new ThirdPartyExecutor();
            executor.init(refID, valuation.getInstitutionId(), serviceConfig, valuationDetails, adroitCallLog);
            es.execute(executor);
        }
        es.shutdown();
        // Wait till all tasks are done
        try {
            boolean finshed = es.awaitTermination(2, TimeUnit.MINUTES);
        } catch (InterruptedException ie){
            logger.error("{}",ie.getStackTrace());
            logger.error("Error while running 3piExecutors {}", ie.getStackTrace());
        }
    }

    public void updateCoOriginationDetails(ApplicationRequest applicationRequest, RoiSchemeMaster roiSchemeMaster) {
        logger.info("Inside updateCoOriginationDetails method");
        CoOrigination coOrigination = null;
        String refId = applicationRequest.getRefID();
        Application application = applicationRequest.getRequest().getApplication();
        String companyName = roiSchemeMaster.getCompanyName();
        // creating request
        OriginationRequest originationRequest = new OriginationRequest();
        originationRequest.setRefID(applicationRequest.getRefID());
        originationRequest.setHeader(applicationRequest.getHeader());
        // fetching previous record if present
        coOrigination = applicationRepository.fetchCoOriginationDetails(refId, applicationRequest.getHeader().getInstitutionId());
        if(coOrigination != null){
            if(coOrigination.getRoiMasterdata() == null){
                CoOriginationDetails roiMaster = new CoOriginationDetails();
                roiMaster.setCompanyName(roiSchemeMaster.getCompanyName());
                roiMaster.setROI(0);
                roiMaster.setExternalPercentage(roiSchemeMaster.getExternalPercentage());
                roiMaster.setInternalPercentage(roiSchemeMaster.getInternalPercentage());
                roiMaster.setScheme(roiSchemeMaster.getSchemeName());
            }else {
                coOrigination.getRoiMasterdata().setROI(roiSchemeMaster.getRoi());
                coOrigination.getRoiMasterdata().setInternalPercentage(roiSchemeMaster.getInternalPercentage());
                coOrigination.getRoiMasterdata().setExternalPercentage(roiSchemeMaster.getExternalPercentage());
                coOrigination.getRoiMasterdata().setScheme(roiSchemeMaster.getSchemeName());
            }
        }else {
            coOrigination = new CoOrigination();
            coOrigination.setRefId(refId);
            coOrigination.setInstitutionId(applicationRequest.getHeader().getInstitutionId());
            List<CoOriginationDetails> coOriginationDetailsList = new ArrayList<>();
            CoOriginationDetails coOriginationDetails = new CoOriginationDetails();
            coOriginationDetails.setCompanyName("SBFC");
            //adding in list
            coOriginationDetailsList.add(coOriginationDetails);
            CoOriginationDetails coOriginationDetails1 = new CoOriginationDetails();
            coOriginationDetails1.setCompanyName("ICICI");
            coOriginationDetailsList.add(coOriginationDetails1);
            CoOriginationDetails coOriginationDetails2 = new CoOriginationDetails();
            coOriginationDetails2.setCompanyName("total");
            coOriginationDetailsList.add(coOriginationDetails2);

            //fetching data from roimaster
            CoOriginationDetails coOriginationDetails3 = new CoOriginationDetails();
            coOriginationDetails3.setScheme(roiSchemeMaster.getSchemeName());
            coOriginationDetails3.setROI(0);
            coOriginationDetails3.setInternalPercentage(roiSchemeMaster.getInternalPercentage());
            coOriginationDetails3.setExternalPercentage(roiSchemeMaster.getExternalPercentage());
            coOriginationDetails3.setCompanyName(roiSchemeMaster.getCompanyName());
            //setting into object
            coOrigination.setRoiMasterdata(coOriginationDetails3);
            coOrigination.setCoOriginationDetailsList(coOriginationDetailsList);

        }
        // saving into db
        originationRequest.setCoOrigination(coOrigination);
        // saving into db
        applicationRepository.saveCoOriginationDetails(originationRequest);
    }

    public static void fixValues(ApplicationRequest applicationRequest) {
        fixAge(applicationRequest);
        fixApplicantType(applicationRequest);
    }

    public static void fixApplicantType(Product product, Applicant applicant) {
        if (StringUtils.isEmpty(applicant.getApplicantType() ) ) {
            if( product == Product.PL ) {// For PL it is always Individual)
                applicant.setApplicantType(ApplicantType.INDIVIDUAL.value());
            } else  if( applicant.getApplicantName() != null ) {
                if (StringUtils.isNotEmpty(applicant.getApplicantName().getFirstName())
                    && applicant.getApplicantName().getFirstName().split(FieldSeparator.SPACE_STR).length > 1) {
                applicant.setApplicantType(ApplicantType.PROPRIETORSHIP.value());
                } else {
                    applicant.setApplicantType(ApplicantType.INDIVIDUAL.value());
                }
            }
        }
    }

    public static void fixApplicantType(ApplicationRequest applicationRequest) {

        Product product = applicationRequest.getHeader().getProduct();
        fixApplicantType(product, applicationRequest.getRequest().getApplicant());
        List<CoApplicant> coApplicant = applicationRequest.getRequest().getCoApplicant();
        if( CollectionUtils.isNotEmpty(coApplicant)) {
            for (Applicant applicant : coApplicant) {
                fixApplicantType(product, applicant);
            }
        }
    }

    public static void fixAge(ApplicationRequest applicationRequest) {
        Request request = applicationRequest.getRequest();
        // Primary applicant's age
        setAge(applicationRequest.getRefID(), request.getApplicant());
        // coapplicant's age
        if( CollectionUtils.isNotEmpty(request.getCoApplicant()) ) {
            for( Applicant applicant : request.getCoApplicant() ){
                setAge(applicationRequest.getRefID(), applicant);
            }
        }
    }

    public static void setAge(String refId, Applicant applicant) {
        try {
            if (applicant.getAge() == 0 && StringUtils.isNotEmpty(applicant.getDateOfBirth()) ) {
                applicant.setAge(GngDateUtil.getAge(applicant.getDateOfBirth()));
            }
        } catch (Exception e){
            logger.error("{} dob calculation failed applicant {} having dob = {} - {}", refId,
                    applicant.getApplicantId(), applicant.getDateOfBirth(), e.getStackTrace());
            logger.error("{}",e.getStackTrace());
        }
    }

    public  PropertyVerification copyAddressesFromRequest(Applicant applicant, CustomerAddress address, String productName , PropertyVerification property) {
        PropertyVerificationInput input = property.getInput();

        property.setApplicantId(applicant.getApplicantId());
        property.setApplicantName(applicant.getApplicantName());

        input.setAddress(address);
        input.setPhone(applicant.getPhone());
        input.setProduct(productName);
        property.setInput(input);
        return property;
    }


    public ValuationInput copyCollateralFromRequest(Collateral collateral, ValuationInput obj) {
        ValuationInput  valuationInput = obj;
        //address mapped to valuation
        valuationInput.setPropertyAddress(collateral.getAddress());
        valuationInput.setPropertyOwnerNames(collateral.getOwnerNames());
        valuationInput.setPropertyType(collateral.getType());
        return valuationInput;
    }

    public LegalVerificationInput copyCollateralFromRequest(Collateral collateral, LegalVerificationInput obj) {
        LegalVerificationInput  legalVerificationInput = obj;

        legalVerificationInput.setPropertyAddress(collateral.getAddress());
        legalVerificationInput.setPropOwnerName(collateral.getOwnerNames());
        legalVerificationInput.setPropType(collateral.getType());
        legalVerificationInput.setPropUsage(collateral.getUsage());

        return legalVerificationInput;
    }

    public void updateApplicantNameOfPropertyVisit(ApplicationRequest applicationRequest) {
        PropertyVisitRequest propertyVisitRequest = new PropertyVisitRequest();
        propertyVisitRequest.setHeader(applicationRequest.getHeader());
        propertyVisitRequest.setRefId(applicationRequest.getRefID());

        Name applicantName = applicationRequest.getRequest().getApplicant().getApplicantName();
        PropertyVisit propertyVisit = applicationRepository.fetchPropertyVisitDetailsByRefId(propertyVisitRequest);

        if(propertyVisit != null &&  CollectionUtils.isNotEmpty(propertyVisit.getPropertyVisitDetailsList())){
            List<PropertyVisitDetails> propertyVisitDetailsList = propertyVisit.getPropertyVisitDetailsList();
            for (PropertyVisitDetails propertyVisitDetail: propertyVisitDetailsList){
                propertyVisitDetail.setApplicantName(applicantName);
            }
            propertyVisitRequest.setPropertyVisit(propertyVisit);
            applicationRepository.updatePropertyVistApplicantName(applicationRequest.getRefID(),applicationRequest.getHeader().getInstitutionId(),applicantName, propertyVisitDetailsList);
        }

    }

    public void updateApplicantNameInPropertyValuation(ApplicationRequest applicationRequest) {

        Valuation valuation = applicationRepository.fetchValuationDetails(applicationRequest.getRefID(),applicationRequest.getHeader().getInstitutionId());
        if(valuation != null){
            List<ValuationDetails> valuationDetailsList =  valuation.getValuationDetailsList();
            if(CollectionUtils.isNotEmpty(valuationDetailsList)){
                for(ValuationDetails valuationDetail: valuationDetailsList){
                    if(StringUtils.equalsIgnoreCase(valuationDetail.getStatus(),ThirdPartyVerification.Status.PENDING.name())){
                        ValuationInput valuationInput = valuationDetail.getValuationInput();
                        valuationInput.setApplicantName(applicationRequest.getRequest().getApplicant().getApplicantName());
                    }
                }
                applicationRepository.saveValuationDetails(valuation,applicationRequest.getRefID(),applicationRequest.getHeader().getInstitutionId());
            }
        }

    }

    public boolean calculatePremium(String refId, Header header, double appliedAmt ,double tenor, String inititationPoint) {
        boolean flag = false;
        try{
            double insuranceAmt = 0;
            double insuranceTenure = 0;
            InsuranceData insuranceData = applicationRepository.fetchInsuranceData(refId);
            if(null != insuranceData && CollectionUtils.isNotEmpty(insuranceData.getApplicantList())){
                for(Applicant applicant : insuranceData.getApplicantList()){
                    if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList()) && !applicant.isWaived()){
                        for(InsurancePolicy insurancePolicy : applicant.getInsurancePolicyList()){
                            if(!insurancePolicy.isException()){
                                if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), IciciHelper.ICICI_COMPANY_NAME)
                                        || StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), ReligareHelper.RELIGARE_LOAN_PROTECTION)
                                        || StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), Constant.COMPANYNAME)){
                                    insuranceAmt = Double.parseDouble(insurancePolicy.getProposedAmount());
                                    insuranceTenure = Double.parseDouble(insurancePolicy.getTenorRequested());
                                    if((appliedAmt != insuranceAmt || insuranceTenure != tenor)  && !insurancePolicy.isException()){
                                        insurancePolicy.setPremiumHit(true);
                                        flag = true;
                                        if(StringUtils.equalsIgnoreCase(inititationPoint, EndPointReferrer.SAVE_LOAN_CHARGES_DETAILS)){
                                            insurancePolicy.setTenorRequested(GngCalculationUtils.setAmount(tenor));
                                            insurancePolicy.setProposedAmount(GngCalculationUtils.setAmount(appliedAmt));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(flag){
                InsuranceRequest insuranceRequest = new InsuranceRequest();
                insuranceData.setInitiationPoint(inititationPoint);
                insuranceRequest.setInsuranceData(insuranceData);
                insuranceRequest.setRefId(refId);
                insuranceRequest.setHeader(header);
                dataEntryManager.calculatePremiumAmount(insuranceRequest);
            }
        } catch (Exception Ex){
            logger.error("Error while calculatePremium during eligibility Screen {}", ExceptionUtils.getStackTrace(Ex));
        }
        return flag;
    }

    public void updateInsurance(ApplicationRequest applicationRequest) {
        try {
            InsuranceData insuranceData = applicationRepository.fetchInsuranceData(applicationRequest.getRefID());
            List<Applicant> applicantListTobeSaved = null;
            List<String> requestId = new ArrayList<>();
            List<String> dbId = new ArrayList<>();
            List<String> deleteId = new ArrayList<>();
            if(!ApplicantType.isIndividual(applicationRequest.getRequest().getApplicant().getApplicantType())){
                deleteId.add(applicationRequest.getRequest().getApplicant().getApplicantId());
            }else{
                requestId.add(applicationRequest.getRequest().getApplicant().getApplicantId());
            }
            if (insuranceData != null) {
                if (CollectionUtils.isNotEmpty(insuranceData.getApplicantList())) {
                    applicantListTobeSaved = insuranceData.getApplicantList();
                    for (Applicant applicant : insuranceData.getApplicantList()) {
                        dbId.add(applicant.getApplicantId());
                    }
                }
                List<CoApplicant> coApplicantList = applicationRequest.getRequest().getCoApplicant();
                if(CollectionUtils.isEmpty(coApplicantList))
                    coApplicantList = new ArrayList<>();

                for (CoApplicant coApplicant : coApplicantList) {
                    if(ApplicantType.isIndividual(coApplicant.getApplicantType())){
                        requestId.add(coApplicant.getApplicantId());
                    }else{
                        deleteId.add(coApplicant.getApplicantId());
                    }
                }

                for (String id : requestId) {
                    if (!dbId.contains(id)) {
                        if(StringUtils.equalsIgnoreCase("0",id)){
                            applicationRequest.getRequest().getApplicant().setInsurancePolicyList(new ArrayList<>());
                            applicantListTobeSaved.add(applicationRequest.getRequest().getApplicant());
                        }
                        for (CoApplicant coApp : coApplicantList) {
                            if (StringUtils.equalsIgnoreCase(coApp.getApplicantId(), id) ) {
                                if(ApplicantType.isIndividual(coApp.getApplicantType())){
                                    coApp.setInsurancePolicyList(new ArrayList<>());
                                    applicantListTobeSaved.add(coApp);
                                }
                            }
                        }
                    }
                }
                for(Applicant insuranceApp : insuranceData.getApplicantList()){
                    if(StringUtils.equalsIgnoreCase(insuranceApp.getApplicantId(), "0")){
                        insuranceApp.setAge(applicationRequest.getRequest().getApplicant().getAge());
                        insuranceApp.setApplicantName(applicationRequest.getRequest().getApplicant().getApplicantName());
                        insuranceApp.setAddress(applicationRequest.getRequest().getApplicant().getAddress());
                        insuranceApp.setDateOfBirth(applicationRequest.getRequest().getApplicant().getDateOfBirth());
                        insuranceApp.setMaritalStatus(applicationRequest.getRequest().getApplicant().getMaritalStatus());
                        insuranceApp.setGender(applicationRequest.getRequest().getApplicant().getGender());
                        insuranceApp.setKyc(applicationRequest.getRequest().getApplicant().getKyc());
                        insuranceApp.setPhone(applicationRequest.getRequest().getApplicant().getPhone());
                    }
                    for(Applicant coAppReq : coApplicantList){
                        if(ApplicantType.isIndividual(coAppReq.getApplicantType())){
                            if(StringUtils.equalsIgnoreCase(insuranceApp.getApplicantId(),coAppReq.getApplicantId())){
                                insuranceApp.setAge(coAppReq.getAge());
                                insuranceApp.setApplicantName(coAppReq.getApplicantName());
                                insuranceApp.setAddress(coAppReq.getAddress());
                                insuranceApp.setDateOfBirth(coAppReq.getDateOfBirth());
                                insuranceApp.setMaritalStatus(coAppReq.getMaritalStatus());
                                insuranceApp.setGender(coAppReq.getGender());
                                insuranceApp.setKyc(coAppReq.getKyc());
                                insuranceApp.setPhone(coAppReq.getPhone());
                            }
                        }
                    }
                }
                if(CollectionUtils.isNotEmpty(deleteId)){
                    deleteApplicantInsurance(applicantListTobeSaved,deleteId);
                }
                if (CollectionUtils.isNotEmpty(applicantListTobeSaved)) {
                    if(applicantListTobeSaved.size() > 1){
                        applicantListTobeSaved.sort(GngUtils.sortByApplicantId);
                    }
                    insuranceData.setApplicantList(applicantListTobeSaved);
                }
                applicationRepository.saveInsuranceData(insuranceData, applicationRequest.getRefID());
            }
        }catch(Exception e){
            logger.error("refId:{} Exception caught in updateInsurance method: msg:{} stacktrace:{} "
                    , applicationRequest.getRefID(), e.getMessage(), ExceptionUtils.getStackTrace(e));
        }
    }

    private void deleteApplicantInsurance(List<Applicant> applicantListTobeSaved, List<String> deleteId) {
        try{
            Iterator itr = applicantListTobeSaved.iterator();
            Applicant applicant = null;
            while(itr.hasNext()) {
                applicant = (Applicant) itr.next();
               if(deleteId.contains(applicant.getApplicantId())){
                   itr.remove();
               }
            }
        }catch(Exception ex){
        logger.error("error in insurance {}", ex.getStackTrace());
        }
    }

    public void populateInsuranceAmount(List<InsurenceCat> insurenceCatList, double icicAmount, double iciciLombard, double religare, double religareLoanProtection, double tataAmount, double pnbMetlife) {
        try{
            if(CollectionUtils.isNotEmpty(insurenceCatList)){
                checkInsurancepresent(insurenceCatList,icicAmount,iciciLombard,religare,religareLoanProtection, pnbMetlife,tataAmount);
            }
            Iterator<InsurenceCat> itr = insurenceCatList.iterator();
            InsurenceCat insurenceCat = null;
            while(itr.hasNext()){
                insurenceCat = itr.next();
                if(StringUtils.equalsIgnoreCase(insurenceCat.getCompanyName(),IciciHelper.ICICI_COMPANY_NAME)){
                    if(icicAmount == 0 ){
                        itr.remove();
                    }else{
                        insurenceCat.setAmount(icicAmount);
                    }
                }else if(StringUtils.equalsIgnoreCase(insurenceCat.getCompanyName(), PolicyHelper.InsuranceCompany.RELIGARE.companyName)){
                    if(religare == 0){
                        itr.remove();
                    }else{
                        insurenceCat.setAmount(religare);
                    }
                }else if(StringUtils.equalsIgnoreCase(insurenceCat.getCompanyName(), ReligareHelper.RELIGARE_LOAN_PROTECTION)){
                    if(religareLoanProtection == 0){
                        itr.remove();
                    }else{
                        insurenceCat.setAmount(religareLoanProtection);
                    }
                }else if(StringUtils.equalsIgnoreCase(insurenceCat.getCompanyName(), PolicyHelper.InsuranceCompany.TATA_AIG.companyName)){
                    if(tataAmount == 0){
                        itr.remove();
                    }else{
                        insurenceCat.setAmount(tataAmount);
                    }
                }else if(StringUtils.equalsIgnoreCase(insurenceCat.getCompanyName(), PolicyHelper.InsuranceCompany.PNB_Metlife.companyName)){
                    if(pnbMetlife == 0){
                        itr.remove();
                    }else{
                        insurenceCat.setAmount(tataAmount);
                    }
                }else if(StringUtils.equalsIgnoreCase(insurenceCat.getCompanyName(),IciciHelper.ICICI_LOMBARD_INSURANCE)){
                    if(iciciLombard == 0 ){
                        itr.remove();
                    }else{
                        insurenceCat.setAmount(iciciLombard);
                    }
                }
            }
        }catch (Exception ex){
            logger.error("{} stacktrace while save Insurance on Screen LoanCharges", ExceptionUtils.getStackTrace(ex));
            logger.error("{} dtail message while save Insurance on Screen LoanCharges", ex.getMessage());
        }
    }

    private void checkInsurancepresent(List<InsurenceCat> insurenceCatList, double icicAmount, double iciciLombard, double religare, double religareLoanProtection, double pnbMetlife, double tataAig) {
        InsurenceCat insurenceCat = null;

        InsurenceCat insurancePolicyIcIci = insurenceCatList.stream().
                filter(insuranceCat1 -> StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(),
                        IciciHelper.ICICI_COMPANY_NAME)).findFirst().orElse(null);

        InsurenceCat insurancePolicyReligare = insurenceCatList.stream().
                filter(insuranceCat1 -> StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(),
                        PolicyHelper.InsuranceCompany.RELIGARE.companyName) ||
                        StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(), "RELIGARE HEALTH INSURANCE")).findAny().orElse(null);

        InsurenceCat insurancePolicyReligareProtection = insurenceCatList.stream().
                filter(insuranceCat1 -> StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(),
                        "RELIGARE LOAN PROTECTION") || StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(),
                        "CARE LOAN PROTECTION")).findAny().orElse(null);

        InsurenceCat insurancePolicyPnbMetlife = insurenceCatList.stream().
                filter(insuranceCat1 -> StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(),
                        PolicyHelper.InsuranceCompany.PNB_Metlife.companyName)).findAny().orElse(null);

        InsurenceCat insurancePolicyTataAig = insurenceCatList.stream().
                filter(insuranceCat1 -> StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(),
                        PolicyHelper.InsuranceCompany.TATA_AIG.companyName)).findAny().orElse(null);

        InsurenceCat insurancePolicyIciciLombad = insurenceCatList.stream().
                filter(insuranceCat1 -> StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(),
                        IciciHelper.ICICI_LOMBARD_INSURANCE)).findAny().orElse(null);

        if(insurancePolicyIcIci == null && icicAmount != 0){
            insurenceCat = new InsurenceCat();
            insurenceCat.setAmount(icicAmount);
            insurenceCat.setCompanyName(IciciHelper.ICICI_COMPANY_NAME);
            insurenceCatList.add(insurenceCat);
        }
        if(insurancePolicyIciciLombad == null && iciciLombard != 0){
            insurenceCat = new InsurenceCat();
            insurenceCat.setAmount(iciciLombard);
            insurenceCat.setCompanyName(IciciHelper.ICICI_LOMBARD_INSURANCE);
            insurenceCatList.add(insurenceCat);
        }
        if (insurancePolicyReligare == null && religare != 0){
            insurenceCat = new InsurenceCat();
            insurenceCat.setAmount(icicAmount);
            insurenceCat.setCompanyName(PolicyHelper.InsuranceCompany.RELIGARE.companyName);
            insurenceCatList.add(insurenceCat);
        }
        if(insurancePolicyReligareProtection == null && religareLoanProtection != 0){
            insurenceCat = new InsurenceCat();
            insurenceCat.setAmount(icicAmount);
            insurenceCat.setCompanyName(ReligareHelper.RELIGARE_LOAN_PROTECTION);
            insurenceCatList.add(insurenceCat);
        }
        if(insurancePolicyPnbMetlife == null && pnbMetlife != 0){
            insurenceCat = new InsurenceCat();
            insurenceCat.setAmount(icicAmount);
            insurenceCat.setCompanyName(PolicyHelper.InsuranceCompany.PNB_Metlife.companyName);
            insurenceCatList.add(insurenceCat);
        }
        if(insurancePolicyTataAig == null && tataAig != 0){
            insurenceCat = new InsurenceCat();
            insurenceCat.setAmount(icicAmount);
            insurenceCat.setCompanyName(PolicyHelper.InsuranceCompany.TATA_AIG.companyName);
            insurenceCatList.add(insurenceCat);
        }

        insurenceCatList.
                forEach(insuranceCat1 -> {
                    if (StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(), "RELIGARE LOAN PROTECTION")) {
                        insuranceCat1.setCompanyName("CARE LOAN PROTECTION");
                    } else if (StringUtils.equalsIgnoreCase(insuranceCat1.getCompanyName(), "RELIGARE HEALTH INSURANCE")) {
                        insuranceCat1.setCompanyName("CARE HEALTH INSURANCE");
                    }
                });
    }


    public boolean calculatePremiumAmountCheck(List<InsurancePolicy> insurancePolicyList) {
        boolean flag =false;
        for(InsurancePolicy insurancePolicy : insurancePolicyList){
            if(insurancePolicy.getPremium()!= null &&insurancePolicy.getPremium() == 0){
                flag = true ;
            }
        }
        return flag;
    }

    public boolean validateInsuranceCnt(List<Applicant> applicantList) {
        boolean flag = false;
        int cnt = 0;
        for(Applicant applicant  : applicantList){
            if(!applicant.isWaived()){
                if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList()) &&
                        ApplicantType.isIndividual(applicant.getApplicantType()
                        )){
                    cnt ++;
                }
            }
        }
        if(cnt == 0 ){
             flag = true;
        }
        return flag;
    }

    public LoanCharges populateInsuranceInLoanCharges(LoanCharges loanCharges, InsuranceData insuranceData) {
        double icicAmount = 0;
        double iciciLombardAmount = 0;
        double religare = 0;
        double religareLoanProtection = 0;
        double tataAmount = 0;
        double pnbMetlife = 0;
        InsurenceCat insurenceCat = null;
        List<InsurenceCat> insurenceCatList = null;
        if(insuranceData != null && CollectionUtils.isNotEmpty(insuranceData.getApplicantList())){
            for (Applicant applicant : insuranceData.getApplicantList()){
                if(CollectionUtils.isNotEmpty(applicant.getInsurancePolicyList()) &&
                        !applicant.isWaived()){
                    for(InsurancePolicy insurancePolicy : applicant.getInsurancePolicyList()){
                        if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), IciciHelper.ICICI_COMPANY_NAME)
                                &&  insurancePolicy.getPremium() != null){
                            icicAmount = icicAmount + insurancePolicy.getPremium();
                        }else if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(),
                                PolicyHelper.InsuranceCompany.RELIGARE.companyName) && insurancePolicy.getPremium() != null) {
                            religare = religare + insurancePolicy.getPremium();
                        }else if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), ReligareHelper.RELIGARE_LOAN_PROTECTION)
                                &&  insurancePolicy.getPremium() != null){
                            religareLoanProtection = religareLoanProtection + insurancePolicy.getPremium();
                        }else if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), PolicyHelper.InsuranceCompany.TATA_AIG.companyName)
                                &&  insurancePolicy.getPremium() != null){
                            tataAmount = tataAmount + insurancePolicy.getPremium();
                        }else if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), PolicyHelper.InsuranceCompany.PNB_Metlife.companyName)
                                &&  insurancePolicy.getPremium() != null){
                            pnbMetlife = pnbMetlife + insurancePolicy.getPremium();
                        }else if(StringUtils.equalsIgnoreCase(insurancePolicy.getCompany(), IciciHelper.ICICI_LOMBARD_INSURANCE)
                                &&  insurancePolicy.getPremium() != null){
                            iciciLombardAmount += insurancePolicy.getPremium();
                        }
                    }
                }
            }
            if (loanCharges == null) {
                loanCharges = new LoanCharges();
                TenureDetails tenureDetails =  new TenureDetails();
                tenureDetails.setRemarks(new Remark());
                InterestDetails interestDetails = new InterestDetails();
                interestDetails.setRemarks(new Remark());
                loanCharges.setInterestDetails(interestDetails);
                loanCharges.setTenureDetails(tenureDetails);
                LoanAmtDetails loanAmtDetails = new LoanAmtDetails();
                loanCharges.setLoanDetails(loanAmtDetails);
            }
        }
        if(loanCharges != null){
            if(CollectionUtils.isNotEmpty(loanCharges.getInsurenceCatList())){
                populateInsuranceAmount(loanCharges.getInsurenceCatList(),GngCalculationUtils.roundValue(icicAmount,0),
                        iciciLombardAmount,GngCalculationUtils.roundValue(religare,0),GngCalculationUtils.roundValue(religareLoanProtection,0),GngCalculationUtils.roundValue(tataAmount,0) ,GngCalculationUtils.roundValue(pnbMetlife,0));
            } else{
                insurenceCatList = new ArrayList<>();
                if(icicAmount != 0 ){
                    insurenceCat = new InsurenceCat();
                    insurenceCat.setCompanyName(IciciHelper.ICICI_COMPANY_NAME);
                    insurenceCat.setAmount(icicAmount);
                    insurenceCatList.add(insurenceCat);
                }
                //icici lombard
                if(iciciLombardAmount != 0 ){
                    insurenceCat = new InsurenceCat();
                    insurenceCat.setCompanyName(IciciHelper.ICICI_LOMBARD_INSURANCE);
                    insurenceCat.setAmount(iciciLombardAmount);
                    insurenceCatList.add(insurenceCat);
                }
                //religare
                if(religare != 0){
                    insurenceCat = new InsurenceCat();
                    insurenceCat.setCompanyName(PolicyHelper.InsuranceCompany.RELIGARE.companyName);
                    insurenceCat.setAmount(religare);
                    insurenceCatList.add(insurenceCat);
                }
                //religareLoanProtection
                if(religareLoanProtection != 0){
                    insurenceCat = new InsurenceCat();
                    insurenceCat.setCompanyName(ReligareHelper.RELIGARE_LOAN_PROTECTION);
                    insurenceCat.setAmount(religareLoanProtection);
                    insurenceCatList.add(insurenceCat);
                }
                if(tataAmount != 0){
                    insurenceCat = new InsurenceCat();
                    insurenceCat.setCompanyName(PolicyHelper.InsuranceCompany.TATA_AIG.companyName);
                    insurenceCat.setAmount(tataAmount);
                    insurenceCatList.add(insurenceCat);
                }
                if(pnbMetlife != 0){
                    insurenceCat = new InsurenceCat();
                    insurenceCat.setCompanyName(PolicyHelper.InsuranceCompany.PNB_Metlife.companyName);
                    insurenceCat.setAmount(pnbMetlife);
                    insurenceCatList.add(insurenceCat);
                }
                loanCharges.setInsurenceCatList(insurenceCatList);
            }
            loanCharges.setInsuranceAmt(GngCalculationUtils.additionForDouble
                    (icicAmount, religare, religareLoanProtection, tataAmount, pnbMetlife,iciciLombardAmount));
        }
        return loanCharges ;
    }

    public void deleteImageFromFSFiles(OCRRequest ocrRequest) {
        if (Institute.isInstitute(ocrRequest.getHeader().getInstitutionId(), Institute.SBFC) &&
                ocrRequest.getOcrDetails() != null && CollectionUtils.isNotEmpty(ocrRequest.getOcrDetails().getOcrDetailsList())) {
            ocrRequest.getOcrDetails().getOcrDetailsList().stream().forEach(ocrDetailsObj -> {
                if (ocrDetailsObj.getUploadFileDetails() != null && ocrDetailsObj.getUploadFileDetails().isDeleted()) {
                    FileUploadRequest fileUploadRequest = buildFileUploadRequest(ocrRequest, ocrDetailsObj);
                    String newImageId = uploadFileRepository.softDocument(fileUploadRequest);
                    if(newImageId != null) {
                        ocrDetailsObj.getUploadFileDetails().setImageId(newImageId);
                        ocrDetailsObj.getUploadFileDetails().setFileId(newImageId);
                    }
                }
            });
        }
    }

    private FileUploadRequest buildFileUploadRequest(OCRRequest ocrRequest, OCRDetailsObj ocrDetailsObj) {
        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        FileHeader fileHeader = new FileHeader();
        fileHeader.setInstitutionId(ocrRequest.getHeader().getInstitutionId());
        fileUploadRequest.setFileHeader(fileHeader);
        fileUploadRequest.setGonogoReferanceId(ocrRequest.getRefId());
        fileUploadRequest.setImageID(ocrDetailsObj.getUploadFileDetails().getImageId());
        return fileUploadRequest;
    }

    public void setImageDetailsToOCR(OCRDetails ocrDetails, OCRRequest ocrRequest) throws Exception {
        List<ImagesDetails> imagesDetailsList = new ArrayList<>();
        Set<String> refSet = new HashSet<>();
        refSet.add(ocrRequest.getRefId());
        List<KycImageDetails> appImgDetails = uploadFileRepository
                .getKycImageDetails(refSet, ocrRequest.getHeader().getInstitutionId(), Status.ALL.name(), "");
        if (CollectionUtils.isNotEmpty(appImgDetails)) {
            appImgDetails.forEach(kycImageDetails -> {
                if (kycImageDetails != null) {
                    imagesDetailsList.addAll(kycImageDetails.getImageMap());
                }
            });
        }
        if (CollectionUtils.isNotEmpty(imagesDetailsList)) {
            imagesDetailsList.forEach(imagesDetails -> {
                if (ocrDetails != null && CollectionUtils.isNotEmpty(ocrDetails.getOcrDetailsList())) {
                    ocrDetails.getOcrDetailsList().forEach(ocrDetailsObj -> {
                        if (ocrDetailsObj != null && ocrDetailsObj.getUploadFileDetails() != null
                                && StringUtils.equals(ocrDetailsObj.getUploadFileDetails().getFileId(), imagesDetails.getFileId())) {
                            ocrDetailsObj.getUploadFileDetails().setImageId(imagesDetails.getImageId());
                            ocrDetailsObj.getUploadFileDetails().setLatitude(imagesDetails.getLatitude());
                            ocrDetailsObj.getUploadFileDetails().setLongitude(imagesDetails.getLongitude());
                            ocrDetailsObj.getUploadFileDetails().setImageSide(imagesDetails.getImageSide());
                            ocrDetailsObj.getUploadFileDetails().setDeleted(imagesDetails.isDeleted());
                            ocrDetailsObj.getUploadFileDetails().setUploadDate(imagesDetails.getUploadDate());
                            ocrDetailsObj.getUploadFileDetails().setFileExtension(imagesDetails.getImageExtension());

                        }
                    });
                }
            });
        }
        if(Roles.isDocSamplerRole(ocrRequest.getHeader().getLoggedInUserRole())) {
            RCUSummary rcuSummary = ocrDetails.getRcuSummary();
            if(rcuSummary != null && CollectionUtils.isNotEmpty(rcuSummary.getRcuActivityList())) {
                List<String> doneByRole = rcuSummary.getRcuActivityList().stream().map(RCUActivity::getDoneBy).collect(Collectors.toList());
                if(CollectionUtils.isNotEmpty(doneByRole) && !doneByRole.contains(Roles.Role.RCU_AGENCY)) {
                    int difference = 0;
                    if(rcuSummary.getRcuSamplingDate() != null) {
                        difference = GngDateUtil.getDifferenceDays(rcuSummary.getRcuSamplingDate(), new Date());
                    }
                    rcuSummary.setSystemTatInDays(difference);
                }
            }
        }
        ocrRequest.setOcrDetails(ocrDetails);
        ocrRequest.setAgencyCode(ocrDetails.getAgencyCode());
        ocrRequest.setAgencyName(ocrDetails.getAgencyName());
        ocrRequest.setRcuSummary(ocrDetails.getRcuSummary());
        ocrRequest.setStatus(ocrDetails.getStatus());
        ocrRequest.setAgencyStatus(ocrDetails.getAgencyStatus());
        ocrDetails.setInitiatedBy(ocrDetails.getInstitutionId());
        applicationRepository.saveOCRData(ocrRequest);
    }

    public void setLogsInTimelineDataList(String institutionId, List<ActivityLogs> activityLogsList, List<TimelineData> timelineDataList){
        try{
            Map<String ,Map<String , String>> fieldValue = masterRepository.fetchLogsMaster(institutionId ,GNGWorkflowConstant.SCREEN_NAME.toFaceValue(), GNGWorkflowConstant.TIMELINE.toFaceValue());
            if(MapUtils.isNotEmpty(fieldValue)) {
                Map<String , String> actionName = fieldValue.get(GNGWorkflowConstant.ACTION_NAME.toFaceValue());
                if (MapUtils.isNotEmpty(actionName)) {
                    activityLogsList.forEach(activityLogs -> {
                        TimelineData timelineData = new TimelineData();

                        if (StringUtils.isBlank(activityLogs.getAction())
                                || StringUtils.equalsIgnoreCase(GNGWorkflowConstant.COMPONENT_ACTION.toFaceValue(), activityLogs.getAction())) {
                            if (actionName.containsKey(activityLogs.getStep())) {
                                timelineData.setAction(actionName.get(activityLogs.getStep()));
                            } else {
                                timelineData.setAction(activityLogs.getStep());
                            }
                        } else if (actionName.containsKey(activityLogs.getAction())) {
                            timelineData.setAction(actionName.get(activityLogs.getAction()));
                        } else if(actionName.containsKey(activityLogs.getStep())) {
                            timelineData.setAction(actionName.get(activityLogs.getStep()));
                        } else {
                            timelineData.setAction(activityLogs.getAction());
                        }
                        timelineData.setRemarks(activityLogs.getRemarks());
                        timelineData.setActionDate(activityLogs.getActionDate());
                        timelineData.setDuration(activityLogs.getDuration());
                        timelineData.setCustomMsg(activityLogs.getCustomMsg());
                        timelineData.setStage(activityLogs.getStage());
                        timelineData.setStageChangedTo(activityLogs.getChangedStage());
                        timelineData.setLockedUserId(activityLogs.getCurrentUserId());
                        timelineData.setChangedUserId(activityLogs.getChangedUserId());

                        User loggedInUser = new User();
                        List<Role> roleList = new ArrayList<>();
                        Role role = new Role();
                        loggedInUser.setUserId(activityLogs.getUserId());
                        loggedInUser.setUserName(activityLogs.getUserName());
                        role.setRole(activityLogs.getUserRole());
                        roleList.add(role);
                        loggedInUser.setRoles(roleList);

                        timelineData.setLoggedInUserDetails(loggedInUser);
                        timelineData.setLockedUserName(activityLogs.getCurrentUserName());
                        timelineData.setChangedUserName(activityLogs.getChangedUserName());

                        timelineDataList.add(timelineData);
                    });
                } else {
                    logger.error("ActionName not configured for this institution");
                }
            } else {
                logger.error("LogsMaster not configured for this institution");
            }
        }catch (Exception e){
            logger.error("Error in method setLogsInTimelineDataList due to {}",e.getStackTrace());
        }
    }

    public void makeNameCapital(Request request){
        if(request.getApplicant() != null){
            makeNameCapitalForApplicant(request.getApplicant());
        }

        if(request.getCoApplicant() != null && CollectionUtils.isNotEmpty(request.getCoApplicant())){
            for(CoApplicant coApplicant:request.getCoApplicant()){
                makeNameCapitalForApplicant(coApplicant);
            }
        }
    }

    private void makeNameCapitalForApplicant(Applicant applicant){
        Name applicantName = makeNameCapitalHelper(applicant.getApplicantName());
        Name fatherName = makeNameCapitalHelper(applicant.getFatherName());
        Name spouseName = makeNameCapitalHelper(applicant.getSpouseName());
        Name motherName = makeNameCapitalHelper(applicant.getMotherName());
        String mothersMaidenName = StringUtils.isNotEmpty(applicant.getMothersMaidenName()) ? applicant.getMothersMaidenName().toUpperCase(Locale.getDefault()) : "";

        applicant.setApplicantName(applicantName);
        applicant.setFatherName(fatherName);
        applicant.setSpouseName(spouseName);
        applicant.setMotherName(motherName);
        applicant.setMothersMaidenName(mothersMaidenName);
    }

    private Name makeNameCapitalHelper(Name name){
        if(name != null){
            String prefix = StringUtils.isNotEmpty(name.getPrefix()) ? name.getPrefix().toUpperCase(Locale.getDefault()) : "";
            String firstName = StringUtils.isNotEmpty(name.getFirstName()) ? name.getFirstName().toUpperCase(Locale.getDefault()) : "";
            String lastName = StringUtils.isNotEmpty(name.getLastName()) ? name.getLastName().toUpperCase(Locale.getDefault()) : "";
            String middleName = StringUtils.isNotEmpty(name.getMiddleName()) ? name.getMiddleName().toUpperCase(Locale.getDefault()) : "";
            String suffix = StringUtils.isNotEmpty(name.getSuffix()) ? name.getSuffix().toUpperCase(Locale.getDefault()) : "";

            name.setFirstName(firstName);
            name.setLastName(lastName);
            name.setMiddleName(middleName);
            name.setPrefix(prefix);
            name.setSuffix(suffix);

            return name;
        }
        return new Name();
    }
}

