package com.softcell.gonogo.serialnumbervalidation.google;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by prasenjit wadmare on 24/10/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoogleResponse {

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("oError")
    private ThirdPartyException error;

}
