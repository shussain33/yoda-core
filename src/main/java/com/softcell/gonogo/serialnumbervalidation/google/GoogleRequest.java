package com.softcell.gonogo.serialnumbervalidation.google;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by prasenjit wadmare on 24/10/17.
 */
@Data
@Builder
public class GoogleRequest {


    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sRDSCode")
    private String rdsCode;

    @JsonProperty("sDateAndTime")
    private String dateAndTime;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sBlockStatus")
    private String blockStatus;

    @JsonProperty("sCustomerCode")
    private String customerCode;


}

