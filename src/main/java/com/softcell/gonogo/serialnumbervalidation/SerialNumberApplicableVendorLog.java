package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by sampat on 14/1/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "serialNumberApplicableVendorLog")
public class SerialNumberApplicableVendorLog {

    @JsonProperty("sInstitutionId")
    private String institutionId;

    @JsonProperty("sReferenceId")
    private String referenceId;

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("dtDateTime")
    private Date dateTime = new Date();

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("bDealerSkipApplicable")
    private boolean dealerSkipApplicable;

    @JsonProperty("sCustomMsg")
    private String customMsg;

    @JsonProperty("sStatus")
    private String status;
}
