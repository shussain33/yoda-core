package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by yogeshb on 29/3/17.
 */
@Document(collection = "rollbackFeatureLog")
public class RollbackFeatureLog {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("dtDateTime")
    private Date dateTime = new Date();

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sStatus")
    private String status;

    public static Builder builder() {
        return new Builder();
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RollbackFeatureLog{");
        sb.append("refID='").append(refID).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", dateTime=").append(dateTime);
        sb.append(", dealerId='").append(dealerId).append('\'');
        sb.append(", serialNumber='").append(serialNumber).append('\'');
        sb.append(", imeiNumber='").append(imeiNumber).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RollbackFeatureLog that = (RollbackFeatureLog) o;

        if (refID != null ? !refID.equals(that.refID) : that.refID != null) return false;
        if (vendor != null ? !vendor.equals(that.vendor) : that.vendor != null) return false;
        if (dateTime != null ? !dateTime.equals(that.dateTime) : that.dateTime != null) return false;
        if (dealerId != null ? !dealerId.equals(that.dealerId) : that.dealerId != null) return false;
        if (serialNumber != null ? !serialNumber.equals(that.serialNumber) : that.serialNumber != null) return false;
        if (imeiNumber != null ? !imeiNumber.equals(that.imeiNumber) : that.imeiNumber != null) return false;
        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        int result = refID != null ? refID.hashCode() : 0;
        result = 31 * result + (vendor != null ? vendor.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (dealerId != null ? dealerId.hashCode() : 0);
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        result = 31 * result + (imeiNumber != null ? imeiNumber.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    public static class Builder {
        private RollbackFeatureLog rollbackFeatureLog = new RollbackFeatureLog();

        public RollbackFeatureLog build() {
            return rollbackFeatureLog;
        }

        public Builder refID(String refID) {
            this.rollbackFeatureLog.refID = refID;
            return this;
        }

        public Builder vendor(String vendor) {
            this.rollbackFeatureLog.vendor = vendor;
            return this;
        }

        public Builder dealerId(String dealerId) {
            this.rollbackFeatureLog.dealerId = dealerId;
            return this;
        }

        public Builder serialNumber(String serialNumber) {
            this.rollbackFeatureLog.serialNumber = serialNumber;
            return this;
        }

        public Builder imeiNumber(String imeiNumber) {
            this.rollbackFeatureLog.imeiNumber = imeiNumber;
            return this;
        }

        public Builder status(String status) {
            this.rollbackFeatureLog.status = status;
            return this;
        }

    }

}
