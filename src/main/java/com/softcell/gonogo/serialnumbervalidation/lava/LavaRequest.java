package com.softcell.gonogo.serialnumbervalidation.lava;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author prasenjit wadmare
 * @date    02 Jan 2018
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LavaRequest {

    @JsonProperty("imei")
    private String imeiNumber;

    @JsonProperty("model")
    private String modelNumber;

    @JsonProperty("brand")
    private String brandName;

    @JsonProperty("retailerCode")
    private String retailerCode;

    @JsonProperty("price")
    private String price;

    @JsonProperty("partner")
    private String partnerCode;

    @JsonProperty("key")
    private String key;

    @JsonProperty("subject")
    private String subject;

    @JsonProperty("tokenName")
    private String tokenName;

}
