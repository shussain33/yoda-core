package com.softcell.gonogo.serialnumbervalidation.lava;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author prasenjit wadmare
 * @date    02 Jan 2018
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LavaResponse {

    @JsonProperty("bVerifiedFlag")
    private boolean verifiedFlag;

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sErrorCode")
    private int errorCode;

    @JsonProperty("oError")
    private ThirdPartyException error;


}
