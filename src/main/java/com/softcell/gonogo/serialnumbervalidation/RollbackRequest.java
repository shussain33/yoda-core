package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogeshb on 27/3/17.
 */
public class RollbackRequest {
    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.InstWithProductGrp.class
            }
    )
    @Valid
    private Header header;

    /**
     * Following are the possible values for vendor
     */
    @JsonProperty("sVendor")
    @NotEmpty(
            groups = {
                    RollbackRequest.FetchGrp.class
            }
    )
    private String vendor;

    /**
     * Mandatory field
     */
    @JsonProperty("sReferenceID")
    @NotEmpty(
            groups = {
                    RollbackRequest.FetchGrp.class
            }
    )
    private String referenceID;

    /**
     * Mandatory field
     */
    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    /**
     * Mandatory field
     */
    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("bIsResetStage")
    private boolean resetStage;

    /**
     * For Kent
     */
    @JsonProperty("sStoreCode")
    private String storeCode;

    @JsonProperty("sSchemeCode")
    private String schemeCode;

    @JsonProperty("sMake")
    private String make;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public boolean isResetStage() {
        return resetStage;
    }

    public void setResetStage(boolean resetStage) {
        this.resetStage = resetStage;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getSchemeCode() {
        return schemeCode;
    }

    public void setSchemeCode(String schemeCode) {
        this.schemeCode = schemeCode;
    }

    public String getMake() { return make; }

    public void setMake(String make) { this.make = make; }

    public interface FetchGrp {
    }
}
