package com.softcell.gonogo.serialnumbervalidation.apple;

/**
 * Created by yogeshb on 20/3/17.
 */
public class ReportResponse {

    private String status;

    private boolean saleReported;

    private String errorCode;

    private String errorMessage;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSaleReported() {
        return saleReported;
    }

    public void setSaleReported(boolean saleReported) {
        this.saleReported = saleReported;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReportResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", saleReported=").append(saleReported);
        sb.append(", errorCode='").append(errorCode).append('\'');
        sb.append(", errorMessage='").append(errorMessage).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
