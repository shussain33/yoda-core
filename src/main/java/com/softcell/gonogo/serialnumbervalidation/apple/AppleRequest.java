package com.softcell.gonogo.serialnumbervalidation.apple;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by yogeshb on 20/3/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppleRequest {

    @JsonProperty("sUserId")
    private String userId;

    @JsonProperty("sPassword")
    private String password;

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("sImeiNumber")
    private String imei;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sMpn")
    private String mpn;

    @JsonProperty("lStoreAppleId")
    private long storeAppleId;

    @JsonProperty("sBankName")
    private String bankName;

    @JsonProperty("iTenure")
    private Integer tenure;

    @JsonProperty("iPaymentMethod")
    private int paymentMethod;

    @JsonProperty("sScheme")
    private String scheme;

    public static Builder builder() {
        return new Builder();
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getTenure() {
        return tenure;
    }

    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public long getStoreAppleId() {
        return storeAppleId;
    }

    public void setStoreAppleId(long storeAppleId) {
        this.storeAppleId = storeAppleId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public static class Builder {

        private AppleRequest appleRequest = new AppleRequest();

        public AppleRequest build() {
            return appleRequest;
        }

        public Builder userId(String userId) {
            this.appleRequest.userId = userId;
            return this;

        }

        public Builder password(String password) {
            this.appleRequest.password = password;
            return this;
        }

        public Builder vendor(String vendor) {
            this.appleRequest.vendor = vendor;
            return this;
        }

        public Builder imei(String imei) {
            this.appleRequest.imei = imei;
            return this;
        }

        public Builder serialNumber(String serialNumber) {
            this.appleRequest.serialNumber = serialNumber;
            return this;
        }

        public Builder mpn(String mpn) {
            this.appleRequest.mpn = mpn;
            return this;
        }

        public Builder storeAppleId(long storeAppleId) {
            this.appleRequest.storeAppleId = storeAppleId;
            return this;
        }

        public Builder bankName(String bankName) {
            this.appleRequest.bankName = bankName;
            return this;
        }

        public Builder tenure(Integer tenure) {
            this.appleRequest.tenure = tenure;
            return this;
        }

        public Builder paymentMethod(int paymentMethod) {
            this.appleRequest.paymentMethod = paymentMethod;
            return this;
        }

        public Builder scheme(String scheme) {
            this.appleRequest.scheme = scheme;
            return this;
        }

    }
}
