package com.softcell.gonogo.serialnumbervalidation.apple;

/**
 * Created by yogeshb on 20/3/17.
 */
public class QueryResponse {

    private String status;
    private boolean saleEligible;
    private String errorCode;
    private String errorMessage;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSaleEligible() {
        return saleEligible;
    }

    public void setSaleEligible(boolean saleEligible) {
        this.saleEligible = saleEligible;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("QueryResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", saleEligible=").append(saleEligible);
        sb.append(", errorCode='").append(errorCode).append('\'');
        sb.append(", errorMessage='").append(errorMessage).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
