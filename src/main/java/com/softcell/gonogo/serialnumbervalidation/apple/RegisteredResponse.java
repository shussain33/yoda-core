package com.softcell.gonogo.serialnumbervalidation.apple;

/**
 * Created by yogeshb on 20/3/17.
 */
public class RegisteredResponse {
    private String status;
    private boolean saleRegistered;
    private String transactionId;
    private String errorCode;
    private String errorMessage;
    private String distributorId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSaleRegistered() {
        return saleRegistered;
    }

    public void setSaleRegistered(boolean saleRegistered) {
        this.saleRegistered = saleRegistered;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RegisteredResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", saleRegistered=").append(saleRegistered);
        sb.append(", transactionId='").append(transactionId).append('\'');
        sb.append(", errorCode='").append(errorCode).append('\'');
        sb.append(", errorMessage='").append(errorMessage).append('\'');
        sb.append(", distributorId='").append(distributorId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
