package com.softcell.gonogo.serialnumbervalidation.apple;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by yogeshb on 20/3/17.
 */
public class AppleResponse {
    @JsonProperty("oQueryResponse")
    private QueryResponse queryResponse;

    @JsonProperty("oRegisteredResponse")
    private RegisteredResponse registeredResponse;

    @JsonProperty("oReportResponse")
    private ReportResponse reportResponse;

    @JsonProperty("oError")
    private ThirdPartyException error;

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public QueryResponse getQueryResponse() {
        return queryResponse;
    }

    public void setQueryResponse(QueryResponse queryResponse) {
        this.queryResponse = queryResponse;
    }

    public RegisteredResponse getRegisteredResponse() {
        return registeredResponse;
    }

    public void setRegisteredResponse(RegisteredResponse registeredResponse) {
        this.registeredResponse = registeredResponse;
    }

    public ReportResponse getReportResponse() {
        return reportResponse;
    }

    public void setReportResponse(ReportResponse reportResponse) {
        this.reportResponse = reportResponse;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private AppleResponse appleResponse = new AppleResponse();

        public AppleResponse build(){
            return appleResponse;
        }

        public Builder error(ThirdPartyException error){
            this.appleResponse.error = error;
            return this;
        }

        public Builder queryResponse(QueryResponse queryResponse){
            this.appleResponse.queryResponse = queryResponse;
            return this;
        }

        public Builder registeredResponse(RegisteredResponse registeredResponse){
            this.appleResponse.registeredResponse = registeredResponse;
            return this;
        }

        public Builder reportResponse(ReportResponse reportResponse){
            this.appleResponse.reportResponse = reportResponse;
            return this;
        }
    }


}
