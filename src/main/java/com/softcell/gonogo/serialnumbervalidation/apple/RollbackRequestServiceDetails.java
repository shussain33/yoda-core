package com.softcell.gonogo.serialnumbervalidation.apple;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by yogeshb on 27/3/17.
 */
public class RollbackRequestServiceDetails {

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sVendor")
    private String vendor;

    public static Builder builder() {
        return new Builder();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RollbackRequestServiceDetails)) return false;

        RollbackRequestServiceDetails that = (RollbackRequestServiceDetails) o;

        if (getSerialNumber() != null ? !getSerialNumber().equals(that.getSerialNumber()) : that.getSerialNumber() != null)
            return false;
        if (getImeiNumber() != null ? !getImeiNumber().equals(that.getImeiNumber()) : that.getImeiNumber() != null)
            return false;
        if (getSkuCode() != null ? !getSkuCode().equals(that.getSkuCode()) : that.getSkuCode() != null) return false;
        return getVendor() != null ? getVendor().equals(that.getVendor()) : that.getVendor() == null;
    }

    @Override
    public int hashCode() {
        int result = getSerialNumber() != null ? getSerialNumber().hashCode() : 0;
        result = 31 * result + (getImeiNumber() != null ? getImeiNumber().hashCode() : 0);
        result = 31 * result + (getSkuCode() != null ? getSkuCode().hashCode() : 0);
        result = 31 * result + (getVendor() != null ? getVendor().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RollbackRequestServiceDetails{");
        sb.append("serialNumber='").append(serialNumber).append('\'');
        sb.append(", imeiNumber='").append(imeiNumber).append('\'');
        sb.append(", skuCode='").append(skuCode).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static class Builder {
        private RollbackRequestServiceDetails rollbackRequestServiceDetails = new RollbackRequestServiceDetails();

        public RollbackRequestServiceDetails build() {
            return rollbackRequestServiceDetails;
        }

        public Builder serialNumber(String serialNumber) {
            this.rollbackRequestServiceDetails.serialNumber = serialNumber;
            return this;
        }

        public Builder imeiNumber(String imeiNumber) {
            this.rollbackRequestServiceDetails.imeiNumber = imeiNumber;
            return this;
        }

        public Builder skuCode(String skuCode) {
            this.rollbackRequestServiceDetails.skuCode = skuCode;
            return this;
        }

        public Builder vendor(String vendor) {
            this.rollbackRequestServiceDetails.vendor = vendor;
            return this;
        }
    }
}
