package com.softcell.gonogo.serialnumbervalidation.voltas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * created by prasenjit wadmare on 13/12/2017
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VoltasResponse {

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sCustomerName")
    private String customerName;

    @JsonProperty("sProductCategory")
    private String productCategory;

    @JsonProperty("sUnitStatus")
    private String unitStatus;

    @JsonProperty("sPurchaseDate")
    private String purchaseDate;

    @JsonProperty("sAlternateContact")
    private String alternateContact;

    @JsonProperty("sCustomerEmail")
    private String customerEmail;

    @JsonProperty("sArea")
    private String area;

    @JsonProperty("sBranchName")
    private String branchName;

    @JsonProperty("sCallType")
    private String callType;

    @JsonProperty("sClosedDate")
    private String closedDate;

    @JsonProperty("sFranchisee")
    private String franchisee;

    @JsonProperty("sLotNumber")
    private String lotNumber;

    @JsonProperty("sOpenDate")
    private String openDate;

    @JsonProperty("sSRNumber")
    private String serviceRequestNumber;

    @JsonProperty("sSRStatus")
    private String serviceRequestStatus;

    @JsonProperty("sPostalCode")
    private String postalCode;

    @JsonProperty("sOutput")
    private String output;

    @JsonProperty("sHelpDeskNumber")
    private String helpDeskNumber;

    @JsonProperty("oError")
    private ThirdPartyException error;

}
