package com.softcell.gonogo.serialnumbervalidation.voltas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *  created by prasenjit wadmare on 21/12/2017
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceCodeRegResponse {

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("sCustomerName")
    private String customerName;

    @JsonProperty("sProductCategory")
    private String productCategory;

    @JsonProperty("sCustomerEmail")
    private String customerEmail;

    @JsonProperty("sSRNumber")
    private String serviceRequestNumber;

    @JsonProperty("sSRStatus")
    private String serviceRequestStatus;

    @JsonProperty("sOutput")
    private String output;

    @JsonProperty("sHelpDeskNumber")
    private String helpDeskNumber;

    @JsonProperty("oError")
    private ThirdPartyException error;


}
