package com.softcell.gonogo.serialnumbervalidation.voltas;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by prasenjit wadmare on 13/12/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VoltasRequest {

    @JsonProperty("sSRNumber")
    private String serviceRequestNumber;

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("sPassword")
    private String password;

    @JsonProperty("sSessionType")
    private String sessionType;

}
