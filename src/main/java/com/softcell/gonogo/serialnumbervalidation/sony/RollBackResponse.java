package com.softcell.gonogo.serialnumbervalidation.sony;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by yogeshb on 27/3/17.
 */
public class RollBackResponse {

    private String status;

    private boolean saleRollback;

    private String errorCode;

    private String errorMessage;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("iResCode")
    private int responseCode;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSaleRollback() {
        return saleRollback;
    }

    public void setSaleRollback(boolean saleRollback) {
        this.saleRollback = saleRollback;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }


    public static class Builder {
        private RollBackResponse rollBackResponse = new RollBackResponse();

        public RollBackResponse build() {
            return rollBackResponse;
        }

        public Builder status(String status) {
            this.rollBackResponse.status = status;
            return this;
        }

        public Builder saleRollback(boolean saleRollback) {
            this.rollBackResponse.saleRollback = saleRollback;
            return this;
        }

        public Builder errorCode(String errorCode) {
            this.rollBackResponse.errorCode = errorCode;
            return this;
        }

        public Builder errorMessage(String errorMessage) {
            this.rollBackResponse.errorMessage = errorMessage;
            return this;
        }

        public Builder error(ThirdPartyException error) {
            this.rollBackResponse.error = error;
            return this;
        }

        public Builder responseCode(int responseCode) {
            this.rollBackResponse.responseCode = responseCode;
            return this;
        }

    }

}
