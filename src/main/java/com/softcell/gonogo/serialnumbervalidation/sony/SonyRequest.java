package com.softcell.gonogo.serialnumbervalidation.sony;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by yogeshb on 7/3/17.
 */
public class SonyRequest {

    @JsonProperty("sSecureKey")
    private String secureKey;

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sMaterialName")
    private String materialName;

    @JsonProperty("sStoreCode")
    private String storeCode;//Optional

    @JsonProperty("sProductCode")
    private String productCode;

    @JsonProperty("bConfirmationStatus")
    private boolean confirmationStatus;//Optional

    @JsonProperty("iRequestID")
    private int requestID;//Optional

    @JsonProperty("iCategoryID")
    private int categoryID;//Optional

    @JsonProperty("sModel")
    private String model;

    public String getSecureKey() {
        return secureKey;
    }

    public void setSecureKey(String secureKey) {
        this.secureKey = secureKey;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public boolean isConfirmationStatus() {
        return confirmationStatus;
    }

    public void setConfirmationStatus(boolean confirmationStatus) {
        this.confirmationStatus = confirmationStatus;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SonyRequest{");
        sb.append("secureKey='").append(secureKey).append('\'');
        sb.append(", imeiNumber='").append(imeiNumber).append('\'');
        sb.append(", serialNumber='").append(serialNumber).append('\'');
        sb.append(", materialName='").append(materialName).append('\'');
        sb.append(", storeCode='").append(storeCode).append('\'');
        sb.append(", productCode='").append(productCode).append('\'');
        sb.append(", confirmationStatus=").append(confirmationStatus);
        sb.append(", requestID=").append(requestID);
        sb.append(", categoryID=").append(categoryID);
        sb.append(", model='").append(model).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SonyRequest that = (SonyRequest) o;

        if (confirmationStatus != that.confirmationStatus) return false;
        if (requestID != that.requestID) return false;
        if (categoryID != that.categoryID) return false;
        if (secureKey != null ? !secureKey.equals(that.secureKey) : that.secureKey != null) return false;
        if (imeiNumber != null ? !imeiNumber.equals(that.imeiNumber) : that.imeiNumber != null) return false;
        if (serialNumber != null ? !serialNumber.equals(that.serialNumber) : that.serialNumber != null) return false;
        if (materialName != null ? !materialName.equals(that.materialName) : that.materialName != null) return false;
        if (storeCode != null ? !storeCode.equals(that.storeCode) : that.storeCode != null) return false;
        if (productCode != null ? !productCode.equals(that.productCode) : that.productCode != null) return false;
        return model != null ? model.equals(that.model) : that.model == null;
    }

    @Override
    public int hashCode() {
        int result = secureKey != null ? secureKey.hashCode() : 0;
        result = 31 * result + (imeiNumber != null ? imeiNumber.hashCode() : 0);
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        result = 31 * result + (materialName != null ? materialName.hashCode() : 0);
        result = 31 * result + (storeCode != null ? storeCode.hashCode() : 0);
        result = 31 * result + (productCode != null ? productCode.hashCode() : 0);
        result = 31 * result + (confirmationStatus ? 1 : 0);
        result = 31 * result + requestID;
        result = 31 * result + categoryID;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        return result;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private SonyRequest sonyRequest = new SonyRequest();

        public SonyRequest build() {
            return sonyRequest;
        }

        public Builder secureKey(String secureKey) {
            this.sonyRequest.secureKey = secureKey;
            return this;
        }

        public Builder imeiNumber(String imeiNumber) {
            this.sonyRequest.imeiNumber = imeiNumber;
            return this;
        }

        public Builder serialNumber(String serialNumber) {
            this.sonyRequest.serialNumber = serialNumber;
            return this;
        }

        public Builder materialName(String materialName) {
            this.sonyRequest.materialName = materialName;
            return this;
        }

        public Builder storeCode(String storeCode) {
            this.sonyRequest.storeCode = storeCode;
            return this;
        }

        public Builder productCode(String productCode) {
            this.sonyRequest.productCode = productCode;
            return this;
        }

        public Builder confirmationStatus(boolean confirmationStatus) {
            this.sonyRequest.confirmationStatus = confirmationStatus;
            return this;
        }

        public Builder requestID(int requestID) {
            this.sonyRequest.requestID = requestID;
            return this;
        }

        public Builder categoryID(int categoryID) {
            this.sonyRequest.categoryID = categoryID;
            return this;
        }

        public Builder model(String model) {
            this.sonyRequest.model = model;
            return this;
        }

    }
}
