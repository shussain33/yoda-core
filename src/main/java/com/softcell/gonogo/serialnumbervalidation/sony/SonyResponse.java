package com.softcell.gonogo.serialnumbervalidation.sony;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by yogeshb on 7/3/17.
 */
public class SonyResponse {
    private int authCheck;
    private String comments;
    private long requestId;
    private String serialNumber;
    private long valid;
    private String imeiNumber;

    @JsonProperty("oError")
    private ThirdPartyException error;

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public long getValid() {
        return valid;
    }

    public void setValid(long valid) {
        this.valid = valid;
    }

    public int getAuthCheck() {

        return authCheck;
    }

    public void setAuthCheck(int authCheck) {
        this.authCheck = authCheck;
    }


    public static Builder builder() {
        return new Builder();
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public static class Builder {

        private SonyResponse sonyResponse = new SonyResponse();

        public SonyResponse build() {
            return sonyResponse;
        }

        public Builder authCheck(int authCheck) {
            this.sonyResponse.authCheck = authCheck;
            return this;
        }

        public Builder comments(String comments) {
            this.sonyResponse.comments = comments;
            return this;
        }

        public Builder requestId(long requestId) {
            this.sonyResponse.requestId = requestId;
            return this;
        }

        public Builder serialNumber(String serialNumber) {
            this.sonyResponse.serialNumber = serialNumber;
            return this;
        }

        public Builder valid(long valid) {
            this.sonyResponse.valid = valid;
            return this;
        }

        public Builder imeiNumber(String imeiNumber) {
            this.sonyResponse.imeiNumber = imeiNumber;
            return this;
        }

        public Builder error(ThirdPartyException error) {
            this.sonyResponse.error = error;
            return this;
        }


    }
}
