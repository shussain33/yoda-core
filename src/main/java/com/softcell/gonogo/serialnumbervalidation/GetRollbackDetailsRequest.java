package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by yogeshb on 28/3/17.
 */
public class GetRollbackDetailsRequest {
    /**
     * Mandatory field
     */
    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.InstWithProductGrp.class
            }
    )
    @Valid
    private Header header;


    /**
     * Mandatory field
     */
    @JsonProperty("sReferenceID")
    @NotEmpty(
            groups = {
                    GetRollbackDetailsRequest.FetchGrp.class
            }
    )
    private String referenceID;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GetRollbackDetailsRequest that = (GetRollbackDetailsRequest) o;

        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        return referenceID != null ? referenceID.equals(that.referenceID) : that.referenceID == null;
    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + (referenceID != null ? referenceID.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GetRollbackDetailsRequest{");
        sb.append("header=").append(header);
        sb.append(", referenceID='").append(referenceID).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public interface FetchGrp {
    }

}
