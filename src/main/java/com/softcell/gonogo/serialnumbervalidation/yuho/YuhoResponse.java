package com.softcell.gonogo.serialnumbervalidation.yuho;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by prasenjit wadmare on 07/11/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class YuhoResponse {

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("sIMEI")
    private String imeiNumber;

    @JsonProperty("oError")
    private ThirdPartyException error;


}
