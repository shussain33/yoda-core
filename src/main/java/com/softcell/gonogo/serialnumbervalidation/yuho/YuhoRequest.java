package com.softcell.gonogo.serialnumbervalidation.yuho;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by prasenjit wadmare on 07/11/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class YuhoRequest {

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sModelCode")
    private String modelCode;

    @JsonProperty("sRetailerCode")
    private String retailerCode;

    @JsonProperty("sAuthHeader")
    private String authHeader;

    @JsonProperty("sSource")
    private String source;

}
