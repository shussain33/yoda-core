package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by sampat on 25/8/17.
 */
public class DisbursementResponse {

    private String status;

    private String errorCode;

    private String errorMessage;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("iResCode")
    private int responseCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DisbursementResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", errorCode='").append(errorCode).append('\'');
        sb.append(", errorMessage='").append(errorMessage).append('\'');
        sb.append(", error=").append(error);
        sb.append(", responseCode=").append(responseCode);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DisbursementResponse)) return false;

        DisbursementResponse disbursementResponse = (DisbursementResponse) o;

        if (getResponseCode() != disbursementResponse.getResponseCode()) return false;
        if (getStatus() != null ? !getStatus().equals(disbursementResponse.getStatus()) : disbursementResponse.getStatus() != null) return false;
        if (getErrorCode() != null ? !getErrorCode().equals(disbursementResponse.getErrorCode()) : disbursementResponse.getErrorCode() != null)
            return false;
        if (getErrorMessage() != null ? !getErrorMessage().equals(disbursementResponse.getErrorMessage()) : disbursementResponse.getErrorMessage() != null)
            return false;
        return getError() != null ? getError().equals(disbursementResponse.getError()) : disbursementResponse.getError() == null;
    }

    @Override
    public int hashCode() {
        int result = getStatus() != null ? getStatus().hashCode() : 0;
        result = 31 * result + (getErrorCode() != null ? getErrorCode().hashCode() : 0);
        result = 31 * result + (getErrorMessage() != null ? getErrorMessage().hashCode() : 0);
        result = 31 * result + (getError() != null ? getError().hashCode() : 0);
        result = 31 * result + getResponseCode();
        return result;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DisbursementResponse disbursementResponse = new DisbursementResponse();

        public DisbursementResponse build() {
            return this.disbursementResponse;
        }

        public DisbursementResponse.Builder status(String status) {
            this.disbursementResponse.setStatus(status);
            return this;
        }

        public DisbursementResponse.Builder errorCode(String errorCode) {
            this.disbursementResponse.setErrorCode(errorCode);
            return this;
        }

        public DisbursementResponse.Builder errorMessage(String errorMessage) {
            this.disbursementResponse.setErrorMessage(errorMessage);
            return this;
        }

        public DisbursementResponse.Builder responseCode(int responseCode) {
            this.disbursementResponse.setResponseCode(responseCode);
            return this;
        }

        public DisbursementResponse.Builder error(ThirdPartyException error) {
            this.disbursementResponse.setError(error);
            return this;
        }
    }
}
