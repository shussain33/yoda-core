package com.softcell.gonogo.serialnumbervalidation.comio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ibrar on 16/110/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ComioResponse {

    @JsonProperty("StatusCode")
    private String statusCode;

    @JsonProperty("StatusMsg")
    private String statusMsg;

    @JsonProperty("oError")
    private ThirdPartyException error;
}
