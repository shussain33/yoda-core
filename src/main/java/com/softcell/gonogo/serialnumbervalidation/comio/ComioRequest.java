package com.softcell.gonogo.serialnumbervalidation.comio;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ibrar on 16/11/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ComioRequest {

    @JsonProperty("UserName")
    private String userName;

    @JsonProperty("Password")
    private String password;

    @JsonProperty("AccessKey")
    private String accessKey;

    @JsonProperty("IMEI")
    private String imei;

    @JsonProperty("RequestDate")
    private String requestDate;

    @JsonProperty("RequestType")
    private String requestType;

    @JsonProperty("RetailerCode")
    private String retailerCode;

    @JsonProperty("ModelCode")
    private String modelCode;

}
