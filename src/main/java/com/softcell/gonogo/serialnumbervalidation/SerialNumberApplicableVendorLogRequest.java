package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;


/**
 * Created by sampat on 16/01/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SerialNumberApplicableVendorLogRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("dtStartFrom")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    SerialNumberApplicableVendorLogRequest.FetchGrp.class
            }
    )
    private Date dateStartFrom;

    @JsonProperty("dtEnd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @NotNull(
            groups = {
                    SerialNumberApplicableVendorLogRequest.FetchGrp.class
            }
    )
    private Date endDate;

    public interface FetchGrp {

    }


}
