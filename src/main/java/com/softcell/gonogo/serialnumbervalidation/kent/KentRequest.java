package com.softcell.gonogo.serialnumbervalidation.kent;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 21/9/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KentRequest {


    @JsonProperty("oAuthentication")
    private Authentication Authentication;

    @JsonProperty("oSerialInfo")
    private SerlInfo serlInfo;

}
