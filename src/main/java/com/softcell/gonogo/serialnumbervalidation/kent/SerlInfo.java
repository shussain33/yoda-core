package com.softcell.gonogo.serialnumbervalidation.kent;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SerlInfo {

    @JsonProperty("sSerialNo")
    private String serialNo;

    @JsonProperty("sModelID")
    private String modelID;

    @JsonProperty("sStoreId")
    private String storeId;

    @JsonProperty("sSchemeId")
    private String schemeId;

}
