package com.softcell.gonogo.serialnumbervalidation.kent;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 21/9/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Authentication {

    @JsonProperty("sUsername")
    private String username;

    @JsonProperty("sPassword")
    private String password;

    @JsonProperty("sServicename")
    private String servicename;

    @JsonProperty("sDate")
    private String date;

}

