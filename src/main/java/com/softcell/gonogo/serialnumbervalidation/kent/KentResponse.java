package com.softcell.gonogo.serialnumbervalidation.kent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by sampat on 21/9/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KentResponse {


    @JsonProperty("sOriginalResponse")
    private String originalResponse;

    @JsonProperty("sStatus")
    private String status;

    @JsonProperty("sSaleEligible")
    private String saleEligible;

    @JsonProperty("sStatusCode")
    private String statusCode;

    @JsonProperty("sStatusMsg")
    private String statusMsg;

    @JsonProperty("oError")
    private ThirdPartyException error;

}
