package com.softcell.gonogo.serialnumbervalidation.gionee;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by yogeshb on 7/7/17.
 */
@Data
@Builder
public class GioneeRequest {

    @JsonProperty("UserName")
    private String userName;

    @JsonProperty("Password")
    private String password;

    @JsonProperty("AccessKey")
    private String accessKey;

    @JsonProperty("IMEI")
    private String imei;

    @JsonProperty("RequestDate")
    private String requestDate;

    @JsonProperty("RequestType")
    private String requestType;

    @JsonProperty("RetailerCode")
    private String retailerCode;

    @JsonProperty("ModelCode")
    private String modelCode;

}
