package com.softcell.gonogo.serialnumbervalidation.oppo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Ibrar on 4/10/17.
 */

@Data
@Builder
public class OppoRequest {

    @JsonProperty("sImei")
    private String imei;

    @JsonProperty("sModel")
    private String model;

    @JsonProperty("sOppoDealerId")
    private String oppoDealerId;

    @JsonProperty("sOppoAgent")
    private String oppoAgent;

    @JsonProperty("sAccessCode")
    private String accessCode;

}
