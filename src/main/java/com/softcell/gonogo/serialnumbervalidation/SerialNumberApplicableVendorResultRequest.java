package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * Created by sampat on 22/01/18.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SerialNumberApplicableVendorResultRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("sReferenceId")
    private String referenceId;

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("sStatus")
    private String status;

    public interface FetchGrp {

    }
}
