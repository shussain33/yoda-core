package com.softcell.gonogo.serialnumbervalidation.panasonic;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * Created by prasenjit on 9/8/17.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PanasonicResponse {

    @JsonProperty("sValidateDataResponse")
    private String validateDataResponse;

    @JsonProperty("oError")
    private ThirdPartyException error;

    public String getValidateDataResponse() {
        return validateDataResponse;
    }

    public void setValidateDataResponse(String validateDataResponse) {
        this.validateDataResponse = validateDataResponse;
    }


    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PanasonicResponse)) return false;

        PanasonicResponse that = (PanasonicResponse) o;

        if (!getValidateDataResponse().equals(that.getValidateDataResponse())) return false;
        return getError().equals(that.getError());
    }

    @Override
    public int hashCode() {
        int result = getValidateDataResponse().hashCode();
        result = 31 * result + getError().hashCode();
        return result;
    }


    @Override
    public String toString() {
        return "PanasonicResponse{" +
                "validateDataResponse='" + validateDataResponse + '\'' +
                ", error=" + error +
                '}';
    }




}