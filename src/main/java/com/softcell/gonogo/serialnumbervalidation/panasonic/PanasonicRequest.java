package com.softcell.gonogo.serialnumbervalidation.panasonic;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PanasonicRequest {

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sUserName")
    private String userName;

    @JsonProperty("sPassword")
    private String password;

    @JsonProperty("sAgreementId")
    private String agreementId;

    @JsonProperty("sDealerId")
    private String dealerId;


    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(String agreementId) {
        this.agreementId = agreementId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PanasonicRequest)) return false;

        PanasonicRequest panasonicRequest = (PanasonicRequest) o;

        if (getSkuCode() != null ? !getSkuCode().equals(panasonicRequest.getSkuCode()) : panasonicRequest.getSkuCode() != null) return false;
        if (getSerialNumber() != null ? !getSerialNumber().equals(panasonicRequest.getSerialNumber()) : panasonicRequest.getSerialNumber() != null)
            return false;
        if (getUserName() != null ? !getUserName().equals(panasonicRequest.getUserName()) : panasonicRequest.getUserName() != null)
            return false;
        if (getPassword() != null ? !getPassword().equals(panasonicRequest.getPassword()) : panasonicRequest.getPassword() != null)
            return false;
        if (getAgreementId() != null ? !getAgreementId().equals(panasonicRequest.getAgreementId()) : panasonicRequest.getAgreementId() != null)
            return false;
        return getDealerId() != null ? getDealerId().equals(panasonicRequest.getDealerId()) : panasonicRequest.getDealerId() == null;
    }

    @Override
    public int hashCode() {
        int result = getSkuCode() != null ? getSkuCode().hashCode() : 0;
        result = 31 * result + (getSerialNumber() != null ? getSerialNumber().hashCode() : 0);
        result = 31 * result + (getUserName() != null ? getUserName().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getAgreementId() != null ? getAgreementId().hashCode() : 0);
        result = 31 * result + (getDealerId() != null ? getDealerId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PanasonicRequest{");
        sb.append("skuCode='").append(skuCode).append('\'');
        sb.append(", serialNumber='").append(serialNumber).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", agreementId='").append(agreementId).append('\'');
        sb.append(", dealerId='").append(dealerId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
