package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.constants.Product;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by sampat on 25/8/17.
 */
@Document(collection = "disbursementFeatureLog")
public class DisbursementFeatureLog {

    @JsonProperty("sRefID")
    private String refID;

    @JsonProperty("sInstID")
    private String institutionId;

    @JsonProperty("sVendor")
    private String vendor;

    @JsonProperty("eProduct")
    private Product product;

    @JsonProperty("dtDateTime")
    private Date dateTime = new Date();

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sStatus")
    private String status;

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DisbursementFeatureLog{");
        sb.append("refID='").append(refID).append('\'');
        sb.append(", institutionId='").append(institutionId).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", product=").append(product);
        sb.append(", dateTime=").append(dateTime);
        sb.append(", dealerId='").append(dealerId).append('\'');
        sb.append(", serialNumber='").append(serialNumber).append('\'');
        sb.append(", imeiNumber='").append(imeiNumber).append('\'');
        sb.append(", skuCode='").append(skuCode).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DisbursementFeatureLog)) return false;

        DisbursementFeatureLog disbursementFeatureLog = (DisbursementFeatureLog) o;

        if (getRefID() != null ? !getRefID().equals(disbursementFeatureLog.getRefID()) : disbursementFeatureLog.getRefID() != null) return false;
        if (getInstitutionId() != null ? !getInstitutionId().equals(disbursementFeatureLog.getInstitutionId()) : disbursementFeatureLog.getInstitutionId() != null)
            return false;
        if (getVendor() != null ? !getVendor().equals(disbursementFeatureLog.getVendor()) : disbursementFeatureLog.getVendor() != null) return false;
        if (getProduct() != disbursementFeatureLog.getProduct()) return false;
        if (getDateTime() != null ? !getDateTime().equals(disbursementFeatureLog.getDateTime()) : disbursementFeatureLog.getDateTime() != null)
            return false;
        if (getDealerId() != null ? !getDealerId().equals(disbursementFeatureLog.getDealerId()) : disbursementFeatureLog.getDealerId() != null)
            return false;
        if (getSerialNumber() != null ? !getSerialNumber().equals(disbursementFeatureLog.getSerialNumber()) : disbursementFeatureLog.getSerialNumber() != null)
            return false;
        if (getImeiNumber() != null ? !getImeiNumber().equals(disbursementFeatureLog.getImeiNumber()) : disbursementFeatureLog.getImeiNumber() != null)
            return false;
        if (getSkuCode() != null ? !getSkuCode().equals(disbursementFeatureLog.getSkuCode()) : disbursementFeatureLog.getSkuCode() != null) return false;
        return getStatus() != null ? getStatus().equals(disbursementFeatureLog.getStatus()) : disbursementFeatureLog.getStatus() == null;
    }

    @Override
    public int hashCode() {
        int result = getRefID() != null ? getRefID().hashCode() : 0;
        result = 31 * result + (getInstitutionId() != null ? getInstitutionId().hashCode() : 0);
        result = 31 * result + (getVendor() != null ? getVendor().hashCode() : 0);
        result = 31 * result + (getProduct() != null ? getProduct().hashCode() : 0);
        result = 31 * result + (getDateTime() != null ? getDateTime().hashCode() : 0);
        result = 31 * result + (getDealerId() != null ? getDealerId().hashCode() : 0);
        result = 31 * result + (getSerialNumber() != null ? getSerialNumber().hashCode() : 0);
        result = 31 * result + (getImeiNumber() != null ? getImeiNumber().hashCode() : 0);
        result = 31 * result + (getSkuCode() != null ? getSkuCode().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        return result;
    }

    public static class Builder {

        private DisbursementFeatureLog disbursementFeatureLog = new DisbursementFeatureLog();

        public DisbursementFeatureLog build() {
            return disbursementFeatureLog;
        }

        public DisbursementFeatureLog.Builder refID(String refID) {
            this.disbursementFeatureLog.refID = refID;
            return this;
        }

        public DisbursementFeatureLog.Builder institutionId(String institutionId) {
            this.disbursementFeatureLog.institutionId = institutionId;
            return this;
        }

        public DisbursementFeatureLog.Builder product(Product product) {
            this.disbursementFeatureLog.product = product;
            return this;
        }

        public DisbursementFeatureLog.Builder vendor(String vendor) {
            this.disbursementFeatureLog.vendor = vendor;
            return this;
        }

        public DisbursementFeatureLog.Builder dateTime(Date dateTime) {
            this.disbursementFeatureLog.dateTime = dateTime;
            return this;
        }

        public DisbursementFeatureLog.Builder serialNumber(String serialNumber) {
            this.disbursementFeatureLog.serialNumber = serialNumber;
            return this;
        }

        public DisbursementFeatureLog.Builder dealerId(String dealerId) {
            this.disbursementFeatureLog.dealerId = dealerId;
            return this;
        }

        public DisbursementFeatureLog.Builder imeiNumber(String imeiNumber) {
            this.disbursementFeatureLog.imeiNumber = imeiNumber;
            return this;
        }

        public DisbursementFeatureLog.Builder skuCode(String skuCode) {
            this.disbursementFeatureLog.skuCode = skuCode;
            return this;
        }

        public DisbursementFeatureLog.Builder status(String status) {
            this.disbursementFeatureLog.status = status;
            return this;
        }


    }

}
