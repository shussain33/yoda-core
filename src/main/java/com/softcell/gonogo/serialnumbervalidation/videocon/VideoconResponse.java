package com.softcell.gonogo.serialnumbervalidation.videocon;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

public class VideoconResponse {

    @JsonProperty("iResCode")
    private int responseCode;

    @JsonProperty("oError")
    private ThirdPartyException error;

    @JsonProperty("StatusCode")
    private String statusCode;

    @JsonProperty("StatusMsg")
    private String statusMsg;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }


    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VideoconResponse)) return false;

        VideoconResponse that = (VideoconResponse) o;

        if (getResponseCode() != that.getResponseCode()) return false;
        return getError().equals(that.getError());
    }

    @Override
    public int hashCode() {
        int result = getResponseCode();
        result = 31 * result + getError().hashCode();
        return result;
    }
}
