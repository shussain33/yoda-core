package com.softcell.gonogo.serialnumbervalidation.videocon;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class VideoconRequest {

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sSkuCode")
    private String skuCode;

    @JsonProperty("sFType")
    private String fType;

    @JsonProperty("sInstitutionID")
    private String institutionID;


}