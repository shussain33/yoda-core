package com.softcell.gonogo.serialnumbervalidation.onida;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ibrar on 1/12/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OnidaResponse {

    @JsonProperty("sResCode")
    private String responseCode;

    @JsonProperty("oError")
    private ThirdPartyException error;

}
