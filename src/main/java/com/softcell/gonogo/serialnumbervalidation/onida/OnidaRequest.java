package com.softcell.gonogo.serialnumbervalidation.onida;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ibrar on 1/12/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OnidaRequest {

    @JsonProperty("sSerialNumber")
    private String serialNumber;
}
