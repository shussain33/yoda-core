package com.softcell.gonogo.serialnumbervalidation.carrier;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ibrar on 28/12/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CarrierResponse {

    @JsonProperty("sOriginalResponse")
    private String originalResponse;

    @JsonProperty("iStatusCode")
    private int statusCode;

    @JsonProperty("sMessage")
    private String message;

    @JsonProperty("oError")
    private ThirdPartyException error;
}
