package com.softcell.gonogo.serialnumbervalidation.carrier;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ibrar on 28/12/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarrierRequest {

    @JsonProperty("sSerialNo")
    private String serialNo;

    //make use as a oracle code in carrier request.
    @JsonProperty("sMake")
    private String make;

    @JsonProperty("sSetStatus")
    private int setStatus;

    @JsonProperty("sServiceUserId")
    private String serviceUserId;
}
