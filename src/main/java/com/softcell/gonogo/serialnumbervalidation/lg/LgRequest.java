package com.softcell.gonogo.serialnumbervalidation.lg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 16/3/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LgRequest {

    /**
     * Mandatory field
     */
    @JsonProperty("sSerialNumber")
    private String serialNumber;

    /**
     * Mandatory field
     */
    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    /**
     * Mandatory field
     */
    @JsonProperty("sSkuCode")
    private String skuCode;

    /**
     * Mandatory field
     */
    @JsonProperty("sVendor")
    private String vendor;

    /**
     * Mandatory field
     */

    @JsonProperty("sDealId")
    private String dealId;

    /**
     * Mandatory field
     */

    @JsonProperty("sSchemeId")
    private String schemeId;

    /**
     * Optional field
     */
    @JsonProperty("iFlag")
    private int flag;


}
