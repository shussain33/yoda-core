package com.softcell.gonogo.serialnumbervalidation.lg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 16/3/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LgResponse {

    @JsonProperty("sOriginalResponse")
    private String originalResponse;

    @JsonProperty("oError")
    private ThirdPartyException error;

}
