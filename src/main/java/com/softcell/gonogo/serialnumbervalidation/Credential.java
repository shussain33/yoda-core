package com.softcell.gonogo.serialnumbervalidation;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by yogeshb on 15/3/17.
 */
public class Credential {
    @JsonProperty("sUserId")
    private String userId;

    @JsonProperty("sPassword")
    private String password;



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Credential{");
        sb.append("userId='").append(userId).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Credential that = (Credential) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        return password != null ? password.equals(that.password) : that.password == null;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private Credential credential = new Credential();

        public Credential build(){
            return credential;
        }

        public Builder userId(String userId){
            this.credential.userId = userId;
            return this;
        }

        public Builder password(String password){
            this.credential.password = password;
            return this;
        }

    }
}
