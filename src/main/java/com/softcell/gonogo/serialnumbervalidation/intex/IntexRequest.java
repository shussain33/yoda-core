package com.softcell.gonogo.serialnumbervalidation.intex;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by yogeshb on 16/3/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IntexRequest {

    @JsonProperty("sLoginId")
    private String loginId;

    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sMaterialCode")
    private String materialCode;

}
