package com.softcell.gonogo.serialnumbervalidation.intex;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by yogeshb on 16/3/17.
 */
public class IntexResponse {

    @JsonProperty("sResCode")
    private int responseCode;

    @JsonProperty("oError")
    private ThirdPartyException error;

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public int getResponseCode() {

        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IntexResponse{");
        sb.append("responseCode=").append(responseCode);
        sb.append(", error=").append(error);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IntexResponse that = (IntexResponse) o;

        if (responseCode != that.responseCode) return false;
        return error != null ? error.equals(that.error) : that.error == null;
    }

    @Override
    public int hashCode() {
        int result = responseCode;
        result = 31 * result + (error != null ? error.hashCode() : 0);
        return result;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private IntexResponse intexResponse = new IntexResponse();

        public IntexResponse build() {
            return intexResponse;
        }

        public Builder responseCode(int responseCode) {
            this.intexResponse.responseCode = responseCode;
            return this;
        }

        public Builder error(ThirdPartyException error) {
            this.intexResponse.error = error;
            return this;
        }

    }
}
