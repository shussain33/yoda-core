package com.softcell.gonogo.serialnumbervalidation.nokia;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NokiaResponse {

    @JsonProperty("sResponseCode")
    private String responseCode;

    @JsonProperty("sRequestId")
    private String requestId;

    @JsonProperty("sMsg")
    private String message;

    @JsonProperty("oError")
    private ThirdPartyException error;


}
