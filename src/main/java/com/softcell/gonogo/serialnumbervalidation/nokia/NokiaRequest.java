package com.softcell.gonogo.serialnumbervalidation.nokia;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by prasenjit wadmare on 27/11/17.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NokiaRequest {

    @JsonProperty("sImeiNumber")
    private String imeiNumber;

    @JsonProperty("sModelNumber")
    private String modelNumber;

    @JsonProperty("sRequestId")
    private String requestId;

    @JsonProperty("sClientId")
    private String clientId;

    @JsonProperty("iStatusValue")
    private short statusValue;


}
