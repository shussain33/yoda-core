package com.softcell.gonogo.serialnumbervalidation.samsung;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.ThirdPartyException;

/**
 * Created by yogeshb on 15/3/17.
 */
public class SamsungResponse {

    @JsonProperty("sOriginalResponse")
    private String originalResponse;

    @JsonProperty("oError")
    private ThirdPartyException error;

    public String getOriginalResponse() {
        return originalResponse;
    }

    public void setOriginalResponse(String originalResponse) {
        this.originalResponse = originalResponse;
    }

    public static  Builder builder(){
        return new Builder();
    }

    public ThirdPartyException getError() {
        return error;
    }

    public void setError(ThirdPartyException error) {
        this.error = error;
    }

    public static class Builder{

        private SamsungResponse samsungResponse = new SamsungResponse();

        public SamsungResponse build(){
            return samsungResponse;
        }

        public Builder originalResponse(String originalResponse){
            this.samsungResponse.originalResponse = originalResponse;
            return this;
        }

        public Builder error(ThirdPartyException error){
            this.samsungResponse.error = error;
            return this;
        }
    }


}
