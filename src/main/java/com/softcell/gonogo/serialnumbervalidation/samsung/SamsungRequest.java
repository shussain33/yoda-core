package com.softcell.gonogo.serialnumbervalidation.samsung;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.serialnumbervalidation.Credential;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yogeshb on 15/3/17.
 */
public class SamsungRequest {

    @JsonProperty("oCredential")
    private Credential credential;

    /**
     * Mandatory field
     */
    @JsonProperty("sSerialNumber")
    private String serialNumber;

    @JsonProperty("sImeiNumber")
    private String imeiNumber;


    /**
     * Optional field
     */
    @JsonProperty("sSaleDate")
    private String saleDate;

    /**
     * Mandatory field
     */
    @JsonProperty("sSkuCode")
    private String skuCode;

    /**
     * Mandatory field
     */
    @JsonProperty("sVendor")
    private String vendor;
    /**
     * Optional field
     */
    @JsonProperty("sSaleType")
    private String saleType;

    /**
     * Optional field
     */
    @JsonProperty("sPartnerCode")
    private String partnerCode;

    /**
     * Mandatory field
     */
    @JsonProperty("sReferenceID")
    private String referenceID;

    /**
     * Optional field
     */
    @JsonProperty("sProductType")
    private String productType;

    @JsonProperty("sChannelCode")
    private String channelCode;

    @JsonProperty("sStatus")
    private String status;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public void setReferenceID(String referenceID) {
        this.referenceID = referenceID;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SamsungRequest{");
        sb.append("credential=").append(credential);
        sb.append(", serialNumber='").append(serialNumber).append('\'');
        sb.append(", imeiNumber='").append(imeiNumber).append('\'');
        sb.append(", saleDate='").append(saleDate).append('\'');
        sb.append(", skuCode='").append(skuCode).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        sb.append(", saleType='").append(saleType).append('\'');
        sb.append(", partnerCode='").append(partnerCode).append('\'');
        sb.append(", referenceID='").append(referenceID).append('\'');
        sb.append(", productType='").append(productType).append('\'');
        sb.append(", channelCode='").append(channelCode).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SamsungRequest that = (SamsungRequest) o;

        if (credential != null ? !credential.equals(that.credential) : that.credential != null) return false;
        if (serialNumber != null ? !serialNumber.equals(that.serialNumber) : that.serialNumber != null) return false;
        if (imeiNumber != null ? !imeiNumber.equals(that.imeiNumber) : that.imeiNumber != null) return false;
        if (saleDate != null ? !saleDate.equals(that.saleDate) : that.saleDate != null) return false;
        if (skuCode != null ? !skuCode.equals(that.skuCode) : that.skuCode != null) return false;
        if (vendor != null ? !vendor.equals(that.vendor) : that.vendor != null) return false;
        if (saleType != null ? !saleType.equals(that.saleType) : that.saleType != null) return false;
        if (partnerCode != null ? !partnerCode.equals(that.partnerCode) : that.partnerCode != null) return false;
        if (referenceID != null ? !referenceID.equals(that.referenceID) : that.referenceID != null) return false;
        if (productType != null ? !productType.equals(that.productType) : that.productType != null) return false;
        if (channelCode != null ? !channelCode.equals(that.channelCode) : that.channelCode != null) return false;
        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        int result = credential != null ? credential.hashCode() : 0;
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        result = 31 * result + (imeiNumber != null ? imeiNumber.hashCode() : 0);
        result = 31 * result + (saleDate != null ? saleDate.hashCode() : 0);
        result = 31 * result + (skuCode != null ? skuCode.hashCode() : 0);
        result = 31 * result + (vendor != null ? vendor.hashCode() : 0);
        result = 31 * result + (saleType != null ? saleType.hashCode() : 0);
        result = 31 * result + (partnerCode != null ? partnerCode.hashCode() : 0);
        result = 31 * result + (referenceID != null ? referenceID.hashCode() : 0);
        result = 31 * result + (productType != null ? productType.hashCode() : 0);
        result = 31 * result + (channelCode != null ? channelCode.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static Logger logger = LoggerFactory.getLogger(SamsungRequest.class);

    public static class Builder{
        private SamsungRequest samsungRequest = new SamsungRequest();

        public SamsungRequest build(){
            return samsungRequest;
        }

        public Builder serialNumber(String serialNumber){
            this.samsungRequest.serialNumber = serialNumber;
            return this;
        }

        public Builder imeiNumber(String imeiNumber) {
            this.samsungRequest.imeiNumber = imeiNumber;
            return this;
        }

        public Builder saleDate(String saleDate){

            try {
                String parsedDate = "";
                if (StringUtils.isNotBlank(saleDate)) {
                    long time = Long.parseLong(saleDate);
                    parsedDate = GngDateUtil.toSaleDate(time);
                    this.samsungRequest.saleDate = parsedDate;
                } else {
                    this.samsungRequest.saleDate = saleDate;
                }
            } catch (NumberFormatException e) {
                logger.error("Error converting date for samsung sale date cause {}" + e.getMessage());
                this.samsungRequest.saleDate = saleDate;
            }
            return this;
        }

        public Builder skuCode(String skuCode){
            this.samsungRequest.skuCode = skuCode;
            return this;
        }

        public Builder vendor(String vendor){
            this.samsungRequest.vendor = vendor;
            return this;
        }

        public Builder saleType(String saleType){
            this.samsungRequest.saleType = saleType;
            return this;
        }

        public Builder partnerCode(String partnerCode){
            this.samsungRequest.partnerCode = partnerCode;
            return this;
        }

        public Builder referenceID(String referenceID){
            this.samsungRequest.referenceID = referenceID;
            return this;
        }

        public Builder productType(String productType){
            this.samsungRequest.productType = productType;
            return this;
        }

        public Builder credential(Credential credential){
            this.samsungRequest.credential = credential;
            return this;
        }

        public Builder channelCode(String channelCode) {
            this.samsungRequest.channelCode = channelCode;
            return this;
        }

        public Builder status(String status) {
            this.samsungRequest.status = status;
            return this;
        }

    }
}
