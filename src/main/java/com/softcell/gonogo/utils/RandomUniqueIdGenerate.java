package com.softcell.gonogo.utils;

import java.util.Random;

public class RandomUniqueIdGenerate {

    private static char[] _base62chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
    private static Random _random = new Random();
    private static int length = 7;
    private static char[] _base10digits = "0123456789".toCharArray();

    public static String generateUniqueId() {
        StringBuilder sb = new StringBuilder(length);

        for (int i=0; i<length; i++)
            sb.append(_base62chars[_random.nextInt(62)]);

        return sb.toString();
    }
    public static String generateUniqueId(int newLength) {
        StringBuilder sb = new StringBuilder(newLength);

        for (int i=0; i<newLength; i++)
            sb.append(_base62chars[_random.nextInt(62)]);

        return sb.toString();
    }
    public static String generateNumericOTP(int newLength) {
        StringBuilder sb = new StringBuilder(newLength);

        for (int i=0; i<newLength; i++)
            sb.append(_base10digits[_random.nextInt(10)]);

        return sb.toString();
    }
}
