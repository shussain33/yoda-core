package com.softcell.gonogo.utils.parse;

import com.softcell.gonogo.model.masters.LosUtrDetailsMaster;
import com.softcell.reporting.builder.PojoToJSonTransformer;

import java.util.Set;

/**
 * Created by mahesh on 22/2/17.
 */
public class LosUtrUpdateReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private LosUtrDetailsMaster losUtrDetailsMaster;

    public LosUtrUpdateReportParser(LosUtrDetailsMaster losUtrDetailsMaster) {

        this.losUtrDetailsMaster = losUtrDetailsMaster;
        this.jSonTransformer = new PojoToJSonTransformer<>();

    }

    private LosUtrUpdateReportParser() {

    }

    public Set getAvailableKesy() {
        return jSonTransformer.keySet();
    }

    public PojoToJSonTransformer build() {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setLosUtrUpdateInfo();
        return jSonTransformer;
    }

    private void setLosUtrUpdateInfo() {

        if (null != losUtrDetailsMaster) {

            jSonTransformer.build(losUtrDetailsMaster, "LOS_UTR",
                    jSonTransformer);
        }

    }

}
