package com.softcell.gonogo.utils.parse;

import com.softcell.gonogo.model.request.master.ApplicableSchemeReportDetails;
import com.softcell.reporting.builder.PojoToJSonTransformer;

import java.util.Set;

/**
 * @author mahesh
 */
public class SchemeReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private ApplicableSchemeReportDetails applicableSchemeReportDetails;

    public SchemeReportParser(ApplicableSchemeReportDetails applicableSchemeReportDetails) {

        this.applicableSchemeReportDetails = applicableSchemeReportDetails;
        this.jSonTransformer = new PojoToJSonTransformer<>();

    }

    private SchemeReportParser() {

    }

    public Set getAvailableKesy() {
        return jSonTransformer.keySet();
    }

    public PojoToJSonTransformer build() {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setApplicableSchemeDetails();
        return jSonTransformer;
    }

    private void setApplicableSchemeDetails() {

        if (null != applicableSchemeReportDetails) {

            jSonTransformer.build(applicableSchemeReportDetails, "A_S",
                    jSonTransformer);
        }

    }

}
