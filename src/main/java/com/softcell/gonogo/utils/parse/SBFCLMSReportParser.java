package com.softcell.gonogo.utils.parse;

import com.softcell.gonogo.model.lms.SBFCLMSIntegrationLog;
import com.softcell.reporting.builder.PojoToJSonTransformer;

/**
 * Created by kumar on 30/7/18.
 */
public class SBFCLMSReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private SBFCLMSIntegrationLog sbfclmsIntegrationLog;

    private SBFCLMSReportParser(){    }

    public SBFCLMSReportParser(SBFCLMSIntegrationLog sbfclmsIntegrationLog) {

        this.sbfclmsIntegrationLog = sbfclmsIntegrationLog;
        this.jSonTransformer = new PojoToJSonTransformer<>();

    }

    public PojoToJSonTransformer build() {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setSBFCLMSIntegrationResponse();
        return jSonTransformer;
    }

    private void setSBFCLMSIntegrationResponse() {

        if (null != sbfclmsIntegrationLog) {

            jSonTransformer.build(sbfclmsIntegrationLog, "S_L",
                    jSonTransformer);
        }
    }

}