package com.softcell.gonogo.utils.parse;

import com.softcell.gonogo.serialnumbervalidation.RollbackFeatureLog;
import com.softcell.reporting.builder.PojoToJSonTransformer;

/**
 * Created by mahesh on 30/3/17.
 */
public class RollbackFeatureReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private RollbackFeatureLog rollbackFeatureLog;

    public RollbackFeatureReportParser(RollbackFeatureLog rollbackFeatureLog) {

        this.rollbackFeatureLog = rollbackFeatureLog;
        this.jSonTransformer = new PojoToJSonTransformer<>();

    }

    public PojoToJSonTransformer build() {

        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();

        setRollbackFeatureLog();
        return jSonTransformer;
    }

    private void setRollbackFeatureLog() {

        if (null != rollbackFeatureLog) {

            jSonTransformer.build(rollbackFeatureLog, "R_F",
                    jSonTransformer);
        }

    }

}
