package com.softcell.gonogo.utils.parse;

import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.reporting.builder.PojoToJSonTransformer;

import java.util.Set;

/**
 * Created by mahesh on 16/2/17.
 */
public class SerialNumberReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private SerialNumberInfo serialNumberInfo;

    public SerialNumberReportParser(SerialNumberInfo serialNumberInfo) {

        this.serialNumberInfo = serialNumberInfo;
        this.jSonTransformer = new PojoToJSonTransformer<>();

    }

    private SerialNumberReportParser() {

    }

    public Set getAvailableKesy() {
        return jSonTransformer.keySet();
    }

    public PojoToJSonTransformer build() {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setSerielNumberLogs();
        return jSonTransformer;
    }

    private void setSerielNumberLogs() {

        if (null != serialNumberInfo) {

            jSonTransformer.build(serialNumberInfo, "S_N",
                    jSonTransformer);
        }

    }

}
