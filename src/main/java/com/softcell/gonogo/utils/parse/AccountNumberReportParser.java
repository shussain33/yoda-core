package com.softcell.gonogo.utils.parse;

import com.softcell.gonogo.model.imps.AccountNumberInfo;
import com.softcell.reporting.builder.PojoToJSonTransformer;

import java.util.Set;

/**
 * Created by sampat on 7/11/17.
 */
public class AccountNumberReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private AccountNumberInfo accountNumberInfo;

    public AccountNumberReportParser(AccountNumberInfo accountNumberInfo) {

        this.accountNumberInfo = accountNumberInfo;
        this.jSonTransformer = new PojoToJSonTransformer<>();

    }

    private AccountNumberReportParser() {

    }

    public Set getAvailableKeys() {
        return jSonTransformer.keySet();
    }

    public PojoToJSonTransformer build() {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setAccountNumberInfo();
        return jSonTransformer;
    }

    private void setAccountNumberInfo() {

        if (null != accountNumberInfo) {

            jSonTransformer.build(accountNumberInfo, "A_N",
                    jSonTransformer);
        }

    }

}
