package com.softcell.gonogo.utils.parse;

import com.softcell.gonogo.serialnumbervalidation.SerialNumberApplicableVendorLog;
import com.softcell.reporting.builder.PojoToJSonTransformer;

import java.util.Set;

/**
 * Created by sampat on 7/11/17.
 */
public class SerialNumberApplicableVendorReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private SerialNumberApplicableVendorLog serialNumberApplicableVendorLog;

    public SerialNumberApplicableVendorReportParser(SerialNumberApplicableVendorLog serialNumberApplicableVendorLog) {

        this.serialNumberApplicableVendorLog = serialNumberApplicableVendorLog;
        this.jSonTransformer = new PojoToJSonTransformer<>();

    }

    private SerialNumberApplicableVendorReportParser() {

    }

    public Set getAvailableKeys() {
        return jSonTransformer.keySet();
    }

    public PojoToJSonTransformer build() {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setSerialNumberApplicableVendorLog();
        return jSonTransformer;
    }

    private void setSerialNumberApplicableVendorLog() {

        if (null != serialNumberApplicableVendorLog) {

            jSonTransformer.build(serialNumberApplicableVendorLog, "S_N_A",
                    jSonTransformer);
        }

    }
}
