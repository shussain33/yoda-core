package com.softcell.gonogo.utils;

import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.logger.ActivityLogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created by archana on 22/6/17.
 */
@Component
public class ActivityListener {

    @Autowired
    private ApplicationRepository applicationRepository;

    private Logger logger = LoggerFactory.getLogger(ActivityListener.class);

    @Async
    @EventListener
    public void logActivity(ActivityLogs activityEvent){
        logger.debug("Handling activity event from thread {}", Thread.currentThread().getName());
        // persist in DB
        try{
            applicationRepository.saveActivityLog(activityEvent);
        } catch(Exception ex){
            logger.error("ActivityListener raised Exception while saving activity log -> {} ", ex.getMessage());
        }
    }

}
