package com.softcell.gonogo.utils;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.TopUpDedupeRequest;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.InsuranceRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.UpdateDataRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by archana on 19/6/17.
 */
@Component
public class AuditHelper {

    /*
    Foloowing properties set from http request -
        Action = URI; Ipaddress
    From request header are set InsttId, user
    Currenttime is set for date.
     */
    public ActivityLogs createActivityLog(HttpServletRequest httpRequest, Header applicationHeader){
        // Action and IpAddress from http request
        ActivityLogs activityLog = new ActivityLogs();
        activityLog.setActionDate(new Date());
        if( httpRequest != null) {
            activityLog.setAction(httpRequest.getRequestURI());
            activityLog.setIpAddress(httpRequest.getRemoteAddr());
        }

        // Instt Id, user id, application Id
        if (applicationHeader != null) {
            if (StringUtils.isNotBlank(applicationHeader.getInstitutionId())) {
                activityLog.setInstitutionId(applicationHeader.getInstitutionId());
            }
            // Check whether who has logged in - DSA, CRO etc and set user accordingly
            if (StringUtils.isNotBlank(applicationHeader.getLoggedInUserId())) {
                activityLog.setUserId(applicationHeader.getLoggedInUserId());
                activityLog.setUserName(StringUtils.isNotBlank(applicationHeader.getInstitutionId())
                        ? Cache.getUserName(applicationHeader.getInstitutionId(),applicationHeader.getLoggedInUserId()) : null);
                activityLog.setUserRole(applicationHeader.getLoggedInUserRole());
            }else if (StringUtils.isNotBlank(applicationHeader.getCroId())) {
                activityLog.setUserName(applicationHeader.getCroId());
            }

            if (StringUtils.isNotBlank(applicationHeader.getApplicationId())) {
                activityLog.setApplicationId(applicationHeader.getApplicationId());
            }
        }
        return activityLog;
    }

    public ActivityLogs createActivityLog(ApplicationRequest applicationRequest, HttpServletRequest httpRequest,
                                          String stage, String step, String status, String userId) {

        ActivityLogs activityLog = createActivityLog(httpRequest, applicationRequest.getHeader());
        activityLog.setRefId(applicationRequest.getRefID());

        activityLog.setStage(stage);
        activityLog.setStep(step);
        activityLog.setStatus(status);
        return activityLog;

    }

    public ActivityLogs createActivityLog(Header header, String refId, HttpServletRequest httpRequest,
                                                             String stage, String step, String status, String userId) {

        ActivityLogs activityLog = createActivityLog(httpRequest, header);
        activityLog.setRefId(refId);

        activityLog.setStage(stage);
        activityLog.setStep(step);
        activityLog.setStatus(status);
        return activityLog;

    }

    public static ActivityLogs createActivityLogForComponent( ApplicationRequest applicationRequest, String stage, String componentStep){
        // Action and IpAddress from http request
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setActionDate(new Date());
        activityLogs.setUserName(GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
        activityLogs.setAction(GNGWorkflowConstant.COMPONENT_ACTION.toFaceValue());
        activityLogs.setIpAddress("localhost");
        activityLogs.setRefId(applicationRequest.getRefID());
        activityLogs.setStage(stage);
        activityLogs.setStep(componentStep);
        activityLogs.setStatus(Status.IN_PROCESS.toString());
        // TODO : null check on request
        Header applicationHeader = applicationRequest.getHeader();
        // Instt Id,  application Id
        if (applicationHeader != null) {
            if (StringUtils.isNotBlank(applicationHeader.getInstitutionId())) {
                activityLogs.setInstitutionId(applicationHeader.getInstitutionId());
            }
            if (StringUtils.isNotBlank(applicationHeader.getApplicationId())) {
                activityLogs.setApplicationId(applicationHeader.getApplicationId());
            }
        }
        return activityLogs;
    }

    public static ActivityLogs createActivityLog( ApplicationRequest applicationRequest,
                                                  String stage, String step, String action, String user){
        // Action and IpAddress from http request
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setActionDate(new Date());
        activityLogs.setUserName(user);
        activityLogs.setAction(action);
        activityLogs.setStage(stage);
        activityLogs.setStep(step);

        activityLogs.setIpAddress("localhost");
        activityLogs.setRefId(applicationRequest.getRefID());
        activityLogs.setStatus(Status.IN_PROCESS.toString());
        // TODO : null check on request
        Header applicationHeader = applicationRequest.getHeader();
        // Instt Id,  application Id
        if (applicationHeader != null) {
            if (StringUtils.isNotBlank(applicationHeader.getInstitutionId())) {
                activityLogs.setInstitutionId(applicationHeader.getInstitutionId());
            }
            if (StringUtils.isNotBlank(applicationHeader.getApplicationId())) {
                activityLogs.setApplicationId(applicationHeader.getApplicationId());
            }
        }
        return activityLogs;
    }


    public static ActivityLogs createActivityLogForTopUp(TopUpDedupeRequest topUpDedupeRequest,
                                                         String step, String action, String user){
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setActionDate(new Date());
        activityLogs.setUserName(user);
        activityLogs.setAction(action);
        //activityLogs.setStage(stage);
        activityLogs.setStep(step);

        activityLogs.setIpAddress("localhost");
      //  activityLogs.setRefId(refId);
        activityLogs.setStatus(Status.IN_PROCESS.toString());
        // TODO : null check on request
        Header applicationHeader = topUpDedupeRequest.getHeader();
        // Instt Id,  application Id
        if (applicationHeader != null) {
            if (StringUtils.isNotBlank(applicationHeader.getInstitutionId())) {
                activityLogs.setInstitutionId(applicationHeader.getInstitutionId());
            }
            if (StringUtils.isNotBlank(applicationHeader.getApplicationId())) {
                activityLogs.setApplicationId(applicationHeader.getApplicationId());
            }
        }
        return activityLogs;
    }

    public ActivityLogs createActivityLog(InsuranceRequest insuranceRequest , HttpServletRequest httpRequest,
                                          String userId) {
        ActivityLogs activityLog = createActivityLog(httpRequest, insuranceRequest.getHeader());
        activityLog.setRefId(insuranceRequest.getRefId());
        activityLog.setStage(insuranceRequest.getInsuranceData().getInitiationPoint());
        activityLog.setStep(insuranceRequest.getInsuranceData().getInitiationPoint());
        activityLog.setUserName(userId);
        activityLog.setUserRole(insuranceRequest.getHeader().getLoggedInUserRole());
        return activityLog;

    }

    public ActivityLogs createActivityLog(UpdateDataRequest updateDataRequest, HttpServletRequest httpRequest,
                                          String stage, String step, String status, String userId, String cutomMsg) {

        ActivityLogs activityLog = createActivityLog(httpRequest, updateDataRequest.getHeader());
        activityLog.setRefId(updateDataRequest.getRefId());
        activityLog.setStage(stage);
        activityLog.setStep(step);
        activityLog.setStatus(status);
        activityLog.setUserName(userId);
        activityLog.setUserRole(updateDataRequest.getHeader().getLoggedInUserRole());
        return activityLog;

    }
}
