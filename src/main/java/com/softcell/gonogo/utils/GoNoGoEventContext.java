package com.softcell.gonogo.utils;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * ApplicationContext aware class serving as helper to non-managed beans
 * to avail Spring framework functionalities.
 */
@Component
public class GoNoGoEventContext implements ApplicationEventPublisherAware {

    private static ApplicationEventPublisher applicationEventPublisher;

    public static ApplicationEventPublisher getApplicationEventPublisher() {
        return applicationEventPublisher;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
