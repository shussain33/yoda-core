package com.softcell.gonogo.utils;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class LosTvsHelper {

    /*
    Foloowing properties set from http request -
        Action = URI; Ipaddress
    From request header are set InsttId, user
    Currenttime is set for date.
     */
    public ActivityLogs createActivityLog(HttpServletRequest httpRequest, Header applicationHeader){
        // Action and IpAddress from http request
        ActivityLogs activityLog = new ActivityLogs();
        activityLog.setActionDate(new Date());
        if( httpRequest != null) {
            activityLog.setAction(httpRequest.getRequestURI());
            activityLog.setIpAddress(httpRequest.getRemoteAddr());
        }

        // Instt Id, user id, application Id
        if (applicationHeader != null) {
            if (StringUtils.isNotBlank(applicationHeader.getInstitutionId())) {
                activityLog.setInstitutionId(applicationHeader.getInstitutionId());
            }
            // Check whether who has logged in - DSA, CRO etc and set user accordingly
            if (StringUtils.isNotBlank(applicationHeader.getDsaId())) {
                activityLog.setUserName(applicationHeader.getDsaId());
            }else if (StringUtils.isNotBlank(applicationHeader.getCroId())) {
                activityLog.setUserName(applicationHeader.getCroId());
            }

            if (StringUtils.isNotBlank(applicationHeader.getApplicationId())) {
                activityLog.setApplicationId(applicationHeader.getApplicationId());
            }
        }
        return activityLog;
    }

    public static ActivityLogs createActivityLogForComponent(ApplicationRequest applicationRequest, String stage, String componentStep){
        // Action and IpAddress from http request
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setActionDate(new Date());
        activityLogs.setUserName(GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
        activityLogs.setAction(GNGWorkflowConstant.COMPONENT_ACTION.toFaceValue());
        activityLogs.setIpAddress("localhost");
        activityLogs.setRefId(applicationRequest.getRefID());
        activityLogs.setStage(stage);
        activityLogs.setStep(componentStep);
        activityLogs.setStatus(Status.IN_PROCESS.toString());

        Header applicationHeader = applicationRequest.getHeader();
        // Instt Id,  application Id
        if (applicationHeader != null) {
            if (StringUtils.isNotBlank(applicationHeader.getInstitutionId())) {
                activityLogs.setInstitutionId(applicationHeader.getInstitutionId());
            }
            if (StringUtils.isNotBlank(applicationHeader.getApplicationId())) {
                activityLogs.setApplicationId(applicationHeader.getApplicationId());
            }
        }
        return activityLogs;
    }

    public static ActivityLogs createActivityLog( ApplicationRequest applicationRequest,
                                                  String stage, String step, String action, String user){
        // Action and IpAddress from http request
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setActionDate(new Date());
        activityLogs.setUserName(user);
        activityLogs.setAction(action);
        activityLogs.setStage(stage);
        activityLogs.setStep(step);

        activityLogs.setIpAddress("localhost");
        activityLogs.setRefId(applicationRequest.getRefID());
        activityLogs.setStatus(Status.IN_PROCESS.toString());

        Header applicationHeader = applicationRequest.getHeader();
        // Instt Id,  application Id
        if (applicationHeader != null) {
            if (StringUtils.isNotBlank(applicationHeader.getInstitutionId())) {
                activityLogs.setInstitutionId(applicationHeader.getInstitutionId());
            }
            if (StringUtils.isNotBlank(applicationHeader.getApplicationId())) {
                activityLogs.setApplicationId(applicationHeader.getApplicationId());
            }
        }
        return activityLogs;
    }
}
