/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 2, 2016 5:01:20 PM
 */
package com.softcell.workflow.processors.dedup;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.DedupeDetails;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.ComponentAction;
import com.softcell.workflow.ModuleLog;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author kishorp
 */
@Deprecated
public class DedupeProcessor extends ComponentAction {

    private static final Logger logger = LoggerFactory.getLogger(DedupeProcessor.class);
    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private Component component;

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.ComponentAction#process(com.softcell.gonogo.model.core.GoNoGoCustomerApplication, com.softcell.workflow.component.Component)
     */
    @Override
    public String process(GoNoGoCustomerApplication goNoGoCustomerApplication,
                          Component component) {
        this.component = component;
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        return Status.PASS.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        processModule();
    }

    /**
     *
     */
    private void processModule() {

        if (component.getModuleSettingMap() == null) {
            return;
        }
        // Activity log
        ActivityLogs activityLog =  AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.DEDUPE.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        for (Entry<String, ModuleSetting> module : component.getModuleSettingMap().entrySet()) {

            if (module.getValue().isActive()) {
                ModuleLog dedupeLog = new ModuleLog();
                dedupeLog.setStartTime(new Date());
                goNoGoCustomerApplication.getApplicationLog().setDeduped(dedupeLog);
                if (checkProductDedupeFlag()) {
                    setDedupe();
                }
                checkNegativePinCode();
                dedupeLog.setEndTime(new Date());
            }
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());

        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
    }

    /**
     * @return
     */
    private boolean checkProductDedupeFlag() {

        if (goNoGoCustomerApplication != null) {
            ApplicationRequest application = goNoGoCustomerApplication
                    .getApplicationRequest();
            if (application != null) {
                Request request = application.getRequest();
                if (request.getApplication() != null) {

                    String loanType = request.getApplication().getLoanType();

                    return Cache.DEDUPE_PRODUCT_SET.contains(loanType);
                }
            }
        }
        return false;
    }

    /**
     *
     */
    private void checkNegativePinCode() {

        List<CustomerAddress> addressList = goNoGoCustomerApplication
                .getApplicationRequest().getRequest().getApplicant()
                .getAddress();

        for (CustomerAddress address : addressList) {
            String pinCode = address.getPin() + "";

            if (Cache.NEGATIVE_PINCODESET.get(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()) != null) {

                if (Cache.NEGATIVE_PINCODESET.get(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()).contains(pinCode)){

                    goNoGoCustomerApplication.setNegativePincode(true);

                    break;

                }
            }
        }
    }

    /**
     *
     */
    private void setDedupe() {
        FinalApplicationMongoRepository finalApplicationMongoRepository = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());

        if (goNoGoCustomerApplication.getApplicationRequest() != null) {

            Set<DedupeDetails> dedupeResultSet = finalApplicationMongoRepository.deDupeCustomer(goNoGoCustomerApplication.getApplicationRequest());

            if (dedupeResultSet.contains(goNoGoCustomerApplication.getGngRefId())) {

                dedupeResultSet.remove(goNoGoCustomerApplication.getGngRefId());

            }
            if (null != dedupeResultSet && dedupeResultSet.contains(goNoGoCustomerApplication.getGngRefId())) {
                dedupeResultSet.remove(goNoGoCustomerApplication.getGngRefId());
            }
            Set<String> dedupedRefIdList = dedupeResultSet.stream().map(dedupe -> dedupe.getRefId())
                    .collect(Collectors.toSet());

            goNoGoCustomerApplication.setDedupedApplications(dedupedRefIdList);
        }

    }
}
