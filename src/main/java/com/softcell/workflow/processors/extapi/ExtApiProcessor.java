package com.softcell.workflow.processors.extapi;

import com.softcell.aop.ApplicationProxy;
import com.softcell.constants.CacheConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.workflow.actions.ComponentAction;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.gonogo.workflow.actions.MetaActionInterface;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.module.ModuleSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

public class ExtApiProcessor extends ComponentAction {

    private static final Logger logger = LoggerFactory.getLogger(ExtApiProcessor.class);

    private final String SAATHI_EXECUTOR = "saathiApiExecutor";
    private final String CREDIT_VIDYA_EXECUTOR = "creditVidyaApiExecutor";

    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private Component component;
    private String status = Status.FAIL.toString();

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.ComponentAction#process(com.softcell.gonogo.model.core.GoNoGoCustomerApplication, com.softcell.workflow.component.Component)
     */
    @Override
    public String process(GoNoGoCustomerApplication goNoGoCustomerApplication,
                          Component component) {

        logger.info("> ExtApiProcessor Start for component {} ", component);

        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        this.component = component;

        logger.info("RefID {} : Call to external services {} ; Components to execute {}",
                goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                goNoGoCustomerApplication.getApplicationRequest().getExternalServiceCalls(),
                component.getModuleSettingMap().keySet());

        // For Saathi and CreditVidya check
        saathiModuleCheck();

        logger.info("Call to external services is set as {}", status);
        return status;
    }

    private boolean saathiModuleCheck() {
        boolean saathiModuleFlag = false;

        if (component.getModuleSettingMap().containsKey(SAATHI_EXECUTOR)
                || component.getModuleSettingMap().containsKey(CREDIT_VIDYA_EXECUTOR) ) {

            saathiModuleFlag = true;

            boolean saathiCall = false ; /* when it is reinitiate/reprocess this flag will not come in request
                                        but the call will be decided by module active flag.
                  To know whether request is from reinitiate action , check the reinitiate count.  */

            // Check for reinitiate flow
            if( goNoGoCustomerApplication.getReInitiateCount() == 0 ) { // Normal flow
                // Check for SaathiCall flag
                if (goNoGoCustomerApplication.getApplicationRequest().getExternalServiceCalls() != null) {
                    saathiCall = goNoGoCustomerApplication.getApplicationRequest().getExternalServiceCalls().isCallSaathi();
                }
            } else { // reinitiate flow
                saathiCall = true;
            }

            if( saathiCall ) {
                status = Status.PASS.toString();
            }
        }
        return saathiModuleFlag;
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {

        List<Thread> subTaskList = new ArrayList<Thread>();
        MetaAction metaAction;

        if (component.getModuleSettingMap() == null) {
            return;
        }
        ApplicationContext applicationContext = Cache.COMPONENT_FACTORY_BEAN.get(CacheConstant.BEAN_FACTORY);
        for (Entry<String, ModuleSetting> module : component.getModuleSettingMap().entrySet()) {
            ModuleSetting moduleSetting = module.getValue();
            metaAction = (MetaAction) applicationContext.getBean(moduleSetting.getModuleId());
            metaAction.setModuleSetting(moduleSetting);
            metaAction.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
            MetaActionInterface metaActionInterface = (MetaActionInterface) ApplicationProxy.getProxy(metaAction, MetaActionInterface.class);
            if( StringUtils.equalsIgnoreCase( metaActionInterface.process(goNoGoCustomerApplication,
                    moduleSetting), Status.PASS.toString()) ) {
                Thread t = new Thread(metaActionInterface);
                t.start();
                logger.info("> Module Start " + moduleSetting.getModuleName());
                subTaskList.add(t);
            }
        }

        for (Thread thread : subTaskList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                logger.error("ExtApiProcessor->run()->"+e);
            }
        }
    }


}
