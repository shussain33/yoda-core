package com.softcell.workflow.processors.cloud;

import com.softcell.aop.ApplicationProxy;
import com.softcell.constants.CacheConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.workflow.actions.ComponentAction;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.gonogo.workflow.actions.MetaActionInterface;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author vinodk
 */
public class AmazonS3Processor extends ComponentAction {

    private static final Logger logger = LoggerFactory.getLogger(AmazonS3Processor.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private Component component;

    @Override
    public String process(GoNoGoCustomerApplication goNoGoCustomerApplication,
                          Component component) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        this.component = component;
        return Status.PASS.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        List<Thread> subTaskList = new ArrayList<Thread>();
        MetaAction metaAction;

        if (component.getModuleSettingMap() == null) {
            return;
        }

        ApplicationContext applicationContext = Cache.COMPONENT_FACTORY_BEAN
                .get(CacheConstant.BEAN_FACTORY);

        for (Entry<String, ModuleSetting> module : component.getModuleSettingMap().entrySet()) {

            ModuleSetting moduleSetting = module.getValue();

            metaAction = (MetaAction) applicationContext.getBean(moduleSetting.getModuleId());

            metaAction.setModuleSetting(moduleSetting);

            metaAction.setGoNoGoCustomerApplication(goNoGoCustomerApplication);

            MetaActionInterface metaActionInterface = (MetaActionInterface) ApplicationProxy.getProxy(metaAction, MetaActionInterface.class);

            metaActionInterface.process(goNoGoCustomerApplication,moduleSetting);

            Thread t = new Thread(metaActionInterface);
            t.start();
            subTaskList.add(t);
        }

        for (Thread thread : subTaskList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                logger.error("AmazonS3Processor->run()"+e);
            }
        }
    }
}
