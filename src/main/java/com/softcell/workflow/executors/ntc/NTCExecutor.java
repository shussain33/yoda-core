package com.softcell.workflow.executors.ntc;

import com.google.gson.Gson;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.CibilRespScore;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.ntc.NTCGnGStatus;
import com.softcell.gonogo.model.ntc.NTCResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.response.core.InterimStatus;
import com.softcell.gonogo.service.NTCService;
import com.softcell.gonogo.service.impl.NTCServiceImpl;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.List;

/**
 * NTC executor used for calling NTC service
 *
 * @author bhuvneshk
 */
public class NTCExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(NTCExecutor.class);
    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private ModuleSetting moduleSetting;
    private String institutionId = null;
    private InterimStatus interimStatus;

    private static String getFormattedScore(String score) {
        try {
            if (null != score && score.length() == 5) {
                if (score.charAt(3) == '-') {
                    return "-1";
                } else if (Character.getNumericValue(score.charAt(2)) > 0) {
                    return score.substring(2, score.length());
                } else {
                    if (StringUtils.equalsIgnoreCase(score, "00000")) {
                        return "0";
                    }
                }
            }
        } catch (Exception e) {
            logger.error("NTCExecutor->getFormattedScore()->"+e);
        }
        return null;
    }

    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
        /*
         * if module is not asked to re-process then just return the call.
		 */
        if (!moduleSetting.isActive() ){
            if( goNoGoCustomerApplication.getReInitiateCount() == 0) {
                goNoGoCustomerApplication.getIntrimStatus().setNtcStatus(
                        Status.NOT_AUTHORIZED.toString());
                logger.info(Status.NOT_AUTHORIZED.toString() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for " + moduleSetting.getModuleName());
                logger.error(Status.NOT_AUTHORIZED.toString());
                return;
            }
            return;
        }
        /*if (!moduleSetting.isActive()) {
            goNoGoCustomerApplication.getIntrimStatus().setNtcStatus(
                    Status.NOT_AUTHORIZED.toString());
            logger.info(Status.NOT_AUTHORIZED.toString() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for " + moduleSetting.getModuleName());
            logger.error(Status.NOT_AUTHORIZED.toString());
            return;
        }*/
        if (goNoGoCustomerApplication != null) {
            interimStatus = goNoGoCustomerApplication.getIntrimStatus();

            if (goNoGoCustomerApplication.getApplicationRequest() != null
                && goNoGoCustomerApplication.getApplicationRequest().getHeader() != null) {
                institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
            }
        }
        /**
         * If this service is not allowed for particular products then skip next flow.
         */
        if (checkExclusiveProductFlag()) {
            setInterimStatus(Status.NOT_APPLICABLE.name());
            return;
        }

        Integer cibilScore = getCibilScore();

        logger.info("Inside NTC calculated CIBIL SCORE 1: " + cibilScore);

        // Activity log
        ActivityLogs activityLog =  AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.NTC.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
        try {
            if (cibilScore != null && cibilScore == -1) {
                NTCResponse ntcResponse = callNTCService();
                if (goNoGoCustomerApplication.getApplicationRequest() != null
                        && ntcResponse != null) {
                    goNoGoCustomerApplication.getApplicantComponentResponse()
                            .setNtcResponse(ntcResponse);
                    setNtcGngStatus(ntcResponse);
                }
            }
            setInterimStatus(Status.COMPLETE.name());
        } catch (Exception e) {
            setInterimStatus(Status.ERROR.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            e.printStackTrace();
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);

    }

    private void setNtcGngStatus(NTCResponse ntcResponse) {
        if (interimStatus != null) {
            NTCGnGStatus ntcGngStatus = populatGNGStatus(ntcResponse);
            interimStatus.setNtcGngStatus(ntcGngStatus);
        }
    }

    private NTCGnGStatus populatGNGStatus(NTCResponse ntcResponse) {

        NTCGnGStatus ntcGnGStatus = new NTCGnGStatus();

        ntcGnGStatus.setStatus(ntcResponse.getStatus());
        ntcGnGStatus.setScore(ntcResponse.getNtcScore());
        ntcGnGStatus.setBand(ntcResponse.getNtcScoreBand());

        if (ntcResponse.getErrors() != null && ntcResponse.getErrors().size() > 0) {
            ntcGnGStatus.setMessage(ntcResponse.getErrors().get(0).getDescription());
        }
        return ntcGnGStatus;
    }

    private void setInterimStatus(String status) {
        if (interimStatus != null) {
            interimStatus.setNtcStatus(status);
        }
    }

    /**
     * Call NTC service only if cibil score is less then zero
     *
     * @throws Exception
     */
    private NTCResponse callNTCService() throws Exception {
        NTCService ntcService = new NTCServiceImpl();
        NTCResponse ntcResponse = ntcService.callNTC(goNoGoCustomerApplication);
        return ntcResponse;
    }

    /**
     * It is use to get the cibil score form multi bureau response
     *
     * @return
     */
    private Integer getCibilScore() {
        logger.info("Inside getCibilScore");
        Integer scoreInt = null;
        if (goNoGoCustomerApplication.getApplicantComponentResponse() != null
                && goNoGoCustomerApplication.getApplicantComponentResponse()
                .getMultiBureauJsonRespose() != null) {

            ResponseMultiJsonDomain multiBureauResponse = goNoGoCustomerApplication
                    .getApplicantComponentResponse()
                    .getMultiBureauJsonRespose();

            List<Finished> finishedList = multiBureauResponse.getFinishedList();

            if (finishedList != null && !finishedList.isEmpty()) {
                for (Finished finished : finishedList) {

                    logger.info("Inside getCibilScore Finished List ");
                    if (finished.getBureau().equals(GNGWorkflowConstant.CIBIL.name())) {


                        logger.info("Inside getCibilScore Cibil bureau Object");

                        String cibilObj = new Gson().toJson(finished
                                .getResponseJsonObject());

                        CibilResponse cibilResponse = new Gson().fromJson(cibilObj,
                                CibilResponse.class);

                        List<CibilRespScore> cibilScoreList = cibilResponse.getScoreList();

                        if (cibilScoreList != null && !cibilScoreList.isEmpty()) {
                            CibilRespScore cibilScore = cibilScoreList.get(0);
                            logger.info("Inside getCibilScore Cibil Score List");
                            if (cibilScore != null) {
                                String score = cibilScore.getScore();
                                if (StringUtils.isNotBlank(score)) {

                                    scoreInt = Integer.parseInt(getFormattedScore(score));
                                    return scoreInt;
                                }
                            }

                        }
                        break;
                    }
                }
            }
        }

        return scoreInt;
    }

    @Override
    public String finishProcess() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * @return
     */
    private boolean checkExclusiveProductFlag() {

        if (goNoGoCustomerApplication != null) {
            ApplicationRequest application = goNoGoCustomerApplication
                    .getApplicationRequest();
            if (application != null) {
                Request request = application.getRequest();
                if (request.getApplication() != null) {
                    String loanType = request.getApplication().getLoanType();
                    return Cache.NTC_EXCLUDED_PRODUCT_SET.contains(loanType);
                }
            }
        }
        return false;
    }
}
