package com.softcell.workflow.executors.quickcheck;

/**
 * Created by ssg0302 on 6/8/20.
 */
public class QuickCheckConstants {
    public static final String CREATE_MANDATE = "save-mandate";
    public static final String PROCESS_MANDATE = "e-mandate";
    public static final String RESEND_MANDATE_DETAILS = "resend-mandate-details";

    public static final String yyyyMMdd_With_Slash ="yyyy/MM/dd";

    public static final String QUICK_CHECK_VALUES = "QUICK_CHECK_VALUES";
    public static final String DEFAULT_VALUES = "DEFAULT_VALUES";

    public static final String mdtId = "mdtId";
    public static final String mtype = "mtype";
    public static final String umrn = "umrn";
    public static final String spBankCode = "spBankCode";
    public static final String utlsCode = "utlsCode";
    public static final String tdebit = "tdebit";
    public static final String frequency = "frequency";
    public static final String dtype = "dtype";
    public static final String untlCancel = "untlCancel";
    public static final String isAggregator = "isAggregator";
    public static final String subMerchantId = "subMerchantId";
    public static final String categoryCode = "categoryCode";
    public static final String emandateType = "emandateType";

    public static final String instituteName = "instituteName";
}
