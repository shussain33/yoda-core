package com.softcell.workflow.executors.quickcheck;

import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.los.DateUtils;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.RepaymentDetails;
import com.softcell.gonogo.model.quickcheck.EMandateRequest;
import com.softcell.gonogo.model.quickcheck.QuickCheckAPIRequest;
import com.softcell.gonogo.model.request.core.Header;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.Map;

/**
 * Created by ssg0302 on 5/8/20.
 */
public class QuickCheckExecutorHelper {

    private Map<String, Map<String, String>> fieldMasterMap;

    public QuickCheckExecutorHelper(Map<String, Map<String, String>> fieldMasterMap){
        this.fieldMasterMap = fieldMasterMap;
    }

    protected QuickCheckAPIRequest buildQuickCheckApiRequest(GoNoGoCustomerApplication goNoGoCustomerApplication, LoanCharges loanCharges,
                                                             RepaymentDetails repaymentDetails, String requestType, String acknowledgementId){
        Header header = goNoGoCustomerApplication.getApplicationRequest().getHeader();
        String refId = goNoGoCustomerApplication.getGngRefId();
        EMandateRequest eMandateRequest = null;
        if(StringUtils.equalsIgnoreCase(QuickCheckConstants.CREATE_MANDATE, requestType)) {
            Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();
            String phone = applicant.getPhone().stream().filter(obj -> StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL_MOBILE.toFaceValue(), obj.getPhoneType()))
                    .map(Phone::getPhoneNumber).findFirst().orElse(Constant.BLANK);
            String email = applicant.getEmail().stream().filter(obj -> StringUtils.equalsIgnoreCase(GNGWorkflowConstant.PERSONAL.toFaceValue(), obj.getEmailType()))
                    .map(Email::getEmailAddress).findFirst().orElse(Constant.BLANK);

            eMandateRequest = EMandateRequest.builder()
                    .mdtId(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.mdtId, fieldMasterMap))
                    .mtype(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.mtype, fieldMasterMap))
                    .umrn(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.umrn, fieldMasterMap))
                    .mdate(DateUtils.getFormattedDate(new Date(), QuickCheckConstants.yyyyMMdd_With_Slash))
                    .spBankCode(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.spBankCode, fieldMasterMap))
                    .utlsCode(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.utlsCode, fieldMasterMap))
                    .tdebit(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.tdebit, fieldMasterMap))
                    .bankAc(repaymentDetails.getAccountNumber())
                    .ifsc(repaymentDetails.getIfscCode())
                    .amt(String.valueOf(loanCharges.getLoanChargesList().get(0).getFinalEMIAmt()))
                    .micr(Constant.BLANK)
                    .frequency(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.frequency, fieldMasterMap))
                    .dtype(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.dtype, fieldMasterMap))
                    .ref1(Constant.BLANK)
                    .ref2(Constant.BLANK)
                    .phone(phone)
                    .email(email)
                    .pfrom(DateUtils.getFormattedDate(loanCharges.getLoanChargesList().get(0).getEmiStartDate(), QuickCheckConstants.yyyyMMdd_With_Slash))
                    .pto(Constant.BLANK)
                    .untlCancel(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.untlCancel, fieldMasterMap))
                    .cust1(applicant.getApplicantName().getFullName())
                    .cust2(Constant.BLANK)
                    .cust3(Constant.BLANK)
                    .isAggregator(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.isAggregator, fieldMasterMap))
                    .subMerchantId(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.subMerchantId, fieldMasterMap))
                    .categoryCode(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.categoryCode, fieldMasterMap))
                    .build();
        }else if(StringUtils.equalsIgnoreCase(QuickCheckConstants.PROCESS_MANDATE, requestType)){
            eMandateRequest = EMandateRequest.builder()
                    .acknowledgementId(acknowledgementId)
                    .emandateType(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.emandateType, fieldMasterMap))
                    .build();
        }else if(StringUtils.equalsIgnoreCase(QuickCheckConstants.RESEND_MANDATE_DETAILS, requestType)){
            eMandateRequest = EMandateRequest.builder()
                    .acknowledgementId(acknowledgementId)
                    .build();
        }else
            return null;

        return QuickCheckAPIRequest.builder()
                .header(header)
                .refId(refId)
                .request(eMandateRequest)
                .requestType(requestType)
                .bankName(repaymentDetails.getBankName())
                .institutionName(getValue(QuickCheckConstants.DEFAULT_VALUES, QuickCheckConstants.instituteName, fieldMasterMap))
                .transactionId(refId)
                .build();
    }

    public static String getValue(String fieldTypeKey, String gngValue, Map<String, Map<String, String>> map){
        String value = "";
        if( map.get(fieldTypeKey) != null ){
            Map<String, String> master = map.get(fieldTypeKey);
            if(null!=master && StringUtils.isNotEmpty(master.get(gngValue))) value = master.get(gngValue);
        }
        return value;
    }
}
