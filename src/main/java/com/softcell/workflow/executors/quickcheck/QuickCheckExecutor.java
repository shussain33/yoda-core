package com.softcell.workflow.executors.quickcheck;

import com.softcell.constants.Constant;
import com.softcell.constants.Institute;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.search.MasterMongoRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.ops.Repayment;
import com.softcell.gonogo.model.ops.RepaymentDetails;
import com.softcell.gonogo.model.quickcheck.EMandateDetails;
import com.softcell.gonogo.model.quickcheck.ProcessedRequests;
import com.softcell.gonogo.model.quickcheck.QuickCheckAPIRequest;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.service.QuickCheckAPIManager;
import com.softcell.service.impl.QuickCheckAPIManagerImpl;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ssg0302 on 5/8/20.
 */
public class QuickCheckExecutor extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(QuickCheckExecutor.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private QuickCheckExecutorHelper quickCheckExecutorHelper;

    private ApplicationRepository applicationRepository;

    MasterRepository masterRepository;

    QuickCheckAPIManager quickCheckAPIManager;

    private Map<String, Map<String, String>> fieldMasterMap;

    public String process(Object goNoGoCustomerApplication) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;

        if(null == applicationRepository)
            applicationRepository = new ApplicationMongoRepository();

        if(null == masterRepository)
            masterRepository = new MasterMongoRepository(MongoConfig.getMongoTemplate());

        if(null == quickCheckAPIManager)
            quickCheckAPIManager = new QuickCheckAPIManagerImpl();

        return Status.PASS.toString();
    }

    @Override
    public void run(){
        try{
            String refId = goNoGoCustomerApplication.getGngRefId();
            String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
            String requestType = QuickCheckConstants.CREATE_MANDATE;

            LoanCharges loanCharges = applicationRepository.fetchLoanChargesDetailsByRefId(refId, institutionId);
            Repayment repayment = applicationRepository.fetchRepaymentDetails(refId,institutionId);

            if(null == loanCharges) {
                loanCharges = new LoanCharges();
                List<LoanCharges> loanChargesList = new ArrayList<>();
                loanChargesList.add(new LoanCharges());
                loanCharges.setLoanChargesList(loanChargesList);
            }
            if(null == repayment){
                repayment = new Repayment();
                List<RepaymentDetails> repaymentDetailsList = new ArrayList<>();
                repaymentDetailsList.add(new RepaymentDetails());
                repayment.setRepaymentDetailsList(repaymentDetailsList);
            }

            RepaymentDetails repaymentDetails = repayment.getRepaymentDetailsList().get(0);

            getFieldMasterMap(QuickCheckConstants.QUICK_CHECK_VALUES);
            if(null == quickCheckExecutorHelper)
                quickCheckExecutorHelper = new QuickCheckExecutorHelper(fieldMasterMap);

            QuickCheckAPIRequest quickCheckAPIRequest = quickCheckExecutorHelper.buildQuickCheckApiRequest(goNoGoCustomerApplication, loanCharges, repaymentDetails, requestType, Constant.BLANK);

            BaseResponse createMandateResponse = quickCheckAPIManager.createEMandate(quickCheckAPIRequest);
            if(null != createMandateResponse) {
                Object oCreateMandateResponse = createMandateResponse.getPayload().getT();
                if (oCreateMandateResponse instanceof EMandateDetails) {
                    EMandateDetails createEMandateDetails = (EMandateDetails) oCreateMandateResponse;
                    ProcessedRequests createMandateRequest = createEMandateDetails.getProcessedRequests().stream()
                            .filter(obj -> StringUtils.equalsIgnoreCase(QuickCheckConstants.CREATE_MANDATE, obj.getRequestType()))
                            .findAny().orElse(null);
                    if (null != createMandateRequest && null != createMandateRequest.getEmandateResponse()
                            && null != createMandateRequest.getEmandateResponse().getPayload()
                            && StringUtils.equalsIgnoreCase(Constant.SUCCESS, createMandateRequest.getEmandateResponse().getPayload().getStatus())) {
                        requestType = QuickCheckConstants.PROCESS_MANDATE;
                        quickCheckAPIRequest = quickCheckExecutorHelper.buildQuickCheckApiRequest(goNoGoCustomerApplication, loanCharges, repaymentDetails, requestType, createEMandateDetails.getAcknowledgementId());
                        BaseResponse processMandateResponse = quickCheckAPIManager.processEMandate(quickCheckAPIRequest);
                        if(null != processMandateResponse) {
                            Object oProcessMandateResponse = processMandateResponse.getPayload().getT();
                            if (oProcessMandateResponse instanceof EMandateDetails) {
                                EMandateDetails processEMandateDetails = (EMandateDetails) oProcessMandateResponse;
                                ProcessedRequests processMandateRequests = processEMandateDetails.getProcessedRequests().stream()
                                        .filter(obj -> StringUtils.equalsIgnoreCase(QuickCheckConstants.PROCESS_MANDATE, obj.getRequestType()))
                                        .findAny().orElse(null);
                                if (null != processMandateRequests && null != processMandateRequests.getEmandateResponse()
                                        && null != processMandateRequests.getEmandateResponse().getPayload()
                                        && StringUtils.equalsIgnoreCase(Constant.SUCCESS, processMandateRequests.getEmandateResponse().getPayload().getStatus())
                                        && StringUtils.isNotEmpty(processMandateRequests.getEmandateResponse().getPayload().getEmandateURL())) {
                                    requestType = QuickCheckConstants.RESEND_MANDATE_DETAILS;
                                    quickCheckExecutorHelper.buildQuickCheckApiRequest(goNoGoCustomerApplication, loanCharges, repaymentDetails, requestType, createEMandateDetails.getAcknowledgementId());
                                }
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            logger.error("Exception Occurred in QuickCheckExecutor. {} ", e.getStackTrace());
        }
    }

    private Map<String, Map<String, String>> getFieldMasterMap(String value) {
        // fieldMasterMap
        if( fieldMasterMap == null){
            fieldMasterMap = masterRepository.getThirdPartyFieldValues(Institute.SBFC, value);
        }
        return fieldMasterMap;
    }
}
