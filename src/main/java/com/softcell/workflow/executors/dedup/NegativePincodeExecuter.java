package com.softcell.workflow.executors.dedup;

import com.softcell.constants.Status;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author kishor
 *         Negative PinCode executer is used to  check Pincode validation
 *         and address validation.
 */
public class NegativePincodeExecuter extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(NegativePincodeExecuter.class);
    ModuleOutcome moduleOutcome;
    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private ModuleSetting moduleSetting;

    @Override
    public String process(Object goNoGoCustomerApplication, ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub

    }

    @Override
    public String finishProcess() {
        // TODO Auto-generated method stub
        return null;
    }

}
