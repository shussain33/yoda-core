/**
 * kishorp2:32:49 PM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow.executors.dedup;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.core.DedupeMatch;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.jobs.DedupJobV2;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

/**
 * @author kishorp
 *
 */
public class DedupeExecuterV2 extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(DedupeExecuter.class);

    private ModuleSetting moduleSetting;
    /**
     *
     * @param goNoGoCustomerApplication
     * @param moduleSetting
     * @return
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
        checkDedupe();
    }

    private void checkDedupe() {
        try {
            ApplicationRequest applicationRequest = null;
            String institutionId = null;
            if (goNoGoCustomerApplication.getApplicationRequest() != null) {
                applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
                if(applicationRequest.getHeader() == null && applicationRequest.getRequest() == null && applicationRequest.getRequest().getApplicant() == null){
                    logger.debug("gonogoCustomerApplication Header, Request and Applicant detials not found for the {}", goNoGoCustomerApplication.getGngRefId());
                    return;
                }
                if (applicationRequest.getHeader() != null)
                    institutionId = applicationRequest.getHeader().getInstitutionId();
            }

            ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                    GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.DEDUPE.toFaceValue());
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            //Constructing DedupeMatch details object.
            DedupeMatch dedupeMatch = new DedupeMatch();
            dedupeMatch.setRefId(goNoGoCustomerApplication.getGngRefId());
            dedupeMatch.setInstitutionId(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());

            /* The below code is to do Internal dedue check. */
            DedupJobV2 dedupJobV2 = new DedupJobV2();
            dedupJobV2.prerequisite(goNoGoCustomerApplication);
            JobResult jobResult = dedupJobV2.call();

            stopWatch.stop();
            activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
            logger.debug(" goNoGoCustomerApplication {} after finishing dedup job in {} ", goNoGoCustomerApplication, stopWatch.getTotalTimeMillis());
            // Save activity log
            logger.debug(String.format("Publishing activity from thread %s for DedeupeExecuter : refId %s",
                    Thread.currentThread().getName(), goNoGoCustomerApplication.getApplicationRequest().getRefID()));

            GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("" + e.getMessage());
        } finally {
            synchronized (componentManager) {
                componentManager.executionFinished(getActionId());
                componentManager.notifyAll();
            }
        }
    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }
}
