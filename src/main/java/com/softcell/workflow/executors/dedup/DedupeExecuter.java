/**
 * kishorp2:32:49 PM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow.executors.dedup;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.jobs.DedupJob;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

/**
 * @author kishorp
 *
 */
public class DedupeExecuter extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(DedupeExecuter.class);

    ModuleOutcome moduleOutcome;
    private ModuleSetting moduleSetting;
    private ModuleRequestRepository moduleRequestRepository;

    /**
     *
     * @param goNoGoCustomerApplication
     * @param moduleSetting
     * @return
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
       checkDedupe();
    }

    private void checkDedupe() {
        /*
		 * if module is not asked to re-process then just return the call. 
		 */
        /*if (!moduleSetting.isActive() && goNoGoCustomerApplication.getReInitiateCount() > 0) {
            return;
        }
        moduleOutcome = new ModuleOutcome();*/

       /* if (!moduleSetting.isActive()) {
            moduleOutcome.setFieldValue(Status.NOT_AUTHORIZED.toString());

            goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.NOT_AUTHORIZED.toString());

            logger.info(Status.NOT_AUTHORIZED.toString() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for " + moduleSetting.getModuleName());
            logger.error(Status.NOT_AUTHORIZED.toString());
            return;
        }*/
        ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.DEDUPE.toFaceValue());

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        DedupJob dedupJob = new DedupJob();
        dedupJob.prerequisite(goNoGoCustomerApplication, false);
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        dedupJob.setModuleRequestRepository(moduleRequestRepository);
        try {
            JobResult jobResult = dedupJob.call();
            logger.info("" + jobResult.getResult());
            stopWatch.stop();
            activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
            logger.debug(" goNoGoCustomerApplication {} after finishing dedup job in {} ", goNoGoCustomerApplication, stopWatch.getTotalTimeMillis());
            // Save activity log
            logger.debug(String.format("Publishing activity from thread %s for DedeupeExecuter : refId %s",
                    Thread.currentThread().getName(), goNoGoCustomerApplication.getApplicationRequest().getRefID()));
            GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
        }catch(Exception e){
            logger.error("{}",e.getStackTrace());
            logger.error("" + e.getMessage());
        }finally {

        synchronized (componentManager) {
            componentManager.executionFinished(getActionId());
            componentManager.notifyAll();
        }
    }
    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }
}
