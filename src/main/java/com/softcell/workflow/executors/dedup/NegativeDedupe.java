/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 2, 2016 10:19:16 PM
 */
package com.softcell.workflow.executors.dedup;

import com.softcell.constants.Status;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.module.ModuleSetting;

/**
 * @author kishorp
 *
 */
public class NegativeDedupe extends MetaAction {

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#process(java.lang.Object, com.softcell.workflow.component.module.ModuleSetting)
     */
    @Override
    public String process(Object bean, ModuleSetting moduleSetting) {
        return Status.PASS.toString();
    }


    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#run()
     */
    @Override
    public void run() {

    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }

}
