package com.softcell.workflow.executors.intimation;

import com.softcell.config.SmsServiceConfiguration;
import com.softcell.constants.*;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationMongoRepository;
import com.softcell.dao.mongodb.repository.systemConfiguration.AppConfigurationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.HeaderKey;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.configuration.admin.ActionGroup;
import com.softcell.gonogo.model.configuration.admin.IntimationConfig;
import com.softcell.gonogo.model.configuration.admin.IntimationGroup;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.contact.Phone;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.email.EmailResponse;
import com.softcell.gonogo.model.email.PostParameterKeys;
import com.softcell.gonogo.model.email.SendMailRequest;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.HierarchyUser;
import com.softcell.gonogo.model.masters.OrganizationalHierarchyMasterV2;
import com.softcell.gonogo.model.request.TemplateDetails;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.smsservice.SmsServiceBaseRequest;
import com.softcell.gonogo.model.response.SmsResponse;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.model.response.core.Error;
import com.softcell.gonogo.model.response.smsservice.SmsServiceBaseResponse;
import com.softcell.gonogo.model.security.v2.Branch;
import com.softcell.gonogo.model.security.v2.LoginServiceResponse;
import com.softcell.gonogo.service.SmsServiceCaller;
import com.softcell.gonogo.service.impl.SmsServiceCallerImpl;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.AppConfigurationHelper;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.GngUtils;
import com.softcell.utils.SmsUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.util.StopWatch;

import javax.inject.Inject;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by amit on 16/8/18.
 */
public class IntimationExecutor extends Thread {

    private WorkFlowCommunicationManager workFlowCommunicationManager;

    @Inject
    private AppConfigurationRepository appConfigurationRepository;

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private SmsServiceCaller smsServiceCaller;

    private LookupService lookupService;

    private static MongoTemplate mongoTemplate = MongoConfig.getMongoTemplate();

    GoNoGoCustomerApplication goNoGoCustomerApplication;

    private String action;

    private String mailSubject;

    private String mailContent;

    private String smsContent;

    private String verificationType;

    private String agencyName;

    private IntimationConfig intimationConfig;

    private Logger logger = LoggerFactory.getLogger(IntimationExecutor.class);

    private List<HierarchyUser> otherUsers;

    private List<String> otherMailIds = new ArrayList<>();

    private List<String> otherPhoneNumbers = new ArrayList<>();

    public String process(Object goNoGoCustomerApplication, IntimationConfig intimationConfig, String action,
                          List<HierarchyUser> otherUsers, String verificationType, String agencyName) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.intimationConfig = intimationConfig;
        this.action = action;
        this.verificationType = verificationType;
        this.agencyName = agencyName;
        this.otherUsers = (otherUsers == null)? new ArrayList<>() : otherUsers;
        ApplicationContext applicationContext = Cache.COMPONENT_FACTORY_BEAN.get(CacheConstant.BEAN_FACTORY);
        appConfigurationRepository = (AppConfigurationMongoRepository) applicationContext.getAutowireCapableBeanFactory()
                .autowire(AppConfigurationMongoRepository.class, 0, true);
        workFlowCommunicationManager =(WorkFlowCommunicationManagerImpl) applicationContext.getAutowireCapableBeanFactory()
                .autowire(WorkFlowCommunicationManagerImpl.class, 0, true);
        smsServiceCaller = (SmsServiceCallerImpl) applicationContext.getAutowireCapableBeanFactory()
                .autowire(SmsServiceCallerImpl.class, 0, true);
        lookupService = (LookupServiceHandler) applicationContext.getAutowireCapableBeanFactory()
                .autowire(LookupServiceHandler.class, 0, true);
        return null;
    }

    @Override
    public void run() {
        synchronized (this) {
            Header header = goNoGoCustomerApplication.getApplicationRequest().getHeader();
            Branch branch = goNoGoCustomerApplication.getApplicationRequest().getAppMetaData().getBranchV2();
            for (ActionGroup actionGroup : intimationConfig.getActionGroupList()) {
                if (StringUtils.equalsIgnoreCase(action, actionGroup.getActionName())) {
                    if (actionGroup.isEmailIntimation() || actionGroup.isSmsIntimation()) {

                        List<HierarchyUser> userList;
                        List<LoginServiceResponse> finalUserList = new ArrayList<>();
                        if (actionGroup.isEmailIntimation()) {
                            try {
                                userList = getUsers(intimationConfig, actionGroup, IntimationConstants.INTIMATION_TYPE_EMAIL, header, branch, finalUserList);
                                List<String> mailIds = getMailIds(userList, finalUserList);
                                if (CollectionUtils.isNotEmpty(mailIds)) {
                                    String mailTemplate = actionGroup.getEmailTemplate();
                                    this.mailContent = getTemplateContent(header, mailTemplate, goNoGoCustomerApplication.getGngRefId(), true, verificationType);
                                    logger.debug("For {} MailIds : {}", goNoGoCustomerApplication.getGngRefId(), mailIds);
                                    logger.debug("Sending mail for refId {} for action {}", goNoGoCustomerApplication.getGngRefId(),
                                            this.action);
                                    sendMail(header, goNoGoCustomerApplication.getGngRefId(), mailIds);
                                } else {
                                    logger.debug("No mail ids found!");
                                }
                            } catch (Exception e) {
                                logger.error("Error occoured while sending email! {}", ExceptionUtils.getStackTrace(e));
                            }
                        }
                        if (actionGroup.isSmsIntimation()) {
                            try {
                                finalUserList = new ArrayList<>();
                                userList = getUsers(intimationConfig, actionGroup, IntimationConstants.INTIMATION_TYPE_SMS, header, branch, finalUserList);
                                List<String> mobileNoList = getMobileNos(userList, finalUserList);
                                if (CollectionUtils.isNotEmpty(mobileNoList)) {
                                    String smsTemplate = actionGroup.getMessageTemplate();
                                    this.smsContent = getTemplateContent(header, smsTemplate, goNoGoCustomerApplication.getGngRefId(), false, verificationType);
                                    logger.debug("For {} MobileNos : {}, Content: {}", goNoGoCustomerApplication.getGngRefId(), mobileNoList, this.smsContent);
                                    logger.debug("Sending SMS for refId {} for action {}", goNoGoCustomerApplication.getGngRefId(),
                                            this.action);
                                    sendSms(header, goNoGoCustomerApplication.getGngRefId(), mobileNoList);
                                } else {
                                    logger.debug("No mobile numbers found!");
                                }
                            } catch (Exception e) {
                                logger.error("Error occoured while sending sms! {}", ExceptionUtils.getStackTrace(e));
                            }
                        }
                    }
                }
            }
        }
    }

    public void intimateGoNoGoCustomer(String templateName){
        Header header = goNoGoCustomerApplication.getApplicationRequest().getHeader();
        List <String>emailIds = getCustomerMailIds(this.goNoGoCustomerApplication.getApplicationRequest().getRequest());
        if(CollectionUtils.isNotEmpty(emailIds)) {
            this.mailContent = getTemplateContent(header, templateName, goNoGoCustomerApplication.getGngRefId());
            logger.debug("Sending mail for refId {} with  template {}" , goNoGoCustomerApplication.getGngRefId(),
                    templateName);
            sendMail(header, goNoGoCustomerApplication.getGngRefId(), emailIds);
        }else{
            logger.debug("No mail ids found! for refId {} ", goNoGoCustomerApplication.getGngRefId());
        }
    }

    private String getTemplateContent(Header header, String templateKey, String refId){
        try {
            TemplateDetails templateDetails = new AppConfigurationHelper().generateTemplateUsingApplicationData(header.getInstitutionId(), header.getProduct().name(), templateKey);
            if (templateDetails == null) {
                return "Application ID : " + refId + " has been updated, No details found!";
            } else {
                this.mailSubject = fetchApplicationDataForTemplate(header, refId, templateDetails.getMailSubject(),null);
                String content = fetchApplicationDataForTemplate(header, refId, templateDetails.getTemplateContent(),verificationType);
                return content;
            }
        }catch (Exception e) {
            logger.debug("Error while fetching template content for refId : " + refId);
            return "Application ID : " + refId + " has been updated, No details found!";
        }
    }
    private List<String> getCustomerMailIds(Request request) {
        Set<String> mailIds = new HashSet<>();
        request.getApplicant().getEmail().forEach(obj -> {
            mailIds.add(obj.getEmailAddress());
        });
        /*request.getCoApplicant().forEach(coAp -> {
            coAp.getEmail().forEach(coApEmail -> {
                mailIds.add(coApEmail.getEmailAddress());
            });
        });*/
        return new ArrayList<>(mailIds);
    }

    private String getTemplateContent(Header header, String templateName, String refId, boolean isMail, String verificationType) {
        try {
            if (isMail) {
                TemplateDetails templateDetails = new AppConfigurationHelper().generateTemplateUsingApplicationData(refId,
                        header.getInstitutionId(), header.getProduct().name(), templateName, header.getLoggedInUserRole(),verificationType, this.agencyName);
                if (templateDetails == null) {
                    return "Application ID : " + refId + " has been updated, No details found!";
                } else {
                    this.mailSubject = fetchApplicationDataForTemplate(header, refId, templateDetails.getMailSubject(),null);
                    String content = fetchApplicationDataForTemplate(header, refId, templateDetails.getTemplateContent(),verificationType);
                    return content;
            }
        } else {
                String smsMsg = null;
                SmsTemplateConfiguration smsTemplateConfiguration  = lookupService.getSmsTemplateConfiguration(
                        header.getInstitutionId()
                        , header.getProduct().toProductId()
                        , templateName);
                if( null != smsTemplateConfiguration) {
                    smsMsg = fetchApplicationDataForTemplate(header, refId, smsTemplateConfiguration.getSmsText(), null);
                }else {
                    smsTemplateConfiguration = lookupService.getSmsTemplateConfiguration(
                            header.getInstitutionId()
                            , header.getProduct().toProductId()
                            , GNGWorkflowConstant.OTP_SMS.name());
                    String otpMessage = GngUtils.generateOtp();
                    if (smsTemplateConfiguration != null) {
                        smsMsg = smsTemplateConfiguration.getFormattedSMSText(refId, otpMessage);
                    }
                }
            return smsMsg;
        }
        }catch (Exception e) {
            logger.debug("Error while fetching template content for refId : " + refId + ", Mail : " + isMail);
            return "Application ID : " + refId + " has been updated, No details found!";
        }
    }

    private String fetchApplicationDataForTemplate(Header header, String refId, String content, String verificationType) {
        Map<String, String> clientInfo = null;
        Pattern pattern = Pattern.compile("\\{\\{([^}]+)\\}\\}");
        Matcher matcher = pattern.matcher(content);

        // Get application fields configured in template
        List<String> fields = new ArrayList<>();
        while (matcher.find()) {
            String key = matcher.group(1);
            fields.add(key);
        }
        //get application data by fields
        try {
            clientInfo = appConfigurationRepository.getTemplateConfigrationData(refId, fields, header.getInstitutionId(), verificationType);
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
        }
        //Replace application values in template
        if(clientInfo != null) {
            logger.debug(clientInfo.toString());
            if(StringUtils.isNotEmpty(this.agencyName))
                clientInfo.put(TemplateConstants.VERIFICATION_AGENCY, this.agencyName);
            for (Map.Entry<String, String> tempMap : clientInfo.entrySet()) {
                content = content.replaceAll(tempMap.getKey(), tempMap.getValue());
            }
        }
        content = content.replaceAll("\\{\\{", " ");
        content = content.replaceAll("\\}\\}", " ");
        content = content.replaceAll("&nbsp;"," ");
        content = content.replaceAll("&amp;","&");
        content = content.replaceAll("\\<[^>]*>","");
        return content;
    }

    private List<String> getMobileNos(List<Object> userList) {
        Set<String> mobileNoList = new HashSet<>();
        if(CollectionUtils.isNotEmpty(userList)) {
            if(userList.get(0) instanceof HierarchyUser) {
                userList.forEach(user -> {
                    mobileNoList.add(((HierarchyUser)user).getContactNo());
                });
            } else if(userList.get(0) instanceof LoginServiceResponse){
                userList.forEach(user -> {
                    mobileNoList.add(((LoginServiceResponse)user).getMobile());
                });
            }
        }
        if(CollectionUtils.isNotEmpty(this.otherPhoneNumbers)) mobileNoList.addAll(this.otherPhoneNumbers);
        return new ArrayList<>(mobileNoList);
    }

    private List<String> getMobileNos(List<HierarchyUser> userList, List<LoginServiceResponse> finalUserList) {
        Set<String> mobileNoList = new HashSet<>();
        if(CollectionUtils.isNotEmpty(userList)) {
            userList.forEach(user -> {
                mobileNoList.add(user.getContactNo());
            });
        }
        if(CollectionUtils.isNotEmpty(finalUserList)){
            finalUserList.forEach(finalUser -> {
                mobileNoList.add(finalUser.getMobile());
            });
        }

        if(CollectionUtils.isNotEmpty(this.otherPhoneNumbers)) mobileNoList.addAll(this.otherPhoneNumbers);
        return new ArrayList<>(mobileNoList);
    }

    private List<String> getMailIds(List<Object> userList) {
        Set<String> mailIds = new HashSet<>();
        if (CollectionUtils.isNotEmpty(userList)) {
            if (userList.get(0) instanceof HierarchyUser) {
                userList.forEach(user -> {
                    if (StringUtils.isNotEmpty(((HierarchyUser) user).getEmailId()))
                        mailIds.add(((HierarchyUser) user).getEmailId());
                });
            } else if (userList.get(0) instanceof LoginServiceResponse) {
                userList.forEach(user -> {
                    if (StringUtils.isNotEmpty(((LoginServiceResponse) user).getEmail()))
                        mailIds.add(((LoginServiceResponse) user).getEmail());
                });
            }
        }
        if (CollectionUtils.isNotEmpty(this.otherMailIds)) mailIds.addAll(this.otherMailIds);
        return new ArrayList<>(mailIds);
    }

    private List<String> getMailIds(List<HierarchyUser> userList, List<LoginServiceResponse> finalUserList) {
        Set<String> mailIds = new HashSet<>();
        if (CollectionUtils.isNotEmpty(userList)) {
            userList.forEach(hierarchyUser -> {
                if (StringUtils.isNotEmpty(hierarchyUser.getEmailId()))
                    mailIds.add(hierarchyUser.getEmailId());
            });
        }
        if (CollectionUtils.isNotEmpty(finalUserList)) {
            finalUserList.forEach(loginServiceUser -> {
                if (StringUtils.isNotEmpty(loginServiceUser.getEmail()))
                    mailIds.add(loginServiceUser.getEmail());
            });
        }
        if (CollectionUtils.isNotEmpty(this.otherMailIds)) mailIds.addAll(this.otherMailIds);
        return new ArrayList<>(mailIds);
    }

    private List<HierarchyUser> getUsers(IntimationConfig intimationConfig, ActionGroup actionGroup, String intimationType,
                                         Header header, Branch branch, List<LoginServiceResponse> finalUserList) {
        List<HierarchyUser> userList;
        List<IntimationGroup> intimationGroups = null;
        List<String> groupNames = null;
        Set<String> defaultIntimationGroups = new HashSet<>();
        if(StringUtils.equals(IntimationConstants.INTIMATION_TYPE_EMAIL, intimationType)){
            groupNames = actionGroup.getEmailGroups();
        } else if(StringUtils.equals(IntimationConstants.INTIMATION_TYPE_SMS, intimationType)){
            groupNames = actionGroup.getMsgGroups();
        }
        //Intimation groups are generic(default names) in types : fetch exact group names:
        if(CollectionUtils.isNotEmpty(groupNames)) {
            intimationGroups = extractActualGroupsAccordingToData(intimationConfig.getGroups(), intimationConfig.getDefaultGroups(),
                    defaultIntimationGroups, groupNames, header, branch);
        }else
            intimationGroups = new ArrayList<>();

        logger.info("Intimation for refId {} on action {}, ConfiguredGroups : {}, ActualGroups : {}, ActualDefaultIntimationGroups : {}, OtherUsers : {}, OtherMailIds: {}, OtherPhoneNumbers: {}",
                goNoGoCustomerApplication.getGngRefId(), this.action, groupNames, intimationGroups, defaultIntimationGroups, this.otherUsers, this.otherMailIds, this.otherPhoneNumbers);
        // add direct users assigned to thread
        userList = intimationGroups.stream().filter(group -> (null != group.getUsers())).flatMap(group -> group.getUsers().stream()).collect(Collectors.toList());
        if(CollectionUtils.isNotEmpty(this.otherUsers)){
            if(CollectionUtils.isEmpty(userList))
                userList = new ArrayList<>();
            userList.addAll(this.otherUsers);
            if(otherMailIds == null) otherMailIds = new ArrayList<>();
            if(otherPhoneNumbers == null) otherPhoneNumbers = new ArrayList<>();
            this.otherUsers.forEach(user -> {
                otherMailIds.add(user.getEmailId());
                otherPhoneNumbers.add(user.getContactNo());
            });
        }
        getFinalUsersList(defaultIntimationGroups, header, branch, finalUserList);
        return userList;
    }

    private void getFinalUsersList(Set<String> defaultIntimationGroups, Header header, Branch branch, List<LoginServiceResponse> finalUserList){
        Set<String> tmpIds = new HashSet<>();
        defaultIntimationGroups.forEach(searchType -> {
            List<LoginServiceResponse> tmpList = null;
            String[] searchCriteria = new String[2];
            if (StringUtils.equalsIgnoreCase(searchType, IntimationConstants.BRANCH)) {
                searchCriteria[0] = branch.getBranchName(); // BranchWise
            } else if (searchType.contains("(" + IntimationConstants.GROUP_TYPE_PAN_INDIA + ")")) {
                searchCriteria[0] = header.getProduct().name(); // ProductWise
            } else if (searchType.startsWith(IntimationConstants.BRANCH + "_")) {
                String role1 = searchType.replaceAll(IntimationConstants.BRANCH + "_", "");
                searchCriteria[0] = branch.getBranchName();
                searchCriteria[1] = role1; //BranchRoleWise
            } else if (StringUtils.equalsIgnoreCase(searchType, IntimationConstants.ZONE)) {
                searchCriteria[0] = branch.getZone(); //ZoneWise
            } else if (searchType.startsWith(IntimationConstants.ZONE + "_")) {
                String role2 = searchType.replaceAll(IntimationConstants.ZONE + "_", "");
                searchCriteria[0] = branch.getZone();
                searchCriteria[1] = role2; //ZoneRoleWise
            } else if (searchType.startsWith(IntimationConstants.LOCATION + "_")) {
                String role2 = searchType.replaceAll(IntimationConstants.LOCATION + "_", "");
                searchCriteria[0] = branch.getLocation();
                searchCriteria[1] = role2; //LocationRoleWise
            } else if (searchType.startsWith(IntimationConstants.REGION + "_")) {
                String role2 = searchType.replaceAll(IntimationConstants.REGION + "_", "");
                searchCriteria[0] = branch.getRegion();
                searchCriteria[1] = role2; //RegionRoleWise
            }else{
                searchCriteria[0] = searchType ;  // RoleWise
            }
            tmpList = appConfigurationRepository.fetchUserDetails(header.getInstitutionId(), searchType, searchCriteria);
            tmpList.forEach(user -> {
                if (!tmpIds.contains(user.getId())) {
                    tmpIds.add(user.getId());
                    finalUserList.add(user);
                }
            });
        });
    }

    private List<IntimationGroup> extractActualGroupsAccordingToData(List<IntimationGroup> groups, Set<String> defaultGroups, Set<String> defaultIntimationGroups,
                                                                     List<String> groupNames, Header header, Branch branch) {
        List<IntimationGroup> filteredGroups = new ArrayList<>();
        String instId = header.getInstitutionId();
        String product = header.getProduct().name();
        int branchId = branch.getBranchId();

        //TODO Amit : Fetching Data from DB for now
        OrganizationalHierarchyMasterV2 hierarchyMaster =
                appConfigurationRepository.fetchHierarchyMasterForProductBranch(instId, product, branchId);

        /* // Skipped this code to remove cache dependency
        // Load BranchId and Branch Object data into cache (Temporary fix to bypass cache loading on startup)
        Cache.initBranchIdObjectMap();
        if(Cache.BRANCH_ID_OBJECT_MAP.get(instId) != null && Cache.BRANCH_ID_OBJECT_MAP.get(instId).get(product) != null){
            Branch branch = Cache.BRANCH_ID_OBJECT_MAP.get(instId).get(product).get(header.getBranchCode());
        */
        if (hierarchyMaster != null && hierarchyMaster.getBranch() != null) {
            branch = hierarchyMaster.getBranch();
            if (branch != null && CollectionUtils.isNotEmpty(groupNames)) {
                for (String groupType : groupNames) {
                    String searchKey = null;
                    if (groupType.contains("(" + IntimationConstants.GROUP_TYPE_PAN_INDIA + ")")) {
                        searchKey = product + "(" + IntimationConstants.GROUP_TYPE_PAN_INDIA + ")";
                    } else if (groupType.contains(IntimationConstants.BRANCH + "_")) {
                        String role1 = groupType.replaceAll(IntimationConstants.BRANCH + "_", "");
                        searchKey = branch.getBranchName() + "_" + role1;
                    } else if (groupType.contains(IntimationConstants.ZONE + "_")) {
                        String role2 = groupType.replaceAll(IntimationConstants.ZONE + "_", "");
                        searchKey = branch.getZone() + "_" + role2;
                    } else if (StringUtils.equalsIgnoreCase(groupType, IntimationConstants.BRANCH)) {
                        searchKey = branch.getBranchName();
                    } else if (StringUtils.equalsIgnoreCase(groupType, IntimationConstants.ZONE)) {
                        searchKey = branch.getZone();
                    } else if (groupType.contains(IntimationConstants.LOCATION + "_")) {
                        String role2 = groupType.replaceAll(IntimationConstants.LOCATION + "_", "");
                        searchKey = branch.getLocation() + "_" + role2;
                    } else if (groupType.contains(IntimationConstants.REGION + "_")) {
                        String role2 = groupType.replaceAll(IntimationConstants.REGION + "_", "");
                        searchKey = branch.getRegion() + "_" + role2;
                    } else {
                        searchKey = groupType;
                    }
                    boolean added = addApplicationSpecificGroup(searchKey);
                    if (!added && CollectionUtils.isNotEmpty(groups)) {
                        for (IntimationGroup group : groups) {
                            if (StringUtils.equalsIgnoreCase(group.getGroupName(), groupType))
                                filteredGroups.add(group);
                        }
                    }
                    if(!added && !defaultGroups.isEmpty()){
                        for(String grp : defaultGroups){
                            if (StringUtils.equalsIgnoreCase(grp, groupType))
                                defaultIntimationGroups.add(grp);
                        }
                    }
                }
            }
        }else {
            for (String searchKey : groupNames) {
                addApplicationSpecificGroup(searchKey);
            }
        }
        return filteredGroups;
    }

    private boolean addApplicationSpecificGroup(String searchKey) {
        Request request = null;
        boolean apAdded = false, coapAdded =false;
        if(this.goNoGoCustomerApplication != null && this.goNoGoCustomerApplication.getApplicationRequest() != null){
            request = this.goNoGoCustomerApplication.getApplicationRequest().getRequest();
        }
        Applicant applicant = null;
        List<CoApplicant> coApplicants = null;
        switch (searchKey){
            case IntimationConstants.GROUP_TYPE_CUSTOMER :
                if (request != null){
                    applicant = request.getApplicant();
                    apAdded = extractApplicantEmailAndPhone(applicant);
                    coApplicants = request.getCoApplicant();
                    coapAdded = extractCoApplicantEmailAndPhone(coApplicants);
                }
                break;
            case IntimationConstants.GROUP_TYPE_APPLICANT :
                if (request != null) {
                    applicant = request.getApplicant();
                    apAdded = extractApplicantEmailAndPhone(applicant);
                }
                break;
            case IntimationConstants.GROUP_TYPE_CO_APPLICANTS :
                if (request != null) {
                    coApplicants = request.getCoApplicant();
                    coapAdded = extractCoApplicantEmailAndPhone(coApplicants);
                }
                break;

        }
        if(apAdded || coapAdded)
            return true;
        else
            return false;
    }

    private boolean extractApplicantEmailAndPhone(Applicant applicant) {
        boolean added = false;
        if(applicant != null) {
            if (CollectionUtils.isNotEmpty(applicant.getEmail())){
                for (Email email : applicant.getEmail()) {
                    if (StringUtils.isNotEmpty(email.getEmailAddress())) {
                        this.otherMailIds.add(email.getEmailAddress());
                        added = true;
                        break;
                    }
                }
            }
            if(CollectionUtils.isNotEmpty(applicant.getPhone())){
                for (Phone phone : applicant.getPhone()) {
                    if (StringUtils.isNotEmpty(phone.getPhoneNumber())) {
                        this.otherPhoneNumbers.add(phone.getPhoneNumber());
                        added = true;
                        break;
                    }
                }
            }
        }
        return added;
    }

    private boolean extractCoApplicantEmailAndPhone(List<CoApplicant> coApplicants) {
        boolean added = false;
        if(CollectionUtils.isNotEmpty(coApplicants)){
            for(CoApplicant coApp: coApplicants){
                if (CollectionUtils.isNotEmpty(coApp.getEmail())){
                    for (Email email : coApp.getEmail()) {
                        if (StringUtils.isNotEmpty(email.getEmailAddress())) {
                            this.otherMailIds.add(email.getEmailAddress());
                            added = true;
                            break;
                        }
                    }
                }
                if(CollectionUtils.isNotEmpty(coApp.getPhone())){
                    for (Phone phone : coApp.getPhone()) {
                        if (StringUtils.isNotEmpty(phone.getPhoneNumber())) {
                            this.otherPhoneNumbers.add(phone.getPhoneNumber());
                            added = true;
                            break;
                        }
                    }
                }
            }
        }
        return added;
    }

    private BaseResponse sendMail(Header header, String refId, List<String> mailIds) {
        logger.info("Inside sendVerificationMail()");
        BaseResponse response = null;
        WFJobCommDomain emailconfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                header.getInstitutionId(), UrlType.GUPSHUP.toValue());

        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setAction(EndPointReferrer.VERIFY_EMAIL);
        activityLogs.setRefId(refId);
        activityLogs.setInstitutionId(header.getInstitutionId());
        activityLogs.setUserName(header.getLoggedInUserId());
        activityLogs.setCustomMsg("verification email");

        if (emailconfig == null) {
            return getNoCofigurationBaseResponse(activityLogs);
        }
        Map<String, String> postParmeters = getRequestParams(emailconfig, mailIds);
        SendMailRequest mailRequest = SendMailRequest.builder().requestParams(postParmeters).build();
        logger.info("MailRequest for refId {} for action {} :: {}", refId, this.action, mailRequest);
        String url = Arrays.asList(emailconfig.getBaseUrl(), emailconfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        logger.info("URL", url);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try {
            EmailResponse emailResponse = (EmailResponse) TransportUtils.postJsonRequest(mailRequest, url, EmailResponse.class);
            response = GngUtils.getBaseResponse(HttpStatus.OK, emailResponse);
            logger.info("MailResponse for refId {} for action {} :: {}", refId, this.action, emailResponse);
            activityLogs.setStatus(GNGWorkflowConstant.SUCCESS.name());
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Exception occured while sending mail to customer refId : " + refId + ", Action : " + action);
        }
        stopWatch.stop();
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from EmailSender in thread %s",
                Thread.currentThread().getName()));
        //applicationEventPublisher.publishEvent(activityLogs);
        return response;

    }

    private BaseResponse sendSms(Header header, String refId, List<String> mobileNoList) {
        logger.info("Inside sendVerificationSMS()");
        SmsResponse smsResponse = null;
        ActivityLogs activityLogs = new ActivityLogs();
        activityLogs.setAction(EndPointReferrer.SEND_SMS);
        activityLogs.setRefId(refId);
        activityLogs.setInstitutionId(header.getInstitutionId());
        activityLogs.setUserName(header.getLoggedInUserId());
        activityLogs.setCustomMsg("verification sms");

        if (Cache.URL_CONFIGURATION.getSmsServiceConfiguration() == null
                || Cache.URL_CONFIGURATION.getSmsServiceConfiguration().get(
                header.getInstitutionId()) == null) {

            logger.error("Sms service app not found on server.");

            Collection<Error> errors = new ArrayList<>();
            errors.add(Error.builder()
                    .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                    .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                    .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                    .level(Error.SEVERITY.CRITICAL.name())
                    .build());
            return GngUtils.getBaseResponse(HttpStatus.FAILED_DEPENDENCY, errors);

        } else {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            SmsServiceConfiguration smsServiceConfiguration = Cache.URL_CONFIGURATION
                    .getSmsServiceConfiguration().get(header.getInstitutionId());
            String[] stringArrayParam = mobileNoList.toArray(new String[0]);
            SmsServiceBaseRequest smsServiceBaseRequest = SmsServiceBaseRequest.builder().institutionId(header.getInstitutionId())
                    .mobileNumber(stringArrayParam).otpMsg(false).message(smsContent).build();
            try {
                // Call Sms service and then take further action based on
                // response from Sms Service.
                logger.info("SMSRequest for refId {} for action {} :: ", refId, this.action, smsServiceBaseRequest);
                SmsServiceBaseResponse smsServiceBaseResposne = smsServiceCaller
                        .callSmsService(smsServiceBaseRequest,
                                smsServiceConfiguration.getUrl());
                logger.info("SMSResponse for refId {} for action {} :: {}", refId, this.action, smsServiceBaseResposne);

                if (smsServiceBaseResposne == null) {
                    smsResponse = SmsUtils.getSmsErrorResponse(
                            "Sms sending failed, try again", null);
                    logger.error("SmsServiceBase Response was null or empty");

                    Collection<Error> errors = new ArrayList<>();
                    errors.add(Error.builder()
                            .message(ErrorCode.EXTERNAL_SERVICE_FAILURE)
                            .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                            .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                            .level(Error.SEVERITY.CRITICAL.name())
                            .build());
                    return GngUtils.getBaseResponse(HttpStatus.GATEWAY_TIMEOUT, errors);
                } else {
                    if (smsServiceBaseResposne.getErrors() != null
                            && smsServiceBaseResposne.getErrors().length > 0) {
                        smsResponse = SmsUtils.getSmsErrorResponse(
                                "Sms sending failed, try again",
                                smsServiceBaseResposne.getSmsStatus());
                        for (com.softcell.gonogo.model.response.smsservice.Error error : smsServiceBaseResposne.getErrors()) {
                            logger.error(error.toString());
                        }
                    }
                }

            } catch (Exception e) {
                smsResponse = SmsUtils.getSmsErrorResponse(
                        "Sms sending failed, try again", null);
                logger.error(e.toString());
            }
            stopWatch.stop();
            activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
            logger.debug(String.format(" Publishing activity log from EmailSender in thread %s",
                    Thread.currentThread().getName()));
            //applicationEventPublisher.publishEvent(activityLogs);

        }
        return GngUtils.getBaseResponse(HttpStatus.OK, smsResponse);
    }

    private BaseResponse getNoCofigurationBaseResponse(ActivityLogs activityLogs) {
        Collection<Error> errors = new ArrayList<>();
        errors.add(Error.builder()
                .message(ErrorCode.CONFIGURATION_NOT_FOUND)
                .errorCode(String.valueOf(Error.ERROR_TYPE.SYSTEM.toCode()))
                .errorType(Error.ERROR_TYPE.SYSTEM.toValue())
                .level(Error.SEVERITY.CRITICAL.name())
                .build());
        activityLogs.setStatus(GNGWorkflowConstant.FAILED.name());
        return GngUtils.getBaseResponse(CustomHttpStatus.CONFIGURATION_NOT_FOUND, errors);
    }

    private Map<String,String> getRequestParams(WFJobCommDomain emailconfig, List<String> mailIds) {
        List<HeaderKey> keyValue = emailconfig.getKeys();
        Map<String, String> postParameters = new HashMap<>();
        for (HeaderKey key : keyValue) {
            if (key.getKeyName().equalsIgnoreCase(PostParameterKeys.CONTENT)) {
                postParameters.put(key.getKeyName(), (StringUtils.isNotEmpty(this.mailContent) ? this.mailContent : key.getKeyValue()));
            }else if( key.getKeyName().equalsIgnoreCase(PostParameterKeys.SUBJECT)){
                postParameters.put(key.getKeyName(), StringUtils.isNotEmpty(this.mailSubject) ? this.mailSubject : key.getKeyValue());
            } else {
                postParameters.put(key.getKeyName(), key.getKeyValue());
            }
        }
        postParameters.put(PostParameterKeys.RECIPIENTS, getFormatMailIds(mailIds));
        return postParameters;
    }

    private String getFormatMailIds(List<String> mailIds) {
        String to = "";
        boolean multipleMailIds = false;
        for(String id : mailIds) {
            if(multipleMailIds)
                to += ",";
            to += id;
            multipleMailIds = true;
        }
        return to;
    }
}
