package com.softcell.workflow.executors.sobre;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.ScoringCommunication;
import com.softcell.config.ScoringConfiguration;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.Buckets;
import com.softcell.gonogo.model.ScoringCallLog;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.Application;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibleOffers;
import com.softcell.gonogo.model.core.ocr.OCRDetails;
import com.softcell.gonogo.model.core.perfios.PerfiosData;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.request.thirdparty.FaceMatchRequest;
import com.softcell.gonogo.model.core.scoring.response.ScoringApplicantResponse;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.service.factory.ScoringJsonBuilder;
import com.softcell.gonogo.service.factory.impl.ScoringJsonBuilderImpl;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ssg0302 on 8/7/19.
 */
public class ScoringExecutorV2Helper{
    private static final Logger logger = LoggerFactory.getLogger(ScoringExecutorV2Helper.class);

    private ApplicationRepository applicationMongoRepository;

    private ModuleRequestRepository moduleRequestRepository;

    private ExternalAPILogRepository externalAPILogRepository;

    protected static final String APPLICANTID = "sApplID";

    protected static final String CUSTOMERID = "customerId";

    protected String policyName = Constant.DEFAULT;

    public ScoringExecutorV2Helper(String policyName){
        if(null == applicationMongoRepository)
            applicationMongoRepository = new ApplicationMongoRepository();

        if(null == moduleRequestRepository)
            moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());

        if(null == externalAPILogRepository)
            externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());

        this.policyName=policyName;
    }

    protected ScoringApplicantResponse callApplicantScoring(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception {
        logger.info("Proceeding for APPLICANT for refId : {} and Policy : {}", goNoGoCustomerApplication.getGngRefId(), this.policyName);
        String instId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        ScoringCommunication scoringCommunication;
        String refId = goNoGoCustomerApplication.getApplicationRequest().getRefID();
        String instituteId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        Applicant applicant = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant();

        setApplicantData(applicant, refId, instituteId);

        if (instId == null) {
            logger.info("Institution id not provided !");
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
            return null;
        } else {
            scoringCommunication = ScoringConfiguration.getCreditial(instId);
            if (scoringCommunication == null) {
                logger.info("{} - Scoring not configured for institute {} ! Cannot execute scoring for {}", instId, goNoGoCustomerApplication.getGngRefId());
                goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
                return null;
            }
        }
        ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();
        ScoringApplicantRequest scoringApplicantRequest = builder.buildApplicantScoringJSonVersion2(goNoGoCustomerApplication);
        scoringApplicantRequest.setPolicyName(this.policyName);
        scoringApplicantRequest.getApplication().setPolicyName(this.policyName);
        if(StringUtils.equalsIgnoreCase(this.policyName, "EligibleOffers"))
            scoringApplicantRequest.getApplication().setCallDetails("Eligibility2");

        logger.debug("SOBRE objectMapper formed for {}", goNoGoCustomerApplication.getGngRefId());

        if (scoringApplicantRequest != null) {
            moduleRequestRepository.saveScoringApplicantRequest(scoringApplicantRequest);
        }
        // Create json from request
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String valueJson = objectMapper.writeValueAsString(scoringApplicantRequest);
            valueJson = valueJson.replaceAll(APPLICANTID, CUSTOMERID);
            scoringApplicantRequest.setRequest(valueJson);
            moduleRequestRepository.saveScoringApplicantRequest(scoringApplicantRequest);
            String response = connectSobre(valueJson, scoringCommunication,goNoGoCustomerApplication);
            response = response.replaceAll(CUSTOMERID, APPLICANTID);
            logger.debug("{} SOBRE response received",  goNoGoCustomerApplication.getGngRefId());
            saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());
            // Deserialize json response
            ScoringApplicantResponse scoringApplicantResponse = null;
            if (StringUtils.isNotEmpty(response)) {
                scoringApplicantResponse = objectMapper.readValue(response, ScoringApplicantResponse.class);
            }
            logger.debug("{} SOBRE response deserialized",  goNoGoCustomerApplication.getGngRefId());
            saveSobreRequestResponse(scoringApplicantRequest,scoringApplicantResponse);
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
            return scoringApplicantResponse;
        } catch (Exception e) {
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
            //e.printStackTrace();
            logger.error("callApplicantScoring() has exception {}", e.getMessage());
            return null;
        }
    }

    private void setApplicantData(Applicant applicant, String refId, String instituteId){
        try {
            //Setting Perfios data to Sobre request to validate and get new eligibility
            logger.debug("Setting Perfios Summary of Applicant ");
            PerfiosData perfiosData = applicationMongoRepository.fetchPerfiosData(refId, instituteId);
            if (null != perfiosData && CollectionUtils.isNotEmpty(perfiosData.getSaveDataList())) {
                List<LinkedHashMap> summaryInfoList = perfiosData.getSaveDataList().stream()
                        .filter(bankData ->
                                (null != bankData && null != bankData.getData()  && !Status.FAILED.name().equalsIgnoreCase(bankData.getStatus()))
                        ).map(bankData -> {
                            try {
                                LinkedHashMap summaryInfo = (LinkedHashMap) ((LinkedHashMap) ((LinkedHashMap) bankData.getData()).get("PIR:Data")).get("SummaryInfo");
                                return summaryInfo.size() > 0 ? summaryInfo : null;
                            } catch (Exception e) {
                                return null;
                            }
                        }).collect(Collectors.toList());
                summaryInfoList.removeAll(Collections.singleton(null));
                applicant.setPerfiosData(summaryInfoList);
            }

            //Setting Ocr data to Sobre request to validate applicant details
            logger.debug("Setting OCR Details of Applicant ");
            OCRDetails ocrDetails = applicationMongoRepository.getOCRData(refId, instituteId);
            if (null != ocrDetails) {
                HashMap<String, String> ocrKeyValues = new HashMap<>();
                if(CollectionUtils.isNotEmpty(ocrDetails.getOcrDetailsList())){
                    ocrDetails.getOcrDetailsList().forEach(ocrDetailsObj -> {
                        if(StringUtils.isNotEmpty(ocrDetailsObj.getDocName())) {
                            if (CollectionUtils.isNotEmpty(ocrDetailsObj.getOcrData())) {
                                ocrDetailsObj.getOcrData().forEach(ocrData -> {
                                    if (StringUtils.isNotEmpty(ocrData.getKeyValue())) {
                                        String value;
                                        if (ocrData.getKeyValue().length() == 10 && StringUtils.countMatches(ocrData.getKeyValue(), "/") == 2)
                                            value = ocrData.getKeyValue().replaceAll("/", "");
                                        else
                                            value = ocrData.getKeyValue();
                                        ocrKeyValues.put(StringUtils.join(new Object[]{StringUtils.deleteWhitespace(ocrDetailsObj.getDocName()), ocrData.getKeyName()}, FieldSeparator.UNDER_SQURE), value);
                                    }
                                });
                            }
                        }
                    });
                }
                applicant.setOcrDetails(ocrKeyValues);
            }

            logger.debug("Setting FaceMatch Details of Applicant");
            FaceMatchRequest faceMatchRequest = applicationMongoRepository.fetchFaceMatchData(refId);
            if(null != faceMatchRequest && null != faceMatchRequest.getFaceMatchDetails()){
                applicant.setFaceMatchDetails(faceMatchRequest.getFaceMatchDetails());
            }
        }catch(Exception e){
            logger.error("Exception occurred while setting Applicant Data");
        }
    }
    private String connectSobre(String valueJson, ScoringCommunication scoringCommunication, GoNoGoCustomerApplication goNoGoCustomerApplication) throws IOException {
        String url = scoringCommunication.getScoringServiceUrl();
        StringBuilder endpoint = new StringBuilder(url) ;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
            endpoint.append("/processApplication?")
                    .append("INSTITUTION_ID=").append(scoringCommunication.getInstitutionId())
                    .append("&USER_ID=").append(scoringCommunication.getUserId())
                    .append("&PASSWORD=").append(scoringCommunication.getPassword());

            logger.debug("Sending SOBRE objectMapper for {} to url {} is {}", goNoGoCustomerApplication.getGngRefId(),
                    endpoint.toString(), valueJson);

        String response = new HttpTransportationService().postRequest(endpoint.toString(), valueJson, MediaType.APPLICATION_JSON_VALUE);
        if(response != null) {
            response = response.replaceAll(CUSTOMERID, APPLICANTID);
        }
        logger.debug("{} SOBRE response received", goNoGoCustomerApplication.getGngRefId());
        saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());
        stopWatch.stop();
        logger.debug(String.format("Time taken by sobre api %s", stopWatch.getTotalTimeMillis()));
        return response;
    }

    protected void segregrateApplicantsResponse(ScoringApplicantResponse scoringApplicantResponse, GoNoGoCustomerApplication goNoGoCustomerApplication) {
        // Get primary applicant's result
        final String applicantId = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId();
        ScoringApplicantResponse primaryResponse = new ScoringApplicantResponse();
        primaryResponse.setPolicyName(this.policyName);
        primaryResponse.setSummary(scoringApplicantResponse.getSummary());
        // Get primary applicant's result from the the scoring result received
        primaryResponse.setApplicantResult(scoringApplicantResponse.getApplicantResult().stream().filter(result ->
                StringUtils.equalsIgnoreCase(applicantId,result.getApplicantId()) )
                .collect(Collectors.toList()) );
        // Set scoring response in component response
        synchronized (goNoGoCustomerApplication){
            setApplicantResponse(goNoGoCustomerApplication, primaryResponse);
        }
    }

    private void setApplicantResponse(GoNoGoCustomerApplication goNoGoCustomerApplication, ScoringApplicantResponse primaryResponse) {
        /* For different policies we have to set the response into list.
            * But if the policy is default then replace the response*/
        ScoringResponse scoringServiceResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse();
        if(scoringServiceResponse == null || scoringServiceResponse.getApplicantResponse() == null){
            scoringServiceResponse = new ScoringResponse();
            scoringServiceResponse.setApplicantResponse(primaryResponse);
            goNoGoCustomerApplication.getApplicantComponentResponse().setScoringServiceResponse(scoringServiceResponse);
        }else {
            if (StringUtils.equalsIgnoreCase(scoringServiceResponse.getApplicantResponse().getPolicyName(), this.policyName)) {
                scoringServiceResponse.setApplicantResponse(primaryResponse);
            } else {
                List<ScoringApplicantResponse> breApplicantResponses = scoringServiceResponse.getBreApplicantResponses();
                if (CollectionUtils.isEmpty(breApplicantResponses)) {
                    breApplicantResponses = new ArrayList<>();
                    scoringServiceResponse.setBreApplicantResponses(breApplicantResponses);
                    breApplicantResponses.add(primaryResponse);
                } else {
                    // the response may be of same policy - then replace
                    // else add
                    List<ScoringApplicantResponse> temp
                            = breApplicantResponses.stream()
                            .filter(response ->
                                    StringUtils.equalsIgnoreCase(response.getPolicyName(), this.policyName))
                            .collect(Collectors.toList());
                    if (CollectionUtils.isEmpty(temp)) {
                        // add
                        breApplicantResponses.add(primaryResponse);
                    } else {
                        // replace
                        ScoringApplicantResponse previousResponse = temp.get(0);
                        previousResponse.setApplicantResult(primaryResponse.getApplicantResult());
                        previousResponse.setSummary(primaryResponse.getSummary());
                    }
                }
            }
        }
    }

    protected void saveRawResponse(String rawResponse, String refId) {
        logger.debug("Saving data for {}", ActionName.SOBRE_RESPONSE_SAVE);
        RawResponseLog rawResponseLog = RawResponseLog.builder().rawResponse(rawResponse)
                .refId(refId)
                .responseType(ActionName.SOBRE_RESPONSE_SAVE).build();

        externalAPILogRepository.saveRawResonseLog(rawResponseLog);
    }


    protected void caseStatus(ScoringApplicantResponse scoringApplicantResponse, GoNoGoCustomerApplication goNoGoCustomerApplication) {
        if(scoringApplicantResponse != null && scoringApplicantResponse.getSummary() != null){
            decideApplicationStatus(goNoGoCustomerApplication);
        }else{
            goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId("default");
            goNoGoCustomerApplication.getIntrimStatus().setCroStatus(Status.COMPLETE.toString());
            goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());
        }
    }

    private void decideApplicationStatus(GoNoGoCustomerApplication goNoGoCustomerApplication){
        ScoringResponse scoringServiceResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse();
        if(null != scoringServiceResponse) {
            if (null != scoringServiceResponse.getApplicantResponse()
                    && StringUtils.equalsIgnoreCase(scoringServiceResponse.getApplicantResponse().getPolicyName(), this.policyName)) {
                setStatus(scoringServiceResponse.getApplicantResponse(), goNoGoCustomerApplication);
            } else if (CollectionUtils.isNotEmpty(scoringServiceResponse.getBreApplicantResponses())) {
                ScoringApplicantResponse temp
                        = scoringServiceResponse.getBreApplicantResponses().stream()
                        .filter(response ->
                                StringUtils.equalsIgnoreCase(response.getPolicyName(), this.policyName))
                        .findAny().orElse(null);
                if (null != temp) {
                    setStatus(temp, goNoGoCustomerApplication);
                }
            }
        }
    }

    private void setStatus(ScoringApplicantResponse scoringApplicantResponse, GoNoGoCustomerApplication goNoGoCustomerApplication){
        String decision = scoringApplicantResponse.getSummary().getApplicantDecesion();
        String status = "", bucket= "", stage = "";
        if(StringUtils.equalsIgnoreCase(scoringApplicantResponse.getPolicyName(), "MCP_POLICY")){
            if(! StringUtils.equalsIgnoreCase(decision, GNGWorkflowConstant.DECLINED.toFaceValue()))
                status = GNGWorkflowConstant.QUEUED.toFaceValue();
            if( StringUtils.equalsIgnoreCase(decision, GNGWorkflowConstant.DECLINED.toFaceValue()))
                status = GNGWorkflowConstant.DECLINED.toFaceValue();
            bucket = goNoGoCustomerApplication.getApplicationBucket();
        }else if(StringUtils.equalsIgnoreCase(scoringApplicantResponse.getPolicyName(),"DIGI_PL_CALL2")
                || StringUtils.equalsIgnoreCase(scoringApplicantResponse.getPolicyName(),"PERFIOS_POLICY")
                || StringUtils.equalsIgnoreCase(scoringApplicantResponse.getPolicyName(),"OCR_POLICY")){
            bucket = getBucket(scoringApplicantResponse);
            if(StringUtils.isEmpty(bucket))
                status = GNGWorkflowConstant.DECLINED.toFaceValue();
            else if(Buckets.isSTPBucket(bucket))
                status = GNGWorkflowConstant.APPROVED.toFaceValue();
            else
                status = GNGWorkflowConstant.QUEUED.toFaceValue();
        }

        //Setting application stage bucketwise/statuswise
        if(StringUtils.isEmpty(bucket))
            stage = GngUtils.getCurrentStageBasedOnApplicationStatus(status);
        else
            stage = GngUtils.getCurrentStageBasedOnApplicationBucket(bucket);

        logger.debug("Setting Application status to {} and Application stage to {}", status,stage);

        if(StringUtils.isNotEmpty(status)) goNoGoCustomerApplication.setApplicationStatus(status);

        if (StringUtils.isNotEmpty(bucket)) goNoGoCustomerApplication.setApplicationBucket(bucket);

        if(StringUtils.isNotEmpty(stage)) goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(stage);

        if(! StringUtils.equalsIgnoreCase(decision,
                GNGWorkflowConstant.QUEUED.toFaceValue())) {
            goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP.toString());
            logger.info("updated case status from sobre decesion when it is not queued,getApplicantDecesion {}",decision);
        }
    }

    private String getBucket(ScoringApplicantResponse scoringApplicantResponse){
        String bucket = null;
        try {
            //CUSTOM_FIELDS$BUCKET_DECISION
            bucket = ((String) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_BKT_FINAL"));
        }catch(Exception e){
            logger.debug("Exception while retrieving bucket");
        }
        logger.debug("bucket {} allocated ", bucket);
        return bucket;
    }

    protected void setPostEligibility(ScoringApplicantResponse scoringApplicantResponse, GoNoGoCustomerApplication goNoGoCustomerApplication){
        try{
            ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
            Applicant applicant = applicationRequest.getRequest().getApplicant();
            Application application = applicationRequest.getRequest().getApplication();
            double income = Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_INCOME"));
            double fixedOblgation = Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_TOTAL_OBLIGATIONS_FINAL"));
            double elgblLnAmt = Math.round((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_APPROVED_AMOUNT_POST"));
            double emi = Math.ceil((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_APPROVED_EMI_POST"));
            String appliedTenor;
            try{
                appliedTenor = (String)((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_SELECTED_TENOR");
            }catch(Exception e) {
                appliedTenor=String.valueOf(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_SELECTED_TENOR")).intValue());
            }
            String minimumOfferedValue = String.valueOf(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CALCULATED_FIELDS$DIGI_ELIGIBILITY_MIN_VALUE_FINAL")).intValue());

            logger.info("Setting Post Eligibility Values for refId {}", goNoGoCustomerApplication.getGngRefId());
            logger.info("income:{}, fixedOblgation:{}, elgblLnAmt:{}, emi:{}, appliedTenor:{}", income, fixedOblgation, elgblLnAmt, emi,appliedTenor);

            EligibilityDetails eligibilityDetails = new EligibilityDetails();
            eligibilityDetails.setIncome(income);
            eligibilityDetails.setFixedOblgation(fixedOblgation);
            eligibilityDetails.setCreditCardOutStanding(fixedOblgation);
            eligibilityDetails.setAppliedTenor(appliedTenor);
            eligibilityDetails.setExistingLoan(false);

            eligibilityDetails.setElgLnAmt(elgblLnAmt);
            eligibilityDetails.setAppliedLoan(elgblLnAmt);
            eligibilityDetails.setAprvLoan(eligibilityDetails.getElgLnAmt());
            eligibilityDetails.setMonthlySalary(income);
            eligibilityDetails.setTenor(Integer.valueOf(appliedTenor));
            eligibilityDetails.setElgFixedObg(fixedOblgation);
            eligibilityDetails.setEmi(emi);

            EligibleOffers eligibleOffers = new EligibleOffers();
            eligibleOffers.setEligibilityName("OFFER5");
            eligibleOffers.setEligibilityAmount(elgblLnAmt);
            eligibleOffers.setEligibleTenor(appliedTenor);
            eligibleOffers.setFixedObligations(fixedOblgation);
            eligibleOffers.setIncome(income);
            eligibleOffers.setEmi(emi);
            eligibleOffers.setSelectedOffer(true);

            List<EligibleOffers> eligibleOffersList = new ArrayList<>();
            eligibleOffersList.add(eligibleOffers);

            eligibilityDetails.setEligibleOffersList(eligibleOffersList);

            applicant.setPostEligibility(eligibilityDetails);
            applicant.setMinimumOfferedValue(minimumOfferedValue);
            application.setRoi(((Double) ((LinkedHashMap) scoringApplicantResponse.getApplicantResult().get(0).getDerivedFields()).get("CUSTOM_FIELDS$DIGI_ROI")));
        }catch(Exception e){
            logger.debug("Exception while setting post eligibility for refId {}, Exception: {}", goNoGoCustomerApplication.getGngRefId(), e.getStackTrace());
        }
    }

    private void saveSobreRequestResponse(ScoringApplicantRequest scoringApplicantRequest, ScoringApplicantResponse scoringApplicantResponse) {
        try {
            ScoringCallLog callLog = new ScoringCallLog();
            callLog.setRefId(scoringApplicantRequest.getHeader().getApplicationId());
            callLog.setStep(scoringApplicantRequest.getPolicyName());
            callLog.setInstitutionId(scoringApplicantRequest.getHeader().getInstitutionId());
            callLog.setCallDate(new Date());
            logger.debug("Inside saveSobreRequestResponse()- scoringApplicantRequest:1");
            callLog.setRequestString(JsonUtil.ObjectToString(scoringApplicantRequest));
            logger.debug("Inside saveSobreRequestResponse()- scoringApplicantRequest:2");
            callLog.setRequest(scoringApplicantRequest);

            if (scoringApplicantResponse != null) {
                /*callLog.setResponseString(JsonUtil.ObjectToString(scoringApplicantResponse));*/
                callLog.setResponse(scoringApplicantResponse);
            }
            callLog.setRequestType(EndPointReferrer.GET_SCORING_LOG);
            logger.debug("Inside saveSobreRequestResponse(): saveSobreCallLog:3");
            externalAPILogRepository.saveSobreCallLog(callLog);
            logger.debug("Inside saveSobreRequestResponse(): saveSobreCallLog:4");
        } catch (Exception e) {
            logger.error("Exception occured while saving sobreLogs {}", e.getStackTrace());
        }
    }
}
