package com.softcell.workflow.executors.sobre;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.ModuleHelper;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.scoring.response.ScoringApplicantResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.service.DMZConnector;
import com.softcell.service.DataEntryManager;
import com.softcell.service.impl.DMZConnectorImpl;
import com.softcell.service.impl.DataEntryManagerImpl;
import com.softcell.workflow.aggregators.WfResultAggregatorImpl;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssg0302 on 8/7/19.
 */
public class ScoringExecutorV2 extends MetaAction {
    private static final Logger logger = LoggerFactory.getLogger(ScoringExecutorV2.class);

    private ModuleSetting moduleSetting;

    private ModuleRequestRepository moduleRequestRepository;

    private WfResultAggregatorImpl wfResultAggregatorImpl;

    private DMZConnector dmzConnector;

    private ScoringExecutorV2Helper scoringExecutorV2Helper;

    protected LookupService lookupService;

    public ScoringExecutorV2(WfResultAggregatorImpl wfResultAggregatorImpl) {

        this.wfResultAggregatorImpl = wfResultAggregatorImpl;

        if (null == dmzConnector) {
            dmzConnector = new DMZConnectorImpl();
        }

        if (null == lookupService) {
            lookupService = new LookupServiceHandler();
        }
    }

    @Override
    public String process(Object goNoGoCustomerApplication, ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
        ModuleOutcome moduleOutcome = new ModuleOutcome();
        moduleOutcome.setOrder(ScoringDisplayName.APPLICATION_SCORE_ORDERE);
        moduleOutcome.setFieldName(ScoringDisplayName.APPLICATION_SCORE);
        moduleOutcome.setFieldValue("-");

        goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
        goNoGoCustomerApplication.getIntrimStatus().setScoringModuleResult(moduleOutcome);
        goNoGoCustomerApplication.getIntrimStatus().setAppsStatus(Status.VERIFIED.toString());
        goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());

        // FIX for age ; sometimes age is not set , set now to avoid error
        ModuleHelper.fixAge(goNoGoCustomerApplication.getApplicationRequest());

        logger.info("Scoring executor calling sobre service for {} in stage {}", goNoGoCustomerApplication.getGngRefId(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
        try{
            ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                    GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.SOBRE.toFaceValue());
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            ScoringApplicantResponse scoringApplicantResponse = null;
            boolean executeForApplicant = StringUtils.equalsIgnoreCase(this.executionBase, APPLICANT);

            try{

                if(null == scoringExecutorV2Helper)
                    scoringExecutorV2Helper=new ScoringExecutorV2Helper(this.policyName);

                //Check configuration for APPLICANT
                if (executeForApplicant) {
                    logger.info("Scoring is based on APPLICANT for refid {}", goNoGoCustomerApplication.getGngRefId());
                    scoringApplicantResponse = scoringExecutorV2Helper.callApplicantScoring(goNoGoCustomerApplication);
                    if (scoringApplicantResponse != null) {
                        //Applicant and coApplicant response filtering.
                        scoringExecutorV2Helper.segregrateApplicantsResponse(scoringApplicantResponse,goNoGoCustomerApplication);

                        logger.info("Scoring executor getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
                        if (scoringApplicantResponse.getApplicantResult() != null) {
                            if (CollectionUtils.isNotEmpty(scoringApplicantResponse.getApplicantResult())) {
                                //TODO save status as message.
                                moduleOutcome.setMessage("_");
                            } else {
                                moduleOutcome.setMessage("_");
                            }
                        } else {
                            moduleOutcome.setMessage("_");
                        }
                    } else {
                        logger.warn("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
                        moduleOutcome.setMessage("Scoring Down");
                    }

                    activityLog.setCustomMsg(moduleOutcome.getMessage());
                    activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                }
            } catch (Exception e) {
                //e.printStackTrace();
                logger.error("run() has exception {}", e.getMessage());
            }
            try {
                logger.info("Scoring executor call finished for {} in stage {}", goNoGoCustomerApplication.getGngRefId(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                activityLog.setStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                boolean skipSetStatus = false;
                if(StringUtils.equalsIgnoreCase(this.policyName, "EligibleOffers")){
                    scoringExecutorV2Helper.setPostEligibility(scoringApplicantResponse, goNoGoCustomerApplication);
                    skipSetStatus = true;
                }

                if(executeForApplicant && !skipSetStatus){
                        scoringExecutorV2Helper.caseStatus(scoringApplicantResponse,goNoGoCustomerApplication);
                }
                logger.info("Before exiting scoring executor for refId {} stage is {}", goNoGoCustomerApplication.getGngRefId(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());

                // Set status in activity log
                String appStatus = null!=goNoGoCustomerApplication.getApplicationStatus() ? goNoGoCustomerApplication.getApplicationStatus().toUpperCase():null;
                activityLog.setStatus(appStatus);
                activityLog.setChangedStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());

            } catch (Exception exception) {
                //exception.printStackTrace();
                logger.error("run() has exception in queueApplication block {}", exception.getMessage());
            }

            stopWatch.stop();
            activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        /*
         * Partial Scoring response save in db.
         */
            moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());

            moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);

            try {
                // Update cibil score for MIS data

                List<GoNoGoCustomerApplication> list = new ArrayList<>();
                list.add(goNoGoCustomerApplication);
                DataEntryManager dataEntryManager = new DataEntryManagerImpl();
                dataEntryManager.migrateCibilDataForApplications(list);
            } catch (Exception e) {
                logger.debug("Error while updating Cibil data for MIS for case ID : " + goNoGoCustomerApplication.getGngRefId());
            }
            // Save activity log
            logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
            GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
        }finally {
            // Since the call can be from service/standalone, need to check whether component manager has started this.
            if (componentManager != null) {
                synchronized (componentManager) {
                    componentManager.executionFinished(getActionId());
                    componentManager.notifyAll();
                }
            }
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see com.softcell.gonogo.workflow.actions.MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }
}
