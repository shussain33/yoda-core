package com.softcell.workflow.executors.sobre;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.aop.ApplicationProxy;
import com.softcell.config.ScoringCommunication;
import com.softcell.config.ScoringConfiguration;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.Roles;
import com.softcell.gonogo.model.ScoringCallLog;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.cam.CamDetailsRequest;
import com.softcell.gonogo.model.core.eligibility.Eligibility;
import com.softcell.gonogo.model.core.eligibility.EligibilityDetails;
import com.softcell.gonogo.model.core.eligibility.EligibilityRequest;
import com.softcell.gonogo.model.core.eligibility.FoirDetail;
import com.softcell.gonogo.model.core.request.DeviationDetails;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequest;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicantRequestV2;
import com.softcell.gonogo.model.core.request.scoring.ScoringApplicationRequest;
import com.softcell.gonogo.model.core.scoring.response.*;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationRequest;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.ops.LoanCharges;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.factory.ScoringJsonBuilder;
import com.softcell.gonogo.service.factory.impl.ScoringJsonBuilderImpl;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.DMZConnector;
import com.softcell.service.DataEntryManager;
import com.softcell.service.impl.DMZConnectorImpl;
import com.softcell.service.impl.DataEntryManagerImpl;
import com.softcell.utils.CroUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.aggregators.WfResultAggregator;
import com.softcell.workflow.aggregators.WfResultAggregatorImpl;
import com.softcell.workflow.component.module.ModuleSetting;
import net.logstash.logback.encoder.org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ssg0302 on 8/7/19.
 */
public class ScoringExecutorV3 extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(ScoringExecutorV3.class);

    private ModuleSetting moduleSetting;

    private ModuleRequestRepository moduleRequestRepository;

    private WfResultAggregatorImpl wfResultAggregatorImpl;

    private ExternalAPILogRepository externalAPILogRepository;

    private DMZConnector dmzConnector;

    private ApplicationRepository applicationMongoRepository;


    private LookupService lookupService;

    private static final String APPLICANTID = "sApplID";

    private static final String CUSTOMERID = "customerId";

    public ScoringExecutorV3(WfResultAggregatorImpl wfResultAggregatorImpl) {

        this.wfResultAggregatorImpl = wfResultAggregatorImpl;

        if (null == dmzConnector) {
            dmzConnector = new DMZConnectorImpl();
        }

        if (null == lookupService) {
            lookupService = new LookupServiceHandler();
        }

        if(null == applicationMongoRepository){
            applicationMongoRepository = new ApplicationMongoRepository();
        }

    }

    /**
     * (non-Javadoc)
     *
     * @see MetaAction#process(Object,
     * ModuleSetting)
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    /**
     * (non-Javadoc)
     *
     * @see MetaAction#run()
     */
    @Override
    public void run() {

        externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());

        ModuleOutcome moduleOutcome = new ModuleOutcome();
        moduleOutcome.setOrder(ScoringDisplayName.APPLICATION_SCORE_ORDERE);
        moduleOutcome.setFieldName(ScoringDisplayName.APPLICATION_SCORE);
        moduleOutcome.setFieldValue("-");
        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();

        goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
        goNoGoCustomerApplication.getIntrimStatus().setScoringModuleResult(moduleOutcome);
        goNoGoCustomerApplication.getIntrimStatus().setAppsStatus(Status.VERIFIED.toString());
        goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());

        logger.info("Scoring executor calling sobre service for {} in stage {}", goNoGoCustomerApplication.getGngRefId(),goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());

        try{
            // Activity log
            ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                    GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.SOBRE.toFaceValue());
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            ScoringResponse scoringResponse = null;
            ScoringApplicantResponse scoringApplicantResponse = null;
            boolean executeForApplication = StringUtils.equalsIgnoreCase(this.executionBase, APPLICATION);
            boolean executeForApplicant = StringUtils.equalsIgnoreCase(this.executionBase, APPLICANT);

            // Fetch module outcome
            try {
                //Check configuration for APPLICANT
                if (executeForApplication) {
                    logger.info("Scoring is based on APPLICATION for refid {}", goNoGoCustomerApplication.getGngRefId());
                    scoringResponse = callScoring();
                    if (scoringResponse != null) {
                        logger.info("Scoring executor getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
                        moduleOutcome.setMessage(scoringResponse.getStatus());
                        if (scoringResponse.getScoreData() != null && scoringResponse.getScoreData().getScoreValue() != null) {
                            moduleOutcome.setFieldValue(scoringResponse.getScoreData().getScoreValue());
                        }
                    } else {
                        logger.warn("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
                        moduleOutcome.setMessage("Scoring Down");
                    }
                    goNoGoCustomerApplication.getApplicantComponentResponse().setScoringServiceResponse(scoringResponse);
                    activityLog.setCustomMsg(moduleOutcome.getMessage());
                    activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                } else {
                    logger.info("Scoring is based on APPLICANT for refid {}", goNoGoCustomerApplication.getGngRefId());
                    scoringApplicantResponse = callApplicantScoring();
                    if (scoringApplicantResponse != null) {
                        //Applicant and coApplicant response filtering.
                        segregrateApplicantsResponse(scoringApplicantResponse);

                        logger.info("Scoring executor getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
                        if (scoringApplicantResponse.getApplicantResult() != null) {
                            if (CollectionUtils.isNotEmpty(scoringApplicantResponse.getApplicantResult())) {
                                //TODO save status as message.
                                moduleOutcome.setMessage("_");
                            } else {
                                moduleOutcome.setMessage("_");
                            }
                        } else {
                            moduleOutcome.setMessage("_");
                        }
                    } else {
                        logger.warn("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
                        moduleOutcome.setMessage("Scoring Down");
                    }

                    activityLog.setCustomMsg(moduleOutcome.getMessage());
                    activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                }
            } catch (Exception e) {
                logger.error("run() has exception {}",  e.getStackTrace());
            }

            try {
                logger.info("Scoring executor call finished for {} in stage {}", goNoGoCustomerApplication.getGngRefId(), goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                activityLog.setStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());
                if(executeForApplication){
                    queueApplication();
                }

                if(executeForApplicant){
                    caseStatus(scoringApplicantResponse);
                }
                logger.info("Before exiting scoring executor for refId {} stage is {}", goNoGoCustomerApplication.getGngRefId(),goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());

                // Set status in activity log
                String appStatus = goNoGoCustomerApplication.getApplicationStatus().toUpperCase();
                activityLog.setStatus(appStatus);
                activityLog.setChangedStage(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());

            } catch (Exception exception) {
                logger.error("run() has exception in queueApplication block {}", exception.getStackTrace());
            }

            stopWatch.stop();
            activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
                    /*
         * Partial Scoring response save in db.
         */
            moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());

            moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
            // Save activity log
            logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
            GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);

        }finally {
            // Since the call can be from service/standalone, need to check whether component manager has started this.
            if (componentManager != null) {
                synchronized (componentManager) {
                    componentManager.executionFinished(getActionId());
                    componentManager.notifyAll();
                }
            }
        }
    }

    private void caseStatus(ScoringApplicantResponse scoringApplicantResponse) {
        if(StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(),GNGWorkflowConstant.DE.toFaceValue())
                || StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(),GNGWorkflowConstant.LEAD.toFaceValue())) {
            if (scoringApplicantResponse != null && scoringApplicantResponse.getSummary() != null) {
                // setting Status and croId from sobre response
                goNoGoCustomerApplication.setApplicationStatus(scoringApplicantResponse.getSummary().getApplicantDecesion());
                // Setting Status from sobre when it is not Queued
                if (!StringUtils.equalsIgnoreCase(scoringApplicantResponse.getSummary().getApplicantDecesion(),
                        GNGWorkflowConstant.QUEUED.name())) {
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP.toString());
                    logger.debug("updated case status from sobre decesion when it is not queued,getApplicantDecesion {}", scoringApplicantResponse.getSummary().getApplicantDecesion());
                }
            } else {
                goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId("default");
                goNoGoCustomerApplication.getIntrimStatus().setCroStatus(Status.COMPLETE.toString());
                goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());
            }
        }
        String institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        // changing stage from DE to CR_Q only in case of SBFC_LAP
        if(goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct() == Product.LAP){
            if(Institute.isInstitute(institutionId, Institute.SBFC)
                    || Institute.isInstitute(institutionId, Institute.JVFS)){
                if(StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(),GNGWorkflowConstant.DE.toFaceValue()))
                    goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());
            }
        }
    }

    private void segregrateApplicantsResponse(ScoringApplicantResponse scoringApplicantResponse) {
        // Get primary applicant's result
        final String applicantId = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId();
        ScoringApplicantResponse primaryResponse = new ScoringApplicantResponse();
        primaryResponse.setSummary(scoringApplicantResponse.getSummary());
        // Get primary applicant's result from the the scoring result received
        if( scoringApplicantResponse.getApplicantResult() != null) {
            primaryResponse.setApplicantResult(scoringApplicantResponse.getApplicantResult().stream().filter(result ->
                    StringUtils.equalsIgnoreCase(applicantId, result.getApplicantId()))
                    .collect(Collectors.toList()));
        }
        // Set scoring response in component response
        ScoringResponse scoringResponse = new ScoringResponse();
        scoringResponse.setApplicantResponse(primaryResponse);
        goNoGoCustomerApplication.getApplicantComponentResponse().setScoringServiceResponse(scoringResponse);

                        /*       Set coapplicants' results  */
        ScoringApplicantResponse coappResponse;
        // Coapplicant list
        List<CoApplicant> coApplicantList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();
        // Coapplicant response list from application
        List<ComponentResponse> coAppCompList = goNoGoCustomerApplication.getApplicantComponentResponseList();
        ComponentResponse coAppCompResponse;
        // Iterate thr' coapplicants list
        if( CollectionUtils.isNotEmpty(coAppCompList) ) {
            for (CoApplicant coapp : coApplicantList) {
                coappResponse = new ScoringApplicantResponse();
                coappResponse.setSummary(scoringApplicantResponse.getSummary());
                if( scoringApplicantResponse.getApplicantResult() != null ) {
                    coappResponse.setApplicantResult(scoringApplicantResponse.getApplicantResult().stream().filter(result ->
                            StringUtils.equalsIgnoreCase(coapp.getApplicantId(), result.getApplicantId()))
                            .collect(Collectors.toList()));
                }
                scoringResponse = new ScoringResponse();
                scoringResponse.setCoApplicantResponse(coappResponse);

                                    /* Set scoring response in component response
                Need to setup coappComponent response if the list is empty
                    or the component response for the application not found */
                if (CollectionUtils.isEmpty(coAppCompList)) {
                    coAppCompList = new ArrayList<>();
                    goNoGoCustomerApplication.setApplicantComponentResponseList(coAppCompList);
                }
                List<ComponentResponse> coappResponseList = coAppCompList.stream().filter(compResponse ->
                        StringUtils.equalsIgnoreCase(coapp.getApplicantId(), compResponse.getApplicantId()))
                        .collect(Collectors.toList());
                if (CollectionUtils.isEmpty(coappResponseList)) {
                    coAppCompResponse = new ComponentResponse();
                    coAppCompResponse.setReferenceId(goNoGoCustomerApplication.getGngRefId());
                    coAppCompResponse.setApplicationId(coapp.getApplicantId());
                    coAppCompList.add(coAppCompResponse);
                } else {
                    coAppCompResponse = coappResponseList.get(0);
                }

                coAppCompResponse.setScoringServiceResponse(scoringResponse);
            }
        }
    }

    private void queueApplication() {
                /* If application is not reinitiated then send to queue manager
            to decide manager.
         */
        if (goNoGoCustomerApplication.getReInitiateCount() == NumberUtils.INTEGER_ZERO) {

            WfResultAggregatorImpl queueManager = new WfResultAggregatorImpl();
            WfResultAggregator qManggerProxy = (WfResultAggregator) ApplicationProxy.getProxy(queueManager, WfResultAggregator.class);
            wfResultAggregatorImpl.setQueue(goNoGoCustomerApplication);

            qManggerProxy = null;


        } else {
                        /* If the application is reInitiated then reset the Cro Decisions
             * and not the cro Queue hence this else Statement.
             */
            List<CroDecision> croDecisions = CroUtils.getCroDecision(goNoGoCustomerApplication);
            goNoGoCustomerApplication.setCroDecisions(croDecisions);

        }

        if (lookupService.checkActionsAccess(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId(),
                ActionName.TVR_DATA_PUSH) && !goNoGoCustomerApplication.getApplicationRequest().isQdeDecision()) {
            /**
             * TVR call
             */
            logger.debug("Going to call TVR push service for refId : {} ", goNoGoCustomerApplication.getGngRefId());

            dmzConnector.pushTvr(goNoGoCustomerApplication.getGngRefId(),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                    goNoGoCustomerApplication);
        }
    }

    private ScoringResponse callScoring() throws Exception {
        String instId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        LookupService lookupService = new LookupServiceHandler();
        ScoringCommunication scoringCommunication;
        if (instId == null) {
            logger.info("Institution id Not Valid for Scoring");
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
            return null;
        } else {
            scoringCommunication = ScoringConfiguration.getCreditial(instId);
            if (scoringCommunication == null) {
                goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
                return null;
            }
        }
        ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();
        ScoringApplicationRequest scoringApplicationRequest = builder.buildScoringJSon(goNoGoCustomerApplication);
        if (executeForCoApplicant) {
            List<ScoringApplicationRequest> coApplicationRequests = builder.buildScoringJsonForCoApplicant(goNoGoCustomerApplication);
            scoringApplicationRequest.setCoApplicationRequests(coApplicationRequests);
        }
        scoringApplicationRequest.setGngRefId(goNoGoCustomerApplication.getGngRefId());
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        logger.info("scoringApplicationRequest is formed");

        if (scoringApplicationRequest != null) {
            moduleRequestRepository.saveScoringRequest(scoringApplicationRequest);

        }

        ObjectMapper request = new ObjectMapper();

        try {

            String valueJson = request.writeValueAsString(scoringApplicationRequest);
            String url = scoringCommunication.getScoringServiceUrl();
            String endpoint = url + "/CalculateScoreSync?" + "INSTITUTION_ID=" + scoringCommunication.getInstitutionId() + "&USER_ID=" + scoringCommunication.getUserId() +
                    "&PASSWORD=" + scoringCommunication.getPassword();
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            String response = new HttpTransportationService().postRequest(endpoint, valueJson, MediaType.APPLICATION_JSON_VALUE);

            if (lookupService.checkActionsAccess(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId(),
                    ActionName.SOBRE_RESPONSE_SAVE)) {
                saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());
            }

            stopWatch.stop();
            logger.debug(String.format("Time taken by sobre api %s", stopWatch.getTotalTimeMillis()));

            ObjectMapper objectMapper = new ObjectMapper();
            ScoringResponse scoringResponse = objectMapper.readValue(response, ScoringResponse.class);
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());

            return scoringResponse;
        } catch (Exception e) {
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
            logger.error("callScoring() has exception {}", e.getStackTrace());
            return null;
        } finally {
            builder = null;
            scoringApplicationRequest = null;
        }
    }

    private ScoringApplicantResponse callApplicantScoring() throws Exception {
        logger.info("Proceeding for APPLICANT for {} ", goNoGoCustomerApplication.getGngRefId());
        String instId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        LookupService lookupService = new LookupServiceHandler();
        ScoringCommunication scoringCommunication;
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();

        //Fetching and setting eligibility
        EligibilityRequest eligibilityRequest = EligibilityRequest.builder().header(applicationRequest.getHeader())
                .refId(applicationRequest.getRefID()).build();
        EligibilityDetails eligibility = applicationMongoRepository.fetchEligibilityDetails(eligibilityRequest);
        if(eligibility != null){
            goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setEligibilityDetails(eligibility);
            if( CollectionUtils.isNotEmpty(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplication().getCollateral())) {
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setPropertyNature(goNoGoCustomerApplication
                        .getApplicationRequest().getRequest().getApplication().getCollateral().get(0).getUsage());
            }
        }

        //fetching CamDetails.
        CamDetailsRequest camDetailsRequest = CamDetailsRequest.builder().header(applicationRequest.getHeader())
                .refId(applicationRequest.getRefID()).build();
        CamDetails camDetails = applicationMongoRepository.fetchCamDetails(camDetailsRequest);

        //fetching ValuationDetails.
        ValuationRequest valuationRequest = ValuationRequest.builder()
                .refId(applicationRequest.getRefID())
                .header(applicationRequest.getHeader())
                .build();
        Valuation valuation = applicationMongoRepository.fetchValuationDetails(valuationRequest);

        VerificationDetails verification = applicationMongoRepository.fetchVerificationDetailsByRefId(applicationRequest.getRefID(),instId);
        DedupeMatch dedupeMatchdedupeMatch = applicationMongoRepository.fetchDedupeInfo(applicationRequest.getRefID(), instId);
        LoanCharges loanCharges=applicationMongoRepository.fetchLoanChargesDetailsByRefId(applicationRequest.getRefID(), instId);
        //fetching
        if (instId == null) {
            logger.info("Institution id not provided !");
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
            return null;
        } else {
            scoringCommunication = ScoringConfiguration.getCreditial(instId);
            if (scoringCommunication == null) {
                logger.info("{} - Scoring not configured for institute {} ! Cannot execute scoring for {}", instId, goNoGoCustomerApplication.getGngRefId());
                goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
                return null;
            }
        }
        ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();

        ScoringApplicantRequestV2 scoringApplicantRequest2=new ScoringApplicantRequestV2();
        scoringApplicantRequest2.setCamDetails(camDetails);
        scoringApplicantRequest2.setValuation(valuation);
        scoringApplicantRequest2.setVerification(verification);
        scoringApplicantRequest2.setDedupeMatch(dedupeMatchdedupeMatch);
        scoringApplicantRequest2.setLoanCharges(loanCharges);
        scoringApplicantRequest2.setEligibility(eligibilityRequest.getEligibilityDetails());

        ScoringApplicantRequestV2 scoringApplicantRequest = builder.buildApplicantScoringJSonV2(goNoGoCustomerApplication, scoringApplicantRequest2);
        scoringApplicantRequest.getApplication().setPolicyName(this.policyName);
        logger.debug("SOBRE objectMapper formed for {}", goNoGoCustomerApplication.getGngRefId());
        // Persist scoring request
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        if (scoringApplicantRequest != null) {
            moduleRequestRepository.saveScoringApplicantRequestV2(scoringApplicantRequest);
        }
        // Create json from request
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String valueJson = objectMapper.writeValueAsString(scoringApplicantRequest);
            valueJson = valueJson.replaceAll(APPLICANTID, CUSTOMERID);
            scoringApplicantRequest.setRequest(valueJson);
            moduleRequestRepository.saveScoringApplicantRequestV2(scoringApplicantRequest);
            String response = connectSobre(valueJson, scoringCommunication);
            response = response.replaceAll(CUSTOMERID, APPLICANTID);
            logger.debug("{} SOBRE response received",  goNoGoCustomerApplication.getGngRefId());
            saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());
            logger.info("Sobre response -4");
            // Deserialize json response
            ScoringApplicantResponse scoringApplicantResponse = null;
            if (StringUtils.isNotEmpty(response)) {
                scoringApplicantResponse = objectMapper.readValue(response, ScoringApplicantResponse.class);
            }
            logger.info("Sobre response -5");
            saveSobreRequestResponse(scoringApplicantRequest, scoringApplicantResponse);
            logger.info("Sobre response -6");
            if (eligibility != null && scoringApplicantResponse != null) {
                if (Institute.isInstitute(instId, Institute.AMBIT)) {
                    if(scoringApplicantResponse.getApplicantResult().get(0).getEligibilityResponse()!= null) {
                        eligibility.getFoirDetail().setEligibleAmount(String.valueOf(scoringApplicantResponse.getApplicantResult().get(0).getEligibilityResponse().getEligibilityAmount()));
                        applicationMongoRepository.saveEligibilityDetails(eligibility);
                        logger.info("Sobre response -7");
                    }
                }
            }
            logger.debug("{} SOBRE response deserialized",  goNoGoCustomerApplication.getGngRefId());
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
            return scoringApplicantResponse;
        } catch (Exception e) {
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
            logger.error("callApplicantScoring() has exception {}", e.getStackTrace()
            );
            return null;
        } finally {
            builder = null;
            scoringApplicantRequest = null;
        }
    }


    /**
     * (non-Javadoc)
     *
     * @see MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }

    private void saveRawResponse(String rawResponse, String refId) {
        logger.debug("Saving data for {}", ActionName.SOBRE_RESPONSE_SAVE);
        RawResponseLog rawResponseLog = RawResponseLog.builder().rawResponse(rawResponse)
                .refId(refId)
                .responseType(ActionName.SOBRE_RESPONSE_SAVE).build();

        externalAPILogRepository.saveRawResonseLog(rawResponseLog);
    }

    public void sobreCall(String applicantId)    {
        ModuleOutcome moduleOutcome = new ModuleOutcome();
        moduleOutcome.setOrder(ScoringDisplayName.APPLICATION_SCORE_ORDERE);
        moduleOutcome.setFieldName(ScoringDisplayName.APPLICATION_SCORE);
        moduleOutcome.setFieldValue("-");

        logger.debug("Proceeding for APPLICANT  with applicant ID {} for {}", applicantId, goNoGoCustomerApplication.getGngRefId());
        String instId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        boolean primary = isPrimary(applicantId);
        ScoringCommunication scoringCommunication;
        if (instId == null) {logger.info("Institution id not provided !");

            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
            return;
        } else {
            scoringCommunication = ScoringConfiguration.getCreditial(instId);
            if (scoringCommunication == null) {
                logger.debug("{} - Scoring not configured for institute {} ! Cannot execute scoring for {}", instId, goNoGoCustomerApplication.getGngRefId());
                goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.NOT_AUTHORIZED.toString());
                return;
            }
        }
        ScoringJsonBuilder builder = new ScoringJsonBuilderImpl();
        ScoringApplicantRequest scoringApplicantRequest = builder.buildApplicantRequest(goNoGoCustomerApplication,applicantId, primary);
        logger.debug("SOBRE objectMapper formed for {}", goNoGoCustomerApplication.getGngRefId());
        // Persist scoring request
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        if (scoringApplicantRequest != null) {
            moduleRequestRepository.saveScoringApplicantRequest(scoringApplicantRequest);
        }
        // Create json from request
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String valueJson = objectMapper.writeValueAsString(scoringApplicantRequest);
            valueJson = valueJson.replaceAll(APPLICANTID, CUSTOMERID);
            String response = connectSobre(valueJson, scoringCommunication);
            response = response.replaceAll(CUSTOMERID, APPLICANTID);
            logger.debug("{} SOBRE response received",  goNoGoCustomerApplication.getGngRefId());
            saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());
            logger.info("Sobre response -1");
            // Deserialize json response
            ScoringApplicantResponse scoringApplicantResponse = null;
            if (StringUtils.isNotEmpty(response)) {
                scoringApplicantResponse = objectMapper.readValue(response, ScoringApplicantResponse.class);
                logger.info("Sobre response -2");
            }
            logger.debug("{} SOBRE response deserialized",  goNoGoCustomerApplication.getGngRefId());
            // Set response in gonogoAppcn
            if (scoringApplicantResponse != null) {
                if (scoringApplicantResponse.getApplicantResult() != null) {
                    if (CollectionUtils.isNotEmpty(scoringApplicantResponse.getApplicantResult())) {
                        setResponse(scoringApplicantResponse, applicantId, primary);
                        //TODO save status as message.
                        moduleOutcome.setMessage("_");
                    } else {
                        moduleOutcome.setMessage("_");
                    }
                } else {
                    moduleOutcome.setMessage("_");
                }
            }else {
                logger.warn("Scoring executor not getting response from sobre service for  {} referenceID", goNoGoCustomerApplication.getGngRefId());
                moduleOutcome.setMessage("Scoring Down");
            }
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
        } catch (Exception e) {
            goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(Status.COMPLETE.toString());
            logger.error("sobreCall() has exception {}", e.getStackTrace());
            return ;
        } finally {
            builder = null;
            scoringApplicantRequest = null;
        }

    }

    private void setResponse(ScoringApplicantResponse scoringApplicantResponse, String applicantId, boolean primary) {
        ComponentResponse componentResponse;
        ScoringResponse scoringResponse ;
        if( primary){
            componentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();
            if( componentResponse != null){
                scoringResponse = componentResponse.getScoringServiceResponse();
                if( scoringResponse == null ){
                    scoringResponse = new ScoringResponse();
                    componentResponse.setScoringServiceResponse(scoringResponse);
                }
                scoringResponse.setApplicantResponse(scoringApplicantResponse);
            }else  {
                // for componentResponse = null
                componentResponse = new ComponentResponse();
                componentResponse.setReferenceId(goNoGoCustomerApplication.getGngRefId());
                componentResponse.setApplicationId(applicantId);
                scoringResponse = new ScoringResponse();
                scoringResponse.setApplicantResponse(scoringApplicantResponse);
                componentResponse.setScoringServiceResponse(scoringResponse);
                goNoGoCustomerApplication.setApplicantComponentResponse(componentResponse);
            }
        } else {
            // TODO for coapplicant
            // get coapplicant's component reponse
            // Coapplicant response list from application
            List<ComponentResponse> coAppCompList = goNoGoCustomerApplication.getApplicantComponentResponseList();
            if( CollectionUtils.isEmpty(coAppCompList) ) {
                coAppCompList = new ArrayList<>();
                goNoGoCustomerApplication.setApplicantComponentResponseList(coAppCompList);
            }
            List<ComponentResponse> coappResponseList = coAppCompList.stream().filter(compResponse ->
                    StringUtils.equalsIgnoreCase(applicantId, compResponse.getApplicantId()) )
                    .collect(Collectors.toList());
            if( CollectionUtils.isEmpty(coappResponseList) ){
                componentResponse = new ComponentResponse();
                componentResponse.setReferenceId(goNoGoCustomerApplication.getGngRefId());
                componentResponse.setApplicationId(applicantId);
                scoringResponse = new ScoringResponse();
                scoringResponse.setCoApplicantResponse(scoringApplicantResponse);
                componentResponse.setScoringServiceResponse(scoringResponse);
                coAppCompList.add(componentResponse);
            } else {
                componentResponse = coappResponseList.get(0);
                scoringResponse = componentResponse.getScoringServiceResponse();
                if( scoringResponse == null ){
                    scoringResponse = new ScoringResponse();
                    componentResponse.setScoringServiceResponse(scoringResponse);
                }
                scoringResponse.setApplicantResponse(scoringApplicantResponse);
            }
        }
    }

    private boolean isPrimary(String applicantId) {
        boolean primary = false;
        if (org.apache.commons.lang.StringUtils.equalsIgnoreCase(applicantId,
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId())) {
            primary = true;
        }
        return primary;
    }

    private String connectSobre(String valueJson, ScoringCommunication scoringCommunication) throws IOException {
        String url = scoringCommunication.getScoringServiceUrl();
        StringBuilder endpoint = new StringBuilder(url) ;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        if( StringUtils.equalsIgnoreCase(executionBase, APPLICANT)) {
            endpoint.append("/processApplication?")
                    .append("INSTITUTION_ID=").append(scoringCommunication.getInstitutionId())
                    .append("&USER_ID=").append(scoringCommunication.getUserId())
                    .append("&PASSWORD=").append(scoringCommunication.getPassword());

            logger.debug("Sending SOBRE objectMapper for {} to url {} is {}", goNoGoCustomerApplication.getGngRefId(),
                    endpoint.toString(), valueJson);

        } else {
            // TODO for APPLICATION
        }
        String response = new HttpTransportationService().postRequest(endpoint.toString(), valueJson, MediaType.APPLICATION_JSON_VALUE);
        if(response != null) {
            response = response.replaceAll(CUSTOMERID, APPLICANTID);
        }
        logger.debug("{} SOBRE response received", goNoGoCustomerApplication.getGngRefId());
        saveRawResponse(response, goNoGoCustomerApplication.getGngRefId());
        logger.info("Sobre response -3");
        stopWatch.stop();
        logger.debug(String.format("Time taken by sobre api %s", stopWatch.getTotalTimeMillis()));
        return response;
    }

    private void saveSobreRequestResponse(ScoringApplicantRequestV2 scoringApplicantRequest, ScoringApplicantResponse scoringApplicantResponse) {
        try {
            ScoringCallLog callLog = new ScoringCallLog();
            callLog.setRefId(scoringApplicantRequest.getHeader().getApplicationId());
            callLog.setStep(scoringApplicantRequest.getApplication().getPolicyName());
            callLog.setInstitutionId(scoringApplicantRequest.getHeader().getInstitutionId());
            callLog.setCallDate(new Date());
            logger.debug("Inside saveSobreRequestResponse()- scoringApplicantRequest:1");
            callLog.setRequestString(JsonUtil.ObjectToString(scoringApplicantRequest));
            logger.debug("Inside saveSobreRequestResponse()- scoringApplicantRequest:2");
            callLog.setRequest(scoringApplicantRequest);

            if (scoringApplicantResponse != null) {
                /*callLog.setResponseString(JsonUtil.ObjectToString(scoringApplicantResponse));*/
                callLog.setResponse(scoringApplicantResponse);
            }
            callLog.setRequestType(EndPointReferrer.GET_SCORING_LOG);
            logger.debug("Inside saveSobreRequestResponse(): saveSobreCallLog:3");
            externalAPILogRepository.saveSobreCallLog(callLog);
            logger.debug("Inside saveSobreRequestResponse(): saveSobreCallLog:4");
        } catch (Exception e) {
            logger.error("Exception occured while saving sobreLogs {}", e.getStackTrace());
        }
    }

}