/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 3, 2016 10:29:06 AM
 */
package com.softcell.workflow.executors.extapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.model.saathi.SaathiCallLog;
import com.softcell.gonogo.model.saathi.SaathiGetCdRequest;
import com.softcell.gonogo.model.saathi.SaathiResponse;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.utils.TransportUtils;
import com.softcell.utils.JsonUtil;
import com.softcell.workflow.component.module.ModuleSetting;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

/**
 * @author kishorp
 */
public class SaathiExtApiExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(SaathiExtApiExecutor.class);
    private static final String  PROSPECTNO = "prospectno";
    private static final String  MOBILENO ="mobileno";

    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private SaathiCallLog apiLog;

    private ModuleSetting moduleSetting;

    private ModuleRequestRepository moduleRequestRepository;

    private ExternalAPILogRepository externalAPILogRepository;

    private WorkFlowCommunicationManager workFlowCommunicationManager;

    /**
     * @param goNoGoCustomerApplication
     * @param moduleSetting
     * @return
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {

        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.softcell.gonogo.workflow.actions.MetaAction#run()
     */
    @Override
    public void run() {

        if (!moduleSetting.isActive()) {
            if (goNoGoCustomerApplication.getReInitiateCount() == 0) {
                logger.info(Status.UNAUTHORISED.toString() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for " + moduleSetting.getModuleName());

                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.NOT_AUTHORIZED.toString());
                logger.error(Status.UNAUTHORISED.toString());
                return;
            }
            return;
        }
        // ---------------------
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());
        workFlowCommunicationManager = new WorkFlowCommunicationManagerImpl();

        // Activity log
        ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.TVS_CD_API_CALL.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
        try {
            SaathiResponse saathiResponse = getCustomerData();
            goNoGoCustomerApplication.getApplicantComponentResponse().setSaathiResponse(saathiResponse);
            goNoGoCustomerApplication.getIntrimStatus().setSaathiPullStatus(Status.COMPLETE.toString());
            // Set moduleoutcome
            setModuleOutcome(saathiResponse);
            logger.info("Saathi Get response : {}", saathiResponse);
            externalAPILogRepository.saveTvscdCallLog(apiLog);
            activityLog.setCustomMsg(saathiResponse.getStatusMessage());
        } catch (Exception e) {
            goNoGoCustomerApplication.getIntrimStatus().setSaathiPullStatus(Status.ERROR.toString());
            // Set moduleoutcome
            setModuleOutcome(null);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(e.getMessage());
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());

        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
    }

    private void setModuleOutcome(SaathiResponse saathiResponse) {
        ModuleOutcome moduleOutcome = new ModuleOutcome();

        moduleOutcome.setMessage("");
        moduleOutcome.setFieldName(ScoringDisplayName.SAATHI_CASE_STATUS);
        moduleOutcome.setFieldValue(GNGWorkflowConstant.NOT_AVAILABLE.toFaceValue());

        if( saathiResponse != null && saathiResponse.getCasedetails() != null ) {
            String status = saathiResponse.getCasedetails().getStatus();
            if (StringUtils.isNotBlank(status) ){
                moduleOutcome.setFieldValue(status);
            }
        }
        moduleOutcome.setOrder(ScoringDisplayName.SAATHI_RESULT_ORDER);
        goNoGoCustomerApplication.getIntrimStatus().setSaathiModuleResult(moduleOutcome);
    }

    /*
    * (non-Javadoc)
    *
    * @see com.softcell.gonogo.workflow.actions.MetaAction#finishProcess()
    */
    @Override
    public String finishProcess() {
        return null;
    }

    private SaathiResponse getCustomerData() throws Exception {
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        if (org.springframework.util.CollectionUtils.isEmpty(applicationRequest.getRequest().getApplicant().getPhone()) ) {
            throw new GoNoGoException("Phone number not present");
        }

        String urlType = UrlType.TVS_CD_GET_STATUS.toValue();
        WFJobCommDomain customerDataPullConfig = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);

        //Build request for the connector call
        SaathiGetCdRequest saathiGetCdRequest = new SaathiGetCdRequest();
        saathiGetCdRequest.setData(formRequest(applicationRequest));
        saathiGetCdRequest.setHeaderKeyList(customerDataPullConfig.getKeys());

        String url = Arrays.asList(customerDataPullConfig.getBaseUrl(), customerDataPullConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        //Make the API call
        SaathiResponse saathiResponse = (SaathiResponse) TransportUtils.postJsonRequest(saathiGetCdRequest,
                url, SaathiResponse.class);

        apiLog = new SaathiCallLog();
        apiLog.setApi(urlType);
        apiLog.setMobileNumber(applicationRequest.getRequest().getApplicant().getPhone().get(0).getPhoneNumber());
        apiLog.setRefId(applicationRequest.getRefID());
        apiLog.setResponse(saathiResponse);

        return saathiResponse;
    }

    private String formRequest(ApplicationRequest applicationRequest) throws JsonProcessingException{
        Map<String, String> requestData = new HashMap<>();
        requestData.put(MOBILENO, applicationRequest.getRequest().getApplicant().getPhone().get(0).getPhoneNumber());
        requestData.put(PROSPECTNO, applicationRequest.getRefID());
        return JsonUtil.ObjectToString(requestData);
    }

    private WFJobCommDomain getServiceConfiguration(String institutionId, String urlType) throws GoNoGoException {
        WFJobCommDomain customerDataPullConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                institutionId, urlType);

        if (null == customerDataPullConfig) {
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType));
        }
        return customerDataPullConfig;
    }
}
