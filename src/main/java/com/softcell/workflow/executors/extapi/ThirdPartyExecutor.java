package com.softcell.workflow.executors.extapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.softcell.constants.Constant;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.IntimationConstants;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.UploadFileMongoRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.*;
import com.softcell.gonogo.model.adroit.*;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.cam.CamDetails;
import com.softcell.gonogo.model.core.perfios.RetriveFileData;
import com.softcell.gonogo.model.core.valuation.Valuation;
import com.softcell.gonogo.model.core.valuation.ValuationDetails;
import com.softcell.gonogo.model.core.verification.VerificationDetails;
import com.softcell.gonogo.model.finfort.FileUploadInfo;
import com.softcell.gonogo.model.finfort.FinfortCallLog;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.rest.utils.EndPointReferrer;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.thirdparty.utils.AdroitHelper;
import com.softcell.service.thirdparty.utils.FinfortHelper;
import com.softcell.service.thirdparty.utils.ThirdPartyIntegrationHelper;
import com.softcell.service.utils.TransportUtils;
import com.softcell.ssl2.finfort.model.ApiResponse;
import com.softcell.ssl2.finfort.model.DetailedFinancials;
import com.softcell.ssl2.finfort.model.FileUploadData;
import com.softcell.ssl2.finfort.model.Report;
import com.softcell.ssl2.perfios.PerfiosResponse;
import com.softcell.utils.GngUtils;
import com.softcell.utils.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by archana on 12/10/18.
 */
public class ThirdPartyExecutor implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyExecutor.class);
    /*
     * @param  Object obj
     *                  When agengy type = VALUATION_INITIATE obj = ApplicationRequest
     *                  When agengy type = VALUATION_CHECK or VALUATION_GET_IMAGE
     *                     obj = ValuationDetails
     * @param Object requestFromObject
     *                  When agengy type = VALUATION_INITIATE obj = Valuation
     *
    * */
    // This is third party request object to be formed
    private Object thirdPartyRequestObj;
    // This is application object for which third party request is to be formed
    private Object requestFromObject;

    // Identifiers of various domain objects
    private String refId;
    private String institutionId;
    private String ackId;
    private String applicantId;
    //Domain objects requiredto process various requests
    private ApplicationRequest applicationRequest;
    private ValuationDetails valuationDetails;
    private CamDetails camDetails;
    private VerificationDetails verificationDetails;

    // Flag required for processing
    private boolean initied = false;
    private boolean threadNotify = false;
    boolean executed = false;

    // Configurations and helpers
    private WFJobCommDomain serviceConfig;
    private ThirdPartyIntegrationHelper.ThirdParty thirdPartyService;

    private ThirdPartyIntegrationHelper thirdPartyHelper;
    private FinfortHelper finfortHelper;

    // Database services
    private ExternalAPILogRepository externalAPILogRepository;
    private ApplicationRepository applicationRepository;
    private WorkFlowCommunicationManager workFlowCommunicationManager;
    public ThirdPartyExecutor(){
        externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());
        applicationRepository = new ApplicationMongoRepository();

        thirdPartyHelper = new ThirdPartyIntegrationHelper();
        thirdPartyHelper.setFileRepository(new UploadFileMongoRepository(MongoConfig.getMongoTemplate(), MongoConfig.getGridFSTemplate()));
        finfortHelper = new FinfortHelper();
        workFlowCommunicationManager = new WorkFlowCommunicationManagerImpl();
    }

    /*
     * @param  Object obj
     *                  When agengy type = VALUATION_INITIATE obj = ApplicationRequest
     *                  When agengy type = VALUATION_CHECK or VALUATION_GET_IMAGE
     *                     obj = ValuationDetails
     * @param Object requestFromObject
     *                  When agengy type = VALUATION_INITIATE obj = Valuation
     *
    * */
    public void init(String refId, String institutionId, WFJobCommDomain serviceConfig, Object obj,
                     Object requestFromObject){
        if( serviceConfig != null && obj != null) {
            logger.info("{} - {}", refId, serviceConfig);
            this.refId = refId;
            this.institutionId = institutionId;
            this.serviceConfig = serviceConfig;

            this.requestFromObject = requestFromObject;
            thirdPartyService = ThirdPartyIntegrationHelper.ThirdParty.getService(serviceConfig.getServiceId());

            if(thirdPartyService != null){
                if( thirdPartyService == ThirdPartyIntegrationHelper.ThirdParty.ADROIT) {
                    buildAdroitRequest(obj, requestFromObject);
                }
                else if( thirdPartyService == ThirdPartyIntegrationHelper.ThirdParty.FINFORT) {
                    // Check for type ; if ftp connection then set thirdPartyRequestObj to filelist
                    if( thirdPartyHelper.isRequestType( this.serviceConfig.getType(), thirdPartyHelper.SFTP_CONNECTION) ){
                        thirdPartyRequestObj = obj;
                    } else {
                        buildFinfortRequest(obj, requestFromObject);
                    }
                }

                if( this.thirdPartyRequestObj != null){
                    initied = true;
                }
            } // Third party service available
        }
        logger.info("{} : 3PExecutor initiated {} for {} {} ", refId, initied, serviceConfig.getServiceId(), serviceConfig.getType());
    }

    @Override
    public void run(){
        threadNotify = true;
        execute();
    }

    public boolean execute(){
        try {
            if (initied) {
                logger.info("Started 3rdParty executor for {}-{} for refId {}", serviceConfig.getServiceId(), serviceConfig.getType(), refId);

                // If service type is SFTP CONNECTION then take another flow
                if( thirdPartyHelper.isRequestType( this.serviceConfig.getType(), thirdPartyHelper.SFTP_CONNECTION) ){
                    transferFiles();
                } else {
                    callThirdParty();
                }
                executed = true;
            } else {
                logger.error("3PExecutor not initiated !");
            }
        } catch (Exception e){
            executed = false;
            logger.error("{}",e.getStackTrace());
            logger.error("Error while executing thirdparty service call {}" ,e.getStackTrace());
        } finally {
            try {
                if( threadNotify ) {
                    synchronized (this) {
                        notifyAll();
                    }
                }
            } catch(Exception e){
                logger.error("Exception in notifying thread wait...{}", e.getStackTrace());
                logger.error("{}",e.getStackTrace());
            }
        }
        return executed;
    }

    private void callThirdParty() throws Exception {
        // Form Url
        String url = formUrl();
        // Set keys
        Map<String, String> headerMap = new HashMap<>();
        introduceKeyValue(serviceConfig.getKeys());
        if (CollectionUtils.isNotEmpty(serviceConfig.getKeys())) {
            for (HeaderKey key : serviceConfig.getKeys()) {
                headerMap.put(key.getKeyName(), key.getKeyValue());
            }
        }
        String inputJson = JsonUtil.ObjectToString(thirdPartyRequestObj);
        logger.debug("{} request {} for url {}", refId, inputJson, url);
        //Make the API call
        String response = new HttpTransportationService().postData(url, inputJson, headerMap, MediaType.APPLICATION_JSON_VALUE);
        logger.debug("{} response {}", refId, response);

        if (thirdPartyService == ThirdPartyIntegrationHelper.ThirdParty.ADROIT) {
            handleAdroitResponse(url, headerMap, response);
        } else if (thirdPartyService == ThirdPartyIntegrationHelper.ThirdParty.FINFORT) {
            handleFinfortResponse(url, headerMap, response);
        }
    }

    private void transferFiles() {
        // Check for service
        if( ThirdPartyIntegrationHelper.ThirdParty.iService(serviceConfig.getServiceId(),
                ThirdPartyIntegrationHelper.ThirdParty.FINFORT) ) {
            // Form Url
            String url = formUrl();

           /* Transfer files and save in db
                    Iterate over the filelist
                        Call connector
                        Download file
                        Save in image store
            */
            // File list is stored in thirdPartyRequestObj which is of type FileUploadRequest
            com.softcell.ssl2.finfort.model.FileUploadRequest fileUploadRequest
                    = (com.softcell.ssl2.finfort.model.FileUploadRequest) ((FileUploadInfo)thirdPartyRequestObj).getUploadRequest();
            applicantId = ((FileUploadInfo)thirdPartyRequestObj).getApplicantId();
            ackId = fileUploadRequest.getAckId();
            List<FileUploadData> filelist = fileUploadRequest.getFileUploadData();
            if( CollectionUtils.isNotEmpty(filelist)){
                logger.info("AckId {} Download {} files for {}", ackId, filelist.size(), serviceConfig.getServiceId());
                String fileName = null;

                FinfortCallLog finfortCallLog = new FinfortCallLog();
                finfortCallLog.setRefId(refId);
                finfortCallLog.setInstitutionId(institutionId);
                finfortCallLog.setApplicantId(applicantId);
                finfortCallLog.setAcknowledgementId(ackId);
                finfortCallLog.setRequestType(ThirdPartyIntegrationHelper.SFTP_CONNECTION);

                FTPPayload payload  = new FTPPayload();
                payload.setInstitutionId(this.institutionId);

                for(FileUploadData fileData : filelist){
                    try {
                        fileName = fileData.getName();
                        // Set log details
                        finfortCallLog.setRequest(fileName);
                        finfortCallLog.setCallDate(new Date());

                        // Set payload
                        payload.setFileName(fileName);
                        payload.setPath(fileData.getPath());

                        //Make the API call
                        FTPResponse ftpResponse = (FTPResponse) TransportUtils.postJsonRequest(payload, url, FTPResponse.class);
                        if( StringUtils.equalsIgnoreCase(ftpResponse.getStatus(), Constant.SUCCESS)) {
                            thirdPartyHelper.saveFileData(refId, institutionId, applicantId, (String)requestFromObject,  ackId, serviceConfig,
                                    fileData, fileName, ftpResponse.getBytes());
                            finfortCallLog.setResponse(ThirdPartyIntegrationHelper.MSG_DOWNLOADED);
                        } else {
                            finfortCallLog.setResponse(ftpResponse.getError());
                        }
                        externalAPILogRepository.saveFinfortCallLog(finfortCallLog);
                    } catch( Exception e ){
                        logger.error("{}",e.getStackTrace());
                        logger.error("AckId {} file {} - problem while transferring {}", ackId, fileName, e.getStackTrace());
                        finfortCallLog.setResponse(ThirdPartyIntegrationHelper.MSG_DOWNLOAD_ERROR + "-" + e.getStackTrace());
                        externalAPILogRepository.saveFinfortCallLog(finfortCallLog);
                    }
                }
            }
        } else {
            logger.info("{}  service not configured for {}",serviceConfig.getServiceId(), serviceConfig.getType());
        }
    }

    private void buildAdroitRequest(Object obj, Object requestFromObject) {
        if(thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_INITIATE) ) {
            this.applicationRequest = (ApplicationRequest)obj;
            this.thirdPartyRequestObj = thirdPartyHelper.buildValuationTriggerRequest(refId, institutionId, serviceConfig.getServiceId(),
                    this.applicationRequest, (Valuation)requestFromObject);
        } else if(thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_CHECK) ) {
            valuationDetails = (ValuationDetails)obj;
            this.ackId = ((ThirdPartyCallLog)requestFromObject).getAcknowledgementId();
            this.thirdPartyRequestObj = thirdPartyHelper.buildValuationCheckRequest( institutionId, serviceConfig.getServiceId(), (ThirdPartyCallLog)requestFromObject);
        } else if(thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_GET_IMAGE) ) {
            valuationDetails = (ValuationDetails)obj;
            this.ackId = valuationDetails.getAcknowledgmentId();
            this.thirdPartyRequestObj = thirdPartyHelper.buildValuationImageRequest( valuationDetails.getValuationOutput().getImageList().get(0).getImageId(),
                    institutionId, serviceConfig.getServiceId(), valuationDetails);
        }
    }

    private void buildFinfortRequest(Object obj, Object requestFromObject) {
        if(thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.INITIATE_INDIVIDUAL) ||
                thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.INITIATE_CORPORATE)){
            this.applicationRequest = (ApplicationRequest) obj;
            this.thirdPartyRequestObj = finfortHelper.buildInitiateRequest(this.refId, this.institutionId,
                    serviceConfig.getServiceId(), applicationRequest, (Applicant)this.requestFromObject);

        } else if(thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.FETCH_REPORT) ){
            this.ackId = ((FinfortCallLog)requestFromObject).getAcknowledgementId();
            this.applicantId = ((FinfortCallLog)requestFromObject).getApplicantId();
            this.applicationRequest = (ApplicationRequest)obj;
            this.camDetails = applicationRepository.fetchCamDetails(refId, institutionId, null);
            this.verificationDetails = applicationRepository.fetchVerificationDetailsByRefId(refId, institutionId);
            this.thirdPartyRequestObj = finfortHelper.buildFetchRequest(this.ackId);
        }
    }

    private void handleAdroitResponse(String url, Map<String, String> headerMap, String response) throws IOException {
        String inputJson;
        if (StringUtils.isNotEmpty(response) && (StringUtils.contains(response, AdroitHelper.SUCCESS)
                || StringUtils.contains(response, Integer.toString(HttpStatus.SC_OK)))) {
            if (thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_INITIATE)) {
                AdroitVerificationResponse verificationResponse = (AdroitVerificationResponse) JsonUtil.StringToObject(response, AdroitVerificationResponse.class);
                saveAdroitCall(thirdPartyRequestObj, verificationResponse);
            } else if (thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_CHECK)) {
                AdroitVerificationResponse verificationResponse = (AdroitVerificationResponse) JsonUtil.StringToObject(response, AdroitVerificationResponse.class);
                saveAdroitCall(thirdPartyRequestObj, verificationResponse);
                AdroitHelper.populateValuationOutput(valuationDetails, verificationResponse);
            } else if (thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_GET_IMAGE)) {
                List<ImageList> imageList = valuationDetails.getValuationOutput().getImageList();
                // Getting first image
                saveImageData(response, imageList.get(0));
                // There may be more images ; iterate through the list
                if (imageList.size() > 1) {
                    for (int i = 1; i < imageList.size(); i++) {
                        this.thirdPartyRequestObj = thirdPartyHelper.buildValuationImageRequest(imageList.get(i).getImageId(),
                                                        institutionId, serviceConfig.getServiceId(), valuationDetails);
                        inputJson = JsonUtil.ObjectToString(thirdPartyRequestObj);
                        logger.debug("{} request {} for url {}", refId, inputJson, url);
                        //Make the API call
                        response = new HttpTransportationService().postData(url, inputJson, headerMap, MediaType.APPLICATION_JSON_VALUE);
                        saveImageData(response, imageList.get(i));
                    }
                }
            }
        } else {
            try {
                saveAdroitCall(thirdPartyRequestObj, response);
            } catch (Exception e) {
                logger.error("{}",e.getStackTrace());
                logger.error("Error while saving response {} : {}", response, e.getStackTrace());
            }

        }
    }

    private void handleFinfortResponse(String url, Map<String, String> headerMap, String response) throws IOException {
        String inputJson;
        if (StringUtils.isNotEmpty(response) && (StringUtils.contains(response, AdroitHelper.SUCCESS)
                || StringUtils.contains(response, Integer.toString(HttpStatus.SC_OK)))) {
            // This is for trigger ; check the request type

            if (thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.INITIATE_INDIVIDUAL )
                    || thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.INITIATE_CORPORATE ) ) {
                ApiResponse apiResponse = (ApiResponse) JsonUtil.StringToObject(response, ApiResponse.class);
                ((Applicant) this.requestFromObject).getThirdPartyCall().setFinfortInitiated(true);
                saveFinFortCallLog(thirdPartyRequestObj, apiResponse);
            } else if (thirdPartyHelper.isRequestType(serviceConfig.getType(), ThirdPartyIntegrationHelper.FETCH_REPORT )){

                Report report = getReport(response);

                finfortHelper.populateFinancialData(refId, institutionId, applicantId, applicationRequest,camDetails,
                                        verificationDetails, report );
                // save camdetails and verification details
                applicationRepository.saveCamDetails(refId, institutionId, camDetails, EndPointReferrer.CAM_PL_BS_DETAILS);
                // Save verification details
                Header header = new Header();
                header.setInstitutionId(institutionId);
                header.setLoggedInUserRole(GNGWorkflowConstant.USER_SYSTEM.toFaceValue());
                header.setDealerId(FinfortHelper.AGENCY_CODE);
                Map<String, Set<String>> verificationStatusDealersMap = new HashMap<>();
                verificationStatusDealersMap.put(IntimationConstants.VERIFICATION_STATUS_VERIFIED, new HashSet<>());

                applicationRepository.saveVerificationDetails(refId, header, EndPointReferrer.ITR_VERIFICATION ,
                                                                verificationDetails, verificationStatusDealersMap);
            } else {
                // Yet to come another requests
            }
        } else { // failure
            try {
                //ApiResponse apiResponse = JsonUtil.StringToObject(response, ApiResponse.class);
                saveFinFortCallLog(thirdPartyRequestObj, response);
            } catch (Exception e) {
                logger.error("{}",e.getStackTrace());
                logger.error("Error while saving response {} : {}", response, e.getStackTrace());
            }

        }
    }

    private Report getReport(String response) throws IOException {
        Report report =new Report();
        Object payload = ((ApiResponse ) JsonUtil.StringToObject(response, ApiResponse.class)).getPayload();
        report = JsonUtil.MapToObject((Map<String, Object>)payload, Report.class);

        return report;
    }
    /* It is possible that the header values are to be set from application*/
    private void introduceKeyValue(List<HeaderKey> keys) {
        if(StringUtils.equalsIgnoreCase(serviceConfig.getServiceId(), ThirdPartyIntegrationHelper.ThirdParty.ADROIT.name()) ||
            StringUtils.equalsIgnoreCase(serviceConfig.getServiceId(), ThirdPartyIntegrationHelper.ThirdParty.FINFORT.name())) {
            for (HeaderKey key : keys) {
                if (AdroitHelper.getHeaderKeys().contains(key.getKeyName())) {
                    if (StringUtils.equalsIgnoreCase(key.getKeyName(), AdroitHelper.HEADER_APPLICATIONID)) {
                        key.setKeyValue(this.refId);
                    }
                }
            }
        }
    }

    private void saveImageData(String response, ImageList imageinfo) throws IOException {
        AdroitVerificationResponse verificationResponse = (AdroitVerificationResponse) JsonUtil.StringToObject(response, AdroitVerificationResponse.class);
        saveAdroitCall(thirdPartyRequestObj, verificationResponse);
        logger.info("Saving image data from {} ...", serviceConfig.getServiceId());
        thirdPartyHelper.storeValuationImages(refId, institutionId, valuationDetails, verificationResponse.getPayload().getFiledata(),
                serviceConfig.getServiceId(), serviceConfig.getProductName(), imageinfo);

    }


    private void saveAdroitCall(Object adrequest, Object response) {
        AdroitCallLog callLog = new AdroitCallLog();
        AdroitVerificationResponse adresponse;
        callLog.setRefId(refId);
        callLog.setInstitutionId(institutionId);

        //callLog.setAcknowledgementId(adresponse.getAcknowledgementId());
        String requestType = Constant.BLANK;
        String collId = null;
        callLog.setCallDate(new Date());
        if(response instanceof String ) {
            callLog.setAcknowledgementId(ackId);
            callLog.setRequestType(serviceConfig.getType());
            callLog.setStrResponse((String)response);
        } else {
            adresponse = (AdroitVerificationResponse)response;
            if(thirdPartyHelper.isRequestType( serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_INITIATE ) ){
                collId = ((AdroitRequest) thirdPartyRequestObj).getCollateralId();
                requestType = ThirdPartyIntegrationHelper.VALUATION_INITIATE;
                ackId = adresponse.getAcknowledgementId();
            } else if( thirdPartyHelper.isRequestType( serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_CHECK) ) {
                collId = valuationDetails.getCollateralId();
                requestType = ThirdPartyIntegrationHelper.VALUATION_CHECK;
            } else if( thirdPartyHelper.isRequestType( serviceConfig.getType(), ThirdPartyIntegrationHelper.VALUATION_GET_IMAGE) ) {
                collId = valuationDetails.getCollateralId();
                requestType = ThirdPartyIntegrationHelper.VALUATION_GET_IMAGE;
            }

            callLog.setCollateralId(collId);
            callLog.setRequestType(requestType);
            callLog.setAcknowledgementId(ackId);
            callLog.setRequest(adrequest);
        }
        externalAPILogRepository.saveAdroitCallLog(callLog);
    }

    private void saveFinFortCallLog(Object finfortRequest, Object response) {
        FinfortCallLog callLog = new FinfortCallLog();
        callLog.setRefId(refId);
        callLog.setInstitutionId(institutionId);
        callLog.setApplicantId(((Applicant)this.requestFromObject).getApplicantId());

        String requestType = Constant.BLANK;
        callLog.setCallDate(new Date());
        if( response instanceof String ) {
            callLog.setStrResponse((String)response);
        } else {
            ApiResponse apiResponse = (ApiResponse)response;
            callLog.setAcknowledgementId(apiResponse.getAckId());
            if(thirdPartyHelper.isRequestType( serviceConfig.getType(), ThirdPartyIntegrationHelper.INITIATE_INDIVIDUAL) ){
                requestType = ThirdPartyIntegrationHelper.INITIATE_INDIVIDUAL;
            } else if( thirdPartyHelper.isRequestType( serviceConfig.getType(), ThirdPartyIntegrationHelper.INITIATE_CORPORATE) ) {
                requestType = ThirdPartyIntegrationHelper.INITIATE_CORPORATE;
            }
            callLog.setRequestType(requestType);
            callLog.setRequest(finfortRequest);
            callLog.setResponse(apiResponse);
            externalAPILogRepository.saveFinfortCallLog(callLog);
        }
    }

    private String formUrl(){
        StringBuilder sb = new StringBuilder();
        sb.append(serviceConfig.getBaseUrl());
        if( !serviceConfig.getBaseUrl().endsWith(FieldSeparator.FORWARD_SLASH)){
            sb.append(FieldSeparator.FORWARD_SLASH);
        }
        sb.append(serviceConfig.getEndpoint());
        return sb.toString();
    }
    public RetriveFileData callPerfios(RetriveFileData retriveFileData, Header header, String refId) throws JsonProcessingException {
        institutionId = header.getInstitutionId();
      
        logger.debug("Perfios retrive-file call initaite for refId {} ",refId );
        serviceConfig = workFlowCommunicationManager.getWfCommDomainJob(
                institutionId, header.getProduct().name(), ThirdPartyIntegrationHelper.PERFIOS, ThirdPartyIntegrationHelper.PERFIOS);
        String url = serviceConfig.getBaseUrl();
        Map<String, String> headerMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(serviceConfig.getKeys())) {
            for (HeaderKey key : serviceConfig.getKeys()) {
                headerMap.put(key.getKeyName(), key.getKeyValue());
            }
        }
        headerMap.put("sApplicationId" ,refId);

        String inputJson = JsonUtil.ObjectToString(retriveFileData);
        try {
            String response = new HttpTransportationService().postData(url, inputJson, headerMap, MediaType.APPLICATION_JSON_VALUE);
            PerfiosResponse apiResponse = JsonUtil.StringToObject(response, PerfiosResponse.class);
            if(apiResponse.getPayload() != null){
                retriveFileData.setData(apiResponse.getPayload());
                retriveFileData.setStatus(Constant.SUCCESS);
            }else{
                retriveFileData.setData(apiResponse);
                logger.debug("The Perfios Transaction referred to by the Client has not been completed for given ackId {} ", retriveFileData.getAcknowledgeId());
                retriveFileData.setStatus(Constant.FAILED);
            }
        } catch (IOException e) {
            logger.error("{}",e.getStackTrace());
        }
        return retriveFileData;
    }

    public static HashMap<String, String> setSSLHeader(WFJobCommDomain serviceConfig, String refId) {
        HashMap<String, String> headerMap = new HashMap<>();
        introduceKeyValue(serviceConfig.getKeys(), refId);
        if (CollectionUtils.isNotEmpty(serviceConfig.getKeys())) {
            for (HeaderKey key : serviceConfig.getKeys()) {
                headerMap.put(key.getKeyName(), key.getKeyValue());
            }
        }

        logger.info("{} : SSL header : {}", refId, headerMap);
        return headerMap;
    }

    private static void introduceKeyValue(List<HeaderKey> keys, String refId) {
        for (HeaderKey key : keys) {
            if (AdroitHelper.getHeaderKeys().contains(key.getKeyName())) {
                if (StringUtils.equalsIgnoreCase(key.getKeyName(), AdroitHelper.HEADER_APPLICATIONID)) {
                    key.setKeyValue(refId);
                }
            }
        }

    }
}
