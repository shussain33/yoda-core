/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 3, 2016 10:29:06 AM
 */
package com.softcell.workflow.executors.extapi;

import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.exceptions.category.GoNoGoException;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.creditVidya.GetUserProfileCallLog;
import com.softcell.gonogo.model.creditVidya.UserProfileRequest;
import com.softcell.gonogo.model.creditVidya.UserProfileResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.model.saathi.SaathiResponse;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.WorkFlowCommunicationManager;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.utils.TransportUtils;
import com.softcell.workflow.component.module.ModuleSetting;
import java.util.Arrays;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

/**
 * @author kishorp
 */
public class CreditVidyaApiExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(CreditVidyaApiExecutor.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private GetUserProfileCallLog apiLog;

    private ModuleSetting moduleSetting;
    private ModuleRequestRepository moduleRequestRepository;
    private ExternalAPILogRepository externalAPILogRepository;
    private WorkFlowCommunicationManager workFlowCommunicationManager;

    /**
     * @param goNoGoCustomerApplication
     * @param moduleSetting
     * @return
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {

        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        // Check Saatghi's response
        String status = Status.FAIL.toString();
        if( isSaathiStatus() ) {
            status = Status.PASS.toString();
        }

        logger.info("RefID {} : Saathi Status {}", this.goNoGoCustomerApplication.getApplicationRequest().getRefID(),
                this.goNoGoCustomerApplication.getApplicantComponentResponse().getSaathiResponse());
        logger.info("Credit Vidya processing is signalled as {} because of Saathi status = {}", status, isSaathiStatus());

        return status;
    }

    private boolean isSaathiStatus() {
        boolean status = false;
        SaathiResponse saathiResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getSaathiResponse();
        if ( saathiResponse != null && saathiResponse.getCasedetails() != null
                && StringUtils.equalsIgnoreCase( saathiResponse.getCasedetails().getStatus(),
                                                    GNGWorkflowConstant.YES.toFaceValue() )
           ) {
            status = true;
        }
        return status;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.softcell.gonogo.workflow.actions.MetaAction#run()
     */
    @Override
    public void run() {
        if (!moduleSetting.isActive()) {
            if (goNoGoCustomerApplication.getReInitiateCount() == 0) {
                logger.info(Status.UNAUTHORISED.toString() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for " + moduleSetting.getModuleName());

                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.NOT_AUTHORIZED.toString());
                logger.error(Status.UNAUTHORISED.toString());
                return;
            }
            return;
        }
        // ---------------------
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());
        workFlowCommunicationManager = new WorkFlowCommunicationManagerImpl();

        // Activity log
        ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.CREDIT_VIDYA_API_CALL.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
        try {
            UserProfileResponse userProfileResponse = getCustomerData();
            goNoGoCustomerApplication.getApplicantComponentResponse().setCreditVidyaResponse(userProfileResponse);
            goNoGoCustomerApplication.getIntrimStatus().setCreditVidyaStatus(Status.COMPLETE.toString());
            setModuleOutcome(userProfileResponse);
            logger.info("Credit Vidya response : {}", userProfileResponse);
            externalAPILogRepository.saveCreditVidyaCallLog(apiLog);
            activityLog.setCustomMsg(userProfileResponse.getStatus());
        } catch (Exception e) {
            goNoGoCustomerApplication.getIntrimStatus().setCreditVidyaStatus(Status.ERROR.toString());
            setModuleOutcome(null);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(e.getMessage());
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());

        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
    }

    private void setModuleOutcome(UserProfileResponse creditVidyaResponse) {
        ModuleOutcome moduleOutcome = new ModuleOutcome();

        moduleOutcome.setMessage("");
        moduleOutcome.setFieldName(ScoringDisplayName.CREDIT_VIDYA_STATUS);

        if( creditVidyaResponse != null) {
            moduleOutcome.setFieldValue(creditVidyaResponse.getStatus());
            if (creditVidyaResponse.getErrorMessage() != null ){
                moduleOutcome.setMessage(creditVidyaResponse.getErrorMessage());
            }
        } else {
            moduleOutcome.setFieldValue(GNGWorkflowConstant.NOT_AVAILABLE.toFaceValue());
        }
        moduleOutcome.setOrder(ScoringDisplayName.CREDIT_VIDYA_RESULT_ORDER);
        goNoGoCustomerApplication.getIntrimStatus().setCreditVidyaModuleResult(moduleOutcome);
    }
    /*
     * (non-Javadoc)
     *
     * @see com.softcell.gonogo.workflow.actions.MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }

    private UserProfileResponse getCustomerData() throws Exception {
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        if (CollectionUtils.isEmpty(applicationRequest.getRequest().getApplicant().getPhone()) ) {
            throw new GoNoGoException("Phone number not present");
        }

        String urlType = UrlType.CREDIT_VIDYA_GET_USER_PROFILE.toValue();
        WFJobCommDomain customerDataPullConfig = getServiceConfiguration(applicationRequest.getHeader().getInstitutionId(), urlType);

        //Build request for the connector call
        UserProfileRequest userProfileRequest = new UserProfileRequest();
        userProfileRequest.setLoginName(applicationRequest.getRequest().getApplicant().getPhone().get(0).getPhoneNumber());
        userProfileRequest.setHeaderKeyList(customerDataPullConfig.getKeys());

        String url = Arrays.asList(customerDataPullConfig.getBaseUrl(), customerDataPullConfig.getEndpoint())
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
        //Make the API call
        UserProfileResponse userProfileResponse = (UserProfileResponse) TransportUtils.postJsonRequest(userProfileRequest,
                url, UserProfileResponse.class);

        apiLog = new GetUserProfileCallLog();
        apiLog.setRefId(applicationRequest.getRefID());
        apiLog.setMobileNumber(userProfileRequest.getLoginName());
        apiLog.setResponse(userProfileResponse);

        return userProfileResponse;
    }

    private WFJobCommDomain getServiceConfiguration(String institutionId, String urlType) throws GoNoGoException {
        WFJobCommDomain customerDataPullConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                institutionId, urlType);

        if (null == customerDataPullConfig) {
            throw new GoNoGoException(String.format(ErrorCode.CONFIGURATION_NOT_FOUND
                    + " institution : %s , URL Type : %s", institutionId, urlType));
        }
        return customerDataPullConfig;
    }
}
