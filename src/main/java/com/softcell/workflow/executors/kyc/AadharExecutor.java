/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 4, 2016 12:29:46 PM
 */
package com.softcell.workflow.executors.kyc;

import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.aadhar.AadhaarRequest;
import com.softcell.gonogo.model.core.kyc.response.aadhar.AadharMainResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.AadharServiceCaller;
import com.softcell.gonogo.service.factory.AadharEntityBuilder;
import com.softcell.gonogo.service.factory.impl.AadharEntityBuilderImpl;
import com.softcell.gonogo.service.impl.AadharServiceCallerImpl;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

/**
 *
 * @author yogeshb
 *
 */
public class AadharExecutor extends MetaAction {

    private AadharEntityBuilder aadharEntityBuilder;

    private AadharServiceCaller serviceCaller;

    private ModuleRequestRepository moduleRequestRepository;

    private Logger logger = LoggerFactory.getLogger(AadharExecutor.class);

    private ModuleSetting moduleSetting;

    /**
     *
     * @param goNoGoCustomerApplication
     * @param moduleSetting
     * @return
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    /**
     * @see com.softcell.gonogo.workflow.actions.MetaAction#run()
     *
     */
    @Override
    public void run() {
        /*
		 * if module is not asked to re-process then just return the call.
		 */
        /*if (!moduleSetting.isActive() && goNoGoCustomerApplication.getReInitiateCount() > 0) {
            return;
        }*/
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());

        ModuleOutcome applicationAadharResult = new ModuleOutcome();
        applicationAadharResult.setFieldName(ScoringDisplayName.AADHAR_RESULT);
        applicationAadharResult.setOrder(ScoringDisplayName.AADHAR_RESULT_ORDER);

        goNoGoCustomerApplication.getIntrimStatus().setAadharModuleResult(applicationAadharResult);

       /* if (!moduleSetting.isActive()) {
            logger.info(Status.UNAUTHORISED.name() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for "+ moduleSetting.getModuleName());
            logger.info(Status.UNAUTHORISED.name());

            applicationAadharResult.setFieldValue(Status.NOT_AUTHORIZED.name());
            applicationAadharResult.setMessage(Status.NOT_AUTHORIZED.name());
            goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(Status.UNAUTHORISED.toString());

            return;
        }*/

        // Activity log
        ActivityLogs activityLog =  AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.AADHAAR.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        AadharMainResponse aadharResponse = null;

        try {
            aadharResponse = callAadharService(goNoGoCustomerApplication.getApplicationRequest());
        } catch (Exception e) {
            logger.error("Error while connecting aadhar service");
        }

        if( aadharResponse == null || StringUtils.equals(Constant.ERROR,   aadharResponse.getTxnStatus())){
            goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(
                    Status.COMPLETE.toString());
            applicationAadharResult.setFieldValue("No Response");
            applicationAadharResult.setMessage(Status.NO_RESPONSE
                    .toString());
            goNoGoCustomerApplication.getIntrimStatus()
                    .setAadharModuleResult(applicationAadharResult);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(applicationAadharResult.getMessage());
        } else {
            // TODO : CHECK
            if( goNoGoCustomerApplication.getApplicantComponentResponse() == null ){
                getGoNoGoCustomerApplication().setApplicantComponentResponse(new ComponentResponse());
            }
            goNoGoCustomerApplication.getApplicantComponentResponse().setAdharServiceResponse(aadharResponse);

            //if (!StringUtils.equals(Constant.ERROR,   aadharResponse.getTxnStatus())) {
                goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(
                        Status.VERIFIED.toString());

                applicationAadharResult.setFieldValue(aadharResponse
                        .getKycResponse().getAadharResponseDetails()
                        .getAadharResponse().getResult());
                applicationAadharResult.setMessage(aadharResponse
                        .getKycResponse().getAadharResponseDetails()
                        .getUidaiStatus());
                goNoGoCustomerApplication.getIntrimStatus()
                        .setAadharModuleResult(applicationAadharResult);
            /*} else {
                goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(
                        Status.COMPLETE.toString());
                applicationAadharResult.setFieldValue("No Response");
                applicationAadharResult.setMessage(Status.NO_RESPONSE
                        .toString());
                goNoGoCustomerApplication.getIntrimStatus()
                        .setAadharModuleResult(applicationAadharResult);
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                activityLog.setCustomMsg(applicationAadharResult.getMessage());
            }*/
        } /*else {
            goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(
                    Status.COMPLETE.toString());
            applicationAadharResult
                    .setMessage(Status.NO_RESPONSE.toString());
            applicationAadharResult.setFieldValue("No Response");
            goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(
                    Status.COMPLETE.toString());
            goNoGoCustomerApplication.getIntrimStatus().setAadharModuleResult(
                    applicationAadharResult);
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(applicationAadharResult.getMessage());
        }*/

        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());

        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
    }

    /**
     *
     * @param applicationRequest
     * @return
     * @throws Exception
     */
    private AadharMainResponse callAadharService( ApplicationRequest applicationRequest) throws Exception {

        if (applicationRequest != null) {

            aadharEntityBuilder = new AadharEntityBuilderImpl();

            AadhaarRequest aadhaarRequest = aadharEntityBuilder.buildAuthenticationRequest(applicationRequest);

            if (null != aadhaarRequest
                    && null != aadhaarRequest.getKycRequest()
                    .getAadharHolderDetails().getAadharNumber()) {

                moduleRequestRepository.saveAadharRequest(aadhaarRequest);

                logger.debug("Aadhaar request build and saved in database");

                serviceCaller = new AadharServiceCallerImpl();
                return serviceCaller.callAadharEkycService(aadhaarRequest,
                        applicationRequest.getHeader().getInstitutionId());

            } else {
                logger.error("Aadhaar request build failed");
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public String finishProcess() {
        return null;
    }

    @Override
    public GoNoGoCustomerApplication getGoNoGoCustomerApplication() {
        return goNoGoCustomerApplication;
    }

    @Override
    public void setGoNoGoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
    }
}
