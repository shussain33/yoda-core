/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 3, 2016 9:36:31 PM
 */
package com.softcell.workflow.executors.kyc;

import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.kyc.request.karza.KarzaAuthenticationRequest;
import com.softcell.gonogo.model.core.kyc.response.karza.KarzaClientResponse;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.service.KarzaServiceCaller;
import com.softcell.gonogo.service.impl.KarzaServiceCallerImpl;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * @author kishorp
 */
public class KarzaExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(KarzaExecutor.class);

    private ModuleSetting moduleSetting;

    private ModuleRequestRepository moduleRequestRepository;

    private ExternalAPILogRepository externalAPILogRepository;

    private KarzaServiceCaller karzaServiceCaller;

    private static final String APPLICANTID = "sApplID";

    private static final String CUSTOMERID = "customerId";


    /**
     * (non-Javadoc)
     *
     * @see MetaAction#process(Object,
     * ModuleSetting)
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    /**
     * (non-Javadoc)
     *
     * @see MetaAction#run()
     */
    @Override
    public void run() {

        externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());
        if(karzaServiceCaller == null) {
            karzaServiceCaller = new KarzaServiceCallerImpl(MongoConfig.getMongoTemplate());
        }
        logger.info("Karza executor calling karza service for {} in stage {}", goNoGoCustomerApplication.getGngRefId(),goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId());

        try{
            KarzaClientResponse karzaClientResponse = null;
            boolean executeForApplication = StringUtils.equalsIgnoreCase(this.executionBase, APPLICATION);
            boolean executeForApplicant = StringUtils.equalsIgnoreCase(this.executionBase, APPLICANT);

            // Fetch module outcome
            try {
                //Check configuration for APPLICANT
                if (executeForApplicant) {
                    logger.info("Karza is based on APPLICANT for refid {}", goNoGoCustomerApplication.getGngRefId());
                    callKarzaUtil();
                } else {
                    logger.info("Karza is based on APPLICATION for refid {}", goNoGoCustomerApplication.getGngRefId());


                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("run() has exception {}", e.getMessage());
            }
            moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());

            moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);

        }finally {
            // Since the call can be from service/standalone, need to check whether component manager has started this.
            if (componentManager != null) {
                synchronized (componentManager) {
                    componentManager.executionFinished(getActionId());
                    componentManager.notifyAll();
                }
            }
        }
    }

    /**
     * (non-Javadoc)
     *
     * @see MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }

    /**
     * Method to call the Karza Util to get all the Karza API responses.
     *
     * @throws Exception
     */
    private void callKarzaUtil() throws Exception {
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        if (applicationRequest != null && applicationRequest.getRequest() != null
                && applicationRequest.getRequest().getApplicant() != null) {
            final Applicant applicant = applicationRequest.getRequest().getApplicant();
            if (CollectionUtils.isNotEmpty(applicant.getKyc())) {
                final Map<Applicant, List<KarzaAuthenticationRequest>> karzaAuthenticationRequestMap =
                        karzaServiceCaller.buildKarzaAuthenticationRequest(applicationRequest,
                                this.getKycConfig(), this.isExecuteForCoApplicant());
                if (karzaAuthenticationRequestMap.size() != 0) {
                    final String instId = applicationRequest.getHeader().getInstitutionId();
                    final String product = applicationRequest.getHeader().getProduct().name();
                    karzaServiceCaller.buildKarzaResponse(instId,
                            applicationRequest.getRefID(),
                            product, karzaAuthenticationRequestMap, this.getKycConfig(),
                            this.isExecuteForCoApplicant(),
                            applicationRequest.getHeader().getLoggedInUserId());
                }
            }
        }
    }

}

