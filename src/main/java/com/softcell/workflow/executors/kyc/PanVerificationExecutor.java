package com.softcell.workflow.executors.kyc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.softcell.config.PanCommunicationDomain;
import com.softcell.config.PanServiceConfiguration;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.Name;
import com.softcell.gonogo.model.core.kyc.request.pan.PanRequest;
import com.softcell.gonogo.model.core.kyc.response.PanServiceResponse;
import com.softcell.gonogo.model.core.kyc.response.pan.PanInfo;
import com.softcell.gonogo.model.core.kyc.response.pan.PanResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.InterimStatus;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.service.factory.PanEntityBuilder;
import com.softcell.gonogo.service.factory.impl.PanEntityBuilderImpl;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.NameMatchEngine;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author kishorp
 */
public class PanVerificationExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(PanVerificationExecutor.class);

    private ModuleSetting moduleSetting;

    private ModuleRequestRepository moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());


    /**
     * @param goNoGoCustomerApplication
     * @param moduleSetting
     * @return
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;

        return Status.PASS.name();
    }

    @Override
    public void run() {

        ModuleOutcome moduleOutcome = new ModuleOutcome();
        moduleOutcome.setFieldName(ScoringDisplayName.PAN_RESULT);
        moduleOutcome.setOrder(ScoringDisplayName.PAN_RESULT_ORDER);
        if (goNoGoCustomerApplication.getIntrimStatus() != null) {
            goNoGoCustomerApplication.getIntrimStatus().setPanModuleResult(
                    moduleOutcome);
        } else {
            InterimStatus interimStatus = new InterimStatus();
            goNoGoCustomerApplication.setIntrimStatus(interimStatus);
            goNoGoCustomerApplication.getIntrimStatus().setPanModuleResult(
                    moduleOutcome);
        }

        /**
         * Add scoring values
         */
        if( goNoGoCustomerApplication.getApplScoreVector() != null) {
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
        }

        /**
         * Check module is active or not
         */
        /*if (!moduleSetting.isActive()) {
            moduleOutcome.setFieldValue(Status.NOT_AUTHORIZED.name());

            goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.NOT_AUTHORIZED.name());

            logger.info(Status.NOT_AUTHORIZED.name() + " to "
                    + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                    + " for " + moduleSetting.getModuleName());
            return;
        }
*/
        // Activity log
        ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.PAN.toFaceValue());

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        PanCommunicationDomain panCommunicationDomain = PanServiceConfiguration
                .getCreditial(goNoGoCustomerApplication
                        .getApplicationRequest().getHeader().getInstitutionId());

        logger.info("PanVerificationExecutor calling pan service started");

        PanResponse applicantPanResponse = callPanService(panCommunicationDomain);

        // TODO : CHECK
        if( goNoGoCustomerApplication.getApplicantComponentResponse() == null ){
            getGoNoGoCustomerApplication().setApplicantComponentResponse(new ComponentResponse());
        }
        goNoGoCustomerApplication.getApplicantComponentResponse().setPanServiceResponse(applicantPanResponse);

        logger.info("PanVerificationExecutor calling pan service completed");


        /* call pan service for co-applicant if allowed, by default false or not allowed    */
        if (executeForCoApplicant) {
            callCoApplicantPanService(panCommunicationDomain);
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());

        /* E for existing and valid, N Not exist, F fake pan */
        goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.IN_PROCESS.name());

        try {

            if (applicantPanResponse != null) {

                logger.info("PanVerificationExecutor getting pan response successfully.");

                if (null != applicantPanResponse.getKycResponse()
                        && !applicantPanResponse.getKycResponse().getPanResponseDetails().getNsdlStatus().equals(Status.ERROR)) {

                    PanInfo panInfo = applicantPanResponse.getKycResponse()
                            .getPanResponseDetails().getPanInfo();
                    activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
                    if (null != panInfo && panInfo.getPanStatus().equals(Status.E.name())) {

                        NameMatchEngine nameMatchEngine = new NameMatchEngine();

                        Name applicantName = goNoGoCustomerApplication
                                .getApplicationRequest().getRequest()
                                .getApplicant().getApplicantName();
                        String applicantNameFullName = GngUtils.stringToFirstMidlleLastName(
                                Arrays.asList(applicantName.getFirstName(), applicantName.getMiddleName(),
                                        applicantName.getLastName()));

                        /* Create fullName of pan service */
                        String panNameFullName = GngUtils.stringToFirstMidlleLastName(
                                Arrays.asList(panInfo.getFirstName(),
                                        panInfo.getMiddleName(),
                                        panInfo.getLastName()));

                        /* check name is same as pan return name */
                        float matchResult = nameMatchEngine.match(
                                panNameFullName.toString(),
                                applicantNameFullName.toString()) * 100;

                        moduleOutcome.setMessage(Status.EXIST.name());
                        moduleOutcome.setFieldValue(panNameFullName.toString());
                        moduleOutcome.setNameScore(CustomFormat.DF.format(matchResult));

                        goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.name());
                    } else {
                        if (null != applicantPanResponse.getKycResponse()
                                && applicantPanResponse.getKycResponse()
                                .getPanResponseDetails().getPanInfo()
                                .getPanStatus().equals(Status.F.name())) {

                            moduleOutcome.setMessage(Status.FAKE_PAN.name());
                            moduleOutcome.setFieldValue(FieldSeparator.HYPHEN); // if fake  pan
                            moduleOutcome.setNameScore(FieldSeparator.BLANK);

                            goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.name());
                        } else {
                            moduleOutcome.setMessage(Status.NOT_FOUND.name());
                            moduleOutcome.setFieldValue(FieldSeparator.HYPHEN);
                            moduleOutcome.setNameScore(FieldSeparator.BLANK);

                            goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.name());
                        }
                    }
                } else {
                    logger.warn("PanVerificationExecutor getting pan response with error.");

                    moduleOutcome.setFieldValue(Status.ERROR.name());
                    moduleOutcome.setMessage(applicantPanResponse.getKycResponse().getPanResponseDetails().getErrorList().get(0).getDescription());
                    goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.name());
                    activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
                }
            } else {
                logger.warn("PanVerificationExecutor not getting pan response.");

                moduleOutcome.setFieldValue(Status.NO_RESPONSE.name());
                moduleOutcome.setMessage(Status.NO_RESPONSE.toString());
                goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.name());
                activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            }
        } catch (Exception ex) {

            moduleOutcome.setFieldValue(Status.NO_RESPONSE.name());
            moduleOutcome.setMessage(Status.ERROR.name());
            goNoGoCustomerApplication.getIntrimStatus().setPanStatus(Status.COMPLETE.name());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            logger.error("PanVerificationExecutor raised Exception -> {} ", ex.getMessage());
        }

        /* Partial Pan response save in db.  */
        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        logger.debug("PanVerificationExecutor saved pan response in DB.");

        // Save activity log
        activityLog.setCustomMsg(moduleOutcome.getMessage());
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
    }

    /**
     * @param panCommunicationDomain
     * @return
     */
    private PanResponse callPanService(PanCommunicationDomain panCommunicationDomain) {
        PanResponse panResponse = null;
        /**
         * Building request to send pan service
         */
        PanEntityBuilder builder = new PanEntityBuilderImpl();
        PanRequest panRequest = builder.buildPanRequest(goNoGoCustomerApplication);
        if (null != panRequest && null != panCommunicationDomain) {
            panRequest.setGngRefId(goNoGoCustomerApplication.getGngRefId());
            moduleRequestRepository.savePanRequest(panRequest);
            panResponse = panService(panRequest, panCommunicationDomain);
        }
        return panResponse;

    }

    /**
     * @return
     */
    private void callCoApplicantPanService(PanCommunicationDomain panCommunicationDomain) {

        List<CoApplicant> applicants = goNoGoCustomerApplication
                .getApplicationRequest().getRequest().getCoApplicant();


        if (panCommunicationDomain == null && (applicants == null || applicants.isEmpty())) {
            logger.debug("Insufficient data available to call co-applicant pan service cause by" +
                    " null app or data not available for send .");
            return;
        }

        PanResponse panResponse;

        PanRequest panRequest;

        PanEntityBuilder builder = new PanEntityBuilderImpl();

        int count = 0;

        PanServiceResponse panServiceResponse;

        for (CoApplicant applicant : applicants) {

            applicant.setApplicantId(applicant.getApplicantId());

            panRequest = builder.buildPanRequest(goNoGoCustomerApplication);

            if (null != panRequest) {
                moduleRequestRepository.savePanRequest(panRequest);
                panResponse = panService(panRequest, panCommunicationDomain);
                if (null != panResponse) {
                    goNoGoCustomerApplication
                            .getApplicantComponentResponseList().get(count)
                            .setPanServiceResponse(panResponse);
                    panServiceResponse = new PanServiceResponse();
                    panServiceResponse.setRefId(goNoGoCustomerApplication
                            .getGngRefId());
                    panServiceResponse.setCoApplicantId(applicant
                            .getApplicantId());
                    panServiceResponse.setPanResponse(panResponse);
                    moduleRequestRepository.savePanResponse(panServiceResponse);
                }
            }
            count++;
        }

    }

    /**
     * @param panRequest
     * @param panCommunicationDomain
     * @return
     */
    private PanResponse panService(PanRequest panRequest,
                                   PanCommunicationDomain panCommunicationDomain) {
        ObjectMapper mapper = new ObjectMapper();
        PanResponse panResponse = null;
        try {
            final String panJsonRequest = mapper.writeValueAsString(panRequest);
            String url = panCommunicationDomain.getPanUrl();
            String tempResponse = new HttpTransportationService().postRequest(url, new HashMap<String, String>() {{
                put("INSTITUTION_ID", panCommunicationDomain.getInstitutionId());
                put("AGGREGATOR_ID", panCommunicationDomain.getAggregatorId());
                put("MEMBER_ID", panCommunicationDomain.getMemberId());
                put("PASSWORD", panCommunicationDomain.getPassword());
                put("inputJson_", panJsonRequest);
            }});
            if (StringUtils.isNotBlank(tempResponse)) {
                panResponse = mapper.readValue(tempResponse, PanResponse.class);
            }
        } catch (Exception e) {
            logger.error("Exception Occurred while send request to pan service by cause {}", e.getCause());
        }
        return panResponse;
    }

    /**
     * @return
     */
    @Override
    public String finishProcess() {
        return null;
    }

    @Override
    public GoNoGoCustomerApplication getGoNoGoCustomerApplication() {
        return goNoGoCustomerApplication;
    }

    @Override
    public void setGoNoGoCustomerApplication(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
    }
}
