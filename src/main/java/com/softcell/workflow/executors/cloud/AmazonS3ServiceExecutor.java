package com.softcell.workflow.executors.cloud;

import com.mongodb.gridfs.GridFSDBFile;
import com.softcell.config.AmazonS3Configuration;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.SalesForceMongoRepository;
import com.softcell.dao.mongodb.repository.SalesForceRepository;
import com.softcell.dao.mongodb.repository.UploadFileMongoRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadFileResponse;
import com.softcell.gonogo.model.cloudservice.AmazonS3UploadInputStreamRequest;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.UploadFileDetails;
import com.softcell.gonogo.model.core.kyc.response.ImagesDetails;
import com.softcell.gonogo.model.core.kyc.response.KycImageDetails;
import com.softcell.gonogo.model.salesforce.SalesForceInfo;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.service.AmazonS3CloudStorageService;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class AmazonS3ServiceExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(AmazonS3ServiceExecutor.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private ModuleSetting moduleSetting;

    private UploadFileRepository uploadFileRepository;
    private SalesForceRepository salesForceRepository;


    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {

        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;

        this.moduleSetting = moduleSetting;

        this.uploadFileRepository = new UploadFileMongoRepository(MongoConfig.getMongoTemplate(), MongoConfig.getGridFSTemplate());

        this.salesForceRepository = new SalesForceMongoRepository(MongoConfig.getMongoTemplate());

        return Status.PASS.toString();
    }

    @Override
    public void run() {
        /*
         * if module is not asked to re-process then just return the call.
		 */
        if (!moduleSetting.isActive() && goNoGoCustomerApplication.getReInitiateCount() > 0) {
            return;
        }
        if (!moduleSetting.isActive()) {
            logger.info(Status.UNAUTHORISED.toString()
                        + " to " + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for "+ moduleSetting.getModuleName());
            goNoGoCustomerApplication.getIntrimStatus().setAwsS3Status(Status.NOT_AUTHORIZED.toString());
            return;
        }
        goNoGoCustomerApplication.getIntrimStatus().setAwsS3Status(Status.IN_PROCESS.toString());

        executeUpload();
    }

    @Override
    public String finishProcess() {
        // TODO Auto-generated method stub
        return null;
    }

    private void executeUpload() {
        boolean isProfilePic = false;
        String refId = goNoGoCustomerApplication.getGngRefId();

        Map<String, AmazonS3Configuration> s3ConfigMap = Cache.URL_CONFIGURATION
                .getAmazonS3Configuration();

        AmazonS3Configuration s3Config = s3ConfigMap != null ? s3ConfigMap
                .get(goNoGoCustomerApplication.getApplicationRequest()
                        .getInstitutionId()) : null;

        SalesForceInfo salesForceInfo = salesForceRepository
                .getByApplicationRefId(refId);

        if (null != s3Config && null != salesForceInfo
                && null != salesForceInfo.getLeadId()) {

            AmazonS3CloudStorageService s3CloudService = new AmazonS3CloudStorageService(
                    s3Config.getAccessKey(), s3Config.getSecretKey());

            Set<String> refSet = new HashSet<String>();
            refSet.add(refId);
            List<KycImageDetails> kycImageDetails = this.uploadFileRepository
                    .getKycImageDetails(refSet, goNoGoCustomerApplication
                            .getApplicationRequest().getInstitutionId(), Status.IMAGES.name());

            for (KycImageDetails kycImageDetail : kycImageDetails) {
                String applicantId = kycImageDetail.getApplicantId();
                String contactId = salesForceInfo.getContactIdApplicantIdMap() != null ? salesForceInfo
                        .getContactIdApplicantIdMap().get(applicantId) : "image"
                        + new Random().nextInt(100);
                for (ImagesDetails imageDetail : kycImageDetail.getImageMap()) {
                    try {

                        GridFSDBFile gridFsDbFile = this.uploadFileRepository
                                .getById(imageDetail.getImageId(),
                                        goNoGoCustomerApplication
                                                .getApplicationRequest()
                                                .getHeader().getInstitutionId());

                        boolean isUploaded = uploadFileRepository
                                .isUploadedToAmazonS3(imageDetail.getImageId());
                        if (isUploaded == false) {

                            UploadFileDetails uploadDetails = new UploadFileDetails();
                            // While saving the image in the database image
                            // reason is mapped to filetype and now while
                            // retrieving filetype is mapped to image name and
                            // here for further methods we need object of
                            // uploddetails object.
                            uploadDetails.setFileType(imageDetail
                                    .getImageName());
                            uploadDetails.setFileExtension(imageDetail
                                    .getImageExtension());

                            // filename is retrieved in imagetype
                            uploadDetails.setFileName(imageDetail
                                    .getImageType());
							/*
							 * Target file name will be leadId/filename eg. lead
							 * id is 1004567 and filename is upload.jpg then
							 * target file name will be "1004567/upload.jpg"
							 */
                            // TODO: remove hardcoded .jpg below and add
                            // imageDetail.getExtension() once UI side code is
                            // changed.
                            String targetFileName = salesForceInfo.getLeadId()
                                    + "/"
                                    + contactId + "/" + uploadDetails.getFileType() + "/"
                                    + gridFsDbFile.getFilename() + ".jpg";

                            AmazonS3UploadInputStreamRequest uploadRequest = new AmazonS3UploadInputStreamRequest();

                            uploadRequest.mapObjectFromMongoGridFsFile(
                                    gridFsDbFile, s3Config.getBucketName(),
                                    targetFileName);

                            AmazonS3UploadFileResponse response = s3CloudService
                                    .uploadFile(uploadRequest);

                            if (response.getStatus() == com.softcell.gonogo.model.cloudservice.AmazonS3UploadFileResponse.Status.SUCCESS) {

                                uploadFileRepository.updateFileAmazonUrl(
                                        imageDetail.getImageId(),
                                        response.getS3Url());

                                goNoGoCustomerApplication.getIntrimStatus()
                                        .setAwsS3Status(
                                                Status.COMPLETE.toString());

                                String institutionId = goNoGoCustomerApplication
                                        .getApplicationRequest().getHeader()
                                        .getInstitutionId();

                                salesForceRepository
                                        .updateAmazonS3UrlInSalesForce(
                                                response.getS3Url(),
                                                salesForceInfo.getLeadId(),
                                                institutionId, isProfilePic,
                                                uploadDetails, salesForceInfo,
                                                applicantId);

                            } else {
                                logger.error(response.getErrorMessage());
                                goNoGoCustomerApplication
                                        .getIntrimStatus()
                                        .setAwsS3Status(Status.ERROR.toString());
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e.getCause());
                        goNoGoCustomerApplication.getIntrimStatus()
                                .setAwsS3Status(Status.FAILED.toString());
                    }
                }
            }
        }
    }
}
