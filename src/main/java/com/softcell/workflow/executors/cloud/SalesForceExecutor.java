package com.softcell.workflow.executors.cloud;

import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.SalesForceMongoRepository;
import com.softcell.dao.mongodb.repository.SalesForceRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.module.ModuleSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;

public class SalesForceExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(SalesForceExecutor.class);

    private static DecimalFormat df = new DecimalFormat("#");

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private ModuleSetting moduleSetting;

    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
        /*
         * if module is not asked to re-process then just return the call.
		 */
        if (!moduleSetting.isActive() && goNoGoCustomerApplication.getReInitiateCount() > 0) {
            return;
        }
        SalesForceRepository s = new SalesForceMongoRepository(MongoConfig.getMongoTemplate());
        s.connectSalesForce(goNoGoCustomerApplication);
    }

    @Override
    public String finishProcess() {
        // TODO Auto-generated method stub
        return null;
    }
}
