/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 3, 2016 10:29:06 AM
 */
package com.softcell.workflow.executors.multibureau;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.softcell.config.MultiBureauConfig;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogMongoRepositoryImpl;
import com.softcell.dao.mongodb.repository.serialnumber.ExternalAPILogRepository;
import com.softcell.gonogo.ModuleHelper;
import com.softcell.gonogo.ModuleManager;
import com.softcell.gonogo.model.core.Applicant;
import com.softcell.gonogo.model.core.CoApplicant;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.logger.RawResponseLog;
import com.softcell.gonogo.model.multibureau.MultiBureauCoApplicantResponse;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.FinishedInprocessObject;
import com.softcell.gonogo.model.multibureau.commercialCibil.pickup.OutputAckDomain;
import com.softcell.gonogo.model.multibureau.commercialCibil.requestResponseDomain.CommercialRequestResponseDomain;
import com.softcell.gonogo.model.multibureau.crifHighmark.INDVReportFile;
import com.softcell.gonogo.model.multibureau.experian.InProfileResponse;
import com.softcell.gonogo.model.multibureau.pickup.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.queue.management.Roles;
import com.softcell.gonogo.service.factory.MultiBureauRequestEntityBuilder;
import com.softcell.gonogo.service.factory.impl.MultiBureauRequestRequestEntityBuilderImpl;
import com.softcell.gonogo.service.impl.HttpTransportationService;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.service.thirdparty.utils.ThirdPartyIntegrationHelper;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.component.module.ModuleSetting;
import com.softcell.workflow.utils.MultiBureauUtility;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author kishorp
 */
public class MultiBureauExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(MultiBureauExecutor.class);

    private ModuleSetting moduleSetting;

    private ModuleRequestRepository moduleRequestRepository;

    private ExternalAPILogRepository externalAPILogRepository;

    // Permission to call Corporate MB
    // Commercial execution is restricted to manual trigger
    private boolean corpExecutionPermitted = false;

    private boolean consumerExecutionPermitted = true;

    private MultiBureauRequestEntityBuilder bureauEntityBuilder = new MultiBureauRequestRequestEntityBuilderImpl();

    private String refId;


    /**
     * @param goNoGoCustomerApplication
     * @param moduleSetting
     * @return
     */
    @Override
    public String process(Object goNoGoCustomerApplication,
                          ModuleSetting moduleSetting) {

        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        refId = this.goNoGoCustomerApplication.getApplicationRequest().getRefID();
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
        try {
            if(! executeMB() ) return;
            // FIX for age ; sometimes age is not set , set now to avoid error
            ModuleHelper.fixValues(goNoGoCustomerApplication.getApplicationRequest());

            refId = this.goNoGoCustomerApplication.getApplicationRequest().getRefID();
            logger.debug("MBExecutor started for refId {} ", refId);
            // Set up the variables
            moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
            externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());

            consumerExecutionPermitted = isConsumerExecutionPermitted(loggedInUserRole);

            logger.debug("Role {} is executing MB, has Consumer MB permission = {}, Corp MB permission = {}",
                    loggedInUserRole, consumerExecutionPermitted, corpExecutionPermitted);
            ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                    GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.MB.toFaceValue());
            StopWatch stopWatch = new StopWatch();

            String applicantType = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantType();

            // check for already executed
            if (! alreadyExecuted(true, null, applicantType)) {
                logger.debug("MB to be executed for primary applicant ({})", refId);
                // Call MB as per applicant type and the permissions for the logged in role
                if (isIndividual(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant())) {
                    logger.debug("Primary applicant is individual ({})", refId);
                    /* CPA role has no permission to execute consumer MB.*/
                    if (consumerExecutionPermitted) {
                        logger.debug("{} calling consumer mb for {} ...", loggedInUserRole,refId);
                        RequestJsonDomain requestJsonDomain = bureauEntityBuilder.buildInitialRequest(goNoGoCustomerApplication);
                        callConsumerMultiBureau(requestJsonDomain, activityLog, stopWatch);
                    }
                } else { // Non individual
                    /* FOS role has no permission to execute corporate MB. */
                    logger.debug("Primary applicant is Inindividual ({})", refId);
                    if (corpExecutionPermitted) {
                        logger.debug("{} calling corporate mb for {} ...", loggedInUserRole,refId);
                        CommercialRequestResponseDomain requestJsonDomain = bureauEntityBuilder.buildCorpRequest(goNoGoCustomerApplication.getApplicationRequest(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant());
                        callCorpMultiBureau(requestJsonDomain, activityLog, stopWatch);
                    }
                }
            }

            if (executeForCoApplicant) {
                logger.debug("Executing for coapplicants ({})...", refId);
                callCoApplicantMBService();
            }
            if(stopWatch.isRunning()) {
                stopWatch.stop();
                activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
            }
            logger.debug("MBExecutor COMPLETED for refId {}; saving in db", refId);
            moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
            // Save activity log
            logger.debug("Publishing activity from thread {} for MultibureauExecutor : refId {}",
                    Thread.currentThread().getName(), refId);
            GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
        } finally {
            // Since the call can be from service/standalone, need to check whether component manager has started this.
            if (componentManager != null) {
                synchronized (componentManager) {
                    componentManager.executionFinished(getActionId());
                    componentManager.notifyAll();
                }
            }
        }
    }

    private boolean executeMB() {
        boolean execute = true;
        /* For Aggregator  cases do not run MB
                "applicationRequest.header.applicationSource":"PAISABAZAR WEB"
                "applicationRequest.header.dsaId":"PL_WEBSITE"
        */

        if (Institute.isInstitute(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                Institute.SBFC )&&
                goNoGoCustomerApplication.getApplicationRequest().getAggregatorData() != null &&
                !goNoGoCustomerApplication.getApplicationRequest().getAggregatorData().isCibilHit()){
            return false ;
        }
        return execute;
    }


    public void runForApplicant(String applicantId)
    {
        //Correct invalid data of Component response for applicants
        ModuleManager moduleManager = new ModuleManager();
        moduleManager.removeInvalidComponantResponse(goNoGoCustomerApplication);

        refId = this.goNoGoCustomerApplication.getApplicationRequest().getRefID();
        // Set up the variables
        moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());
        externalAPILogRepository = new ExternalAPILogMongoRepositoryImpl(MongoConfig.getMongoTemplate());

        logger.debug("Role {} is executing MB, has Consumer MB permission = {}, Corp MB permission = {}",
                loggedInUserRole, consumerExecutionPermitted, corpExecutionPermitted);
        ActivityLogs activityLog = AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.MB.toFaceValue());
        StopWatch stopWatch = new StopWatch();

        logger.info("Received request for applicant {} ({})", applicantId, refId);
        // check whether the requested applicant is this - primary
        if( StringUtils.equalsIgnoreCase(applicantId,
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId())) {
            manualTriggerForPrimaryApplicant(activityLog, stopWatch);
        } else { // for co applicant
            manualTriggerForCoApplicant(applicantId, activityLog, stopWatch);
        }

        if(stopWatch.isRunning()) {
            stopWatch.stop();
            activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        }
        logger.debug("MBExecutor COMPLETED for refId {}; saving in db", refId);
        moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
        // Save activity log
        logger.debug("Publishing activity from thread {} for MultibureauExecutor : refId {}",
                Thread.currentThread().getName(), refId);
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
    }

    private void manualTriggerForCoApplicant(String applicantId, ActivityLogs activityLog, StopWatch stopWatch) {
        List<CoApplicant> applicants = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();
        List<ComponentResponse> applicantComponentResponseList = goNoGoCustomerApplication.getApplicantComponentResponseList();
        MbCommunicationDomain mbCommunicationDomain = MultiBureauConfig.getMbCommunicationDomain(goNoGoCustomerApplication.getApplicationRequest()
                .getHeader().getInstitutionId());
        ComponentResponse componentResponse = null;
        if (CollectionUtils.isNotEmpty(applicants)) {
            List<CoApplicant> coApplicantList = applicants.stream().filter(applicant -> StringUtils.equalsIgnoreCase(applicantId, applicant.getApplicantId()))
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(coApplicantList)) {
                CoApplicant coApplicant = coApplicantList.get(0);
                if (StringUtils.equalsIgnoreCase(applicantId, coApplicant.getApplicantId())) {
                    if (StringUtils.isEmpty(coApplicant.getApplicantType())) {
                        // TODO fill error message
                        logger.info("RefId {}, Applicant {} ({}) has no applicant type set !! Can not call MB service !!",
                                refId, coApplicant.getApplicantId(), coApplicant.getApplicantName());
                    } else {
                        boolean individual = isIndividual(coApplicant);
                        if( individual ){
                           if(coApplicant.getBureauHitCnt() <
                                    mbCommunicationDomain.getNumberOfReTry()){
                                componentResponse = callConsumerForCoApplicant(coApplicant, activityLog, stopWatch);
                                if(componentResponse != null && componentResponse.getMultiBureauJsonRespose() != null &&
                                        StringUtils.equalsIgnoreCase(componentResponse.getMultiBureauJsonRespose().getStatus(),
                                                Status.COMPLETED.name())){
                                        coApplicant.setBureauHitCnt(
                                        coApplicant.getBureauHitCnt()+ 1
                                        );
                                }
                            }else{
                               coApplicant.setBureauMsg("Bureau hit count "+coApplicant.getBureauHitCnt()+
                               "Limit Exceeded for Applicant " + coApplicant.getApplicantId());
                            }
                        } else { // Nonindividual
                            if(coApplicant.getBureauHitCnt() < mbCommunicationDomain.getNumberOfReTry()){
                                componentResponse = callCorpForCoApplicant(coApplicant, activityLog, stopWatch);
                                if(componentResponse != null && componentResponse.getMbCorpJsonResponse() != null &&
                                        StringUtils.equalsIgnoreCase(componentResponse.getMbCorpJsonResponse().getStatus(),
                                Status.COMPLETED.name())){
                                coApplicant.setBureauHitCnt(coApplicant.getBureauHitCnt()+ 1);
                                }
                            }else{
                                coApplicant.setBureauMsg("Bureau hit count  "+coApplicant.getBureauHitCnt()+
                                "Limit Exceeded for Applicant " + coApplicant.getApplicantId());
                            }
                        }
                        if( componentResponse != null){
                            if( applicantComponentResponseList == null) {
                                applicantComponentResponseList = new ArrayList<>();
                                goNoGoCustomerApplication.setApplicantComponentResponseList(applicantComponentResponseList);
                                applicantComponentResponseList.add(componentResponse);
                            } else {
                                List<ComponentResponse> coAppCompList = applicantComponentResponseList.stream().filter(compResponse ->
                                            StringUtils.equalsIgnoreCase(compResponse.getApplicantId(),applicantId))
                                            .collect(Collectors.toList());
                                // replace the response if already available
                                if(CollectionUtils.isNotEmpty(coAppCompList)) {
                                    // replace
                                    ComponentResponse previousResponse = coAppCompList.get(0);
                                    if( individual ) {
                                        previousResponse.setMultiBureauJsonRespose(componentResponse.getMultiBureauJsonRespose());
                                        previousResponse.setMbCorpJsonResponse(null);
                                    } else {
                                        previousResponse.setMbCorpJsonResponse(componentResponse.getMbCorpJsonResponse());
                                        previousResponse.setMultiBureauJsonRespose(null);
                                    }
                                } else {
                                    applicantComponentResponseList.add(componentResponse);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private ComponentResponse callConsumerForCoApplicant(CoApplicant applicant, ActivityLogs activityLog, StopWatch stopWatch) {
        ComponentResponse componentResponse = null;
        ApplicationRequest applicationRequest = goNoGoCustomerApplication.getApplicationRequest();
        RequestJsonDomain requestJsonDomain = bureauEntityBuilder.buildCoApplicantInitialRequest(applicationRequest, applicant);
        logger.debug("Consumer MB request for coapplicant {} = {}", applicant.getApplicantId(), requestJsonDomain);

        if( stopWatch != null ) stopWatch.start();
        if( activityLog != null ) activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());

        if (null != requestJsonDomain) {
            // Save request
            moduleRequestRepository.saveBureauRequest(requestJsonDomain);
            // Call MB service
            logger.debug("Calling Consumer MB for coApplicant {} ...", applicant.getApplicantId());
            ResponseMultiJsonDomain coApplicantMultiBureauResponse = callMultiBureauService(requestJsonDomain);
            //logger.info("Recevied response {}", coApplicantMultiBureauResponse);

            if (coApplicantMultiBureauResponse != null) {
                MultiBureauCoApplicantResponse coApplicantResponse = new MultiBureauCoApplicantResponse();
                coApplicantResponse.setApplicantRefId(refId);
                coApplicantResponse.setCoApplicantRefId(applicant.getApplicantId());
                coApplicantResponse.setMultiBureauJsonRespose(coApplicantMultiBureauResponse);
                moduleRequestRepository.saveCoApplicantMBResponse(coApplicantResponse);

                // Set outcome in GonogoCustomerApplication
                componentResponse = new ComponentResponse();
                componentResponse.setReferenceId(refId);
                componentResponse.setApplicantId(applicant.getApplicantId());
                componentResponse.setMultiBureauJsonRespose(coApplicantMultiBureauResponse);
            }
        }
        return componentResponse;
    }

    private void manualTriggerForPrimaryApplicant(ActivityLogs activityLog, StopWatch stopWatch) {
        //  Call MB without  restriction of MAX count
        MbCommunicationDomain mbCommunicationDomain = MultiBureauConfig.getMbCommunicationDomain(goNoGoCustomerApplication.getApplicationRequest()
                .getHeader().getInstitutionId());
        if (isIndividual(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant())) {
            logger.debug("MANUAL TRIGGER - Individual Applicant ( ref id {} ), {} calling consumer mb ...",
                    refId, loggedInUserRole);

            RequestJsonDomain requestJsonDomain = bureauEntityBuilder.buildInitialRequest(goNoGoCustomerApplication);
            //check if consumerMb response exist for applicantId.
            if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getBureauHitCnt() <
                    mbCommunicationDomain.getNumberOfReTry()){
                callConsumerMultiBureau(requestJsonDomain, activityLog, stopWatch);
                if(goNoGoCustomerApplication.getApplicantComponentResponse() != null &&
                 goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose()!= null &&
                 StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose().getStatus(),
                  Status.COMPLETED.name()))
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setBureauHitCnt(
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getBureauHitCnt()+ 1
                );
            }else{
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setBureauMsg(
                 "Bureau hit count  "+ goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getBureauHitCnt()+
                 "Limit Exceeded for Applicant " + goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId());
            }
        } else {
            //  call commercial MB
            logger.debug("MANUAL TRIGGER Non-individual Applicant ( ref id {} ), {} calling corporate mb ...",
                    refId, loggedInUserRole);
            CommercialRequestResponseDomain requestJsonDomain = bureauEntityBuilder.buildCorpRequest(goNoGoCustomerApplication.getApplicationRequest(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant());
            if(goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getBureauHitCnt() <
                    mbCommunicationDomain.getNumberOfReTry()){
                callCorpMultiBureau(requestJsonDomain, activityLog, stopWatch);
                if( goNoGoCustomerApplication.getApplicantComponentResponse() != null &&
                 goNoGoCustomerApplication.getApplicantComponentResponse().getMbCorpJsonResponse() != null &&
                 StringUtils.equalsIgnoreCase(goNoGoCustomerApplication.getApplicantComponentResponse().getMbCorpJsonResponse().getStatus(),
                 Status.COMPLETED.name()))
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setBureauHitCnt(
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getBureauHitCnt()+ 1
                );
            }else{
                goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().setBureauMsg(
                "Bureau hit count  "+ goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getBureauHitCnt()+
                "Limit Exceeded for Applicant " + goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getApplicantId());
            }
        }
    }

    private boolean alreadyExecuted(boolean isPrimary, String applicantId,String applicantType) {
        boolean alreadyExecuted = false;
        ComponentResponse applicantComponentResponse;
        if (isPrimary) {
            applicantComponentResponse = goNoGoCustomerApplication.getApplicantComponentResponse();
            if (applicantComponentResponse != null) {
                if(isIndividual(applicantType)) {
                    if (applicantComponentResponse.getMultiBureauJsonRespose() != null
                            && StringUtils.equalsIgnoreCase(applicantComponentResponse.getMultiBureauJsonRespose().getStatus(), Status.COMPLETED.name())) {
                        alreadyExecuted = true;
                    }
                } else if (applicantComponentResponse.getMbCorpJsonResponse() != null
                        && StringUtils.equalsIgnoreCase(applicantComponentResponse.getMbCorpJsonResponse().getStatus(), Status.COMPLETED.name())) {
                    alreadyExecuted = true;
                }
            }
            logger.debug("MB already executed for primary = {}", alreadyExecuted);
        } else {
            List<ComponentResponse> applicantComponentResponseList = goNoGoCustomerApplication.getApplicantComponentResponseList();
            if (CollectionUtils.isNotEmpty(applicantComponentResponseList)) {
                List<ComponentResponse> coApplicantResponse = applicantComponentResponseList.stream()
                        .filter(response -> StringUtils.equalsIgnoreCase(response.getApplicantId(), applicantId))
                        .collect(Collectors.toList());
                if (!coApplicantResponse.isEmpty()) {
                    // There is only single element in the filtered list, so get the first element
                    applicantComponentResponse = coApplicantResponse.get(0);
                    if(isIndividual(applicantType)) {
                        if (applicantComponentResponse.getMultiBureauJsonRespose() != null
                                && StringUtils.equalsIgnoreCase(applicantComponentResponse.getMultiBureauJsonRespose().getStatus(), Status.COMPLETED.name())) {
                            alreadyExecuted = true;
                        }
                    } else if (applicantComponentResponse.getMbCorpJsonResponse() != null
                            && StringUtils.equalsIgnoreCase(applicantComponentResponse.getMbCorpJsonResponse().getStatus(), Status.COMPLETED.name())) {
                        alreadyExecuted = true;
                    }
                }
            }
            logger.debug("MB already executed for coapplicant {} = {}", applicantId, alreadyExecuted);
        }

        return alreadyExecuted;
    }

    private void callConsumerMultiBureau(RequestJsonDomain requestJsonDomain, ActivityLogs activityLog, StopWatch stopWatch) {
        /* Create RequestJsonDomain object. */

        logger.debug("MBExecutor request formed as {}", requestJsonDomain);

        stopWatch.start();
        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
        if (requestJsonDomain != null && requestJsonDomain.getRequest() != null) {
            requestJsonDomain.setGngRefId(goNoGoCustomerApplication.getGngRefId());
            moduleRequestRepository.saveBureauRequest(requestJsonDomain);

            ResponseMultiJsonDomain applicantMultiBureauResponse = callMultiBureauService(requestJsonDomain);
            logger.debug("MBExecutor called consumer MB and received response as {}", applicantMultiBureauResponse);
            goNoGoCustomerApplication.getApplicantComponentResponse().setMultiBureauJsonRespose(applicantMultiBureauResponse);
            // Set Corp response to null if present ; This scenario happens when applicant type is changed
            goNoGoCustomerApplication.getApplicantComponentResponse().setMbCorpJsonResponse(null);
            goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.COMPLETE.toString());
            updateMbStatus(applicantMultiBureauResponse, goNoGoCustomerApplication);
            activityLog.setCustomMsg(goNoGoCustomerApplication.getIntrimStatus().getCibilModuleResult().getMessage());
        } else { // No request formed !!
            logger.debug("MBExecutor received improper/null request.");
            ModuleOutcome moduleOutcome = setModuleOutcome(GNGWorkflowConstant.CIBIL);
            ModuleOutcome experianModuleOutcome = setModuleOutcome(GNGWorkflowConstant.EXPERIAN);
            ModuleOutcome highmarkModuleOutcome = setModuleOutcome(GNGWorkflowConstant.HIGHMARK);
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
            goNoGoCustomerApplication.getApplScoreVector().add(experianModuleOutcome);
            goNoGoCustomerApplication.getApplScoreVector().add(highmarkModuleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setCibilModuleResult(moduleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setExperianModuleResult(experianModuleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setChmModuleResult(highmarkModuleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.toString());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(moduleOutcome.getMessage());
        }
    }

    private void callCorpMultiBureau(CommercialRequestResponseDomain requestJsonDomain, ActivityLogs activityLog,
                                     StopWatch stopWatch) {
        stopWatch.start();
        activityLog.setStatus(GNGWorkflowConstant.SUCCESS.toFaceValue());
        if (requestJsonDomain != null && requestJsonDomain.getRequest() != null) {
            requestJsonDomain.setGngRefId(goNoGoCustomerApplication.getGngRefId());
            moduleRequestRepository.saveComercialBureauRequest(requestJsonDomain);

            OutputAckDomain outputAckDomain = callCorpMultiBureauService(requestJsonDomain);
            logger.debug("MBExecutor called corporate MB and received response as {}", outputAckDomain);
            goNoGoCustomerApplication.getApplicantComponentResponse().setMbCorpJsonResponse(outputAckDomain);
            // Set Consumer response to null if present ; This scenario happens when applicant type is changed
            goNoGoCustomerApplication.getApplicantComponentResponse().setMultiBureauJsonRespose(null);
            goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.COMPLETE.toString());
            updateMbCorpStatus(outputAckDomain, goNoGoCustomerApplication);
            activityLog.setCustomMsg(goNoGoCustomerApplication.getIntrimStatus().getCibilModuleResult().getMessage());
        } else { // No request formed !!
            ModuleOutcome moduleOutcome = setModuleOutcome(GNGWorkflowConstant.CIBIL);
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setCibilModuleResult(moduleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.toString());
            activityLog.setStatus(GNGWorkflowConstant.FAILED.toFaceValue());
            activityLog.setCustomMsg(moduleOutcome.getMessage());
        }
    }

    private void callCoApplicantMBService() {
        List<CoApplicant> applicants = goNoGoCustomerApplication.getApplicationRequest().getRequest().getCoApplicant();

        List<ComponentResponse> coApplicantComponentResponseList =
                goNoGoCustomerApplication.getApplicantComponentResponseList();
        if (CollectionUtils.isEmpty(coApplicantComponentResponseList)) {
            coApplicantComponentResponseList = new ArrayList<>();
        }
        ComponentResponse componentResponse = null;

        /* FOS role has no permission to execute corporate MB
                    In such case MB for individual coapplicants will be called.*/
        if (CollectionUtils.isNotEmpty(applicants)) {
            for (CoApplicant applicant : applicants) {
                if (StringUtils.isEmpty(applicant.getApplicantId()) || StringUtils.isEmpty(applicant.getApplicantType())) {
                    // TODO fill error message
                    logger.info("RefId : {}, CoApplicant {} ({}) has no applicant id or type set !! Can not call MB service !!",
                            refId, applicant.getApplicantId(), applicant.getApplicantName());
                } else {
                    if (!alreadyExecuted(false, applicant.getApplicantId(), applicant.getApplicantType())) {
                        // Check whether Individual applicant and call accordingly
                        boolean individual = isIndividual(applicant);
                        logger.debug("CoApplicant {} is individual -> {}", applicant.getApplicantId(), individual);
                        if (individual) {
                            if (consumerExecutionPermitted) {
                                logger.debug("Calling consumer mb for individual applicant {} ...", applicant.getApplicantId());
                                componentResponse = callConsumerForCoApplicant(applicant, null, null);
                                addToResponselist(componentResponse, coApplicantComponentResponseList );
                            }
                        } else{
                            if (corpExecutionPermitted) {
                                logger.debug("Calling corporate mb for non-individual applicant {} ...", applicant.getApplicantId());
                                componentResponse = callCorpForCoApplicant(applicant, null, null);
                                addToResponselist(componentResponse, coApplicantComponentResponseList );
                            }
                        }
                    } else {
                        logger.info("MB already executed for coApplicant with applicantId {}", applicant.getApplicantId());
                    }
                }
            }
        }
        if( CollectionUtils.isNotEmpty(coApplicantComponentResponseList) &&
                CollectionUtils.isEmpty(goNoGoCustomerApplication.getApplicantComponentResponseList())) {
            goNoGoCustomerApplication.setApplicantComponentResponseList(coApplicantComponentResponseList);
        }
    }

    private ComponentResponse callCorpForCoApplicant(CoApplicant applicant, ActivityLogs activityLog, StopWatch stopWatch) {
        ComponentResponse componentResponse = null;
        String gngId = goNoGoCustomerApplication.getGngRefId();
        if( stopWatch != null){
        stopWatch.start(); }
        CommercialRequestResponseDomain corpRequestJsonDomain = bureauEntityBuilder.buildCorpRequest(goNoGoCustomerApplication.getApplicationRequest(), goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant());
        logger.debug("Commercial MB request for coApplicant {} = {}  ", applicant.getApplicantId(), corpRequestJsonDomain);
        if (corpRequestJsonDomain != null) {
            // Save Bureau request
            moduleRequestRepository.saveComercialBureauRequest(corpRequestJsonDomain);
            // Call MB corp service.
            logger.debug("Calling commercial MB for coApplicant {} ", applicant.getApplicantId());
            OutputAckDomain outputAckDomain = callCorpMultiBureauService(corpRequestJsonDomain);
            logger.debug("Commercial MB response {} ", outputAckDomain);

            if (outputAckDomain != null) {
                MultiBureauCoApplicantResponse coApplicantResponse = new MultiBureauCoApplicantResponse();
                coApplicantResponse.setApplicantRefId(gngId);
                coApplicantResponse.setCoApplicantRefId(applicant.getApplicantId());
                coApplicantResponse.setMbCorpJsonResponse(outputAckDomain);
                moduleRequestRepository.saveCoApplicantMBResponse(coApplicantResponse);

                // Set outcome in GonogoCustomerApplication
                componentResponse = new ComponentResponse();
                componentResponse.setReferenceId(gngId);
                componentResponse.setApplicantId(applicant.getApplicantId());
                componentResponse.setMbCorpJsonResponse(outputAckDomain);

            }
        }
        return componentResponse;
    }

    private void updateMbStatus(ResponseMultiJsonDomain applicantMultiBureauResponse, GoNoGoCustomerApplication goNoGoCustomerApplication) {
        /**
         *   Update status of cibil response
         */
        ModuleOutcome moduleOutcome, experialModuleOutcome, highmarkModuleOutcome;

        moduleOutcome = setModuleOutcome(GNGWorkflowConstant.CIBIL);
        experialModuleOutcome = setModuleOutcome(GNGWorkflowConstant.EXPERIAN);
        highmarkModuleOutcome = setModuleOutcome(GNGWorkflowConstant.HIGHMARK);

        goNoGoCustomerApplication.getIntrimStatus().setCibilModuleResult(moduleOutcome);
        goNoGoCustomerApplication.getIntrimStatus().setExperianModuleResult(experialModuleOutcome);
        goNoGoCustomerApplication.getIntrimStatus().setChmModuleResult(highmarkModuleOutcome);

        if (applicantMultiBureauResponse != null) {
            List<Finished> finishedList = applicantMultiBureauResponse.getFinishedList();
            if (finishedList != null) {
                for (Finished finished : finishedList) {
                    if (finished.getBureau().equals(GNGWorkflowConstant.CIBIL.toFaceValue())) {
                        /**
                         * set status of cibil response
                         */
                        this.goNoGoCustomerApplication.getIntrimStatus().setCibilScore(Status.COMPLETE.toString());
                        moduleOutcome.setMessage(finished.getStatus());
                        String cibilObj = new Gson().toJson(finished.getResponseJsonObject());
                        CibilResponse cibilResponse = new Gson().fromJson(cibilObj, CibilResponse.class);
                        if (cibilResponse.getScoreList() != null && cibilResponse.getScoreList().size() > 0) {
                            moduleOutcome.setFieldValue(cibilResponse.getScoreList().get(0).getScore());
                        } else {
                            moduleOutcome.setFieldValue("-");
                        }
                    } else if (StringUtils.equals(GNGWorkflowConstant.EXPERIAN.toFaceValue(), finished.getBureau())) {
                        experialModuleOutcome.setMessage(finished.getStatus());
                        this.goNoGoCustomerApplication.getIntrimStatus().setExperianStatus(Status.COMPLETE.toString());
                        if (null != finished.getResponseJsonObject()) {
                            try {
                                ObjectMapper objectMapper = new ObjectMapper();
                                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                                InProfileResponse experianResponse = null;

                                String responseJsonObject = GngUtils.getBureauResponse(finished.getResponseJsonObject());
                                if (responseJsonObject != null) {
                                    experianResponse = objectMapper.readValue(responseJsonObject, InProfileResponse.class);

                                    if (null != experianResponse && null != experianResponse.getScore()) {
                                        experialModuleOutcome.setFieldValue(experianResponse.getScore().getBureauScore());
                                    } else {
                                        experialModuleOutcome.setFieldValue("-");
                                    }
                                } else {
                                    experialModuleOutcome.setFieldValue("-");
                                }
                            } catch (Exception e) {
                                logger.error("Error parsing experian response in multibureau executor cause {}", e.getMessage());
                            }
                        } else {
                            experialModuleOutcome.setFieldValue("-");
                        }
                    } else if (StringUtils.equals(GNGWorkflowConstant.HIGHMARK.toFaceValue(), finished.getBureau())) {
                        // for CRIF Highmark - yet to get response classes from Highmark
                        setValuesForHighMark(highmarkModuleOutcome, finished);
                    }
                }
            } else {
                experialModuleOutcome.setFieldValue("-");
                experialModuleOutcome.setMessage(Status.ERROR.toString());
                moduleOutcome.setFieldValue("-");
                moduleOutcome.setMessage(Status.ERROR.toString());
            }
        } else {
            experialModuleOutcome.setFieldValue("-");
            experialModuleOutcome.setMessage(Status.ERROR.toString());
            moduleOutcome.setMessage(Status.ERROR.toString());
            moduleOutcome.setFieldValue("-");
        }
    }

    void updateMbCorpStatus(OutputAckDomain outputAckDomain, GoNoGoCustomerApplication goNoGoCustomerApplication) {
        ModuleOutcome moduleOutcome;
        moduleOutcome = setModuleOutcome(GNGWorkflowConstant.CIBIL);
        goNoGoCustomerApplication.getIntrimStatus().setCibilModuleResult(moduleOutcome);
        if (outputAckDomain != null) {
            List<FinishedInprocessObject> finishedList = outputAckDomain.getFinishedList();
            if (finishedList != null) {
                for (FinishedInprocessObject finished : finishedList) {
                    if (finished.getBureauName().equals(GNGWorkflowConstant.CIBIL.toFaceValue())) {
                        this.goNoGoCustomerApplication.getIntrimStatus().setCibilScore(Status.COMPLETE.toString());
                        moduleOutcome.setMessage(finished.getStatus());
                        String cibilObj = new Gson().toJson(finished.getJsonObject());
                        CibilResponse cibilResponse = new Gson().fromJson(cibilObj, CibilResponse.class);
                        if (cibilResponse.getScoreList() != null && cibilResponse.getScoreList().size() > 0) {
                            moduleOutcome.setFieldValue(cibilResponse.getScoreList().get(0).getScore());
                        } else {
                            moduleOutcome.setFieldValue("-");
                        }
                    }
                }
            } else {
                moduleOutcome.setFieldValue("-");
                moduleOutcome.setMessage(Status.ERROR.toString());
            }
        } else {
            moduleOutcome.setMessage(Status.ERROR.toString());
            moduleOutcome.setFieldValue("-");
        }
    }

    private ModuleOutcome setModuleOutcome(GNGWorkflowConstant bureauName) {
        ModuleOutcome moduleOutcome = new ModuleOutcome();

        if (bureauName == GNGWorkflowConstant.CIBIL) {
            moduleOutcome.setFieldName(ScoringDisplayName.CIBIL_SCORE);
            moduleOutcome.setOrder(ScoringDisplayName.CIBIL_SCORE_ORDER);
        } else if (bureauName == GNGWorkflowConstant.EXPERIAN) {
            moduleOutcome.setFieldName(ScoringDisplayName.EXPERIAN_SCORE);
            moduleOutcome.setOrder(ScoringDisplayName.EXPERIAN_SCORE_ORDER);
        } else if (bureauName == GNGWorkflowConstant.HIGHMARK) {
            moduleOutcome.setFieldName(ScoringDisplayName.HIGHMARK_SCORE);
            moduleOutcome.setOrder(ScoringDisplayName.HIGHMARK_SCORE_ORDER);
        }
        moduleOutcome.setFieldValue("NO RESPONSE");
        moduleOutcome.setMessage("No Score");
        return moduleOutcome;
    }

    private void setValuesForHighMark(ModuleOutcome highMarkOutcome, Finished finished) {

        highMarkOutcome.setMessage(finished.getStatus());

        this.goNoGoCustomerApplication.getIntrimStatus().setHighmarkStatus(Status.COMPLETE.toString());
        if (null != finished.getResponseJsonObject()) {

            try {
                INDVReportFile highmarkResponse = null;
                String responseJsonObject = MultiBureauUtility.getBureauResponse(finished.getResponseJsonObject());
                if (StringUtils.isNotBlank(responseJsonObject)) {

                    if (null != highmarkResponse &&
                            null != highmarkResponse.getINDVReports() &&
                            null != highmarkResponse.getINDVReports().getINDVReport() &&
                            null != highmarkResponse.getINDVReports().getINDVReport().getScores() &&
                            CollectionUtils.isNotEmpty(highmarkResponse.getINDVReports().getINDVReport().getScores().getScoreList())
                            &&
                            null != highmarkResponse.getINDVReports().getINDVReport().getScores().getScoreList().get(0)) {
                        highMarkOutcome.setFieldValue(highmarkResponse.getINDVReports().getINDVReport()
                                .getScores().getScoreList().get(0).getScoreValue());
                    } else {
                        highMarkOutcome.setFieldValue("-");
                    }
                }
            } catch (Exception e) {
                logger.error("{}",e.getStackTrace());
                logger.error("Error parsing experian response in multibureau executor cause {}", e.getMessage());
            }
        } else {
            highMarkOutcome.setFieldValue("-");
        }
    }

    @Override
    public String finishProcess() {
        return null;
    }

    /* Decide permission for commercial bureau call for the logged in user. */
    private boolean isCorpExecutionPermitted(String loggedInUserRole) {
        boolean permission = true;
        permission = !StringUtils.equalsIgnoreCase(loggedInUserRole, Roles.Role.FOS.name()) ;
        // Add other roles as per requirement.
        return permission;
    }

    /* Decide permission for consumer bureau call for the logged in user. */
    private boolean isConsumerExecutionPermitted(String loggedInUserRole) {
        boolean permission = true;
        permission = !StringUtils.equalsIgnoreCase(loggedInUserRole, Roles.Role.CPA.name());
        // Add other roles as per requirement.
        return permission;
    }


    private String sendToCODExSService(String requestJson, String institutionId) {
        String tempResponse = null;
        try {
            MbCommunicationDomain mbCommunicationDomain = MultiBureauConfig.getMbCommunicationDomain(institutionId);
            tempResponse = new HttpTransportationService().postRequest(mbCommunicationDomain.getMbUrl(), getPostMap(requestJson, mbCommunicationDomain));
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred in calling mb service for cause {}", e.getMessage());
        }
        return tempResponse;
    }

    private Map<String,String> getPostMap(String requestJson, MbCommunicationDomain mbCommunicationDomain) {
        Map<String, String> postMap = new HashMap<String, String>() {{
            put("INSTITUTION_ID", mbCommunicationDomain.getInstitutionId());
            put("AGGREGATOR_ID", mbCommunicationDomain.getAggregatorId());
            put("MEMBER_ID", mbCommunicationDomain.getMemberId());
            put("PASSWORD", mbCommunicationDomain.getPassword());
            put("inputJson_", requestJson);
        }};
        return postMap;
    }

    private String sendToCODExSCorpService(String requestJson, String institutionId) {
        String tempResponse = null;
        try {
            MbCommunicationDomain mbCommunicationDomain = MultiBureauConfig.getMbCorpCommunicationDomain(institutionId);
            tempResponse = new HttpTransportationService().postRequest(mbCommunicationDomain.getMbUrl(), getPostMap(requestJson, mbCommunicationDomain));
        } catch (Exception e) {
            logger.error("{}",e.getStackTrace());
            logger.error("Error occurred in calling mb service for cause {}", e.getMessage());
        }
        return tempResponse;
    }

    /**
     * @param requestJsonDomain
     * @return
     */
    private ResponseMultiJsonDomain callMultiBureauService(RequestJsonDomain requestJsonDomain) {
        String requestJson = null;
        ResponseMultiJsonDomain multiJsonDomain = null;
        ObjectMapper mapper = new ObjectMapper();
        LookupService lookupService = new LookupServiceHandler();

        try {
            requestJson = mapper.writeValueAsString(requestJsonDomain);
        } catch (JsonGenerationException e) {
            logger.error("" + e);
        } catch (JsonMappingException e) {
            logger.error("" + e);
        } catch (IOException e) {
            logger.error("" + e);
        }

        logger.info("MBExecutor submitting consumer request for {} referenceId : {}",
                goNoGoCustomerApplication.getGngRefId(), requestJson);

        String ackJson = sendToCODExSService(requestJson,
                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
        logger.info("MBExecutor received ack response for consumer for {} referenceId : {}",
                goNoGoCustomerApplication.getGngRefId(), ackJson);

        if (lookupService.checkActionsAccess(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId(),
                ActionName.MB_RESPONSE_SAVE)) {
            saveRawResponse(ackJson, goNoGoCustomerApplication.getGngRefId());
        }

        AcknowledgmentDomain ackJsonDomain = null;
        if (StringUtils.isNotBlank(ackJson)) {
            try {
                ackJsonDomain = mapper.readValue(ackJson, AcknowledgmentDomain.class);
                logger.debug("MBExecutor converted ack response for consumer for {} referenceId into object",
                        goNoGoCustomerApplication.getGngRefId());

                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.IN_PROCESS.toString());

            } catch (JsonParseException e) {
                logger.error("" + e);
                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.toString());
            } catch (JsonMappingException e) {
                logger.error("" + e);
                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.toString());
            } catch (IOException e) {
                logger.error("" + e);
                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.toString());
            }
        } else {
            logger.warn("MultiBureauExecutor not getting response from MultiBureau  service for {} referenceId.", goNoGoCustomerApplication.getGngRefId());
        }

        if (ackJsonDomain != null) {
            if (StringUtils.equalsIgnoreCase(ackJsonDomain.getStatus(), "SUCCESS")) {
                IssueJsonDomain issueJsonDomain = bureauEntityBuilder.buildIssueRequest(ackJsonDomain);
                String issueJson = null;
                try {
                    issueJson = mapper.writeValueAsString(issueJsonDomain);
                } catch (JsonGenerationException e) {
                    logger.error("" + e);
                } catch (JsonMappingException e) {
                    logger.error("" + e);
                } catch (IOException e) {
                    logger.error("" + e);
                }

                if (StringUtils.isNotBlank(issueJson)) {
                    int i = 0;
                    while (i < 12) {
                        try {
                            logger.info("Re Trying MB {} time for {}"
                                    , i, goNoGoCustomerApplication.getGngRefId());

                            Thread.sleep(3 * 2000L);

                            logger.info("MultiBureauExecutor calling MultiBureau issue request");
                            String responseJson = sendToCODExSService(issueJson,
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());


                            if (lookupService.checkActionsAccess(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId(),
                                    ActionName.MB_RESPONSE_SAVE)) {

                                saveRawResponse(responseJson, goNoGoCustomerApplication.getGngRefId());
                            }

                            if (responseJson != null) {
                                logger.info("MultiBureauExecutor getting response from MultiBureau " +
                                                "issue request service for {} referenceId.",
                                        goNoGoCustomerApplication.getGngRefId());
                                try {

                                    ObjectMapper objectMapper = new ObjectMapper();

                                    multiJsonDomain = objectMapper.readValue(responseJson, ResponseMultiJsonDomain.class);

                                    if (multiJsonDomain != null && !StringUtils.equalsIgnoreCase(multiJsonDomain.getStatus(), "IN-PROCESS") && multiJsonDomain.getInprocessList() == null) {
                                        return multiJsonDomain;
                                    }
                                } catch (IOException e) {
                                    logger.error("" + e);
                                    goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.toString());

                                    return multiJsonDomain;
                                }
                            } else {
                                logger.warn("MultiBureauExecutor not getting response " +
                                                "from MultiBureau issue request service for {} referenceId."
                                        , goNoGoCustomerApplication.getGngRefId());
                            }
                            i++;
                        } catch (InterruptedException e) {
                            logger.error("{}",e.getStackTrace());
                            logger.error("" + e);
                            goNoGoCustomerApplication.getIntrimStatus()
                                    .setMbStatus(Status.ERROR.toString());
                        }
                    }
                }
            } else {

                /**
                 * Send error response back to client
                 */
                multiJsonDomain = new ResponseMultiJsonDomain();
                multiJsonDomain.setStatus("REJECTED");
                List<WarningAndError> errorList = ackJsonDomain.getErrors();
                if (errorList != null && !errorList.isEmpty()) {
                    multiJsonDomain.setErrors(errorList);
                }
            }

        }
        return multiJsonDomain;
    }

    private OutputAckDomain callCorpMultiBureauService(
            CommercialRequestResponseDomain commercialRequestResponseDomain) {
        String requestJson = null;
        OutputAckDomain outputAckDomain = null;
        ObjectMapper mapper = new ObjectMapper();
        LookupService lookupService = new LookupServiceHandler();

        try {
            requestJson = mapper.writeValueAsString(commercialRequestResponseDomain);
        } catch (JsonGenerationException e) {
            logger.error("" + e);
        } catch (JsonMappingException e) {
            logger.error("" + e);
        } catch (IOException e) {
            logger.error("" + e);
        }
        logger.info("MBExecutor submitting corporate request for {} referenceId : {}",
                goNoGoCustomerApplication.getGngRefId(), requestJson);
        String ackJson = sendToCODExSCorpService(requestJson,
                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
        logger.info("MBExecutor received ack response for corporate for {} referenceId : {}",
                goNoGoCustomerApplication.getGngRefId(), ackJson);

        saveRawResponse(ackJson, goNoGoCustomerApplication.getGngRefId());

        if (StringUtils.isNotBlank(ackJson)) {
            try {
                outputAckDomain = mapper.readValue(ackJson, OutputAckDomain.class);
                logger.debug("MBExecutor converted ack response for corporate for {} referenceId into object",
                        goNoGoCustomerApplication.getGngRefId());
                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.IN_PROCESS.toString());
            } catch (JsonParseException e) {
                logger.error("" + e);
                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(
                        Status.ERROR.toString());
            } catch (JsonMappingException e) {
                logger.error("" + e);
                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(
                        Status.ERROR.toString());
            } catch (IOException e) {
                logger.error("" + e);
                goNoGoCustomerApplication.getIntrimStatus().setMbStatus(
                        Status.ERROR.toString());
            }
        } else {
            logger.warn("MultiBureauExecutor not getting response from MultiBureau  service for {} referenceId.", goNoGoCustomerApplication.getGngRefId());
        }
        if (outputAckDomain != null) {
            if (StringUtils.equalsIgnoreCase(outputAckDomain.getStatus(), "SUCCESS")) {

                IssueJsonDomain issueJsonDomain = bureauEntityBuilder.buildCorpIssueRequest(outputAckDomain);
                String issueJson = null;
                try {
                    issueJson = mapper.writeValueAsString(issueJsonDomain);
                } catch (JsonGenerationException e) {
                    logger.error("" + e);
                } catch (JsonMappingException e) {
                    logger.error("" + e);
                } catch (IOException e) {
                    logger.error("" + e);
                }
                if (StringUtils.isNotBlank(issueJson)) {
                    int i = 0;
                    while (i < 12) {
                        try {
                            logger.info("Retrying MB {} time for corp result for {}"
                                    , i, goNoGoCustomerApplication.getGngRefId());
                            Thread.sleep(3 * 2000L);
                            logger.info("MultiBureauExecutor calling MultiBureau issue request");
                            String responseJson = sendToCODExSCorpService(issueJson,
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId());
                            if (lookupService.checkActionsAccess(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId(),
                                    ActionName.MB_RESPONSE_SAVE)) {
                                saveRawResponse(responseJson, goNoGoCustomerApplication.getGngRefId());
                            }
                            if (responseJson != null) {
                                logger.info("MultiBureauExecutor getting response from MultiBureau " +
                                                "issue request service for {} referenceId.",
                                        goNoGoCustomerApplication.getGngRefId());
                                try {
                                    ObjectMapper objectMapper = new ObjectMapper();
                                    outputAckDomain = objectMapper.readValue(responseJson, OutputAckDomain.class);
                                    if (outputAckDomain != null && !StringUtils.equalsIgnoreCase(outputAckDomain.getStatus(), "IN-PROCESS") && outputAckDomain.getInprocessList() == null) {
                                        return outputAckDomain;
                                    }
                                } catch (IOException e) {
                                    logger.error("" + e);
                                    goNoGoCustomerApplication.getIntrimStatus().setMbStatus(Status.ERROR.toString());
                                    return outputAckDomain;
                                }
                            } else {
                                logger.warn("MultiBureauExecutor not getting response " +
                                                "from MultiBureau issue request service for {} referenceId."
                                        , goNoGoCustomerApplication.getGngRefId());
                            }
                            i++;
                        } catch (InterruptedException e) {
                            logger.error("" + e);
                            goNoGoCustomerApplication.getIntrimStatus()
                                    .setMbStatus(Status.ERROR.toString());
                        }
                    }
                }
            } else {
                /**
                 * Send error response back to client
                 */
                //outputAckDomain.setStatus("REJECTED");
                /*List<ErrorWarning> errorList = outputAckDomain.getErrors();
                if (errorList != null && !errorList.isEmpty()) {
                    outputAckDomain.setErrors(errorList);
                }*/
            }
        }
        return outputAckDomain;
    }

    private void saveRawResponse(String rawResponse, String refId) {

        logger.debug("Saving data for {}", ActionName.MB_RESPONSE_SAVE);
        RawResponseLog rawResponseLog = RawResponseLog.builder().rawResponse(rawResponse)
                .refId(refId)
                .responseType(ActionName.MB_RESPONSE_SAVE).build();

        externalAPILogRepository.saveRawResonseLog(rawResponseLog);
    }

    private boolean isIndividual(Applicant applicant) {
        if (ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicant.getApplicantType())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isIndividual(String applicantType) {
        if (ApplicantType.INDIVIDUAL.value().equalsIgnoreCase(applicantType)) {
            return true;
        } else {
            return false;
        }
    }

    private void addToResponselist(ComponentResponse componentResponse, List<ComponentResponse> coApplicantComponentResponseList) {
        List<ComponentResponse> coAppCompList = coApplicantComponentResponseList.stream().filter(compResponse ->
                StringUtils.equalsIgnoreCase(compResponse.getApplicantId(),componentResponse.getApplicantId()))
                .collect(Collectors.toList());
        // replace the response if already available
        if(CollectionUtils.isNotEmpty(coAppCompList)) {
            // replace
            ComponentResponse previousResponse = coAppCompList.get(0);

            previousResponse.setMultiBureauJsonRespose(componentResponse.getMultiBureauJsonRespose());
            previousResponse.setMbCorpJsonResponse(componentResponse.getMbCorpJsonResponse());
        } else {
            coApplicantComponentResponseList.add(componentResponse);
        }
    }

}
