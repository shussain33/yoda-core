package com.softcell.workflow.executors.multibureau;

import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ResponseConstants;
import com.softcell.gonogo.model.scoring.AddressScoringResult;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;

/**
 * core class for Scoring strings {this is Bi-operation so need two strings }
 */
public class AddressMatching {

    public static DecimalFormat df = new DecimalFormat("#.##");


    private static String cleanData(String rowData) {

        StringBuffer stdString = new StringBuffer();

        char[] dataByte = rowData.toCharArray();

        char old = 0;

        for (char singleByte : dataByte) {
            if (old == 32) {
                old = 0;
                continue;
            }
            if ((47 < singleByte && singleByte < 58)
                    || (64 < singleByte && singleByte < 91)
                    || (96 < singleByte && singleByte < 123)
                    || (singleByte == 32)
                    || (singleByte == 35)
                    || (singleByte == 45)
                    || (singleByte == 47)
                    || (singleByte == 92)) {

                stdString.append(singleByte);

                old = singleByte;
            }
        }
        return stdString.toString().toLowerCase();
    }

    public float fuzzyScore(String str1, String str2) {
        str1 = cleanData(str1);
        str2 = cleanData(str2);
        char[] str1Byte = str1.toCharArray();
        char[] str2Byte = str2.toCharArray();
        float hits = 0;
        float possible = str1Byte.length + str2Byte.length - 2;
        for (int i = 0; i < (str1Byte.length - 1); i++) {

            for (int j = 0; j < (str2Byte.length - 1); j++) {
                if (str1Byte[i] == str2Byte[j]
                        && str1Byte[i + 1] == str2Byte[j + 1]) {
                    hits++;
                    break;
                }
            }
        }
        for (int i = 0; i < (str2Byte.length - 1); i++) {

            String token1 = str2Byte[i] + "" + str2Byte[i + 1];

            for (int j = 0; j < (str1Byte.length - 1); j++) {
                String token2 = str1Byte[j] + "" + str1Byte[j + 1];
                if (token1.toLowerCase().equals(token2.toLowerCase())) {
                    hits++;
                    break;
                }
            }
        }

        float score = (hits * 100) / possible;
        score = Float.valueOf(df.format(score));

        return score;
    }

    public AddressScoringResult getScore(AddressScoringResult addScore){

        double maxScore = 0;

        double cibildays = 0;

        AddressScoringResult addressScoringResult = null;

        if (maxScore != addScore.getScore()) {

            maxScore = Math.max(maxScore, addScore.getScore());

            if (maxScore == addScore.getScore())
                    addressScoringResult = addScore;

        } else {

            if (cibildays < addScore.getCibilAddSubdateToDateDays()) {

                maxScore = addScore.getScore();

                if (maxScore == addScore.getScore())
                    addressScoringResult = addScore;
            }
        }

        return addressScoringResult;

    }

}