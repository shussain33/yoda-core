/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 2, 2016 5:57:08 PM
 */
package com.softcell.workflow.executors.multibureau;

import com.google.common.collect.Iterables;
import com.google.gson.Gson;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.ScoringDisplayName;
import com.softcell.constants.Status;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.scoring.CibilRespAccount;
import com.softcell.gonogo.model.core.request.scoring.CibilRespAddress;
import com.softcell.gonogo.model.core.request.scoring.CibilRespEnquiry;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.model.scoring.AddressScoringResult;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.utils.GngDateUtil;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author kishorp
 */
public class AddressScoreExecutor extends MetaAction {

    private static final Logger logger = LoggerFactory.getLogger(AddressScoreExecutor.class);
    private static HashMap<String, String> statemap;
    private static DecimalFormat df = new DecimalFormat("#.##");

    static {
        statemap = new HashMap<String, String>();
        statemap.put("01", "JAMMU & KASHMIR");
        statemap.put("02", "HIMACHAL PRADESH");
        statemap.put("03", "PUNJAB");
        statemap.put("04", "CHANDIGARH");
        statemap.put("05", "UTTARANCHAL");
        statemap.put("06", "HARYANA");
        statemap.put("07", "DELHI");
        statemap.put("08", "RAJASTHAN");
        statemap.put("09", "UTTAR PRADESH");
        statemap.put("10", "BIHAR");
        statemap.put("11", "SIKKIM");
        statemap.put("12", "ARUNACHAL PRADESH");
        statemap.put("13", "NAGALAND");
        statemap.put("14", "MANIPUR");
        statemap.put("15", "MIZORAM");
        statemap.put("16", "TRIPURA");
        statemap.put("17", "MEGHALAYA");
        statemap.put("18", "ASSAM");
        statemap.put("19", "WEST BENGAL");
        statemap.put("20", "JHARKHAND");
        statemap.put("21", "ORISSA");
        statemap.put("22", "CHHATTISGARH");
        statemap.put("23", "MADHYA PRADESH");
        statemap.put("24", "GUJARAT");
        statemap.put("25", "DAMAN & DIU");
        statemap.put("26", "DADRA & NAGAR HAVELI");
        statemap.put("27", "MAHARASHTRA");
        statemap.put("28", "ANDHRA PRADESH");
        statemap.put("29", "KARNATAKA");
        statemap.put("30", "GOA");
        statemap.put("31", "LAKSHADWEEP");
        statemap.put("32", "KERALA");
        statemap.put("33", "TAMIL NADU");
        statemap.put("34", "PONDICHERRY");
        statemap.put("35", "ANDAMAN & NICOBAR ISLANDS");
        statemap.put("36", "TELANGANA");
        statemap.put("99", "APO ADDRESS");
    }


    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private ModuleSetting moduleSetting;

    public static boolean getValue(Object a) {
        return false;
    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#process(java.lang.Object, com.softcell.workflow.component.module.ModuleSetting)
     */
    @Override
    public String process(Object bean, ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) bean;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#run()
     */
    @Override
    public void run() {
        /*
         * if module is not asked to re-process then just return the call.
		 */
        // TODO - restreucture the checks
        /*if (!moduleSetting.isActive() && goNoGoCustomerApplication.getReInitiateCount() > 0) {
            return;
        }
        if (!moduleSetting.isActive()) {
            goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.NOT_AUTHORIZED.toString());

            ModuleOutcome moduleOutcome = new ModuleOutcome();
            moduleOutcome.setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
            moduleOutcome.setFieldValue(Status.NOT_AUTHORIZED.toString());
            moduleOutcome.setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);

            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(moduleOutcome);
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

            moduleOutcome = new ModuleOutcome();
            moduleOutcome.setFieldName(ScoringDisplayName.OFFICE_ADDRESS);
            moduleOutcome.setFieldValue(Status.NOT_AUTHORIZED.toString());
            moduleOutcome.setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);

            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(moduleOutcome);
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

            logger.info(Status.NOT_AUTHORIZED.toString() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for " + moduleSetting.getModuleName());
            logger.error(Status.NOT_AUTHORIZED.toString());
            return;
        }*/

        //---------------------------
        if (!moduleSetting.isActive() ){
            if( goNoGoCustomerApplication.getReInitiateCount() == 0) {
                goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.NOT_AUTHORIZED.toString());

                ModuleOutcome moduleOutcome = new ModuleOutcome();
                moduleOutcome.setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
                moduleOutcome.setFieldValue(Status.NOT_AUTHORIZED.toString());
                moduleOutcome.setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);

                goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(moduleOutcome);
                goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

                moduleOutcome = new ModuleOutcome();
                moduleOutcome.setFieldName(ScoringDisplayName.OFFICE_ADDRESS);
                moduleOutcome.setFieldValue(Status.NOT_AUTHORIZED.toString());
                moduleOutcome.setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);

                goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(moduleOutcome);
                goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

                logger.info(Status.NOT_AUTHORIZED.toString() + " to "
                        + goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                        + " for " + moduleSetting.getModuleName());
                logger.error(Status.NOT_AUTHORIZED.toString());
                return;
            }
            return;
        }
        // Activity log
        ActivityLogs activityLog =  AuditHelper.createActivityLogForComponent(goNoGoCustomerApplication.getApplicationRequest(),
                GNGWorkflowConstant.BRE.toFaceValue(), GNGWorkflowConstant.ADDRESS_VERIFICATION.toFaceValue());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        getScore();
        goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.COMPLETE.toString());

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());

        activityLog.setCustomMsg(goNoGoCustomerApplication.getIntrimStatus().getResidenceAddressResult() != null
                ? goNoGoCustomerApplication.getIntrimStatus().getResidenceAddressResult().getMessage() : "Error");
        // Save activity log
        logger.debug(String.format("Publishing activity from thread %s", Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);
    }

    private String getScore() {
       ResponseMultiJsonDomain responseMultiJsonDomain = goNoGoCustomerApplication.getApplicantComponentResponse().getMultiBureauJsonRespose();
       if (responseMultiJsonDomain == null) {
            ModuleOutcome moduleOutcome = new ModuleOutcome();
            moduleOutcome.setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
            moduleOutcome.setFieldValue("");
            moduleOutcome.setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);
            moduleOutcome.setMessage("Not Match");
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(moduleOutcome);

            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
            moduleOutcome = new ModuleOutcome();
            moduleOutcome.setFieldName(ScoringDisplayName.OFFICE_ADDRESS);
            moduleOutcome.setFieldValue("");
            moduleOutcome.setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);
            moduleOutcome.setMessage("Not Match");
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(moduleOutcome);
            return "No Match";

        } else if (StringUtils.equals(Status.COMPLETED.name(),responseMultiJsonDomain.getStatus())) {
            goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.VERIFIED.toString());
            List<Finished> finishedList = responseMultiJsonDomain
                    .getFinishedList();
            if (finishedList != null) {
                for (Finished finished : finishedList) {


                    if (finished.getBureau().equals("CIBIL")) {

                        String cibilObj = new Gson().toJson(finished
                                .getResponseJsonObject());

                        CibilResponse cibilResponse = new Gson().fromJson(cibilObj,
                                CibilResponse.class);

                        int numberOfEnquiries = 0;
                        List<CibilRespEnquiry> enquiriesList = cibilResponse.getEnquiryList();

                        if (enquiriesList != null && !enquiriesList.isEmpty()) {
                            numberOfEnquiries = enquiriesList.size();
                        }

                        List<CibilRespAccount> accountList = cibilResponse.getAccountList();

                        if (accountList != null && !accountList.isEmpty()) {
                            numberOfEnquiries = numberOfEnquiries + accountList.size();
                        }
                        List<CibilRespAddress> cibilRespAddressList = cibilResponse.getAddressList();
                        List<CustomerAddress> addressList = goNoGoCustomerApplication
                                .getApplicationRequest().getRequest()
                                .getApplicant().getAddress();
                        AddressMatching addressMatching = new AddressMatching();
                        List<AddressScoringResult> addressScoringResultList = new ArrayList<AddressScoringResult>();
                        DateTime cibileProcessDate = null;
                        if (cibilResponse.getHeader() != null) {
                            if (cibilResponse.getHeader().getDateProceed() != null) {
                                try {
                                    cibileProcessDate = GngDateUtil.getDate(cibilResponse.getHeader().getDateProceed());
                                } catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                            }
                        }

                        AddressScoringResult addressScoringResult;
                        ModuleOutcome moduleOutcome;
                        if (cibilRespAddressList != null && numberOfEnquiries > 0) {
                            for (CibilRespAddress cibilRespAddress : cibilRespAddressList) {
                                for (CustomerAddress applicantAdress : addressList) {

                                    if (cibilRespAddress.getPinCode() != null)
                                        if (cibilRespAddress.getPinCode().trim()
                                                .equals(applicantAdress.getPin() + "")) {

                                            StringBuffer cibilAddress = new StringBuffer();

                                            if (cibilRespAddress
                                                    .getAddressLine1() != null) {
                                                cibilAddress
                                                        .append(cibilRespAddress
                                                                .getAddressLine1()).append(' ');
                                            }

                                            if (cibilRespAddress
                                                    .getAddressLine2() != null) {
                                                cibilAddress
                                                        .append(cibilRespAddress
                                                                .getAddressLine2()).append(' ');
                                                ;
                                            }

                                            if (cibilRespAddress
                                                    .getAddressLine3() != null) {
                                                cibilAddress
                                                        .append(cibilRespAddress
                                                                .getAddressLine3()).append(' ');
                                                ;
                                            }

                                            if (cibilRespAddress
                                                    .getAddressLine4() != null) {
                                                cibilAddress
                                                        .append(cibilRespAddress
                                                                .getAddressLine4()).append(' ');
                                                ;
                                            }

                                            if (cibilRespAddress
                                                    .getStateCode() != null) {
                                                String state = statemap.get(cibilRespAddress
                                                        .getStateCode());
                                                if (state != null)
                                                    cibilAddress
                                                            .append(state).append(' ');
                                                ;
                                            }

                                            /**
                                             * Applicant Address will match each cibil
                                             * address field
                                             */
                                            StringBuffer applicantAddress = new StringBuffer();

                                            if (applicantAdress
                                                    .getAddressLine1() != null) {
                                                applicantAddress
                                                        .append(applicantAdress
                                                                .getAddressLine1()).append(' ');
                                            }

                                            if (applicantAdress.getAddressLine2() != null) {
                                                applicantAddress.append(applicantAdress
                                                        .getAddressLine2()).append(' ');
                                            }

                                            if (applicantAdress.getLine3() != null) {
                                                applicantAddress.append(applicantAdress.getLine3()).append(' ');
                                            }

                                            if (applicantAdress.getLine4() != null) {
                                                applicantAddress.append(applicantAdress.getLine4()).append(' ');
                                            }
                                            if (applicantAdress.getCity() != null) {
                                                applicantAddress.append(applicantAdress.getCity()).append(' ');
                                            }

                                            if (applicantAdress.getDistrict() != null) {
                                                applicantAddress.append(applicantAdress.getDistrict()).append(' ');
                                            }

                                            if (applicantAdress.getState() != null) {
                                                applicantAddress.append(applicantAdress.getState()).append(' ');
                                            }

                                            float addressScore = addressMatching.fuzzyScore(
                                                    cibilAddress.toString(),
                                                    applicantAddress.toString());

                                            addressScoringResult = new AddressScoringResult();
                                            addressScoringResult.setAddType(applicantAdress.getAddressType());
                                            addressScoringResult.setGngRefNo(goNoGoCustomerApplication.getApplicationRequest().getRefID());
                                            addressScoringResult.setAdd1(cibilAddress.toString());
                                            addressScoringResult.setAdd2(applicantAddress.toString());
                                            addressScoringResult.setScore(addressScore);
                                            /**
                                             * Cibil  Address Stability Months
                                             */
                                            if (cibilRespAddress.getDateReported() != null && cibileProcessDate != null) {
                                                DateTime addressReportedDate;
                                                try {
                                                    addressReportedDate = GngDateUtil.getDate(cibilRespAddress.getDateReported());
                                                    Months month = Months.monthsBetween(addressReportedDate, cibileProcessDate);
                                                    addressScoringResult.setCibilAddSubdateToDateDays(month.getMonths());
                                                } catch (Exception exception) {
                                                    exception.printStackTrace();
                                                }

                                            }

                                            addressScoringResultList.add(addressScoringResult);

                                        }
                                }
                            }

                            double officeAddress = 0.0;
                            double residentialAddress = 0;
                            int offiStabilityOfAddress = 0;
                            int resiStabilityOfAddress = 0;

                            //** get address having maximum score and cibil days defferance in days. if two score are same.
                            if (!CollectionUtils.isEmpty(addressScoringResultList)) {

                                AddressScoringResult addressOfficeToScore = null;

                                AddressScoringResult lastOfficeAddr = Iterables.getLast(addressScoringResultList.parallelStream()
                                        .filter(offAddr ->
                                                StringUtils.equals(offAddr.getAddType(),
                                                        GNGWorkflowConstant.OFFICE.toFaceValue()))
                                        .collect(Collectors.toList()),null);


                                if (null != lastOfficeAddr) {
                                    //Get the latest address score which will be last record.
                                    addressOfficeToScore = addressMatching.getScore(lastOfficeAddr);

                                    officeAddress = addressOfficeToScore.getScore();

                                    offiStabilityOfAddress = addressOfficeToScore.getCibilAddSubdateToDateDays();
                                }


                                AddressScoringResult lastResidenceAddr = Iterables.getLast(addressScoringResultList.parallelStream()
                                        .filter(offAddr ->
                                                StringUtils.equals(offAddr.getAddType(),
                                                        GNGWorkflowConstant.RESIDENCE.toFaceValue()))
                                        .collect(Collectors.toList()), null);


                                if (null != lastResidenceAddr) {
                                    addressOfficeToScore = addressMatching.getScore(lastResidenceAddr);

                                    residentialAddress = addressOfficeToScore.getScore();

                                    resiStabilityOfAddress = addressOfficeToScore.getCibilAddSubdateToDateDays();
                                }

                            }

                            moduleOutcome = new ModuleOutcome();
                            moduleOutcome.setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
                            moduleOutcome.setFieldValue(df.format(residentialAddress));
                            moduleOutcome.setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);

                            if (Math.max(residentialAddress, officeAddress) > 70) {
                                moduleOutcome.setMessage("Match");
                            } else {
                                moduleOutcome.setMessage("Not Match");
                            }

                            moduleOutcome.setAddStability(resiStabilityOfAddress);

                            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

                            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(moduleOutcome);

                            moduleOutcome = new ModuleOutcome();

                            moduleOutcome.setFieldName(ScoringDisplayName.OFFICE_ADDRESS);

                            moduleOutcome.setFieldValue(df.format(officeAddress));

                            moduleOutcome.setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);

                            if (Math.max(residentialAddress, officeAddress) > 70) {
                                moduleOutcome.setMessage("Match");
                            } else {
                                moduleOutcome.setMessage("Not Match");
                            }

                            moduleOutcome.setAddStability(offiStabilityOfAddress);

                            goNoGoCustomerApplication.getApplScoreVector().add(
                                    moduleOutcome);
                            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(moduleOutcome);

                            goNoGoCustomerApplication.getApplicantComponentResponse().setAddressScoringResult(addressScoringResultList);


                            if (Math.max(residentialAddress, officeAddress) > 70) {
                                return "Match";
                            }

                        } else {
                            moduleOutcome = new ModuleOutcome();
                            moduleOutcome.setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
                            moduleOutcome.setFieldValue("");
                            moduleOutcome.setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);
                            moduleOutcome.setMessage("Not Match");
                            moduleOutcome.setAddStability(-1);

                            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
                            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(moduleOutcome);

                            moduleOutcome = new ModuleOutcome();
                            moduleOutcome.setFieldName(ScoringDisplayName.OFFICE_ADDRESS);
                            moduleOutcome.setFieldValue("");
                            moduleOutcome.setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);
                            moduleOutcome.setMessage("Not Match");
                            moduleOutcome.setAddStability(-1);

                            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
                            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(moduleOutcome);

                        }

                    }
                }
            } else {

                // Verification  scoring handle
                goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.CIBIL_NO_HIT.toString());


                ModuleOutcome moduleOutcome = new ModuleOutcome();
                moduleOutcome.setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
                moduleOutcome.setFieldValue("");
                moduleOutcome.setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);
                moduleOutcome.setMessage("Not Match");
                moduleOutcome.setAddStability(-1);

                goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
                goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(moduleOutcome);

                moduleOutcome = new ModuleOutcome();
                moduleOutcome.setFieldName(ScoringDisplayName.OFFICE_ADDRESS);
                moduleOutcome.setFieldValue("");
                moduleOutcome.setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);
                moduleOutcome.setMessage("Not Match");
                moduleOutcome.setAddStability(-1);
                goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
                goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(moduleOutcome);

            }

        } else {

            ModuleOutcome moduleOutcome = new ModuleOutcome();
            moduleOutcome.setFieldName(ScoringDisplayName.RESIDENTIAL_ADDRESS);
            moduleOutcome.setFieldValue("");
            moduleOutcome.setOrder(ScoringDisplayName.RESIDENTIAL_ADDRESS_ORDER);
            moduleOutcome.setMessage("Not Match");
            moduleOutcome.setAddStability(-1);
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setResidenceAddressResult(moduleOutcome);
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);

            moduleOutcome = new ModuleOutcome();
            moduleOutcome.setFieldName(ScoringDisplayName.OFFICE_ADDRESS);
            moduleOutcome.setFieldValue("");
            moduleOutcome.setOrder(ScoringDisplayName.OFFICE_ADDRESS_ORDER);
            moduleOutcome.setMessage("Not Match");
            moduleOutcome.setAddStability(-1);
            goNoGoCustomerApplication.getApplScoreVector().add(moduleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setOfficeModuleResult(moduleOutcome);
            goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(Status.COMPLETE.toString());


        }

        return "No Match";
    }

    /* (non-Javadoc)
     * @see com.softcell.gonogo.workflow.actions.MetaAction#finishProcess()
     */
    @Override
    public String finishProcess() {
        return null;
    }
}
