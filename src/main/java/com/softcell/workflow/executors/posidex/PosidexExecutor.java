package com.softcell.workflow.executors.posidex;

import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestMongoRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.request.posidex.PosidexRequestLog;
import com.softcell.gonogo.model.posidex.DedupeEnquiryRequest;
import com.softcell.gonogo.model.posidex.DedupeEnquiryResponse;
import com.softcell.gonogo.model.posidex.GetEnquiryRequest;
import com.softcell.gonogo.model.response.PosidexResponse;
import com.softcell.gonogo.service.factory.PosidexRequestBuilder;
import com.softcell.gonogo.service.factory.impl.PosidexRequestBuilderImpl;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.nextgen.constants.UrlType;
import com.softcell.nextgen.domain.WFJobCommDomain;
import com.softcell.service.impl.WorkFlowCommunicationManagerImpl;
import com.softcell.service.utils.TransportUtils;
import com.softcell.workflow.component.module.ModuleSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by yogeshb on 21/8/17.
 */
public class PosidexExecutor extends MetaAction {

    private final static Logger LOGGER = LoggerFactory.getLogger(PosidexExecutor.class);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    private ModuleSetting moduleSetting;

    private ModuleRequestRepository moduleRequestRepository = new ModuleRequestMongoRepository(MongoConfig.getMongoTemplate());

    @Override
    public String process(Object goNoGoCustomerApplication, ModuleSetting moduleSetting) {
        this.goNoGoCustomerApplication = (GoNoGoCustomerApplication) goNoGoCustomerApplication;
        this.moduleSetting = moduleSetting;
        return Status.PASS.toString();
    }

    @Override
    public void run() {
        /*if (!moduleSetting.isActive()) {
            if (goNoGoCustomerApplication.getReInitiateCount() == 0) {

                LOGGER.info("{} to {} for {}",
                        Status.UNAUTHORISED.toString(),
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                        moduleSetting.getModuleName());

                goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.NOT_AUTHORIZED.toString());

                return;
            }
            return;
        }*/
        try {
            PosidexRequestBuilder posidexRequestBuilder = new PosidexRequestBuilderImpl();

            WorkFlowCommunicationManagerImpl workFlowCommunicationManager = new WorkFlowCommunicationManagerImpl();

            WFJobCommDomain posidexConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), UrlType.POSIDEX_DO_ENQUIRY.toValue());

            if (null == posidexConfig) {
                return;
            }

            PosidexResponse posidexResponse = new PosidexResponse();

            PosidexRequestLog posidexRequestLog = new PosidexRequestLog();
            posidexRequestLog.setDate(new Date());
            posidexRequestLog.setRefId(goNoGoCustomerApplication.getGngRefId());
            /**
             * Initial Request to posidex
             */
            DedupeEnquiryRequest dedupeEnquiryRequest = posidexRequestBuilder.buildInitialRequest(goNoGoCustomerApplication, posidexConfig);

            posidexRequestLog.setDedupeDoEnquiryRequest(dedupeEnquiryRequest);

            String posidexFirstServiceUrl = Arrays.asList(posidexConfig.getBaseUrl(), posidexConfig.getEndpoint())
                    .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));


            DedupeEnquiryResponse dedupeEnquiryResponse = (DedupeEnquiryResponse) TransportUtils.postJsonRequest(dedupeEnquiryRequest, posidexFirstServiceUrl, DedupeEnquiryResponse.class);

            if (null != dedupeEnquiryResponse) {

                posidexResponse.setDedupeDoEnquiryResponse(dedupeEnquiryResponse);

                WFJobCommDomain posidexGetEnquiryStatusConfig = workFlowCommunicationManager.getWfCommDomainJobByType(
                        goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(), UrlType.POSIDEX_GET_ENQUIRY_STATUS.toValue());

                if (null == posidexGetEnquiryStatusConfig) {
                    return;
                }

                GetEnquiryRequest getEnquiryRequest = posidexRequestBuilder.buildIssueRequest(dedupeEnquiryRequest, posidexGetEnquiryStatusConfig);

                posidexRequestLog.setGetEnquiryStatusRequest(getEnquiryRequest);

                String posidexSecondServiceUrl = Arrays.asList(posidexGetEnquiryStatusConfig.getBaseUrl(), posidexGetEnquiryStatusConfig.getEndpoint())
                        .parallelStream().collect(Collectors.joining(FieldSeparator.BLANK));

                DedupeEnquiryResponse posidexGetStatusResponse;

                int retryCount = 0;
                do {
                    Thread.sleep(5000);
                    posidexGetStatusResponse = (DedupeEnquiryResponse) TransportUtils.postJsonRequest(getEnquiryRequest, posidexSecondServiceUrl, DedupeEnquiryResponse.class);
                    if (retryCount != 0) {
                        LOGGER.debug("Posidex retry attempt :->> [{}]", retryCount);
                    }
                    retryCount++;
                }
                while (retryCount <= posidexGetEnquiryStatusConfig.getNoOfRetry() && StringUtils.equals("ER", posidexGetStatusResponse.getStatusCode()) && StringUtils.equals("Enquiry is in progress, please try after sometime", posidexGetStatusResponse.getStatusMessage()));

                if (null != posidexGetStatusResponse) {

                    posidexResponse.setDedupeEnquiryStatusResponse(posidexGetStatusResponse);

                    goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.COMPLETE.name());

                } else {

                    goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.ERROR.name());
                }

            } else {

                goNoGoCustomerApplication.getIntrimStatus().setPosidexDedupeStatus(Status.ERROR.name());
            }


            goNoGoCustomerApplication.getApplicantComponentResponse().setPosidexResponse(posidexResponse);
        /* Partial Posidex response save in db.  */
            moduleRequestRepository.savePosidexRequest(posidexRequestLog);
            moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);

        } catch (Exception e) {
            LOGGER.error("Error occurred while executing posidex api with probable cause: [{}]", e.getMessage());
        } finally {
            synchronized (componentManager) {
                componentManager.executionFinished(getActionId());
                componentManager.notifyAll();
            }
        }
        LOGGER.debug("Finised execution of : " + this.getClass().getName());
    }

    @Override
    public String finishProcess() {
        return null;
    }
}
