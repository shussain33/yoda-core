package com.softcell.workflow;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * @author kishorp This Class is used to create unique application Id
 */
public class SequenceGenerator {
    private static DateTimeFormatter dtfOut = DateTimeFormat
            .forPattern("yyMMdd");

    public static String getApplicationId() {
        DateTime jodatime = new DateTime();
        SecureRandom random = new SecureRandom();
        String appId = dtfOut.print(jodatime)
                + new BigInteger(30, random).toString(32).toUpperCase();
        return appId;
    }

    public static String generateSequence(long num) {
        String var = String.format("%06d", num);
        return var;
    }


    public static String genrateSequenceForProspectNumber(int num) {
        String var = String.format("%05d", num);
        return var;
    }

    public static String genrateSequenceForNewProduct(long num) {
        String var = String.format("%010d", num);
        return var;
    }

    public static String getReportId() {
        DateTime jodatime = new DateTime();
        SecureRandom random = new SecureRandom();
        String appId = dtfOut.print(jodatime)
                + new BigInteger(30, random).toString(32).toUpperCase();
        return appId;
    }
}
