package com.softcell.workflow;

import com.softcell.constants.Constant;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.Acknowledgement;
import com.softcell.gonogo.model.response.core.AckHeader;
import com.softcell.gonogo.model.response.core.ErrorMessage;
import com.softcell.gonogo.model.response.core.Warnings;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * This file contains utility method for component
 *
 * @author prateek
 */
@Component
public class RequestAggregator {

    private static final Logger logger = LoggerFactory.getLogger(RequestAggregator.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    /**
     * @param applicationRequest
     * @return
     */
    public static GoNoGoCustomerApplication getGoNoGo(
            ApplicationRequest applicationRequest) {
        GoNoGoCustomerApplication customerApplication = new GoNoGoCustomerApplication();
        customerApplication.setApplicationRequest(applicationRequest);

        /**
         * Setter for multiproduct.
         */
        customerApplication.setGngRefId(applicationRequest.getRefID());
        customerApplication.setRootID(applicationRequest.getRefID());
        customerApplication.setParentID(applicationRequest.getRefID());
        customerApplication.setProductSequenceNumber(1);
        return customerApplication;
    }

    private Object getSequenceObject(ApplicationRequest applicationRequest) {
        /**
         * For TVS, need to check for branch code.
         If not present-> give error, else generate reference in format of
         Branch Code + Porfolio + Last(Branch code will be fetched from UAM + Portfolio will be fetched from Scheme Master + Last 5 digits incremental for every branch starting with 00001) :eg.3006CD00001
         */
        Object sequenceObject;
        String branchCode = applicationRequest.getHeader().getBranchCode();
        if (StringUtils.isNotBlank(branchCode)) {
            //FixME - CD is hardcoded for a while as per TVS Demo requirement.
            sequenceObject = StringUtils.join(branchCode, "CD",
                    applicationRepository.getSequenceForTvs(
                            applicationRequest.getHeader().getInstitutionId(),
                            applicationRequest.getHeader().getProduct(),
                            branchCode));
        } else {
            sequenceObject = getAcknowledgement(
                    applicationRequest,
                    Constant.ERROR,
                    Status.BUSINESS_VALIDATION_FAILED.name(),
                    ErrorCode.BLANK_BRANCH_CODE,
                    null, null);
        }
        return sequenceObject;
    }

    public static Acknowledgement getAcknowledgement(ApplicationRequest applicationRequest, String status,
                                                     String errorCode, String errorDesc, String warningCode, String warningDesc) {

        Acknowledgement ack = new Acknowledgement();
        AckHeader ackHeader = new AckHeader();


        if (null != applicationRequest
                && null != applicationRequest.getHeader()) {

            ackHeader.setApplicationId(applicationRequest.getHeader()
                    .getApplicationId());
            ackHeader.setInstitutionId(applicationRequest.getHeader()
                    .getInstitutionId());
        }

        ackHeader.setRequestReceivedTime(new Date());
        ackHeader.setResponseDateTime(new Date());
        ackHeader.setResponseType(MediaType.APPLICATION_JSON_VALUE);
        ack.setHeader(ackHeader);
        ack.setGonogoRefId(applicationRequest.getRefID());
        ack.setStatus(status);
        Warnings warnings = new Warnings();
        warnings.setCode(warningCode);
        warnings.setDescription(warningDesc);
        ack.setWarnings(warnings);
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.setCode(errorCode);
        errorMessage.setDescription(errorDesc);
        ack.setErrors(errorMessage);

        return ack;
    }

    public Object generateReferenceId(ApplicationRequest applicationRequest) {

        logger.debug("getGeneratedReferenceID service started ");

        Object sequenceObject;


        switch (applicationRequest.getHeader().getInstitutionId()) {
            case "4059":
            case "4016": {
                sequenceObject = getSequenceObject(applicationRequest);
            }
            break;

            case "4011" : // DMI
            case "3979" :
                sequenceObject = SequenceGenerator.getApplicationId();
                break;
            case "4075" : //sbfc
            case "4021" :
                sequenceObject = generateSequenceObject(applicationRequest);
                break;

            case "4084" : // JFVS
            case "4025" : // JFVS prod
                sequenceObject = generateSequenceObject(applicationRequest);
                break;
            case "4072" : // VOLC
                sequenceObject = generateSequenceObject(applicationRequest);
                break;

            case "4032" : // ABFL
                sequenceObject = generateSequenceObject(applicationRequest);
                break;

            case "4080" : // Five Star
                sequenceObject = generateSequenceObject(applicationRequest);
                break;

            case "4170" : //TRIC
            case "4071" : //TRIC prod
                sequenceObject = generateSequenceObject(applicationRequest);
                break;

            case "4160" : // Ambit
            case "4074" : //Ambit prod
                sequenceObject = generateSequenceObject(applicationRequest);
                break;


            default: {
                String dealerId = applicationRequest.getHeader().getDealerId();

                if (StringUtils.isNotBlank(dealerId)) {
                    /**
                     * This logic for cdl product based on dealer id.
                     * refId = dealerNumber+sequenceNumber;
                     */
                    logger.debug("getGeneratedReferenceID service generating refId for CDL dealer {}", dealerId);

                    sequenceObject = dealerId
                            + applicationRepository.getSequence(dealerId, applicationRequest
                            .getHeader().getInstitutionId());

                } else {
                    sequenceObject = generateSequenceObject(applicationRequest);
                }
            }
        }

        return sequenceObject;
    }

    private Object generateSequenceObject(ApplicationRequest applicationRequest) {
        Object sequenceObject; /**
         * This logic for other products excluding cdl.
         * refId = institutionId+productId+sequenceNumber;
         */
        if (StringUtils.isNotBlank(applicationRequest.getHeader().getInstitutionId())
                && StringUtils.isNotBlank(applicationRequest.getRequest().getApplication().getProductID())
                && StringUtils.isNotBlank(applicationRequest.getRequest().getApplication().getLoanType())) {

            String institutionID = applicationRequest.getHeader().getInstitutionId();

            String productCode = applicationRequest.getRequest().getApplication().getProductID();

            String productName = applicationRequest.getRequest().getApplication().getLoanType();

            if (applicationRepository.isProductExist(institutionID, productCode, productName)) {

                sequenceObject = institutionID
                        + productCode
                        + applicationRepository
                        .getSequenceForNewProducts(productCode,
                                institutionID);

            } else {

                sequenceObject = getAcknowledgement(
                        applicationRequest,
                        Constant.ERROR,
                        ErrorCode.INVALID_PRODUCT_CODE,
                        ErrorCode.INVALID_PRODUCT_DSCR,
                        null, null);

            }
        } else {

            sequenceObject = getAcknowledgement(
                    applicationRequest,
                    Constant.ERROR,
                    ErrorCode.INVALID_REQUEST_CODE,
                    ErrorCode.INVALID_REQUEST_PARAMS,
                    null, null);

        }
        return sequenceObject;
    }

}
