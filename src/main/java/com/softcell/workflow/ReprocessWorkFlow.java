/**
 * kishorp12:06:44 AM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow;

import com.softcell.aop.ApplicationProxy;
import com.softcell.constants.CacheConstant;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.FinalApplicationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.workflow.actions.ComponentAction;
import com.softcell.gonogo.workflow.actions.ComponentActionInterface;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.ComponentSetting;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author kishorp
 *
 */
public class ReprocessWorkFlow extends Thread {

    public static final Logger logger = LoggerFactory.getLogger(ReprocessWorkFlow.class);

    /* make sure GoNoGoCustomerApplication is not set null  before starting work flow*/
    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    /* make sure ComponentSetting is not set null before starting work flow*/
    private ComponentSetting componentSetting;


    // This flag is set when QDE SoBRE is run
    private boolean saveApplicationRequestFlag;

    /**
     *
     * @param goNoGoCustomerApplication
     * 		the application that need to be process through work flow.
     *      This application is either fresh or need to be process again.
     * @param componentSetting
     * 		A component setting for new application is default, which is
     *      configured in database against institution and product level.
     *      For re-process application this will defined by user or CRO.
     *      In re-process flow application may be execute either end-to-end
     *      or partially.
     *
     */
    public ReprocessWorkFlow(GoNoGoCustomerApplication goNoGoCustomerApplication, ComponentSetting componentSetting) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        this.componentSetting = componentSetting;
    }

    /**
     *
     * @param goNoGoCustomerApplication
     * 		the application that need to be process through work flow.
     *      This application is either fresh or need to be process again.
     * @param componentSetting
     * 		A component setting for new application is default, which is
     *      configured in database against institution and product level.
     *      For re-process application this will defined by user or CRO.
     *      In re-process flow application may be execute either end-to-end
     *      or partially.
     *
     */
    private void execute(GoNoGoCustomerApplication goNoGoCustomerApplication, ComponentSetting componentSetting) throws Exception {
        try {
            for (Entry<Integer, List<Component>> workFlowSteps : componentSetting
                    .getComponentMap().entrySet()) {
                List<Thread> taskList = new ArrayList<Thread>();
                scheduleNode(taskList, workFlowSteps);
                waitToCompleteFlow(taskList);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            saveApplication();
        }

    }

    /**
     * @param taskList
     *        the task list is scheduled in work flow.
     *        This will wait to complete all join task.
     */
    private void waitToCompleteFlow(List<Thread> taskList) {
        for (Thread thread : taskList) {
            try {
                thread.join();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Save method is call after completing each node of work flow.
     */
    private void saveApplication() throws Exception {
        FinalApplicationMongoRepository finalApplcation = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());
        FinalApplicationRepository finalApp = (FinalApplicationRepository) ApplicationProxy.getProxy(finalApplcation, FinalApplicationRepository.class);
        finalApp.saveFinalApplication(goNoGoCustomerApplication);
    }

    /**
     * @param taskList
     * 			The task list is schedule each task and start execute.
     * 			Each task should be an independent. Each task which
     * 			used in current task must complete before this work
     * 			node.
     * @param workFlowSteps
     * 			The work-flow step is set of task which need to execute
     * 			concurrently. Each member of task set is independent of
     * 			each others.
     */
    private void scheduleNode(List<Thread> taskList,
                              Entry<Integer, List<Component>> workFlowSteps) {

        for (Component component : workFlowSteps.getValue()) {

            ApplicationContext applicationContext = Cache.COMPONENT_FACTORY_BEAN.get(CacheConstant.BEAN_FACTORY);
            ComponentAction componentAction = (ComponentAction) applicationContext.getBean(component.getComponentId());
            componentAction.setComponent(component);
            componentAction.setGoNoGoCustomerApplication(goNoGoCustomerApplication);
            ComponentActionInterface componentActionInterface =
                    (ComponentActionInterface) ApplicationProxy.getProxy(componentAction,
                                                                         ComponentActionInterface.class);
            if( StringUtils.equalsIgnoreCase(componentActionInterface.process(goNoGoCustomerApplication, component) ,
                    Status.PASS.toString()) ) {
                Thread cActionTask = new Thread(componentActionInterface);
                cActionTask.start();
                taskList.add(cActionTask);
            }
        }
    }

    @Override
    public void run() {
        try {
            execute(goNoGoCustomerApplication, componentSetting);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public boolean isSaveApplicationRequestFlag() {
        return saveApplicationRequestFlag;
    }

    public void setSaveApplicationRequestFlag(boolean saveApplicationRequestFlag) {
        this.saveApplicationRequestFlag = saveApplicationRequestFlag;
    }

}
