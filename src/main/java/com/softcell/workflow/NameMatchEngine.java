/**
 * kishorp10:38:54 AM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow;

import org.apache.commons.codec.language.DoubleMetaphone;
import org.apache.commons.codec.language.Soundex;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.spell.JaroWinklerDistance;
import org.apache.lucene.search.spell.LevensteinDistance;
import org.apache.lucene.search.spell.NGramDistance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * @author kishorp
 */
public class NameMatchEngine {

    private static final Logger LOGGER = LoggerFactory.getLogger(NameMatchEngine.class);
    private LevensteinDistance levensteinDistance;


    private static AlignToken alignName(String name1, String name2) {
        AlignToken alignToken = new AlignToken();
        HashSet<String> name1Set = new HashSet<String>();
        HashSet<String> name2Set = new HashSet<String>();
        String[] name1Tokens = StringUtils.split(name1, ' ');
        String[] name2Tokens = StringUtils.split(name2, ' ');
        for (String token : name1Tokens) {
            if (token.trim().length() > 0)
                name1Set.add(token.trim().toLowerCase());
        }
        for (String token : name2Tokens) {
            if (token.trim().length() > 0)
                name2Set.add(token.trim().toLowerCase());
        }
        StringBuffer name1Buffer = new StringBuffer();
        StringBuffer name2Buffer = new StringBuffer();
        if (name1Set.size() > name2Set.size()) {
            for (String token : name1Set) {
                name1Buffer.append(token);
                if (name2Set.contains(token)) {
                    name2Buffer.append(token);
                    name2Set.remove(token);
                }
            }
            for (String token2 : name2Set) {
                name2Buffer.append(token2);
            }
        } else {

            for (String token : name2Set) {
                name1Buffer.append(token);
                if (name1Set.contains(token)) {
                    name2Buffer.append(token);
                    name1Set.remove(token);
                }
            }
            for (String token2 : name1Set) {
                name2Buffer.append(token2);
            }

        }
        alignToken.setName1(name1Buffer.toString());
        alignToken.setName2(name2Buffer.toString());
        return alignToken;

    }

    public float match(String name1, String name2) {
    	
        float ldScore =0F;
        if(null != name1 && null!=name2)     {


        String[] name1Tokens = StringUtils.splitPreserveAllTokens(name1.toLowerCase().trim(),
                ' ');
        String[] name2Tokens = StringUtils.split(name2.toLowerCase().trim(),
                ' ');

        Set<String> name1Set = new HashSet<String>();
        Set<String> name2Set = new HashSet<String>();

        for (int i = 0; i < name1Tokens.length; i++) {
        	if(name1Tokens[i].trim().length()>0)
            name1Set.add(name1Tokens[i]);
        }

        for (int j = 0; j < name2Tokens.length; j++) {
        	if(name2Tokens[j].trim().length()>0)
            name2Set.add(name2Tokens[j]);
        }
        if (name1Set.size() == name2Set.size())
            return  getLdMatch(name1Tokens, name2Tokens);
        else {
            int matchCount = 0;
            for (String token : name1Set) {
                if (name2Set.contains(token)) {
                    matchCount++;
                }
            }
            if (matchCount == name1Set.size()) {
                return getLdMatch(name1Tokens, name2Tokens);
            } else {
                float soundexScore = checkSoundexScore(name1Set, name2Set);
                float metaphone = checkMetaPhoneScore(name1Set, name2Set);
                // AlignToken aling = alignName(name1, name2);

                ldScore = getLdMatch(name1Tokens, name2Tokens);
                if (ldScore >= 0.75 && soundexScore == 1.0 && metaphone == 1.0) {
                    return ldScore;
                } else {
                    if (ldScore >= 0.90 && soundexScore == 1.0) {
                        return ldScore;
                    }
                    if (ldScore >= 0.90 && metaphone == 1.0) {
                        return ldScore;
                    }
                }
            }
        }
        }else {
            LOGGER.warn("name1 0r name2 should not be null.");
        }
         return ldScore;

    }

    private float getLdMatch(String[] name1Tokens, String[] name2Tokens) {
        float scoreValue = 0.0f;
        float finalScore = 0.0f;
        int tokenCount = 0;
        if (name1Tokens.length > name2Tokens.length) {
            for (String token1 : name1Tokens) {
                tokenCount++;
                float maxScore = 0.0f;
                for (String token2 : name2Tokens) {
                	 
                    scoreValue = scoreValue
                            + getLdMatch(token1.trim(), token2.trim());
                    scoreValue = getLdMatch(token1.trim(), token2.trim());
                    if (maxScore < scoreValue)
                        maxScore = scoreValue;
                    if (scoreValue == 1.0f)
                        break;
                }
                finalScore = finalScore + maxScore;
            }
        } else {

            for (String token2 : name2Tokens) {
                tokenCount++;
                float maxScore = 0.0f;
                for (String token1 : name1Tokens) {

                    scoreValue = getLdMatch(token1.trim(), token2.trim());
                    if (maxScore < scoreValue)
                        maxScore = scoreValue;
                    if (scoreValue == 1.0f)
                        break;

                }

                finalScore = finalScore + maxScore;

            }

        }
        return (finalScore / (tokenCount));
    }

    private float checkSoundexScore(Set<String> name1Set, Set<String> name2Set) {
        Soundex doubleMetaphone = new Soundex();
        Set<String> metaPhoneKey1 = new HashSet<>();
        Set<String> metaPhoneKey2 = new HashSet<>();
        for (String token : name1Set) {
            metaPhoneKey1.add(doubleMetaphone.soundex(token));
        }
        for (String token : name2Set) {
            metaPhoneKey2.add(doubleMetaphone.soundex(token));
        }
        if (metaPhoneKey2.size() == metaPhoneKey1.size())
            return 1.0f;
        return 0.0f;
    }

    private float checkMetaPhoneScore(Set<String> name1Set, Set<String> name2Set) {
        DoubleMetaphone doubleMetaphone = new DoubleMetaphone();
        Set<String> metaPhoneKey1 = new HashSet<>();
        Set<String> metaPhoneKey2 = new HashSet<>();
        for (String token : name1Set) {
            metaPhoneKey1.add(doubleMetaphone.doubleMetaphone(token));
        }
        for (String token : name2Set) {
            metaPhoneKey2.add(doubleMetaphone.doubleMetaphone(token));
        }
        if (metaPhoneKey2.size() == metaPhoneKey1.size())
            return 1.0f;
        return 0.0f;
    }

    private float checkMetaPhoneScore(String name1, String name2) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @param name1
     * @param name2
     */
    private float getLdMatch(String name1, String name2) {
        levensteinDistance = new LevensteinDistance();
        return levensteinDistance.getDistance(name1, name2);
    }

    private float getJaroWinklerDistanceScore(String name1, String name2) {
        JaroWinklerDistance jaroWinklerDistance = new JaroWinklerDistance();
        return jaroWinklerDistance.getDistance(name1, name2);
    }

    private float getNgramScore(String name1, String name2) {
        NGramDistance nGramDistance = new NGramDistance();
        return nGramDistance.getDistance(name1, name2);
    }

}

class AlignToken {
    private String name1;
    private String name2;

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    @Override
    public String toString() {
        return "AlignToken [name1=" + name1 + ", name2=" + name2 + "]";
    }
}