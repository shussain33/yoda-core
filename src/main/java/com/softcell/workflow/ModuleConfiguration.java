/**
 * kishorp1:28:00 PM  Copyright Softcell Technology
 **/
package com.softcell.workflow;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author kishorp
 *
 */
public class ModuleConfiguration {
    /*The module name is used to identify service to be call.*/
    @JsonProperty("sModuleName")
    private String moduleName;
    /*run flag is use to run module , true for run and false to skip.*/
    @JsonProperty("bRunModule")
    private boolean runModule;

    /**
     * @return the moduleName
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * @param moduleName the moduleName to set
     *        set module name that need to add work flow.
     */
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    /**
     * @return the runModule
     */
    public boolean isRunModule() {
        return runModule;
    }

    /**
     * @param runModule the runModule to set
     *        set flag true to run flow.
     */
    public void setRunModule(boolean runModule) {
        this.runModule = runModule;
    }

}
