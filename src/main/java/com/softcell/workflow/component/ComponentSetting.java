/**
 *
 */
package com.softcell.workflow.component;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author kishorp
 *         <p/>
 *         <pre>
 *         The component setting is use to define scope of services to  execute.
 *         <p> Each institution has only one work flow. which will be applicable to all  products.</p>
 *         </pre>
 */
public class ComponentSetting {
    /**
     * Name of institution
     */
    private String institutionName;
    /**
     * The map particular component and their setting for service executions.
     */
    private Map<Integer, List<Component>> componentMap = new TreeMap<Integer, List<Component>>();

    /**
     * Use to execute component thread
     */

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public Map<Integer, List<Component>> getComponentMap() {
        return componentMap;
    }

    public void setComponentMap(Map<Integer, List<Component>> componentMap) {
        this.componentMap = componentMap;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ComponentSetting [institutionName=");
        builder.append(institutionName);
        builder.append(", componentMap=");
        builder.append(componentMap);
        builder.append("]");
        return builder.toString();
    }

}
