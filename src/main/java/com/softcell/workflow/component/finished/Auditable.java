/**
 * kishor11:56:07 PM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow.component.finished;

import com.softcell.constants.AuditType;
import com.softcell.dao.mongodb.utils.MongoRepositoryUtil;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Field;

/**
 * @author kishor
 */
public interface Auditable extends Runnable {
    /**
     * @param auditData           Any Object which will modified after after this action
     *                            but need to save last state of an object. This will be
     *                            moved into another database.
     * @param auditCollectionName Collection name in which  data need to be save for
     *                            audit purpose.
     *                            this name is not same as original collection name.
     *                            In case of same collection name
     * @return true if data is successfully audit else false.
     */
    public boolean saveToAudit(AuditData auditData, String auditCollectionName);

    /**
     * @param auditObject Any Object which will modified after after this action
     *                    but need to save last state of an object. This will be
     *                    moved into another database.
     * @param description Audit description to track audit reason.
     */
    public void process(GoNoGoCroApplicationResponse auditObject, Object description, AuditType auditType);

    /**
     * @param auditObject Any Object which will modified after after this action
     *                    but need to save last state of an object. This will be
     *                    moved into another database.
     * @return OriginalCallectinName appended by Audit String.
     */
    default String getAuditCollectionName(Object auditObject) {

        String collectionName = auditObject.getClass().getSimpleName() + MongoRepositoryUtil.MongoGngUtilCollection._Audit;

        for (Field field : auditObject.getClass().getFields()) {

            Document document = field.getAnnotation(Document.class);

            field.setAccessible(true);

            if (document != null)
                return collectionName = document.collection() + MongoRepositoryUtil.MongoGngUtilCollection._Audit;
        }
        return collectionName;
    }


    void start();
}
