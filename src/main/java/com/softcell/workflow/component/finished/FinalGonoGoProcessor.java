/**
 * kishorp3:12:57 PM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow.component.finished;

import com.softcell.aop.ApplicationProxy;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.FinalApplicationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import com.softcell.gonogo.workflow.actions.ComponentAction;
import com.softcell.workflow.component.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map.Entry;

/**
 * @author kishorp
 *         <pre>
 *                 A <em>FinalGonoGoProcessor</em> class will extends the properties
 *                 of Component action class.This class will execute all final action
 *                 of work flow.
 *                 <p> Application processing will stop after successful execution of this class</p>
 *                 </pre>
 * @see com.softcell.gonogo.workflow.actions.ComponentAction
 */
public class FinalGonoGoProcessor extends ComponentAction {
    private static final Logger logger = LoggerFactory.getLogger(FinalGonoGoProcessor.class);
    GoNoGoCustomerApplication goNoGoCustomerApplication;
    Component component;

    @Override
    public String process(GoNoGoCustomerApplication goNoGoCustomerApplication,
                          Component component) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        this.component = component;
        return Status.PASS.toString();
    }

    @Override
    public void run() {

        try {

            saveToMongo(goNoGoCustomerApplication);

        } catch (Exception e) {

            logger.error(e.getMessage());

        }
    }

    /**
     * @param goNoGoCustomerApplication2
     */
    private void saveToMongo(
            GoNoGoCustomerApplication goNoGoCustomerApplication2) throws Exception {
        FinalApplicationMongoRepository finalApplcation = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());
        FinalApplicationRepository finalApp = (FinalApplicationRepository) ApplicationProxy.getProxy(finalApplcation, FinalApplicationRepository.class);
        updateScoringBucket(goNoGoCustomerApplication);
        finalApp.saveFinalApplication(goNoGoCustomerApplication);

    }

    /**
     * @param goNoGoCustomerApplication
     */
    private void updateScoringBucket(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {

        HashSet<Integer> scoringsSet = new HashSet<Integer>();
        for (ModuleOutcome score : goNoGoCustomerApplication
                .getApplScoreVector()) {
            scoringsSet.add(score.getOrder());
        }


        for (Entry<Integer, ModuleOutcome> bucket : Cache.APPLICATION_SCORING_BUCKET
                .entrySet()) {
            if (!scoringsSet.contains(bucket.getKey())) {
                goNoGoCustomerApplication.getApplScoreVector().add(
                        bucket.getValue());
            }
        }

    }
}
