/**
 * kishor12:01:21 AM  Copyright Softcell Technology
 **/
package com.softcell.workflow.component.finished;

import com.softcell.constants.AuditType;
import com.softcell.dao.mongodb.repository.LoggerMongoRepository;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;

/**
 * @author kishor
 */
public class ReProcessAudit extends Thread implements Auditable {


    private AuditData auditData;


    private String auditCollectionName;

    /**
     *
     * @param auditData           Any Object which will modified after after this action
     *                            but need to save last state of an object. This will be
     *                            moved into another database.
     * @param auditCollectionName Collection name in which  data need to be save for
     *                            audit purpose.
     *                            this name is not same as original collection name.
     *                            In case of same collection name
     * @return boolean flag to represent whether entity is saved to db or not
     */
    @Override
    public boolean saveToAudit(AuditData auditData, String auditCollectionName) {
        LoggerMongoRepository loggerMongoRepository = new LoggerMongoRepository();
        return loggerMongoRepository.saveAuditData(auditData, auditCollectionName);
    }

    /**
     *
     */
    @Override
    public void start() {
        run();
    }

    @Override
    public void run() {
        saveToAudit(this.auditData, this.auditCollectionName);
    }

    /**
     *
     * @param auditObject Any Object which will modified after after this action
     *                    but need to save last state of an object. This will be
     *                    moved into another database.
     * @param description Audit description to track audit reason.
     * @param auditType
     */
    @Override
    public void process(GoNoGoCroApplicationResponse auditObject, Object description, AuditType auditType) {
        this.auditCollectionName = getAuditCollectionName(auditObject);
        this.auditData = new AuditData();
        this.auditData.setAuditData(auditObject);
        this.auditData.setAuditType(auditType);
    }

    public boolean saveDataForAudit(GoNoGoCroApplicationResponse auditObject, Object description, AuditType auditType) {
        this.auditCollectionName = getAuditCollectionName(auditObject);
        this.auditData = new AuditData();
        this.auditData.setAuditData(auditObject);
        this.auditData.setAuditType(auditType);

        return saveToAudit(this.auditData, this.auditCollectionName);
    }
}
