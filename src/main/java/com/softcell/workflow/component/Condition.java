package com.softcell.workflow.component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by ssg0302 on 24/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Condition {

    private String className; // type of class of defined Key.

    private String key; // name of the key(of defined Class) against which we are comparing values

    private String operator; //relational operators like equals, not-equals, greater-than etc..

    private String value; // expected value to satisfy condition

    private boolean evalValue; // to store result of condition.
}
