package com.softcell.workflow.component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by ssg0302 on 24/7/19.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
/*
* This class is to apply some runtime conditions on Component(Executors) to not start execution of itself if the conditions are satisfied.
* */
public class ComponentCondition {

    private List<ComponentCondition> componentConditions;

    private String conditionName = "Default";

    private String conditionalOperator; // AND, OR

    private List<Condition> conditions;

    private boolean result;
}
