package com.softcell.workflow.component;

import com.google.common.base.Objects;
import com.softcell.workflow.component.module.ModuleSetting;
import java.util.List;
import java.util.Map;

/**
 * @author kishorp
 *         <pre>This Class is used to set component setting for each
 *                 institution id, which will be used in Workflow. this properties would
 *                 be initialized from xml file app. workflow.xml in src
 *                 folder of project.</pre>
 */
public class Component {

    /**
     * Name Service or component
     */
    private String componentName;
    /**
     * Component Id to identify bean
     */
    private String componentId;


    private List<Integer> dependency;

    private ComponentCondition componentCondition;

    private boolean active;

    private boolean coApplicant ;

    private String policyName;

    // -------------------------------------------------
    /**
     * List of all modules for particular components
     */
    Map<String, ModuleSetting> moduleSettingMap;

    private String executionBase;


    public List<Integer> getDependency() {
        return dependency;
    }

    public void setDependency(List<Integer> dependency) {
        this.dependency = dependency;
    }



    public Map<String, ModuleSetting> getModuleSettingMap() {
        return moduleSettingMap;
    }

    public void setModuleSettingMap(Map<String, ModuleSetting> moduleSettingMap) {
        this.moduleSettingMap = moduleSettingMap;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isCoApplicant() {
        return coApplicant;
    }

    public String getExecutionBase() {
        return executionBase;
    }

    public void setExecutionBase(String executionBase) {
        this.executionBase = executionBase;
    }

    public void setCoApplicant(boolean coApplicant) {
        this.coApplicant = coApplicant;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public ComponentCondition getComponentCondition() { return componentCondition; }

    public void setComponentCondition(ComponentCondition componentCondition) { this.componentCondition = componentCondition; }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Component{");
        sb.append("moduleSettingMap=").append(moduleSettingMap);
        sb.append(", componentName='").append(componentName).append('\'');
        sb.append(", componentId='").append(componentId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Component)) return false;
        Component component = (Component) o;
        return Objects.equal(getModuleSettingMap(), component.getModuleSettingMap()) &&
                Objects.equal(getComponentName(), component.getComponentName()) &&
                Objects.equal(getComponentId(), component.getComponentId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getModuleSettingMap(), getComponentName(), getComponentId());
    }
}
