/**
 *
 */
package com.softcell.workflow.component;

/**
 * @author kishorp
 *         This enum is used to define component names.
 */
public enum ComponentName {
    MULTIBUERAU, KYC_PAN, KYC_ADHAR, SCRORING, DEFAULT
}
