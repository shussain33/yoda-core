/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 2, 2016 3:50:51 PM
 */
package com.softcell.workflow.component.module;

/**
 * @author kishorp
 *
 *         <pre>
 * A <em> ModuleSetting </em> is used to set individual module 
 * setting of components.
 *  Each module will be execute parallel ways.
 * Component will wait till to complete all modules.
 *
 *         <pre>
 * By Default module is inactive. This module setting will configured in component setting.
 * </pre>
 *
 *         </pre>
 */
public class ModuleSetting {

    private String moduleName;

    private boolean active;

    private String moduleId;

    private boolean coApplicant;

    public boolean isCoApplicant() {
        return coApplicant;
    }

    public void setCoApplicant(boolean coApplicant) {
        this.coApplicant = coApplicant;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ModuleSetting [moduleName=");
        builder.append(moduleName);
        builder.append(", active=");
        builder.append(active);
        builder.append(", moduleId=");
        builder.append(moduleId);
        builder.append(", executeForCoApplicant=");
        builder.append(coApplicant);
        builder.append("]");
        return builder.toString();
    }

}
