package com.softcell.workflow.component.manager;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.workflow.component.ComponentSetting;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kishorp
 *         <p/>
 *         <pre>
 *         <em>SubTaskWorker</em> Will execute common action;
 *         </pre>
 *         <p/>
 *         <pre>
 *         This start all task which are independent and call before scoring call
 *         </pre>
 */
public class SubTaskWorker extends Thread {
    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private ComponentSetting componentSetting;

    public SubTaskWorker(GoNoGoCustomerApplication goNoGoCustomerApplication,
                         ComponentSetting componentSetting) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        this.componentSetting = componentSetting;
    }

    @Override
    public synchronized void start() {
        run();
    }

    @Override
    public void run() {
        requestAllOtherComponents(goNoGoCustomerApplication, componentSetting);
    }

    protected void requestAllOtherComponents(

            GoNoGoCustomerApplication goNoGoCustomerApplication,
            ComponentSetting componentSetting) {

        List<Thread> teskList = new ArrayList<>();
        for (Thread task : teskList) {
            try {
                task.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {

            }
        }

    }
}
