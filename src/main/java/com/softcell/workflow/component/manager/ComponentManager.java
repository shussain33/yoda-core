/**
 *
 */
package com.softcell.workflow.component.manager;

import com.rits.cloning.Cloner;
import com.softcell.aop.ApplicationProxy;
import com.softcell.config.MultiBreComponentConfiguration;
import com.softcell.constants.CacheConstant;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.FinalApplicationRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.workflow.actions.MetaAction;
import com.softcell.workflow.component.Component;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

/**
 * @author kishorp
 */
@org.springframework.stereotype.Component
@Scope("prototype")
public class ComponentManager extends Thread {

    private static final Logger logger = LoggerFactory.getLogger(ComponentManager.class);
    private String loggedInUserRole;

    ApplicationContext applicationContext = Cache.COMPONENT_FACTORY_BEAN.get(CacheConstant.BEAN_FACTORY);

    private GoNoGoCustomerApplication goNoGoCustomerApplication;

    //private String institutionId ;

    private Set<Integer> done = new LinkedHashSet<>();

    // Components to be executed provided by caller
    private Map<Integer, Component> componentsToBeExecuted;

    // Copy of componentsToBeExecuted used for execution process; This collection size reduces as the components are executed
    private Map<Integer, Component> yetToExecute;

    // Components are moved here from yetToExecute when they finish execution
    private Map<Integer, Component> running = new HashMap<>();

    private ExecutorService threadExecutorService;

    private int poolSize = 4; // TODO : get from configuration

    private MultiBreComponentConfiguration componentSetting;

    public void executionFinished(Integer id){
        running.remove(id);
        done.add(id);
    }

    public ComponentManager(GoNoGoCustomerApplication goNoGoCustomerApplication, Map<Integer, Component> componentsToBeExecuted,
                            String loggedInUserRole) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        this.componentsToBeExecuted = componentsToBeExecuted;
        this.loggedInUserRole = loggedInUserRole;

        threadExecutorService = Executors.newFixedThreadPool(poolSize);
    }

    @Deprecated
    public ComponentManager(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        //this.institutionId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId();
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
    }

    public String execute() {

        //if (componentSetting != null && null != componentSetting.getComponentMap()) {
        if( componentsToBeExecuted != null && ! CollectionUtils.sizeIsEmpty( componentsToBeExecuted )) {

            logger.debug("Component manager started workflow");
            Cloner cloner = new Cloner();

            yetToExecute = cloner.deepClone( componentsToBeExecuted );

            logger.debug("yetToExecute {}", yetToExecute );

            // TODO : filter on basis of active flag

            // Filter map on the basis of dependency size zero => Independent components
            Map<Integer, Component> scheduled
                    = yetToExecute.entrySet().stream()
                    .filter(map -> CollectionUtils.isEmpty( ((Component)map.getValue()).getDependency() ) )
                    .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue() ) );

            // Remove scheduled entries  from yetToExecute
            yetToExecute.keySet().removeAll(scheduled.keySet());

            logger.debug("scheduled {}", scheduled );

            logger.debug("yetToExecute {}", yetToExecute );

            synchronized(this) {
                /* It is possible that all executors running parallely and so yetToExecute is empty
                    Also initially at the entry in the loop, running is empty since no threads yet started
                    So for entry in the loop , the initial condition is
                        scheduled is not empty
                    And for the subsequent iterations the condition is modified with addition
                        ( yetToExecute not empty and running not empty)
                    So the condition to check and execute turns out to be
                        check till all the collections  - running, scheduled, yetToExecute, are not empty
                */
                while ( ! ( scheduled.isEmpty()
                        && yetToExecute.isEmpty() &&  running.isEmpty() ) ){
                    // start execution of running components
                    executeComponents(scheduled);

                    /*executeComponents may remove(through removeDependent()) the threads before they invoke
                     because executors do not satisfy some conditions and on basis of that we declares whether
                     to invoke that thread or not.
                    */
                    if(scheduled.isEmpty() && yetToExecute.isEmpty() &&  running.isEmpty()){
                        //get out from while loop if nothing is invoked and not needed to add itself into wait.
                        break;
                    }

                    // wait till all the components finish
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // Check which threads have finished and so the other dependent components can start
                    // Remove done entries  from yetToExecute
                    // add to scheduled list
                    scheduleDependentComponents(scheduled);
                }
            }// synchronized

            // Save the results
            logger.debug("Application Processed, Saved and Send for Decision ");
            FinalApplicationMongoRepository finalApplication = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());
            FinalApplicationRepository finalApp = (FinalApplicationRepository) ApplicationProxy.getProxy(finalApplication, FinalApplicationRepository.class);
            finalApp.saveFinalApplication(goNoGoCustomerApplication);
            finalApplication = null;
        }
        threadExecutorService.shutdown();
        logger.debug("Component manager finished workflow");
        return "success";
    }

    private void scheduleDependentComponents(Map<Integer, Component> scheduled) {
        /* Check which threads have finished and so the other dependent components can start
            Remove done entries  from yetToExecute
            add to scheduled list
        */
        /* Iterate over yetToExecute list
        *   For each component check whether the dependency component is in done set
        *   If all the dependency components are in done set then mark this component as scheduled
        *   and remove from yetToExecute list*/

        Integer key;
        Component value;
        boolean dependenciesExecuted;

        logger.debug(" yetToExecute ids = {} " , yetToExecute.entrySet());
        logger.debug(" done ids = {} " , done);

        for(  Entry<Integer, Component> entry : yetToExecute.entrySet() ) {
            key = entry.getKey().intValue();
            value = entry.getValue();
            dependenciesExecuted = true;
            for(Integer id : value.getDependency() ){
                if( !done.contains(id)){
                    dependenciesExecuted = false;
                    break;
                }
            }
            if( dependenciesExecuted ){
                logger.debug(" scheduling id = {} , dependencies done {}" , key, value.getDependency());
                scheduled.put(key, value);
            }
        }
        // Remove scheduled entries from yetToExecute
        yetToExecute.keySet().removeAll(scheduled.keySet());
    }

    private void executeComponents(Map<Integer, Component> scheduled) {
        Integer key;
        Component value;
        for(  Entry<Integer, Component> entry : scheduled.entrySet() ){
            key = entry.getKey().intValue();
            value = entry.getValue();
            MetaAction componentExecutor = (MetaAction) applicationContext.getBean(value.getComponentId());
            componentExecutor.setComponentManager(this);
            componentExecutor.setLoggedInUserRole(loggedInUserRole);
            if( value != null && StringUtils.isNotEmpty(value.getPolicyName()) ) {
                componentExecutor.setPolicyName(value.getPolicyName());
            }
            if (StringUtils.isNotEmpty(value.getExecutionBase())){
                componentExecutor.setExecutionBase(value.getExecutionBase()); }
            componentExecutor.setExecuteForCoApplicant(value.isCoApplicant());
            componentExecutor.setActionId(key);

            if(null != value.getComponentCondition()) {
                boolean result = componentExecutor.checkConditions(value.getComponentCondition());
                if(!result) {
                    logger.debug("{} and it's dependent are in removing state, because condition is Satisfied {}", value.getComponentName(), value.getComponentCondition().toString());
                    removeDependent(key);
                    continue;
                }
            }

            /*Thread t = new Thread(componentExecutor);
            t.start();*/
            threadExecutorService.execute(componentExecutor);

            running.put(key, value);
        }
        scheduled.clear();
    }

    /*This method remove its dependent keys from yetToExecute*/
    private void removeDependent(int rKey) {
        logger.debug("removing dependent of {} key from yetToExecute ids = {} ", rKey, yetToExecute.entrySet());
        Integer key;
        Component value;
        int oKey = rKey;
        Map<Integer, Component> dependent = new HashMap<>();
        for (Entry<Integer, Component> entry : yetToExecute.entrySet()) {
            key = entry.getKey().intValue();
            value = entry.getValue();
            for (Integer id : value.getDependency()) {
                if (id == oKey || id == rKey || dependent.containsKey(id)) {
                    logger.debug(" dependent id = {} added" , key);
                    dependent.put(key,value);
                    oKey = key;
                }
            }
        }
        yetToExecute.keySet().removeAll(dependent.keySet());
        logger.debug("dependent of {} are removed, yetToExecute ids = {}", rKey, yetToExecute.entrySet());
    }

    @Override
    public void run() {
        try {
            execute();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public GoNoGoCustomerApplication getGoNoGoCustomerApplication() {
        return goNoGoCustomerApplication;
    }
}
