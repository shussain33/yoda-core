/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 2, 2016 4:36:22 PM
 */
package com.softcell.workflow.component.manager;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.workflow.actions.ComponentAction;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.module.ModuleSetting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author kishorp
 *
 */
public class StepExecuter extends Thread {
    private GoNoGoCustomerApplication goNoGoCustomerApplication;
    private Component component;

    public StepExecuter(GoNoGoCustomerApplication goNoGoCustomerApplication,
                        Component component) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        this.component = component;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
        execute();
    }

    /**
     *
     */
    private void execute() {

        List<Thread> teskList = new ArrayList<>(); // list of all task to
        // complete parallel
        ComponentAction componentAction;
        for (Entry<String, ModuleSetting> module : component
                .getModuleSettingMap().entrySet()) {

        }
    }

}
