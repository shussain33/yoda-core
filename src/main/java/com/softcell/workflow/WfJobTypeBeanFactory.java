package com.softcell.workflow;

import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.workflow.executors.cloud.AmazonS3ServiceExecutor;
import com.softcell.workflow.executors.cloud.SalesForceExecutor;
import com.softcell.workflow.executors.dedup.DedupeExecuter;
import com.softcell.workflow.executors.dedup.NegativeDedupe;
import com.softcell.workflow.executors.kyc.AadharExecutor;
import com.softcell.workflow.executors.kyc.PanVerificationExecutor;
import com.softcell.workflow.executors.multibureau.AddressScoreExecutor;
import com.softcell.workflow.executors.multibureau.MultiBureauExecutor;
import com.softcell.workflow.executors.ntc.NTCExecutor;
import com.softcell.workflow.executors.sobre.ScoringExecutor;
import com.softcell.workflow.processors.cloud.AmazonS3Processor;
import com.softcell.workflow.processors.cloud.SalesForceProcessor;
import com.softcell.workflow.processors.dedup.DedupeProcessor;
import com.softcell.workflow.processors.kyc.KycProcessor;
import com.softcell.workflow.processors.multibureu.MultiBureauProcessor;
import com.softcell.workflow.processors.ntc.NTCProcessor;
import com.softcell.workflow.processors.sobre.ScoringProcessor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by prateek on 14/2/17.
 */
@Component
public class WfJobTypeBeanFactory {

    private static final Logger logger = LoggerFactory.getLogger(WfJobTypeBeanFactory.class);

    @Bean
    public MultiBureauProcessor multiBureauProcessor() {
        return new MultiBureauProcessor();
    }

    private MultiBureauExecutor multiBureauExecutor() {
        return new MultiBureauExecutor();
    }

    private NTCProcessor ntcProcessor() {
        return new NTCProcessor();
    }

    private NTCExecutor ntcExecutor() {
        return new NTCExecutor();
    }

    private DedupeProcessor dedupeProcessor() {
        return new DedupeProcessor();
    }

    private NegativeDedupe negativeDedupe() {
        return new NegativeDedupe();
    }

    private DedupeExecuter dedupeExecuter() {
        return new DedupeExecuter();
    }

    private ScoringProcessor scoringProcessor() {
        return new ScoringProcessor();
    }

 /*   private ScoringExecutor scoringExecutor() {
        return new ScoringExecutor();
    }
*/
    private AddressScoreExecutor verificationScoring() {
        return new AddressScoreExecutor();
    }

    private PanVerificationExecutor panVerificationExecutor() {
        return new PanVerificationExecutor();
    }

    private KycProcessor kycProcessor() {
        return new KycProcessor();
    }

    private SalesForceProcessor salesForceProcessor() {
        return new SalesForceProcessor();
    }

    private SalesForceExecutor salesForceExecutor() {
        return new SalesForceExecutor();
    }

    private AmazonS3Processor amazonS3Processor() {
        return new AmazonS3Processor();
    }

    private AmazonS3ServiceExecutor amazonS3ServiceExecutor() {
        return new AmazonS3ServiceExecutor();
    }

    private AadharExecutor aadharExecutor() {
        return new AadharExecutor();
    }

    public <T> T getBean(String beanId) {

        if (StringUtils.equalsIgnoreCase(WfJobTypeConst.MULTIBUREAU_PROCESSOR.getValue(), beanId)) {

            return (T) multiBureauProcessor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.MULTIBUREAU_JOB.getValue(), beanId)) {

            return (T) new MultiBureauExecutor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.NTC_PROCESSOR.getValue(), beanId)) {

            return (T) ntcProcessor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.NTC_EXECUTOR.getValue(), beanId)) {

            return (T) ntcExecutor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.DEDUP_PROCESSOR.getValue(), beanId)) {

            return (T) dedupeProcessor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.NEGATIVE_DEDUP.getValue(), beanId)) {

            return (T) negativeDedupe();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.DEDUP_EXECUTOR.getValue(), beanId)) {

            return (T) dedupeExecuter();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.SCORING_PROCESSOR.getValue(), beanId)) {

            return (T) scoringProcessor();

        } /*else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.SCORING_JOB.getValue(), beanId)) {

            return (T) scoringExecutor();

        } */else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.VERIFICATION_SCORING_JOB.getValue(), beanId)) {

            return (T) verificationScoring();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.PAN_VERIFICATION_JOB.getValue(), beanId)) {

            return (T) panVerificationExecutor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.KYC_PROCESSOR.getValue(), beanId)) {

            return (T) kycProcessor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.SALESFORCE_PROCESSOR.getValue(), beanId)) {

            return (T) salesForceProcessor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.SALESFORCE_EXECUTOR.getValue(), beanId)) {

            return (T) salesForceExecutor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.AMAZON_PROCESSOR.getValue(), beanId)) {

            return (T) amazonS3Processor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.AMAZON_EXECUTOR.getValue(), beanId)) {

            return (T) amazonS3ServiceExecutor();

        } else if (StringUtils.equalsIgnoreCase(WfJobTypeConst.AADHAR_EXECUTOR.getValue(), beanId)) {

            return (T) aadharExecutor();

        } else {
            logger.error(String.format("No bean found in application of id type [%s]", beanId));
        }

        return null;
    }


}
