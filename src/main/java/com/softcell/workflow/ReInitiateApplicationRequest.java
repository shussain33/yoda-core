package com.softcell.workflow;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author kishor
 */
public class ReInitiateApplicationRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    /*re-process app of application to start and
     * execution work flow*/
    @JsonProperty("oWorkFlowConfig")
    @NotNull(groups = {WorkFlowRestartConfiguration.ReInitGrp.class})
    @Valid
    private WorkFlowRestartConfiguration workFlowRestartConfiguration;

    /*application request of applicant after modification of data*/
    @JsonProperty("oApplicationRequest")
    private ApplicationRequest applicationRequest;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public WorkFlowRestartConfiguration getWorkFlowRestartConfiguration() {
        return workFlowRestartConfiguration;
    }

    public void setWorkFlowRestartConfiguration(WorkFlowRestartConfiguration
                                                        workFlowRestartConfiguration) {
        this.workFlowRestartConfiguration = workFlowRestartConfiguration;
    }

    public ApplicationRequest getApplicationRequest() {
        return applicationRequest;
    }

    public void setApplicationRequest(ApplicationRequest applicationRequest) {
        this.applicationRequest = applicationRequest;
    }

   public interface FetchGrp{

    }


}
