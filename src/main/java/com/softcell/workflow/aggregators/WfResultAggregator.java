package com.softcell.workflow.aggregators;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;

/**
 * This is solely created for application tracking AOP
 * Purpose is to create proxies  and used this method for AOP logs
 *
 * @author bhuvneshk
 */
public interface WfResultAggregator {

    /**
     *
     * @param goNoGoCustomerApplication
     */
    void setQueue(GoNoGoCustomerApplication goNoGoCustomerApplication);
}
