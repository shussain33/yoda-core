/**
 * kishorp5:26:14 PM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow.aggregators;


import com.softcell.constants.*;
import com.softcell.constants.templates.EmailTemplateConstant;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.workflow.WorkFlowMongoRepository;
import com.softcell.dao.mongodb.repository.workflow.WorkFlowRepository;
import com.softcell.gonogo.exceptions.SystemException;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.contact.Email;
import com.softcell.gonogo.model.core.CroDecision;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.scoring.response.DecisionResponse;
import com.softcell.gonogo.model.core.scoring.response.Eligibility;
import com.softcell.gonogo.model.core.scoring.response.ScoringResponse;
import com.softcell.gonogo.model.masters.PlPincodeEmailMaster;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.service.lookup.LookupServiceHandler;
import com.softcell.service.ClientAuthenticationManager;
import com.softcell.service.DMZConnector;
import com.softcell.service.EmailSender;
import com.softcell.service.impl.DMZConnectorImpl;
import com.softcell.service.impl.EmailSenderImpl;
import com.softcell.utils.CroUtils;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * @author kishorp
 */
@Service
public class WfResultAggregatorImpl implements WfResultAggregator {

    private static final Logger logger = LoggerFactory.getLogger(WfResultAggregatorImpl.class);

    @Autowired
    private WorkFlowRepository workFlowRepository;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private DMZConnector dmzConnector;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;

    public WfResultAggregatorImpl() {
        if (null == workFlowRepository) {
            workFlowRepository = new WorkFlowMongoRepository(MongoConfig.getMongoTemplate());

        }
        if (null == emailSender) {
            emailSender = new EmailSenderImpl();
        }

        if (null == dmzConnector) {
            dmzConnector = new DMZConnectorImpl();
        }

        if(null == lookupService){
            lookupService = new LookupServiceHandler();
        }
    }

    /**
     * This method will set CRO queue. for particular product.
     *
     * @param goNoGoCustomerApplication
     * @return
     */

    public void setQueue(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        if (goNoGoCustomerApplication != null
                && goNoGoCustomerApplication.getApplicationRequest() != null
                && goNoGoCustomerApplication.getApplicationRequest()
                .getRequest() != null) {

            Request request = goNoGoCustomerApplication.getApplicationRequest()
                    .getRequest();
            /* Logging for Error case all error cases will be in
             */
            if ("NEW".equals(goNoGoCustomerApplication.getApplicationStatus())
                    || StringUtils.isBlank(goNoGoCustomerApplication.getApplicationStatus())) {

                goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId("default");
                goNoGoCustomerApplication.getIntrimStatus().setCroStatus(Status.COMPLETE.toString());
                // This is for HDBFS.
                /*List<CroDecision> croDecisions = CroUtils.getCroDecision(goNoGoCustomerApplication);
                goNoGoCustomerApplication.setCroDecisions(croDecisions);
                */
                goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());
                goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());
            }
            if (request.getApplication() != null) {

                String loanType = request.getApplication().getLoanType();
                if (StringUtils.equalsIgnoreCase(loanType, Product.PL.name())) {
                    setPLQueue(goNoGoCustomerApplication);
                    return;
                } else if (StringUtils.equalsIgnoreCase(loanType, Product.CCBT.toProductName())) {
                    setCCBTQueue(goNoGoCustomerApplication);
                    return;
                } else if (StringUtils.equalsIgnoreCase(loanType, Product.CDL.name())
                        || StringUtils.equalsIgnoreCase(loanType, Product.DPL.name())) {
                    setCDLQueue(goNoGoCustomerApplication);

                        /**
                         * Send SMS
                         */
                    if(GNGWorkflowConstant.APPROVED.toFaceValue().equalsIgnoreCase(goNoGoCustomerApplication.getApplicationStatus())) {
                        if (lookupService.checkActionsAccess(
                                goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                                , goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId()
                                , ActionName.SEND_APPROVAL_SMS)) {
                            SmsTemplateConfiguration smsTemplateConfiguration =
                                    lookupService.getSmsTemplateConfiguration(
                                            goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()
                                            , goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId()
                                            , GNGWorkflowConstant.APPROVAL_SMS.name());
                            String smsMessage = null;
                            if (smsTemplateConfiguration != null) {
                                smsMessage = smsTemplateConfiguration.getFormattedSMSText(goNoGoCustomerApplication.getGngRefId());
                            } else {
                                smsMessage = "TVS Loan approved. For Ref Id : " + goNoGoCustomerApplication.getGngRefId();
                            }
                            //Send SMS
                          clientAuthenticationManager.sendSMS(goNoGoCustomerApplication.getApplicationRequest(), smsMessage);
                        }

                    }
                    return;
                }
            }
        }
    }

    private void setCDLQueue(GoNoGoCustomerApplication goNoGoCustomerApplication) {
        ScoringResponse scoringResponse = goNoGoCustomerApplication
                .getApplicantComponentResponse().getScoringServiceResponse();

        if (scoringResponse != null) {
            if (scoringResponse.getEligibilityResponse() != null || scoringResponse.getDecisionResponse() != null) {

                DecisionResponse decisionResponse = scoringResponse.getDecisionResponse();
                goNoGoCustomerApplication.setApplicationStatus(decisionResponse.getDecision());
                goNoGoCustomerApplication.getApplicationRequest().
                        setCurrentStageId(GngUtils.getCurrentStageBasedOnApplicationStatus(decisionResponse.getDecision()));

                logger.info("CRO decision from scoring is {}", decisionResponse.getDecision());

                if (StringUtils.equalsIgnoreCase(decisionResponse.getDecision().toUpperCase(), Status.APPROVED.toString())) {
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP.toString());
                    List<CroDecision> croDecisions = CroUtils.getCroDecision(goNoGoCustomerApplication);
                    goNoGoCustomerApplication.setCroDecisions(croDecisions);
                } else if (StringUtils.equalsIgnoreCase(decisionResponse.getDecision().toUpperCase(), Status.DECLINED.toString())) {

                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP.toString());
                    List<CroDecision> croDecisions = CroUtils.getCroDecision(goNoGoCustomerApplication);
                    goNoGoCustomerApplication.setCroDecisions(croDecisions);
                } else {
                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId("default");
                    goNoGoCustomerApplication.getIntrimStatus().setCroStatus(Status.COMPLETE.toString());
                    List<CroDecision> croDecisions = CroUtils.getCroDecision(goNoGoCustomerApplication);
                    goNoGoCustomerApplication.setCroDecisions(croDecisions);
                }
            }
        }
    }

    /**
     * This method handles ccbt product Queue
     *
     * @param goNoGoCustomerApplication
     */
    private void setCCBTQueue(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        ScoringResponse scoringResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse();

        if (scoringResponse != null) {

            if (scoringResponse.getEligibilityResponse() != null || scoringResponse.getDecisionResponse() != null) {

                DecisionResponse decisionResponse = scoringResponse.getDecisionResponse();

                goNoGoCustomerApplication.setApplicationStatus(decisionResponse.getDecision());
                goNoGoCustomerApplication.getApplicationRequest().
                        setCurrentStageId(GngUtils.getCurrentStageBasedOnApplicationStatus(decisionResponse.getDecision()));


                /**
                 * Mail id finding logic start
                 */
                long pincode = getPinCode(goNoGoCustomerApplication);

                String[] to = getEMailIDForTo(goNoGoCustomerApplication);

                String[] cc = getEmailIDForCC(pincode);
                /**
                 * Mail id finding logic end
                 */

                if (StringUtils.equalsIgnoreCase(decisionResponse.getDecision().toUpperCase(), Status.APPROVED.name())) {

                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP_CCBT.name());

                    List<CroDecision> croDecisions = getCroDecisionPLAndCCBT(goNoGoCustomerApplication);

                    goNoGoCustomerApplication.setCroDecisions(croDecisions);

                    /**
                     * Email for Approved Application
                     */
                    emailSender.sendCustomerAckEmail(goNoGoCustomerApplication, to, cc,
                            EmailTemplateConstant.CCBT_APPROVED_SUBJECT_LINE
                                    + goNoGoCustomerApplication.getGngRefId()
                                    + " .",
                            EmailTemplateConstant.CCBT_APPROVED_TEMPLATE);

                } else if (StringUtils.equalsIgnoreCase(decisionResponse.getDecision().toUpperCase(), Status.DECLINED.name())) {

                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP_CCBT.name());

                    List<CroDecision> croDecisions = getCroDecisionPLAndCCBT(goNoGoCustomerApplication);

                    goNoGoCustomerApplication.setCroDecisions(croDecisions);

                    /**
                     * Email for Declined Application
                     */

                    emailSender.sendCustomerAckEmail(goNoGoCustomerApplication, to, cc,
                            EmailTemplateConstant.CCBT_DECLINED_SUBJECT_LINE,
                            EmailTemplateConstant.CCBT_DECLINED_TEMPLATE);

                } else {

                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId("CCBT_QUEUE");

                    goNoGoCustomerApplication.getIntrimStatus().setCroStatus(Status.COMPLETE.toString());

                    List<CroDecision> croDecisions = getCroDecisionPLAndCCBT(goNoGoCustomerApplication);

                    goNoGoCustomerApplication.setCroDecisions(croDecisions);

                    /**
                     * Email for Pending Application
                     */
                    emailSender.sendCustomerAckEmail(goNoGoCustomerApplication, to, cc,
                            EmailTemplateConstant.CCBT_PENDING_SUBJECT_LINE
                                    + goNoGoCustomerApplication.getGngRefId()
                                    + " .",
                            EmailTemplateConstant.CCBT_PENDING_TEMPLATE);
                }
            }
        }
    }

    private void setPLQueue(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        ScoringResponse scoringResponse = goNoGoCustomerApplication.getApplicantComponentResponse().getScoringServiceResponse();

        if (scoringResponse != null) {

            if (scoringResponse.getEligibilityResponse() != null || scoringResponse.getDecisionResponse() != null) {

                DecisionResponse decisionResponse = scoringResponse.getDecisionResponse();

                goNoGoCustomerApplication.setApplicationStatus(decisionResponse.getDecision());
                goNoGoCustomerApplication.getApplicationRequest().
                        setCurrentStageId(GngUtils.getCurrentStageBasedOnApplicationStatus(decisionResponse.getDecision()));

                /**
                 * Mail id finding logic start
                 */
                long pincode = getPinCode(goNoGoCustomerApplication);
                String[] to = getEMailIDForTo(goNoGoCustomerApplication);
                String[] cc = getEmailIDForCC(pincode);
                /**
                 * Mail id finding logic end
                 */

                if (StringUtils.equalsIgnoreCase(decisionResponse.getDecision().toUpperCase(), Status.APPROVED.toString())) {

                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP_PL.name());

                    List<CroDecision> croDecisions = getCroDecisionPLAndCCBT(goNoGoCustomerApplication);

                    goNoGoCustomerApplication.setCroDecisions(croDecisions);

                    /**
                     * Email for Approved Application
                     */
                    emailSender.sendCustomerAckEmail(goNoGoCustomerApplication, to, cc,
                            EmailTemplateConstant.PL_APPROVED_SUBJECT_LINE
                                    + goNoGoCustomerApplication.getGngRefId()
                                    + " .",
                            EmailTemplateConstant.PL_APPROVED_TEMPLATE);

                } else if (StringUtils.equalsIgnoreCase(decisionResponse.getDecision().toUpperCase(), Status.DECLINED.toString())) {

                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId(Status.STP_PL.toString());

                    List<CroDecision> croDecisions = getCroDecisionPLAndCCBT(goNoGoCustomerApplication);

                    goNoGoCustomerApplication.setCroDecisions(croDecisions);

                    /**
                     * Email for Declined Application
                     */

                    emailSender.sendCustomerAckEmail(goNoGoCustomerApplication, to, cc,
                            EmailTemplateConstant.PL_DECLINED_SUBJECT_LINE,
                            EmailTemplateConstant.PL_DECLINED_TEMPLATE);

                } else {

                    goNoGoCustomerApplication.getApplicationRequest().getHeader().setCroId("PL_QUEUE");

                    goNoGoCustomerApplication.getIntrimStatus().setCroStatus(Status.COMPLETE.toString());

                    List<CroDecision> croDecisions = getCroDecisionPLAndCCBT(goNoGoCustomerApplication);

                    goNoGoCustomerApplication.setCroDecisions(croDecisions);

                    /**
                     * Email for Pending Application
                     */

                    emailSender.sendCustomerAckEmail(goNoGoCustomerApplication, to, cc,
                            EmailTemplateConstant.PL_PENDING_SUBJECT_LINE
                                    + goNoGoCustomerApplication.getGngRefId()
                                    + " .",
                            EmailTemplateConstant.PL_PENDING_TEMPLATE);
                }
            }
        }
    }


    private String[] getEmailIDForCC(long pincode) {
        List<PlPincodeEmailMaster> plPincodeEmailMasterList = workFlowRepository.getPincodeEmailMaster(String.valueOf(pincode));

        if (null != plPincodeEmailMasterList && !plPincodeEmailMasterList.isEmpty()) {

            Set<String> ccList = new HashSet<String>();

            for (PlPincodeEmailMaster plPincodeEmailMaster : plPincodeEmailMasterList) {
                ccList.add(plPincodeEmailMaster.getFprMailId());
            }

            return ccList.toArray(new String[ccList.size()]);

        } else {
            logger.warn("No Email id found against ==> " + pincode);
            return null;
        }

    }

    private String[] getEMailIDForTo(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        // String emailForTo = null;

        try {

            List<Email> emails = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getEmail();

            Optional<String> emailForTo = emails.parallelStream()
                    .filter(email -> GNGWorkflowConstant.PERSONAL.toFaceValue().equalsIgnoreCase(email.getEmailType()))
                    .map(email -> email.getEmailAddress()).findFirst();


            String s = emailForTo.get();

            String[] to = new String[1];

            if (null != s) {
                to[0] = s;
            }

            return to;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("exception occurred while fetching emailId for To with probable cause ", ex.getMessage());
            return null;
        }
    }

    private long getPinCode(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        try {

            List<CustomerAddress> addressList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress();

            Optional<Long> first = addressList.parallelStream()
                    .filter(address -> GNGWorkflowConstant.PERMANENT.toFaceValue().equalsIgnoreCase(address.getAddressType()))
                    .map(address -> address.getPin()).findFirst();


            return first.get();


        } catch (NoSuchElementException e) {

            logger.error(" no object found as addressType {} ", GNGWorkflowConstant.PERMANENT.toFaceValue());

            return 0;

        } catch (Exception e) {
            logger.error("error occurred while finding pin codes with probable cause as {}", e.getMessage());
            throw new SystemException(String.format("error occurred while finding pin codes with probable cause as", e.getMessage()));
        }
    }

    /**
     * @param goNoGoCustomerApplication
     * @return This code is added for PL salary amount
     */
    private List<CroDecision> getCroDecisionPLAndCCBT(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        try {

            Eligibility eligibilityResponse = goNoGoCustomerApplication
                    .getApplicantComponentResponse()
                    .getScoringServiceResponse().getEligibilityResponse();

            List<CroDecision> croDecisions = new ArrayList<CroDecision>();

            CroDecision croDecision = new CroDecision();

            if (null != eligibilityResponse) {
                if (null != eligibilityResponse.getApprovedAmount()) {
                    croDecision.setAmtApproved(eligibilityResponse.getApprovedAmount());
                }
                croDecision.setEligibleAmt(eligibilityResponse.getEligibilityAmount());

                croDecision.setTenor(Integer.parseInt(CustomFormat.DECIMAL_FORMATE_0
                        .format(eligibilityResponse.getMaxTenor())));

                croDecision.setDownPayment(eligibilityResponse.getDp());
            }
            croDecisions.add(croDecision);

            return croDecisions;

        } catch (Exception ex) {
            ex.printStackTrace();

            logger.error("error occurred while getting cro decision for product Pl & CCBT with probable cause {}", ex.getMessage());

            throw new SystemException(String.format("error occurred while getting cro decision for product Pl & CCBT with probable cause {}", ex.getMessage()));
        }
    }
}
