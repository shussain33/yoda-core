package com.softcell.workflow;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author kishorp
 *         Use to  fetch all data after running
 *         data.
 */
public class AuditDataRequest {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sRefID")
    @NotBlank(groups = {AuditDataRequest.FetchGrp.class})
    private String refId;

    @JsonProperty("sProduct")
    private String product;

    @JsonProperty("iNoOfRecord")
    private int numberAuditRecords;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getNumberAuditRecords() {
        return numberAuditRecords;
    }

    public void setNumberAuditRecords(int numberAuditRecords) {
        this.numberAuditRecords = numberAuditRecords;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AuditDataRequest [header=").append(header)
                .append(", refId=").append(refId).append(", product=")
                .append(product).append(", numberAuditRecords=")
                .append(numberAuditRecords).append("]");
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuditDataRequest)) return false;
        AuditDataRequest that = (AuditDataRequest) o;
        return Objects.equal(getNumberAuditRecords(), that.getNumberAuditRecords()) &&
                Objects.equal(getHeader(), that.getHeader()) &&
                Objects.equal(getRefId(), that.getRefId()) &&
                Objects.equal(getProduct(), that.getProduct());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getHeader(), getRefId(), getProduct(), getNumberAuditRecords());
    }

    public interface FetchGrp {
    }
}
