package com.softcell.workflow;


import com.softcell.constants.ActionName;
import com.softcell.constants.Constant;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Status;
import com.softcell.constants.error.ErrorCode;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.modulelog.ModuleRequestRepository;
import com.softcell.dao.mongodb.repository.search.MasterMongoRepository;
import com.softcell.dao.mongodb.repository.search.MasterRepository;
import com.softcell.gonogo.model.configuration.SmsTemplateConfiguration;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.Acknowledgement;
import com.softcell.gonogo.model.response.core.BaseResponse;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.nextgen.JobResult;
import com.softcell.nextgen.constants.WfJobTypeConst;
import com.softcell.nextgen.jobs.DedupJob;
import com.softcell.nextgen.jobs.Job;
import com.softcell.nextgen.jobs.PosidexJob;
import com.softcell.nextgen.manager.NextGenWorkFlowManager;
import com.softcell.service.ClientAuthenticationManager;
import com.softcell.service.ExternalApiManager;
import com.softcell.utils.GngDateUtil;
import com.softcell.workflow.component.manager.ComponentManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


/**
 * @author kishor
 */
@Component
public class PartialWorkFlow {
    private static final Logger logger = LoggerFactory.getLogger(PartialWorkFlow.class);

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private RequestAggregator requestAggregator;

    @Autowired
    private NextGenWorkFlowManager nextGenWorkFlowManager;

    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    @Autowired
    private ModuleRequestRepository moduleRequestRepository;

    @Autowired
    private LookupService lookupService;

    @Autowired
    private ClientAuthenticationManager clientAuthenticationManager;

    @Autowired
    private ExternalApiManager externalApiManager;

    /**
     * @param applicationRequest
     */

    public void executeBRE(ApplicationRequest applicationRequest) throws Exception {
        startWorkFlow(applicationRequest);
    }

    private void startWorkFlow(ApplicationRequest applicationRequest) throws Exception {

        logger.debug("startWorkFlow service started ");

        try {

            GoNoGoCustomerApplication goNoGoCustomerApplication = RequestAggregator
                    .getGoNoGo(applicationRequest);

            ComponentManager componentManager = new ComponentManager(
                    goNoGoCustomerApplication);

            componentManager.start();

            logger.debug("startWorkFlow service started a componentManager thread ");

        } catch (Exception e) {

            logger.error("PartialWorkFlow->startWorkFlow()" + e.getMessage());

            throw new Exception(e.getMessage());
        }
    }

    private void startWorkFlow(GoNoGoCustomerApplication goNoGoCustomerApplication) throws Exception {

        logger.debug("startWorkFlow service started ");

        try {
            ComponentManager componentManager = new ComponentManager(
                    goNoGoCustomerApplication);

            componentManager.start();

            logger.debug("startWorkFlow service started a componentManager thread ");

        } catch (Exception e) {

            logger.error("PartialWorkFlow->startWorkFlow()" + e.getMessage());

            throw new Exception(e.getMessage());
        }
    }


    /**
     * @param applicationRequest
     * @param httpRequest
     * @return
     */

    public Acknowledgement workFlowStepOne(
            ApplicationRequest applicationRequest,
            //TODO ?? can be injected !!
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("workFlowStepOne service started");

        /**
         * Generate unique reference id
         */
        Object object = requestAggregator.generateReferenceId(applicationRequest);

        if (object instanceof Acknowledgement) {
            return (Acknowledgement) object;
        }
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        String refID = (String) object;
        applicationRequest.setRefID(refID);

        logger.debug("workFlowStepOne service generated refId {}", refID);

        /**
         * Calculate Age of Applicant
         */
        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();

        if (StringUtils.isNotBlank(dob)) {
            applicationRequest.getRequest().getApplicant().setAge(GngDateUtil.getAge(dob));
        }

        /**
         * This is added as application id was created in submit-application service.
         */
        applicationRequest.getHeader().setApplicationId(SequenceGenerator.getApplicationId());

        logger.debug("workFlowStepOne service generated application id {}", applicationRequest.
                getHeader().getApplicationId());
        /**
         *  Save the application.
         */
        String saveResult = applicationRepository.saveApplication(applicationRequest);

        // Whether to push to external system
        if (lookupService.checkActionsAccess(
                applicationRequest.getHeader().getInstitutionId()
                , applicationRequest.getHeader().getProduct().toProductId()
                , ActionName.PUSH_DATA_TO_SAATHI_SERVICE)) {
            try {
                externalApiManager.pushDataToSaathi(applicationRequest);
            } catch(Exception e){
                logger.debug("Exception {} occurred while {} for refid {}", e.getMessage(),
                        ActionName.PUSH_DATA_TO_SAATHI_SERVICE.name(), applicationRequest.getRefID());
            }
        }
        /**
         * SMS for customer application download link
         */
        if (lookupService.checkActionsAccess(
                applicationRequest.getHeader().getInstitutionId()
                , applicationRequest.getHeader().getProduct().toProductId()
                , ActionName.SEND_DONWLOAD_LINK_SMS)) {
            //Get the SMS contents
            SmsTemplateConfiguration smsTemplateConfiguration =
                    lookupService.getSmsTemplateConfiguration(
                            applicationRequest.getHeader().getInstitutionId()
                            , applicationRequest.getHeader().getProduct().toProductId()
                            , GNGWorkflowConstant.DOWNLOAD_LINK_SMS.name());
            String smsMessage = null;
            if (smsTemplateConfiguration != null) {
                smsMessage = smsTemplateConfiguration.getSmsText();
                //Send SMS
                BaseResponse smsResponse = clientAuthenticationManager.sendSMS(applicationRequest, smsMessage);
            }
        } else {
            logger.info("Action {} not defined/enabled for InstituionId {} & ProductId {} "
                    , ActionName.SEND_DONWLOAD_LINK_SMS.name()
                    , applicationRequest.getInstitutionId()
                    , applicationRequest.getHeader().getProduct().toProductId());
        }

        stopWatch.stop();
        // Save activity log
        ActivityLogs activityLogs = getActivityLogs(applicationRequest, httpRequest,
                GNGWorkflowConstant.DE.toFaceValue(), GNGWorkflowConstant.DE_STEP1.toFaceValue());
        activityLogs.setStatus(Status.NEW.toString());
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from PartialWorkflow in thread %s",
                Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLogs);

        /**
         * Return Acknowledgement based on db operation.
         */
        if (saveResult != null) {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.SUCCESS, null, null,
                    null, null);
        } else {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                    ErrorCode.INVALID_REQUEST_DSCR, null, null);
        }
    }

    /**
     * @param applicationRequest
     * @param httpRequest
     * @return
     */

    public Acknowledgement workFlowStepTwo(
            ApplicationRequest applicationRequest,
            HttpServletRequest httpRequest) throws Exception {
        /**
         * Generate unique reference id if it was not created in step1 .
         */
        logger.debug("workFlowStepTwo service started ");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        if (StringUtils.isBlank(applicationRequest.getRefID())) {

            Object object = requestAggregator.generateReferenceId(applicationRequest);

            if (object instanceof Acknowledgement) {
                stopWatch.stop();
                return (Acknowledgement) object;
            }

            String refID = (String) object;
            applicationRequest.setRefID(refID);
            logger.debug("workFlowStepTwo service generated refId {}", refID);
        }

        stopWatch.stop();
        // Save activity log
        ActivityLogs activityLogs = getActivityLogs(applicationRequest, httpRequest,
                GNGWorkflowConstant.DE.toFaceValue(), GNGWorkflowConstant.DE_STEP2.toFaceValue());
        activityLogs.setStatus(Status.NEW.toString());
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from PartialWorkflow in thread %s",
                Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLogs);

        /* Save the application, Return Acknowledgement based on db operation.
         */
        if (applicationRepository.upsertApplication(applicationRequest)) {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.SUCCESS, null, null,
                    null, null);
        } else {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                    ErrorCode.INVALID_REQUEST_DSCR, null, null);
        }
    }

    /**
     * @param applicationRequest
     * @param httpRequest
     * @return
     */
    public Acknowledgement workFlowStepThree(
            ApplicationRequest applicationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("workFlowStepThree service started ");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        /**
         * Generate unique reference id if it was not created in step1 .
         */
        if (StringUtils.isBlank(applicationRequest.getRefID())) {

            Object object = requestAggregator.generateReferenceId(applicationRequest);
            if (object instanceof Acknowledgement) {
                stopWatch.stop();
                return (Acknowledgement) object;
            }

            String refID = (String) object;
            applicationRequest.setRefID(refID);
            logger.debug("workFlowStepThree service generated refId {}", refID);
        }

        stopWatch.stop();
        // Save activity log
        ActivityLogs activityLogs = getActivityLogs(applicationRequest, httpRequest,
                GNGWorkflowConstant.DE.toFaceValue(), GNGWorkflowConstant.DE_STEP3.toFaceValue());
        activityLogs.setStatus(Status.NEW.toString());
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from PartialWorkflow in thread %s",
                Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLogs);

        /**
         * Save the application, Return Acknowledgement based on db operation.
         */
        if (applicationRepository.upsertApplication(applicationRequest)) {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.SUCCESS, null, null,
                    null, null);
        } else {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                    ErrorCode.INVALID_REQUEST_DSCR, null, null);
        }
    }

    /**
     * @param applicationRequest
     * @param httpRequest
     * @return
     */
    public Acknowledgement workFlowStartStep(
            ApplicationRequest applicationRequest,
            HttpServletRequest httpRequest) throws Exception {

        logger.debug("workFlowStartStep service started ");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        applicationRequest.setCurrentStageId(GNGWorkflowConstant.BRE.toFaceValue());
        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();

        if (StringUtils.isNotBlank(dob)) {
            applicationRequest.getRequest().getApplicant().setAge(GngDateUtil.getAge(dob));
        }

        if (StringUtils.isBlank(applicationRequest.getRefID())) {
            Object object = requestAggregator.generateReferenceId(applicationRequest);

            if (object instanceof Acknowledgement) {
                stopWatch.stop();
                return (Acknowledgement) object;
            }

            String refID = (String) object;
            applicationRequest.setRefID(refID);
            logger.debug("workFlowStartStep service generated refId {}", refID);
        }

        if (StringUtils.isBlank(applicationRequest.getHeader().getApplicationId())) {
            applicationRequest.getHeader().setApplicationId(SequenceGenerator.getApplicationId());
            logger.debug("workFlowStartStep service generated application {}", applicationRequest.
                    getHeader().getApplicationId());
        }


        /**
         * Set DealerRank in applicationRequest
         */
        MasterRepository masterRepository = new MasterMongoRepository(
                MongoConfig.getMongoTemplate());

        DealerEmailMaster dealerEmailMaster = masterRepository
                .getDealerRanking(applicationRequest.getHeader()
                        .getDealerId(), applicationRequest.getHeader()
                        .getInstitutionId());

        if (null != dealerEmailMaster) {
            applicationRequest.setDealerRank(dealerEmailMaster.getDealerComm());
        }

        stopWatch.stop();
        // Save activity log
        ActivityLogs activityLogs = getActivityLogs(applicationRequest, httpRequest,
                GNGWorkflowConstant.DE.toFaceValue(), GNGWorkflowConstant.DE_STEP4.toFaceValue());
        activityLogs.setStatus(Status.SUBMITTED.toString());
        activityLogs.setDuration(stopWatch.getLastTaskTimeMillis());
        logger.debug(String.format(" Publishing activity log from PartialWorkflow in thread %s",
                Thread.currentThread().getName()));
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLogs);

        /**
         * Save application and initiate workflow.
         */
        if (applicationRepository.upsertApplication(applicationRequest)) {

            startWorkFlow(applicationRequest);
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.SUCCESS, null, null,
                    null, null);
        } else {
            return RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                    ErrorCode.INVALID_REQUEST_DSCR, null, null);
        }
    }


    /**
     * @param applicationRequest
     * @param httpRequest
     * @param stage
     * @return
     */
    private ActivityLogs getActivityLogs(ApplicationRequest applicationRequest, HttpServletRequest httpRequest, String stage, String step) {

        ActivityLogs activityLog = auditHelper.createActivityLog(httpRequest, applicationRequest.getHeader());
        activityLog.setRefId(applicationRequest.getRefID());
        activityLog.setStatus(stage);
        activityLog.setStage(stage);
        activityLog.setStep(step);
        return activityLog;

    }

    public Acknowledgement workFlowDedupeStep(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {

        applicationRequest.setCurrentStageId(GNGWorkflowConstant.BRE.toFaceValue());
        String dob = applicationRequest.getRequest().getApplicant().getDateOfBirth();

        if (StringUtils.isNotBlank(dob)) {
            applicationRequest.getRequest().getApplicant().setAge(GngDateUtil.getAge(dob));
        }

        if (StringUtils.isBlank(applicationRequest.getRefID())) {
            Object object = requestAggregator.generateReferenceId(applicationRequest);

            if (object instanceof Acknowledgement) {
                return (Acknowledgement) object;
            }

            String refID = (String) object;
            applicationRequest.setRefID(refID);
            logger.debug("workFlowStartStep service generated refId {}", refID);
        }

        if (StringUtils.isBlank(applicationRequest.getHeader().getApplicationId())) {
            applicationRequest.getHeader().setApplicationId(SequenceGenerator.getApplicationId());
            logger.debug("workFlowStartStep service generated application {}", applicationRequest.
                    getHeader().getApplicationId());
        }

        applicationRepository.upsertApplication(applicationRequest);

        GoNoGoCustomerApplication goNoGoCustomerApplication = RequestAggregator
                .getGoNoGo(applicationRequest);

        List<Job> jobs = getDedupeJobs(goNoGoCustomerApplication);

        nextGenWorkFlowManager.addJobs(jobs);

        List<Future<JobResult>> futures = nextGenWorkFlowManager.processJob();

        boolean internalDedupe = false;
        boolean posidexDedupe = false;


        if (null != futures) {
            for (Future<JobResult> jobResultFuture : futures) {
                while (!jobResultFuture.isDone()) {
                    logger.warn("Thread is in In process");
                }
                try {
                    JobResult jobResult = jobResultFuture.get();
                    Map<String, Object> result = jobResult.getResult();
                    if (StringUtils.equals(WfJobTypeConst.DEDUP_PROCESSOR.getValue(), result.get("JOB_NAME").toString())) {
                        internalDedupe = (boolean) result.get("DEDUPE_STATUS");
                    } else if (StringUtils.equals(WfJobTypeConst.POSIDEX_DEDUPE_EXECUTOR.getValue(), result.get("JOB_NAME").toString())) {
                        posidexDedupe = (boolean) result.get("DEDUPE_STATUS");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }
        }

        if (!(internalDedupe || posidexDedupe)) {
            startWorkFlow(goNoGoCustomerApplication);
        } else {
            goNoGoCustomerApplication.setApplicationStatus(GNGWorkflowConstant.DEDUPE_QUEUE.toFaceValue());
            goNoGoCustomerApplication.getApplicationRequest().setCurrentStageId(GNGWorkflowConstant.DEDUPE_FOUND.toFaceValue());
            moduleRequestRepository.partialApplicationDbSave(goNoGoCustomerApplication);
            applicationRepository.updateApplicationInElasticsearch(goNoGoCustomerApplication);
            logger.warn("Further workflow not initiated at this movement because either internal dedupe or posidex finds dedupe,Posidex Status:{} & Internal Dedupe status:{} ", posidexDedupe, internalDedupe);
        }

        return RequestAggregator.getAcknowledgement(applicationRequest, Constant.SUCCESS, null, null,
                null, null);


    }

    private List<Job> getDedupeJobs(GoNoGoCustomerApplication goNoGoCustomerApplication) {

        List<Job> jobs = new ArrayList<>();

        DedupJob dedupJob = new DedupJob();

        autowireCapableBeanFactory.autowireBean(dedupJob);

        dedupJob.prerequisite(goNoGoCustomerApplication, false);

        jobs.add(dedupJob);

        PosidexJob posidexJob = new PosidexJob();

        autowireCapableBeanFactory.autowireBean(posidexJob);

        posidexJob.prerequisite(goNoGoCustomerApplication, false);

        jobs.add(posidexJob);

        return jobs;
    }

    public Acknowledgement startRemainingWorkflow(ApplicationRequest applicationRequest, HttpServletRequest httpRequest) throws Exception {
        Acknowledgement acknowledgement;
        if (null != applicationRequest && StringUtils.isNotBlank(applicationRequest.getRefID())) {
            GoNoGoCustomerApplication gonogoDocument = applicationRepository.getGonogoDocument(applicationRequest.getRefID());

            if (null != gonogoDocument) {
                startWorkFlow(gonogoDocument);
                acknowledgement = RequestAggregator.getAcknowledgement(applicationRequest, Constant.SUCCESS, null, null,
                        null, null);
            } else {
                acknowledgement = RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.NO_DATA_FOUND_CODE,
                        ErrorCode.APPLICATION_NOT_FOUND, null, null);
            }
        } else {
            acknowledgement = RequestAggregator.getAcknowledgement(applicationRequest, Constant.ERROR, ErrorCode.INVALID_REQUEST_CODE,
                    ErrorCode.INVALID_REQUEST_DSCR, null, null);
        }
        return acknowledgement;
    }
}
