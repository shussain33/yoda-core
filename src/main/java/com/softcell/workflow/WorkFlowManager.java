/**
 * kishorp1:02:03 AM  Copyright Softcell Technology
 **/
package com.softcell.workflow;

import com.rits.cloning.Cloner;
import com.softcell.config.ComponentConfiguration;
import com.softcell.constants.*;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.ComponentConfigurationRepository;
import com.softcell.dao.mongodb.repository.FinalApplicationMongoRepository;
import com.softcell.dao.mongodb.repository.UploadFileRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.PostIPA;
import com.softcell.gonogo.model.core.ReAppraisalDetails;
import com.softcell.gonogo.model.logger.ActivityLogs;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.Acknowledgement;
import com.softcell.gonogo.model.response.core.ErrorMessage;
import com.softcell.gonogo.service.factory.GngResponseEntityBuilder;
import com.softcell.gonogo.service.lookup.LookupService;
import com.softcell.gonogo.utils.AuditHelper;
import com.softcell.gonogo.utils.GoNoGoEventContext;
import com.softcell.service.DMZConnector;
import com.softcell.utils.GngUtils;
import com.softcell.workflow.component.Component;
import com.softcell.workflow.component.ComponentSetting;
import com.softcell.workflow.component.finished.Auditable;
import com.softcell.workflow.component.finished.ReProcessAudit;
import com.softcell.workflow.component.module.ModuleSetting;
import com.softcell.workflow.utils.ReAppraiseReInitiateUtils;
import com.softcell.workflow.utils.ReprocessExecutionMapper;
import com.softcell.workflow.utils.Utility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.StopWatch;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author kishorp
 */
@org.springframework.stereotype.Service(value = "workFlowManager")
public class WorkFlowManager {

    private static final Logger logger = LoggerFactory.getLogger(WorkFlowManager.class);

    private static GngResponseEntityBuilder gngResponseEntityBuilder = new GngResponseEntityBuilder();

    private Auditable auditable;

    private GoNoGoCroApplicationResponse goNoGoCustomerApplication = null;

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ComponentConfigurationRepository componentConfigRepo;

    private ComponentConfiguration componentConfig;

    @Autowired
    private AuditHelper auditHelper;

    @Autowired
    private ApplicationEventPublisher activityEventPublisher;

    @Autowired
    private DMZConnector dmzConnector;

    @Autowired
    private UploadFileRepository uploadFileRepository;

    @Autowired
    private LookupService lookupService;

    /**
     * @param goNoGoCustomerApplication The application to process under the define flow.
     *                                  work flow start and give control to system.
     */
    public void startWorkFlow(GoNoGoCroApplicationResponse goNoGoCustomerApplication) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        ComponentSetting componentSetting = Cache.COMPONENT_SETTING_MAP
                .get((goNoGoCustomerApplication.getApplicationRequest()
                        .getHeader().getInstitutionId()));
        if (componentSetting != null && goNoGoCustomerApplication != null) {
            ReprocessWorkFlow reprocessWorkFlow = new ReprocessWorkFlow(goNoGoCustomerApplication
                    , componentSetting);
            reprocessWorkFlow.start();
        }

    }

    /**
     * @param reInitiateApplicationRequest ReInitiateApplicationRequest request with the details for
     *                                     re-initiating the application.
     * @return Acknowledgement success or failure acknowledgement.
     */
    public Acknowledgement restartUpdatedApplicationWorkflow(
            ReInitiateApplicationRequest reInitiateApplicationRequest) {

        AuditType auditType = null;

        boolean dataSavedForAudit = false;

        Acknowledgement acknowledgement = null;
        /**
         * Cro is re-initiating himself(not coming through re-appraisal)
         */
        auditType = AuditType.RE_INITIATE;
        /**
         * Save the data for audit purpose.
         */
        ReProcessAudit rpAudit = new ReProcessAudit();
        dataSavedForAudit = rpAudit.saveDataForAudit(
                this.goNoGoCustomerApplication, "reprocess", auditType);
        // If data saved then only process otherwise return failure.
        if (dataSavedForAudit) {
            // Set the new application request in existing application if CRO updated the changes.
            if (reInitiateApplicationRequest.getApplicationRequest() != null) {
                this.goNoGoCustomerApplication
                        .setApplicationRequest(reInitiateApplicationRequest
                                .getApplicationRequest());
            }
            //Update the date and reInitiateCount.
            updateGoNoGoFields();
            // Initialize the component settings according to request.
            ComponentSetting componentSetting = reCheckComponentSetting(reInitiateApplicationRequest
                    .getWorkFlowRestartConfiguration());

            if (componentSetting != null) {
                // Restart the work-flow
                ReprocessWorkFlow reprocessWorkFlow = new ReprocessWorkFlow(
                        this.goNoGoCustomerApplication, componentSetting);
                reprocessWorkFlow.start();
                acknowledgement = gngResponseEntityBuilder
                        .getSuccesAcknowledgement(this.goNoGoCustomerApplication
                                .getApplicationRequest());
            } else {
                logger.error("There was error processing the modules sent for re-initiate");
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage
                        .setDescription("There was error processing the modules sent for re-initiate");
                acknowledgement = gngResponseEntityBuilder
                        .getFailureAcknowledgement(this.goNoGoCustomerApplication
                                .getApplicationRequest(), errorMessage);
            }
        } else {
            logger.error("Error while saving the previous data for audit");
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage
                    .setDescription("Error while saving the previous data for audit");
            acknowledgement = gngResponseEntityBuilder
                    .getFailureAcknowledgement(this.goNoGoCustomerApplication
                            .getApplicationRequest(), errorMessage);
        }
        return acknowledgement;
    }

    /**
     * update re-try count.
     */
    private void updateGoNoGoFields() {
        this.goNoGoCustomerApplication
                .setReInitiateCount(this.goNoGoCustomerApplication
                        .getReInitiateCount() + 1);
        this.goNoGoCustomerApplication.setDateTime(new Date());
        ;
    }

    /**
     * @param goNoGoCustomerApplication    The application to process under the define flow.
     *                                     work flow start and give control to system.
     * @param workFlowRestartConfiguration The work-flow restart app is defined by
     *                                     CRO and it will be execute completely or partially.
     *                                     Module defined by user and all those are not executed
     *                                     successfully in last run, due to fatal error or system
     *                                     was unstable.
     */
    public void reStartWorkFlow(GoNoGoCroApplicationResponse goNoGoCustomerApplication,
                                WorkFlowRestartConfiguration workFlowRestartConfiguration) {
        this.goNoGoCustomerApplication = goNoGoCustomerApplication;
        auditable = new ReProcessAudit();
        auditable.process(goNoGoCustomerApplication, "reprocess", AuditType.RE_INITIATE);
        auditable.start();

        ComponentSetting componentSetting = reCheckComponentSetting(workFlowRestartConfiguration);

        updateGoNoGoFields(); // set  re-try count
        if (componentSetting != null && goNoGoCustomerApplication != null) {
            ReprocessWorkFlow reprocessWorkFlow = new ReprocessWorkFlow(goNoGoCustomerApplication
                    , componentSetting);
            reprocessWorkFlow.start();
        }
    }


    /**
     * @param workFlowRestartConfiguration The work-flow restart app is defined by
     *                                     CRO and it will be execute completely or partially.
     *                                     Module defined by user and all those are not executed
     *                                     successfully in last run, due to fatal error or system
     *                                     was unstable.
     * @return modified or customized setting for work flow.
     * For re-process application this will defined by user or CRO.
     * In re-process flow application may be execute either end-to-end
     * or partially.
     */
    private ComponentSetting reCheckComponentSetting(WorkFlowRestartConfiguration workFlowRestartConfiguration) {

        ComponentSetting clonedCompSettings = null;

        try {
            clonedCompSettings = new Cloner().deepClone(Cache.COMPONENT_SETTING_MAP.get(
                    this.goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId()));

            if (clonedCompSettings != null) {
                enrichment(workFlowRestartConfiguration, clonedCompSettings);

                //Set the application status to reprocessing.
                this.goNoGoCustomerApplication
                        .setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());

                this.goNoGoCustomerApplication.getApplicationRequest().
                        setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());

                FinalApplicationMongoRepository finalApplicationMongoRepository = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());

                finalApplicationMongoRepository
                        .updateGoNoGoCustomerApplication(this.goNoGoCustomerApplication);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return clonedCompSettings;
    }

    /**
     * @param workFlowRestartConfiguration The work-flow restart app is defined by
     *                                     CRO and it will be execute completely or partially.
     *                                     Module defined by user and all those are not executed
     *                                     successfully in last run, due to fatal error or system
     *                                     was unstable.
     * @param componentSetting
     */
    private void enrichment(
            WorkFlowRestartConfiguration workFlowRestartConfiguration,
            ComponentSetting componentSetting) {
        for (Entry<Integer, List<Component>> workFlowSteps : componentSetting
                .getComponentMap().entrySet()) {
            for (Component component : workFlowSteps.getValue()) {
                checkComponent(component, workFlowRestartConfiguration);
            }
        }
    }

    /**
     * @param component                    A component level mapping which  is used to define
     *                                     component setting of each  module.
     * @param workFlowRestartConfiguration The work-flow restart configuration is defined by
     *                                     CRO and it will be execute completely or partially.
     *                                     Module defined by user and all those are not executed
     *                                     successfully in last run, due to fatal error or system
     *                                     was unstable.
     */
    private void checkComponent(Component component,
                                WorkFlowRestartConfiguration workFlowRestartConfiguration) {

        List<ModuleConfiguration> customModuleConfigurations = workFlowRestartConfiguration
                .getModuleConfiguration();

        for (Entry<String, ModuleSetting> componentModuleSetting : component
                .getModuleSettingMap().entrySet()) {

            for (ModuleConfiguration customModuleConfiguration : customModuleConfigurations) {
                if (StringUtils.equalsIgnoreCase(ReprocessExecutionMapper
                        .getExecutionMapping(customModuleConfiguration
                                .getModuleName()), componentModuleSetting
                        .getKey())) {
                    componentModuleSetting.getValue().setActive(
                            customModuleConfiguration.isRunModule());
                    setInterimStatusOfModule(componentModuleSetting,
                            Status.DEFAULT, false);
                    logger.info("Resetting "
                            + componentModuleSetting.getValue().getModuleName()
                            + " status to " + Status.DEFAULT.toString());
                    break;
                } else {
                    componentModuleSetting.getValue().setActive(false);
                    logger.info("Module not defined in reprocess configuarion >> "
                            + componentModuleSetting.getKey());
                }
            }
        }
    }

    /**
     * This method sets the interim status of the modules.
     *
     * @param componentModuleSetting moduleId and moduleSettings Entry single entry of Map.Entry
     * @param status                 com.softcell.gonogo.constants.Status
     */
    private void setInterimStatusOfModule(
            Entry<String, ModuleSetting> componentModuleSetting, Status status, boolean resetModuleResult) {

        if (StringUtils.equalsIgnoreCase(componentModuleSetting.getValue()
                .getModuleId(), "multiBureauExecutor")) {
            this.goNoGoCustomerApplication.getIntrimStatus().setMbStatus(
                    status.toString());
            this.goNoGoCustomerApplication.getIntrimStatus().setCibilScore(
                    status.toString());
            if( resetModuleResult ) {
                this.goNoGoCustomerApplication.getIntrimStatus().setMbModuleResult(null);
            }
        } else if (StringUtils.equalsIgnoreCase(componentModuleSetting
                .getValue().getModuleId(), "panVarificationExecuter")) {
            this.goNoGoCustomerApplication.getIntrimStatus().setPanStatus(
                    status.toString());
            if( resetModuleResult ) {
                this.goNoGoCustomerApplication.getIntrimStatus().setPanModuleResult(null);
            }
        } else if (StringUtils.equalsIgnoreCase(componentModuleSetting
                .getValue().getModuleId(), "aadharExecutor")) {
            this.goNoGoCustomerApplication.getIntrimStatus().setAadharStatus(
                    status.toString());
            if( resetModuleResult ) {
                this.goNoGoCustomerApplication.getIntrimStatus().setAadharModuleResult(null);
            }
        } else if (StringUtils.equalsIgnoreCase(componentModuleSetting
                .getValue().getModuleId(), "DedupeExecuter")) {
            this.goNoGoCustomerApplication.getIntrimStatus().setDedupe(
                    status.toString());
        } else if (StringUtils.equalsIgnoreCase(componentModuleSetting
                .getValue().getModuleId(), "verificationScoring")) {
            this.goNoGoCustomerApplication.getIntrimStatus().setVarScoreStatus(
                    status.toString());
        } else if (StringUtils.equalsIgnoreCase(componentModuleSetting
                .getValue().getModuleId(), "scoringExecutor")) {
            this.goNoGoCustomerApplication.getIntrimStatus().setScoreStatus(
                    status.toString());
            if( resetModuleResult ) {
                goNoGoCustomerApplication.getIntrimStatus().setScoringModuleResult(null);
            }
        } else if (StringUtils.equalsIgnoreCase(componentModuleSetting
                .getValue().getModuleId(), "amazonS3ServiceExecutor")) {
            this.goNoGoCustomerApplication.getIntrimStatus().setAwsS3Status(
                    status.toString());
        }
    }

    /**
     * @param applicationRequest @ApplicationRequest Contains the re-appraisal details.
     * @return @Acknowledgement success or failure acknowledgement.
     */
    public Acknowledgement reappraiseApplication(ApplicationRequest applicationRequest) throws Exception {
        Acknowledgement acknowledgement = null;
        String deliveryOrderName=null;
        String dOImageId=null;

        deliveryOrderName = GngUtils.formDeliveryOrderName(applicationRequest.getRefID());

        //check post ipa collection existance
        PostIPA postIPA = applicationRepository.getPostIPA(applicationRequest.getRefID(),
                applicationRequest.getHeader().getInstitutionId());


        // get Image Id using RefId and DeliveryOrderName
        dOImageId = uploadFileRepository.getImageIdByRefIdAndFileName(applicationRequest.getRefID(), deliveryOrderName);

        if (null != postIPA && StringUtils.isNotBlank(dOImageId)) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage
                    .setDescription("Can not reappraise because DO is already generated for this application");
            acknowledgement = gngResponseEntityBuilder
                    .getFailureAcknowledgement(applicationRequest, errorMessage);
        } else {
            this.goNoGoCustomerApplication = applicationRepository
                    .getGoNoGoCustomerApplicationByRefId(applicationRequest
                            .getRefID(), applicationRequest.getHeader()
                            .getInstitutionId());

            this.componentConfig = componentConfigRepo.getByInstitutionIdAndType(
                    applicationRequest.getHeader().getInstitutionId(),
                    ComponentConfigurationType.RE_APPRAISE_RE_INITIATE);

            if (this.goNoGoCustomerApplication != null && this.goNoGoCustomerApplication.getApplicationRequest() != null) {

                // Get the reappraisal details from current request
                ReAppraisalDetails reAppraisalDetails = ReAppraiseReInitiateUtils
                        .getReappraisalDetails(applicationRequest,
                                this.goNoGoCustomerApplication
                                        .getApplicationRequest()
                                        .getReAppraiseDetails());

                boolean error = false;
                // Check added for Cancelled case i.e. we can not take any action on the Cancelled cases.
                if (StringUtils.equals(this.goNoGoCustomerApplication.getApplicationStatus(), GNGWorkflowConstant.CANCELLED.toFaceValue())
                        && StringUtils.equals(this.goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(), GNGWorkflowConstant.CNCLD.toFaceValue())) {

                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage
                            .setDescription("Can't reappraisal because application has been Cancelled");
                    error = true;
                    acknowledgement = gngResponseEntityBuilder
                            .getFailureAcknowledgement(goNoGoCustomerApplication.getApplicationRequest(),
                                    errorMessage);

                }else if (reAppraisalDetails.getReAppraiseCount() >= this.componentConfig
                        .getReAppraiseCount()
                        || this.goNoGoCustomerApplication.getReProcessCount() >= this.componentConfig
                        .getReProcessCnt()
                        || this.goNoGoCustomerApplication.getReInitiateCount() >= this.componentConfig
                        .getReInitiateCnt()) {
                    // If the reAppraise count is reached maximum count then
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage
                            .setDescription("Already reappraised or reinitiated for maximum allowed time");
                    error = true;
                    acknowledgement = gngResponseEntityBuilder
                            .getFailureAcknowledgement(
                                    this.goNoGoCustomerApplication
                                            .getApplicationRequest(), errorMessage);

                }
                /**
                 * If no error then update the application and send the success acknowledgement.
                 */
                if (!error) {
                    /**
                     * Set the details of the application which are common in all cases
                     * of reappraisal eg. AppStatus.
                     */
                    AuditType auditType = AuditType.RE_APPRAISE_RE_INITIATE;
                    // Save the data for audit purpose.
                    ReProcessAudit rpAudit = new ReProcessAudit();
                    boolean dataSavedForAudit = rpAudit.saveDataForAudit(
                            this.goNoGoCustomerApplication, "reprocess", auditType);
                    if (dataSavedForAudit) {
                        ReAppraiseReInitiateUtils
                                .updateReAppraisalDetailsInApplication(this.goNoGoCustomerApplication, reAppraisalDetails);
                        setGoNoGoApplicationDetails(reAppraisalDetails);
                        ReAppraiseReInitiateUtils
                                .updateReAppraiseCount(this.goNoGoCustomerApplication);
                        FinalApplicationMongoRepository finalAppRepo = new FinalApplicationMongoRepository(MongoConfig.getMongoTemplate());

                        /**
                         * Update application in the database.
                         */
                        finalAppRepo
                                .updateGoNoGoCustomerApplication(this.goNoGoCustomerApplication);

                        /**
                         * Return success acknowledgement
                         */
                        //TODO: Push to TVR

                        if (lookupService.checkActionsAccess(goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                                goNoGoCustomerApplication.getApplicationRequest().getHeader().getProduct().toProductId(),
                                ActionName.TVR_DATA_PUSH)&&!goNoGoCustomerApplication.getApplicationRequest().isQdeDecision()) {
                            /**
                             * TVR call
                             */
                            logger.debug("Going to call TVR push service for refId : {} ",goNoGoCustomerApplication.getGngRefId());

                            dmzConnector.pushTvr(goNoGoCustomerApplication.getGngRefId(),
                                    goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId(),
                                goNoGoCustomerApplication);
                        }else {
                            logger.debug("TVR criteria not match to call TVR service for refId : {} ",goNoGoCustomerApplication.getGngRefId());
                        }

                        acknowledgement = gngResponseEntityBuilder
                                .getSuccesAcknowledgement(this.goNoGoCustomerApplication
                                        .getApplicationRequest());
                    } else {
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage
                                .setDescription("ErrorSaving application for audit.");
                        acknowledgement = gngResponseEntityBuilder
                                .getFailureAcknowledgement(applicationRequest,
                                        errorMessage);
                    }
                }

            } else {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage
                        .setDescription("Application for given ref id not found");
                acknowledgement = gngResponseEntityBuilder
                        .getFailureAcknowledgement(applicationRequest, errorMessage);
            }
        }
        return acknowledgement;
    }

    /**
     * This method sets the details of the application came for reappraisal.
     * Details set her are common in all three cases of reappraisal
     *
     * @param reAppraisalDetails
     * @ReAppraisalDetails
     */
    private void setGoNoGoApplicationDetails(
            ReAppraisalDetails reAppraisalDetails) {

        this.goNoGoCustomerApplication.getApplicationRequest()
                .setReAppraiseDetails(reAppraisalDetails);

        // Set new date for the application
        this.goNoGoCustomerApplication.setDateTime(new Date());

        // Set application status as queued.
        this.goNoGoCustomerApplication
                .setApplicationStatus(GNGWorkflowConstant.QUEUED.toFaceValue());

        // Set application stage.
        this.goNoGoCustomerApplication.getApplicationRequest()
                .setCurrentStageId(GNGWorkflowConstant.CR_Q.toFaceValue());

        //Set cro ID to default to show queued application to CRO1.
        this.goNoGoCustomerApplication.getApplicationRequest().getHeader()
                .setCroId("default");

        this.goNoGoCustomerApplication.setReappraiseReq(true);
    }


    public Acknowledgement reInitiateApplication(
            ReInitiateApplicationRequest reInitiateAppRequest) throws Exception {
        GngResponseEntityBuilder gngResponseEntityBuilder = new GngResponseEntityBuilder();

        Acknowledgement acknowledgement = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        ActivityLogs activityLog =  auditHelper.createActivityLog(null, reInitiateAppRequest.getHeader());
        activityLog.setRefId(reInitiateAppRequest.getWorkFlowRestartConfiguration().getGngRefId());
        activityLog.setAction(GNGWorkflowConstant.REINITIATE.toFaceValue());

        String deliveryOrderName;
        String doObjectId;

        String gngRefId = reInitiateAppRequest.getWorkFlowRestartConfiguration().getGngRefId();

        deliveryOrderName = GngUtils.formDeliveryOrderName(gngRefId);

        doObjectId = uploadFileRepository.getImageIdByRefIdAndFileName(gngRefId, deliveryOrderName);

        // check for delivery order exist or not
        if (StringUtils.isNotBlank(doObjectId)) {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage
                    .setDescription("Can not re-initiate because DO is already generated for this application");
            acknowledgement = gngResponseEntityBuilder
                    .getFailureAcknowledgement(reInitiateAppRequest.getApplicationRequest(), errorMessage);

            activityLog.setStatus(Status.FAIL.toString());
            activityLog.setCustomMsg(errorMessage.getDescription());
        }else {
            /**
             * Get the corresponding application from the database to save it for
             * audit purpose.
             */
            this.goNoGoCustomerApplication = applicationRepository
                    .getApplicationByRefIdForAuditLog(reInitiateAppRequest
                                    .getWorkFlowRestartConfiguration().getGngRefId(),
                            CacheConstant.WEB.toString(), reInitiateAppRequest
                                    .getHeader().getInstitutionId());

            /**
             *  Get component app for the provided institution and
             *  ReAppraise_ReInitiate type.
             */
            this.componentConfig = componentConfigRepo.getByInstitutionIdAndType(
                    reInitiateAppRequest.getHeader().getInstitutionId(),
                    ComponentConfigurationType.RE_APPRAISE_RE_INITIATE);

            if (this.goNoGoCustomerApplication == null) {
                // If corresponding application is null then send the failure ack.
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage
                        .setDescription("Application for given ref id not found");

                acknowledgement = gngResponseEntityBuilder
                        .getFailureAcknowledgement(null, errorMessage);

                activityLog.setStatus(Status.FAIL.toString());
                activityLog.setCustomMsg(errorMessage.getDescription());

            } else if (StringUtils.equals(this.goNoGoCustomerApplication.getApplicationStatus(), GNGWorkflowConstant.CANCELLED.toFaceValue())
                    && StringUtils.equals(this.goNoGoCustomerApplication.getApplicationRequest().getCurrentStageId(), GNGWorkflowConstant.CNCLD.toFaceValue())) {

                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage
                        .setDescription("Can not re-initiate because application has been Cancelled.");

                acknowledgement = gngResponseEntityBuilder
                        .getFailureAcknowledgement(null, errorMessage);

                activityLog.setStatus(Status.FAIL.toString());
                activityLog.setCustomMsg(errorMessage.getDescription());

            }else if (this.goNoGoCustomerApplication.getReInitiateCount() >= this.componentConfig
                    .getReInitiateCnt()
                    || this.goNoGoCustomerApplication.getReProcessCount() >= this.componentConfig
                    .getReProcessCnt()) {
                // If application already re-initiated maximum allowed count.
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage
                        .setDescription("ReInitiated or ReAppraised already maximum allowed times "
                                + this.componentConfig.getReInitiateCnt());
                acknowledgement = gngResponseEntityBuilder
                        .getFailureAcknowledgement(this.goNoGoCustomerApplication
                                .getApplicationRequest(), errorMessage);

                activityLog.setStatus(Status.FAIL.toString());
                activityLog.setAction(GNGWorkflowConstant.REINITIATE.toFaceValue().concat("-"
                        + this.goNoGoCustomerApplication.getReInitiateCount()));
                activityLog.setCustomMsg(errorMessage.getDescription());
            } else {

                acknowledgement = restartUpdatedApplicationWorkflow(reInitiateAppRequest);
                activityLog.setStatus(Status.SUCCESS.toString());
                // Set action with reinitiate cnt
                activityLog.setAction(GNGWorkflowConstant.REINITIATE.toFaceValue().concat("-"
                        + this.goNoGoCustomerApplication.getReInitiateCount()));
            }
        }

        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        activityEventPublisher.publishEvent(activityLog);

        return acknowledgement;
    }

    public Acknowledgement executeQdeComponents(ApplicationRequest applicationRequest) throws Exception {
        Acknowledgement acknowledgement = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ActivityLogs activityLog =  auditHelper.createActivityLog(null, applicationRequest.getHeader());
        activityLog.setRefId(applicationRequest.getRefID());
        activityLog.setAction(GNGWorkflowConstant.QDE_COMPONENT.toFaceValue());


        // Save application for audit purpose in audit table
        /**
         * Get the corresponding application from the database to save it for
         * audit purpose.
         */
        this.goNoGoCustomerApplication = applicationRepository
                .getApplicationByRefIdForAuditLog(applicationRequest.getRefID(), CacheConstant.WEB.toString(),
                        applicationRequest.getHeader().getInstitutionId());

        ComponentConfigurationType configType = Utility.getConfigurationType(applicationRequest);
        this.componentConfig = componentConfigRepo.getByInstitutionIdAndType(
                applicationRequest.getHeader().getInstitutionId(),
                configType);

        AuditType auditType = Utility.getAuditType(configType);
        // Save the data for audit purpose.
        ReProcessAudit rpAudit = new ReProcessAudit();
        boolean dataSavedForAudit = rpAudit.saveDataForAudit(
                this.goNoGoCustomerApplication, "quick data entry", auditType);
        if (dataSavedForAudit) {

            // TODO : FIXME get workflow-restart config from customer and use it
            logger.debug("Application request value is {} before upsert in qdeComponent" ,applicationRequest.getCurrentStageId());

            applicationRepository.upsertApplication(applicationRequest);

            logger.debug("Application request value is {} after upsert in qdeComponent" ,applicationRequest.getCurrentStageId());
            // Run components
            // Clear interim statuses of the modules
            resetInterimStatus( componentConfig, true);
            //set score status to Default
            goNoGoCustomerApplication.setApplicationRequest(applicationRequest);
            goNoGoCustomerApplication.setApplicationStatus(Status.NEW.toString());

            ReprocessWorkFlow wf = new ReprocessWorkFlow(goNoGoCustomerApplication,componentConfig.getComponentSettings());
            wf.setSaveApplicationRequestFlag(true);
            wf.start();

            /**
             * Return success acknowledgement
             */
            acknowledgement = gngResponseEntityBuilder
                    .getSuccesAcknowledgement(applicationRequest);

        } else {
            ErrorMessage errorMessage = new ErrorMessage();
            errorMessage
                    .setDescription("ErrorSaving application for audit.");
            acknowledgement = gngResponseEntityBuilder
                    .getFailureAcknowledgement(applicationRequest,
                            errorMessage);
        }
        stopWatch.stop();
        activityLog.setDuration(stopWatch.getLastTaskTimeMillis());
        GoNoGoEventContext.getApplicationEventPublisher().publishEvent(activityLog);

        return acknowledgement;
    }

    private void resetInterimStatus(ComponentConfiguration componentConfig, boolean resetModuleResult) {
        Iterator componentItr = componentConfig.getComponentSettings().getComponentMap().values().iterator();
        List<Component> components;
        while(componentItr.hasNext()){
            components = (List<Component>)componentItr.next();
            for( Component component : components){
                component.getModuleSettingMap().entrySet().stream().forEach(e -> setInterimStatusOfModule(e, Status.DEFAULT, resetModuleResult ));
            }
        }
    }
}