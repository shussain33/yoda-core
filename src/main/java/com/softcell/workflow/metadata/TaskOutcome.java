package com.softcell.workflow.metadata;

public class TaskOutcome {

    private String taskOutcomeCode;
    private String taskMasterId;
    private String result;

    public String getTaskOutcomeCode() {
        return taskOutcomeCode;
    }

    public void setTaskOutcomeCode(String taskOutcomeCode) {
        this.taskOutcomeCode = taskOutcomeCode;
    }

    public String getTaskMasterId() {
        return taskMasterId;
    }

    public void setTaskMasterId(String taskMasterId) {
        this.taskMasterId = taskMasterId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


}
