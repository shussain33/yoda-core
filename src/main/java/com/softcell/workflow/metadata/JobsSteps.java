/**
 * Copyright@ Softcell Technologies Limited
 *
 * @date Mar 2, 2016 3:37:24 PM
 */
package com.softcell.workflow.metadata;

/**
 * @author kishorp
 *Step are order by Id;
 */
public class JobsSteps {
    private String stepName;
    private int stepId;

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

}
