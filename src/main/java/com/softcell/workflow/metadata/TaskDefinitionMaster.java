package com.softcell.workflow.metadata;

import java.util.HashMap;
import java.util.Map;

public class TaskDefinitionMaster {
    private String taskMasterId;
    private String taskCode;
    private String taskDesc;
    private String asscociatedActionCode;
    private boolean active;

    private Map<String, TaskOutcome> outComes = new HashMap<String, TaskOutcome>();


    public String getTaskMasterId() {
        return taskMasterId;
    }

    public void setTaskMasterId(String taskMasterId) {
        this.taskMasterId = taskMasterId;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

    public String getAsscociatedActionCode() {
        return asscociatedActionCode;
    }

    public void setAsscociatedActionCode(String asscociatedActionCode) {
        this.asscociatedActionCode = asscociatedActionCode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Map<String, TaskOutcome> getOutComes() {
        return outComes;
    }

    public void setOutComes(Map<String, TaskOutcome> outComes) {
        this.outComes = outComes;
    }


}
