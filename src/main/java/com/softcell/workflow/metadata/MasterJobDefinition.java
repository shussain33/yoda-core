package com.softcell.workflow.metadata;

import java.util.HashMap;
import java.util.Map;

public class MasterJobDefinition {
    private String jobMasterId;
    private String jobMasterCode;
    private boolean active;
    private Map<String, JobsSteps> steps = new HashMap<String, JobsSteps>();


    public Map<String, JobsSteps> getSteps() {
        return steps;
    }

    public void setSteps(Map<String, JobsSteps> steps) {
        this.steps = steps;
    }

    public String getJobMasterId() {
        return jobMasterId;
    }

    public void setJobMasterId(String jobMasterId) {
        this.jobMasterId = jobMasterId;
    }

    public String getJobMasterCode() {
        return jobMasterCode;
    }

    public void setJobMasterCode(String jobMasterCode) {
        this.jobMasterCode = jobMasterCode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
