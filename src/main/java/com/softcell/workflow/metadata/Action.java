package com.softcell.workflow.metadata;

import java.util.HashMap;
import java.util.Map;

public class Action {

    private String actionCode;
    private String actionName;
    private String purpose;
    private String actionType;
    private String actionExecName;

    private Map<String, ActionParam> params = new HashMap<String, ActionParam>();


    public Map<String, ActionParam> getParams() {
        return params;
    }

    public void setParams(Map<String, ActionParam> params) {
        this.params = params;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionExecName() {
        return actionExecName;
    }

    public void setActionExecName(String actionExecName) {
        this.actionExecName = actionExecName;
    }

}	
