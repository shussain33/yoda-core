package com.softcell.workflow.metadata;


public class JobSubStepOutcome {
    private String nextTask;
    private String nextStepId;

    public String getNextTask() {
        return nextTask;
    }

    public void setNextTask(String nextTask) {
        this.nextTask = nextTask;
    }

    public String getNextStepId() {
        return nextStepId;
    }

    public void setNextStepId(String nextStepId) {
        this.nextStepId = nextStepId;
    }
}
