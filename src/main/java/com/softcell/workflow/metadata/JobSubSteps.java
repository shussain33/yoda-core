package com.softcell.workflow.metadata;

import java.util.ArrayList;
import java.util.List;

public class JobSubSteps {
    List<JobSubStepOutcome> subStepOutcome = new ArrayList<JobSubStepOutcome>();
    private String taskDefId;

    public List<JobSubStepOutcome> getSubStepOutcome() {
        return subStepOutcome;
    }

    public void setSubStepOutcome(List<JobSubStepOutcome> subStepOutcome) {
        this.subStepOutcome = subStepOutcome;
    }

    public String getTaskDefId() {
        return taskDefId;
    }

    public void setTaskDefId(String taskDefId) {
        this.taskDefId = taskDefId;
    }


}
