/**
 * kishorp3:44:31 PM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow.metadata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author kishorp
 *
 */
@Document(collection = "schemeMaster")
public class SchemeMaster extends AuditEntity {

    @JsonProperty("sSchmID")
    private String scheemId;

    @JsonProperty("sSchmDesc")
    private String scheemDesc;

    @JsonProperty("sDueDt")
    private String dueDate;

    @JsonProperty("sProdFl")
    private String productFlag;

    @JsonProperty("sChargeI")
    private String chargeI;

    @JsonProperty("sChrgDesc")
    private String chargeDesc;

    @JsonProperty("sSchmName")
    private String scheemName;

    @JsonIgnore
    private boolean active = true;

    @JsonIgnore
    private Date insertDate;

    @JsonProperty("sInstId")
    private String institutionId;


    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getScheemId() {
        return scheemId;
    }

    public void setScheemId(String scheemId) {
        this.scheemId = scheemId;
    }

    public String getScheemDesc() {
        return scheemDesc;
    }

    public void setScheemDesc(String scheemDesc) {
        this.scheemDesc = scheemDesc;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }

    public String getChargeI() {
        return chargeI;
    }

    public void setChargeI(String chargeI) {
        this.chargeI = chargeI;
    }

    public String getChargeDesc() {
        return chargeDesc;
    }

    public void setChargeDesc(String chargeDesc) {
        this.chargeDesc = chargeDesc;
    }

    public String getScheemName() {
        return scheemName;
    }

    public void setScheemName(String scheemName) {
        this.scheemName = scheemName;
    }

    @Override
    public String toString() {
        return "SchemeMaster [scheemId=" + scheemId + ", scheemDesc="
                + scheemDesc + ", dueDate=" + dueDate + ", productFlag="
                + productFlag + ", chargeI=" + chargeI + ", chargeDesc="
                + chargeDesc + ", scheemName=" + scheemName + ", active="
                + active + ", insertDate=" + insertDate + ", institutionId="
                + institutionId + "]";
    }


}
