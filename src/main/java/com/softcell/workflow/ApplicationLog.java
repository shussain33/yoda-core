/**
 * kishorp5:46:46 PM  Copyright Softcell Technolgy
 **/
package com.softcell.workflow;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author kishorp
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({"_id", "pan", "aadhar", "cibil", "equifax", "experian",
        "chm", "ip", "scoring", "postIPA", "otp", "email", "deduped",
        "negativePinCode", "applicationStatus"})
public class ApplicationLog {

    @JsonProperty("_id")
    private String Id;

    @JsonProperty("pan")
    private ModuleLog pan;

    @JsonProperty("aadhar")
    private ModuleLog aadhar;

    @JsonProperty("cibil")
    private ModuleLog cibil;

    @JsonProperty("equifax")
    private ModuleLog equifax;

    @JsonProperty("experian")
    private ModuleLog experian;

    @JsonProperty("chm")
    private ModuleLog chm;

    @JsonProperty("ip")
    private String ip;

    @JsonProperty("scoring")
    private ModuleLog scoring;

    @JsonProperty("postIPA")
    private ModuleLog postIPA;

    @JsonProperty("otp")
    private ModuleLog otp;

    @JsonProperty("email")
    private ModuleLog email;

    @JsonProperty("deduped")
    private ModuleLog deduped;

    @JsonProperty("negativePinCode")
    private ModuleLog negativePinCode;

    @JsonProperty("applicationStatus")
    private String applicationStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    /**
     *
     * @return The Id
     */
    @JsonProperty("_id")
    public String getId() {
        return Id;
    }

    /**
     *
     * @param Id
     *            The _id
     */
    @JsonProperty("_id")
    public void setId(String Id) {
        this.Id = Id;
    }

    /**
     *
     * @return The pan
     */
    @JsonProperty("pan")
    public ModuleLog getPan() {
        return pan;
    }

    /**
     *
     * @param pan
     *            The pan
     */
    @JsonProperty("pan")
    public void setPan(ModuleLog pan) {
        this.pan = pan;
    }

    /**
     *
     * @return The aadhar
     */
    @JsonProperty("aadhar")
    public ModuleLog getAadhar() {
        return aadhar;
    }

    /**
     *
     * @param aadhar
     *            The aadhar
     */
    @JsonProperty("aadhar")
    public void setAadhar(ModuleLog aadhar) {
        this.aadhar = aadhar;
    }

    /**
     *
     * @return The cibil
     */
    @JsonProperty("cibil")
    public ModuleLog getCibil() {
        return cibil;
    }

    /**
     *
     * @param cibil
     *            The cibil
     */
    @JsonProperty("cibil")
    public void setCibil(ModuleLog cibil) {
        this.cibil = cibil;
    }

    /**
     *
     * @return The equifax
     */
    @JsonProperty("equifax")
    public ModuleLog getEquifax() {
        return equifax;
    }

    /**
     *
     * @param equifax
     *            The equifax
     */
    @JsonProperty("equifax")
    public void setEquifax(ModuleLog equifax) {
        this.equifax = equifax;
    }

    /**
     *
     * @return The experian
     */
    @JsonProperty("experian")
    public ModuleLog getExperian() {
        return experian;
    }

    /**
     *
     * @param experian
     *            The experian
     */
    @JsonProperty("experian")
    public void setExperian(ModuleLog experian) {
        this.experian = experian;
    }

    /**
     *
     * @return The chm
     */
    @JsonProperty("chm")
    public ModuleLog getChm() {
        return chm;
    }

    /**
     *
     * @param chm
     *            The chm
     */
    @JsonProperty("chm")
    public void setChm(ModuleLog chm) {
        this.chm = chm;
    }

    /**
     *
     * @return The ip
     */
    @JsonProperty("ip")
    public String getIp() {
        return ip;
    }

    /**
     *
     * @param ip
     *            The ip
     */
    @JsonProperty("ip")
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     *
     * @return The scoring
     */
    @JsonProperty("scoring")
    public ModuleLog getScoring() {
        return scoring;
    }

    /**
     *
     * @param scoring
     *            The scoring
     */
    @JsonProperty("scoring")
    public void setScoring(ModuleLog scoring) {
        this.scoring = scoring;
    }

    /**
     *
     * @return The postIPA
     */
    @JsonProperty("postIPA")
    public ModuleLog getPostIPA() {
        return postIPA;
    }

    /**
     *
     * @param postIPA
     *            The postIPA
     */
    @JsonProperty("postIPA")
    public void setPostIPA(ModuleLog postIPA) {
        this.postIPA = postIPA;
    }

    /**
     *
     * @return The otp
     */
    @JsonProperty("otp")
    public ModuleLog getOtp() {
        return otp;
    }

    /**
     *
     * @param otp
     *            The otp
     */
    @JsonProperty("otp")
    public void setOtp(ModuleLog otp) {
        this.otp = otp;
    }

    /**
     *
     * @return The email
     */
    @JsonProperty("email")
    public ModuleLog getEmail() {
        return email;
    }

    /**
     *
     * @param email
     *            The email
     */
    @JsonProperty("email")
    public void setEmail(ModuleLog email) {
        this.email = email;
    }

    /**
     *
     * @return The deduped
     */
    @JsonProperty("deduped")
    public ModuleLog getDeduped() {
        return deduped;
    }

    /**
     *
     * @param deduped
     *            The deduped
     */
    @JsonProperty("deduped")
    public void setDeduped(ModuleLog deduped) {
        this.deduped = deduped;
    }

    /**
     *
     * @return The negativePinCode
     */
    @JsonProperty("negativePinCode")
    public ModuleLog getNegativePinCode() {
        return negativePinCode;
    }

    /**
     *
     * @param negativePinCode
     *            The negativePinCode
     */
    @JsonProperty("negativePinCode")
    public void setNegativePinCode(ModuleLog negativePinCode) {
        this.negativePinCode = negativePinCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ApplicationLog [Id=");
        builder.append(Id);
        builder.append(", pan=");
        builder.append(pan);
        builder.append(", aadhar=");
        builder.append(aadhar);
        builder.append(", cibil=");
        builder.append(cibil);
        builder.append(", equifax=");
        builder.append(equifax);
        builder.append(", experian=");
        builder.append(experian);
        builder.append(", chm=");
        builder.append(chm);
        builder.append(", ip=");
        builder.append(ip);
        builder.append(", scoring=");
        builder.append(scoring);
        builder.append(", postIPA=");
        builder.append(postIPA);
        builder.append(", otp=");
        builder.append(otp);
        builder.append(", email=");
        builder.append(email);
        builder.append(", deduped=");
        builder.append(deduped);
        builder.append(", negativePinCode=");
        builder.append(negativePinCode);
        builder.append(", applicationStatus=");
        builder.append(applicationStatus);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Id == null) ? 0 : Id.hashCode());
        result = prime * result + ((aadhar == null) ? 0 : aadhar.hashCode());
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime
                * result
                + ((applicationStatus == null) ? 0 : applicationStatus
                .hashCode());
        result = prime * result + ((chm == null) ? 0 : chm.hashCode());
        result = prime * result + ((cibil == null) ? 0 : cibil.hashCode());
        result = prime * result + ((deduped == null) ? 0 : deduped.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((equifax == null) ? 0 : equifax.hashCode());
        result = prime * result
                + ((experian == null) ? 0 : experian.hashCode());
        result = prime * result + ((ip == null) ? 0 : ip.hashCode());
        result = prime * result
                + ((negativePinCode == null) ? 0 : negativePinCode.hashCode());
        result = prime * result + ((otp == null) ? 0 : otp.hashCode());
        result = prime * result + ((pan == null) ? 0 : pan.hashCode());
        result = prime * result + ((postIPA == null) ? 0 : postIPA.hashCode());
        result = prime * result + ((scoring == null) ? 0 : scoring.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApplicationLog other = (ApplicationLog) obj;
        if (Id == null) {
            if (other.Id != null)
                return false;
        } else if (!Id.equals(other.Id))
            return false;
        if (aadhar == null) {
            if (other.aadhar != null)
                return false;
        } else if (!aadhar.equals(other.aadhar))
            return false;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (applicationStatus == null) {
            if (other.applicationStatus != null)
                return false;
        } else if (!applicationStatus.equals(other.applicationStatus))
            return false;
        if (chm == null) {
            if (other.chm != null)
                return false;
        } else if (!chm.equals(other.chm))
            return false;
        if (cibil == null) {
            if (other.cibil != null)
                return false;
        } else if (!cibil.equals(other.cibil))
            return false;
        if (deduped == null) {
            if (other.deduped != null)
                return false;
        } else if (!deduped.equals(other.deduped))
            return false;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (equifax == null) {
            if (other.equifax != null)
                return false;
        } else if (!equifax.equals(other.equifax))
            return false;
        if (experian == null) {
            if (other.experian != null)
                return false;
        } else if (!experian.equals(other.experian))
            return false;
        if (ip == null) {
            if (other.ip != null)
                return false;
        } else if (!ip.equals(other.ip))
            return false;
        if (negativePinCode == null) {
            if (other.negativePinCode != null)
                return false;
        } else if (!negativePinCode.equals(other.negativePinCode))
            return false;
        if (otp == null) {
            if (other.otp != null)
                return false;
        } else if (!otp.equals(other.otp))
            return false;
        if (pan == null) {
            if (other.pan != null)
                return false;
        } else if (!pan.equals(other.pan))
            return false;
        if (postIPA == null) {
            if (other.postIPA != null)
                return false;
        } else if (!postIPA.equals(other.postIPA))
            return false;
        if (scoring == null) {
            if (other.scoring != null)
                return false;
        } else if (!scoring.equals(other.scoring))
            return false;
        return true;
    }

}
