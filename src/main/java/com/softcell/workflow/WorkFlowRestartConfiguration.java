/**
 * kishorp1:06:45 AM  Copyright Softcell Technology
 **/
package com.softcell.workflow;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author kishorp
 *
 */
public class WorkFlowRestartConfiguration {

    @JsonProperty("oHeader")
    @NotNull(groups = {Header.FetchGrp.class})
    @Valid
    private Header header;

    @JsonProperty("sGngRefId")
    @NotEmpty(groups = {WorkFlowRestartConfiguration.FetchGrp.class,WorkFlowRestartConfiguration.ReInitGrp.class})
    private String gngRefId;

    /*ModuleConfiguration is list of all module need to run*/
    @JsonProperty("aModuleConfig")
    private List<ModuleConfiguration> moduleConfiguration;

    /**
     * @return the moduleConfiguration
     */
    public List<ModuleConfiguration> getModuleConfiguration() {
        return moduleConfiguration;
    }

    /**
     * @param moduleConfiguration the moduleConfiguration to
     *        list of the modules that need to be run in work flow
     *        if application type is re-run.
     */
    public void setModuleConfiguration(List<ModuleConfiguration> moduleConfiguration) {
        this.moduleConfiguration = moduleConfiguration;
    }

    public String getGngRefId() {
        return gngRefId;
    }

    public void setGngRefId(String gngRefId) {
        this.gngRefId = gngRefId;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }


    public interface FetchGrp {
    }

    public interface ReInitGrp {
    }
}
