package com.softcell.workflow.utils;

import java.util.HashMap;

/**
 * @author kishor
 *         This class use to identify bean or executer name which  are defined in to
 *         work flow app file.
 */
public class ReprocessExecutionMapper {

    private static HashMap<String, String> executionIdMapping = new HashMap<>();

    static {
        executionIdMapping.put("101", "multiBureauExecutor");
        executionIdMapping.put("201", "panVarificationExecuter");
        executionIdMapping.put("202", "aadharExecutor");
        executionIdMapping.put("301", "DedupeExecuter");
        executionIdMapping.put("302", "NegativeDedupe");
        executionIdMapping.put("401", "verificationScoring");
        executionIdMapping.put("402", "scoringExecutor");
        executionIdMapping.put("501", "salesforceExecutor");
        executionIdMapping.put("601", "amazonS3ServiceExecutor");
        executionIdMapping.put("701", "saathiApiExecutor");
        executionIdMapping.put("801", "creditVidyaApiExecutor");
    }

    private ReprocessExecutionMapper() {

    }

    /**
     * @param reprocessingModuleId Executer  Id will map with work flow mapping.
     *                             <p/>
     *                             sModuleName:
     *                             mb:cibil: 101
     *                             kyc: pan:201
     *                             kyc: adhar:202
     *                             dedupe:minidedupe:301
     *                             dedupe:negPincode:302
     *                             SOBRE: Verscoring: 401
     *                             SOBRE: application scoring: 402
     * @return will return executer classId.
     */

    public static String getExecutionMapping(String reprocessingModuleId) {
        return executionIdMapping.get(reprocessingModuleId);
    }

}
