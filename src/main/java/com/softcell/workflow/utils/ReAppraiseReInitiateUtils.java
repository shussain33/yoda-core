package com.softcell.workflow.utils;

import com.softcell.config.ReAppraisalConfig;
import com.softcell.constants.ReAppraisalReason;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.core.GoNoGoCroApplicationResponse;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.ReAppraisalDetails;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.response.core.ErrorMessage;

/**
 * @author kishor
 */
public class ReAppraiseReInitiateUtils {
    /**
     * @param applicationResponse data of application and customer.
     * @return number of configured time allowed re-try else
     * <em>-1<em> if not configured.
     */
    public static int getNumberOfReTry(GoNoGoCroApplicationResponse applicationResponse) {
        if (applicationResponse != null && applicationResponse.getApplicationRequest() != null &&
                applicationResponse.getApplicationRequest().getHeader() != null) {
            ReAppraisalConfig reAppraisalConfig = Cache.NUMBER_OF_RE_TRY.
                    get(applicationResponse.getApplicationRequest().getHeader()
                            .getInstitutionId());
            return reTry(reAppraisalConfig);
        }
        return -1;
    }

    private static int reTry(ReAppraisalConfig reAppraisalConfig) {

        if (reAppraisalConfig != null) {
            return reAppraisalConfig.getAllowedReTry();
        }
        return -1;
    }


    /**
     * @return ReAppraisalDetails.
     */
    public static ReAppraisalDetails getReappraisalDetails(
            ApplicationRequest applicationRequest, ReAppraisalDetails prevReApprDetails) {

        ReAppraisalDetails reAppraisalDetails = new ReAppraisalDetails();

        //Set count to current ReAppraisal count.
        if (prevReApprDetails != null) {
            reAppraisalDetails.setReAppraiseCount(prevReApprDetails.getReAppraiseCount());
        }
        reAppraisalDetails.setReappraiseReason(applicationRequest
                .getReAppraiseDetails().getReappraiseReason());
        reAppraisalDetails.setReappraiseRemark(applicationRequest
                .getReAppraiseDetails().getReappraiseRemark());
        reAppraisalDetails.setAdditionalsurrogate(applicationRequest
                .getReAppraiseDetails().getAdditionalsurrogate());
        reAppraisalDetails.setHigherLoanAmount(applicationRequest
                .getReAppraiseDetails().getHigherLoanAmount());

        return reAppraisalDetails;
    }

    public static void updateReAppraisalDetailsInApplication(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            ReAppraisalDetails reAppraisalDetails) {

        ReAppraisalReason reapprReason = ReAppraisalReason
                .valueOf(reAppraisalDetails.getReappraiseReason());
        // set reAppraiseApp to false because the reAppraisal application is
        // being re-initiated here.
        switch (reapprReason) {

            case ADDITIONAL_SURROGATE:
                // Update surrogate in application
                goNoGoCustomerApplication.getApplicationRequest().getRequest()
                        .getApplicant()
                        .setSurrogate(reAppraisalDetails.getAdditionalsurrogate());
                break;

            case HIGHER_LOAN_AMOUNT:
                // Update loan amount in application.
                goNoGoCustomerApplication.getApplicationRequest().getRequest()
                        .getApplication()
                        .setLoanAmount(reAppraisalDetails.getHigherLoanAmount());
                break;

            case RELOOK:
                // Currently only status of the application needs to be changed
                // to queue for relook which had done before switch case if
                // anything extra needs to be done for this case can be written
                // here
                break;

            default:
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setDescription("Not supported " + reapprReason);
                break;
        }
    }

    public static boolean isReAppraisalApplication(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {
        // If reAppraisal count is greater than zero and re-initiate count is
        // less than or equal to reAppraisal count then application was
        // submitted for reAppraisal and then CRO is re-initiating
        if (goNoGoCustomerApplication.getApplicationRequest()
                .getReAppraiseDetails() != null) {
            if (goNoGoCustomerApplication.getApplicationRequest()
                    .getReAppraiseDetails().getReAppraiseCount() > 0
                    && goNoGoCustomerApplication.getReInitiateCount() <= goNoGoCustomerApplication
                    .getApplicationRequest().getReAppraiseDetails()
                    .getReAppraiseCount()) {
                return true;
            }
        }
        return false;
    }

    public static void updateReAppraiseCount(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {

        int reAppraiseCount = goNoGoCustomerApplication.getApplicationRequest()
                .getReAppraiseDetails().getReAppraiseCount();

        goNoGoCustomerApplication.getApplicationRequest()
                .getReAppraiseDetails().setReAppraiseCount(reAppraiseCount + 1);
    }
}
