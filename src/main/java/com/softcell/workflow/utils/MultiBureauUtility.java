package com.softcell.workflow.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.gonogo.model.core.request.scoring.CibilRespScore;
import com.softcell.gonogo.model.core.request.scoring.CibilResponse;
import com.softcell.gonogo.model.multibureau.experian.InProfileResponse;
import com.softcell.gonogo.model.multibureau.pickup.Finished;
import com.softcell.gonogo.model.multibureau.pickup.ResponseMultiJsonDomain;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used for multibureau utility method
 *
 * @author bhuvneshk
 */
public class MultiBureauUtility {

    private static final Logger logger = LoggerFactory.getLogger(MultiBureauUtility.class);

    public static Map<String, String> getScore(ResponseMultiJsonDomain multiBureauResponse) throws Exception {

        Map<String, String> bureauScore = new HashMap<String, String>();
        if (multiBureauResponse != null) {

            List<Finished> finishedList = multiBureauResponse.getFinishedList();

            if (finishedList != null && !finishedList.isEmpty()) {
                for (Finished finished : finishedList) {

                    if (finished.getBureau().equals(GNGWorkflowConstant.CIBIL.name())) {

                        String cibilObj = new Gson().toJson(finished
                                .getResponseJsonObject());

                        CibilResponse cibilResponse = new Gson().fromJson(cibilObj,
                                CibilResponse.class);

                        List<CibilRespScore> cibilScoreList = cibilResponse.getScoreList();

                        if (cibilScoreList != null && !cibilScoreList.isEmpty()) {
                            CibilRespScore cibilScore = cibilScoreList.get(0);

                            if (cibilScore != null) {
                                String score = cibilScore.getScore();
                                bureauScore.put(GNGWorkflowConstant.CIBIL.name(), score);
                            }

                        }
                        break;
                    } else if (finished.getBureau().equals(GNGWorkflowConstant.HIGHMARK.name())) {
                        retrieveHighmarkScore(finished, bureauScore);
                        break;
                    }
                }// loop
            }
        }
        return bureauScore;
    }

    private static void retrieveHighmarkScore(Finished finished, Map<String, String> bureauScore) throws Exception{
        InProfileResponse highmarkResponse = null;

        String responseJsonObject = getBureauResponse(finished.getResponseJsonObject());
        if(StringUtils.isNotBlank(responseJsonObject)) {

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            highmarkResponse = objectMapper.readValue(responseJsonObject, InProfileResponse.class);

            if (null != highmarkResponse && null != highmarkResponse.getScore()) {
                bureauScore.put(GNGWorkflowConstant.HIGHMARK.name(), highmarkResponse.getScore().getBureauScore());
            }
        }
    }

    public static String getBureauResponse(Object bureauResponseObject) {
        String bureauResponseString = "";
        if (null != bureauResponseObject) {
            if (bureauResponseObject instanceof LinkedHashMap) {
                try {
                    bureauResponseString = new ObjectMapper().writeValueAsString(bureauResponseObject);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                    logger.error("Error converting bureau response object to string while sending to Salesforce cause");
                }
            } else {
                //it is json string with escape character.
                bureauResponseString = bureauResponseObject.toString().replace("\\", "");
            }
        }
        return bureauResponseString;
    }

}
