package com.softcell.workflow.utils;

import com.softcell.constants.ApplicantType;
import com.softcell.constants.AuditType;
import com.softcell.constants.ComponentConfigurationType;
import com.softcell.constants.FieldSeparator;
import com.softcell.gonogo.model.request.ApplicationRequest;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by archana on 7/8/17.
 */
public class Utility {

    public static ComponentConfigurationType getConfigurationType(ApplicationRequest applicationRequest) {
        ApplicantType applicantType = ApplicantType.get(applicationRequest.getApplicantType());
        ComponentConfigurationType configType ;
        switch(applicantType){
            case EXPRESS: configType = ComponentConfigurationType.EXPRESS_QUICK_DATA_ENTRY;
                break;
            case EXISTING: configType = ComponentConfigurationType.EXISTING_QUICK_DATA_ENTRY;
                break;
            case SOLICITED: configType = ComponentConfigurationType.SOLICITED_QUICK_DATA_ENTRY;
                break;
            default: configType = ComponentConfigurationType.BASIC;
                break;
        }

        return configType;
    }

    private Utility(){

    }

    public static AuditType getAuditType(ComponentConfigurationType configType) throws Exception{
        AuditType auditType;
        switch(configType){
            case EXPRESS_QUICK_DATA_ENTRY: auditType = AuditType.EXPRESS_QDE;
                break;
            case EXISTING_QUICK_DATA_ENTRY: auditType = AuditType.EXISTING_QDE;
                break;
            case SOLICITED_QUICK_DATA_ENTRY: auditType = AuditType.SOLICITED_QDE;
                break;
            default: throw new Exception("Applicant is normal");
        }

        return auditType;
    }


    public static AuditType getAuditType(String applicantType) throws Exception{
        AuditType auditType ;
        if( applicantType.equalsIgnoreCase(ApplicantType.EXPRESS.value())){
            auditType = AuditType.EXPRESS_QDE;
        } else if( applicantType.equalsIgnoreCase(ApplicantType.SOLICITED.value())){
            auditType = AuditType.SOLICITED_QDE;
        } else if( applicantType.equalsIgnoreCase(ApplicantType.EXISTING.value())){
            auditType = AuditType.EXISTING_QDE;
        } else{
            throw new Exception("Normal customer !!");
        }
        return auditType;
    }
    public static String getJoinedUrl(String baseUrl, String endPoint) {
        return Arrays.asList(baseUrl, endPoint)
                .parallelStream().collect(Collectors.joining(FieldSeparator.FORWARD_SLASH));
    }
}
