/**
 * MultiBureauResponseServiceCallbackHandler.java
 * <p/>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */
package com.softcell.connector.soap.los.mb;


/**
 *  MultiBureauResponseServiceCallbackHandler Callback class, Users can extend this class and implement
 *  their own receiveResult and receiveError methods.
 */
public abstract class MultiBureauResponseServiceCallbackHandler {
    protected Object clientData;

    /**
     * User can pass in any object that needs to be accessed once the NonBlocking
     * Web service call is finished and appropriate method of this CallBack is called.
     * @param clientData Object mechanism by which the user can pass in user data
     * that will be avilable at the time this callback is called.
     */
    public MultiBureauResponseServiceCallbackHandler(Object clientData) {
        this.clientData = clientData;
    }

    /**
     * Please use this constructor if you don't want to set any clientData
     */
    public MultiBureauResponseServiceCallbackHandler() {
        this.clientData = null;
    }

    /**
     * Get the client data
     */
    public Object getClientData() {
        return clientData;
    }

    /**
     * auto generated Axis2 call back method for postMBResponse method
     * override this method for handling normal response from postMBResponse operation
     */
    public void receiveResultpostMBResponse(
            com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauResponseAck result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from postMBResponse operation
     */
    public void receiveErrorpostMBResponse(java.lang.Exception e) {
    }

    /**
     * auto generated Axis2 call back method for postEndOfTransaction method
     * override this method for handling normal response from postEndOfTransaction operation
     */
    public void receiveResultpostEndOfTransaction(
            com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauEoTAck result) {
    }

    /**
     * auto generated Axis2 Error handler
     * override this method for handling error response from postEndOfTransaction operation
     */
    public void receiveErrorpostEndOfTransaction(java.lang.Exception e) {
    }
}
