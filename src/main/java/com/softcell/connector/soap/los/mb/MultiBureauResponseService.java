/**
 * MultiBureauResponseService.java
 * <p/>
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */
package com.softcell.connector.soap.los.mb;


/*
 *  MultiBureauResponseService java interface
 */
public interface MultiBureauResponseService {
    /**
     * Auto generated method signature
     *
     * @param multiBureauResponse0
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauResponseAck postMBResponse(
            com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauResponse multiBureauResponse0)
            throws java.rmi.RemoteException;

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @param multiBureauResponse0
     */
    public void startpostMBResponse(
            com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauResponse multiBureauResponse0,
            final com.softcell.connector.soap.los.mb.MultiBureauResponseServiceCallbackHandler callback)
            throws java.rmi.RemoteException;

    /**
     * Auto generated method signature
     *
     * @param multiBureauEoTRequest2
     */
    public com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauEoTAck postEndOfTransaction(
            com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauEoTRequest multiBureauEoTRequest2)
            throws java.rmi.RemoteException;

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @param multiBureauEoTRequest2
     */
    public void startpostEndOfTransaction(
            com.softcell.soap.mb.hdfcbank.xsd.multibureau.MultiBureauEoTRequest multiBureauEoTRequest2,
            final com.softcell.connector.soap.los.mb.MultiBureauResponseServiceCallbackHandler callback)
            throws java.rmi.RemoteException;

    //
}
