package com.softcell.reporting.builder;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by mahesh on 16/2/17.
 */
public class SerialNumberReportConfiguration {

    public static SortedMap<Integer, ColumnConfiguration> serialLogConfigMap = new TreeMap();

    static {
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ReferenceId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Vendor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_vendor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DealerId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("SKUCode");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_skuCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Serial Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_serialNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ImeiNumber");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_imeiNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("MaterialName");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_materialName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ProductCode");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_productCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("StoreCode");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_storeCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Original Response");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_originalResponse999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("MPN");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_mpn999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("StoreAppleId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_storeAppleId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Bank Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_bankName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Tenure");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_tenure999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_scheme999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Channel Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_channelCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Oppo Dealer Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_oppoDealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Distributor Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_distributorId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Custom Message");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_customMsg999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("RDS Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_rdsCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Material Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_materialCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("SR_Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_serviceRequestNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Customer_Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_customerName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Product_Category");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_productCategory999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Unit_Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_unitStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Purchase_Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_purchaseDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Alternate_Contact");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_alternateContact999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Customer_Email");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_customerEmail999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Area");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_area999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Branch_Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_branchName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Call_Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_callType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Closed_Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_closedDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Franchisee");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_franchisee999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Lot_Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_lotNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Open_Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_openDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("SR_Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_serviceRequestStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Postal_Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_postalCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Output");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_output999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Deal_Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_dealId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialLogConfigMap.put(index, columnConfiguration);
        index++;

    }

}
