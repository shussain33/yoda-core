package com.softcell.reporting.builder;

import com.softcell.config.reporting.ReportingConfigurations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;


/**
 * @author kishorp
 */
public class ReportingQueryBuilder {
    /**
     * @param reportingConfigurations exposed json to client
     *                                <p/>
     *                                { oHeader : { sAppID:"", sInstID:"", sSourceID:"",
     *                                sAppSource:"", sReqType:"", dtSubmit:"", sDsaId:"", sCroId:"",
     *                                sDealerId:"", }, sReportId : '', aProductType: [], aFlatConfig
     *                                : { mHeaderMap : [ 0:{ columnKey:"", columnDisplayName:"",
     *                                columnIndex:"", isViewable:true, isDownloadable:true }, 1:{
     *                                columnKey:"", columnDisplayName:"", columnIndex:"",
     *                                isViewable:true, isDownloadable:true } ], reportName:"",
     *                                reportType :"", reportFormat : "", header : "", seperater : ""
     *                                }, sBranchId : "", sUserId : "", oPaggination : { iPageId :0,
     *                                iLimit: 0, iSkip : 0 },
     *                                <p/>
     *                                <p/>
     *                                }
     * @return qury to update record in Collection.
     */
    public static Query getUpdateQueryReportConfig(
            ReportingConfigurations reportingConfigurations) {
        Query query = new Query();
        return query.addCriteria(Criteria.where("reportName")
                .is(reportingConfigurations.getReportName())
                .and("header.institutionId")
                .is(reportingConfigurations.getHeader().getInstitutionId())
                .and("userId").is(reportingConfigurations.getUserId()));

    }
}
