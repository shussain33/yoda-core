package com.softcell.reporting.builder;


import com.softcell.constants.LoyaltyCardType;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.core.scoring.response.DecisionResponse;
import com.softcell.gonogo.model.core.scoring.response.Eligibility;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.core.InvoiceDetails;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.gonogo.model.response.ComponentResponse;
import com.softcell.gonogo.model.surrogate.Surrogate;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngNumUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;

/**
 * @author kishor This class is use to transform POJO of gonogo application to
 *         flat JSON.
 */
public class GoNoGoApplicationToJsonParser {

    private PojoToJSonTransformer jSonTransformer;
    private GoNoGoCustomerApplication customerApplication;

    /**
     * @param customerApplication Customer full detail domains which has all details including a
     *                            work-flow, cro and other details.
     */
    public GoNoGoApplicationToJsonParser(
            GoNoGoCustomerApplication customerApplication) {
        this.customerApplication = customerApplication;
        this.jSonTransformer = new PojoToJSonTransformer<>();
    }

    private GoNoGoApplicationToJsonParser() {
    }

    /**
     * @return All Key which are not having null values. if value is null then
     * its not available in key set.
     */
    public Set getAvailableKesy() {
        return jSonTransformer.keySet();
    }

    /**
     * @return JSON structure with first level keys of complete Customer
     */
    public PojoToJSonTransformer build() {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setGoNoGoApplicationField();
        return jSonTransformer;
    }

    private void setGoNoGoApplicationField() {
        if (customerApplication != null) {
            jSonTransformer.build(customerApplication, "A", jSonTransformer);
            jSonTransformer.build(customerApplication.getApplScoreVector(),
                    "A", jSonTransformer);
            setApplicationRequest(customerApplication.getApplicationRequest());
            setCRODecision(customerApplication.getCroDecisions());
            setDeupList(customerApplication.getDedupedApplications());
            setJsonComponents(customerApplication.getApplicantComponentResponse());
            jSonTransformer.build(customerApplication.getLosDetails(), "LOS",
                    jSonTransformer);
            jSonTransformer.build(customerApplication.getCroJustification(),
                    "CRO_JUST", jSonTransformer);
            jSonTransformer.build(customerApplication.getIntrimStatus()
                    .getCibilModuleResult(), "cibil", jSonTransformer);

            jSonTransformer.build(customerApplication.getIntrimStatus()
                    .getExperianModuleResult(), "experian", jSonTransformer);
            if (null != customerApplication.getInvoiceDetails()) {
                jSonTransformer.build(customerApplication.getInvoiceDetails(), "invoice", jSonTransformer);

            }

        }
    }

    private void setJsonComponents(ComponentResponse applicantComponentResponse) {

        if (null != applicantComponentResponse) {
            if (null != applicantComponentResponse.getScoringServiceResponse()) {
                setEligibility(applicantComponentResponse.getScoringServiceResponse().getEligibilityResponse());
                setDecisionResponse(applicantComponentResponse.getScoringServiceResponse().getDecisionResponse());
            }
        }

    }

    private void setDecisionResponse(DecisionResponse decisionResponse) {
        if (null!=decisionResponse){
            jSonTransformer.build(decisionResponse, "sd",jSonTransformer);
        }
    }

    private void setEligibility(Eligibility eligibilityResponse) {
        if (null != eligibilityResponse) {

            String GridId=String.valueOf(eligibilityResponse.getGridId() !=null? eligibilityResponse.getGridId():
                    (eligibilityResponse.getRuleExucSeq() != null ? eligibilityResponse.getRuleExucSeq() :" "));
            jSonTransformer.put("eligibilityGrid",eligibilityResponse.getEligibleId() != null ? eligibilityResponse.getEligibleId() +"."+GridId : GridId );


            jSonTransformer.build(eligibilityResponse, "se", jSonTransformer);
        }

    }

    private void setDeupList(Set<String> dedupedApplications) {
        if (dedupedApplications != null)
            jSonTransformer
                    .build(dedupedApplications, "dedup", jSonTransformer);
    }

    private void setCRODecision(List<CroDecision> croDecisions) {
        if (croDecisions != null)
            jSonTransformer.build(croDecisions, "CRO", jSonTransformer);

    }

    private void setApplicationRequest(ApplicationRequest applicationRequest) {
        if (applicationRequest != null) {
            jSonTransformer.build(applicationRequest, "appRequest",
                    jSonTransformer);
            jSonTransformer.build(applicationRequest.getHeader(), "header",
                    jSonTransformer);
            setRequest(applicationRequest.getRequest());
        }

    }

    private void setRequest(Request request) {
        if (request != null) {
            jSonTransformer.build(request, "request", jSonTransformer);
            setApplicant(request.getApplicant());
            setapplication(request.getApplication());
        }
    }

    private void setapplication(Application application) {
        if (application != null) {
            jSonTransformer.build(application, "C_L_A", jSonTransformer);
            jSonTransformer.build(application.getProperty(), "C_L_P",
                    jSonTransformer);
            jSonTransformer.build(application.getAsset(), "C_L_A",
                    jSonTransformer);
            jSonTransformer.build(application.getOwnedAsset(), "C_L_A",
                    jSonTransformer);
        }

    }

    private void setApplicant(Applicant applicant) {
        if (applicant != null) {
            jSonTransformer.build(applicant, "C_A", jSonTransformer);
            jSonTransformer.build(applicant.getApplicantName(), "C_N",
                    jSonTransformer);
            jSonTransformer.build(applicant.getFatherName(), "F_N",
                    jSonTransformer);
            jSonTransformer.build(applicant.getMotherName(), "M_N",
                    jSonTransformer);
            jSonTransformer.build(applicant.getSpouseName(), "S_N",
                    jSonTransformer);
            jSonTransformer.build(applicant.getKyc(), "C_KYC", jSonTransformer);
            jSonTransformer.build(applicant.getAddress(), "C", jSonTransformer);

            jSonTransformer.build(applicant.getPhone(), "C", jSonTransformer);
            jSonTransformer.build(applicant.getEmail(), "C", jSonTransformer);
            jSonTransformer.build(applicant.getEmployment(), "C",
                    jSonTransformer);
            jSonTransformer.build(applicant.getBankingDetails(), "C",
                    jSonTransformer);
            jSonTransformer.build(applicant.getLoanDetails(), "C",
                    jSonTransformer);
            jSonTransformer.build(applicant.getIncomeDetails(), "C",
                    jSonTransformer);
            jSonTransformer.put("isAadharVerified", applicant.isAadhaarVerified() ? "Y" : "N");
            //setApplicationReference(applicant.getApplicantReference());
            setSurrogate(applicant.getSurrogate());
            setIncomeDetails(applicant.getIncomeDetails(), "LMI", jSonTransformer);
        }

    }

    private void setIncomeDetails(IncomeDetails incomeDetails, String lmi, PojoToJSonTransformer jSonTransformer) {
        if (null != incomeDetails) {
            setSalariedDetails(incomeDetails.getSalariedDetails(), "S_D", jSonTransformer);
        }
    }

    private void setSalariedDetails(SalariedDetails salariedDetails, String s_d, PojoToJSonTransformer jSonTransformer) {
        if(null!=salariedDetails){
            jSonTransformer.build(salariedDetails.getLastMonthIncomeList(), "M_I",
                    jSonTransformer);
        }
    }

    private void setSurrogate(Surrogate surrogate) {
        if (surrogate != null) {
            jSonTransformer.build(surrogate.getCarSurrogate(), "C_S",
                    jSonTransformer);
            jSonTransformer.build(surrogate.getCreditCardSurrogate(), "C_S",
                    jSonTransformer);
            jSonTransformer.build(surrogate.getHouseSurrogate(), "C_S",
                    jSonTransformer);
            jSonTransformer.build(surrogate.getSalariedSurrogate(), "C_S",
                    jSonTransformer);
            jSonTransformer.build(surrogate.getTraderSurrogate(), "C_S",
                    jSonTransformer);
        }

    }

    private void setApplicationReference(ApplicantReference applicantReference) {
        if (applicantReference != null) {
            jSonTransformer.build(applicantReference.getName(), "A_R",
                    jSonTransformer);
            jSonTransformer.build(applicantReference.getAddress(), "A_R",
                    jSonTransformer);
            jSonTransformer.build(applicantReference.getPhone(), "A_R",
                    jSonTransformer);
            jSonTransformer.build(applicantReference, "A_R", jSonTransformer);
        }
    }

    /**
     * @return JSON structure with first level keys of complete Customer
     */
    public PojoToJSonTransformer build(PostIPA postIPA) {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setPostIPA(postIPA);
        return jSonTransformer;
    }

    public PojoToJSonTransformer build(PostIpaRequest postIpaRequest) {
        if (null == jSonTransformer)
            jSonTransformer = new PojoToJSonTransformer();
        setPostIPA(postIpaRequest.getPostIPA());
        setPostIPADate(postIpaRequest);
        return jSonTransformer;
    }

    public PojoToJSonTransformer build(SerialNumberInfo serialNumberInfo) {

        if (null != serialNumberInfo) {
            if (StringUtils.isNotBlank(serialNumberInfo.getSerialNumber())) {

                jSonTransformer.put("serialNumber_imei", serialNumberInfo.getSerialNumber());
            } else {
                jSonTransformer.put("serialNumber_imei", serialNumberInfo.getImeiNumber());
            }
            jSonTransformer.put("serialNumber_imei_status", serialNumberInfo.getStatus());
        }
        return jSonTransformer;
    }

    public PojoToJSonTransformer build(FileHandoverDetails filehandoverdetails) {
        if (null != filehandoverdetails) {

            if (null != filehandoverdetails.getHandoverDate()) {
                jSonTransformer.put("handoverDate", GngDateUtil.changeDateFormat(filehandoverdetails.getHandoverDate(), "MM/dd/yyyy"));
            }
            if (null != filehandoverdetails.getDisbursementDate()) {
                jSonTransformer.put("disbursementDate", GngDateUtil.changeDateFormat(filehandoverdetails.getDisbursementDate(), "MM/dd/yyyy"));
            }
            if (null != filehandoverdetails.getReceivedDate()) {
                jSonTransformer.put("receivedDate", GngDateUtil.changeDateFormat(filehandoverdetails.getReceivedDate(), "MM/dd/yyyy"));
            }
            jSonTransformer.put("fileStatus", filehandoverdetails.getFileStatus());
            jSonTransformer.put("holdReason", filehandoverdetails.getHoldReason());

        }
        return jSonTransformer;
    }

    public PojoToJSonTransformer build(CustomerAddress customerAddress) {

        if (null != customerAddress) {

            if (StringUtils.isNotBlank(customerAddress.getNegativeArea())) {
                jSonTransformer.put("negativeAreaFlag", true);
                jSonTransformer.put("negativeArea", customerAddress.getNegativeArea());
                jSonTransformer.put("negativeAreaReason", customerAddress.getNegativeAreaReason());

            } else {
                jSonTransformer.put("negativeAreaFlag", false);

            }
        }
        return jSonTransformer;
    }

    public PojoToJSonTransformer build(InvoiceDetails invoiceDetails) {
        /**
         * shipping addess.
         */
        CustomerAddress customerAddress = invoiceDetails.getDeliveryAddress();
        jSonTransformer.put("S_H_addressType", customerAddress.getAddressType());
        jSonTransformer.put("S_H_addressLine1", customerAddress.getAddressLine1());
        jSonTransformer.put("S_H_addressLine2", customerAddress.getAddressLine2());
        jSonTransformer.put("S_H_line3", customerAddress.getLine3());
        jSonTransformer.put("S_H_landMark", customerAddress.getLandMark());
        jSonTransformer.put("S_H_pin", customerAddress.getPin());
        jSonTransformer.put("S_H_city", customerAddress.getCity());
        jSonTransformer.put("S_H_state", customerAddress.getState());


        return jSonTransformer;
    }


    private void setPostIPADate(PostIpaRequest postIpaRequest) {
        if (null != postIpaRequest) {
            jSonTransformer.build(postIpaRequest, "P_IPA_REQUEST",
                    jSonTransformer);
        }
    }

    private void setPostIPA(PostIPA postIPA) {
        jSonTransformer.build(postIPA, "postIPA", jSonTransformer);
        if (null != postIPA) {
            jSonTransformer.build(postIPA.getAssetDetails(), "P_IPA_ASSET",
                    jSonTransformer);
            jSonTransformer.put("mbdPercentage", GngNumUtil.calculateMBDPercentage(postIPA));
        }

    }

    public PojoToJSonTransformer populateMetaData(DealerEmailMaster dealerEmailMaster) {
        if (null == jSonTransformer) {
            jSonTransformer = new PojoToJSonTransformer<>();
        }
        if (null != dealerEmailMaster) {
            jSonTransformer.build(dealerEmailMaster, dealerEmailMaster.getClass().getSimpleName(),
                    jSonTransformer);
            jSonTransformer.put("dealerComm", dealerEmailMaster.getDealerComm());
        }
        return jSonTransformer;
    }

    public PojoToJSonTransformer build(DealerBranchMaster dealerBranchMaster) {

        jSonTransformer.put("branchName", dealerBranchMaster.getBranchName());

        return jSonTransformer;
    }

    public PojoToJSonTransformer build(LoyaltyCardDetails loyaltyCardDetails) {

        jSonTransformer.put("loyaltyCardNumber", loyaltyCardDetails.getLoyaltyCardNo());
        jSonTransformer.put("loyaltyCardType", loyaltyCardDetails.getLoyaltyCardType() != null ? loyaltyCardDetails.getLoyaltyCardType().toFaceValue() : LoyaltyCardType.HDBFS.toFaceValue());
        jSonTransformer.put("loyaltyCardPrice", loyaltyCardDetails.getLoyaltyCardPrice());
        jSonTransformer.put("loyaltyCardEmi", loyaltyCardDetails.getRevisedTotalEmi());

        return jSonTransformer;

    }

    public PojoToJSonTransformer build(ExtendedWarrantyDetails extendedWarrantyDetails) {

        jSonTransformer.put("eWAssetCategory", extendedWarrantyDetails.geteWAssetCategory());
        jSonTransformer.put("extendedWarrantyTenure", extendedWarrantyDetails.getEwWarrantyTenure());
        jSonTransformer.put("extendedWarrantyPremium", extendedWarrantyDetails.getEwWarrantyPremium());
        jSonTransformer.put("ewEmi", extendedWarrantyDetails.getRevisedTotalEmi());

        return jSonTransformer;

    }

    public PojoToJSonTransformer build(InsurancePremiumDetails insurancePremiumDetails) {

        jSonTransformer.put("insuranceAssetCategory", insurancePremiumDetails.getInsAssetCategory());
        jSonTransformer.put("insurancePremium", insurancePremiumDetails.getInsurancePremium());
        return jSonTransformer;

    }
    public PojoToJSonTransformer build(GstDetails gstDetails) {
        if(gstDetails !=null){
            jSonTransformer.put("gstNumber", gstDetails.getGstNumber());
        }
        return jSonTransformer;
    }
    public PojoToJSonTransformer build(List<BankingDetails> bankingDetails) {
        if(!CollectionUtils.isEmpty(bankingDetails) && bankingDetails.size()>0){
            jSonTransformer.put("accountHolderFirstName",bankingDetails.get(0).getAccountHolderName().getFirstName());
            jSonTransformer.put("accountHolderMiddleName",bankingDetails.get(0).getAccountHolderName().getMiddleName());
            jSonTransformer.put("accountHolderLastName",bankingDetails.get(0).getAccountHolderName().getLastName());
        }
        return jSonTransformer;
    }
    public PojoToJSonTransformer build(String branchState) {
        if (branchState!=null){
            jSonTransformer.put("branchState",branchState);
        }
        return jSonTransformer;
    }

    public PojoToJSonTransformer buildApplicantType(String applicantType){
        if(StringUtils.isNotBlank(applicantType))
            jSonTransformer.put("applicantType",applicantType);
        else{
            String applType="NORMAL";
            jSonTransformer.put("applicantType",applType);}
        return jSonTransformer;
    }

}
