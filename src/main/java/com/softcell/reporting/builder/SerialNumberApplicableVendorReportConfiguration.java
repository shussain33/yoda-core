package com.softcell.reporting.builder;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by sampat on 17/01/18.
 */
public class SerialNumberApplicableVendorReportConfiguration {

    public static SortedMap<Integer, ColumnConfiguration> serialNumberApplicableVendorLogConfigMap = new TreeMap();

    static {
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ReferenceId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_referenceId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DealerId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Product");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_product999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time_Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Vendor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_vendor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("IsDealerSkipApplicable");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_isDealerSkipApplicable999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CustomMsg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_customMsg999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_N_A_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialNumberApplicableVendorLogConfigMap.put(index, columnConfiguration);
        index++;

    }
}
