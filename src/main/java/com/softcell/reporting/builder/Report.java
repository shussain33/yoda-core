package com.softcell.reporting.builder;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.reports.ChannelRptDomain;
import com.softcell.gonogo.model.reports.DeviceInfoRptDomain;

import java.util.List;
import java.util.stream.Stream;


/**
 * @author yogeshb
 */

public interface Report {

    /**
     * @param goNoGoCustomerApplication
     * @return
     */
    public List<ChannelRptDomain> createChannelWiseReport(
            List<GoNoGoCustomerApplication> goNoGoCustomerApplication);

    /**
     * @param goNoGoCustomerApplications
     * @return
     */
    public List<DeviceInfoRptDomain> createDeviceInformationReport(
            List<GoNoGoCustomerApplication> goNoGoCustomerApplications);

}
