package com.softcell.reporting.builder;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by mahesh on 22/2/17.
 */
public class LosUtrUpdateReportConfiguration {

    public static SortedMap<Integer, ColumnConfiguration> losUtrUpdateConfigMap = new TreeMap();

    static {
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Reference_Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_UTR_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        losUtrUpdateConfigMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Personal_Mobile");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_UTR_personalMobile999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        losUtrUpdateConfigMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS_ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_UTR_losID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        losUtrUpdateConfigMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("UTR_Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_UTR_utrNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        losUtrUpdateConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS_Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_UTR_losStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        losUtrUpdateConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("NetDisbursal_Amt");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_UTR_netDisbursalAmt999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        losUtrUpdateConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Update_Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_UTR_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        losUtrUpdateConfigMap.put(index, columnConfiguration);
        index++;

    }

}
