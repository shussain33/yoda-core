package com.softcell.reporting.builder;

import com.softcell.reporting.domains.SalesReportResponse;

public class SaleReportParser {

    private PojoToJSonTransformer jSonTransformer;

    private SalesReportResponse salesReportResponse;

    public SaleReportParser(SalesReportResponse salesReportResponse) {
        this.salesReportResponse = salesReportResponse;
    }

    /**
     * @return JSON structure with first level keys of complete Customer
     */
    public PojoToJSonTransformer build() {
        jSonTransformer = new PojoToJSonTransformer();
        setSalesReportResponse();
        return jSonTransformer;
    }

    private void setSalesReportResponse() {
        if (null != salesReportResponse) {
            jSonTransformer.build(salesReportResponse,
                    KEYWORD.FIRST.toString(), jSonTransformer);
            if (null != salesReportResponse.getPostIpa()) {
                jSonTransformer.build(salesReportResponse.getPostIpa(),
                        KEYWORD.POST_IPA.toString(), jSonTransformer);
                if (null != salesReportResponse.getPostIpa().getAssetDetails()) {
                    jSonTransformer.build(salesReportResponse.getPostIpa().getAssetDetails(),
                            KEYWORD.POST_IPA_ASSET.toString(), jSonTransformer);
                }
            }
        }
    }

    enum KEYWORD {
        FIRST {
            @Override
            public String toString() {
                return "f";
            }
        },
        POST_IPA {
            @Override
            public String toString() {
                return "PA";
            }
        },
        POST_IPA_ASSET {
            @Override
            public String toString() {
                return "PAA";
            }
        }
    }

}
