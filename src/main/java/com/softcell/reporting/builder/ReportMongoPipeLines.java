package com.softcell.reporting.builder;

import com.mongodb.BasicDBObject;
import com.softcell.dao.mongodb.repository.AnalyticsMongoRepository;
import com.softcell.gonogo.model.analytics.StackRequest;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.RequestCriteria;
import com.softcell.reporting.domains.FirstLastLoginReportRequest;
import com.softcell.reporting.domains.SalesReportRequest;
import com.softcell.reporting.domains.SalesReportResponse;
import com.softcell.reporting.request.DailyDisbursalRequest;
import com.softcell.reporting.spring.data.CustomProjectionOperation;
import com.softcell.reporting.spring.data.UnwindOperation;
import com.softcell.utils.GngUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

/**
 * @author kishorp
 */
@Service("reportingPipelineHelper")
public class ReportMongoPipeLines {

    private static  final Logger logger = LoggerFactory.getLogger(ReportMongoPipeLines.class);

    @Autowired
    private ReportMongoPipeLines reportMongoPipeLines;

    @Autowired
    private AnalyticsMongoRepository analyticsMongoRepository;

    /**
     * default constructor
     */
    public ReportMongoPipeLines() {

    }

    /**
     * @param institutionId
     * @param loanType
     * @param startDate
     * @param endDate
     * @return
     */
    private static Criteria salesReportCriteria(String institutionId,
                                                Set<String> loanType, Date startDate, Date endDate) {

        Assert.hasText(institutionId,
                "InsitutionId  must not be null or empty!");
        Assert.notNull(loanType, " Products must not be null or empty");
        Assert.notNull(startDate, "Start Date must not be null or empty");
        Assert.notNull(endDate, "End Date must not be null or empty");

        startDate = new DateTime(startDate).toDate();

        endDate = new DateTime(endDate).toDate();

        return Criteria.where("applicationRequest.header.dateTime")
                .gte(startDate).lte(endDate)
                .and("applicationRequest.header.institutionId")
                .is(institutionId)
                .and("applicationRequest.request.application.loanType")
                .in(loanType);
    }

    private static Criteria firstLastLoginMatchBuilder(
            FirstLastLoginReportRequest firstLastLoginReportRequest) {

        String institutionId = firstLastLoginReportRequest.getInstitutionId();
        Date endDate = firstLastLoginReportRequest.getEndDate();
        Date startDate = firstLastLoginReportRequest.getStartDate();
        Set<String> products = firstLastLoginReportRequest.getProducts();

        Assert.hasText(institutionId,
                "InsitutionId  must not be null or empty !");
        Assert.notEmpty(products, " Products must not be null or empty !");
        Assert.notNull(startDate, "Start Date must not be null or empty !");
        Assert.notNull(endDate, "End Date must not be null or empty !");

        return Criteria.where("header.dateTime").gte(startDate).lte(endDate)
                .and("header.institutionId").is(institutionId)
                .and("request.application.loanType").in(products);
    }

    /**
     * local method to generate criteria for otp report
     * @param startDate
     * @param endDate
     * @return
     */
    private static Criteria otpReportCriteria(Date startDate, Date endDate) {
        return Criteria.where("startTime").gte(startDate).lte(endDate);
    }

    @Deprecated
    public static Aggregation buildAggregationForTopDealer(
            StackRequest stackRequest) {

        Assert.notNull(stackRequest,
                "Stack request Object must not be null or blank for loginbyTopDealer ");

        Date fromDate = new DateTime(stackRequest.getFromDate()).toDate();
        Date toDate = new DateTime(stackRequest.getToDate()).toDate();

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        Collection<String> products = GngUtils.convertToCollectionOfString(stackRequest.getCriteria().getProducts());

        pipelineOperation.add(match(new Criteria().andOperator(
                        Criteria.where("applicationRequest.header.institutionId").is(
                                stackRequest.getHeader().getInstitutionId()),
                        Criteria.where(
                                "applicationRequest.request.application.loanType")
                                .in(products),
                        Criteria.where("applicationRequest.header.dateTime")
                                .gte(fromDate).lte(toDate)))

        );

        UnwindOperation unwindOperation = new UnwindOperation(
                "applicationRequest.request.application.asset", false);
        pipelineOperation.add(unwindOperation);

        pipelineOperation.add(match(Criteria.where(
                "applicationRequest.request.application.asset.dlrName").nin(
                new Object[]{"", "null", null})));

        BasicDBObject basicDBObject = new BasicDBObject(
                "$project",
                new BasicDBObject("dlrName",
                        "$applicationRequest.request.application.asset.dlrName"));
        CustomProjectionOperation customProjectionOperation = new CustomProjectionOperation(
                basicDBObject);
        pipelineOperation.add(customProjectionOperation);

        pipelineOperation.add(group(fields().and("dlrName")).count()
                .as("count"));

        pipelineOperation.add(sort(Sort.Direction.DESC, "count"));

        pipelineOperation.add(limit(5));

        return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());
    }

	/*
     * String institutionId, Set<String> products, Date startDate, Date endDate
	 */

    public static Aggregation buildAggregratorForLoginByDealers(
            StackRequest stackRequest) {

        Date fromDate = new DateTime(stackRequest.getFromDate()).toDate();
        Date toDate = new DateTime(stackRequest.getToDate()).toDate();

        Collection<String> products =GngUtils.convertToCollectionOfString(stackRequest.getCriteria().getProducts());

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        pipelineOperation.add(match(new Criteria().andOperator(
                        Criteria.where("applicationRequest.header.institutionId").is(
                                stackRequest.getHeader().getInstitutionId()),
                        Criteria.where(
                                "applicationRequest.request.application.loanType")
                                .in(products),
                        Criteria.where("applicationRequest.header.dateTime")
                                .gte(fromDate).lte(toDate),
                        Criteria.where("applicationRequest.header.dealerId").exists(
                                true),
                        Criteria.where("applicationRequest.header.dealerId").nin(
                                new ArrayList<String>() {
                                    {
                                        add("");
                                    }
                                })))

        );

        BasicDBObject projection = new BasicDBObject("$project",
                new LinkedHashMap<String, Object>() {
                    {
                        put("dealerId", "$applicationRequest.header.dealerId");
                        put("dealerDescription",
                                "$applicationRequest.request.application.asset.dlrName");
                        put("date", new BasicDBObject("$dateToString",
                                new LinkedHashMap<String, Object>() {
                                    {
                                        put("format", "%Y-%m-%d");
                                        put("date",
                                                "$applicationRequest.header.dateTime");
                                    }
                                }));
                    }
                });

        pipelineOperation.add(new CustomProjectionOperation(projection));

        pipelineOperation.add(lookup("supplierBranchMaster", "dealerId",
                "SUPPLIERID", "supplierBranchMaster"));

        pipelineOperation
                .add(new UnwindOperation("supplierBranchMaster", false));

        BasicDBObject projection2 = new BasicDBObject("$project",
                new LinkedHashMap<String, Object>() {
                    {
                        put("date", "$date");
                        put("supplierDesc", new BasicDBObject("$cond",
                                new LinkedHashMap<String, Object>() {
                                    {
                                        put("if",
                                                new BasicDBObject(
                                                        "$eq",
                                                        new Object[]{
                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                "IDIB000T014"}));
                                        put("then", "VASANTH");
                                        put("else",
                                                new BasicDBObject(
                                                        "$cond",
                                                        new LinkedHashMap<String, Object>() {
                                                            {
                                                                put("if",
                                                                        new BasicDBObject(
                                                                                "$eq",
                                                                                new Object[]{
                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                        "CIUB0000085"}));
                                                                put("then",
                                                                        "SATHYA");
                                                                put("else",
                                                                        new BasicDBObject(
                                                                                "$cond",
                                                                                new LinkedHashMap<String, Object>() {
                                                                                    {
                                                                                        put("if",
                                                                                                new BasicDBObject(
                                                                                                        "$eq",
                                                                                                        new Object[]{
                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                "CNRB0000915"}));
                                                                                        put("then",
                                                                                                "VIVEKS");
                                                                                        put("else",
                                                                                                new BasicDBObject(
                                                                                                        "$cond",
                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                            {
                                                                                                                put("if",
                                                                                                                        new BasicDBObject(
                                                                                                                                "$eq",
                                                                                                                                new Object[]{
                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                        "IDIB000T052"}));
                                                                                                                put("then",
                                                                                                                        "MEENAKSHI");
                                                                                                                put("else",
                                                                                                                        new BasicDBObject(
                                                                                                                                "$cond",
                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                    {
                                                                                                                                        put("if",
                                                                                                                                                new BasicDBObject(
                                                                                                                                                        "$eq",
                                                                                                                                                        new Object[]{
                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                "UTIB0000170"}));
                                                                                                                                        put("then",
                                                                                                                                                "SHARPTRONICS");
                                                                                                                                        put("else",
                                                                                                                                                new BasicDBObject(
                                                                                                                                                        "$cond",
                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                            {
                                                                                                                                                                put("if",
                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                "$eq",
                                                                                                                                                                                new Object[]{
                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                        "HDFC0001850"}));
                                                                                                                                                                put("then",
                                                                                                                                                                        "VENUS");
                                                                                                                                                                put("else",
                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                "$cond",
                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                    {
                                                                                                                                                                                        put("if",
                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                "INDB0000117"}));
                                                                                                                                                                                        put("then",
                                                                                                                                                                                                "J & J");
                                                                                                                                                                                        put("else",
                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                            {
                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                        "ICIC0001193"}));
                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                        "MANGLAM");
                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                "SBTR0000718"}));
                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                "JAI");
                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                        "HDFC0001278"}));
                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                        "RUBA");
                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                "HDFC0001997"}));
                                                                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                                                                "BAJAJ ELEC");
                                                                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                        "SBIN0007274"}));
                                                                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                                                                        "COIMBTR HA");
                                                                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                "UTIB0000405"}));
                                                                                                                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                                                                                                                "SUNDAR RADIOS");
                                                                                                                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                        "HDFC0000281"}));
                                                                                                                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                                                                                                                        "LOTUS ELEC");
                                                                                                                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                "SIBL0000182"}));
                                                                                                                                                                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                                                                                                                                                                "DANAM SALE");
                                                                                                                                                                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                        "SBIN0001253"}));
                                                                                                                                                                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                        "NOBLE ELEC");
                                                                                                                                                                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                "SBTR0000011"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                "RAJAN & CO");
                                                                                                                                                                                                                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "UBIN0531286"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "SALES INDIA");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "HDFC0000004"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "GIRIAS");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "HDFC0000621"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "TMC");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "ICIC0001859"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "MEHTA AUTO");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "IDIB000T052"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "SACS");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "INDB0000361"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "AMSA VISION");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$cond",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("if",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        new BasicDBObject(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                "$eq",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                new Object[]{
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "$supplierBranchMaster.LSOLM_IFSC_CODE_C",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "UTIB0000563"}));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("then",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "PIONEER	");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                put("else",
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        "OTHERS");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                }));
                                                                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                                                                }));
                                                                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                }));
                                                                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                }));
                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                }));
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                        }));
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                }));

                                                                                                                                                                                                            }
                                                                                                                                                                                                        }));
                                                                                                                                                                                    }
                                                                                                                                                                                }));
                                                                                                                                                            }
                                                                                                                                                        }));
                                                                                                                                    }
                                                                                                                                }));
                                                                                                            }
                                                                                                        }));
                                                                                    }
                                                                                }));
                                                            }
                                                        }));
                                    }
                                }

                        ));
                    }
                });

        pipelineOperation.add(new CustomProjectionOperation(projection2));

        pipelineOperation.add(group(fields().and("date").and("supplierDesc"))
                .count().as("count"));

        pipelineOperation.add(sort(Sort.Direction.DESC, "_id.date"));

        pipelineOperation.add(sort(Sort.Direction.DESC, "count"));

        return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    @Deprecated
    public static Aggregation buildAggregationForLoginByDealers(
            Collection<String> topTenDealers, StackRequest stackRequest) {

        Assert.notEmpty(topTenDealers,
                "top 10 dealers list must not be null or blank ");

        Date fromDate = new DateTime(stackRequest.getFromDate()).toDate();
        Date toDate = new DateTime(stackRequest.getToDate()).toDate();

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        Collection<String> products = GngUtils.convertToCollectionOfString(stackRequest.getCriteria().getProducts());

        pipelineOperation.add(match(new Criteria().andOperator(
                        Criteria.where("applicationRequest.header.institutionId").is(
                                stackRequest.getHeader().getInstitutionId()),
                        Criteria.where(
                                "applicationRequest.request.application.loanType")
                                .in(products),
                        Criteria.where("applicationRequest.header.dateTime")
                                .gte(fromDate).lte(toDate)))

        );

        UnwindOperation unwindOperation = new UnwindOperation(
                "applicationRequest.request.application.asset", false);
        pipelineOperation.add(unwindOperation);

        List<Object> objects = new ArrayList<Object>();

        for (String str : topTenDealers) {
            objects.add(new BasicDBObject("$eq", new Object[]{
                    "$applicationRequest.request.application.asset.dlrName",
                    str}));

        }

        BasicDBObject basicDBObject = new BasicDBObject("$project",
                new LinkedHashMap<String, Object>() {
                    {
                        put("dealerName", new BasicDBObject("$cond",
                                new LinkedHashMap<String, Object>() {
                                    {
                                        put("if", new BasicDBObject("$or",
                                                objects));
                                        put("then",
                                                "$applicationRequest.request.application.asset.dlrName");
                                        put("else", "OTHERS");
                                    }
                                }));
                        put("dateTime", new BasicDBObject("$dateToString",
                                new LinkedHashMap<String, Object>() {
                                    {
                                        put("format", "%Y-%m-%d");
                                        put("date",
                                                "$applicationRequest.header.dateTime");
                                    }
                                }));
                    }
                });

        CustomProjectionOperation customProjectionOperation = new CustomProjectionOperation(
                basicDBObject);

        pipelineOperation.add(customProjectionOperation);

        pipelineOperation.add(group("dateTime", "dealerName").count().as(
                "count"));

        return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    public static Aggregation buildAggregationForloginByStage(
            StackRequest stackRequest) {

        Assert.notNull(stackRequest,
                "Stack request Object must not be null or blank for loginbystage ");

        Date fromDate = new DateTime(stackRequest.getFromDate()).toDate();
        Date toDate = new DateTime(stackRequest.getToDate()).toDate();

        RequestCriteria criteria = stackRequest.getCriteria();

        Collection<String> products = GngUtils.convertToCollectionOfString(stackRequest.getCriteria().getProducts());

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("applicationRequest.header.institutionId").is(
                        stackRequest.getHeader().getInstitutionId()),
                Criteria.where("applicationRequest.currentStageId").nin(
                        new ArrayList<String>() {
                            {
                                add("null");
                                add("");
                                add(null);
                                add("DE");
                            }
                        }),
                Criteria.where("applicationRequest.header.dateTime")
                        .gte(fromDate).lte(toDate),
                Criteria.where("applicationRequest.request.application.loanType")
                        .in(products)


        )));

        /*ConditionalOperators.Switch.CaseOperator condition1 = ConditionalOperators.Switch.CaseOperator.when(
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("LOS_DISB")).then("DISBURSED");

        ConditionalOperators.Switch.CaseOperator condition2 = ConditionalOperators.Switch.CaseOperator.when(BooleanOperators.Or.or(
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("LOS_APRV"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("LOS_QDE"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("PD_DE"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("INV_GEN"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("INV_GNR")

        )).then("OPS QUEUE");

        ConditionalOperators.Switch.CaseOperator condition3 = ConditionalOperators.Switch.CaseOperator.when(BooleanOperators.Or.or(
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("APRV"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("SRNV"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("DO"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("SERIAL-NO VALIDATION")
        )).then("APPROVED");

        ConditionalOperators.Switch.CaseOperator condition4 = ConditionalOperators.Switch.CaseOperator.when(BooleanOperators.Or.or(
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("DCLN"),
                ComparisonOperators.valueOf(StringOperators.valueOf("applicationRequest.currentStageId").toUpper())
                        .equalToValue("LOS_DCLN")
        )).then("DECLINED");


        ProjectionOperation  projectionOperation = project()
                .and(DateOperators.dateOf("$applicationRequest.header.dateTime").toString("%Y-%m-%d")).as("date")
                .and(ConditionalOperators.switchCases(condition1, condition2, condition3, condition4).defaultTo("NO DECISION")).as("stage");*/

        BasicDBObject basicDBObject = new BasicDBObject("$project",
                new BasicDBObject("date", new BasicDBObject("$dateToString",
                        new BasicDBObject(new LinkedHashMap<String, Object>() {
                            {
                                put("format", "%Y-%m-%d");
                                put("date",
                                        "$applicationRequest.header.dateTime");
                            }
                        }))).append("stage", new BasicDBObject("$cond",
                        new LinkedHashMap<String, Object>() {
                            {
                                put("if",
                                        new BasicDBObject(
                                                "$or",
                                                new Object[]{new BasicDBObject(
                                                        "$eq",
                                                        new Object[]{
                                                                new BasicDBObject(
                                                                        "$toUpper",
                                                                        "$applicationRequest.currentStageId"),
                                                                "LOS_DISB"})}));
                                put("then", "DISBURSED");
                                put("else", new BasicDBObject("$cond",
                                        new LinkedHashMap<String, Object>() {
                                            {
                                                put("if",
                                                        new BasicDBObject(
                                                                "$or",
                                                                new Object[]{
                                                                        new BasicDBObject(
                                                                                "$eq",
                                                                                new Object[]{
                                                                                        new BasicDBObject(
                                                                                                "$toUpper",
                                                                                                "$applicationRequest.currentStageId"),
                                                                                        "LOS_APRV"}),
                                                                        new BasicDBObject(
                                                                                "$eq",
                                                                                new Object[]{
                                                                                        new BasicDBObject(
                                                                                                "$toUpper",
                                                                                                "$applicationRequest.currentStageId"),
                                                                                        "LOS_QDE"}),
                                                                        new BasicDBObject(
                                                                                "$eq",
                                                                                new Object[]{
                                                                                        new BasicDBObject(
                                                                                                "$toUpper",
                                                                                                "$applicationRequest.currentStageId"),
                                                                                        "PD_DE"}),
                                                                        new BasicDBObject(
                                                                                "$eq",
                                                                                new Object[]{
                                                                                        new BasicDBObject(
                                                                                                "$toUpper",
                                                                                                "$applicationRequest.currentStageId"),
                                                                                        "INV_GEN"}),
                                                                        new BasicDBObject(
                                                                                "$eq",
                                                                                new Object[]{
                                                                                        new BasicDBObject(
                                                                                                "$toUpper",
                                                                                                "$applicationRequest.currentStageId"),
                                                                                        "INV_GNR"})}));

                                                put("then", "OPS QUEUE");
                                                put("else",
                                                        new BasicDBObject(
                                                                "$cond",
                                                                new LinkedHashMap<String, Object>() {
                                                                    {

                                                                        put("if",
                                                                                new BasicDBObject(
                                                                                        "$or",
                                                                                        new Object[]{
                                                                                                new BasicDBObject(
                                                                                                        "$eq",
                                                                                                        new Object[]{
                                                                                                                new BasicDBObject(
                                                                                                                        "$toUpper",
                                                                                                                        "$applicationRequest.currentStageId"),
                                                                                                                "APRV"}),
                                                                                                new BasicDBObject(
                                                                                                        "$eq",
                                                                                                        new Object[]{
                                                                                                                new BasicDBObject(
                                                                                                                        "$toUpper",
                                                                                                                        "$applicationRequest.currentStageId"),
                                                                                                                "SRNV"}),
                                                                                                new BasicDBObject(
                                                                                                        "$eq",
                                                                                                        new Object[]{
                                                                                                                new BasicDBObject(
                                                                                                                        "$toUpper",
                                                                                                                        "$applicationRequest.currentStageId"),
                                                                                                                "DO"}),
                                                                                                new BasicDBObject(
                                                                                                        "$eq",
                                                                                                        new Object[]{
                                                                                                                new BasicDBObject(
                                                                                                                        "$toUpper",
                                                                                                                        "$applicationRequest.currentStageId"),
                                                                                                                "SERIAL-NO VALIDATION"})}));

                                                                        put("then",
                                                                                "APPROVED");
                                                                        put("else",
                                                                                new BasicDBObject(
                                                                                        "$cond",
                                                                                        new LinkedHashMap<String, Object>() {
                                                                                            {
                                                                                                put("if",
                                                                                                        new BasicDBObject(
                                                                                                                "$or",
                                                                                                                new Object[]{
                                                                                                                        new BasicDBObject(
                                                                                                                                "$eq",
                                                                                                                                new Object[]{
                                                                                                                                        new BasicDBObject(
                                                                                                                                                "$toUpper",
                                                                                                                                                "$applicationRequest.currentStageId"),
                                                                                                                                        "DCLN"}),
                                                                                                                        new BasicDBObject(
                                                                                                                                "$eq",
                                                                                                                                new Object[]{
                                                                                                                                        new BasicDBObject(
                                                                                                                                                "$toUpper",
                                                                                                                                                "$applicationRequest.currentStageId"),
                                                                                                                                        "LOS_DCLN"})}));
                                                                                                put("then",
                                                                                                        "DECLINED");
                                                                                                put("else",
                                                                                                        "NO DECISION");
                                                                                            }
                                                                                        }));
                                                                    }
                                                                }));
                                            }
                                        }));
                            }
                        }))

        );

        CustomProjectionOperation customProjectionOperation = new CustomProjectionOperation(
                basicDBObject);

        pipelineOperation.add(customProjectionOperation);

        pipelineOperation.add(group("date", "stage").count().as("count"));

        pipelineOperation.add(sort(Direction.DESC, "date"));

        return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    public static Aggregation assetManufacturerAggregationBuilder(
            StackRequest stackRequest) {

        Assert.notNull(stackRequest,
                "  stackRequest for asset manufacture report must not be null or blank !!");

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        RequestCriteria criteria = stackRequest.getCriteria();

        Collection<String> products = GngUtils.convertToCollectionOfString(criteria.getProducts());

        final List<String> excludeAssetMake = new ArrayList<String>() {
            /**
             *
             */
            private static final long serialVersionUID = 1L;

            {
                add("");
                add(null);
                add("null");
            }
        };

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("applicationRequest.header.institutionId").is(
                        stackRequest.getHeader().getInstitutionId()),
                Criteria.where(
                        "applicationRequest.request.application.loanType")
                        .in(products))));

        UnwindOperation unwindOperation = new UnwindOperation(
                "applicationRequest.request.application.asset", false);
        pipelineOperation.add(unwindOperation);

        pipelineOperation.add(match(Criteria.where(
                "applicationRequest.request.application.asset.assetMake").nin(
                excludeAssetMake)));

        BasicDBObject basicDBObject = new BasicDBObject("$project",
                new LinkedHashMap<String, Object>() {
                    {
                        put("assetCtg", new BasicDBObject("$cond",
                                new LinkedHashMap<String, Object>() {
                                    {
                                        put("if",
                                                new BasicDBObject(
                                                        "$or",
                                                        new Object[]{
                                                                new BasicDBObject(
                                                                        "$eq",
                                                                        new Object[]{
                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                "TELEVISION"}),
                                                                new BasicDBObject(
                                                                        "$eq",
                                                                        new Object[]{
                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                "PANEL - LED"}),
                                                                new BasicDBObject(
                                                                        "$eq",
                                                                        new Object[]{
                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                "PANEL - SMARTLED"}),
                                                                new BasicDBObject(
                                                                        "$eq",
                                                                        new Object[]{
                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                "PANEL - FHD"}),
                                                                new BasicDBObject(
                                                                        "$eq",
                                                                        new Object[]{
                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                "PANEL - HD"}),

                                                        }));
                                        put("then", "TELEVISION");
                                        put("else",
                                                new BasicDBObject(
                                                        "$cond",
                                                        new LinkedHashMap<String, Object>() {
                                                            {
                                                                put("if",
                                                                        new BasicDBObject(
                                                                                "$or",
                                                                                new Object[]{
                                                                                        new BasicDBObject(
                                                                                                "$eq",
                                                                                                new Object[]{
                                                                                                        "$applicationRequest.request.application.asset.assetCtg",
                                                                                                        "AIR CONDITIONER"}),
                                                                                        new BasicDBObject(
                                                                                                "$eq",
                                                                                                new Object[]{
                                                                                                        "$applicationRequest.request.application.asset.assetCtg",
                                                                                                        "SPLIT AC"}),}));
                                                                put("then",
                                                                        "AIR CONDITIONER");
                                                                put("else",
                                                                        new BasicDBObject(
                                                                                "$cond",
                                                                                new LinkedHashMap<String, Object>() {
                                                                                    {
                                                                                        put("if",
                                                                                                new BasicDBObject(
                                                                                                        "$or",
                                                                                                        new Object[]{
                                                                                                                new BasicDBObject(
                                                                                                                        "$eq",
                                                                                                                        new Object[]{
                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                "REF - DC"}),
                                                                                                                new BasicDBObject(
                                                                                                                        "$eq",
                                                                                                                        new Object[]{
                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                "REF-FF"}),
                                                                                                                new BasicDBObject(
                                                                                                                        "$eq",
                                                                                                                        new Object[]{
                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                "REF-FF HOME"}),
                                                                                                                new BasicDBObject(
                                                                                                                        "$eq",
                                                                                                                        new Object[]{
                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                "REFRIGERATORS"})}));
                                                                                        put("then",
                                                                                                "REFRIGERATORS");
                                                                                        put("else",
                                                                                                new BasicDBObject(
                                                                                                        "$cond",
                                                                                                        new LinkedHashMap<String, Object>() {
                                                                                                            {
                                                                                                                put("if",
                                                                                                                        new BasicDBObject(
                                                                                                                                "$eq",
                                                                                                                                new Object[]{
                                                                                                                                        "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                        "MICROWAVE"}));
                                                                                                                put("then",
                                                                                                                        "MICROWAVE");
                                                                                                                put("else",
                                                                                                                        new BasicDBObject(
                                                                                                                                "$cond",
                                                                                                                                new LinkedHashMap<String, Object>() {
                                                                                                                                    {
                                                                                                                                        put("if",
                                                                                                                                                new BasicDBObject(
                                                                                                                                                        "$or",
                                                                                                                                                        new Object[]{
                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                        "$eq",
                                                                                                                                                                        new Object[]{
                                                                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                                                                "WASHING MACHINE"}),
                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                        "$eq",
                                                                                                                                                                        new Object[]{
                                                                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                                                                "WM-FATL"}),
                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                        "$eq",
                                                                                                                                                                        new Object[]{
                                                                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                                                                "WM-FAFL"}),
                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                        "$eq",
                                                                                                                                                                        new Object[]{
                                                                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                                                                "TLWM"}),
                                                                                                                                                                new BasicDBObject(
                                                                                                                                                                        "$eq",
                                                                                                                                                                        new Object[]{
                                                                                                                                                                                "$applicationRequest.request.application.asset.assetCtg",
                                                                                                                                                                                "FLWM"}),}));
                                                                                                                                        put("then",
                                                                                                                                                "WASHING MACHINE");
                                                                                                                                        put("else",
                                                                                                                                                "OTHERS");
                                                                                                                                    }
                                                                                                                                }));
                                                                                                            }
                                                                                                        }));
                                                                                    }
                                                                                }));
                                                            }
                                                        }));

                                    }
                                }));

                        put("assetMake",
                                new BasicDBObject("$toUpper",
                                        "$applicationRequest.request.application.asset.assetMake"));

                    }
                });

        CustomProjectionOperation customProjectionOperation = new CustomProjectionOperation(
                basicDBObject);
        pipelineOperation.add(customProjectionOperation);

        pipelineOperation.add(group(fields().and("assetMake").and("assetCtg"))
                .count().as("count"));

        return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    public static Aggregation buildAggregatorForTopManufacturers(
            StackRequest stackRequest) {

        Assert.notNull(stackRequest,
                "stack Request for unique manufacturers in system must not be null or blank !!");

        Collection<String> products = GngUtils.convertToCollectionOfString(stackRequest.getCriteria().getProducts());

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("applicationRequest.header.institutionId").is(
                        stackRequest.getHeader().getInstitutionId()),
                Criteria.where(
                        "applicationRequest.request.application.loanType")
                        .in(products))));

        UnwindOperation unwindOperation = new UnwindOperation(
                "applicationRequest.request.application.asset", false);
        pipelineOperation.add(unwindOperation);

        pipelineOperation.add(match(Criteria.where(
                "applicationRequest.request.application.asset.assetMake").nin(
                new ArrayList<Object>() {
                    {
                        add("");
                        add(null);
                        add("null");
                    }
                })));

        BasicDBObject basicDBObject = new BasicDBObject(
                "$project",
                new BasicDBObject(
                        "assetMake",
                        new BasicDBObject("$toUpper",
                                "$applicationRequest.request.application.asset.assetMake")));

        CustomProjectionOperation customProjectionOperation = new CustomProjectionOperation(
                basicDBObject);
        pipelineOperation.add(customProjectionOperation);

        pipelineOperation.add(group(fields().and("assetMake")).count().as(
                "count"));

        pipelineOperation.add(sort(Sort.Direction.DESC, "count"));

        pipelineOperation.add(limit(15));

        return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    /**
     * method to generate aggregation pipeline for sales report
     *
     * @param salesReportRequest
     * @return
     */
    public TypedAggregation<SalesReportResponse>  generateSalesReport(SalesReportRequest salesReportRequest) {

        Assert.notNull(salesReportRequest,
                "Sales  request object should not be null");

        String institutionId = salesReportRequest.getInstitutionId();
        Set<String> products = salesReportRequest.getProducts();
        Date startDate = salesReportRequest.getStartDate();
        Date endDate = salesReportRequest.getEndDate();

        TypedAggregation<SalesReportResponse> salesReportPipeLine = salesReportPipeLine(institutionId,
                products, startDate, endDate);

        return salesReportPipeLine;

    }

    /**
     * @param institutionId
     * @param products
     * @param startDate
     * @param endDate
     * @return
     */
    private TypedAggregation<SalesReportResponse> salesReportPipeLine(String institutionId,
                                                 Set<String> products, Date startDate, Date endDate) {

        List<AggregationOperation> operations = new ArrayList<>();

        Criteria criteria = salesReportCriteria(institutionId, products,
                startDate, endDate);

        operations.add(match(criteria));

        operations.add(lookup("postIpaDoc", "_id", "_id",
                "customerDocumentPostIPA"));

        operations.add(project("applicationRequest", "customerDocumentPostIPA",
                "applicationStatus", "losDetails", "croJustification", "invoiceDetails"));

        operations.add(new UnwindOperation("customerDocumentPostIPA", true));

        operations.add(new UnwindOperation(
                "customerDocumentPostIPA.postIPA.assetDetails", true));

        operations.add(new UnwindOperation(
                "applicationRequest.request.application.asset", true));

        operations.add(new UnwindOperation("croJustification", true));

        operations
                .add(project()
                        .and("applicationRequest.header.dsaId")
                        .as("dsaId")
                        .and("applicationRequest.request.applicant.applicantName.firstName")
                        .as("firstName")
                        .and("applicationRequest.request.applicant.applicantName.middleName")
                        .as("middleName")
                        .and("applicationRequest.request.applicant.applicantName.lastName")
                        .as("lastName")
                        .and("applicationRequest.request.applicant.dateOfBirth")
                        .as("dob")
                        .and("applicationRequest.request.applicant.gender")
                        .as("gender")
                        .and("applicationRequest.request.application.loanAmount")
                        .as("appliedAmount")
                        .and("applicationRequest.header.dateTime")
                        .as("applicationDate")
                        .and("applicationRequest.header.dealerId")
                        .as("dealerId")
                        .and("applicationRequest.request.application.asset.assetCtg")
                        .as("assetCategory")
                        .and("applicationRequest.request.application.asset.assetMake")
                        .as("make")
                        .and("applicationRequest.request.application.asset.dlrName")
                        .as("dealername")
                        .and("applicationRequest.request.application.asset.modelNo")
                        .as("modelNo").and("applicationStatus")
                        .as("applicationStatus")
                        .and("applicationRequest.request.applicant.kyc")
                        .as("kycs").and("customerDocumentPostIPA.date")
                        .as("doDate").and("customerDocumentPostIPA.postIPA")
                        .as("postIpa").and("losDetails.losID").as("losId")
                        .and("losDetails.utrNumber").as("utrNo")
                        .and("losDetails.status").as("losStatus")
                        .and("croJustification.remark").as("remark")
                        .and("invoiceDetails.invoiceNumber")
                        .as("invoiceNumber").and("invoiceDetails.invoiceType")
                        .as("invoiceType").and("invoiceDetails.invoiceDate")
                        .as("invoiceDate"));


        return Aggregation.newAggregation(SalesReportResponse.class,operations).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());
    }

    @Deprecated
    public Aggregation lastFirstCaseLogin(
            FirstLastLoginReportRequest firstLastLoginReportRequest) {

        List<AggregationOperation> operations = new ArrayList<>();

        Criteria criteria = salesReportCriteria(
                firstLastLoginReportRequest.getInstitutionId(),
                firstLastLoginReportRequest.getProducts(),
                firstLastLoginReportRequest.getStartDate(),
                firstLastLoginReportRequest.getEndDate());

        MatchOperation matchOperation = match(criteria);
        operations.add(matchOperation);
        operations.add(project("header.dateTime")
                .andExpression("dayOfMonth(header.dateTime)").as("day")
                .andExpression("month(header.dateTime)").as("month")
                .andExpression("year(header.dateTime)").as("year")
                .andExpression("header.dealerId").as("header.dealerId")
                .andExpression("request.application.loanAmount")
                .as("Loan Amount").andExpression("header.dateTime")
                .as("Application Date")
                .andExpression("header.applicationSource").as("Source"));

        operations.add(group("day", "month", "year", "header.dealerId")
                .first("Application Date").as("First Login")
                .last("Application Date").as("Last Login").count().as("count")
                .first("Source").as("Source"));

        return Aggregation.newAggregation(operations);
    }

    public TypedAggregation firstLastLoginReportAggregaterBuilder(
            FirstLastLoginReportRequest firstLastLoginReportRequest) {

        Assert.hasText(firstLastLoginReportRequest.getInstitutionId(),"InsitutionId must not be null or blank !!");

        Assert.notEmpty(firstLastLoginReportRequest.getProducts(),"Products  must not be null or blank !!");

        Assert.notNull(firstLastLoginReportRequest.getEndDate(),"endDate must not be null or blank !!");

        Assert.notNull(firstLastLoginReportRequest.getStartDate(),"startDate must not be null or blank !!");

        List<AggregationOperation> operations = new ArrayList<>();

        Criteria criteria = firstLastLoginMatchBuilder(firstLastLoginReportRequest);

        MatchOperation matchOperation = match(criteria);

        operations.add(matchOperation);

        operations.add(project()
                .and("header.dsaId").as("dsaId")
                .and(DateOperators.dateOf("$header.dateTime").toString("%Y-%m-%d")).as("applicationDate")
                .and("request.application.loanAmount").as("loanAmount")
                .and("header.applicationSource").as("Source"));



        /*operations.add(project("header.dateTime")
                .andExpression("dayOfMonth(header.dateTime)").as("day")
                .andExpression("month(header.dateTime)").as("month")
                .andExpression("year(header.dateTime)").as("year")
                .andExpression("header.dsaId").as("header.dsaId")
                .andExpression("request.application.loanAmount")
                .as("loanAmount").andExpression("header.dateTime")
                .as("applicationDate")
                .andExpression("header.applicationSource").as("Source"));*/

        operations.add(group(
                fields().and("$applicationDate")
                        .and("$dsaId")
        ).count().as("count")
                .first("applicationDate").as("firstLogin")
                .last("applicationDate").as("lastLogin")
        );

        /*GroupMongoId groupMongoId = new GroupMongoId();
        groupMongoId.groupBy("day", "day");
        groupMongoId.groupBy("month", "month");
        groupMongoId.groupBy("year", "year");
        groupMongoId.groupBy("dsaId", "header.dsaId");

        CustomGroupOperation customGroupOperation = new CustomGroupOperation(
                groupMongoId).count("count")
                .first("applicationDate", "firstLogin")
                .last("applicationDate", "lastLogin");

        operations.add(customGroupOperation);*/

        logger.debug(" aggregation generated for first last login zip report is [{}] ", newAggregation(operations));

        return new TypedAggregation(ApplicationRequest.class,operations).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());
    }

    /**
     * @param startDate
     * @param endDate
     * @return
     */
    public Aggregation otpReport(Date startDate, Date endDate) {
        List<AggregationOperation> operations = new ArrayList<>();

        Criteria criteria = otpReportCriteria(startDate, endDate);

        MatchOperation matchOperation = match(criteria);

        operations.add(matchOperation);

        operations.add(project("startTime")
                .andExpression("dayOfMonth(startTime)").as("day")
                .andExpression("month(startTime)").as("month")
                .andExpression("year(startTime)").as("year")
                .andExpression("mobileNumber").as("mobileNumber"));

        operations.add(group("day", "month", "year", "mobileNumber").count()
                .as("count"));

        return Aggregation.newAggregation(operations).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());
    }


    public Aggregation dailyDisbursalReport(DailyDisbursalRequest dailyDisbursalRequest) throws Exception {

        Date startDate = dailyDisbursalRequest.getStartDate();
        Date endDate = dailyDisbursalRequest.getEndDate();

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("applicationRequest.header.institutionId").is(
                        dailyDisbursalRequest.getHeader().getInstitutionId()),
                Criteria.where(
                        "applicationRequest.currentStageId")
                        .is(dailyDisbursalRequest.getCurrentStageId()),
                Criteria.where("disbDetails.disbDate").gte(startDate)
                        .lte(endDate)
                ))

        );

        pipelineOperation.add(new CustomProjectionOperation(new BasicDBObject(
                "$project", new LinkedHashMap<String, Object>(){
            {
                put("branchName", "$applicationRequest.appMetaData.branchV2.branchName");
                put("branchCode","$applicationRequest.header.branchCode");
            }

        })));

       return Aggregation.newAggregation(pipelineOperation).withOptions(
                Aggregation.newAggregationOptions().allowDiskUse(true).build());

    }

    public Aggregation buildPostIpaRequest(List<String> referenceIds) {

        {
            List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

            pipelineOperation.add(match(new Criteria().andOperator(
                    Criteria.where("_id").in(referenceIds)))
            );

            pipelineOperation.add(group(fields().and("$header.branchCode"))
                .count().as("count")
                .sum("$postIPA.netDisbursalAmount").as("total"));

            pipelineOperation.add(sort(Sort.Direction.DESC, "count"));

            return Aggregation.newAggregation(pipelineOperation).withOptions(
                    Aggregation.newAggregationOptions().allowDiskUse(true).build());

        }
    }

}