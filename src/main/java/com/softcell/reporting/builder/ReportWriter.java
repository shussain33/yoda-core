package com.softcell.reporting.builder;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.utils.GngDateUtil;
import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * @author prateek
 */
public class ReportWriter {

    /**
     * @param customerApplications
     * @param flatReportConfiguration
     * @return
     * @throws IOException
     */
    public byte[] writeZipFile(
            List<GoNoGoCustomerApplication> customerApplications,
            FlatReportConfiguration flatReportConfiguration) throws IOException {
        String encoding = "UTF8";
        OutputStreamWriter reportOutputStreamWriter = null;
        ByteArrayOutputStream outputStream1 = null;
        try {
            outputStream1 = new ByteArrayOutputStream();
            reportOutputStreamWriter = new OutputStreamWriter(outputStream1,
                    encoding);
            reportOutputStreamWriter.write(flatReportConfiguration
                    .getFileHeader());
            reportOutputStreamWriter.append('\n');
        } catch (IOException e3) {
        }
        GoNoGoApplicationToJsonParser applicationToJsonParser;
        for (GoNoGoCustomerApplication goNoGoCustomerApplication : customerApplications) {
            try {
                applicationToJsonParser = new GoNoGoApplicationToJsonParser(
                        goNoGoCustomerApplication);
                Object record = applicationToJsonParser.build().enrichJson(
                        flatReportConfiguration);
                if (null != record) {
                    reportOutputStreamWriter.write(record.toString());
                    reportOutputStreamWriter.write('\n');
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        reportOutputStreamWriter.close();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(baos);
        ZipEntry personalInfoFileEntry = new ZipEntry("CREDIT_REPORT_"
                + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv");
        zout.putNextEntry(personalInfoFileEntry);
        zout.write(outputStream1.toByteArray());
        zout.setComment("THIS FILE");
        zout.finish();
        zout.closeEntry();
        return baos.toByteArray();
    }


}
