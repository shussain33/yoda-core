package com.softcell.reporting.builder;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author mahesh
 */
public class SchemeReportConfiguration {

    public static SortedMap<Integer, ColumnConfiguration> schemeConfigMap = new TreeMap();

    static {
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme Description");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_schemeDesc999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ValidFromDate");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_validFromDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ValidToDate");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_validToDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Non-Applicable Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_excludedDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Applicable Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_includedDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Manufacturer");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_maufacturer999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("AssetCategory");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_assetCategory999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("AssetMake");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_assetMake999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Model_No");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_modelNo999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_stateName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_cityName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DealerName");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_dealerName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("MakerId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_makerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("MakeDate");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_makeDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CheckerId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_checkerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CheckDate");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_checkDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("SchemeStatus");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_schemeStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ProductName");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_S_productFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        schemeConfigMap.put(index, columnConfiguration);
        index++;

    }

}
