package com.softcell.reporting.builder;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by mahesh on 30/3/17.
 */
public class RollbackFeatureReportConfiguration {

    public static SortedMap<Integer, ColumnConfiguration> serialRollbackConfigMap = new TreeMap();

    static {
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ReferenceId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("R_F_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialRollbackConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Vendor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("R_F_vendor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialRollbackConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DateTime");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("R_F_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialRollbackConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DealerId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("R_F_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialRollbackConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Serial Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("R_F_serialNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialRollbackConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ImeiNumber");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("R_F_imeiNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialRollbackConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("R_F_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        serialRollbackConfigMap.put(index, columnConfiguration);
        index++;
    }
}
