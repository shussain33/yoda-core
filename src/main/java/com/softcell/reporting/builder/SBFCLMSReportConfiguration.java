package com.softcell.reporting.builder;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by kumar on 30/7/18.
 */
public class SBFCLMSReportConfiguration {

    public static SortedMap<Integer, ColumnConfiguration> SBFCLMSLogConfigMap = new TreeMap();

    static {
        int index = 0;

        SBFCLMSLogConfigMap.put(index, getColumnConfiguration(index, "ReferenceId", "S_L_refId999"));
        index++;

        SBFCLMSLogConfigMap.put(index, getColumnConfiguration(index, "Date", "S_L_dateTime999"));
        index++;

        SBFCLMSLogConfigMap.put(index, getColumnConfiguration(index, "Time", "S_L_dateTimeTIME_STAMP999"));
        index++;

        SBFCLMSLogConfigMap.put(index, getColumnConfiguration(index, "ClientId", "S_L_clientId999"));
        index++;

        SBFCLMSLogConfigMap.put(index, getColumnConfiguration(index, "LoanId", "S_L_loanId999"));
        index++;

        SBFCLMSLogConfigMap.put(index, getColumnConfiguration(index, "ResponseMessage", "S_L_responseMsg999"));
        index++;

        SBFCLMSLogConfigMap.put(index, getColumnConfiguration(index, "Status", "S_L_status999"));
        index++;
    }

    private static ColumnConfiguration getColumnConfiguration(int index, String displayName, String columnKey) {

        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName(displayName);
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey(columnKey);
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);

        return columnConfiguration;
    }
}