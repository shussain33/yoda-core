package com.softcell.reporting.builder;


import com.softcell.constants.FieldSeparator;
import com.softcell.dao.mongodb.repository.ReportingMongoDBRepository;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Format;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;


/**
 * utility class to transform various aspect of pojo to
 * dynamic data from db
 *
 * @param <K>
 * @param <V>
 * @author prateek
 */
public class PojoToJSonTransformer<K, V> extends HashMap {

    private static final long serialVersionUID = 1L;
    private static Logger logger = LoggerFactory.getLogger(PojoToJSonTransformer.class);
    private static PojoToJSonTransformer flatJSon;

    private char separator = ',';

    private static void checkTypeObject(Object object, String memberName) {
        if (object instanceof List) {
            setArrayObject((List) object, memberName);
            return;
        }
        if (object instanceof Set<?>) {
            setOfObject((Set) object, memberName);
            return;
        }
        setPrimitive(object, memberName, 999);
    }

    private static void setArrayObject(List<?> arrayOfObject, String listName) {
        if (arrayOfObject != null) {
            for (int index = 0; index < arrayOfObject.size(); index++) {
                if (arrayOfObject.get(index) != null) {
                    if (arrayOfObject.get(index) instanceof List<?>) {
                        setArrayObject((List) arrayOfObject.get(index), listName + index);
                    } else {
                        String fieldName = listName + FieldSeparator.UNDER_SQURE
                                + arrayOfObject.get(index).getClass().getSimpleName();
                        setPrimitive(arrayOfObject.get(index), fieldName, index);
                    }
                }
            }
        }
    }

    private static void setOfObject(Set setOfObject, String listName) {
        if (setOfObject != null) {
            int counter = 0;
            for (Object object : setOfObject) {
                if (object != null) {
                    String fieldName = listName + FieldSeparator.UNDER_SQURE + object.getClass().getSimpleName();
                    setPrimitive(object, fieldName, counter);
                    counter++;
                }
            }
        }
    }

    private static void setPrimitive(Object object, String pojoName, int index) {
        if (object != null) {
            Field[] fields = object.getClass().getSuperclass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                addFieldValue(field, object, pojoName, index);
            }

            fields = object.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                addFieldValue(field, object, pojoName, index);
            }
        }
    }

    private static void addFieldValue(Field field, Object object, String pojoName, int index) {
        try {
            Object fieldValue = field.get(object);
            if (fieldValue != null && (field.getType().isPrimitive() || fieldValue instanceof String || fieldValue instanceof Long || fieldValue instanceof Enum)) {
                flatJSon.put(pojoName + FieldSeparator.UNDER_SQURE + field.getName() + index, fieldValue);
                return;
            }
            if (fieldValue != null && (fieldValue instanceof Date)) {
                flatJSon.put(pojoName + FieldSeparator.UNDER_SQURE + field.getName() + index,
                        setOfDate((Date) fieldValue));
                flatJSon.put(pojoName + FieldSeparator.UNDER_SQURE + field.getName() + reportFormat.TIME_STAMP + index,
                        setOfTimeStamp((Date) fieldValue));
                return;
            }

        } catch (IllegalAccessException e) {
            logger.error(e.getMessage());
        }
    }

    private static Object setOfDate(Date date) {
        return GngDateUtil.getMmDdYyyySlash(new DateTime(date));
    }

    private static Object setOfTimeStamp(Date date) {
        return GngDateUtil.getTimeStamp(new DateTime(date));
    }

    /**
     * @param object
     * @param memberName
     * @return
     */
    public PojoToJSonTransformer build(Object object, String memberName, PojoToJSonTransformer jSonTransformer) {
        flatJSon = jSonTransformer;
        checkTypeObject(object, memberName);
        return flatJSon;
    }

    /**
     * @param flatReprtConfiguration
     * @return
     */
    public Object enrichJson(FlatReportConfiguration flatReprtConfiguration) {

        ReportingMongoDBRepository.keySet.addAll(this.keySet());

        if (flatReprtConfiguration != null
                && StringUtils.containsIgnoreCase(flatReprtConfiguration.getReportFormat(), reportFormat.CSV.name())) {

            separator = flatReprtConfiguration.getSeperater();

            StringBuilder row = buildReport(flatReprtConfiguration.getHeaderMap());

            if (row != null) {
                return StringUtils.replace(row.toString(), "[\r\n]", " ");
            }
        } else {
            if (flatReprtConfiguration != null) {
                return buildJSONReport(flatReprtConfiguration.getHeaderMap());

            } else {
                return buildReport(this);
            }
        }
        return null;
    }

    private StringBuilder buildReport(Map<Integer, ColumnConfiguration> headerMap) {
        StringBuilder builder = null;
        if (headerMap != null) {
            builder = new StringBuilder();
            for (Entry<Integer, ColumnConfiguration> column : headerMap.entrySet()) {
                Object columnValue = this.get(column.getValue().getColumnKey());
                if (columnValue != null) {
                    if (null == column.getValue().getFormat()) {
                        builder.append('"').append(columnValue).append('"').append(separator);
                    } else {

                        builder.append('"').append(getNewFormat(columnValue, column.getValue().getFormat()))
                                .append('"').append(separator);
                    }
                } else {
                    builder.append('"').append(' ').append('"').append(separator);
                }
            }
        }
        return builder;
    }

    private Object getNewFormat(Object columnValue, Format format) {
        try {

            if (Format.DataType.date.equals(format.getDataType())
                    && Format.Action.convert.equals(format.getAction())) {
                String date = columnValue.toString();
                if (StringUtils.isNotBlank(date))
                    return GngDateUtil.changeDateFormat(format,
                            columnValue.toString());
            }

            if (Format.DataType.string.equals(format.getDataType())
                    && Format.Action.replace.equals(format.getAction())) {
                columnValue = StringUtils.replace(columnValue.toString().trim(),
                        format.getFrom(), format.getTo());
            }
            if (Format.Action.capitalize.equals(format.getCapitalize()) &&
                    Format.DataType.string.equals(format.getDataType())) {
                columnValue = StringUtils.capitalize(columnValue.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return columnValue;
        }
        return columnValue;
    }

    private Map<String, Object> buildJSONReport(Map<Integer, ColumnConfiguration> headerMap) {
        Map<String, Object> keyValueMap = new LinkedHashMap<>();

        if (headerMap != null) {
            for (Entry<Integer, ColumnConfiguration> column : headerMap.entrySet()) {
                Object columnValue = this.get(column.getValue().getColumnKey());
                if (columnValue != null) {
                    keyValueMap.put(column.getValue().getColumnDisplayName(), columnValue);
                } else {
                    keyValueMap.put(column.getValue().getColumnDisplayName(), '-');
                }
            }
            return keyValueMap;
        }
        return keyValueMap;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + separator;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PojoToJSonTransformer<K, V> other = (PojoToJSonTransformer<K, V>) obj;
        if (separator != other.separator)
            return false;
        return true;
    }

    private enum reportFormat {
        CSV, JSON, TIME_STAMP
    }


}