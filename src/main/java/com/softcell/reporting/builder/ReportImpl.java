package com.softcell.reporting.builder;

import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.reports.ChannelRptDomain;
import com.softcell.gonogo.model.reports.DeviceInfoRptDomain;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author yogeshb
 */

@Service
public class ReportImpl implements Report {

    @Override
    public List<ChannelRptDomain> createChannelWiseReport(List<GoNoGoCustomerApplication> goNoGoCustomerApplications) {

        List<ChannelRptDomain> channelReportList = new ArrayList<ChannelRptDomain>();

        for (GoNoGoCustomerApplication goNoGoCustomerApplication : goNoGoCustomerApplications) {

            ChannelRptDomain channelReport = new ChannelRptDomain();
            channelReport.setRefID(goNoGoCustomerApplication.getGngRefId());
            channelReport.setDate(goNoGoCustomerApplication.getDateTime());
            channelReport.setTime(goNoGoCustomerApplication.getDateTime());
            try {
                channelReport.setDsaID(goNoGoCustomerApplication
                        .getApplicationRequest().getHeader().getDsaId());
            } catch (Exception exp) {

            }
            try {
                if (null != goNoGoCustomerApplication.getApplicationRequest()
                        .getHeader()
                        && null != goNoGoCustomerApplication
                        .getApplicationRequest().getHeader()
                        .getApplicationSource()) {
                    channelReport.setChannel(goNoGoCustomerApplication
                            .getApplicationRequest().getHeader()
                            .getApplicationSource());
                } else {
                    channelReport.setChannel("Android");
                }

            } catch (Exception exp) {

            }
            channelReportList.add(channelReport);
        }
        return channelReportList;
    }

    @Override
    @Deprecated
    public List<DeviceInfoRptDomain> createDeviceInformationReport(List<GoNoGoCustomerApplication> goNoGoCustomerApplications) {

        List<DeviceInfoRptDomain> deviceInfoList = new ArrayList<DeviceInfoRptDomain>();

        for (GoNoGoCustomerApplication goNoGoCustomerApplication : goNoGoCustomerApplications) {

            DeviceInfoRptDomain deviceInformationReport = new DeviceInfoRptDomain();

            deviceInformationReport.setReferenceID(goNoGoCustomerApplication.getGngRefId());

            deviceInformationReport.setDate(goNoGoCustomerApplication.getDateTime());

            deviceInformationReport.setTime(goNoGoCustomerApplication.getDateTime());

            if (null != goNoGoCustomerApplication.getApplicationRequest()
                    && null != goNoGoCustomerApplication
                    .getApplicationRequest().getHeader()) {

                deviceInformationReport.setAppSource(goNoGoCustomerApplication.getApplicationRequest().getHeader()
                        .getApplicationSource());

                deviceInformationReport.setDsaID(goNoGoCustomerApplication
                        .getApplicationRequest().getHeader().getDsaId());

                deviceInformationReport
                        .setInstitutionID(goNoGoCustomerApplication
                                .getApplicationRequest().getHeader()
                                .getInstitutionId());

                if (null != goNoGoCustomerApplication.getApplicationRequest()
                        .getHeader().getDeviceInfo()) {

                    deviceInformationReport
                            .setIpAddress(goNoGoCustomerApplication
                                    .getApplicationRequest().getHeader()
                                    .getDeviceInfo().getIpAddress());

                    deviceInformationReport
                            .setImeiNumber(goNoGoCustomerApplication
                                    .getApplicationRequest().getHeader()
                                    .getDeviceInfo().getImeiNumber());

                    deviceInformationReport
                            .setDeviceID(goNoGoCustomerApplication
                                    .getApplicationRequest().getHeader()
                                    .getDeviceInfo().getDeviceID());


                    deviceInformationReport.setDeviceManufacturer(goNoGoCustomerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getDeviceManufacturer());


                    deviceInformationReport.setDeviceModel(goNoGoCustomerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getDeviceModel());


                    if (null != goNoGoCustomerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getBrowserDetails()) {
                        deviceInformationReport
                                .setBrowserName(goNoGoCustomerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getBrowserDetails()
                                        .getBrowserName());
                        deviceInformationReport
                                .setBrowserVersion(goNoGoCustomerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getBrowserDetails()
                                        .getVersion());
                    }

                    if (null != goNoGoCustomerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getGeoLocation()) {
                        deviceInformationReport.setLattitude(String
                                .valueOf(goNoGoCustomerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getGeoLocation()
                                        .getLatitude()));
                        deviceInformationReport.setLongitude(String
                                .valueOf(goNoGoCustomerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getGeoLocation()
                                        .getLongitude()));
                    }

                    if (null != goNoGoCustomerApplication
                            .getApplicationRequest().getHeader()
                            .getDeviceInfo().getOsDetails()) {
                        deviceInformationReport
                                .setMacAddress(goNoGoCustomerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getOsDetails()
                                        .getMacAddress());
                        deviceInformationReport
                                .setOsName(goNoGoCustomerApplication
                                        .getApplicationRequest().getHeader()
                                        .getDeviceInfo().getOsDetails()
                                        .getOsName());
                    }

                }
            }
            deviceInfoList.add(deviceInformationReport);
        }
        return deviceInfoList;
    }
}
