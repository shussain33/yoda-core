package com.softcell.reporting.builder;

import com.softcell.reporting.domains.ColumnConfiguration;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by sampat on 7/11/17.
 */
public class AccountNumberReportConfiguration {

    public static SortedMap<Integer, ColumnConfiguration> accountNumberLogConfigMap = new TreeMap();

    static {
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ReferenceId");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("AttemptID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_attemptID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Product");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_product999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time_Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Bank_Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_bankName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Bank_Branch");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_bankBranch999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account_Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_accountNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account_Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_accountType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("AccountHolder_FName");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_accountHolderFname999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("AccountHolder_MName");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_accountHolderMname999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("AccountHolder_LName");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_accountHolderLname999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("IFSC_Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_ifscCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Years_Held");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_yearsHeld999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Banking_Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_bankingType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account_Identifier");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_accountIdentifier999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Original_ResponseCode");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_originalResponseCode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Original_ResponseMsg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_originalResponseMsg999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Paynimo_Res_AccHolderName");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_responseAccHolderName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Name_Matching_Result");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_nameMatchingResult999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Custom_Message");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_N_customMsg999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        accountNumberLogConfigMap.put(index, columnConfiguration);
        index++;

    }
}
