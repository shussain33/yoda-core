package com.softcell.reporting.builder;

import com.mongodb.BasicDBObject;

public class GroupMongoId extends BasicDBObject {
    /**
     *
     */

    private static final long serialVersionUID = 1L;

    public GroupMongoId() {
        new BasicDBObject();
    }

    public void groupBy(String fieldName, String as) {
        this.put(fieldName, "$" + as);
    }

    public void groupBy(String fieldName) {
        this.put(fieldName, "$" + fieldName);
    }

}