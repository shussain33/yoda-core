package com.softcell.reporting.report;

import com.softcell.config.ReportEmailConfiguration;
import com.softcell.constants.FieldSeparator;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.masters.DealerEmailMaster;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.reporting.builder.GoNoGoApplicationToJsonParser;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Format;
import com.softcell.reporting.domains.Format.Action;
import com.softcell.reporting.domains.Format.DataType;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CCBTReport {

    public static SortedMap<Integer, ColumnConfiguration> defaultConfigMapVersion2 = new TreeMap();

    MongoOperations mongoOperation = (MongoOperations) MongoConfig.getMongoTemplate();


    static {

        //
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Login Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        Format format = new Format();
        format.setAction(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Queue ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("STA");
        format.setTo("Straight  Through Process");
        format.setAction(Action.replace);
        format.setCapitalize(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Current Stage");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Marital Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Gender");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Date Of Birth");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setTo("MM/dd/yyyy");
        format.setFrom("ddMMyyyy");
        format.setAction(Action.convert);
        format.setDataType(DataType.date);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Age");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("No Of Dependents");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_noOfDependents999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LandMark");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Month At City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Month At Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Rent Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_residenceAddressType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //K
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LandMark");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Month At City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Month At Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Rent Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_residenceAddressType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LandMark");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_landMark2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Month At City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtCity2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Month At Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_monthAtAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Rent Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_rentAmount2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employment Constitution");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //**//Emp name
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employment Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//EMP TYPE
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employment Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Industry Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_industryType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time With Employer");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_timeWithEmployer0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Monthly Salary");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_monthlySalary0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Business Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_businessName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ITR ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_itrId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//CREDIT CARD NUMBER
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CREDIT CARD NUMBER");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_cardNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//CREDIT CARDS CATEGORY
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CREDIT CARDS CATEGORY");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_cardCategory0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//CREDIT CARD VINTAGE
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CREDIT CARD VINTAGE");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_cardVintage0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//CREDIT CARD LIMIT
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CREDIT CARD LIMIT");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_limit0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//CREDIT CARD OUTSTANDING
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CREDIT CARD OUTSTANDING");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_outStandingAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//CREDIT CARD LAST PAYMENT
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CREDIT CARD LAST PAYMENT");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_lastPaymentAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount By CRO");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Eligible Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("PAN Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("NO_RESPONSE");
        format.setTo("Pan Not Submitted");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("PAN Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("No Response");
        format.setTo("Pan Not Submitted");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CIBIL Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("cibil_fieldValue999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//APPLICATION SCORE
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("APPLICATION SCORE");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//RESIDENT ADDRESS SCORE
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("RESIDENT ADDRESS SCORE");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//OFFICE ADDRESS SCORE
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("OFFICE ADDRESS SCORE");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Mobile Verification");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_approvedAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_losID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_utrNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Applied Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loan Tenor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//LOAN TYPE
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loan Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ELIGIBLEAMT");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_eligibilityAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Grid ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_gridId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Outcome");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_decision999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        //**//Res. Stability not there
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Res. Stability");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_addStability0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//Off Stability not there
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Off Stability");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_addStability3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//Source not there testing mode
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Source");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_applicationSource999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//FOIR not there
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("FOIR");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_foir_amount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//second time Eligibility
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Eligibility Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_eligibilityAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        //**//Eligibility Grid

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Eligibility Grid");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_gridId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


    }

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Query serialNumberInfoQuery = new Query();
        serialNumberInfoQuery.addCriteria(Criteria.where("refID").is("").and("status").is("VALID"));
        System.out.println(serialNumberInfoQuery);
        Query query = QueryBuilder.buildGoNoGoApplicationQuery("4019",
                new DateTime(DateTimeZone.UTC).withDayOfMonth(1).minusMonths(5).toDate(), new Date(),
                "Consumer Durables");

        CCBTReport CCBTReport = new CCBTReport();

        CCBTReport.getCreditReport(query, query);

        SalesReport salesReport = new SalesReport();
        salesReport.getSaleReport(query, query);
        salesReport.getSaleReportMonthly(query, query);
    }

    /**
     * @param gonogoCustomerQuery
     * @param applicationRequestQuery
     * @return
     * @throws IOException
     */
    public  byte[] getCreditReport(Query gonogoCustomerQuery, Query applicationRequestQuery) throws IOException {

        String encoding = "UTF8";
        List<GoNoGoCustomerApplication> result = mongoOperation.find(gonogoCustomerQuery,GoNoGoCustomerApplication.class);

        List<ApplicationRequest>  applicationRequestRecords = mongoOperation.find(applicationRequestQuery, ApplicationRequest.class);

        if(!CollectionUtils.isEmpty(applicationRequestRecords)) {
            result.addAll(GngUtils.getGoNoGoApplicationWithDECases(applicationRequestRecords));
        }

        OutputStreamWriter reportOutputStreamWriter = null;
        ByteArrayOutputStream outputStream1 = null;
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
                new File(GngUtils.getReSourcePath() + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv")));

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();
        flatReprtConfiguration.setHeaderMap(defaultConfigMapVersion2);
        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);
        flatReprtConfiguration.setReportName("GNGVersion2");
        flatReprtConfiguration.setReportType("CSV");
        flatReprtConfiguration.setReportFormat("CSV");
        GoNoGoApplicationToJsonParser applicationToJsonParser;

        try {
            outputStream1 = new ByteArrayOutputStream();
            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, encoding);
            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());
            reportOutputStreamWriter.append('\n');
            bufferedWriter.write(flatReprtConfiguration.getFileHeader());
            bufferedWriter.newLine();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        /* Post IPA Object to get DO details of customer */
        for (GoNoGoCustomerApplication goNoGoCustomerApplication : result) {
            applicationToJsonParser = new GoNoGoApplicationToJsonParser(goNoGoCustomerApplication);

            Query serialNumberInfoQuery = new Query();
            serialNumberInfoQuery.addCriteria(
                    Criteria.where("refID").is(goNoGoCustomerApplication.getGngRefId()).and("status").is("VALID"));
            SerialNumberInfo serialNumberInfo = mongoOperation.findOne(serialNumberInfoQuery, SerialNumberInfo.class);
            if (null != serialNumberInfo) {
                applicationToJsonParser.build(serialNumberInfo);
            } else {

            }

            Query postIPAQuery = new Query();
            postIPAQuery.addCriteria(Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId()));
            PostIpaRequest postIPA = mongoOperation.findOne(postIPAQuery, PostIpaRequest.class);
            String row;
            addMetaData(goNoGoCustomerApplication, applicationToJsonParser);
            if (null != postIPA) {
                applicationToJsonParser.build();
                row = applicationToJsonParser.build(postIPA).enrichJson(flatReprtConfiguration).toString()
                        .replaceAll("[\\\n|\\\r|\\\t]", "");
            } else {
                row = applicationToJsonParser.build().enrichJson(flatReprtConfiguration).toString()
                        .replaceAll("[\\\n|\\\r|\\\t]", "");
            }

            bufferedWriter.write(row);
            bufferedWriter.newLine();
            reportOutputStreamWriter.write(row);
            reportOutputStreamWriter.write('\n');
        }

        bufferedWriter.flush();
        bufferedWriter.close();
        reportOutputStreamWriter.close();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(baos);
        ZipEntry personalInfoFileEntry = new ZipEntry(
                "CREDIT_REPORT_" + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv");
        zout.putNextEntry(personalInfoFileEntry);
        zout.write(outputStream1.toByteArray());
        zout.setComment("Credit Report");

        zout.finish();
        zout.closeEntry();

        return baos.toByteArray();

    }

    private void addMetaData(GoNoGoCustomerApplication goNoGoCustomerApplication,
                             GoNoGoApplicationToJsonParser applicationToJsonParser) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dealerID")
                .is(goNoGoCustomerApplication.getApplicationRequest().getHeader().getDealerId()).and("active").is(true));
        DealerEmailMaster dealerEmailMaster = mongoOperation.findOne(query, DealerEmailMaster.class);

        if (null != dealerEmailMaster) {
            applicationToJsonParser.populateMetaData(dealerEmailMaster);
        }
    }

    /**
     * @param reportType
     * @param productType
     * @param institutionID
     * @return This method is returns email registered for particular report.
     */
    public List<ReportEmailConfiguration> getReportEmailConfiguration(String reportType, String productType,
                                                                      String institutionID) {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("active").is(true).and("reportType").is(reportType).and("productType")
                    .is(productType).and("institutionID").is(institutionID));
            return mongoOperation.find(query, ReportEmailConfiguration.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }

    }

    /**
     * @return This method returns all details which is filter for building
     * query to generate report.
     */
    public List<ReportingModuleConfiguration> getReportingModuleConfiguration() {
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("active").is(true));
            return mongoOperation.find(query, ReportingModuleConfiguration.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }
}

