/**
 * kishorp5:04:53 PM  Copyright Softcell Technology
 **/
package com.softcell.reporting.report;

import com.google.common.base.CharMatcher;
import com.softcell.config.ReportEmailConfiguration;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.GNGWorkflowConstant;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.ReportingRepository;
import com.softcell.dao.mongodb.repository.loyaltycard.LoyaltyCardRepository;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.masters.BranchMaster;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.emudra.ESignedLog;
import com.softcell.gonogo.model.request.emudra.SFTPLog;
import com.softcell.gonogo.model.request.emudra.SFTPLogOutput;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.insurance.InsurancePremiumDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.reporting.builder.GoNoGoApplicationToJsonParser;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Format;
import com.softcell.reporting.domains.Format.Action;
import com.softcell.reporting.domains.Format.DataType;
import com.softcell.utils.GnGFileUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author kishorp This class is used to generate credit report of HDBFS. For PL
 *         salary and CDL.
 */
@Component
public class HDBFSReport {

    private static final Logger logger = LoggerFactory.getLogger(HDBFSReport.class);
    public static SortedMap<Integer, ColumnConfiguration> defaultConfigMapVersion2 = new TreeMap();

    static {

        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Product Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("applicantType");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DSA ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Login Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Source");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_applicationSource999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Submit Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        Format format = new Format();
        format.setAction(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("HDB Branch Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("branchName");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("HDB Branch State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("branchState");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Queue ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("STA");
        format.setTo("Straight  Through Process");
        format.setAction(Action.replace);
        format.setCapitalize(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Current Stage");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Father First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("F_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Father Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("F_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Father Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("F_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Mother First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("M_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Mother Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("M_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Mother Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("M_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Marital Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Gender");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setTo("MM/dd/yyyy");
        format.setFrom("ddMMyyyy");
        format.setAction(Action.convert);
        format.setDataType(DataType.date);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Age");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Education");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_education999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("statusFlag");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_statusFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line31");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line32");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailAddress2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employ Constitution ");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employer Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employer Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Aadhar Verified");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("isAadharVerified");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Net Take Home-salary ");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_SalariedSurrogate_netTakeHome0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car Model Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CarSurrogate_modelName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car Category");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CarSurrogate_category0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car manufacturer Year");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CarSurrogate_manufactureYear0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car Registration Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_S_CarSurrogate_registrationNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car Surrogate Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_S_CarSurrogate_sType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Own House Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_houseType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("House Surrogate-Document Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_documentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("House Surrogate-Owner Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_ownerName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("House Surrogate-Owner Mob No");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_ownerMobNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("House Surrogate-Service Provider");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_serviceProvider0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("House Surrogate-ConsumerIdOrAccNo");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_consumerIdOrAccNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Trader-Year In Business");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_TraderSurrogate_yearInBussines0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Trader-Business Proof");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_TraderSurrogate_bussinesProof0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_cardNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Cards Category");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_S_CreditCardSurrogate_cardCategory0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Vintage");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_S_CreditCardSurrogate_cardVintage0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Limit");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_limit0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("Credit Card Outstanding Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_S_CreditCardSurrogate_outStandingAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("Credit Card Last Payment Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("C_S_CreditCardSurrogate_lastPaymentAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Debit Card Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_DebitCardSurrogate_cardNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dedupe Ref Id1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dedupe Ref Id2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dedupe Ref Id3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount By CRO");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Down Payment");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_downPayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Eligible Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Stability1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("-1");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Stability 2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_addStability5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("-1");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("PAN Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("NO_RESPONSE");
        format.setTo("Pan Not Submitted");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("PAN Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("No Response");
        format.setTo("Pan Not Submitted");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);

        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CIBIL Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("cibil_fieldValue999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Experian Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("experian_fieldValue999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residential Address Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Office Address Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Name Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("A_ApplicationScoringResult_nameScore0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Negative Pin Flag");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_negativePincode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Mobile Verification");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoney999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Subvention Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_dealerSubvention999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Instrument");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyInstrument999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        format = new Format();
        format.setFrom("Select Margin Money Instrument");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_approvedAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Make");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("processing Fees");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_processingFees999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("other Charges");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_otherChargesIfAny999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Total Asset Cost");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_totalAssetCost999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Ctg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Advance EMI");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_advanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_scheme999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("Manufacture Sub Born By Dealer");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufSubBorneByDealer999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Price");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName(" Asset Model No");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Confirm");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyConfirmation999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_losID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_utrNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Error Msg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_error999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Reference Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_dealerReferenceNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Flag");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_dealerTieUpFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_croID0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO subject to");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_sSubTo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Applied Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loan Tenor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loan Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("GSTIN NUMBER");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("gstNumber");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loyalty Card Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("loyaltyCardNumber");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loyalty Card Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("loyaltyCardType");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loyalty Card Price");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("loyaltyCardPrice");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loyalty Card Emi");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("loyaltyCardEmi");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("EW Asset Category");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("eWAssetCategory");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("EW Tenor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("extendedWarrantyTenure");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("EW Premium");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("extendedWarrantyPremium");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("EW Emi");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("ewEmi");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Bank Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_BankingDetails_bankName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Bank Branch");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_BankingDetails_branchName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("IFSC Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_BankingDetails_ifscCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_BankingDetails_accountType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_BankingDetails_accountNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account Holder First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("accountHolderFirstName");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account Holder Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("accountHolderMiddleName");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Account Holder Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("accountHolderLastName");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primary Asset Ctg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primary Asset Make");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primary Asset ModelNo");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primary Asset Price");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Eligible Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_eligibilityAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Eligibility Grid");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("eligibilityGrid");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Grid ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_gridId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Outcome");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_decision999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("reMark");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_reMark999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Rule Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("sd_ruleId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme Dscr");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_schemeDscr999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_scheme999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Tenor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_tenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("finance Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_financeAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("advance EMI Tenor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_advanceEMITenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("net Funding Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_netFundingAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("manuf. Subvention Mbd");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufSubventionMbd999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("manufacturing Processing Fee");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration
                .setColumnKey("postIPA_manufacturingProcessingFee999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Sbvn. Waived At POS");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_dlrSbvnWaivedAtPOS999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DO Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_REQUEST_date999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Suspicious Activity");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_suspiciousActivity999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("SerialNumber/IMEI");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("serialNumber_imei");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("SerialNumber/IMEI Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("serialNumber_imei_status");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Invoice Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("invoice_invoiceNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Invoice Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("invoice_invoiceType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Invoice Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("invoice_invoiceDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Invoice Serial Or Imei Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("invoice_serialOrImeiNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Invoice_Shipping_Address_Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_addressType");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_addressLine1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Shipping Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_addressLine2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Shipping Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_line3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Shipping Address LandMark");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_landMark");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Shipping Address PinCode");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_pin");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Shipping Address City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_city");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Shipping Address State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("S_H_state");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Handover Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("handoverDate");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Received Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("receivedDate");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("File Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("fileStatus");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Hold Reason");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("holdReason");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Reappraisal Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Reinitiate  Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Disbursement Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("disbursementDate");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Rank");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_dealerRank999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Negative Area Flag");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("negativeAreaFlag");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Negative Area Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("negativeArea");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Negative Area Reason");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("negativeAreaReason");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("MBD Percentage");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("mbdPercentage");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Insurance Asset Category");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("insuranceAssetCategory");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Insurance Premium");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("insurancePremium");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Monthly Income");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("M_I_monthIncome0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;


    }

    @Autowired
    ApplicationRepository applicationRepository;
    @Autowired
    private ReportingRepository reportingRepository;
    @Autowired
    private LoyaltyCardRepository loyaltyCardRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * @param reportingModuleConfiguration
     * @return
     * @throws IOException
     */
    public byte[] getCreditReport(ReportingModuleConfiguration reportingModuleConfiguration) throws IOException {

        logger.debug(" credit report is running with thread as [{}] at [{}]", Thread.currentThread().getName(), System.currentTimeMillis());

        String productType = reportingModuleConfiguration.getProductType();
        Date[] fromToDate = GngDateUtil.getFromToDate(reportingModuleConfiguration);

        Query gonogoCustomerQuery = QueryBuilder.buildGoNoGoApplicationQuery(
                reportingModuleConfiguration.getInstitutionID(), fromToDate[0], fromToDate[1],
                productType);


        Query applicationRequestQuery = QueryBuilder.buildApplicationRequestQuery(
                reportingModuleConfiguration.getInstitutionID(),
                fromToDate[0], fromToDate[1],
                productType);

        byte[] zipByteArray = null;

        if (StringUtils.equals(productType, Product.CDL.name()) || StringUtils.equals(productType, Product.DPL.name())) {

            zipByteArray = getCreditReportForCDLAndDPL(gonogoCustomerQuery);

        } else if (StringUtils.equals(productType, Product.CCBT.name())) {

            zipByteArray = new CCBTReport().getCreditReport(gonogoCustomerQuery, applicationRequestQuery);

        } else if (StringUtils.equals(productType, Product.PL.name())) {

            zipByteArray = new PLReport().getCreditReport(gonogoCustomerQuery, applicationRequestQuery);

        } else if (StringUtils.equals(productType, Product.AL.name())) {

            zipByteArray = new AutoLoan().getCreditReport(gonogoCustomerQuery, applicationRequestQuery);

        } else if (StringUtils.equals(productType, Product.BL.name())) {

            zipByteArray = new BusinessLoan().getCreditReport(gonogoCustomerQuery, applicationRequestQuery);

        } else if (Cache.securedLoanProduct.contains(productType)) {
            zipByteArray = new SecuredLoan().getCreditReport(gonogoCustomerQuery, applicationRequestQuery);
        }

        return zipByteArray;
    }


    private byte[] getCreditReportForCDLAndDPL(Query gonogoCustomerQuery) throws IOException {

        OutputStreamWriter reportOutputStreamWriter = null;

        ByteArrayOutputStream outputStream1 = null;

        BufferedWriter bufferedWriter = null;

        if (GngDateUtil.checkScheduleTime(23)) {

            bufferedWriter = new BufferedWriter(new FileWriter(new File(GngUtils.getReSourcePath()
                    + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv")));

        }

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

        flatReprtConfiguration.setHeaderMap(defaultConfigMapVersion2);
        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);
        flatReprtConfiguration.setReportName("GNGVersion2");
        flatReprtConfiguration.setReportType("CSV");
        flatReprtConfiguration.setReportFormat("CSV");

        GoNoGoApplicationToJsonParser applicationToJsonParser;

        try {

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            if (GngDateUtil.checkScheduleTime(23)) {

                bufferedWriter.write(flatReprtConfiguration.getFileHeader());
                bufferedWriter.newLine();

            }
        } catch (IOException e3) {

            e3.printStackTrace();

        }

        List<GoNoGoCustomerApplication> goNoGoCustomerApplications = reportingRepository.getGoNoGoCustomerApplicationForCredit(gonogoCustomerQuery);

        /**
         * Post IPA Object to get DO details of customer
         */
        for (GoNoGoCustomerApplication goNoGoCustomerApplication : goNoGoCustomerApplications) {

            applicationToJsonParser = new GoNoGoApplicationToJsonParser(goNoGoCustomerApplication);

            String refId = goNoGoCustomerApplication.getGngRefId();
            String instId = goNoGoCustomerApplication.getApplicationRequest().getHeader() != null
                    ? goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId() : null;
            String dealerId = goNoGoCustomerApplication.getApplicationRequest().getHeader().getDealerId();

            addSerialNumberInformationInCredit(applicationToJsonParser, refId);

            addNegativeAreaDetailsAndInvoiceDetailsInCreditReport(applicationToJsonParser, goNoGoCustomerApplication);

            addFileHandOverDetailsInCreditReport(applicationToJsonParser, refId, instId);

            addLoyaltyCardDetailsInCreditReport(applicationToJsonParser, refId, instId);

            addExtendedWarrantyDetailsInCreditReport(applicationToJsonParser, refId, instId);

            addInsuranceDetailsInCreditReport(applicationToJsonParser, refId, instId);

            addGstDetailsInCreditReport(applicationToJsonParser, refId, instId);

            addDealerBranchNameInCreditReport(applicationToJsonParser, instId, dealerId);

            addBankAccountHolderNameInCreditReport(applicationToJsonParser, instId, refId);

            addApplicantTypeInCreditReport(applicationToJsonParser, goNoGoCustomerApplication.getApplicationRequest().getApplicantType());

            Query postIPAQuery = new Query();

            postIPAQuery.addCriteria(Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId()));

            PostIpaRequest postIPA = reportingRepository.getPostIpaByRefId(goNoGoCustomerApplication.getGngRefId());


            StringBuffer row = new StringBuffer();

            if (null != postIPA) {

                applicationToJsonParser.build();

                row.append(
                        CharMatcher.anyOf("\r\n\t").removeFrom(applicationToJsonParser.build(postIPA).enrichJson(flatReprtConfiguration).toString())
                );

            } else {

                row.append(
                        CharMatcher.anyOf("\r\n\t").removeFrom(applicationToJsonParser.build().enrichJson(flatReprtConfiguration).toString())
                );

            }

            if (GngDateUtil.checkScheduleTime(23)) {

                bufferedWriter.write(row.toString());
                bufferedWriter.newLine();

            }

            reportOutputStreamWriter.write(row.toString());
            reportOutputStreamWriter.write('\n');

        }

        if (GngDateUtil.checkScheduleTime(23)) {
            bufferedWriter.flush();
            bufferedWriter.close();
        }

        reportOutputStreamWriter.close();

        return GnGFileUtils.compressBytes(
                "CREDIT_REPORT_" + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                outputStream1.toByteArray());

    }

    private void addApplicantTypeInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String applicantType) {

        if (StringUtils.isNotBlank(applicantType)) {
            applicationToJsonParser.buildApplicantType(applicantType);
        }
    }

    private void addGstDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId, String instId) {
        GstDetails gstDetails = applicationRepository.fetchGstDetails(refId, instId);
        if (null != gstDetails) {
            applicationToJsonParser.build(gstDetails);
        }
    }

    private void addDealerBranchNameInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String instId, String dealerId) {
        DealerBranchMaster dealerBranchMaster = reportingRepository
                .getBranchMaster(dealerId, instId);
        if (null != dealerBranchMaster) {
            applicationToJsonParser.build(dealerBranchMaster);
            if (StringUtils.isNotBlank(dealerBranchMaster.getBranchName())) {
                BranchMaster branchMaster = applicationRepository.getDealerBranchState(dealerBranchMaster.getBranchName(), instId);
                if (null != branchMaster && StringUtils.isNotBlank(branchMaster.getStateName())) {
                    applicationToJsonParser.build(branchMaster.getStateName());
                }
            }
        }
    }


    private void addBankAccountHolderNameInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String instId, String refId) {
        List<BankingDetails> bankingDetails = applicationRepository.getBankAccountHolderName(instId, refId);
        if (!CollectionUtils.isEmpty(bankingDetails)) {
            applicationToJsonParser.build(bankingDetails);
        }
    }

    private void addInsuranceDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId, String instId) {
        InsurancePremiumDetails insurancePremiumDetails = applicationRepository.fetchInsuranceDetails(instId, refId);
        if (null != insurancePremiumDetails) {
            applicationToJsonParser.build(insurancePremiumDetails);

        }
    }

    private void addExtendedWarrantyDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId, String instId) {

        ExtendedWarrantyDetails extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(refId, instId);

        if (null != extendedWarrantyDetails) {
            applicationToJsonParser.build(extendedWarrantyDetails);
        }
    }

    private void addLoyaltyCardDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId, String instId) {

        LoyaltyCardDetails loyaltyCardDetails = loyaltyCardRepository.fetchLoyaltyCardDetails(refId, instId);

        if (null != loyaltyCardDetails) {
            applicationToJsonParser.build(loyaltyCardDetails);

        }


    }


    private void addFileHandOverDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId, String instId) {
        Query filehandoverdetailsQuery = new Query();


        filehandoverdetailsQuery.addCriteria(Criteria.where("refId").is(refId)
                .and("header.institutionId").is(instId));

        FileHandoverDetails filehandoverdetails = reportingRepository.getFileHandOverDetails(filehandoverdetailsQuery);

        if (null != filehandoverdetails) {

            applicationToJsonParser.build(filehandoverdetails);

        }
    }

    private void addNegativeAreaDetailsAndInvoiceDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, GoNoGoCustomerApplication goNoGoCustomerApplication) {

        if (null != goNoGoCustomerApplication.getApplicationRequest() &&
                null != goNoGoCustomerApplication.getApplicationRequest().getRequest() &&
                null != goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant() &&
                null != goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress()) {

            List<CustomerAddress> customerAddressesList = goNoGoCustomerApplication.getApplicationRequest().getRequest().getApplicant().getAddress();
            /**
             * for negative area purpose
             */
            if (null != customerAddressesList && !customerAddressesList.isEmpty()) {
                for (CustomerAddress customerAddress : customerAddressesList) {

                    if (StringUtils.equals(customerAddress.getAddressType(), GNGWorkflowConstant.RESIDENCE.toFaceValue())) {

                        applicationToJsonParser.build(customerAddress);

                        break;
                    }
                }
            }
        }
        if (null != goNoGoCustomerApplication.getInvoiceDetails() && null != goNoGoCustomerApplication.getInvoiceDetails().getDeliveryAddress()) {
            applicationToJsonParser.build(goNoGoCustomerApplication.getInvoiceDetails());
        }
    }

    private void addSerialNumberInformationInCredit(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId) {

        Query serialNumberInfoQuery = new Query();

        serialNumberInfoQuery.addCriteria(new Criteria().orOperator(
                Criteria.where("refID").is(refId)
                        .and("status").is(Status.VALID.name()),
                Criteria.where("refID").is(refId)
                        .and("status")
                        .is(Status.ROLL_BACK.name())));

        SerialNumberInfo serialNumberInfo = reportingRepository.getSerialNoInfo(serialNumberInfoQuery);

        if (null != serialNumberInfo) {

            applicationToJsonParser.build(serialNumberInfo);

        }
    }


    /**
     * @param reportType
     * @param productType
     * @param institutionID
     * @return This method is returns email registered for particular report.
     */
    public List<ReportEmailConfiguration> getReportEmailConfiguration(
            String reportType, String productType, String institutionID) {

        List<ReportEmailConfiguration> reportEmailConfigurations = new ArrayList<>();

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true)
                    .and("reportType").is(reportType).and("productType")
                    .is(productType).and("institutionID").is(institutionID));

            reportEmailConfigurations = mongoTemplate.find(query, ReportEmailConfiguration.class);

        } catch (DataAccessException ex) {
            ex.printStackTrace();
            logger.error(" error occurred while fetching report email configuration with probable cause [{}] ", ex.getMessage());

        }

        return reportEmailConfigurations;

    }

    /**
     * @return This method returns all details which is filter for building
     * query to generate report.
     */
    public List<ReportingModuleConfiguration> getReportingModuleConfiguration() {

        List<ReportingModuleConfiguration> reportingModuleConfigurations = new ArrayList<>();

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true));

            reportingModuleConfigurations = mongoTemplate.find(query, ReportingModuleConfiguration.class);

        } catch (DataAccessException ex) {

            ex.printStackTrace();

            logger.error(" error occurred while getting active reportConfiguration with probable cause [{}] ", ex.getMessage());
        }

        return reportingModuleConfigurations;

    }


    /**
     * @param refId
     * @return
     * @throws IOException
     */
    public byte[] getSignedXMLEMudra(String refId, String insId) throws IOException {

        SFTPLog sftpLog = new SFTPLog();

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId).and("institutionId").is(insId));

            query.with(new Sort(Sort.Direction.DESC, "date"));

            sftpLog = mongoTemplate.findOne(query, SFTPLog.class);

        } catch (DataAccessException ex) {

            ex.printStackTrace();

            logger.error(" error occurred while getting active reportConfiguration with probable cause [{}] ", ex.getMessage());
        }

        String encoding = "UTF8";
        ByteArrayOutputStream outputStream1 = null;
        outputStream1 = new ByteArrayOutputStream();
        OutputStreamWriter reportOutputStreamWriter = new OutputStreamWriter(outputStream1, encoding);

        reportOutputStreamWriter.write(sftpLog.getFile());

        reportOutputStreamWriter.close();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(baos);
        ZipEntry personalInfoFileEntry = new ZipEntry(sftpLog.getFileName());
        zout.putNextEntry(personalInfoFileEntry);
        zout.write(outputStream1.toByteArray());
        /*zout.setComment("Credit Report");*/

        zout.finish();
        zout.closeEntry();
        return baos.toByteArray();
    }


    public List<SFTPLogOutput> getSignedXMLBYrefId(String refId) {
        List<SFTPLog> sftpLog = new ArrayList<>();
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId));

            query.with(new Sort(Sort.Direction.DESC, "date"));

            sftpLog = mongoTemplate.find(query, SFTPLog.class);

        } catch (DataAccessException ex) {

            ex.printStackTrace();

            logger.error(" error occurred while getting active reportConfiguration with probable cause [{}] ", ex.getMessage());
        }

        List<SFTPLogOutput> sftpLogOutputs = new ArrayList<>();
        for (SFTPLog obj : sftpLog) {
            SFTPLogOutput sftpLogOutput = new SFTPLogOutput();
            sftpLogOutput.setSftpLog(obj);
            sftpLogOutput.setDateTime(GngDateUtil.toAadharTimeStamp(obj.getDate()));
            logger.debug(sftpLogOutput.getDateTime());
            sftpLogOutputs.add(sftpLogOutput);
        }

        return sftpLogOutputs;
    }

    public List<ESignedLog> getESignBYrefId(String refId) {
        List<ESignedLog> eSignedLogs = new ArrayList<>();
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("refId").is(refId));

            query.with(new Sort(Sort.Direction.DESC, "date"));

            eSignedLogs = mongoTemplate.find(query, ESignedLog.class);

        } catch (DataAccessException ex) {

            ex.printStackTrace();

            logger.error(" error occurred while getting active reportConfiguration with probable cause [{}] ", ex.getMessage());
        }
        return eSignedLogs;
    }
}
