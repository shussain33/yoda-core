package com.softcell.reporting.report;

import com.google.common.base.CharMatcher;
import com.softcell.constants.FieldSeparator;
import com.softcell.dao.mongodb.repository.ReportingRepository;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.reporting.builder.GoNoGoApplicationToJsonParser;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Format;
import com.softcell.reporting.domains.Format.Action;
import com.softcell.reporting.domains.Format.DataType;
import com.softcell.utils.GnGFileUtils;
import com.softcell.utils.GngDateUtil;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author harishkhollam  This class is used to generate credit report of TLV.
 */
@Service
public class TVRReport {

    private static final Logger logger = LoggerFactory.getLogger(TVRReport.class);


    @Autowired
    private ReportingRepository reportingRepository;


    public TVRReport(){
    }

    private static SortedMap<Integer, ColumnConfiguration> defaultConfigMapVersion2 = new TreeMap();

    static {

        int index = 0;

        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Reference Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Submit Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        Format format = new Format();
        format.setAction(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("BRE STATUS");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("STA");
        format.setTo("Straight Through Process");
        format.setAction(Action.replace);
        format.setCapitalize(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Grid ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_gridId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Subject to");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_subjectTo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        defaultConfigMapVersion2.put(index, columnConfiguration);
    }


    /**
     *
     * @param configuration
     * @return
     * @throws IOException
     */
    public byte[] getTVRReport(ReportingModuleConfiguration configuration) throws IOException {

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();

        logger.debug("tvr report is handled by  thread [{}] at [{}]" , Thread.currentThread().getName(), System.currentTimeMillis());

        FlatReportConfiguration flatReportConfiguration = new FlatReportConfiguration();

        flatReportConfiguration.setHeaderMap(defaultConfigMapVersion2);

        flatReportConfiguration.setSeperater(FieldSeparator.COMMA);

        flatReportConfiguration.setReportName("GNGVersion2");

        flatReportConfiguration.setReportType("CSV");

        flatReportConfiguration.setReportFormat("CSV");

        OutputStreamWriter reportOutputStreamWriter = null;

        ByteArrayOutputStream outputStream1 = null;

        try {

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReportConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            OutputStreamWriter finalReportOutputStreamWriter = reportOutputStreamWriter;

            StreamUtils.createStreamFromIterator(reportingRepository.getGoNoGoCustomerApplicationForTVR(configuration))
                    .forEach(goNoGoCustomerApplication ->{

                        GoNoGoApplicationToJsonParser applicationToJsonParser = new GoNoGoApplicationToJsonParser(goNoGoCustomerApplication);



                        StringBuilder row = new StringBuilder();

                        row.append(CharMatcher.anyOf("\r\n\t").removeFrom(applicationToJsonParser.build()
                                    .enrichJson(flatReportConfiguration).toString())).append("\n");


                        try {

                            IOUtils.write(row.toString().getBytes(), finalReportOutputStreamWriter,StandardCharsets.UTF_8);

                        } catch (IOException e) {
                            e.printStackTrace();
                            logger.error(" error occurred while writing report to outputStream with probable cause [{}] ", e.getMessage() );
                        }


                    });


        }catch (Exception e ){
            e.printStackTrace();
            logger.error(" error occurred while creating tvr report with probable cause [{}] ", e.getMessage());
        }

        reportOutputStreamWriter.close();

        stopWatch.stop();

        logger.info(" total time taken to sanitize and transform in required format  of records under TVR report is [{}] sec " , stopWatch.getTotalTimeSeconds());

        return GnGFileUtils.compressBytes(
                "TVR_REPORT_"+ GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                outputStream1.toByteArray());


    }

   /* private void addMetaData(
            GoNoGoCustomerApplication goNoGoCustomerApplication,
            GoNoGoApplicationToJsonParser applicationToJsonParser) {

        Query query = new Query();

        query.addCriteria(Criteria.where("dealerID").is(
                goNoGoCustomerApplication.getApplicationRequest().getHeader()
                        .getDealerId()));

        DealerEmailMaster dealerEmailMaster = null;
        try {

            dealerEmailMaster = mongoTemplate.findOne(query,DealerEmailMaster.class);

        }catch (DataAccessException e){
            e.printStackTrace();
            logger.error(" error occurred while setting metadata in tvr report with probable cause [{}] " , e.getMessage());
        }

        if (null != dealerEmailMaster) {
            applicationToJsonParser.populateMetaData(dealerEmailMaster);
        }
    }*/

    /**
     * @param reportType
     * @param productType
     * @param institutionID
     * @return This method is returns email registered for particular report.
     */
   /* public List<ReportEmailConfiguration> getReportEmailConfiguration(
            String reportType, String productType, String institutionID) {

        List<ReportEmailConfiguration>  reportEmailConfigurations = new ArrayList<>();
        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true)
                    .and("reportType").is(reportType).and("productType")
                    .is(productType).and("institutionID").is(institutionID));

            reportEmailConfigurations = mongoTemplate.find(query, ReportEmailConfiguration.class);


        } catch (DataAccessException ex) {
            ex.printStackTrace();
            logger.error(" error occurred while getting report email ckonfiguration with probable cause [{}] " , ex.getMessage());
        }

        return reportEmailConfigurations;

    }*/

    /**
     * @return This method returns all details which is filter for building
     * query to generate report.
     */
    /*public List<ReportingModuleConfiguration> getReportingModuleConfiguration() {

        List<ReportingModuleConfiguration> reportingModuleConfigurations = new ArrayList<>();

        try {

            Query query = new Query();

            query.addCriteria(Criteria.where("active").is(true));

            reportingModuleConfigurations = mongoTemplate.find(query,ReportingModuleConfiguration.class);

        } catch (DataAccessException ex) {
            ex.printStackTrace();
            logger.error(" error occurred while getting reporting module configuration with probable cause [{}] " , ex.getMessage());
        }

        return reportingModuleConfigurations;
    }*/
}
