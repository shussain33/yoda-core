package com.softcell.reporting.report;

import com.mongodb.BasicDBObject;
import com.softcell.gonogo.cache.Cache;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.reporting.spring.data.CustomProjectionOperation;
import com.softcell.reporting.spring.data.UnwindOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Component
public class GoNoGoReportHelper {

    private static final Logger logger = LoggerFactory.getLogger(GoNoGoReportHelper.class);

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    @Qualifier(value = "mongoSecondaryBean")
    private MongoTemplate mongoSecondaryDBTemplate;

    public static final String GONOGO_REPORT_DROPDOWN_TYPE = "GoNoGo_Reports";

    public static final String INSURANCE_REPORT = "INSURANCE_REPORT";
    public static final String CHEQUE_FAVOURING_REPORT = "CHEQUE_FAVOURING_REPORT";
    public static final String PDD_MIS_REPORT = "PDD_MIS_REPORT";

    public List<List<BasicDBObject>> findRecordsBetweenDates(Date startDate, Date endDate, Header header, String fileName) {
        List<List<BasicDBObject>> record = new ArrayList<>();

        if(fileName.contains(INSURANCE_REPORT)){
            record = insuranceReport(startDate, endDate, header, fileName);
        }else if(fileName.contains(CHEQUE_FAVOURING_REPORT)){
            record = chequePrintingReport(startDate, endDate, header);
        }else if(fileName.contains(PDD_MIS_REPORT)){
            record = pddMisReportHelper(startDate,endDate,header);
        }
        return record;
    }

    private List<List<BasicDBObject>> insuranceReport(Date startDate, Date endDate, Header header, String fileName){
        logger.debug("Insurance Report Aggregation Started");

        List<String> insuranceMatchQuery = new ArrayList<>();
        if(fileName.contains("CARE")){
            insuranceMatchQuery.add("RELIGARE HEALTH INSURANCE");
            insuranceMatchQuery.add("RELIGARE LOAN PROTECTION");
            insuranceMatchQuery.add("CARE HEALTH INSURANCE");
            insuranceMatchQuery.add("CARE LOAN PROTECTION");
        }else if(fileName.contains("ICICI")){
            insuranceMatchQuery.add("ICICI PRUDENTIAL LIFE INSURANCE");
        }
        logger.debug("Insurance Selected: {}",insuranceMatchQuery);

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("applicationRequest.header.product").is(header.getProduct().name()),
                Criteria.where("applicationRequest.currentStageId").is("DISB"),
                Criteria.where("applicationRequest.header.institutionId").is(header.getInstitutionId()),
                Criteria.where("dateTime").gte(startDate).lte(endDate)
        )));

        pipelineOperation.add(lookup("loanCharges", "_id", "_id", "loanCharges"));
        pipelineOperation.add(lookup("insuranceData", "_id", "_id", "insuranceData"));

        UnwindOperation unwindOperation1 = new UnwindOperation("insuranceData", false);
        UnwindOperation unwindOperation2 = new UnwindOperation("insuranceData.applicantList", false);
        UnwindOperation unwindOperation3 = new UnwindOperation("insuranceData.applicantList.insurancePolicyList", false);
        UnwindOperation unwindOperation4 = new UnwindOperation("loanCharges", false);

        pipelineOperation.add(unwindOperation1);
        pipelineOperation.add(unwindOperation2);
        pipelineOperation.add(unwindOperation3);
        pipelineOperation.add(unwindOperation4);

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("insuranceData.applicantList.insurancePolicyList.company").in(insuranceMatchQuery)
        )));

        BasicDBObject projection = new BasicDBObject()
                .append("$project", new BasicDBObject()
                        .append("LevioSa ID", new BasicDBObject()
                                .append("$concat", Arrays.asList(
                                        "SBFC",
                                        "",
                                        "$_id"
                                        )
                                )
                        )
                        .append("Company", "$insuranceData.applicantList.insurancePolicyList.company")
                        .append("Policy Reference Number", new BasicDBObject()
                                .append("$concat", Arrays.asList(
                                        "SBFC",
                                        "",
                                        "$insuranceData.applicantList.insurancePolicyList.policyNumber"
                                        )
                                )
                        )
                        .append("Loan Account Number", "$applicationRequest.miFinProspectCode")
                        .append("Salutation", "$insuranceData.applicantList.applicantName.prefix")
                        .append("First Name", "$insuranceData.applicantList.applicantName.firstName")
                        .append("Last Name", "$insuranceData.applicantList.applicantName.lastName")
                        .append("Residence Address", new BasicDBObject().
                                append("$reduce",new BasicDBObject().
                                        append("input",new BasicDBObject().
                                                append("$split", Arrays.asList(
                                                        new BasicDBObject()
                                                                .append("$arrayElemAt",Arrays.asList(
                                                                        new BasicDBObject()
                                                                                .append("$map", new BasicDBObject()
                                                                                        .append("input", new BasicDBObject()
                                                                                                .append("$filter", new BasicDBObject()
                                                                                                        .append("input", "$insuranceData.applicantList.address")
                                                                                                        .append("as", "item")
                                                                                                        .append("cond", new BasicDBObject()
                                                                                                                .append("$eq", Arrays.asList(
                                                                                                                        "$$item.addressType",
                                                                                                                        "RESIDENCE"
                                                                                                                        )
                                                                                                                )
                                                                                                        )
                                                                                                )
                                                                                        )
                                                                                        .append("as", "residence")
                                                                                        .append("in", new BasicDBObject()
                                                                                                .append("$concat", Arrays.asList(
                                                                                                        "$$residence.addressLine1",
                                                                                                        " ",
                                                                                                        "$$residence.addressLine2"
                                                                                                        )
                                                                                                )
                                                                                        )
                                                                                ),
                                                                        0.0
                                                                )),
                                                        ","
                                                        )
                                                )
                                        )
                                        .append("initialValue","")
                                        .append("in",new BasicDBObject()
                                                .append("$cond",new BasicDBObject()
                                                        .append("if",new BasicDBObject()
                                                                .append("$eq",Arrays.asList(
                                                                        "$$value",
                                                                        ""
                                                                        )
                                                                )
                                                        )
                                                        .append("then", "$$this")
                                                        .append("else",new BasicDBObject().
                                                                append("$concat", Arrays.asList(
                                                                        "$$value",
                                                                        ";",
                                                                        "$$this"
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                        .append("Residence Address Pin", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$map", new BasicDBObject()
                                                        .append("input", new BasicDBObject()
                                                                .append("$filter", new BasicDBObject()
                                                                        .append("input", "$insuranceData.applicantList.address")
                                                                        .append("as", "item")
                                                                        .append("cond", new BasicDBObject()
                                                                                .append("$eq", Arrays.asList(
                                                                                        "$$item.addressType",
                                                                                        "RESIDENCE"
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                        .append("as", "residence")
                                                        .append("in", new BasicDBObject()
                                                                .append("$toString", "$$residence.pin")
                                                        )
                                                ),
                                        0.0
                                        )
                                )
                        )
                        .append("Residence Address City", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$map", new BasicDBObject()
                                                        .append("input", new BasicDBObject()
                                                                .append("$filter", new BasicDBObject()
                                                                        .append("input", "$insuranceData.applicantList.address")
                                                                        .append("as", "item")
                                                                        .append("cond", new BasicDBObject()
                                                                                .append("$eq", Arrays.asList(
                                                                                        "$$item.addressType",
                                                                                        "RESIDENCE"
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                        .append("as", "residence")
                                                        .append("in", "$$residence.city")
                                                ),
                                        0.0
                                        )
                                )
                        )
                        .append("Residence Address State", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$map", new BasicDBObject()
                                                        .append("input", new BasicDBObject()
                                                                .append("$filter", new BasicDBObject()
                                                                        .append("input", "$insuranceData.applicantList.address")
                                                                        .append("as", "item")
                                                                        .append("cond", new BasicDBObject()
                                                                                .append("$eq", Arrays.asList(
                                                                                        "$$item.addressType",
                                                                                        "RESIDENCE"
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                        .append("as", "residence")
                                                        .append("in", "$$residence.state")
                                                ),
                                        0.0
                                        )
                                )
                        )
                        .append("Residence Address Country", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$map", new BasicDBObject()
                                                        .append("input", new BasicDBObject()
                                                                .append("$filter", new BasicDBObject()
                                                                        .append("input", "$insuranceData.applicantList.address")
                                                                        .append("as", "item")
                                                                        .append("cond", new BasicDBObject()
                                                                                .append("$eq", Arrays.asList(
                                                                                        "$$item.addressType",
                                                                                        "RESIDENCE"
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                        .append("as", "residence")
                                                        .append("in", "$$residence.country")
                                                ),
                                        0.0
                                        )
                                )
                        )
                        .append("Mobile No", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        "$insuranceData.applicantList.phone.phoneNumber",
                                        0.0
                                        )
                                )
                        )
                        .append("STD Code", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        "$insuranceData.applicantList.phone.countryCode",
                                        0.0
                                        )
                                )
                        )
                        .append("Email", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        "$insuranceData.applicantList.email.emailAddress",
                                        0.0
                                        )
                                )
                        )
                        .append("DOB", new BasicDBObject()
                                .append("$concat", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$substr", Arrays.asList(
                                                        "$insuranceData.applicantList.dateOfBirth",
                                                        0.0,
                                                        2.0
                                                        )
                                                ),
                                        "-",
                                        new BasicDBObject()
                                                .append("$substr", Arrays.asList(
                                                        "$insuranceData.applicantList.dateOfBirth",
                                                        2.0,
                                                        2.0
                                                        )
                                                ),
                                        "-",
                                        new BasicDBObject()
                                                .append("$substr", Arrays.asList(
                                                        "$insuranceData.applicantList.dateOfBirth",
                                                        4.0,
                                                        4.0
                                                        )
                                                )
                                        )
                                )
                        )
                        .append("Gender", "$insuranceData.applicantList.gender")
                        .append("Marital Status", "$insuranceData.applicantList.maritalStatus")
                        .append("Occupation", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        "$insuranceData.applicantList.employment.employmentType",
                                        0.0
                                        )
                                )
                        )
                        .append("Income", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        "$insuranceData.applicantList.employment.itrAmount",
                                        0.0
                                        )
                                )
                        )
                        .append("Age Proof", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$map", new BasicDBObject()
                                                        .append("input", new BasicDBObject()
                                                                .append("$filter", new BasicDBObject()
                                                                        .append("input", "$insuranceData.applicantList.kyc")
                                                                        .append("as", "item")
                                                                        .append("cond", new BasicDBObject()
                                                                                .append("$ne", Arrays.asList(
                                                                                        "$$item.kycName",
                                                                                        "PAN"
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                        .append("as", "otherThanPan")
                                                        .append("in", "$$otherThanPan.kycNumber")
                                                ),
                                        0.0
                                        )
                                )
                        )
                        .append("Loan Amount", "$loanCharges.loanDetails.finalLoanApprovedAmt")
                        .append("Loan Tenure", "$loanCharges.tenureDetails.tenureMonths")
                        .append("Sum Assured", "$insuranceData.applicantList.insurancePolicyList.proposedAmount")
                        .append("Policy Term", "$insuranceData.applicantList.insurancePolicyList.tenorRequested")
                        .append("Premium Amount", "$insuranceData.applicantList.insurancePolicyList.premium")
                        .append("Nominee first Name", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.name.firstName")
                        .append("Nominee SurName", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.name.lastName")
                        .append("Nominee Gender", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.gender")
                        .append("Nominee DOB", new BasicDBObject()
                                .append("$dateToString", new BasicDBObject()
                                        .append("format", "%d-%m-%Y")
                                        .append("date", new BasicDBObject()
                                                .append("$dateFromString", new BasicDBObject()
                                                        .append("dateString", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.dateOfBirth")
                                                )
                                        )
                                        .append("timezone", "+05:30")
                                )
                        )
                        .append("Nominee Relationship with LA", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.relationship")
                        .append("Appointee First Name", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.appointee.name.firstName")
                        .append("Appointee SurName", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.appointee.name.lastName")
                        .append("Appointee Gender", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.appointee.gender")
                        .append("Appointee DOB", new BasicDBObject()
                                .append("$concat", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$substr", Arrays.asList(
                                                        "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.appointee.tempDob",
                                                        0.0,
                                                        2.0
                                                        )
                                                ),
                                        "-",
                                        new BasicDBObject()
                                                .append("$substr", Arrays.asList(
                                                        "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.appointee.tempDob",
                                                        2.0,
                                                        2.0
                                                        )
                                                ),
                                        "-",
                                        new BasicDBObject()
                                                .append("$substr", Arrays.asList(
                                                        "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.appointee.tempDob",
                                                        4.0,
                                                        4.0
                                                        )
                                                )
                                        )
                                )
                        )
                        .append("Appointee Relationship with Nominee", "$insuranceData.applicantList.insurancePolicyList.nomineeInfo.appointee.relationship")
                        .append("Loan Disbursal Date", new BasicDBObject()
                                .append("$dateToString", new BasicDBObject()
                                        .append("format", "%d-%m-%Y")
                                        .append("date", "$loanCharges.disbursedDate")
                                        .append("timezone", "Asia/Kolkata")
                                )
                        )
                        .append("Customer PAN No", new BasicDBObject()
                                .append("$arrayElemAt", Arrays.asList(
                                        new BasicDBObject()
                                                .append("$map", new BasicDBObject()
                                                        .append("input", new BasicDBObject()
                                                                .append("$filter", new BasicDBObject()
                                                                        .append("input", "$insuranceData.applicantList.kyc")
                                                                        .append("as", "item")
                                                                        .append("cond", new BasicDBObject()
                                                                                .append("$eq", Arrays.asList(
                                                                                        "$$item.kycName",
                                                                                        "PAN"
                                                                                        )
                                                                                )
                                                                        )
                                                                )
                                                        )
                                                        .append("as", "pan")
                                                        .append("in", "$$pan.kycNumber")
                                                ),
                                        0.0
                                        )
                                )
                        )
                        .append("Loan Type", "$applicationRequest.header.product")
                        .append("Category", "$insuranceData.applicantList.insurancePolicyList.scheme")
                );

        pipelineOperation.add(new CustomProjectionOperation(projection));

        Aggregation aggregation = newAggregation(pipelineOperation).withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());

        AggregationResults<BasicDBObject> aggregationResults;
        if (!Cache.isSecondaryDbEnable(header.getInstitutionId())) {
            aggregationResults = mongoTemplate.aggregate(aggregation,"goNoGoCustomerApplication",BasicDBObject.class);
        } else {
            aggregationResults = mongoSecondaryDBTemplate.aggregate(aggregation,"goNoGoCustomerApplication",BasicDBObject.class);
            logger.debug("INSURANCE_REPORT query hit from secondary db");
        }

        List<BasicDBObject> output = aggregationResults.getMappedResults();

        List<List<BasicDBObject>> records = new ArrayList<>();
        records.add(output);

        logger.debug("Insurance Report Aggregation Ended");
        return records;
    }

    private List<List<BasicDBObject>> chequePrintingReport(Date startDate, Date endDate, Header header){
        logger.debug("Cheque Printing Report Aggregation Started");

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("applicationRequest.header.product").is(header.getProduct().name()),
                Criteria.where("applicationRequest.currentStageId").is("HOPS"),
                Criteria.where("applicationRequest.header.institutionId").is(header.getInstitutionId()),
                Criteria.where("completed.submit-by-BOPS.updatedOn").gte(startDate).lte(endDate))));

        pipelineOperation.add(lookup("loanCharges", "_id", "_id", "loanCharges"));
        pipelineOperation.add(lookup("dmDetails", "_id", "_id", "dmDetails"));

        UnwindOperation unwindOperation1 = new UnwindOperation("dmDetails", false);
        UnwindOperation unwindOperation2 = new UnwindOperation("dmDetails.dmInputList", false);
        UnwindOperation unwindOperation3 = new UnwindOperation("loanCharges", false);

        pipelineOperation.add(unwindOperation1);
        pipelineOperation.add(unwindOperation2);
        pipelineOperation.add(unwindOperation3);

        BasicDBObject projection1 = new BasicDBObject()
                .append("$project", new BasicDBObject()
                        .append("LEVIOSA REF ID", new BasicDBObject()
                                .append("$concat", Arrays.asList(
                                        "SBFC",
                                        "-",
                                        "$dmDetails._id"
                                        )
                                )
                        )
                        .append("ZONE","$applicationRequest.appMetaData.branchV2.zone")
                        .append("BRANCH", "$applicationRequest.appMetaData.branchV2.branchName")
                        .append("LOCATION", "$dmDetails.branchName")
                        .append("DISBURSAL FAVOURING", "$dmDetails.dmInputList.dmFavourting")
                        .append("MODE OF PAYMENT", "$dmDetails.dmInputList.dmPaymentMode")
                        .append("DISBURSAL AMOUNT", "$dmDetails.loanCharges.netDisbAmt")
                        .append("DISBURSAL_DATE", "$loanCharges.disbursedDate")
                        .append("PR NO","$applicationRequest.miFinProspectCode")
                        .append("STAGE", "$applicationRequest.currentStageId")
                        .append("STATUS", "$applicationStatus")
                        .append("PRODUCT", "$applicationRequest.header.product"));

        pipelineOperation.add(new CustomProjectionOperation(projection1));

        pipelineOperation.add(sort(Sort.Direction.DESC, "DISBURSAL_DATE"));

        BasicDBObject projection2 = new BasicDBObject()
                .append("$project", new BasicDBObject()
                        .append("LEVIOSA REF ID", 1.0)
                        .append("ZONE",1.0)
                        .append("BRANCH", 1.0)
                        .append("LOCATION", 1.0)
                        .append("DISBURSAL FAVOURING", 1.0)
                        .append("MODE OF PAYMENT", 1.0)
                        .append("DISBURSAL AMOUNT", 1.0)
                        .append("DISBURSAL DATE", new BasicDBObject()
                                .append("$dateToString", new BasicDBObject()
                                        .append("format", "%d-%m-%Y")
                                        .append("date", "$DISBURSAL_DATE")
                                        .append("timezone", "Asia/Kolkata")
                                )
                        )
                        .append("PR NO",1.0)
                        .append("STAGE", 1.0)
                        .append("STATUS", 1.0)
                        .append("PRODUCT", 1.0)
                );

        pipelineOperation.add(new CustomProjectionOperation(projection2));

        Aggregation aggregation = newAggregation(pipelineOperation).withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());

        AggregationResults<BasicDBObject> aggregationResults;
        if (!Cache.isSecondaryDbEnable(header.getInstitutionId())) {
            aggregationResults = mongoTemplate.aggregate(aggregation,"goNoGoCustomerApplication",BasicDBObject.class);
        } else {
            aggregationResults = mongoSecondaryDBTemplate.aggregate(aggregation,"goNoGoCustomerApplication",BasicDBObject.class);
            logger.debug("CHEQUE_FAVOURING_REPORT query hit from secondary db");
        }
        List<BasicDBObject> output = aggregationResults.getMappedResults();

        List<List<BasicDBObject>> records = new ArrayList<>();
        records.add(output);

        logger.debug("Cheque Printing Report Aggregation Ended");
        return records;
    }

    private List<List<BasicDBObject>> pddMisReportHelper(Date startDate, Date endDate, Header header){
        List<List<BasicDBObject>> records = new ArrayList<>();

        List<BasicDBObject> pending = pddMisReport(startDate, endDate, header, "Pending");
        List<BasicDBObject> received = pddMisReport(startDate, endDate, header, "Received");
        List<BasicDBObject> waived = pddMisReport(startDate, endDate, header, "Waived");

        records.add(pending);
        records.add(received);
        records.add(waived);

        return records;
    }

    private List<BasicDBObject> pddMisReport(Date startDate, Date endDate, Header header, String reportName){
        logger.debug("pddMisReport Aggregation Started");

        List<BasicDBObject> query_1 = pddMisQueryHelper(header,startDate,endDate,reportName,"listOfDocs", "docDetails",
                "opsDocStatus","desc","opsReceiptDt","remarks.comment","Property");

        List<BasicDBObject> query_2 = pddMisQueryHelper(header,startDate,endDate,reportName,"customerCreditDocs", "customerCreditDetails",
                "opsStatus","desc","opsReceiptDt","opsRemarks.comment","Customer Document");

        List<BasicDBObject> query_3 = pddMisQueryHelper(header,startDate,endDate,reportName,"sanctionConditions", "scDetailsList",
                "status","condition","resolvedDate","opsRemark","Credit Document");

        Map<String,List<BasicDBObject>> map = new HashMap<>();
        hashMapHelper(map,query_1);
        hashMapHelper(map,query_2);
        hashMapHelper(map,query_3);

        uniqueNumberHelper(map);

        List<BasicDBObject> output = new ArrayList<>();
        for(String id:map.keySet()){
            output.addAll(map.get(id));
        }

        for(BasicDBObject basicDBObject:output){
            String loanScheme = basicDBObject.getString("SUB_PRODUCT");
            String disbursalDate = basicDBObject.getString("DISBURSAL_DATE");
            if(StringUtils.isNotEmpty(loanScheme) && StringUtils.isNotEmpty(disbursalDate)){
                String resolutionDate = resolutionDateHelper(loanScheme,disbursalDate);
                basicDBObject.append("RESOLUTION_DATE",resolutionDate);
            }
        }

        for(BasicDBObject basicDBObject:output){
            String resolutionDate = basicDBObject.getString("RESOLUTION_DATE");
            if(StringUtils.isNotEmpty(resolutionDate)){
                long daysAging = daysAgeingHelper(resolutionDate);
                basicDBObject.append("DAYS_AGEING",Long.toString(daysAging));
            }
        }

        for(BasicDBObject basicDBObject:output){
            String daysAgeing = basicDBObject.getString("DAYS_AGEING");
            if(StringUtils.isNotEmpty(daysAgeing)){
                String ageing = ageingHelper(Long.parseLong(daysAgeing));
                basicDBObject.append("AGEING",ageing);
            }
        }

        logger.debug("pddMisReport Aggregation Ended");
        return output;
    }

    private List<BasicDBObject> pddMisQueryHelper(Header header,Date startDate,Date endDate,String reportName,String collection,String collectionList,
                                                  String status,String description,String statusDate,String opsRemark,String docCategory){

        List<AggregationOperation> pipelineOperation = new ArrayList<AggregationOperation>();

        List<String> matchQuery = new ArrayList<>();
        if(reportName.contains("Pending")){
            matchQuery.add("OTC");
            matchQuery.add("PDD");
        }else if(reportName.contains("Received")){
            matchQuery.add("Received");
        }else if(reportName.contains("Waived")){
            matchQuery.add("Waived");
        }

        BasicDBObject hoRemarksConverted = new BasicDBObject();

        if(StringUtils.equalsIgnoreCase(collection,"listOfDocs")){
            hoRemarksConverted = new BasicDBObject().
                    append("$reduce",new BasicDBObject().
                            append("input",new BasicDBObject().
                                    append("$split", Arrays.asList(
                                            new BasicDBObject()
                                                    .append("$arrayElemAt", Arrays.asList(
                                                            "$"+collection+"."+collectionList+"."+opsRemark,
                                                            0.0
                                                            )
                                                    ),
                                            ","
                                            )
                                    )
                            )
                            .append("initialValue","")
                            .append("in",new BasicDBObject()
                                    .append("$cond",new BasicDBObject()
                                            .append("if",new BasicDBObject()
                                                    .append("$eq",Arrays.asList(
                                                            "$$value",
                                                            ""
                                                            )
                                                    )
                                            )
                                            .append("then", "$$this")
                                            .append("else",new BasicDBObject().
                                                    append("$concat", Arrays.asList(
                                                            "$$value",
                                                            ";",
                                                            "$$this"
                                                            )
                                                    )
                                            )
                                    )
                            )
                    );
        }

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("disbursedDate").gte(startDate).lte(endDate),
                Criteria.where("institutionId").is(header.getInstitutionId())
        )));

        pipelineOperation.add(lookup(collection, "_id", "_id", collection));
        pipelineOperation.add(lookup("goNoGoCustomerApplication", "_id", "_id", "goNoGoCustomerApplication"));

        UnwindOperation unwindOperation1 = new UnwindOperation(collection, false);
        UnwindOperation unwindOperation2 = new UnwindOperation(collection+"."+collectionList, false);
        UnwindOperation unwindOperation3 = new UnwindOperation("goNoGoCustomerApplication", false);

        pipelineOperation.add(unwindOperation1);
        pipelineOperation.add(unwindOperation2);
        pipelineOperation.add(unwindOperation3);

        pipelineOperation.add(match(new Criteria().andOperator(
                Criteria.where("goNoGoCustomerApplication.applicationRequest.header.product").is(header.getProduct().name()),
                Criteria.where("goNoGoCustomerApplication.applicationRequest.currentStageId").is("DISB"),
                Criteria.where(collection+"."+collectionList+"."+status).in(matchQuery)
        )));

        BasicDBObject projection1 = new BasicDBObject()
                .append("$project", new BasicDBObject()
                        .append("DIVISION","$goNoGoCustomerApplication.applicationRequest.appMetaData.branchV2.zone")
                        .append("LOCATION","$goNoGoCustomerApplication.applicationRequest.appMetaData.branchV2.location")
                        .append("LEVIOSA_REF_ID", new BasicDBObject()
                                .append("$concat", Arrays.asList(
                                        "SBFC",
                                        "-",
                                        "$_id"
                                        )
                                )
                        )
                        .append("PR_NO","$goNoGoCustomerApplication.applicationRequest.miFinProspectCode")
                        .append("BRANCH_NAME","$goNoGoCustomerApplication.applicationRequest.appMetaData.branchV2.branchName")
                        .append("SUB_PRODUCT","$goNoGoCustomerApplication.applicationRequest.request.applicant.professionIncomeDetails.loanScheme")
                        .append("PRODUCT","$goNoGoCustomerApplication.applicationRequest.header.product")
                        .append("CUSTOMER_NAME",new BasicDBObject()
                                .append("$concat",Arrays.asList(
                                        "$goNoGoCustomerApplication.applicationRequest.request.applicant.applicantName.firstName",
                                        " ",
                                        "$goNoGoCustomerApplication.applicationRequest.request.applicant.applicantName.lastName"
                                        )
                                )
                        )
                        .append("DISB_DATE", "$disbursedDate")
                        .append("PDD_DOCUMENTS", new BasicDBObject().
                                append("$reduce",new BasicDBObject().
                                        append("input",new BasicDBObject().
                                                append("$split", Arrays.asList(
                                                        "$"+collection+"."+collectionList+"."+description,
                                                        ","
                                                        )
                                                )
                                        )
                                        .append("initialValue","")
                                        .append("in",new BasicDBObject()
                                                .append("$cond",new BasicDBObject()
                                                        .append("if",new BasicDBObject()
                                                                .append("$eq",Arrays.asList(
                                                                        "$$value",
                                                                        ""
                                                                        )
                                                                )
                                                        )
                                                        .append("then", "$$this")
                                                        .append("else",new BasicDBObject().
                                                                append("$concat", Arrays.asList(
                                                                        "$$value",
                                                                        ";",
                                                                        "$$this"
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                        .append("DOCS_CATEGORY",docCategory)
                        .append("PDD_CATEGORY","$"+collection+"."+collectionList+"."+status)
                        .append("RECEIVED_ON", new BasicDBObject()
                                .append("$dateToString", new BasicDBObject()
                                        .append("format", "%d-%m-%Y")
                                        .append("date", "$"+collection+"."+collectionList+"."+statusDate)
                                        .append("timezone", "Asia/Kolkata")
                                )
                        )
                        .append("REMARKS",StringUtils.equalsIgnoreCase(collection,"listOfDocs") ? hoRemarksConverted : new BasicDBObject().
                                append("$reduce",new BasicDBObject().
                                        append("input",new BasicDBObject().
                                                append("$split", Arrays.asList(
                                                        "$"+collection+"."+collectionList+"."+opsRemark,
                                                        ","
                                                        )
                                                )
                                        )
                                        .append("initialValue","")
                                        .append("in",new BasicDBObject()
                                                .append("$cond",new BasicDBObject()
                                                        .append("if",new BasicDBObject()
                                                                .append("$eq",Arrays.asList(
                                                                        "$$value",
                                                                        ""
                                                                        )
                                                                )
                                                        )
                                                        .append("then", "$$this")
                                                        .append("else",new BasicDBObject().
                                                                append("$concat", Arrays.asList(
                                                                        "$$value",
                                                                        ";",
                                                                        "$$this"
                                                                        )
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                );

        pipelineOperation.add(new CustomProjectionOperation(projection1));

        pipelineOperation.add(sort(Sort.Direction.DESC, "DISB_DATE"));

        BasicDBObject projection2 = new BasicDBObject()
                .append("$project", new BasicDBObject()
                        .append("DIVISION", 1.0)
                        .append("LOCATION",1.0)
                        .append("LEVIOSA_REF_ID", 1.0)
                        .append("PR_NO", 1.0)
                        .append("BRANCH_NAME", 1.0)
                        .append("SUB_PRODUCT", 1.0)
                        .append("PRODUCT", 1.0)
                        .append("CUSTOMER_NAME",1.0)
                        .append("DISBURSAL_DATE", new BasicDBObject()
                                .append("$dateToString", new BasicDBObject()
                                        .append("format", "%d-%m-%Y")
                                        .append("date", "$DISB_DATE")
                                        .append("timezone", "Asia/Kolkata")
                                )
                        )
                        .append("PDD_DOCUMENTS",1.0)
                        .append("DOCS_CATEGORY", 1.0)
                        .append("PDD_CATEGORY", 1.0)
                        .append("RECEIVED_ON", 1.0)
                        .append("REMARKS",1.0)
                );

        pipelineOperation.add(new CustomProjectionOperation(projection2));

        Aggregation aggregation = newAggregation(pipelineOperation).withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());

        AggregationResults<BasicDBObject> aggregationResults;
        if (!Cache.isSecondaryDbEnable(header.getInstitutionId())) {
            aggregationResults = mongoTemplate.aggregate(aggregation,"loanCharges",BasicDBObject.class);
        } else {
            aggregationResults = mongoSecondaryDBTemplate.aggregate(aggregation,"loanCharges",BasicDBObject.class);
            logger.debug("PDD_MIS_REPORT query hit from secondary db");
        }

        List<BasicDBObject> output = aggregationResults.getMappedResults();

        return output;
    }

    private void hashMapHelper(Map<String,List<BasicDBObject>> map,List<BasicDBObject> list){
        for(BasicDBObject basicDBObject:list){
            String id = basicDBObject.getString("_id");
            if(!map.containsKey(id)){
                map.put(id,new ArrayList<>());
            }
            map.get(id).add(basicDBObject);
        }
    }

    private void uniqueNumberHelper(Map<String,List<BasicDBObject>> map){
        int srNo = 1;
        for(String s:map.keySet()){
            int uniqueNoCount = 1;
            for(BasicDBObject basicDBObject:map.get(s)){
                basicDBObject.append("SR_NO",Integer.toString(srNo));
                srNo++;
                String prNo = basicDBObject.getString("PR_NO");
                StringBuilder builder = new StringBuilder();
                if(StringUtils.isNotEmpty(prNo)){
                    builder.append(prNo).append("-").append(uniqueNoCount);
                    uniqueNoCount++;
                    basicDBObject.append("DOC_UNIQUE_NO",builder.toString());
                }
            }
        }
    }

    private String resolutionDateHelper(String loanScheme,String disbursalDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date answer = new Date();
        try {
            Date date = simpleDateFormat.parse(disbursalDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            boolean checkIfCaseIsBT = StringUtils.containsIgnoreCase(loanScheme,"BT") ||
                    StringUtils.equalsIgnoreCase(loanScheme,"Top Up Parallel") || StringUtils.equalsIgnoreCase(loanScheme,"Top Up Gross");

            if(checkIfCaseIsBT){
                calendar.add(Calendar.DAY_OF_MONTH,21);
                answer = calendar.getTime();
            }else{
                calendar.add(Calendar.DAY_OF_MONTH,7);
                answer = calendar.getTime();
            }
        }catch (Exception e){
            logger.debug("getting exception while converting into date {}",e.getMessage());
            e.printStackTrace();
        }
        return simpleDateFormat.format(answer);
    }

    private long daysAgeingHelper(String resolutionDate){
        long daysAging = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date targetDateForResolution = sdf.parse(resolutionDate);
            Date currDate = new Date();
            long diffInMillies = Math.abs(currDate.getTime() - targetDateForResolution.getTime());
            daysAging = TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS);
        }catch (Exception e){
            logger.debug("getting exception while converting into date {}",e.getMessage());
            e.printStackTrace();
        }
        return daysAging;
    }

    private String ageingHelper(long days){
        if(days<1){
            return "0 Days";
        }else{
            if(days<8){
                return "1 to 7 Days";
            }else{
                if(days<16){
                    return "8 to 15 Days";
                }else{
                    if(days<31){
                        return "16 to 30 Days";
                    }else{
                        if(days<61){
                            return "31 to 60 Days";
                        }else{
                            if(days<91){
                                return "61 to 90 Days";
                            }else{
                                if(days<121){
                                    return "91 to 120 Days";
                                }else{
                                    return "More than 120 Days";
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public List<List<LinkedHashMap<String, String>>> createReportByName(List<List<BasicDBObject>> records,String fileName) {
        List<List<LinkedHashMap<String, String>>> report = new ArrayList<>();

        if(fileName.contains(INSURANCE_REPORT)){
            report = createInsuranceReport(records);
        }else if(fileName.contains(CHEQUE_FAVOURING_REPORT)){
            report = createChequePrintingReport(records);
        }else if (fileName.contains(PDD_MIS_REPORT)){
            report = createPddMisReportHelper(records);
        }

        return report;
    }

    private List<List<LinkedHashMap<String, String>>> createInsuranceReport(List<List<BasicDBObject>> multipleReport){
        logger.debug("createInsuranceReport started");

        List<List<LinkedHashMap<String, String>>> output = new ArrayList<>();

        for(List<BasicDBObject> report:multipleReport) {
            List<LinkedHashMap<String, String>> temp = new ArrayList<>();

            for(BasicDBObject basicDBObject : report){
                try{
                    LinkedHashMap<String, String> map = new LinkedHashMap<>();
                    map.put("RefId",basicDBObject.get("_id") != null ? basicDBObject.get("_id").toString() : "");
                    map.put("LevioSa ID",basicDBObject.get("LevioSa ID") != null ? basicDBObject.get("LevioSa ID").toString() : "");
                    map.put("Company",basicDBObject.get("Company") != null ? basicDBObject.get("Company").toString() : "");
                    map.put("Policy Reference Number",basicDBObject.get("Policy Reference Number") != null ? basicDBObject.get("Policy Reference Number").toString() : "");
                    map.put("Loan Account Number",basicDBObject.get("Loan Account Number") != null ? basicDBObject.get("Loan Account Number").toString() : "");
                    map.put("Salutation",basicDBObject.get("Salutation") != null ? basicDBObject.get("Salutation").toString() : "");
                    map.put("First Name",basicDBObject.get("First Name") != null ? basicDBObject.get("First Name").toString() : "");
                    map.put("Last Name",basicDBObject.get("Last Name") != null ? basicDBObject.get("Last Name").toString() : "");
                    map.put("Residence Address",basicDBObject.get("Residence Address") != null ? basicDBObject.get("Residence Address").toString() : "");
                    map.put("Residence Address Pin",basicDBObject.get("Residence Address Pin") != null ? basicDBObject.get("Residence Address Pin").toString() : "");
                    map.put("Residence Address City",basicDBObject.get("Residence Address City") != null ? basicDBObject.get("Residence Address City").toString() : "");
                    map.put("Residence Address State",basicDBObject.get("Residence Address State") != null ? basicDBObject.get("Residence Address State").toString() : "");
                    map.put("Residence Address Country",basicDBObject.get("Residence Address Country") != null ? basicDBObject.get("Residence Address Country").toString() : "");
                    map.put("Mobile No",basicDBObject.get("Mobile No") != null ? basicDBObject.get("Mobile No").toString() : "");
                    map.put("STD Code",basicDBObject.get("STD Code") != null ? basicDBObject.get("STD Code").toString() : "");
                    map.put("Email",basicDBObject.get("Email") != null ? basicDBObject.get("Email").toString() : "");
                    map.put("DOB",basicDBObject.get("DOB") != null ? basicDBObject.get("DOB").toString() : "");
                    map.put("Gender",basicDBObject.get("Gender") != null ? basicDBObject.get("Gender").toString() : "");
                    map.put("Marital Status",basicDBObject.get("Marital Status") != null ? basicDBObject.get("Marital Status").toString() : "");
                    map.put("Occupation",basicDBObject.get("Occupation") != null ? basicDBObject.get("Occupation").toString() : "");
                    map.put("Income",basicDBObject.get("Income") != null ? basicDBObject.get("Income").toString() : "");
                    map.put("Age Proof",basicDBObject.get("Age Proof") != null ? basicDBObject.get("Age Proof").toString() : "");
                    map.put("Loan Amount",basicDBObject.get("Loan Amount") != null ? basicDBObject.get("Loan Amount").toString() : "");
                    map.put("Loan Tenure",basicDBObject.get("Loan Tenure") != null ? basicDBObject.get("Loan Tenure").toString() : "");
                    map.put("Sum Assured",basicDBObject.get("Sum Assured") != null ? basicDBObject.get("Sum Assured").toString() : "");
                    map.put("Policy Term",basicDBObject.get("Policy Term") != null ? basicDBObject.get("Policy Term").toString() : "");
                    map.put("Premium Amount",basicDBObject.get("Premium Amount") != null ? basicDBObject.get("Premium Amount").toString() : "");
                    map.put("Nominee first Name",basicDBObject.get("Nominee first Name") != null ? basicDBObject.get("Nominee first Name").toString() : "");
                    map.put("Nominee SurName",basicDBObject.get("Nominee SurName") != null ? basicDBObject.get("Nominee SurName").toString() : "");
                    map.put("Nominee Gender",basicDBObject.get("Nominee Gender") != null ? basicDBObject.get("Nominee Gender").toString() : "");
                    map.put("Nominee DOB",basicDBObject.get("Nominee DOB") != null ? basicDBObject.get("Nominee DOB").toString() : "");
                    map.put("Nominee Relationship with LA",basicDBObject.get("Nominee Relationship with LA") != null ? basicDBObject.get("Nominee Relationship with LA").toString() : "");
                    map.put("Appointee First Name",basicDBObject.get("Appointee First Name") != null ? basicDBObject.get("Appointee First Name").toString() : "");
                    map.put("Appointee SurName",basicDBObject.get("Appointee SurName") != null ? basicDBObject.get("Appointee SurName").toString() : "");
                    map.put("Appointee Gender",basicDBObject.get("Appointee Gender") != null ? basicDBObject.get("Appointee Gender").toString() : "");
                    map.put("Appointee DOB",basicDBObject.get("Appointee DOB") != null ? basicDBObject.get("Appointee DOB").toString() : "");
                    map.put("Appointee Relationship with Nominee",basicDBObject.get("Appointee Relationship with Nominee") != null ? basicDBObject.get("Appointee Relationship with Nominee").toString() : "");
                    map.put("Loan Disbursal Date",basicDBObject.get("Loan Disbursal Date") != null ? basicDBObject.get("Loan Disbursal Date").toString() : "");
                    map.put("Customer PAN No",basicDBObject.get("Customer PAN No") != null ? basicDBObject.get("Customer PAN No").toString() : "");
                    map.put("Loan Type",basicDBObject.get("Loan Type") != null ? basicDBObject.get("Loan Type").toString() : "");
                    map.put("Category",basicDBObject.get("Category") != null ? basicDBObject.get("Category").toString() : "");

                    temp.add(map);
                }catch(Exception e){
                    logger.debug("Exception occurred while fetching Insurance Report: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            output.add(temp);
        }

        logger.debug("createInsuranceReport ended");
        return output;
    }

    private List<List<LinkedHashMap<String, String>>> createChequePrintingReport(List<List<BasicDBObject>> multipleReport){
        logger.debug("createChequePrintingReport started");

        List<List<LinkedHashMap<String, String>>> output = new ArrayList<>();

        for(List<BasicDBObject> report:multipleReport){
            List<LinkedHashMap<String, String>> temp = new ArrayList<>();

            for(BasicDBObject basicDBObject : report){
                try{
                    LinkedHashMap<String, String> map = new LinkedHashMap<>();
                    map.put("REFID",basicDBObject.get("_id") != null ? basicDBObject.get("_id").toString() : "");
                    map.put("LEVIOSA REF ID",basicDBObject.get("LEVIOSA REF ID") != null ? basicDBObject.get("LEVIOSA REF ID").toString() : "");
                    map.put("ZONE",basicDBObject.get("ZONE") != null ? basicDBObject.get("ZONE").toString() : "");
                    map.put("BRANCH",basicDBObject.get("BRANCH") != null ? basicDBObject.get("BRANCH").toString() : "");
                    map.put("LOCATION",basicDBObject.get("LOCATION") != null ? basicDBObject.get("LOCATION").toString() : "");
                    map.put("DISBURSAL FAVOURING",basicDBObject.get("DISBURSAL FAVOURING") != null ? basicDBObject.get("DISBURSAL FAVOURING").toString() : "");
                    map.put("MODE OF PAYMENT",basicDBObject.get("MODE OF PAYMENT") != null ? basicDBObject.get("MODE OF PAYMENT").toString() : "");
                    map.put("DISBURSAL AMOUNT",basicDBObject.get("DISBURSAL AMOUNT") != null ? basicDBObject.get("DISBURSAL AMOUNT").toString() : "");
                    map.put("DISBURSAL DATE",basicDBObject.get("DISBURSAL DATE") != null ? basicDBObject.get("DISBURSAL DATE").toString() : "");
                    map.put("PR NO",basicDBObject.get("PR NO") != null ? basicDBObject.get("PR NO").toString() : "");
                    map.put("STAGE",basicDBObject.get("STAGE") != null ? basicDBObject.get("STAGE").toString() : "");
                    map.put("STATUS",basicDBObject.get("STATUS") != null ? basicDBObject.get("STATUS").toString() : "");
                    map.put("PRODUCT",basicDBObject.get("PRODUCT") != null ? basicDBObject.get("PRODUCT").toString() : "");

                    temp.add(map);
                }catch(Exception e){
                    logger.debug("Exception occurred while fetching Cheque Printing Report: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            output.add(temp);
        }

        logger.debug("createChequePrintingReport ended");
        return output;
    }

    private List<List<LinkedHashMap<String, String>>> createPddMisReportHelper(List<List<BasicDBObject>> multipleReport){
        List<List<LinkedHashMap<String, String>>> output = new ArrayList<>();

        List<LinkedHashMap<String, String>> pending = createPddMisReport(multipleReport.get(0),"Pending");
        List<LinkedHashMap<String, String>> received = createPddMisReport(multipleReport.get(1),"Received");
        List<LinkedHashMap<String, String>> waived = createPddMisReport(multipleReport.get(2),"Waived");

        output.add(pending);
        output.add(received);
        output.add(waived);

        return output;
    }

    private List<LinkedHashMap<String, String>> createPddMisReport(List<BasicDBObject> report,String reportName){
        logger.debug("createPddMisReport started");

        List<LinkedHashMap<String, String>> temp = new ArrayList<>();

        for(BasicDBObject basicDBObject : report){
            try{
                LinkedHashMap<String, String> map = new LinkedHashMap<>();
                map.put("SR_NO",basicDBObject.get("SR_NO") != null ? basicDBObject.getString("SR_NO") : "");
                map.put("REF_ID",basicDBObject.get("_id") != null ? basicDBObject.getString("_id") : "");
                map.put("DIVISION",basicDBObject.get("DIVISION") != null ? basicDBObject.getString("DIVISION") : "");
                map.put("LOCATION",basicDBObject.get("LOCATION") != null ? basicDBObject.getString("LOCATION") : "");
                map.put("LEVIOSA_REF_ID",basicDBObject.get("LEVIOSA_REF_ID") != null ? basicDBObject.getString("LEVIOSA_REF_ID") : "");
                map.put("PR_NO",basicDBObject.get("PR_NO") != null ? basicDBObject.getString("PR_NO") : "");
                map.put("DOC_UNIQUE_NO",basicDBObject.get("DOC_UNIQUE_NO") != null ? basicDBObject.getString("DOC_UNIQUE_NO") : "");
                map.put("BRANCH_NAME",basicDBObject.get("BRANCH_NAME") != null ? basicDBObject.getString("BRANCH_NAME") : "");
                map.put("SUB_PRODUCT",basicDBObject.get("SUB_PRODUCT") != null ? basicDBObject.getString("SUB_PRODUCT") : "");
                map.put("PRODUCT",basicDBObject.get("PRODUCT") != null ? basicDBObject.getString("PRODUCT") : "");
                map.put("CUSTOMER_NAME",basicDBObject.get("CUSTOMER_NAME") != null ? basicDBObject.getString("CUSTOMER_NAME") : "");
                map.put("DISBURSAL_DATE",basicDBObject.get("DISBURSAL_DATE") != null ? basicDBObject.getString("DISBURSAL_DATE") : "");
                map.put("TARGETED_RESOLUTION_DATE",basicDBObject.get("RESOLUTION_DATE") != null ? basicDBObject.getString("RESOLUTION_DATE") : "");
                if(reportName.contains("Pending")){
                    map.put("DAYS_AGEING",basicDBObject.get("DAYS_AGEING") != null ? basicDBObject.getString("DAYS_AGEING") : "");
                    map.put("AGEING",basicDBObject.get("AGEING") != null ? basicDBObject.getString("AGEING") : "");
                }
                map.put("PDD_DOCUMENTS",basicDBObject.get("PDD_DOCUMENTS") != null ? basicDBObject.getString("PDD_DOCUMENTS") : "");
                map.put("DOCS_CATEGORY",basicDBObject.get("DOCS_CATEGORY") != null ? basicDBObject.getString("DOCS_CATEGORY") : "");
                if(reportName.contains("Pending")){
                    map.put("PDD_CATEGORY",basicDBObject.get("PDD_CATEGORY") != null ? basicDBObject.getString("PDD_CATEGORY") : "");
                    map.put("DOC_TYPE","");
                    map.put("CURRENT/DELINQUENT_STATUS","");
                }
                if(reportName.contains("Received")){
                    map.put("HO_STATUS_DATE",basicDBObject.get("RECEIVED_ON") != null ? basicDBObject.getString("RECEIVED_ON") : "");
                    map.put("RECD_POD_NO","");
                    map.put("HO_REMARKS",basicDBObject.get("REMARKS") != null ? basicDBObject.getString("REMARKS") : "");
                    map.put("RECEIVED_BY","");
                    map.put("HO_STATUS",basicDBObject.get("PDD_CATEGORY") != null ? basicDBObject.getString("PDD_CATEGORY") : "");
                    map.put("RECEIVED_BY_USER","");
                }
                if(reportName.contains("Waived")){
                    map.put("DOC_TYPE","");
                    map.put("HO_STATUS_DATE",basicDBObject.get("RECEIVED_ON") != null ? basicDBObject.getString("RECEIVED_ON") : "");
                    map.put("HO_REMARKS",basicDBObject.get("REMARKS") != null ? basicDBObject.getString("REMARKS") : "");
                    map.put("HO_STATUS",basicDBObject.get("PDD_CATEGORY") != null ? basicDBObject.getString("PDD_CATEGORY") : "");
                }

                temp.add(map);
            }catch(Exception e){
                logger.debug("Exception occurred while creating PDD MIS Report: " + e.getMessage());
                e.printStackTrace();
            }
        }

        logger.debug("createPddMisReport ended");
        return temp;
    }
}
