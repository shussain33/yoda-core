package com.softcell.reporting.report;

import com.softcell.constants.FieldSeparator;
import com.softcell.dao.mongodb.config.MongoConfig;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.reporting.builder.GoNoGoApplicationToJsonParser;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Format;
import com.softcell.reporting.domains.Format.Action;
import com.softcell.reporting.domains.Format.DataType;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.io.*;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class SalesReport {

    public static SortedMap<Integer, ColumnConfiguration> customReportMap = new TreeMap();
    public static SortedMap<Integer, ColumnConfiguration> saleReportMonthly = new TreeMap();

    static {
        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DSA Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        Format format = new Format();
        format.setTo("MM/dd/yyyy");
        format.setFrom("ddMMyyyy");
        format.setAction(Action.convert);
        format.setDataType(DataType.date);
        columnConfiguration.setFormat(format);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Gender");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Applied Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Login Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setAction(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ID Proof Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ID Proof Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);

        // ============

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        customReportMap.put(index, columnConfiguration);
        index++;

        // ============
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoney999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Subvention Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_dealerSubvention999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Instrument");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyInstrument999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        format = new Format();
        format.setFrom("Select Margin Money Instrument");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_approvedAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Make");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("processing Fees");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_processingFees999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("other Charges");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_otherChargesIfAny999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Total Asset Cost");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_totalAssetCost999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Ctg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Advance EMI");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_advanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_scheme999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("Manufacture Sub Born By Dealer");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufSubBorneByDealer999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Price");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName(" Asset Model No");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Confirm");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyConfirmation999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_losID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_utrNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        customReportMap.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Queue ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("STA");
        format.setTo("State Through Process");
        format.setAction(Action.replace);
        format.setCapitalize(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        customReportMap.put(index, columnConfiguration);

    }

    static {

        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DSA Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        saleReportMonthly.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        saleReportMonthly.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        Format format = new Format();
        format.setTo("MM/dd/yyyy");
        format.setFrom("ddMMyyyy");
        format.setAction(Action.convert);
        format.setDataType(DataType.date);
        columnConfiguration.setFormat(format);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Gender");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Applied Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Login Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        saleReportMonthly.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setAction(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ID Proof Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ID Proof Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);

        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoney999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Subvention Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_dealerSubvention999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Instrument");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyInstrument999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        format = new Format();
        format.setFrom("Select Margin Money Instrument");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_approvedAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Make");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("processing Fees");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_processingFees999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("other Charges");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_otherChargesIfAny999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Total Asset Cost");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_totalAssetCost999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Ctg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Advance EMI");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_advanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_scheme999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("Manufacture Sub Born By Dealer");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufSubBorneByDealer999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Price");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName(" Asset Model No");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Confirm");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyConfirmation999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_losID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_utrNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        saleReportMonthly.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Queue ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("STA");
        format.setTo("State Through Process");
        format.setAction(Action.replace);
        format.setCapitalize(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        saleReportMonthly.put(index, columnConfiguration);

    }

    MongoOperations mongoOperation = MongoConfig.getMongoTemplate();

    public byte[] getSaleReport(Query gonogoCustomerQuery, Query applicationRequestQuery) throws IOException {

        String encoding = "UTF8";
        List<GoNoGoCustomerApplication> result = mongoOperation.find(gonogoCustomerQuery, GoNoGoCustomerApplication.class);

        OutputStreamWriter reportOutputStreamWriter = null;

        ByteArrayOutputStream outputStream1 = null;


        BufferedWriter bufferedWriter = null;
        if (GngDateUtil.checkScheduleTime(23))
            bufferedWriter = new BufferedWriter(new FileWriter(
                    new File(GngUtils.getReSourcePath() + File.separator
                            + "SalesReport_"
                            + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime())
                            + ".csv")));

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

        flatReprtConfiguration.setHeaderMap(customReportMap);

        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

        flatReprtConfiguration.setReportName("GNGVersion2");

        flatReprtConfiguration.setReportType("CSV");

        flatReprtConfiguration.setReportFormat("CSV");

        GoNoGoApplicationToJsonParser applicationToJsonParser;

        try {

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, encoding);

            reportOutputStreamWriter.write(flatReprtConfiguration
                    .getFileHeader());

            reportOutputStreamWriter.append('\n');
            if (GngDateUtil.checkScheduleTime(23)) {
                bufferedWriter.write(flatReprtConfiguration.getFileHeader());
                bufferedWriter.newLine();
            }
        } catch (IOException e3) {

        }
        /* Post IPA Object to get DO details of customer */
        for (GoNoGoCustomerApplication goNoGoCustomerApplication : result) {
            try {

                applicationToJsonParser = new GoNoGoApplicationToJsonParser(goNoGoCustomerApplication);

                Query postIPAQuery = new Query();

                postIPAQuery.addCriteria(Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId()));

                PostIpaRequest postIPA = mongoOperation.findOne(postIPAQuery, PostIpaRequest.class);

                String row;

                if (null != postIPA) {

                    applicationToJsonParser.build();

                    row = applicationToJsonParser.build(postIPA.getPostIPA()).enrichJson(flatReprtConfiguration).toString().replaceAll("[\\\n|\\\r|\\\t]", "");

                } else {

                    row = applicationToJsonParser.build().enrichJson(flatReprtConfiguration).toString().replaceAll("[\\\n|\\\r|\\\t]", "");
                }

                reportOutputStreamWriter.write(row);

                reportOutputStreamWriter.write('\n');
                if (GngDateUtil.checkScheduleTime(23)) {
                    bufferedWriter.write(row);
                    bufferedWriter.newLine();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (GngDateUtil.checkScheduleTime(23)) {
            bufferedWriter.flush();

            bufferedWriter.close();
        }
        reportOutputStreamWriter.close();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ZipOutputStream zout = new ZipOutputStream(baos);

        ZipEntry personalInfoFileEntry = new ZipEntry("Sales_offurred"
                + GngDateUtil.getMmDdYyyySlash(new DateTime()) + ".csv");

        zout.putNextEntry(personalInfoFileEntry);

        zout.write(outputStream1.toByteArray());

        zout.setComment("Sale Report");

        zout.finish();

        zout.closeEntry();

        return baos.toByteArray();

    }

    public byte[] getSaleReportMonthly(Query gonogoCustomerQuery, Query applicationRequestQuery) throws IOException {

        String encoding = "UTF8";
        List<GoNoGoCustomerApplication> result = mongoOperation.find(
                gonogoCustomerQuery, GoNoGoCustomerApplication.class);
        OutputStreamWriter reportOutputStreamWriter = null;
        ByteArrayOutputStream outputStream1 = null;
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
                new File(GngUtils.getReSourcePath() + File.separator
                        + "MonthlySalesReport_"
                        + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime())
                        + ".csv")));
        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();
        flatReprtConfiguration.setHeaderMap(saleReportMonthly);
        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);
        flatReprtConfiguration.setReportName("GNGVersion2");
        flatReprtConfiguration.setReportType("CSV");
        flatReprtConfiguration.setReportFormat("CSV");
        GoNoGoApplicationToJsonParser applicationToJsonParser;
        try {
            outputStream1 = new ByteArrayOutputStream();
            reportOutputStreamWriter = new OutputStreamWriter(outputStream1,
                    encoding);
            reportOutputStreamWriter.write(flatReprtConfiguration
                    .getFileHeader());
            reportOutputStreamWriter.append('\n');
            bufferedWriter.write(flatReprtConfiguration.getFileHeader());
            bufferedWriter.newLine();
        } catch (IOException e3) {
        }
        /* Post IPA Object to get DO details of customer */
        for (GoNoGoCustomerApplication goNoGoCustomerApplication : result) {
            try {
                applicationToJsonParser = new GoNoGoApplicationToJsonParser(
                        goNoGoCustomerApplication);
                Query postIPAQuery = new Query();
                postIPAQuery.addCriteria(Criteria.where("_id").is(
                        goNoGoCustomerApplication.getGngRefId()));
                PostIpaRequest postIPA = mongoOperation.findOne(postIPAQuery,
                        PostIpaRequest.class);
                String row;
                if (null != postIPA) {
                    applicationToJsonParser.build();
                    row = applicationToJsonParser.build(postIPA.getPostIPA())
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\\n|\\\r|\\\t]", "");
                } else {
                    row = applicationToJsonParser.build()
                            .enrichJson(flatReprtConfiguration).toString()
                            .replaceAll("[\\\n|\\\r|\\\t]", "");
                }

                reportOutputStreamWriter.write(row);
                reportOutputStreamWriter.write('\n');
                bufferedWriter.write(row);
                bufferedWriter.newLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        bufferedWriter.flush();
        bufferedWriter.close();
        reportOutputStreamWriter.close();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(baos);
        ZipEntry personalInfoFileEntry = new ZipEntry("Sales_offurred"
                + GngDateUtil.getMmDdYyyySlash(new DateTime()) + ".csv");
        zout.putNextEntry(personalInfoFileEntry);
        zout.write(outputStream1.toByteArray());
        zout.setComment("Sale Report");

        zout.finish();
        zout.closeEntry();

        return baos.toByteArray();

    }

}
