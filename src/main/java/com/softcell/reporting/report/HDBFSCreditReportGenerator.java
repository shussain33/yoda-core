package com.softcell.reporting.report;

import com.google.common.base.CharMatcher;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.Product;
import com.softcell.constants.Status;
import com.softcell.dao.mongodb.helper.QueryBuilder;
import com.softcell.dao.mongodb.repository.ApplicationRepository;
import com.softcell.dao.mongodb.repository.ReportingRepository;
import com.softcell.dao.mongodb.repository.loyaltycard.LoyaltyCardRepository;
import com.softcell.gonogo.model.core.GoNoGoCustomerApplication;
import com.softcell.gonogo.model.core.LoyaltyCardDetails;
import com.softcell.gonogo.model.masters.BranchMaster;
import com.softcell.gonogo.model.masters.DealerBranchMaster;
import com.softcell.gonogo.model.masters.ReportingModuleConfiguration;
import com.softcell.gonogo.model.request.PostIpaRequest;
import com.softcell.gonogo.model.request.extendedwarranty.ExtendedWarrantyDetails;
import com.softcell.gonogo.model.request.manufacturer.samsung.SerialNumberInfo;
import com.softcell.reporting.builder.GoNoGoApplicationToJsonParser;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Format;
import com.softcell.utils.GnGFileUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by priyanka on 30/11/17.
 */
@Service
public class HDBFSCreditReportGenerator {
    private static final Logger logger = LoggerFactory.getLogger(HDBFSCreditReportGenerator.class);

    @Autowired
    private ReportingRepository reportingRepository;

    @Autowired
    ApplicationRepository applicationRepository;

    @Autowired
    LoyaltyCardRepository loyaltyCardRepository;

    public static SortedMap<Integer, ColumnConfiguration> defaultConfigMapVersion2 = new TreeMap();
     static {

         int index = 0;
         ColumnConfiguration columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Product Type");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_L_A_loanType999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Application ID");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("appRequest_refID999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Application Type");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("applicantType");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("DSA ID");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("header_dsaId999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Dealer ID");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("header_dealerId999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Application Login Date");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("header_dateTime999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Application Status");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("A_applicationStatus999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         Format format = new Format();
         format.setAction(Format.Action.capitalize);
         format.setDataType(Format.DataType.string);
         columnConfiguration.setFormat(format);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("HDB Branch Name");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("branchName");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("HDB Branch State");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("branchState");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Queue ID");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("header_croId999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         format = new Format();
         format.setFrom("STA");
         format.setTo("Straight  Through Process");
         format.setAction(Format.Action.replace);
         format.setCapitalize(Format.Action.capitalize);
         format.setDataType(Format.DataType.string);
         columnConfiguration.setFormat(format);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Current Stage");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("appRequest_currentStageId999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("First Name");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_N_firstName999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Last Name");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_N_lastName999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Zip Code");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("City");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_CustomerAddress_city0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("State");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_CustomerAddress_state0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Phone Number");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Aadhar Verified");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("isAadharVerified");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;


         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Credit Cards Category");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration
                 .setColumnKey("C_S_CreditCardSurrogate_cardCategory0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Approved Amount By CRO");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("CIBIL Score");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("cibil_fieldValue999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Mobile Verification");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_A_mobileVerified999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Margin Money");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("postIPA_marginMoney999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(false);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Dealer Name");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_dlrName0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(false);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Total Asset Cost");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("postIPA_totalAssetCost999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(false);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Asset Ctg");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetCtg0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(false);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName(" Asset Model No");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_modelNo0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(false);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("LOS Id");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("LOS_losID999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(false);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("CRO Remark 1");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(false);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Applied Amount");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_L_A_loanAmount999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Loan Tenor");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_L_A_loanTenor999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Loyalty Card Number");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("loyaltyCardNumber");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("EW Asset Category");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("eWAssetCategory");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("EW Tenor");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("extendedWarrantyTenure");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("EW Premium");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("extendedWarrantyPremium");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Primary Asset Ctg");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Primary Asset Make");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Primary Asset ModelNo");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Scheme Dscr");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("postIPA_schemeDscr999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Tenor");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("postIPA_tenor999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("finance Amount");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("postIPA_financeAmount999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("DO Date");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("P_IPA_REQUEST_date999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("SerialNumber/IMEI");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("serialNumber_imei");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("SerialNumber/IMEI Status");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("serialNumber_imei_status");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;
         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Dealer Rank");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("appRequest_dealerRank999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Invoice Number");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("invoice_invoiceNumber999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

         columnConfiguration = new ColumnConfiguration();
         columnConfiguration.setColumnDisplayName("Invoice Date");
         columnConfiguration.setColumnIndex(index);
         columnConfiguration.setColumnKey("invoice_invoiceDate999");
         columnConfiguration.setDownloadable(true);
         columnConfiguration.setViewable(true);
         defaultConfigMapVersion2.put(index, columnConfiguration);
         index++;

     }
    public byte[] generateCreditReport(ReportingModuleConfiguration reportingModuleConfiguration) throws IOException  {
        logger.debug(" credit report is running with thread as [{}] at [{}]", Thread.currentThread().getName(), System.currentTimeMillis());

        String productType = reportingModuleConfiguration.getProductType();
        Date[] fromToDate = GngDateUtil.getFromToDate(reportingModuleConfiguration);

        Query gonogoCustomerQuery = QueryBuilder.buildGoNoGoApplicationQuery(
                reportingModuleConfiguration.getInstitutionID(), fromToDate[0], fromToDate[1],
                productType);

        byte[] zipByteArray = null;

        if (StringUtils.equals(productType, Product.CDL.name()) || StringUtils.equals(productType, Product.DPL.name())) {

          zipByteArray = generateCreditReportForCDLAndDPL(gonogoCustomerQuery);

        }

        return zipByteArray;
    }

    private byte[] generateCreditReportForCDLAndDPL(Query gonogoCustomerQuery) throws IOException {
        OutputStreamWriter reportOutputStreamWriter = null;

        ByteArrayOutputStream outputStream1 = null;

        BufferedWriter bufferedWriter = null;

        if (GngDateUtil.checkScheduleTime(23)){

            bufferedWriter = new BufferedWriter(new FileWriter(new File(GngUtils.getReSourcePath()
                    + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv")));

        }

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

        flatReprtConfiguration.setHeaderMap(defaultConfigMapVersion2);
        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);
        flatReprtConfiguration.setReportName("GNGVersion2");
        flatReprtConfiguration.setReportType("CSV");
        flatReprtConfiguration.setReportFormat("CSV");

        GoNoGoApplicationToJsonParser applicationToJsonParser;

        try {

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            if (GngDateUtil.checkScheduleTime(23)) {

                bufferedWriter.write(flatReprtConfiguration.getFileHeader());
                bufferedWriter.newLine();

            }
        }
        catch (IOException e3) {

            e3.printStackTrace();

        }

        List<GoNoGoCustomerApplication> goNoGoCustomerApplications = reportingRepository.getGoNoGoCustomerApplicationForCredit(gonogoCustomerQuery);

        /**
         * Post IPA Object to get DO details of customer
         */
        for (GoNoGoCustomerApplication goNoGoCustomerApplication : goNoGoCustomerApplications) {

            applicationToJsonParser = new GoNoGoApplicationToJsonParser(goNoGoCustomerApplication);

            String refId = goNoGoCustomerApplication.getGngRefId();
            String instId = goNoGoCustomerApplication.getApplicationRequest().getHeader() != null
                    ? goNoGoCustomerApplication.getApplicationRequest().getHeader().getInstitutionId() : null;
            String dealerId=goNoGoCustomerApplication.getApplicationRequest().getHeader().getDealerId();
            addSerialNumberInformationInCredit(applicationToJsonParser, refId);

            addLoyaltyCardDetailsInCreditReport(applicationToJsonParser, refId, instId);

            addExtendedWarrantyDetailsInCreditReport(applicationToJsonParser, refId, instId);

            addDealerBranchNameInCreditReport(applicationToJsonParser,instId,dealerId);

            addApplicantTypeInCreditReport(applicationToJsonParser,goNoGoCustomerApplication.getApplicationRequest().getApplicantType());

            Query postIPAQuery = new Query();

            postIPAQuery.addCriteria(Criteria.where("_id").is(goNoGoCustomerApplication.getGngRefId()));

            PostIpaRequest postIPA = reportingRepository.getPostIpaByRefId(goNoGoCustomerApplication.getGngRefId());


            StringBuffer row = new StringBuffer();

            if (null != postIPA) {

                applicationToJsonParser.build();

                row.append(
                        CharMatcher.anyOf("\r\n\t").removeFrom(applicationToJsonParser.build(postIPA).enrichJson(flatReprtConfiguration).toString())
                );

            } else {

                row.append(
                        CharMatcher.anyOf("\r\n\t").removeFrom(applicationToJsonParser.build().enrichJson(flatReprtConfiguration).toString())
                );

            }

            if (GngDateUtil.checkScheduleTime(23)) {

                bufferedWriter.write(row.toString());
                bufferedWriter.newLine();

            }

            reportOutputStreamWriter.write(row.toString());
            reportOutputStreamWriter.write('\n');

        }

        if (GngDateUtil.checkScheduleTime(23)) {
            bufferedWriter.flush();
            bufferedWriter.close();
        }

        reportOutputStreamWriter.close();

        return GnGFileUtils.compressBytes(
                "CREDIT_REPORT_"+ GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                outputStream1.toByteArray());

    }
    private void addSerialNumberInformationInCredit(GoNoGoApplicationToJsonParser applicationToJsonParser ,String refId) {

        Query serialNumberInfoQuery = new Query();

        serialNumberInfoQuery.addCriteria(new Criteria().orOperator(
                Criteria.where("refID").is(refId)
                        .and("status").is(Status.VALID.name()),
                Criteria.where("refID").is(refId)
                        .and("status")
                        .is(Status.ROLL_BACK.name())));

        SerialNumberInfo serialNumberInfo = reportingRepository.getSerialNoInfo(serialNumberInfoQuery);

        if (null != serialNumberInfo) {

            applicationToJsonParser.build(serialNumberInfo);

        }
    }

    private void addLoyaltyCardDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId, String instId) {

        LoyaltyCardDetails loyaltyCardDetails = loyaltyCardRepository.fetchLoyaltyCardDetails(refId, instId);

        if (null != loyaltyCardDetails) {
            applicationToJsonParser.build(loyaltyCardDetails);

        }


    }
    private void addApplicantTypeInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String applicantType) {
        if (StringUtils.isNotBlank(applicantType)){
            applicationToJsonParser.buildApplicantType(applicantType);
        }
    }

    private void addExtendedWarrantyDetailsInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String refId, String instId) {

        ExtendedWarrantyDetails extendedWarrantyDetails = applicationRepository.fetchExtendedWarrantyDetails(refId, instId);

        if (null != extendedWarrantyDetails) {
            applicationToJsonParser.build(extendedWarrantyDetails);
        }
    }

    private void addDealerBranchNameInCreditReport(GoNoGoApplicationToJsonParser applicationToJsonParser, String instId, String dealerId) {
        DealerBranchMaster dealerBranchMaster = reportingRepository.getBranchMaster(dealerId,instId);
        if (null!=dealerBranchMaster) {
            applicationToJsonParser.build(dealerBranchMaster);
            if (StringUtils.isNotBlank(dealerBranchMaster.getBranchName())) {
                BranchMaster branchMaster = applicationRepository.getDealerBranchState(dealerBranchMaster.getBranchName(),instId);
                if (null!=branchMaster && StringUtils.isNotBlank(branchMaster.getStateName())) {
                    applicationToJsonParser.build(branchMaster.getStateName());
                }
            }
        }
    }


}
