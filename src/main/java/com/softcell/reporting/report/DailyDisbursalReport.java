package com.softcell.reporting.report;

public class DailyDisbursalReport {

    private String id;
    private String total;
    private String count ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DailyDisbursalReport that = (DailyDisbursalReport) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (total != null ? !total.equals(that.total) : that.total != null) return false;
        return count != null ? count.equals(that.count) : that.count == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (total != null ? total.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DailyDisbursalReport{" +
                "id='" + id + '\'' +
                ", total=" + total +
                ", count=" + count +
                '}';
    }
}
