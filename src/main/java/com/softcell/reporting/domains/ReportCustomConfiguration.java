package com.softcell.reporting.domains;

import com.softcell.reporting.domains.Format.Action;
import com.softcell.reporting.domains.Format.DataType;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

@Component
public class ReportCustomConfiguration {

    private static SortedMap<Integer, ColumnConfiguration> reportConfiguration;


    public static Map<Integer, ColumnConfiguration> buildSalesReportConfiguration() {

        reportConfiguration = new TreeMap<Integer, ColumnConfiguration>();

        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DSA Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Branch Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("branchName");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        Format format = new Format();
        format.setTo("MM/dd/yyyy");
        format.setFrom("ddMMyyyy");
        format.setAction(Action.convert);
        format.setDataType(DataType.date);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Gender");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Applied Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Login Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setAction(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ID Proof Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("ID Proof Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoney999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DO Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_REQUEST_date999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Subvention Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_dealerSubvention999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Instrument");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyInstrument999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        format = new Format();
        format.setFrom("Select Margin Money Instrument");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_approvedAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Make");
        columnConfiguration.setColumnIndex(22);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("processing Fees");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_processingFees999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("other Charges");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_otherChargesIfAny999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Total Asset Cost");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_totalAssetCost999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Ctg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Advance EMI");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_advanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_scheme999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration
                .setColumnDisplayName("Manufacture Sub Born By Dealer");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufSubBorneByDealer999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Price");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName(" Asset Model No");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Confirm");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyConfirmation999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_losID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_utrNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Queue ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("STA");
        format.setTo("State Through Process");
        format.setAction(Action.replace);
        format.setCapitalize(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);

        // adding invoice deatails

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("INVOICE_NUMBER");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("invoice_invoiceNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("INVOICE_TYPE");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("invoice_invoiceType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("INVOICE_DATE");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("invoice_invoiceDate999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Disbursement Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("disbursementDate");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Handover Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("handoverDate");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Received Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("receivedDate");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("File Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("fileStatus");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Hold Reason");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("holdReason");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        return reportConfiguration;

    }

    public static Map<Integer, ColumnConfiguration> buildCreditReportConfiguration() {

        reportConfiguration = new TreeMap<Integer, ColumnConfiguration>();

        int index = 0;
        ColumnConfiguration columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_refID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DSA ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dsaId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dealerId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Login Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Time Stamp");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTimeTIME_STAMP999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Submit Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_dateTime999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_applicationStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        Format format = new Format();
        format.setAction(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Queue ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("header_croId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("STA");
        format.setTo("State Through Process");
        format.setAction(Action.replace);
        format.setCapitalize(Action.capitalize);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Current Stage");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_currentStageId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("First Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_firstName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Middle Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_middleName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Last Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_N_lastName999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Marital Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_maritalStatus999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Gender");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_gender999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DOB");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_dateOfBirth999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setTo("MM/dd/yyyy");
        format.setFrom("ddMMyyyy");
        format.setAction(Action.convert);
        format.setDataType(DataType.date);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Age");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_age999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Education");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_education999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("statusFlag");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_statusFlag999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine10");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine20");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line30");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine11");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine21");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line31");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residence Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_residenceAddressType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine12");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_addressLine22");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Line3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_line32");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Zip Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_pin2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("City");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_city2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("State");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_CustomerAddress_state2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneType3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Ext");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_extension3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Area Code");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_areaCode3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Phone Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Phone_phoneNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailAddress0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailType1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Email Address");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Email_emailAddress1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employ Constitution ");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_constitution0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employer Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_employmentName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Employer Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_Employment_employmentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycName3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("KYC Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_KYC_Kyc_kycNumber3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Net Take Home-salary ");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_SalariedSurrogate_netTakeHome0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car Model Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CarSurrogate_modelName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car Category");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CarSurrogate_category0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car manufacturer Year");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CarSurrogate_manufactureYear0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Car Registration Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CarSurrogate_registrationNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Own House Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_houseType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("House Surrogate-Document Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_OwnHouseSurrogate_documentType0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Trader-Year In Business");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_TraderSurrogate_yearInBussines0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Trader-Business Proof");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_TraderSurrogate_bussinesProof0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_cardNumber0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Cards Category");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_cardCategory0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Vintage");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_cardVintage0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Limit");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_limit0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Outstanding Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_outStandingAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Credit Card Last Payment Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_S_CreditCardSurrogate_lastPaymentAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dedupe Ref Id1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dedupe Ref Id2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dedupe Ref Id3");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("dedup_DedupeDetails_refId2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);

        index++;
        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount By CRO");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_amtApproved0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_decisionUpdateDate0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Down Payment");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_downPayment0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Eligible Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Stability1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_addStability4");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("-1");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Address Stability 2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_addStability5");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("-1");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("PAN Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("NO_RESPONSE");
        format.setTo("Pan Not Submitted");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("PAN Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_message0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        format = new Format();
        format.setFrom("No Response");
        format.setTo("Pan Not Submitted");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CIBIL Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("cibil_fieldValue999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Application Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Residential Address Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue2");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Office Address Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_fieldValue3");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Name Score");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_ApplicationScoringResult_nameScore0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Negative Pin Flag");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("A_negativePincode999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Mobile Verification");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_A_mobileVerified999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoney999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Name");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_dlrName0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Subvention Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_dealerSubvention999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Instrument");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyInstrument999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        format = new Format();
        format.setFrom("Select Margin Money Instrument");
        format.setTo("NA");
        format.setAction(Action.replace);
        format.setDataType(DataType.string);
        columnConfiguration.setFormat(format);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Approved Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_approvedAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Make");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("processing Fees");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_processingFees999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("other Charges");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_otherChargesIfAny999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Total Asset Cost");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_totalAssetCost999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Ctg");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Advance EMI");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_advanceEmi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_scheme999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Manufacture Sub Born By Dealer");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufSubBorneByDealer999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Asset Price");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName(" Asset Model No");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_ASSET_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Margin Money Confirm");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_marginMoneyConfirmation999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Id");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_losID999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Number");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_utrNumber999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("LOS Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("LOS_status999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 1");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("CRO Remark 2");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("CRO_JUST_CroJustification_remark1");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Applied Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loan Tenor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanTenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Loan Type");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("C_L_A_loanType999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primay Asset Ctg");
        columnConfiguration.setColumnIndex(101);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetCtg0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(101, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primay Asset Make");
        columnConfiguration.setColumnIndex(102);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_assetMake0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(102, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primay Asset ModelNo");
        columnConfiguration.setColumnIndex(104);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_modelNo0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(104, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Primay Asset Price");
        columnConfiguration.setColumnIndex(105);
        columnConfiguration.setColumnKey("C_L_A_AssetDetails_price0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(105, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("eligibleAmt0");
        columnConfiguration.setColumnIndex(128);
        columnConfiguration.setColumnKey("CRO_CroDecision_eligibleAmt0");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(128, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("emi");
        columnConfiguration.setColumnIndex(106);
        columnConfiguration.setColumnKey("C_L_A_emi999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(false);
        reportConfiguration.put(106, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Grid ID");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_gridId999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Outcome");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_decision999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("reMark");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("se_reMark999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Scheme Dscr");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_schemeDscr999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("finance Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_financeAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("advance EMI Tenor");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_advanceEMITenor999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("net Funding Amount");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_netFundingAmount999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("manuf. Subvention Mbd");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufSubventionMbd999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("manufacturing Processing Fee");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_manufacturingProcessingFee999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Dealer Sbvn. Waived At POS");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("postIPA_dlrSbvnWaivedAtPOS999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("DO Date");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("P_IPA_REQUEST_date999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Suspicious Activity");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("appRequest_suspiciousActivity999");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("SerialNumber");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("serialNumber");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;

        columnConfiguration = new ColumnConfiguration();
        columnConfiguration.setColumnDisplayName("Serial Number Status");
        columnConfiguration.setColumnIndex(index);
        columnConfiguration.setColumnKey("serialNumberStatus");
        columnConfiguration.setDownloadable(true);
        columnConfiguration.setViewable(true);
        reportConfiguration.put(index, columnConfiguration);
        index++;


        return reportConfiguration;
    }


}
