package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.core.Kyc;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this class serves as a parameter mapping domain for sales report from calling
 * api specific object
 *
 * @author prateek
 */

@Component
public class SalesReportResponse {

    @Id
    public String id;

    @JsonProperty("sDsaId")
    public String dsaId;

    @JsonProperty("sFirstName")
    public String firstName;

    @JsonProperty("sMiddleName")
    private String middleName;

    @JsonProperty("sLastName")
    private String lastName;

    @JsonProperty("sDob")
    private String dob;

    @JsonProperty("sGender")
    private String gender;

    @JsonProperty("lAppliedAmount")
    private long appliedAmount;

    @JsonProperty("dApplicationDate")
    private Date applicationDate;

    @JsonProperty("sDealerId")
    private String dealerId;

    @JsonProperty("sAssetCategory")
    private String assetCategory;

    @JsonProperty("sMake")
    private String make;

    @JsonProperty("sDealerName")
    private String dealerName;

    @JsonProperty("sModelNo")
    private String modelNo;

    @JsonProperty("sApplicationStatus")
    private String applicationStatus;

    @JsonProperty("ckycs")
    private List<Kyc> kycs;

    @JsonProperty("dDoDate")
    private Date doDate;

    @JsonProperty("oPostIpa")
    private SalesPostIPA postIpa;

    @JsonProperty("sRemark")
    private String remark;

    @JsonProperty("sLosId")
    private String losId;

    @JsonProperty("sUtrNo")
    private String utrNo;

    @JsonProperty("sLosStatus")
    private String losStatus;

    @JsonIgnore
    @JsonProperty("sPan")
    private String pan;

    @JsonIgnore
    @JsonProperty("sPassport")
    private String passport;

    @JsonIgnore
    @JsonProperty("sAadhar")
    private String aadhar;

    @JsonIgnore
    @JsonProperty("sVoterId")
    private String voterId;

    @JsonIgnore
    @JsonProperty("sDrivingLicense")
    private String drivingLicense;

    @JsonProperty("sFullName")
    private String fullName;

    @JsonProperty("sInvoiceNumber")
    private String invoiceNumber;

    @JsonProperty("sInvoiceType")
    private String invoiceType;

    @JsonProperty("sInvoiceDate")
    private String invoiceDate;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public SalesReportResponse() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDsaId() {
        return dsaId;
    }

    public void setDsaId(String dsaId) {
        this.dsaId = dsaId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getAppliedAmount() {
        return appliedAmount;
    }

    public void setAppliedAmount(long appliedAmount) {
        this.appliedAmount = appliedAmount;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getAssetCategory() {
        return assetCategory;
    }

    public void setAssetCategory(String assetCategory) {
        this.assetCategory = assetCategory;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public List<Kyc> getKycs() {
        return kycs;
    }

    public void setKycs(List<Kyc> kycs) {
        this.kycs = kycs;
    }

    public Date getDoDate() {
        return doDate;
    }

    public void setDoDate(Date doDate) {
        this.doDate = doDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLosId() {
        return losId;
    }

    public void setLosId(String losId) {
        this.losId = losId;
    }

    public String getUtrNo() {
        return utrNo;
    }

    public void setUtrNo(String utrNo) {
        this.utrNo = utrNo;
    }

    public String getLosStatus() {
        return losStatus;
    }

    public void setLosStatus(String losStatus) {
        this.losStatus = losStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalPropesrties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getDrivingLicense() {
        return drivingLicense;
    }

    public void setDrivingLicense(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public SalesPostIPA getPostIpa() {
        return postIpa;
    }

    public void setPostIpa(SalesPostIPA postIpa) {
        this.postIpa = postIpa;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SalesReportResponse [id=");
        builder.append(id);
        builder.append(", dsaId=");
        builder.append(dsaId);
        builder.append(", firstName=");
        builder.append(firstName);
        builder.append(", middleName=");
        builder.append(middleName);
        builder.append(", lastName=");
        builder.append(lastName);
        builder.append(", dob=");
        builder.append(dob);
        builder.append(", gender=");
        builder.append(gender);
        builder.append(", appliedAmount=");
        builder.append(appliedAmount);
        builder.append(", applicationDate=");
        builder.append(applicationDate);
        builder.append(", dealerId=");
        builder.append(dealerId);
        builder.append(", assetCategory=");
        builder.append(assetCategory);
        builder.append(", make=");
        builder.append(make);
        builder.append(", dealerName=");
        builder.append(dealerName);
        builder.append(", modelNo=");
        builder.append(modelNo);
        builder.append(", applicationStatus=");
        builder.append(applicationStatus);
        builder.append(", kycs=");
        builder.append(kycs);
        builder.append(", doDate=");
        builder.append(doDate);
        builder.append(", postIpa=");
        builder.append(postIpa);
        builder.append(", remark=");
        builder.append(remark);
        builder.append(", losId=");
        builder.append(losId);
        builder.append(", utrNo=");
        builder.append(utrNo);
        builder.append(", losStatus=");
        builder.append(losStatus);
        builder.append(", pan=");
        builder.append(pan);
        builder.append(", passport=");
        builder.append(passport);
        builder.append(", aadhar=");
        builder.append(aadhar);
        builder.append(", voterId=");
        builder.append(voterId);
        builder.append(", drivingLicense=");
        builder.append(drivingLicense);
        builder.append(", fullName=");
        builder.append(fullName);
        builder.append(", invoiceNumber=");
        builder.append(invoiceNumber);
        builder.append(", invoiceType=");
        builder.append(invoiceType);
        builder.append(", invoiceDate=");
        builder.append(invoiceDate);
        builder.append(", additionalProperties=");
        builder.append(additionalProperties);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aadhar == null) ? 0 : aadhar.hashCode());
        result = prime
                * result
                + ((additionalProperties == null) ? 0 : additionalProperties
                .hashCode());
        result = prime * result
                + ((applicationDate == null) ? 0 : applicationDate.hashCode());
        result = prime
                * result
                + ((applicationStatus == null) ? 0 : applicationStatus
                .hashCode());
        result = prime * result
                + (int) (appliedAmount ^ (appliedAmount >>> 32));
        result = prime * result
                + ((assetCategory == null) ? 0 : assetCategory.hashCode());
        result = prime * result
                + ((dealerId == null) ? 0 : dealerId.hashCode());
        result = prime * result
                + ((dealerName == null) ? 0 : dealerName.hashCode());
        result = prime * result + ((doDate == null) ? 0 : doDate.hashCode());
        result = prime * result + ((dob == null) ? 0 : dob.hashCode());
        result = prime * result
                + ((drivingLicense == null) ? 0 : drivingLicense.hashCode());
        result = prime * result + ((dsaId == null) ? 0 : dsaId.hashCode());
        result = prime * result
                + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result
                + ((fullName == null) ? 0 : fullName.hashCode());
        result = prime * result + ((gender == null) ? 0 : gender.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result
                + ((invoiceDate == null) ? 0 : invoiceDate.hashCode());
        result = prime * result
                + ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
        result = prime * result
                + ((invoiceType == null) ? 0 : invoiceType.hashCode());
        result = prime * result + ((kycs == null) ? 0 : kycs.hashCode());
        result = prime * result
                + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((losId == null) ? 0 : losId.hashCode());
        result = prime * result
                + ((losStatus == null) ? 0 : losStatus.hashCode());
        result = prime * result + ((make == null) ? 0 : make.hashCode());
        result = prime * result
                + ((middleName == null) ? 0 : middleName.hashCode());
        result = prime * result + ((modelNo == null) ? 0 : modelNo.hashCode());
        result = prime * result + ((pan == null) ? 0 : pan.hashCode());
        result = prime * result
                + ((passport == null) ? 0 : passport.hashCode());
        result = prime * result + ((postIpa == null) ? 0 : postIpa.hashCode());
        result = prime * result + ((remark == null) ? 0 : remark.hashCode());
        result = prime * result + ((utrNo == null) ? 0 : utrNo.hashCode());
        result = prime * result + ((voterId == null) ? 0 : voterId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SalesReportResponse other = (SalesReportResponse) obj;
        if (aadhar == null) {
            if (other.aadhar != null)
                return false;
        } else if (!aadhar.equals(other.aadhar))
            return false;
        if (additionalProperties == null) {
            if (other.additionalProperties != null)
                return false;
        } else if (!additionalProperties.equals(other.additionalProperties))
            return false;
        if (applicationDate == null) {
            if (other.applicationDate != null)
                return false;
        } else if (!applicationDate.equals(other.applicationDate))
            return false;
        if (applicationStatus == null) {
            if (other.applicationStatus != null)
                return false;
        } else if (!applicationStatus.equals(other.applicationStatus))
            return false;
        if (appliedAmount != other.appliedAmount)
            return false;
        if (assetCategory == null) {
            if (other.assetCategory != null)
                return false;
        } else if (!assetCategory.equals(other.assetCategory))
            return false;
        if (dealerId == null) {
            if (other.dealerId != null)
                return false;
        } else if (!dealerId.equals(other.dealerId))
            return false;
        if (dealerName == null) {
            if (other.dealerName != null)
                return false;
        } else if (!dealerName.equals(other.dealerName))
            return false;
        if (doDate == null) {
            if (other.doDate != null)
                return false;
        } else if (!doDate.equals(other.doDate))
            return false;
        if (dob == null) {
            if (other.dob != null)
                return false;
        } else if (!dob.equals(other.dob))
            return false;
        if (drivingLicense == null) {
            if (other.drivingLicense != null)
                return false;
        } else if (!drivingLicense.equals(other.drivingLicense))
            return false;
        if (dsaId == null) {
            if (other.dsaId != null)
                return false;
        } else if (!dsaId.equals(other.dsaId))
            return false;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (fullName == null) {
            if (other.fullName != null)
                return false;
        } else if (!fullName.equals(other.fullName))
            return false;
        if (gender == null) {
            if (other.gender != null)
                return false;
        } else if (!gender.equals(other.gender))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (invoiceDate == null) {
            if (other.invoiceDate != null)
                return false;
        } else if (!invoiceDate.equals(other.invoiceDate))
            return false;
        if (invoiceNumber == null) {
            if (other.invoiceNumber != null)
                return false;
        } else if (!invoiceNumber.equals(other.invoiceNumber))
            return false;
        if (invoiceType == null) {
            if (other.invoiceType != null)
                return false;
        } else if (!invoiceType.equals(other.invoiceType))
            return false;
        if (kycs == null) {
            if (other.kycs != null)
                return false;
        } else if (!kycs.equals(other.kycs))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        if (losId == null) {
            if (other.losId != null)
                return false;
        } else if (!losId.equals(other.losId))
            return false;
        if (losStatus == null) {
            if (other.losStatus != null)
                return false;
        } else if (!losStatus.equals(other.losStatus))
            return false;
        if (make == null) {
            if (other.make != null)
                return false;
        } else if (!make.equals(other.make))
            return false;
        if (middleName == null) {
            if (other.middleName != null)
                return false;
        } else if (!middleName.equals(other.middleName))
            return false;
        if (modelNo == null) {
            if (other.modelNo != null)
                return false;
        } else if (!modelNo.equals(other.modelNo))
            return false;
        if (pan == null) {
            if (other.pan != null)
                return false;
        } else if (!pan.equals(other.pan))
            return false;
        if (passport == null) {
            if (other.passport != null)
                return false;
        } else if (!passport.equals(other.passport))
            return false;
        if (postIpa == null) {
            if (other.postIpa != null)
                return false;
        } else if (!postIpa.equals(other.postIpa))
            return false;
        if (remark == null) {
            if (other.remark != null)
                return false;
        } else if (!remark.equals(other.remark))
            return false;
        if (utrNo == null) {
            if (other.utrNo != null)
                return false;
        } else if (!utrNo.equals(other.utrNo))
            return false;
        if (voterId == null) {
            if (other.voterId != null)
                return false;
        } else if (!voterId.equals(other.voterId))
            return false;
        return true;
    }

}