package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author prateek
 */

public class CustomReport {

    @JsonProperty("aTags")
    private List<Tag> availableTags;

    @JsonProperty("oHeader")
    private String reportFullHeader;

    @JsonProperty("sReportHeader")
    private String reportViewHeader;

    @JsonProperty("oData")
    private List<Object> reportData;

    @JsonProperty("sReportId")
    private String reportId;

    @JsonProperty("sReportName")
    private String reportName;

    /**
     * @return the availableTags
     */
    public List<Tag> getAvailableTags() {
        return availableTags;
    }

    /**
     * @param availableTags the availableTags to set
     */
    public void setAvailableTags(List<Tag> availableTags) {
        this.availableTags = availableTags;
    }

    /**
     * @return the reportFullHeader
     */
    public String getReportFullHeader() {
        return reportFullHeader;
    }

    /**
     * @param reportFullHeader the reportFullHeader to set
     */
    public void setReportFullHeader(String reportFullHeader) {
        this.reportFullHeader = reportFullHeader;
    }

    /**
     * @return the reportViewHeader
     */
    public String getReportViewHeader() {
        return reportViewHeader;
    }

    /**
     * @param reportViewHeader the reportViewHeader to set
     */
    public void setReportViewHeader(String reportViewHeader) {
        this.reportViewHeader = reportViewHeader;
    }

    public List<Object> getReportData() {
        return reportData;
    }

    public void setReportData(List<Object> reportData) {
        this.reportData = reportData;
    }

    /**
     * @return the reportId
     */
    public String getReportId() {
        return reportId;
    }

    /**
     * @param reportId the reportId to set
     */
    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    /**
     * @return the reportName
     */
    public String getReportName() {
        return reportName;
    }

    /**
     * @param reportName the reportName to set
     */
    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("CustomReport [availableTags=");
        builder.append(availableTags);
        builder.append(", reportFullHeader=");
        builder.append(reportFullHeader);
        builder.append(", reportViewHeader=");
        builder.append(reportViewHeader);
        builder.append(", reportData=");
        builder.append(reportData);
        builder.append(", reportId=");
        builder.append(reportId);
        builder.append(", reportName=");
        builder.append(reportName);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((availableTags == null) ? 0 : availableTags.hashCode());
        result = prime * result
                + ((reportData == null) ? 0 : reportData.hashCode());
        result = prime
                * result
                + ((reportFullHeader == null) ? 0 : reportFullHeader.hashCode());
        result = prime * result
                + ((reportId == null) ? 0 : reportId.hashCode());
        result = prime * result
                + ((reportName == null) ? 0 : reportName.hashCode());
        result = prime
                * result
                + ((reportViewHeader == null) ? 0 : reportViewHeader.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CustomReport other = (CustomReport) obj;
        if (availableTags == null) {
            if (other.availableTags != null)
                return false;
        } else if (!availableTags.equals(other.availableTags))
            return false;
        if (reportData == null) {
            if (other.reportData != null)
                return false;
        } else if (!reportData.equals(other.reportData))
            return false;
        if (reportFullHeader == null) {
            if (other.reportFullHeader != null)
                return false;
        } else if (!reportFullHeader.equals(other.reportFullHeader))
            return false;
        if (reportId == null) {
            if (other.reportId != null)
                return false;
        } else if (!reportId.equals(other.reportId))
            return false;
        if (reportName == null) {
            if (other.reportName != null)
                return false;
        } else if (!reportName.equals(other.reportName))
            return false;
        if (reportViewHeader == null) {
            if (other.reportViewHeader != null)
                return false;
        } else if (!reportViewHeader.equals(other.reportViewHeader))
            return false;
        return true;
    }

}
