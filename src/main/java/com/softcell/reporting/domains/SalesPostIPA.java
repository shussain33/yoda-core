package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.AuditEntity;
import com.softcell.gonogo.model.core.AssetDetails;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "postIPA")
public class SalesPostIPA extends AuditEntity {

    @JsonProperty("dApvAmt")
    private double approvedAmount;

    @JsonProperty("sScheme")
    private String scheme;

    @JsonProperty("sSchemeDscr")
    private String schemeDscr;

    @JsonProperty("dTotAssCost")
    private double totalAssetCost;

    @JsonProperty("dMarMoney")
    private double marginMoney;

    @JsonProperty("sMarginMoneyInstru")
    private String marginMoneyInstrument;

    @JsonProperty("sMarMoneyConfirm")
    private String marginMoneyConfirmation;

    @JsonProperty("dAdvEmi")
    private double advanceEmi;

    @JsonProperty("dProcFees")
    private double processingFees;

    @JsonProperty("aAssMdl")
    private List<String> assetModel;

    @JsonProperty("aAssMdls")
    private AssetDetails assetDetails;

    @JsonProperty("dManfSubDel")
    private double manufSubBorneByDealer;

    @JsonProperty("dDelSubven")
    private double dealerSubvention;

    @JsonProperty("dOtherChrg")
    private double otherChargesIfAny;

    @JsonProperty("dFinanceAmt")
    private double financeAmount;

    @JsonProperty("iTenor")
    private int tenor;

    @JsonProperty("iAdvEmiTenor")
    private int advanceEMITenor;

    @JsonProperty("dNetFundingAmt")
    private double netFundingAmount;

    @JsonProperty("dNetDisbursalAmt")
    private double netDisbursalAmount;

    @JsonProperty("dManSubMbd")
    private double manufSubventionMbd;

    @JsonProperty("dManProcFee")
    private double manufacturingProcessingFee;

    @JsonProperty("dEmi")
    private double emi;

    public double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getSchemeDscr() {
        return schemeDscr;
    }

    public void setSchemeDscr(String schemeDscr) {
        this.schemeDscr = schemeDscr;
    }

    public double getTotalAssetCost() {
        return totalAssetCost;
    }

    public void setTotalAssetCost(double totalAssetCost) {
        this.totalAssetCost = totalAssetCost;
    }

    public double getMarginMoney() {
        return marginMoney;
    }

    public void setMarginMoney(double marginMoney) {
        this.marginMoney = marginMoney;
    }

    public String getMarginMoneyInstrument() {
        return marginMoneyInstrument;
    }

    public void setMarginMoneyInstrument(String marginMoneyInstrument) {
        this.marginMoneyInstrument = marginMoneyInstrument;
    }

    public String getMarginMoneyConfirmation() {
        return marginMoneyConfirmation;
    }

    public void setMarginMoneyConfirmation(String marginMoneyConfirmation) {
        this.marginMoneyConfirmation = marginMoneyConfirmation;
    }

    public double getAdvanceEmi() {
        return advanceEmi;
    }

    public void setAdvanceEmi(double advanceEmi) {
        this.advanceEmi = advanceEmi;
    }

    public double getProcessingFees() {
        return processingFees;
    }

    public void setProcessingFees(double processingFees) {
        this.processingFees = processingFees;
    }

    public List<String> getAssetModel() {
        return assetModel;
    }

    public void setAssetModel(List<String> assetModel) {
        this.assetModel = assetModel;
    }

    public AssetDetails getAssetDetails() {
        return assetDetails;
    }

    public void setAssetDetails(AssetDetails assetDetails) {
        this.assetDetails = assetDetails;
    }

    public double getManufSubBorneByDealer() {
        return manufSubBorneByDealer;
    }

    public void setManufSubBorneByDealer(double manufSubBorneByDealer) {
        this.manufSubBorneByDealer = manufSubBorneByDealer;
    }

    public double getDealerSubvention() {
        return dealerSubvention;
    }

    public void setDealerSubvention(double dealerSubvention) {
        this.dealerSubvention = dealerSubvention;
    }

    public double getOtherChargesIfAny() {
        return otherChargesIfAny;
    }

    public void setOtherChargesIfAny(double otherChargesIfAny) {
        this.otherChargesIfAny = otherChargesIfAny;
    }

    public double getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(double financeAmount) {
        this.financeAmount = financeAmount;
    }

    public int getTenor() {
        return tenor;
    }

    public void setTenor(int tenor) {
        this.tenor = tenor;
    }

    public int getAdvanceEMITenor() {
        return advanceEMITenor;
    }

    public void setAdvanceEMITenor(int advanceEMITenor) {
        this.advanceEMITenor = advanceEMITenor;
    }

    public double getNetFundingAmount() {
        return netFundingAmount;
    }

    public void setNetFundingAmount(double netFundingAmount) {
        this.netFundingAmount = netFundingAmount;
    }

    public double getNetDisbursalAmount() {
        return netDisbursalAmount;
    }

    public void setNetDisbursalAmount(double netDisbursalAmount) {
        this.netDisbursalAmount = netDisbursalAmount;
    }

    public double getManufSubventionMbd() {
        return manufSubventionMbd;
    }

    public void setManufSubventionMbd(double manufSubventionMbd) {
        this.manufSubventionMbd = manufSubventionMbd;
    }

    public double getManufacturingProcessingFee() {
        return manufacturingProcessingFee;
    }

    public void setManufacturingProcessingFee(double manufacturingProcessingFee) {
        this.manufacturingProcessingFee = manufacturingProcessingFee;
    }

    public double getEmi() {
        return emi;
    }

    public void setEmi(double emi) {
        this.emi = emi;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("SalesPostIPA [approvedAmount=");
        builder.append(approvedAmount);
        builder.append(", scheme=");
        builder.append(scheme);
        builder.append(", schemeDscr=");
        builder.append(schemeDscr);
        builder.append(", totalAssetCost=");
        builder.append(totalAssetCost);
        builder.append(", marginMoney=");
        builder.append(marginMoney);
        builder.append(", marginMoneyInstrument=");
        builder.append(marginMoneyInstrument);
        builder.append(", marginMoneyConfirmation=");
        builder.append(marginMoneyConfirmation);
        builder.append(", advanceEmi=");
        builder.append(advanceEmi);
        builder.append(", processingFees=");
        builder.append(processingFees);
        builder.append(", assetModel=");
        builder.append(assetModel);
        builder.append(", assetDetails=");
        builder.append(assetDetails);
        builder.append(", manufSubBorneByDealer=");
        builder.append(manufSubBorneByDealer);
        builder.append(", dealerSubvention=");
        builder.append(dealerSubvention);
        builder.append(", otherChargesIfAny=");
        builder.append(otherChargesIfAny);
        builder.append(", financeAmount=");
        builder.append(financeAmount);
        builder.append(", tenor=");
        builder.append(tenor);
        builder.append(", advanceEMITenor=");
        builder.append(advanceEMITenor);
        builder.append(", netFundingAmount=");
        builder.append(netFundingAmount);
        builder.append(", netDisbursalAmount=");
        builder.append(netDisbursalAmount);
        builder.append(", manufSubventionMbd=");
        builder.append(manufSubventionMbd);
        builder.append(", manufacturingProcessingFee=");
        builder.append(manufacturingProcessingFee);
        builder.append(", emi=");
        builder.append(emi);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + advanceEMITenor;
        long temp;
        temp = Double.doubleToLongBits(advanceEmi);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(approvedAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result
                + ((assetDetails == null) ? 0 : assetDetails.hashCode());
        result = prime * result
                + ((assetModel == null) ? 0 : assetModel.hashCode());
        temp = Double.doubleToLongBits(dealerSubvention);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(emi);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(financeAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(manufSubBorneByDealer);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(manufSubventionMbd);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(manufacturingProcessingFee);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(marginMoney);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime
                * result
                + ((marginMoneyConfirmation == null) ? 0
                : marginMoneyConfirmation.hashCode());
        result = prime
                * result
                + ((marginMoneyInstrument == null) ? 0 : marginMoneyInstrument
                .hashCode());
        temp = Double.doubleToLongBits(netDisbursalAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(netFundingAmount);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(otherChargesIfAny);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(processingFees);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((scheme == null) ? 0 : scheme.hashCode());
        result = prime * result
                + ((schemeDscr == null) ? 0 : schemeDscr.hashCode());
        result = prime * result + tenor;
        temp = Double.doubleToLongBits(totalAssetCost);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SalesPostIPA other = (SalesPostIPA) obj;
        if (advanceEMITenor != other.advanceEMITenor)
            return false;
        if (Double.doubleToLongBits(advanceEmi) != Double
                .doubleToLongBits(other.advanceEmi))
            return false;
        if (Double.doubleToLongBits(approvedAmount) != Double
                .doubleToLongBits(other.approvedAmount))
            return false;
        if (assetDetails == null) {
            if (other.assetDetails != null)
                return false;
        } else if (!assetDetails.equals(other.assetDetails))
            return false;
        if (assetModel == null) {
            if (other.assetModel != null)
                return false;
        } else if (!assetModel.equals(other.assetModel))
            return false;
        if (Double.doubleToLongBits(dealerSubvention) != Double
                .doubleToLongBits(other.dealerSubvention))
            return false;
        if (Double.doubleToLongBits(emi) != Double.doubleToLongBits(other.emi))
            return false;
        if (Double.doubleToLongBits(financeAmount) != Double
                .doubleToLongBits(other.financeAmount))
            return false;
        if (Double.doubleToLongBits(manufSubBorneByDealer) != Double
                .doubleToLongBits(other.manufSubBorneByDealer))
            return false;
        if (Double.doubleToLongBits(manufSubventionMbd) != Double
                .doubleToLongBits(other.manufSubventionMbd))
            return false;
        if (Double.doubleToLongBits(manufacturingProcessingFee) != Double
                .doubleToLongBits(other.manufacturingProcessingFee))
            return false;
        if (Double.doubleToLongBits(marginMoney) != Double
                .doubleToLongBits(other.marginMoney))
            return false;
        if (marginMoneyConfirmation == null) {
            if (other.marginMoneyConfirmation != null)
                return false;
        } else if (!marginMoneyConfirmation
                .equals(other.marginMoneyConfirmation))
            return false;
        if (marginMoneyInstrument == null) {
            if (other.marginMoneyInstrument != null)
                return false;
        } else if (!marginMoneyInstrument.equals(other.marginMoneyInstrument))
            return false;
        if (Double.doubleToLongBits(netDisbursalAmount) != Double
                .doubleToLongBits(other.netDisbursalAmount))
            return false;
        if (Double.doubleToLongBits(netFundingAmount) != Double
                .doubleToLongBits(other.netFundingAmount))
            return false;
        if (Double.doubleToLongBits(otherChargesIfAny) != Double
                .doubleToLongBits(other.otherChargesIfAny))
            return false;
        if (Double.doubleToLongBits(processingFees) != Double
                .doubleToLongBits(other.processingFees))
            return false;
        if (scheme == null) {
            if (other.scheme != null)
                return false;
        } else if (!scheme.equals(other.scheme))
            return false;
        if (schemeDscr == null) {
            if (other.schemeDscr != null)
                return false;
        } else if (!schemeDscr.equals(other.schemeDscr))
            return false;
        if (tenor != other.tenor)
            return false;
        if (Double.doubleToLongBits(totalAssetCost) != Double
                .doubleToLongBits(other.totalAssetCost))
            return false;
        return true;
    }

}
