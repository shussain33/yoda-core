package com.softcell.reporting.domains;

/**
 * @author kishor
 */
public class ReportDetail {
    private String reportName;
    private String reportId;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ReportDetail [reportName=");
        builder.append(reportName);
        builder.append(", reportId=");
        builder.append(reportId);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((reportId == null) ? 0 : reportId.hashCode());
        result = prime * result
                + ((reportName == null) ? 0 : reportName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReportDetail other = (ReportDetail) obj;
        if (reportId == null) {
            if (other.reportId != null)
                return false;
        } else if (!reportId.equals(other.reportId))
            return false;
        if (reportName == null) {
            if (other.reportName != null)
                return false;
        } else if (!reportName.equals(other.reportName))
            return false;
        return true;
    }

}
