package com.softcell.reporting.domains;

/**
 * @author prateek
 *         Tag  is used to
 */
public class Tag {


    private String tagId;
    private String tagDescription;
    private String tagVisibility;
    private Boolean isFavorate;
    public Tag() {
        this.tagVisibility = TAG_VISIBILITY.PRIVATE.name();
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagDescription() {
        return tagDescription;
    }

    public void setTagDescription(String tagDescription) {
        this.tagDescription = tagDescription;
    }

    public String getTagVisibility() {
        return tagVisibility;
    }

    public void setTagVisibility(String tagVisibility) {
        this.tagVisibility = tagVisibility;
    }

    public Boolean getIsFavorate() {
        return isFavorate;
    }

    public void setIsFavorate(Boolean isFavorate) {
        this.isFavorate = isFavorate;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Tag [tagId=");
        builder.append(tagId);
        builder.append(", tagDescription=");
        builder.append(tagDescription);
        builder.append(", tagVisibility=");
        builder.append(tagVisibility);
        builder.append(", isFavorate=");
        builder.append(isFavorate);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((isFavorate == null) ? 0 : isFavorate.hashCode());
        result = prime * result
                + ((tagDescription == null) ? 0 : tagDescription.hashCode());
        result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
        result = prime * result
                + ((tagVisibility == null) ? 0 : tagVisibility.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tag other = (Tag) obj;
        if (isFavorate == null) {
            if (other.isFavorate != null)
                return false;
        } else if (!isFavorate.equals(other.isFavorate))
            return false;
        if (tagDescription == null) {
            if (other.tagDescription != null)
                return false;
        } else if (!tagDescription.equals(other.tagDescription))
            return false;
        if (tagId == null) {
            if (other.tagId != null)
                return false;
        } else if (!tagId.equals(other.tagId))
            return false;
        if (tagVisibility == null) {
            if (other.tagVisibility != null)
                return false;
        } else if (!tagVisibility.equals(other.tagVisibility))
            return false;
        return true;
    }

    public enum TAG_VISIBILITY {
        PUBLIC, PRIVATE;
    }

}
