package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author kishor Pagination is used to limit result in one server call from
 *         client. The default <em> limit</em> is 10. The<em> skip</em> is used
 *         for number of result to be skip from result set.<b> <limit> and
 *         <skip> are optional for first page or request .</b> <em> Page Id</em>
 *         is used to track the request and validate request this is optional.
 */
public class Pagination {
    @JsonProperty("iPageId")
    private int pageId;
    @JsonProperty("iLimit")
    private int limit = 10;
    @JsonProperty("iSkip")
    private int skip;

    /**
     * @return the pageId
     */
    public int getPageId() {
        return pageId;
    }

    /**
     * @param pageId the pageId to set
     */
    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * @return the skip
     */
    public int getSkip() {
        return skip;
    }

    /**
     * @param skip the skip to set
     */
    public void setSkip(int skip) {
        this.skip = skip;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Pagination [pageId=");
        builder.append(pageId);
        builder.append(", limit=");
        builder.append(limit);
        builder.append(", skip=");
        builder.append(skip);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + limit;
        result = prime * result + pageId;
        result = prime * result + skip;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pagination other = (Pagination) obj;
        if (limit != other.limit)
            return false;
        if (pageId != other.pageId)
            return false;
        if (skip != other.skip)
            return false;
        return true;
    }

}
