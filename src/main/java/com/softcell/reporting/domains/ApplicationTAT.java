/**
 *
 */
package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author prateek
 *         <p/>
 *         <p/>
 *         <p/>
 *         This class defines application report
 *         nested json in flat structure
 *         as per report
 */
public class ApplicationTAT implements Comparable<ApplicationTAT> {

    @JsonProperty("")
    private String institutionId;

    @JsonProperty("")
    private String userId;

    @JsonProperty("")
    private String password;

    @JsonProperty("")
    private String refID;

    @JsonProperty("")
    private String applicationId;

    @JsonProperty("")
    private String sourceId;

    @JsonProperty("")
    private String applicationSource;

    @JsonProperty("")
    private String requestType;

    @JsonProperty("")
    private String StringTime;

    @JsonProperty("")
    private String dsaId;

    @JsonProperty("")
    private String croId;

    @JsonProperty("")
    private String dealerId;

    @JsonProperty("")
    private String applicantId;

    @JsonProperty("")
    private String applicantFirstName;

    @JsonProperty("")
    private String applicantMiddleName;

    @JsonProperty("")
    private String applicantLastName;

    @JsonProperty("")
    private String applicantPrefix;

    @JsonProperty("")
    private String applicantSuffix;

    @JsonProperty("")
    private String applicantFatherFirstName;

    @JsonProperty("")
    private String applicantFatherMiddleName;

    @JsonProperty("")
    private String applicantFatherLastName;

    @JsonProperty("")
    private String applicantFatherPrefix;

    @JsonProperty("")
    private String applicantFatherSuffix;

    @JsonProperty("")
    private String applicantSpouseFirstName;

    @JsonProperty("")
    private String applicantSpouseMiddleName;

    @JsonProperty("")
    private String applicantSpouseLastName;

    @JsonProperty("")
    private String applicantSpousePrefix;

    @JsonProperty("")
    private String applicantSpouseSuffix;

    @JsonProperty("")
    private String applicantReligion;

    @JsonProperty("")
    private String applicantGender;

    @JsonProperty("")
    private String applicantStringOfBirth;

    @JsonProperty("")
    private Integer applicantAge;

    @JsonProperty("")
    private String applicantMaritalStatus;

    @JsonProperty("")
    private String kyc1kycName;

    @JsonProperty("")
    private String kyc1kycNumber;

    @JsonProperty("")
    private String kyc1kycStatus;

    @JsonProperty("")
    private String kyc1issueString;

    @JsonProperty("")
    private String kyc1expiryString;

    @JsonProperty("")
    private String kyc2kycName;

    @JsonProperty("")
    private String kyc2kycNumber;

    @JsonProperty("")
    private String kyc2kycStatus;

    @JsonProperty("")
    private String kyc2issueString;

    @JsonProperty("")
    private String kyc2expiryString;

    @JsonProperty("")
    private String kyc3kycName;

    @JsonProperty("")
    private String kyc3kycNumber;

    @JsonProperty("")
    private String kyc3kycStatus;

    @JsonProperty("")
    private String kyc3issueString;

    @JsonProperty("")
    private String kyc3expiryString;

    @JsonProperty("")
    private String kyc4kycName;

    @JsonProperty("")
    private String kyc4kycNumber;

    @JsonProperty("")
    private String kyc4kycStatus;

    @JsonProperty("")
    private String kyc4issueString;

    @JsonProperty("")
    private String kyc4expiryString;

    @JsonProperty("")
    private String kyc5kycName;

    @JsonProperty("")
    private String kyc5kycNumber;

    @JsonProperty("")
    private String kyc5kycStatus;

    @JsonProperty("")
    private String kyc5issueString;

    @JsonProperty("")
    private String kyc5expiryString;

    @JsonProperty("")
    private String add1accommodation;

    @JsonProperty("")
    private String add1timeAtAddress;

    @JsonProperty("")
    private String add1addressType;

    @JsonProperty("")
    private String add1residenceAddressType;

    @JsonProperty("")
    private Integer add1monthAtCity;

    @JsonProperty("")
    private Integer add1monthAtAddress;

    @JsonProperty("")
    private Double add1rentAmount;

    @JsonProperty("")
    private Integer add1yearAtCity;

    @JsonProperty("")
    private String add1addressLine1;

    @JsonProperty("")
    private String add1addressLine2;

    @JsonProperty("")
    private String add1city;

    @JsonProperty("")
    private Long add1pin;

    @JsonProperty("")
    private String add1state;

    @JsonProperty("")
    private String add1country;

    @JsonProperty("")
    private String add1landLoard;

    @JsonProperty("")
    private String add1line3;

    @JsonProperty("")
    private String add1line4;

    @JsonProperty("")
    private String add1village;

    @JsonProperty("")
    private String add1district;

    @JsonProperty("")
    private Float add1distanceFrom;

    @JsonProperty("")
    private String add1landMark;

    @JsonProperty("")
    private String add2accommodation;

    @JsonProperty("")
    private String add2timeAtAddress;

    @JsonProperty("")
    private String add2addressType;

    @JsonProperty("")
    private String add2residenceAddressType;

    @JsonProperty("")
    private Integer add2monthAtCity;

    @JsonProperty("")
    private Integer add2monthAtAddress;

    @JsonProperty("")
    private Double add2rentAmount;

    @JsonProperty("")
    private Integer add2yearAtCity;

    @JsonProperty("")
    private String add2addressLine1;

    @JsonProperty("")
    private String add2addressLine2;

    @JsonProperty("")
    private String add2city;

    @JsonProperty("")
    private Long add2pin;

    @JsonProperty("")
    private String add2state;

    @JsonProperty("")
    private String add2country;

    @JsonProperty("")
    private String add2landLoard;

    @JsonProperty("")
    private String add2line3;

    @JsonProperty("")
    private String add2line4;

    @JsonProperty("")
    private String add2village;

    @JsonProperty("")
    private String add2district;

    @JsonProperty("")
    private Float add2distanceFrom;

    @JsonProperty("")
    private String add2landMark;

    @JsonProperty("")
    private String add3accommodation;

    @JsonProperty("")
    private String add3timeAtAddress;

    @JsonProperty("")
    private String add3addressType;

    @JsonProperty("")
    private String add3residenceAddressType;

    @JsonProperty("")
    private Integer add3monthAtCity;

    @JsonProperty("")
    private Integer add3monthAtAddress;

    @JsonProperty("")
    private Double add3rentAmount;

    @JsonProperty("")
    private Integer add3yearAtCity;

    @JsonProperty("")
    private String add3addressLine1;

    @JsonProperty("")
    private String add3addressLine2;

    @JsonProperty("")
    private String add3city;

    @JsonProperty("")
    private Long add3pin;

    @JsonProperty("")
    private String add3state;

    @JsonProperty("")
    private String add3country;

    @JsonProperty("")
    private String add3landLoard;

    @JsonProperty("")
    private String add3line3;

    @JsonProperty("")
    private String add3line4;

    @JsonProperty("")
    private String add3village;

    @JsonProperty("")
    private String add3district;

    @JsonProperty("")
    private Float add3distanceFrom;

    @JsonProperty("")
    private String add3landMark;

    @JsonProperty("")
    private String add4accommodation;

    @JsonProperty("")
    private String add4timeAtAddress;

    @JsonProperty("")
    private String add4addressType;

    @JsonProperty("")
    private String add4residenceAddressType;

    @JsonProperty("")
    private Integer add4monthAtCity;

    @JsonProperty("")
    private Integer add4monthAtAddress;

    @JsonProperty("")
    private Double add4rentAmount;

    @JsonProperty("")
    private Integer add4yearAtCity;

    @JsonProperty("")
    private String add4addressLine1;

    @JsonProperty("")
    private String add4addressLine2;

    @JsonProperty("")
    private String add4city;

    @JsonProperty("")
    private Long add4pin;

    @JsonProperty("")
    private String add4state;

    @JsonProperty("")
    private String add4country;

    @JsonProperty("")
    private String add4landLoard;

    @JsonProperty("")
    private String add4line3;

    @JsonProperty("")
    private String add4line4;

    @JsonProperty("")
    private String add4village;

    @JsonProperty("")
    private String add4district;

    @JsonProperty("")
    private Float add4distanceFrom;

    @JsonProperty("")
    private String add4landMark;

    @JsonProperty("")
    private String add5accommodation;

    @JsonProperty("")
    private String add5timeAtAddress;

    @JsonProperty("")
    private String add5addressType;

    @JsonProperty("")
    private String add5residenceAddressType;

    @JsonProperty("")
    private Integer add5monthAtCity;

    @JsonProperty("")
    private Integer add5monthAtAddress;

    @JsonProperty("")
    private Double add5rentAmount;

    @JsonProperty("")
    private Integer add5yearAtCity;

    @JsonProperty("")
    private Long add5pin;

    @JsonProperty("")
    private String add5state;

    @JsonProperty("")
    private String add5country;

    @JsonProperty("")
    private String add5landLoard;

    @JsonProperty("")
    private String add5line3;

    @JsonProperty("")
    private String add5line4;

    @JsonProperty("")
    private String add5village;

    @JsonProperty("")
    private String add5district;

    @JsonProperty("")
    private Float add5distanceFrom;

    @JsonProperty("")
    private String add5landMark;

    @JsonProperty("")
    private String phone1phoneType;

    @JsonProperty("")
    private String phone1countryCode;

    @JsonProperty("")
    private String phone1phoneNumber;

    @JsonProperty("")
    private String phone1extension;

    @JsonProperty("")
    private String phone2phoneType;

    @JsonProperty("")
    private String phone2countryCode;

    @JsonProperty("")
    private String phone2phoneNumber;

    @JsonProperty("")
    private String phone2extension;

    @JsonProperty("")
    private String phone3phoneType;

    @JsonProperty("")
    private String phone3countryCode;

    @JsonProperty("")
    private String phone3phoneNumber;

    @JsonProperty("")
    private String phone3extension;

    @JsonProperty("")
    private String phone4phoneType;

    @JsonProperty("")
    private String phone4countryCode;

    @JsonProperty("")
    private String phone4phoneNumber;

    @JsonProperty("")
    private String phone4extension;

    @JsonProperty("")
    private String phone5phoneType;

    @JsonProperty("")
    private String phone5countryCode;

    @JsonProperty("")
    private String phone5phoneNumber;

    @JsonProperty("")
    private String phone5extension;

    @JsonProperty("")
    private String email1emailType;

    @JsonProperty("")
    private String email1emailAddress;

    @JsonProperty("")
    private String email2emailType;

    @JsonProperty("")
    private String email2emailAddress;

    @JsonProperty("")
    private String email3emailType;

    @JsonProperty("")
    private String email3emailAddress;

    @JsonProperty("")
    private String email4emailType;

    @JsonProperty("")
    private String email4emailAddress;

    @JsonProperty("")
    private String email5emailType;

    @JsonProperty("")
    private String email5emailAddress;

    @JsonProperty("")
    private String emp1employmentType;

    @JsonProperty("")
    private String emp1employmentName;

    @JsonProperty("")
    private Integer emp1timeWithEmployer;

    @JsonProperty("")
    private String emp1StringOfJoining;

    @JsonProperty("")
    private String emp1StringOfLeaving;

    @JsonProperty("")
    private Double emp1monthlySalary;

    @JsonProperty("")
    private Double emp1grossSalary;

    @JsonProperty("")
    private String emp1constitution;

    @JsonProperty("")
    private String emp1industType;

    @JsonProperty("")
    private String emp1itrId;

    @JsonProperty("")
    private Double emp1itrAmount;

    @JsonProperty("")
    private String emp1designation;

    @JsonProperty("")
    private String emp1employerCode;

    @JsonProperty("")
    private String emp1employerBranch;

    @JsonProperty("")
    private String emp1modePayment;

    @JsonProperty("")
    private String emp1department;

    @JsonProperty("")
    private String emp1workExps;

    @JsonProperty("")
    private String emp1businessName;

    @JsonProperty("")
    private String emp1commencementString;

    @JsonProperty("")
    private String emp1industryType;

    @JsonProperty("")
    private String emp1LastMonthIncomeMonth1;

    @JsonProperty("")
    private String emp1LastMonthIncomeIncome1;

    @JsonProperty("")
    private String emp1LastMonthIncomeMonth2;

    @JsonProperty("")
    private String emp1LastMonthIncomeIncome2;

    @JsonProperty("")
    private String emp1LastMonthIncomeMonth3;

    @JsonProperty("")
    private String emp1LastMonthIncomeIncome3;

    @JsonProperty("")
    private String emp1LastMonthIncomeMonth4;

    @JsonProperty("")
    private String emp1LastMonthIncomeIncome4;

    @JsonProperty("")
    private String emp1LastMonthIncomeMonth5;

    @JsonProperty("")
    private String emp1LastMonthIncomeIncome5;

    @JsonProperty("")
    private String emp2employmentType;

    @JsonProperty("")
    private String emp2employmentName;

    @JsonProperty("")
    private Integer emp2timeWithEmployer;

    @JsonProperty("")
    private String emp2StringOfJoining;

    @JsonProperty("")
    private String emp2StringOfLeaving;

    @JsonProperty("")
    private Double emp2monthlySalary;

    @JsonProperty("")
    private Double emp2grossSalary;

    @JsonProperty("")
    private String emp2constitution;

    @JsonProperty("")
    private String emp2industType;

    @JsonProperty("")
    private String emp2itrId;

    @JsonProperty("")
    private Double emp2itrAmount;

    @JsonProperty("")
    private String emp2designation;

    @JsonProperty("")
    private String emp2employerCode;

    @JsonProperty("")
    private String emp2employerBranch;

    @JsonProperty("")
    private String emp2modePayment;

    @JsonProperty("")
    private String emp2department;

    @JsonProperty("")
    private String emp2workExps;

    @JsonProperty("")
    private String emp2businessName;

    @JsonProperty("")
    private String emp2commencementString;

    @JsonProperty("")
    private String emp2industryType;

    @JsonProperty("")
    private String emp2LastMonthIncomeMonth1;

    @JsonProperty("")
    private String emp2LastMonthIncomeIncome1;

    @JsonProperty("")
    private String emp2LastMonthIncomeMonth2;

    @JsonProperty("")
    private String emp2LastMonthIncomeIncome2;

    @JsonProperty("")
    private String emp2LastMonthIncomeMonth3;

    @JsonProperty("")
    private String emp2LastMonthIncomeIncome3;

    @JsonProperty("")
    private String emp2LastMonthIncomeMonth4;

    @JsonProperty("")
    private String emp2LastMonthIncomeIncome4;

    @JsonProperty("")
    private String emp2LastMonthIncomeMonth5;

    @JsonProperty("")
    private String emp2LastMonthIncomeIncome5;

    @JsonProperty("")
    private String emp3employmentType;

    @JsonProperty("")
    private String emp3employmentName;

    @JsonProperty("")
    private Integer emp3timeWithEmployer;

    @JsonProperty("")
    private String emp3StringOfJoining;

    @JsonProperty("")
    private String emp3StringOfLeaving;

    @JsonProperty("")
    private Double emp3monthlySalary;

    @JsonProperty("")
    private Double emp3grossSalary;

    @JsonProperty("")
    private String emp3constitution;

    @JsonProperty("")
    private String emp3industType;

    @JsonProperty("")
    private String emp3itrId;

    @JsonProperty("")
    private Double emp3itrAmount;

    @JsonProperty("")
    private String emp3designation;

    @JsonProperty("")
    private String emp3employerCode;

    @JsonProperty("")
    private String emp3employerBranch;

    @JsonProperty("")
    private String emp3modePayment;

    @JsonProperty("")
    private String emp3department;

    @JsonProperty("")
    private String emp3workExps;

    @JsonProperty("")
    private String emp3businessName;

    @JsonProperty("")
    private String emp3commencementString;

    @JsonProperty("")
    private String emp3industryType;

    @JsonProperty("")
    private String emp3LastMonthIncomeMonth1;

    @JsonProperty("")
    private String emp3LastMonthIncomeIncome1;

    @JsonProperty("")
    private String emp3LastMonthIncomeMonth2;

    @JsonProperty("")
    private String emp3LastMonthIncomeIncome2;

    @JsonProperty("")
    private String emp3LastMonthIncomeMonth3;

    @JsonProperty("")
    private String emp3LastMonthIncomeIncome3;

    @JsonProperty("")
    private String emp3LastMonthIncomeMonth4;

    @JsonProperty("")
    private String emp3LastMonthIncomeIncome4;

    @JsonProperty("")
    private String emp3LastMonthIncomeMonth5;

    @JsonProperty("")
    private String emp3LastMonthIncomeIncome5;

    @JsonProperty("")
    private String emp4employmentType;

    @JsonProperty("")
    private String emp4employmentName;

    @JsonProperty("")
    private Integer emp4timeWithEmployer;

    @JsonProperty("")
    private String emp4StringOfJoining;

    @JsonProperty("")
    private String emp4StringOfLeaving;

    @JsonProperty("")
    private Double emp4monthlySalary;

    @JsonProperty("")
    private Double emp4grossSalary;

    @JsonProperty("")
    private String emp4constitution;

    @JsonProperty("")
    private String emp4industType;

    @JsonProperty("")
    private String emp4itrId;

    @JsonProperty("")
    private Double emp4itrAmount;

    @JsonProperty("")
    private String emp4designation;

    @JsonProperty("")
    private String emp4employerCode;

    @JsonProperty("")
    private String emp4employerBranch;

    @JsonProperty("")
    private String emp4modePayment;

    @JsonProperty("")
    private String emp4department;

    @JsonProperty("")
    private String emp4workExps;

    @JsonProperty("")
    private String emp4businessName;

    @JsonProperty("")
    private String emp4commencementString;

    @JsonProperty("")
    private String emp4industryType;

    @JsonProperty("")
    private String emp4LastMonthIncomeMonth1;

    @JsonProperty("")
    private String emp4LastMonthIncomeIncome1;

    @JsonProperty("")
    private String emp4LastMonthIncomeMonth2;

    @JsonProperty("")
    private String emp4LastMonthIncomeIncome2;

    @JsonProperty("")
    private String emp4LastMonthIncomeMonth3;

    @JsonProperty("")
    private String emp4LastMonthIncomeIncome3;

    @JsonProperty("")
    private String emp4LastMonthIncomeMonth4;

    @JsonProperty("")
    private String emp4LastMonthIncomeIncome4;

    @JsonProperty("")
    private String emp4LastMonthIncomeMonth5;

    @JsonProperty("")
    private String emp4LastMonthIncomeIncome5;

    @JsonProperty("")
    private String emp5employmentType;

    @JsonProperty("")
    private String emp5employmentName;

    @JsonProperty("")
    private Integer emp5timeWithEmployer;

    @JsonProperty("")
    private String emp5StringOfJoining;

    @JsonProperty("")
    private String emp5StringOfLeaving;

    @JsonProperty("")
    private Double emp5monthlySalary;

    @JsonProperty("")
    private Double emp5grossSalary;

    @JsonProperty("")
    private String emp5constitution;

    @JsonProperty("")
    private String emp5industType;

    @JsonProperty("")
    private String emp5itrId;

    @JsonProperty("")
    private Double emp5itrAmount;

    @JsonProperty("")
    private String emp5designation;

    @JsonProperty("")
    private String emp5employerCode;

    @JsonProperty("")
    private String emp5employerBranch;

    @JsonProperty("")
    private String emp5modePayment;

    @JsonProperty("")
    private String emp5department;

    @JsonProperty("")
    private String emp5workExps;

    @JsonProperty("")
    private String emp5businessName;

    @JsonProperty("")
    private String emp5commencementString;

    @JsonProperty("")
    private String emp5industryType;

    @JsonProperty("")
    private String emp5LastMonthIncomeMonth1;

    @JsonProperty("")
    private String emp5LastMonthIncomeIncome1;

    @JsonProperty("")
    private String emp5LastMonthIncomeMonth2;

    @JsonProperty("")
    private String emp5LastMonthIncomeIncome2;

    @JsonProperty("")
    private String emp5LastMonthIncomeMonth3;

    @JsonProperty("")
    private String emp5LastMonthIncomeIncome3;

    @JsonProperty("")
    private String emp5LastMonthIncomeMonth4;

    @JsonProperty("")
    private String emp5LastMonthIncomeIncome4;

    @JsonProperty("")
    private String emp5LastMonthIncomeMonth5;

    @JsonProperty("")
    private String emp5LastMonthIncomeIncome5;

    @JsonProperty("")
    private Integer noOfDependents;

    @JsonProperty("")
    private Integer noOfEarningMembers;

    @JsonProperty("")
    private Integer noOfFamilyMembers;

    @JsonProperty("")
    private String applicantReferenceFirstName;

    @JsonProperty("")
    private String applicantReferencemiddleName;

    @JsonProperty("")
    private String applicantReferencelastName;

    @JsonProperty("")
    private String applicantReferenceprefix;

    @JsonProperty("")
    private String applicantReferencesuffix;

    @JsonProperty("")
    private String applicantReferenceaddressLine1;

    @JsonProperty("")
    private String applicantReferenceaddressLine2;

    @JsonProperty("")
    private String applicantReferencecity;

    @JsonProperty("")
    private Long applicantReferencepin;

    @JsonProperty("")
    private String applicantReferencestate;

    @JsonProperty("")
    private String applicantReferencecountry;

    @JsonProperty("")
    private String applicantReferencelandLoard;

    @JsonProperty("")
    private String applicantReferenceline3;

    @JsonProperty("")
    private String applicantReferenceline4;

    @JsonProperty("")
    private String applicantReferencevillage;

    @JsonProperty("")
    private String applicantReferencedistrict;

    @JsonProperty("")
    private Float applicantReferencedistanceFrom;

    @JsonProperty("")
    private String applicantReferencelandMark;

    @JsonProperty("")
    private String applicantReferencerelationType;

    @JsonProperty("")
    private String applicantReferencephoneType;

    @JsonProperty("")
    private String applicantReferenceareaCode;

    @JsonProperty("")
    private String applicantReferencecountryCode;

    @JsonProperty("")
    private String applicantReferencephoneNumber;

    @JsonProperty("")
    private String applicantReferenceextension;

    @JsonProperty("")
    private String applicantReferenceoccupation;

    @JsonProperty("")
    private String education;

    @JsonProperty("")
    private String creditCardNumber;

    @JsonProperty("")
    private Boolean mobileVerified;

    @JsonProperty("")
    private Boolean addharVerified;

    @JsonProperty("")
    private String accountHolderbankName1;

    @JsonProperty("")
    private String accountHolderbranchName1;

    @JsonProperty("")
    private String accountHolderaccountType1;

    @JsonProperty("")
    private String accountHolderaccountNumber1;

    @JsonProperty("")
    private Boolean accountHoldersalaryAccount1;

    @JsonProperty("")
    private String accountHolderinwardChequeReturn1;

    @JsonProperty("")
    private String accountHolderoutwardChequeReturn1;

    @JsonProperty("")
    private Double accountHolderavgBankBalance1;

    @JsonProperty("")
    private Double accountHolderdeductedEmiAmt1;

    @JsonProperty("")
    private String accountHolderanyEmi1;

    @JsonProperty("")
    private String accountHoldercurrentlyRunningLoan1;

    @JsonProperty("")
    private Double accountHoldermentionAmount1;

    @JsonProperty("")
    private String accountHolderfirstName1;

    @JsonProperty("")
    private String accountHoldermiddleName1;

    @JsonProperty("")
    private String accountHolderlastName1;

    @JsonProperty("")
    private String accountHolderprefix1;

    @JsonProperty("")
    private String accountHoldersuffix1;

    @JsonProperty("")
    private String accountHolderbankName2;

    @JsonProperty("")
    private String accountHolderbranchName2;

    @JsonProperty("")
    private String accountHolderaccountType2;

    @JsonProperty("")
    private String accountHolderaccountNumber2;

    @JsonProperty("")
    private Boolean accountHoldersalaryAccount2;

    @JsonProperty("")
    private String accountHolderinwardChequeReturn2;

    @JsonProperty("")
    private String accountHolderoutwardChequeReturn2;

    @JsonProperty("")
    private Double accountHolderavgBankBalance2;

    @JsonProperty("")
    private Double accountHolderdeductedEmiAmt2;

    @JsonProperty("")
    private String accountHolderanyEmi2;

    @JsonProperty("")
    private String accountHoldercurrentlyRunningLoan2;

    @JsonProperty("")
    private Double accountHoldermentionAmount2;

    @JsonProperty("")
    private String accountHolderfirstName2;

    @JsonProperty("")
    private String accountHoldermiddleName2;

    @JsonProperty("")
    private String accountHolderlastName2;

    @JsonProperty("")
    private String accountHolderprefix2;

    @JsonProperty("")
    private String accountHoldersuffix2;

    @JsonProperty("")
    private String accountHolderbankName3;

    @JsonProperty("")
    private String accountHolderbranchName3;

    @JsonProperty("")
    private String accountHolderaccountType3;

    @JsonProperty("")
    private String accountHolderaccountNumber3;

    @JsonProperty("")
    private Boolean accountHoldersalaryAccount3;

    @JsonProperty("")
    private String accountHolderinwardChequeReturn3;

    @JsonProperty("")
    private String accountHolderoutwardChequeReturn3;

    @JsonProperty("")
    private Double accountHolderavgBankBalance3;

    @JsonProperty("")
    private Double accountHolderdeductedEmiAmt3;

    @JsonProperty("")
    private String accountHolderanyEmi3;

    @JsonProperty("")
    private String accountHoldercurrentlyRunningLoan3;

    @JsonProperty("")
    private Double accountHoldermentionAmount3;

    @JsonProperty("")
    private String accountHolderfirstName3;

    @JsonProperty("")
    private String accountHoldermiddleName3;

    @JsonProperty("")
    private String accountHolderlastName3;

    @JsonProperty("")
    private String accountHolderprefix3;

    @JsonProperty("")
    private String accountHoldersuffix3;

    @JsonProperty("")
    private String accountHolderbankName4;

    @JsonProperty("")
    private String accountHolderbranchName4;

    @JsonProperty("")
    private String accountHolderaccountType4;

    @JsonProperty("")
    private String accountHolderaccountNumber4;

    @JsonProperty("")
    private Boolean accountHoldersalaryAccount4;

    @JsonProperty("")
    private String accountHolderinwardChequeReturn4;

    @JsonProperty("")
    private String accountHolderoutwardChequeReturn4;

    @JsonProperty("")
    private Double accountHolderavgBankBalance4;

    @JsonProperty("")
    private Double accountHolderdeductedEmiAmt4;

    @JsonProperty("")
    private String accountHolderanyEmi4;

    @JsonProperty("")
    private String accountHoldercurrentlyRunningLoan4;

    @JsonProperty("")
    private Double accountHoldermentionAmount4;

    @JsonProperty("")
    private String accountHolderfirstName4;

    @JsonProperty("")
    private String accountHoldermiddleName4;

    @JsonProperty("")
    private String accountHolderlastName4;

    @JsonProperty("")
    private String accountHolderprefix4;

    @JsonProperty("")
    private String accountHoldersuffix4;

    @JsonProperty("")
    private String accountHolderbankName5;

    @JsonProperty("")
    private String accountHolderbranchName5;

    @JsonProperty("")
    private String accountHolderaccountType5;

    @JsonProperty("")
    private String accountHolderaccountNumber5;

    @JsonProperty("")
    private Boolean accountHoldersalaryAccount5;

    @JsonProperty("")
    private String accountHolderinwardChequeReturn5;

    @JsonProperty("")
    private String accountHolderoutwardChequeReturn5;

    @JsonProperty("")
    private Double accountHolderavgBankBalance5;

    @JsonProperty("")
    private Double accountHolderdeductedEmiAmt5;

    @JsonProperty("")
    private String accountHolderanyEmi5;

    @JsonProperty("")
    private String accountHoldercurrentlyRunningLoan5;

    @JsonProperty("")
    private Double accountHoldermentionAmount5;

    @JsonProperty("")
    private String accountHolderfirstName5;

    @JsonProperty("")
    private String accountHoldermiddleName5;

    @JsonProperty("")
    private String accountHolderlastName5;

    @JsonProperty("")
    private String accountHolderprefix5;

    @JsonProperty("")
    private String accountHoldersuffix5;

    @JsonProperty("")
    private String loanOwnership1;

    @JsonProperty("")
    private String loanType1;

    @JsonProperty("")
    private String loanPurpose1;

    @JsonProperty("")
    private String loanAcountNo1;

    @JsonProperty("")
    private String creditGranter1;

    @JsonProperty("")
    private Double loanAmt1;

    @JsonProperty("")
    private Double emiAmt1;

    @JsonProperty("")
    private Integer mob1;

    @JsonProperty("")
    private Integer balanceTenure1;

    @JsonProperty("")
    private String repaymentBankName1;

    @JsonProperty("")
    private String obligate1;

    @JsonProperty("")
    private Double loanApr1;

    @JsonProperty("")
    private String loanOwnership2;

    @JsonProperty("")
    private String loanType2;

    @JsonProperty("")
    private String loanPurpose2;

    @JsonProperty("")
    private String loanAcountNo2;

    @JsonProperty("")
    private String creditGranter2;

    @JsonProperty("")
    private Double loanAmt2;

    @JsonProperty("")
    private Double emiAmt2;

    @JsonProperty("")
    private Integer mob2;

    @JsonProperty("")
    private Integer balanceTenure2;

    @JsonProperty("")
    private String repaymentBankName2;

    @JsonProperty("")
    private String obligate2;

    @JsonProperty("")
    private Double loanApr2;

    @JsonProperty("")
    private String loanOwnership3;

    @JsonProperty("")
    private String loanType3;

    @JsonProperty("")
    private String loanPurpose3;

    @JsonProperty("")
    private String loanAcountNo3;

    @JsonProperty("")
    private String creditGranter3;

    @JsonProperty("")
    private Double loanAmt3;

    @JsonProperty("")
    private Double emiAmt3;

    @JsonProperty("")
    private Integer mob3;

    @JsonProperty("")
    private Integer balanceTenure3;

    @JsonProperty("")
    private String repaymentBankName3;

    @JsonProperty("")
    private String obligate3;

    @JsonProperty("")
    private Double loanApr3;

    @JsonProperty("")
    private String loanOwnership4;

    @JsonProperty("")
    private String loanType4;

    @JsonProperty("")
    private String loanPurpose4;

    @JsonProperty("")
    private String loanAcountNo4;

    @JsonProperty("")
    private String creditGranter4;

    @JsonProperty("")
    private Double loanAmt4;

    @JsonProperty("")
    private Double emiAmt4;

    @JsonProperty("")
    private Integer mob4;

    @JsonProperty("")
    private Integer balanceTenure4;

    @JsonProperty("")
    private String repaymentBankName4;

    @JsonProperty("")
    private String obligate4;

    @JsonProperty("")
    private Double loanApr4;

    @JsonProperty("")
    private String loanOwnership5;

    @JsonProperty("")
    private String loanType5;

    @JsonProperty("")
    private String loanPurpose5;

    @JsonProperty("")
    private String loanAcountNo5;

    @JsonProperty("")
    private String creditGranter5;

    @JsonProperty("")
    private Double loanAmt5;

    @JsonProperty("")
    private Double emiAmt5;

    @JsonProperty("")
    private Integer mob5;

    @JsonProperty("")
    private Integer balanceTenure5;

    @JsonProperty("")
    private String repaymentBankName5;

    @JsonProperty("")
    private String obligate5;

    @JsonProperty("")
    private Double loanApr5;

    @JsonProperty("")
    private String incomeDocumentAvailable;

    @JsonProperty("")
    private String otherSourceOfIncome1;

    @JsonProperty("")
    private String otherSourceOfIncome2;

    @JsonProperty("")
    private String otherSourceOfIncome3;

    @JsonProperty("")
    private String otherSourceOfIncome4;

    @JsonProperty("")
    private String otherSourceOfIncome5;

    @JsonProperty("")
    private Double otherSourceIncomeAmount;

    @JsonProperty("")
    private Double basicIncome;

    @JsonProperty("")
    private Double dearnessAllowance;

    @JsonProperty("")
    private Double houseRentAllowance;

    @JsonProperty("")
    private Double cityCompensatoryAllowance;

    @JsonProperty("")
    private Double otherAllowance;

    @JsonProperty("")
    private Double otherSourcesIncome;

    @JsonProperty("")
    private Double grossIncome;

    @JsonProperty("")
    private Double netIncome;

    @JsonProperty("")
    private Double basic;

    @JsonProperty("")
    private Double DA;

    @JsonProperty("")
    private Double HRA;

    @JsonProperty("")
    private Double CCA;

    @JsonProperty("")
    private Double incentive;

    @JsonProperty("")
    private Double other;

    @JsonProperty("")
    private Double otherSourceIncome;

    @JsonProperty("")
    private Double PF;

    @JsonProperty("")
    private Double professionalTax;

    @JsonProperty("")
    private Double LIC;

    @JsonProperty("")
    private Double ESI;

    @JsonProperty("")
    private String lastMonthIncomeMonth1;

    @JsonProperty("")
    private Double lastMonthIncomeMonthIncome1;

    @JsonProperty("")
    private String lastMonthIncomeMonth2;

    @JsonProperty("")
    private Double lastMonthIncomeMonthIncome2;

    @JsonProperty("")
    private String lastMonthIncomeMonth3;

    @JsonProperty("")
    private Double lastMonthIncomeMonthIncome3;

    @JsonProperty("")
    private String lastMonthIncomeMonth4;

    @JsonProperty("")
    private Double lastMonthIncomeMonthIncome4;

    @JsonProperty("")
    private String lastMonthIncomeMonth5;

    @JsonProperty("")
    private Double lastMonthIncomeMonthIncome5;

    @JsonProperty("")
    private Double providentFund;

    @JsonProperty("")
    private Double stateInsurance;

    @JsonProperty("")
    private Double otherDeductions;

    @JsonProperty("")
    private Double lastTwoMonthSalary;

    @JsonProperty("")
    private Double obligations;

    @JsonProperty("")
    private String remarks;

    @JsonProperty("")
    private String year1;

    @JsonProperty("")
    private String itrString1;

    @JsonProperty("")
    private Double turnOver1;

    @JsonProperty("")
    private Double depreciation1;

    @JsonProperty("")
    private Double netProfit1;

    @JsonProperty("")
    private Double otherIncome1;

    @JsonProperty("")
    private String year2;

    @JsonProperty("")
    private String itrString2;

    @JsonProperty("")
    private Double turnOver2;

    @JsonProperty("")
    private Double depreciation2;

    @JsonProperty("")
    private Double netProfit2;

    @JsonProperty("")
    private Double otherIncome2;

    @JsonProperty("")
    private String year3;

    @JsonProperty("")
    private String itrString3;

    @JsonProperty("")
    private Double turnOver3;

    @JsonProperty("")
    private Double depreciation3;

    @JsonProperty("")
    private Double netProfit3;

    @JsonProperty("")
    private Double otherIncome3;

    @JsonProperty("")
    private String year4;

    @JsonProperty("")
    private String itrString4;

    @JsonProperty("")
    private Double turnOver4;

    @JsonProperty("")
    private Double depreciation4;

    @JsonProperty("")
    private Double netProfit4;

    @JsonProperty("")
    private Double otherIncome4;

    @JsonProperty("")
    private String year5;

    @JsonProperty("")
    private String itrString5;

    @JsonProperty("")
    private Double turnOver5;

    @JsonProperty("")
    private Double depreciation5;

    @JsonProperty("")
    private Double netProfit5;

    @JsonProperty("")
    private Double otherIncome5;

    @JsonProperty("")
    private String carSurrogateModelName1;

    @JsonProperty("")
    private String carSurrogateManufactureYear1;

    @JsonProperty("")
    private String carSurrogateRegistrationNumber1;

    @JsonProperty("")
    private String carSurrogateModelName2;

    @JsonProperty("")
    private String carSurrogateManufactureYear2;

    @JsonProperty("")
    private String carSurrogateRegistrationNumber2;

    @JsonProperty("")
    private String carSurrogateModelName3;

    @JsonProperty("")
    private String carSurrogateManufactureYear3;

    @JsonProperty("")
    private String carSurrogateRegistrationNumber3;

    @JsonProperty("")
    private String carSurrogateModelName4;

    @JsonProperty("")
    private String carSurrogateManufactureYear4;

    @JsonProperty("")
    private String carSurrogateRegistrationNumber4;

    @JsonProperty("")
    private String carSurrogateModelName5;

    @JsonProperty("")
    private String carSurrogateManufactureYear5;

    @JsonProperty("")
    private String carSurrogateRegistrationNumber5;

    @JsonProperty("")
    private Double salariedSurrogateNetTakeHome1;

    @JsonProperty("")
    private Double salariedSurrogateNetTakeHome2;

    @JsonProperty("")
    private Double salariedSurrogateNetTakeHome3;

    @JsonProperty("")
    private Double salariedSurrogateNetTakeHome4;

    @JsonProperty("")
    private Double salariedSurrogateNetTakeHome5;

    @JsonProperty("")
    private Integer traderSurrogateYearInBussines1;

    @JsonProperty("")
    private String traderSurrogateBussinesProof1;

    @JsonProperty("")
    private Integer traderSurrogateYearInBussines2;

    @JsonProperty("")
    private String traderSurrogateBussinesProof2;

    @JsonProperty("")
    private Integer traderSurrogateYearInBussines3;

    @JsonProperty("")
    private String traderSurrogateBussinesProof3;

    @JsonProperty("")
    private Integer traderSurrogateYearInBussines4;

    @JsonProperty("")
    private String traderSurrogateBussinesProof4;

    @JsonProperty("")
    private Integer traderSurrogateYearInBussines5;

    @JsonProperty("")
    private String traderSurrogateBussinesProof5;


    public ApplicationTAT() {
        // TODO Auto-generated constructor stub
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((CCA == null) ? 0 : CCA.hashCode());
        result = prime * result + ((DA == null) ? 0 : DA.hashCode());
        result = prime * result + ((ESI == null) ? 0 : ESI.hashCode());
        result = prime * result + ((HRA == null) ? 0 : HRA.hashCode());
        result = prime * result + ((LIC == null) ? 0 : LIC.hashCode());
        result = prime * result + ((PF == null) ? 0 : PF.hashCode());
        result = prime * result
                + ((StringTime == null) ? 0 : StringTime.hashCode());
        result = prime
                * result
                + ((accountHolderaccountNumber1 == null) ? 0
                : accountHolderaccountNumber1.hashCode());
        result = prime
                * result
                + ((accountHolderaccountNumber2 == null) ? 0
                : accountHolderaccountNumber2.hashCode());
        result = prime
                * result
                + ((accountHolderaccountNumber3 == null) ? 0
                : accountHolderaccountNumber3.hashCode());
        result = prime
                * result
                + ((accountHolderaccountNumber4 == null) ? 0
                : accountHolderaccountNumber4.hashCode());
        result = prime
                * result
                + ((accountHolderaccountNumber5 == null) ? 0
                : accountHolderaccountNumber5.hashCode());
        result = prime
                * result
                + ((accountHolderaccountType1 == null) ? 0
                : accountHolderaccountType1.hashCode());
        result = prime
                * result
                + ((accountHolderaccountType2 == null) ? 0
                : accountHolderaccountType2.hashCode());
        result = prime
                * result
                + ((accountHolderaccountType3 == null) ? 0
                : accountHolderaccountType3.hashCode());
        result = prime
                * result
                + ((accountHolderaccountType4 == null) ? 0
                : accountHolderaccountType4.hashCode());
        result = prime
                * result
                + ((accountHolderaccountType5 == null) ? 0
                : accountHolderaccountType5.hashCode());
        result = prime
                * result
                + ((accountHolderanyEmi1 == null) ? 0 : accountHolderanyEmi1
                .hashCode());
        result = prime
                * result
                + ((accountHolderanyEmi2 == null) ? 0 : accountHolderanyEmi2
                .hashCode());
        result = prime
                * result
                + ((accountHolderanyEmi3 == null) ? 0 : accountHolderanyEmi3
                .hashCode());
        result = prime
                * result
                + ((accountHolderanyEmi4 == null) ? 0 : accountHolderanyEmi4
                .hashCode());
        result = prime
                * result
                + ((accountHolderanyEmi5 == null) ? 0 : accountHolderanyEmi5
                .hashCode());
        result = prime
                * result
                + ((accountHolderavgBankBalance1 == null) ? 0
                : accountHolderavgBankBalance1.hashCode());
        result = prime
                * result
                + ((accountHolderavgBankBalance2 == null) ? 0
                : accountHolderavgBankBalance2.hashCode());
        result = prime
                * result
                + ((accountHolderavgBankBalance3 == null) ? 0
                : accountHolderavgBankBalance3.hashCode());
        result = prime
                * result
                + ((accountHolderavgBankBalance4 == null) ? 0
                : accountHolderavgBankBalance4.hashCode());
        result = prime
                * result
                + ((accountHolderavgBankBalance5 == null) ? 0
                : accountHolderavgBankBalance5.hashCode());
        result = prime
                * result
                + ((accountHolderbankName1 == null) ? 0
                : accountHolderbankName1.hashCode());
        result = prime
                * result
                + ((accountHolderbankName2 == null) ? 0
                : accountHolderbankName2.hashCode());
        result = prime
                * result
                + ((accountHolderbankName3 == null) ? 0
                : accountHolderbankName3.hashCode());
        result = prime
                * result
                + ((accountHolderbankName4 == null) ? 0
                : accountHolderbankName4.hashCode());
        result = prime
                * result
                + ((accountHolderbankName5 == null) ? 0
                : accountHolderbankName5.hashCode());
        result = prime
                * result
                + ((accountHolderbranchName1 == null) ? 0
                : accountHolderbranchName1.hashCode());
        result = prime
                * result
                + ((accountHolderbranchName2 == null) ? 0
                : accountHolderbranchName2.hashCode());
        result = prime
                * result
                + ((accountHolderbranchName3 == null) ? 0
                : accountHolderbranchName3.hashCode());
        result = prime
                * result
                + ((accountHolderbranchName4 == null) ? 0
                : accountHolderbranchName4.hashCode());
        result = prime
                * result
                + ((accountHolderbranchName5 == null) ? 0
                : accountHolderbranchName5.hashCode());
        result = prime
                * result
                + ((accountHoldercurrentlyRunningLoan1 == null) ? 0
                : accountHoldercurrentlyRunningLoan1.hashCode());
        result = prime
                * result
                + ((accountHoldercurrentlyRunningLoan2 == null) ? 0
                : accountHoldercurrentlyRunningLoan2.hashCode());
        result = prime
                * result
                + ((accountHoldercurrentlyRunningLoan3 == null) ? 0
                : accountHoldercurrentlyRunningLoan3.hashCode());
        result = prime
                * result
                + ((accountHoldercurrentlyRunningLoan4 == null) ? 0
                : accountHoldercurrentlyRunningLoan4.hashCode());
        result = prime
                * result
                + ((accountHoldercurrentlyRunningLoan5 == null) ? 0
                : accountHoldercurrentlyRunningLoan5.hashCode());
        result = prime
                * result
                + ((accountHolderdeductedEmiAmt1 == null) ? 0
                : accountHolderdeductedEmiAmt1.hashCode());
        result = prime
                * result
                + ((accountHolderdeductedEmiAmt2 == null) ? 0
                : accountHolderdeductedEmiAmt2.hashCode());
        result = prime
                * result
                + ((accountHolderdeductedEmiAmt3 == null) ? 0
                : accountHolderdeductedEmiAmt3.hashCode());
        result = prime
                * result
                + ((accountHolderdeductedEmiAmt4 == null) ? 0
                : accountHolderdeductedEmiAmt4.hashCode());
        result = prime
                * result
                + ((accountHolderdeductedEmiAmt5 == null) ? 0
                : accountHolderdeductedEmiAmt5.hashCode());
        result = prime
                * result
                + ((accountHolderfirstName1 == null) ? 0
                : accountHolderfirstName1.hashCode());
        result = prime
                * result
                + ((accountHolderfirstName2 == null) ? 0
                : accountHolderfirstName2.hashCode());
        result = prime
                * result
                + ((accountHolderfirstName3 == null) ? 0
                : accountHolderfirstName3.hashCode());
        result = prime
                * result
                + ((accountHolderfirstName4 == null) ? 0
                : accountHolderfirstName4.hashCode());
        result = prime
                * result
                + ((accountHolderfirstName5 == null) ? 0
                : accountHolderfirstName5.hashCode());
        result = prime
                * result
                + ((accountHolderinwardChequeReturn1 == null) ? 0
                : accountHolderinwardChequeReturn1.hashCode());
        result = prime
                * result
                + ((accountHolderinwardChequeReturn2 == null) ? 0
                : accountHolderinwardChequeReturn2.hashCode());
        result = prime
                * result
                + ((accountHolderinwardChequeReturn3 == null) ? 0
                : accountHolderinwardChequeReturn3.hashCode());
        result = prime
                * result
                + ((accountHolderinwardChequeReturn4 == null) ? 0
                : accountHolderinwardChequeReturn4.hashCode());
        result = prime
                * result
                + ((accountHolderinwardChequeReturn5 == null) ? 0
                : accountHolderinwardChequeReturn5.hashCode());
        result = prime
                * result
                + ((accountHolderlastName1 == null) ? 0
                : accountHolderlastName1.hashCode());
        result = prime
                * result
                + ((accountHolderlastName2 == null) ? 0
                : accountHolderlastName2.hashCode());
        result = prime
                * result
                + ((accountHolderlastName3 == null) ? 0
                : accountHolderlastName3.hashCode());
        result = prime
                * result
                + ((accountHolderlastName4 == null) ? 0
                : accountHolderlastName4.hashCode());
        result = prime
                * result
                + ((accountHolderlastName5 == null) ? 0
                : accountHolderlastName5.hashCode());
        result = prime
                * result
                + ((accountHoldermentionAmount1 == null) ? 0
                : accountHoldermentionAmount1.hashCode());
        result = prime
                * result
                + ((accountHoldermentionAmount2 == null) ? 0
                : accountHoldermentionAmount2.hashCode());
        result = prime
                * result
                + ((accountHoldermentionAmount3 == null) ? 0
                : accountHoldermentionAmount3.hashCode());
        result = prime
                * result
                + ((accountHoldermentionAmount4 == null) ? 0
                : accountHoldermentionAmount4.hashCode());
        result = prime
                * result
                + ((accountHoldermentionAmount5 == null) ? 0
                : accountHoldermentionAmount5.hashCode());
        result = prime
                * result
                + ((accountHoldermiddleName1 == null) ? 0
                : accountHoldermiddleName1.hashCode());
        result = prime
                * result
                + ((accountHoldermiddleName2 == null) ? 0
                : accountHoldermiddleName2.hashCode());
        result = prime
                * result
                + ((accountHoldermiddleName3 == null) ? 0
                : accountHoldermiddleName3.hashCode());
        result = prime
                * result
                + ((accountHoldermiddleName4 == null) ? 0
                : accountHoldermiddleName4.hashCode());
        result = prime
                * result
                + ((accountHoldermiddleName5 == null) ? 0
                : accountHoldermiddleName5.hashCode());
        result = prime
                * result
                + ((accountHolderoutwardChequeReturn1 == null) ? 0
                : accountHolderoutwardChequeReturn1.hashCode());
        result = prime
                * result
                + ((accountHolderoutwardChequeReturn2 == null) ? 0
                : accountHolderoutwardChequeReturn2.hashCode());
        result = prime
                * result
                + ((accountHolderoutwardChequeReturn3 == null) ? 0
                : accountHolderoutwardChequeReturn3.hashCode());
        result = prime
                * result
                + ((accountHolderoutwardChequeReturn4 == null) ? 0
                : accountHolderoutwardChequeReturn4.hashCode());
        result = prime
                * result
                + ((accountHolderoutwardChequeReturn5 == null) ? 0
                : accountHolderoutwardChequeReturn5.hashCode());
        result = prime
                * result
                + ((accountHolderprefix1 == null) ? 0 : accountHolderprefix1
                .hashCode());
        result = prime
                * result
                + ((accountHolderprefix2 == null) ? 0 : accountHolderprefix2
                .hashCode());
        result = prime
                * result
                + ((accountHolderprefix3 == null) ? 0 : accountHolderprefix3
                .hashCode());
        result = prime
                * result
                + ((accountHolderprefix4 == null) ? 0 : accountHolderprefix4
                .hashCode());
        result = prime
                * result
                + ((accountHolderprefix5 == null) ? 0 : accountHolderprefix5
                .hashCode());
        result = prime
                * result
                + ((accountHoldersalaryAccount1 == null) ? 0
                : accountHoldersalaryAccount1.hashCode());
        result = prime
                * result
                + ((accountHoldersalaryAccount2 == null) ? 0
                : accountHoldersalaryAccount2.hashCode());
        result = prime
                * result
                + ((accountHoldersalaryAccount3 == null) ? 0
                : accountHoldersalaryAccount3.hashCode());
        result = prime
                * result
                + ((accountHoldersalaryAccount4 == null) ? 0
                : accountHoldersalaryAccount4.hashCode());
        result = prime
                * result
                + ((accountHoldersalaryAccount5 == null) ? 0
                : accountHoldersalaryAccount5.hashCode());
        result = prime
                * result
                + ((accountHoldersuffix1 == null) ? 0 : accountHoldersuffix1
                .hashCode());
        result = prime
                * result
                + ((accountHoldersuffix2 == null) ? 0 : accountHoldersuffix2
                .hashCode());
        result = prime
                * result
                + ((accountHoldersuffix3 == null) ? 0 : accountHoldersuffix3
                .hashCode());
        result = prime
                * result
                + ((accountHoldersuffix4 == null) ? 0 : accountHoldersuffix4
                .hashCode());
        result = prime
                * result
                + ((accountHoldersuffix5 == null) ? 0 : accountHoldersuffix5
                .hashCode());
        result = prime
                * result
                + ((add1accommodation == null) ? 0 : add1accommodation
                .hashCode());
        result = prime
                * result
                + ((add1addressLine1 == null) ? 0 : add1addressLine1.hashCode());
        result = prime
                * result
                + ((add1addressLine2 == null) ? 0 : add1addressLine2.hashCode());
        result = prime * result
                + ((add1addressType == null) ? 0 : add1addressType.hashCode());
        result = prime * result
                + ((add1city == null) ? 0 : add1city.hashCode());
        result = prime * result
                + ((add1country == null) ? 0 : add1country.hashCode());
        result = prime
                * result
                + ((add1distanceFrom == null) ? 0 : add1distanceFrom.hashCode());
        result = prime * result
                + ((add1district == null) ? 0 : add1district.hashCode());
        result = prime * result
                + ((add1landLoard == null) ? 0 : add1landLoard.hashCode());
        result = prime * result
                + ((add1landMark == null) ? 0 : add1landMark.hashCode());
        result = prime * result
                + ((add1line3 == null) ? 0 : add1line3.hashCode());
        result = prime * result
                + ((add1line4 == null) ? 0 : add1line4.hashCode());
        result = prime
                * result
                + ((add1monthAtAddress == null) ? 0 : add1monthAtAddress
                .hashCode());
        result = prime * result
                + ((add1monthAtCity == null) ? 0 : add1monthAtCity.hashCode());
        result = prime * result + ((add1pin == null) ? 0 : add1pin.hashCode());
        result = prime * result
                + ((add1rentAmount == null) ? 0 : add1rentAmount.hashCode());
        result = prime
                * result
                + ((add1residenceAddressType == null) ? 0
                : add1residenceAddressType.hashCode());
        result = prime * result
                + ((add1state == null) ? 0 : add1state.hashCode());
        result = prime
                * result
                + ((add1timeAtAddress == null) ? 0 : add1timeAtAddress
                .hashCode());
        result = prime * result
                + ((add1village == null) ? 0 : add1village.hashCode());
        result = prime * result
                + ((add1yearAtCity == null) ? 0 : add1yearAtCity.hashCode());
        result = prime
                * result
                + ((add2accommodation == null) ? 0 : add2accommodation
                .hashCode());
        result = prime
                * result
                + ((add2addressLine1 == null) ? 0 : add2addressLine1.hashCode());
        result = prime
                * result
                + ((add2addressLine2 == null) ? 0 : add2addressLine2.hashCode());
        result = prime * result
                + ((add2addressType == null) ? 0 : add2addressType.hashCode());
        result = prime * result
                + ((add2city == null) ? 0 : add2city.hashCode());
        result = prime * result
                + ((add2country == null) ? 0 : add2country.hashCode());
        result = prime
                * result
                + ((add2distanceFrom == null) ? 0 : add2distanceFrom.hashCode());
        result = prime * result
                + ((add2district == null) ? 0 : add2district.hashCode());
        result = prime * result
                + ((add2landLoard == null) ? 0 : add2landLoard.hashCode());
        result = prime * result
                + ((add2landMark == null) ? 0 : add2landMark.hashCode());
        result = prime * result
                + ((add2line3 == null) ? 0 : add2line3.hashCode());
        result = prime * result
                + ((add2line4 == null) ? 0 : add2line4.hashCode());
        result = prime
                * result
                + ((add2monthAtAddress == null) ? 0 : add2monthAtAddress
                .hashCode());
        result = prime * result
                + ((add2monthAtCity == null) ? 0 : add2monthAtCity.hashCode());
        result = prime * result + ((add2pin == null) ? 0 : add2pin.hashCode());
        result = prime * result
                + ((add2rentAmount == null) ? 0 : add2rentAmount.hashCode());
        result = prime
                * result
                + ((add2residenceAddressType == null) ? 0
                : add2residenceAddressType.hashCode());
        result = prime * result
                + ((add2state == null) ? 0 : add2state.hashCode());
        result = prime
                * result
                + ((add2timeAtAddress == null) ? 0 : add2timeAtAddress
                .hashCode());
        result = prime * result
                + ((add2village == null) ? 0 : add2village.hashCode());
        result = prime * result
                + ((add2yearAtCity == null) ? 0 : add2yearAtCity.hashCode());
        result = prime
                * result
                + ((add3accommodation == null) ? 0 : add3accommodation
                .hashCode());
        result = prime
                * result
                + ((add3addressLine1 == null) ? 0 : add3addressLine1.hashCode());
        result = prime
                * result
                + ((add3addressLine2 == null) ? 0 : add3addressLine2.hashCode());
        result = prime * result
                + ((add3addressType == null) ? 0 : add3addressType.hashCode());
        result = prime * result
                + ((add3city == null) ? 0 : add3city.hashCode());
        result = prime * result
                + ((add3country == null) ? 0 : add3country.hashCode());
        result = prime
                * result
                + ((add3distanceFrom == null) ? 0 : add3distanceFrom.hashCode());
        result = prime * result
                + ((add3district == null) ? 0 : add3district.hashCode());
        result = prime * result
                + ((add3landLoard == null) ? 0 : add3landLoard.hashCode());
        result = prime * result
                + ((add3landMark == null) ? 0 : add3landMark.hashCode());
        result = prime * result
                + ((add3line3 == null) ? 0 : add3line3.hashCode());
        result = prime * result
                + ((add3line4 == null) ? 0 : add3line4.hashCode());
        result = prime
                * result
                + ((add3monthAtAddress == null) ? 0 : add3monthAtAddress
                .hashCode());
        result = prime * result
                + ((add3monthAtCity == null) ? 0 : add3monthAtCity.hashCode());
        result = prime * result + ((add3pin == null) ? 0 : add3pin.hashCode());
        result = prime * result
                + ((add3rentAmount == null) ? 0 : add3rentAmount.hashCode());
        result = prime
                * result
                + ((add3residenceAddressType == null) ? 0
                : add3residenceAddressType.hashCode());
        result = prime * result
                + ((add3state == null) ? 0 : add3state.hashCode());
        result = prime
                * result
                + ((add3timeAtAddress == null) ? 0 : add3timeAtAddress
                .hashCode());
        result = prime * result
                + ((add3village == null) ? 0 : add3village.hashCode());
        result = prime * result
                + ((add3yearAtCity == null) ? 0 : add3yearAtCity.hashCode());
        result = prime
                * result
                + ((add4accommodation == null) ? 0 : add4accommodation
                .hashCode());
        result = prime
                * result
                + ((add4addressLine1 == null) ? 0 : add4addressLine1.hashCode());
        result = prime
                * result
                + ((add4addressLine2 == null) ? 0 : add4addressLine2.hashCode());
        result = prime * result
                + ((add4addressType == null) ? 0 : add4addressType.hashCode());
        result = prime * result
                + ((add4city == null) ? 0 : add4city.hashCode());
        result = prime * result
                + ((add4country == null) ? 0 : add4country.hashCode());
        result = prime
                * result
                + ((add4distanceFrom == null) ? 0 : add4distanceFrom.hashCode());
        result = prime * result
                + ((add4district == null) ? 0 : add4district.hashCode());
        result = prime * result
                + ((add4landLoard == null) ? 0 : add4landLoard.hashCode());
        result = prime * result
                + ((add4landMark == null) ? 0 : add4landMark.hashCode());
        result = prime * result
                + ((add4line3 == null) ? 0 : add4line3.hashCode());
        result = prime * result
                + ((add4line4 == null) ? 0 : add4line4.hashCode());
        result = prime
                * result
                + ((add4monthAtAddress == null) ? 0 : add4monthAtAddress
                .hashCode());
        result = prime * result
                + ((add4monthAtCity == null) ? 0 : add4monthAtCity.hashCode());
        result = prime * result + ((add4pin == null) ? 0 : add4pin.hashCode());
        result = prime * result
                + ((add4rentAmount == null) ? 0 : add4rentAmount.hashCode());
        result = prime
                * result
                + ((add4residenceAddressType == null) ? 0
                : add4residenceAddressType.hashCode());
        result = prime * result
                + ((add4state == null) ? 0 : add4state.hashCode());
        result = prime
                * result
                + ((add4timeAtAddress == null) ? 0 : add4timeAtAddress
                .hashCode());
        result = prime * result
                + ((add4village == null) ? 0 : add4village.hashCode());
        result = prime * result
                + ((add4yearAtCity == null) ? 0 : add4yearAtCity.hashCode());
        result = prime
                * result
                + ((add5accommodation == null) ? 0 : add5accommodation
                .hashCode());
        result = prime * result
                + ((add5addressType == null) ? 0 : add5addressType.hashCode());
        result = prime * result
                + ((add5country == null) ? 0 : add5country.hashCode());
        result = prime
                * result
                + ((add5distanceFrom == null) ? 0 : add5distanceFrom.hashCode());
        result = prime * result
                + ((add5district == null) ? 0 : add5district.hashCode());
        result = prime * result
                + ((add5landLoard == null) ? 0 : add5landLoard.hashCode());
        result = prime * result
                + ((add5landMark == null) ? 0 : add5landMark.hashCode());
        result = prime * result
                + ((add5line3 == null) ? 0 : add5line3.hashCode());
        result = prime * result
                + ((add5line4 == null) ? 0 : add5line4.hashCode());
        result = prime
                * result
                + ((add5monthAtAddress == null) ? 0 : add5monthAtAddress
                .hashCode());
        result = prime * result
                + ((add5monthAtCity == null) ? 0 : add5monthAtCity.hashCode());
        result = prime * result + ((add5pin == null) ? 0 : add5pin.hashCode());
        result = prime * result
                + ((add5rentAmount == null) ? 0 : add5rentAmount.hashCode());
        result = prime
                * result
                + ((add5residenceAddressType == null) ? 0
                : add5residenceAddressType.hashCode());
        result = prime * result
                + ((add5state == null) ? 0 : add5state.hashCode());
        result = prime
                * result
                + ((add5timeAtAddress == null) ? 0 : add5timeAtAddress
                .hashCode());
        result = prime * result
                + ((add5village == null) ? 0 : add5village.hashCode());
        result = prime * result
                + ((add5yearAtCity == null) ? 0 : add5yearAtCity.hashCode());
        result = prime * result
                + ((addharVerified == null) ? 0 : addharVerified.hashCode());
        result = prime * result
                + ((applicantAge == null) ? 0 : applicantAge.hashCode());
        result = prime
                * result
                + ((applicantFatherFirstName == null) ? 0
                : applicantFatherFirstName.hashCode());
        result = prime
                * result
                + ((applicantFatherLastName == null) ? 0
                : applicantFatherLastName.hashCode());
        result = prime
                * result
                + ((applicantFatherMiddleName == null) ? 0
                : applicantFatherMiddleName.hashCode());
        result = prime
                * result
                + ((applicantFatherPrefix == null) ? 0 : applicantFatherPrefix
                .hashCode());
        result = prime
                * result
                + ((applicantFatherSuffix == null) ? 0 : applicantFatherSuffix
                .hashCode());
        result = prime
                * result
                + ((applicantFirstName == null) ? 0 : applicantFirstName
                .hashCode());
        result = prime * result
                + ((applicantGender == null) ? 0 : applicantGender.hashCode());
        result = prime * result
                + ((applicantId == null) ? 0 : applicantId.hashCode());
        result = prime
                * result
                + ((applicantLastName == null) ? 0 : applicantLastName
                .hashCode());
        result = prime
                * result
                + ((applicantMaritalStatus == null) ? 0
                : applicantMaritalStatus.hashCode());
        result = prime
                * result
                + ((applicantMiddleName == null) ? 0 : applicantMiddleName
                .hashCode());
        result = prime * result
                + ((applicantPrefix == null) ? 0 : applicantPrefix.hashCode());
        result = prime
                * result
                + ((applicantReferenceFirstName == null) ? 0
                : applicantReferenceFirstName.hashCode());
        result = prime
                * result
                + ((applicantReferenceaddressLine1 == null) ? 0
                : applicantReferenceaddressLine1.hashCode());
        result = prime
                * result
                + ((applicantReferenceaddressLine2 == null) ? 0
                : applicantReferenceaddressLine2.hashCode());
        result = prime
                * result
                + ((applicantReferenceareaCode == null) ? 0
                : applicantReferenceareaCode.hashCode());
        result = prime
                * result
                + ((applicantReferencecity == null) ? 0
                : applicantReferencecity.hashCode());
        result = prime
                * result
                + ((applicantReferencecountry == null) ? 0
                : applicantReferencecountry.hashCode());
        result = prime
                * result
                + ((applicantReferencecountryCode == null) ? 0
                : applicantReferencecountryCode.hashCode());
        result = prime
                * result
                + ((applicantReferencedistanceFrom == null) ? 0
                : applicantReferencedistanceFrom.hashCode());
        result = prime
                * result
                + ((applicantReferencedistrict == null) ? 0
                : applicantReferencedistrict.hashCode());
        result = prime
                * result
                + ((applicantReferenceextension == null) ? 0
                : applicantReferenceextension.hashCode());
        result = prime
                * result
                + ((applicantReferencelandLoard == null) ? 0
                : applicantReferencelandLoard.hashCode());
        result = prime
                * result
                + ((applicantReferencelandMark == null) ? 0
                : applicantReferencelandMark.hashCode());
        result = prime
                * result
                + ((applicantReferencelastName == null) ? 0
                : applicantReferencelastName.hashCode());
        result = prime
                * result
                + ((applicantReferenceline3 == null) ? 0
                : applicantReferenceline3.hashCode());
        result = prime
                * result
                + ((applicantReferenceline4 == null) ? 0
                : applicantReferenceline4.hashCode());
        result = prime
                * result
                + ((applicantReferencemiddleName == null) ? 0
                : applicantReferencemiddleName.hashCode());
        result = prime
                * result
                + ((applicantReferenceoccupation == null) ? 0
                : applicantReferenceoccupation.hashCode());
        result = prime
                * result
                + ((applicantReferencephoneNumber == null) ? 0
                : applicantReferencephoneNumber.hashCode());
        result = prime
                * result
                + ((applicantReferencephoneType == null) ? 0
                : applicantReferencephoneType.hashCode());
        result = prime
                * result
                + ((applicantReferencepin == null) ? 0 : applicantReferencepin
                .hashCode());
        result = prime
                * result
                + ((applicantReferenceprefix == null) ? 0
                : applicantReferenceprefix.hashCode());
        result = prime
                * result
                + ((applicantReferencerelationType == null) ? 0
                : applicantReferencerelationType.hashCode());
        result = prime
                * result
                + ((applicantReferencestate == null) ? 0
                : applicantReferencestate.hashCode());
        result = prime
                * result
                + ((applicantReferencesuffix == null) ? 0
                : applicantReferencesuffix.hashCode());
        result = prime
                * result
                + ((applicantReferencevillage == null) ? 0
                : applicantReferencevillage.hashCode());
        result = prime
                * result
                + ((applicantReligion == null) ? 0 : applicantReligion
                .hashCode());
        result = prime
                * result
                + ((applicantSpouseFirstName == null) ? 0
                : applicantSpouseFirstName.hashCode());
        result = prime
                * result
                + ((applicantSpouseLastName == null) ? 0
                : applicantSpouseLastName.hashCode());
        result = prime
                * result
                + ((applicantSpouseMiddleName == null) ? 0
                : applicantSpouseMiddleName.hashCode());
        result = prime
                * result
                + ((applicantSpousePrefix == null) ? 0 : applicantSpousePrefix
                .hashCode());
        result = prime
                * result
                + ((applicantSpouseSuffix == null) ? 0 : applicantSpouseSuffix
                .hashCode());
        result = prime
                * result
                + ((applicantStringOfBirth == null) ? 0
                : applicantStringOfBirth.hashCode());
        result = prime * result
                + ((applicantSuffix == null) ? 0 : applicantSuffix.hashCode());
        result = prime * result
                + ((applicationId == null) ? 0 : applicationId.hashCode());
        result = prime
                * result
                + ((applicationSource == null) ? 0 : applicationSource
                .hashCode());
        result = prime * result
                + ((balanceTenure1 == null) ? 0 : balanceTenure1.hashCode());
        result = prime * result
                + ((balanceTenure2 == null) ? 0 : balanceTenure2.hashCode());
        result = prime * result
                + ((balanceTenure3 == null) ? 0 : balanceTenure3.hashCode());
        result = prime * result
                + ((balanceTenure4 == null) ? 0 : balanceTenure4.hashCode());
        result = prime * result
                + ((balanceTenure5 == null) ? 0 : balanceTenure5.hashCode());
        result = prime * result + ((basic == null) ? 0 : basic.hashCode());
        result = prime * result
                + ((basicIncome == null) ? 0 : basicIncome.hashCode());
        result = prime
                * result
                + ((carSurrogateManufactureYear1 == null) ? 0
                : carSurrogateManufactureYear1.hashCode());
        result = prime
                * result
                + ((carSurrogateManufactureYear2 == null) ? 0
                : carSurrogateManufactureYear2.hashCode());
        result = prime
                * result
                + ((carSurrogateManufactureYear3 == null) ? 0
                : carSurrogateManufactureYear3.hashCode());
        result = prime
                * result
                + ((carSurrogateManufactureYear4 == null) ? 0
                : carSurrogateManufactureYear4.hashCode());
        result = prime
                * result
                + ((carSurrogateManufactureYear5 == null) ? 0
                : carSurrogateManufactureYear5.hashCode());
        result = prime
                * result
                + ((carSurrogateModelName1 == null) ? 0
                : carSurrogateModelName1.hashCode());
        result = prime
                * result
                + ((carSurrogateModelName2 == null) ? 0
                : carSurrogateModelName2.hashCode());
        result = prime
                * result
                + ((carSurrogateModelName3 == null) ? 0
                : carSurrogateModelName3.hashCode());
        result = prime
                * result
                + ((carSurrogateModelName4 == null) ? 0
                : carSurrogateModelName4.hashCode());
        result = prime
                * result
                + ((carSurrogateModelName5 == null) ? 0
                : carSurrogateModelName5.hashCode());
        result = prime
                * result
                + ((carSurrogateRegistrationNumber1 == null) ? 0
                : carSurrogateRegistrationNumber1.hashCode());
        result = prime
                * result
                + ((carSurrogateRegistrationNumber2 == null) ? 0
                : carSurrogateRegistrationNumber2.hashCode());
        result = prime
                * result
                + ((carSurrogateRegistrationNumber3 == null) ? 0
                : carSurrogateRegistrationNumber3.hashCode());
        result = prime
                * result
                + ((carSurrogateRegistrationNumber4 == null) ? 0
                : carSurrogateRegistrationNumber4.hashCode());
        result = prime
                * result
                + ((carSurrogateRegistrationNumber5 == null) ? 0
                : carSurrogateRegistrationNumber5.hashCode());
        result = prime
                * result
                + ((cityCompensatoryAllowance == null) ? 0
                : cityCompensatoryAllowance.hashCode());
        result = prime
                * result
                + ((creditCardNumber == null) ? 0 : creditCardNumber.hashCode());
        result = prime * result
                + ((creditGranter1 == null) ? 0 : creditGranter1.hashCode());
        result = prime * result
                + ((creditGranter2 == null) ? 0 : creditGranter2.hashCode());
        result = prime * result
                + ((creditGranter3 == null) ? 0 : creditGranter3.hashCode());
        result = prime * result
                + ((creditGranter4 == null) ? 0 : creditGranter4.hashCode());
        result = prime * result
                + ((creditGranter5 == null) ? 0 : creditGranter5.hashCode());
        result = prime * result + ((croId == null) ? 0 : croId.hashCode());
        result = prime * result
                + ((dealerId == null) ? 0 : dealerId.hashCode());
        result = prime
                * result
                + ((dearnessAllowance == null) ? 0 : dearnessAllowance
                .hashCode());
        result = prime * result
                + ((depreciation1 == null) ? 0 : depreciation1.hashCode());
        result = prime * result
                + ((depreciation2 == null) ? 0 : depreciation2.hashCode());
        result = prime * result
                + ((depreciation3 == null) ? 0 : depreciation3.hashCode());
        result = prime * result
                + ((depreciation4 == null) ? 0 : depreciation4.hashCode());
        result = prime * result
                + ((depreciation5 == null) ? 0 : depreciation5.hashCode());
        result = prime * result + ((dsaId == null) ? 0 : dsaId.hashCode());
        result = prime * result
                + ((education == null) ? 0 : education.hashCode());
        result = prime
                * result
                + ((email1emailAddress == null) ? 0 : email1emailAddress
                .hashCode());
        result = prime * result
                + ((email1emailType == null) ? 0 : email1emailType.hashCode());
        result = prime
                * result
                + ((email2emailAddress == null) ? 0 : email2emailAddress
                .hashCode());
        result = prime * result
                + ((email2emailType == null) ? 0 : email2emailType.hashCode());
        result = prime
                * result
                + ((email3emailAddress == null) ? 0 : email3emailAddress
                .hashCode());
        result = prime * result
                + ((email3emailType == null) ? 0 : email3emailType.hashCode());
        result = prime
                * result
                + ((email4emailAddress == null) ? 0 : email4emailAddress
                .hashCode());
        result = prime * result
                + ((email4emailType == null) ? 0 : email4emailType.hashCode());
        result = prime
                * result
                + ((email5emailAddress == null) ? 0 : email5emailAddress
                .hashCode());
        result = prime * result
                + ((email5emailType == null) ? 0 : email5emailType.hashCode());
        result = prime * result + ((emiAmt1 == null) ? 0 : emiAmt1.hashCode());
        result = prime * result + ((emiAmt2 == null) ? 0 : emiAmt2.hashCode());
        result = prime * result + ((emiAmt3 == null) ? 0 : emiAmt3.hashCode());
        result = prime * result + ((emiAmt4 == null) ? 0 : emiAmt4.hashCode());
        result = prime * result + ((emiAmt5 == null) ? 0 : emiAmt5.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeIncome1 == null) ? 0
                : emp1LastMonthIncomeIncome1.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeIncome2 == null) ? 0
                : emp1LastMonthIncomeIncome2.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeIncome3 == null) ? 0
                : emp1LastMonthIncomeIncome3.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeIncome4 == null) ? 0
                : emp1LastMonthIncomeIncome4.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeIncome5 == null) ? 0
                : emp1LastMonthIncomeIncome5.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeMonth1 == null) ? 0
                : emp1LastMonthIncomeMonth1.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeMonth2 == null) ? 0
                : emp1LastMonthIncomeMonth2.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeMonth3 == null) ? 0
                : emp1LastMonthIncomeMonth3.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeMonth4 == null) ? 0
                : emp1LastMonthIncomeMonth4.hashCode());
        result = prime
                * result
                + ((emp1LastMonthIncomeMonth5 == null) ? 0
                : emp1LastMonthIncomeMonth5.hashCode());
        result = prime
                * result
                + ((emp1StringOfJoining == null) ? 0 : emp1StringOfJoining
                .hashCode());
        result = prime
                * result
                + ((emp1StringOfLeaving == null) ? 0 : emp1StringOfLeaving
                .hashCode());
        result = prime
                * result
                + ((emp1businessName == null) ? 0 : emp1businessName.hashCode());
        result = prime
                * result
                + ((emp1commencementString == null) ? 0
                : emp1commencementString.hashCode());
        result = prime
                * result
                + ((emp1constitution == null) ? 0 : emp1constitution.hashCode());
        result = prime * result
                + ((emp1department == null) ? 0 : emp1department.hashCode());
        result = prime * result
                + ((emp1designation == null) ? 0 : emp1designation.hashCode());
        result = prime
                * result
                + ((emp1employerBranch == null) ? 0 : emp1employerBranch
                .hashCode());
        result = prime
                * result
                + ((emp1employerCode == null) ? 0 : emp1employerCode.hashCode());
        result = prime
                * result
                + ((emp1employmentName == null) ? 0 : emp1employmentName
                .hashCode());
        result = prime
                * result
                + ((emp1employmentType == null) ? 0 : emp1employmentType
                .hashCode());
        result = prime * result
                + ((emp1grossSalary == null) ? 0 : emp1grossSalary.hashCode());
        result = prime * result
                + ((emp1industType == null) ? 0 : emp1industType.hashCode());
        result = prime
                * result
                + ((emp1industryType == null) ? 0 : emp1industryType.hashCode());
        result = prime * result
                + ((emp1itrAmount == null) ? 0 : emp1itrAmount.hashCode());
        result = prime * result
                + ((emp1itrId == null) ? 0 : emp1itrId.hashCode());
        result = prime * result
                + ((emp1modePayment == null) ? 0 : emp1modePayment.hashCode());
        result = prime
                * result
                + ((emp1monthlySalary == null) ? 0 : emp1monthlySalary
                .hashCode());
        result = prime
                * result
                + ((emp1timeWithEmployer == null) ? 0 : emp1timeWithEmployer
                .hashCode());
        result = prime * result
                + ((emp1workExps == null) ? 0 : emp1workExps.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeIncome1 == null) ? 0
                : emp2LastMonthIncomeIncome1.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeIncome2 == null) ? 0
                : emp2LastMonthIncomeIncome2.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeIncome3 == null) ? 0
                : emp2LastMonthIncomeIncome3.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeIncome4 == null) ? 0
                : emp2LastMonthIncomeIncome4.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeIncome5 == null) ? 0
                : emp2LastMonthIncomeIncome5.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeMonth1 == null) ? 0
                : emp2LastMonthIncomeMonth1.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeMonth2 == null) ? 0
                : emp2LastMonthIncomeMonth2.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeMonth3 == null) ? 0
                : emp2LastMonthIncomeMonth3.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeMonth4 == null) ? 0
                : emp2LastMonthIncomeMonth4.hashCode());
        result = prime
                * result
                + ((emp2LastMonthIncomeMonth5 == null) ? 0
                : emp2LastMonthIncomeMonth5.hashCode());
        result = prime
                * result
                + ((emp2StringOfJoining == null) ? 0 : emp2StringOfJoining
                .hashCode());
        result = prime
                * result
                + ((emp2StringOfLeaving == null) ? 0 : emp2StringOfLeaving
                .hashCode());
        result = prime
                * result
                + ((emp2businessName == null) ? 0 : emp2businessName.hashCode());
        result = prime
                * result
                + ((emp2commencementString == null) ? 0
                : emp2commencementString.hashCode());
        result = prime
                * result
                + ((emp2constitution == null) ? 0 : emp2constitution.hashCode());
        result = prime * result
                + ((emp2department == null) ? 0 : emp2department.hashCode());
        result = prime * result
                + ((emp2designation == null) ? 0 : emp2designation.hashCode());
        result = prime
                * result
                + ((emp2employerBranch == null) ? 0 : emp2employerBranch
                .hashCode());
        result = prime
                * result
                + ((emp2employerCode == null) ? 0 : emp2employerCode.hashCode());
        result = prime
                * result
                + ((emp2employmentName == null) ? 0 : emp2employmentName
                .hashCode());
        result = prime
                * result
                + ((emp2employmentType == null) ? 0 : emp2employmentType
                .hashCode());
        result = prime * result
                + ((emp2grossSalary == null) ? 0 : emp2grossSalary.hashCode());
        result = prime * result
                + ((emp2industType == null) ? 0 : emp2industType.hashCode());
        result = prime
                * result
                + ((emp2industryType == null) ? 0 : emp2industryType.hashCode());
        result = prime * result
                + ((emp2itrAmount == null) ? 0 : emp2itrAmount.hashCode());
        result = prime * result
                + ((emp2itrId == null) ? 0 : emp2itrId.hashCode());
        result = prime * result
                + ((emp2modePayment == null) ? 0 : emp2modePayment.hashCode());
        result = prime
                * result
                + ((emp2monthlySalary == null) ? 0 : emp2monthlySalary
                .hashCode());
        result = prime
                * result
                + ((emp2timeWithEmployer == null) ? 0 : emp2timeWithEmployer
                .hashCode());
        result = prime * result
                + ((emp2workExps == null) ? 0 : emp2workExps.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeIncome1 == null) ? 0
                : emp3LastMonthIncomeIncome1.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeIncome2 == null) ? 0
                : emp3LastMonthIncomeIncome2.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeIncome3 == null) ? 0
                : emp3LastMonthIncomeIncome3.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeIncome4 == null) ? 0
                : emp3LastMonthIncomeIncome4.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeIncome5 == null) ? 0
                : emp3LastMonthIncomeIncome5.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeMonth1 == null) ? 0
                : emp3LastMonthIncomeMonth1.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeMonth2 == null) ? 0
                : emp3LastMonthIncomeMonth2.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeMonth3 == null) ? 0
                : emp3LastMonthIncomeMonth3.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeMonth4 == null) ? 0
                : emp3LastMonthIncomeMonth4.hashCode());
        result = prime
                * result
                + ((emp3LastMonthIncomeMonth5 == null) ? 0
                : emp3LastMonthIncomeMonth5.hashCode());
        result = prime
                * result
                + ((emp3StringOfJoining == null) ? 0 : emp3StringOfJoining
                .hashCode());
        result = prime
                * result
                + ((emp3StringOfLeaving == null) ? 0 : emp3StringOfLeaving
                .hashCode());
        result = prime
                * result
                + ((emp3businessName == null) ? 0 : emp3businessName.hashCode());
        result = prime
                * result
                + ((emp3commencementString == null) ? 0
                : emp3commencementString.hashCode());
        result = prime
                * result
                + ((emp3constitution == null) ? 0 : emp3constitution.hashCode());
        result = prime * result
                + ((emp3department == null) ? 0 : emp3department.hashCode());
        result = prime * result
                + ((emp3designation == null) ? 0 : emp3designation.hashCode());
        result = prime
                * result
                + ((emp3employerBranch == null) ? 0 : emp3employerBranch
                .hashCode());
        result = prime
                * result
                + ((emp3employerCode == null) ? 0 : emp3employerCode.hashCode());
        result = prime
                * result
                + ((emp3employmentName == null) ? 0 : emp3employmentName
                .hashCode());
        result = prime
                * result
                + ((emp3employmentType == null) ? 0 : emp3employmentType
                .hashCode());
        result = prime * result
                + ((emp3grossSalary == null) ? 0 : emp3grossSalary.hashCode());
        result = prime * result
                + ((emp3industType == null) ? 0 : emp3industType.hashCode());
        result = prime
                * result
                + ((emp3industryType == null) ? 0 : emp3industryType.hashCode());
        result = prime * result
                + ((emp3itrAmount == null) ? 0 : emp3itrAmount.hashCode());
        result = prime * result
                + ((emp3itrId == null) ? 0 : emp3itrId.hashCode());
        result = prime * result
                + ((emp3modePayment == null) ? 0 : emp3modePayment.hashCode());
        result = prime
                * result
                + ((emp3monthlySalary == null) ? 0 : emp3monthlySalary
                .hashCode());
        result = prime
                * result
                + ((emp3timeWithEmployer == null) ? 0 : emp3timeWithEmployer
                .hashCode());
        result = prime * result
                + ((emp3workExps == null) ? 0 : emp3workExps.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeIncome1 == null) ? 0
                : emp4LastMonthIncomeIncome1.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeIncome2 == null) ? 0
                : emp4LastMonthIncomeIncome2.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeIncome3 == null) ? 0
                : emp4LastMonthIncomeIncome3.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeIncome4 == null) ? 0
                : emp4LastMonthIncomeIncome4.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeIncome5 == null) ? 0
                : emp4LastMonthIncomeIncome5.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeMonth1 == null) ? 0
                : emp4LastMonthIncomeMonth1.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeMonth2 == null) ? 0
                : emp4LastMonthIncomeMonth2.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeMonth3 == null) ? 0
                : emp4LastMonthIncomeMonth3.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeMonth4 == null) ? 0
                : emp4LastMonthIncomeMonth4.hashCode());
        result = prime
                * result
                + ((emp4LastMonthIncomeMonth5 == null) ? 0
                : emp4LastMonthIncomeMonth5.hashCode());
        result = prime
                * result
                + ((emp4StringOfJoining == null) ? 0 : emp4StringOfJoining
                .hashCode());
        result = prime
                * result
                + ((emp4StringOfLeaving == null) ? 0 : emp4StringOfLeaving
                .hashCode());
        result = prime
                * result
                + ((emp4businessName == null) ? 0 : emp4businessName.hashCode());
        result = prime
                * result
                + ((emp4commencementString == null) ? 0
                : emp4commencementString.hashCode());
        result = prime
                * result
                + ((emp4constitution == null) ? 0 : emp4constitution.hashCode());
        result = prime * result
                + ((emp4department == null) ? 0 : emp4department.hashCode());
        result = prime * result
                + ((emp4designation == null) ? 0 : emp4designation.hashCode());
        result = prime
                * result
                + ((emp4employerBranch == null) ? 0 : emp4employerBranch
                .hashCode());
        result = prime
                * result
                + ((emp4employerCode == null) ? 0 : emp4employerCode.hashCode());
        result = prime
                * result
                + ((emp4employmentName == null) ? 0 : emp4employmentName
                .hashCode());
        result = prime
                * result
                + ((emp4employmentType == null) ? 0 : emp4employmentType
                .hashCode());
        result = prime * result
                + ((emp4grossSalary == null) ? 0 : emp4grossSalary.hashCode());
        result = prime * result
                + ((emp4industType == null) ? 0 : emp4industType.hashCode());
        result = prime
                * result
                + ((emp4industryType == null) ? 0 : emp4industryType.hashCode());
        result = prime * result
                + ((emp4itrAmount == null) ? 0 : emp4itrAmount.hashCode());
        result = prime * result
                + ((emp4itrId == null) ? 0 : emp4itrId.hashCode());
        result = prime * result
                + ((emp4modePayment == null) ? 0 : emp4modePayment.hashCode());
        result = prime
                * result
                + ((emp4monthlySalary == null) ? 0 : emp4monthlySalary
                .hashCode());
        result = prime
                * result
                + ((emp4timeWithEmployer == null) ? 0 : emp4timeWithEmployer
                .hashCode());
        result = prime * result
                + ((emp4workExps == null) ? 0 : emp4workExps.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeIncome1 == null) ? 0
                : emp5LastMonthIncomeIncome1.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeIncome2 == null) ? 0
                : emp5LastMonthIncomeIncome2.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeIncome3 == null) ? 0
                : emp5LastMonthIncomeIncome3.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeIncome4 == null) ? 0
                : emp5LastMonthIncomeIncome4.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeIncome5 == null) ? 0
                : emp5LastMonthIncomeIncome5.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeMonth1 == null) ? 0
                : emp5LastMonthIncomeMonth1.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeMonth2 == null) ? 0
                : emp5LastMonthIncomeMonth2.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeMonth3 == null) ? 0
                : emp5LastMonthIncomeMonth3.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeMonth4 == null) ? 0
                : emp5LastMonthIncomeMonth4.hashCode());
        result = prime
                * result
                + ((emp5LastMonthIncomeMonth5 == null) ? 0
                : emp5LastMonthIncomeMonth5.hashCode());
        result = prime
                * result
                + ((emp5StringOfJoining == null) ? 0 : emp5StringOfJoining
                .hashCode());
        result = prime
                * result
                + ((emp5StringOfLeaving == null) ? 0 : emp5StringOfLeaving
                .hashCode());
        result = prime
                * result
                + ((emp5businessName == null) ? 0 : emp5businessName.hashCode());
        result = prime
                * result
                + ((emp5commencementString == null) ? 0
                : emp5commencementString.hashCode());
        result = prime
                * result
                + ((emp5constitution == null) ? 0 : emp5constitution.hashCode());
        result = prime * result
                + ((emp5department == null) ? 0 : emp5department.hashCode());
        result = prime * result
                + ((emp5designation == null) ? 0 : emp5designation.hashCode());
        result = prime
                * result
                + ((emp5employerBranch == null) ? 0 : emp5employerBranch
                .hashCode());
        result = prime
                * result
                + ((emp5employerCode == null) ? 0 : emp5employerCode.hashCode());
        result = prime
                * result
                + ((emp5employmentName == null) ? 0 : emp5employmentName
                .hashCode());
        result = prime
                * result
                + ((emp5employmentType == null) ? 0 : emp5employmentType
                .hashCode());
        result = prime * result
                + ((emp5grossSalary == null) ? 0 : emp5grossSalary.hashCode());
        result = prime * result
                + ((emp5industType == null) ? 0 : emp5industType.hashCode());
        result = prime
                * result
                + ((emp5industryType == null) ? 0 : emp5industryType.hashCode());
        result = prime * result
                + ((emp5itrAmount == null) ? 0 : emp5itrAmount.hashCode());
        result = prime * result
                + ((emp5itrId == null) ? 0 : emp5itrId.hashCode());
        result = prime * result
                + ((emp5modePayment == null) ? 0 : emp5modePayment.hashCode());
        result = prime
                * result
                + ((emp5monthlySalary == null) ? 0 : emp5monthlySalary
                .hashCode());
        result = prime
                * result
                + ((emp5timeWithEmployer == null) ? 0 : emp5timeWithEmployer
                .hashCode());
        result = prime * result
                + ((emp5workExps == null) ? 0 : emp5workExps.hashCode());
        result = prime * result
                + ((grossIncome == null) ? 0 : grossIncome.hashCode());
        result = prime
                * result
                + ((houseRentAllowance == null) ? 0 : houseRentAllowance
                .hashCode());
        result = prime * result
                + ((incentive == null) ? 0 : incentive.hashCode());
        result = prime
                * result
                + ((incomeDocumentAvailable == null) ? 0
                : incomeDocumentAvailable.hashCode());
        result = prime * result
                + ((institutionId == null) ? 0 : institutionId.hashCode());
        result = prime * result
                + ((itrString1 == null) ? 0 : itrString1.hashCode());
        result = prime * result
                + ((itrString2 == null) ? 0 : itrString2.hashCode());
        result = prime * result
                + ((itrString3 == null) ? 0 : itrString3.hashCode());
        result = prime * result
                + ((itrString4 == null) ? 0 : itrString4.hashCode());
        result = prime * result
                + ((itrString5 == null) ? 0 : itrString5.hashCode());
        result = prime
                * result
                + ((kyc1expiryString == null) ? 0 : kyc1expiryString.hashCode());
        result = prime * result
                + ((kyc1issueString == null) ? 0 : kyc1issueString.hashCode());
        result = prime * result
                + ((kyc1kycName == null) ? 0 : kyc1kycName.hashCode());
        result = prime * result
                + ((kyc1kycNumber == null) ? 0 : kyc1kycNumber.hashCode());
        result = prime * result
                + ((kyc1kycStatus == null) ? 0 : kyc1kycStatus.hashCode());
        result = prime
                * result
                + ((kyc2expiryString == null) ? 0 : kyc2expiryString.hashCode());
        result = prime * result
                + ((kyc2issueString == null) ? 0 : kyc2issueString.hashCode());
        result = prime * result
                + ((kyc2kycName == null) ? 0 : kyc2kycName.hashCode());
        result = prime * result
                + ((kyc2kycNumber == null) ? 0 : kyc2kycNumber.hashCode());
        result = prime * result
                + ((kyc2kycStatus == null) ? 0 : kyc2kycStatus.hashCode());
        result = prime
                * result
                + ((kyc3expiryString == null) ? 0 : kyc3expiryString.hashCode());
        result = prime * result
                + ((kyc3issueString == null) ? 0 : kyc3issueString.hashCode());
        result = prime * result
                + ((kyc3kycName == null) ? 0 : kyc3kycName.hashCode());
        result = prime * result
                + ((kyc3kycNumber == null) ? 0 : kyc3kycNumber.hashCode());
        result = prime * result
                + ((kyc3kycStatus == null) ? 0 : kyc3kycStatus.hashCode());
        result = prime
                * result
                + ((kyc4expiryString == null) ? 0 : kyc4expiryString.hashCode());
        result = prime * result
                + ((kyc4issueString == null) ? 0 : kyc4issueString.hashCode());
        result = prime * result
                + ((kyc4kycName == null) ? 0 : kyc4kycName.hashCode());
        result = prime * result
                + ((kyc4kycNumber == null) ? 0 : kyc4kycNumber.hashCode());
        result = prime * result
                + ((kyc4kycStatus == null) ? 0 : kyc4kycStatus.hashCode());
        result = prime
                * result
                + ((kyc5expiryString == null) ? 0 : kyc5expiryString.hashCode());
        result = prime * result
                + ((kyc5issueString == null) ? 0 : kyc5issueString.hashCode());
        result = prime * result
                + ((kyc5kycName == null) ? 0 : kyc5kycName.hashCode());
        result = prime * result
                + ((kyc5kycNumber == null) ? 0 : kyc5kycNumber.hashCode());
        result = prime * result
                + ((kyc5kycStatus == null) ? 0 : kyc5kycStatus.hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonth1 == null) ? 0 : lastMonthIncomeMonth1
                .hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonth2 == null) ? 0 : lastMonthIncomeMonth2
                .hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonth3 == null) ? 0 : lastMonthIncomeMonth3
                .hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonth4 == null) ? 0 : lastMonthIncomeMonth4
                .hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonth5 == null) ? 0 : lastMonthIncomeMonth5
                .hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonthIncome1 == null) ? 0
                : lastMonthIncomeMonthIncome1.hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonthIncome2 == null) ? 0
                : lastMonthIncomeMonthIncome2.hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonthIncome3 == null) ? 0
                : lastMonthIncomeMonthIncome3.hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonthIncome4 == null) ? 0
                : lastMonthIncomeMonthIncome4.hashCode());
        result = prime
                * result
                + ((lastMonthIncomeMonthIncome5 == null) ? 0
                : lastMonthIncomeMonthIncome5.hashCode());
        result = prime
                * result
                + ((lastTwoMonthSalary == null) ? 0 : lastTwoMonthSalary
                .hashCode());
        result = prime * result
                + ((loanAcountNo1 == null) ? 0 : loanAcountNo1.hashCode());
        result = prime * result
                + ((loanAcountNo2 == null) ? 0 : loanAcountNo2.hashCode());
        result = prime * result
                + ((loanAcountNo3 == null) ? 0 : loanAcountNo3.hashCode());
        result = prime * result
                + ((loanAcountNo4 == null) ? 0 : loanAcountNo4.hashCode());
        result = prime * result
                + ((loanAcountNo5 == null) ? 0 : loanAcountNo5.hashCode());
        result = prime * result
                + ((loanAmt1 == null) ? 0 : loanAmt1.hashCode());
        result = prime * result
                + ((loanAmt2 == null) ? 0 : loanAmt2.hashCode());
        result = prime * result
                + ((loanAmt3 == null) ? 0 : loanAmt3.hashCode());
        result = prime * result
                + ((loanAmt4 == null) ? 0 : loanAmt4.hashCode());
        result = prime * result
                + ((loanAmt5 == null) ? 0 : loanAmt5.hashCode());
        result = prime * result
                + ((loanApr1 == null) ? 0 : loanApr1.hashCode());
        result = prime * result
                + ((loanApr2 == null) ? 0 : loanApr2.hashCode());
        result = prime * result
                + ((loanApr3 == null) ? 0 : loanApr3.hashCode());
        result = prime * result
                + ((loanApr4 == null) ? 0 : loanApr4.hashCode());
        result = prime * result
                + ((loanApr5 == null) ? 0 : loanApr5.hashCode());
        result = prime * result
                + ((loanOwnership1 == null) ? 0 : loanOwnership1.hashCode());
        result = prime * result
                + ((loanOwnership2 == null) ? 0 : loanOwnership2.hashCode());
        result = prime * result
                + ((loanOwnership3 == null) ? 0 : loanOwnership3.hashCode());
        result = prime * result
                + ((loanOwnership4 == null) ? 0 : loanOwnership4.hashCode());
        result = prime * result
                + ((loanOwnership5 == null) ? 0 : loanOwnership5.hashCode());
        result = prime * result
                + ((loanPurpose1 == null) ? 0 : loanPurpose1.hashCode());
        result = prime * result
                + ((loanPurpose2 == null) ? 0 : loanPurpose2.hashCode());
        result = prime * result
                + ((loanPurpose3 == null) ? 0 : loanPurpose3.hashCode());
        result = prime * result
                + ((loanPurpose4 == null) ? 0 : loanPurpose4.hashCode());
        result = prime * result
                + ((loanPurpose5 == null) ? 0 : loanPurpose5.hashCode());
        result = prime * result
                + ((loanType1 == null) ? 0 : loanType1.hashCode());
        result = prime * result
                + ((loanType2 == null) ? 0 : loanType2.hashCode());
        result = prime * result
                + ((loanType3 == null) ? 0 : loanType3.hashCode());
        result = prime * result
                + ((loanType4 == null) ? 0 : loanType4.hashCode());
        result = prime * result
                + ((loanType5 == null) ? 0 : loanType5.hashCode());
        result = prime * result + ((mob1 == null) ? 0 : mob1.hashCode());
        result = prime * result + ((mob2 == null) ? 0 : mob2.hashCode());
        result = prime * result + ((mob3 == null) ? 0 : mob3.hashCode());
        result = prime * result + ((mob4 == null) ? 0 : mob4.hashCode());
        result = prime * result + ((mob5 == null) ? 0 : mob5.hashCode());
        result = prime * result
                + ((mobileVerified == null) ? 0 : mobileVerified.hashCode());
        result = prime * result
                + ((netIncome == null) ? 0 : netIncome.hashCode());
        result = prime * result
                + ((netProfit1 == null) ? 0 : netProfit1.hashCode());
        result = prime * result
                + ((netProfit2 == null) ? 0 : netProfit2.hashCode());
        result = prime * result
                + ((netProfit3 == null) ? 0 : netProfit3.hashCode());
        result = prime * result
                + ((netProfit4 == null) ? 0 : netProfit4.hashCode());
        result = prime * result
                + ((netProfit5 == null) ? 0 : netProfit5.hashCode());
        result = prime * result
                + ((noOfDependents == null) ? 0 : noOfDependents.hashCode());
        result = prime
                * result
                + ((noOfEarningMembers == null) ? 0 : noOfEarningMembers
                .hashCode());
        result = prime
                * result
                + ((noOfFamilyMembers == null) ? 0 : noOfFamilyMembers
                .hashCode());
        result = prime * result
                + ((obligate1 == null) ? 0 : obligate1.hashCode());
        result = prime * result
                + ((obligate2 == null) ? 0 : obligate2.hashCode());
        result = prime * result
                + ((obligate3 == null) ? 0 : obligate3.hashCode());
        result = prime * result
                + ((obligate4 == null) ? 0 : obligate4.hashCode());
        result = prime * result
                + ((obligate5 == null) ? 0 : obligate5.hashCode());
        result = prime * result
                + ((obligations == null) ? 0 : obligations.hashCode());
        result = prime * result + ((other == null) ? 0 : other.hashCode());
        result = prime * result
                + ((otherAllowance == null) ? 0 : otherAllowance.hashCode());
        result = prime * result
                + ((otherDeductions == null) ? 0 : otherDeductions.hashCode());
        result = prime * result
                + ((otherIncome1 == null) ? 0 : otherIncome1.hashCode());
        result = prime * result
                + ((otherIncome2 == null) ? 0 : otherIncome2.hashCode());
        result = prime * result
                + ((otherIncome3 == null) ? 0 : otherIncome3.hashCode());
        result = prime * result
                + ((otherIncome4 == null) ? 0 : otherIncome4.hashCode());
        result = prime * result
                + ((otherIncome5 == null) ? 0 : otherIncome5.hashCode());
        result = prime
                * result
                + ((otherSourceIncome == null) ? 0 : otherSourceIncome
                .hashCode());
        result = prime
                * result
                + ((otherSourceIncomeAmount == null) ? 0
                : otherSourceIncomeAmount.hashCode());
        result = prime
                * result
                + ((otherSourceOfIncome1 == null) ? 0 : otherSourceOfIncome1
                .hashCode());
        result = prime
                * result
                + ((otherSourceOfIncome2 == null) ? 0 : otherSourceOfIncome2
                .hashCode());
        result = prime
                * result
                + ((otherSourceOfIncome3 == null) ? 0 : otherSourceOfIncome3
                .hashCode());
        result = prime
                * result
                + ((otherSourceOfIncome4 == null) ? 0 : otherSourceOfIncome4
                .hashCode());
        result = prime
                * result
                + ((otherSourceOfIncome5 == null) ? 0 : otherSourceOfIncome5
                .hashCode());
        result = prime
                * result
                + ((otherSourcesIncome == null) ? 0 : otherSourcesIncome
                .hashCode());
        result = prime * result
                + ((password == null) ? 0 : password.hashCode());
        result = prime
                * result
                + ((phone1countryCode == null) ? 0 : phone1countryCode
                .hashCode());
        result = prime * result
                + ((phone1extension == null) ? 0 : phone1extension.hashCode());
        result = prime
                * result
                + ((phone1phoneNumber == null) ? 0 : phone1phoneNumber
                .hashCode());
        result = prime * result
                + ((phone1phoneType == null) ? 0 : phone1phoneType.hashCode());
        result = prime
                * result
                + ((phone2countryCode == null) ? 0 : phone2countryCode
                .hashCode());
        result = prime * result
                + ((phone2extension == null) ? 0 : phone2extension.hashCode());
        result = prime
                * result
                + ((phone2phoneNumber == null) ? 0 : phone2phoneNumber
                .hashCode());
        result = prime * result
                + ((phone2phoneType == null) ? 0 : phone2phoneType.hashCode());
        result = prime
                * result
                + ((phone3countryCode == null) ? 0 : phone3countryCode
                .hashCode());
        result = prime * result
                + ((phone3extension == null) ? 0 : phone3extension.hashCode());
        result = prime
                * result
                + ((phone3phoneNumber == null) ? 0 : phone3phoneNumber
                .hashCode());
        result = prime * result
                + ((phone3phoneType == null) ? 0 : phone3phoneType.hashCode());
        result = prime
                * result
                + ((phone4countryCode == null) ? 0 : phone4countryCode
                .hashCode());
        result = prime * result
                + ((phone4extension == null) ? 0 : phone4extension.hashCode());
        result = prime
                * result
                + ((phone4phoneNumber == null) ? 0 : phone4phoneNumber
                .hashCode());
        result = prime * result
                + ((phone4phoneType == null) ? 0 : phone4phoneType.hashCode());
        result = prime
                * result
                + ((phone5countryCode == null) ? 0 : phone5countryCode
                .hashCode());
        result = prime * result
                + ((phone5extension == null) ? 0 : phone5extension.hashCode());
        result = prime
                * result
                + ((phone5phoneNumber == null) ? 0 : phone5phoneNumber
                .hashCode());
        result = prime * result
                + ((phone5phoneType == null) ? 0 : phone5phoneType.hashCode());
        result = prime * result
                + ((professionalTax == null) ? 0 : professionalTax.hashCode());
        result = prime * result
                + ((providentFund == null) ? 0 : providentFund.hashCode());
        result = prime * result + ((refID == null) ? 0 : refID.hashCode());
        result = prime * result + ((remarks == null) ? 0 : remarks.hashCode());
        result = prime
                * result
                + ((repaymentBankName1 == null) ? 0 : repaymentBankName1
                .hashCode());
        result = prime
                * result
                + ((repaymentBankName2 == null) ? 0 : repaymentBankName2
                .hashCode());
        result = prime
                * result
                + ((repaymentBankName3 == null) ? 0 : repaymentBankName3
                .hashCode());
        result = prime
                * result
                + ((repaymentBankName4 == null) ? 0 : repaymentBankName4
                .hashCode());
        result = prime
                * result
                + ((repaymentBankName5 == null) ? 0 : repaymentBankName5
                .hashCode());
        result = prime * result
                + ((requestType == null) ? 0 : requestType.hashCode());
        result = prime
                * result
                + ((salariedSurrogateNetTakeHome1 == null) ? 0
                : salariedSurrogateNetTakeHome1.hashCode());
        result = prime
                * result
                + ((salariedSurrogateNetTakeHome2 == null) ? 0
                : salariedSurrogateNetTakeHome2.hashCode());
        result = prime
                * result
                + ((salariedSurrogateNetTakeHome3 == null) ? 0
                : salariedSurrogateNetTakeHome3.hashCode());
        result = prime
                * result
                + ((salariedSurrogateNetTakeHome4 == null) ? 0
                : salariedSurrogateNetTakeHome4.hashCode());
        result = prime
                * result
                + ((salariedSurrogateNetTakeHome5 == null) ? 0
                : salariedSurrogateNetTakeHome5.hashCode());
        result = prime * result
                + ((sourceId == null) ? 0 : sourceId.hashCode());
        result = prime * result
                + ((stateInsurance == null) ? 0 : stateInsurance.hashCode());
        result = prime
                * result
                + ((traderSurrogateBussinesProof1 == null) ? 0
                : traderSurrogateBussinesProof1.hashCode());
        result = prime
                * result
                + ((traderSurrogateBussinesProof2 == null) ? 0
                : traderSurrogateBussinesProof2.hashCode());
        result = prime
                * result
                + ((traderSurrogateBussinesProof3 == null) ? 0
                : traderSurrogateBussinesProof3.hashCode());
        result = prime
                * result
                + ((traderSurrogateBussinesProof4 == null) ? 0
                : traderSurrogateBussinesProof4.hashCode());
        result = prime
                * result
                + ((traderSurrogateBussinesProof5 == null) ? 0
                : traderSurrogateBussinesProof5.hashCode());
        result = prime
                * result
                + ((traderSurrogateYearInBussines1 == null) ? 0
                : traderSurrogateYearInBussines1.hashCode());
        result = prime
                * result
                + ((traderSurrogateYearInBussines2 == null) ? 0
                : traderSurrogateYearInBussines2.hashCode());
        result = prime
                * result
                + ((traderSurrogateYearInBussines3 == null) ? 0
                : traderSurrogateYearInBussines3.hashCode());
        result = prime
                * result
                + ((traderSurrogateYearInBussines4 == null) ? 0
                : traderSurrogateYearInBussines4.hashCode());
        result = prime
                * result
                + ((traderSurrogateYearInBussines5 == null) ? 0
                : traderSurrogateYearInBussines5.hashCode());
        result = prime * result
                + ((turnOver1 == null) ? 0 : turnOver1.hashCode());
        result = prime * result
                + ((turnOver2 == null) ? 0 : turnOver2.hashCode());
        result = prime * result
                + ((turnOver3 == null) ? 0 : turnOver3.hashCode());
        result = prime * result
                + ((turnOver4 == null) ? 0 : turnOver4.hashCode());
        result = prime * result
                + ((turnOver5 == null) ? 0 : turnOver5.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        result = prime * result + ((year1 == null) ? 0 : year1.hashCode());
        result = prime * result + ((year2 == null) ? 0 : year2.hashCode());
        result = prime * result + ((year3 == null) ? 0 : year3.hashCode());
        result = prime * result + ((year4 == null) ? 0 : year4.hashCode());
        result = prime * result + ((year5 == null) ? 0 : year5.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApplicationTAT other = (ApplicationTAT) obj;
        if (CCA == null) {
            if (other.CCA != null)
                return false;
        } else if (!CCA.equals(other.CCA))
            return false;
        if (DA == null) {
            if (other.DA != null)
                return false;
        } else if (!DA.equals(other.DA))
            return false;
        if (ESI == null) {
            if (other.ESI != null)
                return false;
        } else if (!ESI.equals(other.ESI))
            return false;
        if (HRA == null) {
            if (other.HRA != null)
                return false;
        } else if (!HRA.equals(other.HRA))
            return false;
        if (LIC == null) {
            if (other.LIC != null)
                return false;
        } else if (!LIC.equals(other.LIC))
            return false;
        if (PF == null) {
            if (other.PF != null)
                return false;
        } else if (!PF.equals(other.PF))
            return false;
        if (StringTime == null) {
            if (other.StringTime != null)
                return false;
        } else if (!StringTime.equals(other.StringTime))
            return false;
        if (accountHolderaccountNumber1 == null) {
            if (other.accountHolderaccountNumber1 != null)
                return false;
        } else if (!accountHolderaccountNumber1
                .equals(other.accountHolderaccountNumber1))
            return false;
        if (accountHolderaccountNumber2 == null) {
            if (other.accountHolderaccountNumber2 != null)
                return false;
        } else if (!accountHolderaccountNumber2
                .equals(other.accountHolderaccountNumber2))
            return false;
        if (accountHolderaccountNumber3 == null) {
            if (other.accountHolderaccountNumber3 != null)
                return false;
        } else if (!accountHolderaccountNumber3
                .equals(other.accountHolderaccountNumber3))
            return false;
        if (accountHolderaccountNumber4 == null) {
            if (other.accountHolderaccountNumber4 != null)
                return false;
        } else if (!accountHolderaccountNumber4
                .equals(other.accountHolderaccountNumber4))
            return false;
        if (accountHolderaccountNumber5 == null) {
            if (other.accountHolderaccountNumber5 != null)
                return false;
        } else if (!accountHolderaccountNumber5
                .equals(other.accountHolderaccountNumber5))
            return false;
        if (accountHolderaccountType1 == null) {
            if (other.accountHolderaccountType1 != null)
                return false;
        } else if (!accountHolderaccountType1
                .equals(other.accountHolderaccountType1))
            return false;
        if (accountHolderaccountType2 == null) {
            if (other.accountHolderaccountType2 != null)
                return false;
        } else if (!accountHolderaccountType2
                .equals(other.accountHolderaccountType2))
            return false;
        if (accountHolderaccountType3 == null) {
            if (other.accountHolderaccountType3 != null)
                return false;
        } else if (!accountHolderaccountType3
                .equals(other.accountHolderaccountType3))
            return false;
        if (accountHolderaccountType4 == null) {
            if (other.accountHolderaccountType4 != null)
                return false;
        } else if (!accountHolderaccountType4
                .equals(other.accountHolderaccountType4))
            return false;
        if (accountHolderaccountType5 == null) {
            if (other.accountHolderaccountType5 != null)
                return false;
        } else if (!accountHolderaccountType5
                .equals(other.accountHolderaccountType5))
            return false;
        if (accountHolderanyEmi1 == null) {
            if (other.accountHolderanyEmi1 != null)
                return false;
        } else if (!accountHolderanyEmi1.equals(other.accountHolderanyEmi1))
            return false;
        if (accountHolderanyEmi2 == null) {
            if (other.accountHolderanyEmi2 != null)
                return false;
        } else if (!accountHolderanyEmi2.equals(other.accountHolderanyEmi2))
            return false;
        if (accountHolderanyEmi3 == null) {
            if (other.accountHolderanyEmi3 != null)
                return false;
        } else if (!accountHolderanyEmi3.equals(other.accountHolderanyEmi3))
            return false;
        if (accountHolderanyEmi4 == null) {
            if (other.accountHolderanyEmi4 != null)
                return false;
        } else if (!accountHolderanyEmi4.equals(other.accountHolderanyEmi4))
            return false;
        if (accountHolderanyEmi5 == null) {
            if (other.accountHolderanyEmi5 != null)
                return false;
        } else if (!accountHolderanyEmi5.equals(other.accountHolderanyEmi5))
            return false;
        if (accountHolderavgBankBalance1 == null) {
            if (other.accountHolderavgBankBalance1 != null)
                return false;
        } else if (!accountHolderavgBankBalance1
                .equals(other.accountHolderavgBankBalance1))
            return false;
        if (accountHolderavgBankBalance2 == null) {
            if (other.accountHolderavgBankBalance2 != null)
                return false;
        } else if (!accountHolderavgBankBalance2
                .equals(other.accountHolderavgBankBalance2))
            return false;
        if (accountHolderavgBankBalance3 == null) {
            if (other.accountHolderavgBankBalance3 != null)
                return false;
        } else if (!accountHolderavgBankBalance3
                .equals(other.accountHolderavgBankBalance3))
            return false;
        if (accountHolderavgBankBalance4 == null) {
            if (other.accountHolderavgBankBalance4 != null)
                return false;
        } else if (!accountHolderavgBankBalance4
                .equals(other.accountHolderavgBankBalance4))
            return false;
        if (accountHolderavgBankBalance5 == null) {
            if (other.accountHolderavgBankBalance5 != null)
                return false;
        } else if (!accountHolderavgBankBalance5
                .equals(other.accountHolderavgBankBalance5))
            return false;
        if (accountHolderbankName1 == null) {
            if (other.accountHolderbankName1 != null)
                return false;
        } else if (!accountHolderbankName1.equals(other.accountHolderbankName1))
            return false;
        if (accountHolderbankName2 == null) {
            if (other.accountHolderbankName2 != null)
                return false;
        } else if (!accountHolderbankName2.equals(other.accountHolderbankName2))
            return false;
        if (accountHolderbankName3 == null) {
            if (other.accountHolderbankName3 != null)
                return false;
        } else if (!accountHolderbankName3.equals(other.accountHolderbankName3))
            return false;
        if (accountHolderbankName4 == null) {
            if (other.accountHolderbankName4 != null)
                return false;
        } else if (!accountHolderbankName4.equals(other.accountHolderbankName4))
            return false;
        if (accountHolderbankName5 == null) {
            if (other.accountHolderbankName5 != null)
                return false;
        } else if (!accountHolderbankName5.equals(other.accountHolderbankName5))
            return false;
        if (accountHolderbranchName1 == null) {
            if (other.accountHolderbranchName1 != null)
                return false;
        } else if (!accountHolderbranchName1
                .equals(other.accountHolderbranchName1))
            return false;
        if (accountHolderbranchName2 == null) {
            if (other.accountHolderbranchName2 != null)
                return false;
        } else if (!accountHolderbranchName2
                .equals(other.accountHolderbranchName2))
            return false;
        if (accountHolderbranchName3 == null) {
            if (other.accountHolderbranchName3 != null)
                return false;
        } else if (!accountHolderbranchName3
                .equals(other.accountHolderbranchName3))
            return false;
        if (accountHolderbranchName4 == null) {
            if (other.accountHolderbranchName4 != null)
                return false;
        } else if (!accountHolderbranchName4
                .equals(other.accountHolderbranchName4))
            return false;
        if (accountHolderbranchName5 == null) {
            if (other.accountHolderbranchName5 != null)
                return false;
        } else if (!accountHolderbranchName5
                .equals(other.accountHolderbranchName5))
            return false;
        if (accountHoldercurrentlyRunningLoan1 == null) {
            if (other.accountHoldercurrentlyRunningLoan1 != null)
                return false;
        } else if (!accountHoldercurrentlyRunningLoan1
                .equals(other.accountHoldercurrentlyRunningLoan1))
            return false;
        if (accountHoldercurrentlyRunningLoan2 == null) {
            if (other.accountHoldercurrentlyRunningLoan2 != null)
                return false;
        } else if (!accountHoldercurrentlyRunningLoan2
                .equals(other.accountHoldercurrentlyRunningLoan2))
            return false;
        if (accountHoldercurrentlyRunningLoan3 == null) {
            if (other.accountHoldercurrentlyRunningLoan3 != null)
                return false;
        } else if (!accountHoldercurrentlyRunningLoan3
                .equals(other.accountHoldercurrentlyRunningLoan3))
            return false;
        if (accountHoldercurrentlyRunningLoan4 == null) {
            if (other.accountHoldercurrentlyRunningLoan4 != null)
                return false;
        } else if (!accountHoldercurrentlyRunningLoan4
                .equals(other.accountHoldercurrentlyRunningLoan4))
            return false;
        if (accountHoldercurrentlyRunningLoan5 == null) {
            if (other.accountHoldercurrentlyRunningLoan5 != null)
                return false;
        } else if (!accountHoldercurrentlyRunningLoan5
                .equals(other.accountHoldercurrentlyRunningLoan5))
            return false;
        if (accountHolderdeductedEmiAmt1 == null) {
            if (other.accountHolderdeductedEmiAmt1 != null)
                return false;
        } else if (!accountHolderdeductedEmiAmt1
                .equals(other.accountHolderdeductedEmiAmt1))
            return false;
        if (accountHolderdeductedEmiAmt2 == null) {
            if (other.accountHolderdeductedEmiAmt2 != null)
                return false;
        } else if (!accountHolderdeductedEmiAmt2
                .equals(other.accountHolderdeductedEmiAmt2))
            return false;
        if (accountHolderdeductedEmiAmt3 == null) {
            if (other.accountHolderdeductedEmiAmt3 != null)
                return false;
        } else if (!accountHolderdeductedEmiAmt3
                .equals(other.accountHolderdeductedEmiAmt3))
            return false;
        if (accountHolderdeductedEmiAmt4 == null) {
            if (other.accountHolderdeductedEmiAmt4 != null)
                return false;
        } else if (!accountHolderdeductedEmiAmt4
                .equals(other.accountHolderdeductedEmiAmt4))
            return false;
        if (accountHolderdeductedEmiAmt5 == null) {
            if (other.accountHolderdeductedEmiAmt5 != null)
                return false;
        } else if (!accountHolderdeductedEmiAmt5
                .equals(other.accountHolderdeductedEmiAmt5))
            return false;
        if (accountHolderfirstName1 == null) {
            if (other.accountHolderfirstName1 != null)
                return false;
        } else if (!accountHolderfirstName1
                .equals(other.accountHolderfirstName1))
            return false;
        if (accountHolderfirstName2 == null) {
            if (other.accountHolderfirstName2 != null)
                return false;
        } else if (!accountHolderfirstName2
                .equals(other.accountHolderfirstName2))
            return false;
        if (accountHolderfirstName3 == null) {
            if (other.accountHolderfirstName3 != null)
                return false;
        } else if (!accountHolderfirstName3
                .equals(other.accountHolderfirstName3))
            return false;
        if (accountHolderfirstName4 == null) {
            if (other.accountHolderfirstName4 != null)
                return false;
        } else if (!accountHolderfirstName4
                .equals(other.accountHolderfirstName4))
            return false;
        if (accountHolderfirstName5 == null) {
            if (other.accountHolderfirstName5 != null)
                return false;
        } else if (!accountHolderfirstName5
                .equals(other.accountHolderfirstName5))
            return false;
        if (accountHolderinwardChequeReturn1 == null) {
            if (other.accountHolderinwardChequeReturn1 != null)
                return false;
        } else if (!accountHolderinwardChequeReturn1
                .equals(other.accountHolderinwardChequeReturn1))
            return false;
        if (accountHolderinwardChequeReturn2 == null) {
            if (other.accountHolderinwardChequeReturn2 != null)
                return false;
        } else if (!accountHolderinwardChequeReturn2
                .equals(other.accountHolderinwardChequeReturn2))
            return false;
        if (accountHolderinwardChequeReturn3 == null) {
            if (other.accountHolderinwardChequeReturn3 != null)
                return false;
        } else if (!accountHolderinwardChequeReturn3
                .equals(other.accountHolderinwardChequeReturn3))
            return false;
        if (accountHolderinwardChequeReturn4 == null) {
            if (other.accountHolderinwardChequeReturn4 != null)
                return false;
        } else if (!accountHolderinwardChequeReturn4
                .equals(other.accountHolderinwardChequeReturn4))
            return false;
        if (accountHolderinwardChequeReturn5 == null) {
            if (other.accountHolderinwardChequeReturn5 != null)
                return false;
        } else if (!accountHolderinwardChequeReturn5
                .equals(other.accountHolderinwardChequeReturn5))
            return false;
        if (accountHolderlastName1 == null) {
            if (other.accountHolderlastName1 != null)
                return false;
        } else if (!accountHolderlastName1.equals(other.accountHolderlastName1))
            return false;
        if (accountHolderlastName2 == null) {
            if (other.accountHolderlastName2 != null)
                return false;
        } else if (!accountHolderlastName2.equals(other.accountHolderlastName2))
            return false;
        if (accountHolderlastName3 == null) {
            if (other.accountHolderlastName3 != null)
                return false;
        } else if (!accountHolderlastName3.equals(other.accountHolderlastName3))
            return false;
        if (accountHolderlastName4 == null) {
            if (other.accountHolderlastName4 != null)
                return false;
        } else if (!accountHolderlastName4.equals(other.accountHolderlastName4))
            return false;
        if (accountHolderlastName5 == null) {
            if (other.accountHolderlastName5 != null)
                return false;
        } else if (!accountHolderlastName5.equals(other.accountHolderlastName5))
            return false;
        if (accountHoldermentionAmount1 == null) {
            if (other.accountHoldermentionAmount1 != null)
                return false;
        } else if (!accountHoldermentionAmount1
                .equals(other.accountHoldermentionAmount1))
            return false;
        if (accountHoldermentionAmount2 == null) {
            if (other.accountHoldermentionAmount2 != null)
                return false;
        } else if (!accountHoldermentionAmount2
                .equals(other.accountHoldermentionAmount2))
            return false;
        if (accountHoldermentionAmount3 == null) {
            if (other.accountHoldermentionAmount3 != null)
                return false;
        } else if (!accountHoldermentionAmount3
                .equals(other.accountHoldermentionAmount3))
            return false;
        if (accountHoldermentionAmount4 == null) {
            if (other.accountHoldermentionAmount4 != null)
                return false;
        } else if (!accountHoldermentionAmount4
                .equals(other.accountHoldermentionAmount4))
            return false;
        if (accountHoldermentionAmount5 == null) {
            if (other.accountHoldermentionAmount5 != null)
                return false;
        } else if (!accountHoldermentionAmount5
                .equals(other.accountHoldermentionAmount5))
            return false;
        if (accountHoldermiddleName1 == null) {
            if (other.accountHoldermiddleName1 != null)
                return false;
        } else if (!accountHoldermiddleName1
                .equals(other.accountHoldermiddleName1))
            return false;
        if (accountHoldermiddleName2 == null) {
            if (other.accountHoldermiddleName2 != null)
                return false;
        } else if (!accountHoldermiddleName2
                .equals(other.accountHoldermiddleName2))
            return false;
        if (accountHoldermiddleName3 == null) {
            if (other.accountHoldermiddleName3 != null)
                return false;
        } else if (!accountHoldermiddleName3
                .equals(other.accountHoldermiddleName3))
            return false;
        if (accountHoldermiddleName4 == null) {
            if (other.accountHoldermiddleName4 != null)
                return false;
        } else if (!accountHoldermiddleName4
                .equals(other.accountHoldermiddleName4))
            return false;
        if (accountHoldermiddleName5 == null) {
            if (other.accountHoldermiddleName5 != null)
                return false;
        } else if (!accountHoldermiddleName5
                .equals(other.accountHoldermiddleName5))
            return false;
        if (accountHolderoutwardChequeReturn1 == null) {
            if (other.accountHolderoutwardChequeReturn1 != null)
                return false;
        } else if (!accountHolderoutwardChequeReturn1
                .equals(other.accountHolderoutwardChequeReturn1))
            return false;
        if (accountHolderoutwardChequeReturn2 == null) {
            if (other.accountHolderoutwardChequeReturn2 != null)
                return false;
        } else if (!accountHolderoutwardChequeReturn2
                .equals(other.accountHolderoutwardChequeReturn2))
            return false;
        if (accountHolderoutwardChequeReturn3 == null) {
            if (other.accountHolderoutwardChequeReturn3 != null)
                return false;
        } else if (!accountHolderoutwardChequeReturn3
                .equals(other.accountHolderoutwardChequeReturn3))
            return false;
        if (accountHolderoutwardChequeReturn4 == null) {
            if (other.accountHolderoutwardChequeReturn4 != null)
                return false;
        } else if (!accountHolderoutwardChequeReturn4
                .equals(other.accountHolderoutwardChequeReturn4))
            return false;
        if (accountHolderoutwardChequeReturn5 == null) {
            if (other.accountHolderoutwardChequeReturn5 != null)
                return false;
        } else if (!accountHolderoutwardChequeReturn5
                .equals(other.accountHolderoutwardChequeReturn5))
            return false;
        if (accountHolderprefix1 == null) {
            if (other.accountHolderprefix1 != null)
                return false;
        } else if (!accountHolderprefix1.equals(other.accountHolderprefix1))
            return false;
        if (accountHolderprefix2 == null) {
            if (other.accountHolderprefix2 != null)
                return false;
        } else if (!accountHolderprefix2.equals(other.accountHolderprefix2))
            return false;
        if (accountHolderprefix3 == null) {
            if (other.accountHolderprefix3 != null)
                return false;
        } else if (!accountHolderprefix3.equals(other.accountHolderprefix3))
            return false;
        if (accountHolderprefix4 == null) {
            if (other.accountHolderprefix4 != null)
                return false;
        } else if (!accountHolderprefix4.equals(other.accountHolderprefix4))
            return false;
        if (accountHolderprefix5 == null) {
            if (other.accountHolderprefix5 != null)
                return false;
        } else if (!accountHolderprefix5.equals(other.accountHolderprefix5))
            return false;
        if (accountHoldersalaryAccount1 == null) {
            if (other.accountHoldersalaryAccount1 != null)
                return false;
        } else if (!accountHoldersalaryAccount1
                .equals(other.accountHoldersalaryAccount1))
            return false;
        if (accountHoldersalaryAccount2 == null) {
            if (other.accountHoldersalaryAccount2 != null)
                return false;
        } else if (!accountHoldersalaryAccount2
                .equals(other.accountHoldersalaryAccount2))
            return false;
        if (accountHoldersalaryAccount3 == null) {
            if (other.accountHoldersalaryAccount3 != null)
                return false;
        } else if (!accountHoldersalaryAccount3
                .equals(other.accountHoldersalaryAccount3))
            return false;
        if (accountHoldersalaryAccount4 == null) {
            if (other.accountHoldersalaryAccount4 != null)
                return false;
        } else if (!accountHoldersalaryAccount4
                .equals(other.accountHoldersalaryAccount4))
            return false;
        if (accountHoldersalaryAccount5 == null) {
            if (other.accountHoldersalaryAccount5 != null)
                return false;
        } else if (!accountHoldersalaryAccount5
                .equals(other.accountHoldersalaryAccount5))
            return false;
        if (accountHoldersuffix1 == null) {
            if (other.accountHoldersuffix1 != null)
                return false;
        } else if (!accountHoldersuffix1.equals(other.accountHoldersuffix1))
            return false;
        if (accountHoldersuffix2 == null) {
            if (other.accountHoldersuffix2 != null)
                return false;
        } else if (!accountHoldersuffix2.equals(other.accountHoldersuffix2))
            return false;
        if (accountHoldersuffix3 == null) {
            if (other.accountHoldersuffix3 != null)
                return false;
        } else if (!accountHoldersuffix3.equals(other.accountHoldersuffix3))
            return false;
        if (accountHoldersuffix4 == null) {
            if (other.accountHoldersuffix4 != null)
                return false;
        } else if (!accountHoldersuffix4.equals(other.accountHoldersuffix4))
            return false;
        if (accountHoldersuffix5 == null) {
            if (other.accountHoldersuffix5 != null)
                return false;
        } else if (!accountHoldersuffix5.equals(other.accountHoldersuffix5))
            return false;
        if (add1accommodation == null) {
            if (other.add1accommodation != null)
                return false;
        } else if (!add1accommodation.equals(other.add1accommodation))
            return false;
        if (add1addressLine1 == null) {
            if (other.add1addressLine1 != null)
                return false;
        } else if (!add1addressLine1.equals(other.add1addressLine1))
            return false;
        if (add1addressLine2 == null) {
            if (other.add1addressLine2 != null)
                return false;
        } else if (!add1addressLine2.equals(other.add1addressLine2))
            return false;
        if (add1addressType == null) {
            if (other.add1addressType != null)
                return false;
        } else if (!add1addressType.equals(other.add1addressType))
            return false;
        if (add1city == null) {
            if (other.add1city != null)
                return false;
        } else if (!add1city.equals(other.add1city))
            return false;
        if (add1country == null) {
            if (other.add1country != null)
                return false;
        } else if (!add1country.equals(other.add1country))
            return false;
        if (add1distanceFrom == null) {
            if (other.add1distanceFrom != null)
                return false;
        } else if (!add1distanceFrom.equals(other.add1distanceFrom))
            return false;
        if (add1district == null) {
            if (other.add1district != null)
                return false;
        } else if (!add1district.equals(other.add1district))
            return false;
        if (add1landLoard == null) {
            if (other.add1landLoard != null)
                return false;
        } else if (!add1landLoard.equals(other.add1landLoard))
            return false;
        if (add1landMark == null) {
            if (other.add1landMark != null)
                return false;
        } else if (!add1landMark.equals(other.add1landMark))
            return false;
        if (add1line3 == null) {
            if (other.add1line3 != null)
                return false;
        } else if (!add1line3.equals(other.add1line3))
            return false;
        if (add1line4 == null) {
            if (other.add1line4 != null)
                return false;
        } else if (!add1line4.equals(other.add1line4))
            return false;
        if (add1monthAtAddress == null) {
            if (other.add1monthAtAddress != null)
                return false;
        } else if (!add1monthAtAddress.equals(other.add1monthAtAddress))
            return false;
        if (add1monthAtCity == null) {
            if (other.add1monthAtCity != null)
                return false;
        } else if (!add1monthAtCity.equals(other.add1monthAtCity))
            return false;
        if (add1pin == null) {
            if (other.add1pin != null)
                return false;
        } else if (!add1pin.equals(other.add1pin))
            return false;
        if (add1rentAmount == null) {
            if (other.add1rentAmount != null)
                return false;
        } else if (!add1rentAmount.equals(other.add1rentAmount))
            return false;
        if (add1residenceAddressType == null) {
            if (other.add1residenceAddressType != null)
                return false;
        } else if (!add1residenceAddressType
                .equals(other.add1residenceAddressType))
            return false;
        if (add1state == null) {
            if (other.add1state != null)
                return false;
        } else if (!add1state.equals(other.add1state))
            return false;
        if (add1timeAtAddress == null) {
            if (other.add1timeAtAddress != null)
                return false;
        } else if (!add1timeAtAddress.equals(other.add1timeAtAddress))
            return false;
        if (add1village == null) {
            if (other.add1village != null)
                return false;
        } else if (!add1village.equals(other.add1village))
            return false;
        if (add1yearAtCity == null) {
            if (other.add1yearAtCity != null)
                return false;
        } else if (!add1yearAtCity.equals(other.add1yearAtCity))
            return false;
        if (add2accommodation == null) {
            if (other.add2accommodation != null)
                return false;
        } else if (!add2accommodation.equals(other.add2accommodation))
            return false;
        if (add2addressLine1 == null) {
            if (other.add2addressLine1 != null)
                return false;
        } else if (!add2addressLine1.equals(other.add2addressLine1))
            return false;
        if (add2addressLine2 == null) {
            if (other.add2addressLine2 != null)
                return false;
        } else if (!add2addressLine2.equals(other.add2addressLine2))
            return false;
        if (add2addressType == null) {
            if (other.add2addressType != null)
                return false;
        } else if (!add2addressType.equals(other.add2addressType))
            return false;
        if (add2city == null) {
            if (other.add2city != null)
                return false;
        } else if (!add2city.equals(other.add2city))
            return false;
        if (add2country == null) {
            if (other.add2country != null)
                return false;
        } else if (!add2country.equals(other.add2country))
            return false;
        if (add2distanceFrom == null) {
            if (other.add2distanceFrom != null)
                return false;
        } else if (!add2distanceFrom.equals(other.add2distanceFrom))
            return false;
        if (add2district == null) {
            if (other.add2district != null)
                return false;
        } else if (!add2district.equals(other.add2district))
            return false;
        if (add2landLoard == null) {
            if (other.add2landLoard != null)
                return false;
        } else if (!add2landLoard.equals(other.add2landLoard))
            return false;
        if (add2landMark == null) {
            if (other.add2landMark != null)
                return false;
        } else if (!add2landMark.equals(other.add2landMark))
            return false;
        if (add2line3 == null) {
            if (other.add2line3 != null)
                return false;
        } else if (!add2line3.equals(other.add2line3))
            return false;
        if (add2line4 == null) {
            if (other.add2line4 != null)
                return false;
        } else if (!add2line4.equals(other.add2line4))
            return false;
        if (add2monthAtAddress == null) {
            if (other.add2monthAtAddress != null)
                return false;
        } else if (!add2monthAtAddress.equals(other.add2monthAtAddress))
            return false;
        if (add2monthAtCity == null) {
            if (other.add2monthAtCity != null)
                return false;
        } else if (!add2monthAtCity.equals(other.add2monthAtCity))
            return false;
        if (add2pin == null) {
            if (other.add2pin != null)
                return false;
        } else if (!add2pin.equals(other.add2pin))
            return false;
        if (add2rentAmount == null) {
            if (other.add2rentAmount != null)
                return false;
        } else if (!add2rentAmount.equals(other.add2rentAmount))
            return false;
        if (add2residenceAddressType == null) {
            if (other.add2residenceAddressType != null)
                return false;
        } else if (!add2residenceAddressType
                .equals(other.add2residenceAddressType))
            return false;
        if (add2state == null) {
            if (other.add2state != null)
                return false;
        } else if (!add2state.equals(other.add2state))
            return false;
        if (add2timeAtAddress == null) {
            if (other.add2timeAtAddress != null)
                return false;
        } else if (!add2timeAtAddress.equals(other.add2timeAtAddress))
            return false;
        if (add2village == null) {
            if (other.add2village != null)
                return false;
        } else if (!add2village.equals(other.add2village))
            return false;
        if (add2yearAtCity == null) {
            if (other.add2yearAtCity != null)
                return false;
        } else if (!add2yearAtCity.equals(other.add2yearAtCity))
            return false;
        if (add3accommodation == null) {
            if (other.add3accommodation != null)
                return false;
        } else if (!add3accommodation.equals(other.add3accommodation))
            return false;
        if (add3addressLine1 == null) {
            if (other.add3addressLine1 != null)
                return false;
        } else if (!add3addressLine1.equals(other.add3addressLine1))
            return false;
        if (add3addressLine2 == null) {
            if (other.add3addressLine2 != null)
                return false;
        } else if (!add3addressLine2.equals(other.add3addressLine2))
            return false;
        if (add3addressType == null) {
            if (other.add3addressType != null)
                return false;
        } else if (!add3addressType.equals(other.add3addressType))
            return false;
        if (add3city == null) {
            if (other.add3city != null)
                return false;
        } else if (!add3city.equals(other.add3city))
            return false;
        if (add3country == null) {
            if (other.add3country != null)
                return false;
        } else if (!add3country.equals(other.add3country))
            return false;
        if (add3distanceFrom == null) {
            if (other.add3distanceFrom != null)
                return false;
        } else if (!add3distanceFrom.equals(other.add3distanceFrom))
            return false;
        if (add3district == null) {
            if (other.add3district != null)
                return false;
        } else if (!add3district.equals(other.add3district))
            return false;
        if (add3landLoard == null) {
            if (other.add3landLoard != null)
                return false;
        } else if (!add3landLoard.equals(other.add3landLoard))
            return false;
        if (add3landMark == null) {
            if (other.add3landMark != null)
                return false;
        } else if (!add3landMark.equals(other.add3landMark))
            return false;
        if (add3line3 == null) {
            if (other.add3line3 != null)
                return false;
        } else if (!add3line3.equals(other.add3line3))
            return false;
        if (add3line4 == null) {
            if (other.add3line4 != null)
                return false;
        } else if (!add3line4.equals(other.add3line4))
            return false;
        if (add3monthAtAddress == null) {
            if (other.add3monthAtAddress != null)
                return false;
        } else if (!add3monthAtAddress.equals(other.add3monthAtAddress))
            return false;
        if (add3monthAtCity == null) {
            if (other.add3monthAtCity != null)
                return false;
        } else if (!add3monthAtCity.equals(other.add3monthAtCity))
            return false;
        if (add3pin == null) {
            if (other.add3pin != null)
                return false;
        } else if (!add3pin.equals(other.add3pin))
            return false;
        if (add3rentAmount == null) {
            if (other.add3rentAmount != null)
                return false;
        } else if (!add3rentAmount.equals(other.add3rentAmount))
            return false;
        if (add3residenceAddressType == null) {
            if (other.add3residenceAddressType != null)
                return false;
        } else if (!add3residenceAddressType
                .equals(other.add3residenceAddressType))
            return false;
        if (add3state == null) {
            if (other.add3state != null)
                return false;
        } else if (!add3state.equals(other.add3state))
            return false;
        if (add3timeAtAddress == null) {
            if (other.add3timeAtAddress != null)
                return false;
        } else if (!add3timeAtAddress.equals(other.add3timeAtAddress))
            return false;
        if (add3village == null) {
            if (other.add3village != null)
                return false;
        } else if (!add3village.equals(other.add3village))
            return false;
        if (add3yearAtCity == null) {
            if (other.add3yearAtCity != null)
                return false;
        } else if (!add3yearAtCity.equals(other.add3yearAtCity))
            return false;
        if (add4accommodation == null) {
            if (other.add4accommodation != null)
                return false;
        } else if (!add4accommodation.equals(other.add4accommodation))
            return false;
        if (add4addressLine1 == null) {
            if (other.add4addressLine1 != null)
                return false;
        } else if (!add4addressLine1.equals(other.add4addressLine1))
            return false;
        if (add4addressLine2 == null) {
            if (other.add4addressLine2 != null)
                return false;
        } else if (!add4addressLine2.equals(other.add4addressLine2))
            return false;
        if (add4addressType == null) {
            if (other.add4addressType != null)
                return false;
        } else if (!add4addressType.equals(other.add4addressType))
            return false;
        if (add4city == null) {
            if (other.add4city != null)
                return false;
        } else if (!add4city.equals(other.add4city))
            return false;
        if (add4country == null) {
            if (other.add4country != null)
                return false;
        } else if (!add4country.equals(other.add4country))
            return false;
        if (add4distanceFrom == null) {
            if (other.add4distanceFrom != null)
                return false;
        } else if (!add4distanceFrom.equals(other.add4distanceFrom))
            return false;
        if (add4district == null) {
            if (other.add4district != null)
                return false;
        } else if (!add4district.equals(other.add4district))
            return false;
        if (add4landLoard == null) {
            if (other.add4landLoard != null)
                return false;
        } else if (!add4landLoard.equals(other.add4landLoard))
            return false;
        if (add4landMark == null) {
            if (other.add4landMark != null)
                return false;
        } else if (!add4landMark.equals(other.add4landMark))
            return false;
        if (add4line3 == null) {
            if (other.add4line3 != null)
                return false;
        } else if (!add4line3.equals(other.add4line3))
            return false;
        if (add4line4 == null) {
            if (other.add4line4 != null)
                return false;
        } else if (!add4line4.equals(other.add4line4))
            return false;
        if (add4monthAtAddress == null) {
            if (other.add4monthAtAddress != null)
                return false;
        } else if (!add4monthAtAddress.equals(other.add4monthAtAddress))
            return false;
        if (add4monthAtCity == null) {
            if (other.add4monthAtCity != null)
                return false;
        } else if (!add4monthAtCity.equals(other.add4monthAtCity))
            return false;
        if (add4pin == null) {
            if (other.add4pin != null)
                return false;
        } else if (!add4pin.equals(other.add4pin))
            return false;
        if (add4rentAmount == null) {
            if (other.add4rentAmount != null)
                return false;
        } else if (!add4rentAmount.equals(other.add4rentAmount))
            return false;
        if (add4residenceAddressType == null) {
            if (other.add4residenceAddressType != null)
                return false;
        } else if (!add4residenceAddressType
                .equals(other.add4residenceAddressType))
            return false;
        if (add4state == null) {
            if (other.add4state != null)
                return false;
        } else if (!add4state.equals(other.add4state))
            return false;
        if (add4timeAtAddress == null) {
            if (other.add4timeAtAddress != null)
                return false;
        } else if (!add4timeAtAddress.equals(other.add4timeAtAddress))
            return false;
        if (add4village == null) {
            if (other.add4village != null)
                return false;
        } else if (!add4village.equals(other.add4village))
            return false;
        if (add4yearAtCity == null) {
            if (other.add4yearAtCity != null)
                return false;
        } else if (!add4yearAtCity.equals(other.add4yearAtCity))
            return false;
        if (add5accommodation == null) {
            if (other.add5accommodation != null)
                return false;
        } else if (!add5accommodation.equals(other.add5accommodation))
            return false;
        if (add5addressType == null) {
            if (other.add5addressType != null)
                return false;
        } else if (!add5addressType.equals(other.add5addressType))
            return false;
        if (add5country == null) {
            if (other.add5country != null)
                return false;
        } else if (!add5country.equals(other.add5country))
            return false;
        if (add5distanceFrom == null) {
            if (other.add5distanceFrom != null)
                return false;
        } else if (!add5distanceFrom.equals(other.add5distanceFrom))
            return false;
        if (add5district == null) {
            if (other.add5district != null)
                return false;
        } else if (!add5district.equals(other.add5district))
            return false;
        if (add5landLoard == null) {
            if (other.add5landLoard != null)
                return false;
        } else if (!add5landLoard.equals(other.add5landLoard))
            return false;
        if (add5landMark == null) {
            if (other.add5landMark != null)
                return false;
        } else if (!add5landMark.equals(other.add5landMark))
            return false;
        if (add5line3 == null) {
            if (other.add5line3 != null)
                return false;
        } else if (!add5line3.equals(other.add5line3))
            return false;
        if (add5line4 == null) {
            if (other.add5line4 != null)
                return false;
        } else if (!add5line4.equals(other.add5line4))
            return false;
        if (add5monthAtAddress == null) {
            if (other.add5monthAtAddress != null)
                return false;
        } else if (!add5monthAtAddress.equals(other.add5monthAtAddress))
            return false;
        if (add5monthAtCity == null) {
            if (other.add5monthAtCity != null)
                return false;
        } else if (!add5monthAtCity.equals(other.add5monthAtCity))
            return false;
        if (add5pin == null) {
            if (other.add5pin != null)
                return false;
        } else if (!add5pin.equals(other.add5pin))
            return false;
        if (add5rentAmount == null) {
            if (other.add5rentAmount != null)
                return false;
        } else if (!add5rentAmount.equals(other.add5rentAmount))
            return false;
        if (add5residenceAddressType == null) {
            if (other.add5residenceAddressType != null)
                return false;
        } else if (!add5residenceAddressType
                .equals(other.add5residenceAddressType))
            return false;
        if (add5state == null) {
            if (other.add5state != null)
                return false;
        } else if (!add5state.equals(other.add5state))
            return false;
        if (add5timeAtAddress == null) {
            if (other.add5timeAtAddress != null)
                return false;
        } else if (!add5timeAtAddress.equals(other.add5timeAtAddress))
            return false;
        if (add5village == null) {
            if (other.add5village != null)
                return false;
        } else if (!add5village.equals(other.add5village))
            return false;
        if (add5yearAtCity == null) {
            if (other.add5yearAtCity != null)
                return false;
        } else if (!add5yearAtCity.equals(other.add5yearAtCity))
            return false;
        if (addharVerified == null) {
            if (other.addharVerified != null)
                return false;
        } else if (!addharVerified.equals(other.addharVerified))
            return false;
        if (applicantAge == null) {
            if (other.applicantAge != null)
                return false;
        } else if (!applicantAge.equals(other.applicantAge))
            return false;
        if (applicantFatherFirstName == null) {
            if (other.applicantFatherFirstName != null)
                return false;
        } else if (!applicantFatherFirstName
                .equals(other.applicantFatherFirstName))
            return false;
        if (applicantFatherLastName == null) {
            if (other.applicantFatherLastName != null)
                return false;
        } else if (!applicantFatherLastName
                .equals(other.applicantFatherLastName))
            return false;
        if (applicantFatherMiddleName == null) {
            if (other.applicantFatherMiddleName != null)
                return false;
        } else if (!applicantFatherMiddleName
                .equals(other.applicantFatherMiddleName))
            return false;
        if (applicantFatherPrefix == null) {
            if (other.applicantFatherPrefix != null)
                return false;
        } else if (!applicantFatherPrefix.equals(other.applicantFatherPrefix))
            return false;
        if (applicantFatherSuffix == null) {
            if (other.applicantFatherSuffix != null)
                return false;
        } else if (!applicantFatherSuffix.equals(other.applicantFatherSuffix))
            return false;
        if (applicantFirstName == null) {
            if (other.applicantFirstName != null)
                return false;
        } else if (!applicantFirstName.equals(other.applicantFirstName))
            return false;
        if (applicantGender == null) {
            if (other.applicantGender != null)
                return false;
        } else if (!applicantGender.equals(other.applicantGender))
            return false;
        if (applicantId == null) {
            if (other.applicantId != null)
                return false;
        } else if (!applicantId.equals(other.applicantId))
            return false;
        if (applicantLastName == null) {
            if (other.applicantLastName != null)
                return false;
        } else if (!applicantLastName.equals(other.applicantLastName))
            return false;
        if (applicantMaritalStatus == null) {
            if (other.applicantMaritalStatus != null)
                return false;
        } else if (!applicantMaritalStatus.equals(other.applicantMaritalStatus))
            return false;
        if (applicantMiddleName == null) {
            if (other.applicantMiddleName != null)
                return false;
        } else if (!applicantMiddleName.equals(other.applicantMiddleName))
            return false;
        if (applicantPrefix == null) {
            if (other.applicantPrefix != null)
                return false;
        } else if (!applicantPrefix.equals(other.applicantPrefix))
            return false;
        if (applicantReferenceFirstName == null) {
            if (other.applicantReferenceFirstName != null)
                return false;
        } else if (!applicantReferenceFirstName
                .equals(other.applicantReferenceFirstName))
            return false;
        if (applicantReferenceaddressLine1 == null) {
            if (other.applicantReferenceaddressLine1 != null)
                return false;
        } else if (!applicantReferenceaddressLine1
                .equals(other.applicantReferenceaddressLine1))
            return false;
        if (applicantReferenceaddressLine2 == null) {
            if (other.applicantReferenceaddressLine2 != null)
                return false;
        } else if (!applicantReferenceaddressLine2
                .equals(other.applicantReferenceaddressLine2))
            return false;
        if (applicantReferenceareaCode == null) {
            if (other.applicantReferenceareaCode != null)
                return false;
        } else if (!applicantReferenceareaCode
                .equals(other.applicantReferenceareaCode))
            return false;
        if (applicantReferencecity == null) {
            if (other.applicantReferencecity != null)
                return false;
        } else if (!applicantReferencecity.equals(other.applicantReferencecity))
            return false;
        if (applicantReferencecountry == null) {
            if (other.applicantReferencecountry != null)
                return false;
        } else if (!applicantReferencecountry
                .equals(other.applicantReferencecountry))
            return false;
        if (applicantReferencecountryCode == null) {
            if (other.applicantReferencecountryCode != null)
                return false;
        } else if (!applicantReferencecountryCode
                .equals(other.applicantReferencecountryCode))
            return false;
        if (applicantReferencedistanceFrom == null) {
            if (other.applicantReferencedistanceFrom != null)
                return false;
        } else if (!applicantReferencedistanceFrom
                .equals(other.applicantReferencedistanceFrom))
            return false;
        if (applicantReferencedistrict == null) {
            if (other.applicantReferencedistrict != null)
                return false;
        } else if (!applicantReferencedistrict
                .equals(other.applicantReferencedistrict))
            return false;
        if (applicantReferenceextension == null) {
            if (other.applicantReferenceextension != null)
                return false;
        } else if (!applicantReferenceextension
                .equals(other.applicantReferenceextension))
            return false;
        if (applicantReferencelandLoard == null) {
            if (other.applicantReferencelandLoard != null)
                return false;
        } else if (!applicantReferencelandLoard
                .equals(other.applicantReferencelandLoard))
            return false;
        if (applicantReferencelandMark == null) {
            if (other.applicantReferencelandMark != null)
                return false;
        } else if (!applicantReferencelandMark
                .equals(other.applicantReferencelandMark))
            return false;
        if (applicantReferencelastName == null) {
            if (other.applicantReferencelastName != null)
                return false;
        } else if (!applicantReferencelastName
                .equals(other.applicantReferencelastName))
            return false;
        if (applicantReferenceline3 == null) {
            if (other.applicantReferenceline3 != null)
                return false;
        } else if (!applicantReferenceline3
                .equals(other.applicantReferenceline3))
            return false;
        if (applicantReferenceline4 == null) {
            if (other.applicantReferenceline4 != null)
                return false;
        } else if (!applicantReferenceline4
                .equals(other.applicantReferenceline4))
            return false;
        if (applicantReferencemiddleName == null) {
            if (other.applicantReferencemiddleName != null)
                return false;
        } else if (!applicantReferencemiddleName
                .equals(other.applicantReferencemiddleName))
            return false;
        if (applicantReferenceoccupation == null) {
            if (other.applicantReferenceoccupation != null)
                return false;
        } else if (!applicantReferenceoccupation
                .equals(other.applicantReferenceoccupation))
            return false;
        if (applicantReferencephoneNumber == null) {
            if (other.applicantReferencephoneNumber != null)
                return false;
        } else if (!applicantReferencephoneNumber
                .equals(other.applicantReferencephoneNumber))
            return false;
        if (applicantReferencephoneType == null) {
            if (other.applicantReferencephoneType != null)
                return false;
        } else if (!applicantReferencephoneType
                .equals(other.applicantReferencephoneType))
            return false;
        if (applicantReferencepin == null) {
            if (other.applicantReferencepin != null)
                return false;
        } else if (!applicantReferencepin.equals(other.applicantReferencepin))
            return false;
        if (applicantReferenceprefix == null) {
            if (other.applicantReferenceprefix != null)
                return false;
        } else if (!applicantReferenceprefix
                .equals(other.applicantReferenceprefix))
            return false;
        if (applicantReferencerelationType == null) {
            if (other.applicantReferencerelationType != null)
                return false;
        } else if (!applicantReferencerelationType
                .equals(other.applicantReferencerelationType))
            return false;
        if (applicantReferencestate == null) {
            if (other.applicantReferencestate != null)
                return false;
        } else if (!applicantReferencestate
                .equals(other.applicantReferencestate))
            return false;
        if (applicantReferencesuffix == null) {
            if (other.applicantReferencesuffix != null)
                return false;
        } else if (!applicantReferencesuffix
                .equals(other.applicantReferencesuffix))
            return false;
        if (applicantReferencevillage == null) {
            if (other.applicantReferencevillage != null)
                return false;
        } else if (!applicantReferencevillage
                .equals(other.applicantReferencevillage))
            return false;
        if (applicantReligion == null) {
            if (other.applicantReligion != null)
                return false;
        } else if (!applicantReligion.equals(other.applicantReligion))
            return false;
        if (applicantSpouseFirstName == null) {
            if (other.applicantSpouseFirstName != null)
                return false;
        } else if (!applicantSpouseFirstName
                .equals(other.applicantSpouseFirstName))
            return false;
        if (applicantSpouseLastName == null) {
            if (other.applicantSpouseLastName != null)
                return false;
        } else if (!applicantSpouseLastName
                .equals(other.applicantSpouseLastName))
            return false;
        if (applicantSpouseMiddleName == null) {
            if (other.applicantSpouseMiddleName != null)
                return false;
        } else if (!applicantSpouseMiddleName
                .equals(other.applicantSpouseMiddleName))
            return false;
        if (applicantSpousePrefix == null) {
            if (other.applicantSpousePrefix != null)
                return false;
        } else if (!applicantSpousePrefix.equals(other.applicantSpousePrefix))
            return false;
        if (applicantSpouseSuffix == null) {
            if (other.applicantSpouseSuffix != null)
                return false;
        } else if (!applicantSpouseSuffix.equals(other.applicantSpouseSuffix))
            return false;
        if (applicantStringOfBirth == null) {
            if (other.applicantStringOfBirth != null)
                return false;
        } else if (!applicantStringOfBirth.equals(other.applicantStringOfBirth))
            return false;
        if (applicantSuffix == null) {
            if (other.applicantSuffix != null)
                return false;
        } else if (!applicantSuffix.equals(other.applicantSuffix))
            return false;
        if (applicationId == null) {
            if (other.applicationId != null)
                return false;
        } else if (!applicationId.equals(other.applicationId))
            return false;
        if (applicationSource == null) {
            if (other.applicationSource != null)
                return false;
        } else if (!applicationSource.equals(other.applicationSource))
            return false;
        if (balanceTenure1 == null) {
            if (other.balanceTenure1 != null)
                return false;
        } else if (!balanceTenure1.equals(other.balanceTenure1))
            return false;
        if (balanceTenure2 == null) {
            if (other.balanceTenure2 != null)
                return false;
        } else if (!balanceTenure2.equals(other.balanceTenure2))
            return false;
        if (balanceTenure3 == null) {
            if (other.balanceTenure3 != null)
                return false;
        } else if (!balanceTenure3.equals(other.balanceTenure3))
            return false;
        if (balanceTenure4 == null) {
            if (other.balanceTenure4 != null)
                return false;
        } else if (!balanceTenure4.equals(other.balanceTenure4))
            return false;
        if (balanceTenure5 == null) {
            if (other.balanceTenure5 != null)
                return false;
        } else if (!balanceTenure5.equals(other.balanceTenure5))
            return false;
        if (basic == null) {
            if (other.basic != null)
                return false;
        } else if (!basic.equals(other.basic))
            return false;
        if (basicIncome == null) {
            if (other.basicIncome != null)
                return false;
        } else if (!basicIncome.equals(other.basicIncome))
            return false;
        if (carSurrogateManufactureYear1 == null) {
            if (other.carSurrogateManufactureYear1 != null)
                return false;
        } else if (!carSurrogateManufactureYear1
                .equals(other.carSurrogateManufactureYear1))
            return false;
        if (carSurrogateManufactureYear2 == null) {
            if (other.carSurrogateManufactureYear2 != null)
                return false;
        } else if (!carSurrogateManufactureYear2
                .equals(other.carSurrogateManufactureYear2))
            return false;
        if (carSurrogateManufactureYear3 == null) {
            if (other.carSurrogateManufactureYear3 != null)
                return false;
        } else if (!carSurrogateManufactureYear3
                .equals(other.carSurrogateManufactureYear3))
            return false;
        if (carSurrogateManufactureYear4 == null) {
            if (other.carSurrogateManufactureYear4 != null)
                return false;
        } else if (!carSurrogateManufactureYear4
                .equals(other.carSurrogateManufactureYear4))
            return false;
        if (carSurrogateManufactureYear5 == null) {
            if (other.carSurrogateManufactureYear5 != null)
                return false;
        } else if (!carSurrogateManufactureYear5
                .equals(other.carSurrogateManufactureYear5))
            return false;
        if (carSurrogateModelName1 == null) {
            if (other.carSurrogateModelName1 != null)
                return false;
        } else if (!carSurrogateModelName1.equals(other.carSurrogateModelName1))
            return false;
        if (carSurrogateModelName2 == null) {
            if (other.carSurrogateModelName2 != null)
                return false;
        } else if (!carSurrogateModelName2.equals(other.carSurrogateModelName2))
            return false;
        if (carSurrogateModelName3 == null) {
            if (other.carSurrogateModelName3 != null)
                return false;
        } else if (!carSurrogateModelName3.equals(other.carSurrogateModelName3))
            return false;
        if (carSurrogateModelName4 == null) {
            if (other.carSurrogateModelName4 != null)
                return false;
        } else if (!carSurrogateModelName4.equals(other.carSurrogateModelName4))
            return false;
        if (carSurrogateModelName5 == null) {
            if (other.carSurrogateModelName5 != null)
                return false;
        } else if (!carSurrogateModelName5.equals(other.carSurrogateModelName5))
            return false;
        if (carSurrogateRegistrationNumber1 == null) {
            if (other.carSurrogateRegistrationNumber1 != null)
                return false;
        } else if (!carSurrogateRegistrationNumber1
                .equals(other.carSurrogateRegistrationNumber1))
            return false;
        if (carSurrogateRegistrationNumber2 == null) {
            if (other.carSurrogateRegistrationNumber2 != null)
                return false;
        } else if (!carSurrogateRegistrationNumber2
                .equals(other.carSurrogateRegistrationNumber2))
            return false;
        if (carSurrogateRegistrationNumber3 == null) {
            if (other.carSurrogateRegistrationNumber3 != null)
                return false;
        } else if (!carSurrogateRegistrationNumber3
                .equals(other.carSurrogateRegistrationNumber3))
            return false;
        if (carSurrogateRegistrationNumber4 == null) {
            if (other.carSurrogateRegistrationNumber4 != null)
                return false;
        } else if (!carSurrogateRegistrationNumber4
                .equals(other.carSurrogateRegistrationNumber4))
            return false;
        if (carSurrogateRegistrationNumber5 == null) {
            if (other.carSurrogateRegistrationNumber5 != null)
                return false;
        } else if (!carSurrogateRegistrationNumber5
                .equals(other.carSurrogateRegistrationNumber5))
            return false;
        if (cityCompensatoryAllowance == null) {
            if (other.cityCompensatoryAllowance != null)
                return false;
        } else if (!cityCompensatoryAllowance
                .equals(other.cityCompensatoryAllowance))
            return false;
        if (creditCardNumber == null) {
            if (other.creditCardNumber != null)
                return false;
        } else if (!creditCardNumber.equals(other.creditCardNumber))
            return false;
        if (creditGranter1 == null) {
            if (other.creditGranter1 != null)
                return false;
        } else if (!creditGranter1.equals(other.creditGranter1))
            return false;
        if (creditGranter2 == null) {
            if (other.creditGranter2 != null)
                return false;
        } else if (!creditGranter2.equals(other.creditGranter2))
            return false;
        if (creditGranter3 == null) {
            if (other.creditGranter3 != null)
                return false;
        } else if (!creditGranter3.equals(other.creditGranter3))
            return false;
        if (creditGranter4 == null) {
            if (other.creditGranter4 != null)
                return false;
        } else if (!creditGranter4.equals(other.creditGranter4))
            return false;
        if (creditGranter5 == null) {
            if (other.creditGranter5 != null)
                return false;
        } else if (!creditGranter5.equals(other.creditGranter5))
            return false;
        if (croId == null) {
            if (other.croId != null)
                return false;
        } else if (!croId.equals(other.croId))
            return false;
        if (dealerId == null) {
            if (other.dealerId != null)
                return false;
        } else if (!dealerId.equals(other.dealerId))
            return false;
        if (dearnessAllowance == null) {
            if (other.dearnessAllowance != null)
                return false;
        } else if (!dearnessAllowance.equals(other.dearnessAllowance))
            return false;
        if (depreciation1 == null) {
            if (other.depreciation1 != null)
                return false;
        } else if (!depreciation1.equals(other.depreciation1))
            return false;
        if (depreciation2 == null) {
            if (other.depreciation2 != null)
                return false;
        } else if (!depreciation2.equals(other.depreciation2))
            return false;
        if (depreciation3 == null) {
            if (other.depreciation3 != null)
                return false;
        } else if (!depreciation3.equals(other.depreciation3))
            return false;
        if (depreciation4 == null) {
            if (other.depreciation4 != null)
                return false;
        } else if (!depreciation4.equals(other.depreciation4))
            return false;
        if (depreciation5 == null) {
            if (other.depreciation5 != null)
                return false;
        } else if (!depreciation5.equals(other.depreciation5))
            return false;
        if (dsaId == null) {
            if (other.dsaId != null)
                return false;
        } else if (!dsaId.equals(other.dsaId))
            return false;
        if (education == null) {
            if (other.education != null)
                return false;
        } else if (!education.equals(other.education))
            return false;
        if (email1emailAddress == null) {
            if (other.email1emailAddress != null)
                return false;
        } else if (!email1emailAddress.equals(other.email1emailAddress))
            return false;
        if (email1emailType == null) {
            if (other.email1emailType != null)
                return false;
        } else if (!email1emailType.equals(other.email1emailType))
            return false;
        if (email2emailAddress == null) {
            if (other.email2emailAddress != null)
                return false;
        } else if (!email2emailAddress.equals(other.email2emailAddress))
            return false;
        if (email2emailType == null) {
            if (other.email2emailType != null)
                return false;
        } else if (!email2emailType.equals(other.email2emailType))
            return false;
        if (email3emailAddress == null) {
            if (other.email3emailAddress != null)
                return false;
        } else if (!email3emailAddress.equals(other.email3emailAddress))
            return false;
        if (email3emailType == null) {
            if (other.email3emailType != null)
                return false;
        } else if (!email3emailType.equals(other.email3emailType))
            return false;
        if (email4emailAddress == null) {
            if (other.email4emailAddress != null)
                return false;
        } else if (!email4emailAddress.equals(other.email4emailAddress))
            return false;
        if (email4emailType == null) {
            if (other.email4emailType != null)
                return false;
        } else if (!email4emailType.equals(other.email4emailType))
            return false;
        if (email5emailAddress == null) {
            if (other.email5emailAddress != null)
                return false;
        } else if (!email5emailAddress.equals(other.email5emailAddress))
            return false;
        if (email5emailType == null) {
            if (other.email5emailType != null)
                return false;
        } else if (!email5emailType.equals(other.email5emailType))
            return false;
        if (emiAmt1 == null) {
            if (other.emiAmt1 != null)
                return false;
        } else if (!emiAmt1.equals(other.emiAmt1))
            return false;
        if (emiAmt2 == null) {
            if (other.emiAmt2 != null)
                return false;
        } else if (!emiAmt2.equals(other.emiAmt2))
            return false;
        if (emiAmt3 == null) {
            if (other.emiAmt3 != null)
                return false;
        } else if (!emiAmt3.equals(other.emiAmt3))
            return false;
        if (emiAmt4 == null) {
            if (other.emiAmt4 != null)
                return false;
        } else if (!emiAmt4.equals(other.emiAmt4))
            return false;
        if (emiAmt5 == null) {
            if (other.emiAmt5 != null)
                return false;
        } else if (!emiAmt5.equals(other.emiAmt5))
            return false;
        if (emp1LastMonthIncomeIncome1 == null) {
            if (other.emp1LastMonthIncomeIncome1 != null)
                return false;
        } else if (!emp1LastMonthIncomeIncome1
                .equals(other.emp1LastMonthIncomeIncome1))
            return false;
        if (emp1LastMonthIncomeIncome2 == null) {
            if (other.emp1LastMonthIncomeIncome2 != null)
                return false;
        } else if (!emp1LastMonthIncomeIncome2
                .equals(other.emp1LastMonthIncomeIncome2))
            return false;
        if (emp1LastMonthIncomeIncome3 == null) {
            if (other.emp1LastMonthIncomeIncome3 != null)
                return false;
        } else if (!emp1LastMonthIncomeIncome3
                .equals(other.emp1LastMonthIncomeIncome3))
            return false;
        if (emp1LastMonthIncomeIncome4 == null) {
            if (other.emp1LastMonthIncomeIncome4 != null)
                return false;
        } else if (!emp1LastMonthIncomeIncome4
                .equals(other.emp1LastMonthIncomeIncome4))
            return false;
        if (emp1LastMonthIncomeIncome5 == null) {
            if (other.emp1LastMonthIncomeIncome5 != null)
                return false;
        } else if (!emp1LastMonthIncomeIncome5
                .equals(other.emp1LastMonthIncomeIncome5))
            return false;
        if (emp1LastMonthIncomeMonth1 == null) {
            if (other.emp1LastMonthIncomeMonth1 != null)
                return false;
        } else if (!emp1LastMonthIncomeMonth1
                .equals(other.emp1LastMonthIncomeMonth1))
            return false;
        if (emp1LastMonthIncomeMonth2 == null) {
            if (other.emp1LastMonthIncomeMonth2 != null)
                return false;
        } else if (!emp1LastMonthIncomeMonth2
                .equals(other.emp1LastMonthIncomeMonth2))
            return false;
        if (emp1LastMonthIncomeMonth3 == null) {
            if (other.emp1LastMonthIncomeMonth3 != null)
                return false;
        } else if (!emp1LastMonthIncomeMonth3
                .equals(other.emp1LastMonthIncomeMonth3))
            return false;
        if (emp1LastMonthIncomeMonth4 == null) {
            if (other.emp1LastMonthIncomeMonth4 != null)
                return false;
        } else if (!emp1LastMonthIncomeMonth4
                .equals(other.emp1LastMonthIncomeMonth4))
            return false;
        if (emp1LastMonthIncomeMonth5 == null) {
            if (other.emp1LastMonthIncomeMonth5 != null)
                return false;
        } else if (!emp1LastMonthIncomeMonth5
                .equals(other.emp1LastMonthIncomeMonth5))
            return false;
        if (emp1StringOfJoining == null) {
            if (other.emp1StringOfJoining != null)
                return false;
        } else if (!emp1StringOfJoining.equals(other.emp1StringOfJoining))
            return false;
        if (emp1StringOfLeaving == null) {
            if (other.emp1StringOfLeaving != null)
                return false;
        } else if (!emp1StringOfLeaving.equals(other.emp1StringOfLeaving))
            return false;
        if (emp1businessName == null) {
            if (other.emp1businessName != null)
                return false;
        } else if (!emp1businessName.equals(other.emp1businessName))
            return false;
        if (emp1commencementString == null) {
            if (other.emp1commencementString != null)
                return false;
        } else if (!emp1commencementString.equals(other.emp1commencementString))
            return false;
        if (emp1constitution == null) {
            if (other.emp1constitution != null)
                return false;
        } else if (!emp1constitution.equals(other.emp1constitution))
            return false;
        if (emp1department == null) {
            if (other.emp1department != null)
                return false;
        } else if (!emp1department.equals(other.emp1department))
            return false;
        if (emp1designation == null) {
            if (other.emp1designation != null)
                return false;
        } else if (!emp1designation.equals(other.emp1designation))
            return false;
        if (emp1employerBranch == null) {
            if (other.emp1employerBranch != null)
                return false;
        } else if (!emp1employerBranch.equals(other.emp1employerBranch))
            return false;
        if (emp1employerCode == null) {
            if (other.emp1employerCode != null)
                return false;
        } else if (!emp1employerCode.equals(other.emp1employerCode))
            return false;
        if (emp1employmentName == null) {
            if (other.emp1employmentName != null)
                return false;
        } else if (!emp1employmentName.equals(other.emp1employmentName))
            return false;
        if (emp1employmentType == null) {
            if (other.emp1employmentType != null)
                return false;
        } else if (!emp1employmentType.equals(other.emp1employmentType))
            return false;
        if (emp1grossSalary == null) {
            if (other.emp1grossSalary != null)
                return false;
        } else if (!emp1grossSalary.equals(other.emp1grossSalary))
            return false;
        if (emp1industType == null) {
            if (other.emp1industType != null)
                return false;
        } else if (!emp1industType.equals(other.emp1industType))
            return false;
        if (emp1industryType == null) {
            if (other.emp1industryType != null)
                return false;
        } else if (!emp1industryType.equals(other.emp1industryType))
            return false;
        if (emp1itrAmount == null) {
            if (other.emp1itrAmount != null)
                return false;
        } else if (!emp1itrAmount.equals(other.emp1itrAmount))
            return false;
        if (emp1itrId == null) {
            if (other.emp1itrId != null)
                return false;
        } else if (!emp1itrId.equals(other.emp1itrId))
            return false;
        if (emp1modePayment == null) {
            if (other.emp1modePayment != null)
                return false;
        } else if (!emp1modePayment.equals(other.emp1modePayment))
            return false;
        if (emp1monthlySalary == null) {
            if (other.emp1monthlySalary != null)
                return false;
        } else if (!emp1monthlySalary.equals(other.emp1monthlySalary))
            return false;
        if (emp1timeWithEmployer == null) {
            if (other.emp1timeWithEmployer != null)
                return false;
        } else if (!emp1timeWithEmployer.equals(other.emp1timeWithEmployer))
            return false;
        if (emp1workExps == null) {
            if (other.emp1workExps != null)
                return false;
        } else if (!emp1workExps.equals(other.emp1workExps))
            return false;
        if (emp2LastMonthIncomeIncome1 == null) {
            if (other.emp2LastMonthIncomeIncome1 != null)
                return false;
        } else if (!emp2LastMonthIncomeIncome1
                .equals(other.emp2LastMonthIncomeIncome1))
            return false;
        if (emp2LastMonthIncomeIncome2 == null) {
            if (other.emp2LastMonthIncomeIncome2 != null)
                return false;
        } else if (!emp2LastMonthIncomeIncome2
                .equals(other.emp2LastMonthIncomeIncome2))
            return false;
        if (emp2LastMonthIncomeIncome3 == null) {
            if (other.emp2LastMonthIncomeIncome3 != null)
                return false;
        } else if (!emp2LastMonthIncomeIncome3
                .equals(other.emp2LastMonthIncomeIncome3))
            return false;
        if (emp2LastMonthIncomeIncome4 == null) {
            if (other.emp2LastMonthIncomeIncome4 != null)
                return false;
        } else if (!emp2LastMonthIncomeIncome4
                .equals(other.emp2LastMonthIncomeIncome4))
            return false;
        if (emp2LastMonthIncomeIncome5 == null) {
            if (other.emp2LastMonthIncomeIncome5 != null)
                return false;
        } else if (!emp2LastMonthIncomeIncome5
                .equals(other.emp2LastMonthIncomeIncome5))
            return false;
        if (emp2LastMonthIncomeMonth1 == null) {
            if (other.emp2LastMonthIncomeMonth1 != null)
                return false;
        } else if (!emp2LastMonthIncomeMonth1
                .equals(other.emp2LastMonthIncomeMonth1))
            return false;
        if (emp2LastMonthIncomeMonth2 == null) {
            if (other.emp2LastMonthIncomeMonth2 != null)
                return false;
        } else if (!emp2LastMonthIncomeMonth2
                .equals(other.emp2LastMonthIncomeMonth2))
            return false;
        if (emp2LastMonthIncomeMonth3 == null) {
            if (other.emp2LastMonthIncomeMonth3 != null)
                return false;
        } else if (!emp2LastMonthIncomeMonth3
                .equals(other.emp2LastMonthIncomeMonth3))
            return false;
        if (emp2LastMonthIncomeMonth4 == null) {
            if (other.emp2LastMonthIncomeMonth4 != null)
                return false;
        } else if (!emp2LastMonthIncomeMonth4
                .equals(other.emp2LastMonthIncomeMonth4))
            return false;
        if (emp2LastMonthIncomeMonth5 == null) {
            if (other.emp2LastMonthIncomeMonth5 != null)
                return false;
        } else if (!emp2LastMonthIncomeMonth5
                .equals(other.emp2LastMonthIncomeMonth5))
            return false;
        if (emp2StringOfJoining == null) {
            if (other.emp2StringOfJoining != null)
                return false;
        } else if (!emp2StringOfJoining.equals(other.emp2StringOfJoining))
            return false;
        if (emp2StringOfLeaving == null) {
            if (other.emp2StringOfLeaving != null)
                return false;
        } else if (!emp2StringOfLeaving.equals(other.emp2StringOfLeaving))
            return false;
        if (emp2businessName == null) {
            if (other.emp2businessName != null)
                return false;
        } else if (!emp2businessName.equals(other.emp2businessName))
            return false;
        if (emp2commencementString == null) {
            if (other.emp2commencementString != null)
                return false;
        } else if (!emp2commencementString.equals(other.emp2commencementString))
            return false;
        if (emp2constitution == null) {
            if (other.emp2constitution != null)
                return false;
        } else if (!emp2constitution.equals(other.emp2constitution))
            return false;
        if (emp2department == null) {
            if (other.emp2department != null)
                return false;
        } else if (!emp2department.equals(other.emp2department))
            return false;
        if (emp2designation == null) {
            if (other.emp2designation != null)
                return false;
        } else if (!emp2designation.equals(other.emp2designation))
            return false;
        if (emp2employerBranch == null) {
            if (other.emp2employerBranch != null)
                return false;
        } else if (!emp2employerBranch.equals(other.emp2employerBranch))
            return false;
        if (emp2employerCode == null) {
            if (other.emp2employerCode != null)
                return false;
        } else if (!emp2employerCode.equals(other.emp2employerCode))
            return false;
        if (emp2employmentName == null) {
            if (other.emp2employmentName != null)
                return false;
        } else if (!emp2employmentName.equals(other.emp2employmentName))
            return false;
        if (emp2employmentType == null) {
            if (other.emp2employmentType != null)
                return false;
        } else if (!emp2employmentType.equals(other.emp2employmentType))
            return false;
        if (emp2grossSalary == null) {
            if (other.emp2grossSalary != null)
                return false;
        } else if (!emp2grossSalary.equals(other.emp2grossSalary))
            return false;
        if (emp2industType == null) {
            if (other.emp2industType != null)
                return false;
        } else if (!emp2industType.equals(other.emp2industType))
            return false;
        if (emp2industryType == null) {
            if (other.emp2industryType != null)
                return false;
        } else if (!emp2industryType.equals(other.emp2industryType))
            return false;
        if (emp2itrAmount == null) {
            if (other.emp2itrAmount != null)
                return false;
        } else if (!emp2itrAmount.equals(other.emp2itrAmount))
            return false;
        if (emp2itrId == null) {
            if (other.emp2itrId != null)
                return false;
        } else if (!emp2itrId.equals(other.emp2itrId))
            return false;
        if (emp2modePayment == null) {
            if (other.emp2modePayment != null)
                return false;
        } else if (!emp2modePayment.equals(other.emp2modePayment))
            return false;
        if (emp2monthlySalary == null) {
            if (other.emp2monthlySalary != null)
                return false;
        } else if (!emp2monthlySalary.equals(other.emp2monthlySalary))
            return false;
        if (emp2timeWithEmployer == null) {
            if (other.emp2timeWithEmployer != null)
                return false;
        } else if (!emp2timeWithEmployer.equals(other.emp2timeWithEmployer))
            return false;
        if (emp2workExps == null) {
            if (other.emp2workExps != null)
                return false;
        } else if (!emp2workExps.equals(other.emp2workExps))
            return false;
        if (emp3LastMonthIncomeIncome1 == null) {
            if (other.emp3LastMonthIncomeIncome1 != null)
                return false;
        } else if (!emp3LastMonthIncomeIncome1
                .equals(other.emp3LastMonthIncomeIncome1))
            return false;
        if (emp3LastMonthIncomeIncome2 == null) {
            if (other.emp3LastMonthIncomeIncome2 != null)
                return false;
        } else if (!emp3LastMonthIncomeIncome2
                .equals(other.emp3LastMonthIncomeIncome2))
            return false;
        if (emp3LastMonthIncomeIncome3 == null) {
            if (other.emp3LastMonthIncomeIncome3 != null)
                return false;
        } else if (!emp3LastMonthIncomeIncome3
                .equals(other.emp3LastMonthIncomeIncome3))
            return false;
        if (emp3LastMonthIncomeIncome4 == null) {
            if (other.emp3LastMonthIncomeIncome4 != null)
                return false;
        } else if (!emp3LastMonthIncomeIncome4
                .equals(other.emp3LastMonthIncomeIncome4))
            return false;
        if (emp3LastMonthIncomeIncome5 == null) {
            if (other.emp3LastMonthIncomeIncome5 != null)
                return false;
        } else if (!emp3LastMonthIncomeIncome5
                .equals(other.emp3LastMonthIncomeIncome5))
            return false;
        if (emp3LastMonthIncomeMonth1 == null) {
            if (other.emp3LastMonthIncomeMonth1 != null)
                return false;
        } else if (!emp3LastMonthIncomeMonth1
                .equals(other.emp3LastMonthIncomeMonth1))
            return false;
        if (emp3LastMonthIncomeMonth2 == null) {
            if (other.emp3LastMonthIncomeMonth2 != null)
                return false;
        } else if (!emp3LastMonthIncomeMonth2
                .equals(other.emp3LastMonthIncomeMonth2))
            return false;
        if (emp3LastMonthIncomeMonth3 == null) {
            if (other.emp3LastMonthIncomeMonth3 != null)
                return false;
        } else if (!emp3LastMonthIncomeMonth3
                .equals(other.emp3LastMonthIncomeMonth3))
            return false;
        if (emp3LastMonthIncomeMonth4 == null) {
            if (other.emp3LastMonthIncomeMonth4 != null)
                return false;
        } else if (!emp3LastMonthIncomeMonth4
                .equals(other.emp3LastMonthIncomeMonth4))
            return false;
        if (emp3LastMonthIncomeMonth5 == null) {
            if (other.emp3LastMonthIncomeMonth5 != null)
                return false;
        } else if (!emp3LastMonthIncomeMonth5
                .equals(other.emp3LastMonthIncomeMonth5))
            return false;
        if (emp3StringOfJoining == null) {
            if (other.emp3StringOfJoining != null)
                return false;
        } else if (!emp3StringOfJoining.equals(other.emp3StringOfJoining))
            return false;
        if (emp3StringOfLeaving == null) {
            if (other.emp3StringOfLeaving != null)
                return false;
        } else if (!emp3StringOfLeaving.equals(other.emp3StringOfLeaving))
            return false;
        if (emp3businessName == null) {
            if (other.emp3businessName != null)
                return false;
        } else if (!emp3businessName.equals(other.emp3businessName))
            return false;
        if (emp3commencementString == null) {
            if (other.emp3commencementString != null)
                return false;
        } else if (!emp3commencementString.equals(other.emp3commencementString))
            return false;
        if (emp3constitution == null) {
            if (other.emp3constitution != null)
                return false;
        } else if (!emp3constitution.equals(other.emp3constitution))
            return false;
        if (emp3department == null) {
            if (other.emp3department != null)
                return false;
        } else if (!emp3department.equals(other.emp3department))
            return false;
        if (emp3designation == null) {
            if (other.emp3designation != null)
                return false;
        } else if (!emp3designation.equals(other.emp3designation))
            return false;
        if (emp3employerBranch == null) {
            if (other.emp3employerBranch != null)
                return false;
        } else if (!emp3employerBranch.equals(other.emp3employerBranch))
            return false;
        if (emp3employerCode == null) {
            if (other.emp3employerCode != null)
                return false;
        } else if (!emp3employerCode.equals(other.emp3employerCode))
            return false;
        if (emp3employmentName == null) {
            if (other.emp3employmentName != null)
                return false;
        } else if (!emp3employmentName.equals(other.emp3employmentName))
            return false;
        if (emp3employmentType == null) {
            if (other.emp3employmentType != null)
                return false;
        } else if (!emp3employmentType.equals(other.emp3employmentType))
            return false;
        if (emp3grossSalary == null) {
            if (other.emp3grossSalary != null)
                return false;
        } else if (!emp3grossSalary.equals(other.emp3grossSalary))
            return false;
        if (emp3industType == null) {
            if (other.emp3industType != null)
                return false;
        } else if (!emp3industType.equals(other.emp3industType))
            return false;
        if (emp3industryType == null) {
            if (other.emp3industryType != null)
                return false;
        } else if (!emp3industryType.equals(other.emp3industryType))
            return false;
        if (emp3itrAmount == null) {
            if (other.emp3itrAmount != null)
                return false;
        } else if (!emp3itrAmount.equals(other.emp3itrAmount))
            return false;
        if (emp3itrId == null) {
            if (other.emp3itrId != null)
                return false;
        } else if (!emp3itrId.equals(other.emp3itrId))
            return false;
        if (emp3modePayment == null) {
            if (other.emp3modePayment != null)
                return false;
        } else if (!emp3modePayment.equals(other.emp3modePayment))
            return false;
        if (emp3monthlySalary == null) {
            if (other.emp3monthlySalary != null)
                return false;
        } else if (!emp3monthlySalary.equals(other.emp3monthlySalary))
            return false;
        if (emp3timeWithEmployer == null) {
            if (other.emp3timeWithEmployer != null)
                return false;
        } else if (!emp3timeWithEmployer.equals(other.emp3timeWithEmployer))
            return false;
        if (emp3workExps == null) {
            if (other.emp3workExps != null)
                return false;
        } else if (!emp3workExps.equals(other.emp3workExps))
            return false;
        if (emp4LastMonthIncomeIncome1 == null) {
            if (other.emp4LastMonthIncomeIncome1 != null)
                return false;
        } else if (!emp4LastMonthIncomeIncome1
                .equals(other.emp4LastMonthIncomeIncome1))
            return false;
        if (emp4LastMonthIncomeIncome2 == null) {
            if (other.emp4LastMonthIncomeIncome2 != null)
                return false;
        } else if (!emp4LastMonthIncomeIncome2
                .equals(other.emp4LastMonthIncomeIncome2))
            return false;
        if (emp4LastMonthIncomeIncome3 == null) {
            if (other.emp4LastMonthIncomeIncome3 != null)
                return false;
        } else if (!emp4LastMonthIncomeIncome3
                .equals(other.emp4LastMonthIncomeIncome3))
            return false;
        if (emp4LastMonthIncomeIncome4 == null) {
            if (other.emp4LastMonthIncomeIncome4 != null)
                return false;
        } else if (!emp4LastMonthIncomeIncome4
                .equals(other.emp4LastMonthIncomeIncome4))
            return false;
        if (emp4LastMonthIncomeIncome5 == null) {
            if (other.emp4LastMonthIncomeIncome5 != null)
                return false;
        } else if (!emp4LastMonthIncomeIncome5
                .equals(other.emp4LastMonthIncomeIncome5))
            return false;
        if (emp4LastMonthIncomeMonth1 == null) {
            if (other.emp4LastMonthIncomeMonth1 != null)
                return false;
        } else if (!emp4LastMonthIncomeMonth1
                .equals(other.emp4LastMonthIncomeMonth1))
            return false;
        if (emp4LastMonthIncomeMonth2 == null) {
            if (other.emp4LastMonthIncomeMonth2 != null)
                return false;
        } else if (!emp4LastMonthIncomeMonth2
                .equals(other.emp4LastMonthIncomeMonth2))
            return false;
        if (emp4LastMonthIncomeMonth3 == null) {
            if (other.emp4LastMonthIncomeMonth3 != null)
                return false;
        } else if (!emp4LastMonthIncomeMonth3
                .equals(other.emp4LastMonthIncomeMonth3))
            return false;
        if (emp4LastMonthIncomeMonth4 == null) {
            if (other.emp4LastMonthIncomeMonth4 != null)
                return false;
        } else if (!emp4LastMonthIncomeMonth4
                .equals(other.emp4LastMonthIncomeMonth4))
            return false;
        if (emp4LastMonthIncomeMonth5 == null) {
            if (other.emp4LastMonthIncomeMonth5 != null)
                return false;
        } else if (!emp4LastMonthIncomeMonth5
                .equals(other.emp4LastMonthIncomeMonth5))
            return false;
        if (emp4StringOfJoining == null) {
            if (other.emp4StringOfJoining != null)
                return false;
        } else if (!emp4StringOfJoining.equals(other.emp4StringOfJoining))
            return false;
        if (emp4StringOfLeaving == null) {
            if (other.emp4StringOfLeaving != null)
                return false;
        } else if (!emp4StringOfLeaving.equals(other.emp4StringOfLeaving))
            return false;
        if (emp4businessName == null) {
            if (other.emp4businessName != null)
                return false;
        } else if (!emp4businessName.equals(other.emp4businessName))
            return false;
        if (emp4commencementString == null) {
            if (other.emp4commencementString != null)
                return false;
        } else if (!emp4commencementString.equals(other.emp4commencementString))
            return false;
        if (emp4constitution == null) {
            if (other.emp4constitution != null)
                return false;
        } else if (!emp4constitution.equals(other.emp4constitution))
            return false;
        if (emp4department == null) {
            if (other.emp4department != null)
                return false;
        } else if (!emp4department.equals(other.emp4department))
            return false;
        if (emp4designation == null) {
            if (other.emp4designation != null)
                return false;
        } else if (!emp4designation.equals(other.emp4designation))
            return false;
        if (emp4employerBranch == null) {
            if (other.emp4employerBranch != null)
                return false;
        } else if (!emp4employerBranch.equals(other.emp4employerBranch))
            return false;
        if (emp4employerCode == null) {
            if (other.emp4employerCode != null)
                return false;
        } else if (!emp4employerCode.equals(other.emp4employerCode))
            return false;
        if (emp4employmentName == null) {
            if (other.emp4employmentName != null)
                return false;
        } else if (!emp4employmentName.equals(other.emp4employmentName))
            return false;
        if (emp4employmentType == null) {
            if (other.emp4employmentType != null)
                return false;
        } else if (!emp4employmentType.equals(other.emp4employmentType))
            return false;
        if (emp4grossSalary == null) {
            if (other.emp4grossSalary != null)
                return false;
        } else if (!emp4grossSalary.equals(other.emp4grossSalary))
            return false;
        if (emp4industType == null) {
            if (other.emp4industType != null)
                return false;
        } else if (!emp4industType.equals(other.emp4industType))
            return false;
        if (emp4industryType == null) {
            if (other.emp4industryType != null)
                return false;
        } else if (!emp4industryType.equals(other.emp4industryType))
            return false;
        if (emp4itrAmount == null) {
            if (other.emp4itrAmount != null)
                return false;
        } else if (!emp4itrAmount.equals(other.emp4itrAmount))
            return false;
        if (emp4itrId == null) {
            if (other.emp4itrId != null)
                return false;
        } else if (!emp4itrId.equals(other.emp4itrId))
            return false;
        if (emp4modePayment == null) {
            if (other.emp4modePayment != null)
                return false;
        } else if (!emp4modePayment.equals(other.emp4modePayment))
            return false;
        if (emp4monthlySalary == null) {
            if (other.emp4monthlySalary != null)
                return false;
        } else if (!emp4monthlySalary.equals(other.emp4monthlySalary))
            return false;
        if (emp4timeWithEmployer == null) {
            if (other.emp4timeWithEmployer != null)
                return false;
        } else if (!emp4timeWithEmployer.equals(other.emp4timeWithEmployer))
            return false;
        if (emp4workExps == null) {
            if (other.emp4workExps != null)
                return false;
        } else if (!emp4workExps.equals(other.emp4workExps))
            return false;
        if (emp5LastMonthIncomeIncome1 == null) {
            if (other.emp5LastMonthIncomeIncome1 != null)
                return false;
        } else if (!emp5LastMonthIncomeIncome1
                .equals(other.emp5LastMonthIncomeIncome1))
            return false;
        if (emp5LastMonthIncomeIncome2 == null) {
            if (other.emp5LastMonthIncomeIncome2 != null)
                return false;
        } else if (!emp5LastMonthIncomeIncome2
                .equals(other.emp5LastMonthIncomeIncome2))
            return false;
        if (emp5LastMonthIncomeIncome3 == null) {
            if (other.emp5LastMonthIncomeIncome3 != null)
                return false;
        } else if (!emp5LastMonthIncomeIncome3
                .equals(other.emp5LastMonthIncomeIncome3))
            return false;
        if (emp5LastMonthIncomeIncome4 == null) {
            if (other.emp5LastMonthIncomeIncome4 != null)
                return false;
        } else if (!emp5LastMonthIncomeIncome4
                .equals(other.emp5LastMonthIncomeIncome4))
            return false;
        if (emp5LastMonthIncomeIncome5 == null) {
            if (other.emp5LastMonthIncomeIncome5 != null)
                return false;
        } else if (!emp5LastMonthIncomeIncome5
                .equals(other.emp5LastMonthIncomeIncome5))
            return false;
        if (emp5LastMonthIncomeMonth1 == null) {
            if (other.emp5LastMonthIncomeMonth1 != null)
                return false;
        } else if (!emp5LastMonthIncomeMonth1
                .equals(other.emp5LastMonthIncomeMonth1))
            return false;
        if (emp5LastMonthIncomeMonth2 == null) {
            if (other.emp5LastMonthIncomeMonth2 != null)
                return false;
        } else if (!emp5LastMonthIncomeMonth2
                .equals(other.emp5LastMonthIncomeMonth2))
            return false;
        if (emp5LastMonthIncomeMonth3 == null) {
            if (other.emp5LastMonthIncomeMonth3 != null)
                return false;
        } else if (!emp5LastMonthIncomeMonth3
                .equals(other.emp5LastMonthIncomeMonth3))
            return false;
        if (emp5LastMonthIncomeMonth4 == null) {
            if (other.emp5LastMonthIncomeMonth4 != null)
                return false;
        } else if (!emp5LastMonthIncomeMonth4
                .equals(other.emp5LastMonthIncomeMonth4))
            return false;
        if (emp5LastMonthIncomeMonth5 == null) {
            if (other.emp5LastMonthIncomeMonth5 != null)
                return false;
        } else if (!emp5LastMonthIncomeMonth5
                .equals(other.emp5LastMonthIncomeMonth5))
            return false;
        if (emp5StringOfJoining == null) {
            if (other.emp5StringOfJoining != null)
                return false;
        } else if (!emp5StringOfJoining.equals(other.emp5StringOfJoining))
            return false;
        if (emp5StringOfLeaving == null) {
            if (other.emp5StringOfLeaving != null)
                return false;
        } else if (!emp5StringOfLeaving.equals(other.emp5StringOfLeaving))
            return false;
        if (emp5businessName == null) {
            if (other.emp5businessName != null)
                return false;
        } else if (!emp5businessName.equals(other.emp5businessName))
            return false;
        if (emp5commencementString == null) {
            if (other.emp5commencementString != null)
                return false;
        } else if (!emp5commencementString.equals(other.emp5commencementString))
            return false;
        if (emp5constitution == null) {
            if (other.emp5constitution != null)
                return false;
        } else if (!emp5constitution.equals(other.emp5constitution))
            return false;
        if (emp5department == null) {
            if (other.emp5department != null)
                return false;
        } else if (!emp5department.equals(other.emp5department))
            return false;
        if (emp5designation == null) {
            if (other.emp5designation != null)
                return false;
        } else if (!emp5designation.equals(other.emp5designation))
            return false;
        if (emp5employerBranch == null) {
            if (other.emp5employerBranch != null)
                return false;
        } else if (!emp5employerBranch.equals(other.emp5employerBranch))
            return false;
        if (emp5employerCode == null) {
            if (other.emp5employerCode != null)
                return false;
        } else if (!emp5employerCode.equals(other.emp5employerCode))
            return false;
        if (emp5employmentName == null) {
            if (other.emp5employmentName != null)
                return false;
        } else if (!emp5employmentName.equals(other.emp5employmentName))
            return false;
        if (emp5employmentType == null) {
            if (other.emp5employmentType != null)
                return false;
        } else if (!emp5employmentType.equals(other.emp5employmentType))
            return false;
        if (emp5grossSalary == null) {
            if (other.emp5grossSalary != null)
                return false;
        } else if (!emp5grossSalary.equals(other.emp5grossSalary))
            return false;
        if (emp5industType == null) {
            if (other.emp5industType != null)
                return false;
        } else if (!emp5industType.equals(other.emp5industType))
            return false;
        if (emp5industryType == null) {
            if (other.emp5industryType != null)
                return false;
        } else if (!emp5industryType.equals(other.emp5industryType))
            return false;
        if (emp5itrAmount == null) {
            if (other.emp5itrAmount != null)
                return false;
        } else if (!emp5itrAmount.equals(other.emp5itrAmount))
            return false;
        if (emp5itrId == null) {
            if (other.emp5itrId != null)
                return false;
        } else if (!emp5itrId.equals(other.emp5itrId))
            return false;
        if (emp5modePayment == null) {
            if (other.emp5modePayment != null)
                return false;
        } else if (!emp5modePayment.equals(other.emp5modePayment))
            return false;
        if (emp5monthlySalary == null) {
            if (other.emp5monthlySalary != null)
                return false;
        } else if (!emp5monthlySalary.equals(other.emp5monthlySalary))
            return false;
        if (emp5timeWithEmployer == null) {
            if (other.emp5timeWithEmployer != null)
                return false;
        } else if (!emp5timeWithEmployer.equals(other.emp5timeWithEmployer))
            return false;
        if (emp5workExps == null) {
            if (other.emp5workExps != null)
                return false;
        } else if (!emp5workExps.equals(other.emp5workExps))
            return false;
        if (grossIncome == null) {
            if (other.grossIncome != null)
                return false;
        } else if (!grossIncome.equals(other.grossIncome))
            return false;
        if (houseRentAllowance == null) {
            if (other.houseRentAllowance != null)
                return false;
        } else if (!houseRentAllowance.equals(other.houseRentAllowance))
            return false;
        if (incentive == null) {
            if (other.incentive != null)
                return false;
        } else if (!incentive.equals(other.incentive))
            return false;
        if (incomeDocumentAvailable == null) {
            if (other.incomeDocumentAvailable != null)
                return false;
        } else if (!incomeDocumentAvailable
                .equals(other.incomeDocumentAvailable))
            return false;
        if (institutionId == null) {
            if (other.institutionId != null)
                return false;
        } else if (!institutionId.equals(other.institutionId))
            return false;
        if (itrString1 == null) {
            if (other.itrString1 != null)
                return false;
        } else if (!itrString1.equals(other.itrString1))
            return false;
        if (itrString2 == null) {
            if (other.itrString2 != null)
                return false;
        } else if (!itrString2.equals(other.itrString2))
            return false;
        if (itrString3 == null) {
            if (other.itrString3 != null)
                return false;
        } else if (!itrString3.equals(other.itrString3))
            return false;
        if (itrString4 == null) {
            if (other.itrString4 != null)
                return false;
        } else if (!itrString4.equals(other.itrString4))
            return false;
        if (itrString5 == null) {
            if (other.itrString5 != null)
                return false;
        } else if (!itrString5.equals(other.itrString5))
            return false;
        if (kyc1expiryString == null) {
            if (other.kyc1expiryString != null)
                return false;
        } else if (!kyc1expiryString.equals(other.kyc1expiryString))
            return false;
        if (kyc1issueString == null) {
            if (other.kyc1issueString != null)
                return false;
        } else if (!kyc1issueString.equals(other.kyc1issueString))
            return false;
        if (kyc1kycName == null) {
            if (other.kyc1kycName != null)
                return false;
        } else if (!kyc1kycName.equals(other.kyc1kycName))
            return false;
        if (kyc1kycNumber == null) {
            if (other.kyc1kycNumber != null)
                return false;
        } else if (!kyc1kycNumber.equals(other.kyc1kycNumber))
            return false;
        if (kyc1kycStatus == null) {
            if (other.kyc1kycStatus != null)
                return false;
        } else if (!kyc1kycStatus.equals(other.kyc1kycStatus))
            return false;
        if (kyc2expiryString == null) {
            if (other.kyc2expiryString != null)
                return false;
        } else if (!kyc2expiryString.equals(other.kyc2expiryString))
            return false;
        if (kyc2issueString == null) {
            if (other.kyc2issueString != null)
                return false;
        } else if (!kyc2issueString.equals(other.kyc2issueString))
            return false;
        if (kyc2kycName == null) {
            if (other.kyc2kycName != null)
                return false;
        } else if (!kyc2kycName.equals(other.kyc2kycName))
            return false;
        if (kyc2kycNumber == null) {
            if (other.kyc2kycNumber != null)
                return false;
        } else if (!kyc2kycNumber.equals(other.kyc2kycNumber))
            return false;
        if (kyc2kycStatus == null) {
            if (other.kyc2kycStatus != null)
                return false;
        } else if (!kyc2kycStatus.equals(other.kyc2kycStatus))
            return false;
        if (kyc3expiryString == null) {
            if (other.kyc3expiryString != null)
                return false;
        } else if (!kyc3expiryString.equals(other.kyc3expiryString))
            return false;
        if (kyc3issueString == null) {
            if (other.kyc3issueString != null)
                return false;
        } else if (!kyc3issueString.equals(other.kyc3issueString))
            return false;
        if (kyc3kycName == null) {
            if (other.kyc3kycName != null)
                return false;
        } else if (!kyc3kycName.equals(other.kyc3kycName))
            return false;
        if (kyc3kycNumber == null) {
            if (other.kyc3kycNumber != null)
                return false;
        } else if (!kyc3kycNumber.equals(other.kyc3kycNumber))
            return false;
        if (kyc3kycStatus == null) {
            if (other.kyc3kycStatus != null)
                return false;
        } else if (!kyc3kycStatus.equals(other.kyc3kycStatus))
            return false;
        if (kyc4expiryString == null) {
            if (other.kyc4expiryString != null)
                return false;
        } else if (!kyc4expiryString.equals(other.kyc4expiryString))
            return false;
        if (kyc4issueString == null) {
            if (other.kyc4issueString != null)
                return false;
        } else if (!kyc4issueString.equals(other.kyc4issueString))
            return false;
        if (kyc4kycName == null) {
            if (other.kyc4kycName != null)
                return false;
        } else if (!kyc4kycName.equals(other.kyc4kycName))
            return false;
        if (kyc4kycNumber == null) {
            if (other.kyc4kycNumber != null)
                return false;
        } else if (!kyc4kycNumber.equals(other.kyc4kycNumber))
            return false;
        if (kyc4kycStatus == null) {
            if (other.kyc4kycStatus != null)
                return false;
        } else if (!kyc4kycStatus.equals(other.kyc4kycStatus))
            return false;
        if (kyc5expiryString == null) {
            if (other.kyc5expiryString != null)
                return false;
        } else if (!kyc5expiryString.equals(other.kyc5expiryString))
            return false;
        if (kyc5issueString == null) {
            if (other.kyc5issueString != null)
                return false;
        } else if (!kyc5issueString.equals(other.kyc5issueString))
            return false;
        if (kyc5kycName == null) {
            if (other.kyc5kycName != null)
                return false;
        } else if (!kyc5kycName.equals(other.kyc5kycName))
            return false;
        if (kyc5kycNumber == null) {
            if (other.kyc5kycNumber != null)
                return false;
        } else if (!kyc5kycNumber.equals(other.kyc5kycNumber))
            return false;
        if (kyc5kycStatus == null) {
            if (other.kyc5kycStatus != null)
                return false;
        } else if (!kyc5kycStatus.equals(other.kyc5kycStatus))
            return false;
        if (lastMonthIncomeMonth1 == null) {
            if (other.lastMonthIncomeMonth1 != null)
                return false;
        } else if (!lastMonthIncomeMonth1.equals(other.lastMonthIncomeMonth1))
            return false;
        if (lastMonthIncomeMonth2 == null) {
            if (other.lastMonthIncomeMonth2 != null)
                return false;
        } else if (!lastMonthIncomeMonth2.equals(other.lastMonthIncomeMonth2))
            return false;
        if (lastMonthIncomeMonth3 == null) {
            if (other.lastMonthIncomeMonth3 != null)
                return false;
        } else if (!lastMonthIncomeMonth3.equals(other.lastMonthIncomeMonth3))
            return false;
        if (lastMonthIncomeMonth4 == null) {
            if (other.lastMonthIncomeMonth4 != null)
                return false;
        } else if (!lastMonthIncomeMonth4.equals(other.lastMonthIncomeMonth4))
            return false;
        if (lastMonthIncomeMonth5 == null) {
            if (other.lastMonthIncomeMonth5 != null)
                return false;
        } else if (!lastMonthIncomeMonth5.equals(other.lastMonthIncomeMonth5))
            return false;
        if (lastMonthIncomeMonthIncome1 == null) {
            if (other.lastMonthIncomeMonthIncome1 != null)
                return false;
        } else if (!lastMonthIncomeMonthIncome1
                .equals(other.lastMonthIncomeMonthIncome1))
            return false;
        if (lastMonthIncomeMonthIncome2 == null) {
            if (other.lastMonthIncomeMonthIncome2 != null)
                return false;
        } else if (!lastMonthIncomeMonthIncome2
                .equals(other.lastMonthIncomeMonthIncome2))
            return false;
        if (lastMonthIncomeMonthIncome3 == null) {
            if (other.lastMonthIncomeMonthIncome3 != null)
                return false;
        } else if (!lastMonthIncomeMonthIncome3
                .equals(other.lastMonthIncomeMonthIncome3))
            return false;
        if (lastMonthIncomeMonthIncome4 == null) {
            if (other.lastMonthIncomeMonthIncome4 != null)
                return false;
        } else if (!lastMonthIncomeMonthIncome4
                .equals(other.lastMonthIncomeMonthIncome4))
            return false;
        if (lastMonthIncomeMonthIncome5 == null) {
            if (other.lastMonthIncomeMonthIncome5 != null)
                return false;
        } else if (!lastMonthIncomeMonthIncome5
                .equals(other.lastMonthIncomeMonthIncome5))
            return false;
        if (lastTwoMonthSalary == null) {
            if (other.lastTwoMonthSalary != null)
                return false;
        } else if (!lastTwoMonthSalary.equals(other.lastTwoMonthSalary))
            return false;
        if (loanAcountNo1 == null) {
            if (other.loanAcountNo1 != null)
                return false;
        } else if (!loanAcountNo1.equals(other.loanAcountNo1))
            return false;
        if (loanAcountNo2 == null) {
            if (other.loanAcountNo2 != null)
                return false;
        } else if (!loanAcountNo2.equals(other.loanAcountNo2))
            return false;
        if (loanAcountNo3 == null) {
            if (other.loanAcountNo3 != null)
                return false;
        } else if (!loanAcountNo3.equals(other.loanAcountNo3))
            return false;
        if (loanAcountNo4 == null) {
            if (other.loanAcountNo4 != null)
                return false;
        } else if (!loanAcountNo4.equals(other.loanAcountNo4))
            return false;
        if (loanAcountNo5 == null) {
            if (other.loanAcountNo5 != null)
                return false;
        } else if (!loanAcountNo5.equals(other.loanAcountNo5))
            return false;
        if (loanAmt1 == null) {
            if (other.loanAmt1 != null)
                return false;
        } else if (!loanAmt1.equals(other.loanAmt1))
            return false;
        if (loanAmt2 == null) {
            if (other.loanAmt2 != null)
                return false;
        } else if (!loanAmt2.equals(other.loanAmt2))
            return false;
        if (loanAmt3 == null) {
            if (other.loanAmt3 != null)
                return false;
        } else if (!loanAmt3.equals(other.loanAmt3))
            return false;
        if (loanAmt4 == null) {
            if (other.loanAmt4 != null)
                return false;
        } else if (!loanAmt4.equals(other.loanAmt4))
            return false;
        if (loanAmt5 == null) {
            if (other.loanAmt5 != null)
                return false;
        } else if (!loanAmt5.equals(other.loanAmt5))
            return false;
        if (loanApr1 == null) {
            if (other.loanApr1 != null)
                return false;
        } else if (!loanApr1.equals(other.loanApr1))
            return false;
        if (loanApr2 == null) {
            if (other.loanApr2 != null)
                return false;
        } else if (!loanApr2.equals(other.loanApr2))
            return false;
        if (loanApr3 == null) {
            if (other.loanApr3 != null)
                return false;
        } else if (!loanApr3.equals(other.loanApr3))
            return false;
        if (loanApr4 == null) {
            if (other.loanApr4 != null)
                return false;
        } else if (!loanApr4.equals(other.loanApr4))
            return false;
        if (loanApr5 == null) {
            if (other.loanApr5 != null)
                return false;
        } else if (!loanApr5.equals(other.loanApr5))
            return false;
        if (loanOwnership1 == null) {
            if (other.loanOwnership1 != null)
                return false;
        } else if (!loanOwnership1.equals(other.loanOwnership1))
            return false;
        if (loanOwnership2 == null) {
            if (other.loanOwnership2 != null)
                return false;
        } else if (!loanOwnership2.equals(other.loanOwnership2))
            return false;
        if (loanOwnership3 == null) {
            if (other.loanOwnership3 != null)
                return false;
        } else if (!loanOwnership3.equals(other.loanOwnership3))
            return false;
        if (loanOwnership4 == null) {
            if (other.loanOwnership4 != null)
                return false;
        } else if (!loanOwnership4.equals(other.loanOwnership4))
            return false;
        if (loanOwnership5 == null) {
            if (other.loanOwnership5 != null)
                return false;
        } else if (!loanOwnership5.equals(other.loanOwnership5))
            return false;
        if (loanPurpose1 == null) {
            if (other.loanPurpose1 != null)
                return false;
        } else if (!loanPurpose1.equals(other.loanPurpose1))
            return false;
        if (loanPurpose2 == null) {
            if (other.loanPurpose2 != null)
                return false;
        } else if (!loanPurpose2.equals(other.loanPurpose2))
            return false;
        if (loanPurpose3 == null) {
            if (other.loanPurpose3 != null)
                return false;
        } else if (!loanPurpose3.equals(other.loanPurpose3))
            return false;
        if (loanPurpose4 == null) {
            if (other.loanPurpose4 != null)
                return false;
        } else if (!loanPurpose4.equals(other.loanPurpose4))
            return false;
        if (loanPurpose5 == null) {
            if (other.loanPurpose5 != null)
                return false;
        } else if (!loanPurpose5.equals(other.loanPurpose5))
            return false;
        if (loanType1 == null) {
            if (other.loanType1 != null)
                return false;
        } else if (!loanType1.equals(other.loanType1))
            return false;
        if (loanType2 == null) {
            if (other.loanType2 != null)
                return false;
        } else if (!loanType2.equals(other.loanType2))
            return false;
        if (loanType3 == null) {
            if (other.loanType3 != null)
                return false;
        } else if (!loanType3.equals(other.loanType3))
            return false;
        if (loanType4 == null) {
            if (other.loanType4 != null)
                return false;
        } else if (!loanType4.equals(other.loanType4))
            return false;
        if (loanType5 == null) {
            if (other.loanType5 != null)
                return false;
        } else if (!loanType5.equals(other.loanType5))
            return false;
        if (mob1 == null) {
            if (other.mob1 != null)
                return false;
        } else if (!mob1.equals(other.mob1))
            return false;
        if (mob2 == null) {
            if (other.mob2 != null)
                return false;
        } else if (!mob2.equals(other.mob2))
            return false;
        if (mob3 == null) {
            if (other.mob3 != null)
                return false;
        } else if (!mob3.equals(other.mob3))
            return false;
        if (mob4 == null) {
            if (other.mob4 != null)
                return false;
        } else if (!mob4.equals(other.mob4))
            return false;
        if (mob5 == null) {
            if (other.mob5 != null)
                return false;
        } else if (!mob5.equals(other.mob5))
            return false;
        if (mobileVerified == null) {
            if (other.mobileVerified != null)
                return false;
        } else if (!mobileVerified.equals(other.mobileVerified))
            return false;
        if (netIncome == null) {
            if (other.netIncome != null)
                return false;
        } else if (!netIncome.equals(other.netIncome))
            return false;
        if (netProfit1 == null) {
            if (other.netProfit1 != null)
                return false;
        } else if (!netProfit1.equals(other.netProfit1))
            return false;
        if (netProfit2 == null) {
            if (other.netProfit2 != null)
                return false;
        } else if (!netProfit2.equals(other.netProfit2))
            return false;
        if (netProfit3 == null) {
            if (other.netProfit3 != null)
                return false;
        } else if (!netProfit3.equals(other.netProfit3))
            return false;
        if (netProfit4 == null) {
            if (other.netProfit4 != null)
                return false;
        } else if (!netProfit4.equals(other.netProfit4))
            return false;
        if (netProfit5 == null) {
            if (other.netProfit5 != null)
                return false;
        } else if (!netProfit5.equals(other.netProfit5))
            return false;
        if (noOfDependents == null) {
            if (other.noOfDependents != null)
                return false;
        } else if (!noOfDependents.equals(other.noOfDependents))
            return false;
        if (noOfEarningMembers == null) {
            if (other.noOfEarningMembers != null)
                return false;
        } else if (!noOfEarningMembers.equals(other.noOfEarningMembers))
            return false;
        if (noOfFamilyMembers == null) {
            if (other.noOfFamilyMembers != null)
                return false;
        } else if (!noOfFamilyMembers.equals(other.noOfFamilyMembers))
            return false;
        if (obligate1 == null) {
            if (other.obligate1 != null)
                return false;
        } else if (!obligate1.equals(other.obligate1))
            return false;
        if (obligate2 == null) {
            if (other.obligate2 != null)
                return false;
        } else if (!obligate2.equals(other.obligate2))
            return false;
        if (obligate3 == null) {
            if (other.obligate3 != null)
                return false;
        } else if (!obligate3.equals(other.obligate3))
            return false;
        if (obligate4 == null) {
            if (other.obligate4 != null)
                return false;
        } else if (!obligate4.equals(other.obligate4))
            return false;
        if (obligate5 == null) {
            if (other.obligate5 != null)
                return false;
        } else if (!obligate5.equals(other.obligate5))
            return false;
        if (obligations == null) {
            if (other.obligations != null)
                return false;
        } else if (!obligations.equals(other.obligations))
            return false;
        if (this.other == null) {
            if (other.other != null)
                return false;
        } else if (!this.other.equals(other.other))
            return false;
        if (otherAllowance == null) {
            if (other.otherAllowance != null)
                return false;
        } else if (!otherAllowance.equals(other.otherAllowance))
            return false;
        if (otherDeductions == null) {
            if (other.otherDeductions != null)
                return false;
        } else if (!otherDeductions.equals(other.otherDeductions))
            return false;
        if (otherIncome1 == null) {
            if (other.otherIncome1 != null)
                return false;
        } else if (!otherIncome1.equals(other.otherIncome1))
            return false;
        if (otherIncome2 == null) {
            if (other.otherIncome2 != null)
                return false;
        } else if (!otherIncome2.equals(other.otherIncome2))
            return false;
        if (otherIncome3 == null) {
            if (other.otherIncome3 != null)
                return false;
        } else if (!otherIncome3.equals(other.otherIncome3))
            return false;
        if (otherIncome4 == null) {
            if (other.otherIncome4 != null)
                return false;
        } else if (!otherIncome4.equals(other.otherIncome4))
            return false;
        if (otherIncome5 == null) {
            if (other.otherIncome5 != null)
                return false;
        } else if (!otherIncome5.equals(other.otherIncome5))
            return false;
        if (otherSourceIncome == null) {
            if (other.otherSourceIncome != null)
                return false;
        } else if (!otherSourceIncome.equals(other.otherSourceIncome))
            return false;
        if (otherSourceIncomeAmount == null) {
            if (other.otherSourceIncomeAmount != null)
                return false;
        } else if (!otherSourceIncomeAmount
                .equals(other.otherSourceIncomeAmount))
            return false;
        if (otherSourceOfIncome1 == null) {
            if (other.otherSourceOfIncome1 != null)
                return false;
        } else if (!otherSourceOfIncome1.equals(other.otherSourceOfIncome1))
            return false;
        if (otherSourceOfIncome2 == null) {
            if (other.otherSourceOfIncome2 != null)
                return false;
        } else if (!otherSourceOfIncome2.equals(other.otherSourceOfIncome2))
            return false;
        if (otherSourceOfIncome3 == null) {
            if (other.otherSourceOfIncome3 != null)
                return false;
        } else if (!otherSourceOfIncome3.equals(other.otherSourceOfIncome3))
            return false;
        if (otherSourceOfIncome4 == null) {
            if (other.otherSourceOfIncome4 != null)
                return false;
        } else if (!otherSourceOfIncome4.equals(other.otherSourceOfIncome4))
            return false;
        if (otherSourceOfIncome5 == null) {
            if (other.otherSourceOfIncome5 != null)
                return false;
        } else if (!otherSourceOfIncome5.equals(other.otherSourceOfIncome5))
            return false;
        if (otherSourcesIncome == null) {
            if (other.otherSourcesIncome != null)
                return false;
        } else if (!otherSourcesIncome.equals(other.otherSourcesIncome))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (phone1countryCode == null) {
            if (other.phone1countryCode != null)
                return false;
        } else if (!phone1countryCode.equals(other.phone1countryCode))
            return false;
        if (phone1extension == null) {
            if (other.phone1extension != null)
                return false;
        } else if (!phone1extension.equals(other.phone1extension))
            return false;
        if (phone1phoneNumber == null) {
            if (other.phone1phoneNumber != null)
                return false;
        } else if (!phone1phoneNumber.equals(other.phone1phoneNumber))
            return false;
        if (phone1phoneType == null) {
            if (other.phone1phoneType != null)
                return false;
        } else if (!phone1phoneType.equals(other.phone1phoneType))
            return false;
        if (phone2countryCode == null) {
            if (other.phone2countryCode != null)
                return false;
        } else if (!phone2countryCode.equals(other.phone2countryCode))
            return false;
        if (phone2extension == null) {
            if (other.phone2extension != null)
                return false;
        } else if (!phone2extension.equals(other.phone2extension))
            return false;
        if (phone2phoneNumber == null) {
            if (other.phone2phoneNumber != null)
                return false;
        } else if (!phone2phoneNumber.equals(other.phone2phoneNumber))
            return false;
        if (phone2phoneType == null) {
            if (other.phone2phoneType != null)
                return false;
        } else if (!phone2phoneType.equals(other.phone2phoneType))
            return false;
        if (phone3countryCode == null) {
            if (other.phone3countryCode != null)
                return false;
        } else if (!phone3countryCode.equals(other.phone3countryCode))
            return false;
        if (phone3extension == null) {
            if (other.phone3extension != null)
                return false;
        } else if (!phone3extension.equals(other.phone3extension))
            return false;
        if (phone3phoneNumber == null) {
            if (other.phone3phoneNumber != null)
                return false;
        } else if (!phone3phoneNumber.equals(other.phone3phoneNumber))
            return false;
        if (phone3phoneType == null) {
            if (other.phone3phoneType != null)
                return false;
        } else if (!phone3phoneType.equals(other.phone3phoneType))
            return false;
        if (phone4countryCode == null) {
            if (other.phone4countryCode != null)
                return false;
        } else if (!phone4countryCode.equals(other.phone4countryCode))
            return false;
        if (phone4extension == null) {
            if (other.phone4extension != null)
                return false;
        } else if (!phone4extension.equals(other.phone4extension))
            return false;
        if (phone4phoneNumber == null) {
            if (other.phone4phoneNumber != null)
                return false;
        } else if (!phone4phoneNumber.equals(other.phone4phoneNumber))
            return false;
        if (phone4phoneType == null) {
            if (other.phone4phoneType != null)
                return false;
        } else if (!phone4phoneType.equals(other.phone4phoneType))
            return false;
        if (phone5countryCode == null) {
            if (other.phone5countryCode != null)
                return false;
        } else if (!phone5countryCode.equals(other.phone5countryCode))
            return false;
        if (phone5extension == null) {
            if (other.phone5extension != null)
                return false;
        } else if (!phone5extension.equals(other.phone5extension))
            return false;
        if (phone5phoneNumber == null) {
            if (other.phone5phoneNumber != null)
                return false;
        } else if (!phone5phoneNumber.equals(other.phone5phoneNumber))
            return false;
        if (phone5phoneType == null) {
            if (other.phone5phoneType != null)
                return false;
        } else if (!phone5phoneType.equals(other.phone5phoneType))
            return false;
        if (professionalTax == null) {
            if (other.professionalTax != null)
                return false;
        } else if (!professionalTax.equals(other.professionalTax))
            return false;
        if (providentFund == null) {
            if (other.providentFund != null)
                return false;
        } else if (!providentFund.equals(other.providentFund))
            return false;
        if (refID == null) {
            if (other.refID != null)
                return false;
        } else if (!refID.equals(other.refID))
            return false;
        if (remarks == null) {
            if (other.remarks != null)
                return false;
        } else if (!remarks.equals(other.remarks))
            return false;
        if (repaymentBankName1 == null) {
            if (other.repaymentBankName1 != null)
                return false;
        } else if (!repaymentBankName1.equals(other.repaymentBankName1))
            return false;
        if (repaymentBankName2 == null) {
            if (other.repaymentBankName2 != null)
                return false;
        } else if (!repaymentBankName2.equals(other.repaymentBankName2))
            return false;
        if (repaymentBankName3 == null) {
            if (other.repaymentBankName3 != null)
                return false;
        } else if (!repaymentBankName3.equals(other.repaymentBankName3))
            return false;
        if (repaymentBankName4 == null) {
            if (other.repaymentBankName4 != null)
                return false;
        } else if (!repaymentBankName4.equals(other.repaymentBankName4))
            return false;
        if (repaymentBankName5 == null) {
            if (other.repaymentBankName5 != null)
                return false;
        } else if (!repaymentBankName5.equals(other.repaymentBankName5))
            return false;
        if (requestType == null) {
            if (other.requestType != null)
                return false;
        } else if (!requestType.equals(other.requestType))
            return false;
        if (salariedSurrogateNetTakeHome1 == null) {
            if (other.salariedSurrogateNetTakeHome1 != null)
                return false;
        } else if (!salariedSurrogateNetTakeHome1
                .equals(other.salariedSurrogateNetTakeHome1))
            return false;
        if (salariedSurrogateNetTakeHome2 == null) {
            if (other.salariedSurrogateNetTakeHome2 != null)
                return false;
        } else if (!salariedSurrogateNetTakeHome2
                .equals(other.salariedSurrogateNetTakeHome2))
            return false;
        if (salariedSurrogateNetTakeHome3 == null) {
            if (other.salariedSurrogateNetTakeHome3 != null)
                return false;
        } else if (!salariedSurrogateNetTakeHome3
                .equals(other.salariedSurrogateNetTakeHome3))
            return false;
        if (salariedSurrogateNetTakeHome4 == null) {
            if (other.salariedSurrogateNetTakeHome4 != null)
                return false;
        } else if (!salariedSurrogateNetTakeHome4
                .equals(other.salariedSurrogateNetTakeHome4))
            return false;
        if (salariedSurrogateNetTakeHome5 == null) {
            if (other.salariedSurrogateNetTakeHome5 != null)
                return false;
        } else if (!salariedSurrogateNetTakeHome5
                .equals(other.salariedSurrogateNetTakeHome5))
            return false;
        if (sourceId == null) {
            if (other.sourceId != null)
                return false;
        } else if (!sourceId.equals(other.sourceId))
            return false;
        if (stateInsurance == null) {
            if (other.stateInsurance != null)
                return false;
        } else if (!stateInsurance.equals(other.stateInsurance))
            return false;
        if (traderSurrogateBussinesProof1 == null) {
            if (other.traderSurrogateBussinesProof1 != null)
                return false;
        } else if (!traderSurrogateBussinesProof1
                .equals(other.traderSurrogateBussinesProof1))
            return false;
        if (traderSurrogateBussinesProof2 == null) {
            if (other.traderSurrogateBussinesProof2 != null)
                return false;
        } else if (!traderSurrogateBussinesProof2
                .equals(other.traderSurrogateBussinesProof2))
            return false;
        if (traderSurrogateBussinesProof3 == null) {
            if (other.traderSurrogateBussinesProof3 != null)
                return false;
        } else if (!traderSurrogateBussinesProof3
                .equals(other.traderSurrogateBussinesProof3))
            return false;
        if (traderSurrogateBussinesProof4 == null) {
            if (other.traderSurrogateBussinesProof4 != null)
                return false;
        } else if (!traderSurrogateBussinesProof4
                .equals(other.traderSurrogateBussinesProof4))
            return false;
        if (traderSurrogateBussinesProof5 == null) {
            if (other.traderSurrogateBussinesProof5 != null)
                return false;
        } else if (!traderSurrogateBussinesProof5
                .equals(other.traderSurrogateBussinesProof5))
            return false;
        if (traderSurrogateYearInBussines1 == null) {
            if (other.traderSurrogateYearInBussines1 != null)
                return false;
        } else if (!traderSurrogateYearInBussines1
                .equals(other.traderSurrogateYearInBussines1))
            return false;
        if (traderSurrogateYearInBussines2 == null) {
            if (other.traderSurrogateYearInBussines2 != null)
                return false;
        } else if (!traderSurrogateYearInBussines2
                .equals(other.traderSurrogateYearInBussines2))
            return false;
        if (traderSurrogateYearInBussines3 == null) {
            if (other.traderSurrogateYearInBussines3 != null)
                return false;
        } else if (!traderSurrogateYearInBussines3
                .equals(other.traderSurrogateYearInBussines3))
            return false;
        if (traderSurrogateYearInBussines4 == null) {
            if (other.traderSurrogateYearInBussines4 != null)
                return false;
        } else if (!traderSurrogateYearInBussines4
                .equals(other.traderSurrogateYearInBussines4))
            return false;
        if (traderSurrogateYearInBussines5 == null) {
            if (other.traderSurrogateYearInBussines5 != null)
                return false;
        } else if (!traderSurrogateYearInBussines5
                .equals(other.traderSurrogateYearInBussines5))
            return false;
        if (turnOver1 == null) {
            if (other.turnOver1 != null)
                return false;
        } else if (!turnOver1.equals(other.turnOver1))
            return false;
        if (turnOver2 == null) {
            if (other.turnOver2 != null)
                return false;
        } else if (!turnOver2.equals(other.turnOver2))
            return false;
        if (turnOver3 == null) {
            if (other.turnOver3 != null)
                return false;
        } else if (!turnOver3.equals(other.turnOver3))
            return false;
        if (turnOver4 == null) {
            if (other.turnOver4 != null)
                return false;
        } else if (!turnOver4.equals(other.turnOver4))
            return false;
        if (turnOver5 == null) {
            if (other.turnOver5 != null)
                return false;
        } else if (!turnOver5.equals(other.turnOver5))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        if (year1 == null) {
            if (other.year1 != null)
                return false;
        } else if (!year1.equals(other.year1))
            return false;
        if (year2 == null) {
            if (other.year2 != null)
                return false;
        } else if (!year2.equals(other.year2))
            return false;
        if (year3 == null) {
            if (other.year3 != null)
                return false;
        } else if (!year3.equals(other.year3))
            return false;
        if (year4 == null) {
            if (other.year4 != null)
                return false;
        } else if (!year4.equals(other.year4))
            return false;
        if (year5 == null) {
            if (other.year5 != null)
                return false;
        } else if (!year5.equals(other.year5))
            return false;
        return true;
    }


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ApplicationTAT [institutionId=");
        builder.append(institutionId);
        builder.append(", userId=");
        builder.append(userId);
        builder.append(", password=");
        builder.append(password);
        builder.append(", refID=");
        builder.append(refID);
        builder.append(", applicationId=");
        builder.append(applicationId);
        builder.append(", sourceId=");
        builder.append(sourceId);
        builder.append(", applicationSource=");
        builder.append(applicationSource);
        builder.append(", requestType=");
        builder.append(requestType);
        builder.append(", StringTime=");
        builder.append(StringTime);
        builder.append(", dsaId=");
        builder.append(dsaId);
        builder.append(", croId=");
        builder.append(croId);
        builder.append(", dealerId=");
        builder.append(dealerId);
        builder.append(", applicantId=");
        builder.append(applicantId);
        builder.append(", applicantFirstName=");
        builder.append(applicantFirstName);
        builder.append(", applicantMiddleName=");
        builder.append(applicantMiddleName);
        builder.append(", applicantLastName=");
        builder.append(applicantLastName);
        builder.append(", applicantPrefix=");
        builder.append(applicantPrefix);
        builder.append(", applicantSuffix=");
        builder.append(applicantSuffix);
        builder.append(", applicantFatherFirstName=");
        builder.append(applicantFatherFirstName);
        builder.append(", applicantFatherMiddleName=");
        builder.append(applicantFatherMiddleName);
        builder.append(", applicantFatherLastName=");
        builder.append(applicantFatherLastName);
        builder.append(", applicantFatherPrefix=");
        builder.append(applicantFatherPrefix);
        builder.append(", applicantFatherSuffix=");
        builder.append(applicantFatherSuffix);
        builder.append(", applicantSpouseFirstName=");
        builder.append(applicantSpouseFirstName);
        builder.append(", applicantSpouseMiddleName=");
        builder.append(applicantSpouseMiddleName);
        builder.append(", applicantSpouseLastName=");
        builder.append(applicantSpouseLastName);
        builder.append(", applicantSpousePrefix=");
        builder.append(applicantSpousePrefix);
        builder.append(", applicantSpouseSuffix=");
        builder.append(applicantSpouseSuffix);
        builder.append(", applicantReligion=");
        builder.append(applicantReligion);
        builder.append(", applicantGender=");
        builder.append(applicantGender);
        builder.append(", applicantStringOfBirth=");
        builder.append(applicantStringOfBirth);
        builder.append(", applicantAge=");
        builder.append(applicantAge);
        builder.append(", applicantMaritalStatus=");
        builder.append(applicantMaritalStatus);
        builder.append(", kyc1kycName=");
        builder.append(kyc1kycName);
        builder.append(", kyc1kycNumber=");
        builder.append(kyc1kycNumber);
        builder.append(", kyc1kycStatus=");
        builder.append(kyc1kycStatus);
        builder.append(", kyc1issueString=");
        builder.append(kyc1issueString);
        builder.append(", kyc1expiryString=");
        builder.append(kyc1expiryString);
        builder.append(", kyc2kycName=");
        builder.append(kyc2kycName);
        builder.append(", kyc2kycNumber=");
        builder.append(kyc2kycNumber);
        builder.append(", kyc2kycStatus=");
        builder.append(kyc2kycStatus);
        builder.append(", kyc2issueString=");
        builder.append(kyc2issueString);
        builder.append(", kyc2expiryString=");
        builder.append(kyc2expiryString);
        builder.append(", kyc3kycName=");
        builder.append(kyc3kycName);
        builder.append(", kyc3kycNumber=");
        builder.append(kyc3kycNumber);
        builder.append(", kyc3kycStatus=");
        builder.append(kyc3kycStatus);
        builder.append(", kyc3issueString=");
        builder.append(kyc3issueString);
        builder.append(", kyc3expiryString=");
        builder.append(kyc3expiryString);
        builder.append(", kyc4kycName=");
        builder.append(kyc4kycName);
        builder.append(", kyc4kycNumber=");
        builder.append(kyc4kycNumber);
        builder.append(", kyc4kycStatus=");
        builder.append(kyc4kycStatus);
        builder.append(", kyc4issueString=");
        builder.append(kyc4issueString);
        builder.append(", kyc4expiryString=");
        builder.append(kyc4expiryString);
        builder.append(", kyc5kycName=");
        builder.append(kyc5kycName);
        builder.append(", kyc5kycNumber=");
        builder.append(kyc5kycNumber);
        builder.append(", kyc5kycStatus=");
        builder.append(kyc5kycStatus);
        builder.append(", kyc5issueString=");
        builder.append(kyc5issueString);
        builder.append(", kyc5expiryString=");
        builder.append(kyc5expiryString);
        builder.append(", add1accommodation=");
        builder.append(add1accommodation);
        builder.append(", add1timeAtAddress=");
        builder.append(add1timeAtAddress);
        builder.append(", add1addressType=");
        builder.append(add1addressType);
        builder.append(", add1residenceAddressType=");
        builder.append(add1residenceAddressType);
        builder.append(", add1monthAtCity=");
        builder.append(add1monthAtCity);
        builder.append(", add1monthAtAddress=");
        builder.append(add1monthAtAddress);
        builder.append(", add1rentAmount=");
        builder.append(add1rentAmount);
        builder.append(", add1yearAtCity=");
        builder.append(add1yearAtCity);
        builder.append(", add1addressLine1=");
        builder.append(add1addressLine1);
        builder.append(", add1addressLine2=");
        builder.append(add1addressLine2);
        builder.append(", add1city=");
        builder.append(add1city);
        builder.append(", add1pin=");
        builder.append(add1pin);
        builder.append(", add1state=");
        builder.append(add1state);
        builder.append(", add1country=");
        builder.append(add1country);
        builder.append(", add1landLoard=");
        builder.append(add1landLoard);
        builder.append(", add1line3=");
        builder.append(add1line3);
        builder.append(", add1line4=");
        builder.append(add1line4);
        builder.append(", add1village=");
        builder.append(add1village);
        builder.append(", add1district=");
        builder.append(add1district);
        builder.append(", add1distanceFrom=");
        builder.append(add1distanceFrom);
        builder.append(", add1landMark=");
        builder.append(add1landMark);
        builder.append(", add2accommodation=");
        builder.append(add2accommodation);
        builder.append(", add2timeAtAddress=");
        builder.append(add2timeAtAddress);
        builder.append(", add2addressType=");
        builder.append(add2addressType);
        builder.append(", add2residenceAddressType=");
        builder.append(add2residenceAddressType);
        builder.append(", add2monthAtCity=");
        builder.append(add2monthAtCity);
        builder.append(", add2monthAtAddress=");
        builder.append(add2monthAtAddress);
        builder.append(", add2rentAmount=");
        builder.append(add2rentAmount);
        builder.append(", add2yearAtCity=");
        builder.append(add2yearAtCity);
        builder.append(", add2addressLine1=");
        builder.append(add2addressLine1);
        builder.append(", add2addressLine2=");
        builder.append(add2addressLine2);
        builder.append(", add2city=");
        builder.append(add2city);
        builder.append(", add2pin=");
        builder.append(add2pin);
        builder.append(", add2state=");
        builder.append(add2state);
        builder.append(", add2country=");
        builder.append(add2country);
        builder.append(", add2landLoard=");
        builder.append(add2landLoard);
        builder.append(", add2line3=");
        builder.append(add2line3);
        builder.append(", add2line4=");
        builder.append(add2line4);
        builder.append(", add2village=");
        builder.append(add2village);
        builder.append(", add2district=");
        builder.append(add2district);
        builder.append(", add2distanceFrom=");
        builder.append(add2distanceFrom);
        builder.append(", add2landMark=");
        builder.append(add2landMark);
        builder.append(", add3accommodation=");
        builder.append(add3accommodation);
        builder.append(", add3timeAtAddress=");
        builder.append(add3timeAtAddress);
        builder.append(", add3addressType=");
        builder.append(add3addressType);
        builder.append(", add3residenceAddressType=");
        builder.append(add3residenceAddressType);
        builder.append(", add3monthAtCity=");
        builder.append(add3monthAtCity);
        builder.append(", add3monthAtAddress=");
        builder.append(add3monthAtAddress);
        builder.append(", add3rentAmount=");
        builder.append(add3rentAmount);
        builder.append(", add3yearAtCity=");
        builder.append(add3yearAtCity);
        builder.append(", add3addressLine1=");
        builder.append(add3addressLine1);
        builder.append(", add3addressLine2=");
        builder.append(add3addressLine2);
        builder.append(", add3city=");
        builder.append(add3city);
        builder.append(", add3pin=");
        builder.append(add3pin);
        builder.append(", add3state=");
        builder.append(add3state);
        builder.append(", add3country=");
        builder.append(add3country);
        builder.append(", add3landLoard=");
        builder.append(add3landLoard);
        builder.append(", add3line3=");
        builder.append(add3line3);
        builder.append(", add3line4=");
        builder.append(add3line4);
        builder.append(", add3village=");
        builder.append(add3village);
        builder.append(", add3district=");
        builder.append(add3district);
        builder.append(", add3distanceFrom=");
        builder.append(add3distanceFrom);
        builder.append(", add3landMark=");
        builder.append(add3landMark);
        builder.append(", add4accommodation=");
        builder.append(add4accommodation);
        builder.append(", add4timeAtAddress=");
        builder.append(add4timeAtAddress);
        builder.append(", add4addressType=");
        builder.append(add4addressType);
        builder.append(", add4residenceAddressType=");
        builder.append(add4residenceAddressType);
        builder.append(", add4monthAtCity=");
        builder.append(add4monthAtCity);
        builder.append(", add4monthAtAddress=");
        builder.append(add4monthAtAddress);
        builder.append(", add4rentAmount=");
        builder.append(add4rentAmount);
        builder.append(", add4yearAtCity=");
        builder.append(add4yearAtCity);
        builder.append(", add4addressLine1=");
        builder.append(add4addressLine1);
        builder.append(", add4addressLine2=");
        builder.append(add4addressLine2);
        builder.append(", add4city=");
        builder.append(add4city);
        builder.append(", add4pin=");
        builder.append(add4pin);
        builder.append(", add4state=");
        builder.append(add4state);
        builder.append(", add4country=");
        builder.append(add4country);
        builder.append(", add4landLoard=");
        builder.append(add4landLoard);
        builder.append(", add4line3=");
        builder.append(add4line3);
        builder.append(", add4line4=");
        builder.append(add4line4);
        builder.append(", add4village=");
        builder.append(add4village);
        builder.append(", add4district=");
        builder.append(add4district);
        builder.append(", add4distanceFrom=");
        builder.append(add4distanceFrom);
        builder.append(", add4landMark=");
        builder.append(add4landMark);
        builder.append(", add5accommodation=");
        builder.append(add5accommodation);
        builder.append(", add5timeAtAddress=");
        builder.append(add5timeAtAddress);
        builder.append(", add5addressType=");
        builder.append(add5addressType);
        builder.append(", add5residenceAddressType=");
        builder.append(add5residenceAddressType);
        builder.append(", add5monthAtCity=");
        builder.append(add5monthAtCity);
        builder.append(", add5monthAtAddress=");
        builder.append(add5monthAtAddress);
        builder.append(", add5rentAmount=");
        builder.append(add5rentAmount);
        builder.append(", add5yearAtCity=");
        builder.append(add5yearAtCity);
        builder.append(", add5pin=");
        builder.append(add5pin);
        builder.append(", add5state=");
        builder.append(add5state);
        builder.append(", add5country=");
        builder.append(add5country);
        builder.append(", add5landLoard=");
        builder.append(add5landLoard);
        builder.append(", add5line3=");
        builder.append(add5line3);
        builder.append(", add5line4=");
        builder.append(add5line4);
        builder.append(", add5village=");
        builder.append(add5village);
        builder.append(", add5district=");
        builder.append(add5district);
        builder.append(", add5distanceFrom=");
        builder.append(add5distanceFrom);
        builder.append(", add5landMark=");
        builder.append(add5landMark);
        builder.append(", phone1phoneType=");
        builder.append(phone1phoneType);
        builder.append(", phone1countryCode=");
        builder.append(phone1countryCode);
        builder.append(", phone1phoneNumber=");
        builder.append(phone1phoneNumber);
        builder.append(", phone1extension=");
        builder.append(phone1extension);
        builder.append(", phone2phoneType=");
        builder.append(phone2phoneType);
        builder.append(", phone2countryCode=");
        builder.append(phone2countryCode);
        builder.append(", phone2phoneNumber=");
        builder.append(phone2phoneNumber);
        builder.append(", phone2extension=");
        builder.append(phone2extension);
        builder.append(", phone3phoneType=");
        builder.append(phone3phoneType);
        builder.append(", phone3countryCode=");
        builder.append(phone3countryCode);
        builder.append(", phone3phoneNumber=");
        builder.append(phone3phoneNumber);
        builder.append(", phone3extension=");
        builder.append(phone3extension);
        builder.append(", phone4phoneType=");
        builder.append(phone4phoneType);
        builder.append(", phone4countryCode=");
        builder.append(phone4countryCode);
        builder.append(", phone4phoneNumber=");
        builder.append(phone4phoneNumber);
        builder.append(", phone4extension=");
        builder.append(phone4extension);
        builder.append(", phone5phoneType=");
        builder.append(phone5phoneType);
        builder.append(", phone5countryCode=");
        builder.append(phone5countryCode);
        builder.append(", phone5phoneNumber=");
        builder.append(phone5phoneNumber);
        builder.append(", phone5extension=");
        builder.append(phone5extension);
        builder.append(", email1emailType=");
        builder.append(email1emailType);
        builder.append(", email1emailAddress=");
        builder.append(email1emailAddress);
        builder.append(", email2emailType=");
        builder.append(email2emailType);
        builder.append(", email2emailAddress=");
        builder.append(email2emailAddress);
        builder.append(", email3emailType=");
        builder.append(email3emailType);
        builder.append(", email3emailAddress=");
        builder.append(email3emailAddress);
        builder.append(", email4emailType=");
        builder.append(email4emailType);
        builder.append(", email4emailAddress=");
        builder.append(email4emailAddress);
        builder.append(", email5emailType=");
        builder.append(email5emailType);
        builder.append(", email5emailAddress=");
        builder.append(email5emailAddress);
        builder.append(", emp1employmentType=");
        builder.append(emp1employmentType);
        builder.append(", emp1employmentName=");
        builder.append(emp1employmentName);
        builder.append(", emp1timeWithEmployer=");
        builder.append(emp1timeWithEmployer);
        builder.append(", emp1StringOfJoining=");
        builder.append(emp1StringOfJoining);
        builder.append(", emp1StringOfLeaving=");
        builder.append(emp1StringOfLeaving);
        builder.append(", emp1monthlySalary=");
        builder.append(emp1monthlySalary);
        builder.append(", emp1grossSalary=");
        builder.append(emp1grossSalary);
        builder.append(", emp1constitution=");
        builder.append(emp1constitution);
        builder.append(", emp1industType=");
        builder.append(emp1industType);
        builder.append(", emp1itrId=");
        builder.append(emp1itrId);
        builder.append(", emp1itrAmount=");
        builder.append(emp1itrAmount);
        builder.append(", emp1designation=");
        builder.append(emp1designation);
        builder.append(", emp1employerCode=");
        builder.append(emp1employerCode);
        builder.append(", emp1employerBranch=");
        builder.append(emp1employerBranch);
        builder.append(", emp1modePayment=");
        builder.append(emp1modePayment);
        builder.append(", emp1department=");
        builder.append(emp1department);
        builder.append(", emp1workExps=");
        builder.append(emp1workExps);
        builder.append(", emp1businessName=");
        builder.append(emp1businessName);
        builder.append(", emp1commencementString=");
        builder.append(emp1commencementString);
        builder.append(", emp1industryType=");
        builder.append(emp1industryType);
        builder.append(", emp1LastMonthIncomeMonth1=");
        builder.append(emp1LastMonthIncomeMonth1);
        builder.append(", emp1LastMonthIncomeIncome1=");
        builder.append(emp1LastMonthIncomeIncome1);
        builder.append(", emp1LastMonthIncomeMonth2=");
        builder.append(emp1LastMonthIncomeMonth2);
        builder.append(", emp1LastMonthIncomeIncome2=");
        builder.append(emp1LastMonthIncomeIncome2);
        builder.append(", emp1LastMonthIncomeMonth3=");
        builder.append(emp1LastMonthIncomeMonth3);
        builder.append(", emp1LastMonthIncomeIncome3=");
        builder.append(emp1LastMonthIncomeIncome3);
        builder.append(", emp1LastMonthIncomeMonth4=");
        builder.append(emp1LastMonthIncomeMonth4);
        builder.append(", emp1LastMonthIncomeIncome4=");
        builder.append(emp1LastMonthIncomeIncome4);
        builder.append(", emp1LastMonthIncomeMonth5=");
        builder.append(emp1LastMonthIncomeMonth5);
        builder.append(", emp1LastMonthIncomeIncome5=");
        builder.append(emp1LastMonthIncomeIncome5);
        builder.append(", emp2employmentType=");
        builder.append(emp2employmentType);
        builder.append(", emp2employmentName=");
        builder.append(emp2employmentName);
        builder.append(", emp2timeWithEmployer=");
        builder.append(emp2timeWithEmployer);
        builder.append(", emp2StringOfJoining=");
        builder.append(emp2StringOfJoining);
        builder.append(", emp2StringOfLeaving=");
        builder.append(emp2StringOfLeaving);
        builder.append(", emp2monthlySalary=");
        builder.append(emp2monthlySalary);
        builder.append(", emp2grossSalary=");
        builder.append(emp2grossSalary);
        builder.append(", emp2constitution=");
        builder.append(emp2constitution);
        builder.append(", emp2industType=");
        builder.append(emp2industType);
        builder.append(", emp2itrId=");
        builder.append(emp2itrId);
        builder.append(", emp2itrAmount=");
        builder.append(emp2itrAmount);
        builder.append(", emp2designation=");
        builder.append(emp2designation);
        builder.append(", emp2employerCode=");
        builder.append(emp2employerCode);
        builder.append(", emp2employerBranch=");
        builder.append(emp2employerBranch);
        builder.append(", emp2modePayment=");
        builder.append(emp2modePayment);
        builder.append(", emp2department=");
        builder.append(emp2department);
        builder.append(", emp2workExps=");
        builder.append(emp2workExps);
        builder.append(", emp2businessName=");
        builder.append(emp2businessName);
        builder.append(", emp2commencementString=");
        builder.append(emp2commencementString);
        builder.append(", emp2industryType=");
        builder.append(emp2industryType);
        builder.append(", emp2LastMonthIncomeMonth1=");
        builder.append(emp2LastMonthIncomeMonth1);
        builder.append(", emp2LastMonthIncomeIncome1=");
        builder.append(emp2LastMonthIncomeIncome1);
        builder.append(", emp2LastMonthIncomeMonth2=");
        builder.append(emp2LastMonthIncomeMonth2);
        builder.append(", emp2LastMonthIncomeIncome2=");
        builder.append(emp2LastMonthIncomeIncome2);
        builder.append(", emp2LastMonthIncomeMonth3=");
        builder.append(emp2LastMonthIncomeMonth3);
        builder.append(", emp2LastMonthIncomeIncome3=");
        builder.append(emp2LastMonthIncomeIncome3);
        builder.append(", emp2LastMonthIncomeMonth4=");
        builder.append(emp2LastMonthIncomeMonth4);
        builder.append(", emp2LastMonthIncomeIncome4=");
        builder.append(emp2LastMonthIncomeIncome4);
        builder.append(", emp2LastMonthIncomeMonth5=");
        builder.append(emp2LastMonthIncomeMonth5);
        builder.append(", emp2LastMonthIncomeIncome5=");
        builder.append(emp2LastMonthIncomeIncome5);
        builder.append(", emp3employmentType=");
        builder.append(emp3employmentType);
        builder.append(", emp3employmentName=");
        builder.append(emp3employmentName);
        builder.append(", emp3timeWithEmployer=");
        builder.append(emp3timeWithEmployer);
        builder.append(", emp3StringOfJoining=");
        builder.append(emp3StringOfJoining);
        builder.append(", emp3StringOfLeaving=");
        builder.append(emp3StringOfLeaving);
        builder.append(", emp3monthlySalary=");
        builder.append(emp3monthlySalary);
        builder.append(", emp3grossSalary=");
        builder.append(emp3grossSalary);
        builder.append(", emp3constitution=");
        builder.append(emp3constitution);
        builder.append(", emp3industType=");
        builder.append(emp3industType);
        builder.append(", emp3itrId=");
        builder.append(emp3itrId);
        builder.append(", emp3itrAmount=");
        builder.append(emp3itrAmount);
        builder.append(", emp3designation=");
        builder.append(emp3designation);
        builder.append(", emp3employerCode=");
        builder.append(emp3employerCode);
        builder.append(", emp3employerBranch=");
        builder.append(emp3employerBranch);
        builder.append(", emp3modePayment=");
        builder.append(emp3modePayment);
        builder.append(", emp3department=");
        builder.append(emp3department);
        builder.append(", emp3workExps=");
        builder.append(emp3workExps);
        builder.append(", emp3businessName=");
        builder.append(emp3businessName);
        builder.append(", emp3commencementString=");
        builder.append(emp3commencementString);
        builder.append(", emp3industryType=");
        builder.append(emp3industryType);
        builder.append(", emp3LastMonthIncomeMonth1=");
        builder.append(emp3LastMonthIncomeMonth1);
        builder.append(", emp3LastMonthIncomeIncome1=");
        builder.append(emp3LastMonthIncomeIncome1);
        builder.append(", emp3LastMonthIncomeMonth2=");
        builder.append(emp3LastMonthIncomeMonth2);
        builder.append(", emp3LastMonthIncomeIncome2=");
        builder.append(emp3LastMonthIncomeIncome2);
        builder.append(", emp3LastMonthIncomeMonth3=");
        builder.append(emp3LastMonthIncomeMonth3);
        builder.append(", emp3LastMonthIncomeIncome3=");
        builder.append(emp3LastMonthIncomeIncome3);
        builder.append(", emp3LastMonthIncomeMonth4=");
        builder.append(emp3LastMonthIncomeMonth4);
        builder.append(", emp3LastMonthIncomeIncome4=");
        builder.append(emp3LastMonthIncomeIncome4);
        builder.append(", emp3LastMonthIncomeMonth5=");
        builder.append(emp3LastMonthIncomeMonth5);
        builder.append(", emp3LastMonthIncomeIncome5=");
        builder.append(emp3LastMonthIncomeIncome5);
        builder.append(", emp4employmentType=");
        builder.append(emp4employmentType);
        builder.append(", emp4employmentName=");
        builder.append(emp4employmentName);
        builder.append(", emp4timeWithEmployer=");
        builder.append(emp4timeWithEmployer);
        builder.append(", emp4StringOfJoining=");
        builder.append(emp4StringOfJoining);
        builder.append(", emp4StringOfLeaving=");
        builder.append(emp4StringOfLeaving);
        builder.append(", emp4monthlySalary=");
        builder.append(emp4monthlySalary);
        builder.append(", emp4grossSalary=");
        builder.append(emp4grossSalary);
        builder.append(", emp4constitution=");
        builder.append(emp4constitution);
        builder.append(", emp4industType=");
        builder.append(emp4industType);
        builder.append(", emp4itrId=");
        builder.append(emp4itrId);
        builder.append(", emp4itrAmount=");
        builder.append(emp4itrAmount);
        builder.append(", emp4designation=");
        builder.append(emp4designation);
        builder.append(", emp4employerCode=");
        builder.append(emp4employerCode);
        builder.append(", emp4employerBranch=");
        builder.append(emp4employerBranch);
        builder.append(", emp4modePayment=");
        builder.append(emp4modePayment);
        builder.append(", emp4department=");
        builder.append(emp4department);
        builder.append(", emp4workExps=");
        builder.append(emp4workExps);
        builder.append(", emp4businessName=");
        builder.append(emp4businessName);
        builder.append(", emp4commencementString=");
        builder.append(emp4commencementString);
        builder.append(", emp4industryType=");
        builder.append(emp4industryType);
        builder.append(", emp4LastMonthIncomeMonth1=");
        builder.append(emp4LastMonthIncomeMonth1);
        builder.append(", emp4LastMonthIncomeIncome1=");
        builder.append(emp4LastMonthIncomeIncome1);
        builder.append(", emp4LastMonthIncomeMonth2=");
        builder.append(emp4LastMonthIncomeMonth2);
        builder.append(", emp4LastMonthIncomeIncome2=");
        builder.append(emp4LastMonthIncomeIncome2);
        builder.append(", emp4LastMonthIncomeMonth3=");
        builder.append(emp4LastMonthIncomeMonth3);
        builder.append(", emp4LastMonthIncomeIncome3=");
        builder.append(emp4LastMonthIncomeIncome3);
        builder.append(", emp4LastMonthIncomeMonth4=");
        builder.append(emp4LastMonthIncomeMonth4);
        builder.append(", emp4LastMonthIncomeIncome4=");
        builder.append(emp4LastMonthIncomeIncome4);
        builder.append(", emp4LastMonthIncomeMonth5=");
        builder.append(emp4LastMonthIncomeMonth5);
        builder.append(", emp4LastMonthIncomeIncome5=");
        builder.append(emp4LastMonthIncomeIncome5);
        builder.append(", emp5employmentType=");
        builder.append(emp5employmentType);
        builder.append(", emp5employmentName=");
        builder.append(emp5employmentName);
        builder.append(", emp5timeWithEmployer=");
        builder.append(emp5timeWithEmployer);
        builder.append(", emp5StringOfJoining=");
        builder.append(emp5StringOfJoining);
        builder.append(", emp5StringOfLeaving=");
        builder.append(emp5StringOfLeaving);
        builder.append(", emp5monthlySalary=");
        builder.append(emp5monthlySalary);
        builder.append(", emp5grossSalary=");
        builder.append(emp5grossSalary);
        builder.append(", emp5constitution=");
        builder.append(emp5constitution);
        builder.append(", emp5industType=");
        builder.append(emp5industType);
        builder.append(", emp5itrId=");
        builder.append(emp5itrId);
        builder.append(", emp5itrAmount=");
        builder.append(emp5itrAmount);
        builder.append(", emp5designation=");
        builder.append(emp5designation);
        builder.append(", emp5employerCode=");
        builder.append(emp5employerCode);
        builder.append(", emp5employerBranch=");
        builder.append(emp5employerBranch);
        builder.append(", emp5modePayment=");
        builder.append(emp5modePayment);
        builder.append(", emp5department=");
        builder.append(emp5department);
        builder.append(", emp5workExps=");
        builder.append(emp5workExps);
        builder.append(", emp5businessName=");
        builder.append(emp5businessName);
        builder.append(", emp5commencementString=");
        builder.append(emp5commencementString);
        builder.append(", emp5industryType=");
        builder.append(emp5industryType);
        builder.append(", emp5LastMonthIncomeMonth1=");
        builder.append(emp5LastMonthIncomeMonth1);
        builder.append(", emp5LastMonthIncomeIncome1=");
        builder.append(emp5LastMonthIncomeIncome1);
        builder.append(", emp5LastMonthIncomeMonth2=");
        builder.append(emp5LastMonthIncomeMonth2);
        builder.append(", emp5LastMonthIncomeIncome2=");
        builder.append(emp5LastMonthIncomeIncome2);
        builder.append(", emp5LastMonthIncomeMonth3=");
        builder.append(emp5LastMonthIncomeMonth3);
        builder.append(", emp5LastMonthIncomeIncome3=");
        builder.append(emp5LastMonthIncomeIncome3);
        builder.append(", emp5LastMonthIncomeMonth4=");
        builder.append(emp5LastMonthIncomeMonth4);
        builder.append(", emp5LastMonthIncomeIncome4=");
        builder.append(emp5LastMonthIncomeIncome4);
        builder.append(", emp5LastMonthIncomeMonth5=");
        builder.append(emp5LastMonthIncomeMonth5);
        builder.append(", emp5LastMonthIncomeIncome5=");
        builder.append(emp5LastMonthIncomeIncome5);
        builder.append(", noOfDependents=");
        builder.append(noOfDependents);
        builder.append(", noOfEarningMembers=");
        builder.append(noOfEarningMembers);
        builder.append(", noOfFamilyMembers=");
        builder.append(noOfFamilyMembers);
        builder.append(", applicantReferenceFirstName=");
        builder.append(applicantReferenceFirstName);
        builder.append(", applicantReferencemiddleName=");
        builder.append(applicantReferencemiddleName);
        builder.append(", applicantReferencelastName=");
        builder.append(applicantReferencelastName);
        builder.append(", applicantReferenceprefix=");
        builder.append(applicantReferenceprefix);
        builder.append(", applicantReferencesuffix=");
        builder.append(applicantReferencesuffix);
        builder.append(", applicantReferenceaddressLine1=");
        builder.append(applicantReferenceaddressLine1);
        builder.append(", applicantReferenceaddressLine2=");
        builder.append(applicantReferenceaddressLine2);
        builder.append(", applicantReferencecity=");
        builder.append(applicantReferencecity);
        builder.append(", applicantReferencepin=");
        builder.append(applicantReferencepin);
        builder.append(", applicantReferencestate=");
        builder.append(applicantReferencestate);
        builder.append(", applicantReferencecountry=");
        builder.append(applicantReferencecountry);
        builder.append(", applicantReferencelandLoard=");
        builder.append(applicantReferencelandLoard);
        builder.append(", applicantReferenceline3=");
        builder.append(applicantReferenceline3);
        builder.append(", applicantReferenceline4=");
        builder.append(applicantReferenceline4);
        builder.append(", applicantReferencevillage=");
        builder.append(applicantReferencevillage);
        builder.append(", applicantReferencedistrict=");
        builder.append(applicantReferencedistrict);
        builder.append(", applicantReferencedistanceFrom=");
        builder.append(applicantReferencedistanceFrom);
        builder.append(", applicantReferencelandMark=");
        builder.append(applicantReferencelandMark);
        builder.append(", applicantReferencerelationType=");
        builder.append(applicantReferencerelationType);
        builder.append(", applicantReferencephoneType=");
        builder.append(applicantReferencephoneType);
        builder.append(", applicantReferenceareaCode=");
        builder.append(applicantReferenceareaCode);
        builder.append(", applicantReferencecountryCode=");
        builder.append(applicantReferencecountryCode);
        builder.append(", applicantReferencephoneNumber=");
        builder.append(applicantReferencephoneNumber);
        builder.append(", applicantReferenceextension=");
        builder.append(applicantReferenceextension);
        builder.append(", applicantReferenceoccupation=");
        builder.append(applicantReferenceoccupation);
        builder.append(", education=");
        builder.append(education);
        builder.append(", creditCardNumber=");
        builder.append(creditCardNumber);
        builder.append(", mobileVerified=");
        builder.append(mobileVerified);
        builder.append(", addharVerified=");
        builder.append(addharVerified);
        builder.append(", accountHolderbankName1=");
        builder.append(accountHolderbankName1);
        builder.append(", accountHolderbranchName1=");
        builder.append(accountHolderbranchName1);
        builder.append(", accountHolderaccountType1=");
        builder.append(accountHolderaccountType1);
        builder.append(", accountHolderaccountNumber1=");
        builder.append(accountHolderaccountNumber1);
        builder.append(", accountHoldersalaryAccount1=");
        builder.append(accountHoldersalaryAccount1);
        builder.append(", accountHolderinwardChequeReturn1=");
        builder.append(accountHolderinwardChequeReturn1);
        builder.append(", accountHolderoutwardChequeReturn1=");
        builder.append(accountHolderoutwardChequeReturn1);
        builder.append(", accountHolderavgBankBalance1=");
        builder.append(accountHolderavgBankBalance1);
        builder.append(", accountHolderdeductedEmiAmt1=");
        builder.append(accountHolderdeductedEmiAmt1);
        builder.append(", accountHolderanyEmi1=");
        builder.append(accountHolderanyEmi1);
        builder.append(", accountHoldercurrentlyRunningLoan1=");
        builder.append(accountHoldercurrentlyRunningLoan1);
        builder.append(", accountHoldermentionAmount1=");
        builder.append(accountHoldermentionAmount1);
        builder.append(", accountHolderfirstName1=");
        builder.append(accountHolderfirstName1);
        builder.append(", accountHoldermiddleName1=");
        builder.append(accountHoldermiddleName1);
        builder.append(", accountHolderlastName1=");
        builder.append(accountHolderlastName1);
        builder.append(", accountHolderprefix1=");
        builder.append(accountHolderprefix1);
        builder.append(", accountHoldersuffix1=");
        builder.append(accountHoldersuffix1);
        builder.append(", accountHolderbankName2=");
        builder.append(accountHolderbankName2);
        builder.append(", accountHolderbranchName2=");
        builder.append(accountHolderbranchName2);
        builder.append(", accountHolderaccountType2=");
        builder.append(accountHolderaccountType2);
        builder.append(", accountHolderaccountNumber2=");
        builder.append(accountHolderaccountNumber2);
        builder.append(", accountHoldersalaryAccount2=");
        builder.append(accountHoldersalaryAccount2);
        builder.append(", accountHolderinwardChequeReturn2=");
        builder.append(accountHolderinwardChequeReturn2);
        builder.append(", accountHolderoutwardChequeReturn2=");
        builder.append(accountHolderoutwardChequeReturn2);
        builder.append(", accountHolderavgBankBalance2=");
        builder.append(accountHolderavgBankBalance2);
        builder.append(", accountHolderdeductedEmiAmt2=");
        builder.append(accountHolderdeductedEmiAmt2);
        builder.append(", accountHolderanyEmi2=");
        builder.append(accountHolderanyEmi2);
        builder.append(", accountHoldercurrentlyRunningLoan2=");
        builder.append(accountHoldercurrentlyRunningLoan2);
        builder.append(", accountHoldermentionAmount2=");
        builder.append(accountHoldermentionAmount2);
        builder.append(", accountHolderfirstName2=");
        builder.append(accountHolderfirstName2);
        builder.append(", accountHoldermiddleName2=");
        builder.append(accountHoldermiddleName2);
        builder.append(", accountHolderlastName2=");
        builder.append(accountHolderlastName2);
        builder.append(", accountHolderprefix2=");
        builder.append(accountHolderprefix2);
        builder.append(", accountHoldersuffix2=");
        builder.append(accountHoldersuffix2);
        builder.append(", accountHolderbankName3=");
        builder.append(accountHolderbankName3);
        builder.append(", accountHolderbranchName3=");
        builder.append(accountHolderbranchName3);
        builder.append(", accountHolderaccountType3=");
        builder.append(accountHolderaccountType3);
        builder.append(", accountHolderaccountNumber3=");
        builder.append(accountHolderaccountNumber3);
        builder.append(", accountHoldersalaryAccount3=");
        builder.append(accountHoldersalaryAccount3);
        builder.append(", accountHolderinwardChequeReturn3=");
        builder.append(accountHolderinwardChequeReturn3);
        builder.append(", accountHolderoutwardChequeReturn3=");
        builder.append(accountHolderoutwardChequeReturn3);
        builder.append(", accountHolderavgBankBalance3=");
        builder.append(accountHolderavgBankBalance3);
        builder.append(", accountHolderdeductedEmiAmt3=");
        builder.append(accountHolderdeductedEmiAmt3);
        builder.append(", accountHolderanyEmi3=");
        builder.append(accountHolderanyEmi3);
        builder.append(", accountHoldercurrentlyRunningLoan3=");
        builder.append(accountHoldercurrentlyRunningLoan3);
        builder.append(", accountHoldermentionAmount3=");
        builder.append(accountHoldermentionAmount3);
        builder.append(", accountHolderfirstName3=");
        builder.append(accountHolderfirstName3);
        builder.append(", accountHoldermiddleName3=");
        builder.append(accountHoldermiddleName3);
        builder.append(", accountHolderlastName3=");
        builder.append(accountHolderlastName3);
        builder.append(", accountHolderprefix3=");
        builder.append(accountHolderprefix3);
        builder.append(", accountHoldersuffix3=");
        builder.append(accountHoldersuffix3);
        builder.append(", accountHolderbankName4=");
        builder.append(accountHolderbankName4);
        builder.append(", accountHolderbranchName4=");
        builder.append(accountHolderbranchName4);
        builder.append(", accountHolderaccountType4=");
        builder.append(accountHolderaccountType4);
        builder.append(", accountHolderaccountNumber4=");
        builder.append(accountHolderaccountNumber4);
        builder.append(", accountHoldersalaryAccount4=");
        builder.append(accountHoldersalaryAccount4);
        builder.append(", accountHolderinwardChequeReturn4=");
        builder.append(accountHolderinwardChequeReturn4);
        builder.append(", accountHolderoutwardChequeReturn4=");
        builder.append(accountHolderoutwardChequeReturn4);
        builder.append(", accountHolderavgBankBalance4=");
        builder.append(accountHolderavgBankBalance4);
        builder.append(", accountHolderdeductedEmiAmt4=");
        builder.append(accountHolderdeductedEmiAmt4);
        builder.append(", accountHolderanyEmi4=");
        builder.append(accountHolderanyEmi4);
        builder.append(", accountHoldercurrentlyRunningLoan4=");
        builder.append(accountHoldercurrentlyRunningLoan4);
        builder.append(", accountHoldermentionAmount4=");
        builder.append(accountHoldermentionAmount4);
        builder.append(", accountHolderfirstName4=");
        builder.append(accountHolderfirstName4);
        builder.append(", accountHoldermiddleName4=");
        builder.append(accountHoldermiddleName4);
        builder.append(", accountHolderlastName4=");
        builder.append(accountHolderlastName4);
        builder.append(", accountHolderprefix4=");
        builder.append(accountHolderprefix4);
        builder.append(", accountHoldersuffix4=");
        builder.append(accountHoldersuffix4);
        builder.append(", accountHolderbankName5=");
        builder.append(accountHolderbankName5);
        builder.append(", accountHolderbranchName5=");
        builder.append(accountHolderbranchName5);
        builder.append(", accountHolderaccountType5=");
        builder.append(accountHolderaccountType5);
        builder.append(", accountHolderaccountNumber5=");
        builder.append(accountHolderaccountNumber5);
        builder.append(", accountHoldersalaryAccount5=");
        builder.append(accountHoldersalaryAccount5);
        builder.append(", accountHolderinwardChequeReturn5=");
        builder.append(accountHolderinwardChequeReturn5);
        builder.append(", accountHolderoutwardChequeReturn5=");
        builder.append(accountHolderoutwardChequeReturn5);
        builder.append(", accountHolderavgBankBalance5=");
        builder.append(accountHolderavgBankBalance5);
        builder.append(", accountHolderdeductedEmiAmt5=");
        builder.append(accountHolderdeductedEmiAmt5);
        builder.append(", accountHolderanyEmi5=");
        builder.append(accountHolderanyEmi5);
        builder.append(", accountHoldercurrentlyRunningLoan5=");
        builder.append(accountHoldercurrentlyRunningLoan5);
        builder.append(", accountHoldermentionAmount5=");
        builder.append(accountHoldermentionAmount5);
        builder.append(", accountHolderfirstName5=");
        builder.append(accountHolderfirstName5);
        builder.append(", accountHoldermiddleName5=");
        builder.append(accountHoldermiddleName5);
        builder.append(", accountHolderlastName5=");
        builder.append(accountHolderlastName5);
        builder.append(", accountHolderprefix5=");
        builder.append(accountHolderprefix5);
        builder.append(", accountHoldersuffix5=");
        builder.append(accountHoldersuffix5);
        builder.append(", loanOwnership1=");
        builder.append(loanOwnership1);
        builder.append(", loanType1=");
        builder.append(loanType1);
        builder.append(", loanPurpose1=");
        builder.append(loanPurpose1);
        builder.append(", loanAcountNo1=");
        builder.append(loanAcountNo1);
        builder.append(", creditGranter1=");
        builder.append(creditGranter1);
        builder.append(", loanAmt1=");
        builder.append(loanAmt1);
        builder.append(", emiAmt1=");
        builder.append(emiAmt1);
        builder.append(", mob1=");
        builder.append(mob1);
        builder.append(", balanceTenure1=");
        builder.append(balanceTenure1);
        builder.append(", repaymentBankName1=");
        builder.append(repaymentBankName1);
        builder.append(", obligate1=");
        builder.append(obligate1);
        builder.append(", loanApr1=");
        builder.append(loanApr1);
        builder.append(", loanOwnership2=");
        builder.append(loanOwnership2);
        builder.append(", loanType2=");
        builder.append(loanType2);
        builder.append(", loanPurpose2=");
        builder.append(loanPurpose2);
        builder.append(", loanAcountNo2=");
        builder.append(loanAcountNo2);
        builder.append(", creditGranter2=");
        builder.append(creditGranter2);
        builder.append(", loanAmt2=");
        builder.append(loanAmt2);
        builder.append(", emiAmt2=");
        builder.append(emiAmt2);
        builder.append(", mob2=");
        builder.append(mob2);
        builder.append(", balanceTenure2=");
        builder.append(balanceTenure2);
        builder.append(", repaymentBankName2=");
        builder.append(repaymentBankName2);
        builder.append(", obligate2=");
        builder.append(obligate2);
        builder.append(", loanApr2=");
        builder.append(loanApr2);
        builder.append(", loanOwnership3=");
        builder.append(loanOwnership3);
        builder.append(", loanType3=");
        builder.append(loanType3);
        builder.append(", loanPurpose3=");
        builder.append(loanPurpose3);
        builder.append(", loanAcountNo3=");
        builder.append(loanAcountNo3);
        builder.append(", creditGranter3=");
        builder.append(creditGranter3);
        builder.append(", loanAmt3=");
        builder.append(loanAmt3);
        builder.append(", emiAmt3=");
        builder.append(emiAmt3);
        builder.append(", mob3=");
        builder.append(mob3);
        builder.append(", balanceTenure3=");
        builder.append(balanceTenure3);
        builder.append(", repaymentBankName3=");
        builder.append(repaymentBankName3);
        builder.append(", obligate3=");
        builder.append(obligate3);
        builder.append(", loanApr3=");
        builder.append(loanApr3);
        builder.append(", loanOwnership4=");
        builder.append(loanOwnership4);
        builder.append(", loanType4=");
        builder.append(loanType4);
        builder.append(", loanPurpose4=");
        builder.append(loanPurpose4);
        builder.append(", loanAcountNo4=");
        builder.append(loanAcountNo4);
        builder.append(", creditGranter4=");
        builder.append(creditGranter4);
        builder.append(", loanAmt4=");
        builder.append(loanAmt4);
        builder.append(", emiAmt4=");
        builder.append(emiAmt4);
        builder.append(", mob4=");
        builder.append(mob4);
        builder.append(", balanceTenure4=");
        builder.append(balanceTenure4);
        builder.append(", repaymentBankName4=");
        builder.append(repaymentBankName4);
        builder.append(", obligate4=");
        builder.append(obligate4);
        builder.append(", loanApr4=");
        builder.append(loanApr4);
        builder.append(", loanOwnership5=");
        builder.append(loanOwnership5);
        builder.append(", loanType5=");
        builder.append(loanType5);
        builder.append(", loanPurpose5=");
        builder.append(loanPurpose5);
        builder.append(", loanAcountNo5=");
        builder.append(loanAcountNo5);
        builder.append(", creditGranter5=");
        builder.append(creditGranter5);
        builder.append(", loanAmt5=");
        builder.append(loanAmt5);
        builder.append(", emiAmt5=");
        builder.append(emiAmt5);
        builder.append(", mob5=");
        builder.append(mob5);
        builder.append(", balanceTenure5=");
        builder.append(balanceTenure5);
        builder.append(", repaymentBankName5=");
        builder.append(repaymentBankName5);
        builder.append(", obligate5=");
        builder.append(obligate5);
        builder.append(", loanApr5=");
        builder.append(loanApr5);
        builder.append(", incomeDocumentAvailable=");
        builder.append(incomeDocumentAvailable);
        builder.append(", otherSourceOfIncome1=");
        builder.append(otherSourceOfIncome1);
        builder.append(", otherSourceOfIncome2=");
        builder.append(otherSourceOfIncome2);
        builder.append(", otherSourceOfIncome3=");
        builder.append(otherSourceOfIncome3);
        builder.append(", otherSourceOfIncome4=");
        builder.append(otherSourceOfIncome4);
        builder.append(", otherSourceOfIncome5=");
        builder.append(otherSourceOfIncome5);
        builder.append(", otherSourceIncomeAmount=");
        builder.append(otherSourceIncomeAmount);
        builder.append(", basicIncome=");
        builder.append(basicIncome);
        builder.append(", dearnessAllowance=");
        builder.append(dearnessAllowance);
        builder.append(", houseRentAllowance=");
        builder.append(houseRentAllowance);
        builder.append(", cityCompensatoryAllowance=");
        builder.append(cityCompensatoryAllowance);
        builder.append(", otherAllowance=");
        builder.append(otherAllowance);
        builder.append(", otherSourcesIncome=");
        builder.append(otherSourcesIncome);
        builder.append(", grossIncome=");
        builder.append(grossIncome);
        builder.append(", netIncome=");
        builder.append(netIncome);
        builder.append(", basic=");
        builder.append(basic);
        builder.append(", DA=");
        builder.append(DA);
        builder.append(", HRA=");
        builder.append(HRA);
        builder.append(", CCA=");
        builder.append(CCA);
        builder.append(", incentive=");
        builder.append(incentive);
        builder.append(", other=");
        builder.append(other);
        builder.append(", otherSourceIncome=");
        builder.append(otherSourceIncome);
        builder.append(", PF=");
        builder.append(PF);
        builder.append(", professionalTax=");
        builder.append(professionalTax);
        builder.append(", LIC=");
        builder.append(LIC);
        builder.append(", ESI=");
        builder.append(ESI);
        builder.append(", lastMonthIncomeMonth1=");
        builder.append(lastMonthIncomeMonth1);
        builder.append(", lastMonthIncomeMonthIncome1=");
        builder.append(lastMonthIncomeMonthIncome1);
        builder.append(", lastMonthIncomeMonth2=");
        builder.append(lastMonthIncomeMonth2);
        builder.append(", lastMonthIncomeMonthIncome2=");
        builder.append(lastMonthIncomeMonthIncome2);
        builder.append(", lastMonthIncomeMonth3=");
        builder.append(lastMonthIncomeMonth3);
        builder.append(", lastMonthIncomeMonthIncome3=");
        builder.append(lastMonthIncomeMonthIncome3);
        builder.append(", lastMonthIncomeMonth4=");
        builder.append(lastMonthIncomeMonth4);
        builder.append(", lastMonthIncomeMonthIncome4=");
        builder.append(lastMonthIncomeMonthIncome4);
        builder.append(", lastMonthIncomeMonth5=");
        builder.append(lastMonthIncomeMonth5);
        builder.append(", lastMonthIncomeMonthIncome5=");
        builder.append(lastMonthIncomeMonthIncome5);
        builder.append(", providentFund=");
        builder.append(providentFund);
        builder.append(", stateInsurance=");
        builder.append(stateInsurance);
        builder.append(", otherDeductions=");
        builder.append(otherDeductions);
        builder.append(", lastTwoMonthSalary=");
        builder.append(lastTwoMonthSalary);
        builder.append(", obligations=");
        builder.append(obligations);
        builder.append(", remarks=");
        builder.append(remarks);
        builder.append(", year1=");
        builder.append(year1);
        builder.append(", itrString1=");
        builder.append(itrString1);
        builder.append(", turnOver1=");
        builder.append(turnOver1);
        builder.append(", depreciation1=");
        builder.append(depreciation1);
        builder.append(", netProfit1=");
        builder.append(netProfit1);
        builder.append(", otherIncome1=");
        builder.append(otherIncome1);
        builder.append(", year2=");
        builder.append(year2);
        builder.append(", itrString2=");
        builder.append(itrString2);
        builder.append(", turnOver2=");
        builder.append(turnOver2);
        builder.append(", depreciation2=");
        builder.append(depreciation2);
        builder.append(", netProfit2=");
        builder.append(netProfit2);
        builder.append(", otherIncome2=");
        builder.append(otherIncome2);
        builder.append(", year3=");
        builder.append(year3);
        builder.append(", itrString3=");
        builder.append(itrString3);
        builder.append(", turnOver3=");
        builder.append(turnOver3);
        builder.append(", depreciation3=");
        builder.append(depreciation3);
        builder.append(", netProfit3=");
        builder.append(netProfit3);
        builder.append(", otherIncome3=");
        builder.append(otherIncome3);
        builder.append(", year4=");
        builder.append(year4);
        builder.append(", itrString4=");
        builder.append(itrString4);
        builder.append(", turnOver4=");
        builder.append(turnOver4);
        builder.append(", depreciation4=");
        builder.append(depreciation4);
        builder.append(", netProfit4=");
        builder.append(netProfit4);
        builder.append(", otherIncome4=");
        builder.append(otherIncome4);
        builder.append(", year5=");
        builder.append(year5);
        builder.append(", itrString5=");
        builder.append(itrString5);
        builder.append(", turnOver5=");
        builder.append(turnOver5);
        builder.append(", depreciation5=");
        builder.append(depreciation5);
        builder.append(", netProfit5=");
        builder.append(netProfit5);
        builder.append(", otherIncome5=");
        builder.append(otherIncome5);
        builder.append(", carSurrogateModelName1=");
        builder.append(carSurrogateModelName1);
        builder.append(", carSurrogateManufactureYear1=");
        builder.append(carSurrogateManufactureYear1);
        builder.append(", carSurrogateRegistrationNumber1=");
        builder.append(carSurrogateRegistrationNumber1);
        builder.append(", carSurrogateModelName2=");
        builder.append(carSurrogateModelName2);
        builder.append(", carSurrogateManufactureYear2=");
        builder.append(carSurrogateManufactureYear2);
        builder.append(", carSurrogateRegistrationNumber2=");
        builder.append(carSurrogateRegistrationNumber2);
        builder.append(", carSurrogateModelName3=");
        builder.append(carSurrogateModelName3);
        builder.append(", carSurrogateManufactureYear3=");
        builder.append(carSurrogateManufactureYear3);
        builder.append(", carSurrogateRegistrationNumber3=");
        builder.append(carSurrogateRegistrationNumber3);
        builder.append(", carSurrogateModelName4=");
        builder.append(carSurrogateModelName4);
        builder.append(", carSurrogateManufactureYear4=");
        builder.append(carSurrogateManufactureYear4);
        builder.append(", carSurrogateRegistrationNumber4=");
        builder.append(carSurrogateRegistrationNumber4);
        builder.append(", carSurrogateModelName5=");
        builder.append(carSurrogateModelName5);
        builder.append(", carSurrogateManufactureYear5=");
        builder.append(carSurrogateManufactureYear5);
        builder.append(", carSurrogateRegistrationNumber5=");
        builder.append(carSurrogateRegistrationNumber5);
        builder.append(", salariedSurrogateNetTakeHome1=");
        builder.append(salariedSurrogateNetTakeHome1);
        builder.append(", salariedSurrogateNetTakeHome2=");
        builder.append(salariedSurrogateNetTakeHome2);
        builder.append(", salariedSurrogateNetTakeHome3=");
        builder.append(salariedSurrogateNetTakeHome3);
        builder.append(", salariedSurrogateNetTakeHome4=");
        builder.append(salariedSurrogateNetTakeHome4);
        builder.append(", salariedSurrogateNetTakeHome5=");
        builder.append(salariedSurrogateNetTakeHome5);
        builder.append(", traderSurrogateYearInBussines1=");
        builder.append(traderSurrogateYearInBussines1);
        builder.append(", traderSurrogateBussinesProof1=");
        builder.append(traderSurrogateBussinesProof1);
        builder.append(", traderSurrogateYearInBussines2=");
        builder.append(traderSurrogateYearInBussines2);
        builder.append(", traderSurrogateBussinesProof2=");
        builder.append(traderSurrogateBussinesProof2);
        builder.append(", traderSurrogateYearInBussines3=");
        builder.append(traderSurrogateYearInBussines3);
        builder.append(", traderSurrogateBussinesProof3=");
        builder.append(traderSurrogateBussinesProof3);
        builder.append(", traderSurrogateYearInBussines4=");
        builder.append(traderSurrogateYearInBussines4);
        builder.append(", traderSurrogateBussinesProof4=");
        builder.append(traderSurrogateBussinesProof4);
        builder.append(", traderSurrogateYearInBussines5=");
        builder.append(traderSurrogateYearInBussines5);
        builder.append(", traderSurrogateBussinesProof5=");
        builder.append(traderSurrogateBussinesProof5);
        builder.append("]");
        return builder.toString();
    }


    /*
     * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
    @Override
    public int compareTo(ApplicationTAT o) {
        return this.refID.compareTo(o.refID);

    }

    public String printMessage() {
        return "Hello";
    }

}
