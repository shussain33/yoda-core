package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.Map.Entry;

/**
 * @author kishor This app is used to define report format of multiple
 *         structure.
 */
public class FlatReportConfiguration {

    @JsonProperty("mHeaderMap")
    private Map<Integer, ColumnConfiguration> headerMap;

    @JsonProperty("sReportName")
    private String reportName;

    @JsonProperty("sReportType")
    private String reportType;

    @JsonProperty("sReportFomat")
    private String reportFormat;

    @JsonProperty("sHeader")
    private String header;

    @JsonProperty("sSeperator")
    private char seperater = ',';

    /**
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * @return the seperater
     */
    public char getSeperater() {
        return seperater;
    }

    /**
     * @param seperater the seperater to set
     */
    public void setSeperater(char seperater) {
        this.seperater = seperater;
    }

    public Map<Integer, ColumnConfiguration> getHeaderMap() {
        return headerMap;
    }

    public void setHeaderMap(Map<Integer, ColumnConfiguration> headerMap) {
        this.headerMap = headerMap;
    }

    /**
     * @return the reportName
     */
    public String getReportName() {
        return reportName;
    }

    /**
     * @param reportName the reportName to set
     */
    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    /**
     * @return the reportType
     */
    public String getReportType() {
        return reportType;
    }

    /**
     * @param reportType the reportType to set
     */
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    /**
     * @return the reportFormat
     */
    public String getReportFormat() {
        return reportFormat;
    }

    /**
     * @param reportFormat the reportFormat to set
     */
    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    public String getFileHeader() {
        StringBuilder fileHeaderBuilder = new StringBuilder();
        for (Entry<Integer, ColumnConfiguration> column : getHeaderMap()
                .entrySet()) {
            fileHeaderBuilder.append(column.getValue().getColumnDisplayName().toUpperCase())
                    .append(getSeperater());
        }


        return fileHeaderBuilder.toString();

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FlatReportConfiguration [headerMap=");
        builder.append(headerMap);
        builder.append(", reportName=");
        builder.append(reportName);
        builder.append(", reportType=");
        builder.append(reportType);
        builder.append(", reportFormat=");
        builder.append(reportFormat);
        builder.append(", header=");
        builder.append(header);
        builder.append(", seperater=");
        builder.append(seperater);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result
                + ((headerMap == null) ? 0 : headerMap.hashCode());
        result = prime * result
                + ((reportFormat == null) ? 0 : reportFormat.hashCode());
        result = prime * result
                + ((reportName == null) ? 0 : reportName.hashCode());
        result = prime * result
                + ((reportType == null) ? 0 : reportType.hashCode());
        result = prime * result + seperater;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FlatReportConfiguration other = (FlatReportConfiguration) obj;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (headerMap == null) {
            if (other.headerMap != null)
                return false;
        } else if (!headerMap.equals(other.headerMap))
            return false;
        if (reportFormat == null) {
            if (other.reportFormat != null)
                return false;
        } else if (!reportFormat.equals(other.reportFormat))
            return false;
        if (reportName == null) {
            if (other.reportName != null)
                return false;
        } else if (!reportName.equals(other.reportName))
            return false;
        if (reportType == null) {
            if (other.reportType != null)
                return false;
        } else if (!reportType.equals(other.reportType))
            return false;
        if (seperater != other.seperater)
            return false;
        return true;
    }

}
