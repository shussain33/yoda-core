package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

/**
 * @author kishor Header app is used to configured report and its
 *         format. it has used to configured multiple type report.
 *         <p/>
 *         {
 *         sCloumnKey:"",
 *         sColumnDisplayName: "",
 *         sColumnIndex:0,
 *         bViewable:false,
 *         bDownloadable:false
 *         }
 */
public class ColumnConfiguration implements Comparable<ColumnConfiguration> {

    @JsonProperty("oFormat")
    private Format format;

    @JsonProperty("sCloumnKey")
    private String columnKey;

    @JsonProperty("sColDispName")
    private String columnDisplayName;

    @JsonProperty("sColIndex")
    private int columnIndex;

    @JsonProperty("bViewable")
    private boolean viewable;

    @JsonProperty("bDownloadable")
    private boolean downloadable;

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public boolean isViewable() {
        return viewable;
    }

    public void setViewable(boolean viewable) {
        this.viewable = viewable;
    }

    public boolean isDownloadable() {
        return downloadable;
    }

    public void setDownloadable(boolean downloadable) {
        this.downloadable = downloadable;
    }

    /**
     * @return the columnKey
     */
    public String getColumnKey() {
        return columnKey;
    }

    /**
     * @param columnKey the columnKey to set
     */
    public void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }

    /**
     * @return the columnDisplayName
     */
    public String getColumnDisplayName() {
        return columnDisplayName;
    }

    /**
     * @param columnDisplayName the columnDisplayName to set
     */
    public void setColumnDisplayName(String columnDisplayName) {
        this.columnDisplayName = columnDisplayName;
    }

    /**
     * @return the columnIndex
     */
    public int getColumnIndex() {
        return columnIndex;
    }

    /**
     * @param columnIndex the columnIndex to set
     */
    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Messages.getString("ColumnConfiguration.0")); //$NON-NLS-1$
        builder.append(columnKey);
        builder.append(Messages.getString("ColumnConfiguration.1")); //$NON-NLS-1$
        builder.append(columnDisplayName);
        builder.append(Messages.getString("ColumnConfiguration.2")); //$NON-NLS-1$
        builder.append(columnIndex);
        builder.append(Messages.getString("ColumnConfiguration.3")); //$NON-NLS-1$
        builder.append(viewable);
        builder.append(Messages.getString("ColumnConfiguration.4")); //$NON-NLS-1$
        builder.append(downloadable);
        builder.append(Messages.getString("ColumnConfiguration.5")); //$NON-NLS-1$
        return builder.toString();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ColumnConfiguration configuration = (ColumnConfiguration) obj;
        if (columnKey == null) {
            if (configuration.columnKey != null)
                return false;
        } else if (!columnKey.equals(configuration.columnKey))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((columnKey == null) ? 0 : columnKey.hashCode());
        return result;
    }

    public int compareTo(ColumnConfiguration o) {

        if (StringUtils.isNotBlank(this.columnKey) && StringUtils.isNotBlank(o.columnKey)) {
            return this.columnKey.equalsIgnoreCase(o.columnKey) ? 0 : 1;
        }

        return 1;
    }


}
