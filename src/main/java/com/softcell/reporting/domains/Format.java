package com.softcell.reporting.domains;

/**
 * @author kishorp This class is use to define custom conversion of field from
 *         one format to readable format.
 */
public class Format {

    private String to;

    private String from;

    private Action action;

    private DataType dataType;

    private Action capitalize;

    public String getTo() {
        return to;
    }

    public void setTo(String toFormat) {
        this.to = toFormat;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String fromFormat) {
        this.from = fromFormat;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public Action getCapitalize() {
        return capitalize;
    }

    public void setCapitalize(Action capitalize) {
        this.capitalize = capitalize;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Format [to=");
        builder.append(to);
        builder.append(", from=");
        builder.append(from);
        builder.append(", action=");
        builder.append(action);
        builder.append(", dataType=");
        builder.append(dataType);
        builder.append(", capitalize=");
        builder.append(capitalize);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result
                + ((capitalize == null) ? 0 : capitalize.hashCode());
        result = prime * result
                + ((dataType == null) ? 0 : dataType.hashCode());
        result = prime * result + ((from == null) ? 0 : from.hashCode());
        result = prime * result + ((to == null) ? 0 : to.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Format other = (Format) obj;
        if (action != other.action)
            return false;
        if (capitalize != other.capitalize)
            return false;
        if (dataType != other.dataType)
            return false;
        if (from == null) {
            if (other.from != null)
                return false;
        } else if (!from.equals(other.from))
            return false;
        if (to == null) {
            if (other.to != null)
                return false;
        } else if (!to.equals(other.to))
            return false;
        return true;
    }

    public enum Action {
        replace, convert, capitalize
    }

    public enum DataType {
        date, string
    }

}
