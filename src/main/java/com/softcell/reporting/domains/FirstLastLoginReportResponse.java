package com.softcell.reporting.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

@Component
@JsonIgnoreProperties(ignoreUnknown = true)
public class FirstLastLoginReportResponse {

    @JsonProperty(value = "sDay")
    private String day;

    @JsonProperty(value = "sMonth")
    private String month;

    @JsonProperty(value = "sYear")
    private String year;

    @JsonProperty(value = "sDealerId")
    private String dealerId;

    @JsonProperty(value = "sFirstLogin")
    private String firstLogin;

    @JsonProperty(value = "sLastLogin")
    private String lastLogin;

    @JsonProperty(value = "count")
    private Integer count;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("FirstLastLoginReportResponse [day=");
        builder.append(day);
        builder.append(", month=");
        builder.append(month);
        builder.append(", year=");
        builder.append(year);
        builder.append(", dealerId=");
        builder.append(dealerId);
        builder.append(", firstLogin=");
        builder.append(firstLogin);
        builder.append(", lastLogin=");
        builder.append(lastLogin);
        builder.append(", count=");
        builder.append(count);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((count == null) ? 0 : count.hashCode());
        result = prime * result + ((day == null) ? 0 : day.hashCode());
        result = prime * result
                + ((dealerId == null) ? 0 : dealerId.hashCode());
        result = prime * result
                + ((firstLogin == null) ? 0 : firstLogin.hashCode());
        result = prime * result
                + ((lastLogin == null) ? 0 : lastLogin.hashCode());
        result = prime * result + ((month == null) ? 0 : month.hashCode());
        result = prime * result + ((year == null) ? 0 : year.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FirstLastLoginReportResponse other = (FirstLastLoginReportResponse) obj;
        if (count == null) {
            if (other.count != null)
                return false;
        } else if (!count.equals(other.count))
            return false;
        if (day == null) {
            if (other.day != null)
                return false;
        } else if (!day.equals(other.day))
            return false;
        if (dealerId == null) {
            if (other.dealerId != null)
                return false;
        } else if (!dealerId.equals(other.dealerId))
            return false;
        if (firstLogin == null) {
            if (other.firstLogin != null)
                return false;
        } else if (!firstLogin.equals(other.firstLogin))
            return false;
        if (lastLogin == null) {
            if (other.lastLogin != null)
                return false;
        } else if (!lastLogin.equals(other.lastLogin))
            return false;
        if (month == null) {
            if (other.month != null)
                return false;
        } else if (!month.equals(other.month))
            return false;
        if (year == null) {
            if (other.year != null)
                return false;
        } else if (!year.equals(other.year))
            return false;
        return true;
    }

}
