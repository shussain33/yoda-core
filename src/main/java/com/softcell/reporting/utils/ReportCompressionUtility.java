package com.softcell.reporting.utils;


import com.google.common.base.CharMatcher;
import com.softcell.constants.FieldSeparator;
import com.softcell.constants.KYC_TYPES;
import com.softcell.gonogo.model.core.Kyc;
import com.softcell.reporting.builder.SaleReportParser;
import com.softcell.reporting.domains.ColumnConfiguration;
import com.softcell.reporting.domains.FlatReportConfiguration;
import com.softcell.reporting.domains.Format;
import com.softcell.reporting.domains.Format.Action;
import com.softcell.reporting.domains.Format.DataType;
import com.softcell.reporting.domains.SalesReportResponse;
import com.softcell.utils.GnGFileUtils;
import com.softcell.utils.GngDateUtil;
import com.softcell.utils.GngUtils;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author kishorp
 */
@Component
public class ReportCompressionUtility {

    private static  final Logger logger = LoggerFactory.getLogger(ReportCompressionUtility.class);

    public static SortedMap<Integer, ColumnConfiguration> salesreportV1 = new TreeMap<Integer, ColumnConfiguration>() {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        {

            int index = 0;
            ColumnConfiguration columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("DSA_ID");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_dsaId999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("APP_ID");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_id999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("FULL NAME");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_fullName999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("DOB");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_dob999");
            Format format = new Format();
            format.setTo("MM/dd/yyyy");
            format.setFrom("ddMMyyyy");
            format.setAction(Action.convert);
            format.setDataType(DataType.date);
            columnConfiguration.setFormat(format);

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("GENDER");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_gender999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("APPLIED AMT");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_appliedAmount999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("APPLICATION DATE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_applicationDate999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("ASSET CTGR");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_assetCtg999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("MAKE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_assetMake999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("DEALER NAME");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_dlrName999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("MODEL NO");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_modelNo999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("APPLICATION STATUS");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_applicationStatus999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("AADHAAR");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_aadhar999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("PAN");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_pan999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("DRIVING_LICENSE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_drivingLicense999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("PASSPORT");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_passport999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("VOTER_ID");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_voterId999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("DO_DATE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_doDate999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("POSTIPA_ADVANCEEMI");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_advanceEmi999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_MANUFSUBBORNEBYDEALER");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_manufSubBorneByDealer999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_DEALERSUBVENTION");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_dealerSubvention999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_MARGINMONEYINSTRUMENT");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_marginMoneyInstrument999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_OTHERCHARGESIFANY");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_otherChargesIfAny999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("POSTIPA_PROCESSINGFEES");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_manufacturingProcessingFee999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("POSTIPA_SCHEME");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_scheme999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("POSTIPA_TOTALASSETCOST");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_totalAssetCost999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_ASSETDETAILS_ASSETCTG");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_assetCtg999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_ASSETDETAILS_ASSETMAKE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_assetMake999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_ASSETDETAILS_DLRNAME");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_dlrName999");
            columnConfiguration.setDownloadable(true);

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_ASSETDETAILS_MODELNO");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_modelNo999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_ASSETDETAILS_PRICE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PAA_price999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("LOS_ID");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_losId999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("UTR_NO");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_utrNo999");

            put(index, columnConfiguration);

            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("LOS_STATUS");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_losStatus999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("POSTIPA_APPROVEDAMOUNT");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_approvedAmount999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration
                    .setColumnDisplayName("POSTIPA_MARGINMONEYCONFIRMATION");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_marginMoneyConfirmation999");

            put(index, columnConfiguration);
            index++;
            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("NET FUNDING AMOUNT");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("PA_netFundingAmount999");
            put(index, columnConfiguration);
            index++;

            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("REMARK");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("f_remark999");
            put(index, columnConfiguration);
            index++;

            // adding invoice details

            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("INVOICE_NUMBER");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("invoiceNumber999");
            put(index, columnConfiguration);
            index++;

            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("INVOICE_DATE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("invoiceDate");
            put(index, columnConfiguration);
            index++;

            columnConfiguration = new ColumnConfiguration();
            columnConfiguration.setColumnDisplayName("INVOICE_TYPE");
            columnConfiguration.setColumnIndex(index);
            columnConfiguration.setColumnKey("invoiceType");
            put(index, columnConfiguration);
            index++;


        }
    };

    public byte[] salesReport(Stream<SalesReportResponse> aggregationResults) throws IOException {

        OutputStreamWriter reportOutputStreamWriter = null;

        ByteArrayOutputStream outputStream1 = null;

        /*BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
                new File(GngUtils.getReSourcePath() + "SalesReport_"
                        + GngDateUtil.getMmDdYyyyUnderSqure(new DateTime())
                        + ".csv")));*/

        FlatReportConfiguration flatReprtConfiguration = new FlatReportConfiguration();

        flatReprtConfiguration.setHeaderMap(salesreportV1);

        flatReprtConfiguration.setSeperater(FieldSeparator.COMMA);

        flatReprtConfiguration.setReportName("GNGVersion2");

        flatReprtConfiguration.setReportType("CSV");

        flatReprtConfiguration.setReportFormat("CSV");

        try {

            outputStream1 = new ByteArrayOutputStream();

            reportOutputStreamWriter = new OutputStreamWriter(outputStream1, StandardCharsets.UTF_8);

            reportOutputStreamWriter.write(flatReprtConfiguration.getFileHeader());

            reportOutputStreamWriter.append('\n');

            /*bufferedWriter.write(flatReprtConfiguration.getFileHeader());

            bufferedWriter.newLine();*/

        } catch (IOException e) {

            e.printStackTrace();

            logger.error(" error occurred while generating header for sales report with probable cause [{}] ", e.getMessage());

        }

        OutputStreamWriter finalReportOutputStreamWriter = reportOutputStreamWriter;

        if(null != aggregationResults ){


            aggregationResults.forEach(salesReportResponse -> {


                salesReportResponse.getKycs().forEach( kyc -> {

                    String kycNumber = kyc.getKycNumber();

                    if (KYC_TYPES.PAN.name().equalsIgnoreCase(kyc.getKycName())) {

                        salesReportResponse.setPan(kycNumber);

                    } else if (KYC_TYPES.AADHAR.name().equalsIgnoreCase(kyc.getKycName())) {

                        salesReportResponse.setAadhar(kycNumber);

                    } else if (KYC_TYPES.VOTER_ID.name().equalsIgnoreCase(kyc.getKycName())) {

                        salesReportResponse.setVoterId(kycNumber);

                    } else if (KYC_TYPES.DRIVING_LICENSE.name().equalsIgnoreCase(kyc.getKycName())) {

                        salesReportResponse.setDrivingLicense(kycNumber);

                    } else if (KYC_TYPES.PASSPORT.name().equalsIgnoreCase(kyc.getKycName())) {

                        salesReportResponse.setPassport(kycNumber);

                    }

                });

                List<String> strings = Arrays.asList(
                        salesReportResponse.getFirstName(),
                        salesReportResponse.getMiddleName(),
                        salesReportResponse.getLastName());

                String fullName =  GngUtils.stringToFirstMidlleLastName(strings);

                salesReportResponse.setFullName(fullName.trim());

                String row = CharMatcher.anyOf("\r\n\t").removeFrom(new SaleReportParser(salesReportResponse).build()
                        .enrichJson(flatReprtConfiguration).toString());


                try {

                    IOUtils.write(row.toString().getBytes(), finalReportOutputStreamWriter,StandardCharsets.UTF_8);

                } catch (IOException e) {
                    e.printStackTrace();
                    logger.error(" error occurred while writing report to outputStream with probable cause [{}] ", e.getMessage() );
                }

            });
        }else {
            logger.debug(" we are unable to fetch records for sales incentive report ");
        }


        reportOutputStreamWriter.close();


        return GnGFileUtils.compressBytes(
                "SALES_INCENTIVE_REPORT"+ GngDateUtil.getMmDdYyyyUnderSqure(new DateTime()) + ".csv",
                outputStream1.toByteArray());


    }

}
