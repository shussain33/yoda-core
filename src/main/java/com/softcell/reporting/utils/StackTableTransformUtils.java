package com.softcell.reporting.utils;

import com.softcell.gonogo.model.address.CustomerAddress;
import com.softcell.gonogo.model.analytics.StackTable;
import com.softcell.gonogo.model.core.*;
import com.softcell.gonogo.model.request.ApplicationRequest;
import com.softcell.gonogo.model.request.core.Header;
import com.softcell.gonogo.model.request.core.Request;
import com.softcell.gonogo.model.response.core.InterimStatus;
import com.softcell.gonogo.model.response.core.ModuleOutcome;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class StackTableTransformUtils {


    /**
     * Method to transform GoNoGoCustomerApplication to Stack Table
     *
     * @param goNoGoCustomerApplication
     * @return
     */
    public StackTable transformStackTable(
            GoNoGoCustomerApplication goNoGoCustomerApplication) {

        StackTable table = new StackTable();

        if (null != goNoGoCustomerApplication) {

            List<CroDecision> croDecisions = goNoGoCustomerApplication
                    .getCroDecisions();

            if (null != croDecisions && !croDecisions.isEmpty()) {
                table.setApprovedAmount(croDecisions.get(0)
                        .getAmtApproved());
            }

            String applicationStatus = goNoGoCustomerApplication
                    .getApplicationStatus();

            if (StringUtils.isNotBlank(applicationStatus)) {

                table.setApplicationStatus(applicationStatus);
            }

            List<CroJustification> croJustification = goNoGoCustomerApplication
                    .getCroJustification();

            table.setCroJustificationList(croJustification);

            table.setRemark(prepareRemark(croJustification));

            ApplicationRequest applicationRequest = goNoGoCustomerApplication
                    .getApplicationRequest();

            if (null != applicationRequest) {

                table.setApplicationId(applicationRequest.getRefID());

                Header header = applicationRequest.getHeader();

                table.setDate(prepareDate(header));

                table.setDsaId(prepareDsaId(header));

                table.setDealerId(prepareDealerId(header));

                Request request = applicationRequest.getRequest();

                table.setApplicantName(prepareNameSegment(request));

                Applicant applicant = request.getApplicant();

                table.setCustomerId(prepareCustomerId(applicant));

                table.setIncome(prepareIncome(applicant));

                table.setBranch(prepareApplicatCity(applicant));

                table.setCompanyName(prepareCompanyName(applicant));

                Application application = request.getApplication();

                table.setLoanAmount(null != application ? application
                        .getLoanAmount() : 0D);

                table.setStageID(applicationRequest.getCurrentStageId());

            }

            InterimStatus interimStatus = goNoGoCustomerApplication
                    .getIntrimStatus();

            table.setBureauScore(prepareBureaScore(interimStatus));

            table.setApplicationScore(prepareApplicationScore(interimStatus));

        }

        return table;
    }

    private String prepareCompanyName(Applicant applicant) {

        String companyName = "";
        if (null != applicant) {

            List<Employment> employment = applicant.getEmployment();

            if (null != employment && !employment.isEmpty()) {

                companyName = employment.get(0).getEmploymentName();
            }
        }

        return companyName;
    }

    private String prepareRemark(List<CroJustification> croJustification) {

        String remarks = "";

        if (null != croJustification && !croJustification.isEmpty()) {

            remarks = croJustification.get(0).getRemark();

        }
        return remarks;
    }

    private String prepareDealerId(Header header) {

        String dealerId = "";

        if (null != header) {

            dealerId = header.getDealerId();

        }
        return dealerId;
    }

    private String prepareDsaId(Header header) {

        String dsaId = "";

        if (null != header) {

            dsaId = header.getDsaId();

        }

        return dsaId;
    }

    private Date prepareDate(Header header) {

        Date date = null;
        if (null != header) {

            date = header.getDateTime();
        }
        return date;
    }

    private String prepareApplicationScore(InterimStatus interimStatus) {

        String applicationStatus = "";

        if (null != interimStatus) {

            String scoreStatus = interimStatus.getScoreStatus();

            if ("COMPLETE".equalsIgnoreCase(scoreStatus)) {

                ModuleOutcome scoringModuleResult = interimStatus
                        .getScoringModuleResult();

                if (null != scoringModuleResult) {

                    applicationStatus = scoringModuleResult.getFieldValue();
                }

            }

        }
        return applicationStatus;
    }

    private String prepareBureaScore(InterimStatus interimStatus) {

        String score = "";

        if (null != interimStatus) {

            String cibilScore = interimStatus.getCibilScore();

            if (StringUtils.equals("COMPLETE", cibilScore)) {

                ModuleOutcome cibilModuleResult = interimStatus
                        .getCibilModuleResult();

                if (null != cibilModuleResult) {

                    score = cibilModuleResult.getFieldValue();
                }

            }
        }

        return score;
    }

    private double prepareIncome(Applicant applicant) {
        double income = 0D;
        if (null != applicant) {

            List<Employment> employments = applicant.getEmployment();

            if (null != employments && !employments.isEmpty()) {

                Employment employment = employments.get(0);

                if (null != employment) {

                    if (StringUtils.equalsIgnoreCase("SELF-EMPLOYED",
                            employment.getEmploymentType())) {

                        double itrAmount = employment.getItrAmount();

                        if (itrAmount > 0)
                            income = itrAmount;
                        else
                            income = employment.getMonthlySalary();
                    } else {

                        income = employment.getMonthlySalary();

                    }

                }

            }

        }

        return income;
    }

    private String prepareApplicatCity(Applicant applicant) {
        String city = "";
        if (null != applicant) {

            List<CustomerAddress> address = applicant.getAddress();

            if (null != address && !address.isEmpty()) {

                city = address.get(0).getCity();
            }
        }
        return city;
    }

    private String prepareCustomerId(Applicant applicant) {
        String applicantId = "";
        if (null != applicant) {
            applicantId = applicant.getApplicantId();
        }
        return applicantId;
    }

    private String prepareNameSegment(Request request) {
        StringBuilder name = new StringBuilder();

        if (null != request) {

            Applicant applicant = request.getApplicant();

            if (null != applicant) {
                Name applicantName = applicant.getApplicantName();

                if (null != applicantName) {
                    name.append(null != applicantName.getPrefix() ? applicantName
                            .getPrefix() : "");
                    name.append(null != applicantName.getFirstName() ? applicantName
                            .getFirstName() : "");
                    name.append(" ");
                    name.append(null != applicantName.getMiddleName() ? applicantName
                            .getMiddleName() : "");
                    name.append(" ");
                    name.append(null != applicantName.getLastName() ? applicantName
                            .getLastName() : "");
                }
            }
        }

        return name.toString();
    }


}
