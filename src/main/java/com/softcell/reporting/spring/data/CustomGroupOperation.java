package com.softcell.reporting.spring.data;


import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.softcell.reporting.builder.GroupMongoId;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;
import org.springframework.util.Assert;

import java.util.Arrays;

public class CustomGroupOperation implements AggregationOperation {

    private BasicDBObject group;

    private GroupMongoId groupMongoId;

    public CustomGroupOperation(GroupMongoId groupMongoId) {
        this.groupMongoId = groupMongoId;
        group = new BasicDBObject();
        group.append(keyset._id.toString(), getGroups());
    }

    private BasicDBObject getGroups() {
        return groupMongoId;
    }

    public CustomGroupOperation includeFields(String... fields) {
        Assert.notNull(fields);
        Arrays.asList(fields).forEach(field -> group.put(field, 1));
        return this;
    }

    public final CustomGroupOperation first(String field, String as) {
        BasicDBObject first = new BasicDBObject();
        first.put("$first", "$" + field);
        group.put(as, first);
        return this;
    }

    public final CustomGroupOperation last(String field, String as) {
        BasicDBObject last = new BasicDBObject();
        last.put("$last", "$" + field);
        group.put(as, last);
        return this;
    }

    public final CustomGroupOperation count(String as) {
        BasicDBObject sum = new BasicDBObject();
        sum.put("$sum", 1);
        group.put(as, sum);
        return this;
    }

    public final CustomGroupOperation sum(String field, String as) {
        BasicDBObject sum = new BasicDBObject();
        sum.put("$sum", "$" + field);
        group.put(as, sum);
        return this;
    }

    public final CustomGroupOperation avg(String field, String as) {
        BasicDBObject avg = new BasicDBObject();
        avg.put("$avg", "$" + field);
        group.put(as, avg);
        return this;
    }

    public final CustomGroupOperation min(String field, String as) {
        BasicDBObject min = new BasicDBObject();
        min.put("$min", "$" + field);
        group.put(as, min);
        return this;
    }

    public final CustomGroupOperation addToSet(String field, String as) {
        BasicDBObject addToSet = new BasicDBObject();
        addToSet.put("$addToSet", "$" + field);
        group.put(as, addToSet);
        return this;
    }

    public final CustomGroupOperation push(String field, String as) {
        BasicDBObject push = new BasicDBObject();
        push.put("$push", "$" + field);
        group.put(as, push);
        return this;
    }

    @Override
    public DBObject toDBObject(AggregationOperationContext context) {
        return new BasicDBObject().append(keyset.$group.toString(), group);
    }

    private enum keyset {
        $group, _id
    }
}