package com.softcell.reporting.spring.data;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;

public class GoupDateFormatOperation implements AggregationOperation {
    private BasicDBObject dbObject;
    private BasicDBObject groups;

    public GoupDateFormatOperation(String... fields) {
        groups = new BasicDBObject();
        dbObject = new BasicDBObject();
        if (fields.length == 1) {
            for (String field : fields)
                dbObject.put("_id", "$" + field);
        } else {
            BasicDBObject groups = new BasicDBObject();
            for (String field : fields) {
                groups.put(field, "$" + field);
                dbObject.put("_id", groups);
            }
        }
    }

    @Override
    public DBObject toDBObject(AggregationOperationContext context) {
        groups.put("$group", this.dbObject);
        return this.groups;
    }

    public GoupDateFormatOperation dateFormate(String field, String format) {
        groups.put(field, this.dbObject.append(
                "$" + "dateToString",
                new BasicDBObject().append("$format", format).append("$field",
                        field)));
        return this;
    }
}
