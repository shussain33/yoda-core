package com.softcell.reporting.spring.data;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;

public class UnwindOperation implements AggregationOperation {
    private String field;
    private boolean preserveNullAndEmptyArrays;

    public UnwindOperation(String field, boolean preserveNullAndEmptyArrays) {
        this.field = field;
        this.preserveNullAndEmptyArrays = preserveNullAndEmptyArrays;
    }

    @Override
    public DBObject toDBObject(AggregationOperationContext context) {
        BasicDBObject path = new BasicDBObject();
        path.put("path", "$" + field);
        path.put("preserveNullAndEmptyArrays", preserveNullAndEmptyArrays);
        return new BasicDBObject("$unwind", path);
    }
}
