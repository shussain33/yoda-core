package com.softcell.reporting.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

/**
 * @author kishor To get report app for institution is and branch
 *         name. exposed api to client
 */
public class ReportRequest {

    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    @JsonProperty("sBranchId")
    private String branchName;

    @JsonProperty("sUserId")
    private String userId;

    @JsonProperty("sReportId")
    @NotEmpty(
            groups = {
                    ReportRequest.FetchGrp.class
            }
    )
    private String reportId;

    @JsonProperty("aProductType")
    private Set<String> productTypes;

    @JsonProperty("dtAccessDate")
    private Date accessDate;

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    /**
     * @return the header
     */
    public Header getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(Header header) {
        this.header = header;
    }

    /**
     * @return the accessDate
     */
    public Date getAccessDate() {
        return accessDate;
    }

    /**
     * @param accessDate the accessDate to set
     */
    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the productTypes
     */
    public Set<String> getProductTypes() {
        return productTypes;
    }

    /**
     * @param productTypes the productTypes to set
     */
    public void setProductTypes(Set<String> productTypes) {
        this.productTypes = productTypes;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ReportRequest [institutionId=");
        builder.append(", branchName=");
        builder.append(branchName);
        builder.append(", userId=");
        builder.append(userId);
        builder.append(", productTypes=");
        builder.append(productTypes);
        builder.append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((accessDate == null) ? 0 : accessDate.hashCode());
        result = prime * result
                + ((branchName == null) ? 0 : branchName.hashCode());
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result
                + ((productTypes == null) ? 0 : productTypes.hashCode());
        result = prime * result
                + ((reportId == null) ? 0 : reportId.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ReportRequest other = (ReportRequest) obj;
        if (accessDate == null) {
            if (other.accessDate != null)
                return false;
        } else if (!accessDate.equals(other.accessDate))
            return false;
        if (branchName == null) {
            if (other.branchName != null)
                return false;
        } else if (!branchName.equals(other.branchName))
            return false;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (productTypes == null) {
            if (other.productTypes != null)
                return false;
        } else if (!productTypes.equals(other.productTypes))
            return false;
        if (reportId == null) {
            if (other.reportId != null)
                return false;
        } else if (!reportId.equals(other.reportId))
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }

    public interface FetchGrp{}

}
