package com.softcell.reporting.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by mahesh on 22/2/17.
 */
public class LosUtrUpdateReportRequest {


    @JsonProperty("oHeader")
    @NotNull(
            groups = {
                    Header.FetchGrp.class
            }
    )
    @Valid
    private Header header;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }
}
