package com.softcell.reporting.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.softcell.gonogo.model.request.core.Header;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;


/**
 * Created by SuvarnaJ on 28/12/17.
 */
public class DailyDisbursalRequest {

    @JsonProperty("sCurrentStageId")
    private String currentStageId ;

    @NotNull
    @JsonProperty("sCurrentDate")
    private String CurrentDate;

    @JsonProperty("oHeader")
    @Valid
    private Header header;

    private Date startDate;

    private Date endDate;

    public String getCurrentDate() {
        return CurrentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.CurrentDate = currentDate;
    }

    public String getCurrentStageId() {
        return currentStageId;
    }

    public Header getHeader() {
        return header;
    }

    public void setCurrentStageId(String currentStageId) {
        this.currentStageId = currentStageId;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DailyDisbursalRequest that = (DailyDisbursalRequest) o;

        if (currentStageId != null ? !currentStageId.equals(that.currentStageId) : that.currentStageId != null)
            return false;
        if (CurrentDate != null ? !CurrentDate.equals(that.CurrentDate) : that.CurrentDate != null) return false;
        if (header != null ? !header.equals(that.header) : that.header != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        return endDate != null ? endDate.equals(that.endDate) : that.endDate == null;
    }

    @Override
    public int hashCode() {
        int result = currentStageId != null ? currentStageId.hashCode() : 0;
        result = 31 * result + (CurrentDate != null ? CurrentDate.hashCode() : 0);
        result = 31 * result + (header != null ? header.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DailyDisbursalRequest{" +
                "currentStageId='" + currentStageId + '\'' +
                ", CurrentDate='" + CurrentDate + '\'' +
                ", header=" + header +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
