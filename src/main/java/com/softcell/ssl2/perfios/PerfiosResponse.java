package com.softcell.ssl2.perfios;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PerfiosResponse {

    @JsonProperty("error")
    private String error;

    private String path;

    @JsonProperty("statusCode")
    private int statusCode;

    @JsonProperty("payload")
    private Object payload;

    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("acknowledgementId")
    private String ackId;

    @JsonProperty("status")
    private String status;


}