package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ssg228 on 8/4/19.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class ResultBlock {

    @XmlElement(name = "MatchSummary", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    private MatchSummary matchSummary;

    @XmlElement(name = "ErrorWarnings", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    private ErrorWarnings errorWarnings;
}
