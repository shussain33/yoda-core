package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.Data;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Data
@XmlRootElement(name = "MatchResult", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
@XmlAccessorType(XmlAccessType.FIELD)
public class HunterResponse {

    @XmlElement(name = "ResultBlock", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    public ResultBlock resultBlockObject;

}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Value {
    @XmlValue
    int value;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Values {
    @XmlElement(name = "Value", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    List<Value> value = new ArrayList<>();
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Number{
    @XmlValue
    int number;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Warning {

    @XmlElement(name = "Number", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Number number;

    @XmlElement(name = "Message", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Message message;

    @XmlElement(name = "Values", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Values values;

}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Message {

    @XmlValue
    String message;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Warnings {
    @XmlAttribute(name = "warningCount")
    int warningCount;

    @XmlElement(name = "Warning", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Warning warning;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class SchemeID{

    @XmlValue
    int schemeId;
}


@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Scheme {

    @XmlElement(name = "SchemeID", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    SchemeID schemeID;

    @XmlElement(name = "Score", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Score score;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class MatchSchemes {

    @XmlAttribute(name = "schemeCount")
    int schemeCount;

    @XmlElement(name = "Scheme", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Scheme scheme;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class Score{

    @XmlValue
    int score;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class TotalMatchScore{

    @XmlValue
    String totalScore;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class SubmisssionScores {

    @XmlAttribute(name = "scoreCount")
    int scoreCount;

    @XmlElement(name = "scoreType", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    ScoreType scoreType;
}

@Data
@XmlAccessorType(XmlAccessType.FIELD)
class ScoreType{

    @XmlValue
    int valueCount;

    @XmlValue
    String name;

    @XmlValue
    String score;
}
