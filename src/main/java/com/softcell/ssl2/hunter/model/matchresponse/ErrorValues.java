package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ssg228 on 8/4/19.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorValues {

    @XmlElement(name = "Value", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    private String Value;
}
