package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlValue;

/**
 * Created by ssg222 on 19/6/19.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleID{
    @XmlValue
    String ruleID;
}
