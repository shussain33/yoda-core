package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ssg222 on 19/6/19.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Rule {
    @XmlAttribute(name = "ruleCount")
    int ruleCount;

    @XmlAttribute(name = "isGlobal")
    String isGlobal;

    @XmlElement(name = "RuleID", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    RuleID ruleID;

    @XmlElement(name = "Score", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Score score;
}
