package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ssg228 on 8/4/19.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Errors {

    @XmlElement(name = "Error", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    Error error;

    @XmlAttribute(name = "errorCount")
    int errorCount;
}
