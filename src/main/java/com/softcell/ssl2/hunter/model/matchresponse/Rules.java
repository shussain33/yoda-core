package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssg222 on 19/6/19.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Rules {

    @XmlAttribute(name = "totalRuleCount")
    int totalRuleCount;

    @XmlElement(name = "Rule", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    List<Rule> rule = new ArrayList();

}