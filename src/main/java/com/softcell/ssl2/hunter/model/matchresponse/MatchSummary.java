package com.softcell.ssl2.hunter.model.matchresponse;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by ssg228 on 8/4/19.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchSummary {

    @XmlAttribute(name = "matches")
    private String matches;

    @XmlElement(name = "TotalMatchScore", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    private TotalMatchScore totalMatchScore;

    @XmlElement(name = "Rules", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    private Rules rules;

    @XmlElement(name = "MatchSchemes", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    private MatchSchemes matchSchemes;

    @XmlElement(name = "SubmisssionScores", namespace = "http://www.mclsoftware.co.uk/HunterII/WebServices")
    private SubmisssionScores submisssionScores;
}
