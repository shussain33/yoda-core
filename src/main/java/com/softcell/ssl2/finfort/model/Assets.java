package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Assets {

    @JsonProperty("dGrossFixedAssets")
    @Field("dGrossFixedAssets")
    private double grossFixedAssets;

    @JsonProperty("dDepreciation")
    @Field("dDepreciation")
    private double depreciation;

    @JsonProperty("dTangibleAssets")
    @Field("dTangibleAssets")
    private double tangibleAssets;

    @JsonProperty("dProducingProperties")
    @Field("dProducingProperties")
    private double producingProperties;

    @JsonProperty("dIntangibleAssets")
    @Field("dIntangibleAssets")
    private double intangibleAssets;

    @JsonProperty("dPreproducingProperties")
    @Field("dPreproducingProperties")
    private double preproducingProperties;

    @JsonProperty("dTangibleAssetsCapitalWorkInProgress")
    @Field("dTangibleAssetsCapitalWorkInProgress")
    private double tangibleAssetsCapitalWorkInProgress;

    @JsonProperty("dIntangibleAssetsUnderDevelopment")
    @Field("dIntangibleAssetsUnderDevelopment")
    private double intangibleAssetsUnderDevelopment;

    /*@JsonProperty("dNetFixedAssets")
    @Field("dNetFixedAssets")
    private double netFixedAssets;
*/
    @JsonProperty("dNonCurrentInvestments")
    @Field("dNonCurrentInvestments")
    private double nonCurrentInvestments;

    @JsonProperty("dCurrentInvestments")
    @Field("dCurrentInvestments")
    private double currentInvestments;

    @JsonProperty("dInvestments")
    @Field("dInvestments")
    private double investments;

    @JsonProperty("dForeignCurrMonetaryItemTransDiffAssetAccount")
    @Field("dForeignCurrMonetaryItemTransDiffAssetAccount")
    private double foreignCurrMonetaryItemTransDiffAssetAccount;

    @JsonProperty("dOtherNonCurrentAssets")
    @Field("dOtherNonCurrentAssets")
    private double otherNonCurrentAssets;

    @JsonProperty("dOtherCurrentAssets")
    @Field("dOtherCurrentAssets")
    private double otherCurrentAssets;

    @JsonProperty("dCurrentAssets")
    @Field("dCurrentAssets")
    private double currentAssets;

    @JsonProperty("dInventories")
    @Field("dInventories")
    private double inventories;

    @JsonProperty("dTradeReceivablesDebtors")
    @Field("dTradeReceivablesDebtors")
    private double tradeReceivablesDebtors;

    @JsonProperty("dExceedingSixMonths")
    @Field("dExceedingSixMonths")
    private double exceedingSixMonths;

    @JsonProperty("dLessThanSixMonthsLater")
    @Field("dLessThanSixMonthsLater")
    private double lessThanSixMonthsLater;

    @JsonProperty("dLongTermLoansAndAdvances")
    @Field("dLongTermLoansAndAdvances")
    private double longTermLoansAndAdvances;

    @JsonProperty("dShortTermLoansAndAdvances")
    @Field("dShortTermLoansAndAdvances")
    private double shortTermLoansAndAdvances;

    @JsonProperty("dLoansAndAdvances")
    @Field("dLoansAndAdvances")
    private double loansAndAdvances;

    @JsonProperty("dOtherGenuineAdvances")
    @Field("dOtherGenuineAdvances")
    private double otherGenuineAdvances;

    @JsonProperty("dCashAndBankBalances")
    @Field("dCashAndBankBalances")
    private double cashAndBankBalances;

    @JsonProperty("dDeferredTaxAssetsNet")
    @Field("dDeferredTaxAssetsNet")
    private double deferredTaxAssetsNet;

    @JsonProperty("dTotalCurrentAssets")
    @Field("dTotalCurrentAssets")
    private double totalCurrentAssets;

    @JsonProperty("dGivenAssetsTotal")
    @Field("dGivenAssetsTotal")
    private double givenAssetsTotal;

    @JsonProperty("dProvisions")
    @Field("dProvisions")
    private double provisions;

    @JsonProperty("dResidualCurrentAssets")
    @Field("dResidualCurrentAssets")
    private double residualCurrentAssets;


    @JsonProperty("dAnyWorkingCapitalLimtsWhichIsNotPartOfSecuredLoans")
    @Field("dAnyWorkingCapitalLimtsWhichIsNotPartOfSecuredLoans")
    private double anyWorkingCapitalLimtsWhichIsNotPartOfSecuredLoans;

    @JsonProperty("dNetWorkingCapital")
    @Field("dNetWorkingCapital")
    private double netWorkingCapital;

    @JsonProperty("dTotalApplicationOfFunds")
    @Field("dTotalApplicationOfFunds")
    private double totalApplicationOfFunds;

}
