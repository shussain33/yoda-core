package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Bs {

    @JsonProperty("oAssets")
    @Field("oAssets")
    private Assets assets;

    @JsonProperty("oLiabilities")
    @Field("oLiabilities")
    private Liabilities liabilities;

    @JsonProperty("oSubTotals")
    @Field("oSubTotals")
    private SubTotals subTotals;

}
