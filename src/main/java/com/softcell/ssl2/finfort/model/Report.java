package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Report {

    @JsonProperty("oDetailedFinancials")
    @Field("oDetailedFinancials")
    private List<DetailedFinancials> detailedFinancials;

    @JsonProperty("sFFOrderNo")
    @Field("sFFOrderNo")
    private String ffOrderNo;

   /* @Id
    protected String _id;*/

    /*@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String path;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String requestTimestamp;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String responseTimestamp;*/

}
