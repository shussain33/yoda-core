package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Itr {

    @JsonProperty(value = "sApplicantName")
    @Field("sApplicantName")
    private String applicantName;

    @JsonProperty(value = "sAssessmentYear")
    @Field("sAssessmentYear")
    private String assessmentYear;

    @JsonProperty(value = "sAckNo")
    @Field("sAckNo")
    private String ackNo;

    @JsonProperty(value = "sITRFilingYear")
    @Field("sITRFilingYear")
    private String itrFilingYear;

    @JsonProperty(value = "sLocation")
    @Field("sLocation")
    private String location;

    @JsonProperty(value = "sPANNumber")
    @Field("sPANNumber")
    private String pan;

    @JsonProperty(value = "sAgency")
    @Field("sAgency")
    private String agency;

    @JsonProperty(value = "sTaxPaid")
    @Field("sTaxPaid")
    private String taxPaid;

    @JsonProperty(value = "dTotalIncomeGTI")
    @Field("dTotalIncomeGTI")
    private double totalIncomeGTI;

    @JsonProperty(value = "sStatus")
    @Field("sStatus")
    private String Status;

    @JsonProperty(value = "sFinalStatus")
    @Field("sFinalStatus")
    private String finalStatus;

    @JsonProperty(value = "dTotalIncomeGTIAmount")
    @Field("dTotalIncomeGTIAmount")
    private double totalincomeGTIAmount;
}
