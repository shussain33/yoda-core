package com.softcell.ssl2.finfort.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by archana on 16/11/18.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TriggerRequest {
    @JsonIgnore
    private String applicantId;

    private IndividualFinfortRequest individualFinfortRequest;

    private CorporateFinfortRequest corporateFinfortRequest;
}
