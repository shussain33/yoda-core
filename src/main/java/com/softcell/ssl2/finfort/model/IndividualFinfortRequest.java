package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.*;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IndividualFinfortRequest {

    @NotNull
    @NotEmpty(message = "Account should not be null. ")
    @JsonProperty(value = "sAccount")
    @SerializedName("Account")
    @Field(value = "sAccount")
    private String account;

    @NotNull@NotEmpty(message = "LenderBranch should not be null. ")
    @JsonProperty(value = "sLenderBranch")
    @Field("sLenderBranch")
    @SerializedName("LenderBranch")
    private String lenderBranch;

    @NotNull@NotEmpty(message = "LenderDivision should not be null")
    @JsonProperty(value = "sLenderDivision")
    @Field("sLenderDivision")
    @SerializedName("LenderDivision")
    private String lenderDivision;

    @NotNull@NotEmpty(message = "ProductOrdered should not be null")
    @Pattern(regexp = "Salaried Information|Salaried Credit Evaluation|Self Employed Information|Self Employed Credit Evaluation")
    @JsonProperty(value = "sProductOrdered")
    @SerializedName("ProductOrdered")
    @Field("sProductOrdered")
    private String productOrdered;

    @NotNull@NotEmpty(message = "DataPullOptionChosen should not be null")
    @Pattern(regexp = "CSR Assisted|Lender Assisted|Self Service")
    @JsonProperty(value = "sDataPullOptionChosen")
    @SerializedName("DataPullOptionChosen")
    @Field("sDataPullOptionChosen")
    private String dataPullOptionChosen;

    @NotNull@NotEmpty(message = "LenderReferenceNumber should not be null")
    @JsonProperty(value = "sLenderReferenceNumber")
    @SerializedName("LenderReferenceNumber")
    @Field("sLenderReferenceNumber")
    private String lenderReferenceNumber;

    @NotNull@NotEmpty(message = "BorrowerFirstName should not be null")
    @JsonProperty(value = "sBorrowerFirstName")
    @SerializedName("BorrowerFirstName")
    @Field("sBorrowerFirstName")
    private String borrowerFirstName;

    @JsonProperty(value = "sBorrowerMiddleName")
    @SerializedName("BorrowerMiddleName")
    @Field("sBorrowerMiddleName")
    private String borrowerMiddleName;

    @NotNull@NotEmpty(message = "BorrowerLastName should not be null")
    @JsonProperty(value = "sBorrowerLastName")
    @SerializedName("BorrowerLastName")
    @Field("sBorrowerLastName")
    private String borrowerLastName;

    @JsonProperty(value = "sPartnershipOrCompanyName")
    @SerializedName("PartnershipOrCompanyName")
    @Field("sPartnershipOrCompanyName")
    private String partnershipOrCompanyName;

    @NotNull@NotEmpty(message = "DateOfBirth should not be null")
    @JsonProperty(value = "sDateOfBirth")
    @SerializedName("DateOfBirth")
    @Field("sDateOfBirth")
    private String dateOfBirth;

    @NotNull@NotEmpty(message = "BorrowerPAN should not be null")
    @Size(min = 10,max = 10)
    @JsonProperty(value = "sBorrowerPAN")
    @SerializedName("BorrowerPAN")
    @Field("sBorrowerPAN")
    private String borrowerPAN;

    @Email
    @NotEmpty
    @JsonProperty(value = "sBorrowerEmail")
    @SerializedName("BorrowerEmail")
    @Field("sBorrowerEmail")
    private String borrowerEmail;

    @NotNull@NotEmpty(message = "BorrowerMobile should not be null")
    @Size(min = 10,max = 10)
    @JsonProperty(value = "sBorrowerMobile")
    @SerializedName("BorrowerMobile")
    @Field("sBorrowerMobile")
    private String borrowerMobile;

    @Email
    @JsonProperty(value = "sBorrowerAdditionalEmail")
    @SerializedName("BorrowerAdditionalEmail")
    @Field("sBorrowerAdditionalEmail")
    private String borrowerAdditionalEmail;

    @Size(min = 10,max = 10)
    @JsonProperty(value = "sBorrowerAdditionalMobile")
    @SerializedName("BorrowerAdditionalMobile")
    @Field("sBorrowerAdditionalMobile")
    private String borrowerAdditionalMobile;

    @JsonProperty(value = "sBorrowerlanguage")
    @SerializedName("Borrowerlanguage")
    @Field("sBorrowerlanguage")
    private String borrowerlanguage;

    @JsonProperty(value = "sDINNumber")
    @SerializedName("DinNumber")
    @Field("sDINNumber")
    private String dinNumber;

    @JsonProperty(value = "sCIN_LLPIN")
    @SerializedName("CIN_LLPIN")
    @Field("sCIN_LLPIN")
    private String cin_LLPIN;

    @JsonProperty(value = "sBorrowerStreet")
    @SerializedName("BorrowerStreet")
    @Field("sBorrowerStreet")
    private String borrowerStreet;

    @JsonProperty(value = "sCity")
    @SerializedName("City")
    @Field("sCity")
    private String city;

    @JsonProperty(value = "sBorrowerState")
    @SerializedName("BorrowerState")
    @Field("sBorrowerState")
    private String borrowerState;

    @JsonProperty(value = "sCountry")
    @SerializedName("Country")
    @Field("sCountry")
    private String country;

    @Size(min = 06,max = 06)
    @JsonProperty(value = "sBorrowerPinCode")
    @SerializedName("BorrowerPinCode")
    @Field("sBorrowerPinCode")
    private String borrowerPinCode;

    @JsonProperty(value = "sAdditionalInfo")
    @SerializedName("AdditionalInfo")
    @Field("sAdditionalInfo")
    private String additionalInfo;

    @JsonProperty(value = "sChannelPartnerName")
    @SerializedName("channelPartnerName")
    @Field("sChannelPartnerName")
    private String channelPartnerName;

    @Email
    @JsonProperty(value = "sChannelPartnerEmailId")
    @SerializedName("channelPartnerEmailId")
    @Field("sChannelPartnerEmailId")
    private String channelPartnerEmailId;

    @Size(min = 10,max = 10)
    @JsonProperty(value = "sChannelPartnerMobile")
    @SerializedName("channelPartnerMobile")
    @Field("sChannelPartnerMobile")
    private String channelPartnerMobile;

    @JsonProperty(value = "sSalesExecutiveName")
    @SerializedName("salesExecutiveName")
    @Field("sSalesExecutiveName")
    private String salesExecutiveName;

    @Email
    @JsonProperty(value = "sSalesExecutiveEmailId")
    @SerializedName("salesExecutiveEmailId")
    @Field("sSalesExecutiveEmailId")
    private String salesExecutiveEmailId;

    @Size(min = 10,max = 10)
    @JsonProperty(value = "sSalesExecutiveMobile")
    @SerializedName("salesExecutiveMobile")
    @Field("sSalesExecutiveMobile")
    private String salesExecutiveMobile;

    @JsonProperty(value = "sDirectorDIN1")
    @SerializedName("DirectorDIN1")
    @Field("sDirectorDIN1")
    private String directorDIN1;

    @JsonProperty(value = "sDirectorDIN2")
    @SerializedName("DirectorDIN2")
    @Field("sDirectorDIN2")
    private String directorDIN2;

    @JsonProperty(value = "sDirectorDIN3")
    @SerializedName("DirectorDIN3")
    @Field("sDirectorDIN3")
    private String directorDIN3;

    @JsonProperty(value = "sDirectorDIN4")
    @SerializedName("DirectorDIN4")
    @Field("sDirectorDIN4")
    private String directorDIN4;

    @JsonProperty(value = "sDirectorName1")
    @SerializedName("DirectorName1")
    @Field("sDirectorName1")
    private String directorName1;

    @JsonProperty(value = "sDirectorName2")
    @SerializedName("DirectorName2")
    @Field("sDirectorName2")
    private String directorName2;

    @JsonProperty(value = "sDirectorName3")
    @SerializedName("DirectorName3")
    @Field("sDirectorName3")
    private String directorName3;

    @JsonProperty(value = "sDirectorName4")
    @SerializedName("DirectorName4")
    @Field("sDirectorName4")
    private String directorName4;

}
