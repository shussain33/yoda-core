package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FileUploadData {

    @JsonProperty("sExtension")
    @Field("sExtension")
    private String extension;

    @JsonProperty("sType")
    @Field("sType")
    private String type;

    @JsonProperty("sName")
    @Field("sName")
    private String name;

    @JsonProperty("sPath")
    @Field("sPath")
    private String path;


}
