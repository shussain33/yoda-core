package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SubTotal {

    @JsonProperty("dTotalOperatingCost")
    @Field("dTotalOperatingCost")
    private double totalOperatingCost;
}
