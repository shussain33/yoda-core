package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Liabilities {


    @JsonProperty("dShareCapital")
    @Field("dShareCapital")
    private double shareCapital;


    @JsonProperty("dMoneyReceivedAgainstShareWarrants")
    @Field("dMoneyReceivedAgainstShareWarrants")
    private double moneyReceivedAgainstShareWarrants;

    @JsonProperty("dShareApplicationMoneyPendingAllotment")
    @Field("dShareApplicationMoneyPendingAllotment")
    private double shareApplicationMoneyPendingAllotment;

    @JsonProperty("dReservesAndSurplus")
    @Field("dReservesAndSurplus")
    private double reservesAndSurplus;

    @JsonProperty("dAdvancesToGroupCofriends")
    @Field("dAdvancesToGroupCofriends")
    private double advancesToGroupCofriends;

    @JsonProperty("dFromFamilyAndFriendsNonInterestBearing")
    @Field("dFromFamilyAndFriendsNonInterestBearing")
    private double fromFamilyAndFriendsNonInterestBearing;

    @JsonProperty("dMiscExpNotWrittenOff")
    @Field("dMiscExpNotWrittenOff")
    private double dMiscExpNotWrittenOff;

    @JsonProperty("dSecuredDebtTermLoans")
    @Field("dSecuredDebtTermLoans")
    private double securedDebtTermLoans;

    @JsonProperty("dCashCredit")
    @Field("dCashCredit")
    private double cashCredit;

    @JsonProperty("dUnsecuredDebtPersonalLoans")
    @Field("dUnsecuredDebtPersonalLoans")
    private double dUnsecuredDebtPersonalLoans;

    @JsonProperty("dUnsecuredDebtOthersIncludingInttBearingLoansFromFamilyFriends")
    @Field("dUnsecuredDebtOthersIncludingInttBearingLoansFromFamilyFriends")
    private double unsecuredDebtOthersIncludingInttBearingLoansFromFamilyFriends;

    @JsonProperty("dLongTermBorrowings")
    @Field("dLongTermBorrowings")
    private double longTermBorrowings;

    @JsonProperty("dShortTermBorrowings")
    @Field("dShortTermBorrowings")
    private double shortTermBorrowings;

    @JsonProperty("dDeferredGovernmentGrants")
    @Field("dDeferredGovernmentGrants")
    private double deferredGovernmentGrants;

    @JsonProperty("dMinorityInterest")
    @Field("dMinorityInterest")
    private double minorityInterest;

    @JsonProperty("dDeferredTaxLiabilitiesNet")
    @Field("dDeferredTaxLiabilitiesNet")
    private double deferredTaxLiabilitiesNet;

    @JsonProperty("dForeignCurrMonetaryItemTransDiffLiabilityAccount")
    @Field("dForeignCurrMonetaryItemTransDiffLiabilityAccount")
    private double foreignCurrMonetaryItemTransDiffLiabilityAccount;

    @JsonProperty("dOtherLongTermLiabilities")
    @Field("dOtherLongTermLiabilities")
    private double otherLongTermLiabilities;

    @JsonProperty("dLongTermProvisions")
    @Field("dLongTermProvisions")
    private double longTermProvisions;

    @JsonProperty("dTradePayables")
    @Field("dTradePayables")
    private double tradePayables;

    @JsonProperty("dOtherCurrentLiabilities")
    @Field("dOtherCurrentLiabilities")
    private double otherCurrentLiabilities;

    @JsonProperty("dShortTermProvisions")
    @Field("dShortTermProvisions")
    private double shortTermProvisions;

    @JsonProperty("dGivenLiabilitiesTotal")
    @Field("dGivenLiabilitiesTotal")
    private double givenLiabilitiesTotal;

}
