package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SubTotals {

    @JsonProperty("dTotalFundInTheBusiness")
    @Field("dTotalFundInTheBusiness")
    private double totalFundInTheBusiness;

    @JsonProperty("dTotalEquity")
    @Field("dTotalEquity")
    private double totalEquity;

    @JsonProperty("dTotalCurrentLiabilities")
    @Field("dTotalCurrentLiabilities")
    private double totalCurrentLiabilities;

    @JsonProperty("dNetFixedAssets")
    @Field("dNetFixedAssets")
    private double netFixedAssets;

    @JsonProperty("dTotalCurrentAssets")
    @Field("dTotalCurrentAssets")
    private double totalCurrentAssets;

    @JsonProperty("dCapitalWip")
    @Field("dCapitalWip")
    private double capitalWip;

    @JsonProperty("dTotalDebt")
    @Field("dTotalDebt")
    private double totalDebt;

}
