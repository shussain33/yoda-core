package com.softcell.ssl2.finfort.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {

    private String error;

    private String path;

    @JsonProperty("statusCode")
    private int status;

    @JsonProperty("payload")
    private Object payload;

    private String timestamp;

    @JsonProperty("acknowledgementId")
    private String ackId;

    @JsonProperty("vendorTransactionId")
    private String vendorTransactionId;

}