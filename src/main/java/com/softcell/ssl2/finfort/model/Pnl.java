package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Pnl {

    @JsonProperty("oLineItems")
    @Field("oLineItems")
    private LineItems lineItems;

    @JsonProperty("oSubTotals")
    @Field("oSubTotals")
    private SubTotal subTotals;

}
