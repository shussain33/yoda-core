package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinfortStatusRequest {

   @JsonProperty("sMessage")
   private String message;

   @JsonProperty("sAcknowledgementId")
   private String ackId;
}