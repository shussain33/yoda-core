package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DetailedFinancials {

    @JsonProperty("sYear")
    @Field("sYear")
    private String year;

    @JsonProperty("sNature")
    @Field("sNature")
    private String nature;

    @JsonProperty("sStatedOn")
    @Field("sStatedOn")
    private String statedOn;

    @JsonProperty("sFilingStandard")
    @Field("sFilingStandard")
    private String filingStandard;

    @JsonProperty("oBs")
    @Field("oBs")
    private Bs bs;

    @JsonProperty("oPnl")
    @Field("oPnl")
    private Pnl pnl;

    @JsonProperty("oItr")
    @Field("oItr")
    private Itr itr;
}
