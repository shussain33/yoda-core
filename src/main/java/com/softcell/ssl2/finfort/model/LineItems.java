package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LineItems {

    @JsonProperty("dNetRevenue")
    @Field("dNetRevenue")
    private double netRevenue;

    @JsonProperty("dTotalCostOfMaterialsConsumed")
    @Field("dTotalCostOfMaterialsConsumed")
    private double totalCostOfMaterialsConsumed;

    @JsonProperty("dTotalPurchasesOfStockInTrade")
    @Field("dTotalPurchasesOfStockInTrade")
    private double totalPurchasesOfStockInTrade;

    @JsonProperty("dTotalChangesInInventoriesOrFinishedGoods")
    @Field("dTotalChangesInInventoriesOrFinishedGoods")
    private double totalChangesInInventoriesOrFinishedGoods;

    @JsonProperty("dGrossProfit")
    @Field("dGrossProfit")
    private double grossProfit;

    @JsonProperty("dAdminExpense")
    @Field("dAdminExpense")
    private double adminExpense;

    @JsonProperty("dSellingMktgExpenses")
    @Field("dSellingMktgExpenses")
    private double sellingMktgExpenses;

    @JsonProperty("dTotalEmployeeBenefitExpense")
    @Field("dTotalEmployeeBenefitExpense")
    private double totalEmployeeBenefitExpense;

    @JsonProperty("dTotalOtherExpenses")
    @Field("dTotalOtherExpenses")
    private double totalOtherExpenses;

    @JsonProperty("dTotalOperatingExpense")
    @Field("dTotalOperatingExpense")
    private double totalOperatingExpense;

    @JsonProperty("dBackPartnerSalary")
    @Field("dBackPartnerSalary")
    private double backPartnerSalary;

    @JsonProperty("dBackDirectorSalary")
    @Field("dBackDirectorSalary")
    private double backDirectorSalary;

    @JsonProperty("dPartnerInterest")
    @Field("dPartnerInterest")
    private double partnerInterest;

    @JsonProperty("dOperatingProfit")
    @Field("dOperatingProfit")
    private double operatingProfit;

    @JsonProperty("dOtherIncome")
    @Field("dOtherIncome")
    private double otherIncome;

    @JsonProperty("dProfitBeforeDepreciationInterestTax")
    @Field("dProfitBeforeDepreciationInterestTax")
    private double profitBeforeDepreciationInterestTax;

    @JsonProperty("dPbditExcludingPartnerSalaryInterest")
    @Field("dPbditExcludingPartnerSalaryInterest")
    private double pbditExcludingPartnerSalaryInterest;

    @JsonProperty("dInterestToFiAndBank")
    @Field("dInterestToFiAndBank")
    private double interestToFiAndBank;

    @JsonProperty("dInterestToPrivateParties")
    @Field("dInterestToPrivateParties")
    private double interestToPrivateParties;

    @JsonProperty("dInterest")
    @Field("dInterest")
    private double interest;

    @JsonProperty("dProfitBeforeDepreciationAndTaxPbdt")
    @Field("dProfitBeforeDepreciationAndTaxPbdt")
    private double profitBeforeDepreciationAndTaxPbdt;

    @JsonProperty("dDepreciation")
    @Field("dDepreciation")
    private double depreciation;

    @JsonProperty("dProfitBeforeInterestAndTax")
    @Field("dProfitBeforeInterestAndTax")
    private double profitBeforeInterestAndTax;


    @JsonProperty("dProfitBeforeTaxAndExceptionalItemsBeforeTax")
    @Field("dProfitBeforeTaxAndExceptionalItemsBeforeTax")
    private double profitBeforeTaxAndExceptionalItemsBeforeTax;

    @JsonProperty("dExceptionalItemsBeforeTax")
    @Field("dExceptionalItemsBeforeTax")
    private double exceptionalItemsBeforeTax;

    @JsonProperty("dProfitBeforeTax")
    @Field("dProfitBeforeTax")
    private double profitBeforeTax;

    @JsonProperty("dIncomeTax")
    @Field("dIncomeTax")
    private double incomeTax;

    @JsonProperty("dProfitForPeriodFromContinuingOperations")
    @Field("dProfitForPeriodFromContinuingOperations")
    private double profitForPeriodFromContinuingOperations;

    @JsonProperty("dProfitFromDiscontinuingOperationAfterTax")
    @Field("dProfitFromDiscontinuingOperationAfterTax")
    private double profitFromDiscontinuingOperationAfterTax;

    @JsonProperty("dMinorityInterestAndProfitFromAssociatesAndJointVentures")
    @Field("dMinorityInterestAndProfitFromAssociatesAndJointVentures")
    private double minorityInterestAndProfitFromAssociatesAndJointVentures;

    @JsonProperty("dProfitAfterTaxPAT")
    @Field("dProfitAfterTaxPAT")
    private double profitAfterTaxPAT;

    @JsonProperty("dPostTaxCashProfitsSalPartnerInterest")
    @Field("dPostTaxCashProfitsSalPartnerInterest")
    private double postTaxCashProfitsSalPartnerInterest;

    @JsonProperty("dNetProfitRatio")
    @Field("dNetProfitRatio")
    private double netProfitRatio;

}
