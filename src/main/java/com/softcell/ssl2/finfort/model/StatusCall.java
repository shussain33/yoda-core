package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by archana on 16/11/18.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
// This call will be replaced by the pojo from ssl team
public class StatusCall {

    @JsonProperty("acknowledgementId")
    private String ackId;

    @JsonProperty("message")
    private String message;
}
