package com.softcell.ssl2.finfort.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FileUploadRequest {

    @JsonProperty("aFilesUploaded")
    @Field("sFileUploadData")
    private List<FileUploadData> fileUploadData;

    @JsonProperty("sAcknowledgementId")
    @Field("sAcknowledgementId")
    private String ackId;

}
