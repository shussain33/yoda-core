package com.softcell.rest.exhandler;

import java.util.Map;

/**
 * Created by prateek on 23/1/17.
 */
public final class MapUtils {

    private MapUtils() {
    }

    /**
     * Puts entries from the {@code source} map into the {@code target} map, but without overriding
     * any existing entry in {@code target} map, i.e. put only if the key does not exist in the
     * {@code target} map.
     *
     * @param target The target map where to put new entries.
     * @param source The source map from which read the entries.
     */
    static <K, V> void putAllIfAbsent(Map<K, V> target, Map<K, V> source) {

        for (Map.Entry<K, V> entry : source.entrySet()) {
            if (!target.containsKey(entry.getKey())) {
                target.put(entry.getKey(), entry.getValue());
            }
        }
    }
}
