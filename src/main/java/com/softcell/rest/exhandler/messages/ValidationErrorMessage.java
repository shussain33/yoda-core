package com.softcell.rest.exhandler.messages;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prateek on 23/1/17.
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlRootElement(name = "problem") //for JAXB
public class ValidationErrorMessage extends ErrorMessage {

    private static final long serialVersionUID = 1L;

    private List<Error> errors = new ArrayList<>(6);

    public ValidationErrorMessage(ErrorMessage errorMessage) {
        super(errorMessage);
    }

    public ValidationErrorMessage addError(String field, Object rejectedValue, String message) {
        errors.add(new Error(field, rejectedValue, message));
        return this;
    }

    public ValidationErrorMessage addError(String message) {
        errors.add(new Error(null, null, message));
        return this;
    }


    @Data
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Error {

        private final String field;
        private final Object rejected;
        private final String message;


        public Error(String field, Object rejected, String message) {
            this.field = field;
            this.rejected = rejected;
            this.message = message;
        }


    }
}
