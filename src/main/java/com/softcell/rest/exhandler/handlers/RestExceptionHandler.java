package com.softcell.rest.exhandler.handlers;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Contract for class generating {@link org.springframework.http.ResponseEntity} for an instance of the specified
 * Exception Type , used in {@link com.softcell.rest.exhandler.RestHandlerExceptionResolver restHandlerExceptionResolver}
 *
 * @param <E> Type of the handled exception.
 * @param <T> Type of the response message (entity body).
 *            Created by prateek on 23/1/17.
 */

public interface RestExceptionHandler<E extends Exception, T> {

    /**
     * Handles exception and generates {@link ResponseEntity}.
     *
     * @param exception The exception to handle and get data from.
     * @param request   The current request.
     * @return A response entity.
     */

    ResponseEntity<T> handleException(E exception, HttpServletRequest request);

}
