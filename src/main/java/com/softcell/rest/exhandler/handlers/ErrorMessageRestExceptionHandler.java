package com.softcell.rest.exhandler.handlers;

import com.softcell.rest.exhandler.interpolator.MessageInterpolator;
import com.softcell.rest.exhandler.interpolator.MessageInterpolatorAware;
import com.softcell.rest.exhandler.interpolator.NoOpMessageInterpolator;
import com.softcell.rest.exhandler.interpolator.SpelMessageInterpolator;
import com.softcell.rest.exhandler.messages.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * {@link RestExceptionHandler} that produces {@link ErrorMessage}.
 *
 * @param <E> Type of the handled exception.
 *            <p/>
 *            Created by prateek on 23/1/17.
 */
public class ErrorMessageRestExceptionHandler<E extends Exception>
        extends AbstractRestExceptionHandler<E, ErrorMessage> implements MessageSourceAware, MessageInterpolatorAware {

    protected static final String
            DEFAULT_PREFIX = "default",
            TYPE_KEY = "type",
            TITLE_KEY = "title",
            DETAIL_KEY = "detail",
            INSTANCE_KEY = "instance";
    private static final Logger logger = LoggerFactory.getLogger(ErrorMessageRestExceptionHandler.class);
    private MessageSource messageSource;

    private MessageInterpolator interpolator = new SpelMessageInterpolator();


    /**
     * @param exceptionClass Type of the handled exceptions; it's used as a prefix of key to
     *                       resolve messages (via MessageSource).
     * @param status         HTTP status that will be sent to client.
     */
    public ErrorMessageRestExceptionHandler(Class<E> exceptionClass, HttpStatus status) {
        super(exceptionClass, status);
    }

    /**
     * @see AbstractRestExceptionHandler#AbstractRestExceptionHandler(HttpStatus) AbstractRestExceptionHandler
     */
    protected ErrorMessageRestExceptionHandler(HttpStatus status) {
        super(status);
    }


    public ErrorMessage createBody(E ex, HttpServletRequest req) {

        ErrorMessage m = new ErrorMessage();
        m.setType(URI.create(resolveMessage(TYPE_KEY, ex, req)));
        m.setTitle(resolveMessage(TITLE_KEY, ex, req));
        m.setStatus(getStatus());
        m.setDetail(resolveMessage(DETAIL_KEY, ex, req));
        m.setInstance(URI.create(resolveMessage(INSTANCE_KEY, ex, req)));

        return m;
    }


    protected String resolveMessage(String key, E exception, HttpServletRequest request) {

        String template = getMessage(key, LocaleContextHolder.getLocale());

        Map<String, Object> vars = new HashMap<>(2);
        vars.put("ex", exception);
        vars.put("req", request);

        return interpolateMessage(template, vars);
    }

    protected String interpolateMessage(String messageTemplate, Map<String, Object> variables) {

        logger.trace("Interpolating message '{}' with variables: {}", messageTemplate, variables);
        return interpolator.interpolate(messageTemplate, variables);
    }

    protected String getMessage(String key, Locale locale) {

        String prefix = getExceptionClass().getName();

        String message = messageSource.getMessage(prefix + "." + key, null, null, locale);
        if (message == null) {
            message = messageSource.getMessage(DEFAULT_PREFIX + "." + key, null, null, locale);
        }
        if (message == null) {
            message = "";
            logger.debug("No message found for {}.{}, nor {}.{}", prefix, key, DEFAULT_PREFIX, key);
        }
        return message;
    }


    /**
     * Accessors
     **/

    public void setMessageSource(MessageSource messageSource) {
        Assert.notNull(messageSource, "messageSource must not be null");
        this.messageSource = messageSource;
    }

    public void setMessageInterpolator(MessageInterpolator interpolator) {
        this.interpolator = interpolator != null ? interpolator : new NoOpMessageInterpolator();
    }
}
