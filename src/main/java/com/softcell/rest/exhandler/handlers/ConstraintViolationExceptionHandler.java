package com.softcell.rest.exhandler.handlers;

import com.softcell.rest.exhandler.messages.ErrorMessage;
import com.softcell.rest.exhandler.messages.ValidationErrorMessage;
import org.apache.commons.beanutils.ConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ElementKind;
import javax.validation.Path;
import javax.validation.Path.Node;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by prateek on 23/1/17.
 */
public class ConstraintViolationExceptionHandler extends ErrorMessageRestExceptionHandler<ConstraintViolationException> {

    private ConversionService conversionService = new DefaultConversionService();

    public ConstraintViolationExceptionHandler() {
        super(UNPROCESSABLE_ENTITY);
    }


    @Override
    public ErrorMessage createBody(ConstraintViolationException ex, HttpServletRequest req) {
        ErrorMessage tmpl = super.createBody(ex, req);
        ValidationErrorMessage msg = new ValidationErrorMessage(tmpl);

        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            Node pathNode = findLastNonEmptyPathNode(violation.getPropertyPath());

            // path is probably useful only for properties (fields)
            if (pathNode != null && pathNode.getKind() == ElementKind.PROPERTY) {
                msg.addError(pathNode.getName(), convertToString(violation.getInvalidValue()), violation.getMessage());

                // type level constraints etc.
            } else {
                msg.addError(violation.getMessage());
            }
        }
        return msg;
    }

    /**
     * Conversion service used for converting an invalid value to String.
     * When no service provided, the {@link DefaultConversionService} is used.
     *
     * @param conversionService must not be null.
     */
    @Autowired(required = false)
    public void setConversionService(ConversionService conversionService) {
        Assert.notNull(conversionService, "conversionService must not be null");
        this.conversionService = conversionService;
    }


    private Node findLastNonEmptyPathNode(Path path) {

        List<Node> list = new ArrayList<>();
        for (Iterator<Node> it = path.iterator(); it.hasNext(); ) {
            list.add(it.next());
        }
        Collections.reverse(list);
        for (Node node : list) {
            if (!isEmpty(node.getName())) {
                return node;
            }
        }
        return null;
    }

    private String convertToString(Object invalidValue) {

        if (invalidValue == null) {
            return null;
        }
        try {
            return conversionService.convert(invalidValue, String.class);

        } catch (ConversionException ex) {
            return invalidValue.toString();
        }
    }

}
