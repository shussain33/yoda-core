package com.softcell.rest.exhandler.handlers;

import com.softcell.rest.exhandler.messages.ErrorMessage;
import com.softcell.rest.exhandler.messages.ValidationErrorMessage;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

/**
 * Created by prateek on 23/1/17.
 */

public class MethodArgumentNotValidExceptionHandler extends ErrorMessageRestExceptionHandler<MethodArgumentNotValidException> {


    public MethodArgumentNotValidExceptionHandler() {
        super(UNPROCESSABLE_ENTITY);
    }

    @Override
    public ValidationErrorMessage createBody(MethodArgumentNotValidException ex, HttpServletRequest req) {

        ErrorMessage tmpl = super.createBody(ex, req);
        ValidationErrorMessage msg = new ValidationErrorMessage(tmpl);

        BindingResult result = ex.getBindingResult();

        for (ObjectError err : result.getGlobalErrors()) {
            msg.addError(err.getDefaultMessage());
        }
        for (FieldError err : result.getFieldErrors()) {
            msg.addError(err.getField(), err.getRejectedValue(), err.getDefaultMessage());
        }
        return msg;
    }

}
