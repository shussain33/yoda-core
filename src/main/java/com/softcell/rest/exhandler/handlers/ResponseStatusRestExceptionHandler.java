package com.softcell.rest.exhandler.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by prateek on 23/1/17.
 */
public class ResponseStatusRestExceptionHandler implements RestExceptionHandler<Exception, Void> {

    private final HttpStatus status;


    public ResponseStatusRestExceptionHandler(HttpStatus status) {
        this.status = status;
    }

    public ResponseEntity<Void> handleException(Exception ex, HttpServletRequest request) {
        return new ResponseEntity<>(status);
    }
}
