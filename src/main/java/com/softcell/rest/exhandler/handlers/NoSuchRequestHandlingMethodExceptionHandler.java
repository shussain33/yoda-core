package com.softcell.rest.exhandler.handlers;

import com.softcell.rest.exhandler.messages.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Created by prateek on 23/1/17.
 */
public class NoSuchRequestHandlingMethodExceptionHandler extends ErrorMessageRestExceptionHandler<NoSuchRequestHandlingMethodException> {

    private static final Logger logger = LoggerFactory.getLogger(DispatcherServlet.PAGE_NOT_FOUND_LOG_CATEGORY);


    public NoSuchRequestHandlingMethodExceptionHandler() {
        super(NOT_FOUND);
    }

    @Override
    public ResponseEntity<ErrorMessage> handleException(NoSuchRequestHandlingMethodException ex, HttpServletRequest req) {

        logger.warn(ex.getMessage());
        return super.handleException(ex, req);
    }
}
