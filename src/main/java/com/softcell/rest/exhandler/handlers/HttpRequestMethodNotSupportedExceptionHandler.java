package com.softcell.rest.exhandler.handlers;

import com.softcell.rest.exhandler.messages.ErrorMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.util.ObjectUtils.isEmpty;

/**
 * Created by prateek on 23/1/17.
 */
public class HttpRequestMethodNotSupportedExceptionHandler extends ErrorMessageRestExceptionHandler<HttpRequestMethodNotSupportedException> {

    private static final Logger logger = LoggerFactory.getLogger(DispatcherServlet.PAGE_NOT_FOUND_LOG_CATEGORY);

    public HttpRequestMethodNotSupportedExceptionHandler() {
        super(METHOD_NOT_ALLOWED);
    }

    @Override
    public ResponseEntity<ErrorMessage> handleException(HttpRequestMethodNotSupportedException ex, HttpServletRequest req) {
        logger.warn(ex.getMessage());

        return super.handleException(ex, req);
    }

    @Override
    protected HttpHeaders createHeaders(HttpRequestMethodNotSupportedException ex, HttpServletRequest req) {

        HttpHeaders headers = super.createHeaders(ex, req);

        if (!isEmpty(ex.getSupportedMethods())) {
            headers.setAllow(ex.getSupportedHttpMethods());
        }
        return headers;
    }
}
