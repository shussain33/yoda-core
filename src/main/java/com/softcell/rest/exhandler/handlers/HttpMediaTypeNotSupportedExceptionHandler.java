package com.softcell.rest.exhandler.handlers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.HttpMediaTypeNotSupportedException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Created by prateek on 23/1/17.
 */
public class HttpMediaTypeNotSupportedExceptionHandler extends ErrorMessageRestExceptionHandler<HttpMediaTypeNotSupportedException> {

    public HttpMediaTypeNotSupportedExceptionHandler() {
        super(UNSUPPORTED_MEDIA_TYPE);
    }

    @Override
    protected HttpHeaders createHeaders(HttpMediaTypeNotSupportedException ex, HttpServletRequest req) {

        HttpHeaders headers = super.createHeaders(ex, req);
        List<MediaType> mediaTypes = ex.getSupportedMediaTypes();

        if (!isEmpty(mediaTypes)) {
            headers.setAccept(mediaTypes);
        }
        return headers;
    }
}
