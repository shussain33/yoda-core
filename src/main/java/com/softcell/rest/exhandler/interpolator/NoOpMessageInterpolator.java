package com.softcell.rest.exhandler.interpolator;

import java.util.Map;

/**
 * Implementation of the {@link MessageInterpolator} that does nothing, just returns the given
 * message template as-is.
 * <p/>
 * Created by prateek on 23/1/17.
 */
public class NoOpMessageInterpolator implements MessageInterpolator {


    @Override
    public String interpolate(String messageTemplate, Map<String, Object> variables) {
        return messageTemplate;
    }
}
