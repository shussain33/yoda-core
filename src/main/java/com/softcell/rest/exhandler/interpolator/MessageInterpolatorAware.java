package com.softcell.rest.exhandler.interpolator;

import org.springframework.beans.factory.Aware;

/**
 * Interface to be implemented by any object that wishes to be notified
 * of the {@link MessageInterpolator} to use.
 * <p/>
 * <p/>
 * Created by prateek on 23/1/17.
 */
public interface MessageInterpolatorAware extends Aware {

    void setMessageInterpolator(MessageInterpolator messageInterpolator);
}
